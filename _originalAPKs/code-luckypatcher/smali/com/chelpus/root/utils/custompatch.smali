.class public Lcom/chelpus/root/utils/custompatch;
.super Ljava/lang/Object;
.source "custompatch.java"


# static fields
.field public static ART:Z = false

.field static final BUFFER:I = 0x800

.field public static final MAGIC:[B

.field public static OdexPatch:Z = false

.field public static adler:I = 0x0

.field public static armv7:Z = false

.field public static arrayFile2:Ljava/util/ArrayList; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private static final beginTag:I = 0x0

.field public static classesFiles:Ljava/util/ArrayList; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private static final classesTag:I = 0x1

.field public static convert:Z = false

.field private static dataBase:Ljava/lang/String; = null

.field private static dataBaseExist:Z = false

.field public static dir:Ljava/lang/String; = null

.field public static dir2:Ljava/lang/String; = null

.field public static dirapp:Ljava/lang/String; = null

.field private static final endTag:I = 0x4

.field private static final fileInApkTag:I = 0xe

.field public static fixunpack:Z = false

.field public static goodResult:Z = false

.field private static group:Ljava/lang/String; = null

.field private static final libTagALL:I = 0x2

.field private static final libTagARMEABI:I = 0x6

.field private static final libTagARMEABIV7A:I = 0x7

.field private static final libTagMIPS:I = 0x8

.field private static final libTagx86:I = 0x9

.field public static localFile2:Ljava/io/File; = null

.field public static log:Ljava/lang/String; = null

.field public static manualpatch:Z = false

.field public static multidex:Z = false

.field public static multilib_patch:Z = false

.field public static odex:Z = false

.field private static final odexTag:I = 0xa

.field public static odexpatch:Z = false

.field private static final odexpatchTag:I = 0xb

.field private static final otherfilesTag:I = 0x3

.field private static final packageTag:I = 0x5

.field private static pat:Ljava/util/ArrayList; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;",
            ">;"
        }
    .end annotation
.end field

.field public static patchteil:Z = false

.field public static pkgName:Ljava/lang/String; = null

.field public static sddir:Ljava/lang/String; = null

.field private static search:Ljava/util/ArrayList; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Byte;",
            ">;"
        }
    .end annotation
.end field

.field private static searchStr:Ljava/lang/String; = null

.field private static ser:Ljava/util/ArrayList; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;",
            ">;"
        }
    .end annotation
.end field

.field private static final set_copy_file_Tag:I = 0xf

.field private static final set_permissions_Tag:I = 0xd

.field private static final sqlTag:I = 0xc

.field public static system:Z

.field public static tag:I

.field public static uid:Ljava/lang/String;

.field public static unpack:Z

.field private static withFramework:Z


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 37
    const/16 v0, 0x8

    new-array v0, v0, [B

    fill-array-data v0, :array_6a

    sput-object v0, Lcom/chelpus/root/utils/custompatch;->MAGIC:[B

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/chelpus/root/utils/custompatch;->arrayFile2:Ljava/util/ArrayList;

    .line 41
    sput-object v2, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    .line 42
    sput-object v2, Lcom/chelpus/root/utils/custompatch;->ser:Ljava/util/ArrayList;

    .line 43
    sput-object v2, Lcom/chelpus/root/utils/custompatch;->search:Ljava/util/ArrayList;

    .line 44
    sput-boolean v1, Lcom/chelpus/root/utils/custompatch;->patchteil:Z

    .line 45
    sput-boolean v1, Lcom/chelpus/root/utils/custompatch;->unpack:Z

    .line 46
    sput-boolean v1, Lcom/chelpus/root/utils/custompatch;->fixunpack:Z

    .line 47
    sput-boolean v1, Lcom/chelpus/root/utils/custompatch;->manualpatch:Z

    .line 48
    sput-boolean v1, Lcom/chelpus/root/utils/custompatch;->odex:Z

    .line 49
    const-string v0, "/sdcard/"

    sput-object v0, Lcom/chelpus/root/utils/custompatch;->dir:Ljava/lang/String;

    .line 50
    const-string v0, "/sdcard/"

    sput-object v0, Lcom/chelpus/root/utils/custompatch;->dir2:Ljava/lang/String;

    .line 51
    const-string v0, "/data/app/"

    sput-object v0, Lcom/chelpus/root/utils/custompatch;->dirapp:Ljava/lang/String;

    .line 52
    const-string v0, "/data/app/"

    sput-object v0, Lcom/chelpus/root/utils/custompatch;->sddir:Ljava/lang/String;

    .line 53
    const-string v0, ""

    sput-object v0, Lcom/chelpus/root/utils/custompatch;->uid:Ljava/lang/String;

    .line 54
    sput-boolean v1, Lcom/chelpus/root/utils/custompatch;->system:Z

    .line 55
    sput-boolean v1, Lcom/chelpus/root/utils/custompatch;->odexpatch:Z

    .line 56
    sput-boolean v1, Lcom/chelpus/root/utils/custompatch;->OdexPatch:Z

    .line 57
    sput-boolean v1, Lcom/chelpus/root/utils/custompatch;->armv7:Z

    .line 58
    sput-boolean v1, Lcom/chelpus/root/utils/custompatch;->ART:Z

    .line 59
    sput-boolean v1, Lcom/chelpus/root/utils/custompatch;->convert:Z

    .line 78
    sput-boolean v1, Lcom/chelpus/root/utils/custompatch;->dataBaseExist:Z

    .line 79
    const-string v0, ""

    sput-object v0, Lcom/chelpus/root/utils/custompatch;->dataBase:Ljava/lang/String;

    .line 80
    const-string v0, ""

    sput-object v0, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    .line 81
    const-string v0, ""

    sput-object v0, Lcom/chelpus/root/utils/custompatch;->group:Ljava/lang/String;

    .line 82
    const/4 v0, 0x1

    sput-boolean v0, Lcom/chelpus/root/utils/custompatch;->withFramework:Z

    .line 83
    const-string v0, ""

    sput-object v0, Lcom/chelpus/root/utils/custompatch;->pkgName:Ljava/lang/String;

    .line 84
    const-string v0, ""

    sput-object v0, Lcom/chelpus/root/utils/custompatch;->log:Ljava/lang/String;

    .line 88
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/chelpus/root/utils/custompatch;->classesFiles:Ljava/util/ArrayList;

    .line 89
    sput-boolean v1, Lcom/chelpus/root/utils/custompatch;->multidex:Z

    .line 90
    sput-boolean v1, Lcom/chelpus/root/utils/custompatch;->goodResult:Z

    .line 91
    sput-boolean v1, Lcom/chelpus/root/utils/custompatch;->multilib_patch:Z

    return-void

    .line 37
    nop

    :array_6a
    .array-data 1
        0x64t
        0x65t
        0x79t
        0xat
        0x30t
        0x33t
        0x35t
        0x0t
    .end array-data
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static addToLog(Ljava/lang/String;)V
    .registers 3
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 86
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/chelpus/root/utils/custompatch;->log:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/chelpus/root/utils/custompatch;->log:Ljava/lang/String;

    .line 87
    return-void
.end method

.method public static clearTemp()V
    .registers 7

    .prologue
    .line 1898
    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/chelpus/root/utils/custompatch;->dir:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/AndroidManifest.xml"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1899
    .local v3, "tmp":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1900
    .local v2, "tempdex":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_23

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 1901
    :cond_23
    sget-object v4, Lcom/chelpus/root/utils/custompatch;->classesFiles:Ljava/util/ArrayList;

    if-eqz v4, :cond_89

    sget-object v4, Lcom/chelpus/root/utils/custompatch;->classesFiles:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_89

    .line 1902
    sget-object v4, Lcom/chelpus/root/utils/custompatch;->classesFiles:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_35
    :goto_35
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_89

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 1903
    .local v0, "cl":Ljava/io/File;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/chelpus/root/utils/custompatch;->dir:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1904
    new-instance v2, Ljava/io/File;

    .end local v2    # "tempdex":Ljava/io/File;
    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1905
    .restart local v2    # "tempdex":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_35

    invoke-virtual {v2}, Ljava/io/File;->delete()Z
    :try_end_6c
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_6c} :catch_6d

    goto :goto_35

    .line 1936
    .end local v0    # "cl":Ljava/io/File;
    .end local v2    # "tempdex":Ljava/io/File;
    :catch_6d
    move-exception v1

    .line 1938
    .local v1, "e":Ljava/lang/Exception;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 1940
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_88
    :goto_88
    return-void

    .line 1908
    .restart local v2    # "tempdex":Ljava/io/File;
    :cond_89
    :try_start_89
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/chelpus/root/utils/custompatch;->dir:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/classes.dex"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1909
    new-instance v2, Ljava/io/File;

    .end local v2    # "tempdex":Ljava/io/File;
    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1910
    .restart local v2    # "tempdex":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_ac

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 1911
    :cond_ac
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/chelpus/root/utils/custompatch;->dir:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/classes1.dex"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1912
    new-instance v2, Ljava/io/File;

    .end local v2    # "tempdex":Ljava/io/File;
    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1913
    .restart local v2    # "tempdex":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_cf

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 1914
    :cond_cf
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/chelpus/root/utils/custompatch;->dir:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/classes2.dex"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1915
    new-instance v2, Ljava/io/File;

    .end local v2    # "tempdex":Ljava/io/File;
    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1916
    .restart local v2    # "tempdex":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_f2

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 1917
    :cond_f2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/chelpus/root/utils/custompatch;->dir:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/classes3.dex"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1918
    new-instance v2, Ljava/io/File;

    .end local v2    # "tempdex":Ljava/io/File;
    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1919
    .restart local v2    # "tempdex":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_115

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 1920
    :cond_115
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/chelpus/root/utils/custompatch;->dir:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/classes4.dex"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1921
    new-instance v2, Ljava/io/File;

    .end local v2    # "tempdex":Ljava/io/File;
    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1922
    .restart local v2    # "tempdex":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_138

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 1923
    :cond_138
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/chelpus/root/utils/custompatch;->dir:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/classes5.dex"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1924
    new-instance v2, Ljava/io/File;

    .end local v2    # "tempdex":Ljava/io/File;
    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1925
    .restart local v2    # "tempdex":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_15b

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 1926
    :cond_15b
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/chelpus/root/utils/custompatch;->dir:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/classes6.dex"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1927
    new-instance v2, Ljava/io/File;

    .end local v2    # "tempdex":Ljava/io/File;
    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1928
    .restart local v2    # "tempdex":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_17e

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 1929
    :cond_17e
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/chelpus/root/utils/custompatch;->dir:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/classes.dex.apk"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1930
    new-instance v2, Ljava/io/File;

    .end local v2    # "tempdex":Ljava/io/File;
    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1931
    .restart local v2    # "tempdex":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_1a1

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 1932
    :cond_1a1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/chelpus/root/utils/custompatch;->dir:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/classes.dex.dex"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1933
    new-instance v2, Ljava/io/File;

    .end local v2    # "tempdex":Ljava/io/File;
    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1934
    .restart local v2    # "tempdex":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_88

    invoke-virtual {v2}, Ljava/io/File;->delete()Z
    :try_end_1c4
    .catch Ljava/lang/Exception; {:try_start_89 .. :try_end_1c4} :catch_6d

    goto/16 :goto_88
.end method

.method public static main([Ljava/lang/String;)V
    .registers 74
    .param p0, "paramArrayOfString"    # [Ljava/lang/String;

    .prologue
    .line 98
    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "SU Java-Code Running! "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    new-instance v7, Lcom/chelpus/root/utils/custompatch$1;

    invoke-direct {v7}, Lcom/chelpus/root/utils/custompatch$1;-><init>()V

    .line 99
    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getEnclosingClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 98
    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 101
    const/4 v2, 0x0

    invoke-static {v2}, Lcom/chelpus/Utils;->startRootJava(Ljava/lang/Object;)V

    .line 102
    const/4 v2, 0x0

    aget-object v2, p0, v2

    sput-object v2, Lcom/chelpus/root/utils/custompatch;->pkgName:Ljava/lang/String;
    :try_end_30
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_30} :catch_c0

    .line 104
    :try_start_30
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->pkgName:Ljava/lang/String;

    invoke-static {v2}, Lcom/chelpus/Utils;->kill(Ljava/lang/String;)V

    .line 105
    const/4 v2, 0x2

    aget-object v2, p0, v2

    const-string v7, "rw"

    invoke-static {v2, v7}, Lcom/chelpus/Utils;->remount(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_3d
    .catch Ljava/lang/Exception; {:try_start_30 .. :try_end_3d} :catch_ba

    .line 120
    :goto_3d
    const/16 v2, 0x9

    :try_start_3f
    aget-object v2, p0, v2

    if-eqz v2, :cond_52

    const/16 v2, 0x9

    aget-object v2, p0, v2

    const-string v7, "ART"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_52

    const/4 v2, 0x1

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->ART:Z

    .line 121
    :cond_52
    const/16 v2, 0xa

    aget-object v2, p0, v2

    if-eqz v2, :cond_5e

    const/16 v2, 0xa

    aget-object v2, p0, v2

    sput-object v2, Lcom/chelpus/root/utils/custompatch;->uid:Ljava/lang/String;

    .line 122
    :cond_5e
    const/16 v2, 0xb

    aget-object v2, p0, v2

    if-eqz v2, :cond_70

    const/16 v2, 0xb

    aget-object v2, p0, v2

    const-string v7, "yes"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->withFramework:Z
    :try_end_70
    .catch Ljava/lang/Exception; {:try_start_3f .. :try_end_70} :catch_d4

    .line 127
    :cond_70
    :goto_70
    :try_start_70
    new-instance v2, Ljava/io/File;

    const/4 v7, 0x4

    aget-object v7, p0, v7

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v34

    .line 128
    .local v34, "files":[Ljava/io/File;
    move-object/from16 v0, v34

    array-length v7, v0

    const/4 v2, 0x0

    :goto_80
    if-ge v2, v7, :cond_dd

    aget-object v32, v34, v2

    .line 129
    .local v32, "file":Ljava/io/File;
    invoke-virtual/range {v32 .. v32}, Ljava/io/File;->isFile()Z

    move-result v8

    if-eqz v8, :cond_b7

    invoke-virtual/range {v32 .. v32}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    const-string v70, "busybox"

    move-object/from16 v0, v70

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_b7

    invoke-virtual/range {v32 .. v32}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    const-string v70, "reboot"

    move-object/from16 v0, v70

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_b7

    invoke-virtual/range {v32 .. v32}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    const-string v70, "dalvikvm"

    move-object/from16 v0, v70

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_b7

    .line 130
    invoke-virtual/range {v32 .. v32}, Ljava/io/File;->delete()Z
    :try_end_b7
    .catch Ljava/lang/Exception; {:try_start_70 .. :try_end_b7} :catch_d9

    .line 128
    :cond_b7
    add-int/lit8 v2, v2, 0x1

    goto :goto_80

    .line 106
    .end local v32    # "file":Ljava/io/File;
    .end local v34    # "files":[Ljava/io/File;
    :catch_ba
    move-exception v29

    .line 107
    .local v29, "e":Ljava/lang/Exception;
    :try_start_bb
    invoke-virtual/range {v29 .. v29}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_be
    .catch Ljava/lang/Exception; {:try_start_bb .. :try_end_be} :catch_c0

    goto/16 :goto_3d

    .line 1187
    .end local v29    # "e":Ljava/lang/Exception;
    :catch_c0
    move-exception v29

    .restart local v29    # "e":Ljava/lang/Exception;
    invoke-virtual/range {v29 .. v29}, Ljava/lang/Exception;->printStackTrace()V

    .line 1189
    .end local v29    # "e":Ljava/lang/Exception;
    :goto_c4
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->log:Ljava/lang/String;

    invoke-static {v2}, Lcom/chelpus/Utils;->sendFromRootCP(Ljava/lang/String;)Z

    .line 1192
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    sput-object v2, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->startUnderRoot:Ljava/lang/Boolean;

    .line 1193
    invoke-static {}, Lcom/chelpus/Utils;->exitFromRootJava()V

    .line 1194
    return-void

    .line 123
    :catch_d4
    move-exception v29

    .line 124
    .restart local v29    # "e":Ljava/lang/Exception;
    :try_start_d5
    invoke-virtual/range {v29 .. v29}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_70

    .line 132
    .end local v29    # "e":Ljava/lang/Exception;
    :catch_d9
    move-exception v29

    .line 133
    .restart local v29    # "e":Ljava/lang/Exception;
    invoke-virtual/range {v29 .. v29}, Ljava/lang/Exception;->printStackTrace()V

    .line 135
    .end local v29    # "e":Ljava/lang/Exception;
    :cond_dd
    const/4 v2, 0x3

    aget-object v2, p0, v2

    sput-object v2, Lcom/chelpus/root/utils/custompatch;->sddir:Ljava/lang/String;

    .line 136
    const/4 v2, 0x4

    aget-object v2, p0, v2

    sput-object v2, Lcom/chelpus/root/utils/custompatch;->dir:Ljava/lang/String;

    .line 137
    const/4 v2, 0x4

    aget-object v2, p0, v2

    sput-object v2, Lcom/chelpus/root/utils/custompatch;->dir2:Ljava/lang/String;

    .line 138
    const/4 v2, 0x2

    aget-object v2, p0, v2

    sput-object v2, Lcom/chelpus/root/utils/custompatch;->dirapp:Ljava/lang/String;

    .line 139
    invoke-static {}, Lcom/chelpus/root/utils/custompatch;->clearTemp()V

    .line 140
    const-string v22, ""

    .line 141
    .local v22, "dalvikfixfile":Ljava/lang/String;
    const-string v35, ""

    .line 142
    .local v35, "finalText":Ljava/lang/String;
    const-string v15, ""

    .line 144
    .local v15, "beginText":Ljava/lang/String;
    const/16 v31, 0x0

    .local v31, "error":Z
    const/16 v30, 0x0

    .local v30, "end":Z
    const/16 v21, 0x0

    .line 145
    .local v21, "dalvikfix":Z
    const/4 v2, 0x6

    aget-object v2, p0, v2

    const-string v7, "not_system"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10e

    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->system:Z

    .line 146
    :cond_10e
    const/4 v2, 0x6

    aget-object v2, p0, v2

    const-string v7, "system"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11c

    const/4 v2, 0x1

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->system:Z

    .line 147
    :cond_11c
    sget-boolean v2, Lcom/chelpus/root/utils/custompatch;->system:Z

    if-eqz v2, :cond_14f

    .line 148
    new-instance v10, Ljava/io/File;

    sget-object v2, Lcom/chelpus/root/utils/custompatch;->dirapp:Ljava/lang/String;

    invoke-direct {v10, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 149
    .local v10, "appapk":Ljava/io/File;
    new-instance v11, Ljava/io/File;

    sget-object v2, Lcom/chelpus/root/utils/custompatch;->dirapp:Ljava/lang/String;

    const/4 v7, 0x1

    invoke-static {v2, v7}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v11, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 150
    .local v11, "appodex":Ljava/io/File;
    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_14f

    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_14f

    invoke-static {v10}, Lcom/chelpus/Utils;->classes_test(Ljava/io/File;)Z

    move-result v2

    if-nez v2, :cond_14f

    .line 151
    const/4 v2, 0x1

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->odexpatch:Z

    .line 152
    sput-object v11, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 153
    const-string v2, "\nOdex Application.\nOnly ODEX patch is enabled.\n"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 158
    .end local v10    # "appapk":Ljava/io/File;
    .end local v11    # "appodex":Ljava/io/File;
    :cond_14f
    const-string v44, ""

    .line 159
    .local v44, "line":Ljava/lang/String;
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->OdexPatch:Z
    :try_end_154
    .catch Ljava/lang/Exception; {:try_start_d5 .. :try_end_154} :catch_c0

    .line 161
    :try_start_154
    new-instance v37, Ljava/io/FileInputStream;

    const/4 v2, 0x1

    aget-object v2, p0, v2

    move-object/from16 v0, v37

    invoke-direct {v0, v2}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 162
    .local v37, "fis1":Ljava/io/FileInputStream;
    new-instance v39, Ljava/io/InputStreamReader;

    const-string v2, "UTF-8"

    move-object/from16 v0, v39

    move-object/from16 v1, v37

    invoke-direct {v0, v1, v2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 163
    .local v39, "isr1":Ljava/io/InputStreamReader;
    new-instance v17, Ljava/io/BufferedReader;

    move-object/from16 v0, v17

    move-object/from16 v1, v39

    invoke-direct {v0, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 164
    .local v17, "br1":Ljava/io/BufferedReader;
    :cond_172
    :goto_172
    invoke-virtual/range {v17 .. v17}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v44

    if-eqz v44, :cond_905

    .line 165
    invoke-virtual/range {v44 .. v44}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "[ODEX-PATCH]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_187

    const/4 v2, 0x1

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->OdexPatch:Z

    .line 166
    :cond_187
    invoke-virtual/range {v44 .. v44}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "[LIB-ARMEABI-V7A]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_172

    const/4 v2, 0x1

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->armv7:Z
    :try_end_196
    .catch Ljava/io/IOException; {:try_start_154 .. :try_end_196} :catch_197
    .catch Ljava/lang/Exception; {:try_start_154 .. :try_end_196} :catch_c0

    goto :goto_172

    .line 171
    .end local v17    # "br1":Ljava/io/BufferedReader;
    .end local v37    # "fis1":Ljava/io/FileInputStream;
    .end local v39    # "isr1":Ljava/io/InputStreamReader;
    :catch_197
    move-exception v2

    .line 174
    :goto_198
    const/4 v2, 0x2

    :try_start_199
    aget-object v2, p0, v2

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->searchDalvik(Ljava/lang/String;)V
    :try_end_19e
    .catch Ljava/lang/Exception; {:try_start_199 .. :try_end_19e} :catch_c0

    .line 178
    :try_start_19e
    new-instance v36, Ljava/io/FileInputStream;

    const/4 v2, 0x1

    aget-object v2, p0, v2

    move-object/from16 v0, v36

    invoke-direct {v0, v2}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 179
    .local v36, "fis":Ljava/io/FileInputStream;
    new-instance v38, Ljava/io/InputStreamReader;

    const-string v2, "UTF-8"

    move-object/from16 v0, v38

    move-object/from16 v1, v36

    invoke-direct {v0, v1, v2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 180
    .local v38, "isr":Ljava/io/InputStreamReader;
    new-instance v16, Ljava/io/BufferedReader;

    move-object/from16 v0, v16

    move-object/from16 v1, v38

    invoke-direct {v0, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 181
    .local v16, "br":Ljava/io/BufferedReader;
    const-string v23, ""

    .line 182
    .local v23, "data":Ljava/lang/String;
    const/16 v2, 0x7d0

    new-array v0, v2, [Ljava/lang/String;

    move-object/from16 v65, v0

    .line 183
    .local v65, "txtdata":[Ljava/lang/String;
    const/4 v2, 0x1

    new-array v0, v2, [Ljava/lang/String;

    move-object/from16 v52, v0

    const/4 v2, 0x0

    const-string v7, ""

    aput-object v7, v52, v2

    .line 185
    .local v52, "orhex":[Ljava/lang/String;
    const/4 v3, 0x0

    .line 187
    .local v3, "byteOrig":[B
    const/4 v4, 0x0

    .line 189
    .local v4, "mask":[I
    const-string v33, ""

    .line 190
    .local v33, "file_name":Ljava/lang/String;
    const-string v56, ""

    .line 191
    .local v56, "permissionsSet":Ljava/lang/String;
    const-string v54, ""

    .line 192
    .local v54, "path_for_copy":Ljava/lang/String;
    const/16 v60, 0x1

    .local v60, "result":Z
    const/16 v63, 0x1

    .local v63, "sumresult":Z
    const/16 v43, 0x0

    .local v43, "libr":Z
    const/4 v14, 0x0

    .local v14, "begin":Z
    const/16 v48, 0x0

    .local v48, "mark_search":Z
    const/16 v53, 0x0

    .local v53, "other":Z
    const/16 v55, 0x0

    .local v55, "permissions":Z
    const/16 v20, 0x0

    .line 193
    .local v20, "copy_file":Z
    const-string v66, ""

    .line 194
    .local v66, "value1":Ljava/lang/String;
    const-string v67, ""

    .line 195
    .local v67, "value2":Ljava/lang/String;
    const-string v68, ""

    .line 196
    .local v68, "value3":Ljava/lang/String;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    sput-object v2, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    .line 197
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    sput-object v2, Lcom/chelpus/root/utils/custompatch;->ser:Ljava/util/ArrayList;

    .line 198
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    sput-object v2, Lcom/chelpus/root/utils/custompatch;->search:Ljava/util/ArrayList;

    .line 199
    const/16 v57, 0x0

    .line 201
    .local v57, "r":I
    :goto_202
    invoke-virtual/range {v16 .. v16}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v23

    if-eqz v23, :cond_1773

    .line 203
    const-string v2, ""

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_21a

    .line 204
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->pkgName:Ljava/lang/String;

    move-object/from16 v0, v23

    invoke-static {v0, v2}, Lcom/chelpus/Utils;->apply_TAGS(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    .line 206
    :cond_21a
    aput-object v23, v65, v57

    .line 208
    aget-object v2, v65, v57

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "[PACKAGE]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2e5

    .line 233
    const/4 v2, 0x5

    sput v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    .line 234
    const/4 v2, 0x1

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->unpack:Z

    .line 236
    new-instance v9, Ljava/io/File;

    const/4 v2, 0x2

    aget-object v2, p0, v2

    invoke-direct {v9, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 237
    .local v9, "apk":Ljava/io/File;
    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_264

    .line 238
    new-instance v9, Ljava/io/File;

    .end local v9    # "apk":Ljava/io/File;
    const/4 v2, 0x2

    aget-object v2, p0, v2

    const-string v7, "-1/"

    const-string v8, "-2/"

    invoke-virtual {v2, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v9, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 239
    .restart local v9    # "apk":Ljava/io/File;
    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_264

    .line 240
    const/4 v2, 0x2

    const/4 v7, 0x2

    aget-object v7, p0, v7

    const-string v8, "-1/"

    const-string v70, "-2/"

    move-object/from16 v0, v70

    invoke-virtual {v7, v8, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, p0, v2

    .line 243
    :cond_264
    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_290

    .line 244
    new-instance v9, Ljava/io/File;

    .end local v9    # "apk":Ljava/io/File;
    const/4 v2, 0x2

    aget-object v2, p0, v2

    const-string v7, "-1/"

    const-string v8, "/"

    invoke-virtual {v2, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v9, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 245
    .restart local v9    # "apk":Ljava/io/File;
    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_290

    .line 246
    const/4 v2, 0x2

    const/4 v7, 0x2

    aget-object v7, p0, v7

    const-string v8, "-1/"

    const-string v70, ""

    move-object/from16 v0, v70

    invoke-virtual {v7, v8, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, p0, v2

    .line 250
    :cond_290
    invoke-static {v9}, Lcom/chelpus/root/utils/custompatch;->unzip(Ljava/io/File;)V

    .line 252
    sget-boolean v2, Lcom/chelpus/root/utils/custompatch;->odexpatch:Z

    if-nez v2, :cond_2e5

    sget-boolean v2, Lcom/chelpus/root/utils/custompatch;->OdexPatch:Z

    if-nez v2, :cond_2e5

    .line 253
    const/4 v2, 0x2

    aget-object v2, p0, v2

    const/4 v7, 0x1

    invoke-static {v2, v7}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v50

    .line 254
    .local v50, "odexstr":Ljava/lang/String;
    new-instance v49, Ljava/io/File;

    invoke-direct/range {v49 .. v50}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 255
    .local v49, "odexfile":Ljava/io/File;
    invoke-virtual/range {v49 .. v49}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_2b1

    invoke-virtual/range {v49 .. v49}, Ljava/io/File;->delete()Z

    .line 256
    :cond_2b1
    new-instance v49, Ljava/io/File;

    .end local v49    # "odexfile":Ljava/io/File;
    const-string v2, "-1"

    const-string v7, "-2"

    move-object/from16 v0, v50

    invoke-virtual {v0, v2, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v49

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 257
    .restart local v49    # "odexfile":Ljava/io/File;
    invoke-virtual/range {v49 .. v49}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_2cb

    invoke-virtual/range {v49 .. v49}, Ljava/io/File;->delete()Z

    .line 258
    :cond_2cb
    new-instance v49, Ljava/io/File;

    .end local v49    # "odexfile":Ljava/io/File;
    const-string v2, "-2"

    const-string v7, "-1"

    move-object/from16 v0, v50

    invoke-virtual {v0, v2, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v49

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 259
    .restart local v49    # "odexfile":Ljava/io/File;
    invoke-virtual/range {v49 .. v49}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_2e5

    invoke-virtual/range {v49 .. v49}, Ljava/io/File;->delete()Z

    .line 264
    .end local v9    # "apk":Ljava/io/File;
    .end local v49    # "odexfile":Ljava/io/File;
    .end local v50    # "odexstr":Ljava/lang/String;
    :cond_2e5
    if-eqz v14, :cond_322

    aget-object v2, v65, v57

    const-string v7, "["

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_305

    aget-object v2, v65, v57

    const-string v7, "]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_305

    aget-object v2, v65, v57

    const-string v7, "{"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_322

    .line 265
    :cond_305
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ""

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, "\n"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 266
    const/4 v14, 0x0

    .line 268
    :cond_322
    if-eqz v14, :cond_33d

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, "\n"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v7, v65, v57

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    .line 270
    :cond_33d
    aget-object v2, v65, v57

    const-string v7, "["

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_356

    aget-object v2, v65, v57

    const-string v7, "]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_356

    .line 271
    sget v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    packed-switch v2, :pswitch_data_1820

    .line 581
    :cond_356
    :goto_356
    :pswitch_356
    aget-object v2, v65, v57

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "[BEGIN]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_368

    .line 582
    const/4 v2, 0x0

    sput v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    .line 583
    const/4 v14, 0x1

    .line 586
    :cond_368
    aget-object v2, v65, v57

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "[CLASSES]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_39f

    .line 587
    const/4 v2, 0x1

    sput v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    .line 588
    sget-boolean v2, Lcom/chelpus/root/utils/custompatch;->unpack:Z

    if-eqz v2, :cond_39f

    new-instance v2, Ljava/io/File;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Lcom/chelpus/root/utils/custompatch;->dir:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "classes.dex"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v2, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 592
    :cond_39f
    aget-object v2, v65, v57

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "[ODEX]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3b1

    .line 593
    const/16 v2, 0xa

    sput v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    .line 597
    :cond_3b1
    aget-object v2, v65, v57

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "[SQLITE]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3c3

    .line 598
    const/16 v2, 0xc

    sput v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    .line 602
    :cond_3c3
    sget v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    const/16 v7, 0xc

    if-ne v2, v7, :cond_512

    .line 603
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->ser:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 604
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 605
    aget-object v2, v65, v57

    const-string v7, "database"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_3da
    .catch Ljava/io/FileNotFoundException; {:try_start_19e .. :try_end_3da} :catch_967
    .catch Ljava/lang/Exception; {:try_start_19e .. :try_end_3da} :catch_9ff

    move-result v2

    if-eqz v2, :cond_4c5

    .line 607
    :try_start_3dd
    new-instance v40, Lorg/json/JSONObject;

    aget-object v2, v65, v57

    move-object/from16 v0, v40

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 608
    .local v40, "json_obj":Lorg/json/JSONObject;
    const-string v2, "database"

    move-object/from16 v0, v40

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v66

    .line 609
    new-instance v25, Ljava/io/File;

    move-object/from16 v0, v25

    move-object/from16 v1, v66

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 610
    .local v25, "db":Ljava/io/File;
    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "chmod"

    aput-object v8, v2, v7

    const/4 v7, 0x1

    const-string v8, "777"

    aput-object v8, v2, v7

    const/4 v7, 0x2

    aput-object v66, v2, v7

    invoke-static {v2}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 611
    sget-boolean v2, Lcom/chelpus/root/utils/custompatch;->withFramework:Z

    if-nez v2, :cond_435

    .line 612
    new-instance v2, Ljava/io/File;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Lcom/chelpus/root/utils/custompatch;->dir:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual/range {v25 .. v25}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v25

    invoke-static {v0, v2}, Lcom/chelpus/Utils;->copyFile(Ljava/io/File;Ljava/io/File;)V

    .line 613
    :cond_435
    invoke-virtual/range {v25 .. v25}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_1088

    .line 614
    const/4 v2, 0x0

    sput-object v2, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 615
    const/4 v2, 0x0

    aget-object v2, p0, v2

    invoke-virtual/range {v25 .. v25}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v7}, Lcom/chelpus/root/utils/custompatch;->searchfile(Ljava/lang/String;Ljava/lang/String;)V

    .line 616
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    if-eqz v2, :cond_1084

    sget-object v2, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1084

    .line 617
    sget-object v25, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 618
    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "chmod"

    aput-object v8, v2, v7

    const/4 v7, 0x1

    const-string v8, "777"

    aput-object v8, v2, v7

    const/4 v7, 0x2

    aput-object v66, v2, v7

    invoke-static {v2}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 619
    sget-boolean v2, Lcom/chelpus/root/utils/custompatch;->withFramework:Z

    if-nez v2, :cond_494

    .line 620
    new-instance v2, Ljava/io/File;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Lcom/chelpus/root/utils/custompatch;->dir:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual/range {v25 .. v25}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v25

    invoke-static {v0, v2}, Lcom/chelpus/Utils;->copyFile(Ljava/io/File;Ljava/io/File;)V

    .line 621
    :cond_494
    const/4 v2, 0x1

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->dataBaseExist:Z

    .line 624
    :goto_497
    sget-boolean v2, Lcom/chelpus/root/utils/custompatch;->dataBaseExist:Z

    if-eqz v2, :cond_4a1

    invoke-virtual/range {v25 .. v25}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/chelpus/root/utils/custompatch;->dataBase:Ljava/lang/String;

    .line 625
    :cond_4a1
    const-string v2, "---------------------------"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 626
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Open SqLite database\n"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {v25 .. v25}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 627
    const-string v2, "---------------------------\n"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V
    :try_end_4c5
    .catch Lorg/json/JSONException; {:try_start_3dd .. :try_end_4c5} :catch_108d
    .catch Ljava/io/FileNotFoundException; {:try_start_3dd .. :try_end_4c5} :catch_967
    .catch Ljava/lang/Exception; {:try_start_3dd .. :try_end_4c5} :catch_9ff

    .line 632
    .end local v25    # "db":Ljava/io/File;
    .end local v40    # "json_obj":Lorg/json/JSONObject;
    :cond_4c5
    :goto_4c5
    :try_start_4c5
    aget-object v2, v65, v57

    const-string v7, "execute"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_512

    sget-boolean v2, Lcom/chelpus/root/utils/custompatch;->dataBaseExist:Z
    :try_end_4d1
    .catch Ljava/io/FileNotFoundException; {:try_start_4c5 .. :try_end_4d1} :catch_967
    .catch Ljava/lang/Exception; {:try_start_4c5 .. :try_end_4d1} :catch_9ff

    if-eqz v2, :cond_512

    .line 634
    :try_start_4d3
    new-instance v40, Lorg/json/JSONObject;

    aget-object v2, v65, v57

    move-object/from16 v0, v40

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 635
    .restart local v40    # "json_obj":Lorg/json/JSONObject;
    const-string v2, "execute"

    move-object/from16 v0, v40

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v66

    .line 636
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Execute:\n"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v66

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 637
    sget-boolean v2, Lcom/chelpus/root/utils/custompatch;->withFramework:Z
    :try_end_4fe
    .catch Lorg/json/JSONException; {:try_start_4d3 .. :try_end_4fe} :catch_10b2
    .catch Ljava/io/FileNotFoundException; {:try_start_4d3 .. :try_end_4fe} :catch_967
    .catch Ljava/lang/Exception; {:try_start_4d3 .. :try_end_4fe} :catch_9ff

    if-eqz v2, :cond_512

    .line 639
    :try_start_500
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->dataBase:Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-static {v2, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->openDatabase(Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v26

    .line 640
    .local v26, "db3":Landroid/database/sqlite/SQLiteDatabase;
    move-object/from16 v0, v26

    move-object/from16 v1, v66

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 642
    invoke-virtual/range {v26 .. v26}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_512
    .catch Ljava/lang/Exception; {:try_start_500 .. :try_end_512} :catch_1095
    .catch Lorg/json/JSONException; {:try_start_500 .. :try_end_512} :catch_10b2
    .catch Ljava/io/FileNotFoundException; {:try_start_500 .. :try_end_512} :catch_967

    .line 653
    .end local v26    # "db3":Landroid/database/sqlite/SQLiteDatabase;
    .end local v40    # "json_obj":Lorg/json/JSONObject;
    :cond_512
    :goto_512
    if-eqz v53, :cond_561

    .line 654
    :try_start_514
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->ser:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 655
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V
    :try_end_51e
    .catch Ljava/io/FileNotFoundException; {:try_start_514 .. :try_end_51e} :catch_967
    .catch Ljava/lang/Exception; {:try_start_514 .. :try_end_51e} :catch_9ff

    .line 657
    :try_start_51e
    new-instance v40, Lorg/json/JSONObject;

    aget-object v2, v65, v57

    move-object/from16 v0, v40

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 658
    .restart local v40    # "json_obj":Lorg/json/JSONObject;
    const-string v2, "name"

    move-object/from16 v0, v40

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v66

    .line 659
    const-string v2, "---------------------------"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 660
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Patch for file \n"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v66

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, ":"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 661
    const-string v2, "---------------------------\n"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V
    :try_end_557
    .catch Lorg/json/JSONException; {:try_start_51e .. :try_end_557} :catch_10ba
    .catch Ljava/io/FileNotFoundException; {:try_start_51e .. :try_end_557} :catch_967
    .catch Ljava/lang/Exception; {:try_start_51e .. :try_end_557} :catch_9ff

    .line 665
    .end local v40    # "json_obj":Lorg/json/JSONObject;
    :goto_557
    const/4 v2, 0x0

    :try_start_558
    aget-object v2, p0, v2

    move-object/from16 v0, v66

    invoke-static {v2, v0}, Lcom/chelpus/root/utils/custompatch;->searchfile(Ljava/lang/String;Ljava/lang/String;)V

    .line 666
    const/16 v53, 0x0

    .line 668
    :cond_561
    if-eqz v55, :cond_5bf

    .line 669
    aget-object v2, v65, v57

    const-string v7, "file_name"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_56a
    .catch Ljava/io/FileNotFoundException; {:try_start_558 .. :try_end_56a} :catch_967
    .catch Ljava/lang/Exception; {:try_start_558 .. :try_end_56a} :catch_9ff

    move-result v2

    if-eqz v2, :cond_586

    .line 671
    :try_start_56d
    new-instance v40, Lorg/json/JSONObject;

    aget-object v2, v65, v57

    move-object/from16 v0, v40

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 672
    .restart local v40    # "json_obj":Lorg/json/JSONObject;
    const-string v2, "file_name"

    move-object/from16 v0, v40

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_57d
    .catch Lorg/json/JSONException; {:try_start_56d .. :try_end_57d} :catch_10c2
    .catch Ljava/io/FileNotFoundException; {:try_start_56d .. :try_end_57d} :catch_967
    .catch Ljava/lang/Exception; {:try_start_56d .. :try_end_57d} :catch_9ff

    move-result-object v33

    .line 677
    .end local v40    # "json_obj":Lorg/json/JSONObject;
    :goto_57e
    const/4 v2, 0x0

    :try_start_57f
    aget-object v2, p0, v2

    move-object/from16 v0, v33

    invoke-static {v2, v0}, Lcom/chelpus/root/utils/custompatch;->searchfile(Ljava/lang/String;Ljava/lang/String;)V

    .line 679
    :cond_586
    aget-object v2, v65, v57

    const-string v7, "permissions"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5bf

    aget-object v2, v65, v57

    const-string v7, "{"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5bf

    aget-object v2, v65, v57

    const-string v7, "["

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5bf

    aget-object v2, v65, v57

    const-string v7, "]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_5ab
    .catch Ljava/io/FileNotFoundException; {:try_start_57f .. :try_end_5ab} :catch_967
    .catch Ljava/lang/Exception; {:try_start_57f .. :try_end_5ab} :catch_9ff

    move-result v2

    if-nez v2, :cond_5bf

    .line 681
    :try_start_5ae
    new-instance v40, Lorg/json/JSONObject;

    aget-object v2, v65, v57

    move-object/from16 v0, v40

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 682
    .restart local v40    # "json_obj":Lorg/json/JSONObject;
    const-string v2, "permissions"

    move-object/from16 v0, v40

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_5be
    .catch Lorg/json/JSONException; {:try_start_5ae .. :try_end_5be} :catch_10cd
    .catch Ljava/io/FileNotFoundException; {:try_start_5ae .. :try_end_5be} :catch_967
    .catch Ljava/lang/Exception; {:try_start_5ae .. :try_end_5be} :catch_9ff

    move-result-object v56

    .line 688
    .end local v40    # "json_obj":Lorg/json/JSONObject;
    :cond_5bf
    :goto_5bf
    if-eqz v20, :cond_615

    .line 689
    :try_start_5c1
    aget-object v2, v65, v57

    const-string v7, "file_name"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_5c8
    .catch Ljava/io/FileNotFoundException; {:try_start_5c1 .. :try_end_5c8} :catch_967
    .catch Ljava/lang/Exception; {:try_start_5c1 .. :try_end_5c8} :catch_9ff

    move-result v2

    if-eqz v2, :cond_5dc

    .line 691
    :try_start_5cb
    new-instance v40, Lorg/json/JSONObject;

    aget-object v2, v65, v57

    move-object/from16 v0, v40

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 692
    .restart local v40    # "json_obj":Lorg/json/JSONObject;
    const-string v2, "file_name"

    move-object/from16 v0, v40

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_5db
    .catch Lorg/json/JSONException; {:try_start_5cb .. :try_end_5db} :catch_10d5
    .catch Ljava/io/FileNotFoundException; {:try_start_5cb .. :try_end_5db} :catch_967
    .catch Ljava/lang/Exception; {:try_start_5cb .. :try_end_5db} :catch_9ff

    move-result-object v33

    .line 699
    .end local v40    # "json_obj":Lorg/json/JSONObject;
    :cond_5dc
    :goto_5dc
    :try_start_5dc
    aget-object v2, v65, v57

    const-string v7, "\"to\":"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_615

    aget-object v2, v65, v57

    const-string v7, "{"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_615

    aget-object v2, v65, v57

    const-string v7, "["

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_615

    aget-object v2, v65, v57

    const-string v7, "]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_601
    .catch Ljava/io/FileNotFoundException; {:try_start_5dc .. :try_end_601} :catch_967
    .catch Ljava/lang/Exception; {:try_start_5dc .. :try_end_601} :catch_9ff

    move-result v2

    if-nez v2, :cond_615

    .line 701
    :try_start_604
    new-instance v40, Lorg/json/JSONObject;

    aget-object v2, v65, v57

    move-object/from16 v0, v40

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 702
    .restart local v40    # "json_obj":Lorg/json/JSONObject;
    const-string v2, "to"

    move-object/from16 v0, v40

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_614
    .catch Lorg/json/JSONException; {:try_start_604 .. :try_end_614} :catch_10e0
    .catch Ljava/io/FileNotFoundException; {:try_start_604 .. :try_end_614} :catch_967
    .catch Ljava/lang/Exception; {:try_start_604 .. :try_end_614} :catch_9ff

    move-result-object v54

    .line 708
    .end local v40    # "json_obj":Lorg/json/JSONObject;
    :cond_615
    :goto_615
    if-eqz v43, :cond_647

    .line 709
    :try_start_617
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->ser:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 710
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V
    :try_end_621
    .catch Ljava/io/FileNotFoundException; {:try_start_617 .. :try_end_621} :catch_967
    .catch Ljava/lang/Exception; {:try_start_617 .. :try_end_621} :catch_9ff

    .line 712
    :try_start_621
    new-instance v40, Lorg/json/JSONObject;

    aget-object v2, v65, v57

    move-object/from16 v0, v40

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 713
    .restart local v40    # "json_obj":Lorg/json/JSONObject;
    const-string v2, "name"

    move-object/from16 v0, v40

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_631
    .catch Lorg/json/JSONException; {:try_start_621 .. :try_end_631} :catch_10e8
    .catch Ljava/io/FileNotFoundException; {:try_start_621 .. :try_end_631} :catch_967
    .catch Ljava/lang/Exception; {:try_start_621 .. :try_end_631} :catch_9ff

    move-result-object v66

    .line 717
    .end local v40    # "json_obj":Lorg/json/JSONObject;
    :goto_632
    :try_start_632
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->arrayFile2:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 718
    const/4 v2, 0x0

    aget-object v2, p0, v2

    const/4 v7, 0x2

    aget-object v7, p0, v7

    move-object/from16 v0, v66

    invoke-static {v2, v0, v7}, Lcom/chelpus/root/utils/custompatch;->searchlib(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    sput-object v2, Lcom/chelpus/root/utils/custompatch;->arrayFile2:Ljava/util/ArrayList;

    .line 720
    const/16 v43, 0x0

    .line 722
    :cond_647
    aget-object v2, v65, v57

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "[LIB]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_666

    .line 723
    const/4 v2, 0x2

    sput v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    .line 724
    const/16 v43, 0x1

    .line 725
    const/16 v53, 0x0

    .line 726
    const/16 v55, 0x0

    .line 727
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->odex:Z

    .line 728
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->unpack:Z

    .line 729
    const/16 v20, 0x0

    .line 732
    :cond_666
    aget-object v2, v65, v57

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "[LIB-ARMEABI]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_6a7

    .line 733
    const/4 v2, 0x7

    aget-object v2, p0, v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "armeabi"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_696

    const/4 v2, 0x7

    aget-object v2, p0, v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "armeabi-v7a"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10f0

    sget-boolean v2, Lcom/chelpus/root/utils/custompatch;->armv7:Z

    if-nez v2, :cond_10f0

    .line 734
    :cond_696
    const/4 v2, 0x6

    sput v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    .line 735
    const/16 v43, 0x1

    .line 736
    const/16 v53, 0x0

    .line 737
    const/16 v55, 0x0

    .line 738
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->odex:Z

    .line 739
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->unpack:Z

    .line 740
    const/16 v20, 0x0

    .line 746
    :cond_6a7
    :goto_6a7
    aget-object v2, v65, v57

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "[LIB-ARMEABI-V7A]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_6d5

    .line 747
    const/4 v2, 0x7

    aget-object v2, p0, v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "armeabi-v7a"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10f6

    .line 748
    const/4 v2, 0x7

    sput v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    .line 749
    const/16 v43, 0x1

    .line 750
    const/16 v53, 0x0

    .line 751
    const/16 v55, 0x0

    .line 752
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->odex:Z

    .line 753
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->unpack:Z

    .line 754
    const/16 v20, 0x0

    .line 760
    :cond_6d5
    :goto_6d5
    aget-object v2, v65, v57

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "[LIB-MIPS]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_704

    .line 761
    const/4 v2, 0x7

    aget-object v2, p0, v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "mips"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10fc

    .line 762
    const/16 v2, 0x8

    sput v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    .line 763
    const/16 v43, 0x1

    .line 764
    const/16 v53, 0x0

    .line 765
    const/16 v55, 0x0

    .line 766
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->odex:Z

    .line 767
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->unpack:Z

    .line 768
    const/16 v20, 0x0

    .line 774
    :cond_704
    :goto_704
    aget-object v2, v65, v57

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "[LIB-X86]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_733

    .line 775
    const/4 v2, 0x7

    aget-object v2, p0, v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "x86"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1102

    .line 776
    const/16 v2, 0x9

    sput v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    .line 777
    const/16 v43, 0x1

    .line 778
    const/16 v53, 0x0

    .line 779
    const/16 v55, 0x0

    .line 780
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->odex:Z

    .line 781
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->unpack:Z

    .line 782
    const/16 v20, 0x0

    .line 789
    :cond_733
    :goto_733
    aget-object v2, v65, v57

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "[OTHER FILES]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_752

    .line 790
    const/4 v2, 0x3

    sput v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    .line 791
    const/16 v43, 0x0

    .line 792
    const/16 v53, 0x1

    .line 793
    const/16 v55, 0x0

    .line 794
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->odex:Z

    .line 795
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->unpack:Z

    .line 796
    const/16 v20, 0x0

    .line 799
    :cond_752
    aget-object v2, v65, v57

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "[SET_PERMISSIONS]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_772

    .line 800
    const/16 v2, 0xd

    sput v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    .line 801
    const/16 v43, 0x0

    .line 802
    const/16 v53, 0x0

    .line 803
    const/16 v55, 0x1

    .line 804
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->odex:Z

    .line 805
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->unpack:Z

    .line 806
    const/16 v20, 0x0

    .line 809
    :cond_772
    aget-object v2, v65, v57

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "[COPY_FILE]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_792

    .line 810
    const/16 v2, 0xf

    sput v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    .line 811
    const/16 v43, 0x0

    .line 812
    const/16 v53, 0x0

    .line 813
    const/16 v55, 0x0

    .line 814
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->odex:Z

    .line 815
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->unpack:Z

    .line 816
    const/16 v20, 0x1

    .line 819
    :cond_792
    aget-object v2, v65, v57

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "[ODEX-PATCH]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_7b2

    .line 820
    const/16 v2, 0xb

    sput v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    .line 821
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->unpack:Z

    .line 822
    const/16 v20, 0x0

    .line 823
    const/16 v43, 0x0

    .line 824
    const/16 v53, 0x0

    .line 825
    const/16 v55, 0x0

    .line 826
    const/4 v2, 0x1

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->odex:Z

    .line 828
    :cond_7b2
    aget-object v2, v65, v57

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "[FILE_IN_APK]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_7d2

    .line 829
    const/16 v2, 0xe

    sput v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    .line 830
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->unpack:Z

    .line 831
    const/16 v43, 0x0

    .line 832
    const/16 v53, 0x0

    .line 833
    const/16 v55, 0x0

    .line 834
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->odex:Z

    .line 835
    const/16 v20, 0x0

    .line 838
    :cond_7d2
    aget-object v2, v65, v57

    const-string v7, "group"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_803

    aget-object v2, v65, v57

    const-string v7, "{"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_803

    aget-object v2, v65, v57

    const-string v7, "}"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_7ed
    .catch Ljava/io/FileNotFoundException; {:try_start_632 .. :try_end_7ed} :catch_967
    .catch Ljava/lang/Exception; {:try_start_632 .. :try_end_7ed} :catch_9ff

    move-result v2

    if-eqz v2, :cond_803

    .line 840
    :try_start_7f0
    new-instance v40, Lorg/json/JSONObject;

    aget-object v2, v65, v57

    move-object/from16 v0, v40

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 841
    .restart local v40    # "json_obj":Lorg/json/JSONObject;
    const-string v2, "group"

    move-object/from16 v0, v40

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/chelpus/root/utils/custompatch;->group:Ljava/lang/String;
    :try_end_803
    .catch Lorg/json/JSONException; {:try_start_7f0 .. :try_end_803} :catch_1108
    .catch Ljava/io/FileNotFoundException; {:try_start_7f0 .. :try_end_803} :catch_967
    .catch Ljava/lang/Exception; {:try_start_7f0 .. :try_end_803} :catch_9ff

    .line 847
    .end local v40    # "json_obj":Lorg/json/JSONObject;
    :cond_803
    :goto_803
    :try_start_803
    aget-object v2, v65, v57

    const-string v7, "original"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_113a

    aget-object v2, v65, v57

    const-string v7, "{"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_113a

    aget-object v2, v65, v57

    const-string v7, "}"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_113a

    .line 849
    if-eqz v48, :cond_82b

    .line 850
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->ser:Ljava/util/ArrayList;

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->searchProcess(Ljava/util/ArrayList;)Z
    :try_end_828
    .catch Ljava/io/FileNotFoundException; {:try_start_803 .. :try_end_828} :catch_967
    .catch Ljava/lang/Exception; {:try_start_803 .. :try_end_828} :catch_9ff

    move-result v63

    .line 851
    const/16 v48, 0x0

    .line 854
    :cond_82b
    :try_start_82b
    new-instance v40, Lorg/json/JSONObject;

    aget-object v2, v65, v57

    move-object/from16 v0, v40

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 855
    .restart local v40    # "json_obj":Lorg/json/JSONObject;
    const-string v2, "original"

    move-object/from16 v0, v40

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_83b
    .catch Lorg/json/JSONException; {:try_start_82b .. :try_end_83b} :catch_1114
    .catch Ljava/io/FileNotFoundException; {:try_start_82b .. :try_end_83b} :catch_967
    .catch Ljava/lang/Exception; {:try_start_82b .. :try_end_83b} :catch_9ff

    move-result-object v66

    .line 859
    .end local v40    # "json_obj":Lorg/json/JSONObject;
    :goto_83c
    :try_start_83c
    invoke-virtual/range {v66 .. v66}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v66

    .line 860
    sget-boolean v2, Lcom/chelpus/root/utils/custompatch;->convert:Z

    if-eqz v2, :cond_848

    .line 861
    invoke-static/range {v66 .. v66}, Lcom/chelpus/Utils;->rework(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v66

    .line 863
    :cond_848
    const-string v2, "[ \t]+"

    move-object/from16 v0, v66

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    array-length v2, v2

    new-array v0, v2, [Ljava/lang/String;

    move-object/from16 v52, v0

    .line 864
    const-string v2, "[ \t]+"

    move-object/from16 v0, v66

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v52

    .line 865
    move-object/from16 v0, v52

    array-length v2, v0

    new-array v4, v2, [I

    .line 866
    move-object/from16 v0, v52

    array-length v2, v0

    new-array v3, v2, [B
    :try_end_867
    .catch Ljava/io/FileNotFoundException; {:try_start_83c .. :try_end_867} :catch_967
    .catch Ljava/lang/Exception; {:try_start_83c .. :try_end_867} :catch_9ff

    .line 868
    const/16 v64, 0x0

    .local v64, "t":I
    :goto_869
    :try_start_869
    move-object/from16 v0, v52

    array-length v2, v0

    move/from16 v0, v64

    if-ge v0, v2, :cond_113a

    .line 869
    aget-object v2, v52, v64

    const-string v7, "*"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_88a

    aget-object v2, v52, v64

    const-string v7, "**"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_88a

    .line 870
    const/16 v31, 0x1

    .line 871
    const-string v2, "60"

    aput-object v2, v52, v64

    .line 873
    :cond_88a
    aget-object v2, v52, v64

    const-string v7, "**"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_89e

    aget-object v2, v52, v64

    const-string v7, "\\?+"

    invoke-virtual {v2, v7}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_111c

    .line 874
    :cond_89e
    const-string v2, "60"

    aput-object v2, v52, v64

    .line 875
    const/4 v2, 0x1

    aput v2, v4, v64

    .line 878
    :goto_8a5
    aget-object v2, v52, v64

    const-string v7, "W"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_8cd

    aget-object v2, v52, v64

    const-string v7, "w"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_8cd

    aget-object v2, v52, v64

    const-string v7, "R"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_8cd

    aget-object v2, v52, v64

    const-string v7, "r"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_8f3

    .line 880
    :cond_8cd
    aget-object v2, v52, v64

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "w"

    const-string v8, ""

    invoke-virtual {v2, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    const-string v7, "r"

    const-string v8, ""

    invoke-virtual {v2, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v51

    .line 882
    .local v51, "or":Ljava/lang/String;
    invoke-static/range {v51 .. v51}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v69

    .line 883
    .local v69, "y":I
    add-int/lit8 v69, v69, 0x2

    .line 884
    aput v69, v4, v64

    .line 885
    const-string v2, "60"

    aput-object v2, v52, v64

    .line 887
    .end local v51    # "or":Ljava/lang/String;
    .end local v69    # "y":I
    :cond_8f3
    aget-object v2, v52, v64

    const/16 v7, 0x10

    invoke-static {v2, v7}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->byteValue()B

    move-result v2

    aput-byte v2, v3, v64
    :try_end_901
    .catch Ljava/lang/Exception; {:try_start_869 .. :try_end_901} :catch_1121
    .catch Ljava/io/FileNotFoundException; {:try_start_869 .. :try_end_901} :catch_967

    .line 868
    add-int/lit8 v64, v64, 0x1

    goto/16 :goto_869

    .line 169
    .end local v3    # "byteOrig":[B
    .end local v4    # "mask":[I
    .end local v14    # "begin":Z
    .end local v16    # "br":Ljava/io/BufferedReader;
    .end local v20    # "copy_file":Z
    .end local v23    # "data":Ljava/lang/String;
    .end local v33    # "file_name":Ljava/lang/String;
    .end local v36    # "fis":Ljava/io/FileInputStream;
    .end local v38    # "isr":Ljava/io/InputStreamReader;
    .end local v43    # "libr":Z
    .end local v48    # "mark_search":Z
    .end local v52    # "orhex":[Ljava/lang/String;
    .end local v53    # "other":Z
    .end local v54    # "path_for_copy":Ljava/lang/String;
    .end local v55    # "permissions":Z
    .end local v56    # "permissionsSet":Ljava/lang/String;
    .end local v57    # "r":I
    .end local v60    # "result":Z
    .end local v63    # "sumresult":Z
    .end local v64    # "t":I
    .end local v65    # "txtdata":[Ljava/lang/String;
    .end local v66    # "value1":Ljava/lang/String;
    .end local v67    # "value2":Ljava/lang/String;
    .end local v68    # "value3":Ljava/lang/String;
    .restart local v17    # "br1":Ljava/io/BufferedReader;
    .restart local v37    # "fis1":Ljava/io/FileInputStream;
    .restart local v39    # "isr1":Ljava/io/InputStreamReader;
    :cond_905
    :try_start_905
    invoke-virtual/range {v17 .. v17}, Ljava/io/BufferedReader;->close()V

    .line 170
    invoke-virtual/range {v37 .. v37}, Ljava/io/FileInputStream;->close()V
    :try_end_90b
    .catch Ljava/io/IOException; {:try_start_905 .. :try_end_90b} :catch_197
    .catch Ljava/lang/Exception; {:try_start_905 .. :try_end_90b} :catch_c0

    goto/16 :goto_198

    .line 274
    .end local v17    # "br1":Ljava/io/BufferedReader;
    .end local v37    # "fis1":Ljava/io/FileInputStream;
    .end local v39    # "isr1":Ljava/io/InputStreamReader;
    .restart local v3    # "byteOrig":[B
    .restart local v4    # "mask":[I
    .restart local v14    # "begin":Z
    .restart local v16    # "br":Ljava/io/BufferedReader;
    .restart local v20    # "copy_file":Z
    .restart local v23    # "data":Ljava/lang/String;
    .restart local v33    # "file_name":Ljava/lang/String;
    .restart local v36    # "fis":Ljava/io/FileInputStream;
    .restart local v38    # "isr":Ljava/io/InputStreamReader;
    .restart local v43    # "libr":Z
    .restart local v48    # "mark_search":Z
    .restart local v52    # "orhex":[Ljava/lang/String;
    .restart local v53    # "other":Z
    .restart local v54    # "path_for_copy":Ljava/lang/String;
    .restart local v55    # "permissions":Z
    .restart local v56    # "permissionsSet":Ljava/lang/String;
    .restart local v57    # "r":I
    .restart local v60    # "result":Z
    .restart local v63    # "sumresult":Z
    .restart local v65    # "txtdata":[Ljava/lang/String;
    .restart local v66    # "value1":Ljava/lang/String;
    .restart local v67    # "value2":Ljava/lang/String;
    .restart local v68    # "value3":Ljava/lang/String;
    :pswitch_90d
    :try_start_90d
    sget-boolean v2, Lcom/chelpus/root/utils/custompatch;->odexpatch:Z

    if-eqz v2, :cond_96f

    .line 275
    new-instance v2, Ljava/io/File;

    sget-object v7, Lcom/chelpus/root/utils/custompatch;->dirapp:Ljava/lang/String;

    const/4 v8, 0x1

    invoke-static {v7, v8}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v2, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 276
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_356

    .line 277
    const-string v2, "---------------------------------"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 278
    const-string v2, "classes.dex not found!\nApply patch for odex:"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 279
    const-string v2, "---------------------------------\n"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 281
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    const-string v7, ""

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_945

    sget-object v2, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 282
    :cond_945
    sget-boolean v2, Lcom/chelpus/root/utils/custompatch;->manualpatch:Z

    if-nez v2, :cond_94f

    sget-object v2, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->patchProcess(Ljava/util/ArrayList;)Z

    move-result v60

    .line 283
    :cond_94f
    if-nez v60, :cond_953

    const/16 v63, 0x0

    .line 284
    :cond_953
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->ser:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 285
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 286
    const/16 v2, 0xc8

    sput v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    .line 287
    const-string v2, ""

    sput-object v2, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;
    :try_end_965
    .catch Ljava/io/FileNotFoundException; {:try_start_90d .. :try_end_965} :catch_967
    .catch Ljava/lang/Exception; {:try_start_90d .. :try_end_965} :catch_9ff

    goto/16 :goto_356

    .line 1180
    .end local v3    # "byteOrig":[B
    .end local v4    # "mask":[I
    .end local v14    # "begin":Z
    .end local v16    # "br":Ljava/io/BufferedReader;
    .end local v20    # "copy_file":Z
    .end local v23    # "data":Ljava/lang/String;
    .end local v33    # "file_name":Ljava/lang/String;
    .end local v36    # "fis":Ljava/io/FileInputStream;
    .end local v38    # "isr":Ljava/io/InputStreamReader;
    .end local v43    # "libr":Z
    .end local v48    # "mark_search":Z
    .end local v52    # "orhex":[Ljava/lang/String;
    .end local v53    # "other":Z
    .end local v54    # "path_for_copy":Ljava/lang/String;
    .end local v55    # "permissions":Z
    .end local v56    # "permissionsSet":Ljava/lang/String;
    .end local v57    # "r":I
    .end local v60    # "result":Z
    .end local v63    # "sumresult":Z
    .end local v65    # "txtdata":[Ljava/lang/String;
    .end local v66    # "value1":Ljava/lang/String;
    .end local v67    # "value2":Ljava/lang/String;
    .end local v68    # "value3":Ljava/lang/String;
    :catch_967
    move-exception v29

    .line 1182
    .local v29, "e":Ljava/io/FileNotFoundException;
    :try_start_968
    const-string v2, "Custom Patch not Found. It\'s problem with root. Don\'t have access to SD card from root.  If you use SuperSu, try disable \"mount namespace separation\" in SuperSu. If it not help, please update SuperSu and update binary file su from SuperSu."

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V
    :try_end_96d
    .catch Ljava/lang/Exception; {:try_start_968 .. :try_end_96d} :catch_c0

    goto/16 :goto_c4

    .line 291
    .end local v29    # "e":Ljava/io/FileNotFoundException;
    .restart local v3    # "byteOrig":[B
    .restart local v4    # "mask":[I
    .restart local v14    # "begin":Z
    .restart local v16    # "br":Ljava/io/BufferedReader;
    .restart local v20    # "copy_file":Z
    .restart local v23    # "data":Ljava/lang/String;
    .restart local v33    # "file_name":Ljava/lang/String;
    .restart local v36    # "fis":Ljava/io/FileInputStream;
    .restart local v38    # "isr":Ljava/io/InputStreamReader;
    .restart local v43    # "libr":Z
    .restart local v48    # "mark_search":Z
    .restart local v52    # "orhex":[Ljava/lang/String;
    .restart local v53    # "other":Z
    .restart local v54    # "path_for_copy":Ljava/lang/String;
    .restart local v55    # "permissions":Z
    .restart local v56    # "permissionsSet":Ljava/lang/String;
    .restart local v57    # "r":I
    .restart local v60    # "result":Z
    .restart local v63    # "sumresult":Z
    .restart local v65    # "txtdata":[Ljava/lang/String;
    .restart local v66    # "value1":Ljava/lang/String;
    .restart local v67    # "value2":Ljava/lang/String;
    .restart local v68    # "value3":Ljava/lang/String;
    :cond_96f
    :try_start_96f
    sget-boolean v2, Lcom/chelpus/root/utils/custompatch;->unpack:Z

    if-nez v2, :cond_a1d

    .line 293
    const/4 v2, 0x2

    aget-object v2, p0, v2

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->searchDalvik(Ljava/lang/String;)V

    .line 294
    new-instance v2, Ljava/io/File;

    sget-object v7, Lcom/chelpus/root/utils/custompatch;->dirapp:Ljava/lang/String;

    const/4 v8, 0x1

    invoke-static {v7, v8}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_9b7

    sget-object v2, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v7, ".odex"

    invoke-virtual {v2, v7}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_9b7

    .line 295
    new-instance v2, Ljava/io/File;

    sget-object v7, Lcom/chelpus/root/utils/custompatch;->dirapp:Ljava/lang/String;

    const/4 v8, 0x1

    invoke-static {v7, v8}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 296
    const-string v2, "---------------------------------"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 297
    const-string v2, "odex file removed before\npatch for dalvik-cache..."

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 298
    const-string v2, "---------------------------------\n"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 300
    :cond_9b7
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_356

    .line 301
    const-string v2, "---------------------------------"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 302
    const-string v2, "Patch for dalvik-cache:"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 303
    const-string v2, "---------------------------------\n"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 305
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    const-string v7, ""

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9dd

    sget-object v2, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 306
    :cond_9dd
    sget-boolean v2, Lcom/chelpus/root/utils/custompatch;->manualpatch:Z

    if-nez v2, :cond_9e7

    sget-object v2, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->patchProcess(Ljava/util/ArrayList;)Z

    move-result v60

    .line 307
    :cond_9e7
    if-nez v60, :cond_9eb

    const/16 v63, 0x0

    .line 308
    :cond_9eb
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->ser:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 309
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 310
    const/16 v2, 0xc8

    sput v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    .line 311
    const-string v2, ""

    sput-object v2, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;
    :try_end_9fd
    .catch Ljava/io/FileNotFoundException; {:try_start_96f .. :try_end_9fd} :catch_967
    .catch Ljava/lang/Exception; {:try_start_96f .. :try_end_9fd} :catch_9ff

    goto/16 :goto_356

    .line 1183
    .end local v3    # "byteOrig":[B
    .end local v4    # "mask":[I
    .end local v14    # "begin":Z
    .end local v16    # "br":Ljava/io/BufferedReader;
    .end local v20    # "copy_file":Z
    .end local v23    # "data":Ljava/lang/String;
    .end local v33    # "file_name":Ljava/lang/String;
    .end local v36    # "fis":Ljava/io/FileInputStream;
    .end local v38    # "isr":Ljava/io/InputStreamReader;
    .end local v43    # "libr":Z
    .end local v48    # "mark_search":Z
    .end local v52    # "orhex":[Ljava/lang/String;
    .end local v53    # "other":Z
    .end local v54    # "path_for_copy":Ljava/lang/String;
    .end local v55    # "permissions":Z
    .end local v56    # "permissionsSet":Ljava/lang/String;
    .end local v57    # "r":I
    .end local v60    # "result":Z
    .end local v63    # "sumresult":Z
    .end local v65    # "txtdata":[Ljava/lang/String;
    .end local v66    # "value1":Ljava/lang/String;
    .end local v67    # "value2":Ljava/lang/String;
    .end local v68    # "value3":Ljava/lang/String;
    :catch_9ff
    move-exception v29

    .line 1184
    .local v29, "e":Ljava/lang/Exception;
    :try_start_a00
    invoke-virtual/range {v29 .. v29}, Ljava/lang/Exception;->printStackTrace()V

    .line 1185
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ""

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v29

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V
    :try_end_a1b
    .catch Ljava/lang/Exception; {:try_start_a00 .. :try_end_a1b} :catch_c0

    goto/16 :goto_c4

    .line 314
    .end local v29    # "e":Ljava/lang/Exception;
    .restart local v3    # "byteOrig":[B
    .restart local v4    # "mask":[I
    .restart local v14    # "begin":Z
    .restart local v16    # "br":Ljava/io/BufferedReader;
    .restart local v20    # "copy_file":Z
    .restart local v23    # "data":Ljava/lang/String;
    .restart local v33    # "file_name":Ljava/lang/String;
    .restart local v36    # "fis":Ljava/io/FileInputStream;
    .restart local v38    # "isr":Ljava/io/InputStreamReader;
    .restart local v43    # "libr":Z
    .restart local v48    # "mark_search":Z
    .restart local v52    # "orhex":[Ljava/lang/String;
    .restart local v53    # "other":Z
    .restart local v54    # "path_for_copy":Ljava/lang/String;
    .restart local v55    # "permissions":Z
    .restart local v56    # "permissionsSet":Ljava/lang/String;
    .restart local v57    # "r":I
    .restart local v60    # "result":Z
    .restart local v63    # "sumresult":Z
    .restart local v65    # "txtdata":[Ljava/lang/String;
    .restart local v66    # "value1":Ljava/lang/String;
    .restart local v67    # "value2":Ljava/lang/String;
    .restart local v68    # "value3":Ljava/lang/String;
    :cond_a1d
    :try_start_a1d
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->classesFiles:Ljava/util/ArrayList;

    if-eqz v2, :cond_356

    sget-object v2, Lcom/chelpus/root/utils/custompatch;->classesFiles:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_356

    .line 316
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->classesFiles:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v7, 0x1

    if-le v2, v7, :cond_a35

    const/4 v2, 0x1

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->multidex:Z

    .line 317
    :cond_a35
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->classesFiles:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_a3b
    :goto_a3b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_a99

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/io/File;

    .line 318
    .local v18, "cl":Ljava/io/File;
    sput-object v18, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 319
    sget-object v7, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lez v7, :cond_a3b

    .line 320
    const-string v7, "---------------------------------"

    invoke-static {v7}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 321
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Patch for "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ":"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 322
    const-string v7, "---------------------------------\n"

    invoke-static {v7}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 324
    sget-object v7, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_a8a

    sget-object v7, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    invoke-static {v7}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 325
    :cond_a8a
    sget-boolean v7, Lcom/chelpus/root/utils/custompatch;->manualpatch:Z

    if-nez v7, :cond_a94

    sget-object v7, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-static {v7}, Lcom/chelpus/root/utils/custompatch;->patchProcess(Ljava/util/ArrayList;)Z

    move-result v60

    .line 326
    :cond_a94
    if-nez v60, :cond_a3b

    const/16 v63, 0x0

    goto :goto_a3b

    .line 330
    .end local v18    # "cl":Ljava/io/File;
    :cond_a99
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->multidex:Z

    .line 331
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->goodResult:Z

    .line 332
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->ser:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 333
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 334
    const/16 v2, 0xc8

    sput v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    .line 335
    const-string v2, ""

    sput-object v2, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    goto/16 :goto_356

    .line 341
    :pswitch_ab3
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->convert:Z

    .line 342
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_ae6

    .line 343
    const-string v2, "---------------------------------"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 344
    const-string v2, "Dalvik-cache fixed to odex!\nPatch for odex:"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 345
    const-string v2, "---------------------------------\n"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 346
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    const-string v7, ""

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_adc

    sget-object v2, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 347
    :cond_adc
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->patchProcess(Ljava/util/ArrayList;)Z

    move-result v60

    .line 348
    if-nez v60, :cond_ae6

    const/16 v63, 0x0

    .line 350
    :cond_ae6
    const-string v2, "---------------------------------"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 351
    const-string v2, "Dalvik-cache fixed to odex!"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 352
    const-string v2, "---------------------------------\n"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 353
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    const-string v7, ""

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b04

    sget-object v2, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 354
    :cond_b04
    sget-boolean v2, Lcom/chelpus/root/utils/custompatch;->unpack:Z

    if-nez v2, :cond_b0e

    const/4 v2, 0x2

    aget-object v2, p0, v2

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->searchDalvik(Ljava/lang/String;)V

    .line 355
    :cond_b0e
    const/4 v2, 0x0

    aget-object v2, p0, v2

    const/4 v7, 0x2

    aget-object v7, p0, v7

    invoke-static {v2, v7}, Lcom/chelpus/root/utils/custompatch;->searchDalvikOdex(Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_b27

    .line 357
    const/16 v21, 0x1

    .line 358
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v22

    .line 360
    :cond_b27
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->ser:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 361
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 362
    const/16 v2, 0xc8

    sput v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    .line 364
    const-string v2, ""

    sput-object v2, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    goto/16 :goto_356

    .line 368
    :pswitch_b3b
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->convert:Z

    .line 369
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_bb7

    .line 370
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->arrayFile2:Ljava/util/ArrayList;

    if-eqz v2, :cond_bb7

    sget-object v2, Lcom/chelpus/root/utils/custompatch;->arrayFile2:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_bb7

    .line 371
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->arrayFile2:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_b58
    :goto_b58
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_bac

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v42

    check-cast v42, Ljava/io/File;

    .line 372
    .local v42, "lib":Ljava/io/File;
    sput-object v42, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 373
    const-string v7, "---------------------------"

    invoke-static {v7}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 374
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Patch for libraries \n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual {v8}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ":"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 375
    const-string v7, "---------------------------\n"

    invoke-static {v7}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 376
    sget-object v7, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_ba1

    sget-object v7, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    invoke-static {v7}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 377
    :cond_ba1
    sget-object v7, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-static {v7}, Lcom/chelpus/root/utils/custompatch;->patchProcess(Ljava/util/ArrayList;)Z

    move-result v60

    .line 378
    if-nez v60, :cond_b58

    const/16 v63, 0x0

    goto :goto_b58

    .line 380
    .end local v42    # "lib":Ljava/io/File;
    :cond_bac
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->multilib_patch:Z

    .line 381
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->goodResult:Z

    .line 382
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->arrayFile2:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 386
    :cond_bb7
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->ser:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 387
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 388
    const/16 v2, 0xc8

    sput v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    .line 389
    const-string v2, ""

    sput-object v2, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    goto/16 :goto_356

    .line 392
    :pswitch_bcb
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->convert:Z

    .line 393
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_c47

    .line 394
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->arrayFile2:Ljava/util/ArrayList;

    if-eqz v2, :cond_c47

    sget-object v2, Lcom/chelpus/root/utils/custompatch;->arrayFile2:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_c47

    .line 395
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->arrayFile2:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_be8
    :goto_be8
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_c3c

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v42

    check-cast v42, Ljava/io/File;

    .line 396
    .restart local v42    # "lib":Ljava/io/File;
    sput-object v42, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 397
    const-string v7, "--------------------------------"

    invoke-static {v7}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 398
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Patch for (armeabi) libraries \n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual {v8}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ":"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 399
    const-string v7, "--------------------------------\n"

    invoke-static {v7}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 400
    sget-object v7, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_c31

    sget-object v7, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    invoke-static {v7}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 401
    :cond_c31
    sget-object v7, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-static {v7}, Lcom/chelpus/root/utils/custompatch;->patchProcess(Ljava/util/ArrayList;)Z

    move-result v60

    .line 402
    if-nez v60, :cond_be8

    const/16 v63, 0x0

    goto :goto_be8

    .line 404
    .end local v42    # "lib":Ljava/io/File;
    :cond_c3c
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->multilib_patch:Z

    .line 405
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->goodResult:Z

    .line 406
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->arrayFile2:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 410
    :cond_c47
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->ser:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 411
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 412
    const/16 v2, 0xc8

    sput v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    .line 413
    const-string v2, ""

    sput-object v2, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    goto/16 :goto_356

    .line 416
    :pswitch_c5b
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->convert:Z

    .line 417
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_cd7

    .line 418
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->arrayFile2:Ljava/util/ArrayList;

    if-eqz v2, :cond_cd7

    sget-object v2, Lcom/chelpus/root/utils/custompatch;->arrayFile2:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_cd7

    .line 419
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->arrayFile2:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_c78
    :goto_c78
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_ccc

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v42

    check-cast v42, Ljava/io/File;

    .line 420
    .restart local v42    # "lib":Ljava/io/File;
    sput-object v42, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 421
    const-string v7, "---------------------------------------"

    invoke-static {v7}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 422
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Patch for (armeabi-v7a) libraries \n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual {v8}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ":"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 423
    const-string v7, "---------------------------------------\n"

    invoke-static {v7}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 424
    sget-object v7, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_cc1

    sget-object v7, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    invoke-static {v7}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 425
    :cond_cc1
    sget-object v7, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-static {v7}, Lcom/chelpus/root/utils/custompatch;->patchProcess(Ljava/util/ArrayList;)Z

    move-result v60

    .line 426
    if-nez v60, :cond_c78

    const/16 v63, 0x0

    goto :goto_c78

    .line 428
    .end local v42    # "lib":Ljava/io/File;
    :cond_ccc
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->multilib_patch:Z

    .line 429
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->goodResult:Z

    .line 430
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->arrayFile2:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 435
    :cond_cd7
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->ser:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 436
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 437
    const/16 v2, 0xc8

    sput v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    .line 438
    const-string v2, ""

    sput-object v2, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    goto/16 :goto_356

    .line 441
    :pswitch_ceb
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->convert:Z

    .line 442
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_d67

    .line 443
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->arrayFile2:Ljava/util/ArrayList;

    if-eqz v2, :cond_d67

    sget-object v2, Lcom/chelpus/root/utils/custompatch;->arrayFile2:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_d67

    .line 444
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->arrayFile2:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_d08
    :goto_d08
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_d5c

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v42

    check-cast v42, Ljava/io/File;

    .line 445
    .restart local v42    # "lib":Ljava/io/File;
    sput-object v42, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 446
    const-string v7, "---------------------------"

    invoke-static {v7}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 447
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Patch for (MIPS) libraries \n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual {v8}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ":"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 448
    const-string v7, "---------------------------\n"

    invoke-static {v7}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 449
    sget-object v7, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_d51

    sget-object v7, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    invoke-static {v7}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 450
    :cond_d51
    sget-object v7, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-static {v7}, Lcom/chelpus/root/utils/custompatch;->patchProcess(Ljava/util/ArrayList;)Z

    move-result v60

    .line 451
    if-nez v60, :cond_d08

    const/16 v63, 0x0

    goto :goto_d08

    .line 453
    .end local v42    # "lib":Ljava/io/File;
    :cond_d5c
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->multilib_patch:Z

    .line 454
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->goodResult:Z

    .line 455
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->arrayFile2:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 459
    :cond_d67
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->ser:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 460
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 461
    const/16 v2, 0xc8

    sput v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    .line 462
    const-string v2, ""

    sput-object v2, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    goto/16 :goto_356

    .line 465
    :pswitch_d7b
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->convert:Z

    .line 466
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_df7

    .line 467
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->arrayFile2:Ljava/util/ArrayList;

    if-eqz v2, :cond_df7

    sget-object v2, Lcom/chelpus/root/utils/custompatch;->arrayFile2:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_df7

    .line 468
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->arrayFile2:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_d98
    :goto_d98
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_dec

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v42

    check-cast v42, Ljava/io/File;

    .line 469
    .restart local v42    # "lib":Ljava/io/File;
    sput-object v42, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 470
    const-string v7, "---------------------------"

    invoke-static {v7}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 471
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Patch for (x86) libraries \n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual {v8}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ":"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 472
    const-string v7, "---------------------------\n"

    invoke-static {v7}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 473
    sget-object v7, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_de1

    sget-object v7, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    invoke-static {v7}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 474
    :cond_de1
    sget-object v7, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-static {v7}, Lcom/chelpus/root/utils/custompatch;->patchProcess(Ljava/util/ArrayList;)Z

    move-result v60

    .line 475
    if-nez v60, :cond_d98

    const/16 v63, 0x0

    goto :goto_d98

    .line 477
    .end local v42    # "lib":Ljava/io/File;
    :cond_dec
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->multilib_patch:Z

    .line 478
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->goodResult:Z

    .line 479
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->arrayFile2:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 483
    :cond_df7
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->ser:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 484
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 485
    const/16 v2, 0xc8

    sput v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    .line 486
    const-string v2, ""

    sput-object v2, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    goto/16 :goto_356

    .line 490
    :pswitch_e0b
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->convert:Z

    .line 491
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_e24

    .line 493
    sget-boolean v2, Lcom/chelpus/root/utils/custompatch;->manualpatch:Z

    if-nez v2, :cond_e20

    sget-object v2, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->patchProcess(Ljava/util/ArrayList;)Z

    move-result v60

    .line 494
    :cond_e20
    if-nez v60, :cond_e24

    const/16 v63, 0x0

    .line 496
    :cond_e24
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->ser:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 497
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 498
    const/16 v2, 0xc8

    sput v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    .line 499
    const-string v2, ""

    sput-object v2, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    goto/16 :goto_356

    .line 503
    :pswitch_e38
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->convert:Z

    .line 504
    const-string v2, "---------------------------"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 505
    const-string v2, "Patch for odex:"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 506
    const-string v2, "---------------------------\n"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 507
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    const-string v7, ""

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e59

    sget-object v2, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 508
    :cond_e59
    new-instance v2, Ljava/io/File;

    sget-object v7, Lcom/chelpus/root/utils/custompatch;->dirapp:Ljava/lang/String;

    const/4 v8, 0x1

    invoke-static {v7, v8}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v2, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 509
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_e74

    .line 510
    const-string v2, "Odex not found! Please use befor other Patch, and after run Custom Patch!"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 511
    :cond_e74
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v7

    const-wide/16 v70, 0x0

    cmp-long v2, v7, v70

    if-nez v2, :cond_e8a

    .line 512
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 513
    const-string v2, "Odex not found! Please use befor other Patch, and after run Custom Patch!"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 515
    :cond_e8a
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->unpack:Z

    .line 516
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->odex:Z

    .line 518
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_ea2

    .line 519
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->patchProcess(Ljava/util/ArrayList;)Z

    move-result v60

    .line 520
    if-nez v60, :cond_ea2

    const/16 v63, 0x0

    .line 522
    :cond_ea2
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->ser:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 523
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 524
    const/16 v2, 0xc8

    sput v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    .line 525
    const-string v2, ""

    sput-object v2, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    goto/16 :goto_356

    .line 528
    :pswitch_eb6
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->convert:Z

    .line 529
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->ser:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 530
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 531
    const/16 v2, 0xc8

    sput v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    .line 532
    const-string v2, ""

    sput-object v2, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    goto/16 :goto_356

    .line 535
    :pswitch_ecd
    const/16 v55, 0x0

    .line 536
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_356

    .line 537
    const-string v2, "---------------------------"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 538
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Set permissions "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v56

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, " for file:\n"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v33

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 539
    const-string v2, "---------------------------\n"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 540
    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "chmod"

    aput-object v8, v2, v7

    const/4 v7, 0x1

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v70, ""

    move-object/from16 v0, v70

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, v56

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v2, v7

    const/4 v7, 0x2

    sget-object v8, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual {v8}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v2, v7

    invoke-static {v2}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    goto/16 :goto_356

    .line 545
    :pswitch_f35
    const/16 v20, 0x0

    .line 546
    new-instance v2, Ljava/io/File;

    move-object/from16 v0, v54

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Lcom/chelpus/Utils;->getDirs(Ljava/io/File;)Ljava/io/File;

    move-result-object v27

    .line 547
    .local v27, "dir_for_copy":Ljava/io/File;
    invoke-virtual/range {v27 .. v27}, Ljava/io/File;->mkdirs()Z

    .line 548
    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "chmod"

    aput-object v8, v2, v7

    const/4 v7, 0x1

    const-string v8, "777"

    aput-object v8, v2, v7

    const/4 v7, 0x2

    invoke-virtual/range {v27 .. v27}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v2, v7

    invoke-static {v2}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 549
    invoke-virtual/range {v27 .. v27}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v28

    .line 551
    .local v28, "dir_for_test":Ljava/lang/String;
    const-string v2, "---------------------------"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 552
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Copy file "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v33

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, " to:\n"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v54

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 553
    const-string v2, "---------------------------\n"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 554
    new-instance v2, Ljava/io/File;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Lcom/chelpus/root/utils/custompatch;->sddir:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v33

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_fe0

    .line 555
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Error: File "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v33

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, " not found to dir\n"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v7, Lcom/chelpus/root/utils/custompatch;->sddir:Ljava/lang/String;

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, "/"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    goto/16 :goto_356

    .line 557
    :cond_fe0
    new-instance v2, Ljava/io/File;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Lcom/chelpus/root/utils/custompatch;->sddir:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v33

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v7, Ljava/io/File;

    move-object/from16 v0, v54

    invoke-direct {v7, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2, v7}, Lcom/chelpus/Utils;->copyFile(Ljava/io/File;Ljava/io/File;)V

    .line 559
    new-instance v2, Ljava/io/File;

    move-object/from16 v0, v54

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1068

    new-instance v2, Ljava/io/File;

    move-object/from16 v0, v54

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v7

    new-instance v2, Ljava/io/File;

    new-instance v70, Ljava/lang/StringBuilder;

    invoke-direct/range {v70 .. v70}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v71, Lcom/chelpus/root/utils/custompatch;->sddir:Ljava/lang/String;

    invoke-virtual/range {v70 .. v71}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v70

    const-string v71, "/"

    invoke-virtual/range {v70 .. v71}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v70

    move-object/from16 v0, v70

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v70

    invoke-virtual/range {v70 .. v70}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v70

    move-object/from16 v0, v70

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v70

    cmp-long v2, v7, v70

    if-nez v2, :cond_1068

    .line 560
    const-string v2, "File copied success."

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 564
    :goto_1053
    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "chmod"

    aput-object v8, v2, v7

    const/4 v7, 0x1

    const-string v8, "777"

    aput-object v8, v2, v7

    const/4 v7, 0x2

    aput-object v54, v2, v7

    invoke-static {v2}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    goto/16 :goto_356

    .line 562
    :cond_1068
    const-string v2, "File copied with error. Try again."

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    goto :goto_1053

    .line 570
    .end local v27    # "dir_for_copy":Ljava/io/File;
    .end local v28    # "dir_for_test":Ljava/lang/String;
    :pswitch_106e
    const-string v2, "---------------------------"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 571
    const-string v2, "Patch for file from apk:\n"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 572
    const-string v2, "---------------------------\n"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 573
    const-string v2, "You must run rebuild for this application with custom patch, then patch will work.\n"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V
    :try_end_1082
    .catch Ljava/io/FileNotFoundException; {:try_start_a1d .. :try_end_1082} :catch_967
    .catch Ljava/lang/Exception; {:try_start_a1d .. :try_end_1082} :catch_9ff

    goto/16 :goto_356

    .line 622
    .restart local v25    # "db":Ljava/io/File;
    .restart local v40    # "json_obj":Lorg/json/JSONObject;
    :cond_1084
    :try_start_1084
    const-string v66, "Error LP: File of Database not Found!"

    goto/16 :goto_497

    .line 623
    :cond_1088
    const/4 v2, 0x1

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->dataBaseExist:Z
    :try_end_108b
    .catch Lorg/json/JSONException; {:try_start_1084 .. :try_end_108b} :catch_108d
    .catch Ljava/io/FileNotFoundException; {:try_start_1084 .. :try_end_108b} :catch_967
    .catch Ljava/lang/Exception; {:try_start_1084 .. :try_end_108b} :catch_9ff

    goto/16 :goto_497

    .line 628
    .end local v25    # "db":Ljava/io/File;
    .end local v40    # "json_obj":Lorg/json/JSONObject;
    :catch_108d
    move-exception v29

    .line 629
    .local v29, "e":Lorg/json/JSONException;
    :try_start_108e
    const-string v2, "Error LP: Error Name of Database read!"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V
    :try_end_1093
    .catch Ljava/io/FileNotFoundException; {:try_start_108e .. :try_end_1093} :catch_967
    .catch Ljava/lang/Exception; {:try_start_108e .. :try_end_1093} :catch_9ff

    goto/16 :goto_4c5

    .line 643
    .end local v29    # "e":Lorg/json/JSONException;
    .restart local v40    # "json_obj":Lorg/json/JSONObject;
    :catch_1095
    move-exception v29

    .line 644
    .local v29, "e":Ljava/lang/Exception;
    :try_start_1096
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "LuckyPatcher: SQL error - "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v29

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_10b0
    .catch Lorg/json/JSONException; {:try_start_1096 .. :try_end_10b0} :catch_10b2
    .catch Ljava/io/FileNotFoundException; {:try_start_1096 .. :try_end_10b0} :catch_967
    .catch Ljava/lang/Exception; {:try_start_1096 .. :try_end_10b0} :catch_9ff

    goto/16 :goto_512

    .line 647
    .end local v29    # "e":Ljava/lang/Exception;
    .end local v40    # "json_obj":Lorg/json/JSONObject;
    :catch_10b2
    move-exception v29

    .line 648
    .local v29, "e":Lorg/json/JSONException;
    :try_start_10b3
    const-string v2, "Error LP: Error SQL exec read!"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    goto/16 :goto_512

    .line 662
    .end local v29    # "e":Lorg/json/JSONException;
    :catch_10ba
    move-exception v29

    .line 663
    .restart local v29    # "e":Lorg/json/JSONException;
    const-string v2, "Error LP: Error name of file read!"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    goto/16 :goto_557

    .line 673
    .end local v29    # "e":Lorg/json/JSONException;
    :catch_10c2
    move-exception v29

    .line 674
    .restart local v29    # "e":Lorg/json/JSONException;
    invoke-virtual/range {v29 .. v29}, Lorg/json/JSONException;->printStackTrace()V

    .line 675
    const-string v2, "Error LP: Error name of file read!"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    goto/16 :goto_57e

    .line 683
    .end local v29    # "e":Lorg/json/JSONException;
    :catch_10cd
    move-exception v29

    .line 684
    .restart local v29    # "e":Lorg/json/JSONException;
    const-string v2, "Error LP: Error permissions read!"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    goto/16 :goto_5bf

    .line 693
    .end local v29    # "e":Lorg/json/JSONException;
    :catch_10d5
    move-exception v29

    .line 694
    .restart local v29    # "e":Lorg/json/JSONException;
    invoke-virtual/range {v29 .. v29}, Lorg/json/JSONException;->printStackTrace()V

    .line 695
    const-string v2, "Error LP: Error name of file read!"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    goto/16 :goto_5dc

    .line 703
    .end local v29    # "e":Lorg/json/JSONException;
    :catch_10e0
    move-exception v29

    .line 704
    .restart local v29    # "e":Lorg/json/JSONException;
    const-string v2, "Error LP: Error file copy read!"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    goto/16 :goto_615

    .line 714
    .end local v29    # "e":Lorg/json/JSONException;
    :catch_10e8
    move-exception v29

    .line 715
    .restart local v29    # "e":Lorg/json/JSONException;
    const-string v2, "Error LP: Error name of libraries read!"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    goto/16 :goto_632

    .line 743
    .end local v29    # "e":Lorg/json/JSONException;
    :cond_10f0
    const/16 v2, 0xc8

    sput v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    goto/16 :goto_6a7

    .line 757
    :cond_10f6
    const/16 v2, 0xc8

    sput v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    goto/16 :goto_6d5

    .line 771
    :cond_10fc
    const/16 v2, 0xc8

    sput v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    goto/16 :goto_704

    .line 785
    :cond_1102
    const/16 v2, 0xc8

    sput v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    goto/16 :goto_733

    .line 842
    :catch_1108
    move-exception v29

    .line 843
    .restart local v29    # "e":Lorg/json/JSONException;
    const-string v2, "Error LP: Error original hex read!"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 844
    const-string v2, ""

    sput-object v2, Lcom/chelpus/root/utils/custompatch;->group:Ljava/lang/String;

    goto/16 :goto_803

    .line 856
    .end local v29    # "e":Lorg/json/JSONException;
    :catch_1114
    move-exception v29

    .line 857
    .restart local v29    # "e":Lorg/json/JSONException;
    const-string v2, "Error LP: Error original hex read!"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V
    :try_end_111a
    .catch Ljava/io/FileNotFoundException; {:try_start_10b3 .. :try_end_111a} :catch_967
    .catch Ljava/lang/Exception; {:try_start_10b3 .. :try_end_111a} :catch_9ff

    goto/16 :goto_83c

    .line 876
    .end local v29    # "e":Lorg/json/JSONException;
    .restart local v64    # "t":I
    :cond_111c
    const/4 v2, 0x0

    :try_start_111d
    aput v2, v4, v64
    :try_end_111f
    .catch Ljava/lang/Exception; {:try_start_111d .. :try_end_111f} :catch_1121
    .catch Ljava/io/FileNotFoundException; {:try_start_111d .. :try_end_111f} :catch_967

    goto/16 :goto_8a5

    .line 889
    :catch_1121
    move-exception v29

    .line 890
    .local v29, "e":Ljava/lang/Exception;
    :try_start_1122
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v29

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 893
    .end local v29    # "e":Ljava/lang/Exception;
    .end local v64    # "t":I
    :cond_113a
    aget-object v2, v65, v57

    const-string v7, "\"object\""

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_11f3

    aget-object v2, v65, v57

    const-string v7, "{"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_11f3

    aget-object v2, v65, v57

    const-string v7, "}"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_1155
    .catch Ljava/io/FileNotFoundException; {:try_start_1122 .. :try_end_1155} :catch_967
    .catch Ljava/lang/Exception; {:try_start_1122 .. :try_end_1155} :catch_9ff

    move-result v2

    if-eqz v2, :cond_11f3

    .line 896
    :try_start_1158
    new-instance v40, Lorg/json/JSONObject;

    aget-object v2, v65, v57

    move-object/from16 v0, v40

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 897
    .restart local v40    # "json_obj":Lorg/json/JSONObject;
    const-string v2, "object"

    move-object/from16 v0, v40

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1168
    .catch Lorg/json/JSONException; {:try_start_1158 .. :try_end_1168} :catch_12c4
    .catch Ljava/io/FileNotFoundException; {:try_start_1158 .. :try_end_1168} :catch_967
    .catch Ljava/lang/Exception; {:try_start_1158 .. :try_end_1168} :catch_9ff

    move-result-object v68

    .line 901
    .end local v40    # "json_obj":Lorg/json/JSONObject;
    :goto_1169
    :try_start_1169
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "dalvikvm -Xverify:none -Xdexopt:none -cp "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v7, 0x5

    aget-object v7, p0, v7

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, " "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v7, 0x8

    aget-object v7, p0, v7

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, ".nerorunpatch "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v7, 0x0

    aget-object v7, p0, v7

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, " "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, "object"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v68

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, "\n"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 902
    .local v19, "cmd":Ljava/lang/String;
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v47

    .line 903
    .local v47, "localProcess9":Ljava/lang/Process;
    invoke-virtual/range {v47 .. v47}, Ljava/lang/Process;->waitFor()I

    .line 904
    new-instance v46, Ljava/io/DataInputStream;

    invoke-virtual/range {v47 .. v47}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    move-object/from16 v0, v46

    invoke-direct {v0, v2}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 905
    .local v46, "localDataInputStream3":Ljava/io/DataInputStream;
    invoke-virtual/range {v46 .. v46}, Ljava/io/DataInputStream;->available()I

    move-result v2

    new-array v13, v2, [B

    .line 906
    .local v13, "arrayOfByte":[B
    move-object/from16 v0, v46

    invoke-virtual {v0, v13}, Ljava/io/DataInputStream;->read([B)I

    .line 907
    new-instance v61, Ljava/lang/String;

    move-object/from16 v0, v61

    invoke-direct {v0, v13}, Ljava/lang/String;-><init>([B)V

    .line 910
    .local v61, "str":Ljava/lang/String;
    invoke-virtual/range {v47 .. v47}, Ljava/lang/Process;->destroy()V

    .line 912
    const-string v2, "Done"

    move-object/from16 v0, v61

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_12cc

    .line 913
    const-string v62, "Object patched!\n\n"

    .line 914
    .local v62, "str2":Ljava/lang/String;
    invoke-static/range {v62 .. v62}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 915
    const/16 v63, 0x1

    .line 923
    :goto_11f0
    const/4 v2, 0x1

    sput-boolean v2, Lcom/chelpus/root/utils/custompatch;->manualpatch:Z

    .line 926
    .end local v13    # "arrayOfByte":[B
    .end local v19    # "cmd":Ljava/lang/String;
    .end local v46    # "localDataInputStream3":Ljava/io/DataInputStream;
    .end local v47    # "localProcess9":Ljava/lang/Process;
    .end local v61    # "str":Ljava/lang/String;
    .end local v62    # "str2":Ljava/lang/String;
    :cond_11f3
    aget-object v2, v65, v57

    const-string v7, "search"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1303

    aget-object v2, v65, v57

    const-string v7, "{"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1303

    aget-object v2, v65, v57

    const-string v7, "}"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_120e
    .catch Ljava/io/FileNotFoundException; {:try_start_1169 .. :try_end_120e} :catch_967
    .catch Ljava/lang/Exception; {:try_start_1169 .. :try_end_120e} :catch_9ff

    move-result v2

    if-eqz v2, :cond_1303

    .line 929
    :try_start_1211
    new-instance v40, Lorg/json/JSONObject;

    aget-object v2, v65, v57

    move-object/from16 v0, v40

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 930
    .restart local v40    # "json_obj":Lorg/json/JSONObject;
    const-string v2, "search"

    move-object/from16 v0, v40

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1221
    .catch Lorg/json/JSONException; {:try_start_1211 .. :try_end_1221} :catch_12d5
    .catch Ljava/io/FileNotFoundException; {:try_start_1211 .. :try_end_1221} :catch_967
    .catch Ljava/lang/Exception; {:try_start_1211 .. :try_end_1221} :catch_9ff

    move-result-object v68

    .line 934
    .end local v40    # "json_obj":Lorg/json/JSONObject;
    :goto_1222
    :try_start_1222
    invoke-virtual/range {v68 .. v68}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v68

    .line 935
    sget-boolean v2, Lcom/chelpus/root/utils/custompatch;->convert:Z

    if-eqz v2, :cond_122e

    .line 936
    invoke-static/range {v68 .. v68}, Lcom/chelpus/Utils;->rework(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v68

    .line 938
    :cond_122e
    const-string v2, "[ \t]+"

    move-object/from16 v0, v68

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    array-length v2, v2

    new-array v0, v2, [Ljava/lang/String;

    move-object/from16 v52, v0

    .line 939
    const-string v2, "[ \t]+"

    move-object/from16 v0, v68

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v52

    .line 940
    move-object/from16 v0, v52

    array-length v2, v0

    new-array v4, v2, [I

    .line 941
    move-object/from16 v0, v52

    array-length v2, v0

    new-array v3, v2, [B
    :try_end_124d
    .catch Ljava/io/FileNotFoundException; {:try_start_1222 .. :try_end_124d} :catch_967
    .catch Ljava/lang/Exception; {:try_start_1222 .. :try_end_124d} :catch_9ff

    .line 943
    const/16 v64, 0x0

    .restart local v64    # "t":I
    :goto_124f
    :try_start_124f
    move-object/from16 v0, v52

    array-length v2, v0

    move/from16 v0, v64

    if-ge v0, v2, :cond_12fa

    .line 944
    aget-object v2, v52, v64

    const-string v7, "*"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1270

    aget-object v2, v52, v64

    const-string v7, "**"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1270

    .line 945
    const/16 v31, 0x1

    .line 946
    const-string v2, "60"

    aput-object v2, v52, v64

    .line 948
    :cond_1270
    aget-object v2, v52, v64

    const-string v7, "**"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1284

    aget-object v2, v52, v64

    const-string v7, "\\?+"

    invoke-virtual {v2, v7}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_12dd

    .line 949
    :cond_1284
    const-string v2, "60"

    aput-object v2, v52, v64

    .line 950
    const/4 v2, 0x1

    aput v2, v4, v64

    .line 952
    :goto_128b
    aget-object v2, v52, v64

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "R"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_12b3

    .line 954
    aget-object v2, v52, v64

    const-string v7, "R"

    const-string v8, ""

    invoke-virtual {v2, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v51

    .line 956
    .restart local v51    # "or":Ljava/lang/String;
    invoke-static/range {v51 .. v51}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v69

    .line 957
    .restart local v69    # "y":I
    add-int/lit8 v69, v69, 0x2

    .line 958
    aput v69, v4, v64

    .line 959
    const-string v2, "60"

    aput-object v2, v52, v64

    .line 961
    .end local v51    # "or":Ljava/lang/String;
    .end local v69    # "y":I
    :cond_12b3
    aget-object v2, v52, v64

    const/16 v7, 0x10

    invoke-static {v2, v7}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->byteValue()B

    move-result v2

    aput-byte v2, v3, v64
    :try_end_12c1
    .catch Ljava/lang/Exception; {:try_start_124f .. :try_end_12c1} :catch_12e1
    .catch Ljava/io/FileNotFoundException; {:try_start_124f .. :try_end_12c1} :catch_967

    .line 943
    add-int/lit8 v64, v64, 0x1

    goto :goto_124f

    .line 898
    .end local v64    # "t":I
    :catch_12c4
    move-exception v29

    .line 899
    .local v29, "e":Lorg/json/JSONException;
    :try_start_12c5
    const-string v2, "Error LP: Error number by object!"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    goto/16 :goto_1169

    .line 918
    .end local v29    # "e":Lorg/json/JSONException;
    .restart local v13    # "arrayOfByte":[B
    .restart local v19    # "cmd":Ljava/lang/String;
    .restart local v46    # "localDataInputStream3":Ljava/io/DataInputStream;
    .restart local v47    # "localProcess9":Ljava/lang/Process;
    .restart local v61    # "str":Ljava/lang/String;
    :cond_12cc
    const-string v62, "Object not found!\n\n"

    .line 919
    .restart local v62    # "str2":Ljava/lang/String;
    invoke-static/range {v62 .. v62}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 920
    const/16 v63, 0x0

    goto/16 :goto_11f0

    .line 931
    .end local v13    # "arrayOfByte":[B
    .end local v19    # "cmd":Ljava/lang/String;
    .end local v46    # "localDataInputStream3":Ljava/io/DataInputStream;
    .end local v47    # "localProcess9":Ljava/lang/Process;
    .end local v61    # "str":Ljava/lang/String;
    .end local v62    # "str2":Ljava/lang/String;
    :catch_12d5
    move-exception v29

    .line 932
    .restart local v29    # "e":Lorg/json/JSONException;
    const-string v2, "Error LP: Error search hex read!"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V
    :try_end_12db
    .catch Ljava/io/FileNotFoundException; {:try_start_12c5 .. :try_end_12db} :catch_967
    .catch Ljava/lang/Exception; {:try_start_12c5 .. :try_end_12db} :catch_9ff

    goto/16 :goto_1222

    .line 951
    .end local v29    # "e":Lorg/json/JSONException;
    .restart local v64    # "t":I
    :cond_12dd
    const/4 v2, 0x0

    :try_start_12de
    aput v2, v4, v64
    :try_end_12e0
    .catch Ljava/lang/Exception; {:try_start_12de .. :try_end_12e0} :catch_12e1
    .catch Ljava/io/FileNotFoundException; {:try_start_12de .. :try_end_12e0} :catch_967

    goto :goto_128b

    .line 964
    :catch_12e1
    move-exception v29

    .line 965
    .local v29, "e":Ljava/lang/Exception;
    :try_start_12e2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v29

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 967
    .end local v29    # "e":Ljava/lang/Exception;
    :cond_12fa
    if-eqz v31, :cond_1449

    .line 968
    const/16 v60, 0x0

    .line 969
    const-string v2, "Error LP: Patterns to search not valid!\n"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 984
    .end local v64    # "t":I
    :cond_1303
    :goto_1303
    aget-object v2, v65, v57

    const-string v7, "replaced"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_14de

    aget-object v2, v65, v57

    const-string v7, "{"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_14de

    aget-object v2, v65, v57

    const-string v7, "}"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_131e
    .catch Ljava/io/FileNotFoundException; {:try_start_12e2 .. :try_end_131e} :catch_967
    .catch Ljava/lang/Exception; {:try_start_12e2 .. :try_end_131e} :catch_9ff

    move-result v2

    if-eqz v2, :cond_14de

    .line 987
    :try_start_1321
    new-instance v40, Lorg/json/JSONObject;

    aget-object v2, v65, v57

    move-object/from16 v0, v40

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 988
    .restart local v40    # "json_obj":Lorg/json/JSONObject;
    const-string v2, "replaced"

    move-object/from16 v0, v40

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1331
    .catch Lorg/json/JSONException; {:try_start_1321 .. :try_end_1331} :catch_1480
    .catch Ljava/io/FileNotFoundException; {:try_start_1321 .. :try_end_1331} :catch_967
    .catch Ljava/lang/Exception; {:try_start_1321 .. :try_end_1331} :catch_9ff

    move-result-object v67

    .line 992
    .end local v40    # "json_obj":Lorg/json/JSONObject;
    :goto_1332
    :try_start_1332
    invoke-virtual/range {v67 .. v67}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v67

    .line 993
    sget-boolean v2, Lcom/chelpus/root/utils/custompatch;->convert:Z

    if-eqz v2, :cond_133e

    .line 994
    invoke-static/range {v67 .. v67}, Lcom/chelpus/Utils;->rework(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v67

    .line 996
    :cond_133e
    const-string v2, "[ \t]+"

    move-object/from16 v0, v67

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    array-length v2, v2

    new-array v0, v2, [Ljava/lang/String;

    move-object/from16 v58, v0

    .line 997
    .local v58, "rephex":[Ljava/lang/String;
    const-string v2, "[ \t]+"

    move-object/from16 v0, v67

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v58

    .line 998
    move-object/from16 v0, v58

    array-length v2, v0

    new-array v6, v2, [I

    .line 999
    .local v6, "rep_mask":[I
    move-object/from16 v0, v58

    array-length v2, v0

    new-array v5, v2, [B
    :try_end_135d
    .catch Ljava/io/FileNotFoundException; {:try_start_1332 .. :try_end_135d} :catch_967
    .catch Ljava/lang/Exception; {:try_start_1332 .. :try_end_135d} :catch_9ff

    .line 1001
    .local v5, "byteReplace":[B
    const/16 v64, 0x0

    .restart local v64    # "t":I
    :goto_135f
    :try_start_135f
    move-object/from16 v0, v58

    array-length v2, v0

    move/from16 v0, v64

    if-ge v0, v2, :cond_14a6

    .line 1002
    aget-object v2, v58, v64

    const-string v7, "*"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1380

    aget-object v2, v58, v64

    const-string v7, "**"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1380

    .line 1003
    const/16 v31, 0x1

    .line 1004
    const-string v2, "60"

    aput-object v2, v58, v64

    .line 1006
    :cond_1380
    aget-object v2, v58, v64

    const-string v7, "**"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1394

    aget-object v2, v58, v64

    const-string v7, "\\?+"

    invoke-virtual {v2, v7}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1488

    .line 1007
    :cond_1394
    const-string v2, "60"

    aput-object v2, v58, v64

    .line 1008
    const/4 v2, 0x0

    aput v2, v6, v64

    .line 1010
    :goto_139b
    aget-object v2, v58, v64

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "sq"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_13b1

    .line 1011
    const-string v2, "60"

    aput-object v2, v58, v64

    .line 1012
    const/16 v2, 0xfd

    aput v2, v6, v64

    .line 1014
    :cond_13b1
    aget-object v2, v58, v64

    const-string v7, "s1"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_13c5

    aget-object v2, v58, v64

    const-string v7, "S1"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_13cd

    .line 1015
    :cond_13c5
    const-string v2, "60"

    aput-object v2, v58, v64

    .line 1016
    const/16 v2, 0xfe

    aput v2, v6, v64

    .line 1018
    :cond_13cd
    aget-object v2, v58, v64

    const-string v7, "s0"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_13e1

    aget-object v2, v58, v64

    const-string v7, "S0"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_13e9

    .line 1019
    :cond_13e1
    const-string v2, "60"

    aput-object v2, v58, v64

    .line 1020
    const/16 v2, 0xff

    aput v2, v6, v64

    .line 1022
    :cond_13e9
    aget-object v2, v58, v64

    const-string v7, "W"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1411

    aget-object v2, v58, v64

    const-string v7, "w"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1411

    aget-object v2, v58, v64

    const-string v7, "R"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1411

    aget-object v2, v58, v64

    const-string v7, "R"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1437

    .line 1024
    :cond_1411
    aget-object v2, v58, v64

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "w"

    const-string v8, ""

    invoke-virtual {v2, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    const-string v7, "r"

    const-string v8, ""

    invoke-virtual {v2, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v51

    .line 1026
    .restart local v51    # "or":Ljava/lang/String;
    invoke-static/range {v51 .. v51}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v69

    .line 1027
    .restart local v69    # "y":I
    add-int/lit8 v69, v69, 0x2

    .line 1028
    aput v69, v6, v64

    .line 1029
    const-string v2, "60"

    aput-object v2, v58, v64

    .line 1031
    .end local v51    # "or":Ljava/lang/String;
    .end local v69    # "y":I
    :cond_1437
    aget-object v2, v58, v64

    const/16 v7, 0x10

    invoke-static {v2, v7}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->byteValue()B

    move-result v2

    aput-byte v2, v5, v64
    :try_end_1445
    .catch Ljava/lang/Exception; {:try_start_135f .. :try_end_1445} :catch_148d
    .catch Ljava/io/FileNotFoundException; {:try_start_135f .. :try_end_1445} :catch_967

    .line 1001
    add-int/lit8 v64, v64, 0x1

    goto/16 :goto_135f

    .line 971
    .end local v5    # "byteReplace":[B
    .end local v6    # "rep_mask":[I
    .end local v58    # "rephex":[Ljava/lang/String;
    :cond_1449
    const/16 v48, 0x1

    .line 973
    :try_start_144b
    new-instance v45, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;

    move-object/from16 v0, v45

    invoke-direct {v0, v3, v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;-><init>([B[I)V

    .line 974
    .local v45, "local":Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;
    array-length v2, v3

    new-array v2, v2, [B

    move-object/from16 v0, v45

    iput-object v2, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->repByte:[B

    .line 975
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->ser:Ljava/util/ArrayList;

    move-object/from16 v0, v45

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1460
    .catch Ljava/lang/Exception; {:try_start_144b .. :try_end_1460} :catch_1462
    .catch Ljava/io/FileNotFoundException; {:try_start_144b .. :try_end_1460} :catch_967

    goto/16 :goto_1303

    .line 976
    .end local v45    # "local":Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;
    :catch_1462
    move-exception v29

    .line 977
    .restart local v29    # "e":Ljava/lang/Exception;
    :try_start_1463
    invoke-virtual/range {v29 .. v29}, Ljava/lang/Exception;->printStackTrace()V

    .line 978
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v29

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    goto/16 :goto_1303

    .line 989
    .end local v29    # "e":Ljava/lang/Exception;
    .end local v64    # "t":I
    :catch_1480
    move-exception v29

    .line 990
    .local v29, "e":Lorg/json/JSONException;
    const-string v2, "Error LP: Error replaced hex read!"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V
    :try_end_1486
    .catch Ljava/io/FileNotFoundException; {:try_start_1463 .. :try_end_1486} :catch_967
    .catch Ljava/lang/Exception; {:try_start_1463 .. :try_end_1486} :catch_9ff

    goto/16 :goto_1332

    .line 1009
    .end local v29    # "e":Lorg/json/JSONException;
    .restart local v5    # "byteReplace":[B
    .restart local v6    # "rep_mask":[I
    .restart local v58    # "rephex":[Ljava/lang/String;
    .restart local v64    # "t":I
    :cond_1488
    const/4 v2, 0x1

    :try_start_1489
    aput v2, v6, v64
    :try_end_148b
    .catch Ljava/lang/Exception; {:try_start_1489 .. :try_end_148b} :catch_148d
    .catch Ljava/io/FileNotFoundException; {:try_start_1489 .. :try_end_148b} :catch_967

    goto/16 :goto_139b

    .line 1033
    :catch_148d
    move-exception v29

    .line 1034
    .local v29, "e":Ljava/lang/Exception;
    :try_start_148e
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v29

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 1036
    .end local v29    # "e":Ljava/lang/Exception;
    :cond_14a6
    array-length v2, v6

    array-length v7, v4

    if-ne v2, v7, :cond_14b6

    array-length v2, v3

    array-length v7, v5

    if-ne v2, v7, :cond_14b6

    array-length v2, v5

    const/4 v7, 0x4

    if-lt v2, v7, :cond_14b6

    array-length v2, v3

    const/4 v7, 0x4

    if-ge v2, v7, :cond_14b8

    .line 1037
    :cond_14b6
    const/16 v31, 0x1

    .line 1038
    :cond_14b8
    if-eqz v31, :cond_14c1

    .line 1039
    const/16 v60, 0x0

    .line 1040
    const-string v2, "Error LP: Patterns original and replaced not valid!\n- Dimensions of the original hex-string and repleced must be >3.\n- Dimensions of the original hex-string and repleced must be equal.\n- Pattern hex must be: AF 11 4B ** AA **\nCheck the template file and try again!"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 1042
    :cond_14c1
    if-nez v31, :cond_14de

    .line 1043
    sget-boolean v2, Lcom/chelpus/root/utils/custompatch;->multilib_patch:Z

    if-nez v2, :cond_14cb

    sget-boolean v2, Lcom/chelpus/root/utils/custompatch;->multidex:Z

    if-eqz v2, :cond_1624

    :cond_14cb
    sget-object v70, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    new-instance v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;

    const-string v7, "all_lib"

    const/4 v8, 0x0

    invoke-direct/range {v2 .. v8}, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;-><init>([B[I[B[ILjava/lang/String;Z)V

    move-object/from16 v0, v70

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1045
    :goto_14da
    const-string v2, ""

    sput-object v2, Lcom/chelpus/root/utils/custompatch;->group:Ljava/lang/String;

    .line 1049
    .end local v5    # "byteReplace":[B
    .end local v6    # "rep_mask":[I
    .end local v58    # "rephex":[Ljava/lang/String;
    .end local v64    # "t":I
    :cond_14de
    aget-object v2, v65, v57

    const-string v7, "insert"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1683

    aget-object v2, v65, v57

    const-string v7, "{"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1683

    aget-object v2, v65, v57

    const-string v7, "}"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_14f9
    .catch Ljava/io/FileNotFoundException; {:try_start_148e .. :try_end_14f9} :catch_967
    .catch Ljava/lang/Exception; {:try_start_148e .. :try_end_14f9} :catch_9ff

    move-result v2

    if-eqz v2, :cond_1683

    .line 1052
    :try_start_14fc
    new-instance v40, Lorg/json/JSONObject;

    aget-object v2, v65, v57

    move-object/from16 v0, v40

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 1053
    .restart local v40    # "json_obj":Lorg/json/JSONObject;
    const-string v2, "insert"

    move-object/from16 v0, v40

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_150c
    .catch Lorg/json/JSONException; {:try_start_14fc .. :try_end_150c} :catch_1635
    .catch Ljava/io/FileNotFoundException; {:try_start_14fc .. :try_end_150c} :catch_967
    .catch Ljava/lang/Exception; {:try_start_14fc .. :try_end_150c} :catch_9ff

    move-result-object v67

    .line 1057
    .end local v40    # "json_obj":Lorg/json/JSONObject;
    :goto_150d
    :try_start_150d
    invoke-virtual/range {v67 .. v67}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v67

    .line 1058
    sget-boolean v2, Lcom/chelpus/root/utils/custompatch;->convert:Z

    if-eqz v2, :cond_1519

    .line 1059
    invoke-static/range {v67 .. v67}, Lcom/chelpus/Utils;->rework(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v67

    .line 1061
    :cond_1519
    const-string v2, "[ \t]+"

    move-object/from16 v0, v67

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    array-length v2, v2

    new-array v0, v2, [Ljava/lang/String;

    move-object/from16 v58, v0

    .line 1062
    .restart local v58    # "rephex":[Ljava/lang/String;
    const-string v2, "[ \t]+"

    move-object/from16 v0, v67

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v58

    .line 1063
    move-object/from16 v0, v58

    array-length v2, v0

    new-array v6, v2, [I

    .line 1064
    .restart local v6    # "rep_mask":[I
    move-object/from16 v0, v58

    array-length v2, v0

    new-array v5, v2, [B
    :try_end_1538
    .catch Ljava/io/FileNotFoundException; {:try_start_150d .. :try_end_1538} :catch_967
    .catch Ljava/lang/Exception; {:try_start_150d .. :try_end_1538} :catch_9ff

    .line 1066
    .restart local v5    # "byteReplace":[B
    const/16 v64, 0x0

    .restart local v64    # "t":I
    :goto_153a
    :try_start_153a
    move-object/from16 v0, v58

    array-length v2, v0

    move/from16 v0, v64

    if-ge v0, v2, :cond_165b

    .line 1067
    aget-object v2, v58, v64

    const-string v7, "*"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_155b

    aget-object v2, v58, v64

    const-string v7, "**"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_155b

    .line 1068
    const/16 v31, 0x1

    .line 1069
    const-string v2, "60"

    aput-object v2, v58, v64

    .line 1071
    :cond_155b
    aget-object v2, v58, v64

    const-string v7, "**"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_156f

    aget-object v2, v58, v64

    const-string v7, "\\?+"

    invoke-virtual {v2, v7}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_163d

    .line 1072
    :cond_156f
    const-string v2, "60"

    aput-object v2, v58, v64

    .line 1073
    const/4 v2, 0x0

    aput v2, v6, v64

    .line 1075
    :goto_1576
    aget-object v2, v58, v64

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "sq"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_158c

    .line 1076
    const-string v2, "60"

    aput-object v2, v58, v64

    .line 1077
    const/16 v2, 0xfd

    aput v2, v6, v64

    .line 1079
    :cond_158c
    aget-object v2, v58, v64

    const-string v7, "s1"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_15a0

    aget-object v2, v58, v64

    const-string v7, "S1"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_15a8

    .line 1080
    :cond_15a0
    const-string v2, "60"

    aput-object v2, v58, v64

    .line 1081
    const/16 v2, 0xfe

    aput v2, v6, v64

    .line 1083
    :cond_15a8
    aget-object v2, v58, v64

    const-string v7, "s0"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_15bc

    aget-object v2, v58, v64

    const-string v7, "S0"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_15c4

    .line 1084
    :cond_15bc
    const-string v2, "60"

    aput-object v2, v58, v64

    .line 1085
    const/16 v2, 0xff

    aput v2, v6, v64

    .line 1087
    :cond_15c4
    aget-object v2, v58, v64

    const-string v7, "W"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_15ec

    aget-object v2, v58, v64

    const-string v7, "w"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_15ec

    aget-object v2, v58, v64

    const-string v7, "R"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_15ec

    aget-object v2, v58, v64

    const-string v7, "R"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1612

    .line 1089
    :cond_15ec
    aget-object v2, v58, v64

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "w"

    const-string v8, ""

    invoke-virtual {v2, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    const-string v7, "r"

    const-string v8, ""

    invoke-virtual {v2, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v51

    .line 1091
    .restart local v51    # "or":Ljava/lang/String;
    invoke-static/range {v51 .. v51}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v69

    .line 1092
    .restart local v69    # "y":I
    add-int/lit8 v69, v69, 0x2

    .line 1093
    aput v69, v6, v64

    .line 1094
    const-string v2, "60"

    aput-object v2, v58, v64

    .line 1096
    .end local v51    # "or":Ljava/lang/String;
    .end local v69    # "y":I
    :cond_1612
    aget-object v2, v58, v64

    const/16 v7, 0x10

    invoke-static {v2, v7}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->byteValue()B

    move-result v2

    aput-byte v2, v5, v64
    :try_end_1620
    .catch Ljava/lang/Exception; {:try_start_153a .. :try_end_1620} :catch_1642
    .catch Ljava/io/FileNotFoundException; {:try_start_153a .. :try_end_1620} :catch_967

    .line 1066
    add-int/lit8 v64, v64, 0x1

    goto/16 :goto_153a

    .line 1044
    :cond_1624
    :try_start_1624
    sget-object v70, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    new-instance v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;

    sget-object v7, Lcom/chelpus/root/utils/custompatch;->group:Ljava/lang/String;

    const/4 v8, 0x0

    invoke-direct/range {v2 .. v8}, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;-><init>([B[I[B[ILjava/lang/String;Z)V

    move-object/from16 v0, v70

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_14da

    .line 1054
    .end local v5    # "byteReplace":[B
    .end local v6    # "rep_mask":[I
    .end local v58    # "rephex":[Ljava/lang/String;
    .end local v64    # "t":I
    :catch_1635
    move-exception v29

    .line 1055
    .local v29, "e":Lorg/json/JSONException;
    const-string v2, "Error LP: Error insert hex read!"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V
    :try_end_163b
    .catch Ljava/io/FileNotFoundException; {:try_start_1624 .. :try_end_163b} :catch_967
    .catch Ljava/lang/Exception; {:try_start_1624 .. :try_end_163b} :catch_9ff

    goto/16 :goto_150d

    .line 1074
    .end local v29    # "e":Lorg/json/JSONException;
    .restart local v5    # "byteReplace":[B
    .restart local v6    # "rep_mask":[I
    .restart local v58    # "rephex":[Ljava/lang/String;
    .restart local v64    # "t":I
    :cond_163d
    const/4 v2, 0x1

    :try_start_163e
    aput v2, v6, v64
    :try_end_1640
    .catch Ljava/lang/Exception; {:try_start_163e .. :try_end_1640} :catch_1642
    .catch Ljava/io/FileNotFoundException; {:try_start_163e .. :try_end_1640} :catch_967

    goto/16 :goto_1576

    .line 1098
    :catch_1642
    move-exception v29

    .line 1099
    .local v29, "e":Ljava/lang/Exception;
    :try_start_1643
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v29

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 1101
    .end local v29    # "e":Ljava/lang/Exception;
    :cond_165b
    array-length v2, v5

    const/4 v7, 0x4

    if-lt v2, v7, :cond_1663

    array-length v2, v3

    const/4 v7, 0x4

    if-ge v2, v7, :cond_1665

    :cond_1663
    const/16 v31, 0x1

    .line 1102
    :cond_1665
    if-eqz v31, :cond_166e

    .line 1103
    const/16 v60, 0x0

    .line 1104
    const-string v2, "Error LP: Dimensions of the original hex-string and repleced must be >3.\n- Pattern hex must be: AF 11 4B ** AA **\nCheck the template file and try again!"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 1106
    :cond_166e
    if-nez v31, :cond_1683

    .line 1107
    sget-object v70, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    new-instance v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;

    sget-object v7, Lcom/chelpus/root/utils/custompatch;->group:Ljava/lang/String;

    const/4 v8, 0x1

    invoke-direct/range {v2 .. v8}, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;-><init>([B[I[B[ILjava/lang/String;Z)V

    move-object/from16 v0, v70

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1108
    const-string v2, ""

    sput-object v2, Lcom/chelpus/root/utils/custompatch;->group:Ljava/lang/String;

    .line 1112
    .end local v5    # "byteReplace":[B
    .end local v6    # "rep_mask":[I
    .end local v58    # "rephex":[Ljava/lang/String;
    .end local v64    # "t":I
    :cond_1683
    aget-object v2, v65, v57

    const-string v7, "replace_from_file"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1723

    aget-object v2, v65, v57

    const-string v7, "{"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1723

    aget-object v2, v65, v57

    const-string v7, "}"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_169e
    .catch Ljava/io/FileNotFoundException; {:try_start_1643 .. :try_end_169e} :catch_967
    .catch Ljava/lang/Exception; {:try_start_1643 .. :try_end_169e} :catch_9ff

    move-result v2

    if-eqz v2, :cond_1723

    .line 1115
    :try_start_16a1
    new-instance v40, Lorg/json/JSONObject;

    aget-object v2, v65, v57

    move-object/from16 v0, v40

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 1116
    .restart local v40    # "json_obj":Lorg/json/JSONObject;
    const-string v2, "replace_from_file"

    move-object/from16 v0, v40

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_16b1
    .catch Lorg/json/JSONException; {:try_start_16a1 .. :try_end_16b1} :catch_1766
    .catch Ljava/io/FileNotFoundException; {:try_start_16a1 .. :try_end_16b1} :catch_967
    .catch Ljava/lang/Exception; {:try_start_16a1 .. :try_end_16b1} :catch_9ff

    move-result-object v67

    .line 1120
    .end local v40    # "json_obj":Lorg/json/JSONObject;
    :goto_16b2
    :try_start_16b2
    invoke-virtual/range {v67 .. v67}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v67

    .line 1122
    new-instance v12, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v7, Ljava/io/File;

    const/4 v8, 0x1

    aget-object v8, p0, v8

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v7}, Lcom/chelpus/Utils;->getDirs(Ljava/io/File;)Ljava/io/File;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, "/"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v67

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v12, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1123
    .local v12, "arrayFile":Ljava/io/File;
    invoke-virtual {v12}, Ljava/io/File;->length()J

    move-result-wide v7

    long-to-int v0, v7

    move/from16 v41, v0

    .line 1124
    .local v41, "len":I
    move/from16 v0, v41

    new-array v5, v0, [B
    :try_end_16eb
    .catch Ljava/io/FileNotFoundException; {:try_start_16b2 .. :try_end_16eb} :catch_967
    .catch Ljava/lang/Exception; {:try_start_16b2 .. :try_end_16eb} :catch_9ff

    .line 1128
    .restart local v5    # "byteReplace":[B
    :try_start_16eb
    new-instance v24, Ljava/io/FileInputStream;

    move-object/from16 v0, v24

    invoke-direct {v0, v12}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 1129
    .local v24, "data3":Ljava/io/FileInputStream;
    :cond_16f2
    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Ljava/io/FileInputStream;->read([B)I

    move-result v2

    if-gtz v2, :cond_16f2

    .line 1131
    invoke-virtual/range {v24 .. v24}, Ljava/io/FileInputStream;->close()V
    :try_end_16fd
    .catch Ljava/lang/Exception; {:try_start_16eb .. :try_end_16fd} :catch_176e
    .catch Ljava/io/FileNotFoundException; {:try_start_16eb .. :try_end_16fd} :catch_967

    .line 1135
    .end local v24    # "data3":Ljava/io/FileInputStream;
    :goto_16fd
    :try_start_16fd
    move/from16 v0, v41

    new-array v6, v0, [I

    .line 1136
    .restart local v6    # "rep_mask":[I
    const/4 v2, 0x1

    invoke-static {v6, v2}, Ljava/util/Arrays;->fill([II)V

    .line 1139
    if-eqz v31, :cond_170e

    .line 1140
    const/16 v60, 0x0

    .line 1141
    const-string v2, "Error LP: Patterns original and replaced not valid!\n- Dimensions of the original hex-string and repleced must be >3.\n- Dimensions of the original hex-string and repleced must be equal.\n- Pattern hex must be: AF 11 4B ** AA **\nCheck the template file and try again!"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 1143
    :cond_170e
    if-nez v31, :cond_1723

    .line 1144
    sget-object v70, Lcom/chelpus/root/utils/custompatch;->pat:Ljava/util/ArrayList;

    new-instance v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;

    sget-object v7, Lcom/chelpus/root/utils/custompatch;->group:Ljava/lang/String;

    const/4 v8, 0x0

    invoke-direct/range {v2 .. v8}, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;-><init>([B[I[B[ILjava/lang/String;Z)V

    move-object/from16 v0, v70

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1145
    const-string v2, ""

    sput-object v2, Lcom/chelpus/root/utils/custompatch;->group:Ljava/lang/String;

    .line 1149
    .end local v5    # "byteReplace":[B
    .end local v6    # "rep_mask":[I
    .end local v12    # "arrayFile":Ljava/io/File;
    .end local v41    # "len":I
    :cond_1723
    aget-object v2, v65, v57

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "[ADD-BOOT]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1736

    .line 1150
    const-string v2, "Patch on Reboot added!"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 1153
    :cond_1736
    if-eqz v30, :cond_1753

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v35

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, "\n"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v7, v65, v57

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    .line 1154
    :cond_1753
    const-string v2, "[END]"

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1762

    .line 1155
    const/4 v2, 0x4

    sput v2, Lcom/chelpus/root/utils/custompatch;->tag:I

    .line 1156
    const/16 v30, 0x1

    .line 1158
    :cond_1762
    add-int/lit8 v57, v57, 0x1

    goto/16 :goto_202

    .line 1117
    :catch_1766
    move-exception v29

    .line 1118
    .local v29, "e":Lorg/json/JSONException;
    const-string v2, "Error LP: Error replaced hex read!"

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    goto/16 :goto_16b2

    .line 1132
    .end local v29    # "e":Lorg/json/JSONException;
    .restart local v5    # "byteReplace":[B
    .restart local v12    # "arrayFile":Ljava/io/File;
    .restart local v41    # "len":I
    :catch_176e
    move-exception v29

    .line 1133
    .local v29, "e":Ljava/lang/Exception;
    invoke-virtual/range {v29 .. v29}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_16fd

    .line 1161
    .end local v5    # "byteReplace":[B
    .end local v12    # "arrayFile":Ljava/io/File;
    .end local v29    # "e":Ljava/lang/Exception;
    .end local v41    # "len":I
    :cond_1773
    invoke-virtual/range {v16 .. v16}, Ljava/io/BufferedReader;->close()V

    .line 1162
    if-eqz v63, :cond_1790

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ""

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v35

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 1163
    :cond_1790
    if-nez v63, :cond_179b

    sget-boolean v2, Lcom/chelpus/root/utils/custompatch;->patchteil:Z

    if-eqz v2, :cond_1818

    .line 1164
    const-string v2, "Not all patterns are replaced, but the program can work, test it!\nCustom Patch not valid for this Version of the Programm or already patched. "

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 1167
    :cond_179b
    :goto_179b
    if-eqz v21, :cond_17b5

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Changes Fix to: "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v22

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 1169
    :cond_17b5
    sget-boolean v2, Lcom/chelpus/root/utils/custompatch;->fixunpack:Z

    if-eqz v2, :cond_1810

    .line 1170
    const-string v2, "Analise Results:"

    invoke-static {v2}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 1171
    sget-object v2, Lcom/chelpus/root/utils/custompatch;->dir:Ljava/lang/String;

    sget-object v7, Lcom/chelpus/root/utils/custompatch;->classesFiles:Ljava/util/ArrayList;

    sget-object v8, Lcom/chelpus/root/utils/custompatch;->dirapp:Ljava/lang/String;

    sget-object v70, Lcom/chelpus/root/utils/custompatch;->uid:Ljava/lang/String;

    sget-object v71, Lcom/chelpus/root/utils/custompatch;->dirapp:Ljava/lang/String;

    sget-object v72, Lcom/chelpus/root/utils/custompatch;->uid:Ljava/lang/String;

    invoke-static/range {v71 .. v72}, Lcom/chelpus/Utils;->getOdexForCreate(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v71

    move-object/from16 v0, v70

    move-object/from16 v1, v71

    invoke-static {v2, v7, v8, v0, v1}, Lcom/chelpus/Utils;->create_ODEX_root(Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v59

    .line 1172
    .local v59, "res":I
    if-eqz v59, :cond_17f0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Create odex error "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v59

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 1173
    :cond_17f0
    if-nez v59, :cond_1810

    .line 1174
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "\n~Package reworked!~\nChanges Fix to: "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v7, 0x2

    aget-object v7, p0, v7

    const/4 v8, 0x1

    invoke-static {v7, v8}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 1178
    .end local v59    # "res":I
    :cond_1810
    invoke-virtual/range {v38 .. v38}, Ljava/io/InputStreamReader;->close()V

    .line 1179
    invoke-virtual/range {v36 .. v36}, Ljava/io/FileInputStream;->close()V

    goto/16 :goto_c4

    .line 1166
    :cond_1818
    const-string v2, "Custom Patch not valid for this Version of the Programm or already patched. "

    invoke-static {v2}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V
    :try_end_181d
    .catch Ljava/io/FileNotFoundException; {:try_start_16fd .. :try_end_181d} :catch_967
    .catch Ljava/lang/Exception; {:try_start_16fd .. :try_end_181d} :catch_9ff

    goto/16 :goto_179b

    .line 271
    nop

    :pswitch_data_1820
    .packed-switch 0x1
        :pswitch_90d
        :pswitch_b3b
        :pswitch_e0b
        :pswitch_356
        :pswitch_356
        :pswitch_bcb
        :pswitch_c5b
        :pswitch_ceb
        :pswitch_d7b
        :pswitch_ab3
        :pswitch_e38
        :pswitch_eb6
        :pswitch_ecd
        :pswitch_106e
        :pswitch_f35
    .end packed-switch
.end method

.method public static patchProcess(Ljava/util/ArrayList;)Z
    .registers 31
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1197
    .local p0, "patchlist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;>;"
    const/16 v23, 0x1

    .line 1198
    .local v23, "patch":Z
    if-eqz p0, :cond_20

    invoke-virtual/range {p0 .. p0}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_20

    .line 1199
    invoke-virtual/range {p0 .. p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_e
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_20

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;

    .line 1200
    .local v19, "it":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;
    const/4 v4, 0x0

    move-object/from16 v0, v19

    iput-boolean v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->result:Z

    goto :goto_e

    .line 1203
    .end local v19    # "it":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;
    :cond_20
    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "chmod"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "777"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    sget-object v5, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v3}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 1205
    :try_start_39
    new-instance v3, Ljava/io/RandomAccessFile;

    sget-object v4, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    const-string v5, "rw"

    invoke-direct {v3, v4, v5}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v2

    .line 1206
    .local v2, "ChannelDex":Ljava/nio/channels/FileChannel;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Size file:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/chelpus/Utils;->sendFromRootCP(Ljava/lang/String;)Z

    .line 1207
    sget-object v3, Ljava/nio/channels/FileChannel$MapMode;->READ_WRITE:Ljava/nio/channels/FileChannel$MapMode;

    const-wide/16 v4, 0x0

    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v6

    long-to-int v6, v6

    int-to-long v6, v6

    invoke-virtual/range {v2 .. v7}, Ljava/nio/channels/FileChannel;->map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;

    move-result-object v16

    .line 1209
    .local v16, "fileBytes":Ljava/nio/MappedByteBuffer;
    invoke-virtual/range {p0 .. p0}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v3

    array-length v3, v3

    new-array v0, v3, [Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;

    move-object/from16 v25, v0

    .line 1211
    .local v25, "patches":[Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;
    invoke-virtual/range {p0 .. p0}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v0, v3, [Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;

    move-object/from16 v25, v0

    .line 1212
    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;

    move-object v0, v3

    check-cast v0, [Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;

    move-object/from16 v25, v0
    :try_end_8e
    .catch Ljava/io/FileNotFoundException; {:try_start_39 .. :try_end_8e} :catch_49b
    .catch Ljava/nio/BufferUnderflowException; {:try_start_39 .. :try_end_8e} :catch_4ec
    .catch Ljava/lang/Exception; {:try_start_39 .. :try_end_8e} :catch_4d0

    .line 1214
    const/4 v13, -0x1

    .line 1217
    .local v13, "curentPos":I
    const/16 v26, 0x0

    .line 1218
    .local v26, "period":I
    const/4 v8, 0x0

    .line 1219
    .local v8, "Interapt":Z
    :cond_92
    :try_start_92
    invoke-virtual/range {v16 .. v16}, Ljava/nio/MappedByteBuffer;->hasRemaining()Z

    move-result v3

    if-eqz v3, :cond_348

    if-nez v8, :cond_348

    .line 1221
    invoke-virtual/range {v16 .. v16}, Ljava/nio/MappedByteBuffer;->position()I

    move-result v3

    sub-int v3, v3, v26

    const v4, 0x7d000

    if-le v3, v4, :cond_c3

    .line 1222
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Progress size:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {v16 .. v16}, Ljava/nio/MappedByteBuffer;->position()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/chelpus/Utils;->sendFromRootCP(Ljava/lang/String;)Z

    .line 1223
    invoke-virtual/range {v16 .. v16}, Ljava/nio/MappedByteBuffer;->position()I

    move-result v26

    .line 1225
    :cond_c3
    add-int/lit8 v3, v13, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1226
    invoke-virtual/range {v16 .. v16}, Ljava/nio/MappedByteBuffer;->position()I

    move-result v13

    .line 1227
    invoke-virtual/range {v16 .. v16}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v12

    .line 1229
    .local v12, "curentByte":B
    const/16 v17, 0x0

    .local v17, "g":I
    :goto_d4
    move-object/from16 v0, v25

    array-length v3, v0

    move/from16 v0, v17

    if-ge v0, v3, :cond_92

    .line 1230
    move-object/from16 v0, v16

    invoke-virtual {v0, v13}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1232
    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->origByte:[B

    const/4 v4, 0x0

    aget-byte v3, v3, v4

    if-eq v12, v3, :cond_114

    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->origMask:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    const/4 v4, 0x1

    if-eq v3, v4, :cond_114

    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->origMask:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    const/4 v4, 0x1

    if-le v3, v4, :cond_305

    sget-object v3, Lcom/chelpus/root/utils/custompatch;->search:Ljava/util/ArrayList;

    aget-object v4, v25, v17

    iget-object v4, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->origMask:[I

    const/4 v5, 0x0

    aget v4, v4, v5

    add-int/lit8 v4, v4, -0x2

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Byte;

    invoke-virtual {v3}, Ljava/lang/Byte;->byteValue()B

    move-result v3

    if-ne v12, v3, :cond_305

    .line 1234
    :cond_114
    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repMask:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    if-nez v3, :cond_124

    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repByte:[B

    const/4 v4, 0x0

    aput-byte v12, v3, v4
    :try_end_124
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_92 .. :try_end_124} :catch_33f
    .catch Ljava/nio/BufferUnderflowException; {:try_start_92 .. :try_end_124} :catch_3ce
    .catch Ljava/lang/Exception; {:try_start_92 .. :try_end_124} :catch_4ee
    .catch Ljava/io/FileNotFoundException; {:try_start_92 .. :try_end_124} :catch_49b

    .line 1236
    :cond_124
    :try_start_124
    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repMask:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    const/4 v4, 0x1

    if-le v3, v4, :cond_155

    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repMask:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    const/16 v4, 0xfd

    if-ge v3, v4, :cond_155

    aget-object v3, v25, v17

    iget-object v4, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repByte:[B

    const/4 v5, 0x0

    sget-object v3, Lcom/chelpus/root/utils/custompatch;->search:Ljava/util/ArrayList;

    aget-object v6, v25, v17

    iget-object v6, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repMask:[I

    const/4 v7, 0x0

    aget v6, v6, v7

    add-int/lit8 v6, v6, -0x2

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Byte;

    invoke-virtual {v3}, Ljava/lang/Byte;->byteValue()B

    move-result v3

    aput-byte v3, v4, v5
    :try_end_155
    .catch Ljava/lang/Exception; {:try_start_124 .. :try_end_155} :catch_309
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_124 .. :try_end_155} :catch_33f
    .catch Ljava/nio/BufferUnderflowException; {:try_start_124 .. :try_end_155} :catch_3ce
    .catch Ljava/io/FileNotFoundException; {:try_start_124 .. :try_end_155} :catch_49b

    .line 1238
    :cond_155
    :goto_155
    :try_start_155
    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repMask:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    const/16 v4, 0xfd

    if-ne v3, v4, :cond_16f

    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repByte:[B

    const/4 v4, 0x0

    and-int/lit8 v5, v12, 0xf

    and-int/lit8 v6, v12, 0xf

    mul-int/lit8 v6, v6, 0x10

    add-int/2addr v5, v6

    int-to-byte v5, v5

    aput-byte v5, v3, v4

    .line 1239
    :cond_16f
    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repMask:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    const/16 v4, 0xfe

    if-ne v3, v4, :cond_186

    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repByte:[B

    const/4 v4, 0x0

    and-int/lit8 v5, v12, 0xf

    add-int/lit8 v5, v5, 0x10

    int-to-byte v5, v5

    aput-byte v5, v3, v4

    .line 1240
    :cond_186
    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repMask:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    const/16 v4, 0xff

    if-ne v3, v4, :cond_19b

    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repByte:[B

    const/4 v4, 0x0

    and-int/lit8 v5, v12, 0xf

    int-to-byte v5, v5

    aput-byte v5, v3, v4

    .line 1241
    :cond_19b
    const/16 v18, 0x1

    .line 1242
    .local v18, "i":I
    add-int/lit8 v3, v13, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1243
    invoke-virtual/range {v16 .. v16}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v27

    .line 1245
    .local v27, "prufbyte":B
    :goto_1a8
    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->origByte:[B

    aget-byte v3, v3, v18

    move/from16 v0, v27

    if-eq v0, v3, :cond_1dc

    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->origMask:[I

    aget v3, v3, v18

    const/4 v4, 0x1

    if-le v3, v4, :cond_1d3

    sget-object v3, Lcom/chelpus/root/utils/custompatch;->search:Ljava/util/ArrayList;

    aget-object v4, v25, v17

    iget-object v4, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->origMask:[I

    aget v4, v4, v18

    add-int/lit8 v4, v4, -0x2

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Byte;

    invoke-virtual {v3}, Ljava/lang/Byte;->byteValue()B

    move-result v3

    move/from16 v0, v27

    if-eq v0, v3, :cond_1dc

    :cond_1d3
    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->origMask:[I

    aget v3, v3, v18
    :try_end_1d9
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_155 .. :try_end_1d9} :catch_33f
    .catch Ljava/nio/BufferUnderflowException; {:try_start_155 .. :try_end_1d9} :catch_3ce
    .catch Ljava/lang/Exception; {:try_start_155 .. :try_end_1d9} :catch_4ee
    .catch Ljava/io/FileNotFoundException; {:try_start_155 .. :try_end_1d9} :catch_49b

    const/4 v4, 0x1

    if-ne v3, v4, :cond_305

    .line 1248
    :cond_1dc
    :try_start_1dc
    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repMask:[I

    aget v3, v3, v18

    if-nez v3, :cond_1ea

    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repByte:[B

    aput-byte v27, v3, v18

    .line 1250
    :cond_1ea
    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repMask:[I

    aget v3, v3, v18

    const/4 v4, 0x1

    if-le v3, v4, :cond_219

    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repMask:[I

    aget v3, v3, v18

    const/16 v4, 0xfd

    if-ge v3, v4, :cond_219

    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repMask:[I

    aget v3, v3, v18

    add-int/lit8 v29, v3, -0x2

    .local v29, "y":I
    aget-object v3, v25, v17

    iget-object v4, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repByte:[B

    sget-object v3, Lcom/chelpus/root/utils/custompatch;->search:Ljava/util/ArrayList;

    move/from16 v0, v29

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Byte;

    invoke-virtual {v3}, Ljava/lang/Byte;->byteValue()B

    move-result v3

    aput-byte v3, v4, v18
    :try_end_219
    .catch Ljava/lang/Exception; {:try_start_1dc .. :try_end_219} :catch_399
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1dc .. :try_end_219} :catch_33f
    .catch Ljava/nio/BufferUnderflowException; {:try_start_1dc .. :try_end_219} :catch_3ce
    .catch Ljava/io/FileNotFoundException; {:try_start_1dc .. :try_end_219} :catch_49b

    .line 1252
    .end local v29    # "y":I
    :cond_219
    :goto_219
    :try_start_219
    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repMask:[I

    aget v3, v3, v18

    const/16 v4, 0xfd

    if-ne v3, v4, :cond_231

    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repByte:[B

    and-int/lit8 v4, v12, 0xf

    and-int/lit8 v5, v12, 0xf

    mul-int/lit8 v5, v5, 0x10

    add-int/2addr v4, v5

    int-to-byte v4, v4

    aput-byte v4, v3, v18

    .line 1253
    :cond_231
    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repMask:[I

    aget v3, v3, v18

    const/16 v4, 0xfe

    if-ne v3, v4, :cond_246

    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repByte:[B

    and-int/lit8 v4, v27, 0xf

    add-int/lit8 v4, v4, 0x10

    int-to-byte v4, v4

    aput-byte v4, v3, v18

    .line 1254
    :cond_246
    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repMask:[I

    aget v3, v3, v18

    const/16 v4, 0xff

    if-ne v3, v4, :cond_259

    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repByte:[B

    and-int/lit8 v4, v27, 0xf

    int-to-byte v4, v4

    aput-byte v4, v3, v18

    .line 1256
    :cond_259
    add-int/lit8 v18, v18, 0x1

    .line 1258
    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->origByte:[B

    array-length v3, v3

    move/from16 v0, v18

    if-ne v0, v3, :cond_3d5

    .line 1260
    const/4 v15, 0x0

    .line 1261
    .local v15, "f":Z
    aget-object v3, v25, v17

    iget-object v4, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->origMask:[I

    array-length v5, v4

    const/4 v3, 0x0

    :goto_26b
    if-ge v3, v5, :cond_272

    aget v9, v4, v3

    .local v9, "b":I
    if-nez v9, :cond_3d1

    const/4 v15, 0x1

    .line 1262
    .end local v9    # "b":I
    :cond_272
    if-nez v15, :cond_275

    const/4 v8, 0x1

    .line 1265
    :cond_275
    aget-object v3, v25, v17

    iget-boolean v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->insert:Z

    if-eqz v3, :cond_2bf

    .line 1266
    invoke-virtual/range {v16 .. v16}, Ljava/nio/MappedByteBuffer;->position()I

    move-result v22

    .line 1267
    .local v22, "p":I
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v3

    long-to-int v3, v3

    sub-int v20, v3, v22

    .line 1270
    .local v20, "lenght":I
    move/from16 v0, v20

    new-array v10, v0, [B

    .line 1271
    .local v10, "buf":[B
    const/4 v3, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v20

    invoke-virtual {v0, v10, v3, v1}, Ljava/nio/MappedByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 1273
    invoke-static {v10}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v11

    .line 1274
    .local v11, "buffer":Ljava/nio/ByteBuffer;
    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repByte:[B

    array-length v3, v3

    aget-object v4, v25, v17

    iget-object v4, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->origByte:[B

    array-length v4, v4

    sub-int/2addr v3, v4

    add-int v3, v3, v22

    int-to-long v3, v3

    invoke-virtual {v2, v3, v4}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 1275
    invoke-virtual {v2, v11}, Ljava/nio/channels/FileChannel;->write(Ljava/nio/ByteBuffer;)I

    .line 1280
    sget-object v3, Ljava/nio/channels/FileChannel$MapMode;->READ_WRITE:Ljava/nio/channels/FileChannel$MapMode;

    const-wide/16 v4, 0x0

    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v6

    long-to-int v6, v6

    int-to-long v6, v6

    invoke-virtual/range {v2 .. v7}, Ljava/nio/channels/FileChannel;->map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;

    move-result-object v16

    .line 1281
    move-object/from16 v0, v16

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1283
    .end local v10    # "buf":[B
    .end local v11    # "buffer":Ljava/nio/ByteBuffer;
    .end local v20    # "lenght":I
    .end local v22    # "p":I
    :cond_2bf
    int-to-long v3, v13

    invoke-virtual {v2, v3, v4}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 1284
    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repByte:[B

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v11

    .line 1285
    .restart local v11    # "buffer":Ljava/nio/ByteBuffer;
    invoke-virtual {v2, v11}, Ljava/nio/channels/FileChannel;->write(Ljava/nio/ByteBuffer;)I

    .line 1286
    invoke-virtual/range {v16 .. v16}, Ljava/nio/MappedByteBuffer;->force()Ljava/nio/MappedByteBuffer;

    .line 1287
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "\nPattern N"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    add-int/lit8 v4, v17, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": Patch done! \n(Offset: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v13}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 1288
    aget-object v3, v25, v17

    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->result:Z

    .line 1289
    const/4 v3, 0x1

    sput-boolean v3, Lcom/chelpus/root/utils/custompatch;->patchteil:Z

    .line 1229
    .end local v11    # "buffer":Ljava/nio/ByteBuffer;
    .end local v15    # "f":Z
    .end local v18    # "i":I
    .end local v27    # "prufbyte":B
    :cond_305
    add-int/lit8 v17, v17, 0x1

    goto/16 :goto_d4

    .line 1237
    :catch_309
    move-exception v14

    .local v14, "e":Ljava/lang/Exception;
    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repMask:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    add-int/lit8 v29, v3, -0x2

    .restart local v29    # "y":I
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Byte N"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v29

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " not found! Please edit search pattern for byte "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v29

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V
    :try_end_33d
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_219 .. :try_end_33d} :catch_33f
    .catch Ljava/nio/BufferUnderflowException; {:try_start_219 .. :try_end_33d} :catch_3ce
    .catch Ljava/lang/Exception; {:try_start_219 .. :try_end_33d} :catch_4ee
    .catch Ljava/io/FileNotFoundException; {:try_start_219 .. :try_end_33d} :catch_49b

    goto/16 :goto_155

    .line 1304
    .end local v12    # "curentByte":B
    .end local v14    # "e":Ljava/lang/Exception;
    .end local v17    # "g":I
    .end local v29    # "y":I
    :catch_33f
    move-exception v14

    .line 1305
    .local v14, "e":Ljava/lang/IndexOutOfBoundsException;
    :try_start_340
    invoke-virtual {v14}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    .line 1306
    const-string v3, "Byte by search not found! Please edit pattern for search.\n"

    invoke-static {v3}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 1311
    .end local v14    # "e":Ljava/lang/IndexOutOfBoundsException;
    :cond_348
    :goto_348
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V

    .line 1312
    const-string v3, "Analise Results:"

    invoke-static {v3}, Lcom/chelpus/Utils;->sendFromRootCP(Ljava/lang/String;)Z

    .line 1313
    const/16 v17, 0x0

    .restart local v17    # "g":I
    :goto_352
    move-object/from16 v0, v25

    array-length v3, v0

    move/from16 v0, v17

    if-ge v0, v3, :cond_4a1

    .line 1315
    aget-object v3, v25, v17

    iget-boolean v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->result:Z

    if-nez v3, :cond_483

    .line 1317
    const/16 v28, 0x0

    .line 1318
    .local v28, "trueGroup":Z
    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->group:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3db

    .line 1319
    move-object/from16 v0, v25

    array-length v4, v0

    const/4 v3, 0x0

    :goto_371
    if-ge v3, v4, :cond_3db

    aget-object v24, v25, v3

    .line 1320
    .local v24, "patche":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;
    aget-object v5, v25, v17

    iget-object v5, v5, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->group:Ljava/lang/String;

    move-object/from16 v0, v24

    iget-object v6, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->group:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_396

    move-object/from16 v0, v24

    iget-boolean v5, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->result:Z

    if-eqz v5, :cond_396

    .line 1321
    sget-boolean v5, Lcom/chelpus/root/utils/custompatch;->multidex:Z

    if-nez v5, :cond_391

    sget-boolean v5, Lcom/chelpus/root/utils/custompatch;->multilib_patch:Z

    if-eqz v5, :cond_394

    .line 1323
    :cond_391
    const/4 v5, 0x1

    sput-boolean v5, Lcom/chelpus/root/utils/custompatch;->goodResult:Z
    :try_end_394
    .catch Ljava/io/FileNotFoundException; {:try_start_340 .. :try_end_394} :catch_49b
    .catch Ljava/nio/BufferUnderflowException; {:try_start_340 .. :try_end_394} :catch_4ec
    .catch Ljava/lang/Exception; {:try_start_340 .. :try_end_394} :catch_4d0

    .line 1325
    :cond_394
    const/16 v28, 0x1

    .line 1319
    :cond_396
    add-int/lit8 v3, v3, 0x1

    goto :goto_371

    .line 1251
    .end local v24    # "patche":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;
    .end local v28    # "trueGroup":Z
    .restart local v12    # "curentByte":B
    .restart local v18    # "i":I
    .restart local v27    # "prufbyte":B
    :catch_399
    move-exception v14

    .local v14, "e":Ljava/lang/Exception;
    :try_start_39a
    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repMask:[I

    aget v3, v3, v18

    add-int/lit8 v29, v3, -0x2

    .restart local v29    # "y":I
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Byte N"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v29

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " not found! Please edit search pattern for byte "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v29

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    goto/16 :goto_219

    .line 1307
    .end local v12    # "curentByte":B
    .end local v14    # "e":Ljava/lang/Exception;
    .end local v17    # "g":I
    .end local v18    # "i":I
    .end local v27    # "prufbyte":B
    .end local v29    # "y":I
    :catch_3ce
    move-exception v3

    goto/16 :goto_348

    .line 1261
    .restart local v9    # "b":I
    .restart local v12    # "curentByte":B
    .restart local v15    # "f":Z
    .restart local v17    # "g":I
    .restart local v18    # "i":I
    .restart local v27    # "prufbyte":B
    :cond_3d1
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_26b

    .line 1296
    .end local v9    # "b":I
    .end local v15    # "f":Z
    :cond_3d5
    invoke-virtual/range {v16 .. v16}, Ljava/nio/MappedByteBuffer;->get()B
    :try_end_3d8
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_39a .. :try_end_3d8} :catch_33f
    .catch Ljava/nio/BufferUnderflowException; {:try_start_39a .. :try_end_3d8} :catch_3ce
    .catch Ljava/lang/Exception; {:try_start_39a .. :try_end_3d8} :catch_4ee
    .catch Ljava/io/FileNotFoundException; {:try_start_39a .. :try_end_3d8} :catch_49b

    move-result v27

    goto/16 :goto_1a8

    .line 1329
    .end local v12    # "curentByte":B
    .end local v18    # "i":I
    .end local v27    # "prufbyte":B
    .restart local v28    # "trueGroup":Z
    :cond_3db
    if-nez v28, :cond_41f

    :try_start_3dd
    sget-boolean v3, Lcom/chelpus/root/utils/custompatch;->goodResult:Z

    if-nez v3, :cond_41f

    .line 1330
    sget-boolean v3, Lcom/chelpus/root/utils/custompatch;->multidex:Z

    if-eqz v3, :cond_423

    .line 1331
    sget-boolean v3, Lcom/chelpus/root/utils/custompatch;->goodResult:Z

    if-nez v3, :cond_41f

    sget-object v3, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    sget-object v4, Lcom/chelpus/root/utils/custompatch;->classesFiles:Ljava/util/ArrayList;

    sget-object v5, Lcom/chelpus/root/utils/custompatch;->classesFiles:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_41f

    .line 1332
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "\nPattern N"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    add-int/lit8 v4, v17, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":\nError LP: Pattern not found!\nor patch is already applied?!\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 1333
    const/16 v23, 0x0

    .line 1313
    .end local v28    # "trueGroup":Z
    :cond_41f
    :goto_41f
    add-int/lit8 v17, v17, 0x1

    goto/16 :goto_352

    .line 1336
    .restart local v28    # "trueGroup":Z
    :cond_423
    sget-boolean v3, Lcom/chelpus/root/utils/custompatch;->multilib_patch:Z

    if-eqz v3, :cond_462

    .line 1337
    sget-boolean v3, Lcom/chelpus/root/utils/custompatch;->goodResult:Z

    if-nez v3, :cond_41f

    sget-object v3, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    sget-object v4, Lcom/chelpus/root/utils/custompatch;->arrayFile2:Ljava/util/ArrayList;

    sget-object v5, Lcom/chelpus/root/utils/custompatch;->arrayFile2:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_41f

    .line 1338
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "\nPattern N"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    add-int/lit8 v4, v17, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":\nError LP: Pattern not found!\nor patch is already applied?!\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 1339
    const/16 v23, 0x0

    goto :goto_41f

    .line 1342
    :cond_462
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "\nPattern N"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    add-int/lit8 v4, v17, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":\nError LP: Pattern not found!\nor patch is already applied?!\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 1343
    const/16 v23, 0x0

    goto :goto_41f

    .line 1350
    .end local v28    # "trueGroup":Z
    :cond_483
    sget-boolean v3, Lcom/chelpus/root/utils/custompatch;->multidex:Z

    if-nez v3, :cond_48b

    sget-boolean v3, Lcom/chelpus/root/utils/custompatch;->multilib_patch:Z

    if-eqz v3, :cond_41f

    :cond_48b
    aget-object v3, v25, v17

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->group:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_41f

    const/4 v3, 0x1

    sput-boolean v3, Lcom/chelpus/root/utils/custompatch;->goodResult:Z
    :try_end_49a
    .catch Ljava/io/FileNotFoundException; {:try_start_3dd .. :try_end_49a} :catch_49b
    .catch Ljava/nio/BufferUnderflowException; {:try_start_3dd .. :try_end_49a} :catch_4ec
    .catch Ljava/lang/Exception; {:try_start_3dd .. :try_end_49a} :catch_4d0

    goto :goto_41f

    .line 1355
    .end local v2    # "ChannelDex":Ljava/nio/channels/FileChannel;
    .end local v8    # "Interapt":Z
    .end local v13    # "curentPos":I
    .end local v16    # "fileBytes":Ljava/nio/MappedByteBuffer;
    .end local v17    # "g":I
    .end local v25    # "patches":[Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;
    .end local v26    # "period":I
    :catch_49b
    move-exception v21

    .line 1356
    .local v21, "localFileNotFoundException":Ljava/io/FileNotFoundException;
    const-string v3, "Error LP: Program files are not found!\nMove Program to internal storage."

    invoke-static {v3}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 1364
    .end local v21    # "localFileNotFoundException":Ljava/io/FileNotFoundException;
    :cond_4a1
    :goto_4a1
    sget v3, Lcom/chelpus/root/utils/custompatch;->tag:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_4aa

    sget-boolean v3, Lcom/chelpus/root/utils/custompatch;->unpack:Z

    if-eqz v3, :cond_4b0

    :cond_4aa
    sget v3, Lcom/chelpus/root/utils/custompatch;->tag:I

    const/16 v4, 0xa

    if-ne v3, v4, :cond_4bb

    .line 1365
    :cond_4b0
    sget-boolean v3, Lcom/chelpus/root/utils/custompatch;->ART:Z

    if-nez v3, :cond_4bb

    sget-object v3, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    sget-object v4, Lcom/chelpus/root/utils/custompatch;->dirapp:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/chelpus/Utils;->fixadlerOdex(Ljava/io/File;Ljava/lang/String;)V

    .line 1367
    :cond_4bb
    sget-boolean v3, Lcom/chelpus/root/utils/custompatch;->unpack:Z

    if-eqz v3, :cond_4cf

    sget-boolean v3, Lcom/chelpus/root/utils/custompatch;->odexpatch:Z

    if-nez v3, :cond_4cf

    sget-boolean v3, Lcom/chelpus/root/utils/custompatch;->OdexPatch:Z

    if-nez v3, :cond_4cf

    .line 1368
    sget-object v3, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-static {v3}, Lcom/chelpus/Utils;->fixadler(Ljava/io/File;)V

    .line 1370
    const/4 v3, 0x1

    sput-boolean v3, Lcom/chelpus/root/utils/custompatch;->fixunpack:Z

    .line 1372
    :cond_4cf
    return v23

    .line 1360
    :catch_4d0
    move-exception v14

    .line 1361
    .restart local v14    # "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception e"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v14}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    goto :goto_4a1

    .line 1357
    .end local v14    # "e":Ljava/lang/Exception;
    :catch_4ec
    move-exception v3

    goto :goto_4a1

    .line 1308
    .restart local v2    # "ChannelDex":Ljava/nio/channels/FileChannel;
    .restart local v8    # "Interapt":Z
    .restart local v13    # "curentPos":I
    .restart local v16    # "fileBytes":Ljava/nio/MappedByteBuffer;
    .restart local v25    # "patches":[Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;
    .restart local v26    # "period":I
    :catch_4ee
    move-exception v3

    goto/16 :goto_348
.end method

.method public static searchDalvik(Ljava/lang/String;)V
    .registers 7
    .param p0, "apk_file"    # Ljava/lang/String;

    .prologue
    .line 1475
    invoke-static {p0}, Lcom/chelpus/Utils;->getFileDalvikCache(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 1479
    .local v0, "dalv":Ljava/io/File;
    if-eqz v0, :cond_8

    .line 1480
    :try_start_6
    sput-object v0, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 1482
    :cond_8
    sget-boolean v3, Lcom/chelpus/root/utils/custompatch;->odexpatch:Z

    if-eqz v3, :cond_1a

    .line 1483
    new-instance v3, Ljava/io/File;

    sget-object v4, Lcom/chelpus/root/utils/custompatch;->dirapp:Ljava/lang/String;

    const/4 v5, 0x1

    invoke-static {v4, v5}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v3, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;
    :try_end_1a
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_1a} :catch_3e
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_1a} :catch_59

    .line 1487
    :cond_1a
    :try_start_1a
    sget-object v3, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_30

    new-instance v3, Ljava/io/File;

    sget-object v4, Lcom/chelpus/root/utils/custompatch;->dirapp:Ljava/lang/String;

    const/4 v5, 0x1

    invoke-static {v4, v5}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v3, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;
    :try_end_30
    .catch Ljava/lang/Exception; {:try_start_1a .. :try_end_30} :catch_49
    .catch Ljava/io/FileNotFoundException; {:try_start_1a .. :try_end_30} :catch_3e

    .line 1490
    :cond_30
    :goto_30
    :try_start_30
    sget-object v3, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_48

    new-instance v3, Ljava/io/FileNotFoundException;

    invoke-direct {v3}, Ljava/io/FileNotFoundException;-><init>()V

    throw v3
    :try_end_3e
    .catch Ljava/io/FileNotFoundException; {:try_start_30 .. :try_end_3e} :catch_3e
    .catch Ljava/lang/Exception; {:try_start_30 .. :try_end_3e} :catch_59

    .line 1492
    :catch_3e
    move-exception v2

    .line 1493
    .local v2, "localFileNotFoundException":Ljava/io/FileNotFoundException;
    sget-boolean v3, Lcom/chelpus/root/utils/custompatch;->system:Z

    if-nez v3, :cond_48

    const-string v3, "Error LP: Program files are not found!\n\nCheck the location dalvik-cache to solve problems!\n\nDefault: /data/dalvik-cache/*"

    invoke-static {v3}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 1499
    .end local v2    # "localFileNotFoundException":Ljava/io/FileNotFoundException;
    :cond_48
    :goto_48
    return-void

    .line 1489
    :catch_49
    move-exception v1

    .local v1, "e":Ljava/lang/Exception;
    :try_start_4a
    new-instance v3, Ljava/io/File;

    sget-object v4, Lcom/chelpus/root/utils/custompatch;->dirapp:Ljava/lang/String;

    const/4 v5, 0x1

    invoke-static {v4, v5}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v3, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;
    :try_end_58
    .catch Ljava/io/FileNotFoundException; {:try_start_4a .. :try_end_58} :catch_3e
    .catch Ljava/lang/Exception; {:try_start_4a .. :try_end_58} :catch_59

    goto :goto_30

    .line 1495
    .end local v1    # "e":Ljava/lang/Exception;
    :catch_59
    move-exception v1

    .line 1496
    .restart local v1    # "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    goto :goto_48
.end method

.method public static searchDalvikOdex(Ljava/lang/String;Ljava/lang/String;)V
    .registers 9
    .param p0, "param"    # Ljava/lang/String;
    .param p1, "param2"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .prologue
    .line 1503
    invoke-static {p0}, Lcom/chelpus/root/utils/custompatch;->searchDalvik(Ljava/lang/String;)V

    .line 1507
    :try_start_3
    sget-boolean v3, Lcom/chelpus/root/utils/custompatch;->odexpatch:Z

    if-nez v3, :cond_ff

    .line 1508
    sget-object v3, Lcom/chelpus/root/utils/custompatch;->uid:Ljava/lang/String;

    invoke-static {p1, v3}, Lcom/chelpus/Utils;->getOdexForCreate(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1509
    .local v1, "backTemp":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1511
    .local v0, "backFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_1b

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 1512
    :cond_1b
    new-instance v0, Ljava/io/File;

    .end local v0    # "backFile":Ljava/io/File;
    const-string v3, "-2"

    const-string v4, "-1"

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1513
    .restart local v0    # "backFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_31

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 1514
    :cond_31
    new-instance v0, Ljava/io/File;

    .end local v0    # "backFile":Ljava/io/File;
    const-string v3, "-1"

    const-string v4, "-2"

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1515
    .restart local v0    # "backFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_47

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 1516
    :cond_47
    new-instance v0, Ljava/io/File;

    .end local v0    # "backFile":Ljava/io/File;
    const-string v3, "-2"

    const-string v4, ""

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1517
    .restart local v0    # "backFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_5d

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 1518
    :cond_5d
    new-instance v0, Ljava/io/File;

    .end local v0    # "backFile":Ljava/io/File;
    const-string v3, "-1"

    const-string v4, ""

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1519
    .restart local v0    # "backFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_73

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 1520
    :cond_73
    new-instance v0, Ljava/io/File;

    .end local v0    # "backFile":Ljava/io/File;
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1521
    .restart local v0    # "backFile":Ljava/io/File;
    sget-object v3, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-static {v3, v0}, Lcom/chelpus/Utils;->copyFile(Ljava/io/File;Ljava/io/File;)V

    .line 1522
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_fe

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v3

    sget-object v5, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v5

    cmp-long v3, v3, v5

    if-nez v3, :cond_fe

    .line 1523
    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "chmod"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "644"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v3}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 1524
    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "chown"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "1000."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/chelpus/root/utils/custompatch;->uid:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v3}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 1525
    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "chown"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "1000:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/chelpus/root/utils/custompatch;->uid:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v3}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 1526
    sput-object v0, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 1538
    .end local v0    # "backFile":Ljava/io/File;
    .end local v1    # "backTemp":Ljava/lang/String;
    :cond_fe
    :goto_fe
    return-void

    .line 1529
    :cond_ff
    new-instance v3, Ljava/io/File;

    sget-object v4, Lcom/chelpus/root/utils/custompatch;->dirapp:Ljava/lang/String;

    const/4 v5, 0x1

    invoke-static {v4, v5}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v3, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;
    :try_end_10d
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_10d} :catch_10e

    goto :goto_fe

    .line 1533
    :catch_10e
    move-exception v2

    .line 1534
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 1535
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception e"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    goto :goto_fe
.end method

.method public static searchProcess(Ljava/util/ArrayList;)Z
    .registers 25
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1377
    .local p0, "searchlist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;>;"
    const/16 v19, 0x1

    .line 1378
    .local v19, "patch":Z
    const-string v3, ""

    sput-object v3, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    .line 1379
    if-eqz p0, :cond_22

    invoke-virtual/range {p0 .. p0}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_22

    .line 1380
    invoke-virtual/range {p0 .. p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_12
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_22

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;

    .line 1381
    .local v15, "it":Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;
    const/4 v4, 0x0

    iput-boolean v4, v15, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->result:Z

    goto :goto_12

    .line 1386
    .end local v15    # "it":Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;
    :cond_22
    :try_start_22
    new-instance v3, Ljava/io/RandomAccessFile;

    sget-object v4, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    const-string v5, "rw"

    invoke-direct {v3, v4, v5}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v2

    .line 1387
    .local v2, "ChannelDex2":Ljava/nio/channels/FileChannel;
    sget-object v3, Ljava/nio/channels/FileChannel$MapMode;->READ_WRITE:Ljava/nio/channels/FileChannel$MapMode;

    const-wide/16 v4, 0x0

    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v6

    long-to-int v6, v6

    int-to-long v6, v6

    invoke-virtual/range {v2 .. v7}, Ljava/nio/channels/FileChannel;->map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;

    move-result-object v12

    .line 1389
    .local v12, "fileBytes2":Ljava/nio/MappedByteBuffer;
    invoke-virtual/range {p0 .. p0}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v3

    array-length v3, v3

    new-array v0, v3, [Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;

    move-object/from16 v20, v0

    .line 1391
    .local v20, "patches":[Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;
    invoke-virtual/range {p0 .. p0}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v0, v3, [Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;

    move-object/from16 v20, v0

    .line 1392
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;

    move-object v0, v3

    check-cast v0, [Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;

    move-object/from16 v20, v0
    :try_end_5d
    .catch Ljava/io/FileNotFoundException; {:try_start_22 .. :try_end_5d} :catch_20d
    .catch Ljava/nio/BufferUnderflowException; {:try_start_22 .. :try_end_5d} :catch_22f
    .catch Ljava/lang/Exception; {:try_start_22 .. :try_end_5d} :catch_24b

    .line 1398
    const-wide/16 v16, 0x0

    .local v16, "j":J
    :goto_5f
    :try_start_5f
    invoke-virtual {v12}, Ljava/nio/MappedByteBuffer;->hasRemaining()Z

    move-result v3

    if-eqz v3, :cond_109

    .line 1400
    invoke-virtual {v12}, Ljava/nio/MappedByteBuffer;->position()I

    move-result v10

    .line 1401
    .local v10, "curentPos":I
    invoke-virtual {v12}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v9

    .line 1403
    .local v9, "curentByte":B
    const/4 v13, 0x0

    .local v13, "g":I
    :goto_6e
    move-object/from16 v0, v20

    array-length v3, v0

    if-ge v13, v3, :cond_e7

    .line 1404
    invoke-virtual {v12, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1406
    aget-object v3, v20, v13

    iget-boolean v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->result:Z

    if-nez v3, :cond_df

    aget-object v3, v20, v13

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->origByte:[B

    const/4 v4, 0x0

    aget-byte v3, v3, v4

    if-eq v9, v3, :cond_8e

    aget-object v3, v20, v13

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->origMask:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    if-eqz v3, :cond_df

    .line 1408
    :cond_8e
    aget-object v3, v20, v13

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->origMask:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    if-eqz v3, :cond_9e

    aget-object v3, v20, v13

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->repByte:[B

    const/4 v4, 0x0

    aput-byte v9, v3, v4

    .line 1409
    :cond_9e
    const/4 v14, 0x1

    .line 1410
    .local v14, "i":I
    add-int/lit8 v3, v10, 0x1

    invoke-virtual {v12, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1411
    invoke-virtual {v12}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v21

    .line 1413
    .local v21, "prufbyte":B
    :goto_a8
    aget-object v3, v20, v13

    iget-boolean v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->result:Z

    if-nez v3, :cond_b8

    aget-object v3, v20, v13

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->origByte:[B

    aget-byte v3, v3, v14

    move/from16 v0, v21

    if-eq v0, v3, :cond_c0

    :cond_b8
    aget-object v3, v20, v13

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->origMask:[I

    aget v3, v3, v14

    if-eqz v3, :cond_df

    .line 1415
    :cond_c0
    aget-object v3, v20, v13

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->origMask:[I

    aget v3, v3, v14

    if-lez v3, :cond_ce

    aget-object v3, v20, v13

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->repByte:[B

    aput-byte v21, v3, v14

    .line 1417
    :cond_ce
    add-int/lit8 v14, v14, 0x1

    .line 1419
    aget-object v3, v20, v13

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->origByte:[B

    array-length v3, v3

    if-ne v14, v3, :cond_e2

    .line 1421
    aget-object v3, v20, v13

    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->result:Z

    .line 1422
    const/4 v3, 0x1

    sput-boolean v3, Lcom/chelpus/root/utils/custompatch;->patchteil:Z

    .line 1403
    .end local v14    # "i":I
    .end local v21    # "prufbyte":B
    :cond_df
    add-int/lit8 v13, v13, 0x1

    goto :goto_6e

    .line 1425
    .restart local v14    # "i":I
    .restart local v21    # "prufbyte":B
    :cond_e2
    invoke-virtual {v12}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v21

    goto :goto_a8

    .line 1430
    .end local v14    # "i":I
    .end local v21    # "prufbyte":B
    :cond_e7
    add-int/lit8 v3, v10, 0x1

    invoke-virtual {v12, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;
    :try_end_ec
    .catch Ljava/lang/Exception; {:try_start_5f .. :try_end_ec} :catch_f2
    .catch Ljava/io/FileNotFoundException; {:try_start_5f .. :try_end_ec} :catch_20d
    .catch Ljava/nio/BufferUnderflowException; {:try_start_5f .. :try_end_ec} :catch_22f

    .line 1398
    const-wide/16 v3, 0x1

    add-long v16, v16, v3

    goto/16 :goto_5f

    .line 1433
    .end local v9    # "curentByte":B
    .end local v10    # "curentPos":I
    .end local v13    # "g":I
    :catch_f2
    move-exception v11

    .line 1434
    .local v11, "e":Ljava/lang/Exception;
    :try_start_f3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Search byte error: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 1436
    .end local v11    # "e":Ljava/lang/Exception;
    :cond_109
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V

    .line 1437
    const/4 v13, 0x0

    .restart local v13    # "g":I
    :goto_10d
    move-object/from16 v0, v20

    array-length v3, v0

    if-ge v13, v3, :cond_146

    .line 1439
    aget-object v3, v20, v13

    iget-boolean v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->result:Z

    if-nez v3, :cond_143

    .line 1441
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "Bytes by serach N"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    add-int/lit8 v4, v13, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":\nError LP: Bytes not found!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    .line 1442
    const/16 v19, 0x0

    .line 1437
    :cond_143
    add-int/lit8 v13, v13, 0x1

    goto :goto_10d

    .line 1445
    :cond_146
    const/4 v13, 0x0

    :goto_147
    move-object/from16 v0, v20

    array-length v3, v0

    if-ge v13, v3, :cond_213

    .line 1446
    aget-object v3, v20, v13

    iget-boolean v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->result:Z

    if-eqz v3, :cond_17b

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\nBytes by search N"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    add-int/lit8 v4, v13, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    .line 1448
    :cond_17b
    const/16 v22, 0x0

    .local v22, "w":I
    :goto_17d
    aget-object v3, v20, v13

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->origMask:[I

    array-length v3, v3

    move/from16 v0, v22

    if-ge v0, v3, :cond_214

    .line 1449
    aget-object v3, v20, v13

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->origMask:[I

    aget v3, v3, v22

    const/4 v4, 0x1

    if-le v3, v4, :cond_1f7

    .line 1450
    aget-object v3, v20, v13

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->origMask:[I

    aget v3, v3, v22
    :try_end_195
    .catch Ljava/io/FileNotFoundException; {:try_start_f3 .. :try_end_195} :catch_20d
    .catch Ljava/nio/BufferUnderflowException; {:try_start_f3 .. :try_end_195} :catch_22f
    .catch Ljava/lang/Exception; {:try_start_f3 .. :try_end_195} :catch_24b

    add-int/lit8 v23, v3, -0x2

    .line 1451
    .local v23, "y":I
    :try_start_197
    sget-object v3, Lcom/chelpus/root/utils/custompatch;->search:Ljava/util/ArrayList;

    aget-object v4, v20, v13

    iget-object v4, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->repByte:[B

    aget-byte v4, v4, v22

    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v4

    move/from16 v0, v23

    invoke-virtual {v3, v0, v4}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;
    :try_end_1a8
    .catch Ljava/lang/Exception; {:try_start_197 .. :try_end_1a8} :catch_1fa
    .catch Ljava/io/FileNotFoundException; {:try_start_197 .. :try_end_1a8} :catch_20d
    .catch Ljava/nio/BufferUnderflowException; {:try_start_197 .. :try_end_1a8} :catch_22f

    .line 1452
    :goto_1a8
    :try_start_1a8
    aget-object v3, v20, v13

    iget-boolean v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->result:Z

    if-eqz v3, :cond_1f7

    .line 1453
    const/4 v3, 0x1

    new-array v8, v3, [B

    .line 1454
    .local v8, "bytik":[B
    const/4 v4, 0x0

    sget-object v3, Lcom/chelpus/root/utils/custompatch;->search:Ljava/util/ArrayList;

    move/from16 v0, v23

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Byte;

    invoke-virtual {v3}, Ljava/lang/Byte;->byteValue()B

    move-result v3

    aput-byte v3, v8, v4

    .line 1455
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "R"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v23

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v8}, Lcom/chelpus/Utils;->bytesToHex([B)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    .line 1448
    .end local v8    # "bytik":[B
    .end local v23    # "y":I
    :cond_1f7
    add-int/lit8 v22, v22, 0x1

    goto :goto_17d

    .line 1451
    .restart local v23    # "y":I
    :catch_1fa
    move-exception v11

    .restart local v11    # "e":Ljava/lang/Exception;
    sget-object v3, Lcom/chelpus/root/utils/custompatch;->search:Ljava/util/ArrayList;

    aget-object v4, v20, v13

    iget-object v4, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->repByte:[B

    aget-byte v4, v4, v22

    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v4

    move/from16 v0, v23

    invoke-virtual {v3, v0, v4}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V
    :try_end_20c
    .catch Ljava/io/FileNotFoundException; {:try_start_1a8 .. :try_end_20c} :catch_20d
    .catch Ljava/nio/BufferUnderflowException; {:try_start_1a8 .. :try_end_20c} :catch_22f
    .catch Ljava/lang/Exception; {:try_start_1a8 .. :try_end_20c} :catch_24b

    goto :goto_1a8

    .line 1462
    .end local v2    # "ChannelDex2":Ljava/nio/channels/FileChannel;
    .end local v11    # "e":Ljava/lang/Exception;
    .end local v12    # "fileBytes2":Ljava/nio/MappedByteBuffer;
    .end local v13    # "g":I
    .end local v16    # "j":J
    .end local v20    # "patches":[Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;
    .end local v22    # "w":I
    .end local v23    # "y":I
    :catch_20d
    move-exception v18

    .line 1463
    .local v18, "localFileNotFoundException":Ljava/io/FileNotFoundException;
    const-string v3, "Error LP: Program files are not found!\nMove Program to internal storage."

    invoke-static {v3}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 1469
    .end local v18    # "localFileNotFoundException":Ljava/io/FileNotFoundException;
    :cond_213
    :goto_213
    return v19

    .line 1459
    .restart local v2    # "ChannelDex2":Ljava/nio/channels/FileChannel;
    .restart local v12    # "fileBytes2":Ljava/nio/MappedByteBuffer;
    .restart local v13    # "g":I
    .restart local v16    # "j":J
    .restart local v20    # "patches":[Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;
    .restart local v22    # "w":I
    :cond_214
    :try_start_214
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/chelpus/root/utils/custompatch;->searchStr:Ljava/lang/String;
    :try_end_22b
    .catch Ljava/io/FileNotFoundException; {:try_start_214 .. :try_end_22b} :catch_20d
    .catch Ljava/nio/BufferUnderflowException; {:try_start_214 .. :try_end_22b} :catch_22f
    .catch Ljava/lang/Exception; {:try_start_214 .. :try_end_22b} :catch_24b

    .line 1445
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_147

    .line 1464
    .end local v2    # "ChannelDex2":Ljava/nio/channels/FileChannel;
    .end local v12    # "fileBytes2":Ljava/nio/MappedByteBuffer;
    .end local v13    # "g":I
    .end local v16    # "j":J
    .end local v20    # "patches":[Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;
    .end local v22    # "w":I
    :catch_22f
    move-exception v11

    .line 1465
    .local v11, "e":Ljava/nio/BufferUnderflowException;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception e"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v11}, Ljava/nio/BufferUnderflowException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    goto :goto_213

    .line 1466
    .end local v11    # "e":Ljava/nio/BufferUnderflowException;
    :catch_24b
    move-exception v11

    .line 1467
    .local v11, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception e"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v11}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    goto :goto_213
.end method

.method public static searchfile(Ljava/lang/String;Ljava/lang/String;)V
    .registers 13
    .param p0, "param"    # Ljava/lang/String;
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 1688
    :try_start_0
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_1b

    const-string v7, "/mnt/sdcard"

    invoke-virtual {p1, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_1b

    new-instance v7, Ljava/io/File;

    invoke-direct {v7, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v7, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 1756
    :cond_1a
    :goto_1a
    return-void

    .line 1690
    :cond_1b
    const-string v7, "/mnt/sdcard"

    invoke-virtual {p1, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_2b

    const-string v7, "/sdcard"

    invoke-virtual {p1, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_130

    .line 1691
    :cond_2b
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1692
    .local v5, "replacers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v7, "/mnt/sdcard/"

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1693
    const-string v7, "/storage/emulated/legacy/"

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1694
    const-string v7, "/storage/emulated/0/"

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1695
    const-string v7, "/storage/sdcard0/"

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1696
    const-string v7, "/storage/sdcard/"

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1697
    const-string v7, "/storage/sdcard1/"

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1698
    const-string v7, "/sdcard/"

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1699
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_57
    :goto_57
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_cc

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 1700
    .local v4, "replacer":Ljava/lang/String;
    new-instance v8, Ljava/io/File;

    const-string v9, "/mnt/sdcard/"

    invoke-virtual {p1, v9, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v8, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 1701
    sget-object v8, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual {v8}, Ljava/io/File;->exists()Z
    :try_end_75
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_75} :catch_da
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_75} :catch_113

    move-result v8

    if-eqz v8, :cond_57

    .line 1704
    :try_start_78
    new-instance v8, Ljava/io/File;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "test.tmp"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/io/File;->createNewFile()Z

    .line 1705
    new-instance v8, Ljava/io/File;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "test.tmp"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_f9

    .line 1706
    new-instance v8, Ljava/io/File;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "test.tmp"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/io/File;->delete()Z
    :try_end_cc
    .catch Ljava/lang/Exception; {:try_start_78 .. :try_end_cc} :catch_104
    .catch Ljava/io/FileNotFoundException; {:try_start_78 .. :try_end_cc} :catch_da

    .line 1715
    .end local v4    # "replacer":Ljava/lang/String;
    :cond_cc
    :try_start_cc
    sget-object v7, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_1a

    .line 1716
    new-instance v7, Ljava/io/FileNotFoundException;

    invoke-direct {v7}, Ljava/io/FileNotFoundException;-><init>()V

    throw v7
    :try_end_da
    .catch Ljava/io/FileNotFoundException; {:try_start_cc .. :try_end_da} :catch_da
    .catch Ljava/lang/Exception; {:try_start_cc .. :try_end_da} :catch_113

    .line 1749
    .end local v5    # "replacers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :catch_da
    move-exception v1

    .line 1751
    .local v1, "localFileNotFoundException":Ljava/io/FileNotFoundException;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Error LP: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " are not found!\n\nRun the application file to appear in the folder with the data.!\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    goto/16 :goto_1a

    .line 1708
    .end local v1    # "localFileNotFoundException":Ljava/io/FileNotFoundException;
    .restart local v4    # "replacer":Ljava/lang/String;
    .restart local v5    # "replacers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_f9
    :try_start_f9
    new-instance v8, Ljava/io/File;

    const-string v9, "/figjvaja_papka"

    invoke-direct {v8, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v8, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;
    :try_end_102
    .catch Ljava/lang/Exception; {:try_start_f9 .. :try_end_102} :catch_104
    .catch Ljava/io/FileNotFoundException; {:try_start_f9 .. :try_end_102} :catch_da

    goto/16 :goto_57

    .line 1709
    :catch_104
    move-exception v0

    .line 1710
    .local v0, "e":Ljava/lang/Exception;
    :try_start_105
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1711
    new-instance v8, Ljava/io/File;

    const-string v9, "/figjvaja_papka"

    invoke-direct {v8, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v8, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;
    :try_end_111
    .catch Ljava/io/FileNotFoundException; {:try_start_105 .. :try_end_111} :catch_da
    .catch Ljava/lang/Exception; {:try_start_105 .. :try_end_111} :catch_113

    goto/16 :goto_57

    .line 1753
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v4    # "replacer":Ljava/lang/String;
    .end local v5    # "replacers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :catch_113
    move-exception v0

    .line 1754
    .restart local v0    # "e":Ljava/lang/Exception;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Exception e"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    goto/16 :goto_1a

    .line 1718
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_130
    :try_start_130
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "/data/data/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/chelpus/root/utils/custompatch;->pkgName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/shared_prefs/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_17a

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "/dbdata/databases/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/chelpus/root/utils/custompatch;->pkgName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/shared_prefs/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_17a

    const-string v7, "/shared_prefs/"

    invoke-virtual {p1, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_260

    .line 1719
    :cond_17a
    const-string v3, ""

    .line 1720
    .local v3, "rep_str":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "/data/data/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/chelpus/root/utils/custompatch;->pkgName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/shared_prefs/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1b8

    .line 1721
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "/data/data/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/chelpus/root/utils/custompatch;->pkgName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/shared_prefs/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1722
    :cond_1b8
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "/dbdata/databases/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/chelpus/root/utils/custompatch;->pkgName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/shared_prefs/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1f4

    .line 1723
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "/dbdata/databases/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/chelpus/root/utils/custompatch;->pkgName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/shared_prefs/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1724
    :cond_1f4
    const-string v7, "/shared_prefs/"

    invoke-virtual {p1, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1fe

    const-string v3, "/shared_prefs/"

    .line 1725
    :cond_1fe
    new-instance v7, Ljava/io/File;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "/data/data/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Lcom/chelpus/root/utils/custompatch;->pkgName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/shared_prefs/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v3, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v7, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 1726
    sget-object v7, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_252

    .line 1727
    new-instance v7, Ljava/io/File;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "/dbdata/databases/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Lcom/chelpus/root/utils/custompatch;->pkgName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/shared_prefs/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v3, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v7, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 1728
    :cond_252
    sget-object v7, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_1a

    .line 1729
    new-instance v7, Ljava/io/FileNotFoundException;

    invoke-direct {v7}, Ljava/io/FileNotFoundException;-><init>()V

    throw v7

    .line 1731
    .end local v3    # "rep_str":Ljava/lang/String;
    :cond_260
    new-instance v7, Ljava/io/File;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "/data/data/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v7, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 1732
    sget-object v7, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_2aa

    new-instance v7, Ljava/io/File;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "/mnt/asec/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "-1"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v7, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 1733
    :cond_2aa
    sget-object v7, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_2d6

    new-instance v7, Ljava/io/File;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "/mnt/asec/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "-2"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v7, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 1734
    :cond_2d6
    sget-object v7, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_2fc

    new-instance v7, Ljava/io/File;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "/mnt/asec/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v7, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 1735
    :cond_2fc
    sget-object v7, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_322

    new-instance v7, Ljava/io/File;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "/sd-ext/data/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v7, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 1736
    :cond_322
    sget-object v7, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_348

    new-instance v7, Ljava/io/File;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "/data/sdext2/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v7, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 1737
    :cond_348
    sget-object v7, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_36e

    new-instance v7, Ljava/io/File;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "/data/sdext1/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v7, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 1738
    :cond_36e
    sget-object v7, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_394

    new-instance v7, Ljava/io/File;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "/data/sdext/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v7, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 1739
    :cond_394
    sget-object v7, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_3ce

    .line 1740
    new-instance v6, Lcom/chelpus/Utils;

    const-string v7, "fgh"

    invoke-direct {v6, v7}, Lcom/chelpus/Utils;-><init>(Ljava/lang/String;)V

    .line 1741
    .local v6, "ut":Lcom/chelpus/Utils;
    new-instance v7, Ljava/io/File;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "/data/data/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v7, p1}, Lcom/chelpus/Utils;->findFile(Ljava/io/File;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1742
    .local v2, "re":Ljava/lang/String;
    const-string v7, ""

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_3ce

    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v7, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 1744
    .end local v2    # "re":Ljava/lang/String;
    .end local v6    # "ut":Lcom/chelpus/Utils;
    :cond_3ce
    sget-object v7, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_1a

    new-instance v7, Ljava/io/FileNotFoundException;

    invoke-direct {v7}, Ljava/io/FileNotFoundException;-><init>()V

    throw v7
    :try_end_3dc
    .catch Ljava/io/FileNotFoundException; {:try_start_130 .. :try_end_3dc} :catch_da
    .catch Ljava/lang/Exception; {:try_start_130 .. :try_end_3dc} :catch_113
.end method

.method public static searchlib(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;
    .registers 27
    .param p0, "pkgName"    # Ljava/lang/String;
    .param p1, "libname"    # Ljava/lang/String;
    .param p2, "apk_file"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1541
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 1544
    .local v13, "libs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    :try_start_5
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1545
    .local v6, "dataDirs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v18, "/data/data/"

    move-object/from16 v0, v18

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1546
    const-string v18, "/mnt/asec/"

    move-object/from16 v0, v18

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1547
    const-string v18, "/sd-ext/data/"

    move-object/from16 v0, v18

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1548
    const-string v18, "/data/sdext2/"

    move-object/from16 v0, v18

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1549
    const-string v18, "/data/sdext1/"

    move-object/from16 v0, v18

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1550
    const-string v18, "/data/sdext/"

    move-object/from16 v0, v18

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1552
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 1553
    .local v16, "pakDirs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1554
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "-1"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1555
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "-2"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1557
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v18

    const-string v19, "*"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_2c3

    .line 1558
    const/16 v18, 0x1

    sput-boolean v18, Lcom/chelpus/root/utils/custompatch;->multilib_patch:Z

    .line 1559
    new-instance v18, Ljava/io/File;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "/data/data/"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "/lib/"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v11

    .line 1560
    .local v11, "getFiles":[Ljava/io/File;
    if-eqz v11, :cond_e2

    array-length v0, v11

    move/from16 v18, v0

    if-lez v18, :cond_e2

    .line 1561
    array-length v0, v11

    move/from16 v19, v0

    const/16 v18, 0x0

    :goto_be
    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_e2

    aget-object v9, v11, v18

    .line 1562
    .local v9, "file":Ljava/io/File;
    invoke-virtual {v9}, Ljava/io/File;->length()J

    move-result-wide v20

    const-wide/16 v22, 0x0

    cmp-long v20, v20, v22

    if-lez v20, :cond_df

    invoke-virtual {v9}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v20

    const-string v21, ".so"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_df

    invoke-static {v9, v13}, Lcom/chelpus/Utils;->addFileToList(Ljava/io/File;Ljava/util/ArrayList;)V

    .line 1561
    :cond_df
    add-int/lit8 v18, v18, 0x1

    goto :goto_be

    .line 1565
    .end local v9    # "file":Ljava/io/File;
    :cond_e2
    const-string v12, ""

    .line 1566
    .local v12, "index":Ljava/lang/String;
    new-instance v18, Ljava/io/File;

    move-object/from16 v0, v18

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v18

    const-string v19, ""

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v18

    const-string v19, ".apk"

    const-string v20, ""

    invoke-virtual/range {v18 .. v20}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v12

    .line 1567
    new-instance v18, Ljava/io/File;

    const-string v19, "/data/app-lib"

    invoke-direct/range {v18 .. v19}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->exists()Z

    move-result v18

    if-eqz v18, :cond_16e

    .line 1568
    new-instance v18, Ljava/io/File;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "/data/app-lib/"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "/"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v11

    .line 1569
    if-eqz v11, :cond_16e

    array-length v0, v11

    move/from16 v18, v0

    if-lez v18, :cond_16e

    .line 1570
    array-length v0, v11

    move/from16 v19, v0

    const/16 v18, 0x0

    :goto_14a
    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_16e

    aget-object v9, v11, v18

    .line 1571
    .restart local v9    # "file":Ljava/io/File;
    invoke-virtual {v9}, Ljava/io/File;->length()J

    move-result-wide v20

    const-wide/16 v22, 0x0

    cmp-long v20, v20, v22

    if-lez v20, :cond_16b

    invoke-virtual {v9}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v20

    const-string v21, ".so"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_16b

    invoke-static {v9, v13}, Lcom/chelpus/Utils;->addFileToList(Ljava/io/File;Ljava/util/ArrayList;)V

    .line 1570
    :cond_16b
    add-int/lit8 v18, v18, 0x1

    goto :goto_14a

    .line 1575
    .end local v9    # "file":Ljava/io/File;
    :cond_16e
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :cond_172
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_224

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 1577
    .local v7, "dir":Ljava/lang/String;
    new-instance v4, Ljava/io/File;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v19

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "/"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1578
    .local v4, "d":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v19

    if-eqz v19, :cond_225

    .line 1579
    new-instance v3, Lcom/chelpus/Utils;

    const-string v19, "sdf"

    move-object/from16 v0, v19

    invoke-direct {v3, v0}, Lcom/chelpus/Utils;-><init>(Ljava/lang/String;)V

    .line 1580
    .local v3, "ad":Lcom/chelpus/Utils;
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 1581
    .local v10, "foundFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    const-string v19, ".so"

    move-object/from16 v0, v19

    invoke-virtual {v3, v4, v0, v10}, Lcom/chelpus/Utils;->findFileEndText(Ljava/io/File;Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v17

    .line 1582
    .local v17, "result":Ljava/lang/String;
    const-string v19, ""

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_225

    .line 1583
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v19

    if-lez v19, :cond_225

    .line 1584
    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :goto_1da
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_225

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/io/File;

    .line 1585
    .restart local v9    # "file":Ljava/io/File;
    invoke-static {v9, v13}, Lcom/chelpus/Utils;->addFileToList(Ljava/io/File;Ljava/util/ArrayList;)V

    .line 1586
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Found lib:"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V
    :try_end_203
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_203} :catch_204
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_203} :catch_2a5

    goto :goto_1da

    .line 1675
    .end local v3    # "ad":Lcom/chelpus/Utils;
    .end local v4    # "d":Ljava/io/File;
    .end local v6    # "dataDirs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v7    # "dir":Ljava/lang/String;
    .end local v9    # "file":Ljava/io/File;
    .end local v10    # "foundFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    .end local v11    # "getFiles":[Ljava/io/File;
    .end local v12    # "index":Ljava/lang/String;
    .end local v16    # "pakDirs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v17    # "result":Ljava/lang/String;
    :catch_204
    move-exception v14

    .line 1676
    .local v14, "localFileNotFoundException":Ljava/io/FileNotFoundException;
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Error LP: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    sget-object v19, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " are not found!\n\nCheck the location libraries to solve problems!\n"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 1677
    const/4 v13, 0x0

    .line 1683
    .end local v13    # "libs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    .end local v14    # "localFileNotFoundException":Ljava/io/FileNotFoundException;
    :cond_224
    :goto_224
    return-object v13

    .line 1591
    .restart local v4    # "d":Ljava/io/File;
    .restart local v6    # "dataDirs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v7    # "dir":Ljava/lang/String;
    .restart local v11    # "getFiles":[Ljava/io/File;
    .restart local v12    # "index":Ljava/lang/String;
    .restart local v13    # "libs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    .restart local v16    # "pakDirs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_225
    :try_start_225
    new-instance v5, Ljava/io/File;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v19

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "/"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-direct {v5, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1592
    .local v5, "d1":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v19

    if-eqz v19, :cond_172

    .line 1593
    new-instance v3, Lcom/chelpus/Utils;

    const-string v19, "sdf"

    move-object/from16 v0, v19

    invoke-direct {v3, v0}, Lcom/chelpus/Utils;-><init>(Ljava/lang/String;)V

    .line 1594
    .restart local v3    # "ad":Lcom/chelpus/Utils;
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 1595
    .restart local v10    # "foundFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    const-string v19, ".so"

    move-object/from16 v0, v19

    invoke-virtual {v3, v5, v0, v10}, Lcom/chelpus/Utils;->findFileEndText(Ljava/io/File;Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v17

    .line 1596
    .restart local v17    # "result":Ljava/lang/String;
    const-string v19, ""

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_172

    .line 1597
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v19

    if-lez v19, :cond_172

    .line 1598
    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :goto_27b
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_172

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/io/File;

    .line 1599
    .restart local v9    # "file":Ljava/io/File;
    invoke-static {v9, v13}, Lcom/chelpus/Utils;->addFileToList(Ljava/io/File;Ljava/util/ArrayList;)V

    .line 1600
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Found lib:"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V
    :try_end_2a4
    .catch Ljava/io/FileNotFoundException; {:try_start_225 .. :try_end_2a4} :catch_204
    .catch Ljava/lang/Exception; {:try_start_225 .. :try_end_2a4} :catch_2a5

    goto :goto_27b

    .line 1679
    .end local v3    # "ad":Lcom/chelpus/Utils;
    .end local v4    # "d":Ljava/io/File;
    .end local v5    # "d1":Ljava/io/File;
    .end local v6    # "dataDirs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v7    # "dir":Ljava/lang/String;
    .end local v9    # "file":Ljava/io/File;
    .end local v10    # "foundFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    .end local v11    # "getFiles":[Ljava/io/File;
    .end local v12    # "index":Ljava/lang/String;
    .end local v16    # "pakDirs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v17    # "result":Ljava/lang/String;
    :catch_2a5
    move-exception v8

    .line 1680
    .local v8, "e":Ljava/lang/Exception;
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Exception e"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v8}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 1681
    const/4 v13, 0x0

    goto/16 :goto_224

    .line 1609
    .end local v8    # "e":Ljava/lang/Exception;
    .restart local v6    # "dataDirs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v16    # "pakDirs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_2c3
    :try_start_2c3
    new-instance v18, Ljava/io/File;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "/data/data/"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "/lib/"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v18, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 1612
    sget-object v18, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->exists()Z

    move-result v18

    if-nez v18, :cond_381

    .line 1613
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :cond_2fb
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_388

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 1614
    .restart local v7    # "dir":Ljava/lang/String;
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :cond_30b
    :goto_30b
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_2fb

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    .line 1615
    .local v15, "pak":Ljava/lang/String;
    new-instance v4, Ljava/io/File;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1616
    .restart local v4    # "d":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v20

    if-eqz v20, :cond_30b

    .line 1617
    new-instance v3, Lcom/chelpus/Utils;

    const-string v20, "sdf"

    move-object/from16 v0, v20

    invoke-direct {v3, v0}, Lcom/chelpus/Utils;-><init>(Ljava/lang/String;)V

    .line 1618
    .restart local v3    # "ad":Lcom/chelpus/Utils;
    move-object/from16 v0, p1

    invoke-virtual {v3, v4, v0}, Lcom/chelpus/Utils;->findFile(Ljava/io/File;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 1619
    .restart local v17    # "result":Ljava/lang/String;
    const-string v20, ""

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-nez v20, :cond_30b

    .line 1620
    new-instance v20, Ljava/io/File;

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v20, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 1622
    sget-object v20, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    move-object/from16 v0, v20

    invoke-static {v0, v13}, Lcom/chelpus/Utils;->addFileToList(Ljava/io/File;Ljava/util/ArrayList;)V

    .line 1624
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Found lib:"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    goto :goto_30b

    .line 1631
    .end local v3    # "ad":Lcom/chelpus/Utils;
    .end local v4    # "d":Ljava/io/File;
    .end local v7    # "dir":Ljava/lang/String;
    .end local v15    # "pak":Ljava/lang/String;
    .end local v17    # "result":Ljava/lang/String;
    :cond_381
    sget-object v18, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    move-object/from16 v0, v18

    invoke-static {v0, v13}, Lcom/chelpus/Utils;->addFileToList(Ljava/io/File;Ljava/util/ArrayList;)V

    .line 1637
    :cond_388
    new-instance v18, Ljava/io/File;

    move-object/from16 v0, v18

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static/range {v18 .. v18}, Lcom/chelpus/Utils;->getDirs(Ljava/io/File;)Ljava/io/File;

    move-result-object v7

    .line 1638
    .local v7, "dir":Ljava/io/File;
    new-instance v18, Ljava/io/File;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "/lib"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->exists()Z

    move-result v18

    if-eqz v18, :cond_456

    .line 1639
    new-instance v18, Ljava/io/File;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "/lib/arm/"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v18, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 1642
    sget-object v18, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->exists()Z

    move-result v18

    if-eqz v18, :cond_3ec

    .line 1644
    sget-object v18, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    move-object/from16 v0, v18

    invoke-static {v0, v13}, Lcom/chelpus/Utils;->addFileToList(Ljava/io/File;Ljava/util/ArrayList;)V

    .line 1647
    :cond_3ec
    new-instance v18, Ljava/io/File;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "/lib/x86/"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v18, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 1648
    sget-object v18, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->exists()Z

    move-result v18

    if-eqz v18, :cond_421

    .line 1650
    sget-object v18, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    move-object/from16 v0, v18

    invoke-static {v0, v13}, Lcom/chelpus/Utils;->addFileToList(Ljava/io/File;Ljava/util/ArrayList;)V

    .line 1653
    :cond_421
    new-instance v18, Ljava/io/File;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "/lib/mips/"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v18, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 1654
    sget-object v18, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->exists()Z

    move-result v18

    if-eqz v18, :cond_456

    .line 1656
    sget-object v18, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    move-object/from16 v0, v18

    invoke-static {v0, v13}, Lcom/chelpus/Utils;->addFileToList(Ljava/io/File;Ljava/util/ArrayList;)V

    .line 1662
    :cond_456
    sget-object v18, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->exists()Z

    move-result v18

    if-nez v18, :cond_47c

    new-instance v18, Ljava/io/File;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "/system/lib/"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v18, Lcom/chelpus/root/utils/custompatch;->localFile2:Ljava/io/File;

    .line 1663
    :cond_47c
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v18

    if-nez v18, :cond_488

    new-instance v18, Ljava/io/FileNotFoundException;

    invoke-direct/range {v18 .. v18}, Ljava/io/FileNotFoundException;-><init>()V

    throw v18

    .line 1665
    :cond_488
    const-string v12, ""

    .line 1666
    .restart local v12    # "index":Ljava/lang/String;
    new-instance v18, Ljava/io/File;

    move-object/from16 v0, v18

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v18

    const-string v19, ""

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v18

    const-string v19, ".apk"

    const-string v20, ""

    invoke-virtual/range {v18 .. v20}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v12

    .line 1667
    new-instance v18, Ljava/io/File;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "/data/app-lib/"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "/"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->exists()Z

    move-result v18

    if-eqz v18, :cond_224

    .line 1669
    new-instance v18, Ljava/io/File;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "/data/app-lib/"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "/"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-static {v0, v13}, Lcom/chelpus/Utils;->addFileToList(Ljava/io/File;Ljava/util/ArrayList;)V
    :try_end_516
    .catch Ljava/io/FileNotFoundException; {:try_start_2c3 .. :try_end_516} :catch_204
    .catch Ljava/lang/Exception; {:try_start_2c3 .. :try_end_516} :catch_2a5

    goto/16 :goto_224
.end method

.method public static unzip(Ljava/io/File;)V
    .registers 23
    .param p0, "apk"    # Ljava/io/File;

    .prologue
    .line 1759
    sget-object v18, Lcom/chelpus/root/utils/custompatch;->classesFiles:Ljava/util/ArrayList;

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->clear()V

    .line 1760
    const/4 v7, 0x0

    .local v7, "found1":Z
    const/4 v8, 0x0

    .line 1762
    .local v8, "found2":Z
    :try_start_7
    new-instance v6, Ljava/io/FileInputStream;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 1763
    .local v6, "fin":Ljava/io/FileInputStream;
    new-instance v16, Ljava/util/zip/ZipInputStream;

    move-object/from16 v0, v16

    invoke-direct {v0, v6}, Ljava/util/zip/ZipInputStream;-><init>(Ljava/io/InputStream;)V

    .line 1764
    .local v16, "zin":Ljava/util/zip/ZipInputStream;
    const/4 v15, 0x0

    .line 1766
    .local v15, "ze":Ljava/util/zip/ZipEntry;
    invoke-virtual/range {v16 .. v16}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    move-result-object v15

    .line 1767
    const/4 v14, 0x1

    .line 1768
    .local v14, "search":Z
    :goto_1b
    if-eqz v15, :cond_22f

    if-eqz v14, :cond_22f

    .line 1774
    invoke-virtual {v15}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v11

    .line 1776
    .local v11, "haystack":Ljava/lang/String;
    invoke-virtual {v11}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v18

    const-string v19, "classes"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_1a7

    const-string v18, ".dex"

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_1a7

    const-string v18, "/"

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_1a7

    .line 1778
    new-instance v9, Ljava/io/FileOutputStream;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v19, Lcom/chelpus/root/utils/custompatch;->dir:Ljava/lang/String;

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "/"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-direct {v9, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 1780
    .local v9, "fout":Ljava/io/FileOutputStream;
    const/16 v18, 0x800

    move/from16 v0, v18

    new-array v2, v0, [B

    .line 1783
    .local v2, "buffer":[B
    :goto_6b
    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/util/zip/ZipInputStream;->read([B)I

    move-result v12

    .local v12, "length":I
    const/16 v18, -0x1

    move/from16 v0, v18

    if-eq v12, v0, :cond_146

    .line 1784
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v9, v2, v0, v12}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_7e
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7e} :catch_7f

    goto :goto_6b

    .line 1816
    .end local v2    # "buffer":[B
    .end local v6    # "fin":Ljava/io/FileInputStream;
    .end local v9    # "fout":Ljava/io/FileOutputStream;
    .end local v11    # "haystack":Ljava/lang/String;
    .end local v12    # "length":I
    .end local v14    # "search":Z
    .end local v15    # "ze":Ljava/util/zip/ZipEntry;
    .end local v16    # "zin":Ljava/util/zip/ZipInputStream;
    :catch_7f
    move-exception v4

    .line 1817
    .local v4, "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    .line 1819
    :try_start_83
    new-instance v17, Lnet/lingala/zip4j/core/ZipFile;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lnet/lingala/zip4j/core/ZipFile;-><init>(Ljava/io/File;)V

    .line 1823
    .local v17, "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    const-string v18, "classes.dex"

    sget-object v19, Lcom/chelpus/root/utils/custompatch;->dir:Ljava/lang/String;

    invoke-virtual/range {v17 .. v19}, Lnet/lingala/zip4j/core/ZipFile;->extractFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 1824
    const/16 v18, 0x3

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const-string v20, "chmod"

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, "777"

    aput-object v20, v18, v19

    const/16 v19, 0x2

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v21, Lcom/chelpus/root/utils/custompatch;->dir:Ljava/lang/String;

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "classes.dex"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    aput-object v20, v18, v19

    invoke-static/range {v18 .. v18}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 1825
    sget-object v18, Lcom/chelpus/root/utils/custompatch;->classesFiles:Ljava/util/ArrayList;

    new-instance v19, Ljava/io/File;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v21, Lcom/chelpus/root/utils/custompatch;->dir:Ljava/lang/String;

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "classes.dex"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v18 .. v19}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1826
    const-string v18, "AndroidManifest.xml"

    sget-object v19, Lcom/chelpus/root/utils/custompatch;->dir:Ljava/lang/String;

    invoke-virtual/range {v17 .. v19}, Lnet/lingala/zip4j/core/ZipFile;->extractFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 1827
    const/16 v18, 0x3

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const-string v20, "chmod"

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, "777"

    aput-object v20, v18, v19

    const/16 v19, 0x2

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v21, Lcom/chelpus/root/utils/custompatch;->dir:Ljava/lang/String;

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "AndroidManifest.xml"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    aput-object v20, v18, v19

    invoke-static/range {v18 .. v18}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V
    :try_end_12b
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_83 .. :try_end_12b} :catch_23d
    .catch Ljava/lang/Exception; {:try_start_83 .. :try_end_12b} :catch_275

    .line 1838
    .end local v17    # "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    :goto_12b
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Exception e"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v4}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 1842
    .end local v4    # "e":Ljava/lang/Exception;
    :goto_145
    return-void

    .line 1788
    .restart local v2    # "buffer":[B
    .restart local v6    # "fin":Ljava/io/FileInputStream;
    .restart local v9    # "fout":Ljava/io/FileOutputStream;
    .restart local v11    # "haystack":Ljava/lang/String;
    .restart local v12    # "length":I
    .restart local v14    # "search":Z
    .restart local v15    # "ze":Ljava/util/zip/ZipEntry;
    .restart local v16    # "zin":Ljava/util/zip/ZipInputStream;
    :cond_146
    :try_start_146
    invoke-virtual/range {v16 .. v16}, Ljava/util/zip/ZipInputStream;->closeEntry()V

    .line 1789
    invoke-virtual {v9}, Ljava/io/FileOutputStream;->close()V

    .line 1790
    sget-object v18, Lcom/chelpus/root/utils/custompatch;->classesFiles:Ljava/util/ArrayList;

    new-instance v19, Ljava/io/File;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v21, Lcom/chelpus/root/utils/custompatch;->dir:Ljava/lang/String;

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v18 .. v19}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1791
    const/16 v18, 0x3

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const-string v20, "chmod"

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, "777"

    aput-object v20, v18, v19

    const/16 v19, 0x2

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v21, Lcom/chelpus/root/utils/custompatch;->dir:Ljava/lang/String;

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    aput-object v20, v18, v19

    invoke-static/range {v18 .. v18}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 1794
    .end local v2    # "buffer":[B
    .end local v9    # "fout":Ljava/io/FileOutputStream;
    .end local v12    # "length":I
    :cond_1a7
    const-string v18, "AndroidManifest.xml"

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_22a

    .line 1795
    new-instance v10, Ljava/io/FileOutputStream;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v19, Lcom/chelpus/root/utils/custompatch;->dir:Ljava/lang/String;

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "/"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "AndroidManifest.xml"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-direct {v10, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 1797
    .local v10, "fout2":Ljava/io/FileOutputStream;
    const/16 v18, 0x800

    move/from16 v0, v18

    new-array v3, v0, [B

    .line 1799
    .local v3, "buffer2":[B
    :goto_1d9
    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/util/zip/ZipInputStream;->read([B)I

    move-result v13

    .local v13, "length2":I
    const/16 v18, -0x1

    move/from16 v0, v18

    if-eq v13, v0, :cond_1ed

    .line 1800
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v10, v3, v0, v13}, Ljava/io/FileOutputStream;->write([BII)V

    goto :goto_1d9

    .line 1802
    :cond_1ed
    invoke-virtual/range {v16 .. v16}, Ljava/util/zip/ZipInputStream;->closeEntry()V

    .line 1803
    invoke-virtual {v10}, Ljava/io/FileOutputStream;->close()V

    .line 1804
    const/16 v18, 0x3

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const-string v20, "chmod"

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, "777"

    aput-object v20, v18, v19

    const/16 v19, 0x2

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v21, Lcom/chelpus/root/utils/custompatch;->dir:Ljava/lang/String;

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "AndroidManifest.xml"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    aput-object v20, v18, v19

    invoke-static/range {v18 .. v18}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 1805
    const/4 v8, 0x1

    .line 1807
    .end local v3    # "buffer2":[B
    .end local v10    # "fout2":Ljava/io/FileOutputStream;
    .end local v13    # "length2":I
    :cond_22a
    if-eqz v7, :cond_237

    if-eqz v8, :cond_237

    .line 1808
    const/4 v14, 0x0

    .line 1814
    .end local v11    # "haystack":Ljava/lang/String;
    :cond_22f
    invoke-virtual/range {v16 .. v16}, Ljava/util/zip/ZipInputStream;->close()V

    .line 1815
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V

    goto/16 :goto_145

    .line 1811
    .restart local v11    # "haystack":Ljava/lang/String;
    :cond_237
    invoke-virtual/range {v16 .. v16}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;
    :try_end_23a
    .catch Ljava/lang/Exception; {:try_start_146 .. :try_end_23a} :catch_7f

    move-result-object v15

    .line 1813
    goto/16 :goto_1b

    .line 1829
    .end local v6    # "fin":Ljava/io/FileInputStream;
    .end local v11    # "haystack":Ljava/lang/String;
    .end local v14    # "search":Z
    .end local v15    # "ze":Ljava/util/zip/ZipEntry;
    .end local v16    # "zin":Ljava/util/zip/ZipInputStream;
    .restart local v4    # "e":Ljava/lang/Exception;
    :catch_23d
    move-exception v5

    .line 1830
    .local v5, "e1":Lnet/lingala/zip4j/exception/ZipException;
    invoke-virtual {v5}, Lnet/lingala/zip4j/exception/ZipException;->printStackTrace()V

    .line 1831
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Error LP: Error classes.dex decompress! "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 1832
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Exception e1"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v4}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    goto/16 :goto_12b

    .line 1833
    .end local v5    # "e1":Lnet/lingala/zip4j/exception/ZipException;
    :catch_275
    move-exception v5

    .line 1834
    .local v5, "e1":Ljava/lang/Exception;
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V

    .line 1835
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Error LP: Error classes.dex decompress! "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    .line 1836
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Exception e1"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v4}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/chelpus/root/utils/custompatch;->addToLog(Ljava/lang/String;)V

    goto/16 :goto_12b
.end method
