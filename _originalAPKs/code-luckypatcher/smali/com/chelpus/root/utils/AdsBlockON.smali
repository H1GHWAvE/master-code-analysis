.class public Lcom/chelpus/root/utils/AdsBlockON;
.super Ljava/lang/Object;
.source "AdsBlockON.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .registers 26
    .param p0, "params"    # [Ljava/lang/String;

    .prologue
    .line 12
    new-instance v21, Lcom/chelpus/root/utils/AdsBlockON$1;

    invoke-direct/range {v21 .. v21}, Lcom/chelpus/root/utils/AdsBlockON$1;-><init>()V

    invoke-static/range {v21 .. v21}, Lcom/chelpus/Utils;->startRootJava(Ljava/lang/Object;)V

    .line 13
    const-string v14, "#Lucky Patcher block Ads start#"

    .line 14
    .local v14, "starttag":Ljava/lang/String;
    const-string v7, "#Lucky Patcher block Ads finish#"

    .line 16
    .local v7, "endtag":Ljava/lang/String;
    const/16 v21, 0x0

    aget-object v8, p0, v21

    .line 17
    .local v8, "hosts":Ljava/lang/String;
    const/16 v21, 0x1

    aget-object v16, p0, v21

    .line 18
    .local v16, "tmp_hosts":Ljava/lang/String;
    const/16 v21, 0x2

    aget-object v13, p0, v21

    .line 19
    .local v13, "raw":Ljava/lang/String;
    const/16 v21, 0x3

    aget-object v17, p0, v21

    .line 20
    .local v17, "toolfilesdir":Ljava/lang/String;
    const-string v4, "/data/data/hosts"

    .line 21
    .local v4, "data_hosts":Ljava/lang/String;
    const/4 v9, 0x0

    .line 22
    .local v9, "hostsToData":Z
    invoke-static {v8}, Lcom/chelpus/Utils;->getSimulink(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 23
    .local v15, "sym_path":Ljava/lang/String;
    const-string v21, ""

    move-object/from16 v0, v21

    invoke-virtual {v15, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-nez v21, :cond_2e

    move-object v4, v15

    .line 25
    :cond_2e
    :try_start_2e
    const-string v21, "/system"

    const-string v22, "rw"

    invoke-static/range {v21 .. v22}, Lcom/chelpus/Utils;->remount(Ljava/lang/String;Ljava/lang/String;)Z

    .line 26
    const/16 v21, 0x3

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "chmod"

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const-string v23, "777"

    aput-object v23, v21, v22

    const/16 v22, 0x2

    const-string v23, "/system/etc/hosts"

    aput-object v23, v21, v22

    invoke-static/range {v21 .. v21}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 27
    const/16 v21, 0x3

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "chattr"

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const-string v23, "-ai"

    aput-object v23, v21, v22

    const/16 v22, 0x2

    const-string v23, "/system/etc/hosts"

    aput-object v23, v21, v22

    invoke-static/range {v21 .. v21}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 28
    const/4 v11, 0x0

    .line 29
    .local v11, "ram":Ljava/io/RandomAccessFile;
    const/16 v18, 0x0

    .line 30
    .local v18, "wr":Ljava/io/RandomAccessFile;
    new-instance v21, Ljava/io/File;

    move-object/from16 v0, v21

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->exists()Z

    move-result v21

    if-eqz v21, :cond_db

    new-instance v21, Ljava/io/File;

    move-object/from16 v0, v21

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->length()J

    move-result-wide v21

    new-instance v23, Ljava/io/File;

    move-object/from16 v0, v23

    invoke-direct {v0, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v23 .. v23}, Ljava/io/File;->length()J

    move-result-wide v23

    cmp-long v21, v21, v23

    if-nez v21, :cond_db

    .line 31
    const/4 v9, 0x1

    .line 32
    sget-object v21, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v22, "Hosts to Data"

    invoke-virtual/range {v21 .. v22}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 33
    const/16 v21, 0x3

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "chmod"

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const-string v23, "777"

    aput-object v23, v21, v22

    const/16 v22, 0x2

    const-string v23, "/data/data/hosts"

    aput-object v23, v21, v22

    invoke-static/range {v21 .. v21}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 34
    const/16 v21, 0x3

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "chattr"

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const-string v23, "-ai"

    aput-object v23, v21, v22

    const/16 v22, 0x2

    const-string v23, "/data/data/hosts"

    aput-object v23, v21, v22

    invoke-static/range {v21 .. v21}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V
    :try_end_db
    .catch Ljava/lang/Exception; {:try_start_2e .. :try_end_db} :catch_2c9
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2e .. :try_end_db} :catch_3d1

    .line 37
    :cond_db
    :try_start_db
    new-instance v21, Ljava/io/File;

    move-object/from16 v0, v21

    invoke-direct {v0, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->exists()Z

    move-result v21

    if-nez v21, :cond_f4

    .line 38
    new-instance v21, Ljava/io/File;

    const-string v22, "/system/etc/hosts"

    invoke-direct/range {v21 .. v22}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const-string v22, "127.0.0.1       localhost\n"

    invoke-static/range {v21 .. v22}, Lcom/chelpus/Utils;->save_text_to_file(Ljava/io/File;Ljava/lang/String;)Z

    .line 40
    :cond_f4
    if-nez v9, :cond_15b

    new-instance v12, Ljava/io/RandomAccessFile;

    const-string v21, "r"

    move-object/from16 v0, v21

    invoke-direct {v12, v8, v0}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .end local v11    # "ram":Ljava/io/RandomAccessFile;
    .local v12, "ram":Ljava/io/RandomAccessFile;
    move-object v11, v12

    .line 42
    .end local v12    # "ram":Ljava/io/RandomAccessFile;
    .restart local v11    # "ram":Ljava/io/RandomAccessFile;
    :goto_100
    const-wide/16 v21, 0x0

    move-wide/from16 v0, v21

    invoke-virtual {v11, v0, v1}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 43
    new-instance v19, Ljava/io/RandomAccessFile;

    const-string v21, "rw"

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_114
    .catch Ljava/lang/Exception; {:try_start_db .. :try_end_114} :catch_28d
    .catch Ljava/lang/OutOfMemoryError; {:try_start_db .. :try_end_114} :catch_3d1

    .line 44
    .end local v18    # "wr":Ljava/io/RandomAccessFile;
    .local v19, "wr":Ljava/io/RandomAccessFile;
    const-wide/16 v21, 0x0

    :try_start_116
    move-object/from16 v0, v19

    move-wide/from16 v1, v21

    invoke-virtual {v0, v1, v2}, Ljava/io/RandomAccessFile;->setLength(J)V

    .line 45
    const-wide/16 v21, 0x0

    move-object/from16 v0, v19

    move-wide/from16 v1, v21

    invoke-virtual {v0, v1, v2}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 47
    const/16 v20, 0x1

    .line 48
    .local v20, "write":Z
    :cond_128
    :goto_128
    invoke-virtual {v11}, Ljava/io/RandomAccessFile;->readLine()Ljava/lang/String;

    move-result-object v10

    .local v10, "line":Ljava/lang/String;
    if-eqz v10, :cond_169

    .line 49
    invoke-virtual {v10, v14}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v21

    if-nez v21, :cond_166

    if-eqz v20, :cond_166

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v21

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "\n"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/io/RandomAccessFile;->writeBytes(Ljava/lang/String;)V

    .line 51
    :goto_152
    invoke-virtual {v10, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_155
    .catch Ljava/lang/Exception; {:try_start_116 .. :try_end_155} :catch_656
    .catch Ljava/lang/OutOfMemoryError; {:try_start_116 .. :try_end_155} :catch_3d1

    move-result v21

    if-eqz v21, :cond_128

    const/16 v20, 0x1

    goto :goto_128

    .line 41
    .end local v10    # "line":Ljava/lang/String;
    .end local v19    # "wr":Ljava/io/RandomAccessFile;
    .end local v20    # "write":Z
    .restart local v18    # "wr":Ljava/io/RandomAccessFile;
    :cond_15b
    :try_start_15b
    new-instance v12, Ljava/io/RandomAccessFile;

    const-string v21, "r"

    move-object/from16 v0, v21

    invoke-direct {v12, v4, v0}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_164
    .catch Ljava/lang/Exception; {:try_start_15b .. :try_end_164} :catch_28d
    .catch Ljava/lang/OutOfMemoryError; {:try_start_15b .. :try_end_164} :catch_3d1

    .end local v11    # "ram":Ljava/io/RandomAccessFile;
    .restart local v12    # "ram":Ljava/io/RandomAccessFile;
    move-object v11, v12

    .end local v12    # "ram":Ljava/io/RandomAccessFile;
    .restart local v11    # "ram":Ljava/io/RandomAccessFile;
    goto :goto_100

    .line 50
    .end local v18    # "wr":Ljava/io/RandomAccessFile;
    .restart local v10    # "line":Ljava/lang/String;
    .restart local v19    # "wr":Ljava/io/RandomAccessFile;
    .restart local v20    # "write":Z
    :cond_166
    const/16 v20, 0x0

    goto :goto_152

    .line 54
    :cond_169
    :try_start_169
    invoke-virtual {v11}, Ljava/io/RandomAccessFile;->close()V

    .line 55
    invoke-virtual/range {v19 .. v19}, Ljava/io/RandomAccessFile;->close()V
    :try_end_16f
    .catch Ljava/lang/Exception; {:try_start_169 .. :try_end_16f} :catch_656
    .catch Ljava/lang/OutOfMemoryError; {:try_start_169 .. :try_end_16f} :catch_3d1

    .line 65
    :try_start_16f
    new-instance v21, Ljava/io/File;

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v22, Ljava/io/File;

    move-object/from16 v0, v22

    invoke-direct {v0, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static/range {v21 .. v22}, Lcom/chelpus/Utils;->save_text_to_end_file_from_file(Ljava/io/File;Ljava/io/File;)Z
    :try_end_182
    .catch Ljava/lang/Exception; {:try_start_16f .. :try_end_182} :catch_2c9
    .catch Ljava/lang/OutOfMemoryError; {:try_start_16f .. :try_end_182} :catch_3d1

    move-result v21

    if-eqz v21, :cond_64d

    .line 67
    :try_start_185
    sget-object v21, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v22, "/system"

    const-string v23, "rw"

    invoke-static/range {v22 .. v23}, Lcom/chelpus/Utils;->remount(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v22

    invoke-virtual/range {v21 .. v22}, Ljava/io/PrintStream;->println(Z)V

    .line 68
    const/16 v21, 0x3

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "chmod"

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const-string v23, "777"

    aput-object v23, v21, v22

    const/16 v22, 0x2

    const-string v23, "/system/etc/hosts"

    aput-object v23, v21, v22

    invoke-static/range {v21 .. v21}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 69
    sget-object v21, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v22, "/system"

    const-string v23, "rw"

    invoke-static/range {v22 .. v23}, Lcom/chelpus/Utils;->remount(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v22

    invoke-virtual/range {v21 .. v22}, Ljava/io/PrintStream;->println(Z)V

    .line 70
    if-nez v9, :cond_3de

    .line 71
    new-instance v21, Ljava/io/File;

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v22, Ljava/io/File;

    const-string v23, "/system/etc/hosts"

    invoke-direct/range {v22 .. v23}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static/range {v21 .. v22}, Lcom/chelpus/Utils;->copyFile(Ljava/io/File;Ljava/io/File;)V

    .line 72
    new-instance v21, Ljava/io/File;

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->length()J

    move-result-wide v21

    new-instance v23, Ljava/io/File;

    const-string v24, "/system/etc/hosts"

    invoke-direct/range {v23 .. v24}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v23 .. v23}, Ljava/io/File;->length()J

    move-result-wide v23

    cmp-long v21, v21, v23

    if-eqz v21, :cond_39f

    .line 73
    sget-object v21, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    move-result v22

    invoke-virtual/range {v21 .. v22}, Ljava/io/PrintStream;->println(I)V

    .line 74
    sget-object v21, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v22, Ljava/io/File;

    const-string v23, "/system/etc/hosts"

    invoke-direct/range {v22 .. v23}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v22 .. v22}, Ljava/io/File;->length()J

    move-result-wide v22

    invoke-virtual/range {v21 .. v23}, Ljava/io/PrintStream;->println(J)V

    .line 76
    new-instance v21, Ljava/io/File;

    move-object/from16 v0, v21

    invoke-direct {v0, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->delete()Z

    .line 77
    new-instance v21, Ljava/io/File;

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v22, Ljava/io/File;

    const-string v23, "/data/data/hosts"

    invoke-direct/range {v22 .. v23}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static/range {v21 .. v22}, Lcom/chelpus/Utils;->copyFile(Ljava/io/File;Ljava/io/File;)V

    .line 78
    new-instance v21, Ljava/io/File;

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->length()J

    move-result-wide v21

    new-instance v23, Ljava/io/File;

    const-string v24, "/data/data/hosts"

    invoke-direct/range {v23 .. v24}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v23 .. v23}, Ljava/io/File;->length()J

    move-result-wide v23

    cmp-long v21, v21, v23

    if-eqz v21, :cond_2d8

    .line 79
    sget-object v21, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v22, "no_space_to_data"

    invoke-virtual/range {v21 .. v22}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 80
    new-instance v21, Ljava/io/File;

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->delete()Z

    .line 81
    new-instance v21, Ljava/io/File;

    move-object/from16 v0, v21

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->delete()Z

    .line 82
    new-instance v21, Ljava/io/File;

    const-string v22, "/system/etc/hosts"

    invoke-direct/range {v21 .. v22}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const-string v22, "127.0.0.1       localhost\n"

    invoke-static/range {v21 .. v22}, Lcom/chelpus/Utils;->save_text_to_file(Ljava/io/File;Ljava/lang/String;)Z

    .line 83
    const/16 v21, 0x3

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "chmod"

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const-string v23, "0644"

    aput-object v23, v21, v22

    const/16 v22, 0x2

    const-string v23, "/system/etc/hosts"

    aput-object v23, v21, v22

    invoke-static/range {v21 .. v21}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 84
    const-string v21, "/system"

    const-string v22, "ro"

    invoke-static/range {v21 .. v22}, Lcom/chelpus/Utils;->remount(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_28c
    .catch Ljava/lang/Exception; {:try_start_185 .. :try_end_28c} :catch_466
    .catch Ljava/lang/OutOfMemoryError; {:try_start_185 .. :try_end_28c} :catch_3d1

    .line 171
    .end local v10    # "line":Ljava/lang/String;
    .end local v11    # "ram":Ljava/io/RandomAccessFile;
    .end local v19    # "wr":Ljava/io/RandomAccessFile;
    .end local v20    # "write":Z
    :goto_28c
    return-void

    .line 56
    .restart local v11    # "ram":Ljava/io/RandomAccessFile;
    .restart local v18    # "wr":Ljava/io/RandomAccessFile;
    :catch_28d
    move-exception v6

    .line 57
    .local v6, "e1":Ljava/lang/Exception;
    :goto_28e
    :try_start_28e
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    .line 58
    sget-object v21, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v22, "no_space_to_data"

    invoke-virtual/range {v21 .. v22}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 59
    new-instance v21, Ljava/io/File;

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->delete()Z

    .line 60
    const/16 v21, 0x3

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "chmod"

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const-string v23, "0644"

    aput-object v23, v21, v22

    const/16 v22, 0x2

    const-string v23, "/system/etc/hosts"

    aput-object v23, v21, v22

    invoke-static/range {v21 .. v21}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 61
    const-string v21, "/system"

    const-string v22, "ro"

    invoke-static/range {v21 .. v22}, Lcom/chelpus/Utils;->remount(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_2c8
    .catch Ljava/lang/Exception; {:try_start_28e .. :try_end_2c8} :catch_2c9
    .catch Ljava/lang/OutOfMemoryError; {:try_start_28e .. :try_end_2c8} :catch_3d1

    goto :goto_28c

    .line 161
    .end local v6    # "e1":Ljava/lang/Exception;
    .end local v11    # "ram":Ljava/io/RandomAccessFile;
    .end local v18    # "wr":Ljava/io/RandomAccessFile;
    :catch_2c9
    move-exception v5

    .line 163
    .local v5, "e":Ljava/lang/Exception;
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V

    .line 164
    sget-object v21, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v22, "unknown error"

    invoke-virtual/range {v21 .. v22}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 170
    .end local v5    # "e":Ljava/lang/Exception;
    :goto_2d4
    invoke-static {}, Lcom/chelpus/Utils;->exitFromRootJava()V

    goto :goto_28c

    .line 87
    .restart local v10    # "line":Ljava/lang/String;
    .restart local v11    # "ram":Ljava/io/RandomAccessFile;
    .restart local v19    # "wr":Ljava/io/RandomAccessFile;
    .restart local v20    # "write":Z
    :cond_2d8
    const/16 v21, 0x3

    :try_start_2da
    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "chmod"

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const-string v23, "0644"

    aput-object v23, v21, v22

    const/16 v22, 0x2

    const-string v23, "/data/data/hosts"

    aput-object v23, v21, v22

    invoke-static/range {v21 .. v21}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 88
    const/16 v21, 0x3

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "chattr"

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const-string v23, "-ai"

    aput-object v23, v21, v22

    const/16 v22, 0x2

    const-string v23, "/data/data/hosts"

    aput-object v23, v21, v22

    invoke-static/range {v21 .. v21}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 89
    const/16 v21, 0x3

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "chown"

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const-string v23, "0.0"

    aput-object v23, v21, v22

    const/16 v22, 0x2

    const-string v23, "/data/data/hosts"

    aput-object v23, v21, v22

    invoke-static/range {v21 .. v21}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 90
    const/16 v21, 0x3

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "chown"

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const-string v23, "0:0"

    aput-object v23, v21, v22

    const/16 v22, 0x2

    const-string v23, "/data/data/hosts"

    aput-object v23, v21, v22

    invoke-static/range {v21 .. v21}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 91
    const/16 v21, 0x4

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "ln"

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const-string v23, "-s"

    aput-object v23, v21, v22

    const/16 v22, 0x2

    const-string v23, "/data/data/hosts"

    aput-object v23, v21, v22

    const/16 v22, 0x3

    const-string v23, "/system/etc/hosts"

    aput-object v23, v21, v22

    invoke-static/range {v21 .. v21}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 92
    new-instance v21, Ljava/io/File;

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->delete()Z

    .line 93
    const/16 v21, 0x3

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "chmod"

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const-string v23, "0644"

    aput-object v23, v21, v22

    const/16 v22, 0x2

    const-string v23, "/system/etc/hosts"

    aput-object v23, v21, v22

    invoke-static/range {v21 .. v21}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 94
    const-string v21, "/system"

    const-string v22, "ro"

    invoke-static/range {v21 .. v22}, Lcom/chelpus/Utils;->remount(Ljava/lang/String;Ljava/lang/String;)Z

    .line 135
    :cond_39f
    :goto_39f
    sget-object v21, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v22, "host updated!"

    invoke-virtual/range {v21 .. v22}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_3a6
    .catch Ljava/lang/Exception; {:try_start_2da .. :try_end_3a6} :catch_466
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2da .. :try_end_3a6} :catch_3d1

    .line 154
    :cond_3a6
    :goto_3a6
    :try_start_3a6
    new-instance v21, Ljava/io/File;

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->delete()Z

    .line 155
    const/16 v21, 0x3

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "chmod"

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const-string v23, "0644"

    aput-object v23, v21, v22

    const/16 v22, 0x2

    const-string v23, "/system/etc/hosts"

    aput-object v23, v21, v22

    invoke-static/range {v21 .. v21}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V
    :try_end_3cf
    .catch Ljava/lang/Exception; {:try_start_3a6 .. :try_end_3cf} :catch_2c9
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3a6 .. :try_end_3cf} :catch_3d1

    goto/16 :goto_2d4

    .line 166
    .end local v10    # "line":Ljava/lang/String;
    .end local v11    # "ram":Ljava/io/RandomAccessFile;
    .end local v19    # "wr":Ljava/io/RandomAccessFile;
    .end local v20    # "write":Z
    :catch_3d1
    move-exception v3

    .line 167
    .local v3, "E":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v3}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    .line 168
    sget-object v21, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v22, "out.of.memory"

    invoke-virtual/range {v21 .. v22}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_2d4

    .line 100
    .end local v3    # "E":Ljava/lang/OutOfMemoryError;
    .restart local v10    # "line":Ljava/lang/String;
    .restart local v11    # "ram":Ljava/io/RandomAccessFile;
    .restart local v19    # "wr":Ljava/io/RandomAccessFile;
    .restart local v20    # "write":Z
    :cond_3de
    :try_start_3de
    new-instance v21, Ljava/io/File;

    move-object/from16 v0, v21

    invoke-direct {v0, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->delete()Z

    .line 101
    new-instance v21, Ljava/io/File;

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v22, Ljava/io/File;

    const-string v23, "/data/data/hosts"

    invoke-direct/range {v22 .. v23}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static/range {v21 .. v22}, Lcom/chelpus/Utils;->copyFile(Ljava/io/File;Ljava/io/File;)V

    .line 102
    new-instance v21, Ljava/io/File;

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->length()J

    move-result-wide v21

    new-instance v23, Ljava/io/File;

    const-string v24, "/data/data/hosts"

    invoke-direct/range {v23 .. v24}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v23 .. v23}, Ljava/io/File;->length()J

    move-result-wide v23

    cmp-long v21, v21, v23

    if-eqz v21, :cond_4b0

    .line 103
    sget-object v21, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v22, "no_space_to_data"

    invoke-virtual/range {v21 .. v22}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 104
    new-instance v21, Ljava/io/File;

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->delete()Z

    .line 105
    new-instance v21, Ljava/io/File;

    move-object/from16 v0, v21

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->delete()Z

    .line 106
    new-instance v21, Ljava/io/File;

    const-string v22, "/system/etc/hosts"

    invoke-direct/range {v21 .. v22}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const-string v22, "127.0.0.1       localhost\n"

    invoke-static/range {v21 .. v22}, Lcom/chelpus/Utils;->save_text_to_file(Ljava/io/File;Ljava/lang/String;)Z

    .line 107
    const/16 v21, 0x3

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "chmod"

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const-string v23, "0644"

    aput-object v23, v21, v22

    const/16 v22, 0x2

    const-string v23, "/system/etc/hosts"

    aput-object v23, v21, v22

    invoke-static/range {v21 .. v21}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 108
    const-string v21, "/system"

    const-string v22, "ro"

    invoke-static/range {v21 .. v22}, Lcom/chelpus/Utils;->remount(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_464
    .catch Ljava/lang/Exception; {:try_start_3de .. :try_end_464} :catch_466
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3de .. :try_end_464} :catch_3d1

    goto/16 :goto_28c

    .line 137
    :catch_466
    move-exception v5

    .line 139
    .restart local v5    # "e":Ljava/lang/Exception;
    :try_start_467
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V

    .line 140
    new-instance v21, Ljava/io/File;

    const-string v22, "/system/etc/hosts"

    invoke-direct/range {v21 .. v22}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const-string v22, "127.0.0.1       localhost\n"

    invoke-static/range {v21 .. v22}, Lcom/chelpus/Utils;->save_text_to_file(Ljava/io/File;Ljava/lang/String;)Z

    .line 141
    const/16 v21, 0x3

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "chmod"

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const-string v23, "777"

    aput-object v23, v21, v22

    const/16 v22, 0x2

    const-string v23, "/system/etc/hosts"

    aput-object v23, v21, v22

    invoke-static/range {v21 .. v21}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 142
    new-instance v21, Ljava/io/File;

    const-string v22, "/system/etc/hosts"

    invoke-direct/range {v21 .. v22}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v22, Ljava/io/File;

    move-object/from16 v0, v22

    invoke-direct {v0, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static/range {v21 .. v22}, Lcom/chelpus/Utils;->save_text_to_end_file_from_file(Ljava/io/File;Ljava/io/File;)Z

    move-result v21

    if-nez v21, :cond_3a6

    .line 144
    sget-object v21, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v22, "out.of.memory"

    invoke-virtual/range {v21 .. v22}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_4ae
    .catch Ljava/lang/Exception; {:try_start_467 .. :try_end_4ae} :catch_2c9
    .catch Ljava/lang/OutOfMemoryError; {:try_start_467 .. :try_end_4ae} :catch_3d1

    goto/16 :goto_3a6

    .line 111
    .end local v5    # "e":Ljava/lang/Exception;
    :cond_4b0
    :try_start_4b0
    new-instance v21, Ljava/io/File;

    move-object/from16 v0, v21

    invoke-direct {v0, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->length()J

    move-result-wide v21

    new-instance v23, Ljava/io/File;

    move-object/from16 v0, v23

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v23 .. v23}, Ljava/io/File;->length()J

    move-result-wide v23

    cmp-long v21, v21, v23

    if-eqz v21, :cond_5d7

    .line 112
    const/16 v21, 0x3

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "chmod"

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const-string v23, "0777"

    aput-object v23, v21, v22

    const/16 v22, 0x2

    const-string v23, "/system/etc/hosts"

    aput-object v23, v21, v22

    invoke-static/range {v21 .. v21}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 113
    new-instance v21, Ljava/io/File;

    move-object/from16 v0, v21

    invoke-direct {v0, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->delete()Z

    .line 114
    const/16 v21, 0x3

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "chmod"

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const-string v23, "0777"

    aput-object v23, v21, v22

    const/16 v22, 0x2

    const-string v23, "/data/data/hosts"

    aput-object v23, v21, v22

    invoke-static/range {v21 .. v21}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 115
    const/16 v21, 0x3

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "chattr"

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const-string v23, "-ai"

    aput-object v23, v21, v22

    const/16 v22, 0x2

    const-string v23, "/data/data/hosts"

    aput-object v23, v21, v22

    invoke-static/range {v21 .. v21}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 116
    const/16 v21, 0x3

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "chown"

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const-string v23, "0.0"

    aput-object v23, v21, v22

    const/16 v22, 0x2

    const-string v23, "/data/data/hosts"

    aput-object v23, v21, v22

    invoke-static/range {v21 .. v21}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 117
    const/16 v21, 0x3

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "chown"

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const-string v23, "0:0"

    aput-object v23, v21, v22

    const/16 v22, 0x2

    const-string v23, "/data/data/hosts"

    aput-object v23, v21, v22

    invoke-static/range {v21 .. v21}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 118
    const/16 v21, 0x4

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "ln"

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const-string v23, "-s"

    aput-object v23, v21, v22

    const/16 v22, 0x2

    const-string v23, "/data/data/hosts"

    aput-object v23, v21, v22

    const/16 v22, 0x3

    const-string v23, "/system/etc/hosts"

    aput-object v23, v21, v22

    invoke-static/range {v21 .. v21}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 119
    const/16 v21, 0x3

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "chmod"

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const-string v23, "0644"

    aput-object v23, v21, v22

    const/16 v22, 0x2

    const-string v23, "/data/data/hosts"

    aput-object v23, v21, v22

    invoke-static/range {v21 .. v21}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 127
    :goto_5a5
    new-instance v21, Ljava/io/File;

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->delete()Z

    .line 128
    const/16 v21, 0x3

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "chmod"

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const-string v23, "0644"

    aput-object v23, v21, v22

    const/16 v22, 0x2

    const-string v23, "/system/etc/hosts"

    aput-object v23, v21, v22

    invoke-static/range {v21 .. v21}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 129
    const-string v21, "/system"

    const-string v22, "ro"

    invoke-static/range {v21 .. v22}, Lcom/chelpus/Utils;->remount(Ljava/lang/String;Ljava/lang/String;)Z

    goto/16 :goto_39f

    .line 121
    :cond_5d7
    const/16 v21, 0x3

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "chmod"

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const-string v23, "0644"

    aput-object v23, v21, v22

    const/16 v22, 0x2

    const-string v23, "/data/data/hosts"

    aput-object v23, v21, v22

    invoke-static/range {v21 .. v21}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 122
    const/16 v21, 0x3

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "chattr"

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const-string v23, "-ai"

    aput-object v23, v21, v22

    const/16 v22, 0x2

    const-string v23, "/data/data/hosts"

    aput-object v23, v21, v22

    invoke-static/range {v21 .. v21}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 123
    const/16 v21, 0x3

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "chown"

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const-string v23, "0.0"

    aput-object v23, v21, v22

    const/16 v22, 0x2

    const-string v23, "/data/data/hosts"

    aput-object v23, v21, v22

    invoke-static/range {v21 .. v21}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 124
    const/16 v21, 0x3

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const-string v23, "chown"

    aput-object v23, v21, v22

    const/16 v22, 0x1

    const-string v23, "0:0"

    aput-object v23, v21, v22

    const/16 v22, 0x2

    const-string v23, "/data/data/hosts"

    aput-object v23, v21, v22

    invoke-static/range {v21 .. v21}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V
    :try_end_64b
    .catch Ljava/lang/Exception; {:try_start_4b0 .. :try_end_64b} :catch_466
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4b0 .. :try_end_64b} :catch_3d1

    goto/16 :goto_5a5

    .line 151
    :cond_64d
    :try_start_64d
    sget-object v21, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v22, "no_space_to_data"

    invoke-virtual/range {v21 .. v22}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_654
    .catch Ljava/lang/Exception; {:try_start_64d .. :try_end_654} :catch_2c9
    .catch Ljava/lang/OutOfMemoryError; {:try_start_64d .. :try_end_654} :catch_3d1

    goto/16 :goto_3a6

    .line 56
    .end local v10    # "line":Ljava/lang/String;
    .end local v20    # "write":Z
    :catch_656
    move-exception v6

    move-object/from16 v18, v19

    .end local v19    # "wr":Ljava/io/RandomAccessFile;
    .restart local v18    # "wr":Ljava/io/RandomAccessFile;
    goto/16 :goto_28e
.end method
