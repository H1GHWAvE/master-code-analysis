.class public Lcom/chelpus/root/utils/uninstall;
.super Ljava/lang/Object;
.source "uninstall.java"


# static fields
.field public static datadir:Ljava/lang/String;

.field public static dirapp:Ljava/lang/String;

.field public static odexpatch:Z

.field public static system:Z


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    const/4 v1, 0x0

    .line 19
    const-string v0, "/data/app/"

    sput-object v0, Lcom/chelpus/root/utils/uninstall;->dirapp:Ljava/lang/String;

    .line 20
    const-string v0, "/data/data/"

    sput-object v0, Lcom/chelpus/root/utils/uninstall;->datadir:Ljava/lang/String;

    .line 21
    sput-boolean v1, Lcom/chelpus/root/utils/uninstall;->system:Z

    .line 22
    sput-boolean v1, Lcom/chelpus/root/utils/uninstall;->odexpatch:Z

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getLibs(Ljava/io/File;)Ljava/util/ArrayList;
    .registers 19
    .param p0, "apk"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 85
    const/4 v10, 0x0

    .line 86
    .local v10, "in":Ljava/util/zip/ZipInputStream;
    const/4 v8, 0x0

    .line 87
    .local v8, "fin":Ljava/io/FileInputStream;
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 89
    .local v12, "libs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :try_start_7
    new-instance v9, Ljava/io/FileInputStream;

    move-object/from16 v0, p0

    invoke-direct {v9, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_e} :catch_78
    .catchall {:try_start_7 .. :try_end_e} :catchall_10e

    .line 94
    .end local v8    # "fin":Ljava/io/FileInputStream;
    .local v9, "fin":Ljava/io/FileInputStream;
    :try_start_e
    new-instance v11, Ljava/util/zip/ZipInputStream;

    invoke-direct {v11, v9}, Ljava/util/zip/ZipInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_13
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_13} :catch_131
    .catchall {:try_start_e .. :try_end_13} :catchall_12a

    .line 95
    .end local v10    # "in":Ljava/util/zip/ZipInputStream;
    .local v11, "in":Ljava/util/zip/ZipInputStream;
    :try_start_13
    invoke-virtual {v11}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    move-result-object v4

    .local v4, "entry":Ljava/util/zip/ZipEntry;
    :goto_17
    if-eqz v4, :cond_6b

    .line 96
    invoke-virtual {v4}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v15

    const-string v16, "lib/"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_66

    .line 97
    invoke-virtual {v4}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v15

    const-string v16, "/"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v13

    .line 98
    .local v13, "tail":[Ljava/lang/String;
    array-length v15, v13

    add-int/lit8 v15, v15, -0x1

    aget-object v7, v13, v15

    .line 99
    .local v7, "filename":Ljava/lang/String;
    invoke-virtual {v12}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v15

    if-eqz v15, :cond_4d

    const-string v15, ""

    invoke-virtual {v7, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_4d

    const-string v15, "libjnigraphics.so"

    invoke-virtual {v7, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_4d

    invoke-virtual {v12, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 100
    :cond_4d
    invoke-virtual {v12, v7}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_66

    const-string v15, ""

    invoke-virtual {v7, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_66

    const-string v15, "libjnigraphics.so"

    invoke-virtual {v7, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_66

    invoke-virtual {v12, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 95
    .end local v7    # "filename":Ljava/lang/String;
    .end local v13    # "tail":[Ljava/lang/String;
    :cond_66
    invoke-virtual {v11}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;
    :try_end_69
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_69} :catch_135
    .catchall {:try_start_13 .. :try_end_69} :catchall_12d

    move-result-object v4

    goto :goto_17

    .line 136
    :cond_6b
    if-eqz v11, :cond_70

    .line 137
    :try_start_6d
    invoke-virtual {v11}, Ljava/util/zip/ZipInputStream;->close()V
    :try_end_70
    .catch Ljava/io/IOException; {:try_start_6d .. :try_end_70} :catch_11c

    .line 142
    :cond_70
    :goto_70
    if-eqz v9, :cond_75

    .line 143
    :try_start_72
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_75
    .catch Ljava/io/IOException; {:try_start_72 .. :try_end_75} :catch_11f

    .line 147
    :cond_75
    :goto_75
    const/4 v10, 0x0

    .line 148
    .end local v11    # "in":Ljava/util/zip/ZipInputStream;
    .restart local v10    # "in":Ljava/util/zip/ZipInputStream;
    const/4 v8, 0x0

    .line 150
    .end local v4    # "entry":Ljava/util/zip/ZipEntry;
    .end local v9    # "fin":Ljava/io/FileInputStream;
    .restart local v8    # "fin":Ljava/io/FileInputStream;
    :goto_77
    return-object v12

    .line 105
    :catch_78
    move-exception v2

    .line 108
    .local v2, "e":Ljava/io/IOException;
    :goto_79
    :try_start_79
    new-instance v14, Lnet/lingala/zip4j/core/ZipFile;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lnet/lingala/zip4j/core/ZipFile;-><init>(Ljava/io/File;)V

    .line 111
    .local v14, "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    invoke-virtual {v14}, Lnet/lingala/zip4j/core/ZipFile;->getFileHeaders()Ljava/util/List;

    move-result-object v6

    .line 114
    .local v6, "fileHeaderList":Ljava/util/List;
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :cond_88
    :goto_88
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-eqz v16, :cond_fb

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 115
    .local v1, "aFileHeaderList":Ljava/lang/Object;
    move-object v0, v1

    check-cast v0, Lnet/lingala/zip4j/model/FileHeader;

    move-object v5, v0

    .line 116
    .local v5, "fileHeader":Lnet/lingala/zip4j/model/FileHeader;
    invoke-virtual {v5}, Lnet/lingala/zip4j/model/FileHeader;->getFileName()Ljava/lang/String;

    move-result-object v16

    const-string v17, ".so"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v16

    if-eqz v16, :cond_88

    .line 117
    sget-object v16, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v5}, Lnet/lingala/zip4j/model/FileHeader;->getFileName()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 118
    invoke-virtual {v5}, Lnet/lingala/zip4j/model/FileHeader;->getFileName()Ljava/lang/String;

    move-result-object v16

    const-string v17, "/"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v13

    .line 119
    .restart local v13    # "tail":[Ljava/lang/String;
    array-length v0, v13

    move/from16 v16, v0

    add-int/lit8 v16, v16, -0x1

    aget-object v7, v13, v16

    .line 120
    .restart local v7    # "filename":Ljava/lang/String;
    invoke-virtual {v12}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v16

    if-eqz v16, :cond_d9

    const-string v16, ""

    move-object/from16 v0, v16

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-nez v16, :cond_d9

    const-string v16, "libjnigraphics.so"

    move-object/from16 v0, v16

    invoke-virtual {v7, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v16

    if-nez v16, :cond_d9

    .line 121
    invoke-virtual {v12, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 122
    :cond_d9
    invoke-virtual {v12, v7}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v16

    if-nez v16, :cond_88

    const-string v16, ""

    move-object/from16 v0, v16

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-nez v16, :cond_88

    const-string v16, "libjnigraphics.so"

    move-object/from16 v0, v16

    invoke-virtual {v7, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v16

    if-nez v16, :cond_88

    .line 123
    invoke-virtual {v12, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_f6
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_79 .. :try_end_f6} :catch_f7
    .catch Ljava/lang/Exception; {:try_start_79 .. :try_end_f6} :catch_109
    .catchall {:try_start_79 .. :try_end_f6} :catchall_10e

    goto :goto_88

    .line 128
    .end local v1    # "aFileHeaderList":Ljava/lang/Object;
    .end local v5    # "fileHeader":Lnet/lingala/zip4j/model/FileHeader;
    .end local v6    # "fileHeaderList":Ljava/util/List;
    .end local v7    # "filename":Ljava/lang/String;
    .end local v13    # "tail":[Ljava/lang/String;
    .end local v14    # "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    :catch_f7
    move-exception v3

    .line 129
    .local v3, "e1":Lnet/lingala/zip4j/exception/ZipException;
    :try_start_f8
    invoke-virtual {v3}, Lnet/lingala/zip4j/exception/ZipException;->printStackTrace()V
    :try_end_fb
    .catchall {:try_start_f8 .. :try_end_fb} :catchall_10e

    .line 136
    .end local v3    # "e1":Lnet/lingala/zip4j/exception/ZipException;
    :cond_fb
    :goto_fb
    if-eqz v10, :cond_100

    .line 137
    :try_start_fd
    invoke-virtual {v10}, Ljava/util/zip/ZipInputStream;->close()V
    :try_end_100
    .catch Ljava/io/IOException; {:try_start_fd .. :try_end_100} :catch_122

    .line 142
    :cond_100
    :goto_100
    if-eqz v8, :cond_105

    .line 143
    :try_start_102
    invoke-virtual {v8}, Ljava/io/FileInputStream;->close()V
    :try_end_105
    .catch Ljava/io/IOException; {:try_start_102 .. :try_end_105} :catch_124

    .line 147
    :cond_105
    :goto_105
    const/4 v10, 0x0

    .line 148
    const/4 v8, 0x0

    .line 149
    goto/16 :goto_77

    .line 130
    :catch_109
    move-exception v3

    .line 131
    .local v3, "e1":Ljava/lang/Exception;
    :try_start_10a
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_10d
    .catchall {:try_start_10a .. :try_end_10d} :catchall_10e

    goto :goto_fb

    .line 135
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "e1":Ljava/lang/Exception;
    :catchall_10e
    move-exception v15

    .line 136
    :goto_10f
    if-eqz v10, :cond_114

    .line 137
    :try_start_111
    invoke-virtual {v10}, Ljava/util/zip/ZipInputStream;->close()V
    :try_end_114
    .catch Ljava/io/IOException; {:try_start_111 .. :try_end_114} :catch_126

    .line 142
    :cond_114
    :goto_114
    if-eqz v8, :cond_119

    .line 143
    :try_start_116
    invoke-virtual {v8}, Ljava/io/FileInputStream;->close()V
    :try_end_119
    .catch Ljava/io/IOException; {:try_start_116 .. :try_end_119} :catch_128

    .line 147
    :cond_119
    :goto_119
    const/4 v10, 0x0

    .line 148
    const/4 v8, 0x0

    throw v15

    .line 139
    .end local v8    # "fin":Ljava/io/FileInputStream;
    .end local v10    # "in":Ljava/util/zip/ZipInputStream;
    .restart local v4    # "entry":Ljava/util/zip/ZipEntry;
    .restart local v9    # "fin":Ljava/io/FileInputStream;
    .restart local v11    # "in":Ljava/util/zip/ZipInputStream;
    :catch_11c
    move-exception v15

    goto/16 :goto_70

    .line 145
    :catch_11f
    move-exception v15

    goto/16 :goto_75

    .line 139
    .end local v4    # "entry":Ljava/util/zip/ZipEntry;
    .end local v9    # "fin":Ljava/io/FileInputStream;
    .end local v11    # "in":Ljava/util/zip/ZipInputStream;
    .restart local v2    # "e":Ljava/io/IOException;
    .restart local v8    # "fin":Ljava/io/FileInputStream;
    .restart local v10    # "in":Ljava/util/zip/ZipInputStream;
    :catch_122
    move-exception v15

    goto :goto_100

    .line 145
    :catch_124
    move-exception v15

    goto :goto_105

    .line 139
    .end local v2    # "e":Ljava/io/IOException;
    :catch_126
    move-exception v16

    goto :goto_114

    .line 145
    :catch_128
    move-exception v16

    goto :goto_119

    .line 135
    .end local v8    # "fin":Ljava/io/FileInputStream;
    .restart local v9    # "fin":Ljava/io/FileInputStream;
    :catchall_12a
    move-exception v15

    move-object v8, v9

    .end local v9    # "fin":Ljava/io/FileInputStream;
    .restart local v8    # "fin":Ljava/io/FileInputStream;
    goto :goto_10f

    .end local v8    # "fin":Ljava/io/FileInputStream;
    .end local v10    # "in":Ljava/util/zip/ZipInputStream;
    .restart local v9    # "fin":Ljava/io/FileInputStream;
    .restart local v11    # "in":Ljava/util/zip/ZipInputStream;
    :catchall_12d
    move-exception v15

    move-object v8, v9

    .end local v9    # "fin":Ljava/io/FileInputStream;
    .restart local v8    # "fin":Ljava/io/FileInputStream;
    move-object v10, v11

    .end local v11    # "in":Ljava/util/zip/ZipInputStream;
    .restart local v10    # "in":Ljava/util/zip/ZipInputStream;
    goto :goto_10f

    .line 105
    .end local v8    # "fin":Ljava/io/FileInputStream;
    .restart local v9    # "fin":Ljava/io/FileInputStream;
    :catch_131
    move-exception v2

    move-object v8, v9

    .end local v9    # "fin":Ljava/io/FileInputStream;
    .restart local v8    # "fin":Ljava/io/FileInputStream;
    goto/16 :goto_79

    .end local v8    # "fin":Ljava/io/FileInputStream;
    .end local v10    # "in":Ljava/util/zip/ZipInputStream;
    .restart local v9    # "fin":Ljava/io/FileInputStream;
    .restart local v11    # "in":Ljava/util/zip/ZipInputStream;
    :catch_135
    move-exception v2

    move-object v8, v9

    .end local v9    # "fin":Ljava/io/FileInputStream;
    .restart local v8    # "fin":Ljava/io/FileInputStream;
    move-object v10, v11

    .end local v11    # "in":Ljava/util/zip/ZipInputStream;
    .restart local v10    # "in":Ljava/util/zip/ZipInputStream;
    goto/16 :goto_79
.end method

.method public static main([Ljava/lang/String;)V
    .registers 14
    .param p0, "paramArrayOfString"    # [Ljava/lang/String;

    .prologue
    .line 27
    :try_start_0
    new-instance v9, Lcom/chelpus/root/utils/uninstall$1;

    invoke-direct {v9}, Lcom/chelpus/root/utils/uninstall$1;-><init>()V

    invoke-static {v9}, Lcom/chelpus/Utils;->startRootJava(Ljava/lang/Object;)V

    .line 28
    const/4 v9, 0x1

    aget-object v9, p0, v9

    sput-object v9, Lcom/chelpus/root/utils/uninstall;->dirapp:Ljava/lang/String;

    .line 29
    const/4 v9, 0x1

    sput-boolean v9, Lcom/chelpus/root/utils/uninstall;->system:Z

    .line 30
    const/4 v9, 0x2

    aget-object v9, p0, v9

    sput-object v9, Lcom/chelpus/root/utils/uninstall;->datadir:Ljava/lang/String;

    .line 31
    new-instance v1, Ljava/io/File;

    sget-object v9, Lcom/chelpus/root/utils/uninstall;->dirapp:Ljava/lang/String;

    invoke-direct {v1, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 32
    .local v1, "appapk":Ljava/io/File;
    new-instance v2, Ljava/io/File;

    sget-object v9, Lcom/chelpus/root/utils/uninstall;->dirapp:Ljava/lang/String;

    const/4 v10, 0x1

    invoke-static {v9, v10}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v2, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 33
    .local v2, "appodex":Ljava/io/File;
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Start getLibs!"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 34
    invoke-static {v1}, Lcom/chelpus/root/utils/uninstall;->getLibs(Ljava/io/File;)Ljava/util/ArrayList;

    move-result-object v7

    .line 35
    .local v7, "libs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Start delete lib!"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 36
    invoke-virtual {v7}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_92

    .line 37
    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_44
    :goto_44
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_92

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 38
    .local v5, "lib":Ljava/lang/String;
    new-instance v6, Ljava/io/File;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "/system/lib/"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v6, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 39
    .local v6, "libfile":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_44

    invoke-virtual {v6}, Ljava/io/File;->delete()Z
    :try_end_71
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_71} :catch_72

    goto :goto_44

    .line 79
    .end local v1    # "appapk":Ljava/io/File;
    .end local v2    # "appodex":Ljava/io/File;
    .end local v5    # "lib":Ljava/lang/String;
    .end local v6    # "libfile":Ljava/io/File;
    .end local v7    # "libs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :catch_72
    move-exception v4

    .local v4, "e":Ljava/lang/Exception;
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "LuckyPatcher Error uninstall: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    .line 80
    .end local v4    # "e":Ljava/lang/Exception;
    :cond_8e
    :goto_8e
    invoke-static {}, Lcom/chelpus/Utils;->exitFromRootJava()V

    .line 81
    return-void

    .line 42
    .restart local v1    # "appapk":Ljava/io/File;
    .restart local v2    # "appodex":Ljava/io/File;
    .restart local v7    # "libs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_92
    :try_start_92
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Start delete data directory!"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 43
    new-instance v8, Lcom/chelpus/Utils;

    const-string v9, "uninstall system"

    invoke-direct {v8, v9}, Lcom/chelpus/Utils;-><init>(Ljava/lang/String;)V
    :try_end_a0
    .catch Ljava/lang/Exception; {:try_start_92 .. :try_end_a0} :catch_72

    .line 45
    .local v8, "utils":Lcom/chelpus/Utils;
    :try_start_a0
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Start delete dir!"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 46
    new-instance v9, Ljava/io/File;

    sget-object v10, Lcom/chelpus/root/utils/uninstall;->datadir:Ljava/lang/String;

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v9}, Lcom/chelpus/Utils;->deleteFolder(Ljava/io/File;)V

    .line 47
    new-instance v9, Ljava/io/File;

    sget-object v10, Lcom/chelpus/root/utils/uninstall;->datadir:Ljava/lang/String;

    const-string v11, "/data/data/"

    const-string v12, "/dbdata/databases/"

    invoke-virtual {v10, v11, v12}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v9}, Lcom/chelpus/Utils;->deleteFolder(Ljava/io/File;)V
    :try_end_c3
    .catch Ljava/lang/Exception; {:try_start_a0 .. :try_end_c3} :catch_136

    .line 53
    :goto_c3
    :try_start_c3
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Start delete dc!"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_ca
    .catch Ljava/lang/Exception; {:try_start_c3 .. :try_end_ca} :catch_72

    .line 56
    :try_start_ca
    sget-object v9, Lcom/chelpus/root/utils/uninstall;->dirapp:Ljava/lang/String;

    invoke-static {v9}, Lcom/chelpus/Utils;->getFileDalvikCache(Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    .line 57
    .local v3, "dc_file":Ljava/io/File;
    if-eqz v3, :cond_154

    .line 59
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 60
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Dalvik-cache "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " deleted."

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 65
    :goto_f3
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Start delete odex."

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 68
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_103

    invoke-virtual {v2}, Ljava/io/File;->delete()Z
    :try_end_103
    .catch Ljava/lang/Exception; {:try_start_ca .. :try_end_103} :catch_15c

    .line 76
    .end local v3    # "dc_file":Ljava/io/File;
    :cond_103
    :goto_103
    :try_start_103
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Start delete apk!"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 77
    new-instance v0, Ljava/io/File;

    sget-object v9, Lcom/chelpus/root/utils/uninstall;->dirapp:Ljava/lang/String;

    invoke-direct {v0, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 78
    .local v0, "apk":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_8e

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Delete apk:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sget-object v11, Lcom/chelpus/root/utils/uninstall;->dirapp:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_8e

    .line 48
    .end local v0    # "apk":Ljava/io/File;
    :catch_136
    move-exception v4

    .line 50
    .restart local v4    # "e":Ljava/lang/Exception;
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "LuckyPatcher Error uninstall: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 51
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_152
    .catch Ljava/lang/Exception; {:try_start_103 .. :try_end_152} :catch_72

    goto/16 :goto_c3

    .line 63
    .end local v4    # "e":Ljava/lang/Exception;
    .restart local v3    # "dc_file":Ljava/io/File;
    :cond_154
    :try_start_154
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "dalvik-cache not found."

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_15b
    .catch Ljava/lang/Exception; {:try_start_154 .. :try_end_15b} :catch_15c

    goto :goto_f3

    .line 71
    .end local v3    # "dc_file":Ljava/io/File;
    :catch_15c
    move-exception v4

    .line 72
    .restart local v4    # "e":Ljava/lang/Exception;
    :try_start_15d
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Error: Exception e"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v4}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 73
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_17c
    .catch Ljava/lang/Exception; {:try_start_15d .. :try_end_17c} :catch_72

    goto :goto_103
.end method
