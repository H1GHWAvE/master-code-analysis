.class public Lcom/chelpus/root/utils/runpatchsupport;
.super Ljava/lang/Object;
.source "runpatchsupport.java"


# static fields
.field private static ART:Z

.field public static appdir:Ljava/lang/String;

.field public static classesFiles:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private static copyDC:Z

.field private static createAPK:Z

.field public static crkapk:Ljava/io/File;

.field public static dir:Ljava/lang/String;

.field public static dir2:Ljava/lang/String;

.field public static dirapp:Ljava/lang/String;

.field public static filestopatch:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private static pattern1:Z

.field private static pattern2:Z

.field private static pattern3:Z

.field public static print:Ljava/io/PrintStream;

.field public static result:Ljava/lang/String;

.field public static sddir:Ljava/lang/String;

.field public static system:Z

.field public static uid:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 38
    sput-boolean v1, Lcom/chelpus/root/utils/runpatchsupport;->createAPK:Z

    .line 39
    sput-boolean v0, Lcom/chelpus/root/utils/runpatchsupport;->pattern1:Z

    .line 40
    sput-boolean v0, Lcom/chelpus/root/utils/runpatchsupport;->pattern2:Z

    .line 41
    sput-boolean v0, Lcom/chelpus/root/utils/runpatchsupport;->pattern3:Z

    .line 42
    sput-boolean v1, Lcom/chelpus/root/utils/runpatchsupport;->copyDC:Z

    .line 43
    sput-boolean v1, Lcom/chelpus/root/utils/runpatchsupport;->ART:Z

    .line 44
    const-string v0, "/data/app/"

    sput-object v0, Lcom/chelpus/root/utils/runpatchsupport;->dirapp:Ljava/lang/String;

    .line 45
    sput-boolean v1, Lcom/chelpus/root/utils/runpatchsupport;->system:Z

    .line 46
    const-string v0, ""

    sput-object v0, Lcom/chelpus/root/utils/runpatchsupport;->uid:Ljava/lang/String;

    .line 47
    const-string v0, "/sdcard/"

    sput-object v0, Lcom/chelpus/root/utils/runpatchsupport;->dir:Ljava/lang/String;

    .line 48
    const-string v0, "/sdcard/"

    sput-object v0, Lcom/chelpus/root/utils/runpatchsupport;->dir2:Ljava/lang/String;

    .line 49
    const/4 v0, 0x0

    sput-object v0, Lcom/chelpus/root/utils/runpatchsupport;->filestopatch:Ljava/util/ArrayList;

    .line 50
    const-string v0, "/sdcard/"

    sput-object v0, Lcom/chelpus/root/utils/runpatchsupport;->sddir:Ljava/lang/String;

    .line 51
    const-string v0, "/sdcard/"

    sput-object v0, Lcom/chelpus/root/utils/runpatchsupport;->appdir:Ljava/lang/String;

    .line 56
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/chelpus/root/utils/runpatchsupport;->classesFiles:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static byteverify(Ljava/nio/MappedByteBuffer;IB[B[B[B[BLjava/lang/String;Z)Z
    .registers 14
    .param p0, "fileBytes"    # Ljava/nio/MappedByteBuffer;
    .param p1, "curentPos"    # I
    .param p2, "curentByte"    # B
    .param p3, "byteOrig"    # [B
    .param p4, "mask"    # [B
    .param p5, "byteReplace"    # [B
    .param p6, "rep_mask"    # [B
    .param p7, "log"    # Ljava/lang/String;
    .param p8, "pattern"    # Z

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 903
    aget-byte v4, p3, v3

    if-ne p2, v4, :cond_42

    if-eqz p8, :cond_42

    .line 905
    aget-byte v4, p6, v3

    if-nez v4, :cond_e

    aput-byte p2, p5, v3

    .line 906
    :cond_e
    const/4 v0, 0x1

    .line 907
    .local v0, "i":I
    add-int/lit8 v4, p1, 0x1

    invoke-virtual {p0, v4}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 908
    invoke-virtual {p0}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v1

    .line 910
    .local v1, "prufbyte":B
    :goto_18
    aget-byte v4, p3, v0

    if-eq v1, v4, :cond_20

    aget-byte v4, p4, v0

    if-ne v4, v2, :cond_3d

    .line 912
    :cond_20
    aget-byte v4, p6, v0

    if-nez v4, :cond_26

    aput-byte v1, p5, v0

    .line 913
    :cond_26
    add-int/lit8 v0, v0, 0x1

    .line 915
    array-length v4, p3

    if-ne v0, v4, :cond_38

    .line 917
    invoke-virtual {p0, p1}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 918
    invoke-virtual {p0, p5}, Ljava/nio/MappedByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 919
    invoke-virtual {p0}, Ljava/nio/MappedByteBuffer;->force()Ljava/nio/MappedByteBuffer;

    .line 921
    invoke-static {p7}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 930
    .end local v0    # "i":I
    .end local v1    # "prufbyte":B
    :goto_37
    return v2

    .line 926
    .restart local v0    # "i":I
    .restart local v1    # "prufbyte":B
    :cond_38
    invoke-virtual {p0}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v1

    goto :goto_18

    .line 928
    :cond_3d
    add-int/lit8 v2, p1, 0x1

    invoke-virtual {p0, v2}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .end local v0    # "i":I
    .end local v1    # "prufbyte":B
    :cond_42
    move v2, v3

    .line 930
    goto :goto_37
.end method

.method private static final calcChecksum([BI)V
    .registers 7
    .param p0, "paramArrayOfByte"    # [B
    .param p1, "j"    # I

    .prologue
    .line 846
    new-instance v1, Ljava/util/zip/Adler32;

    invoke-direct {v1}, Ljava/util/zip/Adler32;-><init>()V

    .line 847
    .local v1, "localAdler32":Ljava/util/zip/Adler32;
    const/16 v2, 0xc

    array-length v3, p0

    add-int/lit8 v4, p1, 0xc

    sub-int/2addr v3, v4

    invoke-virtual {v1, p0, v2, v3}, Ljava/util/zip/Adler32;->update([BII)V

    .line 848
    invoke-virtual {v1}, Ljava/util/zip/Adler32;->getValue()J

    move-result-wide v2

    long-to-int v0, v2

    .line 849
    .local v0, "i":I
    add-int/lit8 v2, p1, 0x8

    int-to-byte v3, v0

    aput-byte v3, p0, v2

    .line 850
    add-int/lit8 v2, p1, 0x9

    shr-int/lit8 v3, v0, 0x8

    int-to-byte v3, v3

    aput-byte v3, p0, v2

    .line 851
    add-int/lit8 v2, p1, 0xa

    shr-int/lit8 v3, v0, 0x10

    int-to-byte v3, v3

    aput-byte v3, p0, v2

    .line 852
    add-int/lit8 v2, p1, 0xb

    shr-int/lit8 v3, v0, 0x18

    int-to-byte v3, v3

    aput-byte v3, p0, v2

    .line 853
    return-void
.end method

.method private static final calcSignature([BI)V
    .registers 10
    .param p0, "paramArrayOfByte"    # [B
    .param p1, "j"    # I

    .prologue
    const/16 v7, 0x14

    .line 859
    :try_start_2
    const-string v4, "SHA-1"

    invoke-static {v4}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_7
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_2 .. :try_end_7} :catch_41

    move-result-object v2

    .line 866
    .local v2, "localMessageDigest":Ljava/security/MessageDigest;
    const/16 v4, 0x20

    array-length v5, p0

    add-int/lit8 v6, p1, 0x20

    sub-int/2addr v5, v6

    invoke-virtual {v2, p0, v4, v5}, Ljava/security/MessageDigest;->update([BII)V

    .line 869
    add-int/lit8 v4, p1, 0xc

    const/16 v5, 0x14

    :try_start_15
    invoke-virtual {v2, p0, v4, v5}, Ljava/security/MessageDigest;->digest([BII)I

    move-result v0

    .line 870
    .local v0, "i":I
    if-eq v0, v7, :cond_48

    .line 871
    new-instance v4, Ljava/lang/RuntimeException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "unexpected digest write:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "bytes"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_3a
    .catch Ljava/security/DigestException; {:try_start_15 .. :try_end_3a} :catch_3a

    .line 873
    .end local v0    # "i":I
    :catch_3a
    move-exception v1

    .line 875
    .local v1, "localDigestException":Ljava/security/DigestException;
    new-instance v4, Ljava/lang/RuntimeException;

    invoke-direct {v4, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v4

    .line 861
    .end local v1    # "localDigestException":Ljava/security/DigestException;
    .end local v2    # "localMessageDigest":Ljava/security/MessageDigest;
    :catch_41
    move-exception v3

    .line 863
    .local v3, "localNoSuchAlgorithmException":Ljava/security/NoSuchAlgorithmException;
    new-instance v4, Ljava/lang/RuntimeException;

    invoke-direct {v4, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v4

    .line 877
    .end local v3    # "localNoSuchAlgorithmException":Ljava/security/NoSuchAlgorithmException;
    .restart local v0    # "i":I
    .restart local v2    # "localMessageDigest":Ljava/security/MessageDigest;
    :cond_48
    return-void
.end method

.method public static clearTemp()V
    .registers 6

    .prologue
    .line 882
    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/chelpus/root/utils/runpatchsupport;->dir:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/AndroidManifest.xml"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 883
    .local v3, "tmp":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 884
    .local v2, "tempdex":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_23

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 885
    :cond_23
    sget-object v4, Lcom/chelpus/root/utils/runpatchsupport;->classesFiles:Ljava/util/ArrayList;

    if-eqz v4, :cond_67

    sget-object v4, Lcom/chelpus/root/utils/runpatchsupport;->classesFiles:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_67

    .line 886
    sget-object v4, Lcom/chelpus/root/utils/runpatchsupport;->classesFiles:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_35
    :goto_35
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_67

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 887
    .local v0, "cl":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_35

    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_4a
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_4a} :catch_4b

    goto :goto_35

    .line 896
    .end local v0    # "cl":Ljava/io/File;
    .end local v2    # "tempdex":Ljava/io/File;
    :catch_4b
    move-exception v1

    .line 898
    .local v1, "e":Ljava/lang/Exception;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 900
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_66
    :goto_66
    return-void

    .line 890
    .restart local v2    # "tempdex":Ljava/io/File;
    :cond_67
    :try_start_67
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/chelpus/root/utils/runpatchsupport;->dir:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/classes.dex"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 891
    new-instance v2, Ljava/io/File;

    .end local v2    # "tempdex":Ljava/io/File;
    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 892
    .restart local v2    # "tempdex":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_8a

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 893
    :cond_8a
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/chelpus/root/utils/runpatchsupport;->dir:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/classes.dex.apk"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 894
    new-instance v2, Ljava/io/File;

    .end local v2    # "tempdex":Ljava/io/File;
    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 895
    .restart local v2    # "tempdex":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_66

    invoke-virtual {v2}, Ljava/io/File;->delete()Z
    :try_end_ad
    .catch Ljava/lang/Exception; {:try_start_67 .. :try_end_ad} :catch_4b

    goto :goto_66
.end method

.method public static clearTempSD()V
    .registers 5

    .prologue
    .line 1094
    :try_start_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/chelpus/root/utils/runpatchsupport;->sddir:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/Modified/classes.dex.apk"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1095
    .local v2, "tmp":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1096
    .local v1, "tempdex":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_23

    invoke-virtual {v1}, Ljava/io/File;->delete()Z
    :try_end_23
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_23} :catch_24

    .line 1101
    .end local v1    # "tempdex":Ljava/io/File;
    :cond_23
    :goto_23
    return-void

    .line 1097
    :catch_24
    move-exception v0

    .line 1099
    .local v0, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    goto :goto_23
.end method

.method public static fixadler(Ljava/io/File;)V
    .registers 6
    .param p0, "destFile"    # Ljava/io/File;

    .prologue
    .line 828
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 829
    .local v2, "localFileInputStream":Ljava/io/FileInputStream;
    invoke-virtual {v2}, Ljava/io/FileInputStream;->available()I

    move-result v4

    new-array v0, v4, [B

    .line 830
    .local v0, "arrayOfByte":[B
    invoke-virtual {v2, v0}, Ljava/io/FileInputStream;->read([B)I

    .line 831
    const/4 v4, 0x0

    invoke-static {v0, v4}, Lcom/chelpus/root/utils/runpatchsupport;->calcSignature([BI)V

    .line 832
    const/4 v4, 0x0

    invoke-static {v0, v4}, Lcom/chelpus/root/utils/runpatchsupport;->calcChecksum([BI)V

    .line 833
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V

    .line 835
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, p0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 836
    .local v3, "localFileOutputStream":Ljava/io/FileOutputStream;
    invoke-virtual {v3, v0}, Ljava/io/FileOutputStream;->write([B)V

    .line 837
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_24
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_24} :catch_25

    .line 843
    .end local v0    # "arrayOfByte":[B
    .end local v2    # "localFileInputStream":Ljava/io/FileInputStream;
    .end local v3    # "localFileOutputStream":Ljava/io/FileOutputStream;
    :goto_24
    return-void

    .line 839
    :catch_25
    move-exception v1

    .line 841
    .local v1, "localException":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_24
.end method

.method public static main([Ljava/lang/String;)V
    .registers 71
    .param p0, "paramArrayOfString"    # [Ljava/lang/String;

    .prologue
    .line 60
    new-instance v55, Lcom/android/vending/billing/InAppBillingService/LUCK/LogOutputStream;

    const-string v10, "System.out"

    move-object/from16 v0, v55

    invoke-direct {v0, v10}, Lcom/android/vending/billing/InAppBillingService/LUCK/LogOutputStream;-><init>(Ljava/lang/String;)V

    .line 61
    .local v55, "pout":Lcom/android/vending/billing/InAppBillingService/LUCK/LogOutputStream;
    new-instance v10, Ljava/io/PrintStream;

    move-object/from16 v0, v55

    invoke-direct {v10, v0}, Ljava/io/PrintStream;-><init>(Ljava/io/OutputStream;)V

    sput-object v10, Lcom/chelpus/root/utils/runpatchsupport;->print:Ljava/io/PrintStream;

    .line 63
    new-instance v10, Lcom/chelpus/root/utils/runpatchsupport$1;

    invoke-direct {v10}, Lcom/chelpus/root/utils/runpatchsupport$1;-><init>()V

    invoke-static {v10}, Lcom/chelpus/Utils;->startRootJava(Ljava/lang/Object;)V

    .line 65
    const/4 v10, 0x0

    aget-object v10, p0, v10

    invoke-static {v10}, Lcom/chelpus/Utils;->kill(Ljava/lang/String;)V

    .line 66
    sget-object v10, Lcom/chelpus/root/utils/runpatchsupport;->print:Ljava/io/PrintStream;

    const-string v11, "Support-Code Running!"

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 67
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 69
    .local v2, "patchesList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;>;"
    const/4 v10, 0x1

    sput-boolean v10, Lcom/chelpus/root/utils/runpatchsupport;->pattern1:Z

    .line 70
    const/4 v10, 0x1

    sput-boolean v10, Lcom/chelpus/root/utils/runpatchsupport;->pattern2:Z

    .line 71
    const/4 v10, 0x1

    sput-boolean v10, Lcom/chelpus/root/utils/runpatchsupport;->pattern3:Z

    .line 72
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    sput-object v10, Lcom/chelpus/root/utils/runpatchsupport;->filestopatch:Ljava/util/ArrayList;

    .line 74
    :try_start_3c
    new-instance v10, Ljava/io/File;

    const/4 v11, 0x3

    aget-object v11, p0, v11

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v36

    .line 75
    .local v36, "files":[Ljava/io/File;
    move-object/from16 v0, v36

    array-length v11, v0

    const/4 v10, 0x0

    :goto_4c
    if-ge v10, v11, :cond_84

    aget-object v33, v36, v10

    .line 76
    .local v33, "file":Ljava/io/File;
    invoke-virtual/range {v33 .. v33}, Ljava/io/File;->isFile()Z

    move-result v12

    if-eqz v12, :cond_7d

    invoke-virtual/range {v33 .. v33}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v12

    const-string v13, "busybox"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_7d

    invoke-virtual/range {v33 .. v33}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v12

    const-string v13, "reboot"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_7d

    invoke-virtual/range {v33 .. v33}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v12

    const-string v13, "dalvikvm"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_7d

    invoke-virtual/range {v33 .. v33}, Ljava/io/File;->delete()Z
    :try_end_7d
    .catch Ljava/lang/Exception; {:try_start_3c .. :try_end_7d} :catch_80

    .line 75
    :cond_7d
    add-int/lit8 v10, v10, 0x1

    goto :goto_4c

    .line 78
    .end local v33    # "file":Ljava/io/File;
    .end local v36    # "files":[Ljava/io/File;
    :catch_80
    move-exception v28

    .local v28, "e":Ljava/lang/Exception;
    invoke-virtual/range {v28 .. v28}, Ljava/lang/Exception;->printStackTrace()V

    .line 80
    .end local v28    # "e":Ljava/lang/Exception;
    :cond_84
    const/4 v10, 0x1

    :try_start_85
    aget-object v10, p0, v10

    const-string v11, "pattern0"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_92

    const/4 v10, 0x0

    sput-boolean v10, Lcom/chelpus/root/utils/runpatchsupport;->pattern1:Z

    .line 81
    :cond_92
    const/4 v10, 0x1

    aget-object v10, p0, v10

    const-string v11, "pattern1"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_a0

    const/4 v10, 0x0

    sput-boolean v10, Lcom/chelpus/root/utils/runpatchsupport;->pattern2:Z

    .line 82
    :cond_a0
    const/4 v10, 0x1

    aget-object v10, p0, v10

    const-string v11, "pattern2"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_ae

    const/4 v10, 0x0

    sput-boolean v10, Lcom/chelpus/root/utils/runpatchsupport;->pattern3:Z

    .line 83
    :cond_ae
    const/4 v10, 0x6

    aget-object v10, p0, v10

    if-eqz v10, :cond_c1

    const/4 v10, 0x6

    aget-object v10, p0, v10

    const-string v11, "createAPK"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_c1

    const/4 v10, 0x1

    sput-boolean v10, Lcom/chelpus/root/utils/runpatchsupport;->createAPK:Z

    .line 84
    :cond_c1
    const/4 v10, 0x6

    aget-object v10, p0, v10

    if-eqz v10, :cond_d4

    const/4 v10, 0x6

    aget-object v10, p0, v10

    const-string v11, "ART"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_d4

    const/4 v10, 0x1

    sput-boolean v10, Lcom/chelpus/root/utils/runpatchsupport;->ART:Z

    .line 85
    :cond_d4
    const/4 v10, 0x6

    aget-object v10, p0, v10

    if-eqz v10, :cond_df

    const/4 v10, 0x6

    aget-object v10, p0, v10

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 86
    :cond_df
    const/4 v10, 0x7

    aget-object v10, p0, v10

    if-eqz v10, :cond_e9

    const/4 v10, 0x7

    aget-object v10, p0, v10

    sput-object v10, Lcom/chelpus/root/utils/runpatchsupport;->uid:Ljava/lang/String;

    .line 87
    :cond_e9
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "uid:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget-object v12, Lcom/chelpus/root/utils/runpatchsupport;->uid:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_103
    .catch Ljava/lang/NullPointerException; {:try_start_85 .. :try_end_103} :catch_1161
    .catch Ljava/lang/Exception; {:try_start_85 .. :try_end_103} :catch_115e

    .line 91
    :goto_103
    const/4 v10, 0x5

    :try_start_104
    aget-object v10, p0, v10

    const-string v11, "copyDC"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_111

    const/4 v10, 0x1

    sput-boolean v10, Lcom/chelpus/root/utils/runpatchsupport;->copyDC:Z
    :try_end_111
    .catch Ljava/lang/NullPointerException; {:try_start_104 .. :try_end_111} :catch_115b
    .catch Ljava/lang/Exception; {:try_start_104 .. :try_end_111} :catch_1158

    .line 94
    :cond_111
    :goto_111
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchsupport;->createAPK:Z

    if-eqz v10, :cond_11c

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    sput-object v10, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->startUnderRoot:Ljava/lang/Boolean;

    .line 96
    :cond_11c
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 97
    .local v3, "origStr":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 98
    .local v4, "replStr":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 99
    .local v5, "trigger":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Boolean;>;"
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 100
    .local v6, "ResultText":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 101
    .local v7, "markers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 103
    .local v8, "dontConvert":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Boolean;>;"
    const-string v10, "1A ?? FF FF"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 104
    const-string v10, "1A ?? ?? ??"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 105
    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 106
    const-string v10, "(pak intekekt 0)"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 107
    const-string v10, "search_pack"

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 109
    const-string v10, "1B ?? FF FF FF FF"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 110
    const-string v10, "1B ?? ?? ?? ?? ??"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 111
    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 112
    const-string v10, "(pak intekekt 0)"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 113
    const-string v10, "search_pack"

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 115
    const-string v10, "1A ?? FF FF"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 116
    const-string v10, "1A ?? ?? ??"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 117
    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 118
    const-string v10, "(sha intekekt 2)"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 119
    const-string v10, "search"

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 121
    const-string v10, "1B ?? FF FF FF FF"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 122
    const-string v10, "1B ?? ?? ?? ?? ??"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 123
    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 124
    const-string v10, "(sha intekekt 2 32 bit)"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 125
    const-string v10, "search"

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 127
    const-string v10, "0A ?? 39 ?? ?? 00"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 128
    const-string v10, "12 S1 39 ?? ?? 00"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 129
    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 130
    const-string v10, "support2 Fixed!\n(sha intekekt 3)"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 131
    const-string v10, "search"

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 133
    const-string v10, "1A ?? FF FF"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 134
    const-string v10, "1A ?? FF FF"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 135
    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 136
    const-string v10, "support1 Fixed!\n(pak intekekt)"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 137
    const-string v10, "search_pack"

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 139
    const-string v10, "1B ?? FF FF FF FF"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 140
    const-string v10, "1B ?? FF FF FF FF"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 141
    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 142
    const-string v10, "support1 Fixed!\n(pak intekekt 32 bit)"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 143
    const-string v10, "search_pack"

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 147
    const-string v10, "6E 20 FF FF ?? 00 0A ??"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 148
    const-string v10, "6E 20 ?? ?? ?? 00 12 S1"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 149
    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 150
    const-string v10, "support2 Fixed!\n(sha intekekt 4)"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 151
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 156
    const-string v10, "6E 20 FF FF ?? 00"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 157
    const-string v10, "00 00 00 00 00 00"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 158
    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 159
    const-string v10, "support3 Fixed!\n(intent for free)"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 160
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 164
    const-string v10, "0A ?? 39 ?? ?? ??"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 165
    const-string v10, "12 S1 39 ?? ?? ??"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 166
    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 167
    const-string v10, "support1 Fixed!\n(ev1)"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 168
    const-string v10, "search_sign_ver"

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 170
    const-string v10, "0A ?? 38 ?? ?? ??"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 171
    const-string v10, "12 S1 38 ?? ?? ??"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 172
    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 173
    const-string v10, "support1 Fixed!\n(ev1)"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 174
    const-string v10, "search_sign_ver"

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 180
    const-string v10, "1C ?? FF FF"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 181
    const-string v10, "1C ?? ?? ??"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 182
    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 183
    const-string v10, "support1 Fixed!\n(si)"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 184
    const-string v10, "search_sign_ver"

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 203
    const-string v10, "23 ?? ?? ?? 1C ?? ?? ?? 12 ?? 4D ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0C ?? 1F ?? ?? ?? 6E ?? ?? ?? ?? ?? 0A ?? 39 ?? ?? ?? 1A ?? ?? ?? 1A"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 204
    const-string v10, "23 ?? ?? ?? 1C ?? ?? ?? 12 ?? 4D ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0C ?? 1F ?? ?? ?? 6E ?? ?? ?? ?? ?? 12 S1 39 ?? ?? ?? 1A ?? ?? ?? 1A"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 205
    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 206
    const-string v10, "support3 Fixed!\n(s)"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 207
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 237
    const/16 v20, 0x0

    .line 239
    .local v20, "conv":Z
    invoke-static/range {v20 .. v20}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-static/range {v2 .. v9}, Lcom/chelpus/Utils;->convertToPatchItemAuto(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/Boolean;)V

    .line 243
    :try_start_317
    sget-object v10, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->startUnderRoot:Ljava/lang/Boolean;

    invoke-virtual {v10}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    if-eqz v10, :cond_327

    const/4 v10, 0x2

    aget-object v10, p0, v10

    const-string v11, "RW"

    invoke-static {v10, v11}, Lcom/chelpus/Utils;->remount(Ljava/lang/String;Ljava/lang/String;)Z

    .line 244
    :cond_327
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchsupport;->createAPK:Z

    if-nez v10, :cond_543

    sget-boolean v10, Lcom/chelpus/root/utils/runpatchsupport;->ART:Z

    if-nez v10, :cond_543

    .line 246
    const/4 v10, 0x3

    aget-object v10, p0, v10

    sput-object v10, Lcom/chelpus/root/utils/runpatchsupport;->dir:Ljava/lang/String;

    .line 247
    const/4 v10, 0x2

    aget-object v10, p0, v10

    sput-object v10, Lcom/chelpus/root/utils/runpatchsupport;->dirapp:Ljava/lang/String;

    .line 248
    invoke-static {}, Lcom/chelpus/root/utils/runpatchsupport;->clearTemp()V

    .line 249
    const/4 v10, 0x4

    aget-object v10, p0, v10

    const-string v11, "not_system"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_34a

    const/4 v10, 0x0

    sput-boolean v10, Lcom/chelpus/root/utils/runpatchsupport;->system:Z

    .line 250
    :cond_34a
    const/4 v10, 0x4

    aget-object v10, p0, v10

    const-string v11, "system"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_358

    const/4 v10, 0x1

    sput-boolean v10, Lcom/chelpus/root/utils/runpatchsupport;->system:Z

    .line 251
    :cond_358
    sget-object v10, Lcom/chelpus/root/utils/runpatchsupport;->filestopatch:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->clear()V

    .line 252
    const-string v10, "CLASSES mode create odex enabled."

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 253
    const/4 v10, 0x0

    aget-object v49, p0, v10

    .line 254
    .local v49, "packageName":Ljava/lang/String;
    const/4 v10, 0x2

    aget-object v10, p0, v10

    sput-object v10, Lcom/chelpus/root/utils/runpatchsupport;->appdir:Ljava/lang/String;

    .line 255
    const/4 v10, 0x3

    aget-object v10, p0, v10

    sput-object v10, Lcom/chelpus/root/utils/runpatchsupport;->sddir:Ljava/lang/String;

    .line 256
    invoke-static {}, Lcom/chelpus/root/utils/runpatchsupport;->clearTempSD()V

    .line 257
    new-instance v16, Ljava/io/File;

    sget-object v10, Lcom/chelpus/root/utils/runpatchsupport;->appdir:Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-direct {v0, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 258
    .local v16, "apk":Ljava/io/File;
    const-string v10, "Get classes.dex."

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 259
    sget-object v10, Lcom/chelpus/root/utils/runpatchsupport;->print:Ljava/io/PrintStream;

    const-string v11, "Get classes.dex."

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 260
    invoke-static/range {v16 .. v16}, Lcom/chelpus/root/utils/runpatchsupport;->unzipART(Ljava/io/File;)V

    .line 261
    sget-object v10, Lcom/chelpus/root/utils/runpatchsupport;->classesFiles:Ljava/util/ArrayList;

    if-eqz v10, :cond_396

    sget-object v10, Lcom/chelpus/root/utils/runpatchsupport;->classesFiles:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-nez v10, :cond_3bb

    .line 262
    :cond_396
    new-instance v10, Ljava/io/FileNotFoundException;

    invoke-direct {v10}, Ljava/io/FileNotFoundException;-><init>()V

    throw v10
    :try_end_39c
    .catch Ljava/io/FileNotFoundException; {:try_start_317 .. :try_end_39c} :catch_39c
    .catch Ljava/lang/Exception; {:try_start_317 .. :try_end_39c} :catch_3de

    .line 798
    .end local v16    # "apk":Ljava/io/File;
    .end local v49    # "packageName":Ljava/lang/String;
    :catch_39c
    move-exception v43

    .line 799
    .local v43, "localFileNotFoundException":Ljava/io/FileNotFoundException;
    const-string v10, "Error: Program files are not found!\n\nCheck the location dalvik-cache to solve problems!\n\nDefault: /data/dalvik-cache/*"

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 805
    .end local v43    # "localFileNotFoundException":Ljava/io/FileNotFoundException;
    :cond_3a2
    :goto_3a2
    sget-object v10, Lcom/chelpus/root/utils/runpatchsupport;->filestopatch:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_3a8
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_10ef

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v35

    check-cast v35, Ljava/io/File;

    .line 806
    .local v35, "filepatch":Ljava/io/File;
    invoke-static/range {v35 .. v35}, Lcom/chelpus/Utils;->fixadler(Ljava/io/File;)V

    .line 807
    invoke-static {}, Lcom/chelpus/root/utils/runpatchsupport;->clearTempSD()V

    goto :goto_3a8

    .line 264
    .end local v35    # "filepatch":Ljava/io/File;
    .restart local v16    # "apk":Ljava/io/File;
    .restart local v49    # "packageName":Ljava/lang/String;
    :cond_3bb
    :try_start_3bb
    sget-object v10, Lcom/chelpus/root/utils/runpatchsupport;->filestopatch:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->clear()V

    .line 265
    sget-object v10, Lcom/chelpus/root/utils/runpatchsupport;->classesFiles:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_3c6
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_402

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/io/File;

    .line 266
    .local v18, "cl":Ljava/io/File;
    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->exists()Z

    move-result v11

    if-nez v11, :cond_3fa

    new-instance v10, Ljava/io/FileNotFoundException;

    invoke-direct {v10}, Ljava/io/FileNotFoundException;-><init>()V

    throw v10
    :try_end_3de
    .catch Ljava/io/FileNotFoundException; {:try_start_3bb .. :try_end_3de} :catch_39c
    .catch Ljava/lang/Exception; {:try_start_3bb .. :try_end_3de} :catch_3de

    .line 801
    .end local v16    # "apk":Ljava/io/File;
    .end local v18    # "cl":Ljava/io/File;
    .end local v49    # "packageName":Ljava/lang/String;
    :catch_3de
    move-exception v28

    .line 802
    .restart local v28    # "e":Ljava/lang/Exception;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Patch Process Exception: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    goto :goto_3a2

    .line 267
    .end local v28    # "e":Ljava/lang/Exception;
    .restart local v16    # "apk":Ljava/io/File;
    .restart local v18    # "cl":Ljava/io/File;
    .restart local v49    # "packageName":Ljava/lang/String;
    :cond_3fa
    :try_start_3fa
    sget-object v11, Lcom/chelpus/root/utils/runpatchsupport;->filestopatch:Ljava/util/ArrayList;

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3c6

    .line 271
    .end local v18    # "cl":Ljava/io/File;
    :cond_402
    const/4 v10, 0x2

    aget-object v10, p0, v10

    const/4 v11, 0x1

    invoke-static {v10, v11}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v46

    .line 272
    .local v46, "odexstr":Ljava/lang/String;
    new-instance v45, Ljava/io/File;

    invoke-direct/range {v45 .. v46}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 273
    .local v45, "odexfile":Ljava/io/File;
    invoke-virtual/range {v45 .. v45}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_418

    invoke-virtual/range {v45 .. v45}, Ljava/io/File;->delete()Z

    .line 274
    :cond_418
    new-instance v45, Ljava/io/File;

    .end local v45    # "odexfile":Ljava/io/File;
    const-string v10, "-1"

    const-string v11, "-2"

    move-object/from16 v0, v46

    invoke-virtual {v0, v10, v11}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, v45

    invoke-direct {v0, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 275
    .restart local v45    # "odexfile":Ljava/io/File;
    invoke-virtual/range {v45 .. v45}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_432

    invoke-virtual/range {v45 .. v45}, Ljava/io/File;->delete()Z

    .line 276
    :cond_432
    new-instance v45, Ljava/io/File;

    .end local v45    # "odexfile":Ljava/io/File;
    const-string v10, "-2"

    const-string v11, "-1"

    move-object/from16 v0, v46

    invoke-virtual {v0, v10, v11}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, v45

    invoke-direct {v0, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 277
    .restart local v45    # "odexfile":Ljava/io/File;
    invoke-virtual/range {v45 .. v45}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_44c

    invoke-virtual/range {v45 .. v45}, Ljava/io/File;->delete()Z

    .line 338
    .end local v16    # "apk":Ljava/io/File;
    .end local v45    # "odexfile":Ljava/io/File;
    .end local v46    # "odexstr":Ljava/lang/String;
    .end local v49    # "packageName":Ljava/lang/String;
    :cond_44c
    sget-object v10, Lcom/chelpus/root/utils/runpatchsupport;->filestopatch:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v67

    :goto_452
    invoke-interface/range {v67 .. v67}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_3a2

    invoke-interface/range {v67 .. v67}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v35

    check-cast v35, Ljava/io/File;

    .line 339
    .restart local v35    # "filepatch":Ljava/io/File;
    const-string v10, "Find string id."

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 342
    new-instance v62, Ljava/util/ArrayList;

    invoke-direct/range {v62 .. v62}, Ljava/util/ArrayList;-><init>()V

    .line 343
    .local v62, "strings":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v10, "com.android.vending"

    move-object/from16 v0, v62

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 344
    const-string v10, "SHA1withRSA"

    move-object/from16 v0, v62

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 345
    const-string v10, "com.android.vending.billing.InAppBillingService.BIND"

    move-object/from16 v0, v62

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 346
    const-string v10, "Ljava/security/Signature;"

    move-object/from16 v0, v62

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 347
    const-string v10, "verify"

    move-object/from16 v0, v62

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 348
    const-string v10, "Landroid/content/Intent;"

    move-object/from16 v0, v62

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 349
    const-string v10, "setPackage"

    move-object/from16 v0, v62

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 350
    const-string v10, "engineVerify"

    move-object/from16 v0, v62

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 351
    const-string v10, "Ljava/security/SignatureSpi;"

    move-object/from16 v0, v62

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 355
    const-string v10, "String analysis."

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 356
    sget-object v10, Lcom/chelpus/root/utils/runpatchsupport;->print:Ljava/io/PrintStream;

    const-string v11, "String analysis."

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 357
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchsupport;->copyDC:Z

    if-nez v10, :cond_66c

    invoke-virtual/range {v35 .. v35}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    move-object/from16 v0, v62

    invoke-static {v10, v0, v11}, Lcom/chelpus/Utils;->getStringIds(Ljava/lang/String;Ljava/util/ArrayList;Z)Ljava/util/ArrayList;

    move-result-object v48

    .line 360
    .local v48, "offsets":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;>;"
    :goto_4c2
    const/16 v30, 0x0

    .local v30, "f1":Z
    const/16 v31, 0x0

    .local v31, "f2":Z
    const/16 v32, 0x0

    .local v32, "f3":Z
    const/16 v29, 0x0

    .local v29, "ev":Z
    const/16 v58, 0x0

    .line 361
    .local v58, "spi":Z
    const/16 v47, 0x1

    .line 362
    .local v47, "of_to_patch":I
    new-instance v65, Ljava/util/ArrayList;

    invoke-direct/range {v65 .. v65}, Ljava/util/ArrayList;-><init>()V

    .line 363
    .local v65, "types":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/TypesItem;>;"
    new-instance v10, Lcom/android/vending/billing/InAppBillingService/LUCK/TypesItem;

    const-string v11, "Ljava/security/Signature;"

    invoke-direct {v10, v11}, Lcom/android/vending/billing/InAppBillingService/LUCK/TypesItem;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v65

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 364
    new-instance v10, Lcom/android/vending/billing/InAppBillingService/LUCK/TypesItem;

    const-string v11, "Ljava/security/SignatureSpi;"

    invoke-direct {v10, v11}, Lcom/android/vending/billing/InAppBillingService/LUCK/TypesItem;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v65

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 365
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 366
    .local v19, "commands":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;>;"
    new-instance v10, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;

    const-string v11, "Ljava/security/Signature;"

    const-string v12, "verify"

    invoke-direct {v10, v11, v12}, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 367
    new-instance v10, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;

    const-string v11, "Landroid/content/Intent;"

    const-string v12, "setPackage"

    invoke-direct {v10, v11, v12}, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 368
    invoke-virtual/range {v48 .. v48}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_510
    :goto_510
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_926

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;

    .line 370
    .local v39, "it":Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;
    invoke-virtual/range {v65 .. v65}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_520
    :goto_520
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_679

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v40

    check-cast v40, Lcom/android/vending/billing/InAppBillingService/LUCK/TypesItem;

    .line 371
    .local v40, "item":Lcom/android/vending/billing/InAppBillingService/LUCK/TypesItem;
    move-object/from16 v0, v40

    iget-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/TypesItem;->type:Ljava/lang/String;

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->str:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_520

    .line 372
    move-object/from16 v0, v39

    iget-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    move-object/from16 v0, v40

    iput-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/TypesItem;->Type:[B

    goto :goto_520

    .line 281
    .end local v19    # "commands":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;>;"
    .end local v29    # "ev":Z
    .end local v30    # "f1":Z
    .end local v31    # "f2":Z
    .end local v32    # "f3":Z
    .end local v35    # "filepatch":Ljava/io/File;
    .end local v39    # "it":Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;
    .end local v40    # "item":Lcom/android/vending/billing/InAppBillingService/LUCK/TypesItem;
    .end local v47    # "of_to_patch":I
    .end local v48    # "offsets":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;>;"
    .end local v58    # "spi":Z
    .end local v62    # "strings":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v65    # "types":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/TypesItem;>;"
    :cond_543
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchsupport;->createAPK:Z

    if-eqz v10, :cond_60a

    .line 282
    const/4 v10, 0x0

    aget-object v49, p0, v10

    .line 283
    .restart local v49    # "packageName":Ljava/lang/String;
    const/4 v10, 0x2

    aget-object v10, p0, v10

    sput-object v10, Lcom/chelpus/root/utils/runpatchsupport;->appdir:Ljava/lang/String;

    .line 284
    const/4 v10, 0x5

    aget-object v10, p0, v10

    sput-object v10, Lcom/chelpus/root/utils/runpatchsupport;->sddir:Ljava/lang/String;

    .line 286
    invoke-static {}, Lcom/chelpus/root/utils/runpatchsupport;->clearTempSD()V

    .line 287
    new-instance v16, Ljava/io/File;

    sget-object v10, Lcom/chelpus/root/utils/runpatchsupport;->appdir:Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-direct {v0, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 288
    .restart local v16    # "apk":Ljava/io/File;
    invoke-static/range {v16 .. v16}, Lcom/chelpus/root/utils/runpatchsupport;->unzipSD(Ljava/io/File;)V

    .line 289
    new-instance v10, Ljava/io/File;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v12, Lcom/chelpus/root/utils/runpatchsupport;->sddir:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/Modified/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, v49

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ".apk"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v10, Lcom/chelpus/root/utils/runpatchsupport;->crkapk:Ljava/io/File;

    .line 290
    sget-object v10, Lcom/chelpus/root/utils/runpatchsupport;->crkapk:Ljava/io/File;

    move-object/from16 v0, v16

    invoke-static {v0, v10}, Lcom/chelpus/Utils;->copyFile(Ljava/io/File;Ljava/io/File;)V

    .line 292
    new-instance v15, Ljava/io/File;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v11, Lcom/chelpus/root/utils/runpatchsupport;->sddir:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "/Modified/AndroidManifest.xml"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v15, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 293
    .local v15, "androidManifest":Ljava/io/File;
    invoke-virtual {v15}, Ljava/io/File;->exists()Z

    move-result v10

    if-nez v10, :cond_5b8

    new-instance v10, Ljava/io/FileNotFoundException;

    invoke-direct {v10}, Ljava/io/FileNotFoundException;-><init>()V

    throw v10
    :try_end_5b8
    .catch Ljava/io/FileNotFoundException; {:try_start_3fa .. :try_end_5b8} :catch_39c
    .catch Ljava/lang/Exception; {:try_start_3fa .. :try_end_5b8} :catch_3de

    .line 296
    :cond_5b8
    :try_start_5b8
    new-instance v10, Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample;

    invoke-direct {v10}, Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample;-><init>()V

    const-string v11, "19"

    invoke-virtual {v10, v15, v11}, Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample;->changeTargetApi(Ljava/io/File;Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_5c8

    .line 297
    invoke-virtual {v15}, Ljava/io/File;->delete()Z
    :try_end_5c8
    .catch Ljava/lang/Exception; {:try_start_5b8 .. :try_end_5c8} :catch_5da
    .catch Ljava/io/FileNotFoundException; {:try_start_5b8 .. :try_end_5c8} :catch_39c

    .line 301
    :cond_5c8
    :goto_5c8
    :try_start_5c8
    sget-object v10, Lcom/chelpus/root/utils/runpatchsupport;->classesFiles:Ljava/util/ArrayList;

    if-eqz v10, :cond_5d4

    sget-object v10, Lcom/chelpus/root/utils/runpatchsupport;->classesFiles:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-nez v10, :cond_5df

    .line 302
    :cond_5d4
    new-instance v10, Ljava/io/FileNotFoundException;

    invoke-direct {v10}, Ljava/io/FileNotFoundException;-><init>()V

    throw v10

    .line 299
    :catch_5da
    move-exception v28

    .restart local v28    # "e":Ljava/lang/Exception;
    invoke-virtual/range {v28 .. v28}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_5c8

    .line 304
    .end local v28    # "e":Ljava/lang/Exception;
    :cond_5df
    sget-object v10, Lcom/chelpus/root/utils/runpatchsupport;->filestopatch:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->clear()V

    .line 305
    sget-object v10, Lcom/chelpus/root/utils/runpatchsupport;->classesFiles:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_5ea
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_60a

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/io/File;

    .line 306
    .restart local v18    # "cl":Ljava/io/File;
    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->exists()Z

    move-result v11

    if-nez v11, :cond_602

    new-instance v10, Ljava/io/FileNotFoundException;

    invoke-direct {v10}, Ljava/io/FileNotFoundException;-><init>()V

    throw v10

    .line 307
    :cond_602
    sget-object v11, Lcom/chelpus/root/utils/runpatchsupport;->filestopatch:Ljava/util/ArrayList;

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5ea

    .line 311
    .end local v15    # "androidManifest":Ljava/io/File;
    .end local v16    # "apk":Ljava/io/File;
    .end local v18    # "cl":Ljava/io/File;
    .end local v49    # "packageName":Ljava/lang/String;
    :cond_60a
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchsupport;->ART:Z

    if-eqz v10, :cond_44c

    .line 312
    const-string v10, "ART mode create dex enabled."

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 313
    const/4 v10, 0x0

    aget-object v49, p0, v10

    .line 314
    .restart local v49    # "packageName":Ljava/lang/String;
    const/4 v10, 0x2

    aget-object v10, p0, v10

    sput-object v10, Lcom/chelpus/root/utils/runpatchsupport;->appdir:Ljava/lang/String;

    .line 315
    const/4 v10, 0x3

    aget-object v10, p0, v10

    sput-object v10, Lcom/chelpus/root/utils/runpatchsupport;->sddir:Ljava/lang/String;

    .line 317
    invoke-static {}, Lcom/chelpus/root/utils/runpatchsupport;->clearTempSD()V

    .line 318
    new-instance v16, Ljava/io/File;

    sget-object v10, Lcom/chelpus/root/utils/runpatchsupport;->appdir:Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-direct {v0, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 319
    .restart local v16    # "apk":Ljava/io/File;
    invoke-static/range {v16 .. v16}, Lcom/chelpus/root/utils/runpatchsupport;->unzipART(Ljava/io/File;)V

    .line 322
    sget-object v10, Lcom/chelpus/root/utils/runpatchsupport;->classesFiles:Ljava/util/ArrayList;

    if-eqz v10, :cond_63b

    sget-object v10, Lcom/chelpus/root/utils/runpatchsupport;->classesFiles:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-nez v10, :cond_641

    .line 323
    :cond_63b
    new-instance v10, Ljava/io/FileNotFoundException;

    invoke-direct {v10}, Ljava/io/FileNotFoundException;-><init>()V

    throw v10

    .line 325
    :cond_641
    sget-object v10, Lcom/chelpus/root/utils/runpatchsupport;->filestopatch:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->clear()V

    .line 326
    sget-object v10, Lcom/chelpus/root/utils/runpatchsupport;->classesFiles:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_64c
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_44c

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/io/File;

    .line 327
    .restart local v18    # "cl":Ljava/io/File;
    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->exists()Z

    move-result v11

    if-nez v11, :cond_664

    new-instance v10, Ljava/io/FileNotFoundException;

    invoke-direct {v10}, Ljava/io/FileNotFoundException;-><init>()V

    throw v10

    .line 328
    :cond_664
    sget-object v11, Lcom/chelpus/root/utils/runpatchsupport;->filestopatch:Ljava/util/ArrayList;

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_64c

    .line 358
    .end local v16    # "apk":Ljava/io/File;
    .end local v18    # "cl":Ljava/io/File;
    .end local v49    # "packageName":Ljava/lang/String;
    .restart local v35    # "filepatch":Ljava/io/File;
    .restart local v62    # "strings":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_66c
    invoke-virtual/range {v35 .. v35}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    move-object/from16 v0, v62

    invoke-static {v10, v0, v11}, Lcom/chelpus/Utils;->getStringIds(Ljava/lang/String;Ljava/util/ArrayList;Z)Ljava/util/ArrayList;

    move-result-object v48

    .restart local v48    # "offsets":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;>;"
    goto/16 :goto_4c2

    .line 375
    .restart local v19    # "commands":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;>;"
    .restart local v29    # "ev":Z
    .restart local v30    # "f1":Z
    .restart local v31    # "f2":Z
    .restart local v32    # "f3":Z
    .restart local v39    # "it":Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;
    .restart local v47    # "of_to_patch":I
    .restart local v58    # "spi":Z
    .restart local v65    # "types":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/TypesItem;>;"
    :cond_679
    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_67d
    :goto_67d
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_6b6

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v40

    check-cast v40, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;

    .line 376
    .local v40, "item":Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;
    move-object/from16 v0, v40

    iget-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;->object:Ljava/lang/String;

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->str:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_69f

    .line 377
    move-object/from16 v0, v39

    iget-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    move-object/from16 v0, v40

    iput-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;->Object:[B

    .line 379
    :cond_69f
    move-object/from16 v0, v40

    iget-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;->method:Ljava/lang/String;

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->str:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_67d

    .line 380
    move-object/from16 v0, v39

    iget-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    move-object/from16 v0, v40

    iput-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;->Method:[B

    goto :goto_67d

    .line 384
    .end local v40    # "item":Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;
    :cond_6b6
    move-object/from16 v0, v39

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->str:Ljava/lang/String;

    const-string v12, "com.android.vending"

    invoke-virtual {v10, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_736

    .line 387
    const/4 v10, 0x5

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x2

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x0

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 388
    const/4 v10, 0x5

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x3

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x1

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 389
    const/4 v10, 0x6

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x2

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x0

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 390
    const/4 v10, 0x6

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x3

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x1

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 391
    const/4 v10, 0x6

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x4

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x2

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 392
    const/4 v10, 0x6

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x5

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x3

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 394
    const/16 v30, 0x1

    .line 396
    :cond_736
    move-object/from16 v0, v39

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->str:Ljava/lang/String;

    const-string v12, "com.android.vending.billing.InAppBillingService.BIND"

    invoke-virtual {v10, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_888

    .line 397
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "c.a.v.b.i "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, v39

    iget-boolean v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->bits32:Z

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x0

    aget-byte v13, v13, v14

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x1

    aget-byte v13, v13, v14

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x2

    aget-byte v13, v13, v14

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x3

    aget-byte v13, v13, v14

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v12}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 399
    const/4 v10, 0x0

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x2

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x0

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 400
    const/4 v10, 0x0

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x3

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x1

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 401
    const/4 v10, 0x5

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    const/4 v12, 0x2

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x0

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 402
    const/4 v10, 0x5

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    const/4 v12, 0x3

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x1

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 407
    const/4 v10, 0x1

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x2

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x0

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 408
    const/4 v10, 0x1

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x3

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x1

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 409
    const/4 v10, 0x1

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x4

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x2

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 410
    const/4 v10, 0x1

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x5

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x3

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 411
    const/4 v10, 0x6

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    const/4 v12, 0x2

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x0

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 412
    const/4 v10, 0x6

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    const/4 v12, 0x3

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x1

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 413
    const/4 v10, 0x6

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    const/4 v12, 0x4

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x2

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 414
    const/4 v10, 0x6

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    const/4 v12, 0x5

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x3

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 416
    const/16 v31, 0x1

    .line 418
    :cond_888
    move-object/from16 v0, v39

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->str:Ljava/lang/String;

    const-string v12, "SHA1withRSA"

    invoke-virtual {v10, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_908

    .line 420
    const/4 v10, 0x2

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x2

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x0

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 421
    const/4 v10, 0x2

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x3

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x1

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 425
    const/4 v10, 0x3

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x2

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x0

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 426
    const/4 v10, 0x3

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x3

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x1

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 427
    const/4 v10, 0x3

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x4

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x2

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 428
    const/4 v10, 0x3

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x5

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x3

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 430
    const/16 v32, 0x1

    .line 432
    :cond_908
    move-object/from16 v0, v39

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->str:Ljava/lang/String;

    const-string v12, "engineVerify"

    invoke-virtual {v10, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_916

    .line 434
    const/16 v29, 0x1

    .line 436
    :cond_916
    move-object/from16 v0, v39

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->str:Ljava/lang/String;

    const-string v12, "Ljava/security/SignatureSpi;"

    invoke-virtual {v10, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_510

    .line 438
    const/16 v58, 0x1

    goto/16 :goto_510

    .line 441
    .end local v39    # "it":Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;
    :cond_926
    const/16 v51, 0x0

    .local v51, "patch_index1":I
    const/16 v52, 0x1

    .line 442
    .local v52, "patch_index2":I
    if-eqz v30, :cond_92e

    if-nez v31, :cond_956

    .line 443
    :cond_92e
    const/4 v10, 0x0

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    const/4 v11, 0x0

    iput-boolean v11, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->pattern:Z

    .line 444
    const/4 v10, 0x1

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    const/4 v11, 0x0

    iput-boolean v11, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->pattern:Z

    .line 445
    const/4 v10, 0x5

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    const/4 v11, 0x0

    iput-boolean v11, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->pattern:Z

    .line 446
    const/4 v10, 0x6

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    const/4 v11, 0x0

    iput-boolean v11, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->pattern:Z

    .line 448
    :cond_956
    if-nez v32, :cond_976

    .line 449
    const/4 v10, 0x2

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    const/4 v11, 0x0

    iput-boolean v11, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->pattern:Z

    .line 450
    const/4 v10, 0x3

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    const/4 v11, 0x0

    iput-boolean v11, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->pattern:Z

    .line 451
    const/4 v10, 0x4

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    const/4 v11, 0x0

    iput-boolean v11, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->pattern:Z

    .line 453
    :cond_976
    const/16 v44, 0x0

    .line 454
    .local v44, "new_patch_method":Z
    if-eqz v31, :cond_a63

    if-eqz v30, :cond_a63

    .line 455
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Relaced strings:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual/range {v35 .. v35}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/String;

    const/4 v13, 0x0

    const-string v14, "com.android.vending.billing.InAppBillingService.BIND"

    aput-object v14, v12, v13

    const/4 v13, 0x0

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/String;

    const/16 v68, 0x0

    const-class v69, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    .line 457
    invoke-virtual/range {v69 .. v69}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v69

    invoke-virtual/range {v69 .. v69}, Ljava/lang/Package;->getName()Ljava/lang/String;

    move-result-object v69

    aput-object v69, v14, v68

    .line 455
    invoke-static {v11, v12, v13, v14}, Lcom/chelpus/Utils;->replaceStringIds(Ljava/lang/String;[Ljava/lang/String;Z[Ljava/lang/String;)I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 458
    const/16 v44, 0x1

    .line 462
    :goto_9b6
    const-string v10, "Parse data for patch."

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 463
    sget-object v10, Lcom/chelpus/root/utils/runpatchsupport;->print:Ljava/io/PrintStream;

    const-string v11, "Parse data for patch."

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 464
    invoke-virtual/range {v35 .. v35}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    move-object/from16 v0, v19

    invoke-static {v10, v0, v11}, Lcom/chelpus/Utils;->getMethodsIds(Ljava/lang/String;Ljava/util/ArrayList;Z)Z

    .line 467
    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_9d0
    :goto_9d0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_a67

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v40

    check-cast v40, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;

    .line 468
    .restart local v40    # "item":Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;
    move-object/from16 v0, v40

    iget-boolean v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;->found_index_command:Z

    if-eqz v10, :cond_9d0

    .line 469
    move-object/from16 v0, v40

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;->object:Ljava/lang/String;

    const-string v12, "Ljava/security/Signature;"

    invoke-virtual {v10, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_a1e

    .line 470
    const/4 v10, 0x7

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x2

    move-object/from16 v0, v40

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;->index_command:[B

    const/4 v14, 0x0

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 471
    const/4 v10, 0x7

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x3

    move-object/from16 v0, v40

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;->index_command:[B

    const/4 v14, 0x1

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 472
    const/4 v10, 0x7

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    const/4 v12, 0x1

    iput-boolean v12, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->pattern:Z

    .line 474
    :cond_a1e
    move-object/from16 v0, v40

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;->object:Ljava/lang/String;

    const-string v12, "Landroid/content/Intent;"

    invoke-virtual {v10, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_9d0

    sget-boolean v10, Lcom/chelpus/root/utils/runpatchsupport;->pattern3:Z

    if-eqz v10, :cond_9d0

    .line 475
    const/16 v10, 0x8

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x2

    move-object/from16 v0, v40

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;->index_command:[B

    const/4 v14, 0x0

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 476
    const/16 v10, 0x8

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x3

    move-object/from16 v0, v40

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;->index_command:[B

    const/4 v14, 0x1

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 477
    const/16 v10, 0x8

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    const/4 v12, 0x1

    iput-boolean v12, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->pattern:Z

    goto/16 :goto_9d0

    .line 460
    .end local v40    # "item":Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;
    :cond_a63
    const/16 v44, 0x0

    goto/16 :goto_9b6

    .line 481
    :cond_a67
    if-eqz v29, :cond_acb

    if-eqz v58, :cond_acb

    .line 482
    invoke-virtual/range {v35 .. v35}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    move-object/from16 v0, v65

    invoke-static {v10, v0, v11}, Lcom/chelpus/Utils;->getTypesIds(Ljava/lang/String;Ljava/util/ArrayList;Z)Z

    .line 484
    invoke-virtual/range {v65 .. v65}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_a79
    :goto_a79
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_acb

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v40

    check-cast v40, Lcom/android/vending/billing/InAppBillingService/LUCK/TypesItem;

    .line 485
    .local v40, "item":Lcom/android/vending/billing/InAppBillingService/LUCK/TypesItem;
    move-object/from16 v0, v40

    iget-boolean v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/TypesItem;->found_id_type:Z

    if-eqz v10, :cond_a79

    .line 491
    move-object/from16 v0, v40

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/TypesItem;->type:Ljava/lang/String;

    const-string v12, "Ljava/security/SignatureSpi;"

    invoke-virtual {v10, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_a79

    .line 492
    const/16 v10, 0xb

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x2

    move-object/from16 v0, v40

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/TypesItem;->id_type:[B

    const/4 v14, 0x0

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 493
    const/16 v10, 0xb

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x3

    move-object/from16 v0, v40

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/TypesItem;->id_type:[B

    const/4 v14, 0x1

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 494
    const/16 v10, 0xb

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    const/4 v12, 0x1

    iput-boolean v12, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->pattern:Z

    goto :goto_a79

    .line 504
    .end local v40    # "item":Lcom/android/vending/billing/InAppBillingService/LUCK/TypesItem;
    :cond_acb
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v10

    new-array v0, v10, [Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    move-object/from16 v50, v0

    .line 505
    .local v50, "patchList":[Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    const/16 v66, 0x0

    .line 506
    .local v66, "u":I
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_ad9
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_aea

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v40

    check-cast v40, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    .line 507
    .local v40, "item":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    aput-object v40, v50, v66

    .line 508
    add-int/lit8 v66, v66, 0x1

    .line 509
    goto :goto_ad9

    .line 520
    .end local v40    # "item":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    :cond_aea
    const/16 v21, 0x0

    .line 529
    .local v21, "count":I
    :goto_aec
    if-lez v21, :cond_afd

    .line 530
    add-int/lit8 v21, v21, -0x1

    .line 531
    const-string v10, "Reworked inapp string!"

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 532
    sget-object v10, Lcom/chelpus/root/utils/runpatchsupport;->print:Ljava/io/PrintStream;

    const-string v11, "Reworked inapp string!"

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_aec

    .line 534
    :cond_afd
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v63

    .line 535
    .local v63, "time":J
    new-instance v10, Ljava/io/RandomAccessFile;

    const-string v11, "rw"

    move-object/from16 v0, v35

    invoke-direct {v10, v0, v11}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v9

    .line 536
    .local v9, "ChannelDex":Ljava/nio/channels/FileChannel;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Size file:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v9}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v11

    invoke-virtual {v10, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 537
    sget-object v10, Ljava/nio/channels/FileChannel$MapMode;->READ_WRITE:Ljava/nio/channels/FileChannel$MapMode;

    const-wide/16 v11, 0x0

    invoke-virtual {v9}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v13

    long-to-int v13, v13

    int-to-long v13, v13

    invoke-virtual/range {v9 .. v14}, Ljava/nio/channels/FileChannel;->map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;
    :try_end_b35
    .catch Ljava/io/FileNotFoundException; {:try_start_5c8 .. :try_end_b35} :catch_39c
    .catch Ljava/lang/Exception; {:try_start_5c8 .. :try_end_b35} :catch_3de

    move-result-object v34

    .line 538
    .local v34, "fileBytes":Ljava/nio/MappedByteBuffer;
    const/16 v23, 0x0

    .line 540
    .local v23, "curentPos":I
    const/16 v37, 0x0

    .line 543
    .local v37, "i":I
    const/16 v25, 0x5a

    .local v25, "diaposon":I
    const/16 v27, 0x3c

    .local v27, "diaposon_pak":I
    const/16 v26, 0x5a

    .line 544
    .local v26, "diaposon_ev":I
    const/16 v59, 0x0

    .local v59, "start_for_diaposon":I
    const/16 v61, 0x0

    .local v61, "start_for_diaposon_pak":I
    const/16 v60, 0x0

    .line 545
    .local v60, "start_for_diaposon_ev":I
    const/16 v54, 0x0

    .line 546
    .local v54, "period":I
    const-wide/16 v41, 0x0

    .local v41, "j":J
    :goto_b4a
    :try_start_b4a
    invoke-virtual/range {v34 .. v34}, Ljava/nio/MappedByteBuffer;->hasRemaining()Z

    move-result v10

    if-eqz v10, :cond_cb9

    .line 548
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchsupport;->createAPK:Z

    if-nez v10, :cond_b7d

    .line 549
    invoke-virtual/range {v34 .. v34}, Ljava/nio/MappedByteBuffer;->position()I

    move-result v10

    sub-int v10, v10, v54

    const v11, 0x249ef

    if-le v10, v11, :cond_b7d

    .line 550
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Progress size:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual/range {v34 .. v34}, Ljava/nio/MappedByteBuffer;->position()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 551
    invoke-virtual/range {v34 .. v34}, Ljava/nio/MappedByteBuffer;->position()I

    move-result v54

    .line 554
    :cond_b7d
    invoke-virtual/range {v34 .. v34}, Ljava/nio/MappedByteBuffer;->position()I

    move-result v23

    .line 555
    invoke-virtual/range {v34 .. v34}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v22

    .line 559
    .local v22, "curentByte":B
    const/16 v38, 0x0

    .line 560
    .local v38, "increase":Z
    const/16 v17, 0x0

    .local v17, "b":I
    :goto_b89
    move-object/from16 v0, v50

    array-length v10, v0

    move/from16 v0, v17

    if-ge v0, v10, :cond_10e2

    .line 561
    aget-object v53, v50, v17

    .line 562
    .local v53, "patches":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    move-object/from16 v0, v34

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 564
    move-object/from16 v0, v53

    iget-boolean v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->markerTrig:Z

    if-eqz v10, :cond_cf2

    const/4 v10, 0x5

    move/from16 v0, v17

    if-eq v0, v10, :cond_ba9

    const/4 v10, 0x6

    move/from16 v0, v17

    if-ne v0, v10, :cond_cf2

    .line 566
    :cond_ba9
    if-nez v38, :cond_baf

    add-int/lit8 v61, v61, 0x1

    const/16 v38, 0x1

    .line 567
    :cond_baf
    move/from16 v0, v61

    move/from16 v1, v27

    if-ge v0, v1, :cond_e08

    .line 569
    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v11, 0x0

    aget-byte v10, v10, v11

    move/from16 v0, v22

    if-ne v0, v10, :cond_ceb

    .line 571
    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repMask:[I

    const/4 v11, 0x0

    aget v10, v10, v11

    if-nez v10, :cond_bd0

    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    const/4 v11, 0x0

    aput-byte v22, v10, v11

    .line 572
    :cond_bd0
    const/16 v37, 0x1

    .line 573
    add-int/lit8 v10, v23, 0x1

    move-object/from16 v0, v34

    invoke-virtual {v0, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 574
    invoke-virtual/range {v34 .. v34}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v56

    .line 576
    .local v56, "prufbyte":B
    :goto_bdd
    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    aget-byte v10, v10, v37

    move/from16 v0, v56

    if-eq v0, v10, :cond_c0e

    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origMask:[I

    aget v10, v10, v37

    const/4 v11, 0x1

    if-eq v10, v11, :cond_c0e

    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origMask:[I

    aget v10, v10, v37

    const/16 v11, 0x14

    if-eq v10, v11, :cond_c0e

    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origMask:[I

    aget v10, v10, v37

    const/16 v11, 0x15

    if-eq v10, v11, :cond_c0e

    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origMask:[I

    aget v10, v10, v37

    const/16 v11, 0x17

    if-ne v10, v11, :cond_ceb

    .line 578
    :cond_c0e
    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repMask:[I

    aget v10, v10, v37

    if-nez v10, :cond_c1c

    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    aput-byte v56, v10, v37

    .line 579
    :cond_c1c
    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repMask:[I

    aget v10, v10, v37

    const/16 v11, 0x14

    if-ne v10, v11, :cond_c2f

    .line 580
    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    and-int/lit8 v11, v56, 0xf

    int-to-byte v11, v11

    aput-byte v11, v10, v37

    .line 583
    :cond_c2f
    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repMask:[I

    aget v10, v10, v37

    const/16 v11, 0x15

    if-ne v10, v11, :cond_c44

    .line 584
    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    and-int/lit8 v11, v56, 0xf

    add-int/lit8 v11, v11, 0x10

    int-to-byte v11, v11

    aput-byte v11, v10, v37

    .line 588
    :cond_c44
    add-int/lit8 v37, v37, 0x1

    .line 589
    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    array-length v10, v10

    move/from16 v0, v37

    if-ne v0, v10, :cond_e02

    .line 591
    move-object/from16 v0, v34

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 592
    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    move-object/from16 v0, v34

    invoke-virtual {v0, v10}, Ljava/nio/MappedByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 593
    invoke-virtual/range {v34 .. v34}, Ljava/nio/MappedByteBuffer;->force()Ljava/nio/MappedByteBuffer;

    .line 594
    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->resultText:Ljava/lang/String;

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 595
    sget-object v10, Lcom/chelpus/root/utils/runpatchsupport;->print:Ljava/io/PrintStream;

    move-object/from16 v0, v53

    iget-object v11, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->resultText:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 596
    const/4 v10, 0x1

    move-object/from16 v0, v53

    iput-boolean v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->result:Z

    .line 597
    const/4 v10, 0x0

    move-object/from16 v0, v53

    iput-boolean v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->markerTrig:Z

    .line 598
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_c80
    :goto_c80
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_ce2

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    .line 599
    .local v24, "curpatch":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    move-object/from16 v0, v24

    iget-object v11, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->marker:Ljava/lang/String;

    move-object/from16 v0, v53

    iget-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->marker:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_c80

    .line 600
    const/4 v11, 0x0

    move-object/from16 v0, v24

    iput-boolean v11, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->markerTrig:Z
    :try_end_c9f
    .catch Ljava/lang/Exception; {:try_start_b4a .. :try_end_c9f} :catch_ca0
    .catch Ljava/io/FileNotFoundException; {:try_start_b4a .. :try_end_c9f} :catch_39c

    goto :goto_c80

    .line 789
    .end local v17    # "b":I
    .end local v22    # "curentByte":B
    .end local v24    # "curpatch":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    .end local v38    # "increase":Z
    .end local v53    # "patches":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    .end local v56    # "prufbyte":B
    :catch_ca0
    move-exception v28

    .line 790
    .restart local v28    # "e":Ljava/lang/Exception;
    :try_start_ca1
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, ""

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, v28

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 792
    .end local v28    # "e":Ljava/lang/Exception;
    :cond_cb9
    invoke-virtual {v9}, Ljava/nio/channels/FileChannel;->close()V

    .line 793
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, ""

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v11

    sub-long v11, v11, v63

    const-wide/16 v13, 0x3e8

    div-long/2addr v11, v13

    invoke-virtual {v10, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 794
    const-string v10, "Analise Results:"

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z
    :try_end_ce0
    .catch Ljava/io/FileNotFoundException; {:try_start_ca1 .. :try_end_ce0} :catch_39c
    .catch Ljava/lang/Exception; {:try_start_ca1 .. :try_end_ce0} :catch_3de

    goto/16 :goto_452

    .line 602
    .restart local v17    # "b":I
    .restart local v22    # "curentByte":B
    .restart local v38    # "increase":Z
    .restart local v53    # "patches":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    .restart local v56    # "prufbyte":B
    :cond_ce2
    const/16 v61, 0x0

    .line 603
    add-int/lit8 v10, v23, 0x1

    :try_start_ce6
    move-object/from16 v0, v34

    invoke-virtual {v0, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 610
    .end local v56    # "prufbyte":B
    :cond_ceb
    add-int/lit8 v10, v23, 0x1

    move-object/from16 v0, v34

    invoke-virtual {v0, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 622
    :cond_cf2
    :goto_cf2
    move-object/from16 v0, v53

    iget-boolean v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->markerTrig:Z

    if-eqz v10, :cond_e4c

    const/16 v10, 0x9

    move/from16 v0, v17

    if-eq v0, v10, :cond_d04

    const/16 v10, 0xa

    move/from16 v0, v17

    if-ne v0, v10, :cond_e4c

    .line 623
    :cond_d04
    sget-object v10, Lcom/chelpus/root/utils/runpatchsupport;->print:Ljava/io/PrintStream;

    const-string v11, "search jump"

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 624
    if-nez v38, :cond_d11

    add-int/lit8 v60, v60, 0x1

    const/16 v38, 0x1

    .line 625
    :cond_d11
    move/from16 v0, v60

    move/from16 v1, v26

    if-ge v0, v1, :cond_f50

    .line 627
    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v11, 0x0

    aget-byte v10, v10, v11

    move/from16 v0, v22

    if-ne v0, v10, :cond_e45

    .line 629
    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repMask:[I

    const/4 v11, 0x0

    aget v10, v10, v11

    if-nez v10, :cond_d32

    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    const/4 v11, 0x0

    aput-byte v22, v10, v11

    .line 630
    :cond_d32
    const/16 v37, 0x1

    .line 631
    add-int/lit8 v10, v23, 0x1

    move-object/from16 v0, v34

    invoke-virtual {v0, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 632
    invoke-virtual/range {v34 .. v34}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v56

    .line 634
    .restart local v56    # "prufbyte":B
    :goto_d3f
    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    aget-byte v10, v10, v37

    move/from16 v0, v56

    if-eq v0, v10, :cond_d70

    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origMask:[I

    aget v10, v10, v37

    const/4 v11, 0x1

    if-eq v10, v11, :cond_d70

    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origMask:[I

    aget v10, v10, v37

    const/16 v11, 0x14

    if-eq v10, v11, :cond_d70

    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origMask:[I

    aget v10, v10, v37

    const/16 v11, 0x15

    if-eq v10, v11, :cond_d70

    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origMask:[I

    aget v10, v10, v37

    const/16 v11, 0x17

    if-ne v10, v11, :cond_e45

    .line 636
    :cond_d70
    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repMask:[I

    aget v10, v10, v37

    if-nez v10, :cond_d7e

    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    aput-byte v56, v10, v37

    .line 637
    :cond_d7e
    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repMask:[I

    aget v10, v10, v37

    const/16 v11, 0x14

    if-ne v10, v11, :cond_d91

    .line 638
    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    and-int/lit8 v11, v56, 0xf

    int-to-byte v11, v11

    aput-byte v11, v10, v37

    .line 641
    :cond_d91
    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repMask:[I

    aget v10, v10, v37

    const/16 v11, 0x15

    if-ne v10, v11, :cond_da6

    .line 642
    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    and-int/lit8 v11, v56, 0xf

    add-int/lit8 v11, v11, 0x10

    int-to-byte v11, v11

    aput-byte v11, v10, v37

    .line 646
    :cond_da6
    add-int/lit8 v37, v37, 0x1

    .line 647
    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    array-length v10, v10

    move/from16 v0, v37

    if-ne v0, v10, :cond_f4a

    .line 649
    move-object/from16 v0, v34

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 650
    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    move-object/from16 v0, v34

    invoke-virtual {v0, v10}, Ljava/nio/MappedByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 651
    invoke-virtual/range {v34 .. v34}, Ljava/nio/MappedByteBuffer;->force()Ljava/nio/MappedByteBuffer;

    .line 652
    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->resultText:Ljava/lang/String;

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 653
    sget-object v10, Lcom/chelpus/root/utils/runpatchsupport;->print:Ljava/io/PrintStream;

    move-object/from16 v0, v53

    iget-object v11, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->resultText:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 654
    const/4 v10, 0x1

    move-object/from16 v0, v53

    iput-boolean v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->result:Z

    .line 655
    const/4 v10, 0x0

    move-object/from16 v0, v53

    iput-boolean v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->markerTrig:Z

    .line 656
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_de2
    :goto_de2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_e3c

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    .line 657
    .restart local v24    # "curpatch":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    move-object/from16 v0, v24

    iget-object v11, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->marker:Ljava/lang/String;

    move-object/from16 v0, v53

    iget-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->marker:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_de2

    .line 658
    const/4 v11, 0x0

    move-object/from16 v0, v24

    iput-boolean v11, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->markerTrig:Z

    goto :goto_de2

    .line 607
    .end local v24    # "curpatch":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    :cond_e02
    invoke-virtual/range {v34 .. v34}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v56

    goto/16 :goto_bdd

    .line 612
    .end local v56    # "prufbyte":B
    :cond_e08
    const/4 v10, 0x0

    move-object/from16 v0, v53

    iput-boolean v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->markerTrig:Z

    .line 613
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_e11
    :goto_e11
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_e31

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    .line 614
    .restart local v24    # "curpatch":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    move-object/from16 v0, v24

    iget-object v11, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->marker:Ljava/lang/String;

    move-object/from16 v0, v53

    iget-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->marker:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_e11

    .line 615
    const/4 v11, 0x0

    move-object/from16 v0, v24

    iput-boolean v11, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->markerTrig:Z

    goto :goto_e11

    .line 617
    .end local v24    # "curpatch":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    :cond_e31
    const/16 v61, 0x0

    .line 618
    add-int/lit8 v10, v23, 0x1

    move-object/from16 v0, v34

    invoke-virtual {v0, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    goto/16 :goto_cf2

    .line 660
    .restart local v56    # "prufbyte":B
    :cond_e3c
    const/16 v60, 0x0

    .line 661
    add-int/lit8 v10, v23, 0x1

    move-object/from16 v0, v34

    invoke-virtual {v0, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 668
    .end local v56    # "prufbyte":B
    :cond_e45
    add-int/lit8 v10, v23, 0x1

    move-object/from16 v0, v34

    invoke-virtual {v0, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 680
    :cond_e4c
    :goto_e4c
    move-object/from16 v0, v53

    iget-boolean v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->markerTrig:Z

    if-eqz v10, :cond_f94

    const/4 v10, 0x4

    move/from16 v0, v17

    if-ne v0, v10, :cond_f94

    .line 682
    add-int/lit8 v59, v59, 0x1

    .line 683
    move/from16 v0, v59

    move/from16 v1, v25

    if-ge v0, v1, :cond_109d

    .line 685
    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v11, 0x0

    aget-byte v10, v10, v11

    move/from16 v0, v22

    if-ne v0, v10, :cond_f8d

    .line 687
    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repMask:[I

    const/4 v11, 0x0

    aget v10, v10, v11

    if-nez v10, :cond_e7a

    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    const/4 v11, 0x0

    aput-byte v22, v10, v11

    .line 688
    :cond_e7a
    const/16 v37, 0x1

    .line 689
    add-int/lit8 v10, v23, 0x1

    move-object/from16 v0, v34

    invoke-virtual {v0, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 690
    invoke-virtual/range {v34 .. v34}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v56

    .line 692
    .restart local v56    # "prufbyte":B
    :goto_e87
    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    aget-byte v10, v10, v37

    move/from16 v0, v56

    if-eq v0, v10, :cond_eb8

    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origMask:[I

    aget v10, v10, v37

    const/4 v11, 0x1

    if-eq v10, v11, :cond_eb8

    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origMask:[I

    aget v10, v10, v37

    const/16 v11, 0x14

    if-eq v10, v11, :cond_eb8

    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origMask:[I

    aget v10, v10, v37

    const/16 v11, 0x15

    if-eq v10, v11, :cond_eb8

    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origMask:[I

    aget v10, v10, v37

    const/16 v11, 0x17

    if-ne v10, v11, :cond_f8d

    .line 694
    :cond_eb8
    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repMask:[I

    aget v10, v10, v37

    if-nez v10, :cond_ec6

    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    aput-byte v56, v10, v37

    .line 695
    :cond_ec6
    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repMask:[I

    aget v10, v10, v37

    const/16 v11, 0x14

    if-ne v10, v11, :cond_ed9

    .line 696
    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    and-int/lit8 v11, v56, 0xf

    int-to-byte v11, v11

    aput-byte v11, v10, v37

    .line 699
    :cond_ed9
    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repMask:[I

    aget v10, v10, v37

    const/16 v11, 0x15

    if-ne v10, v11, :cond_eee

    .line 700
    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    and-int/lit8 v11, v56, 0xf

    add-int/lit8 v11, v11, 0x10

    int-to-byte v11, v11

    aput-byte v11, v10, v37

    .line 704
    :cond_eee
    add-int/lit8 v37, v37, 0x1

    .line 705
    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    array-length v10, v10

    move/from16 v0, v37

    if-ne v0, v10, :cond_1097

    .line 707
    move-object/from16 v0, v34

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 708
    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    move-object/from16 v0, v34

    invoke-virtual {v0, v10}, Ljava/nio/MappedByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 709
    invoke-virtual/range {v34 .. v34}, Ljava/nio/MappedByteBuffer;->force()Ljava/nio/MappedByteBuffer;

    .line 710
    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->resultText:Ljava/lang/String;

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 711
    sget-object v10, Lcom/chelpus/root/utils/runpatchsupport;->print:Ljava/io/PrintStream;

    move-object/from16 v0, v53

    iget-object v11, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->resultText:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 712
    const/4 v10, 0x1

    move-object/from16 v0, v53

    iput-boolean v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->result:Z

    .line 713
    const/4 v10, 0x0

    move-object/from16 v0, v53

    iput-boolean v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->markerTrig:Z

    .line 714
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_f2a
    :goto_f2a
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_f84

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    .line 715
    .restart local v24    # "curpatch":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    move-object/from16 v0, v24

    iget-object v11, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->marker:Ljava/lang/String;

    move-object/from16 v0, v53

    iget-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->marker:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_f2a

    .line 716
    const/4 v11, 0x0

    move-object/from16 v0, v24

    iput-boolean v11, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->markerTrig:Z

    goto :goto_f2a

    .line 665
    .end local v24    # "curpatch":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    :cond_f4a
    invoke-virtual/range {v34 .. v34}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v56

    goto/16 :goto_d3f

    .line 670
    .end local v56    # "prufbyte":B
    :cond_f50
    const/4 v10, 0x0

    move-object/from16 v0, v53

    iput-boolean v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->markerTrig:Z

    .line 671
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_f59
    :goto_f59
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_f79

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    .line 672
    .restart local v24    # "curpatch":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    move-object/from16 v0, v24

    iget-object v11, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->marker:Ljava/lang/String;

    move-object/from16 v0, v53

    iget-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->marker:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_f59

    .line 673
    const/4 v11, 0x0

    move-object/from16 v0, v24

    iput-boolean v11, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->markerTrig:Z

    goto :goto_f59

    .line 675
    .end local v24    # "curpatch":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    :cond_f79
    const/16 v60, 0x0

    .line 676
    add-int/lit8 v10, v23, 0x1

    move-object/from16 v0, v34

    invoke-virtual {v0, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    goto/16 :goto_e4c

    .line 718
    .restart local v56    # "prufbyte":B
    :cond_f84
    const/16 v59, 0x0

    .line 719
    add-int/lit8 v10, v23, 0x1

    move-object/from16 v0, v34

    invoke-virtual {v0, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 726
    .end local v56    # "prufbyte":B
    :cond_f8d
    add-int/lit8 v10, v23, 0x1

    move-object/from16 v0, v34

    invoke-virtual {v0, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 738
    :cond_f94
    :goto_f94
    move-object/from16 v0, v53

    iget-boolean v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->markerTrig:Z

    if-nez v10, :cond_10de

    .line 739
    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v11, 0x0

    aget-byte v10, v10, v11

    move/from16 v0, v22

    if-ne v0, v10, :cond_10de

    move-object/from16 v0, v53

    iget-boolean v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->pattern:Z

    if-eqz v10, :cond_10de

    .line 741
    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repMask:[I

    const/4 v11, 0x0

    aget v10, v10, v11

    if-nez v10, :cond_fbb

    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    const/4 v11, 0x0

    aput-byte v22, v10, v11

    .line 742
    :cond_fbb
    const/16 v37, 0x1

    .line 743
    add-int/lit8 v10, v23, 0x1

    move-object/from16 v0, v34

    invoke-virtual {v0, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 744
    invoke-virtual/range {v34 .. v34}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v56

    .line 746
    .restart local v56    # "prufbyte":B
    :goto_fc8
    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    aget-byte v10, v10, v37

    move/from16 v0, v56

    if-eq v0, v10, :cond_ff9

    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origMask:[I

    aget v10, v10, v37

    const/4 v11, 0x1

    if-eq v10, v11, :cond_ff9

    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origMask:[I

    aget v10, v10, v37

    const/16 v11, 0x14

    if-eq v10, v11, :cond_ff9

    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origMask:[I

    aget v10, v10, v37

    const/16 v11, 0x15

    if-eq v10, v11, :cond_ff9

    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origMask:[I

    aget v10, v10, v37

    const/16 v11, 0x17

    if-ne v10, v11, :cond_10d7

    .line 748
    :cond_ff9
    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repMask:[I

    aget v10, v10, v37

    if-nez v10, :cond_1007

    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    aput-byte v56, v10, v37

    .line 749
    :cond_1007
    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repMask:[I

    aget v10, v10, v37

    const/16 v11, 0x14

    if-ne v10, v11, :cond_101a

    .line 750
    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    and-int/lit8 v11, v56, 0xf

    int-to-byte v11, v11

    aput-byte v11, v10, v37

    .line 753
    :cond_101a
    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repMask:[I

    aget v10, v10, v37

    const/16 v11, 0x15

    if-ne v10, v11, :cond_102f

    .line 754
    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    and-int/lit8 v11, v56, 0xf

    add-int/lit8 v11, v11, 0x10

    int-to-byte v11, v11

    aput-byte v11, v10, v37

    .line 758
    :cond_102f
    add-int/lit8 v37, v37, 0x1

    .line 759
    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    array-length v10, v10

    move/from16 v0, v37

    if-ne v0, v10, :cond_10d1

    .line 761
    move-object/from16 v0, v34

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 762
    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    move-object/from16 v0, v34

    invoke-virtual {v0, v10}, Ljava/nio/MappedByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 763
    invoke-virtual/range {v34 .. v34}, Ljava/nio/MappedByteBuffer;->force()Ljava/nio/MappedByteBuffer;

    .line 764
    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->resultText:Ljava/lang/String;

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 765
    sget-object v10, Lcom/chelpus/root/utils/runpatchsupport;->print:Ljava/io/PrintStream;

    move-object/from16 v0, v53

    iget-object v11, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->resultText:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 766
    const/4 v10, 0x1

    move-object/from16 v0, v53

    iput-boolean v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->result:Z

    .line 767
    move-object/from16 v0, v53

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->marker:Ljava/lang/String;

    const-string v11, ""

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_10d7

    .line 768
    const/4 v10, 0x1

    move-object/from16 v0, v53

    iput-boolean v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->markerTrig:Z

    .line 769
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_1077
    :goto_1077
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_10d7

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    .line 770
    .restart local v24    # "curpatch":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    move-object/from16 v0, v24

    iget-object v11, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->marker:Ljava/lang/String;

    move-object/from16 v0, v53

    iget-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->marker:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1077

    .line 771
    const/4 v11, 0x1

    move-object/from16 v0, v24

    iput-boolean v11, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->markerTrig:Z

    goto :goto_1077

    .line 723
    .end local v24    # "curpatch":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    :cond_1097
    invoke-virtual/range {v34 .. v34}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v56

    goto/16 :goto_e87

    .line 728
    .end local v56    # "prufbyte":B
    :cond_109d
    const/4 v10, 0x0

    move-object/from16 v0, v53

    iput-boolean v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->markerTrig:Z

    .line 729
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_10a6
    :goto_10a6
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_10c6

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    .line 730
    .restart local v24    # "curpatch":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    move-object/from16 v0, v24

    iget-object v11, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->marker:Ljava/lang/String;

    move-object/from16 v0, v53

    iget-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->marker:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_10a6

    .line 731
    const/4 v11, 0x0

    move-object/from16 v0, v24

    iput-boolean v11, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->markerTrig:Z

    goto :goto_10a6

    .line 733
    .end local v24    # "curpatch":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    :cond_10c6
    const/16 v59, 0x0

    .line 734
    add-int/lit8 v10, v23, 0x1

    move-object/from16 v0, v34

    invoke-virtual {v0, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    goto/16 :goto_f94

    .line 777
    .restart local v56    # "prufbyte":B
    :cond_10d1
    invoke-virtual/range {v34 .. v34}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v56

    goto/16 :goto_fc8

    .line 779
    :cond_10d7
    add-int/lit8 v10, v23, 0x1

    move-object/from16 v0, v34

    invoke-virtual {v0, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 560
    .end local v56    # "prufbyte":B
    :cond_10de
    add-int/lit8 v17, v17, 0x1

    goto/16 :goto_b89

    .line 787
    .end local v53    # "patches":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    :cond_10e2
    add-int/lit8 v10, v23, 0x1

    move-object/from16 v0, v34

    invoke-virtual {v0, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;
    :try_end_10e9
    .catch Ljava/lang/Exception; {:try_start_ce6 .. :try_end_10e9} :catch_ca0
    .catch Ljava/io/FileNotFoundException; {:try_start_ce6 .. :try_end_10e9} :catch_39c

    .line 546
    const-wide/16 v10, 0x1

    add-long v41, v41, v10

    goto/16 :goto_b4a

    .line 810
    .end local v9    # "ChannelDex":Ljava/nio/channels/FileChannel;
    .end local v17    # "b":I
    .end local v19    # "commands":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;>;"
    .end local v21    # "count":I
    .end local v22    # "curentByte":B
    .end local v23    # "curentPos":I
    .end local v25    # "diaposon":I
    .end local v26    # "diaposon_ev":I
    .end local v27    # "diaposon_pak":I
    .end local v29    # "ev":Z
    .end local v30    # "f1":Z
    .end local v31    # "f2":Z
    .end local v32    # "f3":Z
    .end local v34    # "fileBytes":Ljava/nio/MappedByteBuffer;
    .end local v35    # "filepatch":Ljava/io/File;
    .end local v37    # "i":I
    .end local v38    # "increase":Z
    .end local v41    # "j":J
    .end local v44    # "new_patch_method":Z
    .end local v47    # "of_to_patch":I
    .end local v48    # "offsets":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;>;"
    .end local v50    # "patchList":[Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    .end local v51    # "patch_index1":I
    .end local v52    # "patch_index2":I
    .end local v54    # "period":I
    .end local v58    # "spi":Z
    .end local v59    # "start_for_diaposon":I
    .end local v60    # "start_for_diaposon_ev":I
    .end local v61    # "start_for_diaposon_pak":I
    .end local v62    # "strings":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v63    # "time":J
    .end local v65    # "types":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/TypesItem;>;"
    .end local v66    # "u":I
    :cond_10ef
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchsupport;->createAPK:Z

    if-nez v10, :cond_1145

    .line 811
    const-string v10, "Create ODEX:"

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 812
    const/4 v10, 0x3

    aget-object v10, p0, v10

    sget-object v11, Lcom/chelpus/root/utils/runpatchsupport;->classesFiles:Ljava/util/ArrayList;

    const/4 v12, 0x2

    aget-object v12, p0, v12

    sget-object v13, Lcom/chelpus/root/utils/runpatchsupport;->uid:Ljava/lang/String;

    const/4 v14, 0x2

    aget-object v14, p0, v14

    sget-object v67, Lcom/chelpus/root/utils/runpatchsupport;->uid:Ljava/lang/String;

    move-object/from16 v0, v67

    invoke-static {v14, v0}, Lcom/chelpus/Utils;->getOdexForCreate(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-static {v10, v11, v12, v13, v14}, Lcom/chelpus/Utils;->create_ODEX_root(Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v57

    .line 813
    .local v57, "r":I
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "chelpus_return_"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v57

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 814
    if-nez v57, :cond_1145

    sget-boolean v10, Lcom/chelpus/root/utils/runpatchsupport;->ART:Z

    if-nez v10, :cond_1145

    .line 815
    const/4 v10, 0x1

    aget-object v10, p0, v10

    const/4 v11, 0x2

    aget-object v11, p0, v11

    const/4 v12, 0x2

    aget-object v12, p0, v12

    const/4 v13, 0x1

    invoke-static {v12, v13}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v12

    sget-object v13, Lcom/chelpus/root/utils/runpatchsupport;->uid:Ljava/lang/String;

    const/4 v14, 0x3

    aget-object v14, p0, v14

    invoke-static {v10, v11, v12, v13, v14}, Lcom/chelpus/Utils;->afterPatch(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 819
    .end local v57    # "r":I
    :cond_1145
    const-string v10, "Optional Steps After Patch:"

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 820
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchsupport;->createAPK:Z

    if-nez v10, :cond_1151

    invoke-static {}, Lcom/chelpus/Utils;->exitFromRootJava()V

    .line 821
    :cond_1151
    move-object/from16 v0, v55

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/LogOutputStream;->allresult:Ljava/lang/String;

    sput-object v10, Lcom/chelpus/root/utils/runpatchsupport;->result:Ljava/lang/String;

    .line 822
    return-void

    .line 93
    .end local v3    # "origStr":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v4    # "replStr":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v5    # "trigger":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Boolean;>;"
    .end local v6    # "ResultText":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v7    # "markers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v8    # "dontConvert":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Boolean;>;"
    .end local v20    # "conv":Z
    :catch_1158
    move-exception v10

    goto/16 :goto_111

    .line 92
    :catch_115b
    move-exception v10

    goto/16 :goto_111

    .line 89
    :catch_115e
    move-exception v10

    goto/16 :goto_103

    .line 88
    :catch_1161
    move-exception v10

    goto/16 :goto_103
.end method

.method public static unzipART(Ljava/io/File;)V
    .registers 23
    .param p0, "apk"    # Ljava/io/File;

    .prologue
    .line 1003
    const/4 v7, 0x0

    .local v7, "found1":Z
    const/4 v8, 0x0

    .line 1005
    .local v8, "found2":Z
    :try_start_2
    new-instance v6, Ljava/io/FileInputStream;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 1006
    .local v6, "fin":Ljava/io/FileInputStream;
    new-instance v16, Ljava/util/zip/ZipInputStream;

    move-object/from16 v0, v16

    invoke-direct {v0, v6}, Ljava/util/zip/ZipInputStream;-><init>(Ljava/io/InputStream;)V

    .line 1007
    .local v16, "zin":Ljava/util/zip/ZipInputStream;
    const/4 v15, 0x0

    .line 1009
    .local v15, "ze":Ljava/util/zip/ZipEntry;
    invoke-virtual/range {v16 .. v16}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    move-result-object v15

    .line 1010
    const/4 v14, 0x1

    .line 1011
    .local v14, "search":Z
    :goto_16
    if-eqz v15, :cond_227

    if-eqz v14, :cond_227

    .line 1017
    invoke-virtual {v15}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v11

    .line 1019
    .local v11, "haystack":Ljava/lang/String;
    invoke-virtual {v11}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v18

    const-string v19, "classes"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_19f

    const-string v18, ".dex"

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_19f

    const-string v18, "/"

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_19f

    .line 1021
    new-instance v9, Ljava/io/FileOutputStream;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v19, Lcom/chelpus/root/utils/runpatchsupport;->sddir:Ljava/lang/String;

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "/"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-direct {v9, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 1023
    .local v9, "fout":Ljava/io/FileOutputStream;
    const/16 v18, 0x800

    move/from16 v0, v18

    new-array v2, v0, [B

    .line 1026
    .local v2, "buffer":[B
    :goto_66
    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/util/zip/ZipInputStream;->read([B)I

    move-result v12

    .local v12, "length":I
    const/16 v18, -0x1

    move/from16 v0, v18

    if-eq v12, v0, :cond_13e

    .line 1027
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v9, v2, v0, v12}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_79
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_79} :catch_7a

    goto :goto_66

    .line 1059
    .end local v2    # "buffer":[B
    .end local v6    # "fin":Ljava/io/FileInputStream;
    .end local v9    # "fout":Ljava/io/FileOutputStream;
    .end local v11    # "haystack":Ljava/lang/String;
    .end local v12    # "length":I
    .end local v14    # "search":Z
    .end local v15    # "ze":Ljava/util/zip/ZipEntry;
    .end local v16    # "zin":Ljava/util/zip/ZipInputStream;
    :catch_7a
    move-exception v4

    .line 1061
    .local v4, "e":Ljava/lang/Exception;
    :try_start_7b
    new-instance v17, Lnet/lingala/zip4j/core/ZipFile;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lnet/lingala/zip4j/core/ZipFile;-><init>(Ljava/io/File;)V

    .line 1065
    .local v17, "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    const-string v18, "classes.dex"

    sget-object v19, Lcom/chelpus/root/utils/runpatchsupport;->sddir:Ljava/lang/String;

    invoke-virtual/range {v17 .. v19}, Lnet/lingala/zip4j/core/ZipFile;->extractFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 1066
    sget-object v18, Lcom/chelpus/root/utils/runpatchsupport;->classesFiles:Ljava/util/ArrayList;

    new-instance v19, Ljava/io/File;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v21, Lcom/chelpus/root/utils/runpatchsupport;->sddir:Ljava/lang/String;

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "classes.dex"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v18 .. v19}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1067
    const/16 v18, 0x3

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const-string v20, "chmod"

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, "777"

    aput-object v20, v18, v19

    const/16 v19, 0x2

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v21, Lcom/chelpus/root/utils/runpatchsupport;->sddir:Ljava/lang/String;

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "classes.dex"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    aput-object v20, v18, v19

    invoke-static/range {v18 .. v18}, Lcom/chelpus/Utils;->cmdParam([Ljava/lang/String;)Ljava/lang/String;

    .line 1068
    const-string v18, "AndroidManifest.xml"

    sget-object v19, Lcom/chelpus/root/utils/runpatchsupport;->sddir:Ljava/lang/String;

    invoke-virtual/range {v17 .. v19}, Lnet/lingala/zip4j/core/ZipFile;->extractFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 1069
    const/16 v18, 0x3

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const-string v20, "chmod"

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, "777"

    aput-object v20, v18, v19

    const/16 v19, 0x2

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v21, Lcom/chelpus/root/utils/runpatchsupport;->sddir:Ljava/lang/String;

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "AndroidManifest.xml"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    aput-object v20, v18, v19

    invoke-static/range {v18 .. v18}, Lcom/chelpus/Utils;->cmdParam([Ljava/lang/String;)Ljava/lang/String;
    :try_end_123
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_7b .. :try_end_123} :catch_235
    .catch Ljava/lang/Exception; {:try_start_7b .. :try_end_123} :catch_26a

    .line 1078
    .end local v17    # "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    :goto_123
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Exception e"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v4}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 1082
    .end local v4    # "e":Ljava/lang/Exception;
    :goto_13d
    return-void

    .line 1031
    .restart local v2    # "buffer":[B
    .restart local v6    # "fin":Ljava/io/FileInputStream;
    .restart local v9    # "fout":Ljava/io/FileOutputStream;
    .restart local v11    # "haystack":Ljava/lang/String;
    .restart local v12    # "length":I
    .restart local v14    # "search":Z
    .restart local v15    # "ze":Ljava/util/zip/ZipEntry;
    .restart local v16    # "zin":Ljava/util/zip/ZipInputStream;
    :cond_13e
    :try_start_13e
    invoke-virtual/range {v16 .. v16}, Ljava/util/zip/ZipInputStream;->closeEntry()V

    .line 1032
    invoke-virtual {v9}, Ljava/io/FileOutputStream;->close()V

    .line 1033
    sget-object v18, Lcom/chelpus/root/utils/runpatchsupport;->classesFiles:Ljava/util/ArrayList;

    new-instance v19, Ljava/io/File;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v21, Lcom/chelpus/root/utils/runpatchsupport;->sddir:Ljava/lang/String;

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v18 .. v19}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1034
    const/16 v18, 0x3

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const-string v20, "chmod"

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, "777"

    aput-object v20, v18, v19

    const/16 v19, 0x2

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v21, Lcom/chelpus/root/utils/runpatchsupport;->sddir:Ljava/lang/String;

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    aput-object v20, v18, v19

    invoke-static/range {v18 .. v18}, Lcom/chelpus/Utils;->cmdParam([Ljava/lang/String;)Ljava/lang/String;

    .line 1037
    .end local v2    # "buffer":[B
    .end local v9    # "fout":Ljava/io/FileOutputStream;
    .end local v12    # "length":I
    :cond_19f
    const-string v18, "AndroidManifest.xml"

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_222

    .line 1038
    new-instance v10, Ljava/io/FileOutputStream;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v19, Lcom/chelpus/root/utils/runpatchsupport;->sddir:Ljava/lang/String;

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "/"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "AndroidManifest.xml"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-direct {v10, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 1040
    .local v10, "fout2":Ljava/io/FileOutputStream;
    const/16 v18, 0x800

    move/from16 v0, v18

    new-array v3, v0, [B

    .line 1042
    .local v3, "buffer2":[B
    :goto_1d1
    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/util/zip/ZipInputStream;->read([B)I

    move-result v13

    .local v13, "length2":I
    const/16 v18, -0x1

    move/from16 v0, v18

    if-eq v13, v0, :cond_1e5

    .line 1043
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v10, v3, v0, v13}, Ljava/io/FileOutputStream;->write([BII)V

    goto :goto_1d1

    .line 1045
    :cond_1e5
    invoke-virtual/range {v16 .. v16}, Ljava/util/zip/ZipInputStream;->closeEntry()V

    .line 1046
    invoke-virtual {v10}, Ljava/io/FileOutputStream;->close()V

    .line 1047
    const/16 v18, 0x3

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const-string v20, "chmod"

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, "777"

    aput-object v20, v18, v19

    const/16 v19, 0x2

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v21, Lcom/chelpus/root/utils/runpatchsupport;->sddir:Ljava/lang/String;

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "AndroidManifest.xml"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    aput-object v20, v18, v19

    invoke-static/range {v18 .. v18}, Lcom/chelpus/Utils;->cmdParam([Ljava/lang/String;)Ljava/lang/String;

    .line 1048
    const/4 v8, 0x1

    .line 1050
    .end local v3    # "buffer2":[B
    .end local v10    # "fout2":Ljava/io/FileOutputStream;
    .end local v13    # "length2":I
    :cond_222
    if-eqz v7, :cond_22f

    if-eqz v8, :cond_22f

    .line 1051
    const/4 v14, 0x0

    .line 1057
    .end local v11    # "haystack":Ljava/lang/String;
    :cond_227
    invoke-virtual/range {v16 .. v16}, Ljava/util/zip/ZipInputStream;->close()V

    .line 1058
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V

    goto/16 :goto_13d

    .line 1054
    .restart local v11    # "haystack":Ljava/lang/String;
    :cond_22f
    invoke-virtual/range {v16 .. v16}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;
    :try_end_232
    .catch Ljava/lang/Exception; {:try_start_13e .. :try_end_232} :catch_7a

    move-result-object v15

    .line 1056
    goto/16 :goto_16

    .line 1071
    .end local v6    # "fin":Ljava/io/FileInputStream;
    .end local v11    # "haystack":Ljava/lang/String;
    .end local v14    # "search":Z
    .end local v15    # "ze":Ljava/util/zip/ZipEntry;
    .end local v16    # "zin":Ljava/util/zip/ZipInputStream;
    .restart local v4    # "e":Ljava/lang/Exception;
    :catch_235
    move-exception v5

    .line 1072
    .local v5, "e1":Lnet/lingala/zip4j/exception/ZipException;
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Error classes.dex decompress! "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 1073
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Exception e1"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v4}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    goto/16 :goto_123

    .line 1074
    .end local v5    # "e1":Lnet/lingala/zip4j/exception/ZipException;
    :catch_26a
    move-exception v5

    .line 1075
    .local v5, "e1":Ljava/lang/Exception;
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Error classes.dex decompress! "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 1076
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Exception e1"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v4}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    goto/16 :goto_123
.end method

.method public static unzipSD(Ljava/io/File;)V
    .registers 16
    .param p0, "apk"    # Ljava/io/File;

    .prologue
    const/4 v14, -0x1

    .line 936
    :try_start_1
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 937
    .local v4, "fin":Ljava/io/FileInputStream;
    new-instance v8, Ljava/util/zip/ZipInputStream;

    invoke-direct {v8, v4}, Ljava/util/zip/ZipInputStream;-><init>(Ljava/io/InputStream;)V

    .line 938
    .local v8, "zin":Ljava/util/zip/ZipInputStream;
    const/4 v7, 0x0

    .line 939
    .local v7, "ze":Ljava/util/zip/ZipEntry;
    const/4 v1, 0x0

    .line 940
    .local v1, "classesdex":Z
    :cond_d
    :goto_d
    invoke-virtual {v8}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    move-result-object v7

    if-eqz v7, :cond_159

    .line 945
    invoke-virtual {v7}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v10

    const-string v11, "classes"

    invoke-virtual {v10, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_114

    invoke-virtual {v7}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v10

    const-string v11, ".dex"

    invoke-virtual {v10, v11}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_114

    invoke-virtual {v7}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v10

    const-string v11, "/"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_114

    .line 946
    new-instance v5, Ljava/io/FileOutputStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v11, Lcom/chelpus/root/utils/runpatchsupport;->sddir:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "/Modified/"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v7}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v5, v10}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 948
    .local v5, "fout":Ljava/io/FileOutputStream;
    const/16 v10, 0x400

    new-array v0, v10, [B

    .line 950
    .local v0, "buffer":[B
    :goto_61
    invoke-virtual {v8, v0}, Ljava/util/zip/ZipInputStream;->read([B)I

    move-result v6

    .local v6, "length":I
    if-eq v6, v14, :cond_e2

    .line 951
    const/4 v10, 0x0

    invoke-virtual {v5, v0, v10, v6}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_6b
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_6b} :catch_6c

    goto :goto_61

    .line 979
    .end local v0    # "buffer":[B
    .end local v1    # "classesdex":Z
    .end local v4    # "fin":Ljava/io/FileInputStream;
    .end local v5    # "fout":Ljava/io/FileOutputStream;
    .end local v6    # "length":I
    .end local v7    # "ze":Ljava/util/zip/ZipEntry;
    .end local v8    # "zin":Ljava/util/zip/ZipInputStream;
    :catch_6c
    move-exception v2

    .line 981
    .local v2, "e":Ljava/lang/Exception;
    :try_start_6d
    new-instance v9, Lnet/lingala/zip4j/core/ZipFile;

    invoke-direct {v9, p0}, Lnet/lingala/zip4j/core/ZipFile;-><init>(Ljava/io/File;)V

    .line 985
    .local v9, "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    const-string v10, "classes.dex"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v12, Lcom/chelpus/root/utils/runpatchsupport;->sddir:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/Modified/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Lnet/lingala/zip4j/core/ZipFile;->extractFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 986
    sget-object v10, Lcom/chelpus/root/utils/runpatchsupport;->classesFiles:Ljava/util/ArrayList;

    new-instance v11, Ljava/io/File;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v13, Lcom/chelpus/root/utils/runpatchsupport;->sddir:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "/Modified/"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "classes.dex"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 987
    const-string v10, "AndroidManifest.xml"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v12, Lcom/chelpus/root/utils/runpatchsupport;->sddir:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/Modified/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Lnet/lingala/zip4j/core/ZipFile;->extractFile(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_cb
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_6d .. :try_end_cb} :catch_160
    .catch Ljava/lang/Exception; {:try_start_6d .. :try_end_cb} :catch_193

    .line 996
    .end local v9    # "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    :goto_cb
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Decompressunzip "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 1000
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_e1
    return-void

    .line 953
    .restart local v0    # "buffer":[B
    .restart local v1    # "classesdex":Z
    .restart local v4    # "fin":Ljava/io/FileInputStream;
    .restart local v5    # "fout":Ljava/io/FileOutputStream;
    .restart local v6    # "length":I
    .restart local v7    # "ze":Ljava/util/zip/ZipEntry;
    .restart local v8    # "zin":Ljava/util/zip/ZipInputStream;
    :cond_e2
    :try_start_e2
    sget-object v10, Lcom/chelpus/root/utils/runpatchsupport;->classesFiles:Ljava/util/ArrayList;

    new-instance v11, Ljava/io/File;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v13, Lcom/chelpus/root/utils/runpatchsupport;->sddir:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "/Modified/"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v7}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 954
    const/4 v1, 0x1

    .line 955
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchsupport;->createAPK:Z

    if-nez v10, :cond_114

    .line 956
    invoke-virtual {v8}, Ljava/util/zip/ZipInputStream;->closeEntry()V

    .line 957
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V

    .line 960
    .end local v0    # "buffer":[B
    .end local v5    # "fout":Ljava/io/FileOutputStream;
    .end local v6    # "length":I
    :cond_114
    invoke-virtual {v7}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v10

    const-string v11, "AndroidManifest.xml"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_d

    .line 961
    new-instance v5, Ljava/io/FileOutputStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v11, Lcom/chelpus/root/utils/runpatchsupport;->sddir:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "/Modified/"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "AndroidManifest.xml"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v5, v10}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 963
    .restart local v5    # "fout":Ljava/io/FileOutputStream;
    const/16 v10, 0x400

    new-array v0, v10, [B

    .line 965
    .restart local v0    # "buffer":[B
    :goto_144
    invoke-virtual {v8, v0}, Ljava/util/zip/ZipInputStream;->read([B)I

    move-result v6

    .restart local v6    # "length":I
    if-eq v6, v14, :cond_14f

    .line 966
    const/4 v10, 0x0

    invoke-virtual {v5, v0, v10, v6}, Ljava/io/FileOutputStream;->write([BII)V

    goto :goto_144

    .line 969
    :cond_14f
    if-eqz v1, :cond_d

    .line 970
    invoke-virtual {v8}, Ljava/util/zip/ZipInputStream;->closeEntry()V

    .line 971
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V

    goto/16 :goto_d

    .line 977
    .end local v0    # "buffer":[B
    .end local v5    # "fout":Ljava/io/FileOutputStream;
    .end local v6    # "length":I
    :cond_159
    invoke-virtual {v8}, Ljava/util/zip/ZipInputStream;->close()V

    .line 978
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_15f
    .catch Ljava/lang/Exception; {:try_start_e2 .. :try_end_15f} :catch_6c

    goto :goto_e1

    .line 989
    .end local v1    # "classesdex":Z
    .end local v4    # "fin":Ljava/io/FileInputStream;
    .end local v7    # "ze":Ljava/util/zip/ZipEntry;
    .end local v8    # "zin":Ljava/util/zip/ZipInputStream;
    .restart local v2    # "e":Ljava/lang/Exception;
    :catch_160
    move-exception v3

    .line 990
    .local v3, "e1":Lnet/lingala/zip4j/exception/ZipException;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Error classes.dex decompress! "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 991
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Exception e1"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    goto/16 :goto_cb

    .line 992
    .end local v3    # "e1":Lnet/lingala/zip4j/exception/ZipException;
    :catch_193
    move-exception v3

    .line 993
    .local v3, "e1":Ljava/lang/Exception;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Error classes.dex decompress! "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 994
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Exception e1"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    goto/16 :goto_cb
.end method
