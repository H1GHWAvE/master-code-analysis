.class public Lcom/chelpus/root/utils/runpatch;
.super Ljava/lang/Object;
.source "runpatch.java"


# static fields
.field private static dalvikDexIn:Ljava/lang/String;

.field private static dalvikDexIn2:Ljava/lang/String;

.field public static dirapp:Ljava/lang/String;

.field public static odexpatch:Z

.field private static pattern1:Z

.field private static pattern2:Z

.field private static pattern3:Z

.field private static pattern4:Z

.field public static system:Z


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 13
    const-string v0, "/cache/dalvik-cache/data@app@zamenitetodelo-1.apk@classes.dex"

    sput-object v0, Lcom/chelpus/root/utils/runpatch;->dalvikDexIn2:Ljava/lang/String;

    .line 14
    const-string v0, "/data/dalvik-cache/data@app@zamenitetodelo-1.apk@classes.dex"

    sput-object v0, Lcom/chelpus/root/utils/runpatch;->dalvikDexIn:Ljava/lang/String;

    .line 15
    sput-boolean v1, Lcom/chelpus/root/utils/runpatch;->pattern1:Z

    .line 16
    sput-boolean v1, Lcom/chelpus/root/utils/runpatch;->pattern2:Z

    .line 17
    sput-boolean v1, Lcom/chelpus/root/utils/runpatch;->pattern3:Z

    .line 18
    sput-boolean v1, Lcom/chelpus/root/utils/runpatch;->pattern4:Z

    .line 19
    const-string v0, "/data/app/"

    sput-object v0, Lcom/chelpus/root/utils/runpatch;->dirapp:Ljava/lang/String;

    .line 20
    sput-boolean v2, Lcom/chelpus/root/utils/runpatch;->system:Z

    .line 21
    sput-boolean v2, Lcom/chelpus/root/utils/runpatch;->odexpatch:Z

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .registers 106
    .param p0, "paramArrayOfString"    # [Ljava/lang/String;

    .prologue
    .line 25
    const/4 v3, 0x1

    sput-boolean v3, Lcom/chelpus/root/utils/runpatch;->pattern1:Z

    .line 26
    const/4 v3, 0x1

    sput-boolean v3, Lcom/chelpus/root/utils/runpatch;->pattern2:Z

    .line 27
    const/4 v3, 0x1

    sput-boolean v3, Lcom/chelpus/root/utils/runpatch;->pattern3:Z

    .line 28
    const/4 v3, 0x1

    sput-boolean v3, Lcom/chelpus/root/utils/runpatch;->pattern4:Z

    .line 29
    new-instance v3, Lcom/chelpus/root/utils/runpatch$1;

    invoke-direct {v3}, Lcom/chelpus/root/utils/runpatch$1;-><init>()V

    invoke-static {v3}, Lcom/chelpus/Utils;->startRootJava(Ljava/lang/Object;)V

    .line 32
    const/4 v3, 0x3

    aget-object v3, p0, v3

    sput-object v3, Lcom/chelpus/root/utils/runpatch;->dirapp:Ljava/lang/String;

    .line 33
    const/4 v3, 0x2

    aget-object v3, p0, v3

    const-string v4, "not_system"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_27

    const/4 v3, 0x0

    sput-boolean v3, Lcom/chelpus/root/utils/runpatch;->system:Z

    .line 34
    :cond_27
    const/4 v3, 0x2

    aget-object v3, p0, v3

    const-string v4, "system"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_35

    const/4 v3, 0x1

    sput-boolean v3, Lcom/chelpus/root/utils/runpatch;->system:Z

    .line 35
    :cond_35
    new-instance v8, Ljava/io/File;

    sget-object v3, Lcom/chelpus/root/utils/runpatch;->dirapp:Ljava/lang/String;

    invoke-direct {v8, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 36
    .local v8, "appapk":Ljava/io/File;
    new-instance v9, Ljava/io/File;

    sget-object v3, Lcom/chelpus/root/utils/runpatch;->dirapp:Ljava/lang/String;

    const/4 v4, 0x1

    invoke-static {v3, v4}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v9, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 37
    .local v9, "appodex":Ljava/io/File;
    sget-boolean v3, Lcom/chelpus/root/utils/runpatch;->system:Z

    if-eqz v3, :cond_6a

    .line 38
    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_6a

    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_6a

    invoke-static {v8}, Lcom/chelpus/Utils;->classes_test(Ljava/io/File;)Z

    move-result v3

    if-nez v3, :cond_6a

    const/4 v3, 0x1

    sput-boolean v3, Lcom/chelpus/root/utils/runpatch;->odexpatch:Z

    .line 39
    move-object/from16 v56, v9

    .line 40
    .local v56, "localFile2":Ljava/io/File;
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "\nOdex Application.\nOnly ODEX patch is enabled.\n"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 47
    .end local v56    # "localFile2":Ljava/io/File;
    :cond_6a
    const/4 v3, 0x1

    aget-object v3, p0, v3

    if-eqz v3, :cond_a7

    .line 48
    const/4 v3, 0x1

    aget-object v3, p0, v3

    const-string v4, "pattern1"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_7d

    const/4 v3, 0x0

    sput-boolean v3, Lcom/chelpus/root/utils/runpatch;->pattern1:Z

    .line 49
    :cond_7d
    const/4 v3, 0x1

    aget-object v3, p0, v3

    const-string v4, "pattern2"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_8b

    const/4 v3, 0x0

    sput-boolean v3, Lcom/chelpus/root/utils/runpatch;->pattern2:Z

    .line 50
    :cond_8b
    const/4 v3, 0x1

    aget-object v3, p0, v3

    const-string v4, "pattern3"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_99

    const/4 v3, 0x0

    sput-boolean v3, Lcom/chelpus/root/utils/runpatch;->pattern3:Z

    .line 51
    :cond_99
    const/4 v3, 0x1

    aget-object v3, p0, v3

    const-string v4, "pattern4"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_a7

    const/4 v3, 0x0

    sput-boolean v3, Lcom/chelpus/root/utils/runpatch;->pattern4:Z

    .line 54
    :cond_a7
    const/16 v3, 0x1d

    new-array v10, v3, [B

    fill-array-data v10, :array_d6e

    .line 55
    .local v10, "byteOrig":[B
    const/16 v3, 0x1d

    new-array v0, v3, [B

    move-object/from16 v60, v0

    fill-array-data v60, :array_d82

    .line 56
    .local v60, "mask":[B
    const/16 v3, 0x1d

    new-array v0, v3, [B

    move-object/from16 v27, v0

    fill-array-data v27, :array_d96

    .line 57
    .local v27, "byteReplace":[B
    const/16 v3, 0x1d

    new-array v0, v3, [B

    move-object/from16 v85, v0

    fill-array-data v85, :array_daa

    .line 59
    .local v85, "rep_mask":[B
    const/16 v3, 0x17

    new-array v0, v3, [B

    move-object/from16 v20, v0

    fill-array-data v20, :array_dbe

    .line 60
    .local v20, "byteOrig2":[B
    const/16 v3, 0x17

    new-array v0, v3, [B

    move-object/from16 v70, v0

    fill-array-data v70, :array_dce

    .line 61
    .local v70, "mask2":[B
    const/16 v3, 0x17

    new-array v0, v3, [B

    move-object/from16 v37, v0

    fill-array-data v37, :array_dde

    .line 62
    .local v37, "byteReplace2":[B
    const/16 v3, 0x17

    new-array v0, v3, [B

    move-object/from16 v95, v0

    fill-array-data v95, :array_dee

    .line 69
    .local v95, "rep_mask2":[B
    const/16 v3, 0x15

    new-array v0, v3, [B

    move-object/from16 v22, v0

    fill-array-data v22, :array_dfe

    .line 70
    .local v22, "byteOrig4":[B
    const/16 v3, 0x15

    new-array v0, v3, [B

    move-object/from16 v72, v0

    fill-array-data v72, :array_e0e

    .line 71
    .local v72, "mask4":[B
    const/16 v3, 0x15

    new-array v0, v3, [B

    move-object/from16 v39, v0

    fill-array-data v39, :array_e1e

    .line 72
    .local v39, "byteReplace4":[B
    const/16 v3, 0x15

    new-array v0, v3, [B

    move-object/from16 v97, v0

    fill-array-data v97, :array_e2e

    .line 74
    .local v97, "rep_mask4":[B
    const/16 v3, 0x29

    new-array v0, v3, [B

    move-object/from16 v23, v0

    fill-array-data v23, :array_e3e

    .line 75
    .local v23, "byteOrig5":[B
    const/16 v3, 0x29

    new-array v0, v3, [B

    move-object/from16 v73, v0

    fill-array-data v73, :array_e58

    .line 76
    .local v73, "mask5":[B
    const/16 v3, 0x29

    new-array v0, v3, [B

    move-object/from16 v40, v0

    fill-array-data v40, :array_e72

    .line 77
    .local v40, "byteReplace5":[B
    const/16 v3, 0x29

    new-array v0, v3, [B

    move-object/from16 v98, v0

    fill-array-data v98, :array_e8c

    .line 79
    .local v98, "rep_mask5":[B
    const/16 v3, 0x16

    new-array v0, v3, [B

    move-object/from16 v24, v0

    fill-array-data v24, :array_ea6

    .line 80
    .local v24, "byteOrig6":[B
    const/16 v3, 0x16

    new-array v0, v3, [B

    move-object/from16 v74, v0

    fill-array-data v74, :array_eb6

    .line 81
    .local v74, "mask6":[B
    const/16 v3, 0x16

    new-array v0, v3, [B

    move-object/from16 v41, v0

    fill-array-data v41, :array_ec6

    .line 82
    .local v41, "byteReplace6":[B
    const/16 v3, 0x16

    new-array v0, v3, [B

    move-object/from16 v99, v0

    fill-array-data v99, :array_ed6

    .line 84
    .local v99, "rep_mask6":[B
    const/16 v3, 0x22

    new-array v0, v3, [B

    move-object/from16 v25, v0

    fill-array-data v25, :array_ee6

    .line 85
    .local v25, "byteOrig7":[B
    const/16 v3, 0x22

    new-array v0, v3, [B

    move-object/from16 v75, v0

    fill-array-data v75, :array_efc

    .line 86
    .local v75, "mask7":[B
    const/16 v3, 0x22

    new-array v0, v3, [B

    move-object/from16 v42, v0

    fill-array-data v42, :array_f12

    .line 87
    .local v42, "byteReplace7":[B
    const/16 v3, 0x22

    new-array v0, v3, [B

    move-object/from16 v100, v0

    fill-array-data v100, :array_f28

    .line 96
    .local v100, "rep_mask7":[B
    const/16 v3, 0xc

    new-array v0, v3, [B

    move-object/from16 v26, v0

    fill-array-data v26, :array_f3e

    .line 97
    .local v26, "byteOrig9":[B
    const/16 v3, 0xc

    new-array v0, v3, [B

    move-object/from16 v76, v0

    fill-array-data v76, :array_f48

    .line 98
    .local v76, "mask9":[B
    const/16 v3, 0xc

    new-array v0, v3, [B

    move-object/from16 v43, v0

    fill-array-data v43, :array_f52

    .line 99
    .local v43, "byteReplace9":[B
    const/16 v3, 0xc

    new-array v0, v3, [B

    move-object/from16 v101, v0

    fill-array-data v101, :array_f5c

    .line 101
    .local v101, "rep_mask9":[B
    const/16 v3, 0x49

    new-array v11, v3, [B

    fill-array-data v11, :array_f66

    .line 102
    .local v11, "byteOrig10":[B
    const/16 v3, 0x49

    new-array v0, v3, [B

    move-object/from16 v61, v0

    fill-array-data v61, :array_f90

    .line 103
    .local v61, "mask10":[B
    const/16 v3, 0x49

    new-array v0, v3, [B

    move-object/from16 v28, v0

    fill-array-data v28, :array_fba

    .line 104
    .local v28, "byteReplace10":[B
    const/16 v3, 0x49

    new-array v0, v3, [B

    move-object/from16 v86, v0

    fill-array-data v86, :array_fe4

    .line 107
    .local v86, "rep_mask10":[B
    const/16 v3, 0x47

    new-array v12, v3, [B

    fill-array-data v12, :array_100e

    .line 108
    .local v12, "byteOrig12":[B
    const/16 v3, 0x47

    new-array v0, v3, [B

    move-object/from16 v62, v0

    fill-array-data v62, :array_1036

    .line 109
    .local v62, "mask12":[B
    const/16 v3, 0x47

    new-array v0, v3, [B

    move-object/from16 v29, v0

    fill-array-data v29, :array_105e

    .line 110
    .local v29, "byteReplace12":[B
    const/16 v3, 0x47

    new-array v0, v3, [B

    move-object/from16 v87, v0

    fill-array-data v87, :array_1086

    .line 113
    .local v87, "rep_mask12":[B
    const/16 v3, 0x45

    new-array v13, v3, [B

    fill-array-data v13, :array_10ae

    .line 114
    .local v13, "byteOrig13":[B
    const/16 v3, 0x45

    new-array v0, v3, [B

    move-object/from16 v63, v0

    fill-array-data v63, :array_10d6

    .line 115
    .local v63, "mask13":[B
    const/16 v3, 0x45

    new-array v0, v3, [B

    move-object/from16 v30, v0

    fill-array-data v30, :array_10fe

    .line 116
    .local v30, "byteReplace13":[B
    const/16 v3, 0x45

    new-array v0, v3, [B

    move-object/from16 v88, v0

    fill-array-data v88, :array_1126

    .line 118
    .local v88, "rep_mask13":[B
    const/16 v3, 0x2d

    new-array v14, v3, [B

    fill-array-data v14, :array_114e

    .line 119
    .local v14, "byteOrig14":[B
    const/16 v3, 0x2d

    new-array v0, v3, [B

    move-object/from16 v64, v0

    fill-array-data v64, :array_116a

    .line 120
    .local v64, "mask14":[B
    const/16 v3, 0x2d

    new-array v0, v3, [B

    move-object/from16 v31, v0

    fill-array-data v31, :array_1186

    .line 121
    .local v31, "byteReplace14":[B
    const/16 v3, 0x2d

    new-array v0, v3, [B

    move-object/from16 v89, v0

    fill-array-data v89, :array_11a2

    .line 124
    .local v89, "rep_mask14":[B
    const/16 v3, 0x3d

    new-array v15, v3, [B

    fill-array-data v15, :array_11be

    .line 125
    .local v15, "byteOrig15":[B
    const/16 v3, 0x3d

    new-array v0, v3, [B

    move-object/from16 v65, v0

    fill-array-data v65, :array_11e2

    .line 126
    .local v65, "mask15":[B
    const/16 v3, 0x3d

    new-array v0, v3, [B

    move-object/from16 v32, v0

    fill-array-data v32, :array_1206

    .line 127
    .local v32, "byteReplace15":[B
    const/16 v3, 0x3d

    new-array v0, v3, [B

    move-object/from16 v90, v0

    fill-array-data v90, :array_122a

    .line 129
    .local v90, "rep_mask15":[B
    const/16 v3, 0x20

    new-array v0, v3, [B

    move-object/from16 v16, v0

    fill-array-data v16, :array_124e

    .line 130
    .local v16, "byteOrig16":[B
    const/16 v3, 0x20

    new-array v0, v3, [B

    move-object/from16 v66, v0

    fill-array-data v66, :array_1262

    .line 131
    .local v66, "mask16":[B
    const/16 v3, 0x20

    new-array v0, v3, [B

    move-object/from16 v33, v0

    fill-array-data v33, :array_1276

    .line 132
    .local v33, "byteReplace16":[B
    const/16 v3, 0x20

    new-array v0, v3, [B

    move-object/from16 v91, v0

    fill-array-data v91, :array_128a

    .line 134
    .local v91, "rep_mask16":[B
    const/16 v3, 0xd

    new-array v0, v3, [B

    move-object/from16 v17, v0

    fill-array-data v17, :array_129e

    .line 135
    .local v17, "byteOrig17":[B
    const/16 v3, 0xd

    new-array v0, v3, [B

    move-object/from16 v67, v0

    fill-array-data v67, :array_12aa

    .line 136
    .local v67, "mask17":[B
    const/16 v3, 0xd

    new-array v0, v3, [B

    move-object/from16 v34, v0

    fill-array-data v34, :array_12b6

    .line 137
    .local v34, "byteReplace17":[B
    const/16 v3, 0xd

    new-array v0, v3, [B

    move-object/from16 v92, v0

    fill-array-data v92, :array_12c2

    .line 140
    .local v92, "rep_mask17":[B
    const/16 v3, 0x2d

    new-array v0, v3, [B

    move-object/from16 v18, v0

    fill-array-data v18, :array_12ce

    .line 141
    .local v18, "byteOrig18":[B
    const/16 v3, 0x2d

    new-array v0, v3, [B

    move-object/from16 v68, v0

    fill-array-data v68, :array_12ea

    .line 142
    .local v68, "mask18":[B
    const/16 v3, 0x2d

    new-array v0, v3, [B

    move-object/from16 v35, v0

    fill-array-data v35, :array_1306

    .line 143
    .local v35, "byteReplace18":[B
    const/16 v3, 0x2d

    new-array v0, v3, [B

    move-object/from16 v93, v0

    fill-array-data v93, :array_1322

    .line 145
    .local v93, "rep_mask18":[B
    const/16 v3, 0x19

    new-array v0, v3, [B

    move-object/from16 v19, v0

    fill-array-data v19, :array_133e

    .line 146
    .local v19, "byteOrig19":[B
    const/16 v3, 0x19

    new-array v0, v3, [B

    move-object/from16 v69, v0

    fill-array-data v69, :array_1350

    .line 147
    .local v69, "mask19":[B
    const/16 v3, 0x19

    new-array v0, v3, [B

    move-object/from16 v36, v0

    fill-array-data v36, :array_1362

    .line 148
    .local v36, "byteReplace19":[B
    const/16 v3, 0x19

    new-array v0, v3, [B

    move-object/from16 v94, v0

    fill-array-data v94, :array_1374

    .line 150
    .local v94, "rep_mask19":[B
    const/16 v3, 0xa

    new-array v0, v3, [B

    move-object/from16 v21, v0

    fill-array-data v21, :array_1386

    .line 151
    .local v21, "byteOrig20":[B
    const/16 v3, 0xa

    new-array v0, v3, [B

    move-object/from16 v71, v0

    fill-array-data v71, :array_1390

    .line 152
    .local v71, "mask20":[B
    const/16 v3, 0xa

    new-array v0, v3, [B

    move-object/from16 v38, v0

    fill-array-data v38, :array_139a

    .line 153
    .local v38, "byteReplace20":[B
    const/16 v3, 0xa

    new-array v0, v3, [B

    move-object/from16 v96, v0

    fill-array-data v96, :array_13a4

    .line 161
    .local v96, "rep_mask20":[B
    sget-object v3, Lcom/chelpus/root/utils/runpatch;->dalvikDexIn:Ljava/lang/String;

    const-string v4, "zamenitetodelo"

    const/4 v5, 0x0

    aget-object v5, p0, v5

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v48

    .line 165
    .local v48, "dalvikDex":Ljava/lang/String;
    :try_start_30a
    new-instance v55, Ljava/io/File;

    move-object/from16 v0, v55

    move-object/from16 v1, v48

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 166
    .local v55, "localFile1":Ljava/io/File;
    invoke-virtual/range {v55 .. v55}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_32a

    new-instance v55, Ljava/io/File;

    .end local v55    # "localFile1":Ljava/io/File;
    const-string v3, "-1"

    const-string v4, "-2"

    move-object/from16 v0, v48

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v55

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 167
    .restart local v55    # "localFile1":Ljava/io/File;
    :cond_32a
    new-instance v56, Ljava/io/File;

    const-string v3, "-1"

    const-string v4, ""

    move-object/from16 v0, v48

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v56

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 168
    .restart local v56    # "localFile2":Ljava/io/File;
    invoke-virtual/range {v55 .. v55}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_343

    move-object/from16 v56, v55

    .line 169
    :cond_343
    const-string v3, "data@app"

    const-string v4, "mnt@asec"

    move-object/from16 v0, v48

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v49

    .line 170
    .local v49, "dalvikDexTemp":Ljava/lang/String;
    const-string v3, ".apk@classes.dex"

    const-string v4, "@pkg.apk@classes.dex"

    move-object/from16 v0, v49

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v49

    .line 171
    new-instance v55, Ljava/io/File;

    .end local v55    # "localFile1":Ljava/io/File;
    move-object/from16 v0, v55

    move-object/from16 v1, v49

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 172
    .restart local v55    # "localFile1":Ljava/io/File;
    invoke-virtual/range {v55 .. v55}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_368

    move-object/from16 v56, v55

    .line 173
    :cond_368
    new-instance v55, Ljava/io/File;

    .end local v55    # "localFile1":Ljava/io/File;
    const-string v3, "-1"

    const-string v4, "-2"

    move-object/from16 v0, v49

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v55

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 174
    .restart local v55    # "localFile1":Ljava/io/File;
    invoke-virtual/range {v55 .. v55}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_381

    move-object/from16 v56, v55

    .line 175
    :cond_381
    new-instance v55, Ljava/io/File;

    .end local v55    # "localFile1":Ljava/io/File;
    const-string v3, "-1"

    const-string v4, ""

    move-object/from16 v0, v49

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v55

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 176
    .restart local v55    # "localFile1":Ljava/io/File;
    invoke-virtual/range {v55 .. v55}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_39a

    move-object/from16 v56, v55

    .line 178
    :cond_39a
    sget-object v3, Lcom/chelpus/root/utils/runpatch;->dalvikDexIn2:Ljava/lang/String;

    const-string v4, "zamenitetodelo"

    const/4 v5, 0x0

    aget-object v5, p0, v5

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v48

    .line 179
    new-instance v55, Ljava/io/File;

    .end local v55    # "localFile1":Ljava/io/File;
    move-object/from16 v0, v55

    move-object/from16 v1, v48

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 180
    .restart local v55    # "localFile1":Ljava/io/File;
    invoke-virtual/range {v55 .. v55}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_3c5

    new-instance v55, Ljava/io/File;

    .end local v55    # "localFile1":Ljava/io/File;
    const-string v3, "-1"

    const-string v4, "-2"

    move-object/from16 v0, v48

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v55

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 182
    .restart local v55    # "localFile1":Ljava/io/File;
    :cond_3c5
    invoke-virtual/range {v55 .. v55}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_3dc

    new-instance v55, Ljava/io/File;

    .end local v55    # "localFile1":Ljava/io/File;
    const-string v3, "-1"

    const-string v4, ""

    move-object/from16 v0, v48

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v55

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 183
    .restart local v55    # "localFile1":Ljava/io/File;
    :cond_3dc
    invoke-virtual/range {v55 .. v55}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_3e4

    move-object/from16 v56, v55

    .line 184
    :cond_3e4
    const-string v3, "data@app"

    const-string v4, "mnt@asec"

    move-object/from16 v0, v48

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v49

    .line 185
    const-string v3, ".apk@classes.dex"

    const-string v4, "@pkg.apk@classes.dex"

    move-object/from16 v0, v49

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v49

    .line 186
    new-instance v55, Ljava/io/File;

    .end local v55    # "localFile1":Ljava/io/File;
    move-object/from16 v0, v55

    move-object/from16 v1, v49

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 187
    .restart local v55    # "localFile1":Ljava/io/File;
    invoke-virtual/range {v55 .. v55}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_409

    move-object/from16 v56, v55

    .line 188
    :cond_409
    new-instance v55, Ljava/io/File;

    .end local v55    # "localFile1":Ljava/io/File;
    const-string v3, "-1"

    const-string v4, "-2"

    move-object/from16 v0, v49

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v55

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 189
    .restart local v55    # "localFile1":Ljava/io/File;
    invoke-virtual/range {v55 .. v55}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_422

    move-object/from16 v56, v55

    .line 190
    :cond_422
    new-instance v55, Ljava/io/File;

    .end local v55    # "localFile1":Ljava/io/File;
    const-string v3, "-1"

    const-string v4, ""

    move-object/from16 v0, v49

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v55

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 191
    .restart local v55    # "localFile1":Ljava/io/File;
    invoke-virtual/range {v55 .. v55}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_43b

    move-object/from16 v56, v55

    .line 194
    :cond_43b
    sget-object v3, Lcom/chelpus/root/utils/runpatch;->dalvikDexIn:Ljava/lang/String;

    const-string v4, "zamenitetodelo"

    const/4 v5, 0x0

    aget-object v5, p0, v5

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v48

    .line 195
    const-string v3, "/data/"

    const-string v4, "/sd-ext/data/"

    move-object/from16 v0, v48

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v48

    .line 196
    new-instance v55, Ljava/io/File;

    .end local v55    # "localFile1":Ljava/io/File;
    move-object/from16 v0, v55

    move-object/from16 v1, v48

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 197
    .restart local v55    # "localFile1":Ljava/io/File;
    invoke-virtual/range {v55 .. v55}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_470

    new-instance v55, Ljava/io/File;

    .end local v55    # "localFile1":Ljava/io/File;
    const-string v3, "-1"

    const-string v4, "-2"

    move-object/from16 v0, v48

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v55

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 199
    .restart local v55    # "localFile1":Ljava/io/File;
    :cond_470
    invoke-virtual/range {v55 .. v55}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_487

    new-instance v55, Ljava/io/File;

    .end local v55    # "localFile1":Ljava/io/File;
    const-string v3, "-1"

    const-string v4, ""

    move-object/from16 v0, v48

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v55

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 200
    .restart local v55    # "localFile1":Ljava/io/File;
    :cond_487
    invoke-virtual/range {v55 .. v55}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_48f

    move-object/from16 v56, v55

    .line 201
    :cond_48f
    const-string v3, "data@app"

    const-string v4, "mnt@asec"

    move-object/from16 v0, v48

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v49

    .line 202
    const-string v3, ".apk@classes.dex"

    const-string v4, "@pkg.apk@classes.dex"

    move-object/from16 v0, v49

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v49

    .line 203
    new-instance v55, Ljava/io/File;

    .end local v55    # "localFile1":Ljava/io/File;
    move-object/from16 v0, v55

    move-object/from16 v1, v49

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 204
    .restart local v55    # "localFile1":Ljava/io/File;
    invoke-virtual/range {v55 .. v55}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_4b4

    move-object/from16 v56, v55

    .line 205
    :cond_4b4
    new-instance v55, Ljava/io/File;

    .end local v55    # "localFile1":Ljava/io/File;
    const-string v3, "-1"

    const-string v4, "-2"

    move-object/from16 v0, v49

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v55

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 206
    .restart local v55    # "localFile1":Ljava/io/File;
    invoke-virtual/range {v55 .. v55}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_4cd

    move-object/from16 v56, v55

    .line 207
    :cond_4cd
    new-instance v55, Ljava/io/File;

    .end local v55    # "localFile1":Ljava/io/File;
    const-string v3, "-1"

    const-string v4, ""

    move-object/from16 v0, v49

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v55

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 208
    .restart local v55    # "localFile1":Ljava/io/File;
    invoke-virtual/range {v55 .. v55}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_4e6

    move-object/from16 v56, v55

    .line 211
    :cond_4e6
    sget-object v3, Lcom/chelpus/root/utils/runpatch;->dalvikDexIn2:Ljava/lang/String;

    const-string v4, "zamenitetodelo"

    const/4 v5, 0x0

    aget-object v5, p0, v5

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v48

    .line 212
    const-string v3, "/cache/"

    const-string v4, "/sd-ext/data/cache/"

    move-object/from16 v0, v48

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v48

    .line 213
    new-instance v55, Ljava/io/File;

    .end local v55    # "localFile1":Ljava/io/File;
    move-object/from16 v0, v55

    move-object/from16 v1, v48

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 214
    .restart local v55    # "localFile1":Ljava/io/File;
    invoke-virtual/range {v55 .. v55}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_51b

    new-instance v55, Ljava/io/File;

    .end local v55    # "localFile1":Ljava/io/File;
    const-string v3, "-1"

    const-string v4, "-2"

    move-object/from16 v0, v48

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v55

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 216
    .restart local v55    # "localFile1":Ljava/io/File;
    :cond_51b
    invoke-virtual/range {v55 .. v55}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_532

    new-instance v55, Ljava/io/File;

    .end local v55    # "localFile1":Ljava/io/File;
    const-string v3, "-1"

    const-string v4, ""

    move-object/from16 v0, v48

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v55

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 217
    .restart local v55    # "localFile1":Ljava/io/File;
    :cond_532
    invoke-virtual/range {v55 .. v55}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_53a

    move-object/from16 v56, v55

    .line 218
    :cond_53a
    const-string v3, "data@app"

    const-string v4, "mnt@asec"

    move-object/from16 v0, v48

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v49

    .line 219
    const-string v3, ".apk@classes.dex"

    const-string v4, "@pkg.apk@classes.dex"

    move-object/from16 v0, v49

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v49

    .line 220
    new-instance v55, Ljava/io/File;

    .end local v55    # "localFile1":Ljava/io/File;
    move-object/from16 v0, v55

    move-object/from16 v1, v49

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 221
    .restart local v55    # "localFile1":Ljava/io/File;
    invoke-virtual/range {v55 .. v55}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_55f

    move-object/from16 v56, v55

    .line 222
    :cond_55f
    new-instance v55, Ljava/io/File;

    .end local v55    # "localFile1":Ljava/io/File;
    const-string v3, "-1"

    const-string v4, "-2"

    move-object/from16 v0, v49

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v55

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 223
    .restart local v55    # "localFile1":Ljava/io/File;
    invoke-virtual/range {v55 .. v55}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_578

    move-object/from16 v56, v55

    .line 224
    :cond_578
    new-instance v55, Ljava/io/File;

    .end local v55    # "localFile1":Ljava/io/File;
    const-string v3, "-1"

    const-string v4, ""

    move-object/from16 v0, v49

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v55

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 225
    .restart local v55    # "localFile1":Ljava/io/File;
    invoke-virtual/range {v55 .. v55}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_591

    move-object/from16 v56, v55

    .line 228
    :cond_591
    sget-boolean v3, Lcom/chelpus/root/utils/runpatch;->system:Z

    if-eqz v3, :cond_612

    .line 229
    new-instance v104, Ljava/io/File;

    sget-object v3, Lcom/chelpus/root/utils/runpatch;->dirapp:Ljava/lang/String;

    move-object/from16 v0, v104

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 230
    .local v104, "temp":Ljava/io/File;
    invoke-virtual/range {v104 .. v104}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v103

    .line 232
    .local v103, "sysapkname":Ljava/lang/String;
    sget-object v3, Lcom/chelpus/root/utils/runpatch;->dalvikDexIn:Ljava/lang/String;

    const-string v4, "zamenitetodelo-1.apk"

    move-object/from16 v0, v103

    invoke-virtual {v3, v4, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v48

    .line 233
    const-string v3, "data@app"

    const-string v4, "system@app"

    move-object/from16 v0, v48

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v49

    .line 235
    new-instance v55, Ljava/io/File;

    .end local v55    # "localFile1":Ljava/io/File;
    move-object/from16 v0, v55

    move-object/from16 v1, v49

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 236
    .restart local v55    # "localFile1":Ljava/io/File;
    invoke-virtual/range {v55 .. v55}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_5c7

    move-object/from16 v56, v55

    .line 237
    :cond_5c7
    new-instance v55, Ljava/io/File;

    .end local v55    # "localFile1":Ljava/io/File;
    const-string v3, "/data/dalvik-cache/"

    const-string v4, "/sd-ext/data/dalvik-cache/"

    move-object/from16 v0, v49

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v55

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 238
    .restart local v55    # "localFile1":Ljava/io/File;
    invoke-virtual/range {v55 .. v55}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_5e0

    move-object/from16 v56, v55

    .line 239
    :cond_5e0
    new-instance v55, Ljava/io/File;

    .end local v55    # "localFile1":Ljava/io/File;
    const-string v3, "/data/dalvik-cache/"

    const-string v4, "/sd-ext/data/cache/dalvik-cache/"

    move-object/from16 v0, v49

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v55

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 240
    .restart local v55    # "localFile1":Ljava/io/File;
    invoke-virtual/range {v55 .. v55}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_5f9

    move-object/from16 v56, v55

    .line 241
    :cond_5f9
    new-instance v55, Ljava/io/File;

    .end local v55    # "localFile1":Ljava/io/File;
    const-string v3, "/data/dalvik-cache/"

    const-string v4, "/cache/dalvik-cache/"

    move-object/from16 v0, v49

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v55

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 242
    .restart local v55    # "localFile1":Ljava/io/File;
    invoke-virtual/range {v55 .. v55}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_612

    move-object/from16 v56, v55

    .line 247
    .end local v103    # "sysapkname":Ljava/lang/String;
    .end local v104    # "temp":Ljava/io/File;
    :cond_612
    sget-boolean v3, Lcom/chelpus/root/utils/runpatch;->odexpatch:Z

    if-eqz v3, :cond_618

    move-object/from16 v56, v9

    .line 248
    :cond_618
    invoke-virtual/range {v56 .. v56}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_630

    new-instance v3, Ljava/io/FileNotFoundException;

    invoke-direct {v3}, Ljava/io/FileNotFoundException;-><init>()V

    throw v3
    :try_end_624
    .catch Ljava/io/FileNotFoundException; {:try_start_30a .. :try_end_624} :catch_624
    .catch Ljava/lang/Exception; {:try_start_30a .. :try_end_624} :catch_d4e

    .line 825
    .end local v49    # "dalvikDexTemp":Ljava/lang/String;
    .end local v55    # "localFile1":Ljava/io/File;
    .end local v56    # "localFile2":Ljava/io/File;
    :catch_624
    move-exception v57

    .line 826
    .local v57, "localFileNotFoundException":Ljava/io/FileNotFoundException;
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "Error: Program files are not found!\n\nCheck the location dalvik-cache to solve problems!\n\nDefault: /data/dalvik-cache/*"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 833
    .end local v57    # "localFileNotFoundException":Ljava/io/FileNotFoundException;
    :cond_62c
    :goto_62c
    invoke-static {}, Lcom/chelpus/Utils;->exitFromRootJava()V

    .line 834
    return-void

    .line 250
    .restart local v49    # "dalvikDexTemp":Ljava/lang/String;
    .restart local v55    # "localFile1":Ljava/io/File;
    .restart local v56    # "localFile2":Ljava/io/File;
    :cond_630
    :try_start_630
    new-instance v3, Ljava/io/RandomAccessFile;

    const-string v4, "rw"

    move-object/from16 v0, v56

    invoke-direct {v3, v0, v4}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v2

    .line 251
    .local v2, "ChannelDex":Ljava/nio/channels/FileChannel;
    sget-object v3, Ljava/nio/channels/FileChannel$MapMode;->READ_WRITE:Ljava/nio/channels/FileChannel$MapMode;

    const-wide/16 v4, 0x0

    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v6

    long-to-int v6, v6

    int-to-long v6, v6

    invoke-virtual/range {v2 .. v7}, Ljava/nio/channels/FileChannel;->map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;
    :try_end_64a
    .catch Ljava/io/FileNotFoundException; {:try_start_630 .. :try_end_64a} :catch_624
    .catch Ljava/lang/Exception; {:try_start_630 .. :try_end_64a} :catch_d4e

    move-result-object v51

    .line 252
    .local v51, "fileBytes":Ljava/nio/MappedByteBuffer;
    const/16 v77, 0x0

    .line 253
    .local v77, "patch":Z
    const/16 v78, 0x0

    .line 254
    .local v78, "patch1":Z
    const/16 v79, 0x0

    .line 255
    .local v79, "patch2":Z
    const/16 v80, 0x0

    .line 256
    .local v80, "patch4":Z
    const/16 v81, 0x0

    .line 257
    .local v81, "patch5":Z
    const/16 v82, 0x0

    .line 258
    .local v82, "patch6":Z
    const/16 v83, 0x0

    .line 259
    .local v83, "patch7":Z
    const/16 v58, 0x0

    .line 260
    .local v58, "mark5":Z
    const/16 v59, 0x0

    .line 261
    .local v59, "mark6":Z
    const/16 v47, 0x0

    .line 263
    .local v47, "curentPos2":I
    const/16 v102, 0x0

    .line 265
    .local v102, "repbyte":B
    const-wide/16 v53, 0x0

    .local v53, "j":J
    :goto_663
    :try_start_663
    invoke-virtual/range {v51 .. v51}, Ljava/nio/MappedByteBuffer;->hasRemaining()Z

    move-result v3

    if-eqz v3, :cond_d0a

    .line 268
    invoke-virtual/range {v51 .. v51}, Ljava/nio/MappedByteBuffer;->position()I

    move-result v46

    .line 269
    .local v46, "curentPos":I
    invoke-virtual/range {v51 .. v51}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v45

    .line 270
    .local v45, "curentByte":B
    if-nez v58, :cond_675

    if-eqz v59, :cond_677

    :cond_675
    add-int/lit8 v47, v47, 0x1

    .line 271
    :cond_677
    const/16 v3, 0x17c

    move/from16 v0, v47

    if-le v0, v3, :cond_681

    const/16 v58, 0x0

    const/16 v47, 0x0

    .line 274
    :cond_681
    const/4 v3, 0x0

    aget-byte v3, v10, v3

    move/from16 v0, v45

    if-ne v0, v3, :cond_6f7

    sget-boolean v3, Lcom/chelpus/root/utils/runpatch;->pattern1:Z

    if-eqz v3, :cond_6f7

    .line 276
    const/4 v3, 0x0

    aget-byte v3, v85, v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_694

    move/from16 v102, v45

    .line 277
    :cond_694
    const/4 v3, 0x0

    aget-byte v3, v85, v3

    if-nez v3, :cond_69c

    const/4 v3, 0x0

    aput-byte v45, v27, v3

    .line 278
    :cond_69c
    const/16 v52, 0x1

    .local v52, "i":I
    const/16 v44, 0x3e8

    .line 279
    .local v44, "c":I
    add-int/lit8 v3, v46, 0x1

    move-object/from16 v0, v51

    invoke-virtual {v0, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 280
    invoke-virtual/range {v51 .. v51}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v84

    .line 282
    .local v84, "prufbyte":B
    :goto_6ab
    aget-byte v3, v10, v52

    move/from16 v0, v84

    if-eq v0, v3, :cond_6b5

    aget-byte v3, v60, v52

    if-eqz v3, :cond_6f7

    .line 284
    :cond_6b5
    aget-byte v3, v85, v52

    const/4 v4, 0x2

    if-ne v3, v4, :cond_6bc

    move/from16 v102, v84

    .line 285
    :cond_6bc
    aget-byte v3, v85, v52

    if-eqz v3, :cond_6c5

    aget-byte v3, v85, v52

    const/4 v4, 0x2

    if-ne v3, v4, :cond_6c7

    :cond_6c5
    aput-byte v84, v27, v52

    .line 286
    :cond_6c7
    aget-byte v3, v85, v52

    const/4 v4, 0x3

    if-ne v3, v4, :cond_6ce

    move/from16 v44, v52

    .line 287
    :cond_6ce
    add-int/lit8 v52, v52, 0x1

    .line 288
    array-length v3, v10

    move/from16 v0, v52

    if-ne v0, v3, :cond_c89

    .line 290
    const/16 v3, 0x3e8

    move/from16 v0, v44

    if-ge v0, v3, :cond_6dd

    aput-byte v102, v27, v44

    .line 291
    :cond_6dd
    move-object/from16 v0, v51

    move/from16 v1, v46

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 292
    move-object/from16 v0, v51

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 293
    invoke-virtual/range {v51 .. v51}, Ljava/nio/MappedByteBuffer;->force()Ljava/nio/MappedByteBuffer;

    .line 295
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "Check License Key Fixed!\n"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 296
    const/16 v80, 0x1

    .line 306
    .end local v44    # "c":I
    .end local v52    # "i":I
    .end local v84    # "prufbyte":B
    :cond_6f7
    const/4 v3, 0x0

    aget-byte v3, v20, v3

    move/from16 v0, v45

    if-ne v0, v3, :cond_752

    sget-boolean v3, Lcom/chelpus/root/utils/runpatch;->pattern3:Z

    if-eqz v3, :cond_752

    .line 308
    const/4 v3, 0x0

    aget-byte v3, v95, v3

    if-nez v3, :cond_70a

    const/4 v3, 0x0

    aput-byte v45, v37, v3

    .line 309
    :cond_70a
    const/16 v52, 0x1

    .line 310
    .restart local v52    # "i":I
    add-int/lit8 v3, v46, 0x1

    move-object/from16 v0, v51

    invoke-virtual {v0, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 311
    invoke-virtual/range {v51 .. v51}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v84

    .line 313
    .restart local v84    # "prufbyte":B
    :goto_717
    aget-byte v3, v20, v52

    move/from16 v0, v84

    if-eq v0, v3, :cond_722

    aget-byte v3, v70, v52

    const/4 v4, 0x1

    if-ne v3, v4, :cond_74b

    .line 315
    :cond_722
    aget-byte v3, v95, v52

    if-nez v3, :cond_728

    aput-byte v84, v37, v52

    .line 316
    :cond_728
    add-int/lit8 v52, v52, 0x1

    .line 318
    move-object/from16 v0, v20

    array-length v3, v0

    move/from16 v0, v52

    if-ne v0, v3, :cond_c8f

    .line 320
    move-object/from16 v0, v51

    move/from16 v1, v46

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 321
    move-object/from16 v0, v51

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 322
    invoke-virtual/range {v51 .. v51}, Ljava/nio/MappedByteBuffer;->force()Ljava/nio/MappedByteBuffer;

    .line 324
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "License Key Fixed2!\n"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 325
    const/16 v78, 0x1

    .line 333
    :cond_74b
    add-int/lit8 v3, v46, 0x1

    move-object/from16 v0, v51

    invoke-virtual {v0, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 361
    .end local v52    # "i":I
    .end local v84    # "prufbyte":B
    :cond_752
    const/4 v3, 0x0

    aget-byte v3, v22, v3

    move/from16 v0, v45

    if-ne v0, v3, :cond_7ad

    sget-boolean v3, Lcom/chelpus/root/utils/runpatch;->pattern2:Z

    if-eqz v3, :cond_7ad

    .line 363
    const/4 v3, 0x0

    aget-byte v3, v97, v3

    if-nez v3, :cond_765

    const/4 v3, 0x0

    aput-byte v45, v39, v3

    .line 364
    :cond_765
    const/16 v52, 0x1

    .line 365
    .restart local v52    # "i":I
    add-int/lit8 v3, v46, 0x1

    move-object/from16 v0, v51

    invoke-virtual {v0, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 366
    invoke-virtual/range {v51 .. v51}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v84

    .line 368
    .restart local v84    # "prufbyte":B
    :goto_772
    aget-byte v3, v22, v52

    move/from16 v0, v84

    if-eq v0, v3, :cond_77d

    aget-byte v3, v72, v52

    const/4 v4, 0x1

    if-ne v3, v4, :cond_7a6

    .line 370
    :cond_77d
    aget-byte v3, v97, v52

    if-nez v3, :cond_783

    aput-byte v84, v39, v52

    .line 371
    :cond_783
    add-int/lit8 v52, v52, 0x1

    .line 372
    move-object/from16 v0, v22

    array-length v3, v0

    move/from16 v0, v52

    if-ne v0, v3, :cond_c95

    .line 374
    move-object/from16 v0, v51

    move/from16 v1, v46

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 375
    move-object/from16 v0, v51

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 376
    invoke-virtual/range {v51 .. v51}, Ljava/nio/MappedByteBuffer;->force()Ljava/nio/MappedByteBuffer;

    .line 378
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "Cached License Key Fixed!\n"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 380
    const/16 v77, 0x1

    .line 385
    :cond_7a6
    add-int/lit8 v3, v46, 0x1

    move-object/from16 v0, v51

    invoke-virtual {v0, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 387
    .end local v52    # "i":I
    .end local v84    # "prufbyte":B
    :cond_7ad
    const/4 v3, 0x0

    aget-byte v3, v24, v3

    move/from16 v0, v45

    if-ne v0, v3, :cond_808

    sget-boolean v3, Lcom/chelpus/root/utils/runpatch;->pattern2:Z

    if-eqz v3, :cond_808

    .line 389
    const/4 v3, 0x0

    aget-byte v3, v99, v3

    if-nez v3, :cond_7c0

    const/4 v3, 0x0

    aput-byte v45, v41, v3

    .line 390
    :cond_7c0
    const/16 v52, 0x1

    .line 391
    .restart local v52    # "i":I
    add-int/lit8 v3, v46, 0x1

    move-object/from16 v0, v51

    invoke-virtual {v0, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 392
    invoke-virtual/range {v51 .. v51}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v84

    .line 394
    .restart local v84    # "prufbyte":B
    :goto_7cd
    aget-byte v3, v24, v52

    move/from16 v0, v84

    if-eq v0, v3, :cond_7d8

    aget-byte v3, v74, v52

    const/4 v4, 0x1

    if-ne v3, v4, :cond_801

    .line 396
    :cond_7d8
    aget-byte v3, v99, v52

    if-nez v3, :cond_7de

    aput-byte v84, v41, v52

    .line 397
    :cond_7de
    add-int/lit8 v52, v52, 0x1

    .line 399
    move-object/from16 v0, v24

    array-length v3, v0

    move/from16 v0, v52

    if-ne v0, v3, :cond_c9b

    .line 401
    move-object/from16 v0, v51

    move/from16 v1, v46

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 402
    move-object/from16 v0, v51

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 403
    invoke-virtual/range {v51 .. v51}, Ljava/nio/MappedByteBuffer;->force()Ljava/nio/MappedByteBuffer;

    .line 405
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "Internet Connection Fixed!\n"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 406
    const/16 v79, 0x1

    .line 411
    :cond_801
    add-int/lit8 v3, v46, 0x1

    move-object/from16 v0, v51

    invoke-virtual {v0, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 413
    .end local v52    # "i":I
    .end local v84    # "prufbyte":B
    :cond_808
    const/4 v3, 0x0

    aget-byte v3, v19, v3

    move/from16 v0, v45

    if-ne v0, v3, :cond_863

    sget-boolean v3, Lcom/chelpus/root/utils/runpatch;->pattern2:Z

    if-eqz v3, :cond_863

    .line 415
    const/4 v3, 0x0

    aget-byte v3, v94, v3

    if-nez v3, :cond_81b

    const/4 v3, 0x0

    aput-byte v45, v36, v3

    .line 416
    :cond_81b
    const/16 v52, 0x1

    .line 417
    .restart local v52    # "i":I
    add-int/lit8 v3, v46, 0x1

    move-object/from16 v0, v51

    invoke-virtual {v0, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 418
    invoke-virtual/range {v51 .. v51}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v84

    .line 420
    .restart local v84    # "prufbyte":B
    :goto_828
    aget-byte v3, v19, v52

    move/from16 v0, v84

    if-eq v0, v3, :cond_833

    aget-byte v3, v69, v52

    const/4 v4, 0x1

    if-ne v3, v4, :cond_85c

    .line 422
    :cond_833
    aget-byte v3, v94, v52

    if-nez v3, :cond_839

    aput-byte v84, v36, v52

    .line 423
    :cond_839
    add-int/lit8 v52, v52, 0x1

    .line 425
    move-object/from16 v0, v19

    array-length v3, v0

    move/from16 v0, v52

    if-ne v0, v3, :cond_ca1

    .line 427
    move-object/from16 v0, v51

    move/from16 v1, v46

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 428
    move-object/from16 v0, v51

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 429
    invoke-virtual/range {v51 .. v51}, Ljava/nio/MappedByteBuffer;->force()Ljava/nio/MappedByteBuffer;

    .line 431
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "Internet Connection Fixed!\n"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 432
    const/16 v79, 0x1

    .line 437
    :cond_85c
    add-int/lit8 v3, v46, 0x1

    move-object/from16 v0, v51

    invoke-virtual {v0, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 439
    .end local v52    # "i":I
    .end local v84    # "prufbyte":B
    :cond_863
    const/4 v3, 0x0

    aget-byte v3, v21, v3

    move/from16 v0, v45

    if-ne v0, v3, :cond_8b1

    .line 441
    const/4 v3, 0x0

    aget-byte v3, v96, v3

    if-nez v3, :cond_872

    const/4 v3, 0x0

    aput-byte v45, v38, v3

    .line 442
    :cond_872
    const/16 v52, 0x1

    .line 443
    .restart local v52    # "i":I
    add-int/lit8 v3, v46, 0x1

    move-object/from16 v0, v51

    invoke-virtual {v0, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 444
    invoke-virtual/range {v51 .. v51}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v84

    .line 446
    .restart local v84    # "prufbyte":B
    :goto_87f
    aget-byte v3, v21, v52

    move/from16 v0, v84

    if-eq v0, v3, :cond_88a

    aget-byte v3, v71, v52

    const/4 v4, 0x1

    if-ne v3, v4, :cond_8aa

    .line 448
    :cond_88a
    aget-byte v3, v96, v52

    if-nez v3, :cond_890

    aput-byte v84, v38, v52

    .line 449
    :cond_890
    add-int/lit8 v52, v52, 0x1

    .line 451
    move-object/from16 v0, v21

    array-length v3, v0

    move/from16 v0, v52

    if-ne v0, v3, :cond_ca7

    .line 453
    move-object/from16 v0, v51

    move/from16 v1, v46

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 454
    move-object/from16 v0, v51

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 455
    invoke-virtual/range {v51 .. v51}, Ljava/nio/MappedByteBuffer;->force()Ljava/nio/MappedByteBuffer;

    .line 463
    :cond_8aa
    add-int/lit8 v3, v46, 0x1

    move-object/from16 v0, v51

    invoke-virtual {v0, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 465
    .end local v52    # "i":I
    .end local v84    # "prufbyte":B
    :cond_8b1
    const/4 v3, 0x0

    aget-byte v3, v25, v3

    move/from16 v0, v45

    if-ne v0, v3, :cond_90e

    if-nez v81, :cond_90e

    sget-boolean v3, Lcom/chelpus/root/utils/runpatch;->pattern4:Z

    if-eqz v3, :cond_90e

    .line 467
    const/4 v3, 0x0

    aget-byte v3, v100, v3

    if-nez v3, :cond_8c6

    const/4 v3, 0x0

    aput-byte v45, v42, v3

    .line 468
    :cond_8c6
    const/16 v52, 0x1

    .line 469
    .restart local v52    # "i":I
    add-int/lit8 v3, v46, 0x1

    move-object/from16 v0, v51

    invoke-virtual {v0, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 470
    invoke-virtual/range {v51 .. v51}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v84

    .line 472
    .restart local v84    # "prufbyte":B
    :goto_8d3
    aget-byte v3, v25, v52

    move/from16 v0, v84

    if-eq v0, v3, :cond_8de

    aget-byte v3, v75, v52

    const/4 v4, 0x1

    if-ne v3, v4, :cond_907

    .line 474
    :cond_8de
    aget-byte v3, v100, v52

    if-nez v3, :cond_8e4

    aput-byte v84, v42, v52

    .line 475
    :cond_8e4
    add-int/lit8 v52, v52, 0x1

    .line 477
    move-object/from16 v0, v25

    array-length v3, v0

    move/from16 v0, v52

    if-ne v0, v3, :cond_cad

    .line 479
    move-object/from16 v0, v51

    move/from16 v1, v46

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 480
    move-object/from16 v0, v51

    move-object/from16 v1, v42

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 481
    invoke-virtual/range {v51 .. v51}, Ljava/nio/MappedByteBuffer;->force()Ljava/nio/MappedByteBuffer;

    .line 483
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "lvl patch N5!\n"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 484
    const/16 v81, 0x1

    .line 489
    :cond_907
    add-int/lit8 v3, v46, 0x1

    move-object/from16 v0, v51

    invoke-virtual {v0, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 491
    .end local v52    # "i":I
    .end local v84    # "prufbyte":B
    :cond_90e
    const/4 v3, 0x0

    aget-byte v3, v15, v3

    move/from16 v0, v45

    if-ne v0, v3, :cond_969

    if-nez v81, :cond_969

    sget-boolean v3, Lcom/chelpus/root/utils/runpatch;->pattern4:Z

    if-eqz v3, :cond_969

    .line 493
    const/4 v3, 0x0

    aget-byte v3, v90, v3

    if-nez v3, :cond_923

    const/4 v3, 0x0

    aput-byte v45, v32, v3

    .line 494
    :cond_923
    const/16 v52, 0x1

    .line 495
    .restart local v52    # "i":I
    add-int/lit8 v3, v46, 0x1

    move-object/from16 v0, v51

    invoke-virtual {v0, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 496
    invoke-virtual/range {v51 .. v51}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v84

    .line 498
    .restart local v84    # "prufbyte":B
    :goto_930
    aget-byte v3, v15, v52

    move/from16 v0, v84

    if-eq v0, v3, :cond_93b

    aget-byte v3, v65, v52

    const/4 v4, 0x1

    if-ne v3, v4, :cond_962

    .line 500
    :cond_93b
    aget-byte v3, v90, v52

    if-nez v3, :cond_941

    aput-byte v84, v32, v52

    .line 501
    :cond_941
    add-int/lit8 v52, v52, 0x1

    .line 503
    array-length v3, v15

    move/from16 v0, v52

    if-ne v0, v3, :cond_cb3

    .line 505
    move-object/from16 v0, v51

    move/from16 v1, v46

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 506
    move-object/from16 v0, v51

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 507
    invoke-virtual/range {v51 .. v51}, Ljava/nio/MappedByteBuffer;->force()Ljava/nio/MappedByteBuffer;

    .line 509
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "lvl patch N5!\n"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 510
    const/16 v81, 0x1

    .line 515
    :cond_962
    add-int/lit8 v3, v46, 0x1

    move-object/from16 v0, v51

    invoke-virtual {v0, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 518
    .end local v52    # "i":I
    .end local v84    # "prufbyte":B
    :cond_969
    const/4 v3, 0x0

    aget-byte v3, v26, v3

    move/from16 v0, v45

    if-ne v0, v3, :cond_9c4

    sget-boolean v3, Lcom/chelpus/root/utils/runpatch;->pattern3:Z

    if-eqz v3, :cond_9c4

    .line 520
    const/4 v3, 0x0

    aget-byte v3, v101, v3

    if-nez v3, :cond_97c

    const/4 v3, 0x0

    aput-byte v45, v43, v3

    .line 521
    :cond_97c
    const/16 v52, 0x1

    .line 522
    .restart local v52    # "i":I
    add-int/lit8 v3, v46, 0x1

    move-object/from16 v0, v51

    invoke-virtual {v0, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 523
    invoke-virtual/range {v51 .. v51}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v84

    .line 525
    .restart local v84    # "prufbyte":B
    :goto_989
    aget-byte v3, v26, v52

    move/from16 v0, v84

    if-eq v0, v3, :cond_994

    aget-byte v3, v76, v52

    const/4 v4, 0x1

    if-ne v3, v4, :cond_9bd

    .line 527
    :cond_994
    aget-byte v3, v101, v52

    if-nez v3, :cond_99a

    aput-byte v84, v43, v52

    .line 528
    :cond_99a
    add-int/lit8 v52, v52, 0x1

    .line 530
    move-object/from16 v0, v26

    array-length v3, v0

    move/from16 v0, v52

    if-ne v0, v3, :cond_cb9

    .line 532
    move-object/from16 v0, v51

    move/from16 v1, v46

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 533
    move-object/from16 v0, v51

    move-object/from16 v1, v43

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 534
    invoke-virtual/range {v51 .. v51}, Ljava/nio/MappedByteBuffer;->force()Ljava/nio/MappedByteBuffer;

    .line 536
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "lvl patch N6!\n"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 537
    const/16 v82, 0x1

    .line 542
    :cond_9bd
    add-int/lit8 v3, v46, 0x1

    move-object/from16 v0, v51

    invoke-virtual {v0, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 544
    .end local v52    # "i":I
    .end local v84    # "prufbyte":B
    :cond_9c4
    const/4 v3, 0x0

    aget-byte v3, v11, v3

    move/from16 v0, v45

    if-ne v0, v3, :cond_a1d

    sget-boolean v3, Lcom/chelpus/root/utils/runpatch;->pattern4:Z

    if-eqz v3, :cond_a1d

    .line 546
    const/4 v3, 0x0

    aget-byte v3, v86, v3

    if-nez v3, :cond_9d7

    const/4 v3, 0x0

    aput-byte v45, v28, v3

    .line 547
    :cond_9d7
    const/16 v52, 0x1

    .line 548
    .restart local v52    # "i":I
    add-int/lit8 v3, v46, 0x1

    move-object/from16 v0, v51

    invoke-virtual {v0, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 549
    invoke-virtual/range {v51 .. v51}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v84

    .line 551
    .restart local v84    # "prufbyte":B
    :goto_9e4
    aget-byte v3, v11, v52

    move/from16 v0, v84

    if-eq v0, v3, :cond_9ef

    aget-byte v3, v61, v52

    const/4 v4, 0x1

    if-ne v3, v4, :cond_a16

    .line 553
    :cond_9ef
    aget-byte v3, v86, v52

    if-nez v3, :cond_9f5

    aput-byte v84, v28, v52

    .line 554
    :cond_9f5
    add-int/lit8 v52, v52, 0x1

    .line 556
    array-length v3, v11

    move/from16 v0, v52

    if-ne v0, v3, :cond_cbf

    .line 558
    move-object/from16 v0, v51

    move/from16 v1, v46

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 559
    move-object/from16 v0, v51

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 560
    invoke-virtual/range {v51 .. v51}, Ljava/nio/MappedByteBuffer;->force()Ljava/nio/MappedByteBuffer;

    .line 562
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "lvl patch N7!\n"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 563
    const/16 v83, 0x1

    .line 568
    :cond_a16
    add-int/lit8 v3, v46, 0x1

    move-object/from16 v0, v51

    invoke-virtual {v0, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 570
    .end local v52    # "i":I
    .end local v84    # "prufbyte":B
    :cond_a1d
    const/4 v3, 0x0

    aget-byte v3, v18, v3

    move/from16 v0, v45

    if-ne v0, v3, :cond_a78

    sget-boolean v3, Lcom/chelpus/root/utils/runpatch;->pattern4:Z

    if-eqz v3, :cond_a78

    .line 572
    const/4 v3, 0x0

    aget-byte v3, v93, v3

    if-nez v3, :cond_a30

    const/4 v3, 0x0

    aput-byte v45, v35, v3

    .line 573
    :cond_a30
    const/16 v52, 0x1

    .line 574
    .restart local v52    # "i":I
    add-int/lit8 v3, v46, 0x1

    move-object/from16 v0, v51

    invoke-virtual {v0, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 575
    invoke-virtual/range {v51 .. v51}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v84

    .line 577
    .restart local v84    # "prufbyte":B
    :goto_a3d
    aget-byte v3, v18, v52

    move/from16 v0, v84

    if-eq v0, v3, :cond_a48

    aget-byte v3, v68, v52

    const/4 v4, 0x1

    if-ne v3, v4, :cond_a71

    .line 579
    :cond_a48
    aget-byte v3, v93, v52

    if-nez v3, :cond_a4e

    aput-byte v84, v35, v52

    .line 580
    :cond_a4e
    add-int/lit8 v52, v52, 0x1

    .line 582
    move-object/from16 v0, v18

    array-length v3, v0

    move/from16 v0, v52

    if-ne v0, v3, :cond_cc5

    .line 584
    move-object/from16 v0, v51

    move/from16 v1, v46

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 585
    move-object/from16 v0, v51

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 586
    invoke-virtual/range {v51 .. v51}, Ljava/nio/MappedByteBuffer;->force()Ljava/nio/MappedByteBuffer;

    .line 588
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "lvl patch N7!\n"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 589
    const/16 v83, 0x1

    .line 594
    :cond_a71
    add-int/lit8 v3, v46, 0x1

    move-object/from16 v0, v51

    invoke-virtual {v0, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 622
    .end local v52    # "i":I
    .end local v84    # "prufbyte":B
    :cond_a78
    const/4 v3, 0x0

    aget-byte v3, v12, v3

    move/from16 v0, v45

    if-ne v0, v3, :cond_ad1

    sget-boolean v3, Lcom/chelpus/root/utils/runpatch;->pattern4:Z

    if-eqz v3, :cond_ad1

    .line 624
    const/4 v3, 0x0

    aget-byte v3, v87, v3

    if-nez v3, :cond_a8b

    const/4 v3, 0x0

    aput-byte v45, v29, v3

    .line 625
    :cond_a8b
    const/16 v52, 0x1

    .line 626
    .restart local v52    # "i":I
    add-int/lit8 v3, v46, 0x1

    move-object/from16 v0, v51

    invoke-virtual {v0, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 627
    invoke-virtual/range {v51 .. v51}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v84

    .line 629
    .restart local v84    # "prufbyte":B
    :goto_a98
    aget-byte v3, v12, v52

    move/from16 v0, v84

    if-eq v0, v3, :cond_aa3

    aget-byte v3, v62, v52

    const/4 v4, 0x1

    if-ne v3, v4, :cond_aca

    .line 631
    :cond_aa3
    aget-byte v3, v87, v52

    if-nez v3, :cond_aa9

    aput-byte v84, v29, v52

    .line 632
    :cond_aa9
    add-int/lit8 v52, v52, 0x1

    .line 634
    array-length v3, v12

    move/from16 v0, v52

    if-ne v0, v3, :cond_ccb

    .line 636
    move-object/from16 v0, v51

    move/from16 v1, v46

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 637
    move-object/from16 v0, v51

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 638
    invoke-virtual/range {v51 .. v51}, Ljava/nio/MappedByteBuffer;->force()Ljava/nio/MappedByteBuffer;

    .line 640
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "lvl patch N7!\n"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 641
    const/16 v83, 0x1

    .line 646
    :cond_aca
    add-int/lit8 v3, v46, 0x1

    move-object/from16 v0, v51

    invoke-virtual {v0, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 648
    .end local v52    # "i":I
    .end local v84    # "prufbyte":B
    :cond_ad1
    const/4 v3, 0x0

    aget-byte v3, v13, v3

    move/from16 v0, v45

    if-ne v0, v3, :cond_b2a

    sget-boolean v3, Lcom/chelpus/root/utils/runpatch;->pattern4:Z

    if-eqz v3, :cond_b2a

    .line 650
    const/4 v3, 0x0

    aget-byte v3, v88, v3

    if-nez v3, :cond_ae4

    const/4 v3, 0x0

    aput-byte v45, v30, v3

    .line 651
    :cond_ae4
    const/16 v52, 0x1

    .line 652
    .restart local v52    # "i":I
    add-int/lit8 v3, v46, 0x1

    move-object/from16 v0, v51

    invoke-virtual {v0, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 653
    invoke-virtual/range {v51 .. v51}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v84

    .line 655
    .restart local v84    # "prufbyte":B
    :goto_af1
    aget-byte v3, v13, v52

    move/from16 v0, v84

    if-eq v0, v3, :cond_afc

    aget-byte v3, v63, v52

    const/4 v4, 0x1

    if-ne v3, v4, :cond_b23

    .line 657
    :cond_afc
    aget-byte v3, v88, v52

    if-nez v3, :cond_b02

    aput-byte v84, v30, v52

    .line 658
    :cond_b02
    add-int/lit8 v52, v52, 0x1

    .line 660
    array-length v3, v13

    move/from16 v0, v52

    if-ne v0, v3, :cond_cd1

    .line 662
    move-object/from16 v0, v51

    move/from16 v1, v46

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 663
    move-object/from16 v0, v51

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 664
    invoke-virtual/range {v51 .. v51}, Ljava/nio/MappedByteBuffer;->force()Ljava/nio/MappedByteBuffer;

    .line 666
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "lvl patch N7!\n"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 667
    const/16 v83, 0x1

    .line 672
    :cond_b23
    add-int/lit8 v3, v46, 0x1

    move-object/from16 v0, v51

    invoke-virtual {v0, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 674
    .end local v52    # "i":I
    .end local v84    # "prufbyte":B
    :cond_b2a
    const/4 v3, 0x0

    aget-byte v3, v14, v3

    move/from16 v0, v45

    if-ne v0, v3, :cond_b83

    sget-boolean v3, Lcom/chelpus/root/utils/runpatch;->pattern4:Z

    if-eqz v3, :cond_b83

    .line 676
    const/4 v3, 0x0

    aget-byte v3, v89, v3

    if-nez v3, :cond_b3d

    const/4 v3, 0x0

    aput-byte v45, v31, v3

    .line 677
    :cond_b3d
    const/16 v52, 0x1

    .line 678
    .restart local v52    # "i":I
    add-int/lit8 v3, v46, 0x1

    move-object/from16 v0, v51

    invoke-virtual {v0, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 679
    invoke-virtual/range {v51 .. v51}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v84

    .line 681
    .restart local v84    # "prufbyte":B
    :goto_b4a
    aget-byte v3, v14, v52

    move/from16 v0, v84

    if-eq v0, v3, :cond_b55

    aget-byte v3, v64, v52

    const/4 v4, 0x1

    if-ne v3, v4, :cond_b7c

    .line 683
    :cond_b55
    aget-byte v3, v89, v52

    if-nez v3, :cond_b5b

    aput-byte v84, v31, v52

    .line 684
    :cond_b5b
    add-int/lit8 v52, v52, 0x1

    .line 686
    array-length v3, v14

    move/from16 v0, v52

    if-ne v0, v3, :cond_cd7

    .line 688
    move-object/from16 v0, v51

    move/from16 v1, v46

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 689
    move-object/from16 v0, v51

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 690
    invoke-virtual/range {v51 .. v51}, Ljava/nio/MappedByteBuffer;->force()Ljava/nio/MappedByteBuffer;

    .line 692
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "lvl patch N7!\n"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 693
    const/16 v83, 0x1

    .line 698
    :cond_b7c
    add-int/lit8 v3, v46, 0x1

    move-object/from16 v0, v51

    invoke-virtual {v0, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 726
    .end local v52    # "i":I
    .end local v84    # "prufbyte":B
    :cond_b83
    const/4 v3, 0x0

    aget-byte v3, v23, v3

    move/from16 v0, v45

    if-ne v0, v3, :cond_bd7

    sget-boolean v3, Lcom/chelpus/root/utils/runpatch;->pattern4:Z

    if-eqz v3, :cond_bd7

    .line 728
    const/4 v3, 0x0

    aget-byte v3, v98, v3

    if-nez v3, :cond_b96

    const/4 v3, 0x0

    aput-byte v45, v40, v3

    .line 729
    :cond_b96
    const/16 v52, 0x1

    .line 730
    .restart local v52    # "i":I
    add-int/lit8 v3, v46, 0x1

    move-object/from16 v0, v51

    invoke-virtual {v0, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 731
    invoke-virtual/range {v51 .. v51}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v84

    .line 733
    .restart local v84    # "prufbyte":B
    :goto_ba3
    aget-byte v3, v23, v52

    move/from16 v0, v84

    if-eq v0, v3, :cond_bae

    aget-byte v3, v73, v52

    const/4 v4, 0x1

    if-ne v3, v4, :cond_bd7

    .line 735
    :cond_bae
    aget-byte v3, v98, v52

    if-nez v3, :cond_bb4

    aput-byte v84, v40, v52

    .line 736
    :cond_bb4
    add-int/lit8 v52, v52, 0x1

    .line 737
    move-object/from16 v0, v23

    array-length v3, v0

    move/from16 v0, v52

    if-ne v0, v3, :cond_cdd

    .line 739
    move-object/from16 v0, v51

    move/from16 v1, v46

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 740
    move-object/from16 v0, v51

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 741
    invoke-virtual/range {v51 .. v51}, Ljava/nio/MappedByteBuffer;->force()Ljava/nio/MappedByteBuffer;

    .line 743
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "lvl patch N5!\n"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 745
    const/16 v81, 0x1

    .line 752
    .end local v52    # "i":I
    .end local v84    # "prufbyte":B
    :cond_bd7
    add-int/lit8 v3, v46, 0x1

    move-object/from16 v0, v51

    invoke-virtual {v0, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 753
    const/4 v3, 0x0

    aget-byte v3, v16, v3

    move/from16 v0, v45

    if-ne v0, v3, :cond_c1e

    sget-boolean v3, Lcom/chelpus/root/utils/runpatch;->pattern4:Z

    if-eqz v3, :cond_c1e

    .line 755
    const/4 v3, 0x0

    aget-byte v3, v91, v3

    if-nez v3, :cond_bf1

    const/4 v3, 0x0

    aput-byte v45, v33, v3

    .line 756
    :cond_bf1
    const/16 v52, 0x1

    .line 757
    .restart local v52    # "i":I
    add-int/lit8 v3, v46, 0x1

    move-object/from16 v0, v51

    invoke-virtual {v0, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 758
    invoke-virtual/range {v51 .. v51}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v84

    .line 760
    .restart local v84    # "prufbyte":B
    :goto_bfe
    aget-byte v3, v16, v52

    move/from16 v0, v84

    if-eq v0, v3, :cond_c09

    aget-byte v3, v66, v52

    const/4 v4, 0x1

    if-ne v3, v4, :cond_c1e

    .line 762
    :cond_c09
    aget-byte v3, v91, v52

    if-nez v3, :cond_c0f

    aput-byte v84, v33, v52

    .line 763
    :cond_c0f
    add-int/lit8 v52, v52, 0x1

    .line 764
    move-object/from16 v0, v16

    array-length v3, v0

    move/from16 v0, v52

    if-ne v0, v3, :cond_ce3

    .line 767
    invoke-virtual/range {v51 .. v51}, Ljava/nio/MappedByteBuffer;->position()I

    move-result v46

    .line 773
    const/16 v58, 0x1

    .line 781
    .end local v52    # "i":I
    .end local v84    # "prufbyte":B
    :cond_c1e
    add-int/lit8 v3, v46, 0x1

    move-object/from16 v0, v51

    invoke-virtual {v0, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 783
    const/16 v3, 0x10

    move/from16 v0, v45

    if-ge v0, v3, :cond_c7c

    if-eqz v58, :cond_c7c

    sget-boolean v3, Lcom/chelpus/root/utils/runpatch;->pattern4:Z

    if-eqz v3, :cond_c7c

    .line 785
    const/4 v3, 0x0

    aget-byte v3, v92, v3

    if-nez v3, :cond_c39

    const/4 v3, 0x0

    aput-byte v45, v34, v3

    .line 786
    :cond_c39
    const/16 v52, 0x1

    .line 787
    .restart local v52    # "i":I
    add-int/lit8 v3, v46, 0x1

    move-object/from16 v0, v51

    invoke-virtual {v0, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 788
    invoke-virtual/range {v51 .. v51}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v84

    .line 790
    .restart local v84    # "prufbyte":B
    :goto_c46
    aget-byte v3, v17, v52

    move/from16 v0, v84

    if-eq v0, v3, :cond_c51

    aget-byte v3, v67, v52

    const/4 v4, 0x1

    if-ne v3, v4, :cond_c7c

    .line 792
    :cond_c51
    aget-byte v3, v92, v52

    if-nez v3, :cond_c57

    aput-byte v84, v34, v52

    .line 793
    :cond_c57
    add-int/lit8 v52, v52, 0x1

    .line 794
    move-object/from16 v0, v17

    array-length v3, v0

    move/from16 v0, v52

    if-ne v0, v3, :cond_ce9

    .line 796
    move-object/from16 v0, v51

    move/from16 v1, v46

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 797
    move-object/from16 v0, v51

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 798
    invoke-virtual/range {v51 .. v51}, Ljava/nio/MappedByteBuffer;->force()Ljava/nio/MappedByteBuffer;

    .line 800
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "lvl patch N5!\n"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 801
    const/16 v58, 0x0

    .line 802
    const/16 v81, 0x1

    .line 809
    .end local v52    # "i":I
    .end local v84    # "prufbyte":B
    :cond_c7c
    add-int/lit8 v3, v46, 0x1

    move-object/from16 v0, v51

    invoke-virtual {v0, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 265
    const-wide/16 v3, 0x1

    add-long v53, v53, v3

    goto/16 :goto_663

    .line 300
    .restart local v44    # "c":I
    .restart local v52    # "i":I
    .restart local v84    # "prufbyte":B
    :cond_c89
    invoke-virtual/range {v51 .. v51}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v84

    goto/16 :goto_6ab

    .line 330
    .end local v44    # "c":I
    :cond_c8f
    invoke-virtual/range {v51 .. v51}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v84

    goto/16 :goto_717

    .line 383
    :cond_c95
    invoke-virtual/range {v51 .. v51}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v84

    goto/16 :goto_772

    .line 409
    :cond_c9b
    invoke-virtual/range {v51 .. v51}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v84

    goto/16 :goto_7cd

    .line 435
    :cond_ca1
    invoke-virtual/range {v51 .. v51}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v84

    goto/16 :goto_828

    .line 461
    :cond_ca7
    invoke-virtual/range {v51 .. v51}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v84

    goto/16 :goto_87f

    .line 487
    :cond_cad
    invoke-virtual/range {v51 .. v51}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v84

    goto/16 :goto_8d3

    .line 513
    :cond_cb3
    invoke-virtual/range {v51 .. v51}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v84

    goto/16 :goto_930

    .line 540
    :cond_cb9
    invoke-virtual/range {v51 .. v51}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v84

    goto/16 :goto_989

    .line 566
    :cond_cbf
    invoke-virtual/range {v51 .. v51}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v84

    goto/16 :goto_9e4

    .line 592
    :cond_cc5
    invoke-virtual/range {v51 .. v51}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v84

    goto/16 :goto_a3d

    .line 644
    :cond_ccb
    invoke-virtual/range {v51 .. v51}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v84

    goto/16 :goto_a98

    .line 670
    :cond_cd1
    invoke-virtual/range {v51 .. v51}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v84

    goto/16 :goto_af1

    .line 696
    :cond_cd7
    invoke-virtual/range {v51 .. v51}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v84

    goto/16 :goto_b4a

    .line 748
    :cond_cdd
    invoke-virtual/range {v51 .. v51}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v84

    goto/16 :goto_ba3

    .line 777
    :cond_ce3
    invoke-virtual/range {v51 .. v51}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v84

    goto/16 :goto_bfe

    .line 805
    :cond_ce9
    invoke-virtual/range {v51 .. v51}, Ljava/nio/MappedByteBuffer;->get()B
    :try_end_cec
    .catch Ljava/lang/Exception; {:try_start_663 .. :try_end_cec} :catch_cef
    .catch Ljava/io/FileNotFoundException; {:try_start_663 .. :try_end_cec} :catch_624

    move-result v84

    goto/16 :goto_c46

    .line 811
    .end local v45    # "curentByte":B
    .end local v46    # "curentPos":I
    .end local v52    # "i":I
    .end local v84    # "prufbyte":B
    :catch_cef
    move-exception v50

    .line 812
    .local v50, "e":Ljava/lang/Exception;
    :try_start_cf0
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v50

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 814
    .end local v50    # "e":Ljava/lang/Exception;
    :cond_d0a
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V

    .line 816
    if-nez v78, :cond_d16

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "Error: License Key2 patch Failed!\nor patch is already applied?!\n"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 817
    :cond_d16
    if-nez v79, :cond_d1f

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "Error: Internet Connection patch Failed!\nor patch is already applied?!\n\n"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 818
    :cond_d1f
    if-nez v80, :cond_d28

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "Error: Check License Key patch Failed!\nor patch is already applied?!\n"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 819
    :cond_d28
    if-nez v81, :cond_d31

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "Error: lvl patch 5 failed\n"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 820
    :cond_d31
    if-nez v82, :cond_d3a

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "Error: lvl patch 6 failed\n"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 821
    :cond_d3a
    if-nez v83, :cond_d43

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "Error: lvl patch 7 failed\n"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 823
    :cond_d43
    if-nez v77, :cond_62c

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "Error: Cached License Key patch Failed!\nor patch is already applied?!\n"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_d4c
    .catch Ljava/io/FileNotFoundException; {:try_start_cf0 .. :try_end_d4c} :catch_624
    .catch Ljava/lang/Exception; {:try_start_cf0 .. :try_end_d4c} :catch_d4e

    goto/16 :goto_62c

    .line 828
    .end local v2    # "ChannelDex":Ljava/nio/channels/FileChannel;
    .end local v47    # "curentPos2":I
    .end local v49    # "dalvikDexTemp":Ljava/lang/String;
    .end local v51    # "fileBytes":Ljava/nio/MappedByteBuffer;
    .end local v53    # "j":J
    .end local v55    # "localFile1":Ljava/io/File;
    .end local v56    # "localFile2":Ljava/io/File;
    .end local v58    # "mark5":Z
    .end local v59    # "mark6":Z
    .end local v77    # "patch":Z
    .end local v78    # "patch1":Z
    .end local v79    # "patch2":Z
    .end local v80    # "patch4":Z
    .end local v81    # "patch5":Z
    .end local v82    # "patch6":Z
    .end local v83    # "patch7":Z
    .end local v102    # "repbyte":B
    :catch_d4e
    move-exception v50

    .line 829
    .restart local v50    # "e":Ljava/lang/Exception;
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception e"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v50 .. v50}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_62c

    .line 54
    nop

    :array_d6e
    .array-data 1
        0x5t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x2t
        0x1t
        0x0t
        0x0t
        0x3t
        0x1t
        0x0t
        0x0t
        0xft
        0x0t
        0x0t
        0x0t
        0x1at
        0x0t
        0x0t
        0x0t
        0xft
        0x0t
        0x0t
        0x0t
        0x59t
    .end array-data

    .line 55
    nop

    :array_d82
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x1t
    .end array-data

    .line 56
    nop

    :array_d96
    .array-data 1
        0x5t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x2t
        0x1t
        0x0t
        0x0t
        0x3t
        0x1t
        0x0t
        0x0t
        0xft
        0x0t
        0x0t
        0x0t
        0xft
        0x0t
        0x0t
        0x0t
        0xft
        0x0t
        0x0t
        0x0t
        0x59t
    .end array-data

    .line 57
    nop

    :array_daa
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x2t
        0x0t
        0x0t
        0x0t
        0x3t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 59
    nop

    :array_dbe
    .array-data 1
        0x10t
        0x0t
        0x0t
        0x0t
        0x0t
        0xat
        0x0t
        0x38t
        0x0t
        0x8t
        0x0t
        0x54t
        0x10t
        0x0t
        0x0t
        0x72t
        0x10t
        0x0t
        0x0t
        0x0t
        0x0t
        0xet
        0x0t
    .end array-data

    .line 60
    :array_dce
    .array-data 1
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
    .end array-data

    .line 61
    :array_dde
    .array-data 1
        0x10t
        0x0t
        0x0t
        0x0t
        0x0t
        0xat
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x54t
        0x10t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 62
    :array_dee
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 69
    :array_dfe
    .array-data 1
        0xat
        0x0t
        0x38t
        0x0t
        0xet
        0x0t
        0x1at
        0x0t
        0x0t
        0x0t
        0x1at
        0x0t
        0x0t
        0x0t
        0x71t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x72t
    .end array-data

    .line 70
    nop

    :array_e0e
    .array-data 1
        0x0t
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
    .end array-data

    .line 71
    nop

    :array_e1e
    .array-data 1
        0xat
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1at
        0x0t
        0x0t
        0x0t
        0x1at
        0x0t
        0x0t
        0x0t
        0x71t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x72t
    .end array-data

    .line 72
    nop

    :array_e2e
    .array-data 1
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 74
    nop

    :array_e3e
    .array-data 1
        0x1at
        0x66t
        0x66t
        0x66t
        0x70t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x66t
        0x22t
        0x66t
        0x66t
        0x66t
        0x70t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x12t
        0x66t
        0x46t
        0x66t
        0x66t
        0x66t
        0x71t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x12t
    .end array-data

    .line 75
    nop

    :array_e58
    .array-data 1
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
    .end array-data

    .line 76
    nop

    :array_e72
    .array-data 1
        0x1at
        0x66t
        0x66t
        0x66t
        0x70t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x66t
        0x22t
        0x66t
        0x66t
        0x66t
        0x70t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x12t
        0x66t
        0x46t
        0x66t
        0x66t
        0x66t
        0x71t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x12t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x12t
    .end array-data

    .line 77
    nop

    :array_e8c
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 79
    nop

    :array_ea6
    .array-data 1
        0x38t
        0x0t
        0xbt
        0x0t
        0x0t
        0x10t
        0x0t
        0x0t
        0x0t
        0x0t
        0xct
        0x0t
        0x72t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0xet
        0x0t
    .end array-data

    .line 80
    nop

    :array_eb6
    .array-data 1
        0x0t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
    .end array-data

    .line 81
    nop

    :array_ec6
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x10t
        0x0t
        0x0t
        0x0t
        0xct
        0x0t
        0x72t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0xet
        0x0t
        0x0t
    .end array-data

    .line 82
    nop

    :array_ed6
    .array-data 1
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 84
    nop

    :array_ee6
    .array-data 1
        0x22t
        0x66t
        0x66t
        0x66t
        0x70t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x5bt
        0x66t
        0x66t
        0x66t
        0x12t
        0x66t
        0x46t
        0x66t
        0x66t
        0x66t
        0x71t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x12t
        0x66t
    .end array-data

    .line 85
    nop

    :array_efc
    .array-data 1
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
    .end array-data

    .line 86
    nop

    :array_f12
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x12t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 87
    nop

    :array_f28
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 96
    nop

    :array_f3e
    .array-data 1
        0x38t
        0x9t
        0x6t
        0x0t
        0x32t
        0x49t
        0x4t
        0x0t
        0x33t
        0x59t
        -0x44t
        0x0t
    .end array-data

    .line 97
    :array_f48
    .array-data 1
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
    .end array-data

    .line 98
    :array_f52
    .array-data 1
        0x12t
        0x0t
        0x0t
        0x0t
        0x29t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x54t
    .end array-data

    .line 99
    :array_f5c
    .array-data 1
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 101
    :array_f66
    .array-data 1
        0x2ct
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x66t
        0xft
        0x66t
        0x1at
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x1t
        0x66t
        0x28t
        0x66t
        0x1at
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xct
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xct
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x1t
        0x66t
        0x28t
    .end array-data

    .line 102
    nop

    :array_f90
    .array-data 1
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
    .end array-data

    .line 103
    nop

    :array_fba
    .array-data 1
        0x2ct
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x66t
        0xft
        0x66t
        0x1at
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x1t
        0x66t
        0x28t
        0x66t
        0x1at
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x12t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xct
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xct
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x1t
        0x66t
        0x28t
    .end array-data

    .line 104
    nop

    :array_fe4
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 107
    nop

    :array_100e
    .array-data 1
        0x2ct
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x66t
        0xft
        0x66t
        0x1at
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x28t
        0x66t
        0x1at
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xct
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xct
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x1t
        0x66t
        0x28t
    .end array-data

    .line 108
    :array_1036
    .array-data 1
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
    .end array-data

    .line 109
    :array_105e
    .array-data 1
        0x2ct
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x66t
        0xft
        0x66t
        0x1at
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x28t
        0x66t
        0x1at
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x12t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xct
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xct
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x1t
        0x66t
        0x28t
    .end array-data

    .line 110
    :array_1086
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 113
    :array_10ae
    .array-data 1
        0x2ct
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x66t
        0xft
        0x66t
        0x1at
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x28t
        0x66t
        0x1at
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xct
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xct
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x28t
    .end array-data

    .line 114
    nop

    :array_10d6
    .array-data 1
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
    .end array-data

    .line 115
    nop

    :array_10fe
    .array-data 1
        0x2ct
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x66t
        0xft
        0x66t
        0x1at
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x28t
        0x66t
        0x1at
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x12t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xct
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xct
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x28t
    .end array-data

    .line 116
    nop

    :array_1126
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 118
    nop

    :array_114e
    .array-data 1
        0x2ct
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x66t
        0xft
        0x66t
        0x1at
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x28t
        0x66t
        0x1at
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
    .end array-data

    .line 119
    nop

    :array_116a
    .array-data 1
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
    .end array-data

    .line 120
    nop

    :array_1186
    .array-data 1
        0x2ct
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x66t
        0xft
        0x66t
        0x1at
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x28t
        0x66t
        0x1at
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x12t
    .end array-data

    .line 121
    nop

    :array_11a2
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
    .end array-data

    .line 124
    nop

    :array_11be
    .array-data 1
        0xct
        0x66t
        0x71t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xct
        0x66t
        0x21t
        0x66t
        0x12t
        0x66t
        0x35t
        0x66t
        0x66t
        0x66t
        0x22t
        0x66t
        0x66t
        0x66t
        0x1at
        0x66t
        0x66t
        0x66t
        0x70t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x66t
        0x22t
        0x66t
        0x66t
        0x66t
        0x70t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x5bt
        0x66t
        0x66t
        0x66t
        0x12t
        0x66t
        0x46t
        0x66t
        0x66t
        0x66t
        0x71t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
    .end array-data

    .line 125
    nop

    :array_11e2
    .array-data 1
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
    .end array-data

    .line 126
    nop

    :array_1206
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x12t
    .end array-data

    .line 127
    nop

    :array_122a
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
    .end array-data

    .line 129
    nop

    :array_124e
    .array-data 1
        0x1at
        0x66t
        0x66t
        0x66t
        0x71t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xct
        0x66t
        0x71t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xct
        0x66t
        0x21t
        0x66t
        0x66t
        0x66t
        0x35t
        0x66t
        0x66t
        0x66t
        0x22t
        0x66t
        0x66t
        0x66t
    .end array-data

    .line 130
    :array_1262
    .array-data 1
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
    .end array-data

    .line 131
    :array_1276
    .array-data 1
        0x1at
        0x66t
        0x66t
        0x66t
        0x71t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xct
        0x66t
        0x71t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xct
        0x66t
        0x21t
        0x66t
        0x66t
        0x66t
        0x35t
        0x66t
        0x66t
        0x66t
        0x22t
        0x66t
        0x66t
        0x66t
    .end array-data

    .line 132
    :array_128a
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 134
    :array_129e
    .array-data 1
        0x0t
        0x46t
        0x66t
        0x66t
        0x66t
        0x71t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x66t
    .end array-data

    .line 135
    nop

    :array_12aa
    .array-data 1
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
    .end array-data

    .line 136
    nop

    :array_12b6
    .array-data 1
        0x0t
        0x46t
        0x66t
        0x66t
        0x66t
        0x71t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x12t
        0x66t
    .end array-data

    .line 137
    nop

    :array_12c2
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
    .end array-data

    .line 140
    nop

    :array_12ce
    .array-data 1
        0x2ct
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x66t
        0xft
        0x66t
        0x62t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x28t
        0x66t
        0x62t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
    .end array-data

    .line 141
    nop

    :array_12ea
    .array-data 1
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
    .end array-data

    .line 142
    nop

    :array_1306
    .array-data 1
        0x2ct
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x66t
        0xft
        0x66t
        0x1at
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x28t
        0x66t
        0x1at
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x12t
    .end array-data

    .line 143
    nop

    :array_1322
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
    .end array-data

    .line 145
    nop

    :array_133e
    .array-data 1
        0x12t
        0x66t
        0x12t
        0x66t
        0x71t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xbt
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x33t
        0x66t
        0x66t
        0x66t
        -0xdt
    .end array-data

    .line 146
    nop

    :array_1350
    .array-data 1
        0x0t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
    .end array-data

    .line 147
    nop

    :array_1362
    .array-data 1
        0x12t
        0x10t
        0xft
        0x0t
        0x71t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xbt
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x33t
        0x66t
        0x66t
        0x66t
        -0xdt
    .end array-data

    .line 148
    nop

    :array_1374
    .array-data 1
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 150
    nop

    :array_1386
    .array-data 1
        0x6ct
        0x61t
        0x63t
        0x6bt
        0x79t
        0x70t
        0x61t
        0x74t
        0x63t
        0x68t
    .end array-data

    .line 151
    nop

    :array_1390
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 152
    nop

    :array_139a
    .array-data 1
        0x6ct
        0x75t
        0x63t
        0x6bt
        0x79t
        0x70t
        0x61t
        0x74t
        0x63t
        0x68t
    .end array-data

    .line 153
    nop

    :array_13a4
    .array-data 1
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
    .end array-data
.end method
