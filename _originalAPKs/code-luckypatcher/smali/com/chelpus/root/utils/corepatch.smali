.class public Lcom/chelpus/root/utils/corepatch;
.super Ljava/lang/Object;
.source "corepatch.java"


# static fields
.field public static final MAGIC:[B

.field public static adler:I

.field public static fileBytes:Ljava/nio/MappedByteBuffer;

.field public static lastByteReplace:[B

.field public static lastPatchPosition:I

.field public static not_found_bytes_for_patch:Z

.field public static onlyDalvik:Z

.field public static toolfilesdir:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 25
    const/16 v0, 0x8

    new-array v0, v0, [B

    fill-array-data v0, :array_1a

    sput-object v0, Lcom/chelpus/root/utils/corepatch;->MAGIC:[B

    .line 28
    const-string v0, ""

    sput-object v0, Lcom/chelpus/root/utils/corepatch;->toolfilesdir:Ljava/lang/String;

    .line 29
    sput-boolean v1, Lcom/chelpus/root/utils/corepatch;->onlyDalvik:Z

    .line 30
    sput-boolean v1, Lcom/chelpus/root/utils/corepatch;->not_found_bytes_for_patch:Z

    .line 31
    sput-object v2, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    .line 32
    sput v1, Lcom/chelpus/root/utils/corepatch;->lastPatchPosition:I

    .line 33
    sput-object v2, Lcom/chelpus/root/utils/corepatch;->lastByteReplace:[B

    return-void

    .line 25
    :array_1a
    .array-data 1
        0x64t
        0x65t
        0x79t
        0xat
        0x30t
        0x33t
        0x35t
        0x0t
    .end array-data
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static applyPatch(IB[B[B[B[BZ)Z
    .registers 13
    .param p0, "curentPos"    # I
    .param p1, "currentByte"    # B
    .param p2, "byteOrig"    # [B
    .param p3, "mask"    # [B
    .param p4, "byteReplace"    # [B
    .param p5, "rep_mask"    # [B
    .param p6, "pattern"    # Z

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2852
    if-eqz p2, :cond_68

    aget-byte v4, p2, v3

    if-ne p1, v4, :cond_68

    if-eqz p6, :cond_68

    .line 2854
    aget-byte v4, p5, v3

    if-nez v4, :cond_10

    aput-byte p1, p4, v3

    .line 2855
    :cond_10
    const/4 v0, 0x1

    .line 2856
    .local v0, "i":I
    sget-object v4, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    add-int/lit8 v5, p0, 0x1

    invoke-virtual {v4, v5}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2857
    sget-object v4, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v4}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v1

    .line 2859
    .local v1, "prufbyte":B
    :goto_1e
    aget-byte v4, p2, v0

    if-eq v1, v4, :cond_26

    aget-byte v4, p3, v0

    if-ne v4, v2, :cond_61

    .line 2861
    :cond_26
    aget-byte v4, p5, v0

    if-nez v4, :cond_2c

    aput-byte v1, p4, v0

    .line 2862
    :cond_2c
    aget-byte v4, p5, v0

    const/4 v5, 0x3

    if-ne v4, v5, :cond_36

    .line 2863
    and-int/lit8 v4, v1, 0xf

    int-to-byte v4, v4

    aput-byte v4, p4, v0

    .line 2866
    :cond_36
    aget-byte v4, p5, v0

    const/4 v5, 0x2

    if-ne v4, v5, :cond_45

    .line 2867
    and-int/lit8 v4, v1, 0xf

    and-int/lit8 v5, v1, 0xf

    mul-int/lit8 v5, v5, 0x10

    add-int/2addr v4, v5

    int-to-byte v4, v4

    aput-byte v4, p4, v0

    .line 2870
    :cond_45
    add-int/lit8 v0, v0, 0x1

    .line 2872
    array-length v4, p2

    if-ne v0, v4, :cond_5a

    .line 2874
    sget-object v3, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v3, p0}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2875
    sget-object v3, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v3, p4}, Ljava/nio/MappedByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 2876
    sget-object v3, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v3}, Ljava/nio/MappedByteBuffer;->force()Ljava/nio/MappedByteBuffer;

    .line 2884
    .end local v0    # "i":I
    .end local v1    # "prufbyte":B
    :goto_59
    return v2

    .line 2880
    .restart local v0    # "i":I
    .restart local v1    # "prufbyte":B
    :cond_5a
    sget-object v4, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v4}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v1

    goto :goto_1e

    .line 2882
    :cond_61
    sget-object v2, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    add-int/lit8 v4, p0, 0x1

    invoke-virtual {v2, v4}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .end local v0    # "i":I
    .end local v1    # "prufbyte":B
    :cond_68
    move v2, v3

    .line 2884
    goto :goto_59
.end method

.method private static applyPatchCounter(IB[B[B[B[BIIZ)Z
    .registers 15
    .param p0, "curentPos"    # I
    .param p1, "currentByte"    # B
    .param p2, "byteOrig"    # [B
    .param p3, "mask"    # [B
    .param p4, "byteReplace"    # [B
    .param p5, "rep_mask"    # [B
    .param p6, "counter"    # I
    .param p7, "limitCounter"    # I
    .param p8, "pattern"    # Z

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2887
    if-eqz p2, :cond_68

    aget-byte v4, p2, v3

    if-ne p1, v4, :cond_68

    if-eqz p8, :cond_68

    .line 2889
    aget-byte v4, p5, v3

    if-nez v4, :cond_10

    aput-byte p1, p4, v3

    .line 2890
    :cond_10
    const/4 v0, 0x1

    .line 2891
    .local v0, "i":I
    sget-object v4, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    add-int/lit8 v5, p0, 0x1

    invoke-virtual {v4, v5}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2892
    sget-object v4, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v4}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v1

    .line 2894
    .local v1, "prufbyte":B
    :goto_1e
    aget-byte v4, p2, v0

    if-eq v1, v4, :cond_26

    aget-byte v4, p3, v0

    if-ne v4, v2, :cond_61

    .line 2896
    :cond_26
    aget-byte v4, p5, v0

    if-nez v4, :cond_2c

    aput-byte v1, p4, v0

    .line 2897
    :cond_2c
    aget-byte v4, p5, v0

    const/4 v5, 0x3

    if-ne v4, v5, :cond_36

    .line 2898
    and-int/lit8 v4, v1, 0xf

    int-to-byte v4, v4

    aput-byte v4, p4, v0

    .line 2901
    :cond_36
    aget-byte v4, p5, v0

    const/4 v5, 0x2

    if-ne v4, v5, :cond_45

    .line 2902
    and-int/lit8 v4, v1, 0xf

    and-int/lit8 v5, v1, 0xf

    mul-int/lit8 v5, v5, 0x10

    add-int/2addr v4, v5

    int-to-byte v4, v4

    aput-byte v4, p4, v0

    .line 2905
    :cond_45
    add-int/lit8 v0, v0, 0x1

    .line 2907
    array-length v4, p2

    if-ne v0, v4, :cond_5a

    .line 2909
    if-lt p6, p7, :cond_59

    .line 2910
    sput p0, Lcom/chelpus/root/utils/corepatch;->lastPatchPosition:I

    .line 2911
    array-length v4, p4

    new-array v4, v4, [B

    sput-object v4, Lcom/chelpus/root/utils/corepatch;->lastByteReplace:[B

    .line 2912
    sget-object v4, Lcom/chelpus/root/utils/corepatch;->lastByteReplace:[B

    array-length v5, p4

    invoke-static {p4, v3, v4, v3, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2922
    .end local v0    # "i":I
    .end local v1    # "prufbyte":B
    :cond_59
    :goto_59
    return v2

    .line 2918
    .restart local v0    # "i":I
    .restart local v1    # "prufbyte":B
    :cond_5a
    sget-object v4, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v4}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v1

    goto :goto_1e

    .line 2920
    :cond_61
    sget-object v2, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    add-int/lit8 v4, p0, 0x1

    invoke-virtual {v2, v4}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .end local v0    # "i":I
    .end local v1    # "prufbyte":B
    :cond_68
    move v2, v3

    .line 2922
    goto :goto_59
.end method

.method public static main([Ljava/lang/String;)V
    .registers 363
    .param p0, "paramArrayOfString"    # [Ljava/lang/String;

    .prologue
    .line 40
    new-instance v9, Lcom/chelpus/root/utils/corepatch$1;

    invoke-direct {v9}, Lcom/chelpus/root/utils/corepatch$1;-><init>()V

    invoke-static {v9}, Lcom/chelpus/Utils;->startRootJava(Ljava/lang/Object;)V

    .line 41
    const/16 v184, 0x0

    .local v184, "pattern1":Z
    const/16 v33, 0x0

    .local v33, "pattern2":Z
    const/16 v81, 0x0

    .local v81, "pattern3":Z
    const/16 v105, 0x0

    .line 43
    .local v105, "pattern4":Z
    const/16 v212, 0x0

    .line 44
    .local v212, "byteOrig2":[B
    const/4 v0, 0x0

    move-object/16 v294, v0

    .line 45
    .local v294, "mask2":[B
    const/16 v242, 0x0

    .line 46
    .local v242, "byteReplace2":[B
    const/4 v0, 0x0

    move-object/16 v340, v0

    .line 48
    .local v340, "rep_mask2":[B
    const/4 v5, 0x0

    .line 49
    .local v5, "byteOrigOatUpd1":[B
    const/4 v6, 0x0

    .line 50
    .local v6, "maskOatUpd1":[B
    const/4 v7, 0x0

    .line 51
    .local v7, "byteReplaceOatUpd1":[B
    const/4 v8, 0x0

    .line 53
    .local v8, "rep_maskOatUpd1":[B
    const/4 v11, 0x0

    .line 54
    .local v11, "byteOrigOatUpd2":[B
    const/4 v12, 0x0

    .line 55
    .local v12, "maskOatUpd2":[B
    const/4 v13, 0x0

    .line 56
    .local v13, "byteReplaceOatUpd2":[B
    const/4 v14, 0x0

    .line 58
    .local v14, "rep_maskOatUpd2":[B
    const/16 v17, 0x0

    .line 59
    .local v17, "byteOrigOat1":[B
    const/16 v18, 0x0

    .line 60
    .local v18, "maskOat1":[B
    const/16 v19, 0x0

    .line 61
    .local v19, "byteReplaceOat1":[B
    const/16 v20, 0x0

    .line 63
    .local v20, "rep_maskOat1":[B
    const/16 v23, 0x0

    .line 64
    .local v23, "byteOrigOat2":[B
    const/16 v24, 0x0

    .line 65
    .local v24, "maskOat2":[B
    const/16 v25, 0x0

    .line 66
    .local v25, "byteReplaceOat2":[B
    const/16 v26, 0x0

    .line 68
    .local v26, "rep_maskOat2":[B
    const/16 v29, 0x0

    .line 69
    .local v29, "byteOrigOat3":[B
    const/16 v30, 0x0

    .line 70
    .local v30, "maskOat3":[B
    const/16 v31, 0x0

    .line 71
    .local v31, "byteReplaceOat3":[B
    const/16 v32, 0x0

    .line 73
    .local v32, "rep_maskOat3":[B
    const/16 v35, 0x0

    .line 74
    .local v35, "byteOrigOat6_3":[B
    const/16 v36, 0x0

    .line 75
    .local v36, "maskOat6_3":[B
    const/16 v37, 0x0

    .line 76
    .local v37, "byteReplaceOat6_3":[B
    const/16 v38, 0x0

    .line 78
    .local v38, "rep_maskOat6_3":[B
    const/16 v41, 0x0

    .line 79
    .local v41, "byteOrigOatUpd4":[B
    const/16 v42, 0x0

    .line 80
    .local v42, "maskOatUpd4":[B
    const/16 v43, 0x0

    .line 81
    .local v43, "byteReplaceOatUpd4":[B
    const/16 v44, 0x0

    .line 83
    .local v44, "rep_maskOatUpd4":[B
    const/16 v65, 0x0

    .line 84
    .local v65, "byteOrigOatUpd5":[B
    const/16 v66, 0x0

    .line 85
    .local v66, "maskOatUpd5":[B
    const/16 v67, 0x0

    .line 86
    .local v67, "byteReplaceOatUpd5":[B
    const/16 v68, 0x0

    .line 88
    .local v68, "rep_maskOatUpd5":[B
    const/16 v47, 0x0

    .line 89
    .local v47, "byteOrigOat4":[B
    const/16 v48, 0x0

    .line 90
    .local v48, "maskOat4":[B
    const/16 v49, 0x0

    .line 91
    .local v49, "byteReplaceOat4":[B
    const/16 v50, 0x0

    .line 93
    .local v50, "rep_maskOat4":[B
    const/16 v53, 0x0

    .line 94
    .local v53, "byteOrigOat5":[B
    const/16 v54, 0x0

    .line 95
    .local v54, "maskOat5":[B
    const/16 v55, 0x0

    .line 96
    .local v55, "byteReplaceOat5":[B
    const/16 v56, 0x0

    .line 98
    .local v56, "rep_maskOat5":[B
    const/16 v59, 0x0

    .line 99
    .local v59, "byteOrigOat6":[B
    const/16 v60, 0x0

    .line 100
    .local v60, "maskOat6":[B
    const/16 v61, 0x0

    .line 101
    .local v61, "byteReplaceOat6":[B
    const/16 v62, 0x0

    .line 103
    .local v62, "rep_maskOat6":[B
    const/16 v71, 0x0

    .line 104
    .local v71, "byteOrigOat7":[B
    const/16 v72, 0x0

    .line 105
    .local v72, "maskOat7":[B
    const/16 v73, 0x0

    .line 106
    .local v73, "byteReplaceOat7":[B
    const/16 v74, 0x0

    .line 109
    .local v74, "rep_maskOat7":[B
    const/16 v77, 0x0

    .line 110
    .local v77, "byteOrigSOat1":[B
    const/16 v78, 0x0

    .line 111
    .local v78, "maskSOat1":[B
    const/16 v79, 0x0

    .line 112
    .local v79, "byteReplaceSOat1":[B
    const/16 v80, 0x0

    .line 114
    .local v80, "rep_maskSOat1":[B
    const/16 v83, 0x0

    .line 115
    .local v83, "byteOrigSOat6_1":[B
    const/16 v84, 0x0

    .line 116
    .local v84, "maskSOat6_1":[B
    const/16 v85, 0x0

    .line 117
    .local v85, "byteReplaceSOat6_1":[B
    const/16 v86, 0x0

    .line 119
    .local v86, "rep_maskSOat6_1":[B
    const/16 v89, 0x0

    .line 120
    .local v89, "byteOrigSOat2":[B
    const/16 v90, 0x0

    .line 121
    .local v90, "maskSOat2":[B
    const/16 v91, 0x0

    .line 122
    .local v91, "byteReplaceSOat2":[B
    const/16 v92, 0x0

    .line 124
    .local v92, "rep_maskSOat2":[B
    const/16 v125, 0x0

    .line 125
    .local v125, "byteOrigSOat6_2":[B
    const/16 v126, 0x0

    .line 126
    .local v126, "maskSOat6_2":[B
    const/16 v127, 0x0

    .line 127
    .local v127, "byteReplaceSOat6_2":[B
    const/16 v128, 0x0

    .line 129
    .local v128, "rep_maskSOat6_2":[B
    const/16 v95, 0x0

    .line 130
    .local v95, "byteOrigSOat3":[B
    const/16 v96, 0x0

    .line 131
    .local v96, "maskSOat3":[B
    const/16 v97, 0x0

    .line 132
    .local v97, "byteReplaceSOat3":[B
    const/16 v98, 0x0

    .line 134
    .local v98, "rep_maskSOat3":[B
    const/16 v101, 0x0

    .line 135
    .local v101, "byteOrigSOat4":[B
    const/16 v102, 0x0

    .line 136
    .local v102, "maskSOat4":[B
    const/16 v103, 0x0

    .line 137
    .local v103, "byteReplaceSOat4":[B
    const/16 v104, 0x0

    .line 139
    .local v104, "rep_maskSOat4":[B
    const/16 v107, 0x0

    .line 140
    .local v107, "byteOrigSOat5":[B
    const/16 v108, 0x0

    .line 141
    .local v108, "maskSOat5":[B
    const/16 v109, 0x0

    .line 142
    .local v109, "byteReplaceSOat5":[B
    const/16 v110, 0x0

    .line 144
    .local v110, "rep_maskSOat5":[B
    const/16 v113, 0x0

    .line 145
    .local v113, "byteOrigSOat6":[B
    const/16 v114, 0x0

    .line 146
    .local v114, "maskSOat6":[B
    const/16 v115, 0x0

    .line 147
    .local v115, "byteReplaceSOat6":[B
    const/16 v116, 0x0

    .line 149
    .local v116, "rep_maskSOat6":[B
    const/16 v119, 0x0

    .line 150
    .local v119, "byteOrigSOat7":[B
    const/16 v120, 0x0

    .line 151
    .local v120, "maskSOat7":[B
    const/16 v121, 0x0

    .line 152
    .local v121, "byteReplaceSOat7":[B
    const/16 v122, 0x0

    .line 154
    .local v122, "rep_maskSOat7":[B
    const/16 v131, 0x0

    .line 155
    .local v131, "byteOrigSOat8":[B
    const/16 v132, 0x0

    .line 156
    .local v132, "maskSOat8":[B
    const/16 v133, 0x0

    .line 157
    .local v133, "byteReplaceSOat8":[B
    const/16 v134, 0x0

    .line 159
    .local v134, "rep_maskSOat8":[B
    const/16 v137, 0x0

    .line 160
    .local v137, "byteOrigSOat9":[B
    const/16 v138, 0x0

    .line 161
    .local v138, "maskSOat9":[B
    const/16 v139, 0x0

    .line 162
    .local v139, "byteReplaceSOat9":[B
    const/16 v140, 0x0

    .line 164
    .local v140, "rep_maskSOat9":[B
    const/16 v143, 0x0

    .line 165
    .local v143, "byteOrigSOat10":[B
    const/16 v144, 0x0

    .line 166
    .local v144, "maskSOat10":[B
    const/16 v145, 0x0

    .line 167
    .local v145, "byteReplaceSOat10":[B
    const/16 v146, 0x0

    .line 169
    .local v146, "rep_maskSOat10":[B
    const/16 v149, 0x0

    .line 170
    .local v149, "byteOrigSOat11":[B
    const/16 v150, 0x0

    .line 171
    .local v150, "maskSOat11":[B
    const/16 v151, 0x0

    .line 172
    .local v151, "byteReplaceSOat11":[B
    const/16 v152, 0x0

    .line 174
    .local v152, "rep_maskSOat11":[B
    const/16 v155, 0x0

    .line 175
    .local v155, "byteOrigSOat12":[B
    const/16 v156, 0x0

    .line 176
    .local v156, "maskSOat12":[B
    const/16 v157, 0x0

    .line 177
    .local v157, "byteReplaceSOat12":[B
    const/16 v158, 0x0

    .line 179
    .local v158, "rep_maskSOat12":[B
    const/16 v161, 0x0

    .line 180
    .local v161, "byteOrigSOat13":[B
    const/16 v162, 0x0

    .line 181
    .local v162, "maskSOat13":[B
    const/16 v163, 0x0

    .line 182
    .local v163, "byteReplaceSOat13":[B
    const/16 v164, 0x0

    .line 184
    .local v164, "rep_maskSOat13":[B
    const/16 v225, 0x0

    .line 185
    .local v225, "byteOrigS2":[B
    const/4 v0, 0x0

    move-object/16 v306, v0

    .line 186
    .local v306, "maskS2":[B
    const/16 v255, 0x0

    .line 187
    .local v255, "byteReplaceS2":[B
    const/4 v0, 0x0

    move-object/16 v352, v0

    .line 188
    .local v352, "rep_maskS2":[B
    const/16 v226, 0x0

    .line 189
    .local v226, "byteOrigS3":[B
    const/4 v0, 0x0

    move-object/16 v307, v0

    .line 190
    .local v307, "maskS3":[B
    const/4 v0, 0x0

    move-object/16 v256, v0

    .line 191
    .local v256, "byteReplaceS3":[B
    const/4 v0, 0x0

    move-object/16 v353, v0

    .line 192
    .local v353, "rep_maskS3":[B
    const/16 v227, 0x0

    .line 193
    .local v227, "byteOrigS4":[B
    const/4 v0, 0x0

    move-object/16 v308, v0

    .line 194
    .local v308, "maskS4":[B
    const/4 v0, 0x0

    move-object/16 v257, v0

    .line 195
    .local v257, "byteReplaceS4":[B
    const/4 v0, 0x0

    move-object/16 v354, v0

    .line 196
    .local v354, "rep_maskS4":[B
    const/16 v228, 0x0

    .line 197
    .local v228, "byteOrigS5":[B
    const/4 v0, 0x0

    move-object/16 v309, v0

    .line 198
    .local v309, "maskS5":[B
    const/4 v0, 0x0

    move-object/16 v258, v0

    .line 199
    .local v258, "byteReplaceS5":[B
    const/4 v0, 0x0

    move-object/16 v355, v0

    .line 200
    .local v355, "rep_maskS5":[B
    const/16 v229, 0x0

    .line 201
    .local v229, "byteOrigS6":[B
    const/4 v0, 0x0

    move-object/16 v310, v0

    .line 202
    .local v310, "maskS6":[B
    const/4 v0, 0x0

    move-object/16 v259, v0

    .line 203
    .local v259, "byteReplaceS6":[B
    const/4 v0, 0x0

    move-object/16 v356, v0

    .line 204
    .local v356, "rep_maskS6":[B
    const/16 v230, 0x0

    .line 205
    .local v230, "byteOrigS7":[B
    const/4 v0, 0x0

    move-object/16 v311, v0

    .line 206
    .local v311, "maskS7":[B
    const/4 v0, 0x0

    move-object/16 v260, v0

    .line 207
    .local v260, "byteReplaceS7":[B
    const/4 v0, 0x0

    move-object/16 v357, v0

    .line 208
    .local v357, "rep_maskS7":[B
    const/16 v231, 0x0

    .line 209
    .local v231, "byteOrigS8":[B
    const/4 v0, 0x0

    move-object/16 v312, v0

    .line 210
    .local v312, "maskS8":[B
    const/4 v0, 0x0

    move-object/16 v261, v0

    .line 211
    .local v261, "byteReplaceS8":[B
    const/4 v0, 0x0

    move-object/16 v358, v0

    .line 212
    .local v358, "rep_maskS8":[B
    const/16 v192, 0x0

    .line 213
    .local v192, "counter8":I
    const/16 v195, 0x0

    .line 214
    .local v195, "byteOrigS9":[B
    const/16 v196, 0x0

    .line 215
    .local v196, "maskS9":[B
    const/16 v197, 0x0

    .line 216
    .local v197, "byteReplaceS9":[B
    const/16 v198, 0x0

    .line 217
    .local v198, "rep_maskS9":[B
    const/16 v200, 0x0

    .line 218
    .local v200, "counter9":I
    const/16 v222, 0x0

    .line 219
    .local v222, "byteOrigS10":[B
    const/4 v0, 0x0

    move-object/16 v303, v0

    .line 220
    .local v303, "maskS10":[B
    const/16 v252, 0x0

    .line 221
    .local v252, "byteReplaceS10":[B
    const/4 v0, 0x0

    move-object/16 v349, v0

    .line 222
    .local v349, "rep_maskS10":[B
    const/16 v223, 0x0

    .line 223
    .local v223, "byteOrigS11":[B
    const/4 v0, 0x0

    move-object/16 v304, v0

    .line 224
    .local v304, "maskS11":[B
    const/16 v253, 0x0

    .line 225
    .local v253, "byteReplaceS11":[B
    const/4 v0, 0x0

    move-object/16 v350, v0

    .line 226
    .local v350, "rep_maskS11":[B
    const/16 v224, 0x0

    .line 227
    .local v224, "byteOrigS12":[B
    const/4 v0, 0x0

    move-object/16 v305, v0

    .line 228
    .local v305, "maskS12":[B
    const/16 v254, 0x0

    .line 229
    .local v254, "byteReplaceS12":[B
    const/4 v0, 0x0

    move-object/16 v351, v0

    .line 230
    .local v351, "rep_maskS12":[B
    const/16 v167, 0x0

    .line 231
    .local v167, "byteOrigS13":[B
    const/16 v168, 0x0

    .line 232
    .local v168, "maskS13":[B
    const/16 v169, 0x0

    .line 233
    .local v169, "byteReplaceS13":[B
    const/16 v170, 0x0

    .line 234
    .local v170, "rep_maskS13":[B
    const/16 v173, 0x0

    .line 235
    .local v173, "byteOrigS14":[B
    const/16 v174, 0x0

    .line 236
    .local v174, "maskS14":[B
    const/16 v175, 0x0

    .line 237
    .local v175, "byteReplaceS14":[B
    const/16 v176, 0x0

    .line 239
    .local v176, "rep_maskS14":[B
    const/16 v215, 0x0

    .line 240
    .local v215, "byteOrig3":[B
    const/16 v245, 0x0

    .line 241
    .local v245, "byteReplace3":[B
    const/16 v216, 0x0

    .line 242
    .local v216, "byteOrig4":[B
    const/4 v0, 0x0

    move-object/16 v297, v0

    .line 243
    .local v297, "mask4":[B
    const/16 v246, 0x0

    .line 244
    .local v246, "byteReplace4":[B
    const/4 v0, 0x0

    move-object/16 v343, v0

    .line 245
    .local v343, "rep_mask4":[B
    const/16 v217, 0x0

    .line 246
    .local v217, "byteOrig5":[B
    const/4 v0, 0x0

    move-object/16 v298, v0

    .line 247
    .local v298, "mask5":[B
    const/16 v247, 0x0

    .line 248
    .local v247, "byteReplace5":[B
    const/4 v0, 0x0

    move-object/16 v344, v0

    .line 249
    .local v344, "rep_mask5":[B
    const/16 v218, 0x0

    .line 250
    .local v218, "byteOrig6":[B
    const/4 v0, 0x0

    move-object/16 v299, v0

    .line 251
    .local v299, "mask6":[B
    const/16 v248, 0x0

    .line 252
    .local v248, "byteReplace6":[B
    const/4 v0, 0x0

    move-object/16 v345, v0

    .line 253
    .local v345, "rep_mask6":[B
    const/16 v219, 0x0

    .line 254
    .local v219, "byteOrig7":[B
    const/4 v0, 0x0

    move-object/16 v300, v0

    .line 255
    .local v300, "mask7":[B
    const/16 v249, 0x0

    .line 256
    .local v249, "byteReplace7":[B
    const/4 v0, 0x0

    move-object/16 v346, v0

    .line 257
    .local v346, "rep_mask7":[B
    const/16 v220, 0x0

    .line 258
    .local v220, "byteOrig8":[B
    const/4 v0, 0x0

    move-object/16 v301, v0

    .line 259
    .local v301, "mask8":[B
    const/16 v250, 0x0

    .line 260
    .local v250, "byteReplace8":[B
    const/4 v0, 0x0

    move-object/16 v347, v0

    .line 261
    .local v347, "rep_mask8":[B
    const/16 v221, 0x0

    .line 262
    .local v221, "byteOrig9":[B
    const/4 v0, 0x0

    move-object/16 v302, v0

    .line 263
    .local v302, "mask9":[B
    const/16 v251, 0x0

    .line 264
    .local v251, "byteReplace9":[B
    const/4 v0, 0x0

    move-object/16 v348, v0

    .line 265
    .local v348, "rep_mask9":[B
    const/16 v202, 0x0

    .line 266
    .local v202, "byteOrig10":[B
    const/4 v0, 0x0

    move-object/16 v284, v0

    .line 267
    .local v284, "mask10":[B
    const/16 v232, 0x0

    .line 268
    .local v232, "byteReplace10":[B
    const/4 v0, 0x0

    move-object/16 v330, v0

    .line 269
    .local v330, "rep_mask10":[B
    const/16 v203, 0x0

    .line 270
    .local v203, "byteOrig11":[B
    const/4 v0, 0x0

    move-object/16 v285, v0

    .line 271
    .local v285, "mask11":[B
    const/16 v233, 0x0

    .line 272
    .local v233, "byteReplace11":[B
    const/4 v0, 0x0

    move-object/16 v331, v0

    .line 273
    .local v331, "rep_mask11":[B
    const/16 v204, 0x0

    .line 274
    .local v204, "byteOrig12":[B
    const/4 v0, 0x0

    move-object/16 v286, v0

    .line 275
    .local v286, "mask12":[B
    const/16 v234, 0x0

    .line 276
    .local v234, "byteReplace12":[B
    const/4 v0, 0x0

    move-object/16 v332, v0

    .line 277
    .local v332, "rep_mask12":[B
    const/16 v205, 0x0

    .line 278
    .local v205, "byteOrig13":[B
    const/4 v0, 0x0

    move-object/16 v287, v0

    .line 279
    .local v287, "mask13":[B
    const/16 v235, 0x0

    .line 280
    .local v235, "byteReplace13":[B
    const/4 v0, 0x0

    move-object/16 v333, v0

    .line 281
    .local v333, "rep_mask13":[B
    const/16 v206, 0x0

    .line 282
    .local v206, "byteOrig14":[B
    const/4 v0, 0x0

    move-object/16 v288, v0

    .line 283
    .local v288, "mask14":[B
    const/16 v236, 0x0

    .line 284
    .local v236, "byteReplace14":[B
    const/4 v0, 0x0

    move-object/16 v334, v0

    .line 285
    .local v334, "rep_mask14":[B
    const/16 v207, 0x0

    .line 286
    .local v207, "byteOrig15":[B
    const/4 v0, 0x0

    move-object/16 v289, v0

    .line 287
    .local v289, "mask15":[B
    const/16 v237, 0x0

    .line 288
    .local v237, "byteReplace15":[B
    const/4 v0, 0x0

    move-object/16 v335, v0

    .line 289
    .local v335, "rep_mask15":[B
    const/16 v208, 0x0

    .line 290
    .local v208, "byteOrig16":[B
    const/4 v0, 0x0

    move-object/16 v290, v0

    .line 291
    .local v290, "mask16":[B
    const/16 v238, 0x0

    .line 292
    .local v238, "byteReplace16":[B
    const/4 v0, 0x0

    move-object/16 v336, v0

    .line 293
    .local v336, "rep_mask16":[B
    const/16 v209, 0x0

    .line 294
    .local v209, "byteOrig17":[B
    const/4 v0, 0x0

    move-object/16 v291, v0

    .line 295
    .local v291, "mask17":[B
    const/16 v239, 0x0

    .line 296
    .local v239, "byteReplace17":[B
    const/4 v0, 0x0

    move-object/16 v337, v0

    .line 297
    .local v337, "rep_mask17":[B
    const/16 v210, 0x0

    .line 298
    .local v210, "byteOrig18":[B
    const/4 v0, 0x0

    move-object/16 v292, v0

    .line 299
    .local v292, "mask18":[B
    const/16 v240, 0x0

    .line 300
    .local v240, "byteReplace18":[B
    const/4 v0, 0x0

    move-object/16 v338, v0

    .line 301
    .local v338, "rep_mask18":[B
    const/16 v211, 0x0

    .line 302
    .local v211, "byteOrig19":[B
    const/4 v0, 0x0

    move-object/16 v293, v0

    .line 303
    .local v293, "mask19":[B
    const/16 v241, 0x0

    .line 304
    .local v241, "byteReplace19":[B
    const/4 v0, 0x0

    move-object/16 v339, v0

    .line 305
    .local v339, "rep_mask19":[B
    const/16 v213, 0x0

    .line 306
    .local v213, "byteOrig20":[B
    const/4 v0, 0x0

    move-object/16 v295, v0

    .line 307
    .local v295, "mask20":[B
    const/16 v243, 0x0

    .line 308
    .local v243, "byteReplace20":[B
    const/4 v0, 0x0

    move-object/16 v341, v0

    .line 309
    .local v341, "rep_mask20":[B
    const/16 v214, 0x0

    .line 310
    .local v214, "byteOrig21":[B
    const/4 v0, 0x0

    move-object/16 v296, v0

    .line 311
    .local v296, "mask21":[B
    const/16 v244, 0x0

    .line 312
    .local v244, "byteReplace21":[B
    const/4 v0, 0x0

    move-object/16 v342, v0

    .line 314
    .local v342, "rep_mask21":[B
    :try_start_268
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 315
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const/4 v10, 0x1

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 316
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const/4 v10, 0x2

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 317
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const/4 v10, 0x3

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 318
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const/4 v10, 0x4

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 319
    const/4 v9, 0x4

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    if-eqz v9, :cond_2b1

    const/4 v9, 0x4

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "OnlyDalvik"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2b1

    .line 320
    const/4 v9, 0x1

    sput-boolean v9, Lcom/chelpus/root/utils/corepatch;->onlyDalvik:Z
    :try_end_2b1
    .catch Ljava/lang/Exception; {:try_start_268 .. :try_end_2b1} :catch_1f62

    .line 322
    :cond_2b1
    :goto_2b1
    const/4 v9, 0x3

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    if-eqz v9, :cond_2bf

    const/4 v9, 0x3

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    sput-object v9, Lcom/chelpus/root/utils/corepatch;->toolfilesdir:Ljava/lang/String;

    .line 323
    :cond_2bf
    const/4 v9, 0x4

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "framework"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_2e0

    const/4 v9, 0x4

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "OnlyDalvik"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_2e0

    const-string v9, "/system"

    const-string v10, "rw"

    invoke-static {v9, v10}, Lcom/chelpus/Utils;->remount(Ljava/lang/String;Ljava/lang/String;)Z

    .line 325
    :cond_2e0
    const/4 v9, 0x1

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "core.odex"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_314

    const/4 v9, 0x1

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "core.jar"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_314

    const/4 v9, 0x1

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "core-libart.jar"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_314

    const/4 v9, 0x1

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "boot.oat"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_333

    :cond_314
    const/4 v9, 0x4

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "framework"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_333

    .line 327
    const/16 v184, 0x1

    const/16 v33, 0x1

    .line 328
    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    sput-object v9, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->startUnderRoot:Ljava/lang/Boolean;

    .line 329
    const/4 v9, 0x0

    const-string v10, "patch"

    move-object/from16 v0, p0

    aput-object v10, v0, v9

    .line 331
    :cond_333
    const/4 v9, 0x1

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "services.jar"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_34d

    const/4 v9, 0x1

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "services.odex"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_36c

    :cond_34d
    const/4 v9, 0x4

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "framework"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_36c

    .line 332
    const/16 v81, 0x1

    .line 333
    const/16 v105, 0x0

    .line 334
    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    sput-object v9, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->startUnderRoot:Ljava/lang/Boolean;

    .line 335
    const/4 v9, 0x0

    const-string v10, "patch"

    move-object/from16 v0, p0

    aput-object v10, v0, v9

    .line 339
    :cond_36c
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_dbe

    .line 341
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "_patch1"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_388

    const/16 v184, 0x1

    .line 342
    :cond_388
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "_patch2"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_4990

    const/16 v33, 0x1

    move/16 v326, v33

    .line 343
    .end local v33    # "pattern2":Z
    .local v326, "pattern2":Z
    :goto_39a
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "_patch3"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_498b

    const/16 v81, 0x1

    move/16 v327, v81

    .line 344
    .end local v81    # "pattern3":Z
    .local v327, "pattern3":Z
    :goto_3ac
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "_patch4"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_4986

    const/16 v105, 0x1

    move/16 v328, v105

    .line 347
    .end local v105    # "pattern4":Z
    .local v328, "pattern4":Z
    :goto_3be
    const-string v3, "11 90 11 99 01 29 0F D1 01 26 28 1C C0 68 39 1C"

    .line 348
    .local v3, "original":Ljava/lang/String;
    const-string v4, "?? ?? 01 21 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??"

    .line 349
    .local v4, "replace":Ljava/lang/String;
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v5, v9, [B

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v6, v9, [B

    .line 350
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v7, v9, [B

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v8, v9, [B

    .line 351
    invoke-static/range {v3 .. v8}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 354
    const-string v3, "09 90 B0 42 0C D1 D5 F8 DC 0B 39 1C D0 F8 24 E0 F0 47"

    .line 355
    const-string v4, "?? ?? 80 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??"

    .line 356
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v11, v9, [B

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v12, v9, [B

    .line 357
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v13, v9, [B

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v14, v9, [B

    move-object v9, v3

    move-object v10, v4

    .line 358
    invoke-static/range {v9 .. v14}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 362
    const-string v3, "39 1C 08 68 52 46 D0 F8 CC 01 D0 F8 ?? E0 F0 47 05 1C 01 3C 00 F0 04 80 28 1C 06 B0 BD E8 E0 85 D9 F8 ?? E2 F0 47 F7 E7"

    .line 363
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 01 20 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??"

    .line 364
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v17, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v18, v0

    .line 365
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v19, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v20, v0

    move-object v15, v3

    move-object/from16 v16, v4

    .line 366
    invoke-static/range {v15 .. v20}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 369
    const-string v3, "08 68 3A 1C D0 F8 D0 01 43 46 D0 F8 ?? E0 F0 47 05 1C 01 3C 00 F0 04 80 28 1C 09 B0 BD E8 E0 8D D9 F8 ?? E2 F0 47 F7 E7"

    .line 370
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 01 20 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??"

    .line 371
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v23, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v24, v0

    .line 372
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v25, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v26, v0

    move-object/from16 v21, v3

    move-object/from16 v22, v4

    .line 373
    invoke-static/range {v21 .. v26}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 376
    const-string v3, "56 45 00 F0 07 80 01 3C 00 F0 31 80 05 98"

    .line 377
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 01 20"

    .line 378
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v29, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v30, v0

    .line 379
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v31, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v32, v0

    move-object/from16 v27, v3

    move-object/from16 v28, v4

    .line 380
    invoke-static/range {v27 .. v32}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 383
    const-string v3, "56 45 03 d0 00 20 09 b0 bd e8 e0 8d 00 27 00 25"

    .line 384
    const-string v4, "?? ?? 00 00 01 20 ?? ?? ?? ?? ?? ?? ?? ?? ?? ??"

    .line 385
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v35, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v36, v0

    .line 386
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v37, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v38, v0

    move-object/from16 v33, v3

    move-object/from16 v34, v4

    .line 387
    invoke-static/range {v33 .. v38}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 392
    const-string v3, "89 44 24 40 8B 5C 24 40 83 FB 01 75 32 BE 01 00 00 00 8B C5 8B 40 0C 8B CF"

    .line 393
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 90 90 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??"

    .line 394
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v41, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v42, v0

    .line 395
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v43, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v44, v0

    move-object/from16 v39, v3

    move-object/from16 v40, v4

    .line 396
    invoke-static/range {v39 .. v44}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 399
    const-string v3, "8B 54 24 38 8B CF 8B 01 8B 80 CC 01 00 00 FF 50 28 64 66 83 3D 00 00 00 00 00 8B E8 75 12 8B C5"

    .line 400
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? B0 01"

    .line 401
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v47, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v48, v0

    .line 402
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v49, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v50, v0

    move-object/from16 v45, v3

    move-object/from16 v46, v4

    .line 403
    invoke-static/range {v45 .. v50}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 406
    const-string v3, "8B 80 D0 01 00 00 8B D7 FF 50 28 64 66 83 3D 00 00 00 00 00 8B E8 75 12 8B C5 8B 6C 24 20"

    .line 407
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? B0 01 ?? ?? ?? ??"

    .line 408
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v53, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v54, v0

    .line 409
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v55, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v56, v0

    move-object/from16 v51, v3

    move-object/from16 v52, v4

    .line 410
    invoke-static/range {v51 .. v56}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 413
    const-string v3, "33 D2 89 54 24 10 8B 73 08 8B 44 24 38 8B 48 08 89 4C 24 18 3B F1 74 23 64 66 83 3D 00 00 00 00 00 0F 85 80 00 00 00"

    .line 414
    const-string v4, "B3 01 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??"

    .line 415
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v59, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v60, v0

    .line 416
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v61, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v62, v0

    move-object/from16 v57, v3

    move-object/from16 v58, v4

    .line 417
    invoke-static/range {v57 .. v62}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 421
    const-string v3, "E5 03 1A AA C0 03 3F D6 FB 03 00 2A 7F 07 00 71 61 02 00 54 35 00 80 52 E1 03 16 AA 2A 00 40 B9 E0 03 14 AA"

    .line 422
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 35 00 80 52 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??"

    .line 423
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v65, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v66, v0

    .line 424
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v67, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v68, v0

    move-object/from16 v63, v3

    move-object/from16 v64, v4

    .line 425
    invoke-static/range {v63 .. v68}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 428
    const-string v3, "F7 03 01 AA F9 03 02 AA FA 03 1F 2A F5 0A 40 B9 38 0B 40 B9 BF 02 18 6B"

    .line 429
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? 3A 00 80 52 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??"

    .line 430
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v71, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v72, v0

    .line 431
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v73, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v74, v0

    move-object/from16 v69, v3

    move-object/from16 v70, v4

    .line 432
    invoke-static/range {v69 .. v74}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 436
    const-string v3, "CD F8 30 C0 78 B9 1A 99 31 B9 01 3C 00 F0 ?? 80 0B 98"

    .line 437
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 00 20"

    .line 438
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v77, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v78, v0

    .line 439
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v79, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v80, v0

    move-object/from16 v75, v3

    move-object/from16 v76, v4

    .line 440
    invoke-static/range {v75 .. v80}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 443
    const-string v3, "00 29 40 F0 ?? 80 00 2A 40 F0 ?? 80 01 20 00 F0 02 B8"

    .line 444
    const-string v4, "00 45 ?? ?? ?? ?? 00 45 ?? ?? ?? ?? 00 ?? ?? ?? ?? ??"

    .line 445
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v83, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v84, v0

    .line 446
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v85, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v86, v0

    move-object/from16 v81, v3

    move-object/from16 v82, v4

    .line 447
    invoke-static/range {v81 .. v86}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 451
    const-string v3, "00 2D 40 F0 ?? 80 16 99 CD 69 DA F8 E0 20 0B 92 0B 9B 9D 42"

    .line 452
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 9B ??"

    .line 453
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v89, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v90, v0

    .line 454
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v91, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v92, v0

    move-object/from16 v87, v3

    move-object/from16 v88, v4

    .line 455
    invoke-static/range {v87 .. v92}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 458
    const-string v3, "2D E9 E0 4D 8D B0 07 1C 00 90 15 91 16 92 17 93 16 9A 56 6A 31 1C 08 68 D0 F8 BC 01 D0 F8 28 E0 F0 47"

    .line 459
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 01 20 0D B0 BD E8 E0 8D ?? ??"

    .line 460
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v95, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v96, v0

    .line 461
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v97, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v98, v0

    move-object/from16 v93, v3

    move-object/from16 v94, v4

    .line 462
    invoke-static/range {v93 .. v98}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 465
    const-string v3, "1C BF D9 F8 B0 E1 F0 47 BF F3 5B 8F 31 1C 08 68 D0 F8 ?? 02 D0 F8 28 E0 F0 47 14 90 14 ?? 00 ?? ?? D1"

    .line 466
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 00 20 ?? ?? ?? ?? ?? ?? ?? ??"

    .line 467
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v101, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v102, v0

    .line 468
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v103, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v104, v0

    move-object/from16 v99, v3

    move-object/from16 v100, v4

    .line 469
    invoke-static/range {v99 .. v104}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 472
    const-string v3, "F0 47 82 46 BA F1 00 0F ?? D1 01 3C ?? F0 ?? ?? 28 ?? 00 2B"

    .line 473
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? ?? E0 ?? ?? ?? ?? ?? ?? ?? ?? ?? ??"

    .line 474
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v107, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v108, v0

    .line 475
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v109, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v110, v0

    move-object/from16 v105, v3

    move-object/from16 v106, v4

    .line 476
    invoke-static/range {v105 .. v110}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 479
    const-string v3, "F8 0C 00 D0 F8 ?? E0 F0 47 80 46 B8 F1 00 0F ?? D1 D9 F8 ?? ?? 39 1C"

    .line 480
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? E0 ?? ?? ?? ?? ?? ??"

    .line 481
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v113, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v114, v0

    .line 482
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v115, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v116, v0

    move-object/from16 v111, v3

    move-object/from16 v112, v4

    .line 483
    invoke-static/range {v111 .. v116}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 486
    const-string v3, "50 F8 0C 00 D0 F8 ?? E0 F0 47 06 1C ?? BB D9 F8 24 E1 29 1C ?? ?? ?? ?? F0 47"

    .line 487
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? 01 20 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??"

    .line 488
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v119, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v120, v0

    .line 489
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v121, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v122, v0

    move-object/from16 v117, v3

    move-object/from16 v118, v4

    .line 490
    invoke-static/range {v117 .. v122}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 493
    const-string v3, "05 F0 80 05 ?? B9 16 9A 51 46 30 1C 40 68"

    .line 494
    const-string v4, "45 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??"

    .line 495
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v125, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v126, v0

    .line 496
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v127, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v128, v0

    move-object/from16 v123, v3

    move-object/from16 v124, v4

    .line 497
    invoke-static/range {v123 .. v128}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 502
    const-string v3, "BA 01 00 00 00 89 54 24 28 BB FD FF FF FF 89 5C 24 30 8B 4C 24 54 33 C0 89 44 24 2C 85 C9 75 44 8B 54 24 58 85 D2 75 23"

    .line 503
    const-string v4, "?? 00 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 90 90 ?? ?? ?? ?? ?? ?? 90 90"

    .line 504
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v131, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v132, v0

    .line 505
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v133, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v134, v0

    move-object/from16 v129, v3

    move-object/from16 v130, v4

    .line 506
    invoke-static/range {v129 .. v134}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 509
    const-string v3, "85 ED 0F 85 ?? ?? 00 00 8B 54 24 58 8B 6A 1C 8B 5C 24 24 8B 83 ?? 00 00 00 89 44 24 38 8B 4C 24 38 3B E9"

    .line 510
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ED"

    .line 511
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v137, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v138, v0

    .line 512
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v139, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v140, v0

    move-object/from16 v135, v3

    move-object/from16 v136, v4

    .line 513
    invoke-static/range {v135 .. v140}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 517
    const-string v3, "3D 00 80 52 42 00 80 12 E2 3B 00 B9 E3 03 1F 2A E3 37 00 B9 3B 02 00 B5 9C 01 00 B5"

    .line 518
    const-string v4, "FD 03 1F 2A ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? FD 03 1F 2A FD 03 1F 2A"

    .line 519
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v143, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v144, v0

    .line 520
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v145, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v146, v0

    move-object/from16 v141, v3

    move-object/from16 v142, v4

    .line 521
    invoke-static/range {v141 .. v146}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 524
    const-string v3, "3C 00 80 52 42 00 80 12 E2 3B 00 B9 E3 03 1F 2A E3 37 00 B9 3A 02 00 B5 9B 01 00 B5"

    .line 525
    const-string v4, "FC 03 1F 2A ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? FC 03 1F 2A FC 03 1F 2A"

    .line 526
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v149, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v150, v0

    .line 527
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v151, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v152, v0

    move-object/from16 v147, v3

    move-object/from16 v148, v4

    .line 528
    invoke-static/range {v147 .. v152}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 531
    const-string v3, "E2 33 00 B9 E3 33 40 B9 9F 02 03 6B 6A 13 00 54 A0 16 40 B9"

    .line 532
    const-string v4, "?? ?? ?? ?? E3 03 14 2A ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??"

    .line 533
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v155, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v156, v0

    .line 534
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v157, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v158, v0

    move-object/from16 v153, v3

    move-object/from16 v154, v4

    .line 535
    invoke-static/range {v153 .. v158}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 538
    const-string v3, "D6 02 19 12 56 01 00 35 E1 03 18 AA E0 03 15 AA 00 0C 40 B9 E2 03 1B AA"

    .line 539
    const-string v4, "D6 02 19 32 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??"

    .line 540
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v161, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v162, v0

    .line 541
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v163, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v164, v0

    move-object/from16 v159, v3

    move-object/from16 v160, v4

    .line 542
    invoke-static/range {v159 .. v164}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 546
    const/16 v9, 0x10

    new-array v0, v9, [B

    move-object/from16 v225, v0

    .end local v225    # "byteOrigS2":[B
    fill-array-data v225, :array_4996

    .line 547
    .restart local v225    # "byteOrigS2":[B
    const/16 v9, 0x10

    new-array v0, v9, [B

    move-object/16 v306, v0

    .end local v306    # "maskS2":[B
    move-object/from16 v0, v306

    fill-array-data v0, :array_49a2

    .line 548
    .restart local v306    # "maskS2":[B
    const/16 v9, 0x10

    new-array v0, v9, [B

    move-object/from16 v255, v0

    .end local v255    # "byteReplaceS2":[B
    fill-array-data v255, :array_49ae

    .line 549
    .restart local v255    # "byteReplaceS2":[B
    const/16 v9, 0x10

    new-array v0, v9, [B

    move-object/16 v352, v0

    .end local v352    # "rep_maskS2":[B
    move-object/from16 v0, v352

    fill-array-data v0, :array_49ba

    .line 552
    .restart local v352    # "rep_maskS2":[B
    const/16 v9, 0x18

    new-array v0, v9, [B

    move-object/from16 v226, v0

    .end local v226    # "byteOrigS3":[B
    fill-array-data v226, :array_49c6

    .line 553
    .restart local v226    # "byteOrigS3":[B
    const/16 v9, 0x18

    new-array v0, v9, [B

    move-object/16 v307, v0

    .end local v307    # "maskS3":[B
    move-object/from16 v0, v307

    fill-array-data v0, :array_49d6

    .line 554
    .restart local v307    # "maskS3":[B
    const/16 v9, 0x18

    new-array v0, v9, [B

    move-object/16 v256, v0

    .end local v256    # "byteReplaceS3":[B
    move-object/from16 v0, v256

    fill-array-data v0, :array_49e6

    .line 555
    .restart local v256    # "byteReplaceS3":[B
    const/16 v9, 0x18

    new-array v0, v9, [B

    move-object/16 v353, v0

    .end local v353    # "rep_maskS3":[B
    move-object/from16 v0, v353

    fill-array-data v0, :array_49f6

    .line 558
    .restart local v353    # "rep_maskS3":[B
    const/16 v9, 0x13

    new-array v0, v9, [B

    move-object/from16 v227, v0

    .end local v227    # "byteOrigS4":[B
    fill-array-data v227, :array_4a06

    .line 559
    .restart local v227    # "byteOrigS4":[B
    const/16 v9, 0x13

    new-array v0, v9, [B

    move-object/16 v308, v0

    .end local v308    # "maskS4":[B
    move-object/from16 v0, v308

    fill-array-data v0, :array_4a14

    .line 560
    .restart local v308    # "maskS4":[B
    const/16 v9, 0x13

    new-array v0, v9, [B

    move-object/16 v257, v0

    .end local v257    # "byteReplaceS4":[B
    move-object/from16 v0, v257

    fill-array-data v0, :array_4a22

    .line 561
    .restart local v257    # "byteReplaceS4":[B
    const/16 v9, 0x13

    new-array v0, v9, [B

    move-object/16 v354, v0

    .end local v354    # "rep_maskS4":[B
    move-object/from16 v0, v354

    fill-array-data v0, :array_4a30

    .line 564
    .restart local v354    # "rep_maskS4":[B
    const/16 v9, 0xe

    new-array v0, v9, [B

    move-object/from16 v228, v0

    .end local v228    # "byteOrigS5":[B
    fill-array-data v228, :array_4a3e

    .line 565
    .restart local v228    # "byteOrigS5":[B
    const/16 v9, 0xe

    new-array v0, v9, [B

    move-object/16 v309, v0

    .end local v309    # "maskS5":[B
    move-object/from16 v0, v309

    fill-array-data v0, :array_4a4a

    .line 566
    .restart local v309    # "maskS5":[B
    const/16 v9, 0xe

    new-array v0, v9, [B

    move-object/16 v258, v0

    .end local v258    # "byteReplaceS5":[B
    move-object/from16 v0, v258

    fill-array-data v0, :array_4a56

    .line 567
    .restart local v258    # "byteReplaceS5":[B
    const/16 v9, 0xe

    new-array v0, v9, [B

    move-object/16 v355, v0

    .end local v355    # "rep_maskS5":[B
    move-object/from16 v0, v355

    fill-array-data v0, :array_4a62

    .line 570
    .restart local v355    # "rep_maskS5":[B
    const/16 v9, 0x14

    new-array v0, v9, [B

    move-object/from16 v229, v0

    .end local v229    # "byteOrigS6":[B
    fill-array-data v229, :array_4a6e

    .line 571
    .restart local v229    # "byteOrigS6":[B
    const/16 v9, 0x14

    new-array v0, v9, [B

    move-object/16 v310, v0

    .end local v310    # "maskS6":[B
    move-object/from16 v0, v310

    fill-array-data v0, :array_4a7c

    .line 572
    .restart local v310    # "maskS6":[B
    const/16 v9, 0x14

    new-array v0, v9, [B

    move-object/16 v259, v0

    .end local v259    # "byteReplaceS6":[B
    move-object/from16 v0, v259

    fill-array-data v0, :array_4a8a

    .line 573
    .restart local v259    # "byteReplaceS6":[B
    const/16 v9, 0x14

    new-array v0, v9, [B

    move-object/16 v356, v0

    .end local v356    # "rep_maskS6":[B
    move-object/from16 v0, v356

    fill-array-data v0, :array_4a98

    .line 577
    .restart local v356    # "rep_maskS6":[B
    const/16 v9, 0xe

    new-array v0, v9, [B

    move-object/from16 v230, v0

    .end local v230    # "byteOrigS7":[B
    fill-array-data v230, :array_4aa6

    .line 578
    .restart local v230    # "byteOrigS7":[B
    const/16 v9, 0xe

    new-array v0, v9, [B

    move-object/16 v311, v0

    .end local v311    # "maskS7":[B
    move-object/from16 v0, v311

    fill-array-data v0, :array_4ab2

    .line 579
    .restart local v311    # "maskS7":[B
    const/16 v9, 0xe

    new-array v0, v9, [B

    move-object/16 v260, v0

    .end local v260    # "byteReplaceS7":[B
    move-object/from16 v0, v260

    fill-array-data v0, :array_4abe

    .line 580
    .restart local v260    # "byteReplaceS7":[B
    const/16 v9, 0xe

    new-array v0, v9, [B

    move-object/16 v357, v0

    .end local v357    # "rep_maskS7":[B
    move-object/from16 v0, v357

    fill-array-data v0, :array_4aca

    .line 583
    .restart local v357    # "rep_maskS7":[B
    const/16 v9, 0x1c

    new-array v0, v9, [B

    move-object/from16 v231, v0

    .end local v231    # "byteOrigS8":[B
    fill-array-data v231, :array_4ad6

    .line 584
    .restart local v231    # "byteOrigS8":[B
    const/16 v9, 0x1c

    new-array v0, v9, [B

    move-object/16 v312, v0

    .end local v312    # "maskS8":[B
    move-object/from16 v0, v312

    fill-array-data v0, :array_4ae8

    .line 585
    .restart local v312    # "maskS8":[B
    const/16 v9, 0x1c

    new-array v0, v9, [B

    move-object/16 v261, v0

    .end local v261    # "byteReplaceS8":[B
    move-object/from16 v0, v261

    fill-array-data v0, :array_4afa

    .line 586
    .restart local v261    # "byteReplaceS8":[B
    const/16 v9, 0x1c

    new-array v0, v9, [B

    move-object/16 v358, v0

    .end local v358    # "rep_maskS8":[B
    move-object/from16 v0, v358

    fill-array-data v0, :array_4b0c

    .line 587
    .restart local v358    # "rep_maskS8":[B
    const/16 v192, 0x2

    .line 590
    const/16 v9, 0x26

    new-array v0, v9, [B

    move-object/from16 v195, v0

    .end local v195    # "byteOrigS9":[B
    fill-array-data v195, :array_4b1e

    .line 591
    .restart local v195    # "byteOrigS9":[B
    const/16 v9, 0x26

    new-array v0, v9, [B

    move-object/from16 v196, v0

    .end local v196    # "maskS9":[B
    fill-array-data v196, :array_4b36

    .line 592
    .restart local v196    # "maskS9":[B
    const/16 v9, 0x26

    new-array v0, v9, [B

    move-object/from16 v197, v0

    .end local v197    # "byteReplaceS9":[B
    fill-array-data v197, :array_4b4e

    .line 593
    .restart local v197    # "byteReplaceS9":[B
    const/16 v9, 0x26

    new-array v0, v9, [B

    move-object/from16 v198, v0

    .end local v198    # "rep_maskS9":[B
    fill-array-data v198, :array_4b66

    .line 594
    .restart local v198    # "rep_maskS9":[B
    const/16 v200, 0x2

    .line 597
    const/16 v9, 0x1d

    new-array v0, v9, [B

    move-object/from16 v222, v0

    .end local v222    # "byteOrigS10":[B
    fill-array-data v222, :array_4b7e

    .line 598
    .restart local v222    # "byteOrigS10":[B
    const/16 v9, 0x1d

    new-array v0, v9, [B

    move-object/16 v303, v0

    .end local v303    # "maskS10":[B
    move-object/from16 v0, v303

    fill-array-data v0, :array_4b92

    .line 599
    .restart local v303    # "maskS10":[B
    const/16 v9, 0x1d

    new-array v0, v9, [B

    move-object/from16 v252, v0

    .end local v252    # "byteReplaceS10":[B
    fill-array-data v252, :array_4ba6

    .line 600
    .restart local v252    # "byteReplaceS10":[B
    const/16 v9, 0x1d

    new-array v0, v9, [B

    move-object/16 v349, v0

    .end local v349    # "rep_maskS10":[B
    move-object/from16 v0, v349

    fill-array-data v0, :array_4bba

    .line 603
    .restart local v349    # "rep_maskS10":[B
    const/16 v9, 0x1d

    new-array v0, v9, [B

    move-object/from16 v223, v0

    .end local v223    # "byteOrigS11":[B
    fill-array-data v223, :array_4bce

    .line 604
    .restart local v223    # "byteOrigS11":[B
    const/16 v9, 0x1d

    new-array v0, v9, [B

    move-object/16 v304, v0

    .end local v304    # "maskS11":[B
    move-object/from16 v0, v304

    fill-array-data v0, :array_4be2

    .line 605
    .restart local v304    # "maskS11":[B
    const/16 v9, 0x1d

    new-array v0, v9, [B

    move-object/from16 v253, v0

    .end local v253    # "byteReplaceS11":[B
    fill-array-data v253, :array_4bf6

    .line 606
    .restart local v253    # "byteReplaceS11":[B
    const/16 v9, 0x1d

    new-array v0, v9, [B

    move-object/16 v350, v0

    .end local v350    # "rep_maskS11":[B
    move-object/from16 v0, v350

    fill-array-data v0, :array_4c0a

    .line 609
    .restart local v350    # "rep_maskS11":[B
    const/16 v9, 0x19

    new-array v0, v9, [B

    move-object/from16 v224, v0

    .end local v224    # "byteOrigS12":[B
    fill-array-data v224, :array_4c1e

    .line 610
    .restart local v224    # "byteOrigS12":[B
    const/16 v9, 0x19

    new-array v0, v9, [B

    move-object/16 v305, v0

    .end local v305    # "maskS12":[B
    move-object/from16 v0, v305

    fill-array-data v0, :array_4c30

    .line 611
    .restart local v305    # "maskS12":[B
    const/16 v9, 0x19

    new-array v0, v9, [B

    move-object/from16 v254, v0

    .end local v254    # "byteReplaceS12":[B
    fill-array-data v254, :array_4c42

    .line 612
    .restart local v254    # "byteReplaceS12":[B
    const/16 v9, 0x19

    new-array v0, v9, [B

    move-object/16 v351, v0

    .end local v351    # "rep_maskS12":[B
    move-object/from16 v0, v351

    fill-array-data v0, :array_4c54

    .line 615
    .restart local v351    # "rep_maskS12":[B
    const-string v3, "D5 ?? 80 00 39 ?? ?? ?? 71 20 ?? ?? ?? ?? 54 ?? ?? ?? 52 ?? ?? ?? DD ?? ?? 01 38 ?? ?? 00"

    .line 616
    const-string v4, "D6 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??"

    .line 617
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v167, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v168, v0

    .line 618
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v169, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v170, v0

    move-object/from16 v165, v3

    move-object/from16 v166, v4

    .line 619
    invoke-static/range {v165 .. v170}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 622
    const-string v3, "39 ?? 07 00 39 ?? 03 00 0F 03 12 F3 28 FE"

    .line 623
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? 12 03 12 03 0F 03"

    .line 624
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v173, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v174, v0

    .line 625
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v175, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v176, v0

    move-object/from16 v171, v3

    move-object/from16 v172, v4

    .line 626
    invoke-static/range {v171 .. v176}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 629
    const/16 v9, 0x23

    new-array v0, v9, [B

    move-object/from16 v202, v0

    .end local v202    # "byteOrig10":[B
    fill-array-data v202, :array_4c66

    .line 630
    .restart local v202    # "byteOrig10":[B
    const/16 v9, 0x23

    new-array v0, v9, [B

    move-object/16 v284, v0

    .end local v284    # "mask10":[B
    move-object/from16 v0, v284

    fill-array-data v0, :array_4c7c

    .line 631
    .restart local v284    # "mask10":[B
    const/16 v9, 0x23

    new-array v0, v9, [B

    move-object/from16 v232, v0

    .end local v232    # "byteReplace10":[B
    fill-array-data v232, :array_4c92

    .line 632
    .restart local v232    # "byteReplace10":[B
    const/16 v9, 0x23

    new-array v0, v9, [B

    move-object/16 v330, v0

    .end local v330    # "rep_mask10":[B
    move-object/from16 v0, v330

    fill-array-data v0, :array_4ca8

    .line 635
    .restart local v330    # "rep_mask10":[B
    const/16 v9, 0x24

    new-array v0, v9, [B

    move-object/from16 v203, v0

    .end local v203    # "byteOrig11":[B
    fill-array-data v203, :array_4cbe

    .line 636
    .restart local v203    # "byteOrig11":[B
    const/16 v9, 0x24

    new-array v0, v9, [B

    move-object/16 v285, v0

    .end local v285    # "mask11":[B
    move-object/from16 v0, v285

    fill-array-data v0, :array_4cd4

    .line 637
    .restart local v285    # "mask11":[B
    const/16 v9, 0x24

    new-array v0, v9, [B

    move-object/from16 v233, v0

    .end local v233    # "byteReplace11":[B
    fill-array-data v233, :array_4cea

    .line 638
    .restart local v233    # "byteReplace11":[B
    const/16 v9, 0x24

    new-array v0, v9, [B

    move-object/16 v331, v0

    .end local v331    # "rep_mask11":[B
    move-object/from16 v0, v331

    fill-array-data v0, :array_4d00

    .line 641
    .restart local v331    # "rep_mask11":[B
    const/16 v9, 0x1e

    new-array v0, v9, [B

    move-object/from16 v204, v0

    .end local v204    # "byteOrig12":[B
    fill-array-data v204, :array_4d16

    .line 642
    .restart local v204    # "byteOrig12":[B
    const/16 v9, 0x1e

    new-array v0, v9, [B

    move-object/16 v286, v0

    .end local v286    # "mask12":[B
    move-object/from16 v0, v286

    fill-array-data v0, :array_4d2a

    .line 643
    .restart local v286    # "mask12":[B
    const/16 v9, 0x1e

    new-array v0, v9, [B

    move-object/from16 v234, v0

    .end local v234    # "byteReplace12":[B
    fill-array-data v234, :array_4d3e

    .line 644
    .restart local v234    # "byteReplace12":[B
    const/16 v9, 0x1e

    new-array v0, v9, [B

    move-object/16 v332, v0

    .end local v332    # "rep_mask12":[B
    move-object/from16 v0, v332

    fill-array-data v0, :array_4d52

    .line 647
    .restart local v332    # "rep_mask12":[B
    const/16 v9, 0x1c

    new-array v0, v9, [B

    move-object/from16 v205, v0

    .end local v205    # "byteOrig13":[B
    fill-array-data v205, :array_4d66

    .line 648
    .restart local v205    # "byteOrig13":[B
    const/16 v9, 0x1c

    new-array v0, v9, [B

    move-object/16 v287, v0

    .end local v287    # "mask13":[B
    move-object/from16 v0, v287

    fill-array-data v0, :array_4d78

    .line 649
    .restart local v287    # "mask13":[B
    const/16 v9, 0x1c

    new-array v0, v9, [B

    move-object/from16 v235, v0

    .end local v235    # "byteReplace13":[B
    fill-array-data v235, :array_4d8a

    .line 650
    .restart local v235    # "byteReplace13":[B
    const/16 v9, 0x1c

    new-array v0, v9, [B

    move-object/16 v333, v0

    .end local v333    # "rep_mask13":[B
    move-object/from16 v0, v333

    fill-array-data v0, :array_4d9c

    .line 653
    .restart local v333    # "rep_mask13":[B
    const/16 v9, 0x2b

    new-array v0, v9, [B

    move-object/from16 v206, v0

    .end local v206    # "byteOrig14":[B
    fill-array-data v206, :array_4dae

    .line 654
    .restart local v206    # "byteOrig14":[B
    const/16 v9, 0x2b

    new-array v0, v9, [B

    move-object/16 v288, v0

    .end local v288    # "mask14":[B
    move-object/from16 v0, v288

    fill-array-data v0, :array_4dc8

    .line 655
    .restart local v288    # "mask14":[B
    const/16 v9, 0x2b

    new-array v0, v9, [B

    move-object/from16 v236, v0

    .end local v236    # "byteReplace14":[B
    fill-array-data v236, :array_4de2

    .line 656
    .restart local v236    # "byteReplace14":[B
    const/16 v9, 0x2b

    new-array v0, v9, [B

    move-object/16 v334, v0

    .end local v334    # "rep_mask14":[B
    move-object/from16 v0, v334

    fill-array-data v0, :array_4dfc

    .line 659
    .restart local v334    # "rep_mask14":[B
    const/16 v9, 0x2c

    new-array v0, v9, [B

    move-object/from16 v207, v0

    .end local v207    # "byteOrig15":[B
    fill-array-data v207, :array_4e16

    .line 660
    .restart local v207    # "byteOrig15":[B
    const/16 v9, 0x2c

    new-array v0, v9, [B

    move-object/16 v289, v0

    .end local v289    # "mask15":[B
    move-object/from16 v0, v289

    fill-array-data v0, :array_4e30

    .line 661
    .restart local v289    # "mask15":[B
    const/16 v9, 0x2c

    new-array v0, v9, [B

    move-object/from16 v237, v0

    .end local v237    # "byteReplace15":[B
    fill-array-data v237, :array_4e4a

    .line 662
    .restart local v237    # "byteReplace15":[B
    const/16 v9, 0x2c

    new-array v0, v9, [B

    move-object/16 v335, v0

    .end local v335    # "rep_mask15":[B
    move-object/from16 v0, v335

    fill-array-data v0, :array_4e64

    .line 665
    .restart local v335    # "rep_mask15":[B
    const/16 v9, 0x46

    new-array v0, v9, [B

    move-object/from16 v208, v0

    .end local v208    # "byteOrig16":[B
    fill-array-data v208, :array_4e7e

    .line 666
    .restart local v208    # "byteOrig16":[B
    const/16 v9, 0x46

    new-array v0, v9, [B

    move-object/16 v290, v0

    .end local v290    # "mask16":[B
    move-object/from16 v0, v290

    fill-array-data v0, :array_4ea6

    .line 667
    .restart local v290    # "mask16":[B
    const/16 v9, 0x46

    new-array v0, v9, [B

    move-object/from16 v238, v0

    .end local v238    # "byteReplace16":[B
    fill-array-data v238, :array_4ece

    .line 668
    .restart local v238    # "byteReplace16":[B
    const/16 v9, 0x46

    new-array v0, v9, [B

    move-object/16 v336, v0

    .end local v336    # "rep_mask16":[B
    move-object/from16 v0, v336

    fill-array-data v0, :array_4ef6

    .line 671
    .restart local v336    # "rep_mask16":[B
    const/16 v9, 0x38

    new-array v0, v9, [B

    move-object/from16 v209, v0

    .end local v209    # "byteOrig17":[B
    fill-array-data v209, :array_4f1e

    .line 672
    .restart local v209    # "byteOrig17":[B
    const/16 v9, 0x38

    new-array v0, v9, [B

    move-object/16 v291, v0

    .end local v291    # "mask17":[B
    move-object/from16 v0, v291

    fill-array-data v0, :array_4f3e

    .line 673
    .restart local v291    # "mask17":[B
    const/16 v9, 0x38

    new-array v0, v9, [B

    move-object/from16 v239, v0

    .end local v239    # "byteReplace17":[B
    fill-array-data v239, :array_4f5e

    .line 674
    .restart local v239    # "byteReplace17":[B
    const/16 v9, 0x38

    new-array v0, v9, [B

    move-object/16 v337, v0

    .end local v337    # "rep_mask17":[B
    move-object/from16 v0, v337

    fill-array-data v0, :array_4f7e

    .line 677
    .restart local v337    # "rep_mask17":[B
    const/16 v9, 0x29

    new-array v0, v9, [B

    move-object/from16 v210, v0

    .end local v210    # "byteOrig18":[B
    fill-array-data v210, :array_4f9e

    .line 678
    .restart local v210    # "byteOrig18":[B
    const/16 v9, 0x29

    new-array v0, v9, [B

    move-object/16 v292, v0

    .end local v292    # "mask18":[B
    move-object/from16 v0, v292

    fill-array-data v0, :array_4fb8

    .line 679
    .restart local v292    # "mask18":[B
    const/16 v9, 0x29

    new-array v0, v9, [B

    move-object/from16 v240, v0

    .end local v240    # "byteReplace18":[B
    fill-array-data v240, :array_4fd2

    .line 680
    .restart local v240    # "byteReplace18":[B
    const/16 v9, 0x29

    new-array v0, v9, [B

    move-object/16 v338, v0

    .end local v338    # "rep_mask18":[B
    move-object/from16 v0, v338

    fill-array-data v0, :array_4fec

    .line 683
    .restart local v338    # "rep_mask18":[B
    const/16 v9, 0x32

    new-array v0, v9, [B

    move-object/from16 v211, v0

    .end local v211    # "byteOrig19":[B
    fill-array-data v211, :array_5006

    .line 684
    .restart local v211    # "byteOrig19":[B
    const/16 v9, 0x32

    new-array v0, v9, [B

    move-object/16 v293, v0

    .end local v293    # "mask19":[B
    move-object/from16 v0, v293

    fill-array-data v0, :array_5024

    .line 685
    .restart local v293    # "mask19":[B
    const/16 v9, 0x32

    new-array v0, v9, [B

    move-object/from16 v241, v0

    .end local v241    # "byteReplace19":[B
    fill-array-data v241, :array_5042

    .line 686
    .restart local v241    # "byteReplace19":[B
    const/16 v9, 0x32

    new-array v0, v9, [B

    move-object/16 v339, v0

    .end local v339    # "rep_mask19":[B
    move-object/from16 v0, v339

    fill-array-data v0, :array_5060

    .line 689
    .restart local v339    # "rep_mask19":[B
    const/16 v9, 0x1a

    new-array v0, v9, [B

    move-object/from16 v213, v0

    .end local v213    # "byteOrig20":[B
    fill-array-data v213, :array_507e

    .line 690
    .restart local v213    # "byteOrig20":[B
    const/16 v9, 0x1a

    new-array v0, v9, [B

    move-object/16 v295, v0

    .end local v295    # "mask20":[B
    move-object/from16 v0, v295

    fill-array-data v0, :array_5090

    .line 691
    .restart local v295    # "mask20":[B
    const/16 v9, 0x1a

    new-array v0, v9, [B

    move-object/from16 v243, v0

    .end local v243    # "byteReplace20":[B
    fill-array-data v243, :array_50a2

    .line 692
    .restart local v243    # "byteReplace20":[B
    const/16 v9, 0x1a

    new-array v0, v9, [B

    move-object/16 v341, v0

    .end local v341    # "rep_mask20":[B
    move-object/from16 v0, v341

    fill-array-data v0, :array_50b4

    .line 695
    .restart local v341    # "rep_mask20":[B
    const/16 v9, 0x24

    new-array v0, v9, [B

    move-object/from16 v214, v0

    .end local v214    # "byteOrig21":[B
    fill-array-data v214, :array_50c6

    .line 696
    .restart local v214    # "byteOrig21":[B
    const/16 v9, 0x24

    new-array v0, v9, [B

    move-object/16 v296, v0

    .end local v296    # "mask21":[B
    move-object/from16 v0, v296

    fill-array-data v0, :array_50dc

    .line 697
    .restart local v296    # "mask21":[B
    const/16 v9, 0x24

    new-array v0, v9, [B

    move-object/from16 v244, v0

    .end local v244    # "byteReplace21":[B
    fill-array-data v244, :array_50f2

    .line 698
    .restart local v244    # "byteReplace21":[B
    const/16 v9, 0x24

    new-array v0, v9, [B

    move-object/16 v342, v0

    .end local v342    # "rep_mask21":[B
    move-object/from16 v0, v342

    fill-array-data v0, :array_5108

    .restart local v342    # "rep_mask21":[B
    move/from16 v105, v328

    .end local v328    # "pattern4":Z
    .restart local v105    # "pattern4":Z
    move/from16 v81, v327

    .end local v327    # "pattern3":Z
    .restart local v81    # "pattern3":Z
    move/from16 v33, v326

    .line 702
    .end local v3    # "original":Ljava/lang/String;
    .end local v4    # "replace":Ljava/lang/String;
    .end local v326    # "pattern2":Z
    .restart local v33    # "pattern2":Z
    :cond_dbe
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_192e

    .line 703
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restoreCore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_4981

    const/16 v184, 0x1

    const/16 v33, 0x1

    move/16 v326, v33

    .line 704
    .end local v33    # "pattern2":Z
    .restart local v326    # "pattern2":Z
    :goto_ddf
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restoreServices"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_4979

    const/16 v81, 0x1

    const/16 v105, 0x1

    move/16 v328, v105

    .end local v105    # "pattern4":Z
    .restart local v328    # "pattern4":Z
    move/16 v327, v81

    .line 707
    .end local v81    # "pattern3":Z
    .restart local v327    # "pattern3":Z
    :goto_df6
    const-string v3, "11 90 01 21 01 29 0F D1 01 26 28 1C C0 68 39 1C"

    .line 708
    .restart local v3    # "original":Ljava/lang/String;
    const-string v4, "?? ?? 11 99 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??"

    .line 709
    .restart local v4    # "replace":Ljava/lang/String;
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v5, v9, [B

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v6, v9, [B

    .line 710
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v7, v9, [B

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v8, v9, [B

    .line 711
    invoke-static/range {v3 .. v8}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 714
    const-string v3, "09 90 80 42 0C D1 D5 F8 DC 0B 39 1C D0 F8 24 E0 F0 47"

    .line 715
    const-string v4, "?? ?? B0 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??"

    .line 716
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v11, v9, [B

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v12, v9, [B

    .line 717
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v13, v9, [B

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v14, v9, [B

    move-object v9, v3

    move-object v10, v4

    .line 718
    invoke-static/range {v9 .. v14}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 722
    const-string v3, "39 1C 08 68 52 46 D0 F8 CC 01 D0 F8 ?? E0 F0 47 05 1C 01 3C 00 F0 04 80 01 20 06 B0 BD E8 E0 85 D9 F8 ?? E2 F0 47 F7 E7"

    .line 723
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 28 1C ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??"

    .line 724
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v17, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v18, v0

    .line 725
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v19, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v20, v0

    move-object v15, v3

    move-object/from16 v16, v4

    .line 726
    invoke-static/range {v15 .. v20}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 729
    const-string v3, "08 68 3A 1C D0 F8 D0 01 43 46 D0 F8 ?? E0 F0 47 05 1C 01 3C 00 F0 04 80 01 20 09 B0 BD E8 E0 8D D9 F8 ?? E2 F0 47 F7 E7"

    .line 730
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 28 1C ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??"

    .line 731
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v23, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v24, v0

    .line 732
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v25, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v26, v0

    move-object/from16 v21, v3

    move-object/from16 v22, v4

    .line 733
    invoke-static/range {v21 .. v26}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 736
    const-string v3, "56 45 00 F0 07 80 01 3C 00 F0 31 80 01 20"

    .line 737
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 05 98"

    .line 738
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v29, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v30, v0

    .line 739
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v31, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v32, v0

    move-object/from16 v27, v3

    move-object/from16 v28, v4

    .line 740
    invoke-static/range {v27 .. v32}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 743
    const-string v3, "56 45 00 00 01 20 09 b0 bd e8 e0 8d 00 27 00 25"

    .line 744
    const-string v4, "?? ?? 03 d0 00 20 ?? ?? ?? ?? ?? ?? ?? ?? ?? ??"

    .line 745
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v35, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v36, v0

    .line 746
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v37, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v38, v0

    move-object/from16 v33, v3

    move-object/from16 v34, v4

    .line 747
    invoke-static/range {v33 .. v38}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 752
    const-string v3, "89 44 24 40 8B 5C 24 40 83 FB 01 90 90 BE 01 00 00 00 8B C5 8B 40 0C 8B CF"

    .line 753
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 75 32 BE ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??"

    .line 754
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v41, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v42, v0

    .line 755
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v43, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v44, v0

    move-object/from16 v39, v3

    move-object/from16 v40, v4

    .line 756
    invoke-static/range {v39 .. v44}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 759
    const-string v3, "8B 54 24 38 8B CF 8B 01 8B 80 CC 01 00 00 FF 50 28 64 66 83 3D 00 00 00 00 00 8B E8 75 12 B0 01"

    .line 760
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 8B C5"

    .line 761
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v47, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v48, v0

    .line 762
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v49, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v50, v0

    move-object/from16 v45, v3

    move-object/from16 v46, v4

    .line 763
    invoke-static/range {v45 .. v50}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 766
    const-string v3, "8B 80 D0 01 00 00 8B D7 FF 50 28 64 66 83 3D 00 00 00 00 00 8B E8 75 12 B0 01 8B 6C 24 20"

    .line 767
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 8B C5 ?? ?? ?? ??"

    .line 768
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v53, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v54, v0

    .line 769
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v55, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v56, v0

    move-object/from16 v51, v3

    move-object/from16 v52, v4

    .line 770
    invoke-static/range {v51 .. v56}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 773
    const-string v3, "B3 01 89 54 24 10 8B 73 08 8B 44 24 38 8B 48 08 89 4C 24 18 3B F1 74 23 64 66 83 3D 00 00 00 00 00 0F 85 80 00 00 00"

    .line 774
    const-string v4, "33 D2 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??"

    .line 775
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v59, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v60, v0

    .line 776
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v61, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v62, v0

    move-object/from16 v57, v3

    move-object/from16 v58, v4

    .line 777
    invoke-static/range {v57 .. v62}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 781
    const-string v3, "E5 03 1A AA C0 03 3F D6 FB 03 00 2A 7F 07 00 71 35 00 80 52 35 00 80 52 E1 03 16 AA 2A 00 40 B9 E0 03 14 AA"

    .line 782
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 61 02 00 54 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??"

    .line 783
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v65, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v66, v0

    .line 784
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v67, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v68, v0

    move-object/from16 v63, v3

    move-object/from16 v64, v4

    .line 785
    invoke-static/range {v63 .. v68}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 788
    const-string v3, "F7 03 01 AA F9 03 02 AA 3A 00 80 52 F5 0A 40 B9 38 0B 40 B9 BF 02 18 6B"

    .line 789
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? FA 03 1F 2A ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??"

    .line 790
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v71, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v72, v0

    .line 791
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v73, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v74, v0

    move-object/from16 v69, v3

    move-object/from16 v70, v4

    .line 792
    invoke-static/range {v69 .. v74}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 798
    const-string v3, "CD F8 30 C0 78 B9 1A 99 31 B9 01 3C 00 F0 ?? 80 00 20"

    .line 799
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 0B 98"

    .line 800
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v77, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v78, v0

    .line 801
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v79, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v80, v0

    move-object/from16 v75, v3

    move-object/from16 v76, v4

    .line 802
    invoke-static/range {v75 .. v80}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 805
    const-string v3, "00 45 40 F0 ?? 80 00 45 40 F0 ?? 80 00 20 00 F0 02 B8"

    .line 806
    const-string v4, "00 29 ?? ?? ?? ?? 00 2A ?? ?? ?? ?? 01 ?? ?? ?? ?? ??"

    .line 807
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v83, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v84, v0

    .line 808
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v85, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v86, v0

    move-object/from16 v81, v3

    move-object/from16 v82, v4

    .line 809
    invoke-static/range {v81 .. v86}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 812
    const-string v3, "00 2D 40 F0 ?? 80 16 99 CD 69 DA F8 E0 20 0B 92 0B 9B 9B 42"

    .line 813
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 9D ??"

    .line 814
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v89, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v90, v0

    .line 815
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v91, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v92, v0

    move-object/from16 v87, v3

    move-object/from16 v88, v4

    .line 816
    invoke-static/range {v87 .. v92}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 819
    const-string v3, "45 F0 80 05 ?? B9 16 9A 51 46 30 1C 40 68"

    .line 820
    const-string v4, "05 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??"

    .line 821
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v125, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v126, v0

    .line 822
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v127, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v128, v0

    move-object/from16 v123, v3

    move-object/from16 v124, v4

    .line 823
    invoke-static/range {v123 .. v128}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 826
    const-string v3, "2D E9 E0 4D 8D B0 07 1C 00 90 15 91 16 92 17 93 16 9A 56 6A 31 1C 08 68 01 20 0D B0 BD E8 E0 8D F0 47"

    .line 827
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? D0 F8 BC 01 D0 F8 28 E0 ?? ??"

    .line 828
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v95, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v96, v0

    .line 829
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v97, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v98, v0

    move-object/from16 v93, v3

    move-object/from16 v94, v4

    .line 830
    invoke-static/range {v93 .. v98}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 833
    const-string v3, "1C BF D9 F8 B0 E1 F0 47 BF F3 5B 8F 31 1C 08 68 D0 F8 ?? 02 D0 F8 28 E0 00 20 14 90 14 ?? 00 ?? ?? D1"

    .line 834
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? F0 47 ?? ?? ?? ?? ?? ?? ?? ??"

    .line 835
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v101, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v102, v0

    .line 836
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v103, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v104, v0

    move-object/from16 v99, v3

    move-object/from16 v100, v4

    .line 837
    invoke-static/range {v99 .. v104}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 840
    const-string v3, "F0 47 82 46 BA F1 00 0F ?? E0 01 3C ?? F0 ?? ?? 28 ?? 00 2B"

    .line 841
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? ?? D1 ?? ?? ?? ?? ?? ?? ?? ?? ?? ??"

    .line 842
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v107, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v108, v0

    .line 843
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v109, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v110, v0

    move-object/from16 v105, v3

    move-object/from16 v106, v4

    .line 844
    invoke-static/range {v105 .. v110}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 847
    const-string v3, "F8 0C 00 D0 F8 ?? E0 F0 47 80 46 B8 F1 00 0F ?? E0 D9 F8 ?? ?? 39 1C"

    .line 848
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? D1 ?? ?? ?? ?? ?? ??"

    .line 849
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v113, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v114, v0

    .line 850
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v115, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v116, v0

    move-object/from16 v111, v3

    move-object/from16 v112, v4

    .line 851
    invoke-static/range {v111 .. v116}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 854
    const-string v3, "50 F8 0C 00 D0 F8 ?? E0 01 20 06 1C ?? BB D9 F8 24 E1 29 1C ?? ?? ?? ?? F0 47"

    .line 855
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? F0 47 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??"

    .line 856
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v119, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v120, v0

    .line 857
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v121, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v122, v0

    move-object/from16 v117, v3

    move-object/from16 v118, v4

    .line 858
    invoke-static/range {v117 .. v122}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 863
    const-string v3, "BA 00 00 00 00 89 54 24 28 BB FD FF FF FF 89 5C 24 30 8B 4C 24 54 33 C0 89 44 24 2C 85 C9 90 90 8B 54 24 58 85 D2 90 90"

    .line 864
    const-string v4, "?? 01 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 75 44 ?? ?? ?? ?? ?? ?? 75 23"

    .line 865
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v131, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v132, v0

    .line 866
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v133, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v134, v0

    move-object/from16 v129, v3

    move-object/from16 v130, v4

    .line 867
    invoke-static/range {v129 .. v134}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 870
    const-string v3, "85 ED 0F 85 ?? ?? 00 00 8B 54 24 58 8B 6A 1C 8B 5C 24 24 8B 83 ?? 00 00 00 89 44 24 38 8B 4C 24 38 3B ED"

    .line 871
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? E9"

    .line 872
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v137, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v138, v0

    .line 873
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v139, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v140, v0

    move-object/from16 v135, v3

    move-object/from16 v136, v4

    .line 874
    invoke-static/range {v135 .. v140}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 878
    const-string v3, "FD 03 1F 2A 42 00 80 12 E2 3B 00 B9 E3 03 1F 2A E3 37 00 B9 FD 03 1F 2A FD 03 1F 2A"

    .line 879
    const-string v4, "3D 00 80 52 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 3B 02 00 B5 9C 01 00 B5"

    .line 880
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v143, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v144, v0

    .line 881
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v145, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v146, v0

    move-object/from16 v141, v3

    move-object/from16 v142, v4

    .line 882
    invoke-static/range {v141 .. v146}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 885
    const-string v3, "FC 03 1F 2A 42 00 80 12 E2 3B 00 B9 E3 03 1F 2A E3 37 00 B9 FC 03 1F 2A FC 03 1F 2A"

    .line 886
    const-string v4, "3C 00 80 52 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 3A 02 00 B5 9B 01 00 B5"

    .line 887
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v149, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v150, v0

    .line 888
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v151, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v152, v0

    move-object/from16 v147, v3

    move-object/from16 v148, v4

    .line 889
    invoke-static/range {v147 .. v152}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 892
    const-string v3, "E2 33 00 B9 E3 03 14 2A 9F 02 03 6B 6A 13 00 54 A0 16 40 B9"

    .line 893
    const-string v4, "?? ?? ?? ?? E3 33 40 B9 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??"

    .line 894
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v155, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v156, v0

    .line 895
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v157, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v158, v0

    move-object/from16 v153, v3

    move-object/from16 v154, v4

    .line 896
    invoke-static/range {v153 .. v158}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 899
    const-string v3, "D6 02 19 32 56 01 00 35 E1 03 18 AA E0 03 15 AA 00 0C 40 B9 E2 03 1B AA"

    .line 900
    const-string v4, "D6 02 19 12 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??"

    .line 901
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v161, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v162, v0

    .line 902
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v163, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v164, v0

    move-object/from16 v159, v3

    move-object/from16 v160, v4

    .line 903
    invoke-static/range {v159 .. v164}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 907
    const/16 v9, 0x23

    new-array v0, v9, [B

    move-object/from16 v212, v0

    .end local v212    # "byteOrig2":[B
    fill-array-data v212, :array_511e

    .line 908
    .restart local v212    # "byteOrig2":[B
    const/16 v9, 0x23

    new-array v0, v9, [B

    move-object/16 v294, v0

    .end local v294    # "mask2":[B
    move-object/from16 v0, v294

    fill-array-data v0, :array_5134

    .line 909
    .restart local v294    # "mask2":[B
    const/16 v9, 0x23

    new-array v0, v9, [B

    move-object/from16 v242, v0

    .end local v242    # "byteReplace2":[B
    fill-array-data v242, :array_514a

    .line 910
    .restart local v242    # "byteReplace2":[B
    const/16 v9, 0x23

    new-array v0, v9, [B

    move-object/16 v340, v0

    .end local v340    # "rep_mask2":[B
    move-object/from16 v0, v340

    fill-array-data v0, :array_5160

    .line 912
    .restart local v340    # "rep_mask2":[B
    const/16 v9, 0x24

    new-array v0, v9, [B

    move-object/from16 v215, v0

    .end local v215    # "byteOrig3":[B
    fill-array-data v215, :array_5176

    .line 913
    .restart local v215    # "byteOrig3":[B
    const/16 v9, 0x24

    new-array v0, v9, [B

    move-object/from16 v245, v0

    .end local v245    # "byteReplace3":[B
    fill-array-data v245, :array_518c

    .line 915
    .restart local v245    # "byteReplace3":[B
    const/16 v9, 0x1e

    new-array v0, v9, [B

    move-object/from16 v216, v0

    .end local v216    # "byteOrig4":[B
    fill-array-data v216, :array_51a2

    .line 916
    .restart local v216    # "byteOrig4":[B
    const/16 v9, 0x1e

    new-array v0, v9, [B

    move-object/16 v297, v0

    .end local v297    # "mask4":[B
    move-object/from16 v0, v297

    fill-array-data v0, :array_51b6

    .line 917
    .restart local v297    # "mask4":[B
    const/16 v9, 0x1e

    new-array v0, v9, [B

    move-object/from16 v246, v0

    .end local v246    # "byteReplace4":[B
    fill-array-data v246, :array_51ca

    .line 918
    .restart local v246    # "byteReplace4":[B
    const/16 v9, 0x1e

    new-array v0, v9, [B

    move-object/16 v343, v0

    .end local v343    # "rep_mask4":[B
    move-object/from16 v0, v343

    fill-array-data v0, :array_51de

    .line 920
    .restart local v343    # "rep_mask4":[B
    const/16 v9, 0x1c

    new-array v0, v9, [B

    move-object/from16 v217, v0

    .end local v217    # "byteOrig5":[B
    fill-array-data v217, :array_51f2

    .line 921
    .restart local v217    # "byteOrig5":[B
    const/16 v9, 0x1c

    new-array v0, v9, [B

    move-object/16 v298, v0

    .end local v298    # "mask5":[B
    move-object/from16 v0, v298

    fill-array-data v0, :array_5204

    .line 922
    .restart local v298    # "mask5":[B
    const/16 v9, 0x1c

    new-array v0, v9, [B

    move-object/from16 v247, v0

    .end local v247    # "byteReplace5":[B
    fill-array-data v247, :array_5216

    .line 923
    .restart local v247    # "byteReplace5":[B
    const/16 v9, 0x1c

    new-array v0, v9, [B

    move-object/16 v344, v0

    .end local v344    # "rep_mask5":[B
    move-object/from16 v0, v344

    fill-array-data v0, :array_5228

    .line 925
    .restart local v344    # "rep_mask5":[B
    const/16 v9, 0x2b

    new-array v0, v9, [B

    move-object/from16 v218, v0

    .end local v218    # "byteOrig6":[B
    fill-array-data v218, :array_523a

    .line 926
    .restart local v218    # "byteOrig6":[B
    const/16 v9, 0x2b

    new-array v0, v9, [B

    move-object/16 v299, v0

    .end local v299    # "mask6":[B
    move-object/from16 v0, v299

    fill-array-data v0, :array_5254

    .line 927
    .restart local v299    # "mask6":[B
    const/16 v9, 0x2b

    new-array v0, v9, [B

    move-object/from16 v248, v0

    .end local v248    # "byteReplace6":[B
    fill-array-data v248, :array_526e

    .line 928
    .restart local v248    # "byteReplace6":[B
    const/16 v9, 0x2b

    new-array v0, v9, [B

    move-object/16 v345, v0

    .end local v345    # "rep_mask6":[B
    move-object/from16 v0, v345

    fill-array-data v0, :array_5288

    .line 930
    .restart local v345    # "rep_mask6":[B
    const/16 v9, 0x1e

    new-array v0, v9, [B

    move-object/from16 v219, v0

    .end local v219    # "byteOrig7":[B
    fill-array-data v219, :array_52a2

    .line 931
    .restart local v219    # "byteOrig7":[B
    const/16 v9, 0x1e

    new-array v0, v9, [B

    move-object/16 v300, v0

    .end local v300    # "mask7":[B
    move-object/from16 v0, v300

    fill-array-data v0, :array_52b6

    .line 932
    .restart local v300    # "mask7":[B
    const/16 v9, 0x1e

    new-array v0, v9, [B

    move-object/from16 v249, v0

    .end local v249    # "byteReplace7":[B
    fill-array-data v249, :array_52ca

    .line 933
    .restart local v249    # "byteReplace7":[B
    const/16 v9, 0x1e

    new-array v0, v9, [B

    move-object/16 v346, v0

    .end local v346    # "rep_mask7":[B
    move-object/from16 v0, v346

    fill-array-data v0, :array_52de

    .line 935
    .restart local v346    # "rep_mask7":[B
    const/16 v9, 0x1e

    new-array v0, v9, [B

    move-object/from16 v220, v0

    .end local v220    # "byteOrig8":[B
    fill-array-data v220, :array_52f2

    .line 936
    .restart local v220    # "byteOrig8":[B
    const/16 v9, 0x1e

    new-array v0, v9, [B

    move-object/16 v301, v0

    .end local v301    # "mask8":[B
    move-object/from16 v0, v301

    fill-array-data v0, :array_5306

    .line 937
    .restart local v301    # "mask8":[B
    const/16 v9, 0x1e

    new-array v0, v9, [B

    move-object/from16 v250, v0

    .end local v250    # "byteReplace8":[B
    fill-array-data v250, :array_531a

    .line 938
    .restart local v250    # "byteReplace8":[B
    const/16 v9, 0x1e

    new-array v0, v9, [B

    move-object/16 v347, v0

    .end local v347    # "rep_mask8":[B
    move-object/from16 v0, v347

    fill-array-data v0, :array_532e

    .line 940
    .restart local v347    # "rep_mask8":[B
    const/16 v9, 0x26

    new-array v0, v9, [B

    move-object/from16 v221, v0

    .end local v221    # "byteOrig9":[B
    fill-array-data v221, :array_5342

    .line 941
    .restart local v221    # "byteOrig9":[B
    const/16 v9, 0x26

    new-array v0, v9, [B

    move-object/16 v302, v0

    .end local v302    # "mask9":[B
    move-object/from16 v0, v302

    fill-array-data v0, :array_535a

    .line 942
    .restart local v302    # "mask9":[B
    const/16 v9, 0x26

    new-array v0, v9, [B

    move-object/from16 v251, v0

    .end local v251    # "byteReplace9":[B
    fill-array-data v251, :array_5372

    .line 943
    .restart local v251    # "byteReplace9":[B
    const/16 v9, 0x26

    new-array v0, v9, [B

    move-object/16 v348, v0

    .end local v348    # "rep_mask9":[B
    move-object/from16 v0, v348

    fill-array-data v0, :array_538a

    .line 948
    .restart local v348    # "rep_mask9":[B
    const/16 v9, 0x10

    new-array v0, v9, [B

    move-object/from16 v225, v0

    .end local v225    # "byteOrigS2":[B
    fill-array-data v225, :array_53a2

    .line 949
    .restart local v225    # "byteOrigS2":[B
    const/16 v9, 0x10

    new-array v0, v9, [B

    move-object/16 v306, v0

    .end local v306    # "maskS2":[B
    move-object/from16 v0, v306

    fill-array-data v0, :array_53ae

    .line 950
    .restart local v306    # "maskS2":[B
    const/16 v9, 0x10

    new-array v0, v9, [B

    move-object/from16 v255, v0

    .end local v255    # "byteReplaceS2":[B
    fill-array-data v255, :array_53ba

    .line 951
    .restart local v255    # "byteReplaceS2":[B
    const/16 v9, 0x10

    new-array v0, v9, [B

    move-object/16 v352, v0

    .end local v352    # "rep_maskS2":[B
    move-object/from16 v0, v352

    fill-array-data v0, :array_53c6

    .line 954
    .restart local v352    # "rep_maskS2":[B
    const/16 v9, 0x18

    new-array v0, v9, [B

    move-object/from16 v226, v0

    .end local v226    # "byteOrigS3":[B
    fill-array-data v226, :array_53d2

    .line 955
    .restart local v226    # "byteOrigS3":[B
    const/16 v9, 0x18

    new-array v0, v9, [B

    move-object/16 v307, v0

    .end local v307    # "maskS3":[B
    move-object/from16 v0, v307

    fill-array-data v0, :array_53e2

    .line 956
    .restart local v307    # "maskS3":[B
    const/16 v9, 0x18

    new-array v0, v9, [B

    move-object/16 v256, v0

    .end local v256    # "byteReplaceS3":[B
    move-object/from16 v0, v256

    fill-array-data v0, :array_53f2

    .line 957
    .restart local v256    # "byteReplaceS3":[B
    const/16 v9, 0x18

    new-array v0, v9, [B

    move-object/16 v353, v0

    .end local v353    # "rep_maskS3":[B
    move-object/from16 v0, v353

    fill-array-data v0, :array_5402

    .line 960
    .restart local v353    # "rep_maskS3":[B
    const/16 v9, 0x13

    new-array v0, v9, [B

    move-object/from16 v227, v0

    .end local v227    # "byteOrigS4":[B
    fill-array-data v227, :array_5412

    .line 961
    .restart local v227    # "byteOrigS4":[B
    const/16 v9, 0x13

    new-array v0, v9, [B

    move-object/16 v308, v0

    .end local v308    # "maskS4":[B
    move-object/from16 v0, v308

    fill-array-data v0, :array_5420

    .line 962
    .restart local v308    # "maskS4":[B
    const/16 v9, 0x13

    new-array v0, v9, [B

    move-object/16 v257, v0

    .end local v257    # "byteReplaceS4":[B
    move-object/from16 v0, v257

    fill-array-data v0, :array_542e

    .line 963
    .restart local v257    # "byteReplaceS4":[B
    const/16 v9, 0x13

    new-array v0, v9, [B

    move-object/16 v354, v0

    .end local v354    # "rep_maskS4":[B
    move-object/from16 v0, v354

    fill-array-data v0, :array_543c

    .line 966
    .restart local v354    # "rep_maskS4":[B
    const/16 v9, 0xe

    new-array v0, v9, [B

    move-object/from16 v228, v0

    .end local v228    # "byteOrigS5":[B
    fill-array-data v228, :array_544a

    .line 967
    .restart local v228    # "byteOrigS5":[B
    const/16 v9, 0xe

    new-array v0, v9, [B

    move-object/16 v309, v0

    .end local v309    # "maskS5":[B
    move-object/from16 v0, v309

    fill-array-data v0, :array_5456

    .line 968
    .restart local v309    # "maskS5":[B
    const/16 v9, 0xe

    new-array v0, v9, [B

    move-object/16 v258, v0

    .end local v258    # "byteReplaceS5":[B
    move-object/from16 v0, v258

    fill-array-data v0, :array_5462

    .line 969
    .restart local v258    # "byteReplaceS5":[B
    const/16 v9, 0xe

    new-array v0, v9, [B

    move-object/16 v355, v0

    .end local v355    # "rep_maskS5":[B
    move-object/from16 v0, v355

    fill-array-data v0, :array_546e

    .line 972
    .restart local v355    # "rep_maskS5":[B
    const/16 v9, 0x14

    new-array v0, v9, [B

    move-object/from16 v229, v0

    .end local v229    # "byteOrigS6":[B
    fill-array-data v229, :array_547a

    .line 973
    .restart local v229    # "byteOrigS6":[B
    const/16 v9, 0x14

    new-array v0, v9, [B

    move-object/16 v310, v0

    .end local v310    # "maskS6":[B
    move-object/from16 v0, v310

    fill-array-data v0, :array_5488

    .line 974
    .restart local v310    # "maskS6":[B
    const/16 v9, 0x14

    new-array v0, v9, [B

    move-object/16 v259, v0

    .end local v259    # "byteReplaceS6":[B
    move-object/from16 v0, v259

    fill-array-data v0, :array_5496

    .line 975
    .restart local v259    # "byteReplaceS6":[B
    const/16 v9, 0x14

    new-array v0, v9, [B

    move-object/16 v356, v0

    .end local v356    # "rep_maskS6":[B
    move-object/from16 v0, v356

    fill-array-data v0, :array_54a4

    .line 978
    .restart local v356    # "rep_maskS6":[B
    const/16 v9, 0xe

    new-array v0, v9, [B

    move-object/from16 v230, v0

    .end local v230    # "byteOrigS7":[B
    fill-array-data v230, :array_54b2

    .line 979
    .restart local v230    # "byteOrigS7":[B
    const/16 v9, 0xe

    new-array v0, v9, [B

    move-object/16 v311, v0

    .end local v311    # "maskS7":[B
    move-object/from16 v0, v311

    fill-array-data v0, :array_54be

    .line 980
    .restart local v311    # "maskS7":[B
    const/16 v9, 0xe

    new-array v0, v9, [B

    move-object/16 v260, v0

    .end local v260    # "byteReplaceS7":[B
    move-object/from16 v0, v260

    fill-array-data v0, :array_54ca

    .line 981
    .restart local v260    # "byteReplaceS7":[B
    const/16 v9, 0xe

    new-array v0, v9, [B

    move-object/16 v357, v0

    .end local v357    # "rep_maskS7":[B
    move-object/from16 v0, v357

    fill-array-data v0, :array_54d6

    .line 984
    .restart local v357    # "rep_maskS7":[B
    const/16 v9, 0x1c

    new-array v0, v9, [B

    move-object/from16 v231, v0

    .end local v231    # "byteOrigS8":[B
    fill-array-data v231, :array_54e2

    .line 985
    .restart local v231    # "byteOrigS8":[B
    const/16 v9, 0x1c

    new-array v0, v9, [B

    move-object/16 v312, v0

    .end local v312    # "maskS8":[B
    move-object/from16 v0, v312

    fill-array-data v0, :array_54f4

    .line 986
    .restart local v312    # "maskS8":[B
    const/16 v9, 0x1c

    new-array v0, v9, [B

    move-object/16 v261, v0

    .end local v261    # "byteReplaceS8":[B
    move-object/from16 v0, v261

    fill-array-data v0, :array_5506

    .line 987
    .restart local v261    # "byteReplaceS8":[B
    const/16 v9, 0x1c

    new-array v0, v9, [B

    move-object/16 v358, v0

    .end local v358    # "rep_maskS8":[B
    move-object/from16 v0, v358

    fill-array-data v0, :array_5518

    .line 988
    .restart local v358    # "rep_maskS8":[B
    const/16 v192, 0x0

    .line 991
    const/16 v9, 0x26

    new-array v0, v9, [B

    move-object/from16 v195, v0

    .end local v195    # "byteOrigS9":[B
    fill-array-data v195, :array_552a

    .line 992
    .restart local v195    # "byteOrigS9":[B
    const/16 v9, 0x26

    new-array v0, v9, [B

    move-object/from16 v196, v0

    .end local v196    # "maskS9":[B
    fill-array-data v196, :array_5542

    .line 993
    .restart local v196    # "maskS9":[B
    const/16 v9, 0x26

    new-array v0, v9, [B

    move-object/from16 v197, v0

    .end local v197    # "byteReplaceS9":[B
    fill-array-data v197, :array_555a

    .line 994
    .restart local v197    # "byteReplaceS9":[B
    const/16 v9, 0x26

    new-array v0, v9, [B

    move-object/from16 v198, v0

    .end local v198    # "rep_maskS9":[B
    fill-array-data v198, :array_5572

    .line 995
    .restart local v198    # "rep_maskS9":[B
    const/16 v200, 0x0

    .line 998
    const/16 v9, 0x1d

    new-array v0, v9, [B

    move-object/from16 v222, v0

    .end local v222    # "byteOrigS10":[B
    fill-array-data v222, :array_558a

    .line 999
    .restart local v222    # "byteOrigS10":[B
    const/16 v9, 0x1d

    new-array v0, v9, [B

    move-object/16 v303, v0

    .end local v303    # "maskS10":[B
    move-object/from16 v0, v303

    fill-array-data v0, :array_559e

    .line 1000
    .restart local v303    # "maskS10":[B
    const/16 v9, 0x1d

    new-array v0, v9, [B

    move-object/from16 v252, v0

    .end local v252    # "byteReplaceS10":[B
    fill-array-data v252, :array_55b2

    .line 1001
    .restart local v252    # "byteReplaceS10":[B
    const/16 v9, 0x1d

    new-array v0, v9, [B

    move-object/16 v349, v0

    .end local v349    # "rep_maskS10":[B
    move-object/from16 v0, v349

    fill-array-data v0, :array_55c6

    .line 1004
    .restart local v349    # "rep_maskS10":[B
    const/16 v9, 0x1d

    new-array v0, v9, [B

    move-object/from16 v223, v0

    .end local v223    # "byteOrigS11":[B
    fill-array-data v223, :array_55da

    .line 1005
    .restart local v223    # "byteOrigS11":[B
    const/16 v9, 0x1d

    new-array v0, v9, [B

    move-object/16 v304, v0

    .end local v304    # "maskS11":[B
    move-object/from16 v0, v304

    fill-array-data v0, :array_55ee

    .line 1006
    .restart local v304    # "maskS11":[B
    const/16 v9, 0x1d

    new-array v0, v9, [B

    move-object/from16 v253, v0

    .end local v253    # "byteReplaceS11":[B
    fill-array-data v253, :array_5602

    .line 1007
    .restart local v253    # "byteReplaceS11":[B
    const/16 v9, 0x1d

    new-array v0, v9, [B

    move-object/16 v350, v0

    .end local v350    # "rep_maskS11":[B
    move-object/from16 v0, v350

    fill-array-data v0, :array_5616

    .line 1010
    .restart local v350    # "rep_maskS11":[B
    const/16 v9, 0x19

    new-array v0, v9, [B

    move-object/from16 v224, v0

    .end local v224    # "byteOrigS12":[B
    fill-array-data v224, :array_562a

    .line 1011
    .restart local v224    # "byteOrigS12":[B
    const/16 v9, 0x19

    new-array v0, v9, [B

    move-object/16 v305, v0

    .end local v305    # "maskS12":[B
    move-object/from16 v0, v305

    fill-array-data v0, :array_563c

    .line 1012
    .restart local v305    # "maskS12":[B
    const/16 v9, 0x19

    new-array v0, v9, [B

    move-object/from16 v254, v0

    .end local v254    # "byteReplaceS12":[B
    fill-array-data v254, :array_564e

    .line 1013
    .restart local v254    # "byteReplaceS12":[B
    const/16 v9, 0x19

    new-array v0, v9, [B

    move-object/16 v351, v0

    .end local v351    # "rep_maskS12":[B
    move-object/from16 v0, v351

    fill-array-data v0, :array_5660

    .line 1016
    .restart local v351    # "rep_maskS12":[B
    const-string v3, "D6 ?? 80 00 39 ?? ?? ?? 71 20 ?? ?? ?? ?? 54 ?? ?? ?? 52 ?? ?? ?? DD ?? ?? 01 38 ?? ?? 00"

    .line 1017
    const-string v4, "D5 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??"

    .line 1018
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v167, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v168, v0

    .line 1019
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v169, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v170, v0

    move-object/from16 v165, v3

    move-object/from16 v166, v4

    .line 1020
    invoke-static/range {v165 .. v170}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 1023
    const-string v3, "39 ?? 07 00 39 ?? 03 00 12 03 12 03 0F 03"

    .line 1024
    const-string v4, "?? ?? ?? ?? ?? ?? ?? ?? 0F 03 12 F3 28 FE"

    .line 1025
    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v173, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v174, v0

    .line 1026
    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v175, v0

    const-string v9, "[ \t]+"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    new-array v0, v9, [B

    move-object/from16 v176, v0

    move-object/from16 v171, v3

    move-object/from16 v172, v4

    .line 1027
    invoke-static/range {v171 .. v176}, Lcom/chelpus/Utils;->convertStringToArraysPatch(Ljava/lang/String;Ljava/lang/String;[B[B[B[B)V

    .line 1031
    const/16 v9, 0x23

    new-array v0, v9, [B

    move-object/from16 v202, v0

    .end local v202    # "byteOrig10":[B
    fill-array-data v202, :array_5672

    .line 1032
    .restart local v202    # "byteOrig10":[B
    const/16 v9, 0x23

    new-array v0, v9, [B

    move-object/16 v284, v0

    .end local v284    # "mask10":[B
    move-object/from16 v0, v284

    fill-array-data v0, :array_5688

    .line 1033
    .restart local v284    # "mask10":[B
    const/16 v9, 0x23

    new-array v0, v9, [B

    move-object/from16 v232, v0

    .end local v232    # "byteReplace10":[B
    fill-array-data v232, :array_569e

    .line 1034
    .restart local v232    # "byteReplace10":[B
    const/16 v9, 0x23

    new-array v0, v9, [B

    move-object/16 v330, v0

    .end local v330    # "rep_mask10":[B
    move-object/from16 v0, v330

    fill-array-data v0, :array_56b4

    .line 1037
    .restart local v330    # "rep_mask10":[B
    const/16 v9, 0x24

    new-array v0, v9, [B

    move-object/from16 v203, v0

    .end local v203    # "byteOrig11":[B
    fill-array-data v203, :array_56ca

    .line 1038
    .restart local v203    # "byteOrig11":[B
    const/16 v9, 0x24

    new-array v0, v9, [B

    move-object/16 v285, v0

    .end local v285    # "mask11":[B
    move-object/from16 v0, v285

    fill-array-data v0, :array_56e0

    .line 1039
    .restart local v285    # "mask11":[B
    const/16 v9, 0x24

    new-array v0, v9, [B

    move-object/from16 v233, v0

    .end local v233    # "byteReplace11":[B
    fill-array-data v233, :array_56f6

    .line 1040
    .restart local v233    # "byteReplace11":[B
    const/16 v9, 0x24

    new-array v0, v9, [B

    move-object/16 v331, v0

    .end local v331    # "rep_mask11":[B
    move-object/from16 v0, v331

    fill-array-data v0, :array_570c

    .line 1042
    .restart local v331    # "rep_mask11":[B
    const/16 v9, 0x1e

    new-array v0, v9, [B

    move-object/from16 v204, v0

    .end local v204    # "byteOrig12":[B
    fill-array-data v204, :array_5722

    .line 1043
    .restart local v204    # "byteOrig12":[B
    const/16 v9, 0x1e

    new-array v0, v9, [B

    move-object/16 v286, v0

    .end local v286    # "mask12":[B
    move-object/from16 v0, v286

    fill-array-data v0, :array_5736

    .line 1044
    .restart local v286    # "mask12":[B
    const/16 v9, 0x1e

    new-array v0, v9, [B

    move-object/from16 v234, v0

    .end local v234    # "byteReplace12":[B
    fill-array-data v234, :array_574a

    .line 1045
    .restart local v234    # "byteReplace12":[B
    const/16 v9, 0x1e

    new-array v0, v9, [B

    move-object/16 v332, v0

    .end local v332    # "rep_mask12":[B
    move-object/from16 v0, v332

    fill-array-data v0, :array_575e

    .line 1048
    .restart local v332    # "rep_mask12":[B
    const/16 v9, 0x1c

    new-array v0, v9, [B

    move-object/from16 v205, v0

    .end local v205    # "byteOrig13":[B
    fill-array-data v205, :array_5772

    .line 1049
    .restart local v205    # "byteOrig13":[B
    const/16 v9, 0x1c

    new-array v0, v9, [B

    move-object/16 v287, v0

    .end local v287    # "mask13":[B
    move-object/from16 v0, v287

    fill-array-data v0, :array_5784

    .line 1050
    .restart local v287    # "mask13":[B
    const/16 v9, 0x1c

    new-array v0, v9, [B

    move-object/from16 v235, v0

    .end local v235    # "byteReplace13":[B
    fill-array-data v235, :array_5796

    .line 1051
    .restart local v235    # "byteReplace13":[B
    const/16 v9, 0x1c

    new-array v0, v9, [B

    move-object/16 v333, v0

    .end local v333    # "rep_mask13":[B
    move-object/from16 v0, v333

    fill-array-data v0, :array_57a8

    .line 1054
    .restart local v333    # "rep_mask13":[B
    const/16 v9, 0x2b

    new-array v0, v9, [B

    move-object/from16 v206, v0

    .end local v206    # "byteOrig14":[B
    fill-array-data v206, :array_57ba

    .line 1055
    .restart local v206    # "byteOrig14":[B
    const/16 v9, 0x2b

    new-array v0, v9, [B

    move-object/16 v288, v0

    .end local v288    # "mask14":[B
    move-object/from16 v0, v288

    fill-array-data v0, :array_57d4

    .line 1056
    .restart local v288    # "mask14":[B
    const/16 v9, 0x2b

    new-array v0, v9, [B

    move-object/from16 v236, v0

    .end local v236    # "byteReplace14":[B
    fill-array-data v236, :array_57ee

    .line 1057
    .restart local v236    # "byteReplace14":[B
    const/16 v9, 0x2b

    new-array v0, v9, [B

    move-object/16 v334, v0

    .end local v334    # "rep_mask14":[B
    move-object/from16 v0, v334

    fill-array-data v0, :array_5808

    .line 1060
    .restart local v334    # "rep_mask14":[B
    const/16 v9, 0x2c

    new-array v0, v9, [B

    move-object/from16 v207, v0

    .end local v207    # "byteOrig15":[B
    fill-array-data v207, :array_5822

    .line 1061
    .restart local v207    # "byteOrig15":[B
    const/16 v9, 0x2c

    new-array v0, v9, [B

    move-object/16 v289, v0

    .end local v289    # "mask15":[B
    move-object/from16 v0, v289

    fill-array-data v0, :array_583c

    .line 1062
    .restart local v289    # "mask15":[B
    const/16 v9, 0x2c

    new-array v0, v9, [B

    move-object/from16 v237, v0

    .end local v237    # "byteReplace15":[B
    fill-array-data v237, :array_5856

    .line 1063
    .restart local v237    # "byteReplace15":[B
    const/16 v9, 0x2c

    new-array v0, v9, [B

    move-object/16 v335, v0

    .end local v335    # "rep_mask15":[B
    move-object/from16 v0, v335

    fill-array-data v0, :array_5870

    .line 1066
    .restart local v335    # "rep_mask15":[B
    const/16 v9, 0x46

    new-array v0, v9, [B

    move-object/from16 v208, v0

    .end local v208    # "byteOrig16":[B
    fill-array-data v208, :array_588a

    .line 1067
    .restart local v208    # "byteOrig16":[B
    const/16 v9, 0x46

    new-array v0, v9, [B

    move-object/16 v290, v0

    .end local v290    # "mask16":[B
    move-object/from16 v0, v290

    fill-array-data v0, :array_58b2

    .line 1068
    .restart local v290    # "mask16":[B
    const/16 v9, 0x46

    new-array v0, v9, [B

    move-object/from16 v238, v0

    .end local v238    # "byteReplace16":[B
    fill-array-data v238, :array_58da

    .line 1069
    .restart local v238    # "byteReplace16":[B
    const/16 v9, 0x46

    new-array v0, v9, [B

    move-object/16 v336, v0

    .end local v336    # "rep_mask16":[B
    move-object/from16 v0, v336

    fill-array-data v0, :array_5902

    .line 1072
    .restart local v336    # "rep_mask16":[B
    const/16 v9, 0x38

    new-array v0, v9, [B

    move-object/from16 v209, v0

    .end local v209    # "byteOrig17":[B
    fill-array-data v209, :array_592a

    .line 1073
    .restart local v209    # "byteOrig17":[B
    const/16 v9, 0x38

    new-array v0, v9, [B

    move-object/16 v291, v0

    .end local v291    # "mask17":[B
    move-object/from16 v0, v291

    fill-array-data v0, :array_594a

    .line 1074
    .restart local v291    # "mask17":[B
    const/16 v9, 0x38

    new-array v0, v9, [B

    move-object/from16 v239, v0

    .end local v239    # "byteReplace17":[B
    fill-array-data v239, :array_596a

    .line 1075
    .restart local v239    # "byteReplace17":[B
    const/16 v9, 0x38

    new-array v0, v9, [B

    move-object/16 v337, v0

    .end local v337    # "rep_mask17":[B
    move-object/from16 v0, v337

    fill-array-data v0, :array_598a

    .line 1078
    .restart local v337    # "rep_mask17":[B
    const/16 v9, 0x29

    new-array v0, v9, [B

    move-object/from16 v210, v0

    .end local v210    # "byteOrig18":[B
    fill-array-data v210, :array_59aa

    .line 1079
    .restart local v210    # "byteOrig18":[B
    const/16 v9, 0x29

    new-array v0, v9, [B

    move-object/16 v292, v0

    .end local v292    # "mask18":[B
    move-object/from16 v0, v292

    fill-array-data v0, :array_59c4

    .line 1080
    .restart local v292    # "mask18":[B
    const/16 v9, 0x29

    new-array v0, v9, [B

    move-object/from16 v240, v0

    .end local v240    # "byteReplace18":[B
    fill-array-data v240, :array_59de

    .line 1081
    .restart local v240    # "byteReplace18":[B
    const/16 v9, 0x29

    new-array v0, v9, [B

    move-object/16 v338, v0

    .end local v338    # "rep_mask18":[B
    move-object/from16 v0, v338

    fill-array-data v0, :array_59f8

    .line 1084
    .restart local v338    # "rep_mask18":[B
    const/16 v9, 0x32

    new-array v0, v9, [B

    move-object/from16 v211, v0

    .end local v211    # "byteOrig19":[B
    fill-array-data v211, :array_5a12

    .line 1085
    .restart local v211    # "byteOrig19":[B
    const/16 v9, 0x32

    new-array v0, v9, [B

    move-object/16 v293, v0

    .end local v293    # "mask19":[B
    move-object/from16 v0, v293

    fill-array-data v0, :array_5a30

    .line 1086
    .restart local v293    # "mask19":[B
    const/16 v9, 0x32

    new-array v0, v9, [B

    move-object/from16 v241, v0

    .end local v241    # "byteReplace19":[B
    fill-array-data v241, :array_5a4e

    .line 1087
    .restart local v241    # "byteReplace19":[B
    const/16 v9, 0x32

    new-array v0, v9, [B

    move-object/16 v339, v0

    .end local v339    # "rep_mask19":[B
    move-object/from16 v0, v339

    fill-array-data v0, :array_5a6c

    .line 1090
    .restart local v339    # "rep_mask19":[B
    const/16 v9, 0x1a

    new-array v0, v9, [B

    move-object/from16 v213, v0

    .end local v213    # "byteOrig20":[B
    fill-array-data v213, :array_5a8a

    .line 1091
    .restart local v213    # "byteOrig20":[B
    const/16 v9, 0x1a

    new-array v0, v9, [B

    move-object/16 v295, v0

    .end local v295    # "mask20":[B
    move-object/from16 v0, v295

    fill-array-data v0, :array_5a9c

    .line 1092
    .restart local v295    # "mask20":[B
    const/16 v9, 0x1a

    new-array v0, v9, [B

    move-object/from16 v243, v0

    .end local v243    # "byteReplace20":[B
    fill-array-data v243, :array_5aae

    .line 1093
    .restart local v243    # "byteReplace20":[B
    const/16 v9, 0x1a

    new-array v0, v9, [B

    move-object/16 v341, v0

    .end local v341    # "rep_mask20":[B
    move-object/from16 v0, v341

    fill-array-data v0, :array_5ac0

    .line 1096
    .restart local v341    # "rep_mask20":[B
    const/16 v9, 0x24

    new-array v0, v9, [B

    move-object/from16 v214, v0

    .end local v214    # "byteOrig21":[B
    fill-array-data v214, :array_5ad2

    .line 1097
    .restart local v214    # "byteOrig21":[B
    const/16 v9, 0x24

    new-array v0, v9, [B

    move-object/16 v296, v0

    .end local v296    # "mask21":[B
    move-object/from16 v0, v296

    fill-array-data v0, :array_5ae8

    .line 1098
    .restart local v296    # "mask21":[B
    const/16 v9, 0x24

    new-array v0, v9, [B

    move-object/from16 v244, v0

    .end local v244    # "byteReplace21":[B
    fill-array-data v244, :array_5afe

    .line 1099
    .restart local v244    # "byteReplace21":[B
    const/16 v9, 0x24

    new-array v0, v9, [B

    move-object/16 v342, v0

    .end local v342    # "rep_mask21":[B
    move-object/from16 v0, v342

    fill-array-data v0, :array_5b14

    .restart local v342    # "rep_mask21":[B
    move/from16 v105, v328

    .end local v328    # "pattern4":Z
    .restart local v105    # "pattern4":Z
    move/from16 v81, v327

    .end local v327    # "pattern3":Z
    .restart local v81    # "pattern3":Z
    move/from16 v33, v326

    .line 1104
    .end local v3    # "original":Ljava/lang/String;
    .end local v4    # "replace":Ljava/lang/String;
    .end local v326    # "pattern2":Z
    .restart local v33    # "pattern2":Z
    :cond_192e
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_1955

    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_1955

    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "all"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_3c6c

    .line 1105
    :cond_1955
    new-instance v0, Ljava/util/ArrayList;

    move-object/16 v273, v0

    invoke-direct/range {v273 .. v273}, Ljava/util/ArrayList;-><init>()V

    .line 1106
    .local v273, "filesToPatch":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    invoke-virtual/range {v273 .. v273}, Ljava/util/ArrayList;->clear()V

    .line 1107
    if-nez v184, :cond_1964

    if-eqz v33, :cond_2673

    .line 1109
    :cond_1964
    new-instance v0, Ljava/util/ArrayList;

    move-object/16 v313, v0

    invoke-direct/range {v313 .. v313}, Ljava/util/ArrayList;-><init>()V

    .line 1110
    .local v313, "oatForPatch":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    new-instance v9, Ljava/io/File;

    const-string v10, "/system/framework/x86/boot.oat"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_1985

    .line 1111
    new-instance v9, Ljava/io/File;

    const-string v10, "/system/framework/x86/boot.oat"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v313

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1113
    :cond_1985
    new-instance v9, Ljava/io/File;

    const-string v10, "/system/framework/arm64/boot.oat"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_199e

    .line 1114
    new-instance v9, Ljava/io/File;

    const-string v10, "/system/framework/arm64/boot.oat"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v313

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1116
    :cond_199e
    new-instance v9, Ljava/io/File;

    const-string v10, "/system/framework/arm/boot.oat"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_19b7

    .line 1117
    new-instance v9, Ljava/io/File;

    const-string v10, "/system/framework/arm/boot.oat"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v313

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1119
    :cond_19b7
    new-instance v9, Ljava/io/File;

    const-string v10, "/system/framework/oat/x86/boot.oat"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_19d0

    .line 1120
    new-instance v9, Ljava/io/File;

    const-string v10, "/system/framework/oat/x86/boot.oat"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v313

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1122
    :cond_19d0
    new-instance v9, Ljava/io/File;

    const-string v10, "/system/framework/oat/arm64/boot.oat"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_19e9

    .line 1123
    new-instance v9, Ljava/io/File;

    const-string v10, "/system/framework/oat/arm64/boot.oat"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v313

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1125
    :cond_19e9
    new-instance v9, Ljava/io/File;

    const-string v10, "/system/framework/oat/arm/boot.oat"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_1a02

    .line 1126
    new-instance v9, Ljava/io/File;

    const-string v10, "/system/framework/oat/arm/boot.oat"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v313

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1130
    :cond_1a02
    invoke-virtual/range {v313 .. v313}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-lez v9, :cond_1ffd

    sget v9, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->api:I

    const/16 v10, 0x15

    if-lt v9, v10, :cond_1ffd

    .line 1132
    const/4 v0, 0x0

    move/16 v325, v0

    .local v325, "patchOatUpd1":Z
    const/4 v0, 0x0

    move/16 v319, v0

    .local v319, "patchOat1":Z
    const/4 v0, 0x0

    move/16 v320, v0

    .local v320, "patchOat2":Z
    const/4 v0, 0x0

    move/16 v321, v0

    .local v321, "patchOat3":Z
    const/4 v0, 0x0

    move/16 v322, v0

    .line 1133
    .local v322, "patchOat4":Z
    invoke-virtual/range {v313 .. v313}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v34

    :goto_1a26
    invoke-interface/range {v34 .. v34}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_1f93

    invoke-interface/range {v34 .. v34}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object/16 v271, v0

    move-object/from16 v0, v271

    check-cast v0, Ljava/io/File;

    move-object/16 v271, v0

    .line 1134
    .local v271, "fileForPatch":Ljava/io/File;
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "oat file for patch:"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual/range {v271 .. v271}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1135
    const/16 v177, 0x0

    .line 1136
    .local v177, "ChannelDex":Ljava/nio/channels/FileChannel;
    const/4 v0, 0x0

    move/16 v361, v0

    .local v361, "uni":Z
    const/4 v0, 0x0

    move/16 v314, v0

    .local v314, "p11":Z
    const/4 v0, 0x0

    move/16 v315, v0

    .local v315, "p12":Z
    const/4 v0, 0x0

    move/16 v316, v0

    .line 1138
    .local v316, "p2":Z
    :try_start_1a68
    new-instance v9, Ljava/io/RandomAccessFile;

    const-string v10, "rw"

    move-object/from16 v0, v271

    invoke-direct {v9, v0, v10}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v177

    .line 1139
    sget-object v178, Ljava/nio/channels/FileChannel$MapMode;->READ_WRITE:Ljava/nio/channels/FileChannel$MapMode;

    const-wide/16 v179, 0x0

    invoke-virtual/range {v177 .. v177}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v9

    long-to-int v9, v9

    int-to-long v0, v9

    move-wide/from16 v181, v0

    invoke-virtual/range {v177 .. v182}, Ljava/nio/channels/FileChannel;->map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;

    move-result-object v9

    sput-object v9, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    .line 1141
    sget-object v9, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    const/16 v10, 0x1018

    invoke-virtual {v9, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1142
    sget-object v9, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v9}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v9

    sget-object v10, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v10}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v10

    sget-object v15, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v15}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v15

    sget-object v16, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual/range {v16 .. v16}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v16

    move/from16 v0, v16

    invoke-static {v9, v10, v15, v0}, Lcom/chelpus/Utils;->convertFourBytesToInt(BBBB)I

    move-result v0

    move/16 v329, v0

    .line 1143
    .local v329, "posit":I
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Start position:"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v329

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1144
    sget-object v9, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    move/from16 v0, v329

    invoke-virtual {v9, v0}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;
    :try_end_1ad0
    .catch Ljava/io/IOException; {:try_start_1a68 .. :try_end_1ad0} :catch_1f59

    .line 1147
    :goto_1ad0
    :try_start_1ad0
    sget-object v9, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v9}, Ljava/nio/MappedByteBuffer;->hasRemaining()Z

    move-result v9

    if-eqz v9, :cond_1f54

    .line 1149
    sget-object v9, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v9}, Ljava/nio/MappedByteBuffer;->position()I

    move-result v178

    .line 1150
    .local v178, "curentPos":I
    sget-object v9, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v9}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v179

    .local v179, "curentByte":B
    move-object/from16 v180, v5

    move-object/from16 v181, v6

    move-object/from16 v182, v7

    move-object/from16 v183, v8

    .line 1151
    invoke-static/range {v178 .. v184}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_1b44

    .line 1152
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_1b20

    .line 1153
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "position:"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v178

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1154
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core1uni patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1156
    :cond_1b20
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_1b34

    .line 1157
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core1uni restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1159
    :cond_1b34
    const/4 v0, 0x1

    move/16 v325, v0

    .line 1160
    const/4 v0, 0x1

    move/16 v361, v0

    const/4 v0, 0x1

    move/16 v314, v0

    const/4 v0, 0x1

    move/16 v315, v0

    :cond_1b44
    move/from16 v9, v178

    move/from16 v10, v179

    move/from16 v15, v184

    .line 1163
    invoke-static/range {v9 .. v15}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_1ba2

    .line 1164
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_1b7e

    .line 1165
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "position:"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v178

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1166
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core1uni patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1168
    :cond_1b7e
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_1b92

    .line 1169
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core1uni restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1171
    :cond_1b92
    const/4 v0, 0x1

    move/16 v325, v0

    .line 1172
    const/4 v0, 0x1

    move/16 v361, v0

    const/4 v0, 0x1

    move/16 v314, v0

    const/4 v0, 0x1

    move/16 v315, v0

    :cond_1ba2
    move/from16 v15, v178

    move/from16 v16, v179

    move/from16 v21, v184

    .line 1175
    invoke-static/range {v15 .. v21}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_1bf8

    .line 1176
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_1bdc

    .line 1177
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "position:"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v178

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1178
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core11 patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1180
    :cond_1bdc
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_1bf0

    .line 1181
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core11 restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1183
    :cond_1bf0
    const/4 v0, 0x1

    move/16 v319, v0

    .line 1184
    const/4 v0, 0x1

    move/16 v314, v0

    :cond_1bf8
    move/from16 v39, v178

    move/from16 v40, v179

    move/from16 v45, v184

    .line 1187
    invoke-static/range {v39 .. v45}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_1c56

    .line 1188
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_1c32

    .line 1189
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "position:"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v178

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1190
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core1uni patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1192
    :cond_1c32
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_1c46

    .line 1193
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core1uni restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1195
    :cond_1c46
    const/4 v0, 0x1

    move/16 v319, v0

    .line 1196
    const/4 v0, 0x1

    move/16 v314, v0

    const/4 v0, 0x1

    move/16 v315, v0

    const/4 v0, 0x1

    move/16 v361, v0

    :cond_1c56
    move/from16 v63, v178

    move/from16 v64, v179

    move/from16 v69, v184

    .line 1199
    invoke-static/range {v63 .. v69}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_1cb4

    .line 1200
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_1c90

    .line 1201
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "position:"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v178

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1202
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core1uni patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1204
    :cond_1c90
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_1ca4

    .line 1205
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core1uni restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1207
    :cond_1ca4
    const/4 v0, 0x1

    move/16 v319, v0

    .line 1208
    const/4 v0, 0x1

    move/16 v361, v0

    const/4 v0, 0x1

    move/16 v314, v0

    const/4 v0, 0x1

    move/16 v315, v0

    :cond_1cb4
    move/from16 v45, v178

    move/from16 v46, v179

    move/from16 v51, v184

    .line 1211
    invoke-static/range {v45 .. v51}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_1d0a

    .line 1212
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_1cee

    .line 1213
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "position:"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v178

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1214
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core11 patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1216
    :cond_1cee
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_1d02

    .line 1217
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core11 restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1219
    :cond_1d02
    const/4 v0, 0x1

    move/16 v319, v0

    .line 1220
    const/4 v0, 0x1

    move/16 v314, v0

    :cond_1d0a
    move/from16 v21, v178

    move/from16 v22, v179

    move/from16 v27, v184

    .line 1223
    invoke-static/range {v21 .. v27}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_1d60

    .line 1224
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_1d44

    .line 1225
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core12 patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1226
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "position:"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v178

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1228
    :cond_1d44
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_1d58

    .line 1229
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core12 restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1231
    :cond_1d58
    const/4 v0, 0x1

    move/16 v320, v0

    .line 1232
    const/4 v0, 0x1

    move/16 v315, v0

    :cond_1d60
    move/from16 v51, v178

    move/from16 v52, v179

    move/from16 v57, v184

    .line 1234
    invoke-static/range {v51 .. v57}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_1db6

    .line 1235
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_1d9a

    .line 1236
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core12 patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1237
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "position:"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v178

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1239
    :cond_1d9a
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_1dae

    .line 1240
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core12 restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1242
    :cond_1dae
    const/4 v0, 0x1

    move/16 v320, v0

    .line 1243
    const/4 v0, 0x1

    move/16 v315, v0

    :cond_1db6
    move/from16 v27, v178

    move/from16 v28, v179

    .line 1245
    invoke-static/range {v27 .. v33}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_1e0a

    .line 1246
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_1dee

    .line 1247
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core2 patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1248
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "position:"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v178

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1250
    :cond_1dee
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_1e02

    .line 1251
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core2 restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1253
    :cond_1e02
    const/4 v0, 0x1

    move/16 v321, v0

    .line 1254
    const/4 v0, 0x1

    move/16 v316, v0

    :cond_1e0a
    move/from16 v185, v178

    move/from16 v186, v179

    move-object/from16 v187, v35

    move-object/from16 v188, v36

    move-object/from16 v189, v37

    move-object/from16 v190, v38

    move/from16 v191, v33

    .line 1256
    invoke-static/range {v185 .. v191}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_1e68

    .line 1257
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_1e4c

    .line 1258
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core2 patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1259
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "position:"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v178

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1261
    :cond_1e4c
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_1e60

    .line 1262
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core2 restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1264
    :cond_1e60
    const/4 v0, 0x1

    move/16 v321, v0

    .line 1265
    const/4 v0, 0x1

    move/16 v316, v0

    :cond_1e68
    move/from16 v57, v178

    move/from16 v58, v179

    move/from16 v63, v33

    .line 1267
    invoke-static/range {v57 .. v63}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_1ebe

    .line 1268
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_1ea2

    .line 1269
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core2 patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1270
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "position:"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v178

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1272
    :cond_1ea2
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_1eb6

    .line 1273
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core2 restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1275
    :cond_1eb6
    const/4 v0, 0x1

    move/16 v321, v0

    .line 1276
    const/4 v0, 0x1

    move/16 v316, v0

    :cond_1ebe
    move/from16 v69, v178

    move/from16 v70, v179

    move/from16 v75, v33

    .line 1278
    invoke-static/range {v69 .. v75}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_1f14

    .line 1279
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_1ef8

    .line 1280
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core2 patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1281
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "position:"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v178

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1283
    :cond_1ef8
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_1f0c

    .line 1284
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core2 restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_1f0c
    .catch Ljava/lang/Exception; {:try_start_1ad0 .. :try_end_1f0c} :catch_1f74
    .catch Ljava/io/IOException; {:try_start_1ad0 .. :try_end_1f0c} :catch_1f59

    .line 1286
    :cond_1f0c
    const/4 v0, 0x1

    move/16 v321, v0

    .line 1287
    const/4 v0, 0x1

    move/16 v316, v0

    .line 1289
    :cond_1f14
    if-eqz v184, :cond_1f20

    if-eqz v33, :cond_1f20

    move/from16 v0, v361

    if-eqz v0, :cond_1f20

    move/from16 v0, v316

    if-nez v0, :cond_1f54

    :cond_1f20
    if-eqz v184, :cond_1f34

    if-eqz v33, :cond_1f34

    move/from16 v0, v361

    if-eqz v0, :cond_1f34

    move/from16 v0, v314

    if-eqz v0, :cond_1f34

    move/from16 v0, v315

    if-eqz v0, :cond_1f34

    move/from16 v0, v316

    if-nez v0, :cond_1f54

    :cond_1f34
    if-eqz v184, :cond_1f3c

    if-nez v33, :cond_1f3c

    move/from16 v0, v361

    if-nez v0, :cond_1f54

    :cond_1f3c
    if-eqz v184, :cond_1f4c

    if-nez v33, :cond_1f4c

    move/from16 v0, v361

    if-eqz v0, :cond_1f4c

    move/from16 v0, v314

    if-eqz v0, :cond_1f4c

    move/from16 v0, v315

    if-nez v0, :cond_1f54

    :cond_1f4c
    if-nez v184, :cond_1f6b

    if-eqz v33, :cond_1f6b

    move/from16 v0, v316

    if-eqz v0, :cond_1f6b

    .line 1309
    .end local v178    # "curentPos":I
    .end local v179    # "curentByte":B
    :cond_1f54
    :goto_1f54
    :try_start_1f54
    invoke-virtual/range {v177 .. v177}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_1f57
    .catch Ljava/io/IOException; {:try_start_1f54 .. :try_end_1f57} :catch_1f59

    goto/16 :goto_1a26

    .line 1310
    .end local v329    # "posit":I
    :catch_1f59
    move-exception v0

    move-object/16 v270, v0

    .line 1311
    .local v270, "e":Ljava/io/IOException;
    invoke-virtual/range {v270 .. v270}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_1a26

    .line 321
    .end local v177    # "ChannelDex":Ljava/nio/channels/FileChannel;
    .end local v270    # "e":Ljava/io/IOException;
    .end local v271    # "fileForPatch":Ljava/io/File;
    .end local v273    # "filesToPatch":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    .end local v313    # "oatForPatch":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    .end local v314    # "p11":Z
    .end local v315    # "p12":Z
    .end local v316    # "p2":Z
    .end local v319    # "patchOat1":Z
    .end local v320    # "patchOat2":Z
    .end local v321    # "patchOat3":Z
    .end local v322    # "patchOat4":Z
    .end local v325    # "patchOatUpd1":Z
    .end local v361    # "uni":Z
    :catch_1f62
    move-exception v0

    move-object/16 v270, v0

    .local v270, "e":Ljava/lang/Exception;
    invoke-virtual/range {v270 .. v270}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_2b1

    .line 1303
    .end local v270    # "e":Ljava/lang/Exception;
    .restart local v177    # "ChannelDex":Ljava/nio/channels/FileChannel;
    .restart local v178    # "curentPos":I
    .restart local v179    # "curentByte":B
    .restart local v271    # "fileForPatch":Ljava/io/File;
    .restart local v273    # "filesToPatch":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    .restart local v313    # "oatForPatch":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    .restart local v314    # "p11":Z
    .restart local v315    # "p12":Z
    .restart local v316    # "p2":Z
    .restart local v319    # "patchOat1":Z
    .restart local v320    # "patchOat2":Z
    .restart local v321    # "patchOat3":Z
    .restart local v322    # "patchOat4":Z
    .restart local v325    # "patchOatUpd1":Z
    .restart local v329    # "posit":I
    .restart local v361    # "uni":Z
    :cond_1f6b
    :try_start_1f6b
    sget-object v9, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    add-int/lit8 v10, v178, 0x1

    invoke-virtual {v9, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;
    :try_end_1f72
    .catch Ljava/lang/Exception; {:try_start_1f6b .. :try_end_1f72} :catch_1f74
    .catch Ljava/io/IOException; {:try_start_1f6b .. :try_end_1f72} :catch_1f59

    goto/16 :goto_1ad0

    .line 1305
    .end local v178    # "curentPos":I
    .end local v179    # "curentByte":B
    :catch_1f74
    move-exception v0

    move-object/16 v270, v0

    .line 1306
    .restart local v270    # "e":Ljava/lang/Exception;
    :try_start_1f78
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, ""

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, v270

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_1f92
    .catch Ljava/io/IOException; {:try_start_1f78 .. :try_end_1f92} :catch_1f59

    goto :goto_1f54

    .line 1314
    .end local v177    # "ChannelDex":Ljava/nio/channels/FileChannel;
    .end local v270    # "e":Ljava/lang/Exception;
    .end local v271    # "fileForPatch":Ljava/io/File;
    .end local v314    # "p11":Z
    .end local v315    # "p12":Z
    .end local v316    # "p2":Z
    .end local v329    # "posit":I
    .end local v361    # "uni":Z
    :cond_1f93
    move/from16 v0, v325

    if-nez v0, :cond_1fa3

    move/from16 v0, v319

    if-nez v0, :cond_1fa3

    move/from16 v0, v320

    if-nez v0, :cond_1fa3

    move/from16 v0, v321

    if-eqz v0, :cond_1ffd

    .line 1315
    :cond_1fa3
    new-instance v9, Ljava/io/File;

    const-string v10, "/data/dalvik-cache/arm/system@framework@boot.oat"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 1316
    new-instance v9, Ljava/io/File;

    const-string v10, "/data/dalvik-cache/arm/system@framework@boot.art"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 1317
    new-instance v9, Ljava/io/File;

    const-string v10, "/data/dalvik-cache/arm64/system@framework@boot.oat"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 1318
    new-instance v9, Ljava/io/File;

    const-string v10, "/data/dalvik-cache/arm64/system@framework@boot.art"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 1319
    new-instance v9, Ljava/io/File;

    const-string v10, "/data/dalvik-cache/x86/system@framework@boot.oat"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 1320
    new-instance v9, Ljava/io/File;

    const-string v10, "/data/dalvik-cache/x86/system@framework@boot.art"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 1321
    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v16, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->toolfilesdir:Ljava/lang/String;

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "/reboot"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v9, v10

    invoke-static {v9}, Lcom/chelpus/Utils;->cmdParam([Ljava/lang/String;)Ljava/lang/String;

    .line 1331
    .end local v319    # "patchOat1":Z
    .end local v320    # "patchOat2":Z
    .end local v321    # "patchOat3":Z
    .end local v322    # "patchOat4":Z
    .end local v325    # "patchOatUpd1":Z
    :cond_1ffd
    const/4 v0, 0x0

    move-object/16 v281, v0

    .line 1332
    .local v281, "localFile2":Ljava/io/File;
    const/4 v9, 0x4

    :try_start_2002
    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "framework"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_25f6

    .line 1333
    const/4 v9, 0x1

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "core.jar"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_2028

    const/4 v9, 0x1

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "core-libart.jar"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2086

    .line 1334
    :cond_2028
    new-instance v9, Ljava/io/File;

    const/4 v10, 0x1

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v9}, Lcom/chelpus/Utils;->classes_test(Ljava/io/File;)Z

    move-result v9

    if-eqz v9, :cond_2086

    .line 1335
    new-instance v0, Ljava/io/File;

    move-object/16 v359, v0

    const/4 v9, 0x1

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    move-object/from16 v0, v359

    invoke-direct {v0, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1336
    .local v359, "serjar":Ljava/io/File;
    invoke-static/range {v359 .. v359}, Lcom/chelpus/Utils;->getDirs(Ljava/io/File;)Ljava/io/File;

    move-result-object v9

    invoke-virtual {v9}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, v359

    invoke-static {v0, v9}, Lcom/chelpus/root/utils/corepatch;->unzip(Ljava/io/File;Ljava/lang/String;)V

    .line 1337
    new-instance v0, Ljava/io/File;

    move-object/16 v262, v0

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static/range {v359 .. v359}, Lcom/chelpus/Utils;->getDirs(Ljava/io/File;)Ljava/io/File;

    move-result-object v10

    invoke-virtual {v10}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/classes.dex"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, v262

    invoke-direct {v0, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1338
    .local v262, "clasdex":Ljava/io/File;
    invoke-virtual/range {v262 .. v262}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_2086

    .line 1339
    move-object/from16 v0, v273

    move-object/from16 v1, v262

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1344
    .end local v262    # "clasdex":Ljava/io/File;
    .end local v359    # "serjar":Ljava/io/File;
    :cond_2086
    const/4 v9, 0x1

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "core.odex"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_20a0

    const/4 v9, 0x1

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "boot.oat"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_20af

    .line 1345
    :cond_20a0
    new-instance v9, Ljava/io/File;

    const/4 v10, 0x1

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v273

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1380
    :cond_20af
    :goto_20af
    invoke-virtual/range {v273 .. v273}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v34

    :cond_20b3
    :goto_20b3
    invoke-interface/range {v34 .. v34}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2673

    invoke-interface/range {v34 .. v34}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object/16 v275, v0

    move-object/from16 v0, v275

    check-cast v0, Ljava/io/File;

    move-object/16 v275, v0

    .line 1382
    .local v275, "fp":Ljava/io/File;
    move-object/16 v281, v275

    .line 1384
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "file for patch: "

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual/range {v281 .. v281}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v15, " size:"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual/range {v281 .. v281}, Ljava/io/File;->length()J

    move-result-wide v15

    move-wide v0, v15

    invoke-virtual {v10, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1386
    new-instance v9, Ljava/io/RandomAccessFile;

    const-string v10, "rw"

    move-object/from16 v0, v281

    invoke-direct {v9, v0, v10}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v177

    .line 1387
    .restart local v177    # "ChannelDex":Ljava/nio/channels/FileChannel;
    sget-object v186, Ljava/nio/channels/FileChannel$MapMode;->READ_WRITE:Ljava/nio/channels/FileChannel$MapMode;

    const-wide/16 v187, 0x0

    invoke-virtual/range {v177 .. v177}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v9

    long-to-int v9, v9

    int-to-long v0, v9

    move-wide/from16 v189, v0

    move-object/from16 v185, v177

    invoke-virtual/range {v185 .. v190}, Ljava/nio/channels/FileChannel;->map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;

    move-result-object v9

    sput-object v9, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    .line 1389
    const/4 v0, 0x0

    move/16 v317, v0

    .line 1394
    .local v317, "patch1":Z
    invoke-virtual/range {v281 .. v281}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    const-string v10, "boot.oat"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_2123
    .catch Ljava/io/FileNotFoundException; {:try_start_2002 .. :try_end_2123} :catch_2668
    .catch Ljava/lang/Exception; {:try_start_2002 .. :try_end_2123} :catch_3352

    move-result v9

    if-nez v9, :cond_3358

    .line 1397
    const-wide/16 v0, 0x0

    move-wide/16 v279, v0

    .local v279, "j":J
    :goto_212b
    :try_start_212b
    sget-object v9, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v9}, Ljava/nio/MappedByteBuffer;->hasRemaining()Z

    move-result v9

    if-eqz v9, :cond_3260

    .line 1399
    sget-object v9, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v9}, Ljava/nio/MappedByteBuffer;->position()I

    move-result v178

    .line 1400
    .restart local v178    # "curentPos":I
    sget-object v9, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v9}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v179

    .restart local v179    # "curentByte":B
    move-object/from16 v180, v212

    move-object/from16 v181, v294

    move-object/from16 v182, v242

    move-object/from16 v183, v340

    .line 1402
    invoke-static/range {v178 .. v184}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_2179

    .line 1403
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2161

    .line 1404
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1406
    :cond_2161
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2175

    .line 1407
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1409
    :cond_2175
    const/4 v0, 0x1

    move/16 v317, v0

    :cond_2179
    move-object/from16 v180, v215

    move-object/from16 v181, v294

    move-object/from16 v182, v245

    move-object/from16 v183, v340

    .line 1411
    invoke-static/range {v178 .. v184}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_21b3

    .line 1412
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_219b

    .line 1413
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1415
    :cond_219b
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_21af

    .line 1416
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1418
    :cond_21af
    const/4 v0, 0x1

    move/16 v317, v0

    :cond_21b3
    move/from16 v185, v178

    move/from16 v186, v179

    move-object/from16 v187, v216

    move-object/from16 v188, v297

    move-object/from16 v189, v246

    move-object/from16 v190, v343

    move/from16 v191, v33

    .line 1420
    invoke-static/range {v185 .. v191}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_21f3

    .line 1421
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_21db

    .line 1422
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core unsigned install patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1424
    :cond_21db
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_21ef

    .line 1425
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core unsigned install restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1427
    :cond_21ef
    const/4 v0, 0x1

    move/16 v317, v0

    :cond_21f3
    move/from16 v185, v178

    move/from16 v186, v179

    move-object/from16 v187, v217

    move-object/from16 v188, v298

    move-object/from16 v189, v247

    move-object/from16 v190, v344

    move/from16 v191, v33

    .line 1429
    invoke-static/range {v185 .. v191}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_2233

    .line 1430
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_221b

    .line 1431
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core unsigned install patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1433
    :cond_221b
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_222f

    .line 1434
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core unsigned install restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1436
    :cond_222f
    const/4 v0, 0x1

    move/16 v317, v0

    :cond_2233
    move-object/from16 v180, v218

    move-object/from16 v181, v299

    move-object/from16 v182, v248

    move-object/from16 v183, v345

    .line 1438
    invoke-static/range {v178 .. v184}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_226d

    .line 1439
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2255

    .line 1440
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1442
    :cond_2255
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2269

    .line 1443
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1445
    :cond_2269
    const/4 v0, 0x1

    move/16 v317, v0

    :cond_226d
    move-object/from16 v180, v219

    move-object/from16 v181, v300

    move-object/from16 v182, v249

    move-object/from16 v183, v346

    .line 1447
    invoke-static/range {v178 .. v184}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_22a7

    .line 1448
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_228f

    .line 1449
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core 2 patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1451
    :cond_228f
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_22a3

    .line 1452
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core 2 restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1454
    :cond_22a3
    const/4 v0, 0x1

    move/16 v317, v0

    :cond_22a7
    move-object/from16 v180, v220

    move-object/from16 v181, v301

    move-object/from16 v182, v250

    move-object/from16 v183, v347

    .line 1456
    invoke-static/range {v178 .. v184}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_22e1

    .line 1457
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_22c9

    .line 1458
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core 2 patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1460
    :cond_22c9
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_22dd

    .line 1461
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core 2 restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1463
    :cond_22dd
    const/4 v0, 0x1

    move/16 v317, v0

    :cond_22e1
    move-object/from16 v180, v221

    move-object/from16 v181, v302

    move-object/from16 v182, v251

    move-object/from16 v183, v348

    .line 1465
    invoke-static/range {v178 .. v184}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_231b

    .line 1466
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2303

    .line 1467
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core 2 patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1469
    :cond_2303
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2317

    .line 1470
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core 2 restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1472
    :cond_2317
    const/4 v0, 0x1

    move/16 v317, v0

    :cond_231b
    move-object/from16 v180, v202

    move-object/from16 v181, v284

    move-object/from16 v182, v232

    move-object/from16 v183, v330

    .line 1474
    invoke-static/range {v178 .. v184}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_2355

    .line 1475
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_233d

    .line 1476
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1478
    :cond_233d
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2351

    .line 1479
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1481
    :cond_2351
    const/4 v0, 0x1

    move/16 v317, v0

    :cond_2355
    move-object/from16 v180, v203

    move-object/from16 v181, v285

    move-object/from16 v182, v233

    move-object/from16 v183, v331

    .line 1483
    invoke-static/range {v178 .. v184}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_238f

    .line 1484
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2377

    .line 1485
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1487
    :cond_2377
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_238b

    .line 1488
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1490
    :cond_238b
    const/4 v0, 0x1

    move/16 v317, v0

    :cond_238f
    move/from16 v185, v178

    move/from16 v186, v179

    move-object/from16 v187, v204

    move-object/from16 v188, v286

    move-object/from16 v189, v234

    move-object/from16 v190, v332

    move/from16 v191, v33

    .line 1492
    invoke-static/range {v185 .. v191}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_23cf

    .line 1493
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_23b7

    .line 1494
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core unsigned install patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1496
    :cond_23b7
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_23cb

    .line 1497
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core unsigned install restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1499
    :cond_23cb
    const/4 v0, 0x1

    move/16 v317, v0

    :cond_23cf
    move/from16 v185, v178

    move/from16 v186, v179

    move-object/from16 v187, v205

    move-object/from16 v188, v287

    move-object/from16 v189, v235

    move-object/from16 v190, v333

    move/from16 v191, v33

    .line 1501
    invoke-static/range {v185 .. v191}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_240f

    .line 1502
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_23f7

    .line 1503
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core unsigned install patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1505
    :cond_23f7
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_240b

    .line 1506
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core unsigned install restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1508
    :cond_240b
    const/4 v0, 0x1

    move/16 v317, v0

    :cond_240f
    move/from16 v185, v178

    move/from16 v186, v179

    move-object/from16 v187, v213

    move-object/from16 v188, v295

    move-object/from16 v189, v243

    move-object/from16 v190, v341

    move/from16 v191, v33

    .line 1510
    invoke-static/range {v185 .. v191}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_244f

    .line 1511
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2437

    .line 1512
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core unsigned install patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1514
    :cond_2437
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_244b

    .line 1515
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core unsigned install restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1517
    :cond_244b
    const/4 v0, 0x1

    move/16 v317, v0

    :cond_244f
    move-object/from16 v180, v214

    move-object/from16 v181, v296

    move-object/from16 v182, v244

    move-object/from16 v183, v342

    .line 1519
    invoke-static/range {v178 .. v184}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_2489

    .line 1520
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2471

    .line 1521
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1523
    :cond_2471
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2485

    .line 1524
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1526
    :cond_2485
    const/4 v0, 0x1

    move/16 v317, v0

    :cond_2489
    move-object/from16 v180, v206

    move-object/from16 v181, v288

    move-object/from16 v182, v236

    move-object/from16 v183, v334

    .line 1528
    invoke-static/range {v178 .. v184}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_24c3

    .line 1529
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_24ab

    .line 1530
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1532
    :cond_24ab
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_24bf

    .line 1533
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1535
    :cond_24bf
    const/4 v0, 0x1

    move/16 v317, v0

    :cond_24c3
    move-object/from16 v180, v207

    move-object/from16 v181, v289

    move-object/from16 v182, v237

    move-object/from16 v183, v335

    .line 1537
    invoke-static/range {v178 .. v184}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_24fd

    .line 1538
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_24e5

    .line 1539
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core 2 patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1541
    :cond_24e5
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_24f9

    .line 1542
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core 2 restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1544
    :cond_24f9
    const/4 v0, 0x1

    move/16 v317, v0

    :cond_24fd
    move-object/from16 v180, v208

    move-object/from16 v181, v290

    move-object/from16 v182, v238

    move-object/from16 v183, v336

    .line 1546
    invoke-static/range {v178 .. v184}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_2537

    .line 1547
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_251f

    .line 1548
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core 2 patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1550
    :cond_251f
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2533

    .line 1551
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core 2 restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1553
    :cond_2533
    const/4 v0, 0x1

    move/16 v317, v0

    :cond_2537
    move-object/from16 v180, v209

    move-object/from16 v181, v291

    move-object/from16 v182, v239

    move-object/from16 v183, v337

    .line 1555
    invoke-static/range {v178 .. v184}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_2571

    .line 1556
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2559

    .line 1557
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core 2 patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1559
    :cond_2559
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_256d

    .line 1560
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core 2 restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1562
    :cond_256d
    const/4 v0, 0x1

    move/16 v317, v0

    :cond_2571
    move-object/from16 v180, v210

    move-object/from16 v181, v292

    move-object/from16 v182, v240

    move-object/from16 v183, v338

    .line 1564
    invoke-static/range {v178 .. v184}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_25ab

    .line 1565
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2593

    .line 1566
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core 2 patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1568
    :cond_2593
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_25a7

    .line 1569
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core 2 restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1571
    :cond_25a7
    const/4 v0, 0x1

    move/16 v317, v0

    :cond_25ab
    move-object/from16 v180, v211

    move-object/from16 v181, v293

    move-object/from16 v182, v241

    move-object/from16 v183, v339

    .line 1573
    invoke-static/range {v178 .. v184}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_25e5

    .line 1574
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_25cd

    .line 1575
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core 2 patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1577
    :cond_25cd
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_25e1

    .line 1578
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core 2 restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1580
    :cond_25e1
    const/4 v0, 0x1

    move/16 v317, v0

    .line 1583
    :cond_25e5
    sget-object v9, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    add-int/lit8 v10, v178, 0x1

    invoke-virtual {v9, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;
    :try_end_25ec
    .catch Ljava/lang/Exception; {:try_start_212b .. :try_end_25ec} :catch_3242
    .catch Ljava/io/FileNotFoundException; {:try_start_212b .. :try_end_25ec} :catch_2668

    .line 1397
    const-wide/16 v9, 0x1

    move-wide/from16 v0, v279

    add-long/2addr v0, v9

    move-wide/16 v279, v0

    goto/16 :goto_212b

    .line 1348
    .end local v177    # "ChannelDex":Ljava/nio/channels/FileChannel;
    .end local v178    # "curentPos":I
    .end local v179    # "curentByte":B
    .end local v275    # "fp":Ljava/io/File;
    .end local v279    # "j":J
    .end local v317    # "patch1":Z
    :cond_25f6
    const/4 v9, 0x4

    :try_start_25f7
    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "ART"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_317a

    .line 1349
    new-instance v9, Ljava/io/File;

    const-string v10, "/system/framework/core-libart.odex"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_262d

    new-instance v9, Ljava/io/File;

    const-string v10, "/system/framework/core-libart.odex"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->length()J

    move-result-wide v9

    const-wide/16 v15, 0x0

    cmp-long v9, v9, v15

    if-eqz v9, :cond_262d

    .line 1350
    new-instance v9, Ljava/io/File;

    const-string v10, "/system/framework/core-libart.odex"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v273

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1352
    :cond_262d
    new-instance v9, Ljava/io/File;

    const-string v10, "/system/framework/core-libart.jar"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v9}, Lcom/chelpus/Utils;->classes_test(Ljava/io/File;)Z

    move-result v9

    if-eqz v9, :cond_20af

    .line 1353
    new-instance v0, Ljava/io/File;

    move-object/16 v359, v0

    const-string v9, "/system/framework/core-libart.jar"

    move-object/from16 v0, v359

    invoke-direct {v0, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1354
    .restart local v359    # "serjar":Ljava/io/File;
    const-string v9, "/data/app"

    move-object/from16 v0, v359

    invoke-static {v0, v9}, Lcom/chelpus/root/utils/corepatch;->unzip(Ljava/io/File;Ljava/lang/String;)V

    .line 1355
    new-instance v0, Ljava/io/File;

    move-object/16 v262, v0

    const-string v9, "/data/app/classes.dex"

    move-object/from16 v0, v262

    invoke-direct {v0, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1356
    .restart local v262    # "clasdex":Ljava/io/File;
    invoke-virtual/range {v262 .. v262}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_20af

    .line 1357
    move-object/from16 v0, v273

    move-object/from16 v1, v262

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2666
    .catch Ljava/io/FileNotFoundException; {:try_start_25f7 .. :try_end_2666} :catch_2668
    .catch Ljava/lang/Exception; {:try_start_25f7 .. :try_end_2666} :catch_3352

    goto/16 :goto_20af

    .line 1890
    .end local v262    # "clasdex":Ljava/io/File;
    .end local v359    # "serjar":Ljava/io/File;
    :catch_2668
    move-exception v0

    move-object/16 v283, v0

    .line 1891
    .local v283, "localFileNotFoundException":Ljava/io/FileNotFoundException;
    :goto_266c
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Error: core.odex not found!\n\nPlease Odex core.jar and try again!"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1898
    .end local v281    # "localFile2":Ljava/io/File;
    .end local v283    # "localFileNotFoundException":Ljava/io/FileNotFoundException;
    .end local v313    # "oatForPatch":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    :cond_2673
    :goto_2673
    if-nez v81, :cond_2677

    if-eqz v105, :cond_3c6c

    .line 1899
    :cond_2677
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Start patch for services.jar"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1900
    const-string v0, ""

    move-object/16 v277, v0

    .line 1901
    .local v277, "indexDir":Ljava/lang/String;
    new-instance v9, Ljava/io/File;

    const-string v10, "/system/framework/arm/services.odex"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_2695

    const-string v0, "/arm"

    move-object/16 v277, v0

    .line 1902
    :cond_2695
    new-instance v9, Ljava/io/File;

    const-string v10, "/system/framework/arm64/services.odex"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_26a7

    const-string v0, "/arm64"

    move-object/16 v277, v0

    .line 1903
    :cond_26a7
    new-instance v9, Ljava/io/File;

    const-string v10, "/system/framework/x86/services.odex"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_26b9

    const-string v0, "/x86"

    move-object/16 v277, v0

    .line 1904
    :cond_26b9
    new-instance v9, Ljava/io/File;

    const-string v10, "/system/framework/arm/services.odex.xz"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_26cb

    const-string v0, "/arm"

    move-object/16 v277, v0

    .line 1905
    :cond_26cb
    new-instance v9, Ljava/io/File;

    const-string v10, "/system/framework/arm64/services.odex.xz"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_26dd

    const-string v0, "/arm64"

    move-object/16 v277, v0

    .line 1906
    :cond_26dd
    new-instance v9, Ljava/io/File;

    const-string v10, "/system/framework/x86/services.odex.xz"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_26ef

    const-string v0, "/x86"

    move-object/16 v277, v0

    .line 1907
    :cond_26ef
    new-instance v9, Ljava/io/File;

    const-string v10, "/system/framework/oat/arm/services.odex"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_2701

    const-string v0, "/oat/arm"

    move-object/16 v277, v0

    .line 1908
    :cond_2701
    new-instance v9, Ljava/io/File;

    const-string v10, "/system/framework/oat/arm64/services.odex"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_2713

    const-string v0, "/oat/arm64"

    move-object/16 v277, v0

    .line 1909
    :cond_2713
    new-instance v9, Ljava/io/File;

    const-string v10, "/system/framework/oat/x86/services.odex"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_2725

    const-string v0, "/oat/x86"

    move-object/16 v277, v0

    .line 1910
    :cond_2725
    new-instance v9, Ljava/io/File;

    const-string v10, "/system/framework/oat/arm/services.odex.xz"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_2737

    const-string v0, "/oat/arm"

    move-object/16 v277, v0

    .line 1911
    :cond_2737
    new-instance v9, Ljava/io/File;

    const-string v10, "/system/framework/oat/arm64/services.odex.xz"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_2749

    const-string v0, "/oat/arm64"

    move-object/16 v277, v0

    .line 1912
    :cond_2749
    new-instance v9, Ljava/io/File;

    const-string v10, "/system/framework/oat/x86/services.odex.xz"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_275b

    const-string v0, "/oat/x86"

    move-object/16 v277, v0

    .line 1913
    :cond_275b
    const-string v9, ""

    move-object/from16 v0, v277

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_2d03

    sget v9, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->api:I

    const/16 v10, 0x15

    if-lt v9, v10, :cond_2d03

    .line 1916
    const/16 v177, 0x0

    .line 1917
    .restart local v177    # "ChannelDex":Ljava/nio/channels/FileChannel;
    const/4 v0, 0x0

    move/16 v319, v0

    .restart local v319    # "patchOat1":Z
    const/4 v0, 0x0

    move/16 v320, v0

    .restart local v320    # "patchOat2":Z
    const/4 v0, 0x0

    move/16 v321, v0

    .restart local v321    # "patchOat3":Z
    const/4 v0, 0x0

    move/16 v322, v0

    .restart local v322    # "patchOat4":Z
    const/4 v0, 0x0

    move/16 v323, v0

    .local v323, "patchOat5":Z
    const/4 v0, 0x0

    move/16 v324, v0

    .line 1918
    .local v324, "patchOat6":Z
    new-instance v0, Ljava/io/File;

    move-object/16 v274, v0

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "/system/framework"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object/from16 v0, v277

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/services.odex"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, v274

    invoke-direct {v0, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1919
    .local v274, "for_patch":Ljava/io/File;
    const/4 v0, 0x1

    move/16 v276, v0

    .line 1920
    .local v276, "good_odex":Z
    new-instance v9, Ljava/io/File;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "/system/framework"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, v277

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v15, "/services.odex.xz"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_2844

    .line 1922
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "try unpack services.odex.xz"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1923
    new-instance v9, Ljava/io/File;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "/system/framework"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, v277

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v15, "/services.odex.xz"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "/system/framework"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, v277

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/chelpus/Utils;->XZDecompress(Ljava/io/File;Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_3b13

    .line 1924
    const/4 v0, 0x0

    move/16 v276, v0

    .line 1925
    new-instance v9, Ljava/io/File;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "/system/framework"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, v277

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v15, "/services.odex"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 1926
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "not enought space for unpack services.odex.xz"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1933
    :cond_2844
    :goto_2844
    move/from16 v0, v276

    if-eqz v0, :cond_3bd1

    .line 1935
    :try_start_2848
    new-instance v9, Ljava/io/RandomAccessFile;

    const-string v10, "rw"

    move-object/from16 v0, v274

    invoke-direct {v9, v0, v10}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v177

    .line 1936
    sget-object v186, Ljava/nio/channels/FileChannel$MapMode;->READ_WRITE:Ljava/nio/channels/FileChannel$MapMode;

    const-wide/16 v187, 0x0

    invoke-virtual/range {v177 .. v177}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v9

    long-to-int v9, v9

    int-to-long v0, v9

    move-wide/from16 v189, v0

    move-object/from16 v185, v177

    invoke-virtual/range {v185 .. v190}, Ljava/nio/channels/FileChannel;->map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;

    move-result-object v9

    sput-object v9, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    .line 1938
    sget-object v9, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    const/16 v10, 0x1018

    invoke-virtual {v9, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1939
    sget-object v9, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v9}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v9

    sget-object v10, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v10}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v10

    sget-object v15, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v15}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v15

    sget-object v16, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual/range {v16 .. v16}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v16

    move/from16 v0, v16

    invoke-static {v9, v10, v15, v0}, Lcom/chelpus/Utils;->convertFourBytesToInt(BBBB)I

    move-result v0

    move/16 v329, v0

    .line 1940
    .restart local v329    # "posit":I
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Start position:"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v329

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1941
    sget-object v9, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    move/from16 v0, v329

    invoke-virtual {v9, v0}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;
    :try_end_28b2
    .catch Ljava/io/IOException; {:try_start_2848 .. :try_end_28b2} :catch_3bc8

    .line 1944
    :goto_28b2
    :try_start_28b2
    sget-object v9, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v9}, Ljava/nio/MappedByteBuffer;->hasRemaining()Z

    move-result v9

    if-eqz v9, :cond_2c8e

    .line 1946
    sget-object v9, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v9}, Ljava/nio/MappedByteBuffer;->position()I

    move-result v178

    .line 1947
    .restart local v178    # "curentPos":I
    sget-object v9, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v9}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v179

    .restart local v179    # "curentByte":B
    move/from16 v75, v178

    move/from16 v76, v179

    .line 1948
    invoke-static/range {v75 .. v81}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_28fc

    .line 1949
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_28e4

    .line 1950
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core4 patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1952
    :cond_28e4
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_28f8

    .line 1953
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core4 restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1955
    :cond_28f8
    const/4 v0, 0x1

    move/16 v319, v0

    :cond_28fc
    move/from16 v185, v178

    move/from16 v186, v179

    move-object/from16 v187, v83

    move-object/from16 v188, v84

    move-object/from16 v189, v85

    move-object/from16 v190, v86

    move/from16 v191, v81

    .line 1957
    invoke-static/range {v185 .. v191}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_293c

    .line 1958
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2924

    .line 1959
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core4 patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1961
    :cond_2924
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2938

    .line 1962
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core4 restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1964
    :cond_2938
    const/4 v0, 0x1

    move/16 v319, v0

    :cond_293c
    move/from16 v87, v178

    move/from16 v88, v179

    move/from16 v93, v81

    .line 1966
    invoke-static/range {v87 .. v93}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_2974

    .line 1967
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_295c

    .line 1968
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core4 patched!InstallLocationPolice\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1970
    :cond_295c
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2970

    .line 1971
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core4 restored!InstallLocationPolice\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1973
    :cond_2970
    const/4 v0, 0x1

    move/16 v320, v0

    :cond_2974
    move/from16 v123, v178

    move/from16 v124, v179

    move/from16 v129, v81

    .line 1975
    invoke-static/range {v123 .. v129}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_29ac

    .line 1976
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2994

    .line 1977
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core4 patched!InstallLocationPolice\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1979
    :cond_2994
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_29a8

    .line 1980
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core4 restored!InstallLocationPolice\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1982
    :cond_29a8
    const/4 v0, 0x1

    move/16 v320, v0

    :cond_29ac
    move/from16 v93, v178

    move/from16 v94, v179

    move/from16 v99, v81

    .line 1984
    invoke-static/range {v93 .. v99}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_29e4

    .line 1985
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_29cc

    .line 1986
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core4 patched!checkUpgradeKeySetLP\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1988
    :cond_29cc
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_29e0

    .line 1989
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core4 restored!checkUpgradeKeySetLP\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1991
    :cond_29e0
    const/4 v0, 0x1

    move/16 v321, v0

    :cond_29e4
    move/from16 v99, v178

    move/from16 v100, v179

    .line 1993
    invoke-static/range {v99 .. v105}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_2a1a

    .line 1994
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2a02

    .line 1995
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core4 patched!queryIntentServices\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1997
    :cond_2a02
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2a16

    .line 1998
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core4 restored!queryIntentServices\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2000
    :cond_2a16
    const/4 v0, 0x1

    move/16 v322, v0

    :cond_2a1a
    move/from16 v185, v178

    move/from16 v186, v179

    move-object/from16 v187, v107

    move-object/from16 v188, v108

    move-object/from16 v189, v109

    move-object/from16 v190, v110

    move/from16 v191, v105

    .line 2002
    invoke-static/range {v185 .. v191}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_2a5a

    .line 2003
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2a42

    .line 2004
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core4 patched!buildResolveList\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2006
    :cond_2a42
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2a56

    .line 2007
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core4 restored!buildResolveList\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2009
    :cond_2a56
    const/4 v0, 0x1

    move/16 v323, v0

    :cond_2a5a
    move/from16 v111, v178

    move/from16 v112, v179

    move/from16 v117, v81

    .line 2011
    invoke-static/range {v111 .. v117}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_2a92

    .line 2012
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2a7a

    .line 2013
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core4 patched!FixForCM\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2015
    :cond_2a7a
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2a8e

    .line 2016
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core4 restored!FixForCM\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2018
    :cond_2a8e
    const/4 v0, 0x1

    move/16 v324, v0

    :cond_2a92
    move/from16 v117, v178

    move/from16 v118, v179

    move/from16 v123, v81

    .line 2020
    invoke-static/range {v117 .. v123}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_2aca

    .line 2021
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2ab2

    .line 2022
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core4 patched!FixForCM\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2024
    :cond_2ab2
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2ac6

    .line 2025
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core4 restored!FixForCM\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2027
    :cond_2ac6
    const/4 v0, 0x1

    move/16 v324, v0

    :cond_2aca
    move/from16 v129, v178

    move/from16 v130, v179

    move/from16 v135, v81

    .line 2029
    invoke-static/range {v129 .. v135}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_2b02

    .line 2030
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2aea

    .line 2031
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core4 patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2033
    :cond_2aea
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2afe

    .line 2034
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core4 restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2036
    :cond_2afe
    const/4 v0, 0x1

    move/16 v319, v0

    :cond_2b02
    move/from16 v135, v178

    move/from16 v136, v179

    move/from16 v141, v81

    .line 2038
    invoke-static/range {v135 .. v141}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_2b3a

    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2b22

    .line 2039
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core4 patched!InstallLocationPolice\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2041
    :cond_2b22
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2b36

    .line 2042
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core4 restored!InstallLocationPolice\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2044
    :cond_2b36
    const/4 v0, 0x1

    move/16 v320, v0

    :cond_2b3a
    move/from16 v153, v178

    move/from16 v154, v179

    move/from16 v159, v81

    .line 2046
    invoke-static/range {v153 .. v159}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_2b72

    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2b5a

    .line 2047
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core4 patched!InstallLocationPolice\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2049
    :cond_2b5a
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2b6e

    .line 2050
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core4 restored!InstallLocationPolice\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2052
    :cond_2b6e
    const/4 v0, 0x1

    move/16 v320, v0

    :cond_2b72
    move/from16 v159, v178

    move/from16 v160, v179

    move/from16 v165, v81

    .line 2054
    invoke-static/range {v159 .. v165}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_2baa

    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2b92

    .line 2055
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core4 patched!InstallLocationPolice\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2057
    :cond_2b92
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2ba6

    .line 2058
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core4 restored!InstallLocationPolice\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2060
    :cond_2ba6
    const/4 v0, 0x1

    move/16 v320, v0

    :cond_2baa
    move/from16 v141, v178

    move/from16 v142, v179

    move/from16 v147, v81

    .line 2062
    invoke-static/range {v141 .. v147}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_2be2

    .line 2063
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2bca

    .line 2064
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core4 patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2066
    :cond_2bca
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2bde

    .line 2067
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core4 restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2069
    :cond_2bde
    const/4 v0, 0x1

    move/16 v319, v0

    :cond_2be2
    move/from16 v147, v178

    move/from16 v148, v179

    move/from16 v153, v81

    .line 2071
    invoke-static/range {v147 .. v153}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_2c1a

    .line 2072
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2c02

    .line 2073
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core4 patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2075
    :cond_2c02
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2c16

    .line 2076
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Core4 restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2078
    :cond_2c16
    const/4 v0, 0x1

    move/16 v319, v0

    .line 2081
    :cond_2c1a
    if-eqz v81, :cond_3b9f

    move/from16 v0, v319

    if-eqz v0, :cond_3b9f

    move/from16 v0, v320

    if-eqz v0, :cond_3b9f

    move/from16 v0, v321

    if-eqz v0, :cond_3b9f

    move/from16 v0, v322

    if-eqz v0, :cond_3b9f

    move/from16 v0, v323

    if-eqz v0, :cond_3b9f

    move/from16 v0, v324

    if-eqz v0, :cond_3b9f

    .line 2082
    new-instance v9, Ljava/io/File;

    const-string v10, "/data/dalvik-cache/arm/system@framework@services.jar@classes.dex"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 2083
    new-instance v9, Ljava/io/File;

    const-string v10, "/data/dalvik-cache/arm/system@framework@services.jar@classes.art"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 2084
    new-instance v9, Ljava/io/File;

    const-string v10, "/data/dalvik-cache/arm64/system@framework@services.jar@classes.dex"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 2085
    new-instance v9, Ljava/io/File;

    const-string v10, "/data/dalvik-cache/arm64/system@framework@services.jar@classes.art"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 2086
    new-instance v9, Ljava/io/File;

    const-string v10, "/data/dalvik-cache/x86/system@framework@services.jar@classes.dex"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 2087
    new-instance v9, Ljava/io/File;

    const-string v10, "/data/dalvik-cache/x86/system@framework@services.jar@classes.art"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 2088
    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v16, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->toolfilesdir:Ljava/lang/String;

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "/reboot"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v9, v10

    invoke-static {v9}, Lcom/chelpus/Utils;->cmdParam([Ljava/lang/String;)Ljava/lang/String;
    :try_end_2c8e
    .catch Ljava/lang/Exception; {:try_start_28b2 .. :try_end_2c8e} :catch_3ba8
    .catch Ljava/io/IOException; {:try_start_28b2 .. :try_end_2c8e} :catch_3bc8

    .line 2097
    .end local v178    # "curentPos":I
    .end local v179    # "curentByte":B
    :cond_2c8e
    :goto_2c8e
    :try_start_2c8e
    invoke-virtual/range {v177 .. v177}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_2c91
    .catch Ljava/io/IOException; {:try_start_2c8e .. :try_end_2c91} :catch_3bc8

    .line 2104
    .end local v329    # "posit":I
    :goto_2c91
    move/from16 v0, v319

    if-nez v0, :cond_2ca9

    move/from16 v0, v320

    if-nez v0, :cond_2ca9

    move/from16 v0, v321

    if-nez v0, :cond_2ca9

    move/from16 v0, v322

    if-nez v0, :cond_2ca9

    move/from16 v0, v323

    if-nez v0, :cond_2ca9

    move/from16 v0, v324

    if-eqz v0, :cond_2d03

    .line 2105
    :cond_2ca9
    new-instance v9, Ljava/io/File;

    const-string v10, "/data/dalvik-cache/arm/system@framework@services.jar@classes.dex"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 2106
    new-instance v9, Ljava/io/File;

    const-string v10, "/data/dalvik-cache/arm/system@framework@services.jar@classes.art"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 2107
    new-instance v9, Ljava/io/File;

    const-string v10, "/data/dalvik-cache/arm64/system@framework@services.jar@classes.dex"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 2108
    new-instance v9, Ljava/io/File;

    const-string v10, "/data/dalvik-cache/arm64/system@framework@services.jar@classes.art"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 2109
    new-instance v9, Ljava/io/File;

    const-string v10, "/data/dalvik-cache/x86/system@framework@services.jar@classes.dex"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 2110
    new-instance v9, Ljava/io/File;

    const-string v10, "/data/dalvik-cache/x86/system@framework@services.jar@classes.art"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 2111
    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v16, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->toolfilesdir:Ljava/lang/String;

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "/reboot"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v9, v10

    invoke-static {v9}, Lcom/chelpus/Utils;->cmdParam([Ljava/lang/String;)Ljava/lang/String;

    .line 2121
    .end local v177    # "ChannelDex":Ljava/nio/channels/FileChannel;
    .end local v274    # "for_patch":Ljava/io/File;
    .end local v276    # "good_odex":Z
    .end local v319    # "patchOat1":Z
    .end local v320    # "patchOat2":Z
    .end local v321    # "patchOat3":Z
    .end local v322    # "patchOat4":Z
    .end local v323    # "patchOat5":Z
    .end local v324    # "patchOat6":Z
    :cond_2d03
    const/4 v0, 0x0

    move-object/16 v281, v0

    .line 2122
    .restart local v281    # "localFile2":Ljava/io/File;
    const/4 v9, 0x4

    :try_start_2d08
    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "framework"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_3bda

    .line 2123
    const/4 v9, 0x1

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "services.jar"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2d7f

    .line 2124
    new-instance v9, Ljava/io/File;

    const/4 v10, 0x1

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v9}, Lcom/chelpus/Utils;->classes_test(Ljava/io/File;)Z

    move-result v9

    if-eqz v9, :cond_2d7f

    .line 2125
    new-instance v0, Ljava/io/File;

    move-object/16 v359, v0

    const/4 v9, 0x1

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    move-object/from16 v0, v359

    invoke-direct {v0, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2126
    .restart local v359    # "serjar":Ljava/io/File;
    invoke-static/range {v359 .. v359}, Lcom/chelpus/Utils;->getDirs(Ljava/io/File;)Ljava/io/File;

    move-result-object v9

    invoke-virtual {v9}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, v359

    invoke-static {v0, v9}, Lcom/chelpus/root/utils/corepatch;->unzip(Ljava/io/File;Ljava/lang/String;)V

    .line 2127
    new-instance v0, Ljava/io/File;

    move-object/16 v262, v0

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static/range {v359 .. v359}, Lcom/chelpus/Utils;->getDirs(Ljava/io/File;)Ljava/io/File;

    move-result-object v10

    invoke-virtual {v10}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/classes.dex"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, v262

    invoke-direct {v0, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2128
    .restart local v262    # "clasdex":Ljava/io/File;
    invoke-virtual/range {v262 .. v262}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_2d7f

    .line 2129
    move-object/from16 v0, v273

    move-object/from16 v1, v262

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2134
    .end local v262    # "clasdex":Ljava/io/File;
    .end local v359    # "serjar":Ljava/io/File;
    :cond_2d7f
    const/4 v9, 0x1

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "services.odex"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2d9b

    .line 2135
    new-instance v9, Ljava/io/File;

    const/4 v10, 0x1

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v273

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2181
    :cond_2d9b
    :goto_2d9b
    invoke-virtual/range {v273 .. v273}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_2d9f
    :goto_2d9f
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_3c6c

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object/16 v275, v0

    move-object/from16 v0, v275

    check-cast v0, Ljava/io/File;

    move-object/16 v275, v0

    .line 2182
    .restart local v275    # "fp":Ljava/io/File;
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Start patch for "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual/range {v275 .. v275}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2183
    move-object/16 v281, v275

    .line 2184
    const/4 v0, 0x0

    move/16 v278, v0

    .line 2185
    .local v278, "isOat":Z
    invoke-static/range {v281 .. v281}, Lcom/chelpus/Utils;->isELFfiles(Ljava/io/File;)Z

    move-result v10

    if-eqz v10, :cond_2de0

    const/4 v0, 0x1

    move/16 v278, v0

    .line 2187
    :cond_2de0
    new-instance v10, Ljava/io/RandomAccessFile;

    const-string v15, "rw"

    move-object/from16 v0, v281

    invoke-direct {v10, v0, v15}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v177

    .line 2188
    .restart local v177    # "ChannelDex":Ljava/nio/channels/FileChannel;
    sget-object v186, Ljava/nio/channels/FileChannel$MapMode;->READ_WRITE:Ljava/nio/channels/FileChannel$MapMode;

    const-wide/16 v187, 0x0

    invoke-virtual/range {v177 .. v177}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v15

    long-to-int v10, v15

    int-to-long v0, v10

    move-wide/from16 v189, v0

    move-object/from16 v185, v177

    invoke-virtual/range {v185 .. v190}, Ljava/nio/channels/FileChannel;->map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;

    move-result-object v10

    sput-object v10, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    .line 2190
    const/4 v0, 0x0

    move/16 v318, v0

    .line 2192
    .local v318, "patch4":Z
    const/4 v0, 0x0

    move/16 v266, v0

    .local v266, "counter":I
    const/16 v199, 0x0

    .line 2193
    .local v199, "counter2":I
    move/from16 v0, v278

    if-nez v0, :cond_3ebc

    .line 2194
    const/4 v10, 0x0

    sput-object v10, Lcom/chelpus/root/utils/corepatch;->lastByteReplace:[B

    .line 2195
    const/4 v10, 0x0

    sput v10, Lcom/chelpus/root/utils/corepatch;->lastPatchPosition:I
    :try_end_2e15
    .catch Ljava/io/FileNotFoundException; {:try_start_2d08 .. :try_end_2e15} :catch_3c61
    .catch Ljava/lang/Exception; {:try_start_2d08 .. :try_end_2e15} :catch_3d95

    .line 2199
    const-wide/16 v0, 0x0

    move-wide/16 v279, v0

    .restart local v279    # "j":J
    :goto_2e1a
    :try_start_2e1a
    sget-object v10, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v10}, Ljava/nio/MappedByteBuffer;->hasRemaining()Z

    move-result v10

    if-eqz v10, :cond_3dbc

    .line 2201
    sget-object v10, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v10}, Ljava/nio/MappedByteBuffer;->position()I

    move-result v178

    .line 2202
    .restart local v178    # "curentPos":I
    sget-object v10, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v10}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v179

    .restart local v179    # "curentByte":B
    move/from16 v185, v178

    move/from16 v186, v179

    move-object/from16 v187, v225

    move-object/from16 v188, v306

    move-object/from16 v189, v255

    move-object/from16 v190, v352

    move/from16 v191, v81

    .line 2203
    invoke-static/range {v185 .. v191}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_2e6e

    .line 2204
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_2e56

    .line 2205
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2207
    :cond_2e56
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_2e6a

    .line 2208
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2210
    :cond_2e6a
    const/4 v0, 0x1

    move/16 v318, v0

    :cond_2e6e
    move/from16 v185, v178

    move/from16 v186, v179

    move-object/from16 v187, v228

    move-object/from16 v188, v309

    move-object/from16 v189, v258

    move-object/from16 v190, v355

    move/from16 v191, v81

    .line 2212
    invoke-static/range {v185 .. v191}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_2eae

    .line 2213
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_2e96

    .line 2214
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2216
    :cond_2e96
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_2eaa

    .line 2217
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2219
    :cond_2eaa
    const/4 v0, 0x1

    move/16 v318, v0

    :cond_2eae
    move/from16 v185, v178

    move/from16 v186, v179

    move-object/from16 v187, v173

    move-object/from16 v188, v174

    move-object/from16 v189, v175

    move-object/from16 v190, v176

    move/from16 v191, v81

    .line 2221
    invoke-static/range {v185 .. v191}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_2eee

    .line 2222
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_2ed6

    .line 2223
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2225
    :cond_2ed6
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_2eea

    .line 2226
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2228
    :cond_2eea
    const/4 v0, 0x1

    move/16 v318, v0

    :cond_2eee
    move/from16 v185, v178

    move/from16 v186, v179

    move-object/from16 v187, v229

    move-object/from16 v188, v310

    move-object/from16 v189, v259

    move-object/from16 v190, v356

    move/from16 v191, v81

    .line 2230
    invoke-static/range {v185 .. v191}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_2f2e

    .line 2231
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_2f16

    .line 2232
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!checkUpgradeKeySetLP\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2234
    :cond_2f16
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_2f2a

    .line 2235
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!checkUpgradeKeySetLP\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2237
    :cond_2f2a
    const/4 v0, 0x1

    move/16 v318, v0

    :cond_2f2e
    move/from16 v185, v178

    move/from16 v186, v179

    move-object/from16 v187, v230

    move-object/from16 v188, v311

    move-object/from16 v189, v260

    move-object/from16 v190, v357

    move/from16 v191, v81

    .line 2239
    invoke-static/range {v185 .. v191}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_2f6e

    .line 2240
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_2f56

    .line 2241
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!\nCM12"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2243
    :cond_2f56
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_2f6a

    .line 2244
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!\nCM12"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2246
    :cond_2f6a
    const/4 v0, 0x1

    move/16 v318, v0

    :cond_2f6e
    move/from16 v185, v178

    move/from16 v186, v179

    move-object/from16 v187, v231

    move-object/from16 v188, v312

    move-object/from16 v189, v261

    move-object/from16 v190, v358

    move/from16 v191, v266

    move/from16 v193, v105

    .line 2248
    invoke-static/range {v185 .. v193}, Lcom/chelpus/root/utils/corepatch;->applyPatchCounter(IB[B[B[B[BIIZ)Z

    move-result v10

    if-eqz v10, :cond_2fb7

    .line 2249
    move/from16 v0, v266

    add-int/lit8 v0, v0, 0x1

    move/16 v266, v0

    .line 2250
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_2f9f

    .line 2251
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!\nRemove Package from intent"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2253
    :cond_2f9f
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_2fb3

    .line 2254
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!\nRemove Package from intent"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2256
    :cond_2fb3
    const/4 v0, 0x1

    move/16 v318, v0

    :cond_2fb7
    move/from16 v193, v178

    move/from16 v194, v179

    move/from16 v201, v105

    .line 2258
    invoke-static/range {v193 .. v201}, Lcom/chelpus/root/utils/corepatch;->applyPatchCounter(IB[B[B[B[BIIZ)Z

    move-result v10

    if-eqz v10, :cond_2ff1

    .line 2259
    add-int/lit8 v199, v199, 0x1

    .line 2260
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_2fd9

    .line 2261
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!\nRemove Package from intent"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2263
    :cond_2fd9
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_2fed

    .line 2264
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!\nRemove Package from intent"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2266
    :cond_2fed
    const/4 v0, 0x1

    move/16 v318, v0

    :cond_2ff1
    move/from16 v185, v178

    move/from16 v186, v179

    move-object/from16 v187, v222

    move-object/from16 v188, v303

    move-object/from16 v189, v252

    move-object/from16 v190, v349

    move/from16 v191, v105

    .line 2268
    invoke-static/range {v185 .. v191}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_3031

    .line 2269
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_3019

    .line 2270
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!\nbuildResolveList"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2272
    :cond_3019
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_302d

    .line 2273
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!\nbuildResolveList"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2275
    :cond_302d
    const/4 v0, 0x1

    move/16 v318, v0

    :cond_3031
    move/from16 v185, v178

    move/from16 v186, v179

    move-object/from16 v187, v223

    move-object/from16 v188, v304

    move-object/from16 v189, v253

    move-object/from16 v190, v350

    move/from16 v191, v105

    .line 2277
    invoke-static/range {v185 .. v191}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_3071

    .line 2278
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_3059

    .line 2279
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!\nbuildResolveList"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2281
    :cond_3059
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_306d

    .line 2282
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!\nbuildResolveList"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2284
    :cond_306d
    const/4 v0, 0x1

    move/16 v318, v0

    :cond_3071
    move/from16 v185, v178

    move/from16 v186, v179

    move-object/from16 v187, v224

    move-object/from16 v188, v305

    move-object/from16 v189, v254

    move-object/from16 v190, v351

    move/from16 v191, v105

    .line 2286
    invoke-static/range {v185 .. v191}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_30b1

    .line 2287
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_3099

    .line 2288
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!\nbuildResolveList"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2290
    :cond_3099
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_30ad

    .line 2291
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!\nbuildResolveList"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2293
    :cond_30ad
    const/4 v0, 0x1

    move/16 v318, v0

    :cond_30b1
    move/from16 v185, v178

    move/from16 v186, v179

    move-object/from16 v187, v226

    move-object/from16 v188, v307

    move-object/from16 v189, v256

    move-object/from16 v190, v353

    move/from16 v191, v81

    .line 2296
    invoke-static/range {v185 .. v191}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_30f1

    .line 2297
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_30d9

    .line 2298
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!\nCM11"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2300
    :cond_30d9
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_30ed

    .line 2301
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!\nCM11"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2303
    :cond_30ed
    const/4 v0, 0x1

    move/16 v318, v0

    :cond_30f1
    move/from16 v185, v178

    move/from16 v186, v179

    move-object/from16 v187, v227

    move-object/from16 v188, v308

    move-object/from16 v189, v257

    move-object/from16 v190, v354

    move/from16 v191, v81

    .line 2305
    invoke-static/range {v185 .. v191}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_3131

    .line 2306
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_3119

    .line 2307
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 policy patched!\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2309
    :cond_3119
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_312d

    .line 2310
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 policy restored!\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2312
    :cond_312d
    const/4 v0, 0x1

    move/16 v318, v0

    :cond_3131
    move/from16 v165, v178

    move/from16 v166, v179

    move/from16 v171, v81

    .line 2314
    invoke-static/range {v165 .. v171}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_3169

    .line 2315
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_3151

    .line 2316
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 policy patched!\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2318
    :cond_3151
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_3165

    .line 2319
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 policy restored!\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2321
    :cond_3165
    const/4 v0, 0x1

    move/16 v318, v0

    .line 2324
    :cond_3169
    sget-object v10, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    add-int/lit8 v15, v178, 0x1

    invoke-virtual {v10, v15}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;
    :try_end_3170
    .catch Ljava/lang/Exception; {:try_start_2e1a .. :try_end_3170} :catch_3d9b
    .catch Ljava/io/FileNotFoundException; {:try_start_2e1a .. :try_end_3170} :catch_3c61

    .line 2199
    const-wide/16 v15, 0x1

    move-wide/from16 v0, v279

    add-long/2addr v0, v15

    move-wide/16 v279, v0

    goto/16 :goto_2e1a

    .line 1362
    .end local v177    # "ChannelDex":Ljava/nio/channels/FileChannel;
    .end local v178    # "curentPos":I
    .end local v179    # "curentByte":B
    .end local v199    # "counter2":I
    .end local v266    # "counter":I
    .end local v275    # "fp":Ljava/io/File;
    .end local v277    # "indexDir":Ljava/lang/String;
    .end local v278    # "isOat":Z
    .end local v279    # "j":J
    .end local v318    # "patch4":Z
    .restart local v313    # "oatForPatch":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    :cond_317a
    :try_start_317a
    sget-boolean v9, Lcom/chelpus/root/utils/corepatch;->onlyDalvik:Z

    if-eqz v9, :cond_31cb

    .line 1363
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "OnlyDalvik: add for patch "

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const/4 v15, 0x1

    move-object/from16 v0, p0

    aget-object v15, v0, v15

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1364
    new-instance v0, Ljava/io/File;

    move-object/16 v282, v0

    const/4 v9, 0x1

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    move-object/from16 v0, v282

    invoke-direct {v0, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_31aa
    .catch Ljava/io/FileNotFoundException; {:try_start_317a .. :try_end_31aa} :catch_2668
    .catch Ljava/lang/Exception; {:try_start_317a .. :try_end_31aa} :catch_3352

    .line 1365
    .end local v281    # "localFile2":Ljava/io/File;
    .local v282, "localFile2":Ljava/io/File;
    :try_start_31aa
    invoke-virtual/range {v282 .. v282}, Ljava/io/File;->exists()Z

    move-result v9

    if-nez v9, :cond_31bf

    new-instance v9, Ljava/io/FileNotFoundException;

    invoke-direct {v9}, Ljava/io/FileNotFoundException;-><init>()V

    throw v9

    .line 1890
    :catch_31b6
    move-exception v0

    move-object/16 v283, v0

    move-object/16 v281, v282

    .end local v282    # "localFile2":Ljava/io/File;
    .restart local v281    # "localFile2":Ljava/io/File;
    goto/16 :goto_266c

    .line 1366
    .end local v281    # "localFile2":Ljava/io/File;
    .restart local v282    # "localFile2":Ljava/io/File;
    :cond_31bf
    move-object/from16 v0, v273

    move-object/from16 v1, v282

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_31c6
    .catch Ljava/io/FileNotFoundException; {:try_start_31aa .. :try_end_31c6} :catch_31b6
    .catch Ljava/lang/Exception; {:try_start_31aa .. :try_end_31c6} :catch_31e6

    move-object/16 v281, v282

    .end local v282    # "localFile2":Ljava/io/File;
    .restart local v281    # "localFile2":Ljava/io/File;
    goto/16 :goto_20af

    .line 1368
    :cond_31cb
    :try_start_31cb
    new-instance v0, Ljava/io/File;

    move-object/16 v282, v0

    const/4 v9, 0x1

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    move-object/from16 v0, v282

    invoke-direct {v0, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_31da
    .catch Ljava/io/FileNotFoundException; {:try_start_31cb .. :try_end_31da} :catch_2668
    .catch Ljava/lang/Exception; {:try_start_31cb .. :try_end_31da} :catch_3352

    .line 1369
    .end local v281    # "localFile2":Ljava/io/File;
    .restart local v282    # "localFile2":Ljava/io/File;
    :try_start_31da
    invoke-virtual/range {v282 .. v282}, Ljava/io/File;->exists()Z

    move-result v9

    if-nez v9, :cond_320b

    new-instance v9, Ljava/io/FileNotFoundException;

    invoke-direct {v9}, Ljava/io/FileNotFoundException;-><init>()V

    throw v9
    :try_end_31e6
    .catch Ljava/io/FileNotFoundException; {:try_start_31da .. :try_end_31e6} :catch_31b6
    .catch Ljava/lang/Exception; {:try_start_31da .. :try_end_31e6} :catch_31e6

    .line 1893
    :catch_31e6
    move-exception v0

    move-object/16 v270, v0

    move-object/16 v281, v282

    .line 1894
    .end local v282    # "localFile2":Ljava/io/File;
    .restart local v270    # "e":Ljava/lang/Exception;
    .restart local v281    # "localFile2":Ljava/io/File;
    :goto_31ed
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Exception e"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual/range {v270 .. v270}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_2673

    .line 1370
    .end local v270    # "e":Ljava/lang/Exception;
    .end local v281    # "localFile2":Ljava/io/File;
    .restart local v282    # "localFile2":Ljava/io/File;
    :cond_320b
    :try_start_320b
    invoke-virtual/range {v282 .. v282}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v10, "system@framework@core.jar@classes.dex"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_3236

    .line 1371
    new-instance v0, Ljava/io/File;

    move-object/16 v264, v0

    const-string v9, "/system/framework/core.odex"

    move-object/from16 v0, v264

    invoke-direct {v0, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1372
    .local v264, "coreodex":Ljava/io/File;
    invoke-virtual/range {v264 .. v264}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_3236

    .line 1373
    invoke-virtual/range {v264 .. v264}, Ljava/io/File;->length()J

    move-result-wide v9

    const-wide/16 v15, 0x0

    cmp-long v9, v9, v15

    if-nez v9, :cond_3236

    invoke-virtual/range {v264 .. v264}, Ljava/io/File;->delete()Z

    .line 1376
    .end local v264    # "coreodex":Ljava/io/File;
    :cond_3236
    move-object/from16 v0, v273

    move-object/from16 v1, v282

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_323d
    .catch Ljava/io/FileNotFoundException; {:try_start_320b .. :try_end_323d} :catch_31b6
    .catch Ljava/lang/Exception; {:try_start_320b .. :try_end_323d} :catch_31e6

    move-object/16 v281, v282

    .end local v282    # "localFile2":Ljava/io/File;
    .restart local v281    # "localFile2":Ljava/io/File;
    goto/16 :goto_20af

    .line 1586
    .restart local v177    # "ChannelDex":Ljava/nio/channels/FileChannel;
    .restart local v275    # "fp":Ljava/io/File;
    .restart local v279    # "j":J
    .restart local v317    # "patch1":Z
    :catch_3242
    move-exception v0

    move-object/16 v270, v0

    .line 1587
    .restart local v270    # "e":Ljava/lang/Exception;
    :try_start_3246
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, ""

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, v270

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1752
    .end local v270    # "e":Ljava/lang/Exception;
    .end local v279    # "j":J
    :cond_3260
    :goto_3260
    invoke-virtual/range {v177 .. v177}, Ljava/nio/channels/FileChannel;->close()V

    .line 1754
    const/4 v9, 0x4

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "framework"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_384e

    .line 1755
    invoke-virtual/range {v281 .. v281}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v10, "/classes.dex"

    invoke-virtual {v9, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_32fa

    .line 1756
    const/4 v9, 0x0

    sput-boolean v9, Lcom/chelpus/root/utils/corepatch;->not_found_bytes_for_patch:Z

    .line 1757
    invoke-static/range {v281 .. v281}, Lcom/chelpus/Utils;->fixadler(Ljava/io/File;)V

    .line 1758
    move/from16 v0, v317

    if-eqz v0, :cond_3849

    .line 1759
    const/4 v9, 0x1

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, ".jar"

    const-string v15, "-patched.jar"

    invoke-virtual {v9, v10, v15}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    move-object/16 v269, v0

    .line 1760
    .local v269, "destination":Ljava/lang/String;
    const/4 v9, 0x1

    move-object/from16 v0, p0

    aget-object v0, v0, v9

    move-object/16 v360, v0

    .line 1761
    .local v360, "source":Ljava/lang/String;
    new-instance v9, Ljava/io/File;

    move-object/from16 v0, v269

    invoke-direct {v9, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 1762
    new-instance v0, Ljava/util/ArrayList;

    move-object/16 v272, v0

    invoke-direct/range {v272 .. v272}, Ljava/util/ArrayList;-><init>()V

    .line 1763
    .local v272, "files":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;>;"
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "add files"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1764
    new-instance v9, Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;

    invoke-virtual/range {v281 .. v281}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static/range {v281 .. v281}, Lcom/chelpus/Utils;->getDirs(Ljava/io/File;)Ljava/io/File;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "/"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-direct {v9, v10, v15}, Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v272

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_32e0
    .catch Ljava/io/FileNotFoundException; {:try_start_3246 .. :try_end_32e0} :catch_2668
    .catch Ljava/lang/Exception; {:try_start_3246 .. :try_end_32e0} :catch_3352

    .line 1766
    :try_start_32e0
    move-object/from16 v0, v360

    move-object/from16 v1, v269

    move-object/from16 v2, v272

    invoke-static {v0, v1, v2}, Lcom/chelpus/Utils;->addFilesToZip(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1767
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "add files finish"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1768
    new-instance v9, Ljava/io/File;

    move-object/from16 v0, v360

    invoke-direct {v9, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z
    :try_end_32fa
    .catch Ljava/lang/Exception; {:try_start_32e0 .. :try_end_32fa} :catch_3836
    .catch Ljava/io/FileNotFoundException; {:try_start_32e0 .. :try_end_32fa} :catch_2668

    .line 1779
    .end local v269    # "destination":Ljava/lang/String;
    .end local v272    # "files":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;>;"
    .end local v360    # "source":Ljava/lang/String;
    :cond_32fa
    :goto_32fa
    :try_start_32fa
    invoke-virtual/range {v281 .. v281}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v10, "/core.odex"

    invoke-virtual {v9, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3328

    .line 1780
    const/4 v9, 0x0

    move-object/from16 v0, v281

    invoke-static {v0, v9}, Lcom/chelpus/Utils;->fixadlerOdex(Ljava/io/File;Ljava/lang/String;)V

    .line 1781
    move/from16 v0, v317

    if-eqz v0, :cond_3328

    new-instance v9, Ljava/io/File;

    invoke-virtual/range {v281 .. v281}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    const-string v15, "/core.odex"

    const-string v16, "/core-patched.odex"

    move-object/from16 v0, v16

    invoke-virtual {v10, v15, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v281

    invoke-virtual {v0, v9}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 1783
    :cond_3328
    invoke-virtual/range {v281 .. v281}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v10, "/boot.oat"

    invoke-virtual {v9, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_20b3

    .line 1784
    move/from16 v0, v317

    if-eqz v0, :cond_20b3

    new-instance v9, Ljava/io/File;

    invoke-virtual/range {v281 .. v281}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    const-string v15, "/boot.oat"

    const-string v16, "/boot-patched.oat"

    move-object/from16 v0, v16

    invoke-virtual {v10, v15, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v281

    invoke-virtual {v0, v9}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    goto/16 :goto_20b3

    .line 1893
    .end local v177    # "ChannelDex":Ljava/nio/channels/FileChannel;
    .end local v275    # "fp":Ljava/io/File;
    .end local v317    # "patch1":Z
    :catch_3352
    move-exception v0

    move-object/16 v270, v0

    goto/16 :goto_31ed

    .line 1591
    .restart local v177    # "ChannelDex":Ljava/nio/channels/FileChannel;
    .restart local v275    # "fp":Ljava/io/File;
    .restart local v317    # "patch1":Z
    :cond_3358
    const/4 v0, 0x0

    move/16 v325, v0

    .restart local v325    # "patchOatUpd1":Z
    const/4 v0, 0x0

    move/16 v319, v0

    .restart local v319    # "patchOat1":Z
    const/4 v0, 0x0

    move/16 v320, v0

    .restart local v320    # "patchOat2":Z
    const/4 v0, 0x0

    move/16 v321, v0

    .restart local v321    # "patchOat3":Z
    const/4 v0, 0x0

    move/16 v322, v0

    .line 1592
    .restart local v322    # "patchOat4":Z
    sget-object v9, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    const/16 v10, 0x1018

    invoke-virtual {v9, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1593
    sget-object v9, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v9}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v9

    sget-object v10, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v10}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v10

    sget-object v15, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v15}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v15

    sget-object v16, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual/range {v16 .. v16}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v16

    move/from16 v0, v16

    invoke-static {v9, v10, v15, v0}, Lcom/chelpus/Utils;->convertFourBytesToInt(BBBB)I

    move-result v0

    move/16 v329, v0

    .line 1594
    .restart local v329    # "posit":I
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Start position:"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v329

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1595
    sget-object v9, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    move/from16 v0, v329

    invoke-virtual {v9, v0}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;
    :try_end_33b5
    .catch Ljava/io/FileNotFoundException; {:try_start_32fa .. :try_end_33b5} :catch_2668
    .catch Ljava/lang/Exception; {:try_start_32fa .. :try_end_33b5} :catch_3352

    .line 1597
    :goto_33b5
    :try_start_33b5
    sget-object v9, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v9}, Ljava/nio/MappedByteBuffer;->hasRemaining()Z

    move-result v9

    if-eqz v9, :cond_3260

    .line 1599
    sget-object v9, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v9}, Ljava/nio/MappedByteBuffer;->position()I

    move-result v178

    .line 1600
    .restart local v178    # "curentPos":I
    sget-object v9, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v9}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v179

    .restart local v179    # "curentByte":B
    move-object/from16 v180, v5

    move-object/from16 v181, v6

    move-object/from16 v182, v7

    move-object/from16 v183, v8

    .line 1601
    invoke-static/range {v178 .. v184}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_3421

    .line 1602
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_3405

    .line 1603
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "position:"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v178

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1604
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core1uni patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1606
    :cond_3405
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_3419

    .line 1607
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core1uni restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1609
    :cond_3419
    const/4 v0, 0x1

    move/16 v319, v0

    .line 1610
    const/4 v0, 0x1

    move/16 v317, v0

    :cond_3421
    move/from16 v9, v178

    move/from16 v10, v179

    move/from16 v15, v184

    .line 1613
    invoke-static/range {v9 .. v15}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_3477

    .line 1614
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_345b

    .line 1615
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "position:"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v178

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1616
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core1uni patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1618
    :cond_345b
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_346f

    .line 1619
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core1uni restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1621
    :cond_346f
    const/4 v0, 0x1

    move/16 v319, v0

    .line 1622
    const/4 v0, 0x1

    move/16 v317, v0

    :cond_3477
    move/from16 v15, v178

    move/from16 v16, v179

    move/from16 v21, v184

    .line 1625
    invoke-static/range {v15 .. v21}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_34cd

    .line 1626
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_34b1

    .line 1627
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "position:"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v178

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1628
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core11 patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1630
    :cond_34b1
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_34c5

    .line 1631
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core11 restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1633
    :cond_34c5
    const/4 v0, 0x1

    move/16 v319, v0

    .line 1634
    const/4 v0, 0x1

    move/16 v317, v0

    :cond_34cd
    move/from16 v39, v178

    move/from16 v40, v179

    move/from16 v45, v184

    .line 1637
    invoke-static/range {v39 .. v45}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_3523

    .line 1638
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_3507

    .line 1639
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "position:"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v178

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1640
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core1uni patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1642
    :cond_3507
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_351b

    .line 1643
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core1uni restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1645
    :cond_351b
    const/4 v0, 0x1

    move/16 v319, v0

    .line 1646
    const/4 v0, 0x1

    move/16 v317, v0

    :cond_3523
    move/from16 v63, v178

    move/from16 v64, v179

    move/from16 v69, v184

    .line 1649
    invoke-static/range {v63 .. v69}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_3579

    .line 1650
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_355d

    .line 1651
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "position:"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v178

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1652
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core1uni patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1654
    :cond_355d
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_3571

    .line 1655
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core1uni restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1657
    :cond_3571
    const/4 v0, 0x1

    move/16 v319, v0

    .line 1658
    const/4 v0, 0x1

    move/16 v317, v0

    :cond_3579
    move/from16 v45, v178

    move/from16 v46, v179

    move/from16 v51, v184

    .line 1661
    invoke-static/range {v45 .. v51}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_35cf

    .line 1662
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_35b3

    .line 1663
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "position:"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v178

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1664
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core11 patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1666
    :cond_35b3
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_35c7

    .line 1667
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core11 restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1669
    :cond_35c7
    const/4 v0, 0x1

    move/16 v319, v0

    .line 1670
    const/4 v0, 0x1

    move/16 v317, v0

    :cond_35cf
    move/from16 v21, v178

    move/from16 v22, v179

    move/from16 v27, v184

    .line 1673
    invoke-static/range {v21 .. v27}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_3625

    .line 1674
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_3609

    .line 1675
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core12 patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1676
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "position:"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v178

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1678
    :cond_3609
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_361d

    .line 1679
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core12 restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1681
    :cond_361d
    const/4 v0, 0x1

    move/16 v320, v0

    .line 1682
    const/4 v0, 0x1

    move/16 v317, v0

    :cond_3625
    move/from16 v51, v178

    move/from16 v52, v179

    move/from16 v57, v184

    .line 1684
    invoke-static/range {v51 .. v57}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_367b

    .line 1685
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_365f

    .line 1686
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core12 patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1687
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "position:"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v178

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1689
    :cond_365f
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_3673

    .line 1690
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core12 restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1692
    :cond_3673
    const/4 v0, 0x1

    move/16 v320, v0

    .line 1693
    const/4 v0, 0x1

    move/16 v317, v0

    :cond_367b
    move/from16 v27, v178

    move/from16 v28, v179

    .line 1695
    invoke-static/range {v27 .. v33}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_36cf

    .line 1696
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_36b3

    .line 1697
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core2 patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1698
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "position:"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v178

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1700
    :cond_36b3
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_36c7

    .line 1701
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core2 restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1703
    :cond_36c7
    const/4 v0, 0x1

    move/16 v321, v0

    .line 1704
    const/4 v0, 0x1

    move/16 v317, v0

    :cond_36cf
    move/from16 v185, v178

    move/from16 v186, v179

    move-object/from16 v187, v35

    move-object/from16 v188, v36

    move-object/from16 v189, v37

    move-object/from16 v190, v38

    move/from16 v191, v33

    .line 1706
    invoke-static/range {v185 .. v191}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_372d

    .line 1707
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_3711

    .line 1708
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core2 patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1709
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "position:"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v178

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1711
    :cond_3711
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_3725

    .line 1712
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core2 restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1714
    :cond_3725
    const/4 v0, 0x1

    move/16 v321, v0

    .line 1715
    const/4 v0, 0x1

    move/16 v317, v0

    :cond_372d
    move/from16 v57, v178

    move/from16 v58, v179

    move/from16 v63, v33

    .line 1717
    invoke-static/range {v57 .. v63}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_3783

    .line 1718
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_3767

    .line 1719
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core2 patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1720
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "position:"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v178

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1722
    :cond_3767
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_377b

    .line 1723
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core2 restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1725
    :cond_377b
    const/4 v0, 0x1

    move/16 v321, v0

    .line 1726
    const/4 v0, 0x1

    move/16 v317, v0

    :cond_3783
    move/from16 v69, v178

    move/from16 v70, v179

    move/from16 v75, v33

    .line 1728
    invoke-static/range {v69 .. v75}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v9

    if-eqz v9, :cond_37d9

    .line 1729
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "patch"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_37bd

    .line 1730
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core2 patched!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1731
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "position:"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v178

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1733
    :cond_37bd
    const/4 v9, 0x0

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "restore"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_37d1

    .line 1734
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Oat Core2 restored!\n"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1736
    :cond_37d1
    const/4 v0, 0x1

    move/16 v321, v0

    .line 1737
    const/4 v0, 0x1

    move/16 v317, v0

    .line 1739
    :cond_37d9
    if-eqz v184, :cond_37ef

    move/from16 v0, v33

    move/from16 v1, v325

    and-int/2addr v0, v1

    move v9, v0

    if-eqz v9, :cond_37ef

    move/from16 v0, v319

    if-eqz v0, :cond_37ef

    move/from16 v0, v320

    if-eqz v0, :cond_37ef

    move/from16 v0, v321

    if-nez v0, :cond_3807

    :cond_37ef
    if-eqz v184, :cond_37ff

    if-nez v33, :cond_37ff

    move/from16 v0, v325

    if-eqz v0, :cond_37ff

    move/from16 v0, v319

    if-eqz v0, :cond_37ff

    move/from16 v0, v320

    if-nez v0, :cond_3807

    :cond_37ff
    if-nez v184, :cond_380d

    if-eqz v33, :cond_380d

    move/from16 v0, v321

    if-eqz v0, :cond_380d

    .line 1742
    :cond_3807
    const/4 v0, 0x1

    move/16 v317, v0

    .line 1743
    goto/16 :goto_3260

    .line 1745
    :cond_380d
    sget-object v9, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    add-int/lit8 v10, v178, 0x1

    invoke-virtual {v9, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;
    :try_end_3814
    .catch Ljava/lang/Exception; {:try_start_33b5 .. :try_end_3814} :catch_3816
    .catch Ljava/io/FileNotFoundException; {:try_start_33b5 .. :try_end_3814} :catch_2668

    goto/16 :goto_33b5

    .line 1747
    .end local v178    # "curentPos":I
    .end local v179    # "curentByte":B
    :catch_3816
    move-exception v0

    move-object/16 v270, v0

    .line 1748
    .restart local v270    # "e":Ljava/lang/Exception;
    :try_start_381a
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, ""

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, v270

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_3260

    .line 1770
    .end local v270    # "e":Ljava/lang/Exception;
    .end local v319    # "patchOat1":Z
    .end local v320    # "patchOat2":Z
    .end local v321    # "patchOat3":Z
    .end local v322    # "patchOat4":Z
    .end local v325    # "patchOatUpd1":Z
    .end local v329    # "posit":I
    .restart local v269    # "destination":Ljava/lang/String;
    .restart local v272    # "files":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;>;"
    .restart local v360    # "source":Ljava/lang/String;
    :catch_3836
    move-exception v0

    move-object/16 v270, v0

    .line 1771
    .restart local v270    # "e":Ljava/lang/Exception;
    invoke-virtual/range {v270 .. v270}, Ljava/lang/Exception;->printStackTrace()V

    .line 1773
    new-instance v9, Ljava/io/File;

    move-object/from16 v0, v269

    invoke-direct {v9, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    goto/16 :goto_32fa

    .line 1776
    .end local v269    # "destination":Ljava/lang/String;
    .end local v270    # "e":Ljava/lang/Exception;
    .end local v272    # "files":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;>;"
    .end local v360    # "source":Ljava/lang/String;
    :cond_3849
    const/4 v9, 0x1

    sput-boolean v9, Lcom/chelpus/root/utils/corepatch;->not_found_bytes_for_patch:Z

    goto/16 :goto_32fa

    .line 1787
    :cond_384e
    invoke-virtual/range {v281 .. v281}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v10, "/classes.dex"

    invoke-virtual {v9, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3a1a

    .line 1788
    invoke-static/range {v281 .. v281}, Lcom/chelpus/Utils;->fixadler(Ljava/io/File;)V

    .line 1791
    :goto_385d
    const/4 v9, 0x4

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "ART"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_3a4f

    .line 1793
    invoke-virtual/range {v281 .. v281}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v10, "/classes.dex"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_20b3

    move/from16 v0, v317

    if-eqz v0, :cond_20b3

    .line 1794
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "start"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1795
    const-string v0, "/system/framework/core-libart.backup"

    move-object/16 v269, v0

    .line 1796
    .restart local v269    # "destination":Ljava/lang/String;
    const-string v0, "/system/framework/core-libart.jar"

    move-object/16 v360, v0

    .line 1797
    .restart local v360    # "source":Ljava/lang/String;
    const/4 v9, 0x1

    const/4 v10, 0x0

    move-object/from16 v0, v360

    move-object/from16 v1, v269

    invoke-static {v0, v1, v9, v10}, Lcom/chelpus/Utils;->copyFile(Ljava/lang/String;Ljava/lang/String;ZZ)Z

    move-result v9

    if-eqz v9, :cond_3a3c

    .line 1798
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "good space"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1799
    new-instance v9, Ljava/io/File;

    move-object/from16 v0, v269

    invoke-direct {v9, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 1800
    new-instance v0, Ljava/util/ArrayList;

    move-object/16 v272, v0

    invoke-direct/range {v272 .. v272}, Ljava/util/ArrayList;-><init>()V

    .line 1801
    .restart local v272    # "files":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;>;"
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "add files"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1802
    new-instance v9, Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;

    const-string v10, "/data/app/classes.dex"

    const-string v15, "/data/app/"

    invoke-direct {v9, v10, v15}, Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v272

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_38c5
    .catch Ljava/io/FileNotFoundException; {:try_start_381a .. :try_end_38c5} :catch_2668
    .catch Ljava/lang/Exception; {:try_start_381a .. :try_end_38c5} :catch_3352

    .line 1804
    :try_start_38c5
    const-string v9, "/system/framework/core-libart.jar"

    const-string v10, "/system/framework/core-libart.backup"

    move-object/from16 v0, v272

    invoke-static {v9, v10, v0}, Lcom/chelpus/Utils;->addFilesToZip(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1806
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "add files finish"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1807
    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v15, "chmod"

    aput-object v15, v9, v10

    const/4 v10, 0x1

    const-string v15, "0644"

    aput-object v15, v9, v10

    const/4 v10, 0x2

    const-string v15, "/system/framework/core-libart.backup"

    aput-object v15, v9, v10

    invoke-static {v9}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 1808
    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v15, "chown"

    aput-object v15, v9, v10

    const/4 v10, 0x1

    const-string v15, "0:0"

    aput-object v15, v9, v10

    const/4 v10, 0x2

    const-string v15, "/system/framework/core-libart.backup"

    aput-object v15, v9, v10

    invoke-static {v9}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 1809
    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v15, "chmod"

    aput-object v15, v9, v10

    const/4 v10, 0x1

    const-string v15, "0.0"

    aput-object v15, v9, v10

    const/4 v10, 0x2

    const-string v15, "/system/framework/core-libart.backup"

    aput-object v15, v9, v10

    invoke-static {v9}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 1811
    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v15, "rm"

    aput-object v15, v9, v10

    const/4 v10, 0x1

    const-string v15, "/system/framework/core-libart.jar"

    aput-object v15, v9, v10

    invoke-static {v9}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 1812
    new-instance v9, Ljava/io/File;

    const-string v10, "/system/framework/core-libart.jar"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_393b

    .line 1813
    new-instance v9, Ljava/io/File;

    const-string v10, "/system/framework/core-libart.jar"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 1814
    :cond_393b
    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v15, "mv"

    aput-object v15, v9, v10

    const/4 v10, 0x1

    const-string v15, "/system/framework/core.backup"

    aput-object v15, v9, v10

    const/4 v10, 0x2

    const-string v15, "/system/framework/core-libart.jar"

    aput-object v15, v9, v10

    invoke-static {v9}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 1815
    new-instance v9, Ljava/io/File;

    const-string v10, "/system/framework/core-libart.jar"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v9

    if-nez v9, :cond_396e

    .line 1816
    new-instance v9, Ljava/io/File;

    const-string v10, "/system/framework/core-libart.backup"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v10, Ljava/io/File;

    const-string v15, "/system/framework/core-libart.jar"

    invoke-direct {v10, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v10}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 1817
    :cond_396e
    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v15, "chmod"

    aput-object v15, v9, v10

    const/4 v10, 0x1

    const-string v15, "0644"

    aput-object v15, v9, v10

    const/4 v10, 0x2

    const-string v15, "/system/framework/core-libart.jar"

    aput-object v15, v9, v10

    invoke-static {v9}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 1818
    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v15, "chown"

    aput-object v15, v9, v10

    const/4 v10, 0x1

    const-string v15, "0:0"

    aput-object v15, v9, v10

    const/4 v10, 0x2

    const-string v15, "/system/framework/core-libart.jar"

    aput-object v15, v9, v10

    invoke-static {v9}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 1819
    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v15, "chmod"

    aput-object v15, v9, v10

    const/4 v10, 0x1

    const-string v15, "0.0"

    aput-object v15, v9, v10

    const/4 v10, 0x2

    const-string v15, "/system/framework/core-libart.jar"

    aput-object v15, v9, v10

    invoke-static {v9}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 1820
    const-string v9, "/system/framework/core-libart.jar"

    invoke-static {v9}, Lcom/chelpus/Utils;->getFileDalvikCache(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    move-object/16 v268, v0

    .line 1821
    .local v268, "dalv":Ljava/io/File;
    move-object/from16 v0, v268

    if-eqz v0, :cond_39d5

    .line 1822
    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v15, "rm"

    aput-object v15, v9, v10

    const/4 v10, 0x1

    invoke-virtual/range {v268 .. v268}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v9, v10

    invoke-static {v9}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 1823
    invoke-virtual/range {v268 .. v268}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_39d5

    invoke-virtual/range {v268 .. v268}, Ljava/io/File;->delete()Z

    .line 1826
    :cond_39d5
    new-instance v9, Ljava/io/File;

    const-string v10, "/data/dalvik-cache/arm/system@framework@arm@boot.oat"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 1827
    new-instance v9, Ljava/io/File;

    const-string v10, "/data/dalvik-cache/arm/system@framework@arm@boot.art"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 1828
    new-instance v9, Ljava/io/File;

    const-string v10, "/data/dalvik-cache/arm64/system@framework@arm@boot.oat"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 1829
    new-instance v9, Ljava/io/File;

    const-string v10, "/data/dalvik-cache/arm64/system@framework@arm@boot.art"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 1830
    new-instance v9, Ljava/io/File;

    const-string v10, "/data/dalvik-cache/x86/system@framework@arm@boot.oat"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 1831
    new-instance v9, Ljava/io/File;

    const-string v10, "/data/dalvik-cache/x86/system@framework@arm@boot.art"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z
    :try_end_3a11
    .catch Ljava/lang/Exception; {:try_start_38c5 .. :try_end_3a11} :catch_3a23
    .catch Ljava/io/FileNotFoundException; {:try_start_38c5 .. :try_end_3a11} :catch_2668

    .line 1837
    .end local v268    # "dalv":Ljava/io/File;
    :goto_3a11
    :try_start_3a11
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "finish"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_20b3

    .line 1789
    .end local v269    # "destination":Ljava/lang/String;
    .end local v272    # "files":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;>;"
    .end local v360    # "source":Ljava/lang/String;
    :cond_3a1a
    const-string v9, "/system/framework/core.jar"

    move-object/from16 v0, v281

    invoke-static {v0, v9}, Lcom/chelpus/Utils;->fixadlerOdex(Ljava/io/File;Ljava/lang/String;)V

    goto/16 :goto_385d

    .line 1832
    .restart local v269    # "destination":Ljava/lang/String;
    .restart local v272    # "files":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;>;"
    .restart local v360    # "source":Ljava/lang/String;
    :catch_3a23
    move-exception v0

    move-object/16 v270, v0

    .line 1833
    .restart local v270    # "e":Ljava/lang/Exception;
    invoke-virtual/range {v270 .. v270}, Ljava/lang/Exception;->printStackTrace()V

    .line 1834
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "LuckyPatcher: odex not equal lenght patched! Not enougth space in /system/!"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1835
    new-instance v9, Ljava/io/File;

    const-string v10, "/system/framework/core.backup"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    goto :goto_3a11

    .line 1840
    .end local v270    # "e":Ljava/lang/Exception;
    .end local v272    # "files":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;>;"
    :cond_3a3c
    new-instance v9, Ljava/io/File;

    move-object/from16 v0, v269

    invoke-direct {v9, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 1843
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "LuckyPatcher: odex not equal lenght patched! Not enougth space in /system/!"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_20b3

    .line 1849
    .end local v269    # "destination":Ljava/lang/String;
    .end local v360    # "source":Ljava/lang/String;
    :cond_3a4f
    sget-boolean v9, Lcom/chelpus/root/utils/corepatch;->onlyDalvik:Z

    if-nez v9, :cond_20b3

    .line 1850
    invoke-virtual/range {v281 .. v281}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v10, "system@framework@core.jar@classes.dex"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_3acd

    move/from16 v0, v317

    if-eqz v0, :cond_3acd

    .line 1851
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "LuckyPatcher: dalvik-cache patched! "

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1852
    new-instance v0, Ljava/io/File;

    move-object/16 v265, v0

    const-string v9, "/system/framework/core.patched"

    move-object/from16 v0, v265

    invoke-direct {v0, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1853
    .local v265, "corepatched":Ljava/io/File;
    const-string v0, "/system/framework/core.patched"

    move-object/16 v269, v0

    .line 1854
    .restart local v269    # "destination":Ljava/lang/String;
    invoke-virtual/range {v281 .. v281}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    move-object/16 v360, v0

    .line 1855
    .restart local v360    # "source":Ljava/lang/String;
    const/4 v9, 0x1

    const/4 v10, 0x0

    move-object/from16 v0, v360

    move-object/from16 v1, v269

    invoke-static {v0, v1, v9, v10}, Lcom/chelpus/Utils;->copyFile(Ljava/lang/String;Ljava/lang/String;ZZ)Z

    move-result v9

    if-eqz v9, :cond_3b01

    .line 1856
    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v15, "chmod"

    aput-object v15, v9, v10

    const/4 v10, 0x1

    const-string v15, "0644"

    aput-object v15, v9, v10

    const/4 v10, 0x2

    move-object/from16 v0, v269

    aput-object v0, v9, v10

    invoke-static {v9}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 1857
    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v15, "chown"

    aput-object v15, v9, v10

    const/4 v10, 0x1

    const-string v15, "0.0"

    aput-object v15, v9, v10

    const/4 v10, 0x2

    move-object/from16 v0, v269

    aput-object v0, v9, v10

    invoke-static {v9}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 1858
    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v15, "chown"

    aput-object v15, v9, v10

    const/4 v10, 0x1

    const-string v15, "0:0"

    aput-object v15, v9, v10

    const/4 v10, 0x2

    move-object/from16 v0, v269

    aput-object v0, v9, v10

    invoke-static {v9}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 1879
    .end local v265    # "corepatched":Ljava/io/File;
    .end local v269    # "destination":Ljava/lang/String;
    .end local v360    # "source":Ljava/lang/String;
    :cond_3acd
    :goto_3acd
    new-instance v0, Ljava/io/File;

    move-object/16 v263, v0

    const-string v9, "/system/framework/core.patched"

    move-object/from16 v0, v263

    invoke-direct {v0, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1880
    .local v263, "core":Ljava/io/File;
    invoke-virtual/range {v263 .. v263}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_3ae6

    .line 1881
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "LuckyPatcher: root found core.patched! "

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1882
    :cond_3ae6
    new-instance v0, Ljava/io/File;

    move-object/16 v263, v0

    .end local v263    # "core":Ljava/io/File;
    const-string v9, "/system/framework/core.odex"

    move-object/from16 v0, v263

    invoke-direct {v0, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1883
    .restart local v263    # "core":Ljava/io/File;
    invoke-virtual/range {v263 .. v263}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_20b3

    .line 1884
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "LuckyPatcher: root found core.odex! "

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_20b3

    .line 1860
    .end local v263    # "core":Ljava/io/File;
    .restart local v265    # "corepatched":Ljava/io/File;
    .restart local v269    # "destination":Ljava/lang/String;
    .restart local v360    # "source":Ljava/lang/String;
    :cond_3b01
    invoke-virtual/range {v265 .. v265}, Ljava/io/File;->delete()Z

    .line 1861
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "LuckyPatcher: not space to system for odex core.jar! "

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1862
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "LuckyPatcher: odex not equal lenght patched! Not enougth space in /system/!"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_3b12
    .catch Ljava/io/FileNotFoundException; {:try_start_3a11 .. :try_end_3b12} :catch_2668
    .catch Ljava/lang/Exception; {:try_start_3a11 .. :try_end_3b12} :catch_3352

    goto :goto_3acd

    .line 1928
    .end local v265    # "corepatched":Ljava/io/File;
    .end local v269    # "destination":Ljava/lang/String;
    .end local v275    # "fp":Ljava/io/File;
    .end local v281    # "localFile2":Ljava/io/File;
    .end local v313    # "oatForPatch":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    .end local v317    # "patch1":Z
    .end local v360    # "source":Ljava/lang/String;
    .restart local v274    # "for_patch":Ljava/io/File;
    .restart local v276    # "good_odex":Z
    .restart local v277    # "indexDir":Ljava/lang/String;
    .restart local v319    # "patchOat1":Z
    .restart local v320    # "patchOat2":Z
    .restart local v321    # "patchOat3":Z
    .restart local v322    # "patchOat4":Z
    .restart local v323    # "patchOat5":Z
    .restart local v324    # "patchOat6":Z
    :cond_3b13
    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v15, "chmod"

    aput-object v15, v9, v10

    const/4 v10, 0x1

    const-string v15, "644"

    aput-object v15, v9, v10

    const/4 v10, 0x2

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "/system/framework"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, v277

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "/services.odex"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v9, v10

    invoke-static {v9}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 1929
    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v15, "chown"

    aput-object v15, v9, v10

    const/4 v10, 0x1

    const-string v15, "0:0"

    aput-object v15, v9, v10

    const/4 v10, 0x2

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "/system/framework"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, v277

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "/services.odex"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v9, v10

    invoke-static {v9}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 1930
    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v15, "chown"

    aput-object v15, v9, v10

    const/4 v10, 0x1

    const-string v15, "0.0"

    aput-object v15, v9, v10

    const/4 v10, 0x2

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "/system/framework"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, v277

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "/services.odex"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v9, v10

    invoke-static {v9}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    goto/16 :goto_2844

    .line 2091
    .restart local v178    # "curentPos":I
    .restart local v179    # "curentByte":B
    .restart local v329    # "posit":I
    :cond_3b9f
    :try_start_3b9f
    sget-object v9, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    add-int/lit8 v10, v178, 0x1

    invoke-virtual {v9, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;
    :try_end_3ba6
    .catch Ljava/lang/Exception; {:try_start_3b9f .. :try_end_3ba6} :catch_3ba8
    .catch Ljava/io/IOException; {:try_start_3b9f .. :try_end_3ba6} :catch_3bc8

    goto/16 :goto_28b2

    .line 2093
    .end local v178    # "curentPos":I
    .end local v179    # "curentByte":B
    :catch_3ba8
    move-exception v0

    move-object/16 v270, v0

    .line 2094
    .restart local v270    # "e":Ljava/lang/Exception;
    :try_start_3bac
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, ""

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, v270

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_3bc6
    .catch Ljava/io/IOException; {:try_start_3bac .. :try_end_3bc6} :catch_3bc8

    goto/16 :goto_2c8e

    .line 2098
    .end local v270    # "e":Ljava/lang/Exception;
    .end local v329    # "posit":I
    :catch_3bc8
    move-exception v0

    move-object/16 v270, v0

    .line 2099
    .local v270, "e":Ljava/io/IOException;
    invoke-virtual/range {v270 .. v270}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_2c91

    .line 2102
    .end local v270    # "e":Ljava/io/IOException;
    :cond_3bd1
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "LuckyPatcher: odex not equal lenght patched! Not enougth space in /system/!"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_2c91

    .line 2138
    .end local v177    # "ChannelDex":Ljava/nio/channels/FileChannel;
    .end local v274    # "for_patch":Ljava/io/File;
    .end local v276    # "good_odex":Z
    .end local v319    # "patchOat1":Z
    .end local v320    # "patchOat2":Z
    .end local v321    # "patchOat3":Z
    .end local v322    # "patchOat4":Z
    .end local v323    # "patchOat5":Z
    .end local v324    # "patchOat6":Z
    .restart local v281    # "localFile2":Ljava/io/File;
    :cond_3bda
    const/4 v9, 0x4

    :try_start_3bdb
    move-object/from16 v0, p0

    aget-object v9, v0, v9

    const-string v10, "ART"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_3c70

    .line 2140
    new-instance v9, Ljava/io/File;

    const-string v10, "/system/framework/services.odex"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_3c18

    new-instance v9, Ljava/io/File;

    const-string v10, "/system/framework/services.odex"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->length()J

    move-result-wide v9

    const-wide/16 v15, 0x0

    cmp-long v9, v9, v15

    if-eqz v9, :cond_3c18

    .line 2141
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Add services.odex for patch"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2142
    new-instance v9, Ljava/io/File;

    const-string v10, "/system/framework/services.odex"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v273

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2145
    :cond_3c18
    new-instance v9, Ljava/io/File;

    const-string v10, "/system/framework/services.jar"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v9}, Lcom/chelpus/Utils;->classes_test(Ljava/io/File;)Z

    move-result v9

    if-eqz v9, :cond_2d9b

    .line 2146
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "services.jar contain classes,dex"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2147
    new-instance v0, Ljava/io/File;

    move-object/16 v359, v0

    const-string v9, "/system/framework/services.jar"

    move-object/from16 v0, v359

    invoke-direct {v0, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2148
    .restart local v359    # "serjar":Ljava/io/File;
    const-string v9, "/data/app"

    move-object/from16 v0, v359

    invoke-static {v0, v9}, Lcom/chelpus/root/utils/corepatch;->unzip(Ljava/io/File;Ljava/lang/String;)V

    .line 2149
    new-instance v0, Ljava/io/File;

    move-object/16 v262, v0

    const-string v9, "/data/app/classes.dex"

    move-object/from16 v0, v262

    invoke-direct {v0, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2150
    .restart local v262    # "clasdex":Ljava/io/File;
    invoke-virtual/range {v262 .. v262}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_2d9b

    .line 2151
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Add classes.dex for patch"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2152
    move-object/from16 v0, v273

    move-object/from16 v1, v262

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_3c5f
    .catch Ljava/io/FileNotFoundException; {:try_start_3bdb .. :try_end_3c5f} :catch_3c61
    .catch Ljava/lang/Exception; {:try_start_3bdb .. :try_end_3c5f} :catch_3d95

    goto/16 :goto_2d9b

    .line 2768
    .end local v262    # "clasdex":Ljava/io/File;
    .end local v359    # "serjar":Ljava/io/File;
    :catch_3c61
    move-exception v0

    move-object/16 v283, v0

    .line 2769
    .restart local v283    # "localFileNotFoundException":Ljava/io/FileNotFoundException;
    :goto_3c65
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Error: services.odex not found!\n\nPlease Odex services.jar and try again!"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2778
    .end local v273    # "filesToPatch":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    .end local v277    # "indexDir":Ljava/lang/String;
    .end local v281    # "localFile2":Ljava/io/File;
    .end local v283    # "localFileNotFoundException":Ljava/io/FileNotFoundException;
    :cond_3c6c
    :goto_3c6c
    invoke-static {}, Lcom/chelpus/Utils;->exitFromRootJava()V

    .line 2779
    return-void

    .line 2157
    .restart local v273    # "filesToPatch":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    .restart local v277    # "indexDir":Ljava/lang/String;
    .restart local v281    # "localFile2":Ljava/io/File;
    :cond_3c70
    :try_start_3c70
    sget-boolean v9, Lcom/chelpus/root/utils/corepatch;->onlyDalvik:Z

    if-eqz v9, :cond_3ca3

    .line 2158
    new-instance v0, Ljava/io/File;

    move-object/16 v282, v0

    const/4 v9, 0x2

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    move-object/from16 v0, v282

    invoke-direct {v0, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_3c83
    .catch Ljava/io/FileNotFoundException; {:try_start_3c70 .. :try_end_3c83} :catch_3c61
    .catch Ljava/lang/Exception; {:try_start_3c70 .. :try_end_3c83} :catch_3d95

    .line 2159
    .end local v281    # "localFile2":Ljava/io/File;
    .restart local v282    # "localFile2":Ljava/io/File;
    :try_start_3c83
    invoke-virtual/range {v282 .. v282}, Ljava/io/File;->exists()Z

    move-result v9

    if-nez v9, :cond_3c97

    new-instance v9, Ljava/io/FileNotFoundException;

    invoke-direct {v9}, Ljava/io/FileNotFoundException;-><init>()V

    throw v9

    .line 2768
    :catch_3c8f
    move-exception v0

    move-object/16 v283, v0

    move-object/16 v281, v282

    .end local v282    # "localFile2":Ljava/io/File;
    .restart local v281    # "localFile2":Ljava/io/File;
    goto :goto_3c65

    .line 2160
    .end local v281    # "localFile2":Ljava/io/File;
    .restart local v282    # "localFile2":Ljava/io/File;
    :cond_3c97
    move-object/from16 v0, v273

    move-object/from16 v1, v282

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_3c9e
    .catch Ljava/io/FileNotFoundException; {:try_start_3c83 .. :try_end_3c9e} :catch_3c8f
    .catch Ljava/lang/Exception; {:try_start_3c83 .. :try_end_3c9e} :catch_3cdb

    move-object/16 v281, v282

    .end local v282    # "localFile2":Ljava/io/File;
    .restart local v281    # "localFile2":Ljava/io/File;
    goto/16 :goto_2d9b

    .line 2162
    :cond_3ca3
    :try_start_3ca3
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Vhodjashij file "

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const/4 v15, 0x2

    move-object/from16 v0, p0

    aget-object v15, v0, v15

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2163
    new-instance v0, Ljava/io/File;

    move-object/16 v282, v0

    const/4 v9, 0x2

    move-object/from16 v0, p0

    aget-object v9, v0, v9

    move-object/from16 v0, v282

    invoke-direct {v0, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_3ccf
    .catch Ljava/io/FileNotFoundException; {:try_start_3ca3 .. :try_end_3ccf} :catch_3c61
    .catch Ljava/lang/Exception; {:try_start_3ca3 .. :try_end_3ccf} :catch_3d95

    .line 2164
    .end local v281    # "localFile2":Ljava/io/File;
    .restart local v282    # "localFile2":Ljava/io/File;
    :try_start_3ccf
    invoke-virtual/range {v282 .. v282}, Ljava/io/File;->exists()Z

    move-result v9

    if-nez v9, :cond_3d00

    new-instance v9, Ljava/io/FileNotFoundException;

    invoke-direct {v9}, Ljava/io/FileNotFoundException;-><init>()V

    throw v9
    :try_end_3cdb
    .catch Ljava/io/FileNotFoundException; {:try_start_3ccf .. :try_end_3cdb} :catch_3c8f
    .catch Ljava/lang/Exception; {:try_start_3ccf .. :try_end_3cdb} :catch_3cdb

    .line 2771
    :catch_3cdb
    move-exception v0

    move-object/16 v270, v0

    move-object/16 v281, v282

    .line 2772
    .end local v282    # "localFile2":Ljava/io/File;
    .local v270, "e":Ljava/lang/Exception;
    .restart local v281    # "localFile2":Ljava/io/File;
    :goto_3ce2
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Exception e"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual/range {v270 .. v270}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_3c6c

    .line 2165
    .end local v270    # "e":Ljava/lang/Exception;
    .end local v281    # "localFile2":Ljava/io/File;
    .restart local v282    # "localFile2":Ljava/io/File;
    :cond_3d00
    :try_start_3d00
    invoke-virtual/range {v282 .. v282}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v10, "system@framework@services.jar@classes.dex"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_4974

    .line 2166
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Vhodjashij file byl dalvick-cache "

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const/4 v15, 0x2

    move-object/from16 v0, p0

    aget-object v15, v0, v15

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2167
    new-instance v0, Ljava/io/File;

    move-object/16 v359, v0

    const-string v9, "/system/framework/services.jar"

    move-object/from16 v0, v359

    invoke-direct {v0, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2168
    .restart local v359    # "serjar":Ljava/io/File;
    const-string v9, "/data/app"

    move-object/from16 v0, v359

    invoke-static {v0, v9}, Lcom/chelpus/root/utils/corepatch;->unzip(Ljava/io/File;Ljava/lang/String;)V

    .line 2169
    new-instance v0, Ljava/io/File;

    move-object/16 v262, v0

    const-string v9, "/data/app/classes.dex"

    move-object/from16 v0, v262

    invoke-direct {v0, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2170
    .restart local v262    # "clasdex":Ljava/io/File;
    invoke-virtual/range {v262 .. v262}, Ljava/io/File;->exists()Z
    :try_end_3d4b
    .catch Ljava/io/FileNotFoundException; {:try_start_3d00 .. :try_end_3d4b} :catch_3c8f
    .catch Ljava/lang/Exception; {:try_start_3d00 .. :try_end_3d4b} :catch_3cdb

    move-result v9

    if-eqz v9, :cond_496f

    move-object/16 v281, v262

    .line 2171
    .end local v282    # "localFile2":Ljava/io/File;
    .restart local v281    # "localFile2":Ljava/io/File;
    :goto_3d51
    :try_start_3d51
    new-instance v0, Ljava/io/File;

    move-object/16 v264, v0

    const-string v9, "/system/framework/services.odex"

    move-object/from16 v0, v264

    invoke-direct {v0, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2172
    .restart local v264    # "coreodex":Ljava/io/File;
    invoke-virtual/range {v264 .. v264}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_3d70

    .line 2173
    invoke-virtual/range {v264 .. v264}, Ljava/io/File;->length()J

    move-result-wide v9

    const-wide/16 v15, 0x0

    cmp-long v9, v9, v15

    if-nez v9, :cond_3d70

    invoke-virtual/range {v264 .. v264}, Ljava/io/File;->delete()Z

    .line 2176
    .end local v262    # "clasdex":Ljava/io/File;
    .end local v264    # "coreodex":Ljava/io/File;
    .end local v359    # "serjar":Ljava/io/File;
    :cond_3d70
    :goto_3d70
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Add file for patch "

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual/range {v281 .. v281}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2177
    move-object/from16 v0, v273

    move-object/from16 v1, v281

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2d9b

    .line 2771
    :catch_3d95
    move-exception v0

    move-object/16 v270, v0

    goto/16 :goto_3ce2

    .line 2327
    .restart local v177    # "ChannelDex":Ljava/nio/channels/FileChannel;
    .restart local v199    # "counter2":I
    .restart local v266    # "counter":I
    .restart local v275    # "fp":Ljava/io/File;
    .restart local v278    # "isOat":Z
    .restart local v279    # "j":J
    .restart local v318    # "patch4":Z
    :catch_3d9b
    move-exception v0

    move-object/16 v270, v0

    .line 2328
    .restart local v270    # "e":Ljava/lang/Exception;
    invoke-virtual/range {v270 .. v270}, Ljava/lang/Exception;->printStackTrace()V

    .line 2329
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, ""

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, v270

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2331
    .end local v270    # "e":Ljava/lang/Exception;
    :cond_3dbc
    sget v10, Lcom/chelpus/root/utils/corepatch;->lastPatchPosition:I

    if-lez v10, :cond_3dd7

    sget-object v10, Lcom/chelpus/root/utils/corepatch;->lastByteReplace:[B

    if-eqz v10, :cond_3dd7

    .line 2332
    sget-object v10, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    sget v15, Lcom/chelpus/root/utils/corepatch;->lastPatchPosition:I

    invoke-virtual {v10, v15}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2333
    sget-object v10, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    sget-object v15, Lcom/chelpus/root/utils/corepatch;->lastByteReplace:[B

    invoke-virtual {v10, v15}, Ljava/nio/MappedByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 2334
    sget-object v10, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v10}, Ljava/nio/MappedByteBuffer;->force()Ljava/nio/MappedByteBuffer;

    .line 2495
    .end local v279    # "j":J
    :cond_3dd7
    :goto_3dd7
    invoke-virtual/range {v177 .. v177}, Ljava/nio/channels/FileChannel;->close()V

    .line 2497
    const/4 v10, 0x4

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "framework"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_431e

    .line 2498
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Rebuild file!"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2499
    invoke-virtual/range {v281 .. v281}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v10

    const-string v15, "/classes.dex"

    invoke-virtual {v10, v15}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_3e84

    .line 2500
    invoke-static/range {v281 .. v281}, Lcom/chelpus/Utils;->fixadler(Ljava/io/File;)V

    .line 2501
    move/from16 v0, v318

    if-eqz v0, :cond_4319

    .line 2502
    const/4 v10, 0x0

    sput-boolean v10, Lcom/chelpus/root/utils/corepatch;->not_found_bytes_for_patch:Z

    .line 2503
    const/4 v10, 0x1

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "/services.jar"

    const-string v16, "/services-patched.jar"

    move-object/from16 v0, v16

    invoke-virtual {v10, v15, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    move-object/16 v269, v0

    .line 2504
    .restart local v269    # "destination":Ljava/lang/String;
    const/4 v10, 0x1

    move-object/from16 v0, p0

    aget-object v0, v0, v10

    move-object/16 v360, v0

    .line 2505
    .restart local v360    # "source":Ljava/lang/String;
    new-instance v10, Ljava/io/File;

    move-object/from16 v0, v269

    invoke-direct {v10, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->delete()Z

    .line 2506
    new-instance v0, Ljava/util/ArrayList;

    move-object/16 v272, v0

    invoke-direct/range {v272 .. v272}, Ljava/util/ArrayList;-><init>()V

    .line 2507
    .restart local v272    # "files":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;>;"
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "add files"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2508
    new-instance v10, Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;

    invoke-virtual/range {v281 .. v281}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v15

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static/range {v281 .. v281}, Lcom/chelpus/Utils;->getDirs(Ljava/io/File;)Ljava/io/File;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v21, "/"

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-direct {v10, v15, v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v272

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_3e6a
    .catch Ljava/io/FileNotFoundException; {:try_start_3d51 .. :try_end_3e6a} :catch_3c61
    .catch Ljava/lang/Exception; {:try_start_3d51 .. :try_end_3e6a} :catch_3d95

    .line 2510
    :try_start_3e6a
    move-object/from16 v0, v360

    move-object/from16 v1, v269

    move-object/from16 v2, v272

    invoke-static {v0, v1, v2}, Lcom/chelpus/Utils;->addFilesToZip(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2511
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "add files finish"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2512
    new-instance v10, Ljava/io/File;

    move-object/from16 v0, v360

    invoke-direct {v10, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->delete()Z
    :try_end_3e84
    .catch Ljava/lang/Exception; {:try_start_3e6a .. :try_end_3e84} :catch_4306
    .catch Ljava/io/FileNotFoundException; {:try_start_3e6a .. :try_end_3e84} :catch_3c61

    .line 2523
    .end local v269    # "destination":Ljava/lang/String;
    .end local v272    # "files":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;>;"
    .end local v360    # "source":Ljava/lang/String;
    :cond_3e84
    :goto_3e84
    :try_start_3e84
    invoke-virtual/range {v281 .. v281}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v10

    const-string v15, "/services.odex"

    invoke-virtual {v10, v15}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_2d9f

    .line 2524
    invoke-static/range {v281 .. v281}, Lcom/chelpus/Utils;->isELFfiles(Ljava/io/File;)Z

    move-result v10

    if-nez v10, :cond_3e9c

    .line 2525
    const/4 v10, 0x0

    move-object/from16 v0, v281

    invoke-static {v0, v10}, Lcom/chelpus/Utils;->fixadlerOdex(Ljava/io/File;Ljava/lang/String;)V

    .line 2527
    :cond_3e9c
    move/from16 v0, v318

    if-eqz v0, :cond_2d9f

    .line 2528
    new-instance v10, Ljava/io/File;

    invoke-virtual/range {v281 .. v281}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v15

    const-string v16, "/services.odex"

    const-string v21, "/services-patched.odex"

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-virtual {v15, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v15

    invoke-direct {v10, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v281

    invoke-virtual {v0, v10}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    goto/16 :goto_2d9f

    .line 2337
    :cond_3ebc
    const/4 v0, 0x0

    move/16 v319, v0

    .restart local v319    # "patchOat1":Z
    const/4 v0, 0x0

    move/16 v320, v0

    .restart local v320    # "patchOat2":Z
    const/4 v0, 0x0

    move/16 v321, v0

    .restart local v321    # "patchOat3":Z
    const/4 v0, 0x0

    move/16 v322, v0

    .restart local v322    # "patchOat4":Z
    const/4 v0, 0x0

    move/16 v323, v0

    .restart local v323    # "patchOat5":Z
    const/4 v0, 0x0

    move/16 v324, v0

    .line 2338
    .restart local v324    # "patchOat6":Z
    sget-object v10, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    const/16 v15, 0x1018

    invoke-virtual {v10, v15}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2339
    sget-object v10, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v10}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v10

    sget-object v15, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v15}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v15

    sget-object v16, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual/range {v16 .. v16}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v16

    sget-object v21, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual/range {v21 .. v21}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v21

    move/from16 v0, v16

    move/from16 v1, v21

    invoke-static {v10, v15, v0, v1}, Lcom/chelpus/Utils;->convertFourBytesToInt(BBBB)I

    move-result v0

    move/16 v329, v0

    .line 2340
    .restart local v329    # "posit":I
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Start position:"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move/from16 v0, v329

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2341
    sget-object v10, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    move/from16 v0, v329

    invoke-virtual {v10, v0}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;
    :try_end_3f1f
    .catch Ljava/io/FileNotFoundException; {:try_start_3e84 .. :try_end_3f1f} :catch_3c61
    .catch Ljava/lang/Exception; {:try_start_3e84 .. :try_end_3f1f} :catch_3d95

    .line 2344
    :goto_3f1f
    :try_start_3f1f
    sget-object v10, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v10}, Ljava/nio/MappedByteBuffer;->hasRemaining()Z

    move-result v10

    if-eqz v10, :cond_3dd7

    .line 2346
    sget-object v10, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v10}, Ljava/nio/MappedByteBuffer;->position()I

    move-result v178

    .line 2347
    .restart local v178    # "curentPos":I
    sget-object v10, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v10}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v179

    .restart local v179    # "curentByte":B
    move/from16 v75, v178

    move/from16 v76, v179

    .line 2348
    invoke-static/range {v75 .. v81}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_3f6d

    .line 2349
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_3f51

    .line 2350
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2352
    :cond_3f51
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_3f65

    .line 2353
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2355
    :cond_3f65
    const/4 v0, 0x1

    move/16 v319, v0

    .line 2356
    const/4 v0, 0x1

    move/16 v318, v0

    :cond_3f6d
    move/from16 v185, v178

    move/from16 v186, v179

    move-object/from16 v187, v83

    move-object/from16 v188, v84

    move-object/from16 v189, v85

    move-object/from16 v190, v86

    move/from16 v191, v81

    .line 2358
    invoke-static/range {v185 .. v191}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_3fb1

    .line 2359
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_3f95

    .line 2360
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2362
    :cond_3f95
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_3fa9

    .line 2363
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2365
    :cond_3fa9
    const/4 v0, 0x1

    move/16 v319, v0

    .line 2366
    const/4 v0, 0x1

    move/16 v318, v0

    :cond_3fb1
    move/from16 v87, v178

    move/from16 v88, v179

    move/from16 v93, v81

    .line 2368
    invoke-static/range {v87 .. v93}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_3fed

    .line 2369
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_3fd1

    .line 2370
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!InstallLocationPolice\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2372
    :cond_3fd1
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_3fe5

    .line 2373
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!InstallLocationPolice\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2375
    :cond_3fe5
    const/4 v0, 0x1

    move/16 v320, v0

    const/4 v0, 0x1

    move/16 v318, v0

    :cond_3fed
    move/from16 v123, v178

    move/from16 v124, v179

    move/from16 v129, v81

    .line 2377
    invoke-static/range {v123 .. v129}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_4029

    .line 2378
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_400d

    .line 2379
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!InstallLocationPolice\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2381
    :cond_400d
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_4021

    .line 2382
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!InstallLocationPolice\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2384
    :cond_4021
    const/4 v0, 0x1

    move/16 v320, v0

    const/4 v0, 0x1

    move/16 v318, v0

    :cond_4029
    move/from16 v93, v178

    move/from16 v94, v179

    move/from16 v99, v81

    .line 2386
    invoke-static/range {v93 .. v99}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_4065

    .line 2387
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_4049

    .line 2388
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!checkUpgradeKeySetLP\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2390
    :cond_4049
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_405d

    .line 2391
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!checkUpgradeKeySetLP\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2393
    :cond_405d
    const/4 v0, 0x1

    move/16 v321, v0

    const/4 v0, 0x1

    move/16 v318, v0

    :cond_4065
    move/from16 v99, v178

    move/from16 v100, v179

    .line 2395
    invoke-static/range {v99 .. v105}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_409f

    .line 2396
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_4083

    .line 2397
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!queryIntentServices\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2399
    :cond_4083
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_4097

    .line 2400
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!queryIntentServices\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2402
    :cond_4097
    const/4 v0, 0x1

    move/16 v322, v0

    const/4 v0, 0x1

    move/16 v318, v0

    :cond_409f
    move/from16 v185, v178

    move/from16 v186, v179

    move-object/from16 v187, v107

    move-object/from16 v188, v108

    move-object/from16 v189, v109

    move-object/from16 v190, v110

    move/from16 v191, v105

    .line 2404
    invoke-static/range {v185 .. v191}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_40e3

    .line 2405
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_40c7

    .line 2406
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!buildResolveList\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2408
    :cond_40c7
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_40db

    .line 2409
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!buildResolveList\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2411
    :cond_40db
    const/4 v0, 0x1

    move/16 v323, v0

    const/4 v0, 0x1

    move/16 v318, v0

    :cond_40e3
    move/from16 v111, v178

    move/from16 v112, v179

    move/from16 v117, v81

    .line 2413
    invoke-static/range {v111 .. v117}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_411f

    .line 2414
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_4103

    .line 2415
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!FixForCM\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2417
    :cond_4103
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_4117

    .line 2418
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!FixForCM\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2420
    :cond_4117
    const/4 v0, 0x1

    move/16 v324, v0

    const/4 v0, 0x1

    move/16 v318, v0

    :cond_411f
    move/from16 v117, v178

    move/from16 v118, v179

    move/from16 v123, v81

    .line 2422
    invoke-static/range {v117 .. v123}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_415b

    .line 2423
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_413f

    .line 2424
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!FixForCM\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2426
    :cond_413f
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_4153

    .line 2427
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!FixForCM\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2429
    :cond_4153
    const/4 v0, 0x1

    move/16 v324, v0

    const/4 v0, 0x1

    move/16 v318, v0

    :cond_415b
    move/from16 v129, v178

    move/from16 v130, v179

    move/from16 v135, v81

    .line 2431
    invoke-static/range {v129 .. v135}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_4197

    .line 2432
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_417b

    .line 2433
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2435
    :cond_417b
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_418f

    .line 2436
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2438
    :cond_418f
    const/4 v0, 0x1

    move/16 v319, v0

    const/4 v0, 0x1

    move/16 v318, v0

    :cond_4197
    move/from16 v141, v178

    move/from16 v142, v179

    move/from16 v147, v81

    .line 2440
    invoke-static/range {v141 .. v147}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_41d3

    .line 2441
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_41b7

    .line 2442
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2444
    :cond_41b7
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_41cb

    .line 2445
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2447
    :cond_41cb
    const/4 v0, 0x1

    move/16 v319, v0

    const/4 v0, 0x1

    move/16 v318, v0

    :cond_41d3
    move/from16 v147, v178

    move/from16 v148, v179

    move/from16 v153, v81

    .line 2449
    invoke-static/range {v147 .. v153}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_420f

    .line 2450
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_41f3

    .line 2451
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2453
    :cond_41f3
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_4207

    .line 2454
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2456
    :cond_4207
    const/4 v0, 0x1

    move/16 v319, v0

    const/4 v0, 0x1

    move/16 v318, v0

    :cond_420f
    move/from16 v135, v178

    move/from16 v136, v179

    move/from16 v141, v81

    .line 2458
    invoke-static/range {v135 .. v141}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_424b

    .line 2459
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_422f

    .line 2460
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!InstallLocationPolice\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2462
    :cond_422f
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_4243

    .line 2463
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!InstallLocationPolice\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2465
    :cond_4243
    const/4 v0, 0x1

    move/16 v320, v0

    const/4 v0, 0x1

    move/16 v318, v0

    :cond_424b
    move/from16 v153, v178

    move/from16 v154, v179

    move/from16 v159, v81

    .line 2467
    invoke-static/range {v153 .. v159}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_4287

    .line 2468
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_426b

    .line 2469
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!InstallLocationPolice\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2471
    :cond_426b
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_427f

    .line 2472
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!InstallLocationPolice\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2474
    :cond_427f
    const/4 v0, 0x1

    move/16 v320, v0

    const/4 v0, 0x1

    move/16 v318, v0

    :cond_4287
    move/from16 v159, v178

    move/from16 v160, v179

    move/from16 v165, v81

    .line 2476
    invoke-static/range {v159 .. v165}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_42c3

    .line 2477
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_42a7

    .line 2478
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!InstallLocationPolice\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2480
    :cond_42a7
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_42bb

    .line 2481
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!InstallLocationPolice\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2483
    :cond_42bb
    const/4 v0, 0x1

    move/16 v320, v0

    const/4 v0, 0x1

    move/16 v318, v0

    .line 2485
    :cond_42c3
    if-eqz v81, :cond_42dd

    move/from16 v0, v319

    if-eqz v0, :cond_42dd

    move/from16 v0, v320

    if-eqz v0, :cond_42dd

    move/from16 v0, v321

    if-eqz v0, :cond_42dd

    move/from16 v0, v322

    if-eqz v0, :cond_42dd

    move/from16 v0, v323

    if-eqz v0, :cond_42dd

    move/from16 v0, v324

    if-nez v0, :cond_3dd7

    .line 2489
    :cond_42dd
    sget-object v10, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    add-int/lit8 v15, v178, 0x1

    invoke-virtual {v10, v15}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;
    :try_end_42e4
    .catch Ljava/lang/Exception; {:try_start_3f1f .. :try_end_42e4} :catch_42e6
    .catch Ljava/io/FileNotFoundException; {:try_start_3f1f .. :try_end_42e4} :catch_3c61

    goto/16 :goto_3f1f

    .line 2491
    .end local v178    # "curentPos":I
    .end local v179    # "curentByte":B
    :catch_42e6
    move-exception v0

    move-object/16 v270, v0

    .line 2492
    .restart local v270    # "e":Ljava/lang/Exception;
    :try_start_42ea
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, ""

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, v270

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_3dd7

    .line 2514
    .end local v270    # "e":Ljava/lang/Exception;
    .end local v319    # "patchOat1":Z
    .end local v320    # "patchOat2":Z
    .end local v321    # "patchOat3":Z
    .end local v322    # "patchOat4":Z
    .end local v323    # "patchOat5":Z
    .end local v324    # "patchOat6":Z
    .end local v329    # "posit":I
    .restart local v269    # "destination":Ljava/lang/String;
    .restart local v272    # "files":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;>;"
    .restart local v360    # "source":Ljava/lang/String;
    :catch_4306
    move-exception v0

    move-object/16 v270, v0

    .line 2515
    .restart local v270    # "e":Ljava/lang/Exception;
    invoke-virtual/range {v270 .. v270}, Ljava/lang/Exception;->printStackTrace()V

    .line 2517
    new-instance v10, Ljava/io/File;

    move-object/from16 v0, v269

    invoke-direct {v10, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->delete()Z

    goto/16 :goto_3e84

    .line 2520
    .end local v269    # "destination":Ljava/lang/String;
    .end local v270    # "e":Ljava/lang/Exception;
    .end local v272    # "files":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;>;"
    .end local v360    # "source":Ljava/lang/String;
    :cond_4319
    const/4 v10, 0x1

    sput-boolean v10, Lcom/chelpus/root/utils/corepatch;->not_found_bytes_for_patch:Z

    goto/16 :goto_3e84

    .line 2531
    :cond_431e
    invoke-virtual/range {v281 .. v281}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v10

    const-string v15, "/classes.dex"

    invoke-virtual {v10, v15}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_4537

    invoke-static/range {v281 .. v281}, Lcom/chelpus/Utils;->fixadler(Ljava/io/File;)V

    .line 2534
    :goto_432d
    invoke-virtual/range {v281 .. v281}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v10

    const-string v15, "system@framework@services.jar@classes.dex"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_4344

    move/from16 v0, v318

    if-eqz v0, :cond_4344

    .line 2536
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "LuckyPatcher: dalvik-cache patched! "

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2541
    :cond_4344
    invoke-virtual/range {v281 .. v281}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v10

    const-string v15, "/classes.dex"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_44e8

    move/from16 v0, v318

    if-eqz v0, :cond_44e8

    .line 2542
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "start"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2543
    const-string v0, "/system/framework/services.backup"

    move-object/16 v269, v0

    .line 2544
    .restart local v269    # "destination":Ljava/lang/String;
    const-string v0, "/system/framework/services.jar"

    move-object/16 v360, v0

    .line 2545
    .restart local v360    # "source":Ljava/lang/String;
    const/4 v10, 0x1

    const/4 v15, 0x0

    move-object/from16 v0, v360

    move-object/from16 v1, v269

    invoke-static {v0, v1, v10, v15}, Lcom/chelpus/Utils;->copyFile(Ljava/lang/String;Ljava/lang/String;ZZ)Z

    move-result v10

    if-eqz v10, :cond_4561

    .line 2546
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "good space"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2547
    new-instance v10, Ljava/io/File;

    move-object/from16 v0, v269

    invoke-direct {v10, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->delete()Z

    .line 2548
    new-instance v0, Ljava/util/ArrayList;

    move-object/16 v272, v0

    invoke-direct/range {v272 .. v272}, Ljava/util/ArrayList;-><init>()V

    .line 2549
    .restart local v272    # "files":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;>;"
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "add files"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2550
    new-instance v10, Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;

    const-string v15, "/data/app/classes.dex"

    const-string v16, "/data/app/"

    move-object/from16 v0, v16

    invoke-direct {v10, v15, v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v272

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_43a1
    .catch Ljava/io/FileNotFoundException; {:try_start_42ea .. :try_end_43a1} :catch_3c61
    .catch Ljava/lang/Exception; {:try_start_42ea .. :try_end_43a1} :catch_3d95

    .line 2552
    :try_start_43a1
    const-string v10, "/system/framework/services.jar"

    const-string v15, "/system/framework/services.backup"

    move-object/from16 v0, v272

    invoke-static {v10, v15, v0}, Lcom/chelpus/Utils;->addFilesToZip(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2553
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "add files finish"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2554
    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/String;

    const/4 v15, 0x0

    const-string v16, "chmod"

    aput-object v16, v10, v15

    const/4 v15, 0x1

    const-string v16, "0644"

    aput-object v16, v10, v15

    const/4 v15, 0x2

    const-string v16, "/system/framework/services.backup"

    aput-object v16, v10, v15

    invoke-static {v10}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 2555
    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/String;

    const/4 v15, 0x0

    const-string v16, "chown"

    aput-object v16, v10, v15

    const/4 v15, 0x1

    const-string v16, "0:0"

    aput-object v16, v10, v15

    const/4 v15, 0x2

    const-string v16, "/system/framework/services.backup"

    aput-object v16, v10, v15

    invoke-static {v10}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 2556
    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/String;

    const/4 v15, 0x0

    const-string v16, "chmod"

    aput-object v16, v10, v15

    const/4 v15, 0x1

    const-string v16, "0.0"

    aput-object v16, v10, v15

    const/4 v15, 0x2

    const-string v16, "/system/framework/services.backup"

    aput-object v16, v10, v15

    invoke-static {v10}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 2557
    new-instance v0, Ljava/io/File;

    move-object/16 v264, v0

    const-string v10, "/system/framework/services.odex"

    move-object/from16 v0, v264

    invoke-direct {v0, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2559
    .restart local v264    # "coreodex":Ljava/io/File;
    const-string v10, "/system/framework/services.jar"

    invoke-static {v10}, Lcom/chelpus/Utils;->getFileDalvikCache(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    move-object/16 v267, v0

    .line 2560
    .local v267, "d":Ljava/io/File;
    invoke-virtual/range {v264 .. v264}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_4419

    .line 2561
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "fix odex na osnove rebuild services"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2562
    const-string v10, "/system/framework/services.backup"

    move-object/from16 v0, v264

    invoke-static {v0, v10}, Lcom/chelpus/Utils;->fixadlerOdex(Ljava/io/File;Ljava/lang/String;)V

    .line 2565
    :cond_4419
    new-instance v10, Ljava/io/File;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v16, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->toolfilesdir:Ljava/lang/String;

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "/ClearDalvik.on"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-direct {v10, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->createNewFile()Z

    .line 2566
    new-instance v10, Ljava/io/File;

    const-string v15, "/system/framework/services.jar"

    invoke-direct {v10, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->delete()Z

    .line 2567
    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/String;

    const/4 v15, 0x0

    const-string v16, "rm"

    aput-object v16, v10, v15

    const/4 v15, 0x1

    const-string v16, "/system/framework/services.jar"

    aput-object v16, v10, v15

    invoke-static {v10}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 2568
    new-instance v10, Ljava/io/File;

    const-string v15, "/system/framework/services.backup"

    invoke-direct {v10, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v15, Ljava/io/File;

    const-string v16, "/system/framework/services.jar"

    invoke-direct/range {v15 .. v16}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v15}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 2569
    new-instance v10, Ljava/io/File;

    const-string v15, "/system/framework/services.jar"

    invoke-direct {v10, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v10

    if-nez v10, :cond_4483

    .line 2570
    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/String;

    const/4 v15, 0x0

    const-string v16, "mv"

    aput-object v16, v10, v15

    const/4 v15, 0x1

    const-string v16, "/system/framework/services.backup"

    aput-object v16, v10, v15

    const/4 v15, 0x2

    const-string v16, "/system/framework/services.jar"

    aput-object v16, v10, v15

    invoke-static {v10}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V
    :try_end_4483
    .catch Ljava/lang/Exception; {:try_start_43a1 .. :try_end_4483} :catch_4548
    .catch Ljava/io/FileNotFoundException; {:try_start_43a1 .. :try_end_4483} :catch_3c61

    .line 2572
    :cond_4483
    move-object/from16 v0, v267

    if-eqz v0, :cond_44a2

    .line 2573
    const/4 v10, 0x2

    :try_start_4488
    new-array v10, v10, [Ljava/lang/String;

    const/4 v15, 0x0

    const-string v16, "rm"

    aput-object v16, v10, v15

    const/4 v15, 0x1

    invoke-virtual/range {v267 .. v267}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v10, v15

    invoke-static {v10}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 2574
    invoke-virtual/range {v267 .. v267}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_44a2

    invoke-virtual/range {v267 .. v267}, Ljava/io/File;->delete()Z

    .line 2576
    :cond_44a2
    new-instance v10, Ljava/io/File;

    const-string v15, "/data/dalvik-cache/arm/system@framework@arm@boot.oat"

    invoke-direct {v10, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->delete()Z

    .line 2577
    new-instance v10, Ljava/io/File;

    const-string v15, "/data/dalvik-cache/arm/system@framework@arm@boot.art"

    invoke-direct {v10, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->delete()Z

    .line 2578
    new-instance v10, Ljava/io/File;

    const-string v15, "/data/dalvik-cache/arm64/system@framework@arm@boot.oat"

    invoke-direct {v10, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->delete()Z

    .line 2579
    new-instance v10, Ljava/io/File;

    const-string v15, "/data/dalvik-cache/arm64/system@framework@arm@boot.art"

    invoke-direct {v10, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->delete()Z

    .line 2580
    new-instance v10, Ljava/io/File;

    const-string v15, "/data/dalvik-cache/x86/system@framework@arm@boot.oat"

    invoke-direct {v10, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->delete()Z

    .line 2581
    new-instance v10, Ljava/io/File;

    const-string v15, "/data/dalvik-cache/x86/system@framework@arm@boot.art"

    invoke-direct {v10, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->delete()Z
    :try_end_44de
    .catch Ljava/lang/Exception; {:try_start_4488 .. :try_end_44de} :catch_4540
    .catch Ljava/io/FileNotFoundException; {:try_start_4488 .. :try_end_44de} :catch_3c61

    .line 2586
    :goto_44de
    :try_start_44de
    invoke-static {}, Lcom/chelpus/Utils;->clear_dalvik_cache()V
    :try_end_44e1
    .catch Ljava/lang/Exception; {:try_start_44de .. :try_end_44e1} :catch_4548
    .catch Ljava/io/FileNotFoundException; {:try_start_44de .. :try_end_44e1} :catch_3c61

    .line 2594
    .end local v264    # "coreodex":Ljava/io/File;
    .end local v267    # "d":Ljava/io/File;
    :goto_44e1
    :try_start_44e1
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "finish"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2750
    .end local v269    # "destination":Ljava/lang/String;
    .end local v272    # "files":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;>;"
    .end local v360    # "source":Ljava/lang/String;
    :cond_44e8
    :goto_44e8
    sget-boolean v10, Lcom/chelpus/root/utils/corepatch;->onlyDalvik:Z

    if-nez v10, :cond_2d9f

    .line 2751
    new-instance v0, Ljava/io/File;

    move-object/16 v263, v0

    const-string v10, "/system/framework/services.patched"

    move-object/from16 v0, v263

    invoke-direct {v0, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2752
    .restart local v263    # "core":Ljava/io/File;
    invoke-virtual/range {v263 .. v263}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_4505

    .line 2753
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "LuckyPatcher: root found services.patched! "

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2754
    :cond_4505
    new-instance v0, Ljava/io/File;

    move-object/16 v263, v0

    .end local v263    # "core":Ljava/io/File;
    const-string v10, "/system/framework/services.odex"

    move-object/from16 v0, v263

    invoke-direct {v0, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2755
    .restart local v263    # "core":Ljava/io/File;
    invoke-virtual/range {v263 .. v263}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_451e

    .line 2756
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "LuckyPatcher: root found services.odex! "

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2757
    :cond_451e
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_2d9f

    .line 2758
    new-instance v10, Ljava/io/File;

    const-string v15, "/system/framework/patch3.done"

    invoke-direct {v10, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->delete()Z

    goto/16 :goto_2d9f

    .line 2532
    .end local v263    # "core":Ljava/io/File;
    :cond_4537
    const-string v10, "/system/framework/services.jar"

    move-object/from16 v0, v281

    invoke-static {v0, v10}, Lcom/chelpus/Utils;->fixadlerOdex(Ljava/io/File;Ljava/lang/String;)V
    :try_end_453e
    .catch Ljava/io/FileNotFoundException; {:try_start_44e1 .. :try_end_453e} :catch_3c61
    .catch Ljava/lang/Exception; {:try_start_44e1 .. :try_end_453e} :catch_3d95

    goto/16 :goto_432d

    .line 2582
    .restart local v264    # "coreodex":Ljava/io/File;
    .restart local v267    # "d":Ljava/io/File;
    .restart local v269    # "destination":Ljava/lang/String;
    .restart local v272    # "files":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;>;"
    .restart local v360    # "source":Ljava/lang/String;
    :catch_4540
    move-exception v0

    move-object/16 v270, v0

    .line 2583
    .restart local v270    # "e":Ljava/lang/Exception;
    :try_start_4544
    invoke-virtual/range {v270 .. v270}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4547
    .catch Ljava/lang/Exception; {:try_start_4544 .. :try_end_4547} :catch_4548
    .catch Ljava/io/FileNotFoundException; {:try_start_4544 .. :try_end_4547} :catch_3c61

    goto :goto_44de

    .line 2588
    .end local v264    # "coreodex":Ljava/io/File;
    .end local v267    # "d":Ljava/io/File;
    .end local v270    # "e":Ljava/lang/Exception;
    :catch_4548
    move-exception v0

    move-object/16 v270, v0

    .line 2589
    .restart local v270    # "e":Ljava/lang/Exception;
    :try_start_454c
    invoke-virtual/range {v270 .. v270}, Ljava/lang/Exception;->printStackTrace()V

    .line 2590
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "LuckyPatcher: odex not equal lenght patched! Not enougth space in /system/!"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2591
    new-instance v10, Ljava/io/File;

    const-string v15, "/system/framework/services.backup"

    invoke-direct {v10, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->delete()Z

    goto :goto_44e1

    .line 2597
    .end local v270    # "e":Ljava/lang/Exception;
    .end local v272    # "files":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;>;"
    :cond_4561
    new-instance v10, Ljava/io/File;

    move-object/from16 v0, v269

    invoke-direct {v10, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->delete()Z

    .line 2598
    new-instance v0, Ljava/io/File;

    move-object/16 v282, v0

    const/4 v10, 0x2

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    move-object/from16 v0, v282

    invoke-direct {v0, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_457a
    .catch Ljava/io/FileNotFoundException; {:try_start_454c .. :try_end_457a} :catch_3c61
    .catch Ljava/lang/Exception; {:try_start_454c .. :try_end_457a} :catch_3d95

    .line 2599
    .end local v281    # "localFile2":Ljava/io/File;
    .restart local v282    # "localFile2":Ljava/io/File;
    :try_start_457a
    new-instance v10, Ljava/io/RandomAccessFile;

    const-string v15, "rw"

    move-object/from16 v0, v282

    invoke-direct {v10, v0, v15}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v177

    .line 2600
    sget-object v186, Ljava/nio/channels/FileChannel$MapMode;->READ_WRITE:Ljava/nio/channels/FileChannel$MapMode;

    const-wide/16 v187, 0x0

    invoke-virtual/range {v177 .. v177}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v15

    long-to-int v10, v15

    int-to-long v0, v10

    move-wide/from16 v189, v0

    move-object/from16 v185, v177

    invoke-virtual/range {v185 .. v190}, Ljava/nio/channels/FileChannel;->map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;

    move-result-object v10

    sput-object v10, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    .line 2601
    const/4 v0, 0x0

    move/16 v266, v0

    const/16 v199, 0x0

    .line 2602
    const/4 v10, 0x0

    sput-object v10, Lcom/chelpus/root/utils/corepatch;->lastByteReplace:[B

    .line 2603
    const/4 v10, 0x0

    sput v10, Lcom/chelpus/root/utils/corepatch;->lastPatchPosition:I
    :try_end_45a7
    .catch Ljava/io/FileNotFoundException; {:try_start_457a .. :try_end_45a7} :catch_3c8f
    .catch Ljava/lang/Exception; {:try_start_457a .. :try_end_45a7} :catch_3cdb

    .line 2604
    const/4 v0, 0x0

    move/16 v318, v0

    .line 2606
    const-wide/16 v0, 0x0

    move-wide/16 v279, v0

    .restart local v279    # "j":J
    :goto_45b0
    :try_start_45b0
    sget-object v10, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v10}, Ljava/nio/MappedByteBuffer;->hasRemaining()Z

    move-result v10

    if-eqz v10, :cond_492e

    .line 2608
    sget-object v10, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v10}, Ljava/nio/MappedByteBuffer;->position()I

    move-result v178

    .line 2609
    .restart local v178    # "curentPos":I
    sget-object v10, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v10}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v179

    .restart local v179    # "curentByte":B
    move/from16 v185, v178

    move/from16 v186, v179

    move-object/from16 v187, v225

    move-object/from16 v188, v306

    move-object/from16 v189, v255

    move-object/from16 v190, v352

    move/from16 v191, v81

    .line 2610
    invoke-static/range {v185 .. v191}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_4604

    .line 2611
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_45ec

    .line 2612
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2614
    :cond_45ec
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_4600

    .line 2615
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2617
    :cond_4600
    const/4 v0, 0x1

    move/16 v318, v0

    :cond_4604
    move/from16 v185, v178

    move/from16 v186, v179

    move-object/from16 v187, v228

    move-object/from16 v188, v309

    move-object/from16 v189, v258

    move-object/from16 v190, v355

    move/from16 v191, v81

    .line 2619
    invoke-static/range {v185 .. v191}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_4644

    .line 2620
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_462c

    .line 2621
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2623
    :cond_462c
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_4640

    .line 2624
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2626
    :cond_4640
    const/4 v0, 0x1

    move/16 v318, v0

    :cond_4644
    move/from16 v185, v178

    move/from16 v186, v179

    move-object/from16 v187, v173

    move-object/from16 v188, v174

    move-object/from16 v189, v175

    move-object/from16 v190, v176

    move/from16 v191, v81

    .line 2628
    invoke-static/range {v185 .. v191}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_4684

    .line 2629
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_466c

    .line 2630
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2632
    :cond_466c
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_4680

    .line 2633
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2635
    :cond_4680
    const/4 v0, 0x1

    move/16 v318, v0

    :cond_4684
    move/from16 v185, v178

    move/from16 v186, v179

    move-object/from16 v187, v229

    move-object/from16 v188, v310

    move-object/from16 v189, v259

    move-object/from16 v190, v356

    move/from16 v191, v81

    .line 2637
    invoke-static/range {v185 .. v191}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_46c4

    .line 2638
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_46ac

    .line 2639
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!checkUpgradeKeySetLP\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2641
    :cond_46ac
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_46c0

    .line 2642
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!checkUpgradeKeySetLP\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2644
    :cond_46c0
    const/4 v0, 0x1

    move/16 v318, v0

    :cond_46c4
    move/from16 v185, v178

    move/from16 v186, v179

    move-object/from16 v187, v230

    move-object/from16 v188, v311

    move-object/from16 v189, v260

    move-object/from16 v190, v357

    move/from16 v191, v81

    .line 2646
    invoke-static/range {v185 .. v191}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_4704

    .line 2647
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_46ec

    .line 2648
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!\nCM12"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2650
    :cond_46ec
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_4700

    .line 2651
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!\nCM12"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2653
    :cond_4700
    const/4 v0, 0x1

    move/16 v318, v0

    :cond_4704
    move/from16 v185, v178

    move/from16 v186, v179

    move-object/from16 v187, v231

    move-object/from16 v188, v312

    move-object/from16 v189, v261

    move-object/from16 v190, v358

    move/from16 v191, v266

    move/from16 v193, v105

    .line 2655
    invoke-static/range {v185 .. v193}, Lcom/chelpus/root/utils/corepatch;->applyPatchCounter(IB[B[B[B[BIIZ)Z

    move-result v10

    if-eqz v10, :cond_474d

    .line 2656
    move/from16 v0, v266

    add-int/lit8 v0, v0, 0x1

    move/16 v266, v0

    .line 2657
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_4735

    .line 2658
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!\nRemove Package from intent"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2660
    :cond_4735
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_4749

    .line 2661
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!\nRemove Package from intent"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2663
    :cond_4749
    const/4 v0, 0x1

    move/16 v318, v0

    :cond_474d
    move/from16 v193, v178

    move/from16 v194, v179

    move/from16 v201, v105

    .line 2665
    invoke-static/range {v193 .. v201}, Lcom/chelpus/root/utils/corepatch;->applyPatchCounter(IB[B[B[B[BIIZ)Z

    move-result v10

    if-eqz v10, :cond_4787

    .line 2666
    add-int/lit8 v199, v199, 0x1

    .line 2667
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_476f

    .line 2668
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!\nRemove Package from intent"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2670
    :cond_476f
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_4783

    .line 2671
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!\nRemove Package from intent"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2673
    :cond_4783
    const/4 v0, 0x1

    move/16 v318, v0

    :cond_4787
    move/from16 v185, v178

    move/from16 v186, v179

    move-object/from16 v187, v222

    move-object/from16 v188, v303

    move-object/from16 v189, v252

    move-object/from16 v190, v349

    move/from16 v191, v105

    .line 2675
    invoke-static/range {v185 .. v191}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_47c7

    .line 2676
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_47af

    .line 2677
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!\nbuildResolveList"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2679
    :cond_47af
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_47c3

    .line 2680
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!\nbuildResolveList"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2682
    :cond_47c3
    const/4 v0, 0x1

    move/16 v318, v0

    :cond_47c7
    move/from16 v185, v178

    move/from16 v186, v179

    move-object/from16 v187, v223

    move-object/from16 v188, v304

    move-object/from16 v189, v253

    move-object/from16 v190, v350

    move/from16 v191, v105

    .line 2684
    invoke-static/range {v185 .. v191}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_4807

    .line 2685
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_47ef

    .line 2686
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!\nbuildResolveList"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2688
    :cond_47ef
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_4803

    .line 2689
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!\nbuildResolveList"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2691
    :cond_4803
    const/4 v0, 0x1

    move/16 v318, v0

    :cond_4807
    move/from16 v185, v178

    move/from16 v186, v179

    move-object/from16 v187, v224

    move-object/from16 v188, v305

    move-object/from16 v189, v254

    move-object/from16 v190, v351

    move/from16 v191, v105

    .line 2693
    invoke-static/range {v185 .. v191}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_4847

    .line 2694
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_482f

    .line 2695
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!\nbuildResolveList"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2697
    :cond_482f
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_4843

    .line 2698
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!\nbuildResolveList"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2700
    :cond_4843
    const/4 v0, 0x1

    move/16 v318, v0

    :cond_4847
    move/from16 v185, v178

    move/from16 v186, v179

    move-object/from16 v187, v226

    move-object/from16 v188, v307

    move-object/from16 v189, v256

    move-object/from16 v190, v353

    move/from16 v191, v81

    .line 2702
    invoke-static/range {v185 .. v191}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_4887

    .line 2703
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_486f

    .line 2704
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 patched!\nCM11"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2706
    :cond_486f
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_4883

    .line 2707
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 restored!\nCM11"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2709
    :cond_4883
    const/4 v0, 0x1

    move/16 v318, v0

    :cond_4887
    move/from16 v185, v178

    move/from16 v186, v179

    move-object/from16 v187, v227

    move-object/from16 v188, v308

    move-object/from16 v189, v257

    move-object/from16 v190, v354

    move/from16 v191, v81

    .line 2711
    invoke-static/range {v185 .. v191}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_48c7

    .line 2712
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_48af

    .line 2713
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 policy patched!\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2715
    :cond_48af
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_48c3

    .line 2716
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 policy restored!\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2718
    :cond_48c3
    const/4 v0, 0x1

    move/16 v318, v0

    :cond_48c7
    move/from16 v165, v178

    move/from16 v166, v179

    move/from16 v171, v81

    .line 2720
    invoke-static/range {v165 .. v171}, Lcom/chelpus/root/utils/corepatch;->applyPatch(IB[B[B[B[BZ)Z

    move-result v10

    if-eqz v10, :cond_48ff

    .line 2721
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "patch"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_48e7

    .line 2722
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 policy patched!\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2724
    :cond_48e7
    const/4 v10, 0x0

    move-object/from16 v0, p0

    aget-object v10, v0, v10

    const-string v15, "restore"

    invoke-virtual {v10, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_48fb

    .line 2725
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "Core4 policy restored!\n"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2727
    :cond_48fb
    const/4 v0, 0x1

    move/16 v318, v0

    .line 2730
    :cond_48ff
    sget-object v10, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    add-int/lit8 v15, v178, 0x1

    invoke-virtual {v10, v15}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;
    :try_end_4906
    .catch Ljava/lang/Exception; {:try_start_45b0 .. :try_end_4906} :catch_4910
    .catch Ljava/io/FileNotFoundException; {:try_start_45b0 .. :try_end_4906} :catch_3c8f

    .line 2606
    const-wide/16 v15, 0x1

    move-wide/from16 v0, v279

    add-long/2addr v0, v15

    move-wide/16 v279, v0

    goto/16 :goto_45b0

    .line 2732
    .end local v178    # "curentPos":I
    .end local v179    # "curentByte":B
    :catch_4910
    move-exception v0

    move-object/16 v270, v0

    .line 2733
    .restart local v270    # "e":Ljava/lang/Exception;
    :try_start_4914
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, ""

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, v270

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2735
    .end local v270    # "e":Ljava/lang/Exception;
    :cond_492e
    sget v10, Lcom/chelpus/root/utils/corepatch;->lastPatchPosition:I

    if-lez v10, :cond_4949

    sget-object v10, Lcom/chelpus/root/utils/corepatch;->lastByteReplace:[B

    if-eqz v10, :cond_4949

    .line 2736
    sget-object v10, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    sget v15, Lcom/chelpus/root/utils/corepatch;->lastPatchPosition:I

    invoke-virtual {v10, v15}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2737
    sget-object v10, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    sget-object v15, Lcom/chelpus/root/utils/corepatch;->lastByteReplace:[B

    invoke-virtual {v10, v15}, Ljava/nio/MappedByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 2738
    sget-object v10, Lcom/chelpus/root/utils/corepatch;->fileBytes:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v10}, Ljava/nio/MappedByteBuffer;->force()Ljava/nio/MappedByteBuffer;

    .line 2740
    :cond_4949
    invoke-virtual/range {v177 .. v177}, Ljava/nio/channels/FileChannel;->close()V

    .line 2742
    invoke-virtual/range {v282 .. v282}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v10

    const-string v15, "/classes.dex"

    invoke-virtual {v10, v15}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_4967

    .line 2743
    invoke-static/range {v282 .. v282}, Lcom/chelpus/Utils;->fixadler(Ljava/io/File;)V

    .line 2746
    :goto_495b
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v15, "LuckyPatcher: odex not equal lenght patched! Not enougth space in /system/!"

    invoke-virtual {v10, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    move-object/16 v281, v282

    .end local v282    # "localFile2":Ljava/io/File;
    .restart local v281    # "localFile2":Ljava/io/File;
    goto/16 :goto_44e8

    .line 2744
    .end local v281    # "localFile2":Ljava/io/File;
    .restart local v282    # "localFile2":Ljava/io/File;
    :cond_4967
    const-string v10, "/system/framework/services.jar"

    move-object/from16 v0, v282

    invoke-static {v0, v10}, Lcom/chelpus/Utils;->fixadlerOdex(Ljava/io/File;Ljava/lang/String;)V
    :try_end_496e
    .catch Ljava/io/FileNotFoundException; {:try_start_4914 .. :try_end_496e} :catch_3c8f
    .catch Ljava/lang/Exception; {:try_start_4914 .. :try_end_496e} :catch_3cdb

    goto :goto_495b

    .end local v177    # "ChannelDex":Ljava/nio/channels/FileChannel;
    .end local v199    # "counter2":I
    .end local v266    # "counter":I
    .end local v269    # "destination":Ljava/lang/String;
    .end local v275    # "fp":Ljava/io/File;
    .end local v278    # "isOat":Z
    .end local v279    # "j":J
    .end local v318    # "patch4":Z
    .end local v360    # "source":Ljava/lang/String;
    .restart local v262    # "clasdex":Ljava/io/File;
    .restart local v359    # "serjar":Ljava/io/File;
    :cond_496f
    move-object/16 v281, v282

    .end local v282    # "localFile2":Ljava/io/File;
    .restart local v281    # "localFile2":Ljava/io/File;
    goto/16 :goto_3d51

    .end local v262    # "clasdex":Ljava/io/File;
    .end local v281    # "localFile2":Ljava/io/File;
    .end local v359    # "serjar":Ljava/io/File;
    .restart local v282    # "localFile2":Ljava/io/File;
    :cond_4974
    move-object/16 v281, v282

    .end local v282    # "localFile2":Ljava/io/File;
    .restart local v281    # "localFile2":Ljava/io/File;
    goto/16 :goto_3d70

    .end local v33    # "pattern2":Z
    .end local v273    # "filesToPatch":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    .end local v277    # "indexDir":Ljava/lang/String;
    .end local v281    # "localFile2":Ljava/io/File;
    .restart local v326    # "pattern2":Z
    :cond_4979
    move/16 v328, v105

    .end local v105    # "pattern4":Z
    .restart local v328    # "pattern4":Z
    move/16 v327, v81

    .end local v81    # "pattern3":Z
    .restart local v327    # "pattern3":Z
    goto/16 :goto_df6

    .end local v326    # "pattern2":Z
    .end local v327    # "pattern3":Z
    .end local v328    # "pattern4":Z
    .restart local v33    # "pattern2":Z
    .restart local v81    # "pattern3":Z
    .restart local v105    # "pattern4":Z
    :cond_4981
    move/16 v326, v33

    .end local v33    # "pattern2":Z
    .restart local v326    # "pattern2":Z
    goto/16 :goto_ddf

    .end local v81    # "pattern3":Z
    .restart local v327    # "pattern3":Z
    :cond_4986
    move/16 v328, v105

    .end local v105    # "pattern4":Z
    .restart local v328    # "pattern4":Z
    goto/16 :goto_3be

    .end local v327    # "pattern3":Z
    .end local v328    # "pattern4":Z
    .restart local v81    # "pattern3":Z
    .restart local v105    # "pattern4":Z
    :cond_498b
    move/16 v327, v81

    .end local v81    # "pattern3":Z
    .restart local v327    # "pattern3":Z
    goto/16 :goto_3ac

    .end local v326    # "pattern2":Z
    .end local v327    # "pattern3":Z
    .restart local v33    # "pattern2":Z
    .restart local v81    # "pattern3":Z
    :cond_4990
    move/16 v326, v33

    .end local v33    # "pattern2":Z
    .restart local v326    # "pattern2":Z
    goto/16 :goto_39a

    .line 546
    nop

    :array_4996
    .array-data 1
        0x39t
        0x66t
        0x8t
        0x0t
        0x39t
        0x66t
        0x4t
        0x0t
        0x12t
        0x16t
        0xft
        0x6t
        0x12t
        -0xat
        0x28t
        -0x2t
    .end array-data

    .line 547
    :array_49a2
    .array-data 1
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 548
    :array_49ae
    .array-data 1
        0x39t
        0x66t
        0x8t
        0x0t
        0x39t
        0x66t
        0x4t
        0x0t
        0x12t
        0x16t
        0x12t
        0x6t
        0x12t
        0x6t
        0xft
        0x6t
    .end array-data

    .line 549
    :array_49ba
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
    .end array-data

    .line 552
    :array_49c6
    .array-data 1
        0xat
        0x66t
        0x39t
        0x66t
        0x66t
        0x66t
        0x1at
        0x66t
        0x66t
        0x66t
        0x1at
        0x66t
        0x66t
        0x66t
        0x71t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x13t
        0x66t
        -0x13t
        -0x1t
    .end array-data

    .line 553
    :array_49d6
    .array-data 1
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
    .end array-data

    .line 554
    :array_49e6
    .array-data 1
        0x12t
        0x66t
        0x38t
        0x66t
        0x66t
        0x66t
        0x1at
        0x66t
        0x66t
        0x66t
        0x1at
        0x66t
        0x66t
        0x66t
        0x71t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x13t
        0x66t
        -0x13t
        -0x1t
    .end array-data

    .line 555
    :array_49f6
    .array-data 1
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 558
    :array_4a06
    .array-data 1
        -0x80t
        0x0t
        0x39t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x35t
        -0x79t
        0x66t
        0x66t
        0x1at
    .end array-data

    .line 559
    :array_4a14
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
    .end array-data

    .line 560
    :array_4a22
    .array-data 1
        -0x80t
        0x0t
        0x39t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x35t
        -0x78t
        0x66t
        0x66t
        0x1at
    .end array-data

    .line 561
    :array_4a30
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 564
    :array_4a3e
    .array-data 1
        0x39t
        0x66t
        0x7t
        0x0t
        0x39t
        0x66t
        0x3t
        0x0t
        0xft
        0x6t
        0x12t
        -0xat
        0x28t
        -0x2t
    .end array-data

    .line 565
    nop

    :array_4a4a
    .array-data 1
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 566
    nop

    :array_4a56
    .array-data 1
        0x39t
        0x66t
        0x7t
        0x0t
        0x39t
        0x66t
        0x3t
        0x0t
        0x12t
        0x6t
        0x12t
        0x6t
        0xft
        0x6t
    .end array-data

    .line 567
    nop

    :array_4a62
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
    .end array-data

    .line 570
    nop

    :array_4a6e
    .array-data 1
        0x54t
        0x66t
        0x66t
        0x66t
        0x6et
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x66t
        0x38t
        0x4t
        0x4t
        0x0t
        0x12t
        0x14t
        0xft
        0x4t
    .end array-data

    .line 571
    :array_4a7c
    .array-data 1
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 572
    :array_4a8a
    .array-data 1
        0x54t
        0x66t
        0x66t
        0x66t
        0x6et
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x12t
        0x66t
        0x39t
        0x4t
        0x4t
        0x0t
        0x12t
        0x14t
        0xft
        0x4t
    .end array-data

    .line 573
    :array_4a98
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 577
    :array_4aa6
    .array-data 1
        0xat
        0x66t
        0x39t
        0x66t
        0x66t
        0x66t
        0x22t
        0x66t
        0x66t
        0x66t
        0x13t
        0x66t
        -0x13t
        -0x1t
    .end array-data

    .line 578
    nop

    :array_4ab2
    .array-data 1
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
    .end array-data

    .line 579
    nop

    :array_4abe
    .array-data 1
        0x12t
        0x66t
        0x38t
        0x66t
        0x66t
        0x66t
        0x22t
        0x66t
        0x66t
        0x66t
        0x13t
        0x66t
        -0x13t
        -0x1t
    .end array-data

    .line 580
    nop

    :array_4aca
    .array-data 1
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 583
    nop

    :array_4ad6
    .array-data 1
        0x1dt
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xct
        0x66t
        0x39t
        0x66t
        0x66t
        0x0t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xct
        0x66t
        0x1et
        0x66t
    .end array-data

    .line 584
    :array_4ae8
    .array-data 1
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
    .end array-data

    .line 585
    :array_4afa
    .array-data 1
        0x1dt
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xct
        0x66t
        0x33t
        0x66t
        0x66t
        0x0t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xct
        0x66t
        0x1et
        0x66t
    .end array-data

    .line 586
    :array_4b0c
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x2t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 590
    :array_4b1e
    .array-data 1
        0x1dt
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xct
        0x66t
        0x39t
        0x66t
        0x66t
        0x0t
        0x8t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x8t
        0x66t
        0x66t
        0x66t
        0x8t
        0x66t
        0x66t
        0x66t
        0x2t
        0x66t
        0x66t
        0x66t
        0x2t
        0x66t
        0x66t
        0x66t
    .end array-data

    .line 591
    nop

    :array_4b36
    .array-data 1
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
    .end array-data

    .line 592
    nop

    :array_4b4e
    .array-data 1
        0x1dt
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xct
        0x66t
        0x33t
        0x66t
        0x66t
        0x0t
        0x8t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x8t
        0x66t
        0x66t
        0x66t
        0x8t
        0x66t
        0x66t
        0x66t
        0x2t
        0x66t
        0x66t
        0x66t
        0x2t
        0x66t
        0x66t
        0x66t
    .end array-data

    .line 593
    nop

    :array_4b66
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x2t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 597
    nop

    :array_4b7e
    .array-data 1
        0x38t
        0x66t
        0x66t
        0x66t
        0x8t
        0x66t
        0x66t
        0x66t
        0x8t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x66t
        0x39t
        0x66t
        0x66t
        0x66t
        0x38t
        0x66t
        0x66t
        0x66t
        0x1at
    .end array-data

    .line 598
    nop

    :array_4b92
    .array-data 1
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
    .end array-data

    .line 599
    nop

    :array_4ba6
    .array-data 1
        0x38t
        0x66t
        0x66t
        0x66t
        0x8t
        0x66t
        0x66t
        0x66t
        0x8t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x12t
        0x66t
        0x38t
        0x66t
        0x66t
        0x66t
        0x38t
        0x66t
        0x66t
        0x66t
        0x1at
    .end array-data

    .line 600
    nop

    :array_4bba
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 603
    nop

    :array_4bce
    .array-data 1
        0x38t
        0x66t
        0x66t
        0x0t
        0x8t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xct
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x66t
        0x39t
        0x66t
        0x66t
        0x66t
        0x38t
    .end array-data

    .line 604
    nop

    :array_4be2
    .array-data 1
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
    .end array-data

    .line 605
    nop

    :array_4bf6
    .array-data 1
        0x38t
        0x66t
        0x66t
        0x0t
        0x8t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xct
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x12t
        0x66t
        0x38t
        0x66t
        0x66t
        0x66t
        0x38t
    .end array-data

    .line 606
    nop

    :array_4c0a
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 609
    nop

    :array_4c1e
    .array-data 1
        0x38t
        0x66t
        0x66t
        0x66t
        0x8t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x66t
        0x39t
        0x66t
        0x66t
        0x66t
        0x38t
        0x66t
        0x66t
        0x66t
        0x1at
    .end array-data

    .line 610
    nop

    :array_4c30
    .array-data 1
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
    .end array-data

    .line 611
    nop

    :array_4c42
    .array-data 1
        0x38t
        0x66t
        0x66t
        0x66t
        0x8t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x12t
        0x66t
        0x38t
        0x66t
        0x66t
        0x66t
        0x38t
        0x66t
        0x66t
        0x66t
        0x1at
    .end array-data

    .line 612
    nop

    :array_4c54
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 629
    nop

    :array_4c66
    .array-data 1
        0x20t
        0x66t
        0x66t
        0x12t
        0x31t
        0x32t
        0x10t
        0xat
        0x0t
        0x22t
        0x0t
        0x66t
        0x66t
        0x1at
        0x1t
        0x66t
        0x66t
        0x70t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x0t
        0x66t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x0t
        0xft
        0x0t
    .end array-data

    .line 630
    :array_4c7c
    .array-data 1
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 631
    :array_4c92
    .array-data 1
        0x20t
        0x66t
        0x66t
        0x12t
        0x31t
        0x32t
        0x10t
        0xat
        0x0t
        0x22t
        0x0t
        0x66t
        0x66t
        0x1at
        0x1t
        0x66t
        0x66t
        0x70t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x0t
        0x66t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x12t
        0x10t
        0xft
        0x0t
    .end array-data

    .line 632
    :array_4ca8
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
    .end array-data

    .line 635
    :array_4cbe
    .array-data 1
        0x52t
        0x20t
        0x66t
        0x66t
        0x12t
        0x31t
        0x32t
        0x10t
        0xat
        0x0t
        0x22t
        0x0t
        0x66t
        0x66t
        0x1at
        0x1t
        0x66t
        0x66t
        0x70t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x0t
        0x6et
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x0t
        0xft
        0x0t
    .end array-data

    .line 636
    :array_4cd4
    .array-data 1
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 637
    :array_4cea
    .array-data 1
        0x52t
        0x20t
        0x66t
        0x66t
        0x12t
        0x31t
        0x32t
        0x10t
        0xat
        0x0t
        0x22t
        0x0t
        0x66t
        0x66t
        0x1at
        0x1t
        0x66t
        0x66t
        0x70t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x0t
        0x6et
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x12t
        0x10t
        0xft
        0x0t
    .end array-data

    .line 638
    :array_4d00
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
    .end array-data

    .line 641
    :array_4d16
    .array-data 1
        0x12t
        0x3t
        0x21t
        0x41t
        0x21t
        0x66t
        0x32t
        0x66t
        0x66t
        0x0t
        0x66t
        0x66t
        0xft
        0x1t
        0x12t
        0x0t
        0x21t
        0x66t
        0x35t
        0x66t
        0x66t
        0x0t
        0x48t
        0x66t
        0x4t
        0x0t
        0x48t
        0x66t
        0x5t
        0x0t
    .end array-data

    .line 642
    nop

    :array_4d2a
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
    .end array-data

    .line 643
    nop

    :array_4d3e
    .array-data 1
        0x12t
        0x13t
        0x21t
        0x41t
        0x21t
        0x66t
        0x32t
        0x66t
        0x66t
        0x0t
        0x66t
        0x66t
        0xft
        0x1t
        0x12t
        0x0t
        0x21t
        0x66t
        0x35t
        0x66t
        0x66t
        0x0t
        0x48t
        0x66t
        0x4t
        0x0t
        0x48t
        0x66t
        0x5t
        0x0t
    .end array-data

    .line 644
    nop

    :array_4d52
    .array-data 1
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 647
    nop

    :array_4d66
    .array-data 1
        0x12t
        0x1t
        0x21t
        0x42t
        0x21t
        0x66t
        0x32t
        0x66t
        0x66t
        0x0t
        0xft
        0x1t
        0x12t
        0x0t
        0x21t
        0x66t
        0x35t
        0x66t
        0x66t
        0x0t
        0x48t
        0x66t
        0x4t
        0x0t
        0x48t
        0x66t
        0x5t
        0x0t
    .end array-data

    .line 648
    :array_4d78
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
    .end array-data

    .line 649
    :array_4d8a
    .array-data 1
        0x12t
        0x11t
        0x21t
        0x42t
        0x21t
        0x66t
        0x32t
        0x66t
        0x66t
        0x0t
        0xft
        0x1t
        0x12t
        0x0t
        0x21t
        0x66t
        0x35t
        0x66t
        0x66t
        0x0t
        0x48t
        0x66t
        0x4t
        0x0t
        0x48t
        0x66t
        0x5t
        0x0t
    .end array-data

    .line 650
    :array_4d9c
    .array-data 1
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 653
    :array_4dae
    .array-data 1
        0x20t
        0x66t
        0x66t
        0x12t
        0x31t
        0x32t
        0x10t
        0xet
        0x0t
        0x22t
        0x0t
        0x66t
        0x66t
        0x1at
        0x1t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x70t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x0t
        0x66t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x0t
        0xft
        0x0t
    .end array-data

    .line 654
    :array_4dc8
    .array-data 1
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 655
    :array_4de2
    .array-data 1
        0x20t
        0x66t
        0x66t
        0x12t
        0x31t
        0x32t
        0x10t
        0xet
        0x0t
        0x22t
        0x0t
        0x66t
        0x66t
        0x1at
        0x1t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x70t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x0t
        0x66t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x12t
        0x10t
        0xft
        0x0t
    .end array-data

    .line 656
    :array_4dfc
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
    .end array-data

    .line 659
    :array_4e16
    .array-data 1
        0x38t
        0x66t
        0x66t
        0x66t
        0x3at
        0x66t
        0x66t
        0x66t
        0x3at
        0x66t
        0x66t
        0x66t
        -0x70t
        0x66t
        0x66t
        0x66t
        0x21t
        0x66t
        0x37t
        0x66t
        0x66t
        0x66t
        0x22t
        0x66t
        0x66t
        0x66t
        0x70t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x0t
        0xft
        0x0t
    .end array-data

    .line 660
    :array_4e30
    .array-data 1
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 661
    :array_4e4a
    .array-data 1
        0x38t
        0x66t
        0x66t
        0x66t
        0x3at
        0x66t
        0x66t
        0x66t
        0x3at
        0x66t
        0x66t
        0x66t
        -0x70t
        0x66t
        0x66t
        0x66t
        0x21t
        0x66t
        0x37t
        0x66t
        0x66t
        0x66t
        0x22t
        0x66t
        0x66t
        0x66t
        0x70t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x12t
        0x10t
        0xft
        0x0t
    .end array-data

    .line 662
    :array_4e64
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
    .end array-data

    .line 665
    :array_4e7e
    .array-data 1
        0x38t
        0x66t
        0x66t
        0x66t
        0x3at
        0x66t
        0x66t
        0x66t
        0x3at
        0x66t
        0x66t
        0x66t
        -0x70t
        0x66t
        0x66t
        0x66t
        0x21t
        0x66t
        0x37t
        0x66t
        0x66t
        0x66t
        0x22t
        0x66t
        0x66t
        0x66t
        0x70t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x66t
        0x21t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x0t
        0xft
        0x0t
    .end array-data

    .line 666
    nop

    :array_4ea6
    .array-data 1
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 667
    nop

    :array_4ece
    .array-data 1
        0x38t
        0x66t
        0x66t
        0x66t
        0x3at
        0x66t
        0x66t
        0x66t
        0x3at
        0x66t
        0x66t
        0x66t
        -0x70t
        0x66t
        0x66t
        0x66t
        0x21t
        0x66t
        0x37t
        0x66t
        0x66t
        0x66t
        0x22t
        0x66t
        0x66t
        0x66t
        0x70t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x66t
        0x21t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x12t
        0x10t
        0xft
        0x0t
    .end array-data

    .line 668
    nop

    :array_4ef6
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
    .end array-data

    .line 671
    nop

    :array_4f1e
    .array-data 1
        0x38t
        0x66t
        0x66t
        0x66t
        0x3at
        0x66t
        0x66t
        0x66t
        0x3at
        0x66t
        0x66t
        0x66t
        -0x70t
        0x66t
        0x66t
        0x66t
        0x21t
        0x66t
        0x37t
        0x66t
        0x66t
        0x66t
        0x22t
        0x66t
        0x66t
        0x66t
        0x1at
        0x66t
        0x66t
        0x66t
        0x71t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xct
        0x66t
        0x70t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x0t
        0xft
        0x0t
    .end array-data

    .line 672
    :array_4f3e
    .array-data 1
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 673
    :array_4f5e
    .array-data 1
        0x38t
        0x66t
        0x66t
        0x66t
        0x3at
        0x66t
        0x66t
        0x66t
        0x3at
        0x66t
        0x66t
        0x66t
        -0x70t
        0x66t
        0x66t
        0x66t
        0x21t
        0x66t
        0x37t
        0x66t
        0x66t
        0x66t
        0x22t
        0x66t
        0x66t
        0x66t
        0x1at
        0x66t
        0x66t
        0x66t
        0x71t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xct
        0x66t
        0x70t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x12t
        0x10t
        0xft
        0x0t
    .end array-data

    .line 674
    :array_4f7e
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
    .end array-data

    .line 677
    :array_4f9e
    .array-data 1
        0x20t
        0x66t
        0x66t
        0x12t
        0x31t
        0x32t
        0x10t
        0xat
        0x0t
        0x22t
        0x0t
        0x66t
        0x66t
        0x1at
        0x1t
        0x66t
        0x66t
        0x70t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x0t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x0t
        0xft
        0x0t
    .end array-data

    .line 678
    nop

    :array_4fb8
    .array-data 1
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 679
    nop

    :array_4fd2
    .array-data 1
        0x20t
        0x66t
        0x66t
        0x12t
        0x31t
        0x32t
        0x10t
        0xat
        0x0t
        0x22t
        0x0t
        0x66t
        0x66t
        0x1at
        0x1t
        0x66t
        0x66t
        0x70t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x0t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x12t
        0x10t
        0xft
        0x0t
    .end array-data

    .line 680
    nop

    :array_4fec
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
    .end array-data

    .line 683
    nop

    :array_5006
    .array-data 1
        0x38t
        0x66t
        0x66t
        0x66t
        0x3at
        0x66t
        0x66t
        0x66t
        0x3at
        0x66t
        0x66t
        0x66t
        -0x70t
        0x66t
        0x66t
        0x66t
        0x21t
        0x66t
        0x37t
        0x66t
        0x66t
        0x66t
        0x22t
        0x66t
        0x66t
        0x66t
        0x70t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x0t
        0xft
        0x0t
    .end array-data

    .line 684
    nop

    :array_5024
    .array-data 1
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 685
    nop

    :array_5042
    .array-data 1
        0x38t
        0x66t
        0x66t
        0x66t
        0x3at
        0x66t
        0x66t
        0x66t
        0x3at
        0x66t
        0x66t
        0x66t
        -0x70t
        0x66t
        0x66t
        0x66t
        0x21t
        0x66t
        0x37t
        0x66t
        0x66t
        0x66t
        0x22t
        0x66t
        0x66t
        0x66t
        0x70t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x12t
        0x10t
        0xft
        0x0t
    .end array-data

    .line 686
    nop

    :array_5060
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
    .end array-data

    .line 689
    nop

    :array_507e
    .array-data 1
        0x12t
        0x2t
        0x21t
        0x53t
        0x21t
        0x66t
        0x32t
        0x66t
        0x66t
        0x0t
        0xft
        0x2t
        0x12t
        0x1t
        0x12t
        0x0t
        0x21t
        0x66t
        0x35t
        0x66t
        0x66t
        0x0t
        0x48t
        0x66t
        0x5t
        0x0t
    .end array-data

    .line 690
    nop

    :array_5090
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
    .end array-data

    .line 691
    nop

    :array_50a2
    .array-data 1
        0x12t
        0x12t
        0x21t
        0x53t
        0x21t
        0x66t
        0x32t
        0x66t
        0x66t
        0x0t
        0xft
        0x2t
        0x12t
        0x1t
        0x12t
        0x0t
        0x21t
        0x66t
        0x35t
        0x66t
        0x66t
        0x0t
        0x48t
        0x66t
        0x5t
        0x0t
    .end array-data

    .line 692
    nop

    :array_50b4
    .array-data 1
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 695
    nop

    :array_50c6
    .array-data 1
        -0xet
        0x20t
        0x66t
        0x66t
        0x12t
        0x31t
        0x32t
        0x10t
        0xat
        0x0t
        0x22t
        0x0t
        0x66t
        0x66t
        0x1at
        0x1t
        0x66t
        0x66t
        0x70t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x0t
        -0x8t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x0t
        0xft
        0x0t
    .end array-data

    .line 696
    :array_50dc
    .array-data 1
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 697
    :array_50f2
    .array-data 1
        -0xet
        0x20t
        0x66t
        0x66t
        0x12t
        0x31t
        0x32t
        0x10t
        0xat
        0x0t
        0x22t
        0x0t
        0x66t
        0x66t
        0x1at
        0x1t
        0x66t
        0x66t
        0x70t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x0t
        -0x8t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x12t
        0x10t
        0xft
        0x0t
    .end array-data

    .line 698
    :array_5108
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
    .end array-data

    .line 907
    :array_511e
    .array-data 1
        0x20t
        0x66t
        0x66t
        0x12t
        0x10t
        0xft
        0x0t
        0x0t
        0x0t
        0x22t
        0x0t
        0x66t
        0x66t
        0x1at
        0x1t
        0x66t
        0x66t
        0x70t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x0t
        0x66t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x0t
        0xft
        0x0t
    .end array-data

    .line 908
    :array_5134
    .array-data 1
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 909
    :array_514a
    .array-data 1
        0x20t
        0x66t
        0x66t
        0x12t
        0x31t
        0x32t
        0x10t
        0xat
        0x0t
        0x22t
        0x0t
        0x66t
        0x66t
        0x1at
        0x1t
        0x66t
        0x66t
        0x70t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x0t
        0x66t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x0t
        0xft
        0x0t
    .end array-data

    .line 910
    :array_5160
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 912
    :array_5176
    .array-data 1
        0x52t
        0x20t
        0x66t
        0x66t
        0x12t
        0x10t
        0xft
        0x0t
        0x0t
        0x0t
        0x22t
        0x0t
        0x66t
        0x66t
        0x1at
        0x1t
        0x66t
        0x66t
        0x70t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x0t
        0x6et
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x0t
        0xft
        0x0t
    .end array-data

    .line 913
    :array_518c
    .array-data 1
        0x52t
        0x20t
        0x66t
        0x66t
        0x12t
        0x31t
        0x32t
        0x10t
        0xat
        0x0t
        0x22t
        0x0t
        0x66t
        0x66t
        0x1at
        0x1t
        0x66t
        0x66t
        0x70t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x0t
        0x6et
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x0t
        0xft
        0x0t
    .end array-data

    .line 915
    :array_51a2
    .array-data 1
        0x12t
        0x10t
        0xft
        0x0t
        0x21t
        0x66t
        0x32t
        0x66t
        0x66t
        0x0t
        0x66t
        0x66t
        0xft
        0x1t
        0x12t
        0x0t
        0x21t
        0x66t
        0x35t
        0x66t
        0x66t
        0x0t
        0x48t
        0x66t
        0x4t
        0x0t
        0x48t
        0x66t
        0x5t
        0x0t
    .end array-data

    .line 916
    nop

    :array_51b6
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
    .end array-data

    .line 917
    nop

    :array_51ca
    .array-data 1
        0x12t
        0x3t
        0x21t
        0x41t
        0x21t
        0x66t
        0x32t
        0x66t
        0x66t
        0x0t
        0x66t
        0x66t
        0xft
        0x1t
        0x12t
        0x0t
        0x21t
        0x66t
        0x35t
        0x66t
        0x66t
        0x0t
        0x48t
        0x66t
        0x4t
        0x0t
        0x48t
        0x66t
        0x5t
        0x0t
    .end array-data

    .line 918
    nop

    :array_51de
    .array-data 1
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 920
    nop

    :array_51f2
    .array-data 1
        0x12t
        0x10t
        0xft
        0x0t
        0x21t
        0x66t
        0x32t
        0x66t
        0x66t
        0x0t
        0xft
        0x1t
        0x12t
        0x0t
        0x21t
        0x66t
        0x35t
        0x66t
        0x66t
        0x0t
        0x48t
        0x66t
        0x4t
        0x0t
        0x48t
        0x66t
        0x5t
        0x0t
    .end array-data

    .line 921
    :array_5204
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
    .end array-data

    .line 922
    :array_5216
    .array-data 1
        0x12t
        0x1t
        0x21t
        0x42t
        0x21t
        0x66t
        0x32t
        0x66t
        0x66t
        0x0t
        0xft
        0x1t
        0x12t
        0x0t
        0x21t
        0x66t
        0x35t
        0x66t
        0x66t
        0x0t
        0x48t
        0x66t
        0x4t
        0x0t
        0x48t
        0x66t
        0x5t
        0x0t
    .end array-data

    .line 923
    :array_5228
    .array-data 1
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 925
    :array_523a
    .array-data 1
        0x20t
        0x66t
        0x66t
        0x12t
        0x10t
        0xft
        0x0t
        0x0t
        0x0t
        0x22t
        0x0t
        0x66t
        0x66t
        0x1at
        0x1t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x70t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x0t
        0x66t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x0t
        0xft
        0x0t
    .end array-data

    .line 926
    :array_5254
    .array-data 1
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 927
    :array_526e
    .array-data 1
        0x20t
        0x66t
        0x66t
        0x12t
        0x31t
        0x32t
        0x10t
        0xet
        0x0t
        0x22t
        0x0t
        0x66t
        0x66t
        0x1at
        0x1t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x70t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x0t
        0x66t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x0t
        0xft
        0x0t
    .end array-data

    .line 928
    :array_5288
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 930
    :array_52a2
    .array-data 1
        0x52t
        0x20t
        0x66t
        0x66t
        0x12t
        0x10t
        0xft
        0x0t
        0x0t
        0x0t
        0x22t
        0x0t
        0x66t
        0x66t
        0x1at
        0x1t
        0x66t
        0x66t
        0x70t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x0t
        0x38t
        0x3t
        0xbt
        0x0t
    .end array-data

    .line 931
    nop

    :array_52b6
    .array-data 1
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 932
    nop

    :array_52ca
    .array-data 1
        0x52t
        0x20t
        0x66t
        0x66t
        0x12t
        0x31t
        0x32t
        0x10t
        0xat
        0x0t
        0x22t
        0x0t
        0x66t
        0x66t
        0x1at
        0x1t
        0x66t
        0x66t
        0x70t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x0t
        0x38t
        0x3t
        0xbt
        0x0t
    .end array-data

    .line 933
    nop

    :array_52de
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 935
    nop

    :array_52f2
    .array-data 1
        -0xet
        0x20t
        0x66t
        0x66t
        0x12t
        0x10t
        0xft
        0x0t
        0x0t
        0x0t
        0x22t
        0x0t
        0x66t
        0x66t
        0x1at
        0x1t
        0x66t
        0x66t
        0x70t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x0t
        0x38t
        0x3t
        0xbt
        0x0t
    .end array-data

    .line 936
    nop

    :array_5306
    .array-data 1
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 937
    nop

    :array_531a
    .array-data 1
        -0xet
        0x20t
        0x66t
        0x66t
        0x12t
        0x31t
        0x32t
        0x10t
        0xat
        0x0t
        0x22t
        0x0t
        0x66t
        0x66t
        0x1at
        0x1t
        0x66t
        0x66t
        0x70t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x0t
        0x38t
        0x3t
        0xbt
        0x0t
    .end array-data

    .line 938
    nop

    :array_532e
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 940
    nop

    :array_5342
    .array-data 1
        -0xet
        0x20t
        0x66t
        0x66t
        0x12t
        0x10t
        0xft
        0x0t
        0x0t
        0x0t
        0x22t
        0x0t
        0x66t
        0x66t
        0x1at
        0x1t
        0x66t
        0x66t
        0x71t
        0x10t
        0x66t
        0x66t
        0x66t
        0x66t
        0xct
        0x1t
        0x70t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x0t
        0x38t
        0x3t
        0xbt
        0x0t
    .end array-data

    .line 941
    nop

    :array_535a
    .array-data 1
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 942
    nop

    :array_5372
    .array-data 1
        -0xet
        0x20t
        0x66t
        0x66t
        0x12t
        0x31t
        0x32t
        0x10t
        0xet
        0x0t
        0x22t
        0x0t
        0x66t
        0x66t
        0x1at
        0x1t
        0x66t
        0x66t
        0x71t
        0x10t
        0x66t
        0x66t
        0x66t
        0x66t
        0xct
        0x1t
        0x70t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x0t
        0x38t
        0x3t
        0xbt
        0x0t
    .end array-data

    .line 943
    nop

    :array_538a
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 948
    nop

    :array_53a2
    .array-data 1
        0x39t
        0x66t
        0x8t
        0x0t
        0x39t
        0x66t
        0x4t
        0x0t
        0x12t
        0x16t
        0x12t
        0x6t
        0x12t
        0x6t
        0xft
        0x6t
    .end array-data

    .line 949
    :array_53ae
    .array-data 1
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 950
    :array_53ba
    .array-data 1
        0x39t
        0x66t
        0x8t
        0x0t
        0x39t
        0x66t
        0x4t
        0x0t
        0x12t
        0x16t
        0xft
        0x6t
        0x12t
        -0xat
        0x28t
        -0x2t
    .end array-data

    .line 951
    :array_53c6
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
    .end array-data

    .line 954
    :array_53d2
    .array-data 1
        0x12t
        0x66t
        0x38t
        0x66t
        0x66t
        0x66t
        0x1at
        0x66t
        0x66t
        0x66t
        0x1at
        0x66t
        0x66t
        0x66t
        0x71t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x13t
        0x66t
        -0x13t
        -0x1t
    .end array-data

    .line 955
    :array_53e2
    .array-data 1
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
    .end array-data

    .line 956
    :array_53f2
    .array-data 1
        0xat
        0x66t
        0x39t
        0x66t
        0x66t
        0x66t
        0x1at
        0x66t
        0x66t
        0x66t
        0x1at
        0x66t
        0x66t
        0x66t
        0x71t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x13t
        0x66t
        -0x13t
        -0x1t
    .end array-data

    .line 957
    :array_5402
    .array-data 1
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 960
    :array_5412
    .array-data 1
        -0x80t
        0x0t
        0x39t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x35t
        -0x78t
        0x66t
        0x66t
        0x1at
    .end array-data

    .line 961
    :array_5420
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
    .end array-data

    .line 962
    :array_542e
    .array-data 1
        -0x80t
        0x0t
        0x39t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x35t
        -0x79t
        0x66t
        0x66t
        0x1at
    .end array-data

    .line 963
    :array_543c
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 966
    :array_544a
    .array-data 1
        0x39t
        0x66t
        0x7t
        0x0t
        0x39t
        0x66t
        0x3t
        0x0t
        0x12t
        0x6t
        0x12t
        0x6t
        0xft
        0x6t
    .end array-data

    .line 967
    nop

    :array_5456
    .array-data 1
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 968
    nop

    :array_5462
    .array-data 1
        0x39t
        0x66t
        0x7t
        0x0t
        0x39t
        0x66t
        0x3t
        0x0t
        0xft
        0x6t
        0x12t
        -0xat
        0x28t
        -0x2t
    .end array-data

    .line 969
    nop

    :array_546e
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
    .end array-data

    .line 972
    nop

    :array_547a
    .array-data 1
        0x54t
        0x66t
        0x66t
        0x66t
        0x6et
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x12t
        0x66t
        0x39t
        0x4t
        0x4t
        0x0t
        0x12t
        0x14t
        0xft
        0x4t
    .end array-data

    .line 973
    :array_5488
    .array-data 1
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 974
    :array_5496
    .array-data 1
        0x54t
        0x66t
        0x66t
        0x66t
        0x6et
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x66t
        0x38t
        0x4t
        0x4t
        0x0t
        0x12t
        0x14t
        0xft
        0x4t
    .end array-data

    .line 975
    :array_54a4
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 978
    :array_54b2
    .array-data 1
        0x12t
        0x66t
        0x38t
        0x66t
        0x66t
        0x66t
        0x22t
        0x66t
        0x66t
        0x66t
        0x13t
        0x66t
        -0x13t
        -0x1t
    .end array-data

    .line 979
    nop

    :array_54be
    .array-data 1
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
    .end array-data

    .line 980
    nop

    :array_54ca
    .array-data 1
        0xat
        0x66t
        0x39t
        0x66t
        0x66t
        0x66t
        0x22t
        0x66t
        0x66t
        0x66t
        0x13t
        0x66t
        -0x13t
        -0x1t
    .end array-data

    .line 981
    nop

    :array_54d6
    .array-data 1
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 984
    nop

    :array_54e2
    .array-data 1
        0x1dt
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xct
        0x66t
        0x33t
        0x66t
        0x66t
        0x0t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xct
        0x66t
        0x1et
        0x66t
    .end array-data

    .line 985
    :array_54f4
    .array-data 1
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
    .end array-data

    .line 986
    :array_5506
    .array-data 1
        0x1dt
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xct
        0x66t
        0x39t
        0x66t
        0x66t
        0x0t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xct
        0x66t
        0x1et
        0x66t
    .end array-data

    .line 987
    :array_5518
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x3t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 991
    :array_552a
    .array-data 1
        0x1dt
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xct
        0x66t
        0x33t
        0x66t
        0x66t
        0x0t
        0x8t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x8t
        0x66t
        0x66t
        0x66t
        0x8t
        0x66t
        0x66t
        0x66t
        0x2t
        0x66t
        0x66t
        0x66t
        0x2t
        0x66t
        0x66t
        0x66t
    .end array-data

    .line 992
    nop

    :array_5542
    .array-data 1
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
    .end array-data

    .line 993
    nop

    :array_555a
    .array-data 1
        0x1dt
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xct
        0x66t
        0x39t
        0x66t
        0x66t
        0x0t
        0x8t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x8t
        0x66t
        0x66t
        0x66t
        0x8t
        0x66t
        0x66t
        0x66t
        0x2t
        0x66t
        0x66t
        0x66t
        0x2t
        0x66t
        0x66t
        0x66t
    .end array-data

    .line 994
    nop

    :array_5572
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x3t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 998
    nop

    :array_558a
    .array-data 1
        0x38t
        0x66t
        0x66t
        0x66t
        0x8t
        0x66t
        0x66t
        0x66t
        0x8t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x12t
        0x66t
        0x38t
        0x66t
        0x66t
        0x66t
        0x38t
        0x66t
        0x66t
        0x66t
        0x1at
    .end array-data

    .line 999
    nop

    :array_559e
    .array-data 1
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
    .end array-data

    .line 1000
    nop

    :array_55b2
    .array-data 1
        0x38t
        0x66t
        0x66t
        0x66t
        0x8t
        0x66t
        0x66t
        0x66t
        0x8t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x66t
        0x39t
        0x66t
        0x66t
        0x66t
        0x38t
        0x66t
        0x66t
        0x66t
        0x1at
    .end array-data

    .line 1001
    nop

    :array_55c6
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 1004
    nop

    :array_55da
    .array-data 1
        0x38t
        0x66t
        0x66t
        0x0t
        0x8t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xct
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x12t
        0x66t
        0x38t
        0x66t
        0x66t
        0x66t
        0x38t
    .end array-data

    .line 1005
    nop

    :array_55ee
    .array-data 1
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
    .end array-data

    .line 1006
    nop

    :array_5602
    .array-data 1
        0x38t
        0x66t
        0x66t
        0x0t
        0x8t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xct
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x66t
        0x39t
        0x66t
        0x66t
        0x66t
        0x38t
    .end array-data

    .line 1007
    nop

    :array_5616
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 1010
    nop

    :array_562a
    .array-data 1
        0x38t
        0x66t
        0x66t
        0x66t
        0x8t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x12t
        0x66t
        0x38t
        0x66t
        0x66t
        0x66t
        0x38t
        0x66t
        0x66t
        0x66t
        0x1at
    .end array-data

    .line 1011
    nop

    :array_563c
    .array-data 1
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
    .end array-data

    .line 1012
    nop

    :array_564e
    .array-data 1
        0x38t
        0x66t
        0x66t
        0x66t
        0x8t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x66t
        0x39t
        0x66t
        0x66t
        0x66t
        0x38t
        0x66t
        0x66t
        0x66t
        0x1at
    .end array-data

    .line 1013
    nop

    :array_5660
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 1031
    nop

    :array_5672
    .array-data 1
        0x20t
        0x66t
        0x66t
        0x12t
        0x31t
        0x32t
        0x10t
        0xat
        0x0t
        0x22t
        0x0t
        0x66t
        0x66t
        0x1at
        0x1t
        0x66t
        0x66t
        0x70t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x0t
        0x66t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x12t
        0x10t
        0xft
        0x0t
    .end array-data

    .line 1032
    :array_5688
    .array-data 1
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 1033
    :array_569e
    .array-data 1
        0x20t
        0x66t
        0x66t
        0x12t
        0x31t
        0x32t
        0x10t
        0xat
        0x0t
        0x22t
        0x0t
        0x66t
        0x66t
        0x1at
        0x1t
        0x66t
        0x66t
        0x70t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x0t
        0x66t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x0t
        0xft
        0x0t
    .end array-data

    .line 1034
    :array_56b4
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
    .end array-data

    .line 1037
    :array_56ca
    .array-data 1
        0x52t
        0x20t
        0x66t
        0x66t
        0x12t
        0x31t
        0x32t
        0x10t
        0xat
        0x0t
        0x22t
        0x0t
        0x66t
        0x66t
        0x1at
        0x1t
        0x66t
        0x66t
        0x70t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x0t
        0x6et
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x12t
        0x10t
        0xft
        0x0t
    .end array-data

    .line 1038
    :array_56e0
    .array-data 1
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 1039
    :array_56f6
    .array-data 1
        0x52t
        0x20t
        0x66t
        0x66t
        0x12t
        0x31t
        0x32t
        0x10t
        0xat
        0x0t
        0x22t
        0x0t
        0x66t
        0x66t
        0x1at
        0x1t
        0x66t
        0x66t
        0x70t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x0t
        0x6et
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x0t
        0xft
        0x0t
    .end array-data

    .line 1040
    :array_570c
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
    .end array-data

    .line 1042
    :array_5722
    .array-data 1
        0x12t
        0x13t
        0x21t
        0x41t
        0x21t
        0x66t
        0x32t
        0x66t
        0x66t
        0x0t
        0x66t
        0x66t
        0xft
        0x1t
        0x12t
        0x0t
        0x21t
        0x66t
        0x35t
        0x66t
        0x66t
        0x0t
        0x48t
        0x66t
        0x4t
        0x0t
        0x48t
        0x66t
        0x5t
        0x0t
    .end array-data

    .line 1043
    nop

    :array_5736
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
    .end array-data

    .line 1044
    nop

    :array_574a
    .array-data 1
        0x12t
        0x3t
        0x21t
        0x41t
        0x21t
        0x66t
        0x32t
        0x66t
        0x66t
        0x0t
        0x66t
        0x66t
        0xft
        0x1t
        0x12t
        0x0t
        0x21t
        0x66t
        0x35t
        0x66t
        0x66t
        0x0t
        0x48t
        0x66t
        0x4t
        0x0t
        0x48t
        0x66t
        0x5t
        0x0t
    .end array-data

    .line 1045
    nop

    :array_575e
    .array-data 1
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 1048
    nop

    :array_5772
    .array-data 1
        0x12t
        0x11t
        0x21t
        0x42t
        0x21t
        0x66t
        0x32t
        0x66t
        0x66t
        0x0t
        0xft
        0x1t
        0x12t
        0x0t
        0x21t
        0x66t
        0x35t
        0x66t
        0x66t
        0x0t
        0x48t
        0x66t
        0x4t
        0x0t
        0x48t
        0x66t
        0x5t
        0x0t
    .end array-data

    .line 1049
    :array_5784
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
    .end array-data

    .line 1050
    :array_5796
    .array-data 1
        0x12t
        0x1t
        0x21t
        0x42t
        0x21t
        0x66t
        0x32t
        0x66t
        0x66t
        0x0t
        0xft
        0x1t
        0x12t
        0x0t
        0x21t
        0x66t
        0x35t
        0x66t
        0x66t
        0x0t
        0x48t
        0x66t
        0x4t
        0x0t
        0x48t
        0x66t
        0x5t
        0x0t
    .end array-data

    .line 1051
    :array_57a8
    .array-data 1
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 1054
    :array_57ba
    .array-data 1
        0x20t
        0x66t
        0x66t
        0x12t
        0x31t
        0x32t
        0x10t
        0xet
        0x0t
        0x22t
        0x0t
        0x66t
        0x66t
        0x1at
        0x1t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x70t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x0t
        0x66t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x12t
        0x10t
        0xft
        0x0t
    .end array-data

    .line 1055
    :array_57d4
    .array-data 1
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 1056
    :array_57ee
    .array-data 1
        0x20t
        0x66t
        0x66t
        0x12t
        0x31t
        0x32t
        0x10t
        0xet
        0x0t
        0x22t
        0x0t
        0x66t
        0x66t
        0x1at
        0x1t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x70t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x0t
        0x66t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x0t
        0xft
        0x0t
    .end array-data

    .line 1057
    :array_5808
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
    .end array-data

    .line 1060
    :array_5822
    .array-data 1
        0x38t
        0x66t
        0x66t
        0x66t
        0x3at
        0x66t
        0x66t
        0x66t
        0x3at
        0x66t
        0x66t
        0x66t
        -0x70t
        0x66t
        0x66t
        0x66t
        0x21t
        0x66t
        0x37t
        0x66t
        0x66t
        0x66t
        0x22t
        0x66t
        0x66t
        0x66t
        0x70t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x12t
        0x10t
        0xft
        0x0t
    .end array-data

    .line 1061
    :array_583c
    .array-data 1
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 1062
    :array_5856
    .array-data 1
        0x38t
        0x66t
        0x66t
        0x66t
        0x3at
        0x66t
        0x66t
        0x66t
        0x3at
        0x66t
        0x66t
        0x66t
        -0x70t
        0x66t
        0x66t
        0x66t
        0x21t
        0x66t
        0x37t
        0x66t
        0x66t
        0x66t
        0x22t
        0x66t
        0x66t
        0x66t
        0x70t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x0t
        0xft
        0x0t
    .end array-data

    .line 1063
    :array_5870
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
    .end array-data

    .line 1066
    :array_588a
    .array-data 1
        0x38t
        0x66t
        0x66t
        0x66t
        0x3at
        0x66t
        0x66t
        0x66t
        0x3at
        0x66t
        0x66t
        0x66t
        -0x70t
        0x66t
        0x66t
        0x66t
        0x21t
        0x66t
        0x37t
        0x66t
        0x66t
        0x66t
        0x22t
        0x66t
        0x66t
        0x66t
        0x70t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x12t
        0x10t
        0xft
        0x0t
    .end array-data

    .line 1067
    nop

    :array_58b2
    .array-data 1
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 1068
    nop

    :array_58da
    .array-data 1
        0x38t
        0x66t
        0x66t
        0x66t
        0x3at
        0x66t
        0x66t
        0x66t
        0x3at
        0x66t
        0x66t
        0x66t
        -0x70t
        0x66t
        0x66t
        0x66t
        0x21t
        0x66t
        0x37t
        0x66t
        0x66t
        0x66t
        0x22t
        0x66t
        0x66t
        0x66t
        0x70t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x0t
        0xft
        0x0t
    .end array-data

    .line 1069
    nop

    :array_5902
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
    .end array-data

    .line 1072
    nop

    :array_592a
    .array-data 1
        0x38t
        0x66t
        0x66t
        0x66t
        0x3at
        0x66t
        0x66t
        0x66t
        0x3at
        0x66t
        0x66t
        0x66t
        -0x70t
        0x66t
        0x66t
        0x66t
        0x21t
        0x66t
        0x37t
        0x66t
        0x66t
        0x66t
        0x22t
        0x66t
        0x66t
        0x66t
        0x1at
        0x66t
        0x66t
        0x66t
        0x71t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xct
        0x66t
        0x70t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x12t
        0x10t
        0xft
        0x0t
    .end array-data

    .line 1073
    :array_594a
    .array-data 1
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 1074
    :array_596a
    .array-data 1
        0x38t
        0x66t
        0x66t
        0x66t
        0x3at
        0x66t
        0x66t
        0x66t
        0x3at
        0x66t
        0x66t
        0x66t
        -0x70t
        0x66t
        0x66t
        0x66t
        0x21t
        0x66t
        0x37t
        0x66t
        0x66t
        0x66t
        0x22t
        0x66t
        0x66t
        0x66t
        0x1at
        0x66t
        0x66t
        0x66t
        0x71t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xct
        0x66t
        0x70t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x0t
        0xft
        0x0t
    .end array-data

    .line 1075
    :array_598a
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
    .end array-data

    .line 1078
    :array_59aa
    .array-data 1
        0x20t
        0x66t
        0x66t
        0x12t
        0x31t
        0x32t
        0x10t
        0xat
        0x0t
        0x22t
        0x0t
        0x66t
        0x66t
        0x1at
        0x1t
        0x66t
        0x66t
        0x70t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x0t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x12t
        0x10t
        0xft
        0x0t
    .end array-data

    .line 1079
    nop

    :array_59c4
    .array-data 1
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 1080
    nop

    :array_59de
    .array-data 1
        0x20t
        0x66t
        0x66t
        0x12t
        0x31t
        0x32t
        0x10t
        0xat
        0x0t
        0x22t
        0x0t
        0x66t
        0x66t
        0x1at
        0x1t
        0x66t
        0x66t
        0x70t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x0t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x0t
        0xft
        0x0t
    .end array-data

    .line 1081
    nop

    :array_59f8
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
    .end array-data

    .line 1084
    nop

    :array_5a12
    .array-data 1
        0x38t
        0x66t
        0x66t
        0x66t
        0x3at
        0x66t
        0x66t
        0x66t
        0x3at
        0x66t
        0x66t
        0x66t
        -0x70t
        0x66t
        0x66t
        0x66t
        0x21t
        0x66t
        0x37t
        0x66t
        0x66t
        0x66t
        0x22t
        0x66t
        0x66t
        0x66t
        0x70t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x12t
        0x10t
        0xft
        0x0t
    .end array-data

    .line 1085
    nop

    :array_5a30
    .array-data 1
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 1086
    nop

    :array_5a4e
    .array-data 1
        0x38t
        0x66t
        0x66t
        0x66t
        0x3at
        0x66t
        0x66t
        0x66t
        0x3at
        0x66t
        0x66t
        0x66t
        -0x70t
        0x66t
        0x66t
        0x66t
        0x21t
        0x66t
        0x37t
        0x66t
        0x66t
        0x66t
        0x22t
        0x66t
        0x66t
        0x66t
        0x70t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x0t
        0xft
        0x0t
    .end array-data

    .line 1087
    nop

    :array_5a6c
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
    .end array-data

    .line 1090
    nop

    :array_5a8a
    .array-data 1
        0x12t
        0x12t
        0x21t
        0x53t
        0x21t
        0x66t
        0x32t
        0x66t
        0x66t
        0x0t
        0xft
        0x2t
        0x12t
        0x1t
        0x12t
        0x0t
        0x21t
        0x66t
        0x35t
        0x66t
        0x66t
        0x0t
        0x48t
        0x66t
        0x5t
        0x0t
    .end array-data

    .line 1091
    nop

    :array_5a9c
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
    .end array-data

    .line 1092
    nop

    :array_5aae
    .array-data 1
        0x12t
        0x2t
        0x21t
        0x53t
        0x21t
        0x66t
        0x32t
        0x66t
        0x66t
        0x0t
        0xft
        0x2t
        0x12t
        0x1t
        0x12t
        0x0t
        0x21t
        0x66t
        0x35t
        0x66t
        0x66t
        0x0t
        0x48t
        0x66t
        0x5t
        0x0t
    .end array-data

    .line 1093
    nop

    :array_5ac0
    .array-data 1
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 1096
    nop

    :array_5ad2
    .array-data 1
        -0xet
        0x20t
        0x66t
        0x66t
        0x12t
        0x31t
        0x32t
        0x10t
        0xat
        0x0t
        0x22t
        0x0t
        0x66t
        0x66t
        0x1at
        0x1t
        0x66t
        0x66t
        0x70t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x0t
        -0x8t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x12t
        0x10t
        0xft
        0x0t
    .end array-data

    .line 1097
    :array_5ae8
    .array-data 1
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 1098
    :array_5afe
    .array-data 1
        -0xet
        0x20t
        0x66t
        0x66t
        0x12t
        0x31t
        0x32t
        0x10t
        0xat
        0x0t
        0x22t
        0x0t
        0x66t
        0x66t
        0x1at
        0x1t
        0x66t
        0x66t
        0x70t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0x27t
        0x0t
        -0x8t
        0x20t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x0t
        0xft
        0x0t
    .end array-data

    .line 1099
    :array_5b14
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
    .end array-data
.end method

.method public static unzip(Ljava/io/File;Ljava/lang/String;)V
    .registers 20
    .param p0, "apk"    # Ljava/io/File;
    .param p1, "dirr"    # Ljava/lang/String;

    .prologue
    .line 2785
    const/4 v6, 0x0

    .line 2786
    .local v6, "found1":Z
    move-object/from16 v2, p1

    .line 2788
    .local v2, "dir":Ljava/lang/String;
    :try_start_3
    new-instance v5, Ljava/io/FileInputStream;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 2789
    .local v5, "fin":Ljava/io/FileInputStream;
    new-instance v12, Ljava/util/zip/ZipInputStream;

    invoke-direct {v12, v5}, Ljava/util/zip/ZipInputStream;-><init>(Ljava/io/InputStream;)V

    .line 2790
    .local v12, "zin":Ljava/util/zip/ZipInputStream;
    const/4 v11, 0x0

    .line 2792
    .local v11, "ze":Ljava/util/zip/ZipEntry;
    invoke-virtual {v12}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    move-result-object v11

    .line 2793
    const/4 v10, 0x1

    .line 2794
    .local v10, "search":Z
    :goto_15
    if-eqz v11, :cond_111

    if-eqz v10, :cond_111

    .line 2800
    invoke-virtual {v11}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v8

    .line 2802
    .local v8, "haystack":Ljava/lang/String;
    const-string v14, "classes.dex"

    invoke-virtual {v8, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_10e

    .line 2804
    new-instance v7, Ljava/io/FileOutputStream;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "/"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "classes.dex"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v7, v14}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 2806
    .local v7, "fout":Ljava/io/FileOutputStream;
    const/16 v14, 0x800

    new-array v1, v14, [B

    .line 2809
    .local v1, "buffer":[B
    :goto_47
    invoke-virtual {v12, v1}, Ljava/util/zip/ZipInputStream;->read([B)I

    move-result v9

    .local v9, "length":I
    const/4 v14, -0x1

    if-eq v9, v14, :cond_7d

    .line 2810
    const/4 v14, 0x0

    invoke-virtual {v7, v1, v14, v9}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_52
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_52} :catch_53

    goto :goto_47

    .line 2829
    .end local v1    # "buffer":[B
    .end local v5    # "fin":Ljava/io/FileInputStream;
    .end local v7    # "fout":Ljava/io/FileOutputStream;
    .end local v8    # "haystack":Ljava/lang/String;
    .end local v9    # "length":I
    .end local v10    # "search":Z
    .end local v11    # "ze":Ljava/util/zip/ZipEntry;
    .end local v12    # "zin":Ljava/util/zip/ZipInputStream;
    :catch_53
    move-exception v3

    .line 2831
    .local v3, "e":Ljava/lang/Exception;
    :try_start_54
    new-instance v13, Lnet/lingala/zip4j/core/ZipFile;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Lnet/lingala/zip4j/core/ZipFile;-><init>(Ljava/io/File;)V

    .line 2835
    .local v13, "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    const-string v14, "classes.dex"

    invoke-virtual {v13, v14, v2}, Lnet/lingala/zip4j/core/ZipFile;->extractFile(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_60
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_54 .. :try_end_60} :catch_11f
    .catch Ljava/lang/Exception; {:try_start_54 .. :try_end_60} :catch_156

    .line 2845
    .end local v13    # "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    :goto_60
    sget-object v14, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Exception e"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v3}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2849
    .end local v3    # "e":Ljava/lang/Exception;
    :goto_7c
    return-void

    .line 2813
    .restart local v1    # "buffer":[B
    .restart local v5    # "fin":Ljava/io/FileInputStream;
    .restart local v7    # "fout":Ljava/io/FileOutputStream;
    .restart local v8    # "haystack":Ljava/lang/String;
    .restart local v9    # "length":I
    .restart local v10    # "search":Z
    .restart local v11    # "ze":Ljava/util/zip/ZipEntry;
    .restart local v12    # "zin":Ljava/util/zip/ZipInputStream;
    :cond_7d
    const/4 v14, 0x3

    :try_start_7e
    new-array v14, v14, [Ljava/lang/String;

    const/4 v15, 0x0

    const-string v16, "chmod"

    aput-object v16, v14, v15

    const/4 v15, 0x1

    const-string v16, "777"

    aput-object v16, v14, v15

    const/4 v15, 0x2

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "/"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "classes.dex"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-static {v14}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 2814
    const/4 v14, 0x3

    new-array v14, v14, [Ljava/lang/String;

    const/4 v15, 0x0

    const-string v16, "chown"

    aput-object v16, v14, v15

    const/4 v15, 0x1

    const-string v16, "0.0"

    aput-object v16, v14, v15

    const/4 v15, 0x2

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "/"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "classes.dex"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-static {v14}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 2815
    const/4 v14, 0x3

    new-array v14, v14, [Ljava/lang/String;

    const/4 v15, 0x0

    const-string v16, "chown"

    aput-object v16, v14, v15

    const/4 v15, 0x1

    const-string v16, "0:0"

    aput-object v16, v14, v15

    const/4 v15, 0x2

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "/"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "classes.dex"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-static {v14}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 2816
    invoke-virtual {v12}, Ljava/util/zip/ZipInputStream;->closeEntry()V

    .line 2817
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V

    .line 2818
    const/4 v6, 0x1

    .line 2820
    .end local v1    # "buffer":[B
    .end local v7    # "fout":Ljava/io/FileOutputStream;
    .end local v9    # "length":I
    :cond_10e
    if-eqz v6, :cond_119

    .line 2821
    const/4 v10, 0x0

    .line 2827
    .end local v8    # "haystack":Ljava/lang/String;
    :cond_111
    invoke-virtual {v12}, Ljava/util/zip/ZipInputStream;->close()V

    .line 2828
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V

    goto/16 :goto_7c

    .line 2824
    .restart local v8    # "haystack":Ljava/lang/String;
    :cond_119
    invoke-virtual {v12}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;
    :try_end_11c
    .catch Ljava/lang/Exception; {:try_start_7e .. :try_end_11c} :catch_53

    move-result-object v11

    .line 2826
    goto/16 :goto_15

    .line 2838
    .end local v5    # "fin":Ljava/io/FileInputStream;
    .end local v8    # "haystack":Ljava/lang/String;
    .end local v10    # "search":Z
    .end local v11    # "ze":Ljava/util/zip/ZipEntry;
    .end local v12    # "zin":Ljava/util/zip/ZipInputStream;
    .restart local v3    # "e":Ljava/lang/Exception;
    :catch_11f
    move-exception v4

    .line 2839
    .local v4, "e1":Lnet/lingala/zip4j/exception/ZipException;
    sget-object v14, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Error classes.dex decompress! "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2840
    sget-object v14, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Exception e1"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v3}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_60

    .line 2841
    .end local v4    # "e1":Lnet/lingala/zip4j/exception/ZipException;
    :catch_156
    move-exception v4

    .line 2842
    .local v4, "e1":Ljava/lang/Exception;
    sget-object v14, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Error classes.dex decompress! "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2843
    sget-object v14, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Exception e1"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v3}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_60
.end method
