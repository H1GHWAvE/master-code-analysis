.class public Lcom/chelpus/root/utils/liverunpatch;
.super Ljava/lang/Object;
.source "liverunpatch.java"


# static fields
.field private static dalvikDexIn:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 14
    const-string v0, "/data/dalvik-cache/data@app@zamenitetodelo-1.apk@classes.dex"

    sput-object v0, Lcom/chelpus/root/utils/liverunpatch;->dalvikDexIn:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .registers 30
    .param p0, "paramArrayOfString"    # [Ljava/lang/String;

    .prologue
    .line 18
    new-instance v2, Lcom/chelpus/root/utils/liverunpatch$1;

    invoke-direct {v2}, Lcom/chelpus/root/utils/liverunpatch$1;-><init>()V

    invoke-static {v2}, Lcom/chelpus/Utils;->startRootJava(Ljava/lang/Object;)V

    .line 23
    const/4 v2, 0x1

    aget-object v2, p0, v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v23

    .line 24
    .local v23, "orhex":[Ljava/lang/String;
    const/4 v2, 0x2

    aget-object v2, p0, v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v27

    .line 25
    .local v27, "rephex":[Ljava/lang/String;
    move-object/from16 v0, v23

    array-length v2, v0

    new-array v7, v2, [B

    .line 26
    .local v7, "byteOrig":[B
    move-object/from16 v0, v27

    array-length v2, v0

    new-array v8, v2, [B

    .line 27
    .local v8, "byteReplace":[B
    move-object/from16 v0, v23

    array-length v2, v0

    new-array v0, v2, [B

    move-object/from16 v22, v0

    .line 28
    .local v22, "mask":[B
    move-object/from16 v0, v27

    array-length v2, v0

    new-array v0, v2, [B

    move-object/from16 v26, v0

    .line 30
    .local v26, "rep_mask":[B
    const/4 v14, 0x0

    .line 32
    .local v14, "error":Z
    move-object/from16 v0, v26

    array-length v2, v0

    move-object/from16 v0, v22

    array-length v3, v0

    if-ne v2, v3, :cond_251

    array-length v2, v7

    array-length v3, v8

    if-ne v2, v3, :cond_251

    array-length v2, v8

    const/4 v3, 0x3

    if-le v2, v3, :cond_251

    array-length v2, v7

    const/4 v3, 0x3

    if-le v2, v3, :cond_251

    .line 35
    const/16 v28, 0x0

    .local v28, "t":I
    :goto_49
    move-object/from16 v0, v23

    array-length v2, v0

    move/from16 v0, v28

    if-ge v0, v2, :cond_e0

    .line 38
    aget-object v2, v23, v28

    const-string v3, "*"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_69

    aget-object v2, v23, v28

    const-string v3, "**"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_69

    const/4 v14, 0x1

    const-string v2, "00"

    aput-object v2, v23, v28

    .line 39
    :cond_69
    aget-object v2, v27, v28

    const-string v3, "*"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_82

    aget-object v2, v27, v28

    const-string v3, "**"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_82

    const/4 v14, 0x1

    const-string v2, "00"

    aput-object v2, v27, v28

    .line 40
    :cond_82
    aget-object v2, v23, v28

    const-string v3, "**"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_96

    aget-object v2, v23, v28

    const-string v3, "??"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_d8

    :cond_96
    const-string v2, "00"

    aput-object v2, v23, v28

    const/4 v2, 0x1

    aput-byte v2, v22, v28

    .line 41
    :goto_9d
    aget-object v2, v23, v28

    const/16 v3, 0x10

    invoke-static {v2, v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->byteValue()B

    move-result v2

    aput-byte v2, v7, v28

    .line 43
    aget-object v2, v27, v28

    const-string v3, "**"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_bf

    aget-object v2, v27, v28

    const-string v3, "??"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_dc

    :cond_bf
    const-string v2, "00"

    aput-object v2, v27, v28

    const/4 v2, 0x0

    aput-byte v2, v26, v28

    .line 44
    :goto_c6
    aget-object v2, v27, v28

    const/16 v3, 0x10

    invoke-static {v2, v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->byteValue()B

    move-result v2

    aput-byte v2, v8, v28

    .line 35
    add-int/lit8 v28, v28, 0x1

    goto/16 :goto_49

    .line 40
    :cond_d8
    const/4 v2, 0x0

    aput-byte v2, v22, v28

    goto :goto_9d

    .line 43
    :cond_dc
    const/4 v2, 0x1

    aput-byte v2, v26, v28

    goto :goto_c6

    .line 52
    :cond_e0
    if-nez v14, :cond_248

    .line 55
    sget-object v2, Lcom/chelpus/root/utils/liverunpatch;->dalvikDexIn:Ljava/lang/String;

    const-string v3, "zamenitetodelo"

    const/4 v4, 0x0

    aget-object v4, p0, v4

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v11

    .line 59
    .local v11, "dalvikDex":Ljava/lang/String;
    :try_start_ed
    new-instance v19, Ljava/io/File;

    move-object/from16 v0, v19

    invoke-direct {v0, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 60
    .local v19, "localFile1":Ljava/io/File;
    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_109

    new-instance v19, Ljava/io/File;

    .end local v19    # "localFile1":Ljava/io/File;
    const-string v2, "-1"

    const-string v3, "-2"

    invoke-virtual {v11, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 61
    .restart local v19    # "localFile1":Ljava/io/File;
    :cond_109
    new-instance v20, Ljava/io/File;

    const-string v2, "-1"

    const-string v3, ""

    invoke-virtual {v11, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v20

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 62
    .local v20, "localFile2":Ljava/io/File;
    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_120

    move-object/from16 v20, v19

    .line 63
    :cond_120
    const-string v2, "data@app"

    const-string v3, "mnt@asec"

    invoke-virtual {v11, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v12

    .line 64
    .local v12, "dalvikDexTemp":Ljava/lang/String;
    const-string v2, ".apk@classes.dex"

    const-string v3, "@pkg.apk@classes.dex"

    invoke-virtual {v12, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v12

    .line 65
    new-instance v19, Ljava/io/File;

    .end local v19    # "localFile1":Ljava/io/File;
    move-object/from16 v0, v19

    invoke-direct {v0, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 66
    .restart local v19    # "localFile1":Ljava/io/File;
    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_13f

    move-object/from16 v20, v19

    .line 67
    :cond_13f
    new-instance v19, Ljava/io/File;

    .end local v19    # "localFile1":Ljava/io/File;
    const-string v2, "-1"

    const-string v3, "-2"

    invoke-virtual {v12, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 68
    .restart local v19    # "localFile1":Ljava/io/File;
    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_156

    move-object/from16 v20, v19

    .line 69
    :cond_156
    new-instance v19, Ljava/io/File;

    .end local v19    # "localFile1":Ljava/io/File;
    const-string v2, "-1"

    const-string v3, ""

    invoke-virtual {v12, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 70
    .restart local v19    # "localFile1":Ljava/io/File;
    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_16d

    move-object/from16 v20, v19

    .line 71
    :cond_16d
    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_185

    new-instance v2, Ljava/io/FileNotFoundException;

    invoke-direct {v2}, Ljava/io/FileNotFoundException;-><init>()V

    throw v2
    :try_end_179
    .catch Ljava/io/FileNotFoundException; {:try_start_ed .. :try_end_179} :catch_179
    .catch Ljava/lang/Exception; {:try_start_ed .. :try_end_179} :catch_245

    .line 126
    .end local v12    # "dalvikDexTemp":Ljava/lang/String;
    .end local v19    # "localFile1":Ljava/io/File;
    .end local v20    # "localFile2":Ljava/io/File;
    :catch_179
    move-exception v21

    .line 127
    .local v21, "localFileNotFoundException":Ljava/io/FileNotFoundException;
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "Error: Program files are not found!\n\nCheck the location dalvik-cache to solve problems!\n\nDefault: /data/dalvik-cache/*"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 140
    .end local v11    # "dalvikDex":Ljava/lang/String;
    .end local v21    # "localFileNotFoundException":Ljava/io/FileNotFoundException;
    .end local v28    # "t":I
    :cond_181
    :goto_181
    invoke-static {}, Lcom/chelpus/Utils;->exitFromRootJava()V

    .line 141
    return-void

    .line 73
    .restart local v11    # "dalvikDex":Ljava/lang/String;
    .restart local v12    # "dalvikDexTemp":Ljava/lang/String;
    .restart local v19    # "localFile1":Ljava/io/File;
    .restart local v20    # "localFile2":Ljava/io/File;
    .restart local v28    # "t":I
    :cond_185
    :try_start_185
    new-instance v2, Ljava/io/RandomAccessFile;

    const-string v3, "rw"

    move-object/from16 v0, v20

    invoke-direct {v2, v0, v3}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v1

    .line 74
    .local v1, "ChannelDex":Ljava/nio/channels/FileChannel;
    sget-object v2, Ljava/nio/channels/FileChannel$MapMode;->READ_WRITE:Ljava/nio/channels/FileChannel$MapMode;

    const-wide/16 v3, 0x0

    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v5

    long-to-int v5, v5

    int-to-long v5, v5

    invoke-virtual/range {v1 .. v6}, Ljava/nio/channels/FileChannel;->map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;
    :try_end_19f
    .catch Ljava/io/FileNotFoundException; {:try_start_185 .. :try_end_19f} :catch_179
    .catch Ljava/lang/Exception; {:try_start_185 .. :try_end_19f} :catch_245

    move-result-object v15

    .line 75
    .local v15, "fileBytes":Ljava/nio/MappedByteBuffer;
    const/16 v24, 0x0

    .line 80
    .local v24, "patch":Z
    const-wide/16 v17, 0x0

    .local v17, "j":J
    :goto_1a4
    :try_start_1a4
    invoke-virtual {v15}, Ljava/nio/MappedByteBuffer;->hasRemaining()Z

    move-result v2

    if-eqz v2, :cond_237

    .line 83
    invoke-virtual {v15}, Ljava/nio/MappedByteBuffer;->position()I

    move-result v10

    .line 84
    .local v10, "curentPos":I
    invoke-virtual {v15}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v9

    .line 88
    .local v9, "curentByte":B
    const/4 v2, 0x0

    aget-byte v2, v7, v2

    if-ne v9, v2, :cond_20f

    .line 90
    const/4 v2, 0x0

    aget-byte v2, v26, v2

    if-nez v2, :cond_1bf

    const/4 v2, 0x0

    aput-byte v9, v8, v2

    .line 91
    :cond_1bf
    const/16 v16, 0x1

    .line 92
    .local v16, "i":I
    add-int/lit8 v2, v10, 0x1

    invoke-virtual {v15, v2}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 93
    invoke-virtual {v15}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v25

    .line 95
    .local v25, "prufbyte":B
    :goto_1ca
    aget-byte v2, v7, v16

    move/from16 v0, v25

    if-eq v0, v2, :cond_1d5

    aget-byte v2, v22, v16

    const/4 v3, 0x1

    if-ne v2, v3, :cond_20f

    .line 97
    :cond_1d5
    aget-byte v2, v26, v16

    if-nez v2, :cond_1db

    aput-byte v25, v8, v16

    .line 98
    :cond_1db
    add-int/lit8 v16, v16, 0x1

    .line 100
    array-length v2, v7

    move/from16 v0, v16

    if-ne v0, v2, :cond_219

    .line 102
    invoke-virtual {v15, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 103
    invoke-virtual {v15, v8}, Ljava/nio/MappedByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 104
    invoke-virtual {v15}, Ljava/nio/MappedByteBuffer;->force()Ljava/nio/MappedByteBuffer;

    .line 106
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Offset in file:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v10}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " - Patch done!\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 107
    const/16 v24, 0x1

    .line 117
    .end local v16    # "i":I
    .end local v25    # "prufbyte":B
    :cond_20f
    add-int/lit8 v2, v10, 0x1

    invoke-virtual {v15, v2}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 80
    const-wide/16 v2, 0x1

    add-long v17, v17, v2

    goto :goto_1a4

    .line 112
    .restart local v16    # "i":I
    .restart local v25    # "prufbyte":B
    :cond_219
    invoke-virtual {v15}, Ljava/nio/MappedByteBuffer;->get()B
    :try_end_21c
    .catch Ljava/lang/Exception; {:try_start_1a4 .. :try_end_21c} :catch_21e
    .catch Ljava/io/FileNotFoundException; {:try_start_1a4 .. :try_end_21c} :catch_179

    move-result v25

    goto :goto_1ca

    .line 119
    .end local v9    # "curentByte":B
    .end local v10    # "curentPos":I
    .end local v16    # "i":I
    .end local v25    # "prufbyte":B
    :catch_21e
    move-exception v13

    .line 120
    .local v13, "e":Ljava/lang/Exception;
    :try_start_21f
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 122
    .end local v13    # "e":Ljava/lang/Exception;
    :cond_237
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->close()V

    .line 124
    if-nez v24, :cond_181

    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "Error: Pattern not found!\nor patch is already applied?!\n"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_243
    .catch Ljava/io/FileNotFoundException; {:try_start_21f .. :try_end_243} :catch_179
    .catch Ljava/lang/Exception; {:try_start_21f .. :try_end_243} :catch_245

    goto/16 :goto_181

    .line 129
    .end local v1    # "ChannelDex":Ljava/nio/channels/FileChannel;
    .end local v12    # "dalvikDexTemp":Ljava/lang/String;
    .end local v15    # "fileBytes":Ljava/nio/MappedByteBuffer;
    .end local v17    # "j":J
    .end local v19    # "localFile1":Ljava/io/File;
    .end local v20    # "localFile2":Ljava/io/File;
    .end local v24    # "patch":Z
    :catch_245
    move-exception v2

    goto/16 :goto_181

    .line 133
    .end local v11    # "dalvikDex":Ljava/lang/String;
    :cond_248
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "Error: Pattern not valid!\n\nPattern can not be \"*4\" or \"A*\", etc.\n\n It can only be ** "

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_181

    .line 137
    .end local v28    # "t":I
    :cond_251
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "Error: Original & Replace hex-string not valid!\n\nOriginal.hex.length != Replacmant.hex.length.\nOR\nLength of hex-string < 4"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_181
.end method
