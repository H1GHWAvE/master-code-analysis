.class public Lcom/chelpus/root/utils/runpatchsupportOld;
.super Ljava/lang/Object;
.source "runpatchsupportOld.java"


# static fields
.field private static ART:Z

.field public static appdir:Ljava/lang/String;

.field public static classesFiles:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private static copyDC:Z

.field private static createAPK:Z

.field public static crkapk:Ljava/io/File;

.field public static dir:Ljava/lang/String;

.field public static dir2:Ljava/lang/String;

.field public static dirapp:Ljava/lang/String;

.field public static filestopatch:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private static pattern1:Z

.field private static pattern2:Z

.field private static pattern3:Z

.field public static print:Ljava/io/PrintStream;

.field public static result:Ljava/lang/String;

.field public static sddir:Ljava/lang/String;

.field public static system:Z

.field public static uid:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 36
    sput-boolean v1, Lcom/chelpus/root/utils/runpatchsupportOld;->createAPK:Z

    .line 37
    sput-boolean v0, Lcom/chelpus/root/utils/runpatchsupportOld;->pattern1:Z

    .line 38
    sput-boolean v0, Lcom/chelpus/root/utils/runpatchsupportOld;->pattern2:Z

    .line 39
    sput-boolean v0, Lcom/chelpus/root/utils/runpatchsupportOld;->pattern3:Z

    .line 40
    sput-boolean v1, Lcom/chelpus/root/utils/runpatchsupportOld;->copyDC:Z

    .line 41
    sput-boolean v1, Lcom/chelpus/root/utils/runpatchsupportOld;->ART:Z

    .line 42
    const-string v0, "/data/app/"

    sput-object v0, Lcom/chelpus/root/utils/runpatchsupportOld;->dirapp:Ljava/lang/String;

    .line 43
    sput-boolean v1, Lcom/chelpus/root/utils/runpatchsupportOld;->system:Z

    .line 44
    const-string v0, ""

    sput-object v0, Lcom/chelpus/root/utils/runpatchsupportOld;->uid:Ljava/lang/String;

    .line 45
    const-string v0, "/sdcard/"

    sput-object v0, Lcom/chelpus/root/utils/runpatchsupportOld;->dir:Ljava/lang/String;

    .line 46
    const-string v0, "/sdcard/"

    sput-object v0, Lcom/chelpus/root/utils/runpatchsupportOld;->dir2:Ljava/lang/String;

    .line 47
    const/4 v0, 0x0

    sput-object v0, Lcom/chelpus/root/utils/runpatchsupportOld;->filestopatch:Ljava/util/ArrayList;

    .line 48
    const-string v0, "/sdcard/"

    sput-object v0, Lcom/chelpus/root/utils/runpatchsupportOld;->sddir:Ljava/lang/String;

    .line 49
    const-string v0, "/sdcard/"

    sput-object v0, Lcom/chelpus/root/utils/runpatchsupportOld;->appdir:Ljava/lang/String;

    .line 54
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/chelpus/root/utils/runpatchsupportOld;->classesFiles:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static byteverify(Ljava/nio/MappedByteBuffer;IB[B[B[B[BLjava/lang/String;Z)Z
    .registers 14
    .param p0, "fileBytes"    # Ljava/nio/MappedByteBuffer;
    .param p1, "curentPos"    # I
    .param p2, "curentByte"    # B
    .param p3, "byteOrig"    # [B
    .param p4, "mask"    # [B
    .param p5, "byteReplace"    # [B
    .param p6, "rep_mask"    # [B
    .param p7, "log"    # Ljava/lang/String;
    .param p8, "pattern"    # Z

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 940
    aget-byte v4, p3, v3

    if-ne p2, v4, :cond_42

    if-eqz p8, :cond_42

    .line 942
    aget-byte v4, p6, v3

    if-nez v4, :cond_e

    aput-byte p2, p5, v3

    .line 943
    :cond_e
    const/4 v0, 0x1

    .line 944
    .local v0, "i":I
    add-int/lit8 v4, p1, 0x1

    invoke-virtual {p0, v4}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 945
    invoke-virtual {p0}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v1

    .line 947
    .local v1, "prufbyte":B
    :goto_18
    aget-byte v4, p3, v0

    if-eq v1, v4, :cond_20

    aget-byte v4, p4, v0

    if-ne v4, v2, :cond_3d

    .line 949
    :cond_20
    aget-byte v4, p6, v0

    if-nez v4, :cond_26

    aput-byte v1, p5, v0

    .line 950
    :cond_26
    add-int/lit8 v0, v0, 0x1

    .line 952
    array-length v4, p3

    if-ne v0, v4, :cond_38

    .line 954
    invoke-virtual {p0, p1}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 955
    invoke-virtual {p0, p5}, Ljava/nio/MappedByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 956
    invoke-virtual {p0}, Ljava/nio/MappedByteBuffer;->force()Ljava/nio/MappedByteBuffer;

    .line 958
    invoke-static {p7}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 967
    .end local v0    # "i":I
    .end local v1    # "prufbyte":B
    :goto_37
    return v2

    .line 963
    .restart local v0    # "i":I
    .restart local v1    # "prufbyte":B
    :cond_38
    invoke-virtual {p0}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v1

    goto :goto_18

    .line 965
    :cond_3d
    add-int/lit8 v2, p1, 0x1

    invoke-virtual {p0, v2}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .end local v0    # "i":I
    .end local v1    # "prufbyte":B
    :cond_42
    move v2, v3

    .line 967
    goto :goto_37
.end method

.method private static final calcChecksum([BI)V
    .registers 7
    .param p0, "paramArrayOfByte"    # [B
    .param p1, "j"    # I

    .prologue
    .line 883
    new-instance v1, Ljava/util/zip/Adler32;

    invoke-direct {v1}, Ljava/util/zip/Adler32;-><init>()V

    .line 884
    .local v1, "localAdler32":Ljava/util/zip/Adler32;
    const/16 v2, 0xc

    array-length v3, p0

    add-int/lit8 v4, p1, 0xc

    sub-int/2addr v3, v4

    invoke-virtual {v1, p0, v2, v3}, Ljava/util/zip/Adler32;->update([BII)V

    .line 885
    invoke-virtual {v1}, Ljava/util/zip/Adler32;->getValue()J

    move-result-wide v2

    long-to-int v0, v2

    .line 886
    .local v0, "i":I
    add-int/lit8 v2, p1, 0x8

    int-to-byte v3, v0

    aput-byte v3, p0, v2

    .line 887
    add-int/lit8 v2, p1, 0x9

    shr-int/lit8 v3, v0, 0x8

    int-to-byte v3, v3

    aput-byte v3, p0, v2

    .line 888
    add-int/lit8 v2, p1, 0xa

    shr-int/lit8 v3, v0, 0x10

    int-to-byte v3, v3

    aput-byte v3, p0, v2

    .line 889
    add-int/lit8 v2, p1, 0xb

    shr-int/lit8 v3, v0, 0x18

    int-to-byte v3, v3

    aput-byte v3, p0, v2

    .line 890
    return-void
.end method

.method private static final calcSignature([BI)V
    .registers 10
    .param p0, "paramArrayOfByte"    # [B
    .param p1, "j"    # I

    .prologue
    const/16 v7, 0x14

    .line 896
    :try_start_2
    const-string v4, "SHA-1"

    invoke-static {v4}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_7
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_2 .. :try_end_7} :catch_41

    move-result-object v2

    .line 903
    .local v2, "localMessageDigest":Ljava/security/MessageDigest;
    const/16 v4, 0x20

    array-length v5, p0

    add-int/lit8 v6, p1, 0x20

    sub-int/2addr v5, v6

    invoke-virtual {v2, p0, v4, v5}, Ljava/security/MessageDigest;->update([BII)V

    .line 906
    add-int/lit8 v4, p1, 0xc

    const/16 v5, 0x14

    :try_start_15
    invoke-virtual {v2, p0, v4, v5}, Ljava/security/MessageDigest;->digest([BII)I

    move-result v0

    .line 907
    .local v0, "i":I
    if-eq v0, v7, :cond_48

    .line 908
    new-instance v4, Ljava/lang/RuntimeException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "unexpected digest write:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "bytes"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_3a
    .catch Ljava/security/DigestException; {:try_start_15 .. :try_end_3a} :catch_3a

    .line 910
    .end local v0    # "i":I
    :catch_3a
    move-exception v1

    .line 912
    .local v1, "localDigestException":Ljava/security/DigestException;
    new-instance v4, Ljava/lang/RuntimeException;

    invoke-direct {v4, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v4

    .line 898
    .end local v1    # "localDigestException":Ljava/security/DigestException;
    .end local v2    # "localMessageDigest":Ljava/security/MessageDigest;
    :catch_41
    move-exception v3

    .line 900
    .local v3, "localNoSuchAlgorithmException":Ljava/security/NoSuchAlgorithmException;
    new-instance v4, Ljava/lang/RuntimeException;

    invoke-direct {v4, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v4

    .line 914
    .end local v3    # "localNoSuchAlgorithmException":Ljava/security/NoSuchAlgorithmException;
    .restart local v0    # "i":I
    .restart local v2    # "localMessageDigest":Ljava/security/MessageDigest;
    :cond_48
    return-void
.end method

.method public static clearTemp()V
    .registers 6

    .prologue
    .line 919
    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/chelpus/root/utils/runpatchsupportOld;->dir:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/AndroidManifest.xml"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 920
    .local v3, "tmp":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 921
    .local v2, "tempdex":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_23

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 922
    :cond_23
    sget-object v4, Lcom/chelpus/root/utils/runpatchsupportOld;->classesFiles:Ljava/util/ArrayList;

    if-eqz v4, :cond_67

    sget-object v4, Lcom/chelpus/root/utils/runpatchsupportOld;->classesFiles:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_67

    .line 923
    sget-object v4, Lcom/chelpus/root/utils/runpatchsupportOld;->classesFiles:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_35
    :goto_35
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_67

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 924
    .local v0, "cl":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_35

    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_4a
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_4a} :catch_4b

    goto :goto_35

    .line 933
    .end local v0    # "cl":Ljava/io/File;
    .end local v2    # "tempdex":Ljava/io/File;
    :catch_4b
    move-exception v1

    .line 935
    .local v1, "e":Ljava/lang/Exception;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 937
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_66
    :goto_66
    return-void

    .line 927
    .restart local v2    # "tempdex":Ljava/io/File;
    :cond_67
    :try_start_67
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/chelpus/root/utils/runpatchsupportOld;->dir:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/classes.dex"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 928
    new-instance v2, Ljava/io/File;

    .end local v2    # "tempdex":Ljava/io/File;
    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 929
    .restart local v2    # "tempdex":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_8a

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 930
    :cond_8a
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/chelpus/root/utils/runpatchsupportOld;->dir:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/classes.dex.apk"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 931
    new-instance v2, Ljava/io/File;

    .end local v2    # "tempdex":Ljava/io/File;
    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 932
    .restart local v2    # "tempdex":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_66

    invoke-virtual {v2}, Ljava/io/File;->delete()Z
    :try_end_ad
    .catch Ljava/lang/Exception; {:try_start_67 .. :try_end_ad} :catch_4b

    goto :goto_66
.end method

.method public static clearTempSD()V
    .registers 5

    .prologue
    .line 1131
    :try_start_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/chelpus/root/utils/runpatchsupportOld;->sddir:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/Modified/classes.dex.apk"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1132
    .local v2, "tmp":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1133
    .local v1, "tempdex":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_23

    invoke-virtual {v1}, Ljava/io/File;->delete()Z
    :try_end_23
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_23} :catch_24

    .line 1138
    .end local v1    # "tempdex":Ljava/io/File;
    :cond_23
    :goto_23
    return-void

    .line 1134
    :catch_24
    move-exception v0

    .line 1136
    .local v0, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    goto :goto_23
.end method

.method public static fixadler(Ljava/io/File;)V
    .registers 6
    .param p0, "destFile"    # Ljava/io/File;

    .prologue
    .line 865
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 866
    .local v2, "localFileInputStream":Ljava/io/FileInputStream;
    invoke-virtual {v2}, Ljava/io/FileInputStream;->available()I

    move-result v4

    new-array v0, v4, [B

    .line 867
    .local v0, "arrayOfByte":[B
    invoke-virtual {v2, v0}, Ljava/io/FileInputStream;->read([B)I

    .line 868
    const/4 v4, 0x0

    invoke-static {v0, v4}, Lcom/chelpus/root/utils/runpatchsupportOld;->calcSignature([BI)V

    .line 869
    const/4 v4, 0x0

    invoke-static {v0, v4}, Lcom/chelpus/root/utils/runpatchsupportOld;->calcChecksum([BI)V

    .line 870
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V

    .line 872
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, p0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 873
    .local v3, "localFileOutputStream":Ljava/io/FileOutputStream;
    invoke-virtual {v3, v0}, Ljava/io/FileOutputStream;->write([B)V

    .line 874
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_24
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_24} :catch_25

    .line 880
    .end local v0    # "arrayOfByte":[B
    .end local v2    # "localFileInputStream":Ljava/io/FileInputStream;
    .end local v3    # "localFileOutputStream":Ljava/io/FileOutputStream;
    :goto_24
    return-void

    .line 876
    :catch_25
    move-exception v1

    .line 878
    .local v1, "localException":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_24
.end method

.method public static main([Ljava/lang/String;)V
    .registers 70
    .param p0, "paramArrayOfString"    # [Ljava/lang/String;

    .prologue
    .line 58
    new-instance v54, Lcom/android/vending/billing/InAppBillingService/LUCK/LogOutputStream;

    const-string v10, "System.out"

    move-object/from16 v0, v54

    invoke-direct {v0, v10}, Lcom/android/vending/billing/InAppBillingService/LUCK/LogOutputStream;-><init>(Ljava/lang/String;)V

    .line 59
    .local v54, "pout":Lcom/android/vending/billing/InAppBillingService/LUCK/LogOutputStream;
    new-instance v10, Ljava/io/PrintStream;

    move-object/from16 v0, v54

    invoke-direct {v10, v0}, Ljava/io/PrintStream;-><init>(Ljava/io/OutputStream;)V

    sput-object v10, Lcom/chelpus/root/utils/runpatchsupportOld;->print:Ljava/io/PrintStream;

    .line 61
    new-instance v10, Lcom/chelpus/root/utils/runpatchsupportOld$1;

    invoke-direct {v10}, Lcom/chelpus/root/utils/runpatchsupportOld$1;-><init>()V

    invoke-static {v10}, Lcom/chelpus/Utils;->startRootJava(Ljava/lang/Object;)V

    .line 63
    const/4 v10, 0x0

    aget-object v10, p0, v10

    invoke-static {v10}, Lcom/chelpus/Utils;->kill(Ljava/lang/String;)V

    .line 64
    sget-object v10, Lcom/chelpus/root/utils/runpatchsupportOld;->print:Ljava/io/PrintStream;

    const-string v11, "Support-Code Running!"

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 65
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 67
    .local v2, "patchesList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;>;"
    const/4 v10, 0x1

    sput-boolean v10, Lcom/chelpus/root/utils/runpatchsupportOld;->pattern1:Z

    .line 68
    const/4 v10, 0x1

    sput-boolean v10, Lcom/chelpus/root/utils/runpatchsupportOld;->pattern2:Z

    .line 69
    const/4 v10, 0x1

    sput-boolean v10, Lcom/chelpus/root/utils/runpatchsupportOld;->pattern3:Z

    .line 70
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    sput-object v10, Lcom/chelpus/root/utils/runpatchsupportOld;->filestopatch:Ljava/util/ArrayList;

    .line 72
    :try_start_3c
    new-instance v10, Ljava/io/File;

    const/4 v11, 0x3

    aget-object v11, p0, v11

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v36

    .line 73
    .local v36, "files":[Ljava/io/File;
    move-object/from16 v0, v36

    array-length v11, v0

    const/4 v10, 0x0

    :goto_4c
    if-ge v10, v11, :cond_84

    aget-object v33, v36, v10

    .line 74
    .local v33, "file":Ljava/io/File;
    invoke-virtual/range {v33 .. v33}, Ljava/io/File;->isFile()Z

    move-result v12

    if-eqz v12, :cond_7d

    invoke-virtual/range {v33 .. v33}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v12

    const-string v13, "busybox"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_7d

    invoke-virtual/range {v33 .. v33}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v12

    const-string v13, "reboot"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_7d

    invoke-virtual/range {v33 .. v33}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v12

    const-string v13, "dalvikvm"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_7d

    invoke-virtual/range {v33 .. v33}, Ljava/io/File;->delete()Z
    :try_end_7d
    .catch Ljava/lang/Exception; {:try_start_3c .. :try_end_7d} :catch_80

    .line 73
    :cond_7d
    add-int/lit8 v10, v10, 0x1

    goto :goto_4c

    .line 76
    .end local v33    # "file":Ljava/io/File;
    .end local v36    # "files":[Ljava/io/File;
    :catch_80
    move-exception v28

    .local v28, "e":Ljava/lang/Exception;
    invoke-virtual/range {v28 .. v28}, Ljava/lang/Exception;->printStackTrace()V

    .line 78
    .end local v28    # "e":Ljava/lang/Exception;
    :cond_84
    const/4 v10, 0x1

    :try_start_85
    aget-object v10, p0, v10

    const-string v11, "pattern0"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_92

    const/4 v10, 0x0

    sput-boolean v10, Lcom/chelpus/root/utils/runpatchsupportOld;->pattern1:Z

    .line 79
    :cond_92
    const/4 v10, 0x1

    aget-object v10, p0, v10

    const-string v11, "pattern1"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_a0

    const/4 v10, 0x0

    sput-boolean v10, Lcom/chelpus/root/utils/runpatchsupportOld;->pattern2:Z

    .line 80
    :cond_a0
    const/4 v10, 0x1

    aget-object v10, p0, v10

    const-string v11, "pattern2"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_ae

    const/4 v10, 0x0

    sput-boolean v10, Lcom/chelpus/root/utils/runpatchsupportOld;->pattern3:Z

    .line 81
    :cond_ae
    const/4 v10, 0x6

    aget-object v10, p0, v10

    if-eqz v10, :cond_c1

    const/4 v10, 0x6

    aget-object v10, p0, v10

    const-string v11, "createAPK"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_c1

    const/4 v10, 0x1

    sput-boolean v10, Lcom/chelpus/root/utils/runpatchsupportOld;->createAPK:Z

    .line 82
    :cond_c1
    const/4 v10, 0x6

    aget-object v10, p0, v10

    if-eqz v10, :cond_d4

    const/4 v10, 0x6

    aget-object v10, p0, v10

    const-string v11, "ART"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_d4

    const/4 v10, 0x1

    sput-boolean v10, Lcom/chelpus/root/utils/runpatchsupportOld;->ART:Z

    .line 83
    :cond_d4
    const/4 v10, 0x6

    aget-object v10, p0, v10

    if-eqz v10, :cond_df

    const/4 v10, 0x6

    aget-object v10, p0, v10

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 84
    :cond_df
    const/4 v10, 0x7

    aget-object v10, p0, v10

    if-eqz v10, :cond_e9

    const/4 v10, 0x7

    aget-object v10, p0, v10

    sput-object v10, Lcom/chelpus/root/utils/runpatchsupportOld;->uid:Ljava/lang/String;

    .line 85
    :cond_e9
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "uid:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget-object v12, Lcom/chelpus/root/utils/runpatchsupportOld;->uid:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_103
    .catch Ljava/lang/NullPointerException; {:try_start_85 .. :try_end_103} :catch_1373
    .catch Ljava/lang/Exception; {:try_start_85 .. :try_end_103} :catch_1370

    .line 89
    :goto_103
    const/4 v10, 0x5

    :try_start_104
    aget-object v10, p0, v10

    const-string v11, "copyDC"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_111

    const/4 v10, 0x1

    sput-boolean v10, Lcom/chelpus/root/utils/runpatchsupportOld;->copyDC:Z
    :try_end_111
    .catch Ljava/lang/NullPointerException; {:try_start_104 .. :try_end_111} :catch_136d
    .catch Ljava/lang/Exception; {:try_start_104 .. :try_end_111} :catch_136a

    .line 92
    :cond_111
    :goto_111
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchsupportOld;->createAPK:Z

    if-eqz v10, :cond_11c

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    sput-object v10, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->startUnderRoot:Ljava/lang/Boolean;

    .line 94
    :cond_11c
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 95
    .local v3, "origStr":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 96
    .local v4, "replStr":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 97
    .local v5, "trigger":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Boolean;>;"
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 98
    .local v6, "ResultText":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 99
    .local v7, "markers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 101
    .local v8, "dontConvert":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Boolean;>;"
    const-string v10, "1A ?? FF FF"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 102
    const-string v10, "1A ?? ?? ??"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 103
    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 104
    const-string v10, "(pak intekekt 0)"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 105
    const-string v10, "search_pack"

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 107
    const-string v10, "1B ?? FF FF FF FF"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 108
    const-string v10, "1B ?? ?? ?? ?? ??"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 109
    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 110
    const-string v10, "(pak intekekt 0)"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 111
    const-string v10, "search_pack"

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 113
    const-string v10, "1A ?? FF FF"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 114
    const-string v10, "1A ?? ?? ??"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 115
    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 116
    const-string v10, "(sha intekekt 2)"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 117
    const-string v10, "search"

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 119
    const-string v10, "1B ?? FF FF FF FF"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 120
    const-string v10, "1B ?? ?? ?? ?? ??"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 121
    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 122
    const-string v10, "(sha intekekt 2 32 bit)"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 123
    const-string v10, "search"

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 125
    const-string v10, "0A ?? 39 ?? ?? 00"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 126
    const-string v10, "12 S1 39 ?? ?? 00"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 127
    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 128
    const-string v10, "support2 Fixed!\n(sha intekekt 3)"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 129
    const-string v10, "search"

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 131
    const-string v10, "1A ?? FF FF 6E 20 ?? ?? ?? ?? 0C ??"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 132
    const-string v10, "1A ?? ?? ?? 00 00 00 00 00 00 00 00"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 133
    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 134
    const-string v10, "support1 Fixed!\n(pak intekekt)"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 135
    const-string v10, "search_pack"

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 137
    const-string v10, "1A ?? FF FF 6E 20 ?? ?? ?? ??"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 138
    const-string v10, "1A ?? ?? ?? 00 00 00 00 00 00"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 139
    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 140
    const-string v10, "support1 Fixed!\n(pak intekekt)"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 141
    const-string v10, "search_pack"

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 143
    const-string v10, "1A ?? FF FF 6E 20 ?? ?? ?? ??"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 144
    const-string v10, "1A ?? ?? ?? 00 00 00 00 00 00"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 145
    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 146
    const-string v10, "support1 Fixed!\n(pak intekekt)"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 147
    const-string v10, "search_pack"

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 149
    const-string v10, "1B ?? FF FF FF FF 6E 20 ?? ?? ?? ?? 0C ??"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 150
    const-string v10, "1B ?? ?? ?? ?? ?? 00 00 00 00 00 00 00 00"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 151
    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 152
    const-string v10, "support1 Fixed!\n(pak intekekt 32 bit)"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 153
    const-string v10, "search_pack"

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 155
    const-string v10, "1B ?? FF FF FF FF 6E 20 ?? ?? ?? ??"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 156
    const-string v10, "1B ?? ?? ?? ?? ?? 00 00 00 00 00 00"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 157
    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 158
    const-string v10, "support1 Fixed!\n(pak intekekt 32 bit)"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 159
    const-string v10, "search_pack"

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 161
    const-string v10, "1B ?? FF FF FF FF 6E 20 ?? ?? ?? ??"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 162
    const-string v10, "1B ?? ?? ?? ?? ?? 00 00 00 00 00 00"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 163
    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 164
    const-string v10, "support1 Fixed!\n(pak intekekt 32 bit)"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 165
    const-string v10, "search_pack"

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 168
    const-string v10, "6E 20 FF FF ?? 00 0A ??"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 169
    const-string v10, "6E 20 ?? ?? ?? 00 12 S1"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 170
    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 171
    const-string v10, "support2 Fixed!\n(sha intekekt 4)"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 172
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 177
    const-string v10, "6E 20 FF FF ?? 00"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 178
    const-string v10, "00 00 00 00 00 00"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 179
    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 180
    const-string v10, "support3 Fixed!\n(intent for free)"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 181
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 185
    const-string v10, "0A ?? 39 ?? ?? ??"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 186
    const-string v10, "12 S1 39 ?? ?? ??"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 187
    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 188
    const-string v10, "support1 Fixed!\n(ev1)"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 189
    const-string v10, "search_sign_ver"

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 191
    const-string v10, "0A ?? 38 ?? ?? ??"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 192
    const-string v10, "12 S1 38 ?? ?? ??"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 193
    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 194
    const-string v10, "support1 Fixed!\n(ev1)"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 195
    const-string v10, "search_sign_ver"

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 201
    const-string v10, "1C ?? FF FF"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 202
    const-string v10, "1C ?? ?? ??"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 203
    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 204
    const-string v10, "support1 Fixed!\n(si)"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 205
    const-string v10, "search_sign_ver"

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 224
    const-string v10, "23 ?? ?? ?? 1C ?? ?? ?? 12 ?? 4D ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0C ?? 1F ?? ?? ?? 6E ?? ?? ?? ?? ?? 0A ?? 39 ?? ?? ?? 1A ?? ?? ?? 1A"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 225
    const-string v10, "23 ?? ?? ?? 1C ?? ?? ?? 12 ?? 4D ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0C ?? 1F ?? ?? ?? 6E ?? ?? ?? ?? ?? 12 S1 39 ?? ?? ?? 1A ?? ?? ?? 1A"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 226
    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 227
    const-string v10, "support3 Fixed!\n(s)"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 228
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 231
    const-string v10, "1A ?? ?? ?? 6E ?? ?? ?? ?? ?? 54 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 12 ?? 6E ?? ?? ?? ?? ?? 0C ?? 38 ?? ?? ?? 72 ?? ?? ?? ?? ?? 0A ?? 39 ?? ?? ?? 54 ?? ?? ?? 54 ?? ?? ?? 12 ?? 6E"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 232
    const-string v10, "1A ?? ?? ?? 00 00 00 00 00 00 54 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 12 ?? 6E ?? ?? ?? ?? ?? 0C ?? 38 ?? ?? ?? 72 ?? ?? ?? ?? ?? 0A ?? 39 ?? ?? ?? 54 ?? ?? ?? 54 ?? ?? ?? 12 ?? 6E"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 233
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchsupportOld;->pattern2:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 234
    const-string v10, "support4 Fixed!\n(pak)"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 235
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 237
    const-string v10, "1A ?? ?? ?? 6E ?? ?? ?? ?? ?? 54 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0C ?? 72 ?? ?? ?? ?? ?? 0A ?? 39 ?? ?? ?? 54 ?? ?? ?? 54 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0E 00"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 238
    const-string v10, "1A ?? ?? ?? 00 00 00 00 00 00 54 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0C ?? 72 ?? ?? ?? ?? ?? 0A ?? 39 ?? ?? ?? 54 ?? ?? ?? 54 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0E 00"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 239
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchsupportOld;->pattern2:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 240
    const-string v10, "support4 Fixed!\n(pak)"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 241
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 243
    const-string v10, "1A ?? ?? ?? 6E ?? ?? ?? ?? ?? 54 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 12 ?? 6E ?? ?? ?? ?? ?? 0C ?? 72 ?? ?? ?? ?? ?? 0A ?? 39 ?? ?? ?? 54 ?? ?? ?? 54 ?? ?? ?? 12 ?? 6E ?? ?? ?? ?? ?? 0E 00"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 244
    const-string v10, "1A ?? ?? ?? 00 00 00 00 00 00 54 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 12 ?? 6E ?? ?? ?? ?? ?? 0C ?? 72 ?? ?? ?? ?? ?? 0A ?? 39 ?? ?? ?? 54 ?? ?? ?? 54 ?? ?? ?? 12 ?? 6E ?? ?? ?? ?? ?? 0E 00"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 245
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchsupportOld;->pattern2:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 246
    const-string v10, "support4 Fixed!\n(pak)"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 247
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 249
    const/4 v10, 0x0

    aget-object v10, p0, v10

    const-string v11, "com.jetappfactory."

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_43d

    .line 251
    const-string v10, "71 30 ?? ?? ?? ?? 0C ?? 6E 20 ?? ?? ?? ?? 54 ?? ?? ?? 28 ??"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 252
    const-string v10, "71 30 ?? ?? ?? ?? 0C ?? 00 00 00 00 00 00 54 ?? ?? ?? 28 ??"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 253
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchsupportOld;->pattern2:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 254
    const-string v10, "support4 Fixed!\n(pak)"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 255
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 258
    :cond_43d
    const/16 v20, 0x0

    .line 260
    .local v20, "conv":Z
    invoke-static/range {v20 .. v20}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-static/range {v2 .. v9}, Lcom/chelpus/Utils;->convertToPatchItemAuto(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/Boolean;)V

    .line 264
    :try_start_446
    sget-object v10, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->startUnderRoot:Ljava/lang/Boolean;

    invoke-virtual {v10}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    if-eqz v10, :cond_456

    const/4 v10, 0x2

    aget-object v10, p0, v10

    const-string v11, "RW"

    invoke-static {v10, v11}, Lcom/chelpus/Utils;->remount(Ljava/lang/String;Ljava/lang/String;)Z

    .line 265
    :cond_456
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchsupportOld;->createAPK:Z

    if-nez v10, :cond_672

    sget-boolean v10, Lcom/chelpus/root/utils/runpatchsupportOld;->ART:Z

    if-nez v10, :cond_672

    .line 267
    const/4 v10, 0x3

    aget-object v10, p0, v10

    sput-object v10, Lcom/chelpus/root/utils/runpatchsupportOld;->dir:Ljava/lang/String;

    .line 268
    const/4 v10, 0x2

    aget-object v10, p0, v10

    sput-object v10, Lcom/chelpus/root/utils/runpatchsupportOld;->dirapp:Ljava/lang/String;

    .line 269
    invoke-static {}, Lcom/chelpus/root/utils/runpatchsupportOld;->clearTemp()V

    .line 270
    const/4 v10, 0x4

    aget-object v10, p0, v10

    const-string v11, "not_system"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_479

    const/4 v10, 0x0

    sput-boolean v10, Lcom/chelpus/root/utils/runpatchsupportOld;->system:Z

    .line 271
    :cond_479
    const/4 v10, 0x4

    aget-object v10, p0, v10

    const-string v11, "system"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_487

    const/4 v10, 0x1

    sput-boolean v10, Lcom/chelpus/root/utils/runpatchsupportOld;->system:Z

    .line 272
    :cond_487
    sget-object v10, Lcom/chelpus/root/utils/runpatchsupportOld;->filestopatch:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->clear()V

    .line 273
    const-string v10, "CLASSES mode create odex enabled."

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 274
    const/4 v10, 0x0

    aget-object v48, p0, v10

    .line 275
    .local v48, "packageName":Ljava/lang/String;
    const/4 v10, 0x2

    aget-object v10, p0, v10

    sput-object v10, Lcom/chelpus/root/utils/runpatchsupportOld;->appdir:Ljava/lang/String;

    .line 276
    const/4 v10, 0x3

    aget-object v10, p0, v10

    sput-object v10, Lcom/chelpus/root/utils/runpatchsupportOld;->sddir:Ljava/lang/String;

    .line 277
    invoke-static {}, Lcom/chelpus/root/utils/runpatchsupportOld;->clearTempSD()V

    .line 278
    new-instance v16, Ljava/io/File;

    sget-object v10, Lcom/chelpus/root/utils/runpatchsupportOld;->appdir:Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-direct {v0, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 279
    .local v16, "apk":Ljava/io/File;
    const-string v10, "Get classes.dex."

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 280
    sget-object v10, Lcom/chelpus/root/utils/runpatchsupportOld;->print:Ljava/io/PrintStream;

    const-string v11, "Get classes.dex."

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 281
    invoke-static/range {v16 .. v16}, Lcom/chelpus/root/utils/runpatchsupportOld;->unzipART(Ljava/io/File;)V

    .line 282
    sget-object v10, Lcom/chelpus/root/utils/runpatchsupportOld;->classesFiles:Ljava/util/ArrayList;

    if-eqz v10, :cond_4c5

    sget-object v10, Lcom/chelpus/root/utils/runpatchsupportOld;->classesFiles:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-nez v10, :cond_4ea

    .line 283
    :cond_4c5
    new-instance v10, Ljava/io/FileNotFoundException;

    invoke-direct {v10}, Ljava/io/FileNotFoundException;-><init>()V

    throw v10
    :try_end_4cb
    .catch Ljava/io/FileNotFoundException; {:try_start_446 .. :try_end_4cb} :catch_4cb
    .catch Ljava/lang/Exception; {:try_start_446 .. :try_end_4cb} :catch_50d

    .line 835
    .end local v16    # "apk":Ljava/io/File;
    .end local v48    # "packageName":Ljava/lang/String;
    :catch_4cb
    move-exception v43

    .line 836
    .local v43, "localFileNotFoundException":Ljava/io/FileNotFoundException;
    const-string v10, "Error: Program files are not found!\n\nCheck the location dalvik-cache to solve problems!\n\nDefault: /data/dalvik-cache/*"

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 842
    .end local v43    # "localFileNotFoundException":Ljava/io/FileNotFoundException;
    :cond_4d1
    :goto_4d1
    sget-object v10, Lcom/chelpus/root/utils/runpatchsupportOld;->filestopatch:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_4d7
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_1301

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v35

    check-cast v35, Ljava/io/File;

    .line 843
    .local v35, "filepatch":Ljava/io/File;
    invoke-static/range {v35 .. v35}, Lcom/chelpus/Utils;->fixadler(Ljava/io/File;)V

    .line 844
    invoke-static {}, Lcom/chelpus/root/utils/runpatchsupportOld;->clearTempSD()V

    goto :goto_4d7

    .line 285
    .end local v35    # "filepatch":Ljava/io/File;
    .restart local v16    # "apk":Ljava/io/File;
    .restart local v48    # "packageName":Ljava/lang/String;
    :cond_4ea
    :try_start_4ea
    sget-object v10, Lcom/chelpus/root/utils/runpatchsupportOld;->filestopatch:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->clear()V

    .line 286
    sget-object v10, Lcom/chelpus/root/utils/runpatchsupportOld;->classesFiles:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_4f5
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_531

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/io/File;

    .line 287
    .local v18, "cl":Ljava/io/File;
    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->exists()Z

    move-result v11

    if-nez v11, :cond_529

    new-instance v10, Ljava/io/FileNotFoundException;

    invoke-direct {v10}, Ljava/io/FileNotFoundException;-><init>()V

    throw v10
    :try_end_50d
    .catch Ljava/io/FileNotFoundException; {:try_start_4ea .. :try_end_50d} :catch_4cb
    .catch Ljava/lang/Exception; {:try_start_4ea .. :try_end_50d} :catch_50d

    .line 838
    .end local v16    # "apk":Ljava/io/File;
    .end local v18    # "cl":Ljava/io/File;
    .end local v48    # "packageName":Ljava/lang/String;
    :catch_50d
    move-exception v28

    .line 839
    .restart local v28    # "e":Ljava/lang/Exception;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Patch Process Exception: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    goto :goto_4d1

    .line 288
    .end local v28    # "e":Ljava/lang/Exception;
    .restart local v16    # "apk":Ljava/io/File;
    .restart local v18    # "cl":Ljava/io/File;
    .restart local v48    # "packageName":Ljava/lang/String;
    :cond_529
    :try_start_529
    sget-object v11, Lcom/chelpus/root/utils/runpatchsupportOld;->filestopatch:Ljava/util/ArrayList;

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4f5

    .line 292
    .end local v18    # "cl":Ljava/io/File;
    :cond_531
    const/4 v10, 0x2

    aget-object v10, p0, v10

    const/4 v11, 0x1

    invoke-static {v10, v11}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v45

    .line 293
    .local v45, "odexstr":Ljava/lang/String;
    new-instance v44, Ljava/io/File;

    invoke-direct/range {v44 .. v45}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 294
    .local v44, "odexfile":Ljava/io/File;
    invoke-virtual/range {v44 .. v44}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_547

    invoke-virtual/range {v44 .. v44}, Ljava/io/File;->delete()Z

    .line 295
    :cond_547
    new-instance v44, Ljava/io/File;

    .end local v44    # "odexfile":Ljava/io/File;
    const-string v10, "-1"

    const-string v11, "-2"

    move-object/from16 v0, v45

    invoke-virtual {v0, v10, v11}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, v44

    invoke-direct {v0, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 296
    .restart local v44    # "odexfile":Ljava/io/File;
    invoke-virtual/range {v44 .. v44}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_561

    invoke-virtual/range {v44 .. v44}, Ljava/io/File;->delete()Z

    .line 297
    :cond_561
    new-instance v44, Ljava/io/File;

    .end local v44    # "odexfile":Ljava/io/File;
    const-string v10, "-2"

    const-string v11, "-1"

    move-object/from16 v0, v45

    invoke-virtual {v0, v10, v11}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, v44

    invoke-direct {v0, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 298
    .restart local v44    # "odexfile":Ljava/io/File;
    invoke-virtual/range {v44 .. v44}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_57b

    invoke-virtual/range {v44 .. v44}, Ljava/io/File;->delete()Z

    .line 359
    .end local v16    # "apk":Ljava/io/File;
    .end local v44    # "odexfile":Ljava/io/File;
    .end local v45    # "odexstr":Ljava/lang/String;
    .end local v48    # "packageName":Ljava/lang/String;
    :cond_57b
    sget-object v10, Lcom/chelpus/root/utils/runpatchsupportOld;->filestopatch:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v68

    :goto_581
    invoke-interface/range {v68 .. v68}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_4d1

    invoke-interface/range {v68 .. v68}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v35

    check-cast v35, Ljava/io/File;

    .line 360
    .restart local v35    # "filepatch":Ljava/io/File;
    const-string v10, "Find string id."

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 363
    new-instance v63, Ljava/util/ArrayList;

    invoke-direct/range {v63 .. v63}, Ljava/util/ArrayList;-><init>()V

    .line 364
    .local v63, "strings":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v10, "com.android.vending"

    move-object/from16 v0, v63

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 365
    const-string v10, "SHA1withRSA"

    move-object/from16 v0, v63

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 366
    const-string v10, "com.android.vending.billing.InAppBillingService.BIND"

    move-object/from16 v0, v63

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 367
    const-string v10, "Ljava/security/Signature;"

    move-object/from16 v0, v63

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 368
    const-string v10, "verify"

    move-object/from16 v0, v63

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 369
    const-string v10, "Landroid/content/Intent;"

    move-object/from16 v0, v63

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 370
    const-string v10, "setPackage"

    move-object/from16 v0, v63

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 371
    const-string v10, "engineVerify"

    move-object/from16 v0, v63

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 372
    const-string v10, "Ljava/security/SignatureSpi;"

    move-object/from16 v0, v63

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 375
    const-string v10, "String analysis."

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 376
    sget-object v10, Lcom/chelpus/root/utils/runpatchsupportOld;->print:Ljava/io/PrintStream;

    const-string v11, "String analysis."

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 377
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchsupportOld;->copyDC:Z

    if-nez v10, :cond_79b

    invoke-virtual/range {v35 .. v35}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    move-object/from16 v0, v63

    invoke-static {v10, v0, v11}, Lcom/chelpus/Utils;->getStringIds(Ljava/lang/String;Ljava/util/ArrayList;Z)Ljava/util/ArrayList;

    move-result-object v47

    .line 379
    .local v47, "offsets":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;>;"
    :goto_5f1
    const/16 v30, 0x0

    .local v30, "f1":Z
    const/16 v31, 0x0

    .local v31, "f2":Z
    const/16 v32, 0x0

    .local v32, "f3":Z
    const/16 v29, 0x0

    .local v29, "ev":Z
    const/16 v59, 0x0

    .line 380
    .local v59, "spi":Z
    const/16 v46, 0x1

    .line 381
    .local v46, "of_to_patch":I
    new-instance v66, Ljava/util/ArrayList;

    invoke-direct/range {v66 .. v66}, Ljava/util/ArrayList;-><init>()V

    .line 382
    .local v66, "types":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/TypesItem;>;"
    new-instance v10, Lcom/android/vending/billing/InAppBillingService/LUCK/TypesItem;

    const-string v11, "Ljava/security/Signature;"

    invoke-direct {v10, v11}, Lcom/android/vending/billing/InAppBillingService/LUCK/TypesItem;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v66

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 383
    new-instance v10, Lcom/android/vending/billing/InAppBillingService/LUCK/TypesItem;

    const-string v11, "Ljava/security/SignatureSpi;"

    invoke-direct {v10, v11}, Lcom/android/vending/billing/InAppBillingService/LUCK/TypesItem;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v66

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 384
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 385
    .local v19, "commands":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;>;"
    new-instance v10, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;

    const-string v11, "Ljava/security/Signature;"

    const-string v12, "verify"

    invoke-direct {v10, v11, v12}, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 386
    new-instance v10, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;

    const-string v11, "Landroid/content/Intent;"

    const-string v12, "setPackage"

    invoke-direct {v10, v11, v12}, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 387
    invoke-virtual/range {v47 .. v47}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_63f
    :goto_63f
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_ad3

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;

    .line 388
    .local v39, "it":Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;
    invoke-virtual/range {v66 .. v66}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_64f
    :goto_64f
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_7a8

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v40

    check-cast v40, Lcom/android/vending/billing/InAppBillingService/LUCK/TypesItem;

    .line 389
    .local v40, "item":Lcom/android/vending/billing/InAppBillingService/LUCK/TypesItem;
    move-object/from16 v0, v40

    iget-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/TypesItem;->type:Ljava/lang/String;

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->str:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_64f

    .line 390
    move-object/from16 v0, v39

    iget-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    move-object/from16 v0, v40

    iput-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/TypesItem;->Type:[B

    goto :goto_64f

    .line 302
    .end local v19    # "commands":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;>;"
    .end local v29    # "ev":Z
    .end local v30    # "f1":Z
    .end local v31    # "f2":Z
    .end local v32    # "f3":Z
    .end local v35    # "filepatch":Ljava/io/File;
    .end local v39    # "it":Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;
    .end local v40    # "item":Lcom/android/vending/billing/InAppBillingService/LUCK/TypesItem;
    .end local v46    # "of_to_patch":I
    .end local v47    # "offsets":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;>;"
    .end local v59    # "spi":Z
    .end local v63    # "strings":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v66    # "types":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/TypesItem;>;"
    :cond_672
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchsupportOld;->createAPK:Z

    if-eqz v10, :cond_739

    .line 303
    const/4 v10, 0x0

    aget-object v48, p0, v10

    .line 304
    .restart local v48    # "packageName":Ljava/lang/String;
    const/4 v10, 0x2

    aget-object v10, p0, v10

    sput-object v10, Lcom/chelpus/root/utils/runpatchsupportOld;->appdir:Ljava/lang/String;

    .line 305
    const/4 v10, 0x5

    aget-object v10, p0, v10

    sput-object v10, Lcom/chelpus/root/utils/runpatchsupportOld;->sddir:Ljava/lang/String;

    .line 307
    invoke-static {}, Lcom/chelpus/root/utils/runpatchsupportOld;->clearTempSD()V

    .line 308
    new-instance v16, Ljava/io/File;

    sget-object v10, Lcom/chelpus/root/utils/runpatchsupportOld;->appdir:Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-direct {v0, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 309
    .restart local v16    # "apk":Ljava/io/File;
    invoke-static/range {v16 .. v16}, Lcom/chelpus/root/utils/runpatchsupportOld;->unzipSD(Ljava/io/File;)V

    .line 310
    new-instance v10, Ljava/io/File;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v12, Lcom/chelpus/root/utils/runpatchsupportOld;->sddir:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/Modified/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, v48

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ".apk"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v10, Lcom/chelpus/root/utils/runpatchsupportOld;->crkapk:Ljava/io/File;

    .line 311
    sget-object v10, Lcom/chelpus/root/utils/runpatchsupportOld;->crkapk:Ljava/io/File;

    move-object/from16 v0, v16

    invoke-static {v0, v10}, Lcom/chelpus/Utils;->copyFile(Ljava/io/File;Ljava/io/File;)V

    .line 313
    new-instance v15, Ljava/io/File;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v11, Lcom/chelpus/root/utils/runpatchsupportOld;->sddir:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "/Modified/AndroidManifest.xml"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v15, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 314
    .local v15, "androidManifest":Ljava/io/File;
    invoke-virtual {v15}, Ljava/io/File;->exists()Z

    move-result v10

    if-nez v10, :cond_6e7

    new-instance v10, Ljava/io/FileNotFoundException;

    invoke-direct {v10}, Ljava/io/FileNotFoundException;-><init>()V

    throw v10
    :try_end_6e7
    .catch Ljava/io/FileNotFoundException; {:try_start_529 .. :try_end_6e7} :catch_4cb
    .catch Ljava/lang/Exception; {:try_start_529 .. :try_end_6e7} :catch_50d

    .line 317
    :cond_6e7
    :try_start_6e7
    new-instance v10, Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample;

    invoke-direct {v10}, Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample;-><init>()V

    const-string v11, "19"

    invoke-virtual {v10, v15, v11}, Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample;->changeTargetApi(Ljava/io/File;Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_6f7

    .line 318
    invoke-virtual {v15}, Ljava/io/File;->delete()Z
    :try_end_6f7
    .catch Ljava/lang/Exception; {:try_start_6e7 .. :try_end_6f7} :catch_709
    .catch Ljava/io/FileNotFoundException; {:try_start_6e7 .. :try_end_6f7} :catch_4cb

    .line 322
    :cond_6f7
    :goto_6f7
    :try_start_6f7
    sget-object v10, Lcom/chelpus/root/utils/runpatchsupportOld;->classesFiles:Ljava/util/ArrayList;

    if-eqz v10, :cond_703

    sget-object v10, Lcom/chelpus/root/utils/runpatchsupportOld;->classesFiles:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-nez v10, :cond_70e

    .line 323
    :cond_703
    new-instance v10, Ljava/io/FileNotFoundException;

    invoke-direct {v10}, Ljava/io/FileNotFoundException;-><init>()V

    throw v10

    .line 320
    :catch_709
    move-exception v28

    .restart local v28    # "e":Ljava/lang/Exception;
    invoke-virtual/range {v28 .. v28}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_6f7

    .line 325
    .end local v28    # "e":Ljava/lang/Exception;
    :cond_70e
    sget-object v10, Lcom/chelpus/root/utils/runpatchsupportOld;->filestopatch:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->clear()V

    .line 326
    sget-object v10, Lcom/chelpus/root/utils/runpatchsupportOld;->classesFiles:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_719
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_739

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/io/File;

    .line 327
    .restart local v18    # "cl":Ljava/io/File;
    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->exists()Z

    move-result v11

    if-nez v11, :cond_731

    new-instance v10, Ljava/io/FileNotFoundException;

    invoke-direct {v10}, Ljava/io/FileNotFoundException;-><init>()V

    throw v10

    .line 328
    :cond_731
    sget-object v11, Lcom/chelpus/root/utils/runpatchsupportOld;->filestopatch:Ljava/util/ArrayList;

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_719

    .line 332
    .end local v15    # "androidManifest":Ljava/io/File;
    .end local v16    # "apk":Ljava/io/File;
    .end local v18    # "cl":Ljava/io/File;
    .end local v48    # "packageName":Ljava/lang/String;
    :cond_739
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchsupportOld;->ART:Z

    if-eqz v10, :cond_57b

    .line 333
    const-string v10, "ART mode create dex enabled."

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 334
    const/4 v10, 0x0

    aget-object v48, p0, v10

    .line 335
    .restart local v48    # "packageName":Ljava/lang/String;
    const/4 v10, 0x2

    aget-object v10, p0, v10

    sput-object v10, Lcom/chelpus/root/utils/runpatchsupportOld;->appdir:Ljava/lang/String;

    .line 336
    const/4 v10, 0x3

    aget-object v10, p0, v10

    sput-object v10, Lcom/chelpus/root/utils/runpatchsupportOld;->sddir:Ljava/lang/String;

    .line 338
    invoke-static {}, Lcom/chelpus/root/utils/runpatchsupportOld;->clearTempSD()V

    .line 339
    new-instance v16, Ljava/io/File;

    sget-object v10, Lcom/chelpus/root/utils/runpatchsupportOld;->appdir:Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-direct {v0, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 340
    .restart local v16    # "apk":Ljava/io/File;
    invoke-static/range {v16 .. v16}, Lcom/chelpus/root/utils/runpatchsupportOld;->unzipART(Ljava/io/File;)V

    .line 343
    sget-object v10, Lcom/chelpus/root/utils/runpatchsupportOld;->classesFiles:Ljava/util/ArrayList;

    if-eqz v10, :cond_76a

    sget-object v10, Lcom/chelpus/root/utils/runpatchsupportOld;->classesFiles:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-nez v10, :cond_770

    .line 344
    :cond_76a
    new-instance v10, Ljava/io/FileNotFoundException;

    invoke-direct {v10}, Ljava/io/FileNotFoundException;-><init>()V

    throw v10

    .line 346
    :cond_770
    sget-object v10, Lcom/chelpus/root/utils/runpatchsupportOld;->filestopatch:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->clear()V

    .line 347
    sget-object v10, Lcom/chelpus/root/utils/runpatchsupportOld;->classesFiles:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_77b
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_57b

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/io/File;

    .line 348
    .restart local v18    # "cl":Ljava/io/File;
    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->exists()Z

    move-result v11

    if-nez v11, :cond_793

    new-instance v10, Ljava/io/FileNotFoundException;

    invoke-direct {v10}, Ljava/io/FileNotFoundException;-><init>()V

    throw v10

    .line 349
    :cond_793
    sget-object v11, Lcom/chelpus/root/utils/runpatchsupportOld;->filestopatch:Ljava/util/ArrayList;

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_77b

    .line 378
    .end local v16    # "apk":Ljava/io/File;
    .end local v18    # "cl":Ljava/io/File;
    .end local v48    # "packageName":Ljava/lang/String;
    .restart local v35    # "filepatch":Ljava/io/File;
    .restart local v63    # "strings":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_79b
    invoke-virtual/range {v35 .. v35}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    move-object/from16 v0, v63

    invoke-static {v10, v0, v11}, Lcom/chelpus/Utils;->getStringIds(Ljava/lang/String;Ljava/util/ArrayList;Z)Ljava/util/ArrayList;

    move-result-object v47

    .restart local v47    # "offsets":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;>;"
    goto/16 :goto_5f1

    .line 393
    .restart local v19    # "commands":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;>;"
    .restart local v29    # "ev":Z
    .restart local v30    # "f1":Z
    .restart local v31    # "f2":Z
    .restart local v32    # "f3":Z
    .restart local v39    # "it":Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;
    .restart local v46    # "of_to_patch":I
    .restart local v59    # "spi":Z
    .restart local v66    # "types":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/TypesItem;>;"
    :cond_7a8
    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_7ac
    :goto_7ac
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_7e5

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v40

    check-cast v40, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;

    .line 394
    .local v40, "item":Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;
    move-object/from16 v0, v40

    iget-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;->object:Ljava/lang/String;

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->str:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7ce

    .line 395
    move-object/from16 v0, v39

    iget-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    move-object/from16 v0, v40

    iput-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;->Object:[B

    .line 397
    :cond_7ce
    move-object/from16 v0, v40

    iget-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;->method:Ljava/lang/String;

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->str:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7ac

    .line 398
    move-object/from16 v0, v39

    iget-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    move-object/from16 v0, v40

    iput-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;->Method:[B

    goto :goto_7ac

    .line 402
    .end local v40    # "item":Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;
    :cond_7e5
    move-object/from16 v0, v39

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->str:Ljava/lang/String;

    const-string v12, "com.android.vending"

    invoke-virtual {v10, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_955

    .line 405
    const/4 v10, 0x5

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x2

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x0

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 406
    const/4 v10, 0x5

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x3

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x1

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 407
    const/4 v10, 0x6

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x2

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x0

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 408
    const/4 v10, 0x6

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x3

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x1

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 409
    const/4 v10, 0x7

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x2

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x0

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 410
    const/4 v10, 0x7

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x3

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x1

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 421
    const/16 v10, 0x8

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x2

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x0

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 422
    const/16 v10, 0x8

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x3

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x1

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 423
    const/16 v10, 0x8

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x4

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x2

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 424
    const/16 v10, 0x8

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x5

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x3

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 425
    const/16 v10, 0x9

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x2

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x0

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 426
    const/16 v10, 0x9

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x3

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x1

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 427
    const/16 v10, 0x9

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x4

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x2

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 428
    const/16 v10, 0x9

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x5

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x3

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 429
    const/16 v10, 0xa

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x2

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x0

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 430
    const/16 v10, 0xa

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x3

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x1

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 431
    const/16 v10, 0xa

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x4

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x2

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 432
    const/16 v10, 0xa

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x5

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x3

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 441
    const/16 v30, 0x1

    .line 443
    :cond_955
    move-object/from16 v0, v39

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->str:Ljava/lang/String;

    const-string v12, "com.android.vending.billing.InAppBillingService.BIND"

    invoke-virtual {v10, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_a35

    .line 444
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "c.a.v.b.i "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, v39

    iget-boolean v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->bits32:Z

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x0

    aget-byte v13, v13, v14

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x1

    aget-byte v13, v13, v14

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x2

    aget-byte v13, v13, v14

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x3

    aget-byte v13, v13, v14

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v12}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 446
    const/4 v10, 0x0

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x2

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x0

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 447
    const/4 v10, 0x0

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x3

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x1

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 451
    const/4 v10, 0x1

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x2

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x0

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 452
    const/4 v10, 0x1

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x3

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x1

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 453
    const/4 v10, 0x1

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x4

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x2

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 454
    const/4 v10, 0x1

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x5

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x3

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 456
    const/16 v31, 0x1

    .line 458
    :cond_a35
    move-object/from16 v0, v39

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->str:Ljava/lang/String;

    const-string v12, "SHA1withRSA"

    invoke-virtual {v10, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_ab5

    .line 460
    const/4 v10, 0x2

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x2

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x0

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 461
    const/4 v10, 0x2

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x3

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x1

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 465
    const/4 v10, 0x3

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x2

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x0

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 466
    const/4 v10, 0x3

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x3

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x1

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 467
    const/4 v10, 0x3

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x4

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x2

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 468
    const/4 v10, 0x3

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x5

    move-object/from16 v0, v39

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x3

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 470
    const/16 v32, 0x1

    .line 472
    :cond_ab5
    move-object/from16 v0, v39

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->str:Ljava/lang/String;

    const-string v12, "engineVerify"

    invoke-virtual {v10, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_ac3

    .line 474
    const/16 v29, 0x1

    .line 476
    :cond_ac3
    move-object/from16 v0, v39

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->str:Ljava/lang/String;

    const-string v12, "Ljava/security/SignatureSpi;"

    invoke-virtual {v10, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_63f

    .line 478
    const/16 v59, 0x1

    goto/16 :goto_63f

    .line 481
    .end local v39    # "it":Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;
    :cond_ad3
    const/16 v50, 0x0

    .local v50, "patch_index1":I
    const/16 v51, 0x1

    .line 482
    .local v51, "patch_index2":I
    if-eqz v30, :cond_adb

    if-nez v31, :cond_b52

    .line 483
    :cond_adb
    const/4 v10, 0x0

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    const/4 v11, 0x0

    iput-boolean v11, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->pattern:Z

    .line 484
    const/4 v10, 0x1

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    const/4 v11, 0x0

    iput-boolean v11, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->pattern:Z

    .line 485
    const/4 v10, 0x5

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    const/4 v11, 0x0

    iput-boolean v11, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->pattern:Z

    .line 486
    const/4 v10, 0x6

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    const/4 v11, 0x0

    iput-boolean v11, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->pattern:Z

    .line 487
    const/4 v10, 0x7

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    const/4 v11, 0x0

    iput-boolean v11, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->pattern:Z

    .line 488
    const/16 v10, 0x8

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    const/4 v11, 0x0

    iput-boolean v11, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->pattern:Z

    .line 489
    const/16 v10, 0x8

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    const-string v11, ""

    iput-object v11, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->marker:Ljava/lang/String;

    .line 490
    const/16 v10, 0x9

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    const/4 v11, 0x0

    iput-boolean v11, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->pattern:Z

    .line 491
    const/16 v10, 0x9

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    const-string v11, ""

    iput-object v11, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->marker:Ljava/lang/String;

    .line 492
    const/16 v10, 0xa

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    const/4 v11, 0x0

    iput-boolean v11, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->pattern:Z

    .line 493
    const/16 v10, 0xa

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    const-string v11, ""

    iput-object v11, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->marker:Ljava/lang/String;

    .line 495
    :cond_b52
    if-nez v32, :cond_b72

    .line 496
    const/4 v10, 0x2

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    const/4 v11, 0x0

    iput-boolean v11, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->pattern:Z

    .line 497
    const/4 v10, 0x3

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    const/4 v11, 0x0

    iput-boolean v11, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->pattern:Z

    .line 498
    const/4 v10, 0x4

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    const/4 v11, 0x0

    iput-boolean v11, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->pattern:Z

    .line 500
    :cond_b72
    const-string v10, "Parse data for patch."

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 501
    sget-object v10, Lcom/chelpus/root/utils/runpatchsupportOld;->print:Ljava/io/PrintStream;

    const-string v11, "Parse data for patch."

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 502
    invoke-virtual/range {v35 .. v35}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    move-object/from16 v0, v19

    invoke-static {v10, v0, v11}, Lcom/chelpus/Utils;->getMethodsIds(Ljava/lang/String;Ljava/util/ArrayList;Z)Z

    .line 505
    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_b8c
    :goto_b8c
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_c22

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v40

    check-cast v40, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;

    .line 506
    .restart local v40    # "item":Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;
    move-object/from16 v0, v40

    iget-boolean v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;->found_index_command:Z

    if-eqz v10, :cond_b8c

    .line 507
    move-object/from16 v0, v40

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;->object:Ljava/lang/String;

    const-string v12, "Ljava/security/Signature;"

    invoke-virtual {v10, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_bdd

    .line 508
    const/16 v10, 0xb

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x2

    move-object/from16 v0, v40

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;->index_command:[B

    const/4 v14, 0x0

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 509
    const/16 v10, 0xb

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x3

    move-object/from16 v0, v40

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;->index_command:[B

    const/4 v14, 0x1

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 510
    const/16 v10, 0xb

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    const/4 v12, 0x1

    iput-boolean v12, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->pattern:Z

    .line 512
    :cond_bdd
    move-object/from16 v0, v40

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;->object:Ljava/lang/String;

    const-string v12, "Landroid/content/Intent;"

    invoke-virtual {v10, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_b8c

    sget-boolean v10, Lcom/chelpus/root/utils/runpatchsupportOld;->pattern3:Z

    if-eqz v10, :cond_b8c

    .line 513
    const/16 v10, 0xc

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x2

    move-object/from16 v0, v40

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;->index_command:[B

    const/4 v14, 0x0

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 514
    const/16 v10, 0xc

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x3

    move-object/from16 v0, v40

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;->index_command:[B

    const/4 v14, 0x1

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 515
    const/16 v10, 0xc

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    const/4 v12, 0x1

    iput-boolean v12, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->pattern:Z

    goto/16 :goto_b8c

    .line 519
    .end local v40    # "item":Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;
    :cond_c22
    if-eqz v29, :cond_c86

    if-eqz v59, :cond_c86

    .line 520
    invoke-virtual/range {v35 .. v35}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    move-object/from16 v0, v66

    invoke-static {v10, v0, v11}, Lcom/chelpus/Utils;->getTypesIds(Ljava/lang/String;Ljava/util/ArrayList;Z)Z

    .line 522
    invoke-virtual/range {v66 .. v66}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_c34
    :goto_c34
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_c86

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v40

    check-cast v40, Lcom/android/vending/billing/InAppBillingService/LUCK/TypesItem;

    .line 523
    .local v40, "item":Lcom/android/vending/billing/InAppBillingService/LUCK/TypesItem;
    move-object/from16 v0, v40

    iget-boolean v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/TypesItem;->found_id_type:Z

    if-eqz v10, :cond_c34

    .line 529
    move-object/from16 v0, v40

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/TypesItem;->type:Ljava/lang/String;

    const-string v12, "Ljava/security/SignatureSpi;"

    invoke-virtual {v10, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_c34

    .line 530
    const/16 v10, 0xf

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x2

    move-object/from16 v0, v40

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/TypesItem;->id_type:[B

    const/4 v14, 0x0

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 531
    const/16 v10, 0xf

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x3

    move-object/from16 v0, v40

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/TypesItem;->id_type:[B

    const/4 v14, 0x1

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 532
    const/16 v10, 0xf

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    const/4 v12, 0x1

    iput-boolean v12, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->pattern:Z

    goto :goto_c34

    .line 541
    .end local v40    # "item":Lcom/android/vending/billing/InAppBillingService/LUCK/TypesItem;
    :cond_c86
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v10

    new-array v0, v10, [Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    move-object/from16 v49, v0

    .line 542
    .local v49, "patchList":[Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    const/16 v67, 0x0

    .line 543
    .local v67, "u":I
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_c94
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_ca5

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v40

    check-cast v40, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    .line 544
    .local v40, "item":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    aput-object v40, v49, v67

    .line 545
    add-int/lit8 v67, v67, 0x1

    .line 546
    goto :goto_c94

    .line 557
    .end local v40    # "item":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    :cond_ca5
    const/16 v21, 0x0

    .line 558
    .local v21, "count":I
    const-string v10, "Set Strings."

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 559
    sget-object v10, Lcom/chelpus/root/utils/runpatchsupportOld;->print:Ljava/io/PrintStream;

    const-string v11, "Set Strings."

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 560
    new-instance v58, Ljava/util/ArrayList;

    invoke-direct/range {v58 .. v58}, Ljava/util/ArrayList;-><init>()V

    .line 561
    .local v58, "sitesFromFile":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
    const-string v10, "com.android.vending.billing.InAppBillingService.BIND"

    invoke-virtual {v10}, Ljava/lang/String;->getBytes()[B

    move-result-object v10

    move-object/from16 v0, v58

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 562
    invoke-virtual/range {v58 .. v58}, Ljava/util/ArrayList;->size()I

    move-result v10

    new-array v0, v10, [[B

    move-object/from16 v57, v0

    .line 563
    .local v57, "sites":[[B
    move-object/from16 v0, v58

    move-object/from16 v1, v57

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v10

    check-cast v10, [[B

    move-object v0, v10

    check-cast v0, [[B

    move-object/from16 v57, v0

    .line 564
    invoke-virtual/range {v35 .. v35}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    const/16 v12, 0x4c

    move-object/from16 v0, v57

    invoke-static {v10, v0, v11, v12}, Lcom/chelpus/Utils;->setStringIds(Ljava/lang/String;[[BZB)I

    move-result v21

    .line 566
    :goto_ce7
    if-lez v21, :cond_cf8

    .line 567
    add-int/lit8 v21, v21, -0x1

    .line 568
    const-string v10, "Reworked inapp string!"

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 569
    sget-object v10, Lcom/chelpus/root/utils/runpatchsupportOld;->print:Ljava/io/PrintStream;

    const-string v11, "Reworked inapp string!"

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_ce7

    .line 571
    :cond_cf8
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v64

    .line 572
    .local v64, "time":J
    new-instance v10, Ljava/io/RandomAccessFile;

    const-string v11, "rw"

    move-object/from16 v0, v35

    invoke-direct {v10, v0, v11}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v9

    .line 573
    .local v9, "ChannelDex":Ljava/nio/channels/FileChannel;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Size file:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v9}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v11

    invoke-virtual {v10, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 574
    sget-object v10, Ljava/nio/channels/FileChannel$MapMode;->READ_WRITE:Ljava/nio/channels/FileChannel$MapMode;

    const-wide/16 v11, 0x0

    invoke-virtual {v9}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v13

    long-to-int v13, v13

    int-to-long v13, v13

    invoke-virtual/range {v9 .. v14}, Ljava/nio/channels/FileChannel;->map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;
    :try_end_d30
    .catch Ljava/io/FileNotFoundException; {:try_start_6f7 .. :try_end_d30} :catch_4cb
    .catch Ljava/lang/Exception; {:try_start_6f7 .. :try_end_d30} :catch_50d

    move-result-object v34

    .line 575
    .local v34, "fileBytes":Ljava/nio/MappedByteBuffer;
    const/16 v23, 0x0

    .line 577
    .local v23, "curentPos":I
    const/16 v37, 0x0

    .line 580
    .local v37, "i":I
    const/16 v25, 0x5a

    .local v25, "diaposon":I
    const/16 v27, 0x3c

    .local v27, "diaposon_pak":I
    const/16 v26, 0x5a

    .line 581
    .local v26, "diaposon_ev":I
    const/16 v60, 0x0

    .local v60, "start_for_diaposon":I
    const/16 v62, 0x0

    .local v62, "start_for_diaposon_pak":I
    const/16 v61, 0x0

    .line 582
    .local v61, "start_for_diaposon_ev":I
    const/16 v53, 0x0

    .line 583
    .local v53, "period":I
    const-wide/16 v41, 0x0

    .local v41, "j":J
    :goto_d45
    :try_start_d45
    invoke-virtual/range {v34 .. v34}, Ljava/nio/MappedByteBuffer;->hasRemaining()Z

    move-result v10

    if-eqz v10, :cond_ecb

    .line 585
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchsupportOld;->createAPK:Z

    if-nez v10, :cond_d78

    .line 586
    invoke-virtual/range {v34 .. v34}, Ljava/nio/MappedByteBuffer;->position()I

    move-result v10

    sub-int v10, v10, v53

    const v11, 0x249ef

    if-le v10, v11, :cond_d78

    .line 587
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Progress size:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual/range {v34 .. v34}, Ljava/nio/MappedByteBuffer;->position()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 588
    invoke-virtual/range {v34 .. v34}, Ljava/nio/MappedByteBuffer;->position()I

    move-result v53

    .line 591
    :cond_d78
    invoke-virtual/range {v34 .. v34}, Ljava/nio/MappedByteBuffer;->position()I

    move-result v23

    .line 592
    invoke-virtual/range {v34 .. v34}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v22

    .line 596
    .local v22, "curentByte":B
    const/16 v38, 0x0

    .line 597
    .local v38, "increase":Z
    const/16 v17, 0x0

    .local v17, "b":I
    :goto_d84
    move-object/from16 v0, v49

    array-length v10, v0

    move/from16 v0, v17

    if-ge v0, v10, :cond_12f4

    .line 598
    aget-object v52, v49, v17

    .line 599
    .local v52, "patches":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    move-object/from16 v0, v34

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 601
    move-object/from16 v0, v52

    iget-boolean v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->markerTrig:Z

    if-eqz v10, :cond_f04

    const/4 v10, 0x5

    move/from16 v0, v17

    if-eq v0, v10, :cond_dbb

    const/4 v10, 0x6

    move/from16 v0, v17

    if-eq v0, v10, :cond_dbb

    const/4 v10, 0x7

    move/from16 v0, v17

    if-eq v0, v10, :cond_dbb

    const/16 v10, 0x8

    move/from16 v0, v17

    if-eq v0, v10, :cond_dbb

    const/16 v10, 0x9

    move/from16 v0, v17

    if-eq v0, v10, :cond_dbb

    const/16 v10, 0xa

    move/from16 v0, v17

    if-ne v0, v10, :cond_f04

    .line 603
    :cond_dbb
    if-nez v38, :cond_dc1

    add-int/lit8 v62, v62, 0x1

    const/16 v38, 0x1

    .line 604
    :cond_dc1
    move/from16 v0, v62

    move/from16 v1, v27

    if-ge v0, v1, :cond_101a

    .line 606
    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v11, 0x0

    aget-byte v10, v10, v11

    move/from16 v0, v22

    if-ne v0, v10, :cond_efd

    .line 608
    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repMask:[I

    const/4 v11, 0x0

    aget v10, v10, v11

    if-nez v10, :cond_de2

    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    const/4 v11, 0x0

    aput-byte v22, v10, v11

    .line 609
    :cond_de2
    const/16 v37, 0x1

    .line 610
    add-int/lit8 v10, v23, 0x1

    move-object/from16 v0, v34

    invoke-virtual {v0, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 611
    invoke-virtual/range {v34 .. v34}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v55

    .line 613
    .local v55, "prufbyte":B
    :goto_def
    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    aget-byte v10, v10, v37

    move/from16 v0, v55

    if-eq v0, v10, :cond_e20

    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origMask:[I

    aget v10, v10, v37

    const/4 v11, 0x1

    if-eq v10, v11, :cond_e20

    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origMask:[I

    aget v10, v10, v37

    const/16 v11, 0x14

    if-eq v10, v11, :cond_e20

    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origMask:[I

    aget v10, v10, v37

    const/16 v11, 0x15

    if-eq v10, v11, :cond_e20

    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origMask:[I

    aget v10, v10, v37

    const/16 v11, 0x17

    if-ne v10, v11, :cond_efd

    .line 615
    :cond_e20
    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repMask:[I

    aget v10, v10, v37

    if-nez v10, :cond_e2e

    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    aput-byte v55, v10, v37

    .line 616
    :cond_e2e
    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repMask:[I

    aget v10, v10, v37

    const/16 v11, 0x14

    if-ne v10, v11, :cond_e41

    .line 617
    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    and-int/lit8 v11, v55, 0xf

    int-to-byte v11, v11

    aput-byte v11, v10, v37

    .line 620
    :cond_e41
    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repMask:[I

    aget v10, v10, v37

    const/16 v11, 0x15

    if-ne v10, v11, :cond_e56

    .line 621
    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    and-int/lit8 v11, v55, 0xf

    add-int/lit8 v11, v11, 0x10

    int-to-byte v11, v11

    aput-byte v11, v10, v37

    .line 625
    :cond_e56
    add-int/lit8 v37, v37, 0x1

    .line 626
    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    array-length v10, v10

    move/from16 v0, v37

    if-ne v0, v10, :cond_1014

    .line 628
    move-object/from16 v0, v34

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 629
    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    move-object/from16 v0, v34

    invoke-virtual {v0, v10}, Ljava/nio/MappedByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 630
    invoke-virtual/range {v34 .. v34}, Ljava/nio/MappedByteBuffer;->force()Ljava/nio/MappedByteBuffer;

    .line 631
    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->resultText:Ljava/lang/String;

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 632
    sget-object v10, Lcom/chelpus/root/utils/runpatchsupportOld;->print:Ljava/io/PrintStream;

    move-object/from16 v0, v52

    iget-object v11, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->resultText:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 633
    const/4 v10, 0x1

    move-object/from16 v0, v52

    iput-boolean v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->result:Z

    .line 634
    const/4 v10, 0x0

    move-object/from16 v0, v52

    iput-boolean v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->markerTrig:Z

    .line 635
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_e92
    :goto_e92
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_ef4

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    .line 636
    .local v24, "curpatch":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    move-object/from16 v0, v24

    iget-object v11, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->marker:Ljava/lang/String;

    move-object/from16 v0, v52

    iget-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->marker:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_e92

    .line 637
    const/4 v11, 0x0

    move-object/from16 v0, v24

    iput-boolean v11, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->markerTrig:Z
    :try_end_eb1
    .catch Ljava/lang/Exception; {:try_start_d45 .. :try_end_eb1} :catch_eb2
    .catch Ljava/io/FileNotFoundException; {:try_start_d45 .. :try_end_eb1} :catch_4cb

    goto :goto_e92

    .line 826
    .end local v17    # "b":I
    .end local v22    # "curentByte":B
    .end local v24    # "curpatch":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    .end local v38    # "increase":Z
    .end local v52    # "patches":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    .end local v55    # "prufbyte":B
    :catch_eb2
    move-exception v28

    .line 827
    .restart local v28    # "e":Ljava/lang/Exception;
    :try_start_eb3
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, ""

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, v28

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 829
    .end local v28    # "e":Ljava/lang/Exception;
    :cond_ecb
    invoke-virtual {v9}, Ljava/nio/channels/FileChannel;->close()V

    .line 830
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, ""

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v11

    sub-long v11, v11, v64

    const-wide/16 v13, 0x3e8

    div-long/2addr v11, v13

    invoke-virtual {v10, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 831
    const-string v10, "Analise Results:"

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z
    :try_end_ef2
    .catch Ljava/io/FileNotFoundException; {:try_start_eb3 .. :try_end_ef2} :catch_4cb
    .catch Ljava/lang/Exception; {:try_start_eb3 .. :try_end_ef2} :catch_50d

    goto/16 :goto_581

    .line 639
    .restart local v17    # "b":I
    .restart local v22    # "curentByte":B
    .restart local v38    # "increase":Z
    .restart local v52    # "patches":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    .restart local v55    # "prufbyte":B
    :cond_ef4
    const/16 v62, 0x0

    .line 640
    add-int/lit8 v10, v23, 0x1

    :try_start_ef8
    move-object/from16 v0, v34

    invoke-virtual {v0, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 647
    .end local v55    # "prufbyte":B
    :cond_efd
    add-int/lit8 v10, v23, 0x1

    move-object/from16 v0, v34

    invoke-virtual {v0, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 659
    :cond_f04
    :goto_f04
    move-object/from16 v0, v52

    iget-boolean v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->markerTrig:Z

    if-eqz v10, :cond_105e

    const/16 v10, 0xd

    move/from16 v0, v17

    if-eq v0, v10, :cond_f16

    const/16 v10, 0xe

    move/from16 v0, v17

    if-ne v0, v10, :cond_105e

    .line 660
    :cond_f16
    sget-object v10, Lcom/chelpus/root/utils/runpatchsupportOld;->print:Ljava/io/PrintStream;

    const-string v11, "search jump"

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 661
    if-nez v38, :cond_f23

    add-int/lit8 v61, v61, 0x1

    const/16 v38, 0x1

    .line 662
    :cond_f23
    move/from16 v0, v61

    move/from16 v1, v26

    if-ge v0, v1, :cond_1162

    .line 664
    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v11, 0x0

    aget-byte v10, v10, v11

    move/from16 v0, v22

    if-ne v0, v10, :cond_1057

    .line 666
    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repMask:[I

    const/4 v11, 0x0

    aget v10, v10, v11

    if-nez v10, :cond_f44

    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    const/4 v11, 0x0

    aput-byte v22, v10, v11

    .line 667
    :cond_f44
    const/16 v37, 0x1

    .line 668
    add-int/lit8 v10, v23, 0x1

    move-object/from16 v0, v34

    invoke-virtual {v0, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 669
    invoke-virtual/range {v34 .. v34}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v55

    .line 671
    .restart local v55    # "prufbyte":B
    :goto_f51
    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    aget-byte v10, v10, v37

    move/from16 v0, v55

    if-eq v0, v10, :cond_f82

    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origMask:[I

    aget v10, v10, v37

    const/4 v11, 0x1

    if-eq v10, v11, :cond_f82

    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origMask:[I

    aget v10, v10, v37

    const/16 v11, 0x14

    if-eq v10, v11, :cond_f82

    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origMask:[I

    aget v10, v10, v37

    const/16 v11, 0x15

    if-eq v10, v11, :cond_f82

    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origMask:[I

    aget v10, v10, v37

    const/16 v11, 0x17

    if-ne v10, v11, :cond_1057

    .line 673
    :cond_f82
    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repMask:[I

    aget v10, v10, v37

    if-nez v10, :cond_f90

    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    aput-byte v55, v10, v37

    .line 674
    :cond_f90
    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repMask:[I

    aget v10, v10, v37

    const/16 v11, 0x14

    if-ne v10, v11, :cond_fa3

    .line 675
    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    and-int/lit8 v11, v55, 0xf

    int-to-byte v11, v11

    aput-byte v11, v10, v37

    .line 678
    :cond_fa3
    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repMask:[I

    aget v10, v10, v37

    const/16 v11, 0x15

    if-ne v10, v11, :cond_fb8

    .line 679
    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    and-int/lit8 v11, v55, 0xf

    add-int/lit8 v11, v11, 0x10

    int-to-byte v11, v11

    aput-byte v11, v10, v37

    .line 683
    :cond_fb8
    add-int/lit8 v37, v37, 0x1

    .line 684
    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    array-length v10, v10

    move/from16 v0, v37

    if-ne v0, v10, :cond_115c

    .line 686
    move-object/from16 v0, v34

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 687
    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    move-object/from16 v0, v34

    invoke-virtual {v0, v10}, Ljava/nio/MappedByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 688
    invoke-virtual/range {v34 .. v34}, Ljava/nio/MappedByteBuffer;->force()Ljava/nio/MappedByteBuffer;

    .line 689
    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->resultText:Ljava/lang/String;

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 690
    sget-object v10, Lcom/chelpus/root/utils/runpatchsupportOld;->print:Ljava/io/PrintStream;

    move-object/from16 v0, v52

    iget-object v11, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->resultText:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 691
    const/4 v10, 0x1

    move-object/from16 v0, v52

    iput-boolean v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->result:Z

    .line 692
    const/4 v10, 0x0

    move-object/from16 v0, v52

    iput-boolean v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->markerTrig:Z

    .line 693
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_ff4
    :goto_ff4
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_104e

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    .line 694
    .restart local v24    # "curpatch":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    move-object/from16 v0, v24

    iget-object v11, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->marker:Ljava/lang/String;

    move-object/from16 v0, v52

    iget-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->marker:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_ff4

    .line 695
    const/4 v11, 0x0

    move-object/from16 v0, v24

    iput-boolean v11, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->markerTrig:Z

    goto :goto_ff4

    .line 644
    .end local v24    # "curpatch":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    :cond_1014
    invoke-virtual/range {v34 .. v34}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v55

    goto/16 :goto_def

    .line 649
    .end local v55    # "prufbyte":B
    :cond_101a
    const/4 v10, 0x0

    move-object/from16 v0, v52

    iput-boolean v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->markerTrig:Z

    .line 650
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_1023
    :goto_1023
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_1043

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    .line 651
    .restart local v24    # "curpatch":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    move-object/from16 v0, v24

    iget-object v11, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->marker:Ljava/lang/String;

    move-object/from16 v0, v52

    iget-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->marker:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1023

    .line 652
    const/4 v11, 0x0

    move-object/from16 v0, v24

    iput-boolean v11, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->markerTrig:Z

    goto :goto_1023

    .line 654
    .end local v24    # "curpatch":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    :cond_1043
    const/16 v62, 0x0

    .line 655
    add-int/lit8 v10, v23, 0x1

    move-object/from16 v0, v34

    invoke-virtual {v0, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    goto/16 :goto_f04

    .line 697
    .restart local v55    # "prufbyte":B
    :cond_104e
    const/16 v61, 0x0

    .line 698
    add-int/lit8 v10, v23, 0x1

    move-object/from16 v0, v34

    invoke-virtual {v0, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 705
    .end local v55    # "prufbyte":B
    :cond_1057
    add-int/lit8 v10, v23, 0x1

    move-object/from16 v0, v34

    invoke-virtual {v0, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 717
    :cond_105e
    :goto_105e
    move-object/from16 v0, v52

    iget-boolean v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->markerTrig:Z

    if-eqz v10, :cond_11a6

    const/4 v10, 0x4

    move/from16 v0, v17

    if-ne v0, v10, :cond_11a6

    .line 719
    add-int/lit8 v60, v60, 0x1

    .line 720
    move/from16 v0, v60

    move/from16 v1, v25

    if-ge v0, v1, :cond_12af

    .line 722
    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v11, 0x0

    aget-byte v10, v10, v11

    move/from16 v0, v22

    if-ne v0, v10, :cond_119f

    .line 724
    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repMask:[I

    const/4 v11, 0x0

    aget v10, v10, v11

    if-nez v10, :cond_108c

    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    const/4 v11, 0x0

    aput-byte v22, v10, v11

    .line 725
    :cond_108c
    const/16 v37, 0x1

    .line 726
    add-int/lit8 v10, v23, 0x1

    move-object/from16 v0, v34

    invoke-virtual {v0, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 727
    invoke-virtual/range {v34 .. v34}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v55

    .line 729
    .restart local v55    # "prufbyte":B
    :goto_1099
    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    aget-byte v10, v10, v37

    move/from16 v0, v55

    if-eq v0, v10, :cond_10ca

    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origMask:[I

    aget v10, v10, v37

    const/4 v11, 0x1

    if-eq v10, v11, :cond_10ca

    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origMask:[I

    aget v10, v10, v37

    const/16 v11, 0x14

    if-eq v10, v11, :cond_10ca

    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origMask:[I

    aget v10, v10, v37

    const/16 v11, 0x15

    if-eq v10, v11, :cond_10ca

    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origMask:[I

    aget v10, v10, v37

    const/16 v11, 0x17

    if-ne v10, v11, :cond_119f

    .line 731
    :cond_10ca
    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repMask:[I

    aget v10, v10, v37

    if-nez v10, :cond_10d8

    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    aput-byte v55, v10, v37

    .line 732
    :cond_10d8
    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repMask:[I

    aget v10, v10, v37

    const/16 v11, 0x14

    if-ne v10, v11, :cond_10eb

    .line 733
    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    and-int/lit8 v11, v55, 0xf

    int-to-byte v11, v11

    aput-byte v11, v10, v37

    .line 736
    :cond_10eb
    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repMask:[I

    aget v10, v10, v37

    const/16 v11, 0x15

    if-ne v10, v11, :cond_1100

    .line 737
    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    and-int/lit8 v11, v55, 0xf

    add-int/lit8 v11, v11, 0x10

    int-to-byte v11, v11

    aput-byte v11, v10, v37

    .line 741
    :cond_1100
    add-int/lit8 v37, v37, 0x1

    .line 742
    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    array-length v10, v10

    move/from16 v0, v37

    if-ne v0, v10, :cond_12a9

    .line 744
    move-object/from16 v0, v34

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 745
    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    move-object/from16 v0, v34

    invoke-virtual {v0, v10}, Ljava/nio/MappedByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 746
    invoke-virtual/range {v34 .. v34}, Ljava/nio/MappedByteBuffer;->force()Ljava/nio/MappedByteBuffer;

    .line 747
    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->resultText:Ljava/lang/String;

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 748
    sget-object v10, Lcom/chelpus/root/utils/runpatchsupportOld;->print:Ljava/io/PrintStream;

    move-object/from16 v0, v52

    iget-object v11, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->resultText:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 749
    const/4 v10, 0x1

    move-object/from16 v0, v52

    iput-boolean v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->result:Z

    .line 750
    const/4 v10, 0x0

    move-object/from16 v0, v52

    iput-boolean v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->markerTrig:Z

    .line 751
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_113c
    :goto_113c
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_1196

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    .line 752
    .restart local v24    # "curpatch":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    move-object/from16 v0, v24

    iget-object v11, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->marker:Ljava/lang/String;

    move-object/from16 v0, v52

    iget-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->marker:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_113c

    .line 753
    const/4 v11, 0x0

    move-object/from16 v0, v24

    iput-boolean v11, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->markerTrig:Z

    goto :goto_113c

    .line 702
    .end local v24    # "curpatch":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    :cond_115c
    invoke-virtual/range {v34 .. v34}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v55

    goto/16 :goto_f51

    .line 707
    .end local v55    # "prufbyte":B
    :cond_1162
    const/4 v10, 0x0

    move-object/from16 v0, v52

    iput-boolean v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->markerTrig:Z

    .line 708
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_116b
    :goto_116b
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_118b

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    .line 709
    .restart local v24    # "curpatch":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    move-object/from16 v0, v24

    iget-object v11, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->marker:Ljava/lang/String;

    move-object/from16 v0, v52

    iget-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->marker:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_116b

    .line 710
    const/4 v11, 0x0

    move-object/from16 v0, v24

    iput-boolean v11, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->markerTrig:Z

    goto :goto_116b

    .line 712
    .end local v24    # "curpatch":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    :cond_118b
    const/16 v61, 0x0

    .line 713
    add-int/lit8 v10, v23, 0x1

    move-object/from16 v0, v34

    invoke-virtual {v0, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    goto/16 :goto_105e

    .line 755
    .restart local v55    # "prufbyte":B
    :cond_1196
    const/16 v60, 0x0

    .line 756
    add-int/lit8 v10, v23, 0x1

    move-object/from16 v0, v34

    invoke-virtual {v0, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 763
    .end local v55    # "prufbyte":B
    :cond_119f
    add-int/lit8 v10, v23, 0x1

    move-object/from16 v0, v34

    invoke-virtual {v0, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 775
    :cond_11a6
    :goto_11a6
    move-object/from16 v0, v52

    iget-boolean v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->markerTrig:Z

    if-nez v10, :cond_12f0

    .line 776
    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v11, 0x0

    aget-byte v10, v10, v11

    move/from16 v0, v22

    if-ne v0, v10, :cond_12f0

    move-object/from16 v0, v52

    iget-boolean v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->pattern:Z

    if-eqz v10, :cond_12f0

    .line 778
    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repMask:[I

    const/4 v11, 0x0

    aget v10, v10, v11

    if-nez v10, :cond_11cd

    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    const/4 v11, 0x0

    aput-byte v22, v10, v11

    .line 779
    :cond_11cd
    const/16 v37, 0x1

    .line 780
    add-int/lit8 v10, v23, 0x1

    move-object/from16 v0, v34

    invoke-virtual {v0, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 781
    invoke-virtual/range {v34 .. v34}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v55

    .line 783
    .restart local v55    # "prufbyte":B
    :goto_11da
    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    aget-byte v10, v10, v37

    move/from16 v0, v55

    if-eq v0, v10, :cond_120b

    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origMask:[I

    aget v10, v10, v37

    const/4 v11, 0x1

    if-eq v10, v11, :cond_120b

    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origMask:[I

    aget v10, v10, v37

    const/16 v11, 0x14

    if-eq v10, v11, :cond_120b

    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origMask:[I

    aget v10, v10, v37

    const/16 v11, 0x15

    if-eq v10, v11, :cond_120b

    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origMask:[I

    aget v10, v10, v37

    const/16 v11, 0x17

    if-ne v10, v11, :cond_12e9

    .line 785
    :cond_120b
    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repMask:[I

    aget v10, v10, v37

    if-nez v10, :cond_1219

    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    aput-byte v55, v10, v37

    .line 786
    :cond_1219
    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repMask:[I

    aget v10, v10, v37

    const/16 v11, 0x14

    if-ne v10, v11, :cond_122c

    .line 787
    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    and-int/lit8 v11, v55, 0xf

    int-to-byte v11, v11

    aput-byte v11, v10, v37

    .line 790
    :cond_122c
    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repMask:[I

    aget v10, v10, v37

    const/16 v11, 0x15

    if-ne v10, v11, :cond_1241

    .line 791
    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    and-int/lit8 v11, v55, 0xf

    add-int/lit8 v11, v11, 0x10

    int-to-byte v11, v11

    aput-byte v11, v10, v37

    .line 795
    :cond_1241
    add-int/lit8 v37, v37, 0x1

    .line 796
    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    array-length v10, v10

    move/from16 v0, v37

    if-ne v0, v10, :cond_12e3

    .line 798
    move-object/from16 v0, v34

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 799
    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    move-object/from16 v0, v34

    invoke-virtual {v0, v10}, Ljava/nio/MappedByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 800
    invoke-virtual/range {v34 .. v34}, Ljava/nio/MappedByteBuffer;->force()Ljava/nio/MappedByteBuffer;

    .line 801
    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->resultText:Ljava/lang/String;

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 802
    sget-object v10, Lcom/chelpus/root/utils/runpatchsupportOld;->print:Ljava/io/PrintStream;

    move-object/from16 v0, v52

    iget-object v11, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->resultText:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 803
    const/4 v10, 0x1

    move-object/from16 v0, v52

    iput-boolean v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->result:Z

    .line 804
    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->marker:Ljava/lang/String;

    const-string v11, ""

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_12e9

    .line 805
    const/4 v10, 0x1

    move-object/from16 v0, v52

    iput-boolean v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->markerTrig:Z

    .line 806
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_1289
    :goto_1289
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_12e9

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    .line 807
    .restart local v24    # "curpatch":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    move-object/from16 v0, v24

    iget-object v11, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->marker:Ljava/lang/String;

    move-object/from16 v0, v52

    iget-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->marker:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1289

    .line 808
    const/4 v11, 0x1

    move-object/from16 v0, v24

    iput-boolean v11, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->markerTrig:Z

    goto :goto_1289

    .line 760
    .end local v24    # "curpatch":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    :cond_12a9
    invoke-virtual/range {v34 .. v34}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v55

    goto/16 :goto_1099

    .line 765
    .end local v55    # "prufbyte":B
    :cond_12af
    const/4 v10, 0x0

    move-object/from16 v0, v52

    iput-boolean v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->markerTrig:Z

    .line 766
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_12b8
    :goto_12b8
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_12d8

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    .line 767
    .restart local v24    # "curpatch":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    move-object/from16 v0, v24

    iget-object v11, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->marker:Ljava/lang/String;

    move-object/from16 v0, v52

    iget-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->marker:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_12b8

    .line 768
    const/4 v11, 0x0

    move-object/from16 v0, v24

    iput-boolean v11, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->markerTrig:Z

    goto :goto_12b8

    .line 770
    .end local v24    # "curpatch":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    :cond_12d8
    const/16 v60, 0x0

    .line 771
    add-int/lit8 v10, v23, 0x1

    move-object/from16 v0, v34

    invoke-virtual {v0, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    goto/16 :goto_11a6

    .line 814
    .restart local v55    # "prufbyte":B
    :cond_12e3
    invoke-virtual/range {v34 .. v34}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v55

    goto/16 :goto_11da

    .line 816
    :cond_12e9
    add-int/lit8 v10, v23, 0x1

    move-object/from16 v0, v34

    invoke-virtual {v0, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 597
    .end local v55    # "prufbyte":B
    :cond_12f0
    add-int/lit8 v17, v17, 0x1

    goto/16 :goto_d84

    .line 824
    .end local v52    # "patches":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    :cond_12f4
    add-int/lit8 v10, v23, 0x1

    move-object/from16 v0, v34

    invoke-virtual {v0, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;
    :try_end_12fb
    .catch Ljava/lang/Exception; {:try_start_ef8 .. :try_end_12fb} :catch_eb2
    .catch Ljava/io/FileNotFoundException; {:try_start_ef8 .. :try_end_12fb} :catch_4cb

    .line 583
    const-wide/16 v10, 0x1

    add-long v41, v41, v10

    goto/16 :goto_d45

    .line 847
    .end local v9    # "ChannelDex":Ljava/nio/channels/FileChannel;
    .end local v17    # "b":I
    .end local v19    # "commands":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;>;"
    .end local v21    # "count":I
    .end local v22    # "curentByte":B
    .end local v23    # "curentPos":I
    .end local v25    # "diaposon":I
    .end local v26    # "diaposon_ev":I
    .end local v27    # "diaposon_pak":I
    .end local v29    # "ev":Z
    .end local v30    # "f1":Z
    .end local v31    # "f2":Z
    .end local v32    # "f3":Z
    .end local v34    # "fileBytes":Ljava/nio/MappedByteBuffer;
    .end local v35    # "filepatch":Ljava/io/File;
    .end local v37    # "i":I
    .end local v38    # "increase":Z
    .end local v41    # "j":J
    .end local v46    # "of_to_patch":I
    .end local v47    # "offsets":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;>;"
    .end local v49    # "patchList":[Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    .end local v50    # "patch_index1":I
    .end local v51    # "patch_index2":I
    .end local v53    # "period":I
    .end local v57    # "sites":[[B
    .end local v58    # "sitesFromFile":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
    .end local v59    # "spi":Z
    .end local v60    # "start_for_diaposon":I
    .end local v61    # "start_for_diaposon_ev":I
    .end local v62    # "start_for_diaposon_pak":I
    .end local v63    # "strings":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v64    # "time":J
    .end local v66    # "types":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/TypesItem;>;"
    .end local v67    # "u":I
    :cond_1301
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchsupportOld;->createAPK:Z

    if-nez v10, :cond_1357

    .line 848
    const-string v10, "Create ODEX:"

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 849
    const/4 v10, 0x3

    aget-object v10, p0, v10

    sget-object v11, Lcom/chelpus/root/utils/runpatchsupportOld;->classesFiles:Ljava/util/ArrayList;

    const/4 v12, 0x2

    aget-object v12, p0, v12

    sget-object v13, Lcom/chelpus/root/utils/runpatchsupportOld;->uid:Ljava/lang/String;

    const/4 v14, 0x2

    aget-object v14, p0, v14

    sget-object v68, Lcom/chelpus/root/utils/runpatchsupportOld;->uid:Ljava/lang/String;

    move-object/from16 v0, v68

    invoke-static {v14, v0}, Lcom/chelpus/Utils;->getOdexForCreate(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-static {v10, v11, v12, v13, v14}, Lcom/chelpus/Utils;->create_ODEX_root(Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v56

    .line 850
    .local v56, "r":I
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "chelpus_return_"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v56

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 851
    if-nez v56, :cond_1357

    sget-boolean v10, Lcom/chelpus/root/utils/runpatchsupportOld;->ART:Z

    if-nez v10, :cond_1357

    .line 852
    const/4 v10, 0x1

    aget-object v10, p0, v10

    const/4 v11, 0x2

    aget-object v11, p0, v11

    const/4 v12, 0x2

    aget-object v12, p0, v12

    const/4 v13, 0x1

    invoke-static {v12, v13}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v12

    sget-object v13, Lcom/chelpus/root/utils/runpatchsupportOld;->uid:Ljava/lang/String;

    const/4 v14, 0x3

    aget-object v14, p0, v14

    invoke-static {v10, v11, v12, v13, v14}, Lcom/chelpus/Utils;->afterPatch(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 856
    .end local v56    # "r":I
    :cond_1357
    const-string v10, "Optional Steps After Patch:"

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 857
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchsupportOld;->createAPK:Z

    if-nez v10, :cond_1363

    invoke-static {}, Lcom/chelpus/Utils;->exitFromRootJava()V

    .line 858
    :cond_1363
    move-object/from16 v0, v54

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/LogOutputStream;->allresult:Ljava/lang/String;

    sput-object v10, Lcom/chelpus/root/utils/runpatchsupportOld;->result:Ljava/lang/String;

    .line 859
    return-void

    .line 91
    .end local v3    # "origStr":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v4    # "replStr":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v5    # "trigger":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Boolean;>;"
    .end local v6    # "ResultText":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v7    # "markers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v8    # "dontConvert":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Boolean;>;"
    .end local v20    # "conv":Z
    :catch_136a
    move-exception v10

    goto/16 :goto_111

    .line 90
    :catch_136d
    move-exception v10

    goto/16 :goto_111

    .line 87
    :catch_1370
    move-exception v10

    goto/16 :goto_103

    .line 86
    :catch_1373
    move-exception v10

    goto/16 :goto_103
.end method

.method public static unzipART(Ljava/io/File;)V
    .registers 23
    .param p0, "apk"    # Ljava/io/File;

    .prologue
    .line 1040
    const/4 v7, 0x0

    .local v7, "found1":Z
    const/4 v8, 0x0

    .line 1042
    .local v8, "found2":Z
    :try_start_2
    new-instance v6, Ljava/io/FileInputStream;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 1043
    .local v6, "fin":Ljava/io/FileInputStream;
    new-instance v16, Ljava/util/zip/ZipInputStream;

    move-object/from16 v0, v16

    invoke-direct {v0, v6}, Ljava/util/zip/ZipInputStream;-><init>(Ljava/io/InputStream;)V

    .line 1044
    .local v16, "zin":Ljava/util/zip/ZipInputStream;
    const/4 v15, 0x0

    .line 1046
    .local v15, "ze":Ljava/util/zip/ZipEntry;
    invoke-virtual/range {v16 .. v16}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    move-result-object v15

    .line 1047
    const/4 v14, 0x1

    .line 1048
    .local v14, "search":Z
    :goto_16
    if-eqz v15, :cond_227

    if-eqz v14, :cond_227

    .line 1054
    invoke-virtual {v15}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v11

    .line 1056
    .local v11, "haystack":Ljava/lang/String;
    invoke-virtual {v11}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v18

    const-string v19, "classes"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_19f

    const-string v18, ".dex"

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_19f

    const-string v18, "/"

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_19f

    .line 1058
    new-instance v9, Ljava/io/FileOutputStream;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v19, Lcom/chelpus/root/utils/runpatchsupportOld;->sddir:Ljava/lang/String;

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "/"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-direct {v9, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 1060
    .local v9, "fout":Ljava/io/FileOutputStream;
    const/16 v18, 0x800

    move/from16 v0, v18

    new-array v2, v0, [B

    .line 1063
    .local v2, "buffer":[B
    :goto_66
    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/util/zip/ZipInputStream;->read([B)I

    move-result v12

    .local v12, "length":I
    const/16 v18, -0x1

    move/from16 v0, v18

    if-eq v12, v0, :cond_13e

    .line 1064
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v9, v2, v0, v12}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_79
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_79} :catch_7a

    goto :goto_66

    .line 1096
    .end local v2    # "buffer":[B
    .end local v6    # "fin":Ljava/io/FileInputStream;
    .end local v9    # "fout":Ljava/io/FileOutputStream;
    .end local v11    # "haystack":Ljava/lang/String;
    .end local v12    # "length":I
    .end local v14    # "search":Z
    .end local v15    # "ze":Ljava/util/zip/ZipEntry;
    .end local v16    # "zin":Ljava/util/zip/ZipInputStream;
    :catch_7a
    move-exception v4

    .line 1098
    .local v4, "e":Ljava/lang/Exception;
    :try_start_7b
    new-instance v17, Lnet/lingala/zip4j/core/ZipFile;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lnet/lingala/zip4j/core/ZipFile;-><init>(Ljava/io/File;)V

    .line 1102
    .local v17, "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    const-string v18, "classes.dex"

    sget-object v19, Lcom/chelpus/root/utils/runpatchsupportOld;->sddir:Ljava/lang/String;

    invoke-virtual/range {v17 .. v19}, Lnet/lingala/zip4j/core/ZipFile;->extractFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 1103
    sget-object v18, Lcom/chelpus/root/utils/runpatchsupportOld;->classesFiles:Ljava/util/ArrayList;

    new-instance v19, Ljava/io/File;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v21, Lcom/chelpus/root/utils/runpatchsupportOld;->sddir:Ljava/lang/String;

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "classes.dex"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v18 .. v19}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1104
    const/16 v18, 0x3

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const-string v20, "chmod"

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, "777"

    aput-object v20, v18, v19

    const/16 v19, 0x2

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v21, Lcom/chelpus/root/utils/runpatchsupportOld;->sddir:Ljava/lang/String;

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "classes.dex"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    aput-object v20, v18, v19

    invoke-static/range {v18 .. v18}, Lcom/chelpus/Utils;->cmdParam([Ljava/lang/String;)Ljava/lang/String;

    .line 1105
    const-string v18, "AndroidManifest.xml"

    sget-object v19, Lcom/chelpus/root/utils/runpatchsupportOld;->sddir:Ljava/lang/String;

    invoke-virtual/range {v17 .. v19}, Lnet/lingala/zip4j/core/ZipFile;->extractFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 1106
    const/16 v18, 0x3

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const-string v20, "chmod"

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, "777"

    aput-object v20, v18, v19

    const/16 v19, 0x2

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v21, Lcom/chelpus/root/utils/runpatchsupportOld;->sddir:Ljava/lang/String;

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "AndroidManifest.xml"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    aput-object v20, v18, v19

    invoke-static/range {v18 .. v18}, Lcom/chelpus/Utils;->cmdParam([Ljava/lang/String;)Ljava/lang/String;
    :try_end_123
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_7b .. :try_end_123} :catch_235
    .catch Ljava/lang/Exception; {:try_start_7b .. :try_end_123} :catch_26a

    .line 1115
    .end local v17    # "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    :goto_123
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Exception e"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v4}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 1119
    .end local v4    # "e":Ljava/lang/Exception;
    :goto_13d
    return-void

    .line 1068
    .restart local v2    # "buffer":[B
    .restart local v6    # "fin":Ljava/io/FileInputStream;
    .restart local v9    # "fout":Ljava/io/FileOutputStream;
    .restart local v11    # "haystack":Ljava/lang/String;
    .restart local v12    # "length":I
    .restart local v14    # "search":Z
    .restart local v15    # "ze":Ljava/util/zip/ZipEntry;
    .restart local v16    # "zin":Ljava/util/zip/ZipInputStream;
    :cond_13e
    :try_start_13e
    invoke-virtual/range {v16 .. v16}, Ljava/util/zip/ZipInputStream;->closeEntry()V

    .line 1069
    invoke-virtual {v9}, Ljava/io/FileOutputStream;->close()V

    .line 1070
    sget-object v18, Lcom/chelpus/root/utils/runpatchsupportOld;->classesFiles:Ljava/util/ArrayList;

    new-instance v19, Ljava/io/File;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v21, Lcom/chelpus/root/utils/runpatchsupportOld;->sddir:Ljava/lang/String;

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v18 .. v19}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1071
    const/16 v18, 0x3

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const-string v20, "chmod"

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, "777"

    aput-object v20, v18, v19

    const/16 v19, 0x2

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v21, Lcom/chelpus/root/utils/runpatchsupportOld;->sddir:Ljava/lang/String;

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    aput-object v20, v18, v19

    invoke-static/range {v18 .. v18}, Lcom/chelpus/Utils;->cmdParam([Ljava/lang/String;)Ljava/lang/String;

    .line 1074
    .end local v2    # "buffer":[B
    .end local v9    # "fout":Ljava/io/FileOutputStream;
    .end local v12    # "length":I
    :cond_19f
    const-string v18, "AndroidManifest.xml"

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_222

    .line 1075
    new-instance v10, Ljava/io/FileOutputStream;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v19, Lcom/chelpus/root/utils/runpatchsupportOld;->sddir:Ljava/lang/String;

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "/"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "AndroidManifest.xml"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-direct {v10, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 1077
    .local v10, "fout2":Ljava/io/FileOutputStream;
    const/16 v18, 0x800

    move/from16 v0, v18

    new-array v3, v0, [B

    .line 1079
    .local v3, "buffer2":[B
    :goto_1d1
    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/util/zip/ZipInputStream;->read([B)I

    move-result v13

    .local v13, "length2":I
    const/16 v18, -0x1

    move/from16 v0, v18

    if-eq v13, v0, :cond_1e5

    .line 1080
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v10, v3, v0, v13}, Ljava/io/FileOutputStream;->write([BII)V

    goto :goto_1d1

    .line 1082
    :cond_1e5
    invoke-virtual/range {v16 .. v16}, Ljava/util/zip/ZipInputStream;->closeEntry()V

    .line 1083
    invoke-virtual {v10}, Ljava/io/FileOutputStream;->close()V

    .line 1084
    const/16 v18, 0x3

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const-string v20, "chmod"

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, "777"

    aput-object v20, v18, v19

    const/16 v19, 0x2

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v21, Lcom/chelpus/root/utils/runpatchsupportOld;->sddir:Ljava/lang/String;

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "AndroidManifest.xml"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    aput-object v20, v18, v19

    invoke-static/range {v18 .. v18}, Lcom/chelpus/Utils;->cmdParam([Ljava/lang/String;)Ljava/lang/String;

    .line 1085
    const/4 v8, 0x1

    .line 1087
    .end local v3    # "buffer2":[B
    .end local v10    # "fout2":Ljava/io/FileOutputStream;
    .end local v13    # "length2":I
    :cond_222
    if-eqz v7, :cond_22f

    if-eqz v8, :cond_22f

    .line 1088
    const/4 v14, 0x0

    .line 1094
    .end local v11    # "haystack":Ljava/lang/String;
    :cond_227
    invoke-virtual/range {v16 .. v16}, Ljava/util/zip/ZipInputStream;->close()V

    .line 1095
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V

    goto/16 :goto_13d

    .line 1091
    .restart local v11    # "haystack":Ljava/lang/String;
    :cond_22f
    invoke-virtual/range {v16 .. v16}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;
    :try_end_232
    .catch Ljava/lang/Exception; {:try_start_13e .. :try_end_232} :catch_7a

    move-result-object v15

    .line 1093
    goto/16 :goto_16

    .line 1108
    .end local v6    # "fin":Ljava/io/FileInputStream;
    .end local v11    # "haystack":Ljava/lang/String;
    .end local v14    # "search":Z
    .end local v15    # "ze":Ljava/util/zip/ZipEntry;
    .end local v16    # "zin":Ljava/util/zip/ZipInputStream;
    .restart local v4    # "e":Ljava/lang/Exception;
    :catch_235
    move-exception v5

    .line 1109
    .local v5, "e1":Lnet/lingala/zip4j/exception/ZipException;
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Error classes.dex decompress! "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 1110
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Exception e1"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v4}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    goto/16 :goto_123

    .line 1111
    .end local v5    # "e1":Lnet/lingala/zip4j/exception/ZipException;
    :catch_26a
    move-exception v5

    .line 1112
    .local v5, "e1":Ljava/lang/Exception;
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Error classes.dex decompress! "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 1113
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Exception e1"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v4}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    goto/16 :goto_123
.end method

.method public static unzipSD(Ljava/io/File;)V
    .registers 16
    .param p0, "apk"    # Ljava/io/File;

    .prologue
    const/4 v14, -0x1

    .line 973
    :try_start_1
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 974
    .local v4, "fin":Ljava/io/FileInputStream;
    new-instance v8, Ljava/util/zip/ZipInputStream;

    invoke-direct {v8, v4}, Ljava/util/zip/ZipInputStream;-><init>(Ljava/io/InputStream;)V

    .line 975
    .local v8, "zin":Ljava/util/zip/ZipInputStream;
    const/4 v7, 0x0

    .line 976
    .local v7, "ze":Ljava/util/zip/ZipEntry;
    const/4 v1, 0x0

    .line 977
    .local v1, "classesdex":Z
    :cond_d
    :goto_d
    invoke-virtual {v8}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    move-result-object v7

    if-eqz v7, :cond_159

    .line 982
    invoke-virtual {v7}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v10

    const-string v11, "classes"

    invoke-virtual {v10, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_114

    invoke-virtual {v7}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v10

    const-string v11, ".dex"

    invoke-virtual {v10, v11}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_114

    invoke-virtual {v7}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v10

    const-string v11, "/"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_114

    .line 983
    new-instance v5, Ljava/io/FileOutputStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v11, Lcom/chelpus/root/utils/runpatchsupportOld;->sddir:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "/Modified/"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v7}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v5, v10}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 985
    .local v5, "fout":Ljava/io/FileOutputStream;
    const/16 v10, 0x400

    new-array v0, v10, [B

    .line 987
    .local v0, "buffer":[B
    :goto_61
    invoke-virtual {v8, v0}, Ljava/util/zip/ZipInputStream;->read([B)I

    move-result v6

    .local v6, "length":I
    if-eq v6, v14, :cond_e2

    .line 988
    const/4 v10, 0x0

    invoke-virtual {v5, v0, v10, v6}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_6b
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_6b} :catch_6c

    goto :goto_61

    .line 1016
    .end local v0    # "buffer":[B
    .end local v1    # "classesdex":Z
    .end local v4    # "fin":Ljava/io/FileInputStream;
    .end local v5    # "fout":Ljava/io/FileOutputStream;
    .end local v6    # "length":I
    .end local v7    # "ze":Ljava/util/zip/ZipEntry;
    .end local v8    # "zin":Ljava/util/zip/ZipInputStream;
    :catch_6c
    move-exception v2

    .line 1018
    .local v2, "e":Ljava/lang/Exception;
    :try_start_6d
    new-instance v9, Lnet/lingala/zip4j/core/ZipFile;

    invoke-direct {v9, p0}, Lnet/lingala/zip4j/core/ZipFile;-><init>(Ljava/io/File;)V

    .line 1022
    .local v9, "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    const-string v10, "classes.dex"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v12, Lcom/chelpus/root/utils/runpatchsupportOld;->sddir:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/Modified/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Lnet/lingala/zip4j/core/ZipFile;->extractFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 1023
    sget-object v10, Lcom/chelpus/root/utils/runpatchsupportOld;->classesFiles:Ljava/util/ArrayList;

    new-instance v11, Ljava/io/File;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v13, Lcom/chelpus/root/utils/runpatchsupportOld;->sddir:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "/Modified/"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "classes.dex"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1024
    const-string v10, "AndroidManifest.xml"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v12, Lcom/chelpus/root/utils/runpatchsupportOld;->sddir:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/Modified/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Lnet/lingala/zip4j/core/ZipFile;->extractFile(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_cb
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_6d .. :try_end_cb} :catch_160
    .catch Ljava/lang/Exception; {:try_start_6d .. :try_end_cb} :catch_193

    .line 1033
    .end local v9    # "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    :goto_cb
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Decompressunzip "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 1037
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_e1
    return-void

    .line 990
    .restart local v0    # "buffer":[B
    .restart local v1    # "classesdex":Z
    .restart local v4    # "fin":Ljava/io/FileInputStream;
    .restart local v5    # "fout":Ljava/io/FileOutputStream;
    .restart local v6    # "length":I
    .restart local v7    # "ze":Ljava/util/zip/ZipEntry;
    .restart local v8    # "zin":Ljava/util/zip/ZipInputStream;
    :cond_e2
    :try_start_e2
    sget-object v10, Lcom/chelpus/root/utils/runpatchsupportOld;->classesFiles:Ljava/util/ArrayList;

    new-instance v11, Ljava/io/File;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v13, Lcom/chelpus/root/utils/runpatchsupportOld;->sddir:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "/Modified/"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v7}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 991
    const/4 v1, 0x1

    .line 992
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchsupportOld;->createAPK:Z

    if-nez v10, :cond_114

    .line 993
    invoke-virtual {v8}, Ljava/util/zip/ZipInputStream;->closeEntry()V

    .line 994
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V

    .line 997
    .end local v0    # "buffer":[B
    .end local v5    # "fout":Ljava/io/FileOutputStream;
    .end local v6    # "length":I
    :cond_114
    invoke-virtual {v7}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v10

    const-string v11, "AndroidManifest.xml"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_d

    .line 998
    new-instance v5, Ljava/io/FileOutputStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v11, Lcom/chelpus/root/utils/runpatchsupportOld;->sddir:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "/Modified/"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "AndroidManifest.xml"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v5, v10}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 1000
    .restart local v5    # "fout":Ljava/io/FileOutputStream;
    const/16 v10, 0x400

    new-array v0, v10, [B

    .line 1002
    .restart local v0    # "buffer":[B
    :goto_144
    invoke-virtual {v8, v0}, Ljava/util/zip/ZipInputStream;->read([B)I

    move-result v6

    .restart local v6    # "length":I
    if-eq v6, v14, :cond_14f

    .line 1003
    const/4 v10, 0x0

    invoke-virtual {v5, v0, v10, v6}, Ljava/io/FileOutputStream;->write([BII)V

    goto :goto_144

    .line 1006
    :cond_14f
    if-eqz v1, :cond_d

    .line 1007
    invoke-virtual {v8}, Ljava/util/zip/ZipInputStream;->closeEntry()V

    .line 1008
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V

    goto/16 :goto_d

    .line 1014
    .end local v0    # "buffer":[B
    .end local v5    # "fout":Ljava/io/FileOutputStream;
    .end local v6    # "length":I
    :cond_159
    invoke-virtual {v8}, Ljava/util/zip/ZipInputStream;->close()V

    .line 1015
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_15f
    .catch Ljava/lang/Exception; {:try_start_e2 .. :try_end_15f} :catch_6c

    goto :goto_e1

    .line 1026
    .end local v1    # "classesdex":Z
    .end local v4    # "fin":Ljava/io/FileInputStream;
    .end local v7    # "ze":Ljava/util/zip/ZipEntry;
    .end local v8    # "zin":Ljava/util/zip/ZipInputStream;
    .restart local v2    # "e":Ljava/lang/Exception;
    :catch_160
    move-exception v3

    .line 1027
    .local v3, "e1":Lnet/lingala/zip4j/exception/ZipException;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Error classes.dex decompress! "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 1028
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Exception e1"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    goto/16 :goto_cb

    .line 1029
    .end local v3    # "e1":Lnet/lingala/zip4j/exception/ZipException;
    :catch_193
    move-exception v3

    .line 1030
    .local v3, "e1":Ljava/lang/Exception;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Error classes.dex decompress! "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 1031
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Exception e1"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    goto/16 :goto_cb
.end method
