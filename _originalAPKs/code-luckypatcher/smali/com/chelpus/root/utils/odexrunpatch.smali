.class public Lcom/chelpus/root/utils/odexrunpatch;
.super Ljava/lang/Object;
.source "odexrunpatch.java"


# static fields
.field public static ART:Z

.field private static amazon:Z

.field public static appdir:Ljava/lang/String;

.field public static classesFiles:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field public static copyDC:Z

.field private static createAPK:Z

.field public static crkapk:Ljava/io/File;

.field private static dependencies:Z

.field public static dir:Ljava/lang/String;

.field public static dirapp:Ljava/lang/String;

.field public static filestopatch:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private static pattern1:Z

.field private static pattern2:Z

.field private static pattern3:Z

.field private static pattern4:Z

.field private static pattern5:Z

.field private static pattern6:Z

.field public static print:Ljava/io/PrintStream;

.field public static result:Ljava/lang/String;

.field private static samsung:Z

.field public static sddir:Ljava/lang/String;

.field public static system:Z

.field public static uid:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 30
    sput-boolean v1, Lcom/chelpus/root/utils/odexrunpatch;->createAPK:Z

    .line 31
    sput-boolean v0, Lcom/chelpus/root/utils/odexrunpatch;->pattern1:Z

    .line 32
    sput-boolean v0, Lcom/chelpus/root/utils/odexrunpatch;->pattern2:Z

    .line 33
    sput-boolean v0, Lcom/chelpus/root/utils/odexrunpatch;->pattern3:Z

    .line 34
    sput-boolean v0, Lcom/chelpus/root/utils/odexrunpatch;->pattern4:Z

    .line 35
    sput-boolean v0, Lcom/chelpus/root/utils/odexrunpatch;->pattern5:Z

    .line 36
    sput-boolean v0, Lcom/chelpus/root/utils/odexrunpatch;->pattern6:Z

    .line 37
    sput-boolean v0, Lcom/chelpus/root/utils/odexrunpatch;->dependencies:Z

    .line 38
    sput-boolean v0, Lcom/chelpus/root/utils/odexrunpatch;->amazon:Z

    .line 39
    sput-boolean v0, Lcom/chelpus/root/utils/odexrunpatch;->samsung:Z

    .line 40
    const-string v0, "/sdcard/"

    sput-object v0, Lcom/chelpus/root/utils/odexrunpatch;->dir:Ljava/lang/String;

    .line 41
    const-string v0, ""

    sput-object v0, Lcom/chelpus/root/utils/odexrunpatch;->uid:Ljava/lang/String;

    .line 42
    const/4 v0, 0x0

    sput-object v0, Lcom/chelpus/root/utils/odexrunpatch;->filestopatch:Ljava/util/ArrayList;

    .line 43
    sput-boolean v1, Lcom/chelpus/root/utils/odexrunpatch;->system:Z

    .line 44
    sput-boolean v1, Lcom/chelpus/root/utils/odexrunpatch;->copyDC:Z

    .line 45
    sput-boolean v1, Lcom/chelpus/root/utils/odexrunpatch;->ART:Z

    .line 46
    const-string v0, "/data/app/"

    sput-object v0, Lcom/chelpus/root/utils/odexrunpatch;->dirapp:Ljava/lang/String;

    .line 47
    const-string v0, "/sdcard/"

    sput-object v0, Lcom/chelpus/root/utils/odexrunpatch;->sddir:Ljava/lang/String;

    .line 48
    const-string v0, "/sdcard/"

    sput-object v0, Lcom/chelpus/root/utils/odexrunpatch;->appdir:Ljava/lang/String;

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/chelpus/root/utils/odexrunpatch;->classesFiles:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static byteverify(Ljava/nio/MappedByteBuffer;IB[B[B[B[BLjava/lang/String;Z)Z
    .registers 14
    .param p0, "fileBytes"    # Ljava/nio/MappedByteBuffer;
    .param p1, "curentPos"    # I
    .param p2, "curentByte"    # B
    .param p3, "byteOrig"    # [B
    .param p4, "mask"    # [B
    .param p5, "byteReplace"    # [B
    .param p6, "rep_mask"    # [B
    .param p7, "log"    # Ljava/lang/String;
    .param p8, "pattern"    # Z

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1058
    aget-byte v4, p3, v3

    if-ne p2, v4, :cond_42

    if-eqz p8, :cond_42

    .line 1060
    aget-byte v4, p6, v3

    if-nez v4, :cond_e

    aput-byte p2, p5, v3

    .line 1061
    :cond_e
    const/4 v0, 0x1

    .line 1062
    .local v0, "i":I
    add-int/lit8 v4, p1, 0x1

    invoke-virtual {p0, v4}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1063
    invoke-virtual {p0}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v1

    .line 1065
    .local v1, "prufbyte":B
    :goto_18
    aget-byte v4, p3, v0

    if-eq v1, v4, :cond_20

    aget-byte v4, p4, v0

    if-ne v4, v2, :cond_3d

    .line 1067
    :cond_20
    aget-byte v4, p6, v0

    if-nez v4, :cond_26

    aput-byte v1, p5, v0

    .line 1068
    :cond_26
    add-int/lit8 v0, v0, 0x1

    .line 1070
    array-length v4, p3

    if-ne v0, v4, :cond_38

    .line 1072
    invoke-virtual {p0, p1}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1073
    invoke-virtual {p0, p5}, Ljava/nio/MappedByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 1074
    invoke-virtual {p0}, Ljava/nio/MappedByteBuffer;->force()Ljava/nio/MappedByteBuffer;

    .line 1076
    invoke-static {p7}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 1085
    .end local v0    # "i":I
    .end local v1    # "prufbyte":B
    :goto_37
    return v2

    .line 1081
    .restart local v0    # "i":I
    .restart local v1    # "prufbyte":B
    :cond_38
    invoke-virtual {p0}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v1

    goto :goto_18

    .line 1083
    :cond_3d
    add-int/lit8 v2, p1, 0x1

    invoke-virtual {p0, v2}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .end local v0    # "i":I
    .end local v1    # "prufbyte":B
    :cond_42
    move v2, v3

    .line 1085
    goto :goto_37
.end method

.method public static clearTemp()V
    .registers 6

    .prologue
    .line 1037
    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/chelpus/root/utils/odexrunpatch;->dir:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/AndroidManifest.xml"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1038
    .local v3, "tmp":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1039
    .local v2, "tempdex":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_23

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 1040
    :cond_23
    sget-object v4, Lcom/chelpus/root/utils/odexrunpatch;->classesFiles:Ljava/util/ArrayList;

    if-eqz v4, :cond_67

    sget-object v4, Lcom/chelpus/root/utils/odexrunpatch;->classesFiles:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_67

    .line 1041
    sget-object v4, Lcom/chelpus/root/utils/odexrunpatch;->classesFiles:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_35
    :goto_35
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_67

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 1042
    .local v0, "cl":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_35

    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_4a
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_4a} :catch_4b

    goto :goto_35

    .line 1051
    .end local v0    # "cl":Ljava/io/File;
    .end local v2    # "tempdex":Ljava/io/File;
    :catch_4b
    move-exception v1

    .line 1053
    .local v1, "e":Ljava/lang/Exception;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 1055
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_66
    :goto_66
    return-void

    .line 1045
    .restart local v2    # "tempdex":Ljava/io/File;
    :cond_67
    :try_start_67
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/chelpus/root/utils/odexrunpatch;->dir:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/classes.dex"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1046
    new-instance v2, Ljava/io/File;

    .end local v2    # "tempdex":Ljava/io/File;
    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1047
    .restart local v2    # "tempdex":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_8a

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 1048
    :cond_8a
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/chelpus/root/utils/odexrunpatch;->dir:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/classes.dex.apk"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1049
    new-instance v2, Ljava/io/File;

    .end local v2    # "tempdex":Ljava/io/File;
    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1050
    .restart local v2    # "tempdex":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_66

    invoke-virtual {v2}, Ljava/io/File;->delete()Z
    :try_end_ad
    .catch Ljava/lang/Exception; {:try_start_67 .. :try_end_ad} :catch_4b

    goto :goto_66
.end method

.method public static clearTempSD()V
    .registers 5

    .prologue
    .line 1266
    :try_start_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/chelpus/root/utils/odexrunpatch;->sddir:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/Modified/classes.dex.apk"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1267
    .local v2, "tmp":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1268
    .local v1, "tempdex":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_23

    invoke-virtual {v1}, Ljava/io/File;->delete()Z
    :try_end_23
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_23} :catch_24

    .line 1272
    .end local v1    # "tempdex":Ljava/io/File;
    :cond_23
    :goto_23
    return-void

    .line 1269
    :catch_24
    move-exception v0

    .line 1270
    .local v0, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    goto :goto_23
.end method

.method public static main([Ljava/lang/String;)V
    .registers 81
    .param p0, "paramArrayOfString"    # [Ljava/lang/String;

    .prologue
    .line 58
    new-instance v65, Lcom/android/vending/billing/InAppBillingService/LUCK/LogOutputStream;

    const-string v10, "System.out"

    move-object/from16 v0, v65

    invoke-direct {v0, v10}, Lcom/android/vending/billing/InAppBillingService/LUCK/LogOutputStream;-><init>(Ljava/lang/String;)V

    .line 59
    .local v65, "pout":Lcom/android/vending/billing/InAppBillingService/LUCK/LogOutputStream;
    new-instance v10, Ljava/io/PrintStream;

    move-object/from16 v0, v65

    invoke-direct {v10, v0}, Ljava/io/PrintStream;-><init>(Ljava/io/OutputStream;)V

    sput-object v10, Lcom/chelpus/root/utils/odexrunpatch;->print:Ljava/io/PrintStream;

    .line 61
    new-instance v10, Lcom/chelpus/root/utils/odexrunpatch$1;

    invoke-direct {v10}, Lcom/chelpus/root/utils/odexrunpatch$1;-><init>()V

    invoke-static {v10}, Lcom/chelpus/Utils;->startRootJava(Ljava/lang/Object;)V

    .line 62
    const/4 v10, 0x0

    aget-object v10, p0, v10

    invoke-static {v10}, Lcom/chelpus/Utils;->kill(Ljava/lang/String;)V

    .line 63
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 64
    .local v2, "patchesList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;>;"
    const/4 v10, 0x1

    sput-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->pattern1:Z

    .line 65
    const/4 v10, 0x1

    sput-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->pattern2:Z

    .line 66
    const/4 v10, 0x1

    sput-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->pattern3:Z

    .line 67
    const/4 v10, 0x1

    sput-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->pattern4:Z

    .line 68
    const/4 v10, 0x1

    sput-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->pattern5:Z

    .line 69
    const/4 v10, 0x1

    sput-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->pattern6:Z

    .line 70
    const/4 v10, 0x1

    sput-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->dependencies:Z

    .line 71
    const/4 v10, 0x1

    sput-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->amazon:Z

    .line 72
    const/4 v10, 0x1

    sput-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->samsung:Z

    .line 73
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    sput-object v10, Lcom/chelpus/root/utils/odexrunpatch;->filestopatch:Ljava/util/ArrayList;

    .line 75
    :try_start_47
    new-instance v10, Ljava/io/File;

    const/4 v11, 0x3

    aget-object v11, p0, v11

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v42

    .line 76
    .local v42, "files":[Ljava/io/File;
    move-object/from16 v0, v42

    array-length v11, v0

    const/4 v10, 0x0

    :goto_57
    if-ge v10, v11, :cond_8f

    aget-object v38, v42, v10

    .line 77
    .local v38, "file":Ljava/io/File;
    invoke-virtual/range {v38 .. v38}, Ljava/io/File;->isFile()Z

    move-result v12

    if-eqz v12, :cond_88

    invoke-virtual/range {v38 .. v38}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v12

    const-string v13, "busybox"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_88

    invoke-virtual/range {v38 .. v38}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v12

    const-string v13, "reboot"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_88

    invoke-virtual/range {v38 .. v38}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v12

    const-string v13, "dalvikvm"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_88

    invoke-virtual/range {v38 .. v38}, Ljava/io/File;->delete()Z
    :try_end_88
    .catch Ljava/lang/Exception; {:try_start_47 .. :try_end_88} :catch_8b

    .line 76
    :cond_88
    add-int/lit8 v10, v10, 0x1

    goto :goto_57

    .line 79
    .end local v38    # "file":Ljava/io/File;
    .end local v42    # "files":[Ljava/io/File;
    :catch_8b
    move-exception v34

    .local v34, "e":Ljava/lang/Exception;
    invoke-virtual/range {v34 .. v34}, Ljava/lang/Exception;->printStackTrace()V

    .line 82
    .end local v34    # "e":Ljava/lang/Exception;
    :cond_8f
    const/4 v10, 0x1

    :try_start_90
    aget-object v10, p0, v10

    const-string v11, "pattern1"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_9d

    const/4 v10, 0x0

    sput-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->pattern1:Z

    .line 83
    :cond_9d
    const/4 v10, 0x1

    aget-object v10, p0, v10

    const-string v11, "pattern2"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_ab

    const/4 v10, 0x0

    sput-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->pattern2:Z

    .line 84
    :cond_ab
    const/4 v10, 0x1

    aget-object v10, p0, v10

    const-string v11, "pattern3"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_b9

    const/4 v10, 0x0

    sput-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->pattern3:Z

    .line 85
    :cond_b9
    const/4 v10, 0x1

    aget-object v10, p0, v10

    const-string v11, "pattern4"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_c7

    const/4 v10, 0x0

    sput-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->pattern4:Z

    .line 86
    :cond_c7
    const/4 v10, 0x1

    aget-object v10, p0, v10

    const-string v11, "pattern5"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_d5

    const/4 v10, 0x0

    sput-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->pattern5:Z

    .line 87
    :cond_d5
    const/4 v10, 0x1

    aget-object v10, p0, v10

    const-string v11, "pattern6"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_e3

    const/4 v10, 0x0

    sput-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->pattern6:Z

    .line 88
    :cond_e3
    const/4 v10, 0x1

    aget-object v10, p0, v10

    const-string v11, "dependencies"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_f1

    const/4 v10, 0x0

    sput-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->dependencies:Z

    .line 89
    :cond_f1
    const/4 v10, 0x1

    aget-object v10, p0, v10

    const-string v11, "amazon"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_ff

    const/4 v10, 0x0

    sput-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->amazon:Z

    .line 90
    :cond_ff
    const/4 v10, 0x1

    aget-object v10, p0, v10

    const-string v11, "samsung"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_10d

    const/4 v10, 0x0

    sput-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->samsung:Z

    .line 91
    :cond_10d
    const/4 v10, 0x6

    aget-object v10, p0, v10

    const-string v11, "createAPK"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_11b

    const/4 v10, 0x1

    sput-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->createAPK:Z

    .line 92
    :cond_11b
    const/4 v10, 0x6

    aget-object v10, p0, v10

    if-eqz v10, :cond_12e

    const/4 v10, 0x6

    aget-object v10, p0, v10

    const-string v11, "ART"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_12e

    const/4 v10, 0x1

    sput-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->ART:Z

    .line 93
    :cond_12e
    const/4 v10, 0x7

    aget-object v10, p0, v10

    if-eqz v10, :cond_138

    const/4 v10, 0x7

    aget-object v10, p0, v10

    sput-object v10, Lcom/chelpus/root/utils/odexrunpatch;->uid:Ljava/lang/String;

    .line 94
    :cond_138
    const/4 v10, 0x6

    aget-object v10, p0, v10

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z
    :try_end_13e
    .catch Ljava/lang/NullPointerException; {:try_start_90 .. :try_end_13e} :catch_1591
    .catch Ljava/lang/Exception; {:try_start_90 .. :try_end_13e} :catch_158e

    .line 98
    :goto_13e
    const/4 v10, 0x5

    :try_start_13f
    aget-object v10, p0, v10

    const-string v11, "copyDC"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_14c

    const/4 v10, 0x1

    sput-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->copyDC:Z
    :try_end_14c
    .catch Ljava/lang/NullPointerException; {:try_start_13f .. :try_end_14c} :catch_158b
    .catch Ljava/lang/Exception; {:try_start_13f .. :try_end_14c} :catch_1588

    .line 101
    :cond_14c
    :goto_14c
    sget-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->createAPK:Z

    if-eqz v10, :cond_157

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    sput-object v10, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->startUnderRoot:Ljava/lang/Boolean;

    .line 102
    :cond_157
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 103
    .local v3, "origStr":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 104
    .local v4, "replStr":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 105
    .local v5, "trigger":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Boolean;>;"
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 106
    .local v6, "ResultText":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 107
    .local v7, "markers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 109
    .local v8, "dontConvert":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Boolean;>;"
    const/16 v10, 0x1d

    new-array v0, v10, [B

    move-object/from16 v17, v0

    fill-array-data v17, :array_1594

    .line 110
    .local v17, "byteOrig":[B
    const/16 v10, 0x1d

    new-array v0, v10, [B

    move-object/from16 v50, v0

    fill-array-data v50, :array_15a8

    .line 111
    .local v50, "mask":[B
    const/16 v10, 0x1d

    new-array v0, v10, [B

    move-object/from16 v20, v0

    fill-array-data v20, :array_15bc

    .line 112
    .local v20, "byteReplace":[B
    const/16 v10, 0x1d

    new-array v0, v10, [B

    move-object/from16 v68, v0

    fill-array-data v68, :array_15d0

    .line 115
    .local v68, "rep_mask":[B
    const-string v10, "1A ?? FF FF"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 116
    const-string v10, "1A ?? ?? ??"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 117
    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 118
    const-string v10, "(pak intekekt 0)"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 119
    const-string v10, "search_pack"

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 121
    const-string v10, "1B ?? FF FF FF FF"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 122
    const-string v10, "1B ?? ?? ?? ?? ??"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 123
    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 124
    const-string v10, "(pak intekekt 0)"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 125
    const-string v10, "search_pack"

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 127
    const-string v10, "1A ?? FF FF"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 128
    const-string v10, "1A ?? ?? ??"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 129
    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 130
    const-string v10, "(sha intekekt 2)"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 131
    const-string v10, "search"

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 133
    const-string v10, "1B ?? FF FF FF FF"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 134
    const-string v10, "1B ?? ?? ?? ?? ??"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 135
    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 136
    const-string v10, "(sha intekekt 2 32 bit)"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 137
    const-string v10, "search"

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 139
    const-string v10, "0A ?? 39 ?? ?? 00"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 140
    const-string v10, "12 S1 39 ?? ?? 00"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 141
    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 142
    const-string v10, "lvl patch N2!\n(sha intekekt 3)"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 143
    const-string v10, "search"

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 147
    const-string v10, "6E 20 FF FF ?? 00 0A ??"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 148
    const-string v10, "6E 20 ?? ?? ?? 00 12 S1"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 149
    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 150
    const-string v10, "support2 Fixed!\n(sha intekekt 4)"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 151
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 155
    const-string v10, "70 ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 38 ?? ?? ?? 62 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0A ?? 44 00 01 00 2B 00 ?? ?? ?? ?? 62 ?? ?? ?? 11"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 156
    const-string v10, "70 ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 38 ?? ?? ?? 62 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0A ?? 12 10 00 00 2B 00 ?? ?? ?? ?? 62 ?? ?? ?? 11"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 157
    sget-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->pattern3:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 158
    const-string v10, "lvl patch N2!\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 159
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 161
    const-string v10, "12 ?? 12 ?? 71 ?? ?? ?? ?? ?? 0B ?? 52 ?? ?? ?? 39 ?? ?? ?? 53 ?? ?? ?? 31"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 162
    const-string v10, "12 S1 12 S1 71 ?? ?? ?? ?? ?? 0B ?? 52 ?? ?? ?? 39 ?? ?? ?? 53 ?? ?? ?? 31"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 163
    sget-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->pattern5:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 164
    const-string v10, "lvl patch N3!\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 165
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 174
    const-string v10, "0a ?? 38 ?? 0e 00 1a ?? ?? ?? 1A ?? ?? ?? 71 ?? ?? ?? ?? ?? 72"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 175
    const-string v10, "0a ?? 33 00 ?? ?? 1a ?? ?? ?? 1A ?? ?? ?? 71 ?? ?? ?? ?? ?? 72"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 176
    sget-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->pattern2:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 177
    const-string v10, "lvl patch N4!\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 178
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 180
    const-string v10, "1A ?? ?? ?? 70 ?? ?? ?? ?? ?? 27 ?? 22 ?? ?? ?? 70 ?? ?? ?? ?? ?? 12 ?? 46 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0A ?? ?? ?? ?? ?? 12"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 181
    const-string v10, "1A ?? ?? ?? 70 ?? ?? ?? ?? ?? 27 ?? 22 ?? ?? ?? 70 ?? ?? ?? ?? ?? 12 ?? 46 ?? ?? ?? 71 ?? ?? ?? ?? ?? 12 ?? ?? ?? ?? ?? 12"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 182
    sget-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->pattern4:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 183
    const-string v10, "lvl patch N5!\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 184
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 192
    const-string v10, "22 ?? ?? ?? 70 ?? ?? ?? ?? ?? 5B ?? ?? ?? 12 ?? 46 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0A ?? ?? ?? ?? ?? 12 ??"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 193
    const-string v10, "22 ?? ?? ?? 70 ?? ?? ?? ?? ?? 5B ?? ?? ?? 12 ?? 46 ?? ?? ?? 71 ?? ?? ?? ?? ?? 12 ?? ?? ?? ?? ?? 12 ??"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 194
    sget-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->pattern4:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 195
    const-string v10, "lvl patch N5!\nparse response code"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 196
    const-string v10, "patch5"

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 198
    const-string v10, "12 ?? 12 ?? 71 ?? ?? ?? ?? ?? 0B ?? ?? ?? ?? ?? ?? ?? 31 ?? ?? ?? 3B ?? ?? ?? 01"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 199
    const-string v10, "12 S1 12 S1 71 ?? ?? ?? ?? ?? 0B ?? ?? ?? ?? ?? ?? ?? 31 ?? ?? ?? 3B ?? ?? ?? 01"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 200
    sget-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->pattern5:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 201
    const-string v10, "lvl patch N3!\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 202
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 204
    const-string v10, "2C ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 0A ?? 0F ?? 1A ?? ?? ?? ?? ?? ?? ?? ?? ?? 01 ?? 28 ?? 1A ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 0A ?? ?? ?? ?? ?? ?? ?? 0C ?? ?? ?? ?? ?? ?? ?? 0C ?? ?? ?? ?? ?? ?? ?? 01 ?? 28"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 205
    const-string v10, "2C ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 0A ?? 0F ?? 1A ?? ?? ?? ?? ?? ?? ?? ?? ?? 01 ?? 28 ?? 1A ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 12 ?? ?? ?? ?? ?? ?? ?? 0C ?? ?? ?? ?? ?? ?? ?? 0C ?? ?? ?? ?? ?? ?? ?? 01 ?? 28"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 206
    sget-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->pattern4:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 207
    const-string v10, "lvl patch N7!\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 208
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 210
    const-string v10, "2C ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 0A ?? 0F ?? 1A ?? ?? ?? ?? ?? ?? ?? ?? ?? 28 ?? 1A ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 0A ?? ?? ?? ?? ?? ?? ?? 0C ?? ?? ?? ?? ?? ?? ?? 0C ?? ?? ?? ?? ?? ?? ?? 01 ?? 28"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 211
    const-string v10, "2C ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 0A ?? 0F ?? 1A ?? ?? ?? ?? ?? ?? ?? ?? ?? 28 ?? 1A ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 12 ?? ?? ?? ?? ?? ?? ?? 0C ?? ?? ?? ?? ?? ?? ?? 0C ?? ?? ?? ?? ?? ?? ?? 01 ?? 28"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 212
    sget-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->pattern4:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 213
    const-string v10, "lvl patch N7!\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 214
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 216
    const-string v10, "2C ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 0A ?? 0F ?? 1A ?? ?? ?? ?? ?? ?? ?? ?? ?? 28 ?? 1A ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 0A ?? ?? ?? ?? ?? ?? ?? 0C ?? ?? ?? ?? ?? ?? ?? 0C ?? ?? ?? ?? ?? ?? ?? 28"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 217
    const-string v10, "2C ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 0A ?? 0F ?? 1A ?? ?? ?? ?? ?? ?? ?? ?? ?? 28 ?? 1A ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 12 ?? ?? ?? ?? ?? ?? ?? 0C ?? ?? ?? ?? ?? ?? ?? 0C ?? ?? ?? ?? ?? ?? ?? 28"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 218
    sget-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->pattern4:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 219
    const-string v10, "lvl patch N7!\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 220
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 222
    const-string v10, "2C ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 0A ?? 0F ?? 1A ?? ?? ?? ?? ?? ?? ?? ?? ?? 28 ?? 1A ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 0A"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 223
    const-string v10, "2C ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 0A ?? 0F ?? 1A ?? ?? ?? ?? ?? ?? ?? ?? ?? 28 ?? 1A ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 12"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 224
    sget-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->pattern4:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 225
    const-string v10, "lvl patch N7!\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 226
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 228
    const-string v10, "0C ?? 71 ?? ?? ?? ?? ?? 0C ?? 21 ?? 12 ?? 35 ?? ?? ?? 22 ?? ?? ?? 1A ?? ?? ?? 70 ?? ?? ?? ?? ?? 27 ?? 22 ?? ?? ?? 70 ?? ?? ?? ?? ?? 5B ?? ?? ?? 12 ?? 46 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0A"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 229
    const-string v10, "0C ?? 71 ?? ?? ?? ?? ?? 0C ?? 21 ?? 12 ?? 35 ?? ?? ?? 22 ?? ?? ?? 1A ?? ?? ?? 70 ?? ?? ?? ?? ?? 27 ?? 22 ?? ?? ?? 70 ?? ?? ?? ?? ?? 5B ?? ?? ?? 12 ?? 46 ?? ?? ?? 71 ?? ?? ?? ?? ?? 12"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 230
    sget-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->pattern4:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 231
    const-string v10, "lvl patch N5!\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 232
    const-string v10, "patch5"

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 235
    const/16 v10, 0x20

    new-array v0, v10, [B

    move-object/from16 v18, v0

    fill-array-data v18, :array_15e4

    .line 236
    .local v18, "byteOrig16":[B
    const/16 v10, 0x20

    new-array v0, v10, [B

    move-object/from16 v51, v0

    fill-array-data v51, :array_15f8

    .line 237
    .local v51, "mask16":[B
    const/16 v10, 0x20

    new-array v0, v10, [B

    move-object/from16 v21, v0

    fill-array-data v21, :array_160c

    .line 238
    .local v21, "byteReplace16":[B
    const/16 v10, 0x20

    new-array v0, v10, [B

    move-object/from16 v69, v0

    fill-array-data v69, :array_1620

    .line 240
    .local v69, "rep_mask16":[B
    const/16 v10, 0xd

    new-array v0, v10, [B

    move-object/from16 v19, v0

    fill-array-data v19, :array_1634

    .line 241
    .local v19, "byteOrig17":[B
    const/16 v10, 0xd

    new-array v0, v10, [B

    move-object/from16 v52, v0

    fill-array-data v52, :array_1640

    .line 242
    .local v52, "mask17":[B
    const/16 v10, 0xd

    new-array v0, v10, [B

    move-object/from16 v22, v0

    fill-array-data v22, :array_164c

    .line 243
    .local v22, "byteReplace17":[B
    const/16 v10, 0xd

    new-array v0, v10, [B

    move-object/from16 v70, v0

    fill-array-data v70, :array_1658

    .line 245
    .local v70, "rep_mask17":[B
    const-string v10, "2C ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 0A ?? 0F ?? 62 ?? ?? ?? ?? ?? ?? ?? ?? ?? 28 ?? 62 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 0A"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 246
    const-string v10, "2C ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 0A ?? 0F ?? 62 ?? ?? ?? ?? ?? ?? ?? ?? ?? 28 ?? 62 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 12"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 247
    sget-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->pattern4:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 248
    const-string v10, "lvl patch N7!\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 249
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 251
    const-string v10, "12 ?? 12 ?? 71 ?? ?? ?? ?? ?? 0B ?? ?? ?? ?? ?? ?? ?? ?? ?? 33 ?? ?? ?? 53"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 252
    const-string v10, "12 S1 12 S1 71 ?? ?? ?? ?? ?? 0B ?? ?? ?? ?? ?? ?? ?? ?? ?? 33 ?? ?? ?? 53"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 253
    sget-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->pattern5:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 254
    const-string v10, "lvl patch N3!\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 255
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 257
    const-string v10, "6C 61 63 6B 79 70 61 74 63 68"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 258
    const-string v10, "6C 75 63 75 79 70 75 74 63 68"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 259
    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 260
    const-string v10, ""

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 261
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 263
    const-string v10, "6C 75 63 6B 79 70 61 74 63 68"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 264
    const-string v10, "6C 75 63 75 79 70 75 74 75 68"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 265
    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 266
    const-string v10, ""

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 267
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 269
    const-string v10, "64 69 6D 6F 6E 76 69 64 65 6F 2E"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 270
    const-string v10, "64 69 6D 69 6E 69 69 64 65 6F 2E"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 271
    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 272
    const-string v10, ""

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 273
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 275
    const-string v10, "12 ?? 12 ?? 71 ?? ?? ?? ?? ?? 0B ?? 52 ?? ?? ?? 33 ?? ?? ?? 53 ?? ?? ?? 31"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 276
    const-string v10, "12 S1 12 S1 71 ?? ?? ?? ?? ?? 0B ?? 52 ?? ?? ?? 33 ?? ?? ?? 53 ?? ?? ?? 31"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 277
    sget-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->pattern5:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 278
    const-string v10, "lvl patch N3!\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 279
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 281
    const-string v10, "6E 10 ?? ?? ?? ?? 0A ?? 6E 10 ?? ?? ?? ?? 0C ?? 6E 10 ?? ?? ?? ?? 0C ?? 6E 40"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 282
    const-string v10, "6E 10 ?? ?? ?? ?? 12 ?? 6E 10 ?? ?? ?? ?? 0C ?? 6E 10 ?? ?? ?? ?? 0C ?? 6E 40"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 283
    sget-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->pattern4:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 284
    const-string v10, "lvl patch N7!\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 285
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 288
    const-string v10, "62 01 ?? ?? 6E 20 ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 0A 01 71 10 ?? ?? ?? ?? 0C 02 71 10 ?? ?? ?? ?? 6E 10 ?? ?? ?? ?? 0C 02 6E 10 ?? ?? ?? ?? 0C 03 6E 40 ?? ?? ?? ?? 28"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 289
    const-string v10, "62 01 ?? ?? 6E 20 ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 12 01 71 10 ?? ?? ?? ?? 0C 02 71 10 ?? ?? ?? ?? 6E 10 ?? ?? ?? ?? 0C 02 6E 10 ?? ?? ?? ?? 0C 03 6E 40 ?? ?? ?? ?? 28"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 290
    sget-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->pattern4:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 291
    const-string v10, "lvl patch N7!\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 292
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 303
    const-string v10, "12 ?? 12 ?? 71 ?? ?? ?? ?? ?? 0B ?? 52 ?? ?? ?? 39 ?? ?? ?? 53 ?? ?? ?? 31"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 304
    const-string v10, "12 S0 12 S0 71 ?? ?? ?? ?? ?? 0B ?? 52 ?? ?? ?? 39 ?? ?? ?? 53 ?? ?? ?? 31"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 305
    sget-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->pattern6:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 306
    const-string v10, "lvl patch N3!\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 307
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 309
    const-string v10, "12 ?? 12 ?? 71 ?? ?? ?? ?? ?? 0B ?? 52 ?? ?? ?? 33 ?? ?? ?? 53 ?? ?? ?? 31"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 310
    const-string v10, "12 S0 12 S0 71 ?? ?? ?? ?? ?? 0B ?? 52 ?? ?? ?? 33 ?? ?? ?? 53 ?? ?? ?? 31"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 311
    sget-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->pattern6:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 312
    const-string v10, "lvl patch N3!\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 313
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 315
    const-string v10, "12 ?? 12 ?? 71 ?? ?? ?? ?? ?? 0B ?? ?? ?? ?? ?? ?? ?? ?? ?? 33 ?? ?? ?? 53"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 316
    const-string v10, "12 S0 12 S0 71 ?? ?? ?? ?? ?? 0B ?? ?? ?? ?? ?? ?? ?? ?? ?? 33 ?? ?? ?? 53"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 317
    sget-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->pattern6:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 318
    const-string v10, "lvl patch N3!\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 319
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 321
    const-string v10, "12 ?? 12 ?? 71 ?? ?? ?? ?? ?? 0B ?? ?? ?? ?? ?? ?? ?? 31 ?? ?? ?? 3B ?? ?? ?? 01"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 322
    const-string v10, "12 S0 12 S0 71 ?? ?? ?? ?? ?? 0B ?? ?? ?? ?? ?? ?? ?? 31 ?? ?? ?? 3B ?? ?? ?? 01"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 323
    sget-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->pattern6:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 324
    const-string v10, "lvl patch N3!\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 325
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 327
    const-string v10, "13 00 00 01 33 R0 ?? ?? 54 ?? ?? ?? 71 10 ?? ?? ?? ?? 0C 01"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 328
    const-string v10, "13 W0 00 01 33 00 00 01 54 ?? ?? ?? 71 10 ?? ?? ?? ?? 0C 01"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 329
    sget-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->pattern4:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 330
    const-string v10, "lvl patch N3!\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 331
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 333
    const-string v10, "1A 05 ?? ?? 63 00 ?? ?? 38 00 09 00 62 00 ?? ?? 1A 01 ?? ?? 6E 20 ?? ?? ?? ?? 54 60 ?? ?? 1A 01 ?? ?? 6E 20 ?? ?? ?? ?? ?? ?? 38 00 0A 00 62 00"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 334
    const-string v10, "1A 05 ?? ?? 63 00 ?? ?? 38 00 09 00 62 00 ?? ?? 1A 01 ?? ?? 6E 20 ?? ?? ?? ?? 54 60 ?? ?? 1A 01 ?? ?? 6E 20 ?? ?? ?? ?? ?? ?? 33 00 ?? ?? 62 00"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 335
    sget-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->amazon:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 336
    const-string v10, "amazon patch N1!\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 337
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 339
    const-string v10, "6E 20 ?? ?? ?? ?? 0C 00 38 00 05 00 12 10 ?? ?? 0F 00 12 00 ?? ?? 0D 00 ?? ?? 27 00"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 340
    const-string v10, "6E 20 ?? ?? ?? ?? 0C 00 33 00 ?? ?? 12 10 ?? ?? 0F 00 12 00 ?? ?? 0D 00 ?? ?? 27 00"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 341
    sget-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->amazon:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 342
    const-string v10, "amazon patch N1!\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 343
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 346
    const-string v10, "13 ?? 09 00 6E ?? ?? ?? ?? ?? 0C ?? 71 ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0A ?? 32 ?? ?? ?? 59 ?? ?? ?? 22 ?? ?? ?? 70 ?? ?? ?? ?? ?? 27"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 347
    const-string v10, "13 ?? 09 00 6E ?? ?? ?? ?? ?? 0C ?? 71 ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0A ?? 32 00 ?? ?? 59 ?? ?? ?? 22 ?? ?? ?? 70 ?? ?? ?? ?? ?? 27"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 348
    sget-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->samsung:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 349
    const-string v10, "samsung patch N1!\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 350
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 352
    const-string v10, "13 ?? 09 00 13 ?? 0B 00 6E ?? ?? ?? ?? ?? 0C ?? 71 ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0A ?? 32 ?? ?? ?? 59 ?? ?? ?? 22 ?? ?? ?? 12 ?? 70 ?? ?? ?? ?? ?? 27"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 353
    const-string v10, "13 ?? 09 00 13 ?? 0B 00 6E ?? ?? ?? ?? ?? 0C ?? 71 ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0A ?? 32 00 ?? ?? 59 ?? ?? ?? 22 ?? ?? ?? 12 ?? 70 ?? ?? ?? ?? ?? 27"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 354
    sget-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->samsung:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 355
    const-string v10, "samsung patch N1!\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 356
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 358
    const-string v10, "54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 71 ?? ?? ?? ?? ?? 0A ?? 0F ?? 00 00"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 359
    const-string v10, "54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 71 ?? ?? ?? ?? ?? 12 S1 0F ?? 00 00"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 360
    sget-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->samsung:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 361
    const-string v10, "samsung patch N2!\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 362
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 364
    const-string v10, "54 ?? ?? ?? 54 ?? ?? ?? 54 ?? ?? ?? 54 ?? ?? ?? 54 ?? ?? ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0A ?? 0F ?? 00 00"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 365
    const-string v10, "54 ?? ?? ?? 54 ?? ?? ?? 54 ?? ?? ?? 54 ?? ?? ?? 54 ?? ?? ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 12 S1 0F ?? 00 00"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 366
    sget-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->samsung:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 367
    const-string v10, "samsung patch N2!\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 368
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 370
    const-string v10, "54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0C ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 71 ?? ?? ?? ?? ?? 0A ?? 0F ?? 00 00"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 371
    const-string v10, "54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0C ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 71 ?? ?? ?? ?? ?? 12 S1 0F ?? 00 00"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 372
    sget-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->samsung:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 373
    const-string v10, "samsung patch N3!\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 374
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 376
    const-string v10, "54 ?? ?? ?? 54 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 54 ?? ?? ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0A ?? 38 ?? ?? ??"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 377
    const-string v10, "54 ?? ?? ?? 54 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 54 ?? ?? ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0A ?? 33 00 ?? ??"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 378
    sget-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->samsung:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 379
    const-string v10, "samsung patch N3!\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 380
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 382
    const-string v10, "13 ?? 32 00 33 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0A ?? 38 ?? ?? ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 12"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 383
    const-string v10, "13 ?? 32 00 32 00 ?? ?? 6E ?? ?? ?? ?? ?? 0A ?? 38 ?? ?? ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 12"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 384
    sget-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->samsung:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 385
    const-string v10, "samsung patch N4!\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 386
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 388
    const-string v10, "13 ?? 32 00 33 ?? ?? ?? 70 ?? ?? ?? ?? ?? 0A ?? 38 ?? ?? ?? 54 ?? ?? ?? 59 ?? ?? ?? ?? ?? 28"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 389
    const-string v10, "13 ?? 32 00 32 00 ?? ?? 70 ?? ?? ?? ?? ?? 0A ?? 38 ?? ?? ?? 54 ?? ?? ?? 59 ?? ?? ?? ?? ?? 28"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 390
    sget-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->samsung:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 391
    const-string v10, "samsung patch N4!\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 392
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 403
    const-string v10, "38 ?? 06 00 32 ?? 04 00 33 ?? ?? ?? 1A ?? ?? ?? 71"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 404
    const-string v10, "12 ?? 00 00 32 00 04 00 33 ?? ?? ?? ?? ?? ?? ?? ??"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 405
    sget-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->pattern4:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 406
    const-string v10, "lvl patch N6!\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 407
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 408
    const/4 v10, 0x0

    aget-object v10, p0, v10

    const-string v11, "com.buak.Link2SD"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_7f5

    .line 410
    const-string v10, "00 05 2E 6F 64 65 78 00"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 411
    const-string v10, "00 05 2E 6F 64 65 79 00"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 412
    sget-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->pattern4:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 413
    const-string v10, "lvl patch N7!\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 414
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 417
    :cond_7f5
    const-string v10, "00 04 6F 64 65 78 00"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 418
    const-string v10, "00 04 6F 64 65 79 00"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 419
    sget-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->pattern4:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 420
    const-string v10, "lvl patch N7!\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 421
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 423
    const-string v10, "2F 4C 75 63 6B 79 50 61 74 63 68 65 72"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 424
    const-string v10, "2F 4C 75 63 6B 79 50 79 74 63 68 65 72"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 425
    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 426
    const-string v10, ""

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 427
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 429
    sget-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->ART:Z

    if-nez v10, :cond_93a

    .line 431
    const-string v10, "13 ?? 09 00 12 ?? 12 ?? 6E ?? ?? ?? ?? ?? 0C ??"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 432
    const-string v10, "12 00 0F 00 12 ?? 12 ?? 6E ?? ?? ?? ?? ?? 0C ??"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 433
    sget-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->dependencies:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 434
    const-string v10, "com.android.vending dependencies removed\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 435
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 436
    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 439
    const-string v10, "12 ?? 12 ?? 13 ?? 09 00 6E ?? ?? ?? ?? ?? 0C ??"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 440
    const-string v10, "12 ?? 12 ?? 12 00 0F 00 6E ?? ?? ?? ?? ?? 0C ??"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 441
    sget-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->dependencies:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 442
    const-string v10, "com.android.vending dependencies removed\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 443
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 444
    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 472
    :goto_88c
    const/16 v27, 0x0

    .line 474
    .local v27, "conv":Z
    invoke-static/range {v27 .. v27}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-static/range {v2 .. v9}, Lcom/chelpus/Utils;->convertToPatchItemAuto(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/Boolean;)V

    .line 477
    :try_start_895
    sget-object v10, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->startUnderRoot:Ljava/lang/Boolean;

    invoke-virtual {v10}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    if-eqz v10, :cond_8a5

    const/4 v10, 0x2

    aget-object v10, p0, v10

    const-string v11, "RW"

    invoke-static {v10, v11}, Lcom/chelpus/Utils;->remount(Ljava/lang/String;Ljava/lang/String;)Z

    .line 478
    :cond_8a5
    sget-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->createAPK:Z

    if-nez v10, :cond_adc

    sget-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->ART:Z

    if-nez v10, :cond_adc

    .line 479
    const/4 v10, 0x3

    aget-object v10, p0, v10

    sput-object v10, Lcom/chelpus/root/utils/odexrunpatch;->dir:Ljava/lang/String;

    .line 480
    const/4 v10, 0x2

    aget-object v10, p0, v10

    sput-object v10, Lcom/chelpus/root/utils/odexrunpatch;->dirapp:Ljava/lang/String;

    .line 481
    invoke-static {}, Lcom/chelpus/root/utils/odexrunpatch;->clearTemp()V

    .line 482
    const/4 v10, 0x4

    aget-object v10, p0, v10

    const-string v11, "not_system"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_8c8

    const/4 v10, 0x0

    sput-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->system:Z

    .line 483
    :cond_8c8
    const/4 v10, 0x4

    aget-object v10, p0, v10

    const-string v11, "system"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_8d6

    const/4 v10, 0x1

    sput-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->system:Z

    .line 484
    :cond_8d6
    sget-object v10, Lcom/chelpus/root/utils/odexrunpatch;->filestopatch:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->clear()V

    .line 485
    const-string v10, "CLASSES mode create odex enabled."

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 486
    const/4 v10, 0x0

    aget-object v59, p0, v10

    .line 487
    .local v59, "packageName":Ljava/lang/String;
    const/4 v10, 0x2

    aget-object v10, p0, v10

    sput-object v10, Lcom/chelpus/root/utils/odexrunpatch;->appdir:Ljava/lang/String;

    .line 488
    const/4 v10, 0x3

    aget-object v10, p0, v10

    sput-object v10, Lcom/chelpus/root/utils/odexrunpatch;->sddir:Ljava/lang/String;

    .line 489
    invoke-static {}, Lcom/chelpus/root/utils/odexrunpatch;->clearTempSD()V

    .line 490
    new-instance v15, Ljava/io/File;

    sget-object v10, Lcom/chelpus/root/utils/odexrunpatch;->appdir:Ljava/lang/String;

    invoke-direct {v15, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 491
    .local v15, "apk":Ljava/io/File;
    const-string v10, "Get classes.dex."

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 492
    sget-object v10, Lcom/chelpus/root/utils/odexrunpatch;->print:Ljava/io/PrintStream;

    const-string v11, "Get classes.dex."

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 493
    invoke-static {v15}, Lcom/chelpus/root/utils/odexrunpatch;->unzipART(Ljava/io/File;)V

    .line 494
    sget-object v10, Lcom/chelpus/root/utils/odexrunpatch;->classesFiles:Ljava/util/ArrayList;

    if-eqz v10, :cond_912

    sget-object v10, Lcom/chelpus/root/utils/odexrunpatch;->classesFiles:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-nez v10, :cond_986

    :cond_912
    new-instance v10, Ljava/io/FileNotFoundException;

    invoke-direct {v10}, Ljava/io/FileNotFoundException;-><init>()V

    throw v10
    :try_end_918
    .catch Ljava/io/FileNotFoundException; {:try_start_895 .. :try_end_918} :catch_918
    .catch Ljava/lang/Exception; {:try_start_895 .. :try_end_918} :catch_9a9

    .line 1008
    .end local v15    # "apk":Ljava/io/File;
    .end local v59    # "packageName":Ljava/lang/String;
    :catch_918
    move-exception v47

    .line 1009
    .local v47, "localFileNotFoundException":Ljava/io/FileNotFoundException;
    const-string v10, "Error: Program files are not found!\n\nCheck the location dalvik-cache to solve problems!\n\nDefault: /data/dalvik-cache/*"

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 1010
    invoke-virtual/range {v47 .. v47}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 1018
    .end local v47    # "localFileNotFoundException":Ljava/io/FileNotFoundException;
    :cond_921
    :goto_921
    sget-object v10, Lcom/chelpus/root/utils/odexrunpatch;->filestopatch:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_927
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_1529

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v41

    check-cast v41, Ljava/io/File;

    .line 1019
    .local v41, "filepatch":Ljava/io/File;
    invoke-static/range {v41 .. v41}, Lcom/chelpus/Utils;->fixadler(Ljava/io/File;)V

    .line 1020
    invoke-static {}, Lcom/chelpus/root/utils/odexrunpatch;->clearTempSD()V

    goto :goto_927

    .line 447
    .end local v27    # "conv":Z
    .end local v41    # "filepatch":Ljava/io/File;
    :cond_93a
    const-string v10, "13 ?? 09 00 12 ?? 12 ?? 6E ?? ?? ?? ?? ?? 0C ??"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 448
    const-string v10, "12 S0 00 00 12 S0 12 S0 6E ?? ?? ?? ?? ?? 0C ??"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 449
    sget-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->dependencies:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 450
    const-string v10, "com.android.vending dependencies removed\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 451
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 452
    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 455
    const-string v10, "12 ?? 12 ?? 13 ?? 09 00 6E ?? ?? ?? ?? ?? 0C ??"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 456
    const-string v10, "12 S0 12 S0 12 S0 00 00 6E ?? ?? ?? ?? ?? 0C ??"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 457
    sget-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->dependencies:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 458
    const-string v10, "com.android.vending dependencies removed\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 459
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 460
    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_88c

    .line 496
    .restart local v15    # "apk":Ljava/io/File;
    .restart local v27    # "conv":Z
    .restart local v59    # "packageName":Ljava/lang/String;
    :cond_986
    :try_start_986
    sget-object v10, Lcom/chelpus/root/utils/odexrunpatch;->filestopatch:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->clear()V

    .line 497
    sget-object v10, Lcom/chelpus/root/utils/odexrunpatch;->classesFiles:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_991
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_9d4

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Ljava/io/File;

    .line 498
    .local v25, "cl":Ljava/io/File;
    invoke-virtual/range {v25 .. v25}, Ljava/io/File;->exists()Z

    move-result v11

    if-nez v11, :cond_9cc

    new-instance v10, Ljava/io/FileNotFoundException;

    invoke-direct {v10}, Ljava/io/FileNotFoundException;-><init>()V

    throw v10
    :try_end_9a9
    .catch Ljava/io/FileNotFoundException; {:try_start_986 .. :try_end_9a9} :catch_918
    .catch Ljava/lang/Exception; {:try_start_986 .. :try_end_9a9} :catch_9a9

    .line 1012
    .end local v15    # "apk":Ljava/io/File;
    .end local v25    # "cl":Ljava/io/File;
    .end local v59    # "packageName":Ljava/lang/String;
    :catch_9a9
    move-exception v34

    .line 1013
    .restart local v34    # "e":Ljava/lang/Exception;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Exception e"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual/range {v34 .. v34}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 1014
    invoke-virtual/range {v34 .. v34}, Ljava/lang/Exception;->printStackTrace()V

    .line 1015
    invoke-virtual/range {v34 .. v34}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_921

    .line 499
    .end local v34    # "e":Ljava/lang/Exception;
    .restart local v15    # "apk":Ljava/io/File;
    .restart local v25    # "cl":Ljava/io/File;
    .restart local v59    # "packageName":Ljava/lang/String;
    :cond_9cc
    :try_start_9cc
    sget-object v11, Lcom/chelpus/root/utils/odexrunpatch;->filestopatch:Ljava/util/ArrayList;

    move-object/from16 v0, v25

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_991

    .line 503
    .end local v25    # "cl":Ljava/io/File;
    :cond_9d4
    const/4 v10, 0x2

    aget-object v10, p0, v10

    const/4 v11, 0x1

    invoke-static {v10, v11}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v56

    .line 504
    .local v56, "odexstr":Ljava/lang/String;
    new-instance v55, Ljava/io/File;

    invoke-direct/range {v55 .. v56}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 505
    .local v55, "odexfile":Ljava/io/File;
    invoke-virtual/range {v55 .. v55}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_9ea

    invoke-virtual/range {v55 .. v55}, Ljava/io/File;->delete()Z

    .line 506
    :cond_9ea
    new-instance v55, Ljava/io/File;

    .end local v55    # "odexfile":Ljava/io/File;
    const-string v10, "-1"

    const-string v11, "-2"

    move-object/from16 v0, v56

    invoke-virtual {v0, v10, v11}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, v55

    invoke-direct {v0, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 507
    .restart local v55    # "odexfile":Ljava/io/File;
    invoke-virtual/range {v55 .. v55}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_a04

    invoke-virtual/range {v55 .. v55}, Ljava/io/File;->delete()Z

    .line 508
    :cond_a04
    new-instance v55, Ljava/io/File;

    .end local v55    # "odexfile":Ljava/io/File;
    const-string v10, "-2"

    const-string v11, "-1"

    move-object/from16 v0, v56

    invoke-virtual {v0, v10, v11}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, v55

    invoke-direct {v0, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 509
    .restart local v55    # "odexfile":Ljava/io/File;
    invoke-virtual/range {v55 .. v55}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_a1e

    invoke-virtual/range {v55 .. v55}, Ljava/io/File;->delete()Z

    .line 555
    .end local v15    # "apk":Ljava/io/File;
    .end local v55    # "odexfile":Ljava/io/File;
    .end local v56    # "odexstr":Ljava/lang/String;
    .end local v59    # "packageName":Ljava/lang/String;
    :cond_a1e
    sget-object v10, Lcom/chelpus/root/utils/odexrunpatch;->filestopatch:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v79

    :goto_a24
    invoke-interface/range {v79 .. v79}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_921

    invoke-interface/range {v79 .. v79}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v40

    check-cast v40, Ljava/io/File;

    .line 556
    .local v40, "file_patch":Ljava/io/File;
    const-string v10, "Find string id."

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 559
    new-instance v75, Ljava/util/ArrayList;

    invoke-direct/range {v75 .. v75}, Ljava/util/ArrayList;-><init>()V

    .line 560
    .local v75, "strings":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v10, "com.android.vending"

    move-object/from16 v0, v75

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 561
    const-string v10, "SHA1withRSA"

    move-object/from16 v0, v75

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 562
    const-string v10, "com.android.vending.billing.InAppBillingService.BIND"

    move-object/from16 v0, v75

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 563
    const-string v10, "Ljava/security/Signature;"

    move-object/from16 v0, v75

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 564
    const-string v10, "verify"

    move-object/from16 v0, v75

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 567
    const-string v10, "String analysis."

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 568
    sget-object v10, Lcom/chelpus/root/utils/odexrunpatch;->print:Ljava/io/PrintStream;

    const-string v11, "String analysis."

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 569
    invoke-virtual/range {v40 .. v40}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    move-object/from16 v0, v75

    invoke-static {v10, v0, v11}, Lcom/chelpus/Utils;->getStringIds(Ljava/lang/String;Ljava/util/ArrayList;Z)Ljava/util/ArrayList;

    move-result-object v58

    .line 571
    .local v58, "offsets":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;>;"
    const/16 v35, 0x0

    .local v35, "f1":Z
    const/16 v36, 0x0

    .local v36, "f2":Z
    const/16 v37, 0x0

    .line 572
    .local v37, "f3":Z
    const/16 v57, 0x1

    .line 573
    .local v57, "of_to_patch":I
    new-instance v26, Ljava/util/ArrayList;

    invoke-direct/range {v26 .. v26}, Ljava/util/ArrayList;-><init>()V

    .line 574
    .local v26, "commands":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;>;"
    new-instance v10, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;

    const-string v11, "Ljava/security/Signature;"

    const-string v12, "verify"

    invoke-direct {v10, v11, v12}, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v26

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 575
    invoke-virtual/range {v58 .. v58}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_a93
    :goto_a93
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_cc1

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v45

    check-cast v45, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;

    .line 577
    .local v45, "it":Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;
    invoke-virtual/range {v26 .. v26}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_aa3
    :goto_aa3
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_bbf

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v46

    check-cast v46, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;

    .line 578
    .local v46, "item":Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;
    move-object/from16 v0, v46

    iget-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;->object:Ljava/lang/String;

    move-object/from16 v0, v45

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->str:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_ac5

    .line 579
    move-object/from16 v0, v45

    iget-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    move-object/from16 v0, v46

    iput-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;->Object:[B

    .line 581
    :cond_ac5
    move-object/from16 v0, v46

    iget-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;->method:Ljava/lang/String;

    move-object/from16 v0, v45

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->str:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_aa3

    .line 582
    move-object/from16 v0, v45

    iget-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    move-object/from16 v0, v46

    iput-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;->Method:[B

    goto :goto_aa3

    .line 512
    .end local v26    # "commands":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;>;"
    .end local v35    # "f1":Z
    .end local v36    # "f2":Z
    .end local v37    # "f3":Z
    .end local v40    # "file_patch":Ljava/io/File;
    .end local v45    # "it":Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;
    .end local v46    # "item":Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;
    .end local v57    # "of_to_patch":I
    .end local v58    # "offsets":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;>;"
    .end local v75    # "strings":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_adc
    sget-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->createAPK:Z

    if-eqz v10, :cond_b64

    .line 513
    const/4 v10, 0x0

    aget-object v59, p0, v10

    .line 514
    .restart local v59    # "packageName":Ljava/lang/String;
    const/4 v10, 0x2

    aget-object v10, p0, v10

    sput-object v10, Lcom/chelpus/root/utils/odexrunpatch;->appdir:Ljava/lang/String;

    .line 515
    const/4 v10, 0x5

    aget-object v10, p0, v10

    sput-object v10, Lcom/chelpus/root/utils/odexrunpatch;->sddir:Ljava/lang/String;

    .line 517
    invoke-static {}, Lcom/chelpus/root/utils/odexrunpatch;->clearTempSD()V

    .line 518
    new-instance v15, Ljava/io/File;

    sget-object v10, Lcom/chelpus/root/utils/odexrunpatch;->appdir:Ljava/lang/String;

    invoke-direct {v15, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 519
    .restart local v15    # "apk":Ljava/io/File;
    invoke-static {v15}, Lcom/chelpus/root/utils/odexrunpatch;->unzipSD(Ljava/io/File;)V

    .line 520
    new-instance v10, Ljava/io/File;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v12, Lcom/chelpus/root/utils/odexrunpatch;->sddir:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/Modified/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, v59

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ".apk"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v10, Lcom/chelpus/root/utils/odexrunpatch;->crkapk:Ljava/io/File;

    .line 521
    sget-object v10, Lcom/chelpus/root/utils/odexrunpatch;->crkapk:Ljava/io/File;

    invoke-static {v15, v10}, Lcom/chelpus/Utils;->copyFile(Ljava/io/File;Ljava/io/File;)V

    .line 522
    sget-object v10, Lcom/chelpus/root/utils/odexrunpatch;->classesFiles:Ljava/util/ArrayList;

    if-eqz v10, :cond_b33

    sget-object v10, Lcom/chelpus/root/utils/odexrunpatch;->classesFiles:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-nez v10, :cond_b39

    .line 523
    :cond_b33
    new-instance v10, Ljava/io/FileNotFoundException;

    invoke-direct {v10}, Ljava/io/FileNotFoundException;-><init>()V

    throw v10

    .line 525
    :cond_b39
    sget-object v10, Lcom/chelpus/root/utils/odexrunpatch;->filestopatch:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->clear()V

    .line 526
    sget-object v10, Lcom/chelpus/root/utils/odexrunpatch;->classesFiles:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_b44
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_b64

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Ljava/io/File;

    .line 527
    .restart local v25    # "cl":Ljava/io/File;
    invoke-virtual/range {v25 .. v25}, Ljava/io/File;->exists()Z

    move-result v11

    if-nez v11, :cond_b5c

    new-instance v10, Ljava/io/FileNotFoundException;

    invoke-direct {v10}, Ljava/io/FileNotFoundException;-><init>()V

    throw v10

    .line 528
    :cond_b5c
    sget-object v11, Lcom/chelpus/root/utils/odexrunpatch;->filestopatch:Ljava/util/ArrayList;

    move-object/from16 v0, v25

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_b44

    .line 532
    .end local v15    # "apk":Ljava/io/File;
    .end local v25    # "cl":Ljava/io/File;
    .end local v59    # "packageName":Ljava/lang/String;
    :cond_b64
    sget-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->ART:Z

    if-eqz v10, :cond_a1e

    .line 533
    const/4 v10, 0x0

    aget-object v59, p0, v10

    .line 534
    .restart local v59    # "packageName":Ljava/lang/String;
    const/4 v10, 0x2

    aget-object v10, p0, v10

    sput-object v10, Lcom/chelpus/root/utils/odexrunpatch;->appdir:Ljava/lang/String;

    .line 535
    const/4 v10, 0x3

    aget-object v10, p0, v10

    sput-object v10, Lcom/chelpus/root/utils/odexrunpatch;->sddir:Ljava/lang/String;

    .line 537
    invoke-static {}, Lcom/chelpus/root/utils/odexrunpatch;->clearTempSD()V

    .line 538
    new-instance v15, Ljava/io/File;

    sget-object v10, Lcom/chelpus/root/utils/odexrunpatch;->appdir:Ljava/lang/String;

    invoke-direct {v15, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 539
    .restart local v15    # "apk":Ljava/io/File;
    invoke-static {v15}, Lcom/chelpus/root/utils/odexrunpatch;->unzipART(Ljava/io/File;)V

    .line 542
    sget-object v10, Lcom/chelpus/root/utils/odexrunpatch;->classesFiles:Ljava/util/ArrayList;

    if-eqz v10, :cond_b8e

    sget-object v10, Lcom/chelpus/root/utils/odexrunpatch;->classesFiles:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-nez v10, :cond_b94

    .line 543
    :cond_b8e
    new-instance v10, Ljava/io/FileNotFoundException;

    invoke-direct {v10}, Ljava/io/FileNotFoundException;-><init>()V

    throw v10

    .line 545
    :cond_b94
    sget-object v10, Lcom/chelpus/root/utils/odexrunpatch;->filestopatch:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->clear()V

    .line 546
    sget-object v10, Lcom/chelpus/root/utils/odexrunpatch;->classesFiles:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_b9f
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_a1e

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Ljava/io/File;

    .line 547
    .restart local v25    # "cl":Ljava/io/File;
    invoke-virtual/range {v25 .. v25}, Ljava/io/File;->exists()Z

    move-result v11

    if-nez v11, :cond_bb7

    new-instance v10, Ljava/io/FileNotFoundException;

    invoke-direct {v10}, Ljava/io/FileNotFoundException;-><init>()V

    throw v10

    .line 548
    :cond_bb7
    sget-object v11, Lcom/chelpus/root/utils/odexrunpatch;->filestopatch:Ljava/util/ArrayList;

    move-object/from16 v0, v25

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_b9f

    .line 586
    .end local v15    # "apk":Ljava/io/File;
    .end local v25    # "cl":Ljava/io/File;
    .end local v59    # "packageName":Ljava/lang/String;
    .restart local v26    # "commands":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;>;"
    .restart local v35    # "f1":Z
    .restart local v36    # "f2":Z
    .restart local v37    # "f3":Z
    .restart local v40    # "file_patch":Ljava/io/File;
    .restart local v45    # "it":Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;
    .restart local v57    # "of_to_patch":I
    .restart local v58    # "offsets":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;>;"
    .restart local v75    # "strings":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_bbf
    move-object/from16 v0, v45

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->str:Ljava/lang/String;

    const-string v12, "com.android.vending.billing.InAppBillingService.BIND"

    invoke-virtual {v10, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_c3f

    .line 588
    const/4 v10, 0x0

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x2

    move-object/from16 v0, v45

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x0

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 589
    const/4 v10, 0x0

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x3

    move-object/from16 v0, v45

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x1

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 593
    const/4 v10, 0x1

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x2

    move-object/from16 v0, v45

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x0

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 594
    const/4 v10, 0x1

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x3

    move-object/from16 v0, v45

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x1

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 595
    const/4 v10, 0x1

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x4

    move-object/from16 v0, v45

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x2

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 596
    const/4 v10, 0x1

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x5

    move-object/from16 v0, v45

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x3

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 598
    const/16 v36, 0x0

    .line 600
    :cond_c3f
    move-object/from16 v0, v45

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->str:Ljava/lang/String;

    const-string v12, "SHA1withRSA"

    invoke-virtual {v10, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_a93

    .line 602
    const/4 v10, 0x2

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x2

    move-object/from16 v0, v45

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x0

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 603
    const/4 v10, 0x2

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x3

    move-object/from16 v0, v45

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x1

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 607
    const/4 v10, 0x3

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x2

    move-object/from16 v0, v45

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x0

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 608
    const/4 v10, 0x3

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x3

    move-object/from16 v0, v45

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x1

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 609
    const/4 v10, 0x3

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x4

    move-object/from16 v0, v45

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x2

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 610
    const/4 v10, 0x3

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x5

    move-object/from16 v0, v45

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x3

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 612
    const/16 v37, 0x1

    goto/16 :goto_a93

    .line 615
    .end local v45    # "it":Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;
    :cond_cc1
    const/16 v61, 0x0

    .local v61, "patch_index1":I
    const/16 v62, 0x1

    .line 616
    .local v62, "patch_index2":I
    if-eqz v35, :cond_cc9

    if-nez v36, :cond_cdd

    .line 617
    :cond_cc9
    const/4 v10, 0x0

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    const/4 v11, 0x0

    iput-boolean v11, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->pattern:Z

    .line 618
    const/4 v10, 0x1

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    const/4 v11, 0x0

    iput-boolean v11, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->pattern:Z

    .line 620
    :cond_cdd
    if-nez v37, :cond_cfd

    .line 621
    const/4 v10, 0x2

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    const/4 v11, 0x0

    iput-boolean v11, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->pattern:Z

    .line 622
    const/4 v10, 0x3

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    const/4 v11, 0x0

    iput-boolean v11, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->pattern:Z

    .line 623
    const/4 v10, 0x4

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    const/4 v11, 0x0

    iput-boolean v11, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->pattern:Z

    .line 625
    :cond_cfd
    const-string v10, "Parse data for patch."

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 626
    sget-object v10, Lcom/chelpus/root/utils/odexrunpatch;->print:Ljava/io/PrintStream;

    const-string v11, "Parse data for patch."

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 627
    invoke-virtual/range {v40 .. v40}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    move-object/from16 v0, v26

    invoke-static {v10, v0, v11}, Lcom/chelpus/Utils;->getMethodsIds(Ljava/lang/String;Ljava/util/ArrayList;Z)Z

    .line 628
    sget-object v11, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const/4 v10, 0x0

    move-object/from16 v0, v26

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;->index_command:[B

    const/4 v12, 0x0

    aget-byte v10, v10, v12

    invoke-static {v10}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v11, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 629
    sget-object v11, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const/4 v10, 0x0

    move-object/from16 v0, v26

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;->index_command:[B

    const/4 v12, 0x1

    aget-byte v10, v10, v12

    invoke-static {v10}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v11, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 630
    invoke-virtual/range {v26 .. v26}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_d45
    :goto_d45
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_d97

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v46

    check-cast v46, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;

    .line 631
    .restart local v46    # "item":Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;
    move-object/from16 v0, v46

    iget-boolean v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;->found_index_command:Z

    if-eqz v10, :cond_d45

    .line 632
    move-object/from16 v0, v46

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;->object:Ljava/lang/String;

    const-string v12, "Ljava/security/Signature;"

    invoke-virtual {v10, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_d45

    .line 633
    const/16 v10, 0xb

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x2

    move-object/from16 v0, v46

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;->index_command:[B

    const/4 v14, 0x0

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 634
    const/16 v10, 0xb

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x3

    move-object/from16 v0, v46

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;->index_command:[B

    const/4 v14, 0x1

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 635
    const/16 v10, 0xb

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    const/4 v12, 0x1

    iput-boolean v12, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->pattern:Z

    goto :goto_d45

    .line 644
    .end local v46    # "item":Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;
    :cond_d97
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v10

    new-array v0, v10, [Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    move-object/from16 v60, v0

    .line 645
    .local v60, "patchList":[Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    const/16 v78, 0x0

    .line 646
    .local v78, "u":I
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_da5
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_db6

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v46

    check-cast v46, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    .line 647
    .local v46, "item":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    aput-object v46, v60, v78

    .line 648
    add-int/lit8 v78, v78, 0x1

    .line 649
    goto :goto_da5

    .line 652
    .end local v46    # "item":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    :cond_db6
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v76

    .line 653
    .local v76, "time":J
    new-instance v10, Ljava/io/RandomAccessFile;

    const-string v11, "rw"

    move-object/from16 v0, v40

    invoke-direct {v10, v0, v11}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v9

    .line 654
    .local v9, "ChannelDex":Ljava/nio/channels/FileChannel;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Size file:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v9}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v11

    invoke-virtual {v10, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 655
    sget-object v10, Ljava/nio/channels/FileChannel$MapMode;->READ_WRITE:Ljava/nio/channels/FileChannel$MapMode;

    const-wide/16 v11, 0x0

    invoke-virtual {v9}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v13

    long-to-int v13, v13

    int-to-long v13, v13

    invoke-virtual/range {v9 .. v14}, Ljava/nio/channels/FileChannel;->map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;
    :try_end_dee
    .catch Ljava/io/FileNotFoundException; {:try_start_9cc .. :try_end_dee} :catch_918
    .catch Ljava/lang/Exception; {:try_start_9cc .. :try_end_dee} :catch_9a9

    move-result-object v39

    .line 656
    .local v39, "fileBytes":Ljava/nio/MappedByteBuffer;
    const/16 v48, 0x0

    .line 657
    .local v48, "mark5":Z
    const/16 v49, 0x0

    .line 658
    .local v49, "mark6":Z
    const/16 v32, 0x5a

    .local v32, "diaposon":I
    const/16 v33, 0x28

    .line 659
    .local v33, "diaposon_pak":I
    const/16 v73, 0x0

    .local v73, "start_for_diaposon":I
    const/16 v74, 0x0

    .line 661
    .local v74, "start_for_diaposon_pak":I
    const/16 v29, -0x1

    .local v29, "curentPos":I
    const/16 v30, 0x0

    .line 662
    .local v30, "curentPos2":I
    const/16 v72, 0x0

    .local v72, "repbyte_no_license":B
    const/16 v71, 0x0

    .line 664
    .local v71, "repbyte_license":B
    const/16 v64, 0x0

    .line 665
    .local v64, "period":I
    :goto_e05
    :try_start_e05
    invoke-virtual/range {v39 .. v39}, Ljava/nio/MappedByteBuffer;->hasRemaining()Z

    move-result v10

    if-eqz v10, :cond_10c2

    .line 666
    sget-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->createAPK:Z

    if-nez v10, :cond_e38

    .line 667
    invoke-virtual/range {v39 .. v39}, Ljava/nio/MappedByteBuffer;->position()I

    move-result v10

    sub-int v10, v10, v64

    const v11, 0x249ef

    if-le v10, v11, :cond_e38

    .line 668
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Progress size:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual/range {v39 .. v39}, Ljava/nio/MappedByteBuffer;->position()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 669
    invoke-virtual/range {v39 .. v39}, Ljava/nio/MappedByteBuffer;->position()I

    move-result v64

    .line 672
    :cond_e38
    invoke-virtual/range {v39 .. v39}, Ljava/nio/MappedByteBuffer;->position()I

    move-result v29

    .line 673
    invoke-virtual/range {v39 .. v39}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v28

    .line 674
    .local v28, "curentByte":B
    if-nez v48, :cond_e44

    if-eqz v49, :cond_e46

    :cond_e44
    add-int/lit8 v30, v30, 0x1

    .line 675
    :cond_e46
    const/16 v10, 0x17c

    move/from16 v0, v30

    if-le v0, v10, :cond_e50

    .line 676
    const/16 v48, 0x0

    .line 677
    const/16 v30, 0x0

    .line 681
    :cond_e50
    const/4 v10, 0x0

    aget-byte v10, v17, v10

    move/from16 v0, v28

    if-ne v0, v10, :cond_eeb

    sget-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->pattern1:Z

    if-eqz v10, :cond_eeb

    .line 682
    const/4 v10, 0x0

    aget-byte v10, v68, v10

    if-nez v10, :cond_e63

    const/4 v10, 0x0

    aput-byte v28, v20, v10

    .line 683
    :cond_e63
    const/16 v43, 0x1

    .line 684
    .local v43, "i":I
    const/16 v24, 0x3e8

    .line 685
    .local v24, "c_no_license":I
    const/16 v23, 0x3e8

    .line 686
    .local v23, "c_license":I
    add-int/lit8 v10, v29, 0x1

    move-object/from16 v0, v39

    invoke-virtual {v0, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 687
    invoke-virtual/range {v39 .. v39}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v66

    .line 688
    .local v66, "prufbyte":B
    const/16 v72, 0x0

    .line 689
    const/16 v71, 0x0

    .line 690
    :goto_e78
    aget-byte v10, v17, v43

    move/from16 v0, v66

    if-eq v0, v10, :cond_e82

    aget-byte v10, v50, v43

    if-eqz v10, :cond_eeb

    .line 691
    :cond_e82
    aget-byte v10, v68, v43

    const/4 v11, 0x2

    if-ne v10, v11, :cond_e9b

    invoke-virtual/range {v39 .. v39}, Ljava/nio/MappedByteBuffer;->position()I

    move-result v10

    add-int/lit8 v10, v10, 0x7

    move-object/from16 v0, v39

    invoke-virtual {v0, v10}, Ljava/nio/MappedByteBuffer;->get(I)B

    move-result v10

    move/from16 v0, v66

    if-ne v0, v10, :cond_e9b

    .line 692
    move/from16 v23, v43

    .line 693
    move/from16 v72, v66

    .line 695
    :cond_e9b
    aget-byte v10, v68, v43

    if-eqz v10, :cond_ea4

    aget-byte v10, v68, v43

    const/4 v11, 0x2

    if-ne v10, v11, :cond_ea6

    :cond_ea4
    aput-byte v66, v20, v43

    .line 696
    :cond_ea6
    aget-byte v10, v68, v43

    const/4 v11, 0x3

    if-ne v10, v11, :cond_eaf

    .line 697
    move/from16 v71, v66

    .line 698
    move/from16 v24, v43

    .line 700
    :cond_eaf
    add-int/lit8 v43, v43, 0x1

    .line 701
    move-object/from16 v0, v17

    array-length v10, v0

    move/from16 v0, v43

    if-ne v0, v10, :cond_1049

    .line 702
    const/16 v10, 0x3e8

    move/from16 v0, v24

    if-ge v0, v10, :cond_eeb

    if-eqz v71, :cond_eeb

    .line 703
    aput-byte v72, v20, v24

    .line 704
    add-int/lit8 v10, v23, 0x7

    const/16 v11, 0x3e8

    if-ge v10, v11, :cond_eeb

    if-eqz v72, :cond_eeb

    .line 705
    add-int/lit8 v10, v23, 0x8

    aput-byte v71, v20, v10

    .line 706
    move-object/from16 v0, v39

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 707
    move-object/from16 v0, v39

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 708
    invoke-virtual/range {v39 .. v39}, Ljava/nio/MappedByteBuffer;->force()Ljava/nio/MappedByteBuffer;

    .line 710
    const-string v10, "lvl patch N1!\n"

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 711
    sget-object v10, Lcom/chelpus/root/utils/odexrunpatch;->print:Ljava/io/PrintStream;

    const-string v11, "lvl patch N1!\n"

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 721
    .end local v23    # "c_license":I
    .end local v24    # "c_no_license":I
    .end local v43    # "i":I
    .end local v66    # "prufbyte":B
    :cond_eeb
    new-instance v53, Ljava/util/ArrayList;

    invoke-direct/range {v53 .. v53}, Ljava/util/ArrayList;-><init>()V

    .line 722
    .local v53, "move_byte":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;"
    new-instance v54, Ljava/util/ArrayList;

    invoke-direct/range {v54 .. v54}, Ljava/util/ArrayList;-><init>()V

    .line 723
    .local v54, "move_byte_position":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/16 v44, 0x0

    .line 724
    .local v44, "increase":Z
    const/16 v16, 0x0

    .local v16, "b":I
    :goto_ef9
    move-object/from16 v0, v60

    array-length v10, v0

    move/from16 v0, v16

    if-ge v0, v10, :cond_1483

    .line 725
    aget-object v63, v60, v16

    .line 726
    .local v63, "patches":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    move-object/from16 v0, v39

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 728
    move-object/from16 v0, v63

    iget-boolean v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->markerTrig:Z

    if-eqz v10, :cond_10fb

    const/4 v10, 0x5

    move/from16 v0, v16

    if-eq v0, v10, :cond_f30

    const/4 v10, 0x6

    move/from16 v0, v16

    if-eq v0, v10, :cond_f30

    const/4 v10, 0x7

    move/from16 v0, v16

    if-eq v0, v10, :cond_f30

    const/16 v10, 0x8

    move/from16 v0, v16

    if-eq v0, v10, :cond_f30

    const/16 v10, 0x9

    move/from16 v0, v16

    if-eq v0, v10, :cond_f30

    const/16 v10, 0xa

    move/from16 v0, v16

    if-ne v0, v10, :cond_10fb

    .line 730
    :cond_f30
    if-nez v44, :cond_f36

    .line 731
    add-int/lit8 v74, v74, 0x1

    .line 732
    const/16 v44, 0x1

    .line 734
    :cond_f36
    move/from16 v0, v74

    move/from16 v1, v33

    if-ge v0, v1, :cond_1221

    .line 736
    move-object/from16 v0, v63

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v11, 0x0

    aget-byte v10, v10, v11

    move/from16 v0, v28

    if-ne v0, v10, :cond_10f4

    .line 737
    invoke-virtual/range {v53 .. v53}, Ljava/util/ArrayList;->clear()V

    .line 738
    invoke-virtual/range {v54 .. v54}, Ljava/util/ArrayList;->clear()V

    .line 739
    move-object/from16 v0, v63

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repMask:[I

    const/4 v11, 0x0

    aget v10, v10, v11

    if-nez v10, :cond_f5d

    .line 740
    move-object/from16 v0, v63

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    const/4 v11, 0x0

    aput-byte v28, v10, v11

    .line 741
    :cond_f5d
    const/16 v43, 0x1

    .line 742
    .restart local v43    # "i":I
    add-int/lit8 v10, v29, 0x1

    move-object/from16 v0, v39

    invoke-virtual {v0, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 743
    invoke-virtual/range {v39 .. v39}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v66

    .line 745
    .restart local v66    # "prufbyte":B
    :goto_f6a
    move-object/from16 v0, v63

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    aget-byte v10, v10, v43

    move/from16 v0, v66

    if-eq v0, v10, :cond_f9b

    move-object/from16 v0, v63

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origMask:[I

    aget v10, v10, v43

    const/4 v11, 0x1

    if-eq v10, v11, :cond_f9b

    move-object/from16 v0, v63

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origMask:[I

    aget v10, v10, v43

    const/16 v11, 0x14

    if-eq v10, v11, :cond_f9b

    move-object/from16 v0, v63

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origMask:[I

    aget v10, v10, v43

    const/16 v11, 0x15

    if-eq v10, v11, :cond_f9b

    move-object/from16 v0, v63

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origMask:[I

    aget v10, v10, v43

    const/16 v11, 0x17

    if-ne v10, v11, :cond_10f4

    .line 747
    :cond_f9b
    move-object/from16 v0, v63

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repMask:[I

    aget v10, v10, v43

    if-nez v10, :cond_fa9

    .line 748
    move-object/from16 v0, v63

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    aput-byte v66, v10, v43

    .line 749
    :cond_fa9
    move-object/from16 v0, v63

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repMask:[I

    aget v10, v10, v43

    const/16 v11, 0x14

    if-ne v10, v11, :cond_fbc

    .line 750
    move-object/from16 v0, v63

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    and-int/lit8 v11, v66, 0xf

    int-to-byte v11, v11

    aput-byte v11, v10, v43

    .line 753
    :cond_fbc
    move-object/from16 v0, v63

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repMask:[I

    aget v10, v10, v43

    const/16 v11, 0x15

    if-ne v10, v11, :cond_fd1

    .line 754
    move-object/from16 v0, v63

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    and-int/lit8 v11, v66, 0xf

    add-int/lit8 v11, v11, 0x10

    int-to-byte v11, v11

    aput-byte v11, v10, v43

    .line 757
    :cond_fd1
    move-object/from16 v0, v63

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origMask:[I

    aget v10, v10, v43

    const/16 v11, 0x17

    if-ne v10, v11, :cond_fea

    .line 758
    move-object/from16 v0, v63

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    aget-byte v10, v10, v43

    invoke-static/range {v66 .. v66}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v11

    move-object/from16 v0, v53

    invoke-virtual {v0, v10, v11}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 761
    :cond_fea
    move-object/from16 v0, v63

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repMask:[I

    aget v10, v10, v43

    const/16 v11, 0x17

    if-ne v10, v11, :cond_1003

    .line 762
    move-object/from16 v0, v63

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    aget-byte v10, v10, v43

    invoke-static/range {v43 .. v43}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    move-object/from16 v0, v54

    invoke-virtual {v0, v10, v11}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 765
    :cond_1003
    add-int/lit8 v43, v43, 0x1

    .line 766
    move-object/from16 v0, v63

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    array-length v10, v10

    move/from16 v0, v43

    if-ne v0, v10, :cond_121b

    .line 767
    invoke-virtual/range {v53 .. v53}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-gtz v10, :cond_101a

    invoke-virtual/range {v54 .. v54}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-lez v10, :cond_1055

    .line 768
    :cond_101a
    const/16 v67, 0x0

    .local v67, "r":I
    :goto_101c
    invoke-virtual/range {v53 .. v53}, Ljava/util/ArrayList;->size()I

    move-result v10

    move/from16 v0, v67

    if-ge v0, v10, :cond_104f

    .line 769
    move-object/from16 v0, v63

    iget-object v11, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    move-object/from16 v0, v54

    move/from16 v1, v67

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v12

    move-object/from16 v0, v53

    move/from16 v1, v67

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Byte;

    invoke-virtual {v10}, Ljava/lang/Byte;->byteValue()B

    move-result v10

    aput-byte v10, v11, v12

    .line 768
    add-int/lit8 v67, v67, 0x1

    goto :goto_101c

    .line 716
    .end local v16    # "b":I
    .end local v44    # "increase":Z
    .end local v53    # "move_byte":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;"
    .end local v54    # "move_byte_position":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v63    # "patches":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    .end local v67    # "r":I
    .restart local v23    # "c_license":I
    .restart local v24    # "c_no_license":I
    :cond_1049
    invoke-virtual/range {v39 .. v39}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v66

    goto/16 :goto_e78

    .line 772
    .end local v23    # "c_license":I
    .end local v24    # "c_no_license":I
    .restart local v16    # "b":I
    .restart local v44    # "increase":Z
    .restart local v53    # "move_byte":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;"
    .restart local v54    # "move_byte_position":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .restart local v63    # "patches":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    .restart local v67    # "r":I
    :cond_104f
    invoke-virtual/range {v53 .. v53}, Ljava/util/ArrayList;->clear()V

    .line 773
    invoke-virtual/range {v54 .. v54}, Ljava/util/ArrayList;->clear()V

    .line 775
    .end local v67    # "r":I
    :cond_1055
    move-object/from16 v0, v39

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 776
    move-object/from16 v0, v63

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    move-object/from16 v0, v39

    invoke-virtual {v0, v10}, Ljava/nio/MappedByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 777
    invoke-virtual/range {v39 .. v39}, Ljava/nio/MappedByteBuffer;->force()Ljava/nio/MappedByteBuffer;

    .line 778
    move-object/from16 v0, v63

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->resultText:Ljava/lang/String;

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 779
    sget-object v10, Lcom/chelpus/root/utils/odexrunpatch;->print:Ljava/io/PrintStream;

    move-object/from16 v0, v63

    iget-object v11, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->resultText:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 780
    const/4 v10, 0x1

    move-object/from16 v0, v63

    iput-boolean v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->result:Z

    .line 781
    const/4 v10, 0x0

    move-object/from16 v0, v63

    iput-boolean v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->markerTrig:Z

    .line 782
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_1086
    :goto_1086
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_10eb

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    .line 783
    .local v31, "curpatch":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    move-object/from16 v0, v31

    iget-object v11, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->marker:Ljava/lang/String;

    move-object/from16 v0, v63

    iget-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->marker:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1086

    .line 784
    const/4 v11, 0x0

    move-object/from16 v0, v31

    iput-boolean v11, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->markerTrig:Z
    :try_end_10a5
    .catch Ljava/lang/Exception; {:try_start_e05 .. :try_end_10a5} :catch_10a6
    .catch Ljava/io/FileNotFoundException; {:try_start_e05 .. :try_end_10a5} :catch_918

    goto :goto_1086

    .line 997
    .end local v16    # "b":I
    .end local v28    # "curentByte":B
    .end local v31    # "curpatch":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    .end local v43    # "i":I
    .end local v44    # "increase":Z
    .end local v53    # "move_byte":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;"
    .end local v54    # "move_byte_position":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v63    # "patches":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    .end local v66    # "prufbyte":B
    :catch_10a6
    move-exception v34

    .line 998
    .restart local v34    # "e":Ljava/lang/Exception;
    :try_start_10a7
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, ""

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, v34

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 999
    invoke-virtual/range {v34 .. v34}, Ljava/lang/Exception;->printStackTrace()V

    .line 1002
    .end local v34    # "e":Ljava/lang/Exception;
    :cond_10c2
    invoke-virtual {v9}, Ljava/nio/channels/FileChannel;->close()V

    .line 1003
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, ""

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v11

    sub-long v11, v11, v76

    const-wide/16 v13, 0x3e8

    div-long/2addr v11, v13

    invoke-virtual {v10, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 1004
    const-string v10, "Analise Results:"

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z
    :try_end_10e9
    .catch Ljava/io/FileNotFoundException; {:try_start_10a7 .. :try_end_10e9} :catch_918
    .catch Ljava/lang/Exception; {:try_start_10a7 .. :try_end_10e9} :catch_9a9

    goto/16 :goto_a24

    .line 786
    .restart local v16    # "b":I
    .restart local v28    # "curentByte":B
    .restart local v43    # "i":I
    .restart local v44    # "increase":Z
    .restart local v53    # "move_byte":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;"
    .restart local v54    # "move_byte_position":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .restart local v63    # "patches":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    .restart local v66    # "prufbyte":B
    :cond_10eb
    const/16 v74, 0x0

    .line 787
    add-int/lit8 v10, v29, 0x1

    :try_start_10ef
    move-object/from16 v0, v39

    invoke-virtual {v0, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 794
    .end local v43    # "i":I
    .end local v66    # "prufbyte":B
    :cond_10f4
    add-int/lit8 v10, v29, 0x1

    move-object/from16 v0, v39

    invoke-virtual {v0, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 806
    :cond_10fb
    :goto_10fb
    move-object/from16 v0, v63

    iget-boolean v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->markerTrig:Z

    if-eqz v10, :cond_12bc

    const/4 v10, 0x4

    move/from16 v0, v16

    if-ne v0, v10, :cond_12bc

    .line 808
    add-int/lit8 v73, v73, 0x1

    .line 809
    move/from16 v0, v73

    move/from16 v1, v32

    if-ge v0, v1, :cond_13db

    .line 811
    move-object/from16 v0, v63

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v11, 0x0

    aget-byte v10, v10, v11

    move/from16 v0, v28

    if-ne v0, v10, :cond_12b5

    .line 812
    invoke-virtual/range {v53 .. v53}, Ljava/util/ArrayList;->clear()V

    .line 813
    invoke-virtual/range {v54 .. v54}, Ljava/util/ArrayList;->clear()V

    .line 814
    move-object/from16 v0, v63

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repMask:[I

    const/4 v11, 0x0

    aget v10, v10, v11

    if-nez v10, :cond_112f

    .line 815
    move-object/from16 v0, v63

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    const/4 v11, 0x0

    aput-byte v28, v10, v11

    .line 816
    :cond_112f
    const/16 v43, 0x1

    .line 817
    .restart local v43    # "i":I
    add-int/lit8 v10, v29, 0x1

    move-object/from16 v0, v39

    invoke-virtual {v0, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 818
    invoke-virtual/range {v39 .. v39}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v66

    .line 820
    .restart local v66    # "prufbyte":B
    :goto_113c
    move-object/from16 v0, v63

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    aget-byte v10, v10, v43

    move/from16 v0, v66

    if-eq v0, v10, :cond_116d

    move-object/from16 v0, v63

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origMask:[I

    aget v10, v10, v43

    const/4 v11, 0x1

    if-eq v10, v11, :cond_116d

    move-object/from16 v0, v63

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origMask:[I

    aget v10, v10, v43

    const/16 v11, 0x14

    if-eq v10, v11, :cond_116d

    move-object/from16 v0, v63

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origMask:[I

    aget v10, v10, v43

    const/16 v11, 0x15

    if-eq v10, v11, :cond_116d

    move-object/from16 v0, v63

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origMask:[I

    aget v10, v10, v43

    const/16 v11, 0x17

    if-ne v10, v11, :cond_12b5

    .line 822
    :cond_116d
    move-object/from16 v0, v63

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repMask:[I

    aget v10, v10, v43

    if-nez v10, :cond_117b

    .line 823
    move-object/from16 v0, v63

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    aput-byte v66, v10, v43

    .line 824
    :cond_117b
    move-object/from16 v0, v63

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repMask:[I

    aget v10, v10, v43

    const/16 v11, 0x14

    if-ne v10, v11, :cond_118e

    .line 825
    move-object/from16 v0, v63

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    and-int/lit8 v11, v66, 0xf

    int-to-byte v11, v11

    aput-byte v11, v10, v43

    .line 828
    :cond_118e
    move-object/from16 v0, v63

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repMask:[I

    aget v10, v10, v43

    const/16 v11, 0x15

    if-ne v10, v11, :cond_11a3

    .line 829
    move-object/from16 v0, v63

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    and-int/lit8 v11, v66, 0xf

    add-int/lit8 v11, v11, 0x10

    int-to-byte v11, v11

    aput-byte v11, v10, v43

    .line 832
    :cond_11a3
    move-object/from16 v0, v63

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origMask:[I

    aget v10, v10, v43

    const/16 v11, 0x17

    if-ne v10, v11, :cond_11bc

    .line 833
    move-object/from16 v0, v63

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    aget-byte v10, v10, v43

    invoke-static/range {v66 .. v66}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v11

    move-object/from16 v0, v53

    invoke-virtual {v0, v10, v11}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 836
    :cond_11bc
    move-object/from16 v0, v63

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repMask:[I

    aget v10, v10, v43

    const/16 v11, 0x17

    if-ne v10, v11, :cond_11d5

    .line 837
    move-object/from16 v0, v63

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    aget-byte v10, v10, v43

    invoke-static/range {v43 .. v43}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    move-object/from16 v0, v54

    invoke-virtual {v0, v10, v11}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 840
    :cond_11d5
    add-int/lit8 v43, v43, 0x1

    .line 841
    move-object/from16 v0, v63

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    array-length v10, v10

    move/from16 v0, v43

    if-ne v0, v10, :cond_13d5

    .line 842
    invoke-virtual/range {v53 .. v53}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-gtz v10, :cond_11ec

    invoke-virtual/range {v54 .. v54}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-lez v10, :cond_125b

    .line 843
    :cond_11ec
    const/16 v67, 0x0

    .restart local v67    # "r":I
    :goto_11ee
    invoke-virtual/range {v53 .. v53}, Ljava/util/ArrayList;->size()I

    move-result v10

    move/from16 v0, v67

    if-ge v0, v10, :cond_1255

    .line 844
    move-object/from16 v0, v63

    iget-object v11, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    move-object/from16 v0, v54

    move/from16 v1, v67

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v12

    move-object/from16 v0, v53

    move/from16 v1, v67

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Byte;

    invoke-virtual {v10}, Ljava/lang/Byte;->byteValue()B

    move-result v10

    aput-byte v10, v11, v12

    .line 843
    add-int/lit8 v67, v67, 0x1

    goto :goto_11ee

    .line 791
    .end local v67    # "r":I
    :cond_121b
    invoke-virtual/range {v39 .. v39}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v66

    goto/16 :goto_f6a

    .line 796
    .end local v43    # "i":I
    .end local v66    # "prufbyte":B
    :cond_1221
    const/4 v10, 0x0

    move-object/from16 v0, v63

    iput-boolean v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->markerTrig:Z

    .line 797
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_122a
    :goto_122a
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_124a

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    .line 798
    .restart local v31    # "curpatch":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    move-object/from16 v0, v31

    iget-object v11, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->marker:Ljava/lang/String;

    move-object/from16 v0, v63

    iget-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->marker:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_122a

    .line 799
    const/4 v11, 0x0

    move-object/from16 v0, v31

    iput-boolean v11, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->markerTrig:Z

    goto :goto_122a

    .line 801
    .end local v31    # "curpatch":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    :cond_124a
    const/16 v74, 0x0

    .line 802
    add-int/lit8 v10, v29, 0x1

    move-object/from16 v0, v39

    invoke-virtual {v0, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    goto/16 :goto_10fb

    .line 847
    .restart local v43    # "i":I
    .restart local v66    # "prufbyte":B
    .restart local v67    # "r":I
    :cond_1255
    invoke-virtual/range {v53 .. v53}, Ljava/util/ArrayList;->clear()V

    .line 848
    invoke-virtual/range {v54 .. v54}, Ljava/util/ArrayList;->clear()V

    .line 850
    .end local v67    # "r":I
    :cond_125b
    move-object/from16 v0, v39

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 851
    move-object/from16 v0, v63

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    move-object/from16 v0, v39

    invoke-virtual {v0, v10}, Ljava/nio/MappedByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 852
    invoke-virtual/range {v39 .. v39}, Ljava/nio/MappedByteBuffer;->force()Ljava/nio/MappedByteBuffer;

    .line 853
    move-object/from16 v0, v63

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->resultText:Ljava/lang/String;

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 854
    sget-object v10, Lcom/chelpus/root/utils/odexrunpatch;->print:Ljava/io/PrintStream;

    move-object/from16 v0, v63

    iget-object v11, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->resultText:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 855
    const/4 v10, 0x1

    move-object/from16 v0, v63

    iput-boolean v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->result:Z

    .line 856
    const/4 v10, 0x0

    move-object/from16 v0, v63

    iput-boolean v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->markerTrig:Z

    .line 857
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_128c
    :goto_128c
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_12ac

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    .line 858
    .restart local v31    # "curpatch":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    move-object/from16 v0, v31

    iget-object v11, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->marker:Ljava/lang/String;

    move-object/from16 v0, v63

    iget-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->marker:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_128c

    .line 859
    const/4 v11, 0x0

    move-object/from16 v0, v31

    iput-boolean v11, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->markerTrig:Z

    goto :goto_128c

    .line 861
    .end local v31    # "curpatch":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    :cond_12ac
    const/16 v73, 0x0

    .line 862
    add-int/lit8 v10, v29, 0x1

    move-object/from16 v0, v39

    invoke-virtual {v0, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 869
    .end local v43    # "i":I
    .end local v66    # "prufbyte":B
    :cond_12b5
    add-int/lit8 v10, v29, 0x1

    move-object/from16 v0, v39

    invoke-virtual {v0, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 881
    :cond_12bc
    :goto_12bc
    move-object/from16 v0, v63

    iget-boolean v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->markerTrig:Z

    if-nez v10, :cond_147f

    .line 882
    move-object/from16 v0, v63

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v11, 0x0

    aget-byte v10, v10, v11

    move/from16 v0, v28

    if-ne v0, v10, :cond_147f

    move-object/from16 v0, v63

    iget-boolean v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->pattern:Z

    if-eqz v10, :cond_147f

    .line 883
    invoke-virtual/range {v53 .. v53}, Ljava/util/ArrayList;->clear()V

    .line 884
    invoke-virtual/range {v54 .. v54}, Ljava/util/ArrayList;->clear()V

    .line 885
    move-object/from16 v0, v63

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repMask:[I

    const/4 v11, 0x0

    aget v10, v10, v11

    if-nez v10, :cond_12e9

    move-object/from16 v0, v63

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    const/4 v11, 0x0

    aput-byte v28, v10, v11

    .line 886
    :cond_12e9
    const/16 v43, 0x1

    .line 887
    .restart local v43    # "i":I
    add-int/lit8 v10, v29, 0x1

    move-object/from16 v0, v39

    invoke-virtual {v0, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 888
    invoke-virtual/range {v39 .. v39}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v66

    .line 890
    .restart local v66    # "prufbyte":B
    :goto_12f6
    move-object/from16 v0, v63

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    aget-byte v10, v10, v43

    move/from16 v0, v66

    if-eq v0, v10, :cond_1327

    move-object/from16 v0, v63

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origMask:[I

    aget v10, v10, v43

    const/4 v11, 0x1

    if-eq v10, v11, :cond_1327

    move-object/from16 v0, v63

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origMask:[I

    aget v10, v10, v43

    const/16 v11, 0x14

    if-eq v10, v11, :cond_1327

    move-object/from16 v0, v63

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origMask:[I

    aget v10, v10, v43

    const/16 v11, 0x15

    if-eq v10, v11, :cond_1327

    move-object/from16 v0, v63

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origMask:[I

    aget v10, v10, v43

    const/16 v11, 0x17

    if-ne v10, v11, :cond_1478

    .line 892
    :cond_1327
    move-object/from16 v0, v63

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repMask:[I

    aget v10, v10, v43

    if-nez v10, :cond_1335

    move-object/from16 v0, v63

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    aput-byte v66, v10, v43

    .line 893
    :cond_1335
    move-object/from16 v0, v63

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repMask:[I

    aget v10, v10, v43

    const/16 v11, 0x14

    if-ne v10, v11, :cond_1348

    .line 894
    move-object/from16 v0, v63

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    and-int/lit8 v11, v66, 0xf

    int-to-byte v11, v11

    aput-byte v11, v10, v43

    .line 897
    :cond_1348
    move-object/from16 v0, v63

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repMask:[I

    aget v10, v10, v43

    const/16 v11, 0x15

    if-ne v10, v11, :cond_135d

    .line 898
    move-object/from16 v0, v63

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    and-int/lit8 v11, v66, 0xf

    add-int/lit8 v11, v11, 0x10

    int-to-byte v11, v11

    aput-byte v11, v10, v43

    .line 901
    :cond_135d
    move-object/from16 v0, v63

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origMask:[I

    aget v10, v10, v43

    const/16 v11, 0x17

    if-ne v10, v11, :cond_1376

    .line 902
    move-object/from16 v0, v63

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    aget-byte v10, v10, v43

    invoke-static/range {v66 .. v66}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v11

    move-object/from16 v0, v53

    invoke-virtual {v0, v10, v11}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 905
    :cond_1376
    move-object/from16 v0, v63

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repMask:[I

    aget v10, v10, v43

    const/16 v11, 0x17

    if-ne v10, v11, :cond_138f

    .line 906
    move-object/from16 v0, v63

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    aget-byte v10, v10, v43

    invoke-static/range {v43 .. v43}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    move-object/from16 v0, v54

    invoke-virtual {v0, v10, v11}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 909
    :cond_138f
    add-int/lit8 v43, v43, 0x1

    .line 910
    move-object/from16 v0, v63

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    array-length v10, v10

    move/from16 v0, v43

    if-ne v0, v10, :cond_1472

    .line 911
    invoke-virtual/range {v53 .. v53}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-gtz v10, :cond_13a6

    invoke-virtual/range {v54 .. v54}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-lez v10, :cond_1415

    .line 912
    :cond_13a6
    const/16 v67, 0x0

    .restart local v67    # "r":I
    :goto_13a8
    invoke-virtual/range {v53 .. v53}, Ljava/util/ArrayList;->size()I

    move-result v10

    move/from16 v0, v67

    if-ge v0, v10, :cond_140f

    .line 913
    move-object/from16 v0, v63

    iget-object v11, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    move-object/from16 v0, v54

    move/from16 v1, v67

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v12

    move-object/from16 v0, v53

    move/from16 v1, v67

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Byte;

    invoke-virtual {v10}, Ljava/lang/Byte;->byteValue()B

    move-result v10

    aput-byte v10, v11, v12

    .line 912
    add-int/lit8 v67, v67, 0x1

    goto :goto_13a8

    .line 866
    .end local v67    # "r":I
    :cond_13d5
    invoke-virtual/range {v39 .. v39}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v66

    goto/16 :goto_113c

    .line 871
    .end local v43    # "i":I
    .end local v66    # "prufbyte":B
    :cond_13db
    const/4 v10, 0x0

    move-object/from16 v0, v63

    iput-boolean v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->markerTrig:Z

    .line 872
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_13e4
    :goto_13e4
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_1404

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    .line 873
    .restart local v31    # "curpatch":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    move-object/from16 v0, v31

    iget-object v11, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->marker:Ljava/lang/String;

    move-object/from16 v0, v63

    iget-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->marker:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_13e4

    .line 874
    const/4 v11, 0x0

    move-object/from16 v0, v31

    iput-boolean v11, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->markerTrig:Z

    goto :goto_13e4

    .line 876
    .end local v31    # "curpatch":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    :cond_1404
    const/16 v73, 0x0

    .line 877
    add-int/lit8 v10, v29, 0x1

    move-object/from16 v0, v39

    invoke-virtual {v0, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    goto/16 :goto_12bc

    .line 916
    .restart local v43    # "i":I
    .restart local v66    # "prufbyte":B
    .restart local v67    # "r":I
    :cond_140f
    invoke-virtual/range {v53 .. v53}, Ljava/util/ArrayList;->clear()V

    .line 917
    invoke-virtual/range {v54 .. v54}, Ljava/util/ArrayList;->clear()V

    .line 919
    .end local v67    # "r":I
    :cond_1415
    move-object/from16 v0, v39

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 920
    move-object/from16 v0, v63

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    move-object/from16 v0, v39

    invoke-virtual {v0, v10}, Ljava/nio/MappedByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 921
    invoke-virtual/range {v39 .. v39}, Ljava/nio/MappedByteBuffer;->force()Ljava/nio/MappedByteBuffer;

    .line 922
    move-object/from16 v0, v63

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->resultText:Ljava/lang/String;

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 923
    sget-object v10, Lcom/chelpus/root/utils/odexrunpatch;->print:Ljava/io/PrintStream;

    move-object/from16 v0, v63

    iget-object v11, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->resultText:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 924
    const/4 v10, 0x1

    move-object/from16 v0, v63

    iput-boolean v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->result:Z

    .line 925
    move-object/from16 v0, v63

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->marker:Ljava/lang/String;

    const-string v11, ""

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_1478

    .line 926
    const/4 v10, 0x1

    move-object/from16 v0, v63

    iput-boolean v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->markerTrig:Z

    .line 927
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_1452
    :goto_1452
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_1478

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    .line 928
    .restart local v31    # "curpatch":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    move-object/from16 v0, v31

    iget-object v11, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->marker:Ljava/lang/String;

    move-object/from16 v0, v63

    iget-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->marker:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1452

    .line 929
    const/4 v11, 0x1

    move-object/from16 v0, v31

    iput-boolean v11, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->markerTrig:Z

    goto :goto_1452

    .line 935
    .end local v31    # "curpatch":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    :cond_1472
    invoke-virtual/range {v39 .. v39}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v66

    goto/16 :goto_12f6

    .line 937
    :cond_1478
    add-int/lit8 v10, v29, 0x1

    move-object/from16 v0, v39

    invoke-virtual {v0, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 724
    .end local v43    # "i":I
    .end local v66    # "prufbyte":B
    :cond_147f
    add-int/lit8 v16, v16, 0x1

    goto/16 :goto_ef9

    .line 944
    .end local v63    # "patches":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    :cond_1483
    const/4 v10, 0x0

    aget-byte v10, v18, v10

    move/from16 v0, v28

    if-ne v0, v10, :cond_14c3

    sget-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->pattern4:Z

    if-eqz v10, :cond_14c3

    .line 945
    const/4 v10, 0x0

    aget-byte v10, v69, v10

    if-nez v10, :cond_1496

    const/4 v10, 0x0

    aput-byte v28, v21, v10

    .line 946
    :cond_1496
    const/16 v43, 0x1

    .line 947
    .restart local v43    # "i":I
    add-int/lit8 v10, v29, 0x1

    move-object/from16 v0, v39

    invoke-virtual {v0, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 948
    invoke-virtual/range {v39 .. v39}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v66

    .line 950
    .restart local v66    # "prufbyte":B
    :goto_14a3
    aget-byte v10, v18, v43

    move/from16 v0, v66

    if-eq v0, v10, :cond_14ae

    aget-byte v10, v51, v43

    const/4 v11, 0x1

    if-ne v10, v11, :cond_14c3

    .line 951
    :cond_14ae
    aget-byte v10, v69, v43

    if-nez v10, :cond_14b4

    aput-byte v66, v21, v43

    .line 952
    :cond_14b4
    add-int/lit8 v43, v43, 0x1

    .line 953
    move-object/from16 v0, v18

    array-length v10, v0

    move/from16 v0, v43

    if-ne v0, v10, :cond_151f

    .line 955
    invoke-virtual/range {v39 .. v39}, Ljava/nio/MappedByteBuffer;->position()I

    move-result v29

    .line 961
    const/16 v48, 0x1

    .line 971
    .end local v43    # "i":I
    .end local v66    # "prufbyte":B
    :cond_14c3
    const/16 v10, 0x10

    move/from16 v0, v28

    if-ge v0, v10, :cond_1516

    if-eqz v48, :cond_1516

    sget-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->pattern4:Z

    if-eqz v10, :cond_1516

    .line 972
    const/4 v10, 0x0

    aget-byte v10, v70, v10

    if-nez v10, :cond_14d7

    const/4 v10, 0x0

    aput-byte v28, v22, v10

    .line 973
    :cond_14d7
    const/16 v43, 0x1

    .line 974
    .restart local v43    # "i":I
    add-int/lit8 v10, v29, 0x1

    move-object/from16 v0, v39

    invoke-virtual {v0, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 975
    invoke-virtual/range {v39 .. v39}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v66

    .line 977
    .restart local v66    # "prufbyte":B
    :goto_14e4
    aget-byte v10, v19, v43

    move/from16 v0, v66

    if-eq v0, v10, :cond_14ef

    aget-byte v10, v52, v43

    const/4 v11, 0x1

    if-ne v10, v11, :cond_1516

    .line 978
    :cond_14ef
    aget-byte v10, v70, v43

    if-nez v10, :cond_14f5

    aput-byte v66, v22, v43

    .line 979
    :cond_14f5
    add-int/lit8 v43, v43, 0x1

    .line 980
    move-object/from16 v0, v19

    array-length v10, v0

    move/from16 v0, v43

    if-ne v0, v10, :cond_1524

    .line 981
    move-object/from16 v0, v39

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 982
    move-object/from16 v0, v39

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 983
    invoke-virtual/range {v39 .. v39}, Ljava/nio/MappedByteBuffer;->force()Ljava/nio/MappedByteBuffer;

    .line 985
    const-string v10, "lvl patch N5!\n"

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 986
    const/16 v48, 0x0

    .line 993
    .end local v43    # "i":I
    .end local v66    # "prufbyte":B
    :cond_1516
    add-int/lit8 v10, v29, 0x1

    move-object/from16 v0, v39

    invoke-virtual {v0, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    goto/16 :goto_e05

    .line 965
    .restart local v43    # "i":I
    .restart local v66    # "prufbyte":B
    :cond_151f
    invoke-virtual/range {v39 .. v39}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v66

    goto :goto_14a3

    .line 989
    :cond_1524
    invoke-virtual/range {v39 .. v39}, Ljava/nio/MappedByteBuffer;->get()B
    :try_end_1527
    .catch Ljava/lang/Exception; {:try_start_10ef .. :try_end_1527} :catch_10a6
    .catch Ljava/io/FileNotFoundException; {:try_start_10ef .. :try_end_1527} :catch_918

    move-result v66

    goto :goto_14e4

    .line 1022
    .end local v9    # "ChannelDex":Ljava/nio/channels/FileChannel;
    .end local v16    # "b":I
    .end local v26    # "commands":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;>;"
    .end local v28    # "curentByte":B
    .end local v29    # "curentPos":I
    .end local v30    # "curentPos2":I
    .end local v32    # "diaposon":I
    .end local v33    # "diaposon_pak":I
    .end local v35    # "f1":Z
    .end local v36    # "f2":Z
    .end local v37    # "f3":Z
    .end local v39    # "fileBytes":Ljava/nio/MappedByteBuffer;
    .end local v40    # "file_patch":Ljava/io/File;
    .end local v43    # "i":I
    .end local v44    # "increase":Z
    .end local v48    # "mark5":Z
    .end local v49    # "mark6":Z
    .end local v53    # "move_byte":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;"
    .end local v54    # "move_byte_position":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v57    # "of_to_patch":I
    .end local v58    # "offsets":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;>;"
    .end local v60    # "patchList":[Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    .end local v61    # "patch_index1":I
    .end local v62    # "patch_index2":I
    .end local v64    # "period":I
    .end local v66    # "prufbyte":B
    .end local v71    # "repbyte_license":B
    .end local v72    # "repbyte_no_license":B
    .end local v73    # "start_for_diaposon":I
    .end local v74    # "start_for_diaposon_pak":I
    .end local v75    # "strings":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v76    # "time":J
    .end local v78    # "u":I
    :cond_1529
    sget-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->createAPK:Z

    if-nez v10, :cond_157a

    .line 1023
    const/4 v10, 0x3

    aget-object v10, p0, v10

    sget-object v11, Lcom/chelpus/root/utils/odexrunpatch;->classesFiles:Ljava/util/ArrayList;

    const/4 v12, 0x2

    aget-object v12, p0, v12

    sget-object v13, Lcom/chelpus/root/utils/odexrunpatch;->uid:Ljava/lang/String;

    const/4 v14, 0x2

    aget-object v14, p0, v14

    sget-object v79, Lcom/chelpus/root/utils/odexrunpatch;->uid:Ljava/lang/String;

    move-object/from16 v0, v79

    invoke-static {v14, v0}, Lcom/chelpus/Utils;->getOdexForCreate(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-static {v10, v11, v12, v13, v14}, Lcom/chelpus/Utils;->create_ODEX_root(Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v67

    .line 1024
    .restart local v67    # "r":I
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "chelpus_return_"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v67

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 1025
    if-nez v67, :cond_157a

    sget-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->ART:Z

    if-nez v10, :cond_157a

    .line 1026
    const/4 v10, 0x1

    aget-object v10, p0, v10

    const/4 v11, 0x2

    aget-object v11, p0, v11

    const/4 v12, 0x2

    aget-object v12, p0, v12

    const/4 v13, 0x1

    invoke-static {v12, v13}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v12

    sget-object v13, Lcom/chelpus/root/utils/odexrunpatch;->uid:Ljava/lang/String;

    const/4 v14, 0x3

    aget-object v14, p0, v14

    invoke-static {v10, v11, v12, v13, v14}, Lcom/chelpus/Utils;->afterPatch(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1030
    .end local v67    # "r":I
    :cond_157a
    sget-boolean v10, Lcom/chelpus/root/utils/odexrunpatch;->createAPK:Z

    if-nez v10, :cond_1581

    invoke-static {}, Lcom/chelpus/Utils;->exitFromRootJava()V

    .line 1031
    :cond_1581
    move-object/from16 v0, v65

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/LogOutputStream;->allresult:Ljava/lang/String;

    sput-object v10, Lcom/chelpus/root/utils/odexrunpatch;->result:Ljava/lang/String;

    .line 1033
    return-void

    .line 100
    .end local v3    # "origStr":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v4    # "replStr":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v5    # "trigger":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Boolean;>;"
    .end local v6    # "ResultText":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v7    # "markers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v8    # "dontConvert":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Boolean;>;"
    .end local v17    # "byteOrig":[B
    .end local v18    # "byteOrig16":[B
    .end local v19    # "byteOrig17":[B
    .end local v20    # "byteReplace":[B
    .end local v21    # "byteReplace16":[B
    .end local v22    # "byteReplace17":[B
    .end local v27    # "conv":Z
    .end local v50    # "mask":[B
    .end local v51    # "mask16":[B
    .end local v52    # "mask17":[B
    .end local v68    # "rep_mask":[B
    .end local v69    # "rep_mask16":[B
    .end local v70    # "rep_mask17":[B
    :catch_1588
    move-exception v10

    goto/16 :goto_14c

    .line 99
    :catch_158b
    move-exception v10

    goto/16 :goto_14c

    .line 96
    :catch_158e
    move-exception v10

    goto/16 :goto_13e

    .line 95
    :catch_1591
    move-exception v10

    goto/16 :goto_13e

    .line 109
    :array_1594
    .array-data 1
        0x5t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x2t
        0x1t
        0x0t
        0x0t
        0x3t
        0x1t
        0x0t
        0x0t
        0xft
        0x0t
        0x0t
        0x0t
        0x1at
        0x0t
        0x0t
        0x0t
        0xft
        0x0t
        0x0t
        0x0t
        0x59t
    .end array-data

    .line 110
    nop

    :array_15a8
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x1t
    .end array-data

    .line 111
    nop

    :array_15bc
    .array-data 1
        0x5t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x0t
        0x0t
        0x2t
        0x1t
        0x0t
        0x0t
        0x3t
        0x1t
        0x0t
        0x0t
        0xft
        0x0t
        0x0t
        0x0t
        0xft
        0x0t
        0x0t
        0x0t
        0xft
        0x0t
        0x0t
        0x0t
        0x59t
    .end array-data

    .line 112
    nop

    :array_15d0
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x2t
        0x0t
        0x0t
        0x0t
        0x3t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 235
    nop

    :array_15e4
    .array-data 1
        0x1at
        0x66t
        0x66t
        0x66t
        0x71t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xct
        0x66t
        0x71t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xct
        0x66t
        0x21t
        0x66t
        0x66t
        0x66t
        0x35t
        0x66t
        0x66t
        0x66t
        0x22t
        0x66t
        0x66t
        0x66t
    .end array-data

    .line 236
    :array_15f8
    .array-data 1
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
    .end array-data

    .line 237
    :array_160c
    .array-data 1
        0x1at
        0x66t
        0x66t
        0x66t
        0x71t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xct
        0x66t
        0x71t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xct
        0x66t
        0x21t
        0x66t
        0x66t
        0x66t
        0x35t
        0x66t
        0x66t
        0x66t
        0x22t
        0x66t
        0x66t
        0x66t
    .end array-data

    .line 238
    :array_1620
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 240
    :array_1634
    .array-data 1
        0x0t
        0x46t
        0x66t
        0x66t
        0x66t
        0x71t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0xat
        0x66t
    .end array-data

    .line 241
    nop

    :array_1640
    .array-data 1
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
    .end array-data

    .line 242
    nop

    :array_164c
    .array-data 1
        0x0t
        0x46t
        0x66t
        0x66t
        0x66t
        0x71t
        0x66t
        0x66t
        0x66t
        0x66t
        0x66t
        0x12t
        0x66t
    .end array-data

    .line 243
    nop

    :array_1658
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
    .end array-data
.end method

.method public static unzipART(Ljava/io/File;)V
    .registers 23
    .param p0, "apk"    # Ljava/io/File;

    .prologue
    .line 1137
    const/4 v7, 0x0

    .local v7, "found1":Z
    const/4 v8, 0x0

    .line 1139
    .local v8, "found2":Z
    :try_start_2
    new-instance v6, Ljava/io/FileInputStream;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 1140
    .local v6, "fin":Ljava/io/FileInputStream;
    new-instance v16, Ljava/util/zip/ZipInputStream;

    move-object/from16 v0, v16

    invoke-direct {v0, v6}, Ljava/util/zip/ZipInputStream;-><init>(Ljava/io/InputStream;)V

    .line 1141
    .local v16, "zin":Ljava/util/zip/ZipInputStream;
    const/4 v15, 0x0

    .line 1143
    .local v15, "ze":Ljava/util/zip/ZipEntry;
    invoke-virtual/range {v16 .. v16}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    move-result-object v15

    .line 1144
    const/4 v14, 0x1

    .line 1145
    .local v14, "search":Z
    :goto_16
    if-eqz v15, :cond_227

    if-eqz v14, :cond_227

    .line 1151
    invoke-virtual {v15}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v11

    .line 1153
    .local v11, "haystack":Ljava/lang/String;
    invoke-virtual {v11}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v18

    const-string v19, "classes"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_19f

    const-string v18, ".dex"

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_19f

    const-string v18, "/"

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_19f

    .line 1155
    new-instance v9, Ljava/io/FileOutputStream;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v19, Lcom/chelpus/root/utils/odexrunpatch;->sddir:Ljava/lang/String;

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "/"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-direct {v9, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 1157
    .local v9, "fout":Ljava/io/FileOutputStream;
    const/16 v18, 0x800

    move/from16 v0, v18

    new-array v2, v0, [B

    .line 1160
    .local v2, "buffer":[B
    :goto_66
    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/util/zip/ZipInputStream;->read([B)I

    move-result v12

    .local v12, "length":I
    const/16 v18, -0x1

    move/from16 v0, v18

    if-eq v12, v0, :cond_13e

    .line 1161
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v9, v2, v0, v12}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_79
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_79} :catch_7a

    goto :goto_66

    .line 1193
    .end local v2    # "buffer":[B
    .end local v6    # "fin":Ljava/io/FileInputStream;
    .end local v9    # "fout":Ljava/io/FileOutputStream;
    .end local v11    # "haystack":Ljava/lang/String;
    .end local v12    # "length":I
    .end local v14    # "search":Z
    .end local v15    # "ze":Ljava/util/zip/ZipEntry;
    .end local v16    # "zin":Ljava/util/zip/ZipInputStream;
    :catch_7a
    move-exception v4

    .line 1195
    .local v4, "e":Ljava/lang/Exception;
    :try_start_7b
    new-instance v17, Lnet/lingala/zip4j/core/ZipFile;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lnet/lingala/zip4j/core/ZipFile;-><init>(Ljava/io/File;)V

    .line 1199
    .local v17, "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    const-string v18, "classes.dex"

    sget-object v19, Lcom/chelpus/root/utils/odexrunpatch;->sddir:Ljava/lang/String;

    invoke-virtual/range {v17 .. v19}, Lnet/lingala/zip4j/core/ZipFile;->extractFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 1200
    sget-object v18, Lcom/chelpus/root/utils/odexrunpatch;->classesFiles:Ljava/util/ArrayList;

    new-instance v19, Ljava/io/File;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v21, Lcom/chelpus/root/utils/odexrunpatch;->sddir:Ljava/lang/String;

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "classes.dex"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v18 .. v19}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1201
    const/16 v18, 0x3

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const-string v20, "chmod"

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, "777"

    aput-object v20, v18, v19

    const/16 v19, 0x2

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v21, Lcom/chelpus/root/utils/odexrunpatch;->sddir:Ljava/lang/String;

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "classes.dex"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    aput-object v20, v18, v19

    invoke-static/range {v18 .. v18}, Lcom/chelpus/Utils;->cmdParam([Ljava/lang/String;)Ljava/lang/String;

    .line 1202
    const-string v18, "AndroidManifest.xml"

    sget-object v19, Lcom/chelpus/root/utils/odexrunpatch;->sddir:Ljava/lang/String;

    invoke-virtual/range {v17 .. v19}, Lnet/lingala/zip4j/core/ZipFile;->extractFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 1203
    const/16 v18, 0x3

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const-string v20, "chmod"

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, "777"

    aput-object v20, v18, v19

    const/16 v19, 0x2

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v21, Lcom/chelpus/root/utils/odexrunpatch;->sddir:Ljava/lang/String;

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "AndroidManifest.xml"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    aput-object v20, v18, v19

    invoke-static/range {v18 .. v18}, Lcom/chelpus/Utils;->cmdParam([Ljava/lang/String;)Ljava/lang/String;
    :try_end_123
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_7b .. :try_end_123} :catch_235
    .catch Ljava/lang/Exception; {:try_start_7b .. :try_end_123} :catch_26a

    .line 1212
    .end local v17    # "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    :goto_123
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Exception e"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v4}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 1216
    .end local v4    # "e":Ljava/lang/Exception;
    :goto_13d
    return-void

    .line 1165
    .restart local v2    # "buffer":[B
    .restart local v6    # "fin":Ljava/io/FileInputStream;
    .restart local v9    # "fout":Ljava/io/FileOutputStream;
    .restart local v11    # "haystack":Ljava/lang/String;
    .restart local v12    # "length":I
    .restart local v14    # "search":Z
    .restart local v15    # "ze":Ljava/util/zip/ZipEntry;
    .restart local v16    # "zin":Ljava/util/zip/ZipInputStream;
    :cond_13e
    :try_start_13e
    invoke-virtual/range {v16 .. v16}, Ljava/util/zip/ZipInputStream;->closeEntry()V

    .line 1166
    invoke-virtual {v9}, Ljava/io/FileOutputStream;->close()V

    .line 1167
    sget-object v18, Lcom/chelpus/root/utils/odexrunpatch;->classesFiles:Ljava/util/ArrayList;

    new-instance v19, Ljava/io/File;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v21, Lcom/chelpus/root/utils/odexrunpatch;->sddir:Ljava/lang/String;

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v18 .. v19}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1168
    const/16 v18, 0x3

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const-string v20, "chmod"

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, "777"

    aput-object v20, v18, v19

    const/16 v19, 0x2

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v21, Lcom/chelpus/root/utils/odexrunpatch;->sddir:Ljava/lang/String;

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    aput-object v20, v18, v19

    invoke-static/range {v18 .. v18}, Lcom/chelpus/Utils;->cmdParam([Ljava/lang/String;)Ljava/lang/String;

    .line 1171
    .end local v2    # "buffer":[B
    .end local v9    # "fout":Ljava/io/FileOutputStream;
    .end local v12    # "length":I
    :cond_19f
    const-string v18, "AndroidManifest.xml"

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_222

    .line 1172
    new-instance v10, Ljava/io/FileOutputStream;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v19, Lcom/chelpus/root/utils/odexrunpatch;->sddir:Ljava/lang/String;

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "/"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "AndroidManifest.xml"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-direct {v10, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 1174
    .local v10, "fout2":Ljava/io/FileOutputStream;
    const/16 v18, 0x800

    move/from16 v0, v18

    new-array v3, v0, [B

    .line 1176
    .local v3, "buffer2":[B
    :goto_1d1
    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/util/zip/ZipInputStream;->read([B)I

    move-result v13

    .local v13, "length2":I
    const/16 v18, -0x1

    move/from16 v0, v18

    if-eq v13, v0, :cond_1e5

    .line 1177
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v10, v3, v0, v13}, Ljava/io/FileOutputStream;->write([BII)V

    goto :goto_1d1

    .line 1179
    :cond_1e5
    invoke-virtual/range {v16 .. v16}, Ljava/util/zip/ZipInputStream;->closeEntry()V

    .line 1180
    invoke-virtual {v10}, Ljava/io/FileOutputStream;->close()V

    .line 1181
    const/16 v18, 0x3

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const-string v20, "chmod"

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, "777"

    aput-object v20, v18, v19

    const/16 v19, 0x2

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v21, Lcom/chelpus/root/utils/odexrunpatch;->sddir:Ljava/lang/String;

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "AndroidManifest.xml"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    aput-object v20, v18, v19

    invoke-static/range {v18 .. v18}, Lcom/chelpus/Utils;->cmdParam([Ljava/lang/String;)Ljava/lang/String;

    .line 1182
    const/4 v8, 0x1

    .line 1184
    .end local v3    # "buffer2":[B
    .end local v10    # "fout2":Ljava/io/FileOutputStream;
    .end local v13    # "length2":I
    :cond_222
    if-eqz v7, :cond_22f

    if-eqz v8, :cond_22f

    .line 1185
    const/4 v14, 0x0

    .line 1191
    .end local v11    # "haystack":Ljava/lang/String;
    :cond_227
    invoke-virtual/range {v16 .. v16}, Ljava/util/zip/ZipInputStream;->close()V

    .line 1192
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V

    goto/16 :goto_13d

    .line 1188
    .restart local v11    # "haystack":Ljava/lang/String;
    :cond_22f
    invoke-virtual/range {v16 .. v16}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;
    :try_end_232
    .catch Ljava/lang/Exception; {:try_start_13e .. :try_end_232} :catch_7a

    move-result-object v15

    .line 1190
    goto/16 :goto_16

    .line 1205
    .end local v6    # "fin":Ljava/io/FileInputStream;
    .end local v11    # "haystack":Ljava/lang/String;
    .end local v14    # "search":Z
    .end local v15    # "ze":Ljava/util/zip/ZipEntry;
    .end local v16    # "zin":Ljava/util/zip/ZipInputStream;
    .restart local v4    # "e":Ljava/lang/Exception;
    :catch_235
    move-exception v5

    .line 1206
    .local v5, "e1":Lnet/lingala/zip4j/exception/ZipException;
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Error classes.dex decompress! "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 1207
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Exception e1"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v4}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    goto/16 :goto_123

    .line 1208
    .end local v5    # "e1":Lnet/lingala/zip4j/exception/ZipException;
    :catch_26a
    move-exception v5

    .line 1209
    .local v5, "e1":Ljava/lang/Exception;
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Error classes.dex decompress! "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 1210
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Exception e1"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v4}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    goto/16 :goto_123
.end method

.method public static unzipSD(Ljava/io/File;)V
    .registers 14
    .param p0, "apk"    # Ljava/io/File;

    .prologue
    .line 1091
    :try_start_0
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 1092
    .local v3, "fin":Ljava/io/FileInputStream;
    new-instance v7, Ljava/util/zip/ZipInputStream;

    invoke-direct {v7, v3}, Ljava/util/zip/ZipInputStream;-><init>(Ljava/io/InputStream;)V

    .line 1093
    .local v7, "zin":Ljava/util/zip/ZipInputStream;
    const/4 v6, 0x0

    .line 1094
    .local v6, "ze":Ljava/util/zip/ZipEntry;
    :cond_b
    :goto_b
    invoke-virtual {v7}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    move-result-object v6

    if-eqz v6, :cond_e0

    .line 1098
    invoke-virtual {v6}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v9

    const-string v10, "classes"

    invoke-virtual {v9, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_b

    invoke-virtual {v6}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v9

    const-string v10, ".dex"

    invoke-virtual {v9, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_b

    invoke-virtual {v6}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v9

    const-string v10, "/"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_b

    .line 1099
    new-instance v4, Ljava/io/FileOutputStream;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v10, Lcom/chelpus/root/utils/odexrunpatch;->sddir:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/Modified/"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v6}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v4, v9}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 1101
    .local v4, "fout":Ljava/io/FileOutputStream;
    const/16 v9, 0x400

    new-array v0, v9, [B

    .line 1103
    .local v0, "buffer":[B
    :goto_5f
    invoke-virtual {v7, v0}, Ljava/util/zip/ZipInputStream;->read([B)I

    move-result v5

    .local v5, "length":I
    const/4 v9, -0x1

    if-eq v5, v9, :cond_b1

    .line 1104
    const/4 v9, 0x0

    invoke-virtual {v4, v0, v9, v5}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_6a
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_6a} :catch_6b

    goto :goto_5f

    .line 1116
    .end local v0    # "buffer":[B
    .end local v3    # "fin":Ljava/io/FileInputStream;
    .end local v4    # "fout":Ljava/io/FileOutputStream;
    .end local v5    # "length":I
    .end local v6    # "ze":Ljava/util/zip/ZipEntry;
    .end local v7    # "zin":Ljava/util/zip/ZipInputStream;
    :catch_6b
    move-exception v1

    .line 1118
    .local v1, "e":Ljava/lang/Exception;
    :try_start_6c
    new-instance v8, Lnet/lingala/zip4j/core/ZipFile;

    invoke-direct {v8, p0}, Lnet/lingala/zip4j/core/ZipFile;-><init>(Ljava/io/File;)V

    .line 1122
    .local v8, "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    const-string v9, "classes.dex"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v11, Lcom/chelpus/root/utils/odexrunpatch;->sddir:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "/Modified/"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Lnet/lingala/zip4j/core/ZipFile;->extractFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 1123
    sget-object v9, Lcom/chelpus/root/utils/odexrunpatch;->classesFiles:Ljava/util/ArrayList;

    new-instance v10, Ljava/io/File;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v12, Lcom/chelpus/root/utils/odexrunpatch;->sddir:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/Modified/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "classes.dex"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_b0
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_6c .. :try_end_b0} :catch_e7
    .catch Ljava/lang/Exception; {:try_start_6c .. :try_end_b0} :catch_119

    .line 1135
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v8    # "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    :goto_b0
    return-void

    .line 1106
    .restart local v0    # "buffer":[B
    .restart local v3    # "fin":Ljava/io/FileInputStream;
    .restart local v4    # "fout":Ljava/io/FileOutputStream;
    .restart local v5    # "length":I
    .restart local v6    # "ze":Ljava/util/zip/ZipEntry;
    .restart local v7    # "zin":Ljava/util/zip/ZipInputStream;
    :cond_b1
    :try_start_b1
    invoke-virtual {v7}, Ljava/util/zip/ZipInputStream;->closeEntry()V

    .line 1107
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V

    .line 1108
    sget-object v9, Lcom/chelpus/root/utils/odexrunpatch;->classesFiles:Ljava/util/ArrayList;

    new-instance v10, Ljava/io/File;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v12, Lcom/chelpus/root/utils/odexrunpatch;->sddir:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/Modified/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v6}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_b

    .line 1114
    .end local v0    # "buffer":[B
    .end local v4    # "fout":Ljava/io/FileOutputStream;
    .end local v5    # "length":I
    :cond_e0
    invoke-virtual {v7}, Ljava/util/zip/ZipInputStream;->close()V

    .line 1115
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_e6
    .catch Ljava/lang/Exception; {:try_start_b1 .. :try_end_e6} :catch_6b

    goto :goto_b0

    .line 1125
    .end local v3    # "fin":Ljava/io/FileInputStream;
    .end local v6    # "ze":Ljava/util/zip/ZipEntry;
    .end local v7    # "zin":Ljava/util/zip/ZipInputStream;
    .restart local v1    # "e":Ljava/lang/Exception;
    :catch_e7
    move-exception v2

    .line 1126
    .local v2, "e1":Lnet/lingala/zip4j/exception/ZipException;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Error classes.dex decompress! "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 1127
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Exception e1"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    goto :goto_b0

    .line 1128
    .end local v2    # "e1":Lnet/lingala/zip4j/exception/ZipException;
    :catch_119
    move-exception v2

    .line 1129
    .local v2, "e1":Ljava/lang/Exception;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Error classes.dex decompress! "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 1130
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Exception e1"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    goto/16 :goto_b0
.end method
