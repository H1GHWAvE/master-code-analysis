.class public Lcom/chelpus/root/utils/checkRoot;
.super Ljava/lang/Object;
.source "checkRoot.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .registers 30
    .param p0, "paramArrayOfString"    # [Ljava/lang/String;

    .prologue
    .line 14
    const-string v4, ""

    .line 15
    .local v4, "api":Ljava/lang/String;
    const-string v20, ""

    .line 16
    .local v20, "runtime":Ljava/lang/String;
    new-instance v24, Lcom/chelpus/root/utils/checkRoot$1;

    invoke-direct/range {v24 .. v24}, Lcom/chelpus/root/utils/checkRoot$1;-><init>()V

    invoke-static/range {v24 .. v24}, Lcom/chelpus/Utils;->startRootJava(Ljava/lang/Object;)V

    .line 17
    const-string v22, ""

    .line 18
    .local v22, "toolfilesdir":Ljava/lang/String;
    const/16 v24, 0x0

    aget-object v24, p0, v24

    if-eqz v24, :cond_18

    const/16 v24, 0x0

    aget-object v22, p0, v24

    .line 19
    :cond_18
    const/16 v24, 0x1

    aget-object v24, p0, v24

    if-eqz v24, :cond_22

    const/16 v24, 0x1

    aget-object v4, p0, v24

    .line 20
    :cond_22
    const/16 v24, 0x2

    aget-object v24, p0, v24

    if-eqz v24, :cond_2c

    const/16 v24, 0x2

    aget-object v20, p0, v24

    .line 21
    :cond_2c
    sput-object v22, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->toolfilesdir:Ljava/lang/String;

    .line 22
    sget-object v24, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v25, "root found!"

    invoke-virtual/range {v24 .. v25}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 24
    new-instance v10, Ljava/io/File;

    const-string v24, "/data/lp"

    move-object/from16 v0, v24

    invoke-direct {v10, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 25
    .local v10, "dir":Ljava/io/File;
    new-instance v23, Ljava/io/File;

    const-string v24, "/data/lp/lp_utils"

    invoke-direct/range {v23 .. v24}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 26
    .local v23, "utils_file":Ljava/io/File;
    invoke-virtual/range {v23 .. v23}, Ljava/io/File;->exists()Z

    move-result v24

    if-nez v24, :cond_331

    .line 27
    invoke-virtual {v10}, Ljava/io/File;->mkdirs()Z

    .line 28
    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v24

    if-nez v24, :cond_5b

    sget-object v24, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v25, "LP: dirs for utils not created."

    invoke-virtual/range {v24 .. v25}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 30
    :cond_5b
    :try_start_5b
    invoke-virtual/range {v23 .. v23}, Ljava/io/File;->createNewFile()Z

    .line 31
    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v24

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "%chelpus%"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "%chelpus%"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Lcom/chelpus/Utils;->save_text_to_file(Ljava/io/File;Ljava/lang/String;)Z

    .line 32
    const/16 v24, 0x3

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    const-string v26, "chmod"

    aput-object v26, v24, v25

    const/16 v25, 0x1

    const-string v26, "777"

    aput-object v26, v24, v25

    const/16 v25, 0x2

    invoke-virtual/range {v23 .. v23}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v26

    aput-object v26, v24, v25

    invoke-static/range {v24 .. v24}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 33
    const/16 v24, 0x3

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    const-string v26, "chmod"

    aput-object v26, v24, v25

    const/16 v25, 0x1

    const-string v26, "777"

    aput-object v26, v24, v25

    const/16 v25, 0x2

    invoke-virtual {v10}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v26

    aput-object v26, v24, v25

    invoke-static/range {v24 .. v24}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V
    :try_end_ca
    .catch Ljava/io/IOException; {:try_start_5b .. :try_end_ca} :catch_324

    .line 67
    :goto_ca
    invoke-static {}, Lcom/chelpus/Utils;->initXposedParam()Z

    move-result v24

    if-nez v24, :cond_d7

    .line 68
    sget-object v24, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v25, "Xposed settings not initialized"

    invoke-virtual/range {v24 .. v25}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 70
    :cond_d7
    new-instance v24, Ljava/io/File;

    const-string v25, "/system/xbin/busybox"

    invoke-direct/range {v24 .. v25}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v24 .. v24}, Ljava/io/File;->exists()Z

    move-result v24

    if-nez v24, :cond_119

    new-instance v24, Ljava/io/File;

    const-string v25, "/system/bin/busybox"

    invoke-direct/range {v24 .. v25}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v24 .. v24}, Ljava/io/File;->exists()Z

    move-result v24

    if-nez v24, :cond_119

    new-instance v24, Ljava/io/File;

    const-string v25, "/bin/busybox"

    invoke-direct/range {v24 .. v25}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v24 .. v24}, Ljava/io/File;->exists()Z

    move-result v24

    if-nez v24, :cond_119

    new-instance v24, Ljava/io/File;

    const-string v25, "/sbin/busybox"

    invoke-direct/range {v24 .. v25}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v24 .. v24}, Ljava/io/File;->exists()Z

    move-result v24

    if-nez v24, :cond_119

    .line 71
    sget-object v24, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v25, "LuckyPatcher: busybox not install!"

    invoke-virtual/range {v24 .. v25}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 72
    sget-object v24, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v25, "busybox not found!"

    invoke-virtual/range {v24 .. v25}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 74
    :cond_119
    new-instance v24, Ljava/io/File;

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v25

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, "/dalvikvm"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-direct/range {v24 .. v25}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v24 .. v24}, Ljava/io/File;->exists()Z

    move-result v24

    if-eqz v24, :cond_1a3

    .line 75
    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v24

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "/dalvikvm"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 76
    .local v9, "dalvikvm":Ljava/lang/String;
    const/16 v24, 0x3

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    const-string v26, "chmod"

    aput-object v26, v24, v25

    const/16 v25, 0x1

    const-string v26, "777"

    aput-object v26, v24, v25

    const/16 v25, 0x2

    aput-object v9, v24, v25

    invoke-static/range {v24 .. v24}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 77
    const/16 v24, 0x3

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    const-string v26, "chown"

    aput-object v26, v24, v25

    const/16 v25, 0x1

    const-string v26, "0.0"

    aput-object v26, v24, v25

    const/16 v25, 0x2

    aput-object v9, v24, v25

    invoke-static/range {v24 .. v24}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 78
    const/16 v24, 0x3

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    const-string v26, "chown"

    aput-object v26, v24, v25

    const/16 v25, 0x1

    const-string v26, "0:0"

    aput-object v26, v24, v25

    const/16 v25, 0x2

    aput-object v9, v24, v25

    invoke-static/range {v24 .. v24}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 80
    .end local v9    # "dalvikvm":Ljava/lang/String;
    :cond_1a3
    new-instance v24, Ljava/io/File;

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v25

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, "/busybox"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-direct/range {v24 .. v25}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v24 .. v24}, Ljava/io/File;->exists()Z

    move-result v24

    if-eqz v24, :cond_22d

    .line 81
    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v24

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "/busybox"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 82
    .local v8, "busybox":Ljava/lang/String;
    const/16 v24, 0x3

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    const-string v26, "chmod"

    aput-object v26, v24, v25

    const/16 v25, 0x1

    const-string v26, "06777"

    aput-object v26, v24, v25

    const/16 v25, 0x2

    aput-object v8, v24, v25

    invoke-static/range {v24 .. v24}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 83
    const/16 v24, 0x3

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    const-string v26, "chown"

    aput-object v26, v24, v25

    const/16 v25, 0x1

    const-string v26, "0.0"

    aput-object v26, v24, v25

    const/16 v25, 0x2

    aput-object v8, v24, v25

    invoke-static/range {v24 .. v24}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 84
    const/16 v24, 0x3

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    const-string v26, "chown"

    aput-object v26, v24, v25

    const/16 v25, 0x1

    const-string v26, "0:0"

    aput-object v26, v24, v25

    const/16 v25, 0x2

    aput-object v8, v24, v25

    invoke-static/range {v24 .. v24}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 86
    .end local v8    # "busybox":Ljava/lang/String;
    :cond_22d
    new-instance v24, Ljava/io/File;

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v25

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, "/reboot"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-direct/range {v24 .. v25}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v24 .. v24}, Ljava/io/File;->exists()Z

    move-result v24

    if-eqz v24, :cond_2b7

    .line 87
    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v24

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "/reboot"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 88
    .local v19, "reboot":Ljava/lang/String;
    const/16 v24, 0x3

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    const-string v26, "chmod"

    aput-object v26, v24, v25

    const/16 v25, 0x1

    const-string v26, "777"

    aput-object v26, v24, v25

    const/16 v25, 0x2

    aput-object v19, v24, v25

    invoke-static/range {v24 .. v24}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 89
    const/16 v24, 0x3

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    const-string v26, "chown"

    aput-object v26, v24, v25

    const/16 v25, 0x1

    const-string v26, "0.0"

    aput-object v26, v24, v25

    const/16 v25, 0x2

    aput-object v19, v24, v25

    invoke-static/range {v24 .. v24}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 90
    const/16 v24, 0x3

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    const-string v26, "chown"

    aput-object v26, v24, v25

    const/16 v25, 0x1

    const-string v26, "0:0"

    aput-object v26, v24, v25

    const/16 v25, 0x2

    aput-object v19, v24, v25

    invoke-static/range {v24 .. v24}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 94
    .end local v19    # "reboot":Ljava/lang/String;
    :cond_2b7
    const/4 v15, 0x0

    .line 95
    .local v15, "j":I
    const-string v7, "/data/app/"

    .line 101
    .local v7, "appDirStr":Ljava/lang/String;
    :try_start_2ba
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 102
    .local v6, "appDir":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v24

    if-eqz v24, :cond_435

    .line 103
    invoke-virtual {v6}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v13

    .line 104
    .local v13, "files":[Ljava/io/File;
    sget-object v24, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v25, "\nUnusedOdexList:"

    invoke-virtual/range {v24 .. v25}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 105
    array-length v0, v13

    move/from16 v25, v0

    const/16 v24, 0x0

    :goto_2d5
    move/from16 v0, v24

    move/from16 v1, v25

    if-ge v0, v1, :cond_42e

    aget-object v12, v13, v24

    .line 106
    .local v12, "file":Ljava/io/File;
    invoke-virtual {v12}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v26

    const-string v27, "apk"

    invoke-static/range {v26 .. v27}, Lcom/chelpus/Utils;->changeExtension(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 107
    .local v2, "Temp":Ljava/lang/String;
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 109
    .local v3, "TempFile":Ljava/io/File;
    invoke-virtual {v12}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v26

    const-string v27, ".odex"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v26

    if-eqz v26, :cond_321

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v26

    if-nez v26, :cond_321

    .line 110
    invoke-virtual {v12}, Ljava/io/File;->delete()Z

    .line 111
    sget-object v26, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v27

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, "|"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V
    :try_end_31f
    .catch Ljava/lang/Exception; {:try_start_2ba .. :try_end_31f} :catch_442

    .line 112
    add-int/lit8 v15, v15, 0x1

    .line 105
    :cond_321
    add-int/lit8 v24, v24, 0x1

    goto :goto_2d5

    .line 34
    .end local v2    # "Temp":Ljava/lang/String;
    .end local v3    # "TempFile":Ljava/io/File;
    .end local v6    # "appDir":Ljava/io/File;
    .end local v7    # "appDirStr":Ljava/lang/String;
    .end local v12    # "file":Ljava/io/File;
    .end local v13    # "files":[Ljava/io/File;
    .end local v15    # "j":I
    :catch_324
    move-exception v11

    .line 35
    .local v11, "e":Ljava/io/IOException;
    invoke-virtual {v11}, Ljava/io/IOException;->printStackTrace()V

    .line 36
    sget-object v24, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v25, "LP: file for utils not created."

    invoke-virtual/range {v24 .. v25}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_ca

    .line 39
    .end local v11    # "e":Ljava/io/IOException;
    :cond_331
    const-string v18, ""

    .line 40
    .local v18, "path":Ljava/lang/String;
    const-string v5, ""

    .line 41
    .local v5, "apiS":Ljava/lang/String;
    const-string v21, ""

    .line 42
    .local v21, "runtimeS":Ljava/lang/String;
    invoke-static/range {v23 .. v23}, Lcom/chelpus/Utils;->read_from_file(Ljava/io/File;)Ljava/lang/String;

    move-result-object v17

    .line 43
    .local v17, "params_string":Ljava/lang/String;
    const-string v24, "%chelpus%"

    move-object/from16 v0, v17

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v16

    .line 44
    .local v16, "params":[Ljava/lang/String;
    if-eqz v16, :cond_366

    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v24, v0

    if-lez v24, :cond_366

    .line 45
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_34f
    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v24, v0

    move/from16 v0, v24

    if-ge v14, v0, :cond_366

    .line 46
    packed-switch v14, :pswitch_data_460

    .line 45
    :goto_35b
    add-int/lit8 v14, v14, 0x1

    goto :goto_34f

    .line 48
    :pswitch_35e
    aget-object v18, v16, v14

    .line 49
    goto :goto_35b

    .line 51
    :pswitch_361
    aget-object v5, v16, v14

    .line 53
    :pswitch_363
    aget-object v21, v16, v14

    goto :goto_35b

    .line 57
    .end local v14    # "i":I
    :cond_366
    move-object/from16 v0, v18

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_3a4

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_3a4

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_3a4

    .line 58
    sget-object v24, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v25, "Utils file found and correct"

    invoke-virtual/range {v24 .. v25}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 59
    const/16 v24, 0x3

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    const-string v26, "chmod"

    aput-object v26, v24, v25

    const/16 v25, 0x1

    const-string v26, "777"

    aput-object v26, v24, v25

    const/16 v25, 0x2

    invoke-virtual/range {v23 .. v23}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v26

    aput-object v26, v24, v25

    invoke-static/range {v24 .. v24}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    goto/16 :goto_ca

    .line 61
    :cond_3a4
    sget-object v24, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "Utils file contain incorrect path "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 62
    const/16 v24, 0x3

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    const-string v26, "chmod"

    aput-object v26, v24, v25

    const/16 v25, 0x1

    const-string v26, "777"

    aput-object v26, v24, v25

    const/16 v25, 0x2

    invoke-virtual/range {v23 .. v23}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v26

    aput-object v26, v24, v25

    invoke-static/range {v24 .. v24}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 63
    const/16 v24, 0x3

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    const-string v26, "chmod"

    aput-object v26, v24, v25

    const/16 v25, 0x1

    const-string v26, "777"

    aput-object v26, v24, v25

    const/16 v25, 0x2

    invoke-virtual {v10}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v26

    aput-object v26, v24, v25

    invoke-static/range {v24 .. v24}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 64
    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v24

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "%chelpus%"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "%chelpus%"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Lcom/chelpus/Utils;->save_text_to_file(Ljava/io/File;Ljava/lang/String;)Z

    goto/16 :goto_ca

    .line 117
    .end local v5    # "apiS":Ljava/lang/String;
    .end local v16    # "params":[Ljava/lang/String;
    .end local v17    # "params_string":Ljava/lang/String;
    .end local v18    # "path":Ljava/lang/String;
    .end local v21    # "runtimeS":Ljava/lang/String;
    .restart local v6    # "appDir":Ljava/io/File;
    .restart local v7    # "appDirStr":Ljava/lang/String;
    .restart local v13    # "files":[Ljava/io/File;
    .restart local v15    # "j":I
    :cond_42e
    :try_start_42e
    sget-object v24, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v25, "\n"

    invoke-virtual/range {v24 .. v25}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V
    :try_end_435
    .catch Ljava/lang/Exception; {:try_start_42e .. :try_end_435} :catch_442

    .line 126
    .end local v6    # "appDir":Ljava/io/File;
    .end local v13    # "files":[Ljava/io/File;
    :cond_435
    :goto_435
    if-lez v15, :cond_43e

    sget-object v24, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v25, "Unused ODEX in /data/app/ removed!"

    invoke-virtual/range {v24 .. v25}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 127
    :cond_43e
    invoke-static {}, Lcom/chelpus/Utils;->exitFromRootJava()V

    .line 128
    return-void

    .line 121
    :catch_442
    move-exception v11

    .line 122
    .local v11, "e":Ljava/lang/Exception;
    sget-object v24, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "Exception e"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual {v11}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_435

    .line 46
    :pswitch_data_460
    .packed-switch 0x0
        :pswitch_35e
        :pswitch_361
        :pswitch_363
    .end packed-switch
.end method
