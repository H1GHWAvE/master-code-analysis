.class public Lcom/chelpus/root/utils/createapkcustom;
.super Ljava/lang/Object;
.source "createapkcustom.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chelpus/root/utils/createapkcustom$Decompress;
    }
.end annotation


# static fields
.field static final BUFFER:I = 0x800

.field private static final all:I = 0x4

.field public static appdir:Ljava/lang/String; = null

.field private static final armeabi:I = 0x0

.field private static final armeabiv7a:I = 0x1

.field private static final beginTag:I = 0x0

.field public static classesFiles:Ljava/util/ArrayList; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private static final classesTag:I = 0x1

.field public static crkapk:Ljava/io/File; = null

.field public static dir:Ljava/lang/String; = null

.field public static dir2:Ljava/lang/String; = null

.field private static final endTag:I = 0x4

.field private static final fileInApkTag:I = 0xa

.field public static goodResult:Z = false

.field private static group:Ljava/lang/String; = null

.field private static final libTagALL:I = 0x2

.field private static final libTagARMEABI:I = 0x6

.field private static final libTagARMEABIV7A:I = 0x7

.field private static final libTagMIPS:I = 0x8

.field private static final libTagx86:I = 0x9

.field private static libs:Ljava/util/ArrayList; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static localFile2:Ljava/io/File; = null

.field public static manualpatch:Z = false

.field private static final mips:I = 0x2

.field public static multidex:Z = false

.field public static multilib_patch:Z = false

.field public static packageName:Ljava/lang/String; = null

.field private static final packageTag:I = 0x5

.field private static pat:Ljava/util/ArrayList; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;",
            ">;"
        }
    .end annotation
.end field

.field private static patchedLibs:Ljava/util/ArrayList; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static patchteil:Z = false

.field private static print:Ljava/io/PrintStream; = null

.field public static sddir:Ljava/lang/String; = null

.field private static search:Ljava/util/ArrayList; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Byte;",
            ">;"
        }
    .end annotation
.end field

.field private static ser:Ljava/util/ArrayList; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;",
            ">;"
        }
    .end annotation
.end field

.field public static tag:I = 0x0

.field public static tooldir:Ljava/lang/String; = null

.field public static unpack:Z = false

.field private static final x86:I = 0x3


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 44
    sput-object v0, Lcom/chelpus/root/utils/createapkcustom;->pat:Ljava/util/ArrayList;

    .line 45
    sput-object v0, Lcom/chelpus/root/utils/createapkcustom;->ser:Ljava/util/ArrayList;

    .line 46
    sput-object v0, Lcom/chelpus/root/utils/createapkcustom;->search:Ljava/util/ArrayList;

    .line 47
    sput-boolean v1, Lcom/chelpus/root/utils/createapkcustom;->patchteil:Z

    .line 48
    sput-boolean v1, Lcom/chelpus/root/utils/createapkcustom;->unpack:Z

    .line 49
    sput-boolean v1, Lcom/chelpus/root/utils/createapkcustom;->manualpatch:Z

    .line 50
    const-string v0, "/sdcard/"

    sput-object v0, Lcom/chelpus/root/utils/createapkcustom;->dir:Ljava/lang/String;

    .line 51
    const-string v0, "/sdcard/"

    sput-object v0, Lcom/chelpus/root/utils/createapkcustom;->dir2:Ljava/lang/String;

    .line 52
    const-string v0, "/sdcard/"

    sput-object v0, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    .line 53
    const-string v0, "/sdcard/"

    sput-object v0, Lcom/chelpus/root/utils/createapkcustom;->appdir:Ljava/lang/String;

    .line 54
    const-string v0, "/sdcard/"

    sput-object v0, Lcom/chelpus/root/utils/createapkcustom;->tooldir:Ljava/lang/String;

    .line 55
    const-string v0, ""

    sput-object v0, Lcom/chelpus/root/utils/createapkcustom;->packageName:Ljava/lang/String;

    .line 75
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/chelpus/root/utils/createapkcustom;->libs:Ljava/util/ArrayList;

    .line 76
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/chelpus/root/utils/createapkcustom;->patchedLibs:Ljava/util/ArrayList;

    .line 78
    const-string v0, ""

    sput-object v0, Lcom/chelpus/root/utils/createapkcustom;->group:Ljava/lang/String;

    .line 79
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/chelpus/root/utils/createapkcustom;->classesFiles:Ljava/util/ArrayList;

    .line 80
    sput-boolean v1, Lcom/chelpus/root/utils/createapkcustom;->multidex:Z

    .line 81
    sput-boolean v1, Lcom/chelpus/root/utils/createapkcustom;->goodResult:Z

    .line 82
    sput-boolean v1, Lcom/chelpus/root/utils/createapkcustom;->multilib_patch:Z

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1202
    return-void
.end method

.method static synthetic access$000()Ljava/io/PrintStream;
    .registers 1

    .prologue
    .line 41
    sget-object v0, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    return-object v0
.end method

.method private static final calcChecksum([BI)V
    .registers 7
    .param p0, "paramArrayOfByte"    # [B
    .param p1, "j"    # I

    .prologue
    .line 1014
    new-instance v1, Ljava/util/zip/Adler32;

    invoke-direct {v1}, Ljava/util/zip/Adler32;-><init>()V

    .line 1015
    .local v1, "localAdler32":Ljava/util/zip/Adler32;
    const/16 v2, 0xc

    array-length v3, p0

    add-int/lit8 v4, p1, 0xc

    sub-int/2addr v3, v4

    invoke-virtual {v1, p0, v2, v3}, Ljava/util/zip/Adler32;->update([BII)V

    .line 1016
    invoke-virtual {v1}, Ljava/util/zip/Adler32;->getValue()J

    move-result-wide v2

    long-to-int v0, v2

    .line 1017
    .local v0, "i":I
    add-int/lit8 v2, p1, 0x8

    int-to-byte v3, v0

    aput-byte v3, p0, v2

    .line 1018
    add-int/lit8 v2, p1, 0x9

    shr-int/lit8 v3, v0, 0x8

    int-to-byte v3, v3

    aput-byte v3, p0, v2

    .line 1019
    add-int/lit8 v2, p1, 0xa

    shr-int/lit8 v3, v0, 0x10

    int-to-byte v3, v3

    aput-byte v3, p0, v2

    .line 1020
    add-int/lit8 v2, p1, 0xb

    shr-int/lit8 v3, v0, 0x18

    int-to-byte v3, v3

    aput-byte v3, p0, v2

    .line 1021
    return-void
.end method

.method private static final calcSignature([BI)V
    .registers 10
    .param p0, "paramArrayOfByte"    # [B
    .param p1, "j"    # I

    .prologue
    const/16 v7, 0x14

    .line 1027
    :try_start_2
    const-string v4, "SHA-1"

    invoke-static {v4}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_7
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_2 .. :try_end_7} :catch_41

    move-result-object v2

    .line 1034
    .local v2, "localMessageDigest":Ljava/security/MessageDigest;
    const/16 v4, 0x20

    array-length v5, p0

    add-int/lit8 v6, p1, 0x20

    sub-int/2addr v5, v6

    invoke-virtual {v2, p0, v4, v5}, Ljava/security/MessageDigest;->update([BII)V

    .line 1037
    add-int/lit8 v4, p1, 0xc

    const/16 v5, 0x14

    :try_start_15
    invoke-virtual {v2, p0, v4, v5}, Ljava/security/MessageDigest;->digest([BII)I

    move-result v0

    .line 1038
    .local v0, "i":I
    if-eq v0, v7, :cond_48

    .line 1039
    new-instance v4, Ljava/lang/RuntimeException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "unexpected digest write:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "bytes"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_3a
    .catch Ljava/security/DigestException; {:try_start_15 .. :try_end_3a} :catch_3a

    .line 1041
    .end local v0    # "i":I
    :catch_3a
    move-exception v1

    .line 1043
    .local v1, "localDigestException":Ljava/security/DigestException;
    new-instance v4, Ljava/lang/RuntimeException;

    invoke-direct {v4, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v4

    .line 1029
    .end local v1    # "localDigestException":Ljava/security/DigestException;
    .end local v2    # "localMessageDigest":Ljava/security/MessageDigest;
    :catch_41
    move-exception v3

    .line 1031
    .local v3, "localNoSuchAlgorithmException":Ljava/security/NoSuchAlgorithmException;
    new-instance v4, Ljava/lang/RuntimeException;

    invoke-direct {v4, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v4

    .line 1045
    .end local v3    # "localNoSuchAlgorithmException":Ljava/security/NoSuchAlgorithmException;
    .restart local v0    # "i":I
    .restart local v2    # "localMessageDigest":Ljava/security/MessageDigest;
    :cond_48
    return-void
.end method

.method public static clearTemp()V
    .registers 7

    .prologue
    .line 1128
    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/Modified/classes.dex.apk"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1132
    .local v2, "tmp":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1133
    .local v1, "tempdex":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_23

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 1134
    :cond_23
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/tmp/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1135
    new-instance v1, Ljava/io/File;

    .end local v1    # "tempdex":Ljava/io/File;
    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1136
    .restart local v1    # "tempdex":Ljava/io/File;
    new-instance v3, Lcom/chelpus/Utils;

    const-string v4, "createcustompatch"

    invoke-direct {v3, v4}, Lcom/chelpus/Utils;-><init>(Ljava/lang/String;)V

    .line 1137
    .local v3, "ut":Lcom/chelpus/Utils;
    invoke-virtual {v3, v1}, Lcom/chelpus/Utils;->deleteFolder(Ljava/io/File;)V
    :try_end_47
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_47} :catch_48

    .line 1141
    .end local v1    # "tempdex":Ljava/io/File;
    .end local v3    # "ut":Lcom/chelpus/Utils;
    :goto_47
    return-void

    .line 1138
    :catch_48
    move-exception v0

    .line 1139
    .local v0, "e":Ljava/lang/Exception;
    sget-object v4, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_47
.end method

.method public static extractFile(Ljava/io/File;Ljava/lang/String;)Ljava/lang/String;
    .registers 7
    .param p0, "apk"    # Ljava/io/File;
    .param p1, "file"    # Ljava/lang/String;

    .prologue
    .line 1156
    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    .line 1157
    .local v2, "zipFile":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/tmp/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1159
    .local v1, "unzipLocation":Ljava/lang/String;
    new-instance v0, Lcom/chelpus/root/utils/createapkcustom$Decompress;

    invoke-direct {v0, v2, v1}, Lcom/chelpus/root/utils/createapkcustom$Decompress;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1160
    .local v0, "d":Lcom/chelpus/root/utils/createapkcustom$Decompress;
    invoke-virtual {v0, p1}, Lcom/chelpus/root/utils/createapkcustom$Decompress;->unzip(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public static extractLibs(Ljava/io/File;)V
    .registers 7
    .param p0, "apk"    # Ljava/io/File;

    .prologue
    .line 1144
    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    .line 1145
    .local v3, "zipFile":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/tmp/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1146
    .local v2, "unzipLocation":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/tmp/lib/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1147
    .local v1, "tmp":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_41

    .line 1149
    new-instance v0, Lcom/chelpus/root/utils/createapkcustom$Decompress;

    invoke-direct {v0, v3, v2}, Lcom/chelpus/root/utils/createapkcustom$Decompress;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1150
    .local v0, "d":Lcom/chelpus/root/utils/createapkcustom$Decompress;
    invoke-virtual {v0}, Lcom/chelpus/root/utils/createapkcustom$Decompress;->unzip()V

    .line 1153
    .end local v0    # "d":Lcom/chelpus/root/utils/createapkcustom$Decompress;
    :cond_41
    return-void
.end method

.method public static fixadler(Ljava/io/File;)V
    .registers 6
    .param p0, "destFile"    # Ljava/io/File;

    .prologue
    .line 997
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 998
    .local v2, "localFileInputStream":Ljava/io/FileInputStream;
    invoke-virtual {v2}, Ljava/io/FileInputStream;->available()I

    move-result v4

    new-array v0, v4, [B

    .line 999
    .local v0, "arrayOfByte":[B
    invoke-virtual {v2, v0}, Ljava/io/FileInputStream;->read([B)I

    .line 1000
    const/4 v4, 0x0

    invoke-static {v0, v4}, Lcom/chelpus/root/utils/createapkcustom;->calcSignature([BI)V

    .line 1001
    const/4 v4, 0x0

    invoke-static {v0, v4}, Lcom/chelpus/root/utils/createapkcustom;->calcChecksum([BI)V

    .line 1002
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V

    .line 1003
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, p0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 1004
    .local v3, "localFileOutputStream":Ljava/io/FileOutputStream;
    invoke-virtual {v3, v0}, Ljava/io/FileOutputStream;->write([B)V

    .line 1005
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_24
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_24} :catch_25

    .line 1011
    .end local v0    # "arrayOfByte":[B
    .end local v2    # "localFileInputStream":Ljava/io/FileInputStream;
    .end local v3    # "localFileOutputStream":Ljava/io/FileOutputStream;
    :goto_24
    return-void

    .line 1007
    :catch_25
    move-exception v1

    .line 1009
    .local v1, "localException":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_24
.end method

.method public static getClassesDex()V
    .registers 7

    .prologue
    .line 883
    :try_start_0
    new-instance v0, Ljava/io/File;

    sget-object v4, Lcom/chelpus/root/utils/createapkcustom;->appdir:Ljava/lang/String;

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 884
    .local v0, "apk":Ljava/io/File;
    new-instance v4, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/Modified/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/chelpus/root/utils/createapkcustom;->packageName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ".apk"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v4, Lcom/chelpus/root/utils/createapkcustom;->crkapk:Ljava/io/File;

    .line 885
    sget-object v4, Lcom/chelpus/root/utils/createapkcustom;->crkapk:Ljava/io/File;

    invoke-static {v0, v4}, Lcom/chelpus/Utils;->copyFile(Ljava/io/File;Ljava/io/File;)V

    .line 886
    sget-object v4, Lcom/chelpus/root/utils/createapkcustom;->crkapk:Ljava/io/File;

    invoke-static {v4}, Lcom/chelpus/root/utils/createapkcustom;->unzip(Ljava/io/File;)V

    .line 887
    sget-object v4, Lcom/chelpus/root/utils/createapkcustom;->classesFiles:Ljava/util/ArrayList;

    if-eqz v4, :cond_45

    sget-object v4, Lcom/chelpus/root/utils/createapkcustom;->classesFiles:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-nez v4, :cond_54

    :cond_45
    new-instance v4, Ljava/io/FileNotFoundException;

    invoke-direct {v4}, Ljava/io/FileNotFoundException;-><init>()V

    throw v4
    :try_end_4b
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_4b} :catch_4b
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_4b} :catch_7e

    .line 895
    :catch_4b
    move-exception v3

    .line 896
    .local v3, "localFileNotFoundException":Ljava/io/FileNotFoundException;
    sget-object v4, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v5, "Error LP: unzip classes.dex fault!\n\n"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 902
    .end local v3    # "localFileNotFoundException":Ljava/io/FileNotFoundException;
    :cond_53
    :goto_53
    return-void

    .line 888
    :cond_54
    :try_start_54
    sget-object v4, Lcom/chelpus/root/utils/createapkcustom;->classesFiles:Ljava/util/ArrayList;

    if-eqz v4, :cond_53

    sget-object v4, Lcom/chelpus/root/utils/createapkcustom;->classesFiles:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_53

    .line 889
    sget-object v4, Lcom/chelpus/root/utils/createapkcustom;->classesFiles:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_66
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_53

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/File;

    .line 890
    .local v1, "cl":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_66

    new-instance v4, Ljava/io/FileNotFoundException;

    invoke-direct {v4}, Ljava/io/FileNotFoundException;-><init>()V

    throw v4
    :try_end_7e
    .catch Ljava/io/FileNotFoundException; {:try_start_54 .. :try_end_7e} :catch_4b
    .catch Ljava/lang/Exception; {:try_start_54 .. :try_end_7e} :catch_7e

    .line 898
    .end local v1    # "cl":Ljava/io/File;
    :catch_7e
    move-exception v2

    .line 899
    .local v2, "e":Ljava/lang/Exception;
    sget-object v4, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Extract classes.dex error: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_53
.end method

.method public static getFileFromApk(Ljava/lang/String;)Ljava/lang/String;
    .registers 6
    .param p0, "file"    # Ljava/lang/String;

    .prologue
    .line 982
    :try_start_0
    new-instance v0, Ljava/io/File;

    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->appdir:Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 983
    .local v0, "apk":Ljava/io/File;
    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/Modified/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/chelpus/root/utils/createapkcustom;->packageName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".apk"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v2, Lcom/chelpus/root/utils/createapkcustom;->crkapk:Ljava/io/File;

    .line 984
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->crkapk:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_3c

    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->crkapk:Ljava/io/File;

    invoke-static {v0, v2}, Lcom/chelpus/Utils;->copyFile(Ljava/io/File;Ljava/io/File;)V

    .line 986
    :cond_3c
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->crkapk:Ljava/io/File;

    invoke-static {v2, p0}, Lcom/chelpus/root/utils/createapkcustom;->extractFile(Ljava/io/File;Ljava/lang/String;)Ljava/lang/String;
    :try_end_41
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_41} :catch_43

    move-result-object v2

    .line 991
    .end local v0    # "apk":Ljava/io/File;
    :goto_42
    return-object v2

    .line 988
    :catch_43
    move-exception v1

    .line 989
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Lib select error: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 991
    const-string v2, ""

    goto :goto_42
.end method

.method public static main([Ljava/lang/String;)Ljava/lang/String;
    .registers 56
    .param p0, "paramArrayOfString"    # [Ljava/lang/String;

    .prologue
    .line 86
    new-instance v39, Lcom/android/vending/billing/InAppBillingService/LUCK/LogOutputStream;

    const-string v2, "System.out"

    move-object/from16 v0, v39

    invoke-direct {v0, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/LogOutputStream;-><init>(Ljava/lang/String;)V

    .line 87
    .local v39, "pout":Lcom/android/vending/billing/InAppBillingService/LUCK/LogOutputStream;
    new-instance v2, Ljava/io/PrintStream;

    move-object/from16 v0, v39

    invoke-direct {v2, v0}, Ljava/io/PrintStream;-><init>(Ljava/io/OutputStream;)V

    sput-object v2, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    .line 89
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v7, "SU Java-Code Running!"

    invoke-virtual {v2, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 90
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->patchedLibs:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 91
    const/4 v2, 0x0

    aget-object v2, p0, v2

    sput-object v2, Lcom/chelpus/root/utils/createapkcustom;->packageName:Ljava/lang/String;

    .line 92
    const/4 v2, 0x2

    aget-object v2, p0, v2

    sput-object v2, Lcom/chelpus/root/utils/createapkcustom;->appdir:Ljava/lang/String;

    .line 93
    const/4 v2, 0x3

    aget-object v2, p0, v2

    sput-object v2, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    .line 94
    const/4 v2, 0x4

    aget-object v2, p0, v2

    sput-object v2, Lcom/chelpus/root/utils/createapkcustom;->tooldir:Ljava/lang/String;

    .line 95
    invoke-static {}, Lcom/chelpus/root/utils/createapkcustom;->clearTemp()V

    .line 96
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/createapkcustom;->manualpatch:Z

    .line 98
    const-string v26, ""

    .line 99
    .local v26, "finalText":Ljava/lang/String;
    const-string v12, ""

    .line 101
    .local v12, "beginText":Ljava/lang/String;
    const/16 v23, 0x0

    .local v23, "error":Z
    const/16 v22, 0x0

    .line 102
    .local v22, "end":Z
    invoke-static {}, Lcom/chelpus/root/utils/createapkcustom;->getClassesDex()V

    .line 109
    :try_start_43
    new-instance v27, Ljava/io/FileInputStream;

    const/4 v2, 0x1

    aget-object v2, p0, v2

    move-object/from16 v0, v27

    invoke-direct {v0, v2}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 110
    .local v27, "fis":Ljava/io/FileInputStream;
    new-instance v28, Ljava/io/InputStreamReader;

    const-string v2, "UTF-8"

    move-object/from16 v0, v28

    move-object/from16 v1, v27

    invoke-direct {v0, v1, v2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 111
    .local v28, "isr":Ljava/io/InputStreamReader;
    new-instance v13, Ljava/io/BufferedReader;

    move-object/from16 v0, v28

    invoke-direct {v13, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 112
    .local v13, "br":Ljava/io/BufferedReader;
    const-string v16, ""

    .line 113
    .local v16, "data":Ljava/lang/String;
    const/16 v2, 0x3e8

    new-array v0, v2, [Ljava/lang/String;

    move-object/from16 v47, v0

    .line 114
    .local v47, "txtdata":[Ljava/lang/String;
    const/4 v2, 0x1

    new-array v0, v2, [Ljava/lang/String;

    move-object/from16 v38, v0

    const/4 v2, 0x0

    const-string v7, ""

    aput-object v7, v38, v2

    .line 116
    .local v38, "orhex":[Ljava/lang/String;
    const/4 v3, 0x0

    .line 118
    .local v3, "byteOrig":[B
    const/4 v4, 0x0

    .line 120
    .local v4, "mask":[I
    const/16 v42, 0x1

    .local v42, "result":Z
    const/16 v45, 0x1

    .local v45, "sumresult":Z
    const/16 v32, 0x0

    .local v32, "libr":Z
    const/4 v11, 0x0

    .local v11, "begin":Z
    const/16 v36, 0x0

    .local v36, "mark_search":Z
    const/16 v25, 0x0

    .line 121
    .local v25, "fileInApk":Z
    const-string v48, ""

    .line 122
    .local v48, "value1":Ljava/lang/String;
    const-string v49, ""

    .line 123
    .local v49, "value2":Ljava/lang/String;
    const-string v50, ""

    .line 124
    .local v50, "value3":Ljava/lang/String;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    sput-object v2, Lcom/chelpus/root/utils/createapkcustom;->pat:Ljava/util/ArrayList;

    .line 125
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    sput-object v2, Lcom/chelpus/root/utils/createapkcustom;->ser:Ljava/util/ArrayList;

    .line 126
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    sput-object v2, Lcom/chelpus/root/utils/createapkcustom;->search:Ljava/util/ArrayList;

    .line 127
    const/16 v40, 0x0

    .line 129
    .local v40, "r":I
    :goto_9b
    invoke-virtual {v13}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v16

    if-eqz v16, :cond_dfa

    .line 131
    const-string v2, ""

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b3

    .line 132
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->packageName:Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-static {v0, v2}, Lcom/chelpus/Utils;->apply_TAGS(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 134
    :cond_b3
    aput-object v16, v47, v40

    .line 136
    if-eqz v11, :cond_f4

    aget-object v2, v47, v40

    const-string v7, "["

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_d5

    aget-object v2, v47, v40

    const-string v7, "]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_d5

    aget-object v2, v47, v40

    const-string v7, "{"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_f4

    .line 138
    :cond_d5
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 139
    const/4 v11, 0x0

    .line 141
    :cond_f4
    if-eqz v11, :cond_10f

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, "\n"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v7, v47, v40

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 142
    :cond_10f
    aget-object v2, v47, v40

    const-string v7, "["

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_128

    aget-object v2, v47, v40

    const-string v7, "]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_128

    .line 143
    sget v2, Lcom/chelpus/root/utils/createapkcustom;->tag:I

    packed-switch v2, :pswitch_data_e46

    .line 282
    :cond_128
    :goto_128
    :pswitch_128
    aget-object v2, v47, v40

    const-string v7, "[BEGIN]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_136

    .line 283
    const/4 v2, 0x0

    sput v2, Lcom/chelpus/root/utils/createapkcustom;->tag:I

    .line 284
    const/4 v11, 0x1

    .line 287
    :cond_136
    aget-object v2, v47, v40

    const-string v7, "[CLASSES]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_14a

    aget-object v2, v47, v40

    const-string v7, "[ODEX]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_14d

    .line 288
    :cond_14a
    const/4 v2, 0x1

    sput v2, Lcom/chelpus/root/utils/createapkcustom;->tag:I

    .line 290
    :cond_14d
    aget-object v2, v47, v40

    const-string v7, "[PACKAGE]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_15a

    .line 291
    const/4 v2, 0x5

    sput v2, Lcom/chelpus/root/utils/createapkcustom;->tag:I

    .line 294
    :cond_15a
    if-eqz v32, :cond_17e

    .line 295
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->ser:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 296
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V
    :try_end_166
    .catch Ljava/io/FileNotFoundException; {:try_start_43 .. :try_end_166} :catch_3c7
    .catch Ljava/io/IOException; {:try_start_43 .. :try_end_166} :catch_456
    .catch Ljava/lang/InterruptedException; {:try_start_43 .. :try_end_166} :catch_4f7

    .line 298
    :try_start_166
    new-instance v29, Lorg/json/JSONObject;

    aget-object v2, v47, v40

    move-object/from16 v0, v29

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 299
    .local v29, "json_obj":Lorg/json/JSONObject;
    const-string v2, "name"

    move-object/from16 v0, v29

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_176
    .catch Lorg/json/JSONException; {:try_start_166 .. :try_end_176} :catch_77b
    .catch Ljava/io/FileNotFoundException; {:try_start_166 .. :try_end_176} :catch_3c7
    .catch Ljava/io/IOException; {:try_start_166 .. :try_end_176} :catch_456
    .catch Ljava/lang/InterruptedException; {:try_start_166 .. :try_end_176} :catch_4f7

    move-result-object v48

    .line 303
    .end local v29    # "json_obj":Lorg/json/JSONObject;
    :goto_177
    :try_start_177
    sget v2, Lcom/chelpus/root/utils/createapkcustom;->tag:I

    packed-switch v2, :pswitch_data_e5e

    .line 326
    :goto_17c
    :pswitch_17c
    const/16 v32, 0x0

    .line 328
    :cond_17e
    if-eqz v25, :cond_1b7

    .line 329
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->ser:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 330
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V
    :try_end_18a
    .catch Ljava/io/FileNotFoundException; {:try_start_177 .. :try_end_18a} :catch_3c7
    .catch Ljava/io/IOException; {:try_start_177 .. :try_end_18a} :catch_456
    .catch Ljava/lang/InterruptedException; {:try_start_177 .. :try_end_18a} :catch_4f7

    .line 332
    :try_start_18a
    new-instance v29, Lorg/json/JSONObject;

    aget-object v2, v47, v40

    move-object/from16 v0, v29

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 333
    .restart local v29    # "json_obj":Lorg/json/JSONObject;
    const-string v2, "name"

    move-object/from16 v0, v29

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_19a
    .catch Lorg/json/JSONException; {:try_start_18a .. :try_end_19a} :catch_7d5
    .catch Ljava/io/FileNotFoundException; {:try_start_18a .. :try_end_19a} :catch_3c7
    .catch Ljava/io/IOException; {:try_start_18a .. :try_end_19a} :catch_456
    .catch Ljava/lang/InterruptedException; {:try_start_18a .. :try_end_19a} :catch_4f7

    move-result-object v48

    .line 337
    .end local v29    # "json_obj":Lorg/json/JSONObject;
    :goto_19b
    :try_start_19b
    invoke-static/range {v48 .. v48}, Lcom/chelpus/root/utils/createapkcustom;->getFileFromApk(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    .line 338
    .local v24, "file":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    move-object/from16 v0, v24

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_7df

    .line 339
    new-instance v2, Ljava/io/File;

    move-object/from16 v0, v24

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v2, Lcom/chelpus/root/utils/createapkcustom;->localFile2:Ljava/io/File;

    .line 343
    :goto_1b5
    const/16 v25, 0x0

    .line 345
    .end local v24    # "file":Ljava/lang/String;
    :cond_1b7
    aget-object v2, v47, v40

    const-string v7, "[LIB-ARMEABI]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1cb

    .line 346
    const/4 v2, 0x6

    sput v2, Lcom/chelpus/root/utils/createapkcustom;->tag:I

    .line 347
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/createapkcustom;->unpack:Z

    .line 348
    const/16 v25, 0x0

    .line 349
    const/16 v32, 0x1

    .line 351
    :cond_1cb
    aget-object v2, v47, v40

    const-string v7, "[LIB-ARMEABI-V7A]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1df

    .line 352
    const/4 v2, 0x7

    sput v2, Lcom/chelpus/root/utils/createapkcustom;->tag:I

    .line 353
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/createapkcustom;->unpack:Z

    .line 354
    const/16 v25, 0x0

    .line 355
    const/16 v32, 0x1

    .line 357
    :cond_1df
    aget-object v2, v47, v40

    const-string v7, "[LIB-MIPS]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1f4

    .line 358
    const/16 v2, 0x8

    sput v2, Lcom/chelpus/root/utils/createapkcustom;->tag:I

    .line 359
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/createapkcustom;->unpack:Z

    .line 360
    const/16 v25, 0x0

    .line 361
    const/16 v32, 0x1

    .line 363
    :cond_1f4
    aget-object v2, v47, v40

    const-string v7, "[LIB-X86]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_209

    .line 364
    const/16 v2, 0x9

    sput v2, Lcom/chelpus/root/utils/createapkcustom;->tag:I

    .line 365
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/createapkcustom;->unpack:Z

    .line 366
    const/16 v25, 0x0

    .line 367
    const/16 v32, 0x1

    .line 369
    :cond_209
    aget-object v2, v47, v40

    const-string v7, "[LIB]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_21d

    .line 370
    const/4 v2, 0x2

    sput v2, Lcom/chelpus/root/utils/createapkcustom;->tag:I

    .line 371
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/createapkcustom;->unpack:Z

    .line 372
    const/16 v25, 0x0

    .line 373
    const/16 v32, 0x1

    .line 376
    :cond_21d
    aget-object v2, v47, v40

    const-string v7, "[FILE_IN_APK]"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_232

    .line 377
    const/16 v2, 0xa

    sput v2, Lcom/chelpus/root/utils/createapkcustom;->tag:I

    .line 378
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/createapkcustom;->unpack:Z

    .line 379
    const/16 v32, 0x0

    .line 380
    const/16 v25, 0x1

    .line 385
    :cond_232
    aget-object v2, v47, v40

    const-string v7, "group"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_239
    .catch Ljava/io/FileNotFoundException; {:try_start_19b .. :try_end_239} :catch_3c7
    .catch Ljava/io/IOException; {:try_start_19b .. :try_end_239} :catch_456
    .catch Ljava/lang/InterruptedException; {:try_start_19b .. :try_end_239} :catch_4f7

    move-result v2

    if-eqz v2, :cond_24f

    .line 387
    :try_start_23c
    new-instance v29, Lorg/json/JSONObject;

    aget-object v2, v47, v40

    move-object/from16 v0, v29

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 388
    .restart local v29    # "json_obj":Lorg/json/JSONObject;
    const-string v2, "group"

    move-object/from16 v0, v29

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/chelpus/root/utils/createapkcustom;->group:Ljava/lang/String;
    :try_end_24f
    .catch Lorg/json/JSONException; {:try_start_23c .. :try_end_24f} :catch_7e8
    .catch Ljava/io/FileNotFoundException; {:try_start_23c .. :try_end_24f} :catch_3c7
    .catch Ljava/io/IOException; {:try_start_23c .. :try_end_24f} :catch_456
    .catch Ljava/lang/InterruptedException; {:try_start_23c .. :try_end_24f} :catch_4f7

    .line 394
    .end local v29    # "json_obj":Lorg/json/JSONObject;
    :cond_24f
    :goto_24f
    :try_start_24f
    aget-object v2, v47, v40

    const-string v7, "original"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_820

    .line 396
    if-eqz v36, :cond_263

    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->ser:Ljava/util/ArrayList;

    invoke-static {v2}, Lcom/chelpus/root/utils/createapkcustom;->searchProcess(Ljava/util/ArrayList;)Z
    :try_end_260
    .catch Ljava/io/FileNotFoundException; {:try_start_24f .. :try_end_260} :catch_3c7
    .catch Ljava/io/IOException; {:try_start_24f .. :try_end_260} :catch_456
    .catch Ljava/lang/InterruptedException; {:try_start_24f .. :try_end_260} :catch_4f7

    move-result v45

    const/16 v36, 0x0

    .line 398
    :cond_263
    :try_start_263
    new-instance v29, Lorg/json/JSONObject;

    aget-object v2, v47, v40

    move-object/from16 v0, v29

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 399
    .restart local v29    # "json_obj":Lorg/json/JSONObject;
    const-string v2, "original"

    move-object/from16 v0, v29

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_273
    .catch Lorg/json/JSONException; {:try_start_263 .. :try_end_273} :catch_7f6
    .catch Ljava/io/FileNotFoundException; {:try_start_263 .. :try_end_273} :catch_3c7
    .catch Ljava/io/IOException; {:try_start_263 .. :try_end_273} :catch_456
    .catch Ljava/lang/InterruptedException; {:try_start_263 .. :try_end_273} :catch_4f7

    move-result-object v48

    .line 403
    .end local v29    # "json_obj":Lorg/json/JSONObject;
    :goto_274
    :try_start_274
    invoke-virtual/range {v48 .. v48}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v48

    .line 405
    const-string v2, "[ \t]+"

    move-object/from16 v0, v48

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    array-length v2, v2

    new-array v0, v2, [Ljava/lang/String;

    move-object/from16 v38, v0

    .line 406
    const-string v2, "[ \t]+"

    move-object/from16 v0, v48

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v38

    .line 407
    move-object/from16 v0, v38

    array-length v2, v0

    new-array v4, v2, [I

    .line 408
    move-object/from16 v0, v38

    array-length v2, v0

    new-array v3, v2, [B
    :try_end_297
    .catch Ljava/io/FileNotFoundException; {:try_start_274 .. :try_end_297} :catch_3c7
    .catch Ljava/io/IOException; {:try_start_274 .. :try_end_297} :catch_456
    .catch Ljava/lang/InterruptedException; {:try_start_274 .. :try_end_297} :catch_4f7

    .line 410
    const/16 v46, 0x0

    .local v46, "t":I
    :goto_299
    :try_start_299
    move-object/from16 v0, v38

    array-length v2, v0

    move/from16 v0, v46

    if-ge v0, v2, :cond_820

    .line 411
    aget-object v2, v38, v46

    const-string v7, "*"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2ba

    aget-object v2, v38, v46

    const-string v7, "**"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2ba

    const/16 v23, 0x1

    const-string v2, "60"

    aput-object v2, v38, v46

    .line 412
    :cond_2ba
    aget-object v2, v38, v46

    const-string v7, "**"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2ce

    aget-object v2, v38, v46

    const-string v7, "\\?+"

    invoke-virtual {v2, v7}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_800

    :cond_2ce
    const-string v2, "60"

    aput-object v2, v38, v46

    const/4 v2, 0x1

    aput v2, v4, v46

    .line 414
    :goto_2d5
    aget-object v2, v38, v46

    const-string v7, "W"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2fd

    aget-object v2, v38, v46

    const-string v7, "w"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2fd

    aget-object v2, v38, v46

    const-string v7, "R"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2fd

    aget-object v2, v38, v46

    const-string v7, "r"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_323

    .line 415
    :cond_2fd
    aget-object v2, v38, v46

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "w"

    const-string v8, ""

    invoke-virtual {v2, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    const-string v7, "r"

    const-string v8, ""

    invoke-virtual {v2, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v37

    .line 416
    .local v37, "or":Ljava/lang/String;
    invoke-static/range {v37 .. v37}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v51

    .local v51, "y":I
    add-int/lit8 v51, v51, 0x2

    .line 417
    aput v51, v4, v46

    const-string v2, "60"

    aput-object v2, v38, v46

    .line 418
    .end local v37    # "or":Ljava/lang/String;
    .end local v51    # "y":I
    :cond_323
    aget-object v2, v38, v46

    const/16 v7, 0x10

    invoke-static {v2, v7}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->byteValue()B

    move-result v2

    aput-byte v2, v3, v46
    :try_end_331
    .catch Ljava/lang/Exception; {:try_start_299 .. :try_end_331} :catch_805
    .catch Ljava/io/FileNotFoundException; {:try_start_299 .. :try_end_331} :catch_3c7
    .catch Ljava/io/IOException; {:try_start_299 .. :try_end_331} :catch_456
    .catch Ljava/lang/InterruptedException; {:try_start_299 .. :try_end_331} :catch_4f7

    .line 410
    add-int/lit8 v46, v46, 0x1

    goto/16 :goto_299

    .line 146
    .end local v46    # "t":I
    :pswitch_335
    :try_start_335
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_128

    .line 147
    invoke-static {}, Lcom/chelpus/root/utils/createapkcustom;->getClassesDex()V

    .line 148
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->classesFiles:Ljava/util/ArrayList;

    if-eqz v2, :cond_3b1

    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->classesFiles:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_3b1

    .line 150
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->classesFiles:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v7, 0x1

    if-le v2, v7, :cond_358

    const/4 v2, 0x1

    sput-boolean v2, Lcom/chelpus/root/utils/createapkcustom;->multidex:Z

    .line 151
    :cond_358
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->classesFiles:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_35e
    :goto_35e
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3b1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/io/File;

    .line 152
    .local v14, "cl":Ljava/io/File;
    sput-object v14, Lcom/chelpus/root/utils/createapkcustom;->localFile2:Ljava/io/File;

    .line 153
    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v8, "---------------------------------"

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 154
    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v52, "Patch for "

    move-object/from16 v0, v52

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v14}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v52

    move-object/from16 v0, v52

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v52, ":"

    move-object/from16 v0, v52

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 155
    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v8, "---------------------------------\n"

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 158
    sget-boolean v7, Lcom/chelpus/root/utils/createapkcustom;->manualpatch:Z

    if-nez v7, :cond_3ac

    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->pat:Ljava/util/ArrayList;

    invoke-static {v7}, Lcom/chelpus/root/utils/createapkcustom;->patchProcess(Ljava/util/ArrayList;)Z

    move-result v42

    .line 159
    :cond_3ac
    if-nez v42, :cond_35e

    const/16 v45, 0x0

    goto :goto_35e

    .line 163
    .end local v14    # "cl":Ljava/io/File;
    :cond_3b1
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/createapkcustom;->multidex:Z

    .line 164
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/createapkcustom;->goodResult:Z

    .line 165
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->ser:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 166
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 167
    const/16 v2, 0xc8

    sput v2, Lcom/chelpus/root/utils/createapkcustom;->tag:I
    :try_end_3c5
    .catch Ljava/io/FileNotFoundException; {:try_start_335 .. :try_end_3c5} :catch_3c7
    .catch Ljava/io/IOException; {:try_start_335 .. :try_end_3c5} :catch_456
    .catch Ljava/lang/InterruptedException; {:try_start_335 .. :try_end_3c5} :catch_4f7

    goto/16 :goto_128

    .line 616
    .end local v3    # "byteOrig":[B
    .end local v4    # "mask":[I
    .end local v11    # "begin":Z
    .end local v13    # "br":Ljava/io/BufferedReader;
    .end local v16    # "data":Ljava/lang/String;
    .end local v25    # "fileInApk":Z
    .end local v27    # "fis":Ljava/io/FileInputStream;
    .end local v28    # "isr":Ljava/io/InputStreamReader;
    .end local v32    # "libr":Z
    .end local v36    # "mark_search":Z
    .end local v38    # "orhex":[Ljava/lang/String;
    .end local v40    # "r":I
    .end local v42    # "result":Z
    .end local v45    # "sumresult":Z
    .end local v47    # "txtdata":[Ljava/lang/String;
    .end local v48    # "value1":Ljava/lang/String;
    .end local v49    # "value2":Ljava/lang/String;
    .end local v50    # "value3":Ljava/lang/String;
    :catch_3c7
    move-exception v19

    .line 618
    .local v19, "e1":Ljava/io/FileNotFoundException;
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v7, "Custom Patch not Found!\n"

    invoke-virtual {v2, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 624
    .end local v19    # "e1":Ljava/io/FileNotFoundException;
    :goto_3cf
    invoke-static {}, Lcom/chelpus/root/utils/createapkcustom;->clearTemp()V

    .line 626
    move-object/from16 v0, v39

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/LogOutputStream;->allresult:Ljava/lang/String;

    move-object/from16 v42, v0

    .line 628
    .local v42, "result":Ljava/lang/String;
    :try_start_3d8
    invoke-virtual/range {v39 .. v39}, Lcom/android/vending/billing/InAppBillingService/LUCK/LogOutputStream;->close()V
    :try_end_3db
    .catch Ljava/io/IOException; {:try_start_3d8 .. :try_end_3db} :catch_e40

    .line 633
    :goto_3db
    return-object v42

    .line 173
    .restart local v3    # "byteOrig":[B
    .restart local v4    # "mask":[I
    .restart local v11    # "begin":Z
    .restart local v13    # "br":Ljava/io/BufferedReader;
    .restart local v16    # "data":Ljava/lang/String;
    .restart local v25    # "fileInApk":Z
    .restart local v27    # "fis":Ljava/io/FileInputStream;
    .restart local v28    # "isr":Ljava/io/InputStreamReader;
    .restart local v32    # "libr":Z
    .restart local v36    # "mark_search":Z
    .restart local v38    # "orhex":[Ljava/lang/String;
    .restart local v40    # "r":I
    .local v42, "result":Z
    .restart local v45    # "sumresult":Z
    .restart local v47    # "txtdata":[Ljava/lang/String;
    .restart local v48    # "value1":Ljava/lang/String;
    .restart local v49    # "value2":Ljava/lang/String;
    .restart local v50    # "value3":Ljava/lang/String;
    :pswitch_3dc
    :try_start_3dc
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v7, "---------------------------"

    invoke-virtual {v2, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 174
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Patch for file from apk\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/chelpus/root/utils/createapkcustom;->localFile2:Ljava/io/File;

    invoke-virtual {v8}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v8

    new-instance v52, Ljava/lang/StringBuilder;

    invoke-direct/range {v52 .. v52}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v53, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual/range {v52 .. v53}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v52

    const-string v53, "/tmp"

    invoke-virtual/range {v52 .. v53}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v52

    invoke-virtual/range {v52 .. v52}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v52

    const-string v53, ""

    move-object/from16 v0, v52

    move-object/from16 v1, v53

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ":"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 175
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v7, "---------------------------\n"

    invoke-virtual {v2, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 177
    sget-boolean v2, Lcom/chelpus/root/utils/createapkcustom;->manualpatch:Z

    if-nez v2, :cond_437

    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->pat:Ljava/util/ArrayList;

    invoke-static {v2}, Lcom/chelpus/root/utils/createapkcustom;->patchProcess(Ljava/util/ArrayList;)Z

    move-result v42

    .line 178
    :cond_437
    if-nez v42, :cond_43b

    const/16 v45, 0x0

    .line 180
    :cond_43b
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->patchedLibs:Ljava/util/ArrayList;

    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->localFile2:Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 182
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->ser:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 183
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 184
    const/16 v2, 0xc8

    sput v2, Lcom/chelpus/root/utils/createapkcustom;->tag:I
    :try_end_454
    .catch Ljava/io/FileNotFoundException; {:try_start_3dc .. :try_end_454} :catch_3c7
    .catch Ljava/io/IOException; {:try_start_3dc .. :try_end_454} :catch_456
    .catch Ljava/lang/InterruptedException; {:try_start_3dc .. :try_end_454} :catch_4f7

    goto/16 :goto_128

    .line 619
    .end local v3    # "byteOrig":[B
    .end local v4    # "mask":[I
    .end local v11    # "begin":Z
    .end local v13    # "br":Ljava/io/BufferedReader;
    .end local v16    # "data":Ljava/lang/String;
    .end local v25    # "fileInApk":Z
    .end local v27    # "fis":Ljava/io/FileInputStream;
    .end local v28    # "isr":Ljava/io/InputStreamReader;
    .end local v32    # "libr":Z
    .end local v36    # "mark_search":Z
    .end local v38    # "orhex":[Ljava/lang/String;
    .end local v40    # "r":I
    .end local v42    # "result":Z
    .end local v45    # "sumresult":Z
    .end local v47    # "txtdata":[Ljava/lang/String;
    .end local v48    # "value1":Ljava/lang/String;
    .end local v49    # "value2":Ljava/lang/String;
    .end local v50    # "value3":Ljava/lang/String;
    :catch_456
    move-exception v20

    .line 620
    .local v20, "e2":Ljava/io/IOException;
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Patch process Error LP: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_3cf

    .line 187
    .end local v20    # "e2":Ljava/io/IOException;
    .restart local v3    # "byteOrig":[B
    .restart local v4    # "mask":[I
    .restart local v11    # "begin":Z
    .restart local v13    # "br":Ljava/io/BufferedReader;
    .restart local v16    # "data":Ljava/lang/String;
    .restart local v25    # "fileInApk":Z
    .restart local v27    # "fis":Ljava/io/FileInputStream;
    .restart local v28    # "isr":Ljava/io/InputStreamReader;
    .restart local v32    # "libr":Z
    .restart local v36    # "mark_search":Z
    .restart local v38    # "orhex":[Ljava/lang/String;
    .restart local v40    # "r":I
    .restart local v42    # "result":Z
    .restart local v45    # "sumresult":Z
    .restart local v47    # "txtdata":[Ljava/lang/String;
    .restart local v48    # "value1":Ljava/lang/String;
    .restart local v49    # "value2":Ljava/lang/String;
    .restart local v50    # "value3":Ljava/lang/String;
    :pswitch_473
    :try_start_473
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->libs:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_479
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_4fd

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Ljava/lang/String;

    .line 188
    .local v31, "lib":Ljava/lang/String;
    new-instance v7, Ljava/io/File;

    move-object/from16 v0, v31

    invoke-direct {v7, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v7, Lcom/chelpus/root/utils/createapkcustom;->localFile2:Ljava/io/File;

    .line 189
    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v8, "---------------------------"

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 190
    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v52, "Patch for libraries \n"

    move-object/from16 v0, v52

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v52, Lcom/chelpus/root/utils/createapkcustom;->localFile2:Ljava/io/File;

    invoke-virtual/range {v52 .. v52}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v52

    new-instance v53, Ljava/lang/StringBuilder;

    invoke-direct/range {v53 .. v53}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v54, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual/range {v53 .. v54}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v53

    const-string v54, "/tmp"

    invoke-virtual/range {v53 .. v54}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v53

    invoke-virtual/range {v53 .. v53}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v53

    const-string v54, ""

    invoke-virtual/range {v52 .. v54}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v52

    move-object/from16 v0, v52

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v52, ":"

    move-object/from16 v0, v52

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 191
    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v8, "---------------------------\n"

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 193
    sget-boolean v7, Lcom/chelpus/root/utils/createapkcustom;->manualpatch:Z

    if-nez v7, :cond_4eb

    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->pat:Ljava/util/ArrayList;

    invoke-static {v7}, Lcom/chelpus/root/utils/createapkcustom;->patchProcess(Ljava/util/ArrayList;)Z

    move-result v42

    .line 194
    :cond_4eb
    if-nez v42, :cond_4ef

    const/16 v45, 0x0

    .line 195
    :cond_4ef
    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->patchedLibs:Ljava/util/ArrayList;

    move-object/from16 v0, v31

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_4f6
    .catch Ljava/io/FileNotFoundException; {:try_start_473 .. :try_end_4f6} :catch_3c7
    .catch Ljava/io/IOException; {:try_start_473 .. :try_end_4f6} :catch_456
    .catch Ljava/lang/InterruptedException; {:try_start_473 .. :try_end_4f6} :catch_4f7

    goto :goto_479

    .line 621
    .end local v3    # "byteOrig":[B
    .end local v4    # "mask":[I
    .end local v11    # "begin":Z
    .end local v13    # "br":Ljava/io/BufferedReader;
    .end local v16    # "data":Ljava/lang/String;
    .end local v25    # "fileInApk":Z
    .end local v27    # "fis":Ljava/io/FileInputStream;
    .end local v28    # "isr":Ljava/io/InputStreamReader;
    .end local v31    # "lib":Ljava/lang/String;
    .end local v32    # "libr":Z
    .end local v36    # "mark_search":Z
    .end local v38    # "orhex":[Ljava/lang/String;
    .end local v40    # "r":I
    .end local v42    # "result":Z
    .end local v45    # "sumresult":Z
    .end local v47    # "txtdata":[Ljava/lang/String;
    .end local v48    # "value1":Ljava/lang/String;
    .end local v49    # "value2":Ljava/lang/String;
    .end local v50    # "value3":Ljava/lang/String;
    :catch_4f7
    move-exception v21

    .line 622
    .local v21, "e3":Ljava/lang/InterruptedException;
    invoke-virtual/range {v21 .. v21}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto/16 :goto_3cf

    .line 198
    .end local v21    # "e3":Ljava/lang/InterruptedException;
    .restart local v3    # "byteOrig":[B
    .restart local v4    # "mask":[I
    .restart local v11    # "begin":Z
    .restart local v13    # "br":Ljava/io/BufferedReader;
    .restart local v16    # "data":Ljava/lang/String;
    .restart local v25    # "fileInApk":Z
    .restart local v27    # "fis":Ljava/io/FileInputStream;
    .restart local v28    # "isr":Ljava/io/InputStreamReader;
    .restart local v32    # "libr":Z
    .restart local v36    # "mark_search":Z
    .restart local v38    # "orhex":[Ljava/lang/String;
    .restart local v40    # "r":I
    .restart local v42    # "result":Z
    .restart local v45    # "sumresult":Z
    .restart local v47    # "txtdata":[Ljava/lang/String;
    .restart local v48    # "value1":Ljava/lang/String;
    .restart local v49    # "value2":Ljava/lang/String;
    .restart local v50    # "value3":Ljava/lang/String;
    :cond_4fd
    const/4 v2, 0x0

    :try_start_4fe
    sput-boolean v2, Lcom/chelpus/root/utils/createapkcustom;->multilib_patch:Z

    .line 199
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/createapkcustom;->goodResult:Z

    .line 200
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->ser:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 201
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 202
    const/16 v2, 0xc8

    sput v2, Lcom/chelpus/root/utils/createapkcustom;->tag:I

    goto/16 :goto_128

    .line 205
    :pswitch_513
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->libs:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_519
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_597

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Ljava/lang/String;

    .line 206
    .restart local v31    # "lib":Ljava/lang/String;
    new-instance v7, Ljava/io/File;

    move-object/from16 v0, v31

    invoke-direct {v7, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v7, Lcom/chelpus/root/utils/createapkcustom;->localFile2:Ljava/io/File;

    .line 207
    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v8, "--------------------------------"

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 208
    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v52, "Patch for (armeabi) libraries \n"

    move-object/from16 v0, v52

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v52, Lcom/chelpus/root/utils/createapkcustom;->localFile2:Ljava/io/File;

    invoke-virtual/range {v52 .. v52}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v52

    new-instance v53, Ljava/lang/StringBuilder;

    invoke-direct/range {v53 .. v53}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v54, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual/range {v53 .. v54}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v53

    const-string v54, "/tmp"

    invoke-virtual/range {v53 .. v54}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v53

    invoke-virtual/range {v53 .. v53}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v53

    const-string v54, ""

    invoke-virtual/range {v52 .. v54}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v52

    move-object/from16 v0, v52

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v52, ":"

    move-object/from16 v0, v52

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 209
    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v8, "--------------------------------\n"

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 211
    sget-boolean v7, Lcom/chelpus/root/utils/createapkcustom;->manualpatch:Z

    if-nez v7, :cond_58b

    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->pat:Ljava/util/ArrayList;

    invoke-static {v7}, Lcom/chelpus/root/utils/createapkcustom;->patchProcess(Ljava/util/ArrayList;)Z

    move-result v42

    .line 212
    :cond_58b
    if-nez v42, :cond_58f

    const/16 v45, 0x0

    .line 213
    :cond_58f
    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->patchedLibs:Ljava/util/ArrayList;

    move-object/from16 v0, v31

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_519

    .line 216
    .end local v31    # "lib":Ljava/lang/String;
    :cond_597
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/createapkcustom;->multilib_patch:Z

    .line 217
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/createapkcustom;->goodResult:Z

    .line 218
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->ser:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 219
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 220
    const/16 v2, 0xc8

    sput v2, Lcom/chelpus/root/utils/createapkcustom;->tag:I

    goto/16 :goto_128

    .line 223
    :pswitch_5ad
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->libs:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_5b3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_631

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Ljava/lang/String;

    .line 224
    .restart local v31    # "lib":Ljava/lang/String;
    new-instance v7, Ljava/io/File;

    move-object/from16 v0, v31

    invoke-direct {v7, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v7, Lcom/chelpus/root/utils/createapkcustom;->localFile2:Ljava/io/File;

    .line 225
    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v8, "---------------------------------------"

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 226
    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v52, "Patch for (armeabi-v7a) libraries \n"

    move-object/from16 v0, v52

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v52, Lcom/chelpus/root/utils/createapkcustom;->localFile2:Ljava/io/File;

    invoke-virtual/range {v52 .. v52}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v52

    new-instance v53, Ljava/lang/StringBuilder;

    invoke-direct/range {v53 .. v53}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v54, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual/range {v53 .. v54}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v53

    const-string v54, "/tmp"

    invoke-virtual/range {v53 .. v54}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v53

    invoke-virtual/range {v53 .. v53}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v53

    const-string v54, ""

    invoke-virtual/range {v52 .. v54}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v52

    move-object/from16 v0, v52

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v52, ":"

    move-object/from16 v0, v52

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 227
    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v8, "---------------------------------------\n"

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 229
    sget-boolean v7, Lcom/chelpus/root/utils/createapkcustom;->manualpatch:Z

    if-nez v7, :cond_625

    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->pat:Ljava/util/ArrayList;

    invoke-static {v7}, Lcom/chelpus/root/utils/createapkcustom;->patchProcess(Ljava/util/ArrayList;)Z

    move-result v42

    .line 230
    :cond_625
    if-nez v42, :cond_629

    const/16 v45, 0x0

    .line 231
    :cond_629
    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->patchedLibs:Ljava/util/ArrayList;

    move-object/from16 v0, v31

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5b3

    .line 234
    .end local v31    # "lib":Ljava/lang/String;
    :cond_631
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/createapkcustom;->multilib_patch:Z

    .line 235
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/createapkcustom;->goodResult:Z

    .line 236
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->ser:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 237
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 238
    const/16 v2, 0xc8

    sput v2, Lcom/chelpus/root/utils/createapkcustom;->tag:I

    goto/16 :goto_128

    .line 241
    :pswitch_647
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->libs:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_64d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_6cb

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Ljava/lang/String;

    .line 242
    .restart local v31    # "lib":Ljava/lang/String;
    new-instance v7, Ljava/io/File;

    move-object/from16 v0, v31

    invoke-direct {v7, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v7, Lcom/chelpus/root/utils/createapkcustom;->localFile2:Ljava/io/File;

    .line 243
    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v8, "---------------------------"

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 244
    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v52, "Patch for (MIPS) libraries \n"

    move-object/from16 v0, v52

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v52, Lcom/chelpus/root/utils/createapkcustom;->localFile2:Ljava/io/File;

    invoke-virtual/range {v52 .. v52}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v52

    new-instance v53, Ljava/lang/StringBuilder;

    invoke-direct/range {v53 .. v53}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v54, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual/range {v53 .. v54}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v53

    const-string v54, "/tmp"

    invoke-virtual/range {v53 .. v54}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v53

    invoke-virtual/range {v53 .. v53}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v53

    const-string v54, ""

    invoke-virtual/range {v52 .. v54}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v52

    move-object/from16 v0, v52

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v52, ":"

    move-object/from16 v0, v52

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 245
    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v8, "---------------------------\n"

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 247
    sget-boolean v7, Lcom/chelpus/root/utils/createapkcustom;->manualpatch:Z

    if-nez v7, :cond_6bf

    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->pat:Ljava/util/ArrayList;

    invoke-static {v7}, Lcom/chelpus/root/utils/createapkcustom;->patchProcess(Ljava/util/ArrayList;)Z

    move-result v42

    .line 248
    :cond_6bf
    if-nez v42, :cond_6c3

    const/16 v45, 0x0

    .line 249
    :cond_6c3
    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->patchedLibs:Ljava/util/ArrayList;

    move-object/from16 v0, v31

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_64d

    .line 252
    .end local v31    # "lib":Ljava/lang/String;
    :cond_6cb
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/createapkcustom;->multilib_patch:Z

    .line 253
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/createapkcustom;->goodResult:Z

    .line 254
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->ser:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 255
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 256
    const/16 v2, 0xc8

    sput v2, Lcom/chelpus/root/utils/createapkcustom;->tag:I

    goto/16 :goto_128

    .line 259
    :pswitch_6e1
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->libs:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_6e7
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_765

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Ljava/lang/String;

    .line 260
    .restart local v31    # "lib":Ljava/lang/String;
    new-instance v7, Ljava/io/File;

    move-object/from16 v0, v31

    invoke-direct {v7, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v7, Lcom/chelpus/root/utils/createapkcustom;->localFile2:Ljava/io/File;

    .line 261
    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v8, "---------------------------"

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 262
    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v52, "Patch for (x86) libraries \n"

    move-object/from16 v0, v52

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v52, Lcom/chelpus/root/utils/createapkcustom;->localFile2:Ljava/io/File;

    invoke-virtual/range {v52 .. v52}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v52

    new-instance v53, Ljava/lang/StringBuilder;

    invoke-direct/range {v53 .. v53}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v54, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual/range {v53 .. v54}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v53

    const-string v54, "/tmp"

    invoke-virtual/range {v53 .. v54}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v53

    invoke-virtual/range {v53 .. v53}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v53

    const-string v54, ""

    invoke-virtual/range {v52 .. v54}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v52

    move-object/from16 v0, v52

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v52, ":"

    move-object/from16 v0, v52

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 263
    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v8, "---------------------------\n"

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 265
    sget-boolean v7, Lcom/chelpus/root/utils/createapkcustom;->manualpatch:Z

    if-nez v7, :cond_759

    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->pat:Ljava/util/ArrayList;

    invoke-static {v7}, Lcom/chelpus/root/utils/createapkcustom;->patchProcess(Ljava/util/ArrayList;)Z

    move-result v42

    .line 266
    :cond_759
    if-nez v42, :cond_75d

    const/16 v45, 0x0

    .line 267
    :cond_75d
    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->patchedLibs:Ljava/util/ArrayList;

    move-object/from16 v0, v31

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_6e7

    .line 270
    .end local v31    # "lib":Ljava/lang/String;
    :cond_765
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/createapkcustom;->multilib_patch:Z

    .line 271
    const/4 v2, 0x0

    sput-boolean v2, Lcom/chelpus/root/utils/createapkcustom;->goodResult:Z

    .line 272
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->ser:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 273
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->pat:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 274
    const/16 v2, 0xc8

    sput v2, Lcom/chelpus/root/utils/createapkcustom;->tag:I

    goto/16 :goto_128

    .line 300
    :catch_77b
    move-exception v18

    .line 301
    .local v18, "e":Lorg/json/JSONException;
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v7, "Error LP: Error name of libraries read!"

    invoke-virtual {v2, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_177

    .line 305
    .end local v18    # "e":Lorg/json/JSONException;
    :pswitch_785
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->libs:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 306
    const/4 v2, 0x4

    move-object/from16 v0, v48

    invoke-static {v2, v0}, Lcom/chelpus/root/utils/createapkcustom;->searchlib(ILjava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    sput-object v2, Lcom/chelpus/root/utils/createapkcustom;->libs:Ljava/util/ArrayList;

    goto/16 :goto_17c

    .line 309
    :pswitch_795
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->libs:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 310
    const/4 v2, 0x0

    move-object/from16 v0, v48

    invoke-static {v2, v0}, Lcom/chelpus/root/utils/createapkcustom;->searchlib(ILjava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    sput-object v2, Lcom/chelpus/root/utils/createapkcustom;->libs:Ljava/util/ArrayList;

    goto/16 :goto_17c

    .line 313
    :pswitch_7a5
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->libs:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 314
    const/4 v2, 0x1

    move-object/from16 v0, v48

    invoke-static {v2, v0}, Lcom/chelpus/root/utils/createapkcustom;->searchlib(ILjava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    sput-object v2, Lcom/chelpus/root/utils/createapkcustom;->libs:Ljava/util/ArrayList;

    goto/16 :goto_17c

    .line 317
    :pswitch_7b5
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->libs:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 318
    const/4 v2, 0x2

    move-object/from16 v0, v48

    invoke-static {v2, v0}, Lcom/chelpus/root/utils/createapkcustom;->searchlib(ILjava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    sput-object v2, Lcom/chelpus/root/utils/createapkcustom;->libs:Ljava/util/ArrayList;

    goto/16 :goto_17c

    .line 321
    :pswitch_7c5
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->libs:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 322
    const/4 v2, 0x3

    move-object/from16 v0, v48

    invoke-static {v2, v0}, Lcom/chelpus/root/utils/createapkcustom;->searchlib(ILjava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    sput-object v2, Lcom/chelpus/root/utils/createapkcustom;->libs:Ljava/util/ArrayList;

    goto/16 :goto_17c

    .line 334
    :catch_7d5
    move-exception v18

    .line 335
    .restart local v18    # "e":Lorg/json/JSONException;
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v7, "Error LP: Error name of file from apk read!"

    invoke-virtual {v2, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_19b

    .line 341
    .end local v18    # "e":Lorg/json/JSONException;
    .restart local v24    # "file":Ljava/lang/String;
    :cond_7df
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v7, "file for patch not found in apk."

    invoke-virtual {v2, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_1b5

    .line 389
    .end local v24    # "file":Ljava/lang/String;
    :catch_7e8
    move-exception v18

    .line 390
    .restart local v18    # "e":Lorg/json/JSONException;
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v7, "Error LP: Error original hex read!"

    invoke-virtual {v2, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 391
    const-string v2, ""

    sput-object v2, Lcom/chelpus/root/utils/createapkcustom;->group:Ljava/lang/String;

    goto/16 :goto_24f

    .line 400
    .end local v18    # "e":Lorg/json/JSONException;
    :catch_7f6
    move-exception v18

    .line 401
    .restart local v18    # "e":Lorg/json/JSONException;
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v7, "Error LP: Error original hex read!"

    invoke-virtual {v2, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_7fe
    .catch Ljava/io/FileNotFoundException; {:try_start_4fe .. :try_end_7fe} :catch_3c7
    .catch Ljava/io/IOException; {:try_start_4fe .. :try_end_7fe} :catch_456
    .catch Ljava/lang/InterruptedException; {:try_start_4fe .. :try_end_7fe} :catch_4f7

    goto/16 :goto_274

    .line 412
    .end local v18    # "e":Lorg/json/JSONException;
    .restart local v46    # "t":I
    :cond_800
    const/4 v2, 0x0

    :try_start_801
    aput v2, v4, v46
    :try_end_803
    .catch Ljava/lang/Exception; {:try_start_801 .. :try_end_803} :catch_805
    .catch Ljava/io/FileNotFoundException; {:try_start_801 .. :try_end_803} :catch_3c7
    .catch Ljava/io/IOException; {:try_start_801 .. :try_end_803} :catch_456
    .catch Ljava/lang/InterruptedException; {:try_start_801 .. :try_end_803} :catch_4f7

    goto/16 :goto_2d5

    .line 420
    :catch_805
    move-exception v18

    .local v18, "e":Ljava/lang/Exception;
    :try_start_806
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 422
    .end local v18    # "e":Ljava/lang/Exception;
    .end local v46    # "t":I
    :cond_820
    aget-object v2, v47, v40

    const-string v7, "\"object\""

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_827
    .catch Ljava/io/FileNotFoundException; {:try_start_806 .. :try_end_827} :catch_3c7
    .catch Ljava/io/IOException; {:try_start_806 .. :try_end_827} :catch_456
    .catch Ljava/lang/InterruptedException; {:try_start_806 .. :try_end_827} :catch_4f7

    move-result v2

    if-eqz v2, :cond_8e3

    .line 425
    :try_start_82a
    new-instance v29, Lorg/json/JSONObject;

    aget-object v2, v47, v40

    move-object/from16 v0, v29

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 426
    .restart local v29    # "json_obj":Lorg/json/JSONObject;
    const-string v2, "object"

    move-object/from16 v0, v29

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_83a
    .catch Lorg/json/JSONException; {:try_start_82a .. :try_end_83a} :catch_998
    .catch Ljava/io/FileNotFoundException; {:try_start_82a .. :try_end_83a} :catch_3c7
    .catch Ljava/io/IOException; {:try_start_82a .. :try_end_83a} :catch_456
    .catch Ljava/lang/InterruptedException; {:try_start_82a .. :try_end_83a} :catch_4f7

    move-result-object v50

    .line 430
    .end local v29    # "json_obj":Lorg/json/JSONObject;
    :goto_83b
    :try_start_83b
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "dalvikvm -Xverify:none -Xdexopt:none -cp "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v7, 0x5

    aget-object v7, p0, v7

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, " "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v7, 0x6

    aget-object v7, p0, v7

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, ".createnerorunpatch "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v7, 0x0

    aget-object v7, p0, v7

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, " "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, "object"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v50

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, " "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, " "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->tooldir:Ljava/lang/String;

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, "\n"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    .line 431
    .local v15, "cmd":Ljava/lang/String;
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v2

    invoke-virtual {v2, v15}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v35

    .line 432
    .local v35, "localProcess9":Ljava/lang/Process;
    invoke-virtual/range {v35 .. v35}, Ljava/lang/Process;->waitFor()I

    .line 433
    new-instance v34, Ljava/io/DataInputStream;

    invoke-virtual/range {v35 .. v35}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    move-object/from16 v0, v34

    invoke-direct {v0, v2}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 434
    .local v34, "localDataInputStream3":Ljava/io/DataInputStream;
    invoke-virtual/range {v34 .. v34}, Ljava/io/DataInputStream;->available()I

    move-result v2

    new-array v10, v2, [B

    .line 435
    .local v10, "arrayOfByte":[B
    move-object/from16 v0, v34

    invoke-virtual {v0, v10}, Ljava/io/DataInputStream;->read([B)I

    .line 436
    new-instance v43, Ljava/lang/String;

    move-object/from16 v0, v43

    invoke-direct {v0, v10}, Ljava/lang/String;-><init>([B)V

    .line 439
    .local v43, "str":Ljava/lang/String;
    invoke-virtual/range {v35 .. v35}, Ljava/lang/Process;->destroy()V

    .line 441
    const-string v2, "Done"

    move-object/from16 v0, v43

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_9a2

    .line 442
    const-string v44, "Object patched!\n\n"

    .line 443
    .local v44, "str2":Ljava/lang/String;
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    move-object/from16 v0, v44

    invoke-virtual {v2, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 444
    const/16 v45, 0x1

    .line 453
    :goto_8db
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->localFile2:Ljava/io/File;

    invoke-static {v2}, Lcom/chelpus/root/utils/createapkcustom;->fixadler(Ljava/io/File;)V

    .line 456
    const/4 v2, 0x1

    sput-boolean v2, Lcom/chelpus/root/utils/createapkcustom;->manualpatch:Z

    .line 460
    .end local v10    # "arrayOfByte":[B
    .end local v15    # "cmd":Ljava/lang/String;
    .end local v34    # "localDataInputStream3":Ljava/io/DataInputStream;
    .end local v35    # "localProcess9":Ljava/lang/Process;
    .end local v43    # "str":Ljava/lang/String;
    .end local v44    # "str2":Ljava/lang/String;
    :cond_8e3
    aget-object v2, v47, v40

    const-string v7, "search"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_8ea
    .catch Ljava/io/FileNotFoundException; {:try_start_83b .. :try_end_8ea} :catch_3c7
    .catch Ljava/io/IOException; {:try_start_83b .. :try_end_8ea} :catch_456
    .catch Ljava/lang/InterruptedException; {:try_start_83b .. :try_end_8ea} :catch_4f7

    move-result v2

    if-eqz v2, :cond_9e3

    .line 463
    :try_start_8ed
    new-instance v29, Lorg/json/JSONObject;

    aget-object v2, v47, v40

    move-object/from16 v0, v29

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 464
    .restart local v29    # "json_obj":Lorg/json/JSONObject;
    const-string v2, "search"

    move-object/from16 v0, v29

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_8fd
    .catch Lorg/json/JSONException; {:try_start_8ed .. :try_end_8fd} :catch_9af
    .catch Ljava/io/FileNotFoundException; {:try_start_8ed .. :try_end_8fd} :catch_3c7
    .catch Ljava/io/IOException; {:try_start_8ed .. :try_end_8fd} :catch_456
    .catch Ljava/lang/InterruptedException; {:try_start_8ed .. :try_end_8fd} :catch_4f7

    move-result-object v50

    .line 468
    .end local v29    # "json_obj":Lorg/json/JSONObject;
    :goto_8fe
    :try_start_8fe
    invoke-virtual/range {v50 .. v50}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v50

    .line 469
    const-string v2, "[ \t]+"

    move-object/from16 v0, v50

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    array-length v2, v2

    new-array v0, v2, [Ljava/lang/String;

    move-object/from16 v38, v0

    .line 470
    const-string v2, "[ \t]+"

    move-object/from16 v0, v50

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v38

    .line 471
    move-object/from16 v0, v38

    array-length v2, v0

    new-array v4, v2, [I

    .line 472
    move-object/from16 v0, v38

    array-length v2, v0

    new-array v3, v2, [B
    :try_end_921
    .catch Ljava/io/FileNotFoundException; {:try_start_8fe .. :try_end_921} :catch_3c7
    .catch Ljava/io/IOException; {:try_start_8fe .. :try_end_921} :catch_456
    .catch Ljava/lang/InterruptedException; {:try_start_8fe .. :try_end_921} :catch_4f7

    .line 474
    const/16 v46, 0x0

    .restart local v46    # "t":I
    :goto_923
    :try_start_923
    move-object/from16 v0, v38

    array-length v2, v0

    move/from16 v0, v46

    if-ge v0, v2, :cond_9d8

    .line 475
    aget-object v2, v38, v46

    const-string v7, "*"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_944

    aget-object v2, v38, v46

    const-string v7, "**"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_944

    const/16 v23, 0x1

    const-string v2, "60"

    aput-object v2, v38, v46

    .line 476
    :cond_944
    aget-object v2, v38, v46

    const-string v7, "**"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_958

    aget-object v2, v38, v46

    const-string v7, "\\?+"

    invoke-virtual {v2, v7}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9b9

    :cond_958
    const-string v2, "60"

    aput-object v2, v38, v46

    const/4 v2, 0x1

    aput v2, v4, v46

    .line 477
    :goto_95f
    aget-object v2, v38, v46

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "R"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_987

    .line 478
    aget-object v2, v38, v46

    const-string v7, "R"

    const-string v8, ""

    invoke-virtual {v2, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v37

    .line 479
    .restart local v37    # "or":Ljava/lang/String;
    invoke-static/range {v37 .. v37}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v51

    .restart local v51    # "y":I
    add-int/lit8 v51, v51, 0x2

    .line 480
    aput v51, v4, v46

    const-string v2, "60"

    aput-object v2, v38, v46

    .line 481
    .end local v37    # "or":Ljava/lang/String;
    .end local v51    # "y":I
    :cond_987
    aget-object v2, v38, v46

    const/16 v7, 0x10

    invoke-static {v2, v7}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->byteValue()B

    move-result v2

    aput-byte v2, v3, v46
    :try_end_995
    .catch Ljava/lang/Exception; {:try_start_923 .. :try_end_995} :catch_9bd
    .catch Ljava/io/FileNotFoundException; {:try_start_923 .. :try_end_995} :catch_3c7
    .catch Ljava/io/IOException; {:try_start_923 .. :try_end_995} :catch_456
    .catch Ljava/lang/InterruptedException; {:try_start_923 .. :try_end_995} :catch_4f7

    .line 474
    add-int/lit8 v46, v46, 0x1

    goto :goto_923

    .line 427
    .end local v46    # "t":I
    :catch_998
    move-exception v18

    .line 428
    .local v18, "e":Lorg/json/JSONException;
    :try_start_999
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v7, "Error LP: Error number by object!"

    invoke-virtual {v2, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_83b

    .line 448
    .end local v18    # "e":Lorg/json/JSONException;
    .restart local v10    # "arrayOfByte":[B
    .restart local v15    # "cmd":Ljava/lang/String;
    .restart local v34    # "localDataInputStream3":Ljava/io/DataInputStream;
    .restart local v35    # "localProcess9":Ljava/lang/Process;
    .restart local v43    # "str":Ljava/lang/String;
    :cond_9a2
    const-string v44, "Object not found!\n\n"

    .line 449
    .restart local v44    # "str2":Ljava/lang/String;
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    move-object/from16 v0, v44

    invoke-virtual {v2, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 450
    const/16 v45, 0x0

    goto/16 :goto_8db

    .line 465
    .end local v10    # "arrayOfByte":[B
    .end local v15    # "cmd":Ljava/lang/String;
    .end local v34    # "localDataInputStream3":Ljava/io/DataInputStream;
    .end local v35    # "localProcess9":Ljava/lang/Process;
    .end local v43    # "str":Ljava/lang/String;
    .end local v44    # "str2":Ljava/lang/String;
    :catch_9af
    move-exception v18

    .line 466
    .restart local v18    # "e":Lorg/json/JSONException;
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v7, "Error LP: Error search hex read!"

    invoke-virtual {v2, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_9b7
    .catch Ljava/io/FileNotFoundException; {:try_start_999 .. :try_end_9b7} :catch_3c7
    .catch Ljava/io/IOException; {:try_start_999 .. :try_end_9b7} :catch_456
    .catch Ljava/lang/InterruptedException; {:try_start_999 .. :try_end_9b7} :catch_4f7

    goto/16 :goto_8fe

    .line 476
    .end local v18    # "e":Lorg/json/JSONException;
    .restart local v46    # "t":I
    :cond_9b9
    const/4 v2, 0x0

    :try_start_9ba
    aput v2, v4, v46
    :try_end_9bc
    .catch Ljava/lang/Exception; {:try_start_9ba .. :try_end_9bc} :catch_9bd
    .catch Ljava/io/FileNotFoundException; {:try_start_9ba .. :try_end_9bc} :catch_3c7
    .catch Ljava/io/IOException; {:try_start_9ba .. :try_end_9bc} :catch_456
    .catch Ljava/lang/InterruptedException; {:try_start_9ba .. :try_end_9bc} :catch_4f7

    goto :goto_95f

    .line 484
    :catch_9bd
    move-exception v18

    .local v18, "e":Ljava/lang/Exception;
    :try_start_9be
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "search pattern read: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 486
    .end local v18    # "e":Ljava/lang/Exception;
    :cond_9d8
    if-eqz v23, :cond_b0d

    const/16 v42, 0x0

    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v7, "Error LP: Patterns to search not valid!\n"

    invoke-virtual {v2, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 494
    .end local v46    # "t":I
    :cond_9e3
    :goto_9e3
    aget-object v2, v47, v40

    const-string v7, "replaced"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_9ea
    .catch Ljava/io/FileNotFoundException; {:try_start_9be .. :try_end_9ea} :catch_3c7
    .catch Ljava/io/IOException; {:try_start_9be .. :try_end_9ea} :catch_456
    .catch Ljava/lang/InterruptedException; {:try_start_9be .. :try_end_9ea} :catch_4f7

    move-result v2

    if-eqz v2, :cond_b9f

    .line 497
    :try_start_9ed
    new-instance v29, Lorg/json/JSONObject;

    aget-object v2, v47, v40

    move-object/from16 v0, v29

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 498
    .restart local v29    # "json_obj":Lorg/json/JSONObject;
    const-string v2, "replaced"

    move-object/from16 v0, v29

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_9fd
    .catch Lorg/json/JSONException; {:try_start_9ed .. :try_end_9fd} :catch_b43
    .catch Ljava/io/FileNotFoundException; {:try_start_9ed .. :try_end_9fd} :catch_3c7
    .catch Ljava/io/IOException; {:try_start_9ed .. :try_end_9fd} :catch_456
    .catch Ljava/lang/InterruptedException; {:try_start_9ed .. :try_end_9fd} :catch_4f7

    move-result-object v49

    .line 502
    .end local v29    # "json_obj":Lorg/json/JSONObject;
    :goto_9fe
    :try_start_9fe
    invoke-virtual/range {v49 .. v49}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v49

    .line 504
    const-string v2, "[ \t]+"

    move-object/from16 v0, v49

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    array-length v2, v2

    new-array v0, v2, [Ljava/lang/String;

    move-object/from16 v41, v0

    .line 505
    .local v41, "rephex":[Ljava/lang/String;
    const-string v2, "[ \t]+"

    move-object/from16 v0, v49

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v41

    .line 506
    move-object/from16 v0, v41

    array-length v2, v0

    new-array v6, v2, [I

    .line 507
    .local v6, "rep_mask":[I
    move-object/from16 v0, v41

    array-length v2, v0

    new-array v5, v2, [B
    :try_end_a21
    .catch Ljava/io/FileNotFoundException; {:try_start_9fe .. :try_end_a21} :catch_3c7
    .catch Ljava/io/IOException; {:try_start_9fe .. :try_end_a21} :catch_456
    .catch Ljava/lang/InterruptedException; {:try_start_9fe .. :try_end_a21} :catch_4f7

    .line 509
    .local v5, "byteReplace":[B
    const/16 v46, 0x0

    .restart local v46    # "t":I
    :goto_a23
    :try_start_a23
    move-object/from16 v0, v41

    array-length v2, v0

    move/from16 v0, v46

    if-ge v0, v2, :cond_b6d

    .line 510
    aget-object v2, v41, v46

    const-string v7, "*"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_a44

    aget-object v2, v41, v46

    const-string v7, "**"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_a44

    const/16 v23, 0x1

    const-string v2, "60"

    aput-object v2, v41, v46

    .line 511
    :cond_a44
    aget-object v2, v41, v46

    const-string v7, "**"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_a58

    aget-object v2, v41, v46

    const-string v7, "\\?+"

    invoke-virtual {v2, v7}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_b4d

    :cond_a58
    const-string v2, "60"

    aput-object v2, v41, v46

    const/4 v2, 0x0

    aput v2, v6, v46

    .line 512
    :goto_a5f
    aget-object v2, v41, v46

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "sq"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_a75

    const-string v2, "60"

    aput-object v2, v41, v46

    const/16 v2, 0xfd

    aput v2, v6, v46

    .line 513
    :cond_a75
    aget-object v2, v41, v46

    const-string v7, "s1"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_a89

    aget-object v2, v41, v46

    const-string v7, "S1"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_a91

    :cond_a89
    const-string v2, "60"

    aput-object v2, v41, v46

    const/16 v2, 0xfe

    aput v2, v6, v46

    .line 514
    :cond_a91
    aget-object v2, v41, v46

    const-string v7, "s0"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_aa5

    aget-object v2, v41, v46

    const-string v7, "S0"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_aad

    :cond_aa5
    const-string v2, "60"

    aput-object v2, v41, v46

    const/16 v2, 0xff

    aput v2, v6, v46

    .line 515
    :cond_aad
    aget-object v2, v41, v46

    const-string v7, "W"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_ad5

    aget-object v2, v41, v46

    const-string v7, "w"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_ad5

    aget-object v2, v41, v46

    const-string v7, "R"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_ad5

    aget-object v2, v41, v46

    const-string v7, "R"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_afb

    .line 516
    :cond_ad5
    aget-object v2, v41, v46

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "w"

    const-string v8, ""

    invoke-virtual {v2, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    const-string v7, "r"

    const-string v8, ""

    invoke-virtual {v2, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v37

    .line 517
    .restart local v37    # "or":Ljava/lang/String;
    invoke-static/range {v37 .. v37}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v51

    .restart local v51    # "y":I
    add-int/lit8 v51, v51, 0x2

    .line 518
    aput v51, v6, v46

    const-string v2, "60"

    aput-object v2, v41, v46

    .line 519
    .end local v37    # "or":Ljava/lang/String;
    .end local v51    # "y":I
    :cond_afb
    aget-object v2, v41, v46

    const/16 v7, 0x10

    invoke-static {v2, v7}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->byteValue()B

    move-result v2

    aput-byte v2, v5, v46
    :try_end_b09
    .catch Ljava/lang/Exception; {:try_start_a23 .. :try_end_b09} :catch_b52
    .catch Ljava/io/FileNotFoundException; {:try_start_a23 .. :try_end_b09} :catch_3c7
    .catch Ljava/io/IOException; {:try_start_a23 .. :try_end_b09} :catch_456
    .catch Ljava/lang/InterruptedException; {:try_start_a23 .. :try_end_b09} :catch_4f7

    .line 509
    add-int/lit8 v46, v46, 0x1

    goto/16 :goto_a23

    .line 487
    .end local v5    # "byteReplace":[B
    .end local v6    # "rep_mask":[I
    .end local v41    # "rephex":[Ljava/lang/String;
    :cond_b0d
    const/16 v36, 0x1

    .line 489
    :try_start_b0f
    new-instance v33, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;

    move-object/from16 v0, v33

    invoke-direct {v0, v3, v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;-><init>([B[I)V

    .line 490
    .local v33, "local":Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;
    array-length v2, v3

    new-array v2, v2, [B

    move-object/from16 v0, v33

    iput-object v2, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->repByte:[B

    .line 491
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->ser:Ljava/util/ArrayList;

    move-object/from16 v0, v33

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_b24
    .catch Ljava/lang/Exception; {:try_start_b0f .. :try_end_b24} :catch_b26
    .catch Ljava/io/FileNotFoundException; {:try_start_b0f .. :try_end_b24} :catch_3c7
    .catch Ljava/io/IOException; {:try_start_b0f .. :try_end_b24} :catch_456
    .catch Ljava/lang/InterruptedException; {:try_start_b0f .. :try_end_b24} :catch_4f7

    goto/16 :goto_9e3

    .line 492
    .end local v33    # "local":Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;
    :catch_b26
    move-exception v18

    .restart local v18    # "e":Ljava/lang/Exception;
    :try_start_b27
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_9e3

    .line 499
    .end local v18    # "e":Ljava/lang/Exception;
    .end local v46    # "t":I
    :catch_b43
    move-exception v18

    .line 500
    .local v18, "e":Lorg/json/JSONException;
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v7, "Error LP: Error replaced hex read!"

    invoke-virtual {v2, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_b4b
    .catch Ljava/io/FileNotFoundException; {:try_start_b27 .. :try_end_b4b} :catch_3c7
    .catch Ljava/io/IOException; {:try_start_b27 .. :try_end_b4b} :catch_456
    .catch Ljava/lang/InterruptedException; {:try_start_b27 .. :try_end_b4b} :catch_4f7

    goto/16 :goto_9fe

    .line 511
    .end local v18    # "e":Lorg/json/JSONException;
    .restart local v5    # "byteReplace":[B
    .restart local v6    # "rep_mask":[I
    .restart local v41    # "rephex":[Ljava/lang/String;
    .restart local v46    # "t":I
    :cond_b4d
    const/4 v2, 0x1

    :try_start_b4e
    aput v2, v6, v46
    :try_end_b50
    .catch Ljava/lang/Exception; {:try_start_b4e .. :try_end_b50} :catch_b52
    .catch Ljava/io/FileNotFoundException; {:try_start_b4e .. :try_end_b50} :catch_3c7
    .catch Ljava/io/IOException; {:try_start_b4e .. :try_end_b50} :catch_456
    .catch Ljava/lang/InterruptedException; {:try_start_b4e .. :try_end_b50} :catch_4f7

    goto/16 :goto_a5f

    .line 521
    :catch_b52
    move-exception v18

    .local v18, "e":Ljava/lang/Exception;
    :try_start_b53
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 522
    .end local v18    # "e":Ljava/lang/Exception;
    :cond_b6d
    array-length v2, v6

    array-length v7, v4

    if-ne v2, v7, :cond_b7d

    array-length v2, v3

    array-length v7, v5

    if-ne v2, v7, :cond_b7d

    array-length v2, v5

    const/4 v7, 0x4

    if-lt v2, v7, :cond_b7d

    array-length v2, v3

    const/4 v7, 0x4

    if-ge v2, v7, :cond_b7f

    :cond_b7d
    const/16 v23, 0x1

    .line 523
    :cond_b7f
    if-eqz v23, :cond_b8a

    const/16 v42, 0x0

    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v7, "Error LP: Patterns original and replaced not valid!\n- Dimensions of the original hex-string and repleced must be >3.\n- Dimensions of the original hex-string and repleced must be equal.\n- Pattern hex must be: AF 11 4B ** AA **\nCheck the template file and try again!"

    invoke-virtual {v2, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 524
    :cond_b8a
    if-nez v23, :cond_b9f

    .line 525
    sget-object v52, Lcom/chelpus/root/utils/createapkcustom;->pat:Ljava/util/ArrayList;

    new-instance v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;

    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->group:Ljava/lang/String;

    const/4 v8, 0x0

    invoke-direct/range {v2 .. v8}, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;-><init>([B[I[B[ILjava/lang/String;Z)V

    move-object/from16 v0, v52

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 526
    const-string v2, ""

    sput-object v2, Lcom/chelpus/root/utils/createapkcustom;->group:Ljava/lang/String;

    .line 530
    .end local v5    # "byteReplace":[B
    .end local v6    # "rep_mask":[I
    .end local v41    # "rephex":[Ljava/lang/String;
    .end local v46    # "t":I
    :cond_b9f
    aget-object v2, v47, v40

    const-string v7, "insert"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_ba6
    .catch Ljava/io/FileNotFoundException; {:try_start_b53 .. :try_end_ba6} :catch_3c7
    .catch Ljava/io/IOException; {:try_start_b53 .. :try_end_ba6} :catch_456
    .catch Ljava/lang/InterruptedException; {:try_start_b53 .. :try_end_ba6} :catch_4f7

    move-result v2

    if-eqz v2, :cond_d1f

    .line 533
    :try_start_ba9
    new-instance v29, Lorg/json/JSONObject;

    aget-object v2, v47, v40

    move-object/from16 v0, v29

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 534
    .restart local v29    # "json_obj":Lorg/json/JSONObject;
    const-string v2, "insert"

    move-object/from16 v0, v29

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_bb9
    .catch Lorg/json/JSONException; {:try_start_ba9 .. :try_end_bb9} :catch_cc9
    .catch Ljava/io/FileNotFoundException; {:try_start_ba9 .. :try_end_bb9} :catch_3c7
    .catch Ljava/io/IOException; {:try_start_ba9 .. :try_end_bb9} :catch_456
    .catch Ljava/lang/InterruptedException; {:try_start_ba9 .. :try_end_bb9} :catch_4f7

    move-result-object v49

    .line 538
    .end local v29    # "json_obj":Lorg/json/JSONObject;
    :goto_bba
    :try_start_bba
    invoke-virtual/range {v49 .. v49}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v49

    .line 540
    const-string v2, "[ \t]+"

    move-object/from16 v0, v49

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    array-length v2, v2

    new-array v0, v2, [Ljava/lang/String;

    move-object/from16 v41, v0

    .line 541
    .restart local v41    # "rephex":[Ljava/lang/String;
    const-string v2, "[ \t]+"

    move-object/from16 v0, v49

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v41

    .line 542
    move-object/from16 v0, v41

    array-length v2, v0

    new-array v6, v2, [I

    .line 543
    .restart local v6    # "rep_mask":[I
    move-object/from16 v0, v41

    array-length v2, v0

    new-array v5, v2, [B
    :try_end_bdd
    .catch Ljava/io/FileNotFoundException; {:try_start_bba .. :try_end_bdd} :catch_3c7
    .catch Ljava/io/IOException; {:try_start_bba .. :try_end_bdd} :catch_456
    .catch Ljava/lang/InterruptedException; {:try_start_bba .. :try_end_bdd} :catch_4f7

    .line 545
    .restart local v5    # "byteReplace":[B
    const/16 v46, 0x0

    .restart local v46    # "t":I
    :goto_bdf
    :try_start_bdf
    move-object/from16 v0, v41

    array-length v2, v0

    move/from16 v0, v46

    if-ge v0, v2, :cond_cef

    .line 546
    aget-object v2, v41, v46

    const-string v7, "*"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_c00

    aget-object v2, v41, v46

    const-string v7, "**"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_c00

    const/16 v23, 0x1

    const-string v2, "60"

    aput-object v2, v41, v46

    .line 547
    :cond_c00
    aget-object v2, v41, v46

    const-string v7, "**"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_c14

    aget-object v2, v41, v46

    const-string v7, "\\?+"

    invoke-virtual {v2, v7}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_cd1

    :cond_c14
    const-string v2, "60"

    aput-object v2, v41, v46

    const/4 v2, 0x0

    aput v2, v6, v46

    .line 548
    :goto_c1b
    aget-object v2, v41, v46

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "sq"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_c31

    const-string v2, "60"

    aput-object v2, v41, v46

    const/16 v2, 0xfd

    aput v2, v6, v46

    .line 549
    :cond_c31
    aget-object v2, v41, v46

    const-string v7, "s1"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_c45

    aget-object v2, v41, v46

    const-string v7, "S1"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_c4d

    :cond_c45
    const-string v2, "60"

    aput-object v2, v41, v46

    const/16 v2, 0xfe

    aput v2, v6, v46

    .line 550
    :cond_c4d
    aget-object v2, v41, v46

    const-string v7, "s0"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_c61

    aget-object v2, v41, v46

    const-string v7, "S0"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_c69

    :cond_c61
    const-string v2, "60"

    aput-object v2, v41, v46

    const/16 v2, 0xff

    aput v2, v6, v46

    .line 551
    :cond_c69
    aget-object v2, v41, v46

    const-string v7, "W"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_c91

    aget-object v2, v41, v46

    const-string v7, "w"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_c91

    aget-object v2, v41, v46

    const-string v7, "R"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_c91

    aget-object v2, v41, v46

    const-string v7, "R"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_cb7

    .line 552
    :cond_c91
    aget-object v2, v41, v46

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v7, "w"

    const-string v8, ""

    invoke-virtual {v2, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    const-string v7, "r"

    const-string v8, ""

    invoke-virtual {v2, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v37

    .line 553
    .restart local v37    # "or":Ljava/lang/String;
    invoke-static/range {v37 .. v37}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v51

    .restart local v51    # "y":I
    add-int/lit8 v51, v51, 0x2

    .line 554
    aput v51, v6, v46

    const-string v2, "60"

    aput-object v2, v41, v46

    .line 555
    .end local v37    # "or":Ljava/lang/String;
    .end local v51    # "y":I
    :cond_cb7
    aget-object v2, v41, v46

    const/16 v7, 0x10

    invoke-static {v2, v7}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->byteValue()B

    move-result v2

    aput-byte v2, v5, v46
    :try_end_cc5
    .catch Ljava/lang/Exception; {:try_start_bdf .. :try_end_cc5} :catch_cd6
    .catch Ljava/io/FileNotFoundException; {:try_start_bdf .. :try_end_cc5} :catch_3c7
    .catch Ljava/io/IOException; {:try_start_bdf .. :try_end_cc5} :catch_456
    .catch Ljava/lang/InterruptedException; {:try_start_bdf .. :try_end_cc5} :catch_4f7

    .line 545
    add-int/lit8 v46, v46, 0x1

    goto/16 :goto_bdf

    .line 535
    .end local v5    # "byteReplace":[B
    .end local v6    # "rep_mask":[I
    .end local v41    # "rephex":[Ljava/lang/String;
    .end local v46    # "t":I
    :catch_cc9
    move-exception v18

    .line 536
    .local v18, "e":Lorg/json/JSONException;
    :try_start_cca
    const-string v2, "Error LP: Error insert hex read!"

    invoke-static {v2}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z
    :try_end_ccf
    .catch Ljava/io/FileNotFoundException; {:try_start_cca .. :try_end_ccf} :catch_3c7
    .catch Ljava/io/IOException; {:try_start_cca .. :try_end_ccf} :catch_456
    .catch Ljava/lang/InterruptedException; {:try_start_cca .. :try_end_ccf} :catch_4f7

    goto/16 :goto_bba

    .line 547
    .end local v18    # "e":Lorg/json/JSONException;
    .restart local v5    # "byteReplace":[B
    .restart local v6    # "rep_mask":[I
    .restart local v41    # "rephex":[Ljava/lang/String;
    .restart local v46    # "t":I
    :cond_cd1
    const/4 v2, 0x1

    :try_start_cd2
    aput v2, v6, v46
    :try_end_cd4
    .catch Ljava/lang/Exception; {:try_start_cd2 .. :try_end_cd4} :catch_cd6
    .catch Ljava/io/FileNotFoundException; {:try_start_cd2 .. :try_end_cd4} :catch_3c7
    .catch Ljava/io/IOException; {:try_start_cd2 .. :try_end_cd4} :catch_456
    .catch Ljava/lang/InterruptedException; {:try_start_cd2 .. :try_end_cd4} :catch_4f7

    goto/16 :goto_c1b

    .line 557
    :catch_cd6
    move-exception v18

    .local v18, "e":Ljava/lang/Exception;
    :try_start_cd7
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 558
    .end local v18    # "e":Ljava/lang/Exception;
    :cond_cef
    array-length v2, v5

    const/4 v7, 0x4

    if-lt v2, v7, :cond_cf7

    array-length v2, v3

    const/4 v7, 0x4

    if-ge v2, v7, :cond_cf9

    :cond_cf7
    const/16 v23, 0x1

    .line 559
    :cond_cf9
    if-eqz v23, :cond_d02

    const/16 v42, 0x0

    const-string v2, "Error LP: Dimensions of the original hex-string and repleced must be >3.\n- Pattern hex must be: AF 11 4B ** AA **\nCheck the template file and try again!"

    invoke-static {v2}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 560
    :cond_d02
    if-nez v23, :cond_d1f

    .line 562
    sget-boolean v2, Lcom/chelpus/root/utils/createapkcustom;->multilib_patch:Z

    if-nez v2, :cond_d0c

    sget-boolean v2, Lcom/chelpus/root/utils/createapkcustom;->multidex:Z

    if-eqz v2, :cond_dda

    :cond_d0c
    sget-object v52, Lcom/chelpus/root/utils/createapkcustom;->pat:Ljava/util/ArrayList;

    new-instance v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;

    const-string v7, "all_lib"

    const/4 v8, 0x1

    invoke-direct/range {v2 .. v8}, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;-><init>([B[I[B[ILjava/lang/String;Z)V

    move-object/from16 v0, v52

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 564
    :goto_d1b
    const-string v2, ""

    sput-object v2, Lcom/chelpus/root/utils/createapkcustom;->group:Ljava/lang/String;

    .line 568
    .end local v5    # "byteReplace":[B
    .end local v6    # "rep_mask":[I
    .end local v41    # "rephex":[Ljava/lang/String;
    .end local v46    # "t":I
    :cond_d1f
    aget-object v2, v47, v40

    const-string v7, "replace_from_file"

    invoke-virtual {v2, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_d26
    .catch Ljava/io/FileNotFoundException; {:try_start_cd7 .. :try_end_d26} :catch_3c7
    .catch Ljava/io/IOException; {:try_start_cd7 .. :try_end_d26} :catch_456
    .catch Ljava/lang/InterruptedException; {:try_start_cd7 .. :try_end_d26} :catch_4f7

    move-result v2

    if-eqz v2, :cond_daa

    .line 571
    :try_start_d29
    new-instance v29, Lorg/json/JSONObject;

    aget-object v2, v47, v40

    move-object/from16 v0, v29

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 572
    .restart local v29    # "json_obj":Lorg/json/JSONObject;
    const-string v2, "replace_from_file"

    move-object/from16 v0, v29

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_d39
    .catch Lorg/json/JSONException; {:try_start_d29 .. :try_end_d39} :catch_deb
    .catch Ljava/io/FileNotFoundException; {:try_start_d29 .. :try_end_d39} :catch_3c7
    .catch Ljava/io/IOException; {:try_start_d29 .. :try_end_d39} :catch_456
    .catch Ljava/lang/InterruptedException; {:try_start_d29 .. :try_end_d39} :catch_4f7

    move-result-object v49

    .line 576
    .end local v29    # "json_obj":Lorg/json/JSONObject;
    :goto_d3a
    :try_start_d3a
    invoke-virtual/range {v49 .. v49}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v49

    .line 578
    new-instance v9, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v7, Ljava/io/File;

    const/4 v8, 0x1

    aget-object v8, p0, v8

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v7}, Lcom/chelpus/Utils;->getDirs(Ljava/io/File;)Ljava/io/File;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, "/"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v49

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v9, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 579
    .local v9, "arrayFile":Ljava/io/File;
    invoke-virtual {v9}, Ljava/io/File;->length()J

    move-result-wide v7

    long-to-int v0, v7

    move/from16 v30, v0

    .line 580
    .local v30, "len":I
    move/from16 v0, v30

    new-array v5, v0, [B
    :try_end_d73
    .catch Ljava/io/FileNotFoundException; {:try_start_d3a .. :try_end_d73} :catch_3c7
    .catch Ljava/io/IOException; {:try_start_d3a .. :try_end_d73} :catch_456
    .catch Ljava/lang/InterruptedException; {:try_start_d3a .. :try_end_d73} :catch_4f7

    .line 584
    .restart local v5    # "byteReplace":[B
    :try_start_d73
    new-instance v17, Ljava/io/FileInputStream;

    move-object/from16 v0, v17

    invoke-direct {v0, v9}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 585
    .local v17, "data3":Ljava/io/FileInputStream;
    :cond_d7a
    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Ljava/io/FileInputStream;->read([B)I
    :try_end_d7f
    .catch Ljava/lang/Exception; {:try_start_d73 .. :try_end_d7f} :catch_df5
    .catch Ljava/io/FileNotFoundException; {:try_start_d73 .. :try_end_d7f} :catch_3c7
    .catch Ljava/io/IOException; {:try_start_d73 .. :try_end_d7f} :catch_456
    .catch Ljava/lang/InterruptedException; {:try_start_d73 .. :try_end_d7f} :catch_4f7

    move-result v2

    if-gtz v2, :cond_d7a

    .line 589
    .end local v17    # "data3":Ljava/io/FileInputStream;
    :goto_d82
    :try_start_d82
    move/from16 v0, v30

    new-array v6, v0, [I

    .line 590
    .restart local v6    # "rep_mask":[I
    const/4 v2, 0x1

    invoke-static {v6, v2}, Ljava/util/Arrays;->fill([II)V

    .line 593
    if-eqz v23, :cond_d95

    const/16 v42, 0x0

    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v7, "Error LP: Patterns original and replaced not valid!\n- Dimensions of the original hex-string and repleced must be >3.\n- Dimensions of the original hex-string and repleced must be equal.\n- Pattern hex must be: AF 11 4B ** AA **\nCheck the template file and try again!"

    invoke-virtual {v2, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 594
    :cond_d95
    if-nez v23, :cond_daa

    .line 595
    sget-object v52, Lcom/chelpus/root/utils/createapkcustom;->pat:Ljava/util/ArrayList;

    new-instance v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;

    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->group:Ljava/lang/String;

    const/4 v8, 0x0

    invoke-direct/range {v2 .. v8}, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;-><init>([B[I[B[ILjava/lang/String;Z)V

    move-object/from16 v0, v52

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 596
    const-string v2, ""

    sput-object v2, Lcom/chelpus/root/utils/createapkcustom;->group:Ljava/lang/String;

    .line 600
    .end local v5    # "byteReplace":[B
    .end local v6    # "rep_mask":[I
    .end local v9    # "arrayFile":Ljava/io/File;
    .end local v30    # "len":I
    :cond_daa
    if-eqz v22, :cond_dc7

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v26

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, "\n"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v7, v47, v40

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    .line 601
    :cond_dc7
    const-string v2, "[END]"

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_dd6

    .line 602
    const/4 v2, 0x4

    sput v2, Lcom/chelpus/root/utils/createapkcustom;->tag:I

    .line 603
    const/16 v22, 0x1

    .line 605
    :cond_dd6
    add-int/lit8 v40, v40, 0x1

    goto/16 :goto_9b

    .line 563
    .restart local v5    # "byteReplace":[B
    .restart local v6    # "rep_mask":[I
    .restart local v41    # "rephex":[Ljava/lang/String;
    .restart local v46    # "t":I
    :cond_dda
    sget-object v52, Lcom/chelpus/root/utils/createapkcustom;->pat:Ljava/util/ArrayList;

    new-instance v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;

    sget-object v7, Lcom/chelpus/root/utils/createapkcustom;->group:Ljava/lang/String;

    const/4 v8, 0x1

    invoke-direct/range {v2 .. v8}, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;-><init>([B[I[B[ILjava/lang/String;Z)V

    move-object/from16 v0, v52

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_d1b

    .line 573
    .end local v5    # "byteReplace":[B
    .end local v6    # "rep_mask":[I
    .end local v41    # "rephex":[Ljava/lang/String;
    .end local v46    # "t":I
    :catch_deb
    move-exception v18

    .line 574
    .local v18, "e":Lorg/json/JSONException;
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v7, "Error LP: Error replaced hex read!"

    invoke-virtual {v2, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_d3a

    .line 586
    .end local v18    # "e":Lorg/json/JSONException;
    .restart local v5    # "byteReplace":[B
    .restart local v9    # "arrayFile":Ljava/io/File;
    .restart local v30    # "len":I
    :catch_df5
    move-exception v18

    .line 587
    .local v18, "e":Ljava/lang/Exception;
    invoke-virtual/range {v18 .. v18}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_d82

    .line 608
    .end local v5    # "byteReplace":[B
    .end local v9    # "arrayFile":Ljava/io/File;
    .end local v18    # "e":Ljava/lang/Exception;
    .end local v30    # "len":I
    :cond_dfa
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->patchedLibs:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_e07

    .line 609
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->patchedLibs:Ljava/util/ArrayList;

    invoke-static {v2}, Lcom/chelpus/root/utils/createapkcustom;->zipLib(Ljava/util/ArrayList;)V

    .line 611
    :cond_e07
    if-eqz v45, :cond_e23

    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v26

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 612
    :cond_e23
    if-nez v45, :cond_e30

    sget-boolean v2, Lcom/chelpus/root/utils/createapkcustom;->patchteil:Z

    if-eqz v2, :cond_e38

    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v7, "Not all patterns are replaced, but the program can work, test it!\nCustom Patch not valid for this Version of the Programm or already patched. "

    invoke-virtual {v2, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 614
    :cond_e30
    :goto_e30
    invoke-static {}, Lcom/chelpus/root/utils/createapkcustom;->clearTemp()V

    .line 615
    invoke-virtual/range {v27 .. v27}, Ljava/io/FileInputStream;->close()V

    goto/16 :goto_3cf

    .line 613
    :cond_e38
    sget-object v2, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v7, "Custom Patch not valid for this Version of the Programm or already patched. "

    invoke-virtual {v2, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_e3f
    .catch Ljava/io/FileNotFoundException; {:try_start_d82 .. :try_end_e3f} :catch_3c7
    .catch Ljava/io/IOException; {:try_start_d82 .. :try_end_e3f} :catch_456
    .catch Ljava/lang/InterruptedException; {:try_start_d82 .. :try_end_e3f} :catch_4f7

    goto :goto_e30

    .line 629
    .end local v3    # "byteOrig":[B
    .end local v4    # "mask":[I
    .end local v11    # "begin":Z
    .end local v13    # "br":Ljava/io/BufferedReader;
    .end local v16    # "data":Ljava/lang/String;
    .end local v25    # "fileInApk":Z
    .end local v27    # "fis":Ljava/io/FileInputStream;
    .end local v28    # "isr":Ljava/io/InputStreamReader;
    .end local v32    # "libr":Z
    .end local v36    # "mark_search":Z
    .end local v38    # "orhex":[Ljava/lang/String;
    .end local v40    # "r":I
    .end local v45    # "sumresult":Z
    .end local v47    # "txtdata":[Ljava/lang/String;
    .end local v48    # "value1":Ljava/lang/String;
    .end local v49    # "value2":Ljava/lang/String;
    .end local v50    # "value3":Ljava/lang/String;
    .local v42, "result":Ljava/lang/String;
    :catch_e40
    move-exception v18

    .line 631
    .local v18, "e":Ljava/io/IOException;
    invoke-virtual/range {v18 .. v18}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_3db

    .line 143
    :pswitch_data_e46
    .packed-switch 0x1
        :pswitch_335
        :pswitch_473
        :pswitch_128
        :pswitch_128
        :pswitch_128
        :pswitch_513
        :pswitch_5ad
        :pswitch_647
        :pswitch_6e1
        :pswitch_3dc
    .end packed-switch

    .line 303
    :pswitch_data_e5e
    .packed-switch 0x2
        :pswitch_785
        :pswitch_17c
        :pswitch_17c
        :pswitch_17c
        :pswitch_795
        :pswitch_7a5
        :pswitch_7b5
        :pswitch_7c5
    .end packed-switch
.end method

.method public static patchProcess(Ljava/util/ArrayList;)Z
    .registers 25
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 638
    .local p0, "patchlist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;>;"
    const/16 v19, 0x1

    .line 641
    .local v19, "patch":Z
    :try_start_2
    new-instance v3, Ljava/io/RandomAccessFile;

    sget-object v4, Lcom/chelpus/root/utils/createapkcustom;->localFile2:Ljava/io/File;

    const-string v5, "rw"

    invoke-direct {v3, v4, v5}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v2

    .line 642
    .local v2, "ChannelDex":Ljava/nio/channels/FileChannel;
    sget-object v3, Ljava/nio/channels/FileChannel$MapMode;->READ_WRITE:Ljava/nio/channels/FileChannel$MapMode;

    const-wide/16 v4, 0x0

    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v6

    long-to-int v6, v6

    int-to-long v6, v6

    invoke-virtual/range {v2 .. v7}, Ljava/nio/channels/FileChannel;->map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;

    move-result-object v13

    .line 644
    .local v13, "fileBytes":Ljava/nio/MappedByteBuffer;
    invoke-virtual/range {p0 .. p0}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v3

    array-length v3, v3

    new-array v0, v3, [Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;

    move-object/from16 v20, v0

    .line 646
    .local v20, "patches":[Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;
    invoke-virtual/range {p0 .. p0}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v0, v3, [Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;

    move-object/from16 v20, v0

    .line 647
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;

    move-object v0, v3

    check-cast v0, [Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;

    move-object/from16 v20, v0
    :try_end_3d
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_3d} :catch_403
    .catch Ljava/nio/BufferUnderflowException; {:try_start_2 .. :try_end_3d} :catch_435
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_3d} :catch_417

    .line 649
    const/4 v11, -0x1

    .line 652
    .local v11, "curentPos":I
    :cond_3e
    :try_start_3e
    invoke-virtual {v13}, Ljava/nio/MappedByteBuffer;->hasRemaining()Z

    move-result v3

    if-eqz v3, :cond_2ab

    .line 654
    add-int/lit8 v3, v11, 0x1

    invoke-virtual {v13, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 655
    invoke-virtual {v13}, Ljava/nio/MappedByteBuffer;->position()I

    move-result v11

    .line 656
    invoke-virtual {v13}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v10

    .line 658
    .local v10, "curentByte":B
    const/4 v14, 0x0

    .local v14, "g":I
    :goto_52
    move-object/from16 v0, v20

    array-length v3, v0

    if-ge v14, v3, :cond_3e

    .line 659
    invoke-virtual {v13, v11}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 661
    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->origByte:[B

    const/4 v4, 0x0

    aget-byte v3, v3, v4

    if-eq v10, v3, :cond_8e

    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->origMask:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    const/4 v4, 0x1

    if-eq v3, v4, :cond_8e

    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->origMask:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    const/4 v4, 0x1

    if-le v3, v4, :cond_267

    sget-object v3, Lcom/chelpus/root/utils/createapkcustom;->search:Ljava/util/ArrayList;

    aget-object v4, v20, v14

    iget-object v4, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->origMask:[I

    const/4 v5, 0x0

    aget v4, v4, v5

    add-int/lit8 v4, v4, -0x2

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Byte;

    invoke-virtual {v3}, Ljava/lang/Byte;->byteValue()B

    move-result v3

    if-ne v10, v3, :cond_267

    .line 663
    :cond_8e
    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repMask:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    if-nez v3, :cond_9e

    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repByte:[B

    const/4 v4, 0x0

    aput-byte v10, v3, v4
    :try_end_9e
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_3e .. :try_end_9e} :catch_2a3
    .catch Ljava/nio/BufferUnderflowException; {:try_start_3e .. :try_end_9e} :catch_32c
    .catch Ljava/lang/Exception; {:try_start_3e .. :try_end_9e} :catch_437
    .catch Ljava/io/FileNotFoundException; {:try_start_3e .. :try_end_9e} :catch_403

    .line 665
    :cond_9e
    :try_start_9e
    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repMask:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    const/4 v4, 0x1

    if-le v3, v4, :cond_cf

    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repMask:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    const/16 v4, 0xfd

    if-ge v3, v4, :cond_cf

    aget-object v3, v20, v14

    iget-object v4, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repByte:[B

    const/4 v5, 0x0

    sget-object v3, Lcom/chelpus/root/utils/createapkcustom;->search:Ljava/util/ArrayList;

    aget-object v6, v20, v14

    iget-object v6, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repMask:[I

    const/4 v7, 0x0

    aget v6, v6, v7

    add-int/lit8 v6, v6, -0x2

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Byte;

    invoke-virtual {v3}, Ljava/lang/Byte;->byteValue()B

    move-result v3

    aput-byte v3, v4, v5
    :try_end_cf
    .catch Ljava/lang/Exception; {:try_start_9e .. :try_end_cf} :catch_26b
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_9e .. :try_end_cf} :catch_2a3
    .catch Ljava/nio/BufferUnderflowException; {:try_start_9e .. :try_end_cf} :catch_32c
    .catch Ljava/io/FileNotFoundException; {:try_start_9e .. :try_end_cf} :catch_403

    .line 667
    :cond_cf
    :goto_cf
    :try_start_cf
    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repMask:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    const/16 v4, 0xfd

    if-ne v3, v4, :cond_e9

    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repByte:[B

    const/4 v4, 0x0

    and-int/lit8 v5, v10, 0xf

    and-int/lit8 v6, v10, 0xf

    mul-int/lit8 v6, v6, 0x10

    add-int/2addr v5, v6

    int-to-byte v5, v5

    aput-byte v5, v3, v4

    .line 668
    :cond_e9
    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repMask:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    const/16 v4, 0xfe

    if-ne v3, v4, :cond_100

    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repByte:[B

    const/4 v4, 0x0

    and-int/lit8 v5, v10, 0xf

    add-int/lit8 v5, v5, 0x10

    int-to-byte v5, v5

    aput-byte v5, v3, v4

    .line 669
    :cond_100
    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repMask:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    const/16 v4, 0xff

    if-ne v3, v4, :cond_115

    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repByte:[B

    const/4 v4, 0x0

    and-int/lit8 v5, v10, 0xf

    int-to-byte v5, v5

    aput-byte v5, v3, v4

    .line 670
    :cond_115
    const/4 v15, 0x1

    .line 671
    .local v15, "i":I
    add-int/lit8 v3, v11, 0x1

    invoke-virtual {v13, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 672
    invoke-virtual {v13}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v21

    .line 674
    .local v21, "prufbyte":B
    :goto_11f
    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->origByte:[B

    aget-byte v3, v3, v15

    move/from16 v0, v21

    if-eq v0, v3, :cond_153

    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->origMask:[I

    aget v3, v3, v15

    const/4 v4, 0x1

    if-le v3, v4, :cond_14a

    sget-object v3, Lcom/chelpus/root/utils/createapkcustom;->search:Ljava/util/ArrayList;

    aget-object v4, v20, v14

    iget-object v4, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->origMask:[I

    aget v4, v4, v15

    add-int/lit8 v4, v4, -0x2

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Byte;

    invoke-virtual {v3}, Ljava/lang/Byte;->byteValue()B

    move-result v3

    move/from16 v0, v21

    if-eq v0, v3, :cond_153

    :cond_14a
    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->origMask:[I

    aget v3, v3, v15
    :try_end_150
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_cf .. :try_end_150} :catch_2a3
    .catch Ljava/nio/BufferUnderflowException; {:try_start_cf .. :try_end_150} :catch_32c
    .catch Ljava/lang/Exception; {:try_start_cf .. :try_end_150} :catch_437
    .catch Ljava/io/FileNotFoundException; {:try_start_cf .. :try_end_150} :catch_403

    const/4 v4, 0x1

    if-ne v3, v4, :cond_267

    .line 677
    :cond_153
    :try_start_153
    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repMask:[I

    aget v3, v3, v15

    if-nez v3, :cond_161

    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repByte:[B

    aput-byte v21, v3, v15

    .line 679
    :cond_161
    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repMask:[I

    aget v3, v3, v15

    const/4 v4, 0x1

    if-le v3, v4, :cond_190

    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repMask:[I

    aget v3, v3, v15

    const/16 v4, 0xfd

    if-ge v3, v4, :cond_190

    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repMask:[I

    aget v3, v3, v15

    add-int/lit8 v23, v3, -0x2

    .local v23, "y":I
    aget-object v3, v20, v14

    iget-object v4, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repByte:[B

    sget-object v3, Lcom/chelpus/root/utils/createapkcustom;->search:Ljava/util/ArrayList;

    move/from16 v0, v23

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Byte;

    invoke-virtual {v3}, Ljava/lang/Byte;->byteValue()B

    move-result v3

    aput-byte v3, v4, v15
    :try_end_190
    .catch Ljava/lang/Exception; {:try_start_153 .. :try_end_190} :catch_2f5
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_153 .. :try_end_190} :catch_2a3
    .catch Ljava/nio/BufferUnderflowException; {:try_start_153 .. :try_end_190} :catch_32c
    .catch Ljava/io/FileNotFoundException; {:try_start_153 .. :try_end_190} :catch_403

    .line 681
    .end local v23    # "y":I
    :cond_190
    :goto_190
    :try_start_190
    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repMask:[I

    aget v3, v3, v15

    const/16 v4, 0xfd

    if-ne v3, v4, :cond_1a8

    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repByte:[B

    and-int/lit8 v4, v10, 0xf

    and-int/lit8 v5, v10, 0xf

    mul-int/lit8 v5, v5, 0x10

    add-int/2addr v4, v5

    int-to-byte v4, v4

    aput-byte v4, v3, v15

    .line 682
    :cond_1a8
    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repMask:[I

    aget v3, v3, v15

    const/16 v4, 0xfe

    if-ne v3, v4, :cond_1bd

    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repByte:[B

    and-int/lit8 v4, v21, 0xf

    add-int/lit8 v4, v4, 0x10

    int-to-byte v4, v4

    aput-byte v4, v3, v15

    .line 683
    :cond_1bd
    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repMask:[I

    aget v3, v3, v15

    const/16 v4, 0xff

    if-ne v3, v4, :cond_1d0

    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repByte:[B

    and-int/lit8 v4, v21, 0xf

    int-to-byte v4, v4

    aput-byte v4, v3, v15

    .line 685
    :cond_1d0
    add-int/lit8 v15, v15, 0x1

    .line 687
    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->origByte:[B

    array-length v3, v3

    if-ne v15, v3, :cond_32f

    .line 689
    aget-object v3, v20, v14

    iget-boolean v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->insert:Z

    if-eqz v3, :cond_21f

    .line 690
    invoke-virtual {v13}, Ljava/nio/MappedByteBuffer;->position()I

    move-result v18

    .line 691
    .local v18, "p":I
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v3

    long-to-int v3, v3

    sub-int v16, v3, v18

    .line 694
    .local v16, "lenght":I
    move/from16 v0, v16

    new-array v8, v0, [B

    .line 695
    .local v8, "buf":[B
    const/4 v3, 0x0

    move/from16 v0, v16

    invoke-virtual {v13, v8, v3, v0}, Ljava/nio/MappedByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 697
    invoke-static {v8}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v9

    .line 698
    .local v9, "buffer":Ljava/nio/ByteBuffer;
    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repByte:[B

    array-length v3, v3

    aget-object v4, v20, v14

    iget-object v4, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->origByte:[B

    array-length v4, v4

    sub-int/2addr v3, v4

    add-int v3, v3, v18

    int-to-long v3, v3

    invoke-virtual {v2, v3, v4}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 699
    invoke-virtual {v2, v9}, Ljava/nio/channels/FileChannel;->write(Ljava/nio/ByteBuffer;)I

    .line 704
    sget-object v3, Ljava/nio/channels/FileChannel$MapMode;->READ_WRITE:Ljava/nio/channels/FileChannel$MapMode;

    const-wide/16 v4, 0x0

    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v6

    long-to-int v6, v6

    int-to-long v6, v6

    invoke-virtual/range {v2 .. v7}, Ljava/nio/channels/FileChannel;->map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;

    move-result-object v13

    .line 705
    move/from16 v0, v18

    invoke-virtual {v13, v0}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 707
    .end local v8    # "buf":[B
    .end local v9    # "buffer":Ljava/nio/ByteBuffer;
    .end local v16    # "lenght":I
    .end local v18    # "p":I
    :cond_21f
    int-to-long v3, v11

    invoke-virtual {v2, v3, v4}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 708
    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repByte:[B

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v9

    .line 709
    .restart local v9    # "buffer":Ljava/nio/ByteBuffer;
    invoke-virtual {v2, v9}, Ljava/nio/channels/FileChannel;->write(Ljava/nio/ByteBuffer;)I

    .line 710
    invoke-virtual {v13}, Ljava/nio/MappedByteBuffer;->force()Ljava/nio/MappedByteBuffer;

    .line 711
    sget-object v3, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "\nPattern N"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    add-int/lit8 v5, v14, 0x1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": Patch done! \n(Offset: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v11}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 712
    aget-object v3, v20, v14

    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->result:Z

    .line 713
    const/4 v3, 0x1

    sput-boolean v3, Lcom/chelpus/root/utils/createapkcustom;->patchteil:Z

    .line 658
    .end local v9    # "buffer":Ljava/nio/ByteBuffer;
    .end local v15    # "i":I
    .end local v21    # "prufbyte":B
    :cond_267
    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_52

    .line 666
    :catch_26b
    move-exception v12

    .local v12, "e":Ljava/lang/Exception;
    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repMask:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    add-int/lit8 v23, v3, -0x2

    .restart local v23    # "y":I
    sget-object v3, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Byte N"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " not found! Please edit search pattern for byte "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_2a1
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_190 .. :try_end_2a1} :catch_2a3
    .catch Ljava/nio/BufferUnderflowException; {:try_start_190 .. :try_end_2a1} :catch_32c
    .catch Ljava/lang/Exception; {:try_start_190 .. :try_end_2a1} :catch_437
    .catch Ljava/io/FileNotFoundException; {:try_start_190 .. :try_end_2a1} :catch_403

    goto/16 :goto_cf

    .line 728
    .end local v10    # "curentByte":B
    .end local v12    # "e":Ljava/lang/Exception;
    .end local v14    # "g":I
    .end local v23    # "y":I
    :catch_2a3
    move-exception v12

    .line 729
    .local v12, "e":Ljava/lang/IndexOutOfBoundsException;
    :try_start_2a4
    sget-object v3, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v4, "Byte by search not found! Please edit pattern for search.\n"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 733
    .end local v12    # "e":Ljava/lang/IndexOutOfBoundsException;
    :cond_2ab
    :goto_2ab
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V

    .line 735
    const/4 v14, 0x0

    .restart local v14    # "g":I
    :goto_2af
    move-object/from16 v0, v20

    array-length v3, v0

    if-ge v14, v3, :cond_40b

    .line 737
    aget-object v3, v20, v14

    iget-boolean v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->result:Z

    if-nez v3, :cond_3ea

    .line 738
    const/16 v22, 0x0

    .line 739
    .local v22, "trueGroup":Z
    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->group:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_335

    .line 740
    const/16 v23, 0x0

    .restart local v23    # "y":I
    :goto_2ca
    move-object/from16 v0, v20

    array-length v3, v0

    move/from16 v0, v23

    if-ge v0, v3, :cond_335

    .line 741
    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->group:Ljava/lang/String;

    aget-object v4, v20, v23

    iget-object v4, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->group:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2f2

    aget-object v3, v20, v23

    iget-boolean v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->result:Z

    if-eqz v3, :cond_2f2

    .line 742
    sget-boolean v3, Lcom/chelpus/root/utils/createapkcustom;->multidex:Z

    if-nez v3, :cond_2ed

    sget-boolean v3, Lcom/chelpus/root/utils/createapkcustom;->multilib_patch:Z

    if-eqz v3, :cond_2f0

    :cond_2ed
    const/4 v3, 0x1

    sput-boolean v3, Lcom/chelpus/root/utils/createapkcustom;->goodResult:Z
    :try_end_2f0
    .catch Ljava/io/FileNotFoundException; {:try_start_2a4 .. :try_end_2f0} :catch_403
    .catch Ljava/nio/BufferUnderflowException; {:try_start_2a4 .. :try_end_2f0} :catch_435
    .catch Ljava/lang/Exception; {:try_start_2a4 .. :try_end_2f0} :catch_417

    .line 743
    :cond_2f0
    const/16 v22, 0x1

    .line 740
    :cond_2f2
    add-int/lit8 v23, v23, 0x1

    goto :goto_2ca

    .line 680
    .end local v22    # "trueGroup":Z
    .end local v23    # "y":I
    .restart local v10    # "curentByte":B
    .restart local v15    # "i":I
    .restart local v21    # "prufbyte":B
    :catch_2f5
    move-exception v12

    .local v12, "e":Ljava/lang/Exception;
    :try_start_2f6
    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->repMask:[I

    aget v3, v3, v15

    add-int/lit8 v23, v3, -0x2

    .restart local v23    # "y":I
    sget-object v3, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Byte N"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " not found! Please edit search pattern for byte "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_190

    .line 730
    .end local v10    # "curentByte":B
    .end local v12    # "e":Ljava/lang/Exception;
    .end local v14    # "g":I
    .end local v15    # "i":I
    .end local v21    # "prufbyte":B
    .end local v23    # "y":I
    :catch_32c
    move-exception v3

    goto/16 :goto_2ab

    .line 720
    .restart local v10    # "curentByte":B
    .restart local v14    # "g":I
    .restart local v15    # "i":I
    .restart local v21    # "prufbyte":B
    :cond_32f
    invoke-virtual {v13}, Ljava/nio/MappedByteBuffer;->get()B
    :try_end_332
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_2f6 .. :try_end_332} :catch_2a3
    .catch Ljava/nio/BufferUnderflowException; {:try_start_2f6 .. :try_end_332} :catch_32c
    .catch Ljava/lang/Exception; {:try_start_2f6 .. :try_end_332} :catch_437
    .catch Ljava/io/FileNotFoundException; {:try_start_2f6 .. :try_end_332} :catch_403

    move-result v21

    goto/16 :goto_11f

    .line 747
    .end local v10    # "curentByte":B
    .end local v15    # "i":I
    .end local v21    # "prufbyte":B
    .restart local v22    # "trueGroup":Z
    :cond_335
    if-nez v22, :cond_37b

    :try_start_337
    sget-boolean v3, Lcom/chelpus/root/utils/createapkcustom;->goodResult:Z

    if-nez v3, :cond_37b

    .line 748
    sget-boolean v3, Lcom/chelpus/root/utils/createapkcustom;->multidex:Z

    if-eqz v3, :cond_37f

    .line 749
    sget-boolean v3, Lcom/chelpus/root/utils/createapkcustom;->goodResult:Z

    if-nez v3, :cond_37b

    sget-object v3, Lcom/chelpus/root/utils/createapkcustom;->localFile2:Ljava/io/File;

    sget-object v4, Lcom/chelpus/root/utils/createapkcustom;->classesFiles:Ljava/util/ArrayList;

    sget-object v5, Lcom/chelpus/root/utils/createapkcustom;->classesFiles:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_37b

    .line 750
    sget-object v3, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "\nPattern N"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    add-int/lit8 v5, v14, 0x1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":\nError LP: Pattern not found!\nor patch is already applied?!\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 751
    const/16 v19, 0x0

    .line 735
    .end local v22    # "trueGroup":Z
    :cond_37b
    :goto_37b
    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_2af

    .line 754
    .restart local v22    # "trueGroup":Z
    :cond_37f
    sget-boolean v3, Lcom/chelpus/root/utils/createapkcustom;->multilib_patch:Z

    if-eqz v3, :cond_3c7

    .line 758
    sget-boolean v3, Lcom/chelpus/root/utils/createapkcustom;->goodResult:Z

    if-nez v3, :cond_37b

    sget-object v4, Lcom/chelpus/root/utils/createapkcustom;->localFile2:Ljava/io/File;

    new-instance v5, Ljava/io/File;

    sget-object v3, Lcom/chelpus/root/utils/createapkcustom;->libs:Ljava/util/ArrayList;

    sget-object v6, Lcom/chelpus/root/utils/createapkcustom;->libs:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-direct {v5, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_37b

    .line 759
    sget-object v3, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "\nPattern N"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    add-int/lit8 v5, v14, 0x1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":\nError LP: Pattern not found!\nor patch is already applied?!\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 760
    const/16 v19, 0x0

    goto :goto_37b

    .line 763
    :cond_3c7
    sget-object v3, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "\nPattern N"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    add-int/lit8 v5, v14, 0x1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":\nError LP: Pattern not found!\nor patch is already applied?!\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 764
    const/16 v19, 0x0

    goto :goto_37b

    .line 770
    .end local v22    # "trueGroup":Z
    :cond_3ea
    sget-boolean v3, Lcom/chelpus/root/utils/createapkcustom;->multidex:Z

    if-nez v3, :cond_3f2

    sget-boolean v3, Lcom/chelpus/root/utils/createapkcustom;->multilib_patch:Z

    if-eqz v3, :cond_37b

    :cond_3f2
    aget-object v3, v20, v14

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;->group:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_37b

    const/4 v3, 0x1

    sput-boolean v3, Lcom/chelpus/root/utils/createapkcustom;->goodResult:Z
    :try_end_401
    .catch Ljava/io/FileNotFoundException; {:try_start_337 .. :try_end_401} :catch_403
    .catch Ljava/nio/BufferUnderflowException; {:try_start_337 .. :try_end_401} :catch_435
    .catch Ljava/lang/Exception; {:try_start_337 .. :try_end_401} :catch_417

    goto/16 :goto_37b

    .line 775
    .end local v2    # "ChannelDex":Ljava/nio/channels/FileChannel;
    .end local v11    # "curentPos":I
    .end local v13    # "fileBytes":Ljava/nio/MappedByteBuffer;
    .end local v14    # "g":I
    .end local v20    # "patches":[Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;
    :catch_403
    move-exception v17

    .line 776
    .local v17, "localFileNotFoundException":Ljava/io/FileNotFoundException;
    sget-object v3, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v4, "Error LP: Program files are not found!\nMove Program to internal storage."

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 782
    .end local v17    # "localFileNotFoundException":Ljava/io/FileNotFoundException;
    :cond_40b
    :goto_40b
    sget v3, Lcom/chelpus/root/utils/createapkcustom;->tag:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_415

    .line 783
    sget-object v3, Lcom/chelpus/root/utils/createapkcustom;->localFile2:Ljava/io/File;

    invoke-static {v3}, Lcom/chelpus/root/utils/createapkcustom;->fixadler(Ljava/io/File;)V

    .line 787
    :cond_415
    const/4 v3, 0x1

    return v3

    .line 779
    :catch_417
    move-exception v12

    .line 780
    .restart local v12    # "e":Ljava/lang/Exception;
    sget-object v3, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception e"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v12}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_40b

    .line 777
    .end local v12    # "e":Ljava/lang/Exception;
    :catch_435
    move-exception v3

    goto :goto_40b

    .line 731
    .restart local v2    # "ChannelDex":Ljava/nio/channels/FileChannel;
    .restart local v11    # "curentPos":I
    .restart local v13    # "fileBytes":Ljava/nio/MappedByteBuffer;
    .restart local v20    # "patches":[Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItem;
    :catch_437
    move-exception v3

    goto/16 :goto_2ab
.end method

.method public static searchProcess(Ljava/util/ArrayList;)Z
    .registers 23
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 792
    .local p0, "searchlist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;>;"
    const/16 v17, 0x1

    .line 794
    .local v17, "patch":Z
    :try_start_2
    new-instance v3, Ljava/io/RandomAccessFile;

    sget-object v4, Lcom/chelpus/root/utils/createapkcustom;->localFile2:Ljava/io/File;

    const-string v5, "rw"

    invoke-direct {v3, v4, v5}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v2

    .line 795
    .local v2, "ChannelDex2":Ljava/nio/channels/FileChannel;
    sget-object v3, Ljava/nio/channels/FileChannel$MapMode;->READ_WRITE:Ljava/nio/channels/FileChannel$MapMode;

    const-wide/16 v4, 0x0

    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v6

    long-to-int v6, v6

    int-to-long v6, v6

    invoke-virtual/range {v2 .. v7}, Ljava/nio/channels/FileChannel;->map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;

    move-result-object v11

    .line 797
    .local v11, "fileBytes2":Ljava/nio/MappedByteBuffer;
    invoke-virtual/range {p0 .. p0}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v3

    array-length v3, v3

    new-array v0, v3, [Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;

    move-object/from16 v18, v0

    .line 799
    .local v18, "patches":[Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;
    invoke-virtual/range {p0 .. p0}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v0, v3, [Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;

    move-object/from16 v18, v0

    .line 800
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;

    move-object v0, v3

    check-cast v0, [Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;

    move-object/from16 v18, v0
    :try_end_3d
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_3d} :catch_1d3
    .catch Ljava/nio/BufferUnderflowException; {:try_start_2 .. :try_end_3d} :catch_1e7
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_3d} :catch_205

    .line 806
    const-wide/16 v14, 0x0

    .local v14, "j":J
    :goto_3f
    :try_start_3f
    invoke-virtual {v11}, Ljava/nio/MappedByteBuffer;->hasRemaining()Z

    move-result v3

    if-eqz v3, :cond_ea

    .line 808
    invoke-virtual {v11}, Ljava/nio/MappedByteBuffer;->position()I

    move-result v9

    .line 809
    .local v9, "curentPos":I
    invoke-virtual {v11}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v8

    .line 811
    .local v8, "curentByte":B
    const/4 v12, 0x0

    .local v12, "g":I
    :goto_4e
    move-object/from16 v0, v18

    array-length v3, v0

    if-ge v12, v3, :cond_c7

    .line 812
    invoke-virtual {v11, v9}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 814
    aget-object v3, v18, v12

    iget-boolean v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->result:Z

    if-nez v3, :cond_bf

    aget-object v3, v18, v12

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->origByte:[B

    const/4 v4, 0x0

    aget-byte v3, v3, v4

    if-eq v8, v3, :cond_6e

    aget-object v3, v18, v12

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->origMask:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    if-eqz v3, :cond_bf

    .line 816
    :cond_6e
    aget-object v3, v18, v12

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->origMask:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    if-eqz v3, :cond_7e

    aget-object v3, v18, v12

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->repByte:[B

    const/4 v4, 0x0

    aput-byte v8, v3, v4

    .line 817
    :cond_7e
    const/4 v13, 0x1

    .line 818
    .local v13, "i":I
    add-int/lit8 v3, v9, 0x1

    invoke-virtual {v11, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 819
    invoke-virtual {v11}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v19

    .line 821
    .local v19, "prufbyte":B
    :goto_88
    aget-object v3, v18, v12

    iget-boolean v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->result:Z

    if-nez v3, :cond_98

    aget-object v3, v18, v12

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->origByte:[B

    aget-byte v3, v3, v13

    move/from16 v0, v19

    if-eq v0, v3, :cond_a0

    :cond_98
    aget-object v3, v18, v12

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->origMask:[I

    aget v3, v3, v13

    if-eqz v3, :cond_bf

    .line 823
    :cond_a0
    aget-object v3, v18, v12

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->origMask:[I

    aget v3, v3, v13

    if-lez v3, :cond_ae

    aget-object v3, v18, v12

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->repByte:[B

    aput-byte v19, v3, v13

    .line 825
    :cond_ae
    add-int/lit8 v13, v13, 0x1

    .line 827
    aget-object v3, v18, v12

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->origByte:[B

    array-length v3, v3

    if-ne v13, v3, :cond_c2

    .line 830
    aget-object v3, v18, v12

    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->result:Z

    .line 831
    const/4 v3, 0x1

    sput-boolean v3, Lcom/chelpus/root/utils/createapkcustom;->patchteil:Z

    .line 811
    .end local v13    # "i":I
    .end local v19    # "prufbyte":B
    :cond_bf
    add-int/lit8 v12, v12, 0x1

    goto :goto_4e

    .line 835
    .restart local v13    # "i":I
    .restart local v19    # "prufbyte":B
    :cond_c2
    invoke-virtual {v11}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v19

    goto :goto_88

    .line 840
    .end local v13    # "i":I
    .end local v19    # "prufbyte":B
    :cond_c7
    add-int/lit8 v3, v9, 0x1

    invoke-virtual {v11, v3}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;
    :try_end_cc
    .catch Ljava/lang/Exception; {:try_start_3f .. :try_end_cc} :catch_d1
    .catch Ljava/io/FileNotFoundException; {:try_start_3f .. :try_end_cc} :catch_1d3
    .catch Ljava/nio/BufferUnderflowException; {:try_start_3f .. :try_end_cc} :catch_1e7

    .line 806
    const-wide/16 v3, 0x1

    add-long/2addr v14, v3

    goto/16 :goto_3f

    .line 843
    .end local v8    # "curentByte":B
    .end local v9    # "curentPos":I
    .end local v12    # "g":I
    :catch_d1
    move-exception v10

    .line 844
    .local v10, "e":Ljava/lang/Exception;
    :try_start_d2
    sget-object v3, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 846
    .end local v10    # "e":Ljava/lang/Exception;
    :cond_ea
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V

    .line 847
    const/4 v12, 0x0

    .restart local v12    # "g":I
    :goto_ee
    move-object/from16 v0, v18

    array-length v3, v0

    if-ge v12, v3, :cond_11e

    .line 849
    aget-object v3, v18, v12

    iget-boolean v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->result:Z

    if-nez v3, :cond_11b

    .line 851
    sget-object v3, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Bytes by serach N"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    add-int/lit8 v5, v12, 0x1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":\nError LP: Bytes not found!"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 852
    const/16 v17, 0x0

    .line 847
    :cond_11b
    add-int/lit8 v12, v12, 0x1

    goto :goto_ee

    .line 855
    :cond_11e
    const/4 v12, 0x0

    :goto_11f
    move-object/from16 v0, v18

    array-length v3, v0

    if-ge v12, v3, :cond_1db

    .line 856
    aget-object v3, v18, v12

    iget-boolean v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->result:Z

    if-eqz v3, :cond_14a

    sget-object v3, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "\nBytes by search N"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    add-int/lit8 v5, v12, 0x1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 858
    :cond_14a
    const/16 v20, 0x0

    .local v20, "w":I
    :goto_14c
    aget-object v3, v18, v12

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->origMask:[I

    array-length v3, v3

    move/from16 v0, v20

    if-ge v0, v3, :cond_1dc

    .line 859
    aget-object v3, v18, v12

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->origMask:[I

    aget v3, v3, v20

    const/4 v4, 0x1

    if-le v3, v4, :cond_1bd

    .line 860
    aget-object v3, v18, v12

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->origMask:[I

    aget v3, v3, v20
    :try_end_164
    .catch Ljava/io/FileNotFoundException; {:try_start_d2 .. :try_end_164} :catch_1d3
    .catch Ljava/nio/BufferUnderflowException; {:try_start_d2 .. :try_end_164} :catch_1e7
    .catch Ljava/lang/Exception; {:try_start_d2 .. :try_end_164} :catch_205

    add-int/lit8 v21, v3, -0x2

    .line 861
    .local v21, "y":I
    :try_start_166
    sget-object v3, Lcom/chelpus/root/utils/createapkcustom;->search:Ljava/util/ArrayList;

    aget-object v4, v18, v12

    iget-object v4, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->repByte:[B

    aget-byte v4, v4, v20

    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v4

    move/from16 v0, v21

    invoke-virtual {v3, v0, v4}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;
    :try_end_177
    .catch Ljava/lang/Exception; {:try_start_166 .. :try_end_177} :catch_1c0
    .catch Ljava/io/FileNotFoundException; {:try_start_166 .. :try_end_177} :catch_1d3
    .catch Ljava/nio/BufferUnderflowException; {:try_start_166 .. :try_end_177} :catch_1e7

    .line 862
    :goto_177
    :try_start_177
    aget-object v3, v18, v12

    iget-boolean v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->result:Z

    if-eqz v3, :cond_1bd

    sget-object v4, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "R"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v21

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v3, Lcom/chelpus/root/utils/createapkcustom;->search:Ljava/util/ArrayList;

    move/from16 v0, v21

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Byte;

    invoke-virtual {v3}, Ljava/lang/Byte;->byteValue()B

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 858
    .end local v21    # "y":I
    :cond_1bd
    add-int/lit8 v20, v20, 0x1

    goto :goto_14c

    .line 861
    .restart local v21    # "y":I
    :catch_1c0
    move-exception v10

    .restart local v10    # "e":Ljava/lang/Exception;
    sget-object v3, Lcom/chelpus/root/utils/createapkcustom;->search:Ljava/util/ArrayList;

    aget-object v4, v18, v12

    iget-object v4, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;->repByte:[B

    aget-byte v4, v4, v20

    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v4

    move/from16 v0, v21

    invoke-virtual {v3, v0, v4}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V
    :try_end_1d2
    .catch Ljava/io/FileNotFoundException; {:try_start_177 .. :try_end_1d2} :catch_1d3
    .catch Ljava/nio/BufferUnderflowException; {:try_start_177 .. :try_end_1d2} :catch_1e7
    .catch Ljava/lang/Exception; {:try_start_177 .. :try_end_1d2} :catch_205

    goto :goto_177

    .line 868
    .end local v2    # "ChannelDex2":Ljava/nio/channels/FileChannel;
    .end local v10    # "e":Ljava/lang/Exception;
    .end local v11    # "fileBytes2":Ljava/nio/MappedByteBuffer;
    .end local v12    # "g":I
    .end local v14    # "j":J
    .end local v18    # "patches":[Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;
    .end local v20    # "w":I
    .end local v21    # "y":I
    :catch_1d3
    move-exception v16

    .line 869
    .local v16, "localFileNotFoundException":Ljava/io/FileNotFoundException;
    sget-object v3, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v4, "Error LP: Program files are not found!\nMove Program to internal storage."

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 876
    .end local v16    # "localFileNotFoundException":Ljava/io/FileNotFoundException;
    :cond_1db
    :goto_1db
    return v17

    .line 865
    .restart local v2    # "ChannelDex2":Ljava/nio/channels/FileChannel;
    .restart local v11    # "fileBytes2":Ljava/nio/MappedByteBuffer;
    .restart local v12    # "g":I
    .restart local v14    # "j":J
    .restart local v18    # "patches":[Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;
    .restart local v20    # "w":I
    :cond_1dc
    :try_start_1dc
    sget-object v3, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_1e3
    .catch Ljava/io/FileNotFoundException; {:try_start_1dc .. :try_end_1e3} :catch_1d3
    .catch Ljava/nio/BufferUnderflowException; {:try_start_1dc .. :try_end_1e3} :catch_1e7
    .catch Ljava/lang/Exception; {:try_start_1dc .. :try_end_1e3} :catch_205

    .line 855
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_11f

    .line 870
    .end local v2    # "ChannelDex2":Ljava/nio/channels/FileChannel;
    .end local v11    # "fileBytes2":Ljava/nio/MappedByteBuffer;
    .end local v12    # "g":I
    .end local v14    # "j":J
    .end local v18    # "patches":[Lcom/android/vending/billing/InAppBillingService/LUCK/SearchItem;
    .end local v20    # "w":I
    :catch_1e7
    move-exception v10

    .line 871
    .local v10, "e":Ljava/nio/BufferUnderflowException;
    sget-object v3, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception e"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v10}, Ljava/nio/BufferUnderflowException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_1db

    .line 872
    .end local v10    # "e":Ljava/nio/BufferUnderflowException;
    :catch_205
    move-exception v10

    .line 873
    .local v10, "e":Ljava/lang/Exception;
    sget-object v3, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception e"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v10}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_1db
.end method

.method public static searchlib(ILjava/lang/String;)Ljava/util/ArrayList;
    .registers 18
    .param p0, "architectura"    # I
    .param p1, "libname"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 905
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 910
    .local v10, "libs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :try_start_5
    new-instance v1, Ljava/io/File;

    sget-object v11, Lcom/chelpus/root/utils/createapkcustom;->appdir:Ljava/lang/String;

    invoke-direct {v1, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 911
    .local v1, "apk":Ljava/io/File;
    new-instance v11, Ljava/io/File;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v13, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "/Modified/"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    sget-object v13, Lcom/chelpus/root/utils/createapkcustom;->packageName:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ".apk"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v11, Lcom/chelpus/root/utils/createapkcustom;->crkapk:Ljava/io/File;

    .line 912
    sget-object v11, Lcom/chelpus/root/utils/createapkcustom;->crkapk:Ljava/io/File;

    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    move-result v11

    if-nez v11, :cond_41

    sget-object v11, Lcom/chelpus/root/utils/createapkcustom;->crkapk:Ljava/io/File;

    invoke-static {v1, v11}, Lcom/chelpus/Utils;->copyFile(Ljava/io/File;Ljava/io/File;)V

    .line 914
    :cond_41
    sget-object v11, Lcom/chelpus/root/utils/createapkcustom;->crkapk:Ljava/io/File;

    invoke-static {v11}, Lcom/chelpus/root/utils/createapkcustom;->extractLibs(Ljava/io/File;)V

    .line 915
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v11

    const-string v12, "*"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_c6

    .line 916
    const/4 v11, 0x1

    sput-boolean v11, Lcom/chelpus/root/utils/createapkcustom;->multilib_patch:Z

    .line 917
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 918
    .local v9, "foundlibs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    new-instance v11, Lcom/chelpus/Utils;

    const-string v12, ""

    invoke-direct {v11, v12}, Lcom/chelpus/Utils;-><init>(Ljava/lang/String;)V

    new-instance v12, Ljava/io/File;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v14, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "/tmp/lib/"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const-string v13, ".so"

    invoke-virtual {v11, v12, v13, v9}, Lcom/chelpus/Utils;->findFileEndText(Ljava/io/File;Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/String;

    .line 919
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v11

    if-lez v11, :cond_c5

    .line 920
    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_8a
    :goto_8a
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_c5

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/io/File;

    .line 922
    .local v8, "file":Ljava/io/File;
    invoke-virtual {v8}, Ljava/io/File;->length()J

    move-result-wide v12

    const-wide/16 v14, 0x0

    cmp-long v12, v12, v14

    if-lez v12, :cond_8a

    invoke-virtual {v8}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_a7
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_a7} :catch_a8
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_a7} :catch_112

    goto :goto_8a

    .line 971
    .end local v1    # "apk":Ljava/io/File;
    .end local v8    # "file":Ljava/io/File;
    .end local v9    # "foundlibs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    :catch_a8
    move-exception v7

    .line 972
    .local v7, "e":Ljava/io/FileNotFoundException;
    sget-object v11, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Lib not found!"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v7}, Ljava/io/FileNotFoundException;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 977
    .end local v7    # "e":Ljava/io/FileNotFoundException;
    :cond_c5
    :goto_c5
    return-object v10

    .line 927
    .restart local v1    # "apk":Ljava/io/File;
    :cond_c6
    packed-switch p0, :pswitch_data_2ca

    goto :goto_c5

    .line 929
    :pswitch_ca
    :try_start_ca
    invoke-virtual {v10}, Ljava/util/ArrayList;->clear()V

    .line 930
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v12, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/tmp/lib/armeabi/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 931
    .local v2, "arh":Ljava/lang/String;
    new-instance v11, Ljava/io/File;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v13, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "/tmp/lib/armeabi/"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    move-result v11

    if-eqz v11, :cond_130

    .line 932
    invoke-virtual {v10, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_111
    .catch Ljava/io/FileNotFoundException; {:try_start_ca .. :try_end_111} :catch_a8
    .catch Ljava/lang/Exception; {:try_start_ca .. :try_end_111} :catch_112

    goto :goto_c5

    .line 974
    .end local v1    # "apk":Ljava/io/File;
    .end local v2    # "arh":Ljava/lang/String;
    :catch_112
    move-exception v7

    .line 975
    .local v7, "e":Ljava/lang/Exception;
    sget-object v11, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Lib select error: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v7}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_c5

    .line 934
    .end local v7    # "e":Ljava/lang/Exception;
    .restart local v1    # "apk":Ljava/io/File;
    .restart local v2    # "arh":Ljava/lang/String;
    :cond_130
    :try_start_130
    new-instance v11, Ljava/io/FileNotFoundException;

    invoke-direct {v11}, Ljava/io/FileNotFoundException;-><init>()V

    throw v11

    .line 936
    .end local v2    # "arh":Ljava/lang/String;
    :pswitch_136
    invoke-virtual {v10}, Ljava/util/ArrayList;->clear()V

    .line 937
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v12, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/tmp/lib/armeabi-v7a/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 938
    .local v3, "arh1":Ljava/lang/String;
    new-instance v11, Ljava/io/File;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v13, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "/tmp/lib/armeabi-v7a/"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    move-result v11

    if-eqz v11, :cond_17f

    .line 939
    invoke-virtual {v10, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_c5

    .line 941
    :cond_17f
    new-instance v11, Ljava/io/FileNotFoundException;

    invoke-direct {v11}, Ljava/io/FileNotFoundException;-><init>()V

    throw v11

    .line 944
    .end local v3    # "arh1":Ljava/lang/String;
    :pswitch_185
    invoke-virtual {v10}, Ljava/util/ArrayList;->clear()V

    .line 945
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v12, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/tmp/lib/mips/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 946
    .local v4, "arh2":Ljava/lang/String;
    new-instance v11, Ljava/io/File;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v13, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "/tmp/lib/mips/"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    move-result v11

    if-eqz v11, :cond_1ce

    .line 947
    invoke-virtual {v10, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_c5

    .line 949
    :cond_1ce
    new-instance v11, Ljava/io/FileNotFoundException;

    invoke-direct {v11}, Ljava/io/FileNotFoundException;-><init>()V

    throw v11

    .line 952
    .end local v4    # "arh2":Ljava/lang/String;
    :pswitch_1d4
    invoke-virtual {v10}, Ljava/util/ArrayList;->clear()V

    .line 953
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v12, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/tmp/lib/x86/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 954
    .local v5, "arh3":Ljava/lang/String;
    new-instance v11, Ljava/io/File;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v13, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "/tmp/lib/x86/"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    move-result v11

    if-eqz v11, :cond_21d

    .line 955
    invoke-virtual {v10, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_c5

    .line 957
    :cond_21d
    new-instance v11, Ljava/io/FileNotFoundException;

    invoke-direct {v11}, Ljava/io/FileNotFoundException;-><init>()V

    throw v11

    .line 960
    .end local v5    # "arh3":Ljava/lang/String;
    :pswitch_223
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v12, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/tmp/lib/armeabi/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 961
    .local v6, "arh4":Ljava/lang/String;
    new-instance v11, Ljava/io/File;

    invoke-direct {v11, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    move-result v11

    if-eqz v11, :cond_24c

    invoke-virtual {v10, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 962
    :cond_24c
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v12, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/tmp/lib/armeabi-v7a/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 963
    new-instance v11, Ljava/io/File;

    invoke-direct {v11, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    move-result v11

    if-eqz v11, :cond_275

    invoke-virtual {v10, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 964
    :cond_275
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v12, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/tmp/lib/mips/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 965
    new-instance v11, Ljava/io/File;

    invoke-direct {v11, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    move-result v11

    if-eqz v11, :cond_29e

    invoke-virtual {v10, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 966
    :cond_29e
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v12, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/tmp/lib/x86/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 967
    new-instance v11, Ljava/io/File;

    invoke-direct {v11, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    move-result v11

    if-eqz v11, :cond_c5

    invoke-virtual {v10, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2c7
    .catch Ljava/io/FileNotFoundException; {:try_start_130 .. :try_end_2c7} :catch_a8
    .catch Ljava/lang/Exception; {:try_start_130 .. :try_end_2c7} :catch_112

    goto/16 :goto_c5

    .line 927
    nop

    :pswitch_data_2ca
    .packed-switch 0x0
        :pswitch_ca
        :pswitch_136
        :pswitch_185
        :pswitch_1d4
        :pswitch_223
    .end packed-switch
.end method

.method public static unzip(Ljava/io/File;)V
    .registers 14
    .param p0, "apk"    # Ljava/io/File;

    .prologue
    .line 1047
    sget-object v9, Lcom/chelpus/root/utils/createapkcustom;->classesFiles:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->clear()V

    .line 1049
    :try_start_5
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 1050
    .local v3, "fin":Ljava/io/FileInputStream;
    new-instance v7, Ljava/util/zip/ZipInputStream;

    invoke-direct {v7, v3}, Ljava/util/zip/ZipInputStream;-><init>(Ljava/io/InputStream;)V

    .line 1051
    .local v7, "zin":Ljava/util/zip/ZipInputStream;
    const/4 v6, 0x0

    .line 1052
    .local v6, "ze":Ljava/util/zip/ZipEntry;
    :cond_10
    :goto_10
    invoke-virtual {v7}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    move-result-object v6

    if-eqz v6, :cond_e5

    .line 1057
    invoke-virtual {v6}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v9

    const-string v10, "classes"

    invoke-virtual {v9, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_10

    invoke-virtual {v6}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v9

    const-string v10, ".dex"

    invoke-virtual {v9, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_10

    invoke-virtual {v6}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v9

    const-string v10, "/"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_10

    .line 1058
    new-instance v4, Ljava/io/FileOutputStream;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v10, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/Modified/"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v6}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v4, v9}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 1060
    .local v4, "fout":Ljava/io/FileOutputStream;
    const/16 v9, 0x400

    new-array v0, v9, [B

    .line 1062
    .local v0, "buffer":[B
    :goto_64
    invoke-virtual {v7, v0}, Ljava/util/zip/ZipInputStream;->read([B)I

    move-result v5

    .local v5, "length":I
    const/4 v9, -0x1

    if-eq v5, v9, :cond_b6

    .line 1063
    const/4 v9, 0x0

    invoke-virtual {v4, v0, v9, v5}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_6f
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_6f} :catch_70

    goto :goto_64

    .line 1077
    .end local v0    # "buffer":[B
    .end local v3    # "fin":Ljava/io/FileInputStream;
    .end local v4    # "fout":Ljava/io/FileOutputStream;
    .end local v5    # "length":I
    .end local v6    # "ze":Ljava/util/zip/ZipEntry;
    .end local v7    # "zin":Ljava/util/zip/ZipInputStream;
    :catch_70
    move-exception v1

    .line 1079
    .local v1, "e":Ljava/lang/Exception;
    :try_start_71
    new-instance v8, Lnet/lingala/zip4j/core/ZipFile;

    invoke-direct {v8, p0}, Lnet/lingala/zip4j/core/ZipFile;-><init>(Ljava/io/File;)V

    .line 1083
    .local v8, "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    const-string v9, "classes.dex"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v11, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "/Modified/"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Lnet/lingala/zip4j/core/ZipFile;->extractFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 1084
    sget-object v9, Lcom/chelpus/root/utils/createapkcustom;->classesFiles:Ljava/util/ArrayList;

    new-instance v10, Ljava/io/File;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v12, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/Modified/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "classes.dex"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_b5
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_71 .. :try_end_b5} :catch_ec
    .catch Ljava/lang/Exception; {:try_start_71 .. :try_end_b5} :catch_122

    .line 1095
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v8    # "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    :goto_b5
    return-void

    .line 1065
    .restart local v0    # "buffer":[B
    .restart local v3    # "fin":Ljava/io/FileInputStream;
    .restart local v4    # "fout":Ljava/io/FileOutputStream;
    .restart local v5    # "length":I
    .restart local v6    # "ze":Ljava/util/zip/ZipEntry;
    .restart local v7    # "zin":Ljava/util/zip/ZipInputStream;
    :cond_b6
    :try_start_b6
    sget-object v9, Lcom/chelpus/root/utils/createapkcustom;->classesFiles:Ljava/util/ArrayList;

    new-instance v10, Ljava/io/File;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v12, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/Modified/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v6}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1066
    invoke-virtual {v7}, Ljava/util/zip/ZipInputStream;->closeEntry()V

    .line 1067
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V

    goto/16 :goto_10

    .line 1075
    .end local v0    # "buffer":[B
    .end local v4    # "fout":Ljava/io/FileOutputStream;
    .end local v5    # "length":I
    :cond_e5
    invoke-virtual {v7}, Ljava/util/zip/ZipInputStream;->close()V

    .line 1076
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_eb
    .catch Ljava/lang/Exception; {:try_start_b6 .. :try_end_eb} :catch_70

    goto :goto_b5

    .line 1086
    .end local v3    # "fin":Ljava/io/FileInputStream;
    .end local v6    # "ze":Ljava/util/zip/ZipEntry;
    .end local v7    # "zin":Ljava/util/zip/ZipInputStream;
    .restart local v1    # "e":Ljava/lang/Exception;
    :catch_ec
    move-exception v2

    .line 1087
    .local v2, "e1":Lnet/lingala/zip4j/exception/ZipException;
    sget-object v9, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Error LP: Error classes.dex decompress! "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1088
    sget-object v9, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Exception e1"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_b5

    .line 1089
    .end local v2    # "e1":Lnet/lingala/zip4j/exception/ZipException;
    :catch_122
    move-exception v2

    .line 1090
    .local v2, "e1":Ljava/lang/Exception;
    sget-object v9, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Error LP: Error classes.dex decompress! "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1091
    sget-object v9, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Exception e1"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_b5
.end method

.method public static zipLib(Ljava/util/ArrayList;)V
    .registers 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1184
    .local p0, "filesIn":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :try_start_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1186
    .local v2, "files":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;>;"
    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_9
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4d

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1187
    .local v1, "file":Ljava/lang/String;
    new-instance v4, Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/chelpus/root/utils/createapkcustom;->sddir:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/tmp/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v1, v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_32
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_32} :catch_33
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_32} :catch_a0

    goto :goto_9

    .line 1194
    .end local v1    # "file":Ljava/lang/String;
    .end local v2    # "files":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;>;"
    :catch_33
    move-exception v0

    .line 1195
    .local v0, "e":Ljava/lang/Exception;
    sget-object v3, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error LP: Error libs compress! "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1201
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_4c
    :goto_4c
    return-void

    .line 1189
    .restart local v2    # "files":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;>;"
    :cond_4d
    :try_start_4d
    sget-object v3, Lcom/chelpus/root/utils/createapkcustom;->crkapk:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/chelpus/root/utils/createapkcustom;->crkapk:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "checlpis.zip"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v2}, Lcom/chelpus/Utils;->addFilesToZip(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1191
    sget-object v3, Lcom/chelpus/root/utils/createapkcustom;->crkapk:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 1192
    sget-object v3, Lcom/chelpus/root/utils/createapkcustom;->crkapk:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_4c

    new-instance v3, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/chelpus/root/utils/createapkcustom;->crkapk:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "checlpis.zip"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sget-object v4, Lcom/chelpus/root/utils/createapkcustom;->crkapk:Ljava/io/File;

    invoke-virtual {v3, v4}, Ljava/io/File;->renameTo(Ljava/io/File;)Z
    :try_end_9f
    .catch Ljava/lang/Exception; {:try_start_4d .. :try_end_9f} :catch_33
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4d .. :try_end_9f} :catch_a0

    goto :goto_4c

    .line 1196
    .end local v2    # "files":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;>;"
    :catch_a0
    move-exception v0

    .line 1197
    .local v0, "e":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v0}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    .line 1198
    sget-object v3, Lcom/chelpus/root/utils/createapkcustom;->print:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error LP: Error libs compress! Out of memory for operation: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_4c
.end method
