.class public Lcom/chelpus/root/utils/copyFile;
.super Ljava/lang/Object;
.source "copyFile.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .registers 8
    .param p0, "paramArrayOfString"    # [Ljava/lang/String;

    .prologue
    .line 10
    new-instance v3, Lcom/chelpus/root/utils/copyFile$1;

    invoke-direct {v3}, Lcom/chelpus/root/utils/copyFile$1;-><init>()V

    invoke-static {v3}, Lcom/chelpus/Utils;->startRootJava(Ljava/lang/Object;)V

    .line 12
    new-instance v2, Ljava/io/File;

    const/4 v3, 0x0

    aget-object v3, p0, v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 13
    .local v2, "source":Ljava/io/File;
    new-instance v0, Ljava/io/File;

    const/4 v3, 0x1

    aget-object v3, p0, v3

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 15
    .local v0, "destination":Ljava/io/File;
    invoke-virtual {v2, v0}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_32

    .line 16
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/io/PrintStream;->println(J)V

    .line 17
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "File copied!"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 18
    invoke-static {}, Lcom/chelpus/Utils;->exitFromRootJava()V

    .line 45
    :goto_31
    return-void

    .line 21
    :cond_32
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v3, v2}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    .line 22
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v3, v0}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    .line 23
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_84

    .line 25
    :try_start_42
    invoke-static {v2, v0}, Lcom/chelpus/Utils;->copyFile(Ljava/io/File;Ljava/io/File;)V
    :try_end_45
    .catch Ljava/lang/IllegalArgumentException; {:try_start_42 .. :try_end_45} :catch_69
    .catch Ljava/lang/Exception; {:try_start_42 .. :try_end_45} :catch_6e

    .line 33
    :goto_45
    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v3

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v5

    cmp-long v3, v3, v5

    if-eqz v3, :cond_73

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-eqz v3, :cond_73

    .line 35
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 36
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "Length of Files not equals. Destination deleted!"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 44
    :goto_65
    invoke-static {}, Lcom/chelpus/Utils;->exitFromRootJava()V

    goto :goto_31

    .line 26
    :catch_69
    move-exception v1

    .line 28
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_45

    .line 29
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    :catch_6e
    move-exception v1

    .line 31
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_45

    .line 38
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_73
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/io/PrintStream;->println(J)V

    .line 39
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "File copied!"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_65

    .line 42
    :cond_84
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "Source File not Found!"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_65
.end method
