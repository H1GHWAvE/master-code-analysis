.class public Lcom/chelpus/root/utils/ClearODEXfiles;
.super Ljava/lang/Object;
.source "ClearODEXfiles.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .registers 18
    .param p0, "paramArrayOfString"    # [Ljava/lang/String;

    .prologue
    .line 13
    const/4 v11, 0x0

    aget-object v10, p0, v11

    .line 14
    .local v10, "toolfilesdir":Ljava/lang/String;
    new-instance v11, Lcom/chelpus/root/utils/ClearODEXfiles$1;

    invoke-direct {v11}, Lcom/chelpus/root/utils/ClearODEXfiles$1;-><init>()V

    invoke-static {v11}, Lcom/chelpus/Utils;->startRootJava(Ljava/lang/Object;)V

    .line 16
    :try_start_b
    new-instance v11, Ljava/io/File;

    const-string v12, "/data/app"

    invoke-direct {v11, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    move-result v11

    if-eqz v11, :cond_9c

    .line 17
    new-instance v11, Ljava/io/File;

    const-string v12, "/data/app"

    invoke-direct {v11, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    .line 18
    .local v3, "files":[Ljava/io/File;
    array-length v13, v3

    const/4 v11, 0x0

    move v12, v11

    :goto_26
    if-ge v12, v13, :cond_9c

    aget-object v9, v3, v12

    .line 19
    .local v9, "tail":Ljava/io/File;
    sget v11, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->api:I

    const/16 v14, 0x15

    if-lt v11, v14, :cond_88

    .line 20
    invoke-virtual {v9}, Ljava/io/File;->isDirectory()Z

    move-result v11

    if-eqz v11, :cond_97

    .line 21
    invoke-virtual {v9}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .line 22
    .local v0, "appFiles":[Ljava/io/File;
    if-eqz v0, :cond_97

    array-length v11, v0

    if-lez v11, :cond_97

    .line 23
    array-length v14, v0

    const/4 v11, 0x0

    :goto_41
    if-ge v11, v14, :cond_97

    aget-object v4, v0, v11

    .line 24
    .local v4, "inFile":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->isFile()Z

    move-result v15

    if-eqz v15, :cond_85

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v15

    const-string v16, ".apk"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_85

    .line 25
    new-instance v6, Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v15

    const/16 v16, 0x1

    invoke-static/range {v15 .. v16}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v15

    invoke-direct {v6, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 27
    .local v6, "odex":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v15

    if-eqz v15, :cond_85

    invoke-virtual {v4}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v15

    invoke-virtual {v15}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v15

    const-string v16, ".apk"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_85

    .line 28
    invoke-static {v4}, Lcom/chelpus/Utils;->classes_test(Ljava/io/File;)Z

    move-result v15

    if-eqz v15, :cond_85

    .line 29
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    .line 23
    .end local v6    # "odex":Ljava/io/File;
    :cond_85
    add-int/lit8 v11, v11, 0x1

    goto :goto_41

    .line 38
    .end local v0    # "appFiles":[Ljava/io/File;
    .end local v4    # "inFile":Ljava/io/File;
    :cond_88
    invoke-virtual {v9}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v11

    const-string v14, ".odex"

    invoke-virtual {v11, v14}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_97

    .line 39
    invoke-virtual {v9}, Ljava/io/File;->delete()Z
    :try_end_97
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_97} :catch_9b

    .line 18
    :cond_97
    add-int/lit8 v11, v12, 0x1

    move v12, v11

    goto :goto_26

    .line 42
    .end local v3    # "files":[Ljava/io/File;
    .end local v9    # "tail":Ljava/io/File;
    :catch_9b
    move-exception v11

    .line 43
    :cond_9c
    sget-object v11, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v12, "LuckyPatcher: Dalvik-Cache deleted."

    invoke-virtual {v11, v12}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 46
    :try_start_a3
    const-string v11, "/system"

    const-string v12, "rw"

    invoke-static {v11, v12}, Lcom/chelpus/Utils;->remount(Ljava/lang/String;Ljava/lang/String;)Z

    .line 47
    new-instance v11, Ljava/io/File;

    const-string v12, "/system/app"

    invoke-direct {v11, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v8

    .line 48
    .local v8, "system_files":[Ljava/io/File;
    array-length v13, v8

    const/4 v11, 0x0

    move v12, v11

    :goto_b8
    if-ge v12, v13, :cond_155

    aget-object v7, v8, v12

    .line 49
    .local v7, "sys_file":Ljava/io/File;
    sget v11, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->api:I

    const/16 v14, 0x15

    if-lt v11, v14, :cond_11a

    .line 50
    invoke-virtual {v7}, Ljava/io/File;->isDirectory()Z

    move-result v11

    if-eqz v11, :cond_150

    .line 51
    invoke-virtual {v7}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .line 52
    .restart local v0    # "appFiles":[Ljava/io/File;
    if-eqz v0, :cond_150

    array-length v11, v0

    if-lez v11, :cond_150

    .line 53
    array-length v14, v0

    const/4 v11, 0x0

    :goto_d3
    if-ge v11, v14, :cond_150

    aget-object v4, v0, v11

    .line 54
    .restart local v4    # "inFile":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->isFile()Z

    move-result v15

    if-eqz v15, :cond_117

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v15

    const-string v16, ".apk"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_117

    .line 55
    new-instance v6, Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v15

    const/16 v16, 0x1

    invoke-static/range {v15 .. v16}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v15

    invoke-direct {v6, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 57
    .restart local v6    # "odex":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v15

    if-eqz v15, :cond_117

    invoke-virtual {v4}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v15

    invoke-virtual {v15}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v15

    const-string v16, ".apk"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_117

    .line 58
    invoke-static {v4}, Lcom/chelpus/Utils;->classes_test(Ljava/io/File;)Z

    move-result v15

    if-eqz v15, :cond_117

    .line 59
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    .line 53
    .end local v6    # "odex":Ljava/io/File;
    :cond_117
    add-int/lit8 v11, v11, 0x1

    goto :goto_d3

    .line 68
    .end local v0    # "appFiles":[Ljava/io/File;
    .end local v4    # "inFile":Ljava/io/File;
    :cond_11a
    new-instance v6, Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v11

    const/4 v14, 0x1

    invoke-static {v11, v14}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v6, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 69
    .restart local v6    # "odex":Ljava/io/File;
    sget-object v11, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v11, v14}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 70
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v11

    if-eqz v11, :cond_150

    invoke-virtual {v7}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v11

    invoke-virtual {v11}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v11

    const-string v14, ".apk"

    invoke-virtual {v11, v14}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_150

    .line 71
    invoke-static {v7}, Lcom/chelpus/Utils;->classes_test(Ljava/io/File;)Z

    move-result v11

    if-eqz v11, :cond_150

    .line 72
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    .line 48
    .end local v6    # "odex":Ljava/io/File;
    :cond_150
    add-int/lit8 v11, v12, 0x1

    move v12, v11

    goto/16 :goto_b8

    .line 77
    .end local v7    # "sys_file":Ljava/io/File;
    :cond_155
    new-instance v11, Ljava/io/File;

    const-string v12, "/system/priv-app"

    invoke-direct {v11, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v8

    .line 78
    array-length v13, v8

    const/4 v11, 0x0

    move v12, v11

    :goto_163
    if-ge v12, v13, :cond_200

    aget-object v7, v8, v12

    .line 79
    .restart local v7    # "sys_file":Ljava/io/File;
    sget v11, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->api:I

    const/16 v14, 0x15

    if-lt v11, v14, :cond_1c5

    .line 80
    invoke-virtual {v7}, Ljava/io/File;->isDirectory()Z

    move-result v11

    if-eqz v11, :cond_1fb

    .line 81
    invoke-virtual {v7}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .line 82
    .restart local v0    # "appFiles":[Ljava/io/File;
    if-eqz v0, :cond_1fb

    array-length v11, v0

    if-lez v11, :cond_1fb

    .line 83
    array-length v14, v0

    const/4 v11, 0x0

    :goto_17e
    if-ge v11, v14, :cond_1fb

    aget-object v4, v0, v11

    .line 84
    .restart local v4    # "inFile":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->isFile()Z

    move-result v15

    if-eqz v15, :cond_1c2

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v15

    const-string v16, ".apk"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_1c2

    .line 85
    new-instance v6, Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v15

    const/16 v16, 0x1

    invoke-static/range {v15 .. v16}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v15

    invoke-direct {v6, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 87
    .restart local v6    # "odex":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v15

    if-eqz v15, :cond_1c2

    invoke-virtual {v4}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v15

    invoke-virtual {v15}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v15

    const-string v16, ".apk"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_1c2

    .line 88
    invoke-static {v4}, Lcom/chelpus/Utils;->classes_test(Ljava/io/File;)Z

    move-result v15

    if-eqz v15, :cond_1c2

    .line 89
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    .line 83
    .end local v6    # "odex":Ljava/io/File;
    :cond_1c2
    add-int/lit8 v11, v11, 0x1

    goto :goto_17e

    .line 98
    .end local v0    # "appFiles":[Ljava/io/File;
    .end local v4    # "inFile":Ljava/io/File;
    :cond_1c5
    new-instance v6, Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v11

    const/4 v14, 0x1

    invoke-static {v11, v14}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v6, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 99
    .restart local v6    # "odex":Ljava/io/File;
    sget-object v11, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v11, v14}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 100
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v11

    if-eqz v11, :cond_1fb

    invoke-virtual {v7}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v11

    invoke-virtual {v11}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v11

    const-string v14, ".apk"

    invoke-virtual {v11, v14}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_1fb

    .line 101
    invoke-static {v7}, Lcom/chelpus/Utils;->classes_test(Ljava/io/File;)Z

    move-result v11

    if-eqz v11, :cond_1fb

    .line 102
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    .line 78
    .end local v6    # "odex":Ljava/io/File;
    :cond_1fb
    add-int/lit8 v11, v12, 0x1

    move v12, v11

    goto/16 :goto_163

    .line 107
    .end local v7    # "sys_file":Ljava/io/File;
    :cond_200
    sget-object v11, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v12, "LuckyPatcher: System apps deodex all."

    invoke-virtual {v11, v12}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_207
    .catch Ljava/lang/Exception; {:try_start_a3 .. :try_end_207} :catch_251

    .line 112
    .end local v8    # "system_files":[Ljava/io/File;
    :goto_207
    :try_start_207
    new-instance v11, Ljava/io/File;

    const-string v12, "/mnt/asec"

    invoke-direct {v11, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v5

    .line 113
    .local v5, "mnt_asec":[Ljava/io/File;
    array-length v13, v5

    const/4 v11, 0x0

    move v12, v11

    :goto_215
    if-ge v12, v13, :cond_24d

    aget-object v1, v5, v12

    .line 114
    .local v1, "dir":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v11

    if-eqz v11, :cond_248

    .line 115
    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    .line 116
    .restart local v3    # "files":[Ljava/io/File;
    array-length v14, v3

    const/4 v11, 0x0

    :goto_225
    if-ge v11, v14, :cond_248

    aget-object v2, v3, v11

    .line 117
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v15

    invoke-virtual {v15}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v15

    const-string v16, ".odex"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_245

    .line 118
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v15

    const-string v16, "rw"

    invoke-static/range {v15 .. v16}, Lcom/chelpus/Utils;->remount(Ljava/lang/String;Ljava/lang/String;)Z

    .line 119
    invoke-virtual {v2}, Ljava/io/File;->delete()Z
    :try_end_245
    .catch Ljava/lang/Exception; {:try_start_207 .. :try_end_245} :catch_24c

    .line 116
    :cond_245
    add-int/lit8 v11, v11, 0x1

    goto :goto_225

    .line 113
    .end local v2    # "file":Ljava/io/File;
    .end local v3    # "files":[Ljava/io/File;
    :cond_248
    add-int/lit8 v11, v12, 0x1

    move v12, v11

    goto :goto_215

    .line 125
    .end local v1    # "dir":Ljava/io/File;
    .end local v5    # "mnt_asec":[Ljava/io/File;
    :catch_24c
    move-exception v11

    .line 127
    :cond_24d
    invoke-static {}, Lcom/chelpus/Utils;->exitFromRootJava()V

    .line 128
    return-void

    .line 108
    :catch_251
    move-exception v11

    goto :goto_207
.end method
