.class public Lcom/chelpus/root/utils/backupdata;
.super Ljava/lang/Object;
.source "backupdata.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .registers 26
    .param p0, "paramArrayOfString"    # [Ljava/lang/String;

    .prologue
    .line 17
    new-instance v21, Lcom/chelpus/root/utils/backupdata$1;

    invoke-direct/range {v21 .. v21}, Lcom/chelpus/root/utils/backupdata$1;-><init>()V

    invoke-static/range {v21 .. v21}, Lcom/chelpus/Utils;->startRootJava(Ljava/lang/Object;)V

    .line 18
    const/16 v21, 0x0

    aget-object v15, p0, v21

    .line 19
    .local v15, "pkg":Ljava/lang/String;
    const/16 v21, 0x1

    aget-object v5, p0, v21

    .line 20
    .local v5, "datadir":Ljava/lang/String;
    const/16 v21, 0x2

    aget-object v2, p0, v21

    .line 21
    .local v2, "backup_data_dir":Ljava/lang/String;
    const/16 v21, 0x3

    aget-object v16, p0, v21

    .line 22
    .local v16, "sd_data_dir":Ljava/lang/String;
    const/4 v10, 0x0

    .line 23
    .local v10, "error":Z
    new-instance v3, Ljava/io/File;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "/data.lpbkp"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 24
    .local v3, "data":Ljava/io/File;
    new-instance v4, Ljava/io/File;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "/data.lpbkp.tmp"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 25
    .local v4, "data2":Ljava/io/File;
    new-instance v7, Ljava/io/File;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "/dbdata.lpbkp"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-direct {v7, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 26
    .local v7, "dbdata":Ljava/io/File;
    new-instance v8, Ljava/io/File;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "/dbdata.lpbkp.tmp"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-direct {v8, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 27
    .local v8, "dbdata2":Ljava/io/File;
    new-instance v17, Ljava/io/File;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "/sddata.lpbkp"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 28
    .local v17, "sddata":Ljava/io/File;
    new-instance v18, Ljava/io/File;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "/sddata.lpbkp.tmp"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 38
    .local v18, "sddata2":Ljava/io/File;
    :try_start_c5
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v21

    if-eqz v21, :cond_ce

    invoke-virtual {v3, v4}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 39
    :cond_ce
    new-instance v19, Lnet/lingala/zip4j/core/ZipFile;

    move-object/from16 v0, v19

    invoke-direct {v0, v3}, Lnet/lingala/zip4j/core/ZipFile;-><init>(Ljava/io/File;)V

    .line 40
    .local v19, "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    new-instance v14, Lnet/lingala/zip4j/model/ZipParameters;

    invoke-direct {v14}, Lnet/lingala/zip4j/model/ZipParameters;-><init>()V

    .line 41
    .local v14, "parameters":Lnet/lingala/zip4j/model/ZipParameters;
    const/16 v21, 0x8

    move/from16 v0, v21

    invoke-virtual {v14, v0}, Lnet/lingala/zip4j/model/ZipParameters;->setCompressionMethod(I)V

    .line 42
    const/16 v21, 0x1

    move/from16 v0, v21

    invoke-virtual {v14, v0}, Lnet/lingala/zip4j/model/ZipParameters;->setCompressionLevel(I)V

    .line 43
    sget-object v21, Ljava/lang/System;->out:Ljava/io/PrintStream;

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 44
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 45
    .local v6, "datadirs":Ljava/io/File;
    sget-object v21, Ljava/lang/System;->out:Ljava/io/PrintStream;

    move-object/from16 v0, v21

    invoke-virtual {v0, v6}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    .line 46
    invoke-virtual {v6}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v13

    .line 47
    .local v13, "folders":[Ljava/io/File;
    if-eqz v13, :cond_1cd

    array-length v0, v13

    move/from16 v21, v0

    if-eqz v21, :cond_1cd

    .line 48
    array-length v0, v13

    move/from16 v22, v0

    const/16 v21, 0x0

    :goto_10b
    move/from16 v0, v21

    move/from16 v1, v22

    if-ge v0, v1, :cond_1cd

    aget-object v11, v13, v21

    .line 49
    .local v11, "file":Ljava/io/File;
    invoke-virtual {v11}, Ljava/io/File;->isDirectory()Z

    move-result v23

    if-eqz v23, :cond_18b

    invoke-virtual {v11}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v23

    const-string v24, "lib"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_122
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_c5 .. :try_end_122} :catch_143
    .catch Ljava/lang/Exception; {:try_start_c5 .. :try_end_122} :catch_1a4

    move-result v23

    if-nez v23, :cond_18b

    .line 51
    :try_start_125
    sget-object v23, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v11}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 52
    move-object/from16 v0, v19

    invoke-virtual {v0, v11, v14}, Lnet/lingala/zip4j/core/ZipFile;->addFolder(Ljava/io/File;Lnet/lingala/zip4j/model/ZipParameters;)V
    :try_end_133
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_125 .. :try_end_133} :catch_136
    .catch Ljava/lang/Exception; {:try_start_125 .. :try_end_133} :catch_1a4

    .line 48
    :cond_133
    :goto_133
    add-int/lit8 v21, v21, 0x1

    goto :goto_10b

    .line 53
    :catch_136
    move-exception v9

    .line 54
    .local v9, "e":Lnet/lingala/zip4j/exception/ZipException;
    :try_start_137
    invoke-virtual {v9}, Lnet/lingala/zip4j/exception/ZipException;->printStackTrace()V

    .line 55
    const/4 v10, 0x1

    .line 56
    sget-object v23, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v24, "error"

    invoke-virtual/range {v23 .. v24}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_142
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_137 .. :try_end_142} :catch_143
    .catch Ljava/lang/Exception; {:try_start_137 .. :try_end_142} :catch_1a4

    goto :goto_133

    .line 111
    .end local v6    # "datadirs":Ljava/io/File;
    .end local v9    # "e":Lnet/lingala/zip4j/exception/ZipException;
    .end local v11    # "file":Ljava/io/File;
    .end local v13    # "folders":[Ljava/io/File;
    .end local v14    # "parameters":Lnet/lingala/zip4j/model/ZipParameters;
    .end local v19    # "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    :catch_143
    move-exception v9

    .line 112
    .restart local v9    # "e":Lnet/lingala/zip4j/exception/ZipException;
    :try_start_144
    invoke-virtual {v9}, Lnet/lingala/zip4j/exception/ZipException;->printStackTrace()V

    .line 113
    const/4 v10, 0x1

    .line 114
    sget-object v21, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v22, "error"

    invoke-virtual/range {v21 .. v22}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_14f
    .catch Ljava/lang/Exception; {:try_start_144 .. :try_end_14f} :catch_1a4

    .line 123
    .end local v9    # "e":Lnet/lingala/zip4j/exception/ZipException;
    :cond_14f
    :goto_14f
    if-nez v10, :cond_164

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v21

    if-nez v21, :cond_164

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v21

    if-nez v21, :cond_164

    .line 124
    sget-object v21, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v22, "empty data..."

    invoke-virtual/range {v21 .. v22}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 126
    :cond_164
    if-nez v10, :cond_2cd

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v21

    if-nez v21, :cond_2cd

    .line 127
    invoke-virtual {v4, v3}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 131
    :goto_16f
    if-nez v10, :cond_2d2

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v21

    if-nez v21, :cond_2d2

    .line 132
    invoke-virtual {v8, v7}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 136
    :goto_17a
    if-nez v10, :cond_2d7

    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->exists()Z

    move-result v21

    if-nez v21, :cond_2d7

    .line 137
    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 141
    :goto_187
    invoke-static {}, Lcom/chelpus/Utils;->exitFromRootJava()V

    .line 142
    return-void

    .line 58
    .restart local v6    # "datadirs":Ljava/io/File;
    .restart local v11    # "file":Ljava/io/File;
    .restart local v13    # "folders":[Ljava/io/File;
    .restart local v14    # "parameters":Lnet/lingala/zip4j/model/ZipParameters;
    .restart local v19    # "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    :cond_18b
    :try_start_18b
    invoke-virtual {v11}, Ljava/io/File;->isFile()Z
    :try_end_18e
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_18b .. :try_end_18e} :catch_143
    .catch Ljava/lang/Exception; {:try_start_18b .. :try_end_18e} :catch_1a4

    move-result v23

    if-eqz v23, :cond_133

    :try_start_191
    move-object/from16 v0, v19

    invoke-virtual {v0, v11, v14}, Lnet/lingala/zip4j/core/ZipFile;->addFile(Ljava/io/File;Lnet/lingala/zip4j/model/ZipParameters;)V
    :try_end_196
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_191 .. :try_end_196} :catch_197
    .catch Ljava/lang/Exception; {:try_start_191 .. :try_end_196} :catch_1a4

    goto :goto_133

    :catch_197
    move-exception v9

    .line 59
    .restart local v9    # "e":Lnet/lingala/zip4j/exception/ZipException;
    :try_start_198
    invoke-virtual {v9}, Lnet/lingala/zip4j/exception/ZipException;->printStackTrace()V

    .line 60
    const/4 v10, 0x1

    .line 61
    sget-object v23, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v24, "error"

    invoke-virtual/range {v23 .. v24}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_1a3
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_198 .. :try_end_1a3} :catch_143
    .catch Ljava/lang/Exception; {:try_start_198 .. :try_end_1a3} :catch_1a4

    goto :goto_133

    .line 117
    .end local v6    # "datadirs":Ljava/io/File;
    .end local v9    # "e":Lnet/lingala/zip4j/exception/ZipException;
    .end local v11    # "file":Ljava/io/File;
    .end local v13    # "folders":[Ljava/io/File;
    .end local v14    # "parameters":Lnet/lingala/zip4j/model/ZipParameters;
    .end local v19    # "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    :catch_1a4
    move-exception v9

    .line 118
    .local v9, "e":Ljava/lang/Exception;
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V

    .line 119
    sget-object v21, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Exception e"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual {v9}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 120
    const/4 v10, 0x1

    .line 121
    sget-object v21, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v22, "error"

    invoke-virtual/range {v21 .. v22}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_14f

    .line 65
    .end local v9    # "e":Ljava/lang/Exception;
    .restart local v6    # "datadirs":Ljava/io/File;
    .restart local v13    # "folders":[Ljava/io/File;
    .restart local v14    # "parameters":Lnet/lingala/zip4j/model/ZipParameters;
    .restart local v19    # "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    :cond_1cd
    :try_start_1cd
    new-instance v21, Ljava/io/File;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "/dbdata/databases/"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-direct/range {v21 .. v22}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->exists()Z

    move-result v21

    if-eqz v21, :cond_25d

    .line 67
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v21

    if-eqz v21, :cond_1f6

    invoke-virtual {v7, v8}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 68
    :cond_1f6
    new-instance v20, Lnet/lingala/zip4j/core/ZipFile;

    move-object/from16 v0, v20

    invoke-direct {v0, v7}, Lnet/lingala/zip4j/core/ZipFile;-><init>(Ljava/io/File;)V

    .line 69
    .local v20, "zipFile2":Lnet/lingala/zip4j/core/ZipFile;
    new-instance v21, Ljava/io/File;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "/dbdata/databases/"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-direct/range {v21 .. v22}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v12

    .line 70
    .local v12, "files":[Ljava/io/File;
    if-eqz v13, :cond_25d

    array-length v0, v13

    move/from16 v21, v0

    if-eqz v21, :cond_25d

    .line 71
    array-length v0, v12

    move/from16 v22, v0

    const/16 v21, 0x0

    :goto_227
    move/from16 v0, v21

    move/from16 v1, v22

    if-ge v0, v1, :cond_25d

    aget-object v11, v12, v21

    .line 72
    .restart local v11    # "file":Ljava/io/File;
    invoke-virtual {v11}, Ljava/io/File;->isDirectory()Z
    :try_end_232
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_1cd .. :try_end_232} :catch_143
    .catch Ljava/lang/Exception; {:try_start_1cd .. :try_end_232} :catch_1a4

    move-result v23

    if-eqz v23, :cond_24a

    .line 74
    :try_start_235
    move-object/from16 v0, v20

    invoke-virtual {v0, v11, v14}, Lnet/lingala/zip4j/core/ZipFile;->addFolder(Ljava/io/File;Lnet/lingala/zip4j/model/ZipParameters;)V
    :try_end_23a
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_235 .. :try_end_23a} :catch_23d
    .catch Ljava/lang/Exception; {:try_start_235 .. :try_end_23a} :catch_1a4

    .line 71
    :goto_23a
    add-int/lit8 v21, v21, 0x1

    goto :goto_227

    .line 75
    :catch_23d
    move-exception v9

    .line 76
    .local v9, "e":Lnet/lingala/zip4j/exception/ZipException;
    :try_start_23e
    invoke-virtual {v9}, Lnet/lingala/zip4j/exception/ZipException;->printStackTrace()V

    .line 77
    const/4 v10, 0x1

    .line 78
    sget-object v23, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v24, "error"

    invoke-virtual/range {v23 .. v24}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_249
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_23e .. :try_end_249} :catch_143
    .catch Ljava/lang/Exception; {:try_start_23e .. :try_end_249} :catch_1a4

    goto :goto_23a

    .line 80
    .end local v9    # "e":Lnet/lingala/zip4j/exception/ZipException;
    :cond_24a
    :try_start_24a
    move-object/from16 v0, v20

    invoke-virtual {v0, v11, v14}, Lnet/lingala/zip4j/core/ZipFile;->addFile(Ljava/io/File;Lnet/lingala/zip4j/model/ZipParameters;)V
    :try_end_24f
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_24a .. :try_end_24f} :catch_250
    .catch Ljava/lang/Exception; {:try_start_24a .. :try_end_24f} :catch_1a4

    goto :goto_23a

    :catch_250
    move-exception v9

    .line 81
    .restart local v9    # "e":Lnet/lingala/zip4j/exception/ZipException;
    :try_start_251
    invoke-virtual {v9}, Lnet/lingala/zip4j/exception/ZipException;->printStackTrace()V

    .line 82
    const/4 v10, 0x1

    .line 83
    sget-object v23, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v24, "error"

    invoke-virtual/range {v23 .. v24}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_23a

    .line 88
    .end local v9    # "e":Lnet/lingala/zip4j/exception/ZipException;
    .end local v11    # "file":Ljava/io/File;
    .end local v12    # "files":[Ljava/io/File;
    .end local v20    # "zipFile2":Lnet/lingala/zip4j/core/ZipFile;
    :cond_25d
    new-instance v21, Ljava/io/File;

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->exists()Z

    move-result v21

    if-eqz v21, :cond_14f

    .line 90
    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->exists()Z

    move-result v21

    if-eqz v21, :cond_275

    invoke-virtual/range {v17 .. v18}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 91
    :cond_275
    new-instance v20, Lnet/lingala/zip4j/core/ZipFile;

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lnet/lingala/zip4j/core/ZipFile;-><init>(Ljava/io/File;)V

    .line 92
    .restart local v20    # "zipFile2":Lnet/lingala/zip4j/core/ZipFile;
    new-instance v21, Ljava/io/File;

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v12

    .line 93
    .restart local v12    # "files":[Ljava/io/File;
    if-eqz v13, :cond_14f

    array-length v0, v13

    move/from16 v21, v0

    if-eqz v21, :cond_14f

    .line 94
    array-length v0, v12

    move/from16 v22, v0

    const/16 v21, 0x0

    :goto_297
    move/from16 v0, v21

    move/from16 v1, v22

    if-ge v0, v1, :cond_14f

    aget-object v11, v12, v21

    .line 95
    .restart local v11    # "file":Ljava/io/File;
    invoke-virtual {v11}, Ljava/io/File;->isDirectory()Z
    :try_end_2a2
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_251 .. :try_end_2a2} :catch_143
    .catch Ljava/lang/Exception; {:try_start_251 .. :try_end_2a2} :catch_1a4

    move-result v23

    if-eqz v23, :cond_2ba

    .line 97
    :try_start_2a5
    move-object/from16 v0, v20

    invoke-virtual {v0, v11, v14}, Lnet/lingala/zip4j/core/ZipFile;->addFolder(Ljava/io/File;Lnet/lingala/zip4j/model/ZipParameters;)V
    :try_end_2aa
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_2a5 .. :try_end_2aa} :catch_2ad
    .catch Ljava/lang/Exception; {:try_start_2a5 .. :try_end_2aa} :catch_1a4

    .line 94
    :goto_2aa
    add-int/lit8 v21, v21, 0x1

    goto :goto_297

    .line 98
    :catch_2ad
    move-exception v9

    .line 99
    .restart local v9    # "e":Lnet/lingala/zip4j/exception/ZipException;
    :try_start_2ae
    invoke-virtual {v9}, Lnet/lingala/zip4j/exception/ZipException;->printStackTrace()V

    .line 100
    const/4 v10, 0x1

    .line 101
    sget-object v23, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v24, "error"

    invoke-virtual/range {v23 .. v24}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_2b9
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_2ae .. :try_end_2b9} :catch_143
    .catch Ljava/lang/Exception; {:try_start_2ae .. :try_end_2b9} :catch_1a4

    goto :goto_2aa

    .line 103
    .end local v9    # "e":Lnet/lingala/zip4j/exception/ZipException;
    :cond_2ba
    :try_start_2ba
    move-object/from16 v0, v20

    invoke-virtual {v0, v11, v14}, Lnet/lingala/zip4j/core/ZipFile;->addFile(Ljava/io/File;Lnet/lingala/zip4j/model/ZipParameters;)V
    :try_end_2bf
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_2ba .. :try_end_2bf} :catch_2c0
    .catch Ljava/lang/Exception; {:try_start_2ba .. :try_end_2bf} :catch_1a4

    goto :goto_2aa

    :catch_2c0
    move-exception v9

    .line 104
    .restart local v9    # "e":Lnet/lingala/zip4j/exception/ZipException;
    :try_start_2c1
    invoke-virtual {v9}, Lnet/lingala/zip4j/exception/ZipException;->printStackTrace()V

    .line 105
    const/4 v10, 0x1

    .line 106
    sget-object v23, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v24, "error"

    invoke-virtual/range {v23 .. v24}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_2cc
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_2c1 .. :try_end_2cc} :catch_143
    .catch Ljava/lang/Exception; {:try_start_2c1 .. :try_end_2cc} :catch_1a4

    goto :goto_2aa

    .line 129
    .end local v6    # "datadirs":Ljava/io/File;
    .end local v9    # "e":Lnet/lingala/zip4j/exception/ZipException;
    .end local v11    # "file":Ljava/io/File;
    .end local v12    # "files":[Ljava/io/File;
    .end local v13    # "folders":[Ljava/io/File;
    .end local v14    # "parameters":Lnet/lingala/zip4j/model/ZipParameters;
    .end local v19    # "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    .end local v20    # "zipFile2":Lnet/lingala/zip4j/core/ZipFile;
    :cond_2cd
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    goto/16 :goto_16f

    .line 134
    :cond_2d2
    invoke-virtual {v8}, Ljava/io/File;->delete()Z

    goto/16 :goto_17a

    .line 139
    :cond_2d7
    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->delete()Z

    goto/16 :goto_187
.end method
