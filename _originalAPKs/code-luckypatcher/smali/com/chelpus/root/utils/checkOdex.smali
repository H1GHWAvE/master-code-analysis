.class public Lcom/chelpus/root/utils/checkOdex;
.super Ljava/lang/Object;
.source "checkOdex.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .registers 10
    .param p0, "paramArrayOfString"    # [Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const-wide/16 v7, 0x0

    .line 12
    new-instance v3, Lcom/chelpus/root/utils/checkOdex$1;

    invoke-direct {v3}, Lcom/chelpus/root/utils/checkOdex$1;-><init>()V

    invoke-static {v3}, Lcom/chelpus/Utils;->startRootJava(Ljava/lang/Object;)V

    .line 13
    new-instance v1, Ljava/io/File;

    const/4 v3, 0x0

    aget-object v3, p0, v3

    invoke-static {v3, v4}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 14
    .local v1, "dirodex":Ljava/io/File;
    new-instance v0, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v4, p0, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x2

    aget-object v4, p0, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 17
    .local v0, "dirapp":Ljava/io/File;
    :try_start_38
    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v3

    cmp-long v3, v3, v7

    if-nez v3, :cond_43

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 18
    :cond_43
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_5c

    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v3

    cmp-long v3, v3, v7

    if-eqz v3, :cond_5c

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "ODEX FILE FOUND!"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_58
    .catch Ljava/lang/Exception; {:try_start_38 .. :try_end_58} :catch_95

    .line 41
    :cond_58
    :goto_58
    invoke-static {}, Lcom/chelpus/Utils;->exitFromRootJava()V

    .line 42
    return-void

    .line 20
    :cond_5c
    :try_start_5c
    invoke-virtual {v0}, Ljava/io/File;->exists()Z
    :try_end_5f
    .catch Ljava/lang/Exception; {:try_start_5c .. :try_end_5f} :catch_95

    move-result v3

    if-eqz v3, :cond_bb

    .line 22
    :try_start_62
    invoke-static {v0, v1}, Lcom/chelpus/Utils;->copyFile(Ljava/io/File;Ljava/io/File;)V
    :try_end_65
    .catch Ljava/lang/Exception; {:try_start_62 .. :try_end_65} :catch_b3

    .line 29
    :goto_65
    :try_start_65
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v3

    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v5

    cmp-long v3, v3, v5

    if-eqz v3, :cond_74

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 30
    :cond_74
    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v3

    cmp-long v3, v3, v7

    if-nez v3, :cond_7f

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 31
    :cond_7f
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_58

    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v3

    cmp-long v3, v3, v7

    if-eqz v3, :cond_58

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "ODEX FILE FOUND!"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_94
    .catch Ljava/lang/Exception; {:try_start_65 .. :try_end_94} :catch_95

    goto :goto_58

    .line 36
    :catch_95
    move-exception v2

    .line 37
    .local v2, "e":Ljava/lang/Exception;
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception e"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_58

    .line 23
    .end local v2    # "e":Ljava/lang/Exception;
    :catch_b3
    move-exception v2

    .line 24
    .restart local v2    # "e":Ljava/lang/Exception;
    :try_start_b4
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 25
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    goto :goto_65

    .line 33
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_bb
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "ODEX FILE NOT FOUND!"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_c2
    .catch Ljava/lang/Exception; {:try_start_b4 .. :try_end_c2} :catch_95

    goto :goto_58
.end method
