.class public Lcom/chelpus/root/utils/runpatchads;
.super Ljava/lang/Object;
.source "runpatchads.java"


# static fields
.field private static ART:Z

.field public static AdsBlockFile:Ljava/lang/String;

.field public static appdir:Ljava/lang/String;

.field public static classesFiles:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private static copyDC:Z

.field private static createAPK:Z

.field public static crkapk:Ljava/io/File;

.field private static dependencies:Z

.field public static dir:Ljava/lang/String;

.field public static dir2:Ljava/lang/String;

.field public static dirapp:Ljava/lang/String;

.field private static fileblock:Z

.field public static filestopatch:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private static full_offline:Z

.field private static pattern1:Z

.field private static pattern2:Z

.field private static pattern3:Z

.field private static pattern4:Z

.field private static pattern5:Z

.field private static pattern6:Z

.field public static print:Ljava/io/PrintStream;

.field public static result:Ljava/lang/String;

.field public static sddir:Ljava/lang/String;

.field public static sites:[[B

.field public static system:Z

.field public static uid:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 36
    sput-boolean v1, Lcom/chelpus/root/utils/runpatchads;->createAPK:Z

    .line 37
    sput-boolean v0, Lcom/chelpus/root/utils/runpatchads;->fileblock:Z

    .line 38
    sput-boolean v0, Lcom/chelpus/root/utils/runpatchads;->pattern1:Z

    .line 39
    sput-boolean v0, Lcom/chelpus/root/utils/runpatchads;->pattern2:Z

    .line 40
    sput-boolean v0, Lcom/chelpus/root/utils/runpatchads;->pattern3:Z

    .line 41
    sput-boolean v0, Lcom/chelpus/root/utils/runpatchads;->pattern4:Z

    .line 42
    sput-boolean v0, Lcom/chelpus/root/utils/runpatchads;->pattern5:Z

    .line 43
    sput-boolean v0, Lcom/chelpus/root/utils/runpatchads;->pattern6:Z

    .line 44
    sput-boolean v0, Lcom/chelpus/root/utils/runpatchads;->dependencies:Z

    .line 45
    sput-boolean v0, Lcom/chelpus/root/utils/runpatchads;->full_offline:Z

    .line 46
    sput-boolean v1, Lcom/chelpus/root/utils/runpatchads;->copyDC:Z

    .line 47
    sput-boolean v1, Lcom/chelpus/root/utils/runpatchads;->ART:Z

    .line 48
    const-string v0, "/data/app/"

    sput-object v0, Lcom/chelpus/root/utils/runpatchads;->dirapp:Ljava/lang/String;

    .line 49
    sput-boolean v1, Lcom/chelpus/root/utils/runpatchads;->system:Z

    .line 50
    const-string v0, ""

    sput-object v0, Lcom/chelpus/root/utils/runpatchads;->uid:Ljava/lang/String;

    .line 51
    const-string v0, "/sdcard/"

    sput-object v0, Lcom/chelpus/root/utils/runpatchads;->dir:Ljava/lang/String;

    .line 52
    const-string v0, "/sdcard/"

    sput-object v0, Lcom/chelpus/root/utils/runpatchads;->dir2:Ljava/lang/String;

    .line 53
    const/4 v0, 0x0

    sput-object v0, Lcom/chelpus/root/utils/runpatchads;->filestopatch:Ljava/util/ArrayList;

    .line 54
    const-string v0, "/sdcard/"

    sput-object v0, Lcom/chelpus/root/utils/runpatchads;->sddir:Ljava/lang/String;

    .line 55
    const-string v0, "/sdcard/"

    sput-object v0, Lcom/chelpus/root/utils/runpatchads;->appdir:Ljava/lang/String;

    .line 58
    const-string v0, ""

    sput-object v0, Lcom/chelpus/root/utils/runpatchads;->AdsBlockFile:Ljava/lang/String;

    .line 62
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/chelpus/root/utils/runpatchads;->classesFiles:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static byteverify(Ljava/nio/MappedByteBuffer;IB[B[B[B[BLjava/lang/String;Z)Z
    .registers 14
    .param p0, "fileBytes"    # Ljava/nio/MappedByteBuffer;
    .param p1, "curentPos"    # I
    .param p2, "curentByte"    # B
    .param p3, "byteOrig"    # [B
    .param p4, "mask"    # [B
    .param p5, "byteReplace"    # [B
    .param p6, "rep_mask"    # [B
    .param p7, "log"    # Ljava/lang/String;
    .param p8, "pattern"    # Z

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1026
    aget-byte v4, p3, v3

    if-ne p2, v4, :cond_42

    if-eqz p8, :cond_42

    .line 1028
    aget-byte v4, p6, v3

    if-nez v4, :cond_e

    aput-byte p2, p5, v3

    .line 1029
    :cond_e
    const/4 v0, 0x1

    .line 1030
    .local v0, "i":I
    add-int/lit8 v4, p1, 0x1

    invoke-virtual {p0, v4}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1031
    invoke-virtual {p0}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v1

    .line 1033
    .local v1, "prufbyte":B
    :goto_18
    aget-byte v4, p3, v0

    if-eq v1, v4, :cond_20

    aget-byte v4, p4, v0

    if-ne v4, v2, :cond_3d

    .line 1035
    :cond_20
    aget-byte v4, p6, v0

    if-nez v4, :cond_26

    aput-byte v1, p5, v0

    .line 1036
    :cond_26
    add-int/lit8 v0, v0, 0x1

    .line 1038
    array-length v4, p3

    if-ne v0, v4, :cond_38

    .line 1040
    invoke-virtual {p0, p1}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1041
    invoke-virtual {p0, p5}, Ljava/nio/MappedByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 1042
    invoke-virtual {p0}, Ljava/nio/MappedByteBuffer;->force()Ljava/nio/MappedByteBuffer;

    .line 1044
    invoke-static {p7}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 1053
    .end local v0    # "i":I
    .end local v1    # "prufbyte":B
    :goto_37
    return v2

    .line 1049
    .restart local v0    # "i":I
    .restart local v1    # "prufbyte":B
    :cond_38
    invoke-virtual {p0}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v1

    goto :goto_18

    .line 1051
    :cond_3d
    add-int/lit8 v2, p1, 0x1

    invoke-virtual {p0, v2}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .end local v0    # "i":I
    .end local v1    # "prufbyte":B
    :cond_42
    move v2, v3

    .line 1053
    goto :goto_37
.end method

.method private static final calcChecksum([BI)V
    .registers 7
    .param p0, "paramArrayOfByte"    # [B
    .param p1, "j"    # I

    .prologue
    .line 969
    new-instance v1, Ljava/util/zip/Adler32;

    invoke-direct {v1}, Ljava/util/zip/Adler32;-><init>()V

    .line 970
    .local v1, "localAdler32":Ljava/util/zip/Adler32;
    const/16 v2, 0xc

    array-length v3, p0

    add-int/lit8 v4, p1, 0xc

    sub-int/2addr v3, v4

    invoke-virtual {v1, p0, v2, v3}, Ljava/util/zip/Adler32;->update([BII)V

    .line 971
    invoke-virtual {v1}, Ljava/util/zip/Adler32;->getValue()J

    move-result-wide v2

    long-to-int v0, v2

    .line 972
    .local v0, "i":I
    add-int/lit8 v2, p1, 0x8

    int-to-byte v3, v0

    aput-byte v3, p0, v2

    .line 973
    add-int/lit8 v2, p1, 0x9

    shr-int/lit8 v3, v0, 0x8

    int-to-byte v3, v3

    aput-byte v3, p0, v2

    .line 974
    add-int/lit8 v2, p1, 0xa

    shr-int/lit8 v3, v0, 0x10

    int-to-byte v3, v3

    aput-byte v3, p0, v2

    .line 975
    add-int/lit8 v2, p1, 0xb

    shr-int/lit8 v3, v0, 0x18

    int-to-byte v3, v3

    aput-byte v3, p0, v2

    .line 976
    return-void
.end method

.method private static final calcSignature([BI)V
    .registers 10
    .param p0, "paramArrayOfByte"    # [B
    .param p1, "j"    # I

    .prologue
    const/16 v7, 0x14

    .line 982
    :try_start_2
    const-string v4, "SHA-1"

    invoke-static {v4}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_7
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_2 .. :try_end_7} :catch_41

    move-result-object v2

    .line 989
    .local v2, "localMessageDigest":Ljava/security/MessageDigest;
    const/16 v4, 0x20

    array-length v5, p0

    add-int/lit8 v6, p1, 0x20

    sub-int/2addr v5, v6

    invoke-virtual {v2, p0, v4, v5}, Ljava/security/MessageDigest;->update([BII)V

    .line 992
    add-int/lit8 v4, p1, 0xc

    const/16 v5, 0x14

    :try_start_15
    invoke-virtual {v2, p0, v4, v5}, Ljava/security/MessageDigest;->digest([BII)I

    move-result v0

    .line 993
    .local v0, "i":I
    if-eq v0, v7, :cond_48

    .line 994
    new-instance v4, Ljava/lang/RuntimeException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "unexpected digest write:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "bytes"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_3a
    .catch Ljava/security/DigestException; {:try_start_15 .. :try_end_3a} :catch_3a

    .line 996
    .end local v0    # "i":I
    :catch_3a
    move-exception v1

    .line 998
    .local v1, "localDigestException":Ljava/security/DigestException;
    new-instance v4, Ljava/lang/RuntimeException;

    invoke-direct {v4, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v4

    .line 984
    .end local v1    # "localDigestException":Ljava/security/DigestException;
    .end local v2    # "localMessageDigest":Ljava/security/MessageDigest;
    :catch_41
    move-exception v3

    .line 986
    .local v3, "localNoSuchAlgorithmException":Ljava/security/NoSuchAlgorithmException;
    new-instance v4, Ljava/lang/RuntimeException;

    invoke-direct {v4, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v4

    .line 1000
    .end local v3    # "localNoSuchAlgorithmException":Ljava/security/NoSuchAlgorithmException;
    .restart local v0    # "i":I
    .restart local v2    # "localMessageDigest":Ljava/security/MessageDigest;
    :cond_48
    return-void
.end method

.method public static clearTemp()V
    .registers 6

    .prologue
    .line 1005
    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/chelpus/root/utils/runpatchads;->dir:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/AndroidManifest.xml"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1006
    .local v3, "tmp":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1007
    .local v2, "tempdex":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_23

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 1008
    :cond_23
    sget-object v4, Lcom/chelpus/root/utils/runpatchads;->classesFiles:Ljava/util/ArrayList;

    if-eqz v4, :cond_67

    sget-object v4, Lcom/chelpus/root/utils/runpatchads;->classesFiles:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_67

    .line 1009
    sget-object v4, Lcom/chelpus/root/utils/runpatchads;->classesFiles:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_35
    :goto_35
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_67

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 1010
    .local v0, "cl":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_35

    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_4a
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_4a} :catch_4b

    goto :goto_35

    .line 1019
    .end local v0    # "cl":Ljava/io/File;
    .end local v2    # "tempdex":Ljava/io/File;
    :catch_4b
    move-exception v1

    .line 1021
    .local v1, "e":Ljava/lang/Exception;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 1023
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_66
    :goto_66
    return-void

    .line 1013
    .restart local v2    # "tempdex":Ljava/io/File;
    :cond_67
    :try_start_67
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/chelpus/root/utils/runpatchads;->dir:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/classes.dex"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1014
    new-instance v2, Ljava/io/File;

    .end local v2    # "tempdex":Ljava/io/File;
    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1015
    .restart local v2    # "tempdex":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_8a

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 1016
    :cond_8a
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/chelpus/root/utils/runpatchads;->dir:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/classes.dex.apk"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1017
    new-instance v2, Ljava/io/File;

    .end local v2    # "tempdex":Ljava/io/File;
    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1018
    .restart local v2    # "tempdex":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_66

    invoke-virtual {v2}, Ljava/io/File;->delete()Z
    :try_end_ad
    .catch Ljava/lang/Exception; {:try_start_67 .. :try_end_ad} :catch_4b

    goto :goto_66
.end method

.method public static clearTempSD()V
    .registers 5

    .prologue
    .line 1197
    :try_start_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/chelpus/root/utils/runpatchads;->sddir:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/Modified/classes.dex.apk"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1198
    .local v2, "tmp":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1199
    .local v1, "tempdex":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_23

    invoke-virtual {v1}, Ljava/io/File;->delete()Z
    :try_end_23
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_23} :catch_24

    .line 1204
    .end local v1    # "tempdex":Ljava/io/File;
    :cond_23
    :goto_23
    return-void

    .line 1200
    :catch_24
    move-exception v0

    .line 1202
    .local v0, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    goto :goto_23
.end method

.method public static fixadler(Ljava/io/File;)V
    .registers 6
    .param p0, "destFile"    # Ljava/io/File;

    .prologue
    .line 951
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 952
    .local v2, "localFileInputStream":Ljava/io/FileInputStream;
    invoke-virtual {v2}, Ljava/io/FileInputStream;->available()I

    move-result v4

    new-array v0, v4, [B

    .line 953
    .local v0, "arrayOfByte":[B
    invoke-virtual {v2, v0}, Ljava/io/FileInputStream;->read([B)I

    .line 954
    const/4 v4, 0x0

    invoke-static {v0, v4}, Lcom/chelpus/root/utils/runpatchads;->calcSignature([BI)V

    .line 955
    const/4 v4, 0x0

    invoke-static {v0, v4}, Lcom/chelpus/root/utils/runpatchads;->calcChecksum([BI)V

    .line 956
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V

    .line 958
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, p0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 959
    .local v3, "localFileOutputStream":Ljava/io/FileOutputStream;
    invoke-virtual {v3, v0}, Ljava/io/FileOutputStream;->write([B)V

    .line 960
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_24
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_24} :catch_25

    .line 966
    .end local v0    # "arrayOfByte":[B
    .end local v2    # "localFileInputStream":Ljava/io/FileInputStream;
    .end local v3    # "localFileOutputStream":Ljava/io/FileOutputStream;
    :goto_24
    return-void

    .line 962
    :catch_25
    move-exception v1

    .line 964
    .local v1, "localException":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_24
.end method

.method public static main([Ljava/lang/String;)V
    .registers 64
    .param p0, "paramArrayOfString"    # [Ljava/lang/String;

    .prologue
    .line 67
    new-instance v47, Lcom/android/vending/billing/InAppBillingService/LUCK/LogOutputStream;

    const-string v10, "System.out"

    move-object/from16 v0, v47

    invoke-direct {v0, v10}, Lcom/android/vending/billing/InAppBillingService/LUCK/LogOutputStream;-><init>(Ljava/lang/String;)V

    .line 68
    .local v47, "pout":Lcom/android/vending/billing/InAppBillingService/LUCK/LogOutputStream;
    new-instance v10, Ljava/io/PrintStream;

    move-object/from16 v0, v47

    invoke-direct {v10, v0}, Ljava/io/PrintStream;-><init>(Ljava/io/OutputStream;)V

    sput-object v10, Lcom/chelpus/root/utils/runpatchads;->print:Ljava/io/PrintStream;

    .line 69
    sget-object v10, Lcom/chelpus/root/utils/runpatchads;->print:Ljava/io/PrintStream;

    const-string v11, "Ads-Code Running!"

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 70
    new-instance v10, Lcom/chelpus/root/utils/runpatchads$1;

    invoke-direct {v10}, Lcom/chelpus/root/utils/runpatchads$1;-><init>()V

    invoke-static {v10}, Lcom/chelpus/Utils;->startRootJava(Ljava/lang/Object;)V

    .line 71
    const-string v10, "Ads-Code Running!"

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 72
    const/4 v10, 0x0

    aget-object v10, p0, v10

    invoke-static {v10}, Lcom/chelpus/Utils;->kill(Ljava/lang/String;)V

    .line 73
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 74
    .local v2, "patchesList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;>;"
    const/4 v10, 0x1

    sput-boolean v10, Lcom/chelpus/root/utils/runpatchads;->fileblock:Z

    .line 75
    const/4 v10, 0x1

    sput-boolean v10, Lcom/chelpus/root/utils/runpatchads;->pattern1:Z

    .line 76
    const/4 v10, 0x1

    sput-boolean v10, Lcom/chelpus/root/utils/runpatchads;->pattern2:Z

    .line 77
    const/4 v10, 0x1

    sput-boolean v10, Lcom/chelpus/root/utils/runpatchads;->pattern3:Z

    .line 78
    const/4 v10, 0x1

    sput-boolean v10, Lcom/chelpus/root/utils/runpatchads;->pattern4:Z

    .line 79
    const/4 v10, 0x1

    sput-boolean v10, Lcom/chelpus/root/utils/runpatchads;->pattern5:Z

    .line 80
    const/4 v10, 0x1

    sput-boolean v10, Lcom/chelpus/root/utils/runpatchads;->pattern6:Z

    .line 81
    const/4 v10, 0x1

    sput-boolean v10, Lcom/chelpus/root/utils/runpatchads;->dependencies:Z

    .line 82
    const/4 v10, 0x1

    sput-boolean v10, Lcom/chelpus/root/utils/runpatchads;->full_offline:Z

    .line 83
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    sput-object v10, Lcom/chelpus/root/utils/runpatchads;->filestopatch:Ljava/util/ArrayList;

    .line 85
    :try_start_53
    new-instance v10, Ljava/io/File;

    const/4 v11, 0x3

    aget-object v11, p0, v11

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v33

    .line 86
    .local v33, "files":[Ljava/io/File;
    move-object/from16 v0, v33

    array-length v11, v0

    const/4 v10, 0x0

    :goto_63
    if-ge v10, v11, :cond_9b

    aget-object v30, v33, v10

    .line 87
    .local v30, "file":Ljava/io/File;
    invoke-virtual/range {v30 .. v30}, Ljava/io/File;->isFile()Z

    move-result v12

    if-eqz v12, :cond_94

    invoke-virtual/range {v30 .. v30}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v12

    const-string v13, "busybox"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_94

    invoke-virtual/range {v30 .. v30}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v12

    const-string v13, "reboot"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_94

    invoke-virtual/range {v30 .. v30}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v12

    const-string v13, "dalvikvm"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_94

    invoke-virtual/range {v30 .. v30}, Ljava/io/File;->delete()Z
    :try_end_94
    .catch Ljava/lang/Exception; {:try_start_53 .. :try_end_94} :catch_97

    .line 86
    :cond_94
    add-int/lit8 v10, v10, 0x1

    goto :goto_63

    .line 89
    .end local v30    # "file":Ljava/io/File;
    .end local v33    # "files":[Ljava/io/File;
    :catch_97
    move-exception v26

    .local v26, "e":Ljava/lang/Exception;
    invoke-virtual/range {v26 .. v26}, Ljava/lang/Exception;->printStackTrace()V

    .line 91
    .end local v26    # "e":Ljava/lang/Exception;
    :cond_9b
    const/4 v10, 0x1

    :try_start_9c
    aget-object v10, p0, v10

    const-string v11, "pattern0"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_a9

    const/4 v10, 0x0

    sput-boolean v10, Lcom/chelpus/root/utils/runpatchads;->fileblock:Z

    .line 92
    :cond_a9
    const/4 v10, 0x1

    aget-object v10, p0, v10

    const-string v11, "pattern1"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_b7

    const/4 v10, 0x0

    sput-boolean v10, Lcom/chelpus/root/utils/runpatchads;->pattern1:Z

    .line 93
    :cond_b7
    const/4 v10, 0x1

    aget-object v10, p0, v10

    const-string v11, "pattern2"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_c5

    const/4 v10, 0x0

    sput-boolean v10, Lcom/chelpus/root/utils/runpatchads;->pattern2:Z

    .line 94
    :cond_c5
    const/4 v10, 0x1

    aget-object v10, p0, v10

    const-string v11, "pattern3"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_d3

    const/4 v10, 0x0

    sput-boolean v10, Lcom/chelpus/root/utils/runpatchads;->pattern3:Z

    .line 95
    :cond_d3
    const/4 v10, 0x1

    aget-object v10, p0, v10

    const-string v11, "pattern4"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_e1

    const/4 v10, 0x0

    sput-boolean v10, Lcom/chelpus/root/utils/runpatchads;->pattern4:Z

    .line 96
    :cond_e1
    const/4 v10, 0x1

    aget-object v10, p0, v10

    const-string v11, "pattern5"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_ef

    const/4 v10, 0x0

    sput-boolean v10, Lcom/chelpus/root/utils/runpatchads;->pattern5:Z

    .line 97
    :cond_ef
    const/4 v10, 0x1

    aget-object v10, p0, v10

    const-string v11, "pattern6"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_fd

    const/4 v10, 0x0

    sput-boolean v10, Lcom/chelpus/root/utils/runpatchads;->pattern6:Z

    .line 98
    :cond_fd
    const/4 v10, 0x1

    aget-object v10, p0, v10

    const-string v11, "dependencies"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_10b

    const/4 v10, 0x0

    sput-boolean v10, Lcom/chelpus/root/utils/runpatchads;->dependencies:Z

    .line 99
    :cond_10b
    const/4 v10, 0x1

    aget-object v10, p0, v10

    const-string v11, "fulloffline"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_119

    const/4 v10, 0x0

    sput-boolean v10, Lcom/chelpus/root/utils/runpatchads;->full_offline:Z

    .line 100
    :cond_119
    const/4 v10, 0x6

    aget-object v10, p0, v10

    if-eqz v10, :cond_12c

    const/4 v10, 0x6

    aget-object v10, p0, v10

    const-string v11, "createAPK"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_12c

    const/4 v10, 0x1

    sput-boolean v10, Lcom/chelpus/root/utils/runpatchads;->createAPK:Z

    .line 101
    :cond_12c
    const/4 v10, 0x6

    aget-object v10, p0, v10

    if-eqz v10, :cond_13f

    const/4 v10, 0x6

    aget-object v10, p0, v10

    const-string v11, "ART"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_13f

    const/4 v10, 0x1

    sput-boolean v10, Lcom/chelpus/root/utils/runpatchads;->ART:Z

    .line 102
    :cond_13f
    const/4 v10, 0x6

    aget-object v10, p0, v10

    if-eqz v10, :cond_14a

    const/4 v10, 0x6

    aget-object v10, p0, v10

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 103
    :cond_14a
    const/4 v10, 0x7

    aget-object v10, p0, v10

    if-eqz v10, :cond_16d

    .line 104
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "1 "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const/4 v11, 0x7

    aget-object v11, p0, v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 105
    const/4 v10, 0x7

    aget-object v10, p0, v10

    sput-object v10, Lcom/chelpus/root/utils/runpatchads;->AdsBlockFile:Ljava/lang/String;

    .line 107
    :cond_16d
    const/16 v10, 0x8

    aget-object v10, p0, v10

    if-eqz v10, :cond_a53

    const/16 v10, 0x8

    aget-object v10, p0, v10

    sput-object v10, Lcom/chelpus/root/utils/runpatchads;->uid:Ljava/lang/String;
    :try_end_179
    .catch Ljava/lang/NullPointerException; {:try_start_9c .. :try_end_179} :catch_a5a
    .catch Ljava/lang/Exception; {:try_start_9c .. :try_end_179} :catch_1295

    .line 112
    :goto_179
    const/4 v10, 0x5

    :try_start_17a
    aget-object v10, p0, v10

    const-string v11, "copyDC"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_187

    const/4 v10, 0x1

    sput-boolean v10, Lcom/chelpus/root/utils/runpatchads;->copyDC:Z
    :try_end_187
    .catch Ljava/lang/NullPointerException; {:try_start_17a .. :try_end_187} :catch_1292
    .catch Ljava/lang/Exception; {:try_start_17a .. :try_end_187} :catch_128f

    .line 115
    :cond_187
    :goto_187
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchads;->createAPK:Z

    if-eqz v10, :cond_192

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    sput-object v10, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->startUnderRoot:Ljava/lang/Boolean;

    .line 116
    :cond_192
    new-instance v53, Ljava/util/ArrayList;

    invoke-direct/range {v53 .. v53}, Ljava/util/ArrayList;-><init>()V

    .line 117
    .local v53, "sitesFromFile":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 118
    .local v3, "origStr":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 119
    .local v4, "replStr":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 120
    .local v5, "trigger":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Boolean;>;"
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 121
    .local v6, "ResultText":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 122
    .local v7, "markers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 124
    .local v8, "dontConvert":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Boolean;>;"
    const-string v10, "1A ?? FF FF"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 125
    const-string v10, "1A ?? ?? ??"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 126
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchads;->pattern5:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 127
    const-string v10, "(offline intekekt 0)"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 128
    const-string v10, "search_offline"

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 130
    const-string v10, "1B ?? FF FF FF FF"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 131
    const-string v10, "1B ?? ?? ?? ?? ??"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 132
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchads;->pattern5:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 133
    const-string v10, "(offline intekekt 0)"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 134
    const-string v10, "search_offline"

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 137
    const-string v10, "6E 10 FF FF ?? 00 0C ??"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 138
    const-string v10, "6E 10 ?? ?? ?? 00 12 S0"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 139
    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 140
    const-string v10, "ads5 Fixed!\noffline (sha intekekt 4)"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 141
    const-string v10, "search_offline"

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 144
    const-string v10, "6E 10 FF FF ?? 00 0C ??"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 145
    const-string v10, "6E 10 ?? ?? ?? 00 12 S0"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 146
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchads;->full_offline:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 147
    const-string v10, "ads5 Fixed!\nfull_offline (sha intekekt 4)"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 148
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 153
    const-string v10, "12 F1 12 E2 1A ?? ?? ?? 70 ??"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 154
    const-string v10, "12 31 12 32 1A ?? ?? ?? 70 ??"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 155
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchads;->pattern1:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 156
    const-string v10, "ads1 Fixed!\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 157
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 159
    const-string v10, "13 ?? d8 02 13 ?? 5a 00 1A ?? ?? ?? 70 ??"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 160
    const-string v10, "13 ?? 03 00 13 ?? 03 00 1A ?? ?? ?? 70 ??"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 161
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchads;->pattern1:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 162
    const-string v10, "ads1 Fixed!\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 163
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 165
    const-string v10, "13 ?? d4 01 13 ?? 3c 00 1A ?? ?? ?? 70 ??"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 166
    const-string v10, "13 ?? 03 00 13 ?? 03 00 1A ?? ?? ?? 70 ??"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 167
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchads;->pattern1:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 168
    const-string v10, "ads1 Fixed!\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 169
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 171
    const-string v10, "13 ?? 2c 01 13 ?? fa 00 1A ?? ?? ?? 70 ??"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 172
    const-string v10, "13 ?? 03 00 13 ?? 03 00 1A ?? ?? ?? 70 ??"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 173
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchads;->pattern1:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 174
    const-string v10, "ads1 Fixed!\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 175
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 177
    const-string v10, "13 ?? 40 01 13 ?? 32 00 1A ?? ?? ?? 70 ??"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 178
    const-string v10, "13 ?? 03 00 13 ?? 03 00 1A ?? ?? ?? 70 ??"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 179
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchads;->pattern1:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 180
    const-string v10, "ads1 Fixed!\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 181
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 183
    const-string v10, "13 ?? 40 01 ?? ?? ?? ?? 13 ?? 32 00 1A ?? ?? ?? 70 ??"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 184
    const-string v10, "13 ?? 03 00 ?? ?? ?? ?? 13 ?? 03 00 1A ?? ?? ?? 70 ??"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 185
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchads;->pattern1:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 186
    const-string v10, "ads1 Fixed!\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 187
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 189
    const-string v10, "13 ?? A0 00 13 ?? 58 02 1A ?? ?? ?? 70 ??"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 190
    const-string v10, "13 ?? 03 00 13 ?? 03 00 1A ?? ?? ?? 70 ??"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 191
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchads;->pattern1:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 192
    const-string v10, "ads1 Fixed!\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 193
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 196
    const-string v10, "13 00 11 00 6E ?? ?? ?? ?? ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 38 ?? 0D 00 12 E1 12 E2 6E ?? ?? ?? ?? ?? 12 E1 12 E2 6E ?? ?? ?? ?? ?? 0E 00 12 E0 12 E1 6E 40"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 197
    const-string v10, "13 00 11 00 6E ?? ?? ?? ?? ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 38 ?? 0D 00 12 11 12 12 6E ?? ?? ?? ?? ?? 12 11 12 12 6E ?? ?? ?? ?? ?? 0E 00 12 10 12 11 6E 40"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 198
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchads;->pattern1:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 199
    const-string v10, "ads1 Fixed!\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 200
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 203
    const-string v10, "13 00 11 00 6E ?? ?? ?? ?? ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 38 ?? 0D 00 12 E0 12 E1 6E ?? ?? ?? ?? ?? 12 E0 12 E1 6E ?? ?? ?? ?? ?? 0E 00 12 E0 12 E1 6E 40"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 204
    const-string v10, "13 00 11 00 6E ?? ?? ?? ?? ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 38 ?? 0D 00 12 10 12 11 6E ?? ?? ?? ?? ?? 12 10 12 11 6E ?? ?? ?? ?? ?? 0E 00 12 10 12 11 6E 40"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 205
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchads;->pattern1:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 206
    const-string v10, "ads1 Fixed!\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 207
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 211
    const-string v10, "6E ?? ?? ?? ?? ?? 39 ?? ?? ?? 22 ?? ?? ?? 70 ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 0A ?? ?? ?? ?? ?? ?? ?? 0A ?? 6E ?? ?? ?? ?? ?? 0A ?? ?? ?? ?? ?? ?? ?? 0A ?? D8 ?? ?? FE D8 ?? ?? FE 6E ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 0E 00"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 212
    const-string v10, "6E ?? ?? ?? ?? ?? 39 ?? ?? ?? 22 ?? ?? ?? 70 ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 12 ?? ?? ?? ?? ?? ?? ?? 0A ?? 6E ?? ?? ?? ?? ?? 12 ?? ?? ?? ?? ?? ?? ?? 0A ?? 12 ?? 00 00 12 ?? 00 00 6E ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 0E 00"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 213
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchads;->pattern1:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 214
    const-string v10, "ads1 Fixed!\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 215
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 218
    const-string v10, "12 04 39 06 0A 00 22 00 ?? ?? 1A 01 ?? ?? 70 20 ?? ?? ?? ?? 27 00 71 10 ?? ?? ?? ?? 70 20 ?? ?? ?? ?? 71 10 ?? ?? ?? ?? 5B ?? ?? ?? 59 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 5B"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 219
    const-string v10, "0E 00 39 06 0A 00 22 00 ?? ?? 1A 01 ?? ?? 70 20 ?? ?? ?? ?? 27 00 71 10 ?? ?? ?? ?? 70 20 ?? ?? ?? ?? 71 10 ?? ?? ?? ?? 5B ?? ?? ?? 59 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 5B"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 220
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchads;->pattern1:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 221
    const-string v10, "ads1 Fixed!\nIMAdView"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 222
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 226
    const-string v10, "12 F1 12 E2 1A ?? ?? ?? 70 ??"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 227
    const-string v10, "12 01 12 02 1A ?? ?? ?? 70 ??"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 228
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchads;->pattern1:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 229
    const-string v10, "ads1 Fixed!\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 230
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 232
    const-string v10, "13 ?? d4 01 13 ?? 3c 00 1A ?? ?? ?? 70 ??"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 233
    const-string v10, "13 ?? 00 00 13 ?? 00 00 1A ?? ?? ?? 70 ??"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 234
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchads;->pattern2:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 235
    const-string v10, "ads2 Fixed!\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 236
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 238
    const-string v10, "13 ?? 2c 01 13 ?? fa 00 1A ?? ?? ?? 70 ??"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 239
    const-string v10, "13 ?? 00 00 13 ?? 00 00 1A ?? ?? ?? 70 ??"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 240
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchads;->pattern2:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 241
    const-string v10, "ads2 Fixed!\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 242
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 244
    const-string v10, "13 ?? 40 01 13 ?? 32 00 1A ?? ?? ?? 70 ??"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 245
    const-string v10, "13 ?? 00 00 13 ?? 00 00 1A ?? ?? ?? 70 ??"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 246
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchads;->pattern2:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 247
    const-string v10, "ads2 Fixed!\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 248
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 250
    const-string v10, "13 ?? 40 01 ?? ?? ?? ?? 13 ?? 32 00 1A ?? ?? ?? 70 ??"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 251
    const-string v10, "13 ?? 00 00 ?? ?? ?? ?? 13 ?? 00 00 1A ?? ?? ?? 70 ??"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 252
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchads;->pattern2:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 253
    const-string v10, "ads2 Fixed!\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 254
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 256
    const-string v10, "13 ?? d8 02 13 ?? 5a 00 1A ?? ?? ?? 70 ??"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 257
    const-string v10, "13 ?? 00 00 13 ?? 00 00 1A ?? ?? ?? 70 ??"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 258
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchads;->pattern2:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 259
    const-string v10, "ads2 Fixed!\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 260
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 262
    const-string v10, "13 ?? A0 00 13 ?? 58 02 1A ?? ?? ?? 70 ??"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 263
    const-string v10, "13 ?? 00 00 13 ?? 00 00 1A ?? ?? ?? 70 ??"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 264
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchads;->pattern2:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 265
    const-string v10, "ads2 Fixed!\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 266
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 269
    const-string v10, "13 00 11 00 6E ?? ?? ?? ?? ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 38 ?? 0D 00 12 E1 12 E2 6E ?? ?? ?? ?? ?? 12 E1 12 E2 6E ?? ?? ?? ?? ?? 0E 00 12 E0 12 E1 6E 40"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 270
    const-string v10, "13 00 11 00 6E ?? ?? ?? ?? ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 38 ?? 0D 00 12 11 12 12 6E ?? ?? ?? ?? ?? 12 11 12 12 6E ?? ?? ?? ?? ?? 0E 00 12 10 12 11 6E 40"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 271
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchads;->pattern2:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 272
    const-string v10, "ads2 Fixed!\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 273
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 276
    const-string v10, "13 00 11 00 6E ?? ?? ?? ?? ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 38 ?? 0D 00 12 E0 12 E1 6E ?? ?? ?? ?? ?? 12 E0 12 E1 6E ?? ?? ?? ?? ?? 0E 00 12 E0 12 E1 6E 40"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 277
    const-string v10, "13 00 11 00 6E ?? ?? ?? ?? ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 38 ?? 0D 00 12 10 12 11 6E ?? ?? ?? ?? ?? 12 10 12 11 6E ?? ?? ?? ?? ?? 0E 00 12 10 12 11 6E 40"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 278
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchads;->pattern2:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 279
    const-string v10, "ads2 Fixed!\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 280
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 284
    const-string v10, "6E ?? ?? ?? ?? ?? 39 ?? ?? ?? 22 ?? ?? ?? 70 ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 0A ?? ?? ?? ?? ?? ?? ?? 0A ?? 6E ?? ?? ?? ?? ?? 0A ?? ?? ?? ?? ?? ?? ?? 0A ?? D8 ?? ?? FE D8 ?? ?? FE 6E ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 0E 00"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 285
    const-string v10, "6E ?? ?? ?? ?? ?? 39 ?? ?? ?? 22 ?? ?? ?? 70 ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 12 ?? ?? ?? ?? ?? ?? ?? 0A ?? 6E ?? ?? ?? ?? ?? 12 ?? ?? ?? ?? ?? ?? ?? 0A ?? 12 ?? 00 00 12 ?? 00 00 6E ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 0E 00"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 286
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchads;->pattern2:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 287
    const-string v10, "ads2 Fixed!\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 288
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 291
    const-string v10, "12 04 39 06 0A 00 22 00 ?? ?? 1A 01 ?? ?? 70 20 ?? ?? ?? ?? 27 00 71 10 ?? ?? ?? ?? 70 20 ?? ?? ?? ?? 71 10 ?? ?? ?? ?? 5B ?? ?? ?? 59 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 5B"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 292
    const-string v10, "0E 00 39 06 0A 00 22 00 ?? ?? 1A 01 ?? ?? 70 20 ?? ?? ?? ?? 27 00 71 10 ?? ?? ?? ?? 70 20 ?? ?? ?? ?? 71 10 ?? ?? ?? ?? 5B ?? ?? ?? 59 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 5B"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 293
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchads;->pattern2:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 294
    const-string v10, "ads2 Fixed!\nIMAdView"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 295
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 298
    const-string v10, "55 ?? ?? ?? 39 00 ?? ?? 22 ?? ?? ?? 54 ?? ?? ?? 70 ?? ?? ?? ?? ?? 54 ?? ?? ?? 72 ?? ?? ?? ?? ?? 0C 01 54"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 299
    const-string v10, "55 ?? ?? ?? 32 00 ?? ?? 22 ?? ?? ?? 54 ?? ?? ?? 70 ?? ?? ?? ?? ?? 54 ?? ?? ?? 72 ?? ?? ?? ?? ?? 0C 01 54"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 300
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchads;->pattern3:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 301
    const-string v10, "ads3 Fixed!\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 302
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 306
    const-string v10, "1D ?? 54 ?? ?? ?? 39 ?? ?? ?? 5B ?? ?? ?? 12 00 5C ?? ?? ?? 22 ?? ?? ?? 70 ?? ?? ?? ?? ?? 5B ?? ?? ?? 54 ?? ?? ?? 6E 10"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 307
    const-string v10, "1D ?? 54 ?? ?? ?? 39 ?? ?? ?? 5B ?? ?? ?? 12 10 5C ?? ?? ?? 22 ?? ?? ?? 70 ?? ?? ?? ?? ?? 5B ?? ?? ?? 54 ?? ?? ?? 6E 10"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 308
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchads;->pattern3:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 309
    const-string v10, "ads3 Fixed!\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 310
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 313
    const-string v10, "1D ?? 70 ?? ?? ?? ?? ?? 0A ?? 38 ?? ?? ?? 1A ?? ?? ?? 71 ?? ?? ?? ?? ?? 1E ?? 0E 00 71"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 314
    const-string v10, "1D ?? 70 ?? ?? ?? ?? ?? 0A ?? 33 00 ?? ?? 1A ?? ?? ?? 71 ?? ?? ?? ?? ?? 1E ?? 0E 00 71"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 315
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchads;->pattern3:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 316
    const-string v10, "ads3 Fixed!\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 317
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 320
    const-string v10, "1D 01 54 10 ?? ?? 38 00 ?? ?? 12 10 1E 01 0F 00 12 00 28 ?? ?? ?? 1E 01 27 00"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 321
    const-string v10, "1D 01 54 10 ?? ?? 33 00 ?? ?? 12 10 1E 01 0F 00 12 00 28 ?? ?? ?? 1E 01 27 00"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 322
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchads;->pattern3:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 323
    const-string v10, "ads3 Fixed!\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 324
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 327
    const-string v10, "71 10 ?? ?? ?? ?? 15 03 FF FF 07 60 07 71 07 82 07 94 07 A5 76 06"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 328
    const-string v10, "71 10 ?? ?? ?? ?? 15 03 00 FF 07 60 07 71 07 82 07 94 07 A5 76 06"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 329
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchads;->pattern4:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 330
    const-string v10, "ads4 Fixed!\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 331
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 334
    const-string v10, "71 10 ?? ?? ?? ?? 15 03 FF FF 07 60 07 71 07 82 07 94 07 A5 74 06"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 335
    const-string v10, "71 10 ?? ?? ?? ?? 15 03 00 FF 07 60 07 71 07 82 07 94 07 A5 74 06"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 336
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchads;->pattern4:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 337
    const-string v10, "ads4 Fixed!\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 338
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 341
    const-string v10, "12 ?? 12 ?? 6E ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0C ?? 1A ?? ?? ?? 6E ?? ?? ?? ?? ?? 0A ?? 33 ?? ?? ?? 1A ?? ?? ?? 71"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 342
    const-string v10, "12 ?? 12 ?? 6E ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0C ?? 1A ?? ?? ?? 6E ?? ?? ?? ?? ?? 0A ?? 33 00 ?? ?? 1A ?? ?? ?? 71"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 343
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchads;->pattern4:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 344
    const-string v10, "ads4 Fixed!\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 345
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 349
    const-string v10, "1A 00 ?? ?? 6E 20 ?? ?? ?? ?? 0C ?? 1F ?? ?? ?? 6E 10 ?? ?? ?? ?? 0C 00 39 00 04 00 12 00 11 00"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 350
    const-string v10, "1A 00 ?? ?? 6E 20 ?? ?? ?? ?? 0C 01 1F ?? ?? ?? 6E 10 ?? ?? ?? ?? 0C 00 33 00 ?? ?? 12 00 11 00"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 351
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchads;->pattern5:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 352
    const-string v10, "ads5 Fixed!\noffline"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 353
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 356
    const-string v10, "6E ?? ?? ?? ?? ?? 0C ?? 38 ?? ?? 00 6E ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0A ?? 33 ?? ?? 00 6E ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0A"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 357
    const-string v10, "6E ?? ?? ?? ?? ?? 0C ?? 32 00 ?? 00 6E ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0A ?? 33 ?? ?? 00 6E ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0A"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 358
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchads;->pattern5:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 359
    const-string v10, "ads5 Fixed!\noffline"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 360
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 363
    const-string v10, "12 ?? 12 ?? 1A ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 1F ?? ?? ?? 39 ?? ?? ?? 0F ?? 6E ?? ?? ?? ?? ?? 0C ?? 38 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0A ?? 33 ?? ?? ?? 01 ?? 28 ?? 01 ?? 28"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 364
    const-string v10, "12 ?? 12 ?? 1A ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 1F ?? ?? ?? 33 00 ?? ?? 0F ?? 6E ?? ?? ?? ?? ?? 0C ?? 38 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0A ?? 33 ?? ?? ?? 01 ?? 28 ?? 01 ?? 28"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 365
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchads;->pattern5:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 366
    const-string v10, "ads5 Fixed!\noffline"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 367
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 370
    const-string v10, "12 ?? 12 ?? 1A ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 1F ?? ?? ?? 39 ?? ?? ?? 01 ?? 0F ?? 6E ?? ?? ?? ?? ?? 0C ?? 38 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0A ?? 33 ?? ?? ?? 01 ?? 28 ?? 01 ?? 28"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 371
    const-string v10, "12 ?? 12 ?? 1A ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 1F ?? ?? ?? 33 00 ?? ?? 01 ?? 0F ?? 6E ?? ?? ?? ?? ?? 0C ?? 38 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0A ?? 33 ?? ?? ?? 01 ?? 28 ?? 01 ?? 28"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 372
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchads;->pattern5:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 373
    const-string v10, "ads5 Fixed!\noffline"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 374
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 377
    const-string v10, "12 15 6E 10 ?? ?? ?? ?? 0C 01 1A ?? ?? ?? 6E 20 ?? ?? ?? ?? 0A 04 12 ?? 33 ?? ?? ?? 0F 05 6E 10"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 378
    const-string v10, "12 05 6E 10 ?? ?? ?? ?? 0C 01 1A ?? ?? ?? 6E 20 ?? ?? ?? ?? 0A 04 12 ?? 33 ?? ?? ?? 0F 05 6E 10"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 379
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchads;->pattern5:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 380
    const-string v10, "ads5 Fixed!\noffline"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 381
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 384
    const-string v10, "54 ?? ?? ?? 1A ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 1F ?? ?? ?? 1A ?? ?? ?? 54 ?? ?? ?? 1A ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 1F ?? ?? ?? 6E ?? ?? ?? ?? ?? 0A ?? 38 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 38"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 385
    const-string v10, "54 ?? ?? ?? 1A ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 1F ?? ?? ?? 1A ?? ?? ?? 54 ?? ?? ?? 1A ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 1F ?? ?? ?? 6E ?? ?? ?? ?? ?? 0A ?? 32 00 ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 38"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 386
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchads;->pattern5:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 387
    const-string v10, "ads5 Fixed!\noffline"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 388
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 391
    const-string v10, "12 01 39 04 04 00 01 10 0F 00 1A ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 1F ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 39 ?? ?? ?? 01 10 28"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 392
    const-string v10, "12 01 33 00 ?? ?? 01 10 0F 00 1A ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 1F ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 39 ?? ?? ?? 01 10 28"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 393
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchads;->pattern5:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 394
    const-string v10, "ads5 Fixed!\noffline"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 395
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 398
    const-string v10, "12 01 6E ?? ?? ?? ?? ?? 0C ?? 1A ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 1F ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 38 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0A ?? 38"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 399
    const-string v10, "12 01 6E ?? ?? ?? ?? ?? 0C ?? 1A ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 1F ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 32 00 ?? ?? 6E ?? ?? ?? ?? ?? 0A ?? 38"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 400
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchads;->pattern5:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 401
    const-string v10, "ads5 Fixed!\noffline"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 402
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 405
    const-string v10, "6E ?? ?? ?? ?? ?? 0C ?? 1A ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 1F ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 38 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0A ?? 38 ?? ?? ?? 12 ?? 0F ?? 12 00 28"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 406
    const-string v10, "6E ?? ?? ?? ?? ?? 0C ?? 1A ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 1F ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 32 00 ?? ?? 6E ?? ?? ?? ?? ?? 0A ?? 32 00 ?? ?? 12 ?? 0F ?? 12 00 28"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 407
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchads;->pattern5:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 408
    const-string v10, "ads5 Fixed!\noffline"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 409
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 413
    const-string v10, "54 ?? ?? ?? 39 ?? ?? ?? 1A ?? ?? ?? 1A ?? ?? ?? 71 ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 0E 00 1A"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 414
    const-string v10, "54 ?? ?? ?? 33 00 ?? ?? 1A ?? ?? ?? 1A ?? ?? ?? 71 ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 0E 00 1A"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 415
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchads;->pattern6:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 416
    const-string v10, "ads6 Fixed!\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 417
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 420
    const-string v10, "12 00 62 ?? ?? ?? 38 ?? ?? 00 62 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 39 ?? ?? 00"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 421
    const-string v10, "12 00 62 ?? ?? ?? 32 00 ?? 00 62 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 39 ?? ?? 00"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 422
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchads;->pattern6:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 423
    const-string v10, "ads6 Fixed!\nangry offline"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 424
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 426
    const/4 v10, 0x0

    aget-object v10, p0, v10

    const-string v11, "com.buak.Link2SD"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_840

    .line 428
    const-string v10, "00 05 2E 6F 64 65 78 00"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 429
    const-string v10, "00 05 2E 6F 64 65 79 00"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 430
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchads;->pattern6:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 431
    const-string v10, "ads6 Fixed!\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 432
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 435
    :cond_840
    const-string v10, "00 04 6F 64 65 78 00"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 436
    const-string v10, "00 04 6F 64 65 79 00"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 437
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchads;->pattern6:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 438
    const-string v10, "ads6 Fixed!\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 439
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 442
    const-string v10, "54 ?? ?? ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 22 ?? ?? ?? 12 E3 12 E4 70 ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 54 ?? ?? ?? 54"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 443
    const-string v10, "54 ?? ?? ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 22 ?? ?? ?? 12 03 12 04 70 ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 54 ?? ?? ?? 54"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 444
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchads;->pattern6:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 445
    const-string v10, "ads6 Fixed!\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 446
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 449
    const-string v10, "12 08 54 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0C ?? 39 ?? 04 00 01 ?? 0F 05 1A ?? ?? ?? 12"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 450
    const-string v10, "12 08 54 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0C ?? 33 00 ?? ?? 01 ?? 0F 05 1A ?? ?? ?? 12"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 451
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchads;->pattern6:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 452
    const-string v10, "ads6 Fixed!\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 453
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 454
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchads;->ART:Z

    if-nez v10, :cond_a5d

    .line 456
    const-string v10, "13 ?? 09 00 12 ?? 12 ?? 6E ?? ?? ?? ?? ?? 0C ??"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 457
    const-string v10, "12 00 0F 00 12 ?? 12 ?? 6E ?? ?? ?? ?? ?? 0C ??"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 458
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchads;->dependencies:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 459
    const-string v10, "com.android.vending dependencies removed\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 460
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 461
    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 464
    const-string v10, "12 ?? 12 ?? 13 ?? 09 00 6E ?? ?? ?? ?? ?? 0C ??"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 465
    const-string v10, "12 ?? 12 ?? 12 00 0F 00 6E ?? ?? ?? ?? ?? 0C ??"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 466
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchads;->dependencies:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 467
    const-string v10, "com.android.vending dependencies removed\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 468
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 469
    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 510
    :goto_8fd
    const/16 v19, 0x0

    .line 512
    .local v19, "conv":Z
    invoke-static/range {v19 .. v19}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-static/range {v2 .. v9}, Lcom/chelpus/Utils;->convertToPatchItemAuto(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/Boolean;)V

    .line 513
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchads;->fileblock:Z

    if-eqz v10, :cond_9b1

    .line 515
    :try_start_90a
    sget-object v10, Lcom/chelpus/root/utils/runpatchads;->AdsBlockFile:Ljava/lang/String;

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 516
    new-instance v50, Ljava/io/FileInputStream;

    sget-object v10, Lcom/chelpus/root/utils/runpatchads;->AdsBlockFile:Ljava/lang/String;

    move-object/from16 v0, v50

    invoke-direct {v0, v10}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 517
    .local v50, "qw":Ljava/io/FileInputStream;
    new-instance v52, Ljava/io/BufferedReader;

    new-instance v10, Ljava/io/InputStreamReader;

    move-object/from16 v0, v50

    invoke-direct {v10, v0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    move-object/from16 v0, v52

    invoke-direct {v0, v10}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 519
    .local v52, "reader":Ljava/io/BufferedReader;
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchads;->fileblock:Z

    if-eqz v10, :cond_aa9

    .line 520
    :cond_92a
    :goto_92a
    invoke-virtual/range {v52 .. v52}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v37

    .local v37, "line":Ljava/lang/String;
    if-eqz v37, :cond_aa9

    .line 522
    invoke-virtual/range {v37 .. v37}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v37

    .line 523
    const-string v10, ""

    move-object/from16 v0, v37

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_92a

    const-string v10, "UTF-8"

    move-object/from16 v0, v37

    invoke-virtual {v0, v10}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v10

    move-object/from16 v0, v53

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_94b
    .catch Ljava/lang/Exception; {:try_start_90a .. :try_end_94b} :catch_94c

    goto :goto_92a

    .line 526
    .end local v37    # "line":Ljava/lang/String;
    .end local v50    # "qw":Ljava/io/FileInputStream;
    .end local v52    # "reader":Ljava/io/BufferedReader;
    :catch_94c
    move-exception v26

    .line 527
    .restart local v26    # "e":Ljava/lang/Exception;
    invoke-virtual/range {v26 .. v26}, Ljava/lang/Exception;->printStackTrace()V

    .line 530
    .end local v26    # "e":Ljava/lang/Exception;
    :goto_950
    :try_start_950
    new-instance v50, Ljava/io/FileInputStream;

    sget-object v10, Lcom/chelpus/root/utils/runpatchads;->AdsBlockFile:Ljava/lang/String;

    const-string v11, "AdsBlockList.txt"

    const-string v12, "AdsBlockList_user_edit.txt"

    invoke-virtual {v10, v11, v12}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, v50

    invoke-direct {v0, v10}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 531
    .restart local v50    # "qw":Ljava/io/FileInputStream;
    new-instance v52, Ljava/io/BufferedReader;

    new-instance v10, Ljava/io/InputStreamReader;

    move-object/from16 v0, v50

    invoke-direct {v10, v0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    move-object/from16 v0, v52

    invoke-direct {v0, v10}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 534
    .restart local v52    # "reader":Ljava/io/BufferedReader;
    :cond_96f
    :goto_96f
    invoke-virtual/range {v52 .. v52}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v37

    .restart local v37    # "line":Ljava/lang/String;
    if-eqz v37, :cond_aae

    .line 536
    invoke-virtual/range {v37 .. v37}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v37

    .line 537
    const-string v10, ""

    move-object/from16 v0, v37

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_96f

    const-string v10, "UTF-8"

    move-object/from16 v0, v37

    invoke-virtual {v0, v10}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v10

    move-object/from16 v0, v53

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_990
    .catch Ljava/lang/Exception; {:try_start_950 .. :try_end_990} :catch_991

    goto :goto_96f

    .line 540
    .end local v37    # "line":Ljava/lang/String;
    .end local v50    # "qw":Ljava/io/FileInputStream;
    .end local v52    # "reader":Ljava/io/BufferedReader;
    :catch_991
    move-exception v26

    .line 541
    .restart local v26    # "e":Ljava/lang/Exception;
    invoke-virtual/range {v26 .. v26}, Ljava/lang/Exception;->printStackTrace()V

    .line 543
    .end local v26    # "e":Ljava/lang/Exception;
    :goto_995
    invoke-virtual/range {v53 .. v53}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_9b1

    .line 544
    invoke-virtual/range {v53 .. v53}, Ljava/util/ArrayList;->size()I

    move-result v10

    new-array v10, v10, [[B

    sput-object v10, Lcom/chelpus/root/utils/runpatchads;->sites:[[B

    .line 545
    sget-object v10, Lcom/chelpus/root/utils/runpatchads;->sites:[[B

    move-object/from16 v0, v53

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v10

    check-cast v10, [[B

    check-cast v10, [[B

    sput-object v10, Lcom/chelpus/root/utils/runpatchads;->sites:[[B

    .line 552
    :cond_9b1
    :try_start_9b1
    sget-object v10, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->startUnderRoot:Ljava/lang/Boolean;

    invoke-virtual {v10}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    if-eqz v10, :cond_9c1

    const/4 v10, 0x2

    aget-object v10, p0, v10

    const-string v11, "RW"

    invoke-static {v10, v11}, Lcom/chelpus/Utils;->remount(Ljava/lang/String;Ljava/lang/String;)Z

    .line 553
    :cond_9c1
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchads;->createAPK:Z

    if-nez v10, :cond_bf5

    sget-boolean v10, Lcom/chelpus/root/utils/runpatchads;->ART:Z

    if-nez v10, :cond_bf5

    .line 554
    const/4 v10, 0x3

    aget-object v10, p0, v10

    sput-object v10, Lcom/chelpus/root/utils/runpatchads;->dir:Ljava/lang/String;

    .line 555
    const/4 v10, 0x2

    aget-object v10, p0, v10

    sput-object v10, Lcom/chelpus/root/utils/runpatchads;->dirapp:Ljava/lang/String;

    .line 556
    invoke-static {}, Lcom/chelpus/root/utils/runpatchads;->clearTemp()V

    .line 557
    const/4 v10, 0x4

    aget-object v10, p0, v10

    const-string v11, "not_system"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_9e4

    const/4 v10, 0x0

    sput-boolean v10, Lcom/chelpus/root/utils/runpatchads;->system:Z

    .line 558
    :cond_9e4
    const/4 v10, 0x4

    aget-object v10, p0, v10

    const-string v11, "system"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_9f2

    const/4 v10, 0x1

    sput-boolean v10, Lcom/chelpus/root/utils/runpatchads;->system:Z

    .line 559
    :cond_9f2
    sget-object v10, Lcom/chelpus/root/utils/runpatchads;->filestopatch:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->clear()V

    .line 560
    const-string v10, "CLASSES mode create odex enabled."

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 561
    const/4 v10, 0x0

    aget-object v43, p0, v10

    .line 562
    .local v43, "packageName":Ljava/lang/String;
    const/4 v10, 0x2

    aget-object v10, p0, v10

    sput-object v10, Lcom/chelpus/root/utils/runpatchads;->appdir:Ljava/lang/String;

    .line 563
    const/4 v10, 0x3

    aget-object v10, p0, v10

    sput-object v10, Lcom/chelpus/root/utils/runpatchads;->sddir:Ljava/lang/String;

    .line 564
    invoke-static {}, Lcom/chelpus/root/utils/runpatchads;->clearTempSD()V

    .line 565
    new-instance v15, Ljava/io/File;

    sget-object v10, Lcom/chelpus/root/utils/runpatchads;->appdir:Ljava/lang/String;

    invoke-direct {v15, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 566
    .local v15, "apk":Ljava/io/File;
    const-string v10, "Get classes.dex."

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 567
    sget-object v10, Lcom/chelpus/root/utils/runpatchads;->print:Ljava/io/PrintStream;

    const-string v11, "Get classes.dex."

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 568
    invoke-static {v15}, Lcom/chelpus/root/utils/runpatchads;->unzipART(Ljava/io/File;)V

    .line 569
    sget-object v10, Lcom/chelpus/root/utils/runpatchads;->classesFiles:Ljava/util/ArrayList;

    if-eqz v10, :cond_a2e

    sget-object v10, Lcom/chelpus/root/utils/runpatchads;->classesFiles:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-nez v10, :cond_ab3

    .line 570
    :cond_a2e
    new-instance v10, Ljava/io/FileNotFoundException;

    invoke-direct {v10}, Ljava/io/FileNotFoundException;-><init>()V

    throw v10
    :try_end_a34
    .catch Ljava/io/FileNotFoundException; {:try_start_9b1 .. :try_end_a34} :catch_a34
    .catch Ljava/lang/Exception; {:try_start_9b1 .. :try_end_a34} :catch_ad6

    .line 922
    .end local v15    # "apk":Ljava/io/File;
    .end local v43    # "packageName":Ljava/lang/String;
    :catch_a34
    move-exception v38

    .line 923
    .local v38, "localFileNotFoundException":Ljava/io/FileNotFoundException;
    const-string v10, "Error: Program files are not found!\n\nCheck the location dalvik-cache to solve problems!\n\nDefault: /data/dalvik-cache/*"

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 929
    .end local v38    # "localFileNotFoundException":Ljava/io/FileNotFoundException;
    :cond_a3a
    :goto_a3a
    sget-object v10, Lcom/chelpus/root/utils/runpatchads;->filestopatch:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_a40
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_1230

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v32

    check-cast v32, Ljava/io/File;

    .line 930
    .local v32, "filepatch":Ljava/io/File;
    invoke-static/range {v32 .. v32}, Lcom/chelpus/Utils;->fixadler(Ljava/io/File;)V

    .line 931
    invoke-static {}, Lcom/chelpus/root/utils/runpatchads;->clearTempSD()V

    goto :goto_a40

    .line 108
    .end local v3    # "origStr":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v4    # "replStr":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v5    # "trigger":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Boolean;>;"
    .end local v6    # "ResultText":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v7    # "markers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v8    # "dontConvert":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Boolean;>;"
    .end local v19    # "conv":Z
    .end local v32    # "filepatch":Ljava/io/File;
    .end local v53    # "sitesFromFile":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
    :cond_a53
    :try_start_a53
    const-string v10, "fignya"

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z
    :try_end_a58
    .catch Ljava/lang/NullPointerException; {:try_start_a53 .. :try_end_a58} :catch_a5a
    .catch Ljava/lang/Exception; {:try_start_a53 .. :try_end_a58} :catch_1295

    goto/16 :goto_179

    .line 109
    :catch_a5a
    move-exception v10

    goto/16 :goto_179

    .line 472
    .restart local v3    # "origStr":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v4    # "replStr":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v5    # "trigger":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Boolean;>;"
    .restart local v6    # "ResultText":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v7    # "markers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v8    # "dontConvert":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Boolean;>;"
    .restart local v53    # "sitesFromFile":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
    :cond_a5d
    const-string v10, "13 ?? 09 00 12 ?? 12 ?? 6E ?? ?? ?? ?? ?? 0C ??"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 473
    const-string v10, "12 S0 00 00 12 S0 12 S0 6E ?? ?? ?? ?? ?? 0C ??"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 474
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchads;->dependencies:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 475
    const-string v10, "com.android.vending dependencies removed\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 476
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 477
    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 480
    const-string v10, "12 ?? 12 ?? 13 ?? 09 00 6E ?? ?? ?? ?? ?? 0C ??"

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 481
    const-string v10, "12 S0 12 S0 12 S0 00 00 6E ?? ?? ?? ?? ?? 0C ??"

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 482
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchads;->dependencies:Z

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 483
    const-string v10, "com.android.vending dependencies removed\n"

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 484
    const-string v10, ""

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 485
    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_8fd

    .line 525
    .restart local v19    # "conv":Z
    .restart local v50    # "qw":Ljava/io/FileInputStream;
    .restart local v52    # "reader":Ljava/io/BufferedReader;
    :cond_aa9
    :try_start_aa9
    invoke-virtual/range {v50 .. v50}, Ljava/io/FileInputStream;->close()V
    :try_end_aac
    .catch Ljava/lang/Exception; {:try_start_aa9 .. :try_end_aac} :catch_94c

    goto/16 :goto_950

    .line 539
    .restart local v37    # "line":Ljava/lang/String;
    :cond_aae
    :try_start_aae
    invoke-virtual/range {v50 .. v50}, Ljava/io/FileInputStream;->close()V
    :try_end_ab1
    .catch Ljava/lang/Exception; {:try_start_aae .. :try_end_ab1} :catch_991

    goto/16 :goto_995

    .line 572
    .end local v37    # "line":Ljava/lang/String;
    .end local v50    # "qw":Ljava/io/FileInputStream;
    .end local v52    # "reader":Ljava/io/BufferedReader;
    .restart local v15    # "apk":Ljava/io/File;
    .restart local v43    # "packageName":Ljava/lang/String;
    :cond_ab3
    :try_start_ab3
    sget-object v10, Lcom/chelpus/root/utils/runpatchads;->filestopatch:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->clear()V

    .line 573
    sget-object v10, Lcom/chelpus/root/utils/runpatchads;->classesFiles:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_abe
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_afb

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/io/File;

    .line 574
    .local v17, "cl":Ljava/io/File;
    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->exists()Z

    move-result v11

    if-nez v11, :cond_af3

    new-instance v10, Ljava/io/FileNotFoundException;

    invoke-direct {v10}, Ljava/io/FileNotFoundException;-><init>()V

    throw v10
    :try_end_ad6
    .catch Ljava/io/FileNotFoundException; {:try_start_ab3 .. :try_end_ad6} :catch_a34
    .catch Ljava/lang/Exception; {:try_start_ab3 .. :try_end_ad6} :catch_ad6

    .line 925
    .end local v15    # "apk":Ljava/io/File;
    .end local v17    # "cl":Ljava/io/File;
    .end local v43    # "packageName":Ljava/lang/String;
    :catch_ad6
    move-exception v26

    .line 926
    .restart local v26    # "e":Ljava/lang/Exception;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Patch Process Exception: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual/range {v26 .. v26}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    goto/16 :goto_a3a

    .line 575
    .end local v26    # "e":Ljava/lang/Exception;
    .restart local v15    # "apk":Ljava/io/File;
    .restart local v17    # "cl":Ljava/io/File;
    .restart local v43    # "packageName":Ljava/lang/String;
    :cond_af3
    :try_start_af3
    sget-object v11, Lcom/chelpus/root/utils/runpatchads;->filestopatch:Ljava/util/ArrayList;

    move-object/from16 v0, v17

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_abe

    .line 579
    .end local v17    # "cl":Ljava/io/File;
    :cond_afb
    const/4 v10, 0x2

    aget-object v10, p0, v10

    const/4 v11, 0x1

    invoke-static {v10, v11}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v40

    .line 580
    .local v40, "odexstr":Ljava/lang/String;
    new-instance v39, Ljava/io/File;

    invoke-direct/range {v39 .. v40}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 581
    .local v39, "odexfile":Ljava/io/File;
    invoke-virtual/range {v39 .. v39}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_b11

    invoke-virtual/range {v39 .. v39}, Ljava/io/File;->delete()Z

    .line 582
    :cond_b11
    new-instance v39, Ljava/io/File;

    .end local v39    # "odexfile":Ljava/io/File;
    const-string v10, "-1"

    const-string v11, "-2"

    move-object/from16 v0, v40

    invoke-virtual {v0, v10, v11}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, v39

    invoke-direct {v0, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 583
    .restart local v39    # "odexfile":Ljava/io/File;
    invoke-virtual/range {v39 .. v39}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_b2b

    invoke-virtual/range {v39 .. v39}, Ljava/io/File;->delete()Z

    .line 584
    :cond_b2b
    new-instance v39, Ljava/io/File;

    .end local v39    # "odexfile":Ljava/io/File;
    const-string v10, "-2"

    const-string v11, "-1"

    move-object/from16 v0, v40

    invoke-virtual {v0, v10, v11}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, v39

    invoke-direct {v0, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 585
    .restart local v39    # "odexfile":Ljava/io/File;
    invoke-virtual/range {v39 .. v39}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_b45

    invoke-virtual/range {v39 .. v39}, Ljava/io/File;->delete()Z

    .line 631
    .end local v15    # "apk":Ljava/io/File;
    .end local v39    # "odexfile":Ljava/io/File;
    .end local v40    # "odexstr":Ljava/lang/String;
    .end local v43    # "packageName":Ljava/lang/String;
    :cond_b45
    sget-object v10, Lcom/chelpus/root/utils/runpatchads;->filestopatch:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v60

    :goto_b4b
    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_a3a

    invoke-interface/range {v60 .. v60}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v32

    check-cast v32, Ljava/io/File;

    .line 632
    .restart local v32    # "filepatch":Ljava/io/File;
    const-string v10, "Find string id."

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 635
    new-instance v56, Ljava/util/ArrayList;

    invoke-direct/range {v56 .. v56}, Ljava/util/ArrayList;-><init>()V

    .line 636
    .local v56, "strings":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v10, "phone"

    move-object/from16 v0, v56

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 637
    const-string v10, "Landroid/net/ConnectivityManager;"

    move-object/from16 v0, v56

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 638
    const-string v10, "getActiveNetworkInfo"

    move-object/from16 v0, v56

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 641
    const-string v10, "String analysis."

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 642
    sget-object v10, Lcom/chelpus/root/utils/runpatchads;->print:Ljava/io/PrintStream;

    const-string v11, "String analysis."

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 643
    invoke-virtual/range {v32 .. v32}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    move-object/from16 v0, v56

    invoke-static {v10, v0, v11}, Lcom/chelpus/Utils;->getStringIds(Ljava/lang/String;Ljava/util/ArrayList;Z)Ljava/util/ArrayList;

    move-result-object v42

    .line 645
    .local v42, "offsets":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;>;"
    const/16 v27, 0x0

    .local v27, "f1":Z
    const/16 v28, 0x0

    .local v28, "f2":Z
    const/16 v29, 0x0

    .line 646
    .local v29, "f3":Z
    const/16 v41, 0x1

    .line 647
    .local v41, "of_to_patch":I
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 648
    .local v18, "commands":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;>;"
    new-instance v10, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;

    const-string v11, "Landroid/net/ConnectivityManager;"

    const-string v12, "getActiveNetworkInfo"

    invoke-direct {v10, v11, v12}, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 649
    invoke-virtual/range {v42 .. v42}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_bac
    :goto_bac
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_d7a

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v35

    check-cast v35, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;

    .line 651
    .local v35, "it":Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;
    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_bbc
    :goto_bbc
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_cdd

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v36

    check-cast v36, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;

    .line 652
    .local v36, "item":Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;
    move-object/from16 v0, v36

    iget-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;->object:Ljava/lang/String;

    move-object/from16 v0, v35

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->str:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_bde

    .line 653
    move-object/from16 v0, v35

    iget-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    move-object/from16 v0, v36

    iput-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;->Object:[B

    .line 655
    :cond_bde
    move-object/from16 v0, v36

    iget-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;->method:Ljava/lang/String;

    move-object/from16 v0, v35

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->str:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_bbc

    .line 656
    move-object/from16 v0, v35

    iget-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    move-object/from16 v0, v36

    iput-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;->Method:[B

    goto :goto_bbc

    .line 587
    .end local v18    # "commands":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;>;"
    .end local v27    # "f1":Z
    .end local v28    # "f2":Z
    .end local v29    # "f3":Z
    .end local v32    # "filepatch":Ljava/io/File;
    .end local v35    # "it":Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;
    .end local v36    # "item":Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;
    .end local v41    # "of_to_patch":I
    .end local v42    # "offsets":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;>;"
    .end local v56    # "strings":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_bf5
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchads;->createAPK:Z

    if-eqz v10, :cond_c7d

    .line 588
    const/4 v10, 0x0

    aget-object v43, p0, v10

    .line 589
    .restart local v43    # "packageName":Ljava/lang/String;
    const/4 v10, 0x2

    aget-object v10, p0, v10

    sput-object v10, Lcom/chelpus/root/utils/runpatchads;->appdir:Ljava/lang/String;

    .line 590
    const/4 v10, 0x5

    aget-object v10, p0, v10

    sput-object v10, Lcom/chelpus/root/utils/runpatchads;->sddir:Ljava/lang/String;

    .line 592
    invoke-static {}, Lcom/chelpus/root/utils/runpatchads;->clearTempSD()V

    .line 593
    new-instance v15, Ljava/io/File;

    sget-object v10, Lcom/chelpus/root/utils/runpatchads;->appdir:Ljava/lang/String;

    invoke-direct {v15, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 594
    .restart local v15    # "apk":Ljava/io/File;
    invoke-static {v15}, Lcom/chelpus/root/utils/runpatchads;->unzipSD(Ljava/io/File;)V

    .line 595
    new-instance v10, Ljava/io/File;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v12, Lcom/chelpus/root/utils/runpatchads;->sddir:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/Modified/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, v43

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ".apk"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v10, Lcom/chelpus/root/utils/runpatchads;->crkapk:Ljava/io/File;

    .line 596
    sget-object v10, Lcom/chelpus/root/utils/runpatchads;->crkapk:Ljava/io/File;

    invoke-static {v15, v10}, Lcom/chelpus/Utils;->copyFile(Ljava/io/File;Ljava/io/File;)V

    .line 597
    sget-object v10, Lcom/chelpus/root/utils/runpatchads;->classesFiles:Ljava/util/ArrayList;

    if-eqz v10, :cond_c4c

    sget-object v10, Lcom/chelpus/root/utils/runpatchads;->classesFiles:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-nez v10, :cond_c52

    .line 598
    :cond_c4c
    new-instance v10, Ljava/io/FileNotFoundException;

    invoke-direct {v10}, Ljava/io/FileNotFoundException;-><init>()V

    throw v10

    .line 600
    :cond_c52
    sget-object v10, Lcom/chelpus/root/utils/runpatchads;->filestopatch:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->clear()V

    .line 601
    sget-object v10, Lcom/chelpus/root/utils/runpatchads;->classesFiles:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_c5d
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_c7d

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/io/File;

    .line 602
    .restart local v17    # "cl":Ljava/io/File;
    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->exists()Z

    move-result v11

    if-nez v11, :cond_c75

    new-instance v10, Ljava/io/FileNotFoundException;

    invoke-direct {v10}, Ljava/io/FileNotFoundException;-><init>()V

    throw v10

    .line 603
    :cond_c75
    sget-object v11, Lcom/chelpus/root/utils/runpatchads;->filestopatch:Ljava/util/ArrayList;

    move-object/from16 v0, v17

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_c5d

    .line 607
    .end local v15    # "apk":Ljava/io/File;
    .end local v17    # "cl":Ljava/io/File;
    .end local v43    # "packageName":Ljava/lang/String;
    :cond_c7d
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchads;->ART:Z

    if-eqz v10, :cond_b45

    .line 608
    const-string v10, "ART mode create dex enabled."

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 609
    const/4 v10, 0x0

    aget-object v43, p0, v10

    .line 610
    .restart local v43    # "packageName":Ljava/lang/String;
    const/4 v10, 0x2

    aget-object v10, p0, v10

    sput-object v10, Lcom/chelpus/root/utils/runpatchads;->appdir:Ljava/lang/String;

    .line 611
    const/4 v10, 0x3

    aget-object v10, p0, v10

    sput-object v10, Lcom/chelpus/root/utils/runpatchads;->sddir:Ljava/lang/String;

    .line 613
    invoke-static {}, Lcom/chelpus/root/utils/runpatchads;->clearTempSD()V

    .line 614
    new-instance v15, Ljava/io/File;

    sget-object v10, Lcom/chelpus/root/utils/runpatchads;->appdir:Ljava/lang/String;

    invoke-direct {v15, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 615
    .restart local v15    # "apk":Ljava/io/File;
    invoke-static {v15}, Lcom/chelpus/root/utils/runpatchads;->unzipART(Ljava/io/File;)V

    .line 618
    sget-object v10, Lcom/chelpus/root/utils/runpatchads;->classesFiles:Ljava/util/ArrayList;

    if-eqz v10, :cond_cac

    sget-object v10, Lcom/chelpus/root/utils/runpatchads;->classesFiles:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-nez v10, :cond_cb2

    .line 619
    :cond_cac
    new-instance v10, Ljava/io/FileNotFoundException;

    invoke-direct {v10}, Ljava/io/FileNotFoundException;-><init>()V

    throw v10

    .line 621
    :cond_cb2
    sget-object v10, Lcom/chelpus/root/utils/runpatchads;->filestopatch:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->clear()V

    .line 622
    sget-object v10, Lcom/chelpus/root/utils/runpatchads;->classesFiles:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_cbd
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_b45

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/io/File;

    .line 623
    .restart local v17    # "cl":Ljava/io/File;
    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->exists()Z

    move-result v11

    if-nez v11, :cond_cd5

    new-instance v10, Ljava/io/FileNotFoundException;

    invoke-direct {v10}, Ljava/io/FileNotFoundException;-><init>()V

    throw v10

    .line 624
    :cond_cd5
    sget-object v11, Lcom/chelpus/root/utils/runpatchads;->filestopatch:Ljava/util/ArrayList;

    move-object/from16 v0, v17

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_cbd

    .line 660
    .end local v15    # "apk":Ljava/io/File;
    .end local v17    # "cl":Ljava/io/File;
    .end local v43    # "packageName":Ljava/lang/String;
    .restart local v18    # "commands":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;>;"
    .restart local v27    # "f1":Z
    .restart local v28    # "f2":Z
    .restart local v29    # "f3":Z
    .restart local v32    # "filepatch":Ljava/io/File;
    .restart local v35    # "it":Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;
    .restart local v41    # "of_to_patch":I
    .restart local v42    # "offsets":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;>;"
    .restart local v56    # "strings":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_cdd
    move-object/from16 v0, v35

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->str:Ljava/lang/String;

    const-string v12, "phone"

    invoke-virtual {v10, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_bac

    .line 661
    move-object/from16 v0, v35

    iget-boolean v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->bits32:Z

    if-nez v10, :cond_d23

    .line 662
    const/4 v10, 0x0

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x2

    move-object/from16 v0, v35

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x0

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 663
    const/4 v10, 0x0

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x3

    move-object/from16 v0, v35

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x1

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 665
    const/4 v10, 0x1

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    const/4 v12, 0x0

    iput-boolean v12, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->pattern:Z

    .line 674
    :goto_d1f
    const/16 v27, 0x1

    goto/16 :goto_bac

    .line 667
    :cond_d23
    const/4 v10, 0x0

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    const/4 v12, 0x0

    iput-boolean v12, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->pattern:Z

    .line 669
    const/4 v10, 0x1

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x2

    move-object/from16 v0, v35

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x0

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 670
    const/4 v10, 0x1

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x3

    move-object/from16 v0, v35

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x1

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 671
    const/4 v10, 0x1

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x4

    move-object/from16 v0, v35

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x2

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 672
    const/4 v10, 0x1

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x5

    move-object/from16 v0, v35

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;->offset:[B

    const/4 v14, 0x3

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    goto :goto_d1f

    .line 678
    .end local v35    # "it":Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;
    :cond_d7a
    if-nez v27, :cond_d80

    sget-boolean v10, Lcom/chelpus/root/utils/runpatchads;->full_offline:Z

    if-eqz v10, :cond_e70

    .line 679
    :cond_d80
    const-string v10, "Parse data for patch."

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 680
    sget-object v10, Lcom/chelpus/root/utils/runpatchads;->print:Ljava/io/PrintStream;

    const-string v11, "Parse data for patch."

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 681
    invoke-virtual/range {v32 .. v32}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    move-object/from16 v0, v18

    invoke-static {v10, v0, v11}, Lcom/chelpus/Utils;->getMethodsIds(Ljava/lang/String;Ljava/util/ArrayList;Z)Z

    .line 682
    sget-object v11, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const/4 v10, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;->index_command:[B

    const/4 v12, 0x0

    aget-byte v10, v10, v12

    invoke-static {v10}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v11, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 683
    sget-object v11, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const/4 v10, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;->index_command:[B

    const/4 v12, 0x1

    aget-byte v10, v10, v12

    invoke-static {v10}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v11, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 684
    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_dc8
    :goto_dc8
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_e50

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v36

    check-cast v36, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;

    .line 685
    .restart local v36    # "item":Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;
    move-object/from16 v0, v36

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;->object:Ljava/lang/String;

    const-string v12, "Landroid/net/ConnectivityManager;"

    invoke-virtual {v10, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_dc8

    .line 686
    move-object/from16 v0, v36

    iget-boolean v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;->found_index_command:Z

    if-eqz v10, :cond_e3a

    .line 687
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v12, "save to command"

    invoke-virtual {v10, v12}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 688
    const/4 v10, 0x2

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x2

    move-object/from16 v0, v36

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;->index_command:[B

    const/4 v14, 0x0

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 689
    const/4 v10, 0x2

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x3

    move-object/from16 v0, v36

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;->index_command:[B

    const/4 v14, 0x1

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 690
    const/4 v10, 0x3

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x2

    move-object/from16 v0, v36

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;->index_command:[B

    const/4 v14, 0x0

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    .line 691
    const/4 v10, 0x3

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v12, 0x3

    move-object/from16 v0, v36

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;->index_command:[B

    const/4 v14, 0x1

    aget-byte v13, v13, v14

    aput-byte v13, v10, v12

    goto :goto_dc8

    .line 694
    :cond_e3a
    const/4 v10, 0x2

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    const/4 v12, 0x0

    iput-boolean v12, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->pattern:Z

    .line 695
    const/4 v10, 0x3

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    const/4 v12, 0x0

    iput-boolean v12, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->pattern:Z

    goto/16 :goto_dc8

    .line 699
    .end local v36    # "item":Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;
    :cond_e50
    if-nez v27, :cond_e70

    .line 700
    const/4 v10, 0x0

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    const/4 v11, 0x0

    iput-boolean v11, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->pattern:Z

    .line 701
    const/4 v10, 0x1

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    const/4 v11, 0x0

    iput-boolean v11, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->pattern:Z

    .line 702
    const/4 v10, 0x2

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    const/4 v11, 0x0

    iput-boolean v11, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->pattern:Z

    .line 706
    :cond_e70
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v10

    new-array v0, v10, [Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    move-object/from16 v44, v0

    .line 707
    .local v44, "patchList":[Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    const/16 v59, 0x0

    .line 708
    .local v59, "u":I
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_e7e
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_e8f

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v36

    check-cast v36, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    .line 709
    .local v36, "item":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    aput-object v36, v44, v59

    .line 710
    add-int/lit8 v59, v59, 0x1

    .line 711
    goto :goto_e7e

    .line 714
    .end local v36    # "item":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    :cond_e8f
    const/16 v20, 0x0

    .line 715
    .local v20, "count":I
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchads;->fileblock:Z

    if-eqz v10, :cond_eae

    .line 716
    const-string v10, "String analysis."

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 717
    sget-object v10, Lcom/chelpus/root/utils/runpatchads;->print:Ljava/io/PrintStream;

    const-string v11, "String analysis."

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 718
    invoke-virtual/range {v32 .. v32}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    sget-object v11, Lcom/chelpus/root/utils/runpatchads;->sites:[[B

    const/4 v12, 0x0

    const/16 v13, 0x40

    invoke-static {v10, v11, v12, v13}, Lcom/chelpus/Utils;->setStringIds(Ljava/lang/String;[[BZB)I

    move-result v20

    .line 720
    :cond_eae
    :goto_eae
    if-lez v20, :cond_ebf

    .line 721
    add-int/lit8 v20, v20, -0x1

    .line 722
    const-string v10, "Site from AdsBlockList blocked!"

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 723
    sget-object v10, Lcom/chelpus/root/utils/runpatchads;->print:Ljava/io/PrintStream;

    const-string v11, "Site from AdsBlockList blocked!"

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_eae

    .line 725
    :cond_ebf
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Relaced strings:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual/range {v32 .. v32}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/String;

    const/4 v13, 0x0

    const-string v14, "com.google.android.gms.ads.identifier.service.START"

    aput-object v14, v12, v13

    const/4 v13, 0x0

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/String;

    const/16 v61, 0x0

    const-string v62, "com.google.android.gms.ads.identifier.service.STAPT"

    aput-object v62, v14, v61

    invoke-static {v11, v12, v13, v14}, Lcom/chelpus/Utils;->replaceStringIds(Ljava/lang/String;[Ljava/lang/String;Z[Ljava/lang/String;)I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 728
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v57

    .line 729
    .local v57, "time":J
    new-instance v10, Ljava/io/RandomAccessFile;

    const-string v11, "rw"

    move-object/from16 v0, v32

    invoke-direct {v10, v0, v11}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v9

    .line 730
    .local v9, "ChannelDex":Ljava/nio/channels/FileChannel;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Size file:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v9}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v11

    invoke-virtual {v10, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 731
    sget-object v10, Ljava/nio/channels/FileChannel$MapMode;->READ_WRITE:Ljava/nio/channels/FileChannel$MapMode;

    const-wide/16 v11, 0x0

    invoke-virtual {v9}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v13

    long-to-int v13, v13

    int-to-long v13, v13

    invoke-virtual/range {v9 .. v14}, Ljava/nio/channels/FileChannel;->map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;
    :try_end_f27
    .catch Ljava/io/FileNotFoundException; {:try_start_af3 .. :try_end_f27} :catch_a34
    .catch Ljava/lang/Exception; {:try_start_af3 .. :try_end_f27} :catch_ad6

    move-result-object v31

    .line 732
    .local v31, "fileBytes":Ljava/nio/MappedByteBuffer;
    const/16 v22, 0x0

    .line 734
    .local v22, "curentPos":I
    const/16 v34, 0x0

    .line 735
    .local v34, "i":I
    const/16 v24, 0x23e

    .local v24, "diaposon":I
    const/16 v25, 0x28

    .line 736
    .local v25, "diaposon_pak":I
    const/16 v54, 0x0

    .local v54, "start_for_diaposon":I
    const/16 v55, 0x0

    .line 739
    .local v55, "start_for_diaposon_pak":I
    const/16 v48, 0x0

    .line 740
    .local v48, "prervat":Z
    const/16 v46, 0x0

    .line 741
    .local v46, "period":I
    :cond_f38
    :goto_f38
    :try_start_f38
    invoke-virtual/range {v31 .. v31}, Ljava/nio/MappedByteBuffer;->hasRemaining()Z

    move-result v10

    if-eqz v10, :cond_109e

    .line 743
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchads;->createAPK:Z

    if-nez v10, :cond_f6b

    .line 744
    invoke-virtual/range {v31 .. v31}, Ljava/nio/MappedByteBuffer;->position()I

    move-result v10

    sub-int v10, v10, v46

    const v11, 0x249ef

    if-le v10, v11, :cond_f6b

    .line 745
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Progress size:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual/range {v31 .. v31}, Ljava/nio/MappedByteBuffer;->position()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 746
    invoke-virtual/range {v31 .. v31}, Ljava/nio/MappedByteBuffer;->position()I

    move-result v46

    .line 749
    :cond_f6b
    invoke-virtual/range {v31 .. v31}, Ljava/nio/MappedByteBuffer;->position()I

    move-result v22

    .line 750
    invoke-virtual/range {v31 .. v31}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v21

    .line 752
    .local v21, "curentByte":B
    const/16 v48, 0x0

    .line 754
    const/16 v16, 0x0

    .local v16, "b":I
    :goto_f77
    move-object/from16 v0, v44

    array-length v10, v0

    move/from16 v0, v16

    if-ge v0, v10, :cond_1225

    .line 755
    aget-object v45, v44, v16

    .line 756
    .local v45, "patches":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    move-object/from16 v0, v31

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 757
    move-object/from16 v0, v45

    iget-boolean v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->markerTrig:Z

    if-eqz v10, :cond_10d7

    const/4 v10, 0x2

    move/from16 v0, v16

    if-ne v0, v10, :cond_10d7

    .line 759
    add-int/lit8 v54, v54, 0x1

    .line 760
    move/from16 v0, v54

    move/from16 v1, v24

    if-ge v0, v1, :cond_11e0

    .line 762
    move-object/from16 v0, v45

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v11, 0x0

    aget-byte v10, v10, v11

    move/from16 v0, v21

    if-ne v0, v10, :cond_10d0

    .line 763
    move-object/from16 v0, v45

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repMask:[I

    const/4 v11, 0x0

    aget v10, v10, v11

    if-nez v10, :cond_fb5

    move-object/from16 v0, v45

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    const/4 v11, 0x0

    aput-byte v21, v10, v11

    .line 764
    :cond_fb5
    const/16 v34, 0x1

    .line 765
    add-int/lit8 v10, v22, 0x1

    move-object/from16 v0, v31

    invoke-virtual {v0, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 766
    invoke-virtual/range {v31 .. v31}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v49

    .line 768
    .local v49, "prufbyte":B
    :goto_fc2
    move-object/from16 v0, v45

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    aget-byte v10, v10, v34

    move/from16 v0, v49

    if-eq v0, v10, :cond_ff3

    move-object/from16 v0, v45

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origMask:[I

    aget v10, v10, v34

    const/4 v11, 0x1

    if-eq v10, v11, :cond_ff3

    move-object/from16 v0, v45

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origMask:[I

    aget v10, v10, v34

    const/16 v11, 0x14

    if-eq v10, v11, :cond_ff3

    move-object/from16 v0, v45

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origMask:[I

    aget v10, v10, v34

    const/16 v11, 0x15

    if-eq v10, v11, :cond_ff3

    move-object/from16 v0, v45

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origMask:[I

    aget v10, v10, v34

    const/16 v11, 0x17

    if-ne v10, v11, :cond_10d0

    .line 770
    :cond_ff3
    move-object/from16 v0, v45

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repMask:[I

    aget v10, v10, v34

    if-nez v10, :cond_1001

    move-object/from16 v0, v45

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    aput-byte v49, v10, v34

    .line 771
    :cond_1001
    move-object/from16 v0, v45

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repMask:[I

    aget v10, v10, v34

    const/16 v11, 0x14

    if-ne v10, v11, :cond_1014

    .line 772
    move-object/from16 v0, v45

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    and-int/lit8 v11, v49, 0xf

    int-to-byte v11, v11

    aput-byte v11, v10, v34

    .line 775
    :cond_1014
    move-object/from16 v0, v45

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repMask:[I

    aget v10, v10, v34

    const/16 v11, 0x15

    if-ne v10, v11, :cond_1029

    .line 776
    move-object/from16 v0, v45

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    and-int/lit8 v11, v49, 0xf

    add-int/lit8 v11, v11, 0x10

    int-to-byte v11, v11

    aput-byte v11, v10, v34

    .line 780
    :cond_1029
    add-int/lit8 v34, v34, 0x1

    .line 781
    move-object/from16 v0, v45

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    array-length v10, v10

    move/from16 v0, v34

    if-ne v0, v10, :cond_11da

    .line 783
    move-object/from16 v0, v31

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 784
    move-object/from16 v0, v45

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    move-object/from16 v0, v31

    invoke-virtual {v0, v10}, Ljava/nio/MappedByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 785
    invoke-virtual/range {v31 .. v31}, Ljava/nio/MappedByteBuffer;->force()Ljava/nio/MappedByteBuffer;

    .line 786
    move-object/from16 v0, v45

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->resultText:Ljava/lang/String;

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 787
    sget-object v10, Lcom/chelpus/root/utils/runpatchads;->print:Ljava/io/PrintStream;

    move-object/from16 v0, v45

    iget-object v11, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->resultText:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 788
    const/4 v10, 0x1

    move-object/from16 v0, v45

    iput-boolean v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->result:Z

    .line 789
    const/4 v10, 0x0

    move-object/from16 v0, v45

    iput-boolean v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->markerTrig:Z

    .line 790
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_1065
    :goto_1065
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_10c7

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    .line 791
    .local v23, "curpatch":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    move-object/from16 v0, v23

    iget-object v11, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->marker:Ljava/lang/String;

    move-object/from16 v0, v45

    iget-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->marker:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1065

    .line 792
    const/4 v11, 0x0

    move-object/from16 v0, v23

    iput-boolean v11, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->markerTrig:Z
    :try_end_1084
    .catch Ljava/lang/Exception; {:try_start_f38 .. :try_end_1084} :catch_1085
    .catch Ljava/io/FileNotFoundException; {:try_start_f38 .. :try_end_1084} :catch_a34

    goto :goto_1065

    .line 913
    .end local v16    # "b":I
    .end local v21    # "curentByte":B
    .end local v23    # "curpatch":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    .end local v45    # "patches":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    .end local v49    # "prufbyte":B
    :catch_1085
    move-exception v26

    .line 914
    .restart local v26    # "e":Ljava/lang/Exception;
    :try_start_1086
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, ""

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, v26

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 916
    .end local v26    # "e":Ljava/lang/Exception;
    :cond_109e
    invoke-virtual {v9}, Ljava/nio/channels/FileChannel;->close()V

    .line 917
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, ""

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v11

    sub-long v11, v11, v57

    const-wide/16 v13, 0x3e8

    div-long/2addr v11, v13

    invoke-virtual {v10, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 918
    const-string v10, "Analise Results:"

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z
    :try_end_10c5
    .catch Ljava/io/FileNotFoundException; {:try_start_1086 .. :try_end_10c5} :catch_a34
    .catch Ljava/lang/Exception; {:try_start_1086 .. :try_end_10c5} :catch_ad6

    goto/16 :goto_b4b

    .line 794
    .restart local v16    # "b":I
    .restart local v21    # "curentByte":B
    .restart local v45    # "patches":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    .restart local v49    # "prufbyte":B
    :cond_10c7
    const/16 v54, 0x0

    .line 795
    add-int/lit8 v10, v22, 0x1

    :try_start_10cb
    move-object/from16 v0, v31

    invoke-virtual {v0, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 802
    .end local v49    # "prufbyte":B
    :cond_10d0
    add-int/lit8 v10, v22, 0x1

    move-object/from16 v0, v31

    invoke-virtual {v0, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 814
    :cond_10d7
    :goto_10d7
    move-object/from16 v0, v45

    iget-boolean v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->markerTrig:Z

    if-nez v10, :cond_1221

    .line 815
    move-object/from16 v0, v45

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    const/4 v11, 0x0

    aget-byte v10, v10, v11

    move/from16 v0, v21

    if-ne v0, v10, :cond_1221

    move-object/from16 v0, v45

    iget-boolean v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->pattern:Z

    if-eqz v10, :cond_1221

    .line 817
    move-object/from16 v0, v45

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repMask:[I

    const/4 v11, 0x0

    aget v10, v10, v11

    if-nez v10, :cond_10fe

    move-object/from16 v0, v45

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    const/4 v11, 0x0

    aput-byte v21, v10, v11

    .line 818
    :cond_10fe
    const/16 v34, 0x1

    .line 819
    add-int/lit8 v10, v22, 0x1

    move-object/from16 v0, v31

    invoke-virtual {v0, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 820
    invoke-virtual/range {v31 .. v31}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v49

    .line 822
    .restart local v49    # "prufbyte":B
    :goto_110b
    move-object/from16 v0, v45

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    aget-byte v10, v10, v34

    move/from16 v0, v49

    if-eq v0, v10, :cond_113c

    move-object/from16 v0, v45

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origMask:[I

    aget v10, v10, v34

    const/4 v11, 0x1

    if-eq v10, v11, :cond_113c

    move-object/from16 v0, v45

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origMask:[I

    aget v10, v10, v34

    const/16 v11, 0x14

    if-eq v10, v11, :cond_113c

    move-object/from16 v0, v45

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origMask:[I

    aget v10, v10, v34

    const/16 v11, 0x15

    if-eq v10, v11, :cond_113c

    move-object/from16 v0, v45

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origMask:[I

    aget v10, v10, v34

    const/16 v11, 0x17

    if-ne v10, v11, :cond_121a

    .line 824
    :cond_113c
    move-object/from16 v0, v45

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repMask:[I

    aget v10, v10, v34

    if-nez v10, :cond_114a

    move-object/from16 v0, v45

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    aput-byte v49, v10, v34

    .line 825
    :cond_114a
    move-object/from16 v0, v45

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repMask:[I

    aget v10, v10, v34

    const/16 v11, 0x14

    if-ne v10, v11, :cond_115d

    .line 826
    move-object/from16 v0, v45

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    and-int/lit8 v11, v49, 0xf

    int-to-byte v11, v11

    aput-byte v11, v10, v34

    .line 829
    :cond_115d
    move-object/from16 v0, v45

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repMask:[I

    aget v10, v10, v34

    const/16 v11, 0x15

    if-ne v10, v11, :cond_1172

    .line 830
    move-object/from16 v0, v45

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    and-int/lit8 v11, v49, 0xf

    add-int/lit8 v11, v11, 0x10

    int-to-byte v11, v11

    aput-byte v11, v10, v34

    .line 834
    :cond_1172
    add-int/lit8 v34, v34, 0x1

    .line 835
    move-object/from16 v0, v45

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->origByte:[B

    array-length v10, v10

    move/from16 v0, v34

    if-ne v0, v10, :cond_1214

    .line 837
    move-object/from16 v0, v31

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 838
    move-object/from16 v0, v45

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->repByte:[B

    move-object/from16 v0, v31

    invoke-virtual {v0, v10}, Ljava/nio/MappedByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 839
    invoke-virtual/range {v31 .. v31}, Ljava/nio/MappedByteBuffer;->force()Ljava/nio/MappedByteBuffer;

    .line 840
    move-object/from16 v0, v45

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->resultText:Ljava/lang/String;

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 841
    sget-object v10, Lcom/chelpus/root/utils/runpatchads;->print:Ljava/io/PrintStream;

    move-object/from16 v0, v45

    iget-object v11, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->resultText:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 842
    const/4 v10, 0x1

    move-object/from16 v0, v45

    iput-boolean v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->result:Z

    .line 843
    move-object/from16 v0, v45

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->marker:Ljava/lang/String;

    const-string v11, ""

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_121a

    .line 844
    const/4 v10, 0x1

    move-object/from16 v0, v45

    iput-boolean v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->markerTrig:Z

    .line 845
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_11ba
    :goto_11ba
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_121a

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    .line 846
    .restart local v23    # "curpatch":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    move-object/from16 v0, v23

    iget-object v11, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->marker:Ljava/lang/String;

    move-object/from16 v0, v45

    iget-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->marker:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_11ba

    .line 847
    const/4 v11, 0x1

    move-object/from16 v0, v23

    iput-boolean v11, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->markerTrig:Z

    goto :goto_11ba

    .line 799
    .end local v23    # "curpatch":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    :cond_11da
    invoke-virtual/range {v31 .. v31}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v49

    goto/16 :goto_fc2

    .line 804
    .end local v49    # "prufbyte":B
    :cond_11e0
    const/4 v10, 0x0

    move-object/from16 v0, v45

    iput-boolean v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->markerTrig:Z

    .line 805
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_11e9
    :goto_11e9
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_1209

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;

    .line 806
    .restart local v23    # "curpatch":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    move-object/from16 v0, v23

    iget-object v11, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->marker:Ljava/lang/String;

    move-object/from16 v0, v45

    iget-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->marker:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_11e9

    .line 807
    const/4 v11, 0x0

    move-object/from16 v0, v23

    iput-boolean v11, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;->markerTrig:Z

    goto :goto_11e9

    .line 809
    .end local v23    # "curpatch":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    :cond_1209
    const/16 v54, 0x0

    .line 810
    add-int/lit8 v10, v22, 0x1

    move-object/from16 v0, v31

    invoke-virtual {v0, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    goto/16 :goto_10d7

    .line 853
    .restart local v49    # "prufbyte":B
    :cond_1214
    invoke-virtual/range {v31 .. v31}, Ljava/nio/MappedByteBuffer;->get()B

    move-result v49

    goto/16 :goto_110b

    .line 855
    :cond_121a
    add-int/lit8 v10, v22, 0x1

    move-object/from16 v0, v31

    invoke-virtual {v0, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 754
    .end local v49    # "prufbyte":B
    :cond_1221
    add-int/lit8 v16, v16, 0x1

    goto/16 :goto_f77

    .line 911
    .end local v45    # "patches":Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    :cond_1225
    if-nez v48, :cond_f38

    add-int/lit8 v10, v22, 0x1

    move-object/from16 v0, v31

    invoke-virtual {v0, v10}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;
    :try_end_122e
    .catch Ljava/lang/Exception; {:try_start_10cb .. :try_end_122e} :catch_1085
    .catch Ljava/io/FileNotFoundException; {:try_start_10cb .. :try_end_122e} :catch_a34

    goto/16 :goto_f38

    .line 934
    .end local v9    # "ChannelDex":Ljava/nio/channels/FileChannel;
    .end local v16    # "b":I
    .end local v18    # "commands":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/CommandItem;>;"
    .end local v20    # "count":I
    .end local v21    # "curentByte":B
    .end local v22    # "curentPos":I
    .end local v24    # "diaposon":I
    .end local v25    # "diaposon_pak":I
    .end local v27    # "f1":Z
    .end local v28    # "f2":Z
    .end local v29    # "f3":Z
    .end local v31    # "fileBytes":Ljava/nio/MappedByteBuffer;
    .end local v32    # "filepatch":Ljava/io/File;
    .end local v34    # "i":I
    .end local v41    # "of_to_patch":I
    .end local v42    # "offsets":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/StringItem;>;"
    .end local v44    # "patchList":[Lcom/android/vending/billing/InAppBillingService/LUCK/PatchesItemAuto;
    .end local v46    # "period":I
    .end local v48    # "prervat":Z
    .end local v54    # "start_for_diaposon":I
    .end local v55    # "start_for_diaposon_pak":I
    .end local v56    # "strings":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v57    # "time":J
    .end local v59    # "u":I
    :cond_1230
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchads;->createAPK:Z

    if-nez v10, :cond_1281

    .line 935
    const/4 v10, 0x3

    aget-object v10, p0, v10

    sget-object v11, Lcom/chelpus/root/utils/runpatchads;->classesFiles:Ljava/util/ArrayList;

    const/4 v12, 0x2

    aget-object v12, p0, v12

    sget-object v13, Lcom/chelpus/root/utils/runpatchads;->uid:Ljava/lang/String;

    const/4 v14, 0x2

    aget-object v14, p0, v14

    sget-object v60, Lcom/chelpus/root/utils/runpatchads;->uid:Ljava/lang/String;

    move-object/from16 v0, v60

    invoke-static {v14, v0}, Lcom/chelpus/Utils;->getOdexForCreate(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-static {v10, v11, v12, v13, v14}, Lcom/chelpus/Utils;->create_ODEX_root(Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v51

    .line 936
    .local v51, "r":I
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "chelpus_return_"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v51

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 937
    if-nez v51, :cond_1281

    sget-boolean v10, Lcom/chelpus/root/utils/runpatchads;->ART:Z

    if-nez v10, :cond_1281

    .line 938
    const/4 v10, 0x1

    aget-object v10, p0, v10

    const/4 v11, 0x2

    aget-object v11, p0, v11

    const/4 v12, 0x2

    aget-object v12, p0, v12

    const/4 v13, 0x1

    invoke-static {v12, v13}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v12

    sget-object v13, Lcom/chelpus/root/utils/runpatchads;->uid:Ljava/lang/String;

    const/4 v14, 0x3

    aget-object v14, p0, v14

    invoke-static {v10, v11, v12, v13, v14}, Lcom/chelpus/Utils;->afterPatch(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 943
    .end local v51    # "r":I
    :cond_1281
    sget-boolean v10, Lcom/chelpus/root/utils/runpatchads;->createAPK:Z

    if-nez v10, :cond_1288

    invoke-static {}, Lcom/chelpus/Utils;->exitFromRootJava()V

    .line 944
    :cond_1288
    move-object/from16 v0, v47

    iget-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/LogOutputStream;->allresult:Ljava/lang/String;

    sput-object v10, Lcom/chelpus/root/utils/runpatchads;->result:Ljava/lang/String;

    .line 945
    return-void

    .line 114
    .end local v3    # "origStr":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v4    # "replStr":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v5    # "trigger":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Boolean;>;"
    .end local v6    # "ResultText":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v7    # "markers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v8    # "dontConvert":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Boolean;>;"
    .end local v19    # "conv":Z
    .end local v53    # "sitesFromFile":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
    :catch_128f
    move-exception v10

    goto/16 :goto_187

    .line 113
    :catch_1292
    move-exception v10

    goto/16 :goto_187

    .line 110
    :catch_1295
    move-exception v10

    goto/16 :goto_179
.end method

.method public static unzipART(Ljava/io/File;)V
    .registers 23
    .param p0, "apk"    # Ljava/io/File;

    .prologue
    .line 1106
    const/4 v7, 0x0

    .local v7, "found1":Z
    const/4 v8, 0x0

    .line 1108
    .local v8, "found2":Z
    :try_start_2
    new-instance v6, Ljava/io/FileInputStream;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 1109
    .local v6, "fin":Ljava/io/FileInputStream;
    new-instance v16, Ljava/util/zip/ZipInputStream;

    move-object/from16 v0, v16

    invoke-direct {v0, v6}, Ljava/util/zip/ZipInputStream;-><init>(Ljava/io/InputStream;)V

    .line 1110
    .local v16, "zin":Ljava/util/zip/ZipInputStream;
    const/4 v15, 0x0

    .line 1112
    .local v15, "ze":Ljava/util/zip/ZipEntry;
    invoke-virtual/range {v16 .. v16}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    move-result-object v15

    .line 1113
    const/4 v14, 0x1

    .line 1114
    .local v14, "search":Z
    :goto_16
    if-eqz v15, :cond_227

    if-eqz v14, :cond_227

    .line 1120
    invoke-virtual {v15}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v11

    .line 1122
    .local v11, "haystack":Ljava/lang/String;
    invoke-virtual {v11}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v18

    const-string v19, "classes"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_19f

    const-string v18, ".dex"

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_19f

    const-string v18, "/"

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_19f

    .line 1124
    new-instance v9, Ljava/io/FileOutputStream;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v19, Lcom/chelpus/root/utils/runpatchads;->sddir:Ljava/lang/String;

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "/"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-direct {v9, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 1126
    .local v9, "fout":Ljava/io/FileOutputStream;
    const/16 v18, 0x800

    move/from16 v0, v18

    new-array v2, v0, [B

    .line 1129
    .local v2, "buffer":[B
    :goto_66
    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/util/zip/ZipInputStream;->read([B)I

    move-result v12

    .local v12, "length":I
    const/16 v18, -0x1

    move/from16 v0, v18

    if-eq v12, v0, :cond_13e

    .line 1130
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v9, v2, v0, v12}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_79
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_79} :catch_7a

    goto :goto_66

    .line 1162
    .end local v2    # "buffer":[B
    .end local v6    # "fin":Ljava/io/FileInputStream;
    .end local v9    # "fout":Ljava/io/FileOutputStream;
    .end local v11    # "haystack":Ljava/lang/String;
    .end local v12    # "length":I
    .end local v14    # "search":Z
    .end local v15    # "ze":Ljava/util/zip/ZipEntry;
    .end local v16    # "zin":Ljava/util/zip/ZipInputStream;
    :catch_7a
    move-exception v4

    .line 1164
    .local v4, "e":Ljava/lang/Exception;
    :try_start_7b
    new-instance v17, Lnet/lingala/zip4j/core/ZipFile;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lnet/lingala/zip4j/core/ZipFile;-><init>(Ljava/io/File;)V

    .line 1168
    .local v17, "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    const-string v18, "classes.dex"

    sget-object v19, Lcom/chelpus/root/utils/runpatchads;->sddir:Ljava/lang/String;

    invoke-virtual/range {v17 .. v19}, Lnet/lingala/zip4j/core/ZipFile;->extractFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 1169
    sget-object v18, Lcom/chelpus/root/utils/runpatchads;->classesFiles:Ljava/util/ArrayList;

    new-instance v19, Ljava/io/File;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v21, Lcom/chelpus/root/utils/runpatchads;->sddir:Ljava/lang/String;

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "classes.dex"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v18 .. v19}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1170
    const/16 v18, 0x3

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const-string v20, "chmod"

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, "777"

    aput-object v20, v18, v19

    const/16 v19, 0x2

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v21, Lcom/chelpus/root/utils/runpatchads;->sddir:Ljava/lang/String;

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "classes.dex"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    aput-object v20, v18, v19

    invoke-static/range {v18 .. v18}, Lcom/chelpus/Utils;->cmdParam([Ljava/lang/String;)Ljava/lang/String;

    .line 1171
    const-string v18, "AndroidManifest.xml"

    sget-object v19, Lcom/chelpus/root/utils/runpatchads;->sddir:Ljava/lang/String;

    invoke-virtual/range {v17 .. v19}, Lnet/lingala/zip4j/core/ZipFile;->extractFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 1172
    const/16 v18, 0x3

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const-string v20, "chmod"

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, "777"

    aput-object v20, v18, v19

    const/16 v19, 0x2

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v21, Lcom/chelpus/root/utils/runpatchads;->sddir:Ljava/lang/String;

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "AndroidManifest.xml"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    aput-object v20, v18, v19

    invoke-static/range {v18 .. v18}, Lcom/chelpus/Utils;->cmdParam([Ljava/lang/String;)Ljava/lang/String;
    :try_end_123
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_7b .. :try_end_123} :catch_235
    .catch Ljava/lang/Exception; {:try_start_7b .. :try_end_123} :catch_26a

    .line 1181
    .end local v17    # "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    :goto_123
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Exception e"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v4}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 1185
    .end local v4    # "e":Ljava/lang/Exception;
    :goto_13d
    return-void

    .line 1134
    .restart local v2    # "buffer":[B
    .restart local v6    # "fin":Ljava/io/FileInputStream;
    .restart local v9    # "fout":Ljava/io/FileOutputStream;
    .restart local v11    # "haystack":Ljava/lang/String;
    .restart local v12    # "length":I
    .restart local v14    # "search":Z
    .restart local v15    # "ze":Ljava/util/zip/ZipEntry;
    .restart local v16    # "zin":Ljava/util/zip/ZipInputStream;
    :cond_13e
    :try_start_13e
    invoke-virtual/range {v16 .. v16}, Ljava/util/zip/ZipInputStream;->closeEntry()V

    .line 1135
    invoke-virtual {v9}, Ljava/io/FileOutputStream;->close()V

    .line 1136
    sget-object v18, Lcom/chelpus/root/utils/runpatchads;->classesFiles:Ljava/util/ArrayList;

    new-instance v19, Ljava/io/File;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v21, Lcom/chelpus/root/utils/runpatchads;->sddir:Ljava/lang/String;

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v18 .. v19}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1137
    const/16 v18, 0x3

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const-string v20, "chmod"

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, "777"

    aput-object v20, v18, v19

    const/16 v19, 0x2

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v21, Lcom/chelpus/root/utils/runpatchads;->sddir:Ljava/lang/String;

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    aput-object v20, v18, v19

    invoke-static/range {v18 .. v18}, Lcom/chelpus/Utils;->cmdParam([Ljava/lang/String;)Ljava/lang/String;

    .line 1140
    .end local v2    # "buffer":[B
    .end local v9    # "fout":Ljava/io/FileOutputStream;
    .end local v12    # "length":I
    :cond_19f
    const-string v18, "AndroidManifest.xml"

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_222

    .line 1141
    new-instance v10, Ljava/io/FileOutputStream;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v19, Lcom/chelpus/root/utils/runpatchads;->sddir:Ljava/lang/String;

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "/"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "AndroidManifest.xml"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-direct {v10, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 1143
    .local v10, "fout2":Ljava/io/FileOutputStream;
    const/16 v18, 0x800

    move/from16 v0, v18

    new-array v3, v0, [B

    .line 1145
    .local v3, "buffer2":[B
    :goto_1d1
    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/util/zip/ZipInputStream;->read([B)I

    move-result v13

    .local v13, "length2":I
    const/16 v18, -0x1

    move/from16 v0, v18

    if-eq v13, v0, :cond_1e5

    .line 1146
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v10, v3, v0, v13}, Ljava/io/FileOutputStream;->write([BII)V

    goto :goto_1d1

    .line 1148
    :cond_1e5
    invoke-virtual/range {v16 .. v16}, Ljava/util/zip/ZipInputStream;->closeEntry()V

    .line 1149
    invoke-virtual {v10}, Ljava/io/FileOutputStream;->close()V

    .line 1150
    const/16 v18, 0x3

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const-string v20, "chmod"

    aput-object v20, v18, v19

    const/16 v19, 0x1

    const-string v20, "777"

    aput-object v20, v18, v19

    const/16 v19, 0x2

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v21, Lcom/chelpus/root/utils/runpatchads;->sddir:Ljava/lang/String;

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "AndroidManifest.xml"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    aput-object v20, v18, v19

    invoke-static/range {v18 .. v18}, Lcom/chelpus/Utils;->cmdParam([Ljava/lang/String;)Ljava/lang/String;

    .line 1151
    const/4 v8, 0x1

    .line 1153
    .end local v3    # "buffer2":[B
    .end local v10    # "fout2":Ljava/io/FileOutputStream;
    .end local v13    # "length2":I
    :cond_222
    if-eqz v7, :cond_22f

    if-eqz v8, :cond_22f

    .line 1154
    const/4 v14, 0x0

    .line 1160
    .end local v11    # "haystack":Ljava/lang/String;
    :cond_227
    invoke-virtual/range {v16 .. v16}, Ljava/util/zip/ZipInputStream;->close()V

    .line 1161
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V

    goto/16 :goto_13d

    .line 1157
    .restart local v11    # "haystack":Ljava/lang/String;
    :cond_22f
    invoke-virtual/range {v16 .. v16}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;
    :try_end_232
    .catch Ljava/lang/Exception; {:try_start_13e .. :try_end_232} :catch_7a

    move-result-object v15

    .line 1159
    goto/16 :goto_16

    .line 1174
    .end local v6    # "fin":Ljava/io/FileInputStream;
    .end local v11    # "haystack":Ljava/lang/String;
    .end local v14    # "search":Z
    .end local v15    # "ze":Ljava/util/zip/ZipEntry;
    .end local v16    # "zin":Ljava/util/zip/ZipInputStream;
    .restart local v4    # "e":Ljava/lang/Exception;
    :catch_235
    move-exception v5

    .line 1175
    .local v5, "e1":Lnet/lingala/zip4j/exception/ZipException;
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Error classes.dex decompress! "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 1176
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Exception e1"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v4}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    goto/16 :goto_123

    .line 1177
    .end local v5    # "e1":Lnet/lingala/zip4j/exception/ZipException;
    :catch_26a
    move-exception v5

    .line 1178
    .local v5, "e1":Ljava/lang/Exception;
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Error classes.dex decompress! "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 1179
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Exception e1"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v4}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    goto/16 :goto_123
.end method

.method public static unzipSD(Ljava/io/File;)V
    .registers 14
    .param p0, "apk"    # Ljava/io/File;

    .prologue
    .line 1059
    :try_start_0
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 1060
    .local v3, "fin":Ljava/io/FileInputStream;
    new-instance v7, Ljava/util/zip/ZipInputStream;

    invoke-direct {v7, v3}, Ljava/util/zip/ZipInputStream;-><init>(Ljava/io/InputStream;)V

    .line 1061
    .local v7, "zin":Ljava/util/zip/ZipInputStream;
    const/4 v6, 0x0

    .line 1062
    .local v6, "ze":Ljava/util/zip/ZipEntry;
    :cond_b
    :goto_b
    invoke-virtual {v7}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    move-result-object v6

    if-eqz v6, :cond_e0

    .line 1066
    invoke-virtual {v6}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v9

    const-string v10, "classes"

    invoke-virtual {v9, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_b

    invoke-virtual {v6}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v9

    const-string v10, ".dex"

    invoke-virtual {v9, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_b

    invoke-virtual {v6}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v9

    const-string v10, "/"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_b

    .line 1067
    new-instance v4, Ljava/io/FileOutputStream;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v10, Lcom/chelpus/root/utils/runpatchads;->sddir:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/Modified/"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v6}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v4, v9}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 1069
    .local v4, "fout":Ljava/io/FileOutputStream;
    const/16 v9, 0x400

    new-array v0, v9, [B

    .line 1071
    .local v0, "buffer":[B
    :goto_5f
    invoke-virtual {v7, v0}, Ljava/util/zip/ZipInputStream;->read([B)I

    move-result v5

    .local v5, "length":I
    const/4 v9, -0x1

    if-eq v5, v9, :cond_b1

    .line 1072
    const/4 v9, 0x0

    invoke-virtual {v4, v0, v9, v5}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_6a
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_6a} :catch_6b

    goto :goto_5f

    .line 1084
    .end local v0    # "buffer":[B
    .end local v3    # "fin":Ljava/io/FileInputStream;
    .end local v4    # "fout":Ljava/io/FileOutputStream;
    .end local v5    # "length":I
    .end local v6    # "ze":Ljava/util/zip/ZipEntry;
    .end local v7    # "zin":Ljava/util/zip/ZipInputStream;
    :catch_6b
    move-exception v1

    .line 1086
    .local v1, "e":Ljava/lang/Exception;
    :try_start_6c
    new-instance v8, Lnet/lingala/zip4j/core/ZipFile;

    invoke-direct {v8, p0}, Lnet/lingala/zip4j/core/ZipFile;-><init>(Ljava/io/File;)V

    .line 1090
    .local v8, "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    const-string v9, "classes.dex"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v11, Lcom/chelpus/root/utils/runpatchads;->sddir:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "/Modified/"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Lnet/lingala/zip4j/core/ZipFile;->extractFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 1091
    sget-object v9, Lcom/chelpus/root/utils/runpatchads;->classesFiles:Ljava/util/ArrayList;

    new-instance v10, Ljava/io/File;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v12, Lcom/chelpus/root/utils/runpatchads;->sddir:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/Modified/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "classes.dex"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_b0
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_6c .. :try_end_b0} :catch_e7
    .catch Ljava/lang/Exception; {:try_start_6c .. :try_end_b0} :catch_119

    .line 1103
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v8    # "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    :goto_b0
    return-void

    .line 1074
    .restart local v0    # "buffer":[B
    .restart local v3    # "fin":Ljava/io/FileInputStream;
    .restart local v4    # "fout":Ljava/io/FileOutputStream;
    .restart local v5    # "length":I
    .restart local v6    # "ze":Ljava/util/zip/ZipEntry;
    .restart local v7    # "zin":Ljava/util/zip/ZipInputStream;
    :cond_b1
    :try_start_b1
    invoke-virtual {v7}, Ljava/util/zip/ZipInputStream;->closeEntry()V

    .line 1075
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V

    .line 1076
    sget-object v9, Lcom/chelpus/root/utils/runpatchads;->classesFiles:Ljava/util/ArrayList;

    new-instance v10, Ljava/io/File;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v12, Lcom/chelpus/root/utils/runpatchads;->sddir:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/Modified/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v6}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_b

    .line 1082
    .end local v0    # "buffer":[B
    .end local v4    # "fout":Ljava/io/FileOutputStream;
    .end local v5    # "length":I
    :cond_e0
    invoke-virtual {v7}, Ljava/util/zip/ZipInputStream;->close()V

    .line 1083
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_e6
    .catch Ljava/lang/Exception; {:try_start_b1 .. :try_end_e6} :catch_6b

    goto :goto_b0

    .line 1093
    .end local v3    # "fin":Ljava/io/FileInputStream;
    .end local v6    # "ze":Ljava/util/zip/ZipEntry;
    .end local v7    # "zin":Ljava/util/zip/ZipInputStream;
    .restart local v1    # "e":Ljava/lang/Exception;
    :catch_e7
    move-exception v2

    .line 1094
    .local v2, "e1":Lnet/lingala/zip4j/exception/ZipException;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Error classes.dex decompress! "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 1095
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Exception e1"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    goto :goto_b0

    .line 1096
    .end local v2    # "e1":Lnet/lingala/zip4j/exception/ZipException;
    :catch_119
    move-exception v2

    .line 1097
    .local v2, "e1":Ljava/lang/Exception;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Error classes.dex decompress! "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    .line 1098
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Exception e1"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/chelpus/Utils;->sendFromRoot(Ljava/lang/String;)Z

    goto/16 :goto_b0
.end method
