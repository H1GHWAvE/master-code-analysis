.class public Lcom/chelpus/root/utils/odex;
.super Ljava/lang/Object;
.source "odex.java"


# static fields
.field private static dalvikDexIn:Ljava/lang/String;

.field private static dalvikDexIn2:Ljava/lang/String;

.field public static dirapp:Ljava/lang/String;

.field public static odexpatch:Z

.field public static system:Z

.field public static toolsfiledir:Ljava/lang/String;

.field public static uid:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    const/4 v1, 0x0

    .line 10
    const-string v0, "/cache/dalvik-cache/data@app@zamenitetodelo-1.apk@classes.dex"

    sput-object v0, Lcom/chelpus/root/utils/odex;->dalvikDexIn2:Ljava/lang/String;

    .line 11
    const-string v0, "/data/dalvik-cache/data@app@zamenitetodelo-1.apk@classes.dex"

    sput-object v0, Lcom/chelpus/root/utils/odex;->dalvikDexIn:Ljava/lang/String;

    .line 12
    const-string v0, "/data/app/"

    sput-object v0, Lcom/chelpus/root/utils/odex;->dirapp:Ljava/lang/String;

    .line 13
    sput-boolean v1, Lcom/chelpus/root/utils/odex;->system:Z

    .line 14
    sput-boolean v1, Lcom/chelpus/root/utils/odex;->odexpatch:Z

    .line 15
    const-string v0, ""

    sput-object v0, Lcom/chelpus/root/utils/odex;->toolsfiledir:Ljava/lang/String;

    .line 16
    const-string v0, ""

    sput-object v0, Lcom/chelpus/root/utils/odex;->uid:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .registers 17
    .param p0, "paramArrayOfString"    # [Ljava/lang/String;

    .prologue
    .line 24
    new-instance v12, Lcom/chelpus/root/utils/odex$1;

    invoke-direct {v12}, Lcom/chelpus/root/utils/odex$1;-><init>()V

    invoke-static {v12}, Lcom/chelpus/Utils;->startRootJava(Ljava/lang/Object;)V

    .line 25
    const/4 v12, 0x1

    aget-object v12, p0, v12

    sput-object v12, Lcom/chelpus/root/utils/odex;->dirapp:Ljava/lang/String;

    .line 26
    const/4 v12, 0x2

    aget-object v12, p0, v12

    const-string v13, "not_system"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1b

    const/4 v12, 0x0

    sput-boolean v12, Lcom/chelpus/root/utils/odex;->system:Z

    .line 27
    :cond_1b
    const/4 v12, 0x2

    aget-object v12, p0, v12

    const-string v13, "system"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_29

    const/4 v12, 0x1

    sput-boolean v12, Lcom/chelpus/root/utils/odex;->system:Z

    .line 28
    :cond_29
    const/4 v12, 0x3

    aget-object v12, p0, v12

    if-eqz v12, :cond_33

    const/4 v12, 0x3

    aget-object v12, p0, v12

    sput-object v12, Lcom/chelpus/root/utils/odex;->toolsfiledir:Ljava/lang/String;

    .line 29
    :cond_33
    const/4 v12, 0x4

    aget-object v12, p0, v12

    if-eqz v12, :cond_3d

    const/4 v12, 0x4

    aget-object v12, p0, v12

    sput-object v12, Lcom/chelpus/root/utils/odex;->uid:Ljava/lang/String;

    .line 30
    :cond_3d
    const/4 v12, 0x0

    aget-object v12, p0, v12

    invoke-static {v12}, Lcom/chelpus/Utils;->kill(Ljava/lang/String;)V

    .line 31
    sget-object v12, Lcom/chelpus/root/utils/odex;->dirapp:Ljava/lang/String;

    const-string v13, "RW"

    invoke-static {v12, v13}, Lcom/chelpus/Utils;->remount(Ljava/lang/String;Ljava/lang/String;)Z

    .line 32
    new-instance v0, Ljava/io/File;

    sget-object v12, Lcom/chelpus/root/utils/odex;->dirapp:Ljava/lang/String;

    invoke-direct {v0, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 33
    .local v0, "appapk":Ljava/io/File;
    new-instance v1, Ljava/io/File;

    sget-object v12, Lcom/chelpus/root/utils/odex;->dirapp:Ljava/lang/String;

    sget-object v13, Lcom/chelpus/root/utils/odex;->uid:Ljava/lang/String;

    invoke-static {v12, v13}, Lcom/chelpus/Utils;->getOdexForCreate(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v1, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 34
    .local v1, "appodex":Ljava/io/File;
    sget-boolean v12, Lcom/chelpus/root/utils/odex;->system:Z

    if-eqz v12, :cond_7f

    .line 35
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v12

    if-eqz v12, :cond_7f

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v12

    if-eqz v12, :cond_7f

    invoke-static {v0}, Lcom/chelpus/Utils;->classes_test(Ljava/io/File;)Z

    move-result v12

    if-nez v12, :cond_7f

    const/4 v12, 0x1

    sput-boolean v12, Lcom/chelpus/root/utils/odex;->odexpatch:Z

    .line 36
    move-object v8, v1

    .line 37
    .local v8, "localFile2":Ljava/io/File;
    sget-object v12, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v13, "\nOdex Application.\nOnly ODEX patch is enabled.\n"

    invoke-virtual {v12, v13}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 40
    .end local v8    # "localFile2":Ljava/io/File;
    :cond_7f
    sget-boolean v12, Lcom/chelpus/root/utils/odex;->odexpatch:Z

    if-nez v12, :cond_320

    .line 41
    sget-object v12, Lcom/chelpus/root/utils/odex;->dalvikDexIn:Ljava/lang/String;

    const-string v13, "zamenitetodelo"

    const/4 v14, 0x0

    aget-object v14, p0, v14

    invoke-virtual {v12, v13, v14}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    .line 45
    .local v4, "dalvikDex":Ljava/lang/String;
    :try_start_8e
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 46
    .local v7, "localFile1":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v12

    if-nez v12, :cond_a6

    new-instance v7, Ljava/io/File;

    .end local v7    # "localFile1":Ljava/io/File;
    const-string v12, "-1"

    const-string v13, "-2"

    invoke-virtual {v4, v12, v13}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v7, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 47
    .restart local v7    # "localFile1":Ljava/io/File;
    :cond_a6
    new-instance v8, Ljava/io/File;

    const-string v12, "-1"

    const-string v13, ""

    invoke-virtual {v4, v12, v13}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v8, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 48
    .restart local v8    # "localFile2":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v12

    if-eqz v12, :cond_ba

    move-object v8, v7

    .line 49
    :cond_ba
    const-string v12, "data@app"

    const-string v13, "mnt@asec"

    invoke-virtual {v4, v12, v13}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    .line 50
    .local v5, "dalvikDexTemp":Ljava/lang/String;
    const-string v12, ".apk@classes.dex"

    const-string v13, "@pkg.apk@classes.dex"

    invoke-virtual {v5, v12, v13}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    .line 51
    new-instance v7, Ljava/io/File;

    .end local v7    # "localFile1":Ljava/io/File;
    invoke-direct {v7, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 52
    .restart local v7    # "localFile1":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v12

    if-eqz v12, :cond_d6

    move-object v8, v7

    .line 53
    :cond_d6
    new-instance v7, Ljava/io/File;

    .end local v7    # "localFile1":Ljava/io/File;
    const-string v12, "-1"

    const-string v13, "-2"

    invoke-virtual {v5, v12, v13}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v7, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 54
    .restart local v7    # "localFile1":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v12

    if-eqz v12, :cond_ea

    move-object v8, v7

    .line 55
    :cond_ea
    new-instance v7, Ljava/io/File;

    .end local v7    # "localFile1":Ljava/io/File;
    const-string v12, "-1"

    const-string v13, ""

    invoke-virtual {v5, v12, v13}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v7, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 56
    .restart local v7    # "localFile1":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v12

    if-eqz v12, :cond_fe

    move-object v8, v7

    .line 58
    :cond_fe
    sget-object v12, Lcom/chelpus/root/utils/odex;->dalvikDexIn2:Ljava/lang/String;

    const-string v13, "zamenitetodelo"

    const/4 v14, 0x0

    aget-object v14, p0, v14

    invoke-virtual {v12, v13, v14}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    .line 59
    new-instance v7, Ljava/io/File;

    .end local v7    # "localFile1":Ljava/io/File;
    invoke-direct {v7, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 60
    .restart local v7    # "localFile1":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v12

    if-nez v12, :cond_121

    new-instance v7, Ljava/io/File;

    .end local v7    # "localFile1":Ljava/io/File;
    const-string v12, "-1"

    const-string v13, "-2"

    invoke-virtual {v4, v12, v13}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v7, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 62
    .restart local v7    # "localFile1":Ljava/io/File;
    :cond_121
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v12

    if-nez v12, :cond_134

    new-instance v7, Ljava/io/File;

    .end local v7    # "localFile1":Ljava/io/File;
    const-string v12, "-1"

    const-string v13, ""

    invoke-virtual {v4, v12, v13}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v7, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 63
    .restart local v7    # "localFile1":Ljava/io/File;
    :cond_134
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v12

    if-eqz v12, :cond_13b

    move-object v8, v7

    .line 64
    :cond_13b
    const-string v12, "data@app"

    const-string v13, "mnt@asec"

    invoke-virtual {v4, v12, v13}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    .line 65
    const-string v12, ".apk@classes.dex"

    const-string v13, "@pkg.apk@classes.dex"

    invoke-virtual {v5, v12, v13}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    .line 66
    new-instance v7, Ljava/io/File;

    .end local v7    # "localFile1":Ljava/io/File;
    invoke-direct {v7, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 67
    .restart local v7    # "localFile1":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v12

    if-eqz v12, :cond_157

    move-object v8, v7

    .line 68
    :cond_157
    new-instance v7, Ljava/io/File;

    .end local v7    # "localFile1":Ljava/io/File;
    const-string v12, "-1"

    const-string v13, "-2"

    invoke-virtual {v5, v12, v13}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v7, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 69
    .restart local v7    # "localFile1":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v12

    if-eqz v12, :cond_16b

    move-object v8, v7

    .line 70
    :cond_16b
    new-instance v7, Ljava/io/File;

    .end local v7    # "localFile1":Ljava/io/File;
    const-string v12, "-1"

    const-string v13, ""

    invoke-virtual {v5, v12, v13}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v7, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 71
    .restart local v7    # "localFile1":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v12

    if-eqz v12, :cond_17f

    move-object v8, v7

    .line 73
    :cond_17f
    sget-object v12, Lcom/chelpus/root/utils/odex;->dalvikDexIn:Ljava/lang/String;

    const-string v13, "zamenitetodelo"

    const/4 v14, 0x0

    aget-object v14, p0, v14

    invoke-virtual {v12, v13, v14}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    .line 74
    const-string v12, "/data/"

    const-string v13, "/sd-ext/data/"

    invoke-virtual {v4, v12, v13}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    .line 75
    new-instance v7, Ljava/io/File;

    .end local v7    # "localFile1":Ljava/io/File;
    invoke-direct {v7, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 76
    .restart local v7    # "localFile1":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v12

    if-nez v12, :cond_1aa

    new-instance v7, Ljava/io/File;

    .end local v7    # "localFile1":Ljava/io/File;
    const-string v12, "-1"

    const-string v13, "-2"

    invoke-virtual {v4, v12, v13}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v7, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 78
    .restart local v7    # "localFile1":Ljava/io/File;
    :cond_1aa
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v12

    if-nez v12, :cond_1bd

    new-instance v7, Ljava/io/File;

    .end local v7    # "localFile1":Ljava/io/File;
    const-string v12, "-1"

    const-string v13, ""

    invoke-virtual {v4, v12, v13}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v7, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 79
    .restart local v7    # "localFile1":Ljava/io/File;
    :cond_1bd
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v12

    if-eqz v12, :cond_1c4

    move-object v8, v7

    .line 80
    :cond_1c4
    const-string v12, "data@app"

    const-string v13, "mnt@asec"

    invoke-virtual {v4, v12, v13}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    .line 81
    const-string v12, ".apk@classes.dex"

    const-string v13, "@pkg.apk@classes.dex"

    invoke-virtual {v5, v12, v13}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    .line 82
    new-instance v7, Ljava/io/File;

    .end local v7    # "localFile1":Ljava/io/File;
    invoke-direct {v7, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 83
    .restart local v7    # "localFile1":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v12

    if-eqz v12, :cond_1e0

    move-object v8, v7

    .line 84
    :cond_1e0
    new-instance v7, Ljava/io/File;

    .end local v7    # "localFile1":Ljava/io/File;
    const-string v12, "-1"

    const-string v13, "-2"

    invoke-virtual {v5, v12, v13}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v7, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 85
    .restart local v7    # "localFile1":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v12

    if-eqz v12, :cond_1f4

    move-object v8, v7

    .line 86
    :cond_1f4
    new-instance v7, Ljava/io/File;

    .end local v7    # "localFile1":Ljava/io/File;
    const-string v12, "-1"

    const-string v13, ""

    invoke-virtual {v5, v12, v13}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v7, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 87
    .restart local v7    # "localFile1":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v12

    if-eqz v12, :cond_208

    move-object v8, v7

    .line 89
    :cond_208
    sget-object v12, Lcom/chelpus/root/utils/odex;->dalvikDexIn2:Ljava/lang/String;

    const-string v13, "zamenitetodelo"

    const/4 v14, 0x0

    aget-object v14, p0, v14

    invoke-virtual {v12, v13, v14}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    .line 90
    const-string v12, "/cache/"

    const-string v13, "/sd-ext/data/cache/"

    invoke-virtual {v4, v12, v13}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    .line 91
    new-instance v7, Ljava/io/File;

    .end local v7    # "localFile1":Ljava/io/File;
    invoke-direct {v7, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 92
    .restart local v7    # "localFile1":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v12

    if-nez v12, :cond_233

    new-instance v7, Ljava/io/File;

    .end local v7    # "localFile1":Ljava/io/File;
    const-string v12, "-1"

    const-string v13, "-2"

    invoke-virtual {v4, v12, v13}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v7, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 94
    .restart local v7    # "localFile1":Ljava/io/File;
    :cond_233
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v12

    if-nez v12, :cond_246

    new-instance v7, Ljava/io/File;

    .end local v7    # "localFile1":Ljava/io/File;
    const-string v12, "-1"

    const-string v13, ""

    invoke-virtual {v4, v12, v13}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v7, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 95
    .restart local v7    # "localFile1":Ljava/io/File;
    :cond_246
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v12

    if-eqz v12, :cond_24d

    move-object v8, v7

    .line 96
    :cond_24d
    const-string v12, "data@app"

    const-string v13, "mnt@asec"

    invoke-virtual {v4, v12, v13}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    .line 97
    const-string v12, ".apk@classes.dex"

    const-string v13, "@pkg.apk@classes.dex"

    invoke-virtual {v5, v12, v13}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    .line 98
    new-instance v7, Ljava/io/File;

    .end local v7    # "localFile1":Ljava/io/File;
    invoke-direct {v7, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 99
    .restart local v7    # "localFile1":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v12

    if-eqz v12, :cond_269

    move-object v8, v7

    .line 100
    :cond_269
    new-instance v7, Ljava/io/File;

    .end local v7    # "localFile1":Ljava/io/File;
    const-string v12, "-1"

    const-string v13, "-2"

    invoke-virtual {v5, v12, v13}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v7, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 101
    .restart local v7    # "localFile1":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v12

    if-eqz v12, :cond_27d

    move-object v8, v7

    .line 102
    :cond_27d
    new-instance v7, Ljava/io/File;

    .end local v7    # "localFile1":Ljava/io/File;
    const-string v12, "-1"

    const-string v13, ""

    invoke-virtual {v5, v12, v13}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v7, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 103
    .restart local v7    # "localFile1":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v12

    if-eqz v12, :cond_291

    move-object v8, v7

    .line 104
    :cond_291
    sget-boolean v12, Lcom/chelpus/root/utils/odex;->system:Z

    if-eqz v12, :cond_30c

    .line 105
    new-instance v11, Ljava/io/File;

    sget-object v12, Lcom/chelpus/root/utils/odex;->dirapp:Ljava/lang/String;

    invoke-direct {v11, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 106
    .local v11, "temp":Ljava/io/File;
    invoke-virtual {v11}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v10

    .line 108
    .local v10, "sysapkname":Ljava/lang/String;
    sget-object v12, Lcom/chelpus/root/utils/odex;->dalvikDexIn:Ljava/lang/String;

    const-string v13, "zamenitetodelo-1.apk"

    invoke-virtual {v12, v13, v10}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    .line 109
    const-string v12, "data@app"

    const-string v13, "system@app"

    invoke-virtual {v4, v12, v13}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    .line 111
    new-instance v7, Ljava/io/File;

    .end local v7    # "localFile1":Ljava/io/File;
    invoke-direct {v7, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 112
    .restart local v7    # "localFile1":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v12

    if-eqz v12, :cond_2bc

    move-object v8, v7

    .line 113
    :cond_2bc
    new-instance v7, Ljava/io/File;

    .end local v7    # "localFile1":Ljava/io/File;
    const-string v12, "/data/dalvik-cache/"

    const-string v13, "/sd-ext/data/dalvik-cache/"

    invoke-virtual {v5, v12, v13}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v7, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 114
    .restart local v7    # "localFile1":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v12

    if-eqz v12, :cond_2d0

    move-object v8, v7

    .line 115
    :cond_2d0
    new-instance v7, Ljava/io/File;

    .end local v7    # "localFile1":Ljava/io/File;
    const-string v12, "/data/dalvik-cache/"

    const-string v13, "/sd-ext/data/cache/dalvik-cache/"

    invoke-virtual {v5, v12, v13}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v7, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 116
    .restart local v7    # "localFile1":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v12

    if-eqz v12, :cond_2e4

    move-object v8, v7

    .line 117
    :cond_2e4
    new-instance v7, Ljava/io/File;

    .end local v7    # "localFile1":Ljava/io/File;
    const-string v12, "/data/dalvik-cache/"

    const-string v13, "/cache/dalvik-cache/"

    invoke-virtual {v5, v12, v13}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v7, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 118
    .restart local v7    # "localFile1":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v12

    if-eqz v12, :cond_2f8

    move-object v8, v7

    .line 119
    :cond_2f8
    new-instance v7, Ljava/io/File;

    .end local v7    # "localFile1":Ljava/io/File;
    const-string v12, "/data/dalvik-cache/"

    const-string v13, "/data/dalvik-cache/arm/"

    invoke-virtual {v5, v12, v13}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v7, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 120
    .restart local v7    # "localFile1":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v12

    if-eqz v12, :cond_30c

    move-object v8, v7

    .line 125
    .end local v10    # "sysapkname":Ljava/lang/String;
    .end local v11    # "temp":Ljava/io/File;
    :cond_30c
    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v12

    if-nez v12, :cond_324

    new-instance v12, Ljava/io/FileNotFoundException;

    invoke-direct {v12}, Ljava/io/FileNotFoundException;-><init>()V

    throw v12
    :try_end_318
    .catch Ljava/io/FileNotFoundException; {:try_start_8e .. :try_end_318} :catch_318
    .catch Ljava/lang/Exception; {:try_start_8e .. :try_end_318} :catch_43f

    .line 157
    .end local v5    # "dalvikDexTemp":Ljava/lang/String;
    .end local v7    # "localFile1":Ljava/io/File;
    .end local v8    # "localFile2":Ljava/io/File;
    :catch_318
    move-exception v9

    .line 158
    .local v9, "localFileNotFoundException":Ljava/io/FileNotFoundException;
    sget-object v12, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v13, "Error: Program files are not found!\n\nCheck the location dalvik-cache to solve problems!\n\nDefault: /data/dalvik-cache/*"

    invoke-virtual {v12, v13}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 165
    .end local v4    # "dalvikDex":Ljava/lang/String;
    .end local v9    # "localFileNotFoundException":Ljava/io/FileNotFoundException;
    :cond_320
    :goto_320
    invoke-static {}, Lcom/chelpus/Utils;->exitFromRootJava()V

    .line 166
    return-void

    .line 127
    .restart local v4    # "dalvikDex":Ljava/lang/String;
    .restart local v5    # "dalvikDexTemp":Ljava/lang/String;
    .restart local v7    # "localFile1":Ljava/io/File;
    .restart local v8    # "localFile2":Ljava/io/File;
    :cond_324
    const/4 v12, 0x1

    :try_start_325
    aget-object v12, p0, v12

    sget-object v13, Lcom/chelpus/root/utils/odex;->uid:Ljava/lang/String;

    invoke-static {v12, v13}, Lcom/chelpus/Utils;->getOdexForCreate(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 128
    .local v3, "backTemp":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 129
    .local v2, "backFile":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v12

    if-eqz v12, :cond_33b

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 130
    :cond_33b
    new-instance v2, Ljava/io/File;

    .end local v2    # "backFile":Ljava/io/File;
    const-string v12, "-2"

    const-string v13, "-1"

    invoke-virtual {v3, v12, v13}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v2, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 131
    .restart local v2    # "backFile":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v12

    if-eqz v12, :cond_351

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 132
    :cond_351
    new-instance v2, Ljava/io/File;

    .end local v2    # "backFile":Ljava/io/File;
    const-string v12, "-1"

    const-string v13, "-2"

    invoke-virtual {v3, v12, v13}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v2, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 133
    .restart local v2    # "backFile":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v12

    if-eqz v12, :cond_367

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 134
    :cond_367
    new-instance v2, Ljava/io/File;

    .end local v2    # "backFile":Ljava/io/File;
    const-string v12, "-2"

    const-string v13, ""

    invoke-virtual {v3, v12, v13}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v2, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 135
    .restart local v2    # "backFile":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v12

    if-eqz v12, :cond_37d

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 136
    :cond_37d
    new-instance v2, Ljava/io/File;

    .end local v2    # "backFile":Ljava/io/File;
    const-string v12, "-1"

    const-string v13, ""

    invoke-virtual {v3, v12, v13}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v2, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 137
    .restart local v2    # "backFile":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v12

    if-eqz v12, :cond_393

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 138
    :cond_393
    new-instance v2, Ljava/io/File;

    .end local v2    # "backFile":Ljava/io/File;
    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 139
    .restart local v2    # "backFile":Ljava/io/File;
    invoke-static {v8, v2}, Lcom/chelpus/Utils;->copyFile(Ljava/io/File;Ljava/io/File;)V

    .line 140
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v12

    if-eqz v12, :cond_436

    .line 141
    sget-object v12, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Changes Fix to: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 142
    sget-boolean v12, Lcom/chelpus/root/utils/odex;->system:Z

    if-nez v12, :cond_45e

    .line 143
    const/4 v12, 0x2

    new-array v12, v12, [Ljava/lang/String;

    const/4 v13, 0x0

    const-string v14, "chmod"

    aput-object v14, v12, v13

    const/4 v13, 0x1

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "644"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v12}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 144
    const/4 v12, 0x3

    new-array v12, v12, [Ljava/lang/String;

    const/4 v13, 0x0

    const-string v14, "chown"

    aput-object v14, v12, v13

    const/4 v13, 0x1

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "1000."

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    sget-object v15, Lcom/chelpus/root/utils/odex;->uid:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v12}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 145
    const/4 v12, 0x3

    new-array v12, v12, [Ljava/lang/String;

    const/4 v13, 0x0

    const-string v14, "chown"

    aput-object v14, v12, v13

    const/4 v13, 0x1

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "1000:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    sget-object v15, Lcom/chelpus/root/utils/odex;->uid:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v12}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 154
    :cond_436
    :goto_436
    sget-object v12, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v13, "Dalvik-cache fixing!"

    invoke-virtual {v12, v13}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_43d
    .catch Ljava/io/FileNotFoundException; {:try_start_325 .. :try_end_43d} :catch_318
    .catch Ljava/lang/Exception; {:try_start_325 .. :try_end_43d} :catch_43f

    goto/16 :goto_320

    .line 160
    .end local v2    # "backFile":Ljava/io/File;
    .end local v3    # "backTemp":Ljava/lang/String;
    .end local v5    # "dalvikDexTemp":Ljava/lang/String;
    .end local v7    # "localFile1":Ljava/io/File;
    .end local v8    # "localFile2":Ljava/io/File;
    :catch_43f
    move-exception v6

    .line 161
    .local v6, "e":Ljava/lang/Exception;
    sget-object v12, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Error: Exception e"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v6}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_320

    .line 147
    .end local v6    # "e":Ljava/lang/Exception;
    .restart local v2    # "backFile":Ljava/io/File;
    .restart local v3    # "backTemp":Ljava/lang/String;
    .restart local v5    # "dalvikDexTemp":Ljava/lang/String;
    .restart local v7    # "localFile1":Ljava/io/File;
    .restart local v8    # "localFile2":Ljava/io/File;
    :cond_45e
    const/4 v12, 0x2

    :try_start_45f
    new-array v12, v12, [Ljava/lang/String;

    const/4 v13, 0x0

    const-string v14, "chmod"

    aput-object v14, v12, v13

    const/4 v13, 0x1

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "644"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v12}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 148
    const/4 v12, 0x3

    new-array v12, v12, [Ljava/lang/String;

    const/4 v13, 0x0

    const-string v14, "chown"

    aput-object v14, v12, v13

    const/4 v13, 0x1

    const-string v14, "0.0"

    aput-object v14, v12, v13

    const/4 v13, 0x2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v12}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V

    .line 149
    const/4 v12, 0x3

    new-array v12, v12, [Ljava/lang/String;

    const/4 v13, 0x0

    const-string v14, "chown"

    aput-object v14, v12, v13

    const/4 v13, 0x1

    const-string v14, "0:0"

    aput-object v14, v12, v13

    const/4 v13, 0x2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v12}, Lcom/chelpus/Utils;->run_all_no_root([Ljava/lang/String;)V
    :try_end_4b1
    .catch Ljava/io/FileNotFoundException; {:try_start_45f .. :try_end_4b1} :catch_318
    .catch Ljava/lang/Exception; {:try_start_45f .. :try_end_4b1} :catch_43f

    goto :goto_436
.end method
