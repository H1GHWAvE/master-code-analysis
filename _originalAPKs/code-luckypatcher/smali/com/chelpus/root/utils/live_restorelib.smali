.class public Lcom/chelpus/root/utils/live_restorelib;
.super Ljava/lang/Object;
.source "live_restorelib.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .registers 11
    .param p0, "paramArrayOfString"    # [Ljava/lang/String;

    .prologue
    .line 12
    new-instance v7, Lcom/chelpus/root/utils/live_restorelib$1;

    invoke-direct {v7}, Lcom/chelpus/root/utils/live_restorelib$1;-><init>()V

    invoke-static {v7}, Lcom/chelpus/Utils;->startRootJava(Ljava/lang/Object;)V

    .line 19
    const/4 v7, 0x0

    aget-object v2, p0, v7

    .line 23
    .local v2, "dalvikDex":Ljava/lang/String;
    :try_start_b
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 26
    .local v4, "localFile1":Ljava/io/File;
    new-instance v5, Ljava/io/File;

    const-string v7, "/data/data/"

    const-string v8, "/mnt/asec/"

    invoke-virtual {v2, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 27
    .local v5, "localFile2":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_24

    move-object v5, v4

    .line 28
    :cond_24
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_42

    new-instance v5, Ljava/io/File;

    .end local v5    # "localFile2":Ljava/io/File;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "-1"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 29
    .restart local v5    # "localFile2":Ljava/io/File;
    :cond_42
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_55

    new-instance v5, Ljava/io/File;

    .end local v5    # "localFile2":Ljava/io/File;
    const-string v7, "-1"

    const-string v8, "-2"

    invoke-virtual {v2, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 30
    .restart local v5    # "localFile2":Ljava/io/File;
    :cond_55
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_6d

    new-instance v7, Ljava/io/FileNotFoundException;

    invoke-direct {v7}, Ljava/io/FileNotFoundException;-><init>()V

    throw v7
    :try_end_61
    .catch Ljava/io/FileNotFoundException; {:try_start_b .. :try_end_61} :catch_61
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_61} :catch_95

    .line 42
    .end local v4    # "localFile1":Ljava/io/File;
    .end local v5    # "localFile2":Ljava/io/File;
    :catch_61
    move-exception v6

    .line 43
    .local v6, "localFileNotFoundException":Ljava/io/FileNotFoundException;
    sget-object v7, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v8, "Error: Backup files are not found!"

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 48
    .end local v6    # "localFileNotFoundException":Ljava/io/FileNotFoundException;
    :goto_69
    invoke-static {}, Lcom/chelpus/Utils;->exitFromRootJava()V

    .line 49
    return-void

    .line 34
    .restart local v4    # "localFile1":Ljava/io/File;
    .restart local v5    # "localFile2":Ljava/io/File;
    :cond_6d
    :try_start_6d
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ".backup"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 35
    .local v1, "backTemp":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 36
    .local v0, "backFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_b3

    new-instance v7, Ljava/io/FileNotFoundException;

    invoke-direct {v7}, Ljava/io/FileNotFoundException;-><init>()V

    throw v7
    :try_end_95
    .catch Ljava/io/FileNotFoundException; {:try_start_6d .. :try_end_95} :catch_61
    .catch Ljava/lang/Exception; {:try_start_6d .. :try_end_95} :catch_95

    .line 45
    .end local v0    # "backFile":Ljava/io/File;
    .end local v1    # "backTemp":Ljava/lang/String;
    .end local v4    # "localFile1":Ljava/io/File;
    .end local v5    # "localFile2":Ljava/io/File;
    :catch_95
    move-exception v3

    .line 46
    .local v3, "e":Ljava/lang/Exception;
    sget-object v7, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Exception e"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v3}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_69

    .line 37
    .end local v3    # "e":Ljava/lang/Exception;
    .restart local v0    # "backFile":Ljava/io/File;
    .restart local v1    # "backTemp":Ljava/lang/String;
    .restart local v4    # "localFile1":Ljava/io/File;
    .restart local v5    # "localFile2":Ljava/io/File;
    :cond_b3
    :try_start_b3
    invoke-static {v0, v5}, Lcom/chelpus/Utils;->copyFile(Ljava/io/File;Ljava/io/File;)V

    .line 38
    sget-object v7, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v8, "Restore - done!"

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_bd
    .catch Ljava/io/FileNotFoundException; {:try_start_b3 .. :try_end_bd} :catch_61
    .catch Ljava/lang/Exception; {:try_start_b3 .. :try_end_bd} :catch_95

    goto :goto_69
.end method
