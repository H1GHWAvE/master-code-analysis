.class public Lcom/chelpus/root/utils/clearDalvikCache;
.super Ljava/lang/Object;
.source "clearDalvikCache.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .registers 18
    .param p0, "paramArrayOfString"    # [Ljava/lang/String;

    .prologue
    .line 14
    new-instance v10, Lcom/chelpus/root/utils/clearDalvikCache$1;

    invoke-direct {v10}, Lcom/chelpus/root/utils/clearDalvikCache$1;-><init>()V

    invoke-static {v10}, Lcom/chelpus/Utils;->startRootJava(Ljava/lang/Object;)V

    .line 15
    const/4 v10, 0x0

    aget-object v10, p0, v10

    sput-object v10, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->toolfilesdir:Ljava/lang/String;

    .line 16
    new-instance v10, Ljava/io/File;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v12, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->toolfilesdir:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/ClearDalvik.on"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_71

    .line 17
    new-instance v10, Ljava/io/File;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v12, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->toolfilesdir:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/ClearDalvik.on"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->delete()Z

    .line 18
    new-instance v10, Ljava/io/File;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v12, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->toolfilesdir:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/ClearDalvik.on"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_75

    .line 19
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v11, "LuckyPatcher: (error) try delete ClearDalvik.on - fault!"

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 131
    :cond_71
    :goto_71
    invoke-static {}, Lcom/chelpus/Utils;->exitFromRootJava()V

    .line 132
    return-void

    .line 22
    :cond_75
    :try_start_75
    new-instance v10, Ljava/io/File;

    const-string v11, "/mnt/asec"

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    .line 23
    .local v3, "mnt_asec":[Ljava/io/File;
    array-length v12, v3

    const/4 v10, 0x0

    move v11, v10

    :goto_83
    if-ge v11, v12, :cond_bb

    aget-object v0, v3, v11

    .line 24
    .local v0, "dir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v10

    if-eqz v10, :cond_b6

    .line 25
    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 26
    .local v2, "files":[Ljava/io/File;
    array-length v13, v2

    const/4 v10, 0x0

    :goto_93
    if-ge v10, v13, :cond_b6

    aget-object v1, v2, v10

    .line 27
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v14

    invoke-virtual {v14}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v14

    const-string v15, ".odex"

    invoke-virtual {v14, v15}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_b3

    .line 28
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v14

    const-string v15, "rw"

    invoke-static {v14, v15}, Lcom/chelpus/Utils;->remount(Ljava/lang/String;Ljava/lang/String;)Z

    .line 29
    invoke-virtual {v1}, Ljava/io/File;->delete()Z
    :try_end_b3
    .catch Ljava/lang/Exception; {:try_start_75 .. :try_end_b3} :catch_ba

    .line 26
    :cond_b3
    add-int/lit8 v10, v10, 0x1

    goto :goto_93

    .line 23
    .end local v1    # "file":Ljava/io/File;
    .end local v2    # "files":[Ljava/io/File;
    :cond_b6
    add-int/lit8 v10, v11, 0x1

    move v11, v10

    goto :goto_83

    .line 35
    .end local v0    # "dir":Ljava/io/File;
    .end local v3    # "mnt_asec":[Ljava/io/File;
    :catch_ba
    move-exception v10

    .line 36
    :cond_bb
    sget v10, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->api:I

    const/16 v11, 0x17

    if-lt v10, v11, :cond_135

    .line 38
    :try_start_c1
    new-instance v10, Ljava/io/File;

    const-string v11, "/data/app"

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_135

    .line 39
    new-instance v10, Ljava/io/File;

    const-string v11, "/data/app"

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 40
    .restart local v2    # "files":[Ljava/io/File;
    array-length v12, v2

    const/4 v10, 0x0

    move v11, v10

    :goto_dc
    if-ge v11, v12, :cond_135

    aget-object v9, v2, v11

    .line 42
    .local v9, "tail":Ljava/io/File;
    invoke-virtual {v9}, Ljava/io/File;->isDirectory()Z

    move-result v10

    if-eqz v10, :cond_130

    .line 43
    invoke-virtual {v9}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v5

    .line 44
    .local v5, "oats":[Ljava/io/File;
    if-eqz v5, :cond_130

    array-length v10, v5

    if-lez v10, :cond_130

    .line 45
    array-length v13, v5

    const/4 v10, 0x0

    :goto_f1
    if-ge v10, v13, :cond_130

    aget-object v4, v5, v10

    .line 46
    .local v4, "oat":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v14

    if-eqz v14, :cond_12d

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v14

    const-string v15, "oat"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_12d

    .line 47
    sget-object v14, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "delete folder:"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 48
    new-instance v14, Lcom/chelpus/Utils;

    const-string v15, ""

    invoke-direct {v14, v15}, Lcom/chelpus/Utils;-><init>(Ljava/lang/String;)V

    invoke-virtual {v14, v4}, Lcom/chelpus/Utils;->deleteFolder(Ljava/io/File;)V
    :try_end_12d
    .catch Ljava/lang/Exception; {:try_start_c1 .. :try_end_12d} :catch_134

    .line 45
    :cond_12d
    add-int/lit8 v10, v10, 0x1

    goto :goto_f1

    .line 40
    .end local v4    # "oat":Ljava/io/File;
    .end local v5    # "oats":[Ljava/io/File;
    :cond_130
    add-int/lit8 v10, v11, 0x1

    move v11, v10

    goto :goto_dc

    .line 54
    .end local v2    # "files":[Ljava/io/File;
    .end local v9    # "tail":Ljava/io/File;
    :catch_134
    move-exception v10

    .line 57
    :cond_135
    :try_start_135
    new-instance v10, Ljava/io/File;

    const-string v11, "/data/dalvik-cache"

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_183

    .line 58
    new-instance v10, Ljava/io/File;

    const-string v11, "/data/dalvik-cache"

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 59
    .restart local v2    # "files":[Ljava/io/File;
    array-length v12, v2

    const/4 v10, 0x0

    move v11, v10

    :goto_150
    if-ge v11, v12, :cond_183

    aget-object v9, v2, v11

    .line 61
    .restart local v9    # "tail":Ljava/io/File;
    invoke-virtual {v9}, Ljava/io/File;->isDirectory()Z

    move-result v10

    if-eqz v10, :cond_175

    .line 62
    invoke-virtual {v9}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v5

    .line 63
    .restart local v5    # "oats":[Ljava/io/File;
    if-eqz v5, :cond_175

    array-length v10, v5

    if-lez v10, :cond_175

    .line 64
    array-length v13, v5

    const/4 v10, 0x0

    :goto_165
    if-ge v10, v13, :cond_175

    aget-object v4, v5, v10

    .line 65
    .restart local v4    # "oat":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->isFile()Z

    move-result v14

    if-eqz v14, :cond_172

    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    .line 64
    :cond_172
    add-int/lit8 v10, v10, 0x1

    goto :goto_165

    .line 69
    .end local v4    # "oat":Ljava/io/File;
    .end local v5    # "oats":[Ljava/io/File;
    :cond_175
    invoke-virtual {v9}, Ljava/io/File;->isFile()Z

    move-result v10

    if-eqz v10, :cond_17e

    invoke-virtual {v9}, Ljava/io/File;->delete()Z
    :try_end_17e
    .catch Ljava/lang/Exception; {:try_start_135 .. :try_end_17e} :catch_182

    .line 59
    :cond_17e
    add-int/lit8 v10, v11, 0x1

    move v11, v10

    goto :goto_150

    .line 71
    .end local v2    # "files":[Ljava/io/File;
    .end local v9    # "tail":Ljava/io/File;
    :catch_182
    move-exception v10

    .line 73
    :cond_183
    :try_start_183
    new-instance v10, Ljava/io/File;

    const-string v11, "/cache/dalvik-cache"

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_1c0

    .line 74
    new-instance v10, Ljava/io/File;

    const-string v11, "/cache/dalvik-cache"

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v2

    .line 75
    .local v2, "files":[Ljava/lang/String;
    array-length v11, v2

    const/4 v10, 0x0

    :goto_19d
    if-ge v10, v11, :cond_1c0

    aget-object v9, v2, v10

    .line 76
    .local v9, "tail":Ljava/lang/String;
    new-instance v12, Ljava/io/File;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "/cache/dalvik-cache/"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12}, Ljava/io/File;->delete()Z
    :try_end_1bc
    .catch Ljava/lang/Exception; {:try_start_183 .. :try_end_1bc} :catch_1bf

    .line 75
    add-int/lit8 v10, v10, 0x1

    goto :goto_19d

    .line 78
    .end local v2    # "files":[Ljava/lang/String;
    .end local v9    # "tail":Ljava/lang/String;
    :catch_1bf
    move-exception v10

    .line 80
    :cond_1c0
    :try_start_1c0
    new-instance v10, Ljava/io/File;

    const-string v11, "/sd-ext/data/dalvik-cache"

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_1fd

    .line 81
    new-instance v10, Ljava/io/File;

    const-string v11, "/sd-ext/data/dalvik-cache"

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v2

    .line 82
    .restart local v2    # "files":[Ljava/lang/String;
    array-length v11, v2

    const/4 v10, 0x0

    :goto_1da
    if-ge v10, v11, :cond_1fd

    aget-object v9, v2, v10

    .line 83
    .restart local v9    # "tail":Ljava/lang/String;
    new-instance v12, Ljava/io/File;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "/sd-ext/data/dalvik-cache/"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12}, Ljava/io/File;->delete()Z
    :try_end_1f9
    .catch Ljava/lang/Exception; {:try_start_1c0 .. :try_end_1f9} :catch_1fc

    .line 82
    add-int/lit8 v10, v10, 0x1

    goto :goto_1da

    .line 85
    .end local v2    # "files":[Ljava/lang/String;
    .end local v9    # "tail":Ljava/lang/String;
    :catch_1fc
    move-exception v10

    .line 87
    :cond_1fd
    :try_start_1fd
    new-instance v10, Ljava/io/File;

    const-string v11, "/sd-ext/data/cache/dalvik-cache"

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_23a

    .line 88
    new-instance v10, Ljava/io/File;

    const-string v11, "/sd-ext/data/cache/dalvik-cache"

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v2

    .line 89
    .restart local v2    # "files":[Ljava/lang/String;
    array-length v11, v2

    const/4 v10, 0x0

    :goto_217
    if-ge v10, v11, :cond_23a

    aget-object v9, v2, v10

    .line 90
    .restart local v9    # "tail":Ljava/lang/String;
    new-instance v12, Ljava/io/File;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "/sd-ext/data/cache/dalvik-cache/"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12}, Ljava/io/File;->delete()Z
    :try_end_236
    .catch Ljava/lang/Exception; {:try_start_1fd .. :try_end_236} :catch_239

    .line 89
    add-int/lit8 v10, v10, 0x1

    goto :goto_217

    .line 92
    .end local v2    # "files":[Ljava/lang/String;
    .end local v9    # "tail":Ljava/lang/String;
    :catch_239
    move-exception v10

    .line 94
    :cond_23a
    :try_start_23a
    new-instance v10, Ljava/io/File;

    const-string v11, "/data/app"

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_27f

    .line 95
    new-instance v10, Ljava/io/File;

    const-string v11, "/data/app"

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v2

    .line 96
    .restart local v2    # "files":[Ljava/lang/String;
    array-length v11, v2

    const/4 v10, 0x0

    :goto_254
    if-ge v10, v11, :cond_27f

    aget-object v9, v2, v10

    .line 97
    .restart local v9    # "tail":Ljava/lang/String;
    const-string v12, ".odex"

    invoke-virtual {v9, v12}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_27b

    .line 98
    new-instance v12, Ljava/io/File;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "/data/app/"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12}, Ljava/io/File;->delete()Z
    :try_end_27b
    .catch Ljava/lang/Exception; {:try_start_23a .. :try_end_27b} :catch_27e

    .line 96
    :cond_27b
    add-int/lit8 v10, v10, 0x1

    goto :goto_254

    .line 100
    .end local v2    # "files":[Ljava/lang/String;
    .end local v9    # "tail":Ljava/lang/String;
    :catch_27e
    move-exception v10

    .line 103
    :cond_27f
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v11, "LuckyPatcher: Dalvik-Cache deleted."

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 106
    :try_start_286
    const-string v10, "/system"

    const-string v11, "rw"

    invoke-static {v10, v11}, Lcom/chelpus/Utils;->remount(Ljava/lang/String;Ljava/lang/String;)Z

    .line 107
    new-instance v10, Ljava/io/File;

    const-string v11, "/system/app"

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v8

    .line 108
    .local v8, "system_files":[Ljava/io/File;
    array-length v11, v8

    const/4 v10, 0x0

    :goto_29a
    if-ge v10, v11, :cond_2ce

    aget-object v7, v8, v10

    .line 109
    .local v7, "sys_file":Ljava/io/File;
    new-instance v6, Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x1

    invoke-static {v12, v13}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v6, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 110
    .local v6, "odex":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v12

    if-eqz v12, :cond_2cb

    invoke-virtual {v7}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v12

    invoke-virtual {v12}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v12

    const-string v13, ".apk"

    invoke-virtual {v12, v13}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_2cb

    .line 111
    invoke-static {v7}, Lcom/chelpus/Utils;->classes_test(Ljava/io/File;)Z

    move-result v12

    if-eqz v12, :cond_2cb

    .line 112
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    .line 108
    :cond_2cb
    add-int/lit8 v10, v10, 0x1

    goto :goto_29a

    .line 116
    .end local v6    # "odex":Ljava/io/File;
    .end local v7    # "sys_file":Ljava/io/File;
    :cond_2ce
    new-instance v10, Ljava/io/File;

    const-string v11, "/system/priv-app"

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v8

    .line 117
    array-length v11, v8

    const/4 v10, 0x0

    :goto_2db
    if-ge v10, v11, :cond_30f

    aget-object v7, v8, v10

    .line 118
    .restart local v7    # "sys_file":Ljava/io/File;
    new-instance v6, Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x1

    invoke-static {v12, v13}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v6, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 119
    .restart local v6    # "odex":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v12

    if-eqz v12, :cond_30c

    invoke-virtual {v7}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v12

    invoke-virtual {v12}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v12

    const-string v13, ".apk"

    invoke-virtual {v12, v13}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_30c

    .line 120
    invoke-static {v7}, Lcom/chelpus/Utils;->classes_test(Ljava/io/File;)Z

    move-result v12

    if-eqz v12, :cond_30c

    .line 121
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    .line 117
    :cond_30c
    add-int/lit8 v10, v10, 0x1

    goto :goto_2db

    .line 125
    .end local v6    # "odex":Ljava/io/File;
    .end local v7    # "sys_file":Ljava/io/File;
    :cond_30f
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v11, "LuckyPatcher: System apps deodex all."

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_316
    .catch Ljava/lang/Exception; {:try_start_286 .. :try_end_316} :catch_318

    goto/16 :goto_71

    .line 126
    .end local v8    # "system_files":[Ljava/io/File;
    :catch_318
    move-exception v10

    goto/16 :goto_71
.end method
