.class Lcom/chelpus/XSupport$12;
.super Lde/robv/android/xposed/XC_MethodHook;
.source "XSupport.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/chelpus/XSupport;->handleLoadPackage(Lde/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/chelpus/XSupport;


# direct methods
.method constructor <init>(Lcom/chelpus/XSupport;)V
    .registers 2
    .param p1, "this$0"    # Lcom/chelpus/XSupport;

    .prologue
    .line 374
    iput-object p1, p0, Lcom/chelpus/XSupport$12;->this$0:Lcom/chelpus/XSupport;

    invoke-direct {p0}, Lde/robv/android/xposed/XC_MethodHook;-><init>()V

    return-void
.end method


# virtual methods
.method protected beforeHookedMethod(Lde/robv/android/xposed/XC_MethodHook$MethodHookParam;)V
    .registers 6
    .param p1, "param"    # Lde/robv/android/xposed/XC_MethodHook$MethodHookParam;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 378
    iget-object v2, p0, Lcom/chelpus/XSupport$12;->this$0:Lcom/chelpus/XSupport;

    invoke-virtual {v2}, Lcom/chelpus/XSupport;->loadPrefs()V

    .line 379
    sget-boolean v2, Lcom/chelpus/XSupport;->enable:Z

    if-eqz v2, :cond_2a

    sget-boolean v2, Lcom/chelpus/XSupport;->patch3:Z

    if-eqz v2, :cond_2a

    .line 381
    sget-boolean v2, Lcom/chelpus/Common;->JB_MR1_NEWER:Z

    if-eqz v2, :cond_2b

    const/4 v1, 0x2

    .line 382
    .local v1, "id":I
    :goto_12
    iget-object v2, p1, Lde/robv/android/xposed/XC_MethodHook$MethodHookParam;->args:[Ljava/lang/Object;

    aget-object v2, v2, v1

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 384
    .local v0, "flags":I
    and-int/lit16 v2, v0, 0x80

    if-nez v2, :cond_2a

    .line 385
    or-int/lit16 v0, v0, 0x80

    .line 386
    iget-object v2, p1, Lde/robv/android/xposed/XC_MethodHook$MethodHookParam;->args:[Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v1

    .line 390
    .end local v0    # "flags":I
    .end local v1    # "id":I
    :cond_2a
    return-void

    .line 381
    :cond_2b
    const/4 v1, 0x1

    goto :goto_12
.end method
