.class Lcom/chelpus/XSupport$22;
.super Lde/robv/android/xposed/XC_MethodHook;
.source "XSupport.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/chelpus/XSupport;->handleLoadPackage(Lde/robv/android/xposed/callbacks/XC_LoadPackage$LoadPackageParam;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/chelpus/XSupport;


# direct methods
.method constructor <init>(Lcom/chelpus/XSupport;)V
    .registers 2
    .param p1, "this$0"    # Lcom/chelpus/XSupport;

    .prologue
    .line 682
    iput-object p1, p0, Lcom/chelpus/XSupport$22;->this$0:Lcom/chelpus/XSupport;

    invoke-direct {p0}, Lde/robv/android/xposed/XC_MethodHook;-><init>()V

    return-void
.end method


# virtual methods
.method protected afterHookedMethod(Lde/robv/android/xposed/XC_MethodHook$MethodHookParam;)V
    .registers 4
    .param p1, "param"    # Lde/robv/android/xposed/XC_MethodHook$MethodHookParam;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 700
    iget-object v0, p0, Lcom/chelpus/XSupport$22;->this$0:Lcom/chelpus/XSupport;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/chelpus/XSupport;->skip1:Z

    .line 725
    return-void
.end method

.method protected beforeHookedMethod(Lde/robv/android/xposed/XC_MethodHook$MethodHookParam;)V
    .registers 4
    .param p1, "param"    # Lde/robv/android/xposed/XC_MethodHook$MethodHookParam;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 686
    iget-object v0, p0, Lcom/chelpus/XSupport$22;->this$0:Lcom/chelpus/XSupport;

    invoke-virtual {v0}, Lcom/chelpus/XSupport;->loadPrefs()V

    .line 687
    sget-boolean v0, Lcom/chelpus/XSupport;->enable:Z

    if-eqz v0, :cond_1a

    sget-boolean v0, Lcom/chelpus/XSupport;->hide:Z

    if-eqz v0, :cond_1a

    .line 689
    iget-object v0, p0, Lcom/chelpus/XSupport$22;->this$0:Lcom/chelpus/XSupport;

    invoke-virtual {v0, p1}, Lcom/chelpus/XSupport;->checkForHideApp(Lde/robv/android/xposed/XC_MethodHook$MethodHookParam;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 690
    iget-object v0, p0, Lcom/chelpus/XSupport$22;->this$0:Lcom/chelpus/XSupport;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/chelpus/XSupport;->skip1:Z

    .line 694
    :cond_1a
    return-void
.end method
