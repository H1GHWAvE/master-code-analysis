.class final Lcom/chelpus/Utils$10;
.super Ljava/lang/Object;
.source "Utils.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/chelpus/Utils;->checkRoot(Ljava/lang/Boolean;Ljava/lang/String;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 3623
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 8

    .prologue
    .line 3628
    :try_start_0
    const-string v2, "/system/bin/su"

    invoke-static {v2}, Lcom/chelpus/Utils;->exists(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_ec

    .line 3630
    sget-boolean v2, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->su:Z

    if-eqz v2, :cond_14

    .line 3631
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "LuckyPatcher: skip root test."

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 3761
    :cond_13
    :goto_13
    return-void

    .line 3634
    :cond_14
    new-instance v2, Lcom/chelpus/Utils;

    const-string v3, ""

    invoke-direct {v2, v3}, Lcom/chelpus/Utils;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "stat -c %a /system/bin/su"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    # getter for: Lcom/chelpus/Utils;->internalBusybox:Ljava/lang/String;
    invoke-static {}, Lcom/chelpus/Utils;->access$400()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " stat -c %a /system/bin/su"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "busybox stat -c %a /system/bin/su"

    aput-object v5, v3, v4

    invoke-virtual {v2, v3}, Lcom/chelpus/Utils;->cmdRoot([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 3635
    .local v1, "result":Ljava/lang/String;
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "LuckyPatcher (chek root): get permissions "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " /system/bin/su"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 3636
    const-string v2, "6755"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_e3

    const-string v2, "[0-9]"

    invoke-virtual {v1, v2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_e3

    .line 3637
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "LuckyPatcher (chek root): Permissions /system/bin/su not correct."

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 3639
    const-string v2, "/system"

    const-string v3, "rw"

    invoke-static {v2, v3}, Lcom/chelpus/Utils;->remount(Ljava/lang/String;Ljava/lang/String;)Z

    .line 3640
    new-instance v2, Lcom/chelpus/Utils;

    const-string v3, ""

    invoke-direct {v2, v3}, Lcom/chelpus/Utils;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "chmod 06755 /system/bin/su"

    aput-object v5, v3, v4

    invoke-virtual {v2, v3}, Lcom/chelpus/Utils;->cmdRoot([Ljava/lang/String;)Ljava/lang/String;

    .line 3641
    const-string v2, "/system"

    const-string v3, "ro"

    invoke-static {v2, v3}, Lcom/chelpus/Utils;->remount(Ljava/lang/String;Ljava/lang/String;)Z

    .line 3643
    new-instance v2, Lcom/chelpus/Utils;

    const-string v3, ""

    invoke-direct {v2, v3}, Lcom/chelpus/Utils;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "stat -c %a /system/bin/su"

    aput-object v5, v3, v4

    invoke-virtual {v2, v3}, Lcom/chelpus/Utils;->cmdRoot([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 3644
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "LuckyPatcher (chek root): "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " /system/bin/su"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 3645
    const-string v2, "6755"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_13

    .line 3646
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "LuckyPatcher (chek root): permission /system/bin/su set 06755"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_db
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_db} :catch_dd

    goto/16 :goto_13

    .line 3757
    .end local v1    # "result":Ljava/lang/String;
    :catch_dd
    move-exception v0

    .line 3758
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_13

    .line 3650
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "result":Ljava/lang/String;
    :cond_e3
    :try_start_e3
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "LuckyPatcher (chek root): Permissions is true.(/system/bin/su)"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_13

    .line 3691
    .end local v1    # "result":Ljava/lang/String;
    :cond_ec
    const-string v2, "/system/xbin/su"

    invoke-static {v2}, Lcom/chelpus/Utils;->exists(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_13

    .line 3692
    new-instance v2, Lcom/chelpus/Utils;

    const-string v3, ""

    invoke-direct {v2, v3}, Lcom/chelpus/Utils;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "chmod 06777 internalBusybox"

    aput-object v5, v3, v4

    invoke-virtual {v2, v3}, Lcom/chelpus/Utils;->cmdRoot([Ljava/lang/String;)Ljava/lang/String;

    .line 3694
    sget-boolean v2, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->su:Z

    if-eqz v2, :cond_113

    .line 3695
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "LuckyPatcher: skip root test."

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_13

    .line 3698
    :cond_113
    new-instance v2, Lcom/chelpus/Utils;

    const-string v3, ""

    invoke-direct {v2, v3}, Lcom/chelpus/Utils;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "stat -c %a /system/xbin/su"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    # getter for: Lcom/chelpus/Utils;->internalBusybox:Ljava/lang/String;
    invoke-static {}, Lcom/chelpus/Utils;->access$400()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " stat -c %a /system/xbin/su"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "busybox stat -c %a /system/xbin/su"

    aput-object v5, v3, v4

    invoke-virtual {v2, v3}, Lcom/chelpus/Utils;->cmdRoot([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 3700
    .restart local v1    # "result":Ljava/lang/String;
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "LuckyPatcher (chek root): get permissions "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " /system/xbin/su"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 3702
    const-string v2, "6755"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1dc

    const-string v2, "[0-9]"

    invoke-virtual {v1, v2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1dc

    .line 3703
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "LuckyPatcher (chek root): Permissions /system/xbin/su not correct."

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 3705
    const-string v2, "/system"

    const-string v3, "rw"

    invoke-static {v2, v3}, Lcom/chelpus/Utils;->remount(Ljava/lang/String;Ljava/lang/String;)Z

    .line 3706
    new-instance v2, Lcom/chelpus/Utils;

    const-string v3, ""

    invoke-direct {v2, v3}, Lcom/chelpus/Utils;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "chmod 06755 /system/xbin/su"

    aput-object v5, v3, v4

    invoke-virtual {v2, v3}, Lcom/chelpus/Utils;->cmdRoot([Ljava/lang/String;)Ljava/lang/String;

    .line 3707
    const-string v2, "/system"

    const-string v3, "ro"

    invoke-static {v2, v3}, Lcom/chelpus/Utils;->remount(Ljava/lang/String;Ljava/lang/String;)Z

    .line 3708
    new-instance v2, Lcom/chelpus/Utils;

    const-string v3, ""

    invoke-direct {v2, v3}, Lcom/chelpus/Utils;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "stat -c %a /system/xbin/su"

    aput-object v5, v3, v4

    invoke-virtual {v2, v3}, Lcom/chelpus/Utils;->cmdRoot([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 3709
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "LuckyPatcher (chek root): "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " /system/xbin/su"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 3710
    const-string v2, "6755"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_13

    .line 3711
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "LuckyPatcher (chek root): permission /system/xbin/su set 06755"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_13

    .line 3715
    :cond_1dc
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "LuckyPatcher (chek root): Permissions is true.(/system/xbin/su)"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_1e3
    .catch Ljava/lang/Exception; {:try_start_e3 .. :try_end_1e3} :catch_dd

    goto/16 :goto_13
.end method
