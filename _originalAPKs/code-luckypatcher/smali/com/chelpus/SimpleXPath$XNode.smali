.class public Lcom/chelpus/SimpleXPath$XNode;
.super Ljava/lang/Object;
.source "SimpleXPath.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chelpus/SimpleXPath;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "XNode"
.end annotation


# instance fields
.field mNode:Lorg/w3c/dom/Node;

.field final synthetic this$0:Lcom/chelpus/SimpleXPath;


# direct methods
.method public constructor <init>(Lcom/chelpus/SimpleXPath;Lorg/w3c/dom/Node;)V
    .registers 3
    .param p1, "this$0"    # Lcom/chelpus/SimpleXPath;
    .param p2, "node"    # Lorg/w3c/dom/Node;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/chelpus/SimpleXPath$XNode;->this$0:Lcom/chelpus/SimpleXPath;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p2, p0, Lcom/chelpus/SimpleXPath$XNode;->mNode:Lorg/w3c/dom/Node;

    .line 44
    return-void
.end method


# virtual methods
.method getAttribute(Ljava/lang/String;)Ljava/lang/String;
    .registers 9
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/chelpus/XNodeException;
        }
    .end annotation

    .prologue
    .line 108
    iget-object v1, p0, Lcom/chelpus/SimpleXPath$XNode;->mNode:Lorg/w3c/dom/Node;

    check-cast v1, Lorg/w3c/dom/Element;

    .line 109
    .local v1, "e":Lorg/w3c/dom/Element;
    invoke-interface {v1, p1}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 111
    .local v0, "attribute":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_27

    .line 112
    const-string v3, "Node [%s] has no [%s] attribute!"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 113
    invoke-virtual {p0}, Lcom/chelpus/SimpleXPath$XNode;->getName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object p1, v4, v5

    .line 112
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 115
    .local v2, "message":Ljava/lang/String;
    new-instance v3, Lcom/chelpus/XNodeException;

    invoke-direct {v3, v2}, Lcom/chelpus/XNodeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 118
    .end local v2    # "message":Ljava/lang/String;
    :cond_27
    return-object v0
.end method

.method getChild(Ljava/lang/String;)Lcom/chelpus/SimpleXPath$XNode;
    .registers 8
    .param p1, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/chelpus/XNodeException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 54
    iget-object v2, p0, Lcom/chelpus/SimpleXPath$XNode;->this$0:Lcom/chelpus/SimpleXPath;

    iget-object v3, p0, Lcom/chelpus/SimpleXPath$XNode;->mNode:Lorg/w3c/dom/Node;

    invoke-virtual {v2, v3, p1}, Lcom/chelpus/SimpleXPath;->getXPathNodes(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 56
    .local v1, "nodes":Ljava/util/List;, "Ljava/util/List<Lcom/chelpus/SimpleXPath$XNode;>;"
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_29

    .line 57
    const-string v2, "Node [%s] has no child at path [%s]!"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/chelpus/SimpleXPath$XNode;->mNode:Lorg/w3c/dom/Node;

    invoke-interface {v4}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    const/4 v4, 0x1

    aput-object p1, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 59
    .local v0, "message":Ljava/lang/String;
    new-instance v2, Lcom/chelpus/XNodeException;

    invoke-direct {v2, v0}, Lcom/chelpus/XNodeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 62
    .end local v0    # "message":Ljava/lang/String;
    :cond_29
    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/chelpus/SimpleXPath$XNode;

    return-object v2
.end method

.method getChildren(Ljava/lang/String;)Ljava/util/List;
    .registers 8
    .param p1, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/chelpus/SimpleXPath$XNode;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/chelpus/XNodeException;
        }
    .end annotation

    .prologue
    .line 73
    iget-object v2, p0, Lcom/chelpus/SimpleXPath$XNode;->this$0:Lcom/chelpus/SimpleXPath;

    iget-object v3, p0, Lcom/chelpus/SimpleXPath$XNode;->mNode:Lorg/w3c/dom/Node;

    invoke-virtual {v2, v3, p1}, Lcom/chelpus/SimpleXPath;->getXPathNodes(Lorg/w3c/dom/Node;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 75
    .local v1, "result":Ljava/util/List;, "Ljava/util/List<Lcom/chelpus/SimpleXPath$XNode;>;"
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_29

    .line 76
    const-string v2, "Node [%s] has no children at path [%s]!"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/chelpus/SimpleXPath$XNode;->mNode:Lorg/w3c/dom/Node;

    invoke-interface {v5}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object p1, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 78
    .local v0, "message":Ljava/lang/String;
    new-instance v2, Lcom/chelpus/XNodeException;

    invoke-direct {v2, v0}, Lcom/chelpus/XNodeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 81
    .end local v0    # "message":Ljava/lang/String;
    :cond_29
    return-object v1
.end method

.method getFloatAttribute(Ljava/lang/String;)F
    .registers 3
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/chelpus/XNodeException;
        }
    .end annotation

    .prologue
    .line 152
    invoke-virtual {p0, p1}, Lcom/chelpus/SimpleXPath$XNode;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    return v0
.end method

.method public getIntAttribute(Ljava/lang/String;)I
    .registers 3
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/chelpus/XNodeException;
        }
    .end annotation

    .prologue
    .line 140
    invoke-virtual {p0, p1}, Lcom/chelpus/SimpleXPath$XNode;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getName()Ljava/lang/String;
    .registers 2

    .prologue
    .line 89
    iget-object v0, p0, Lcom/chelpus/SimpleXPath$XNode;->mNode:Lorg/w3c/dom/Node;

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getStringAttribute(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/chelpus/XNodeException;
        }
    .end annotation

    .prologue
    .line 129
    invoke-virtual {p0, p1}, Lcom/chelpus/SimpleXPath$XNode;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getValue()Ljava/lang/String;
    .registers 3

    .prologue
    .line 97
    iget-object v0, p0, Lcom/chelpus/SimpleXPath$XNode;->mNode:Lorg/w3c/dom/Node;

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
