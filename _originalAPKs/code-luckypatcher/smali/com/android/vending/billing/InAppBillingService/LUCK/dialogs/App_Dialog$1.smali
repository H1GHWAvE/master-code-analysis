.class Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1;
.super Ljava/lang/Object;
.source "App_Dialog.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog;->onCreateDialog()Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog;

.field final synthetic val$bar:Landroid/widget/ProgressBar;

.field final synthetic val$builder:Landroid/text/SpannableStringBuilder;

.field final synthetic val$builder2:Landroid/text/SpannableStringBuilder;

.field final synthetic val$builder3:Landroid/text/SpannableStringBuilder;

.field final synthetic val$txt:Landroid/widget/TextView;

.field final synthetic val$txt2:Landroid/widget/TextView;

.field final synthetic val$txt3:Landroid/widget/TextView;

.field final synthetic val$txt4:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/text/SpannableStringBuilder;Landroid/text/SpannableStringBuilder;Landroid/widget/TextView;Landroid/text/SpannableStringBuilder;Landroid/widget/TextView;Landroid/widget/ProgressBar;)V
    .registers 10
    .param p1, "this$0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog;

    .prologue
    .line 82
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog;

    iput-object p2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1;->val$txt:Landroid/widget/TextView;

    iput-object p3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1;->val$txt2:Landroid/widget/TextView;

    iput-object p4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1;->val$builder:Landroid/text/SpannableStringBuilder;

    iput-object p5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1;->val$builder3:Landroid/text/SpannableStringBuilder;

    iput-object p6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1;->val$txt4:Landroid/widget/TextView;

    iput-object p7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1;->val$builder2:Landroid/text/SpannableStringBuilder;

    iput-object p8, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1;->val$txt3:Landroid/widget/TextView;

    iput-object p9, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1;->val$bar:Landroid/widget/ProgressBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 24

    .prologue
    .line 85
    const-string v14, ""

    .line 89
    .local v14, "str2":Ljava/lang/String;
    :try_start_2
    sget-object v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    iget-object v14, v15, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->name:Ljava/lang/String;

    .line 90
    const-string v15, "#be6e17"

    const-string v16, "bold"

    invoke-static/range {v14 .. v16}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v7

    .line 91
    .local v7, "ios":Landroid/text/SpannableString;
    sget-object v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->handler:Landroid/os/Handler;

    new-instance v16, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1$1;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v7}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1$1;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1;Landroid/text/SpannableString;)V

    invoke-virtual/range {v15 .. v16}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 97
    sget-object v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    iget-boolean v15, v15, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->enable:Z

    if-eqz v15, :cond_25a

    .line 98
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const v16, 0x7f0700f2

    invoke-static/range {v16 .. v16}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "\n"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 99
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1;->val$builder:Landroid/text/SpannableStringBuilder;

    const-string v16, ""

    const-string v17, ""

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v14, v0, v1}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 104
    :goto_4f
    sget-object v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    iget-boolean v15, v15, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->custom:Z

    if-eqz v15, :cond_83

    .line 105
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const v16, 0x7f0701f6

    invoke-static/range {v16 .. v16}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "\n"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 106
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1;->val$builder:Landroid/text/SpannableStringBuilder;

    const v16, -0xf1bbe

    const-string v17, ""

    move/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v14, v0, v1}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;ILjava/lang/String;)Landroid/text/SpannableString;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 108
    :cond_83
    sget-object v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    iget-boolean v15, v15, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->lvl:Z

    if-eqz v15, :cond_b7

    .line 109
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const v16, 0x7f0701f9

    invoke-static/range {v16 .. v16}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "\n"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 110
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1;->val$builder:Landroid/text/SpannableStringBuilder;

    const v16, -0xff008d

    const-string v17, ""

    move/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v14, v0, v1}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;ILjava/lang/String;)Landroid/text/SpannableString;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 112
    :cond_b7
    sget-object v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    iget-boolean v15, v15, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->ads:Z

    if-eqz v15, :cond_eb

    .line 113
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const v16, 0x7f0701f2

    invoke-static/range {v16 .. v16}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "\n"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 114
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1;->val$builder:Landroid/text/SpannableStringBuilder;

    const v16, -0xf1bbe

    const-string v17, ""

    move/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v14, v0, v1}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;ILjava/lang/String;)Landroid/text/SpannableString;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 116
    :cond_eb
    sget-object v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    iget-boolean v15, v15, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->ads:Z

    if-nez v15, :cond_12b

    sget-object v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    iget-boolean v15, v15, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->lvl:Z

    if-nez v15, :cond_12b

    sget-object v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    iget-boolean v15, v15, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->custom:Z

    if-nez v15, :cond_12b

    .line 117
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const v16, 0x7f0701ff

    invoke-static/range {v16 .. v16}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "\n"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 118
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1;->val$builder:Landroid/text/SpannableStringBuilder;

    const v16, -0xffab

    const-string v17, ""

    move/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v14, v0, v1}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;ILjava/lang/String;)Landroid/text/SpannableString;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 120
    :cond_12b
    sget-object v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    iget-boolean v15, v15, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->modified:Z

    if-eqz v15, :cond_28e

    .line 121
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const v16, 0x7f0701fa

    invoke-static/range {v16 .. v16}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "\n"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 122
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1;->val$builder:Landroid/text/SpannableStringBuilder;

    const v16, -0xff008d

    const-string v17, ""

    move/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v14, v0, v1}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;ILjava/lang/String;)Landroid/text/SpannableString;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 127
    :goto_15f
    sget-object v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    iget-boolean v15, v15, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->odex:Z

    if-eqz v15, :cond_2bd

    .line 128
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const v16, 0x7f0701f8

    invoke-static/range {v16 .. v16}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "\n"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 129
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1;->val$builder:Landroid/text/SpannableStringBuilder;

    const v16, -0xf1bbe

    const-string v17, ""

    move/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v14, v0, v1}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;ILjava/lang/String;)Landroid/text/SpannableString;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 134
    :goto_193
    sget-object v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    iget-boolean v15, v15, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->system:Z

    if-eqz v15, :cond_2ec

    .line 135
    const v15, 0x7f070200

    invoke-static {v15}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v14

    .line 136
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1;->val$builder:Landroid/text/SpannableStringBuilder;

    const v16, -0x279e9

    const-string v17, ""

    move/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v14, v0, v1}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;ILjava/lang/String;)Landroid/text/SpannableString;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 142
    :goto_1b4
    sget-object v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->handler:Landroid/os/Handler;

    new-instance v16, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1$2;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1$2;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1;)V

    invoke-virtual/range {v15 .. v16}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 147
    const/4 v15, 0x0

    new-array v9, v15, [Ljava/lang/String;

    .line 148
    .local v9, "requestedPermissions":[Ljava/lang/String;
    const/4 v15, 0x0

    new-array v2, v15, [Ljava/lang/String;

    .line 149
    .local v2, "decripts":[Ljava/lang/String;
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v8

    .line 150
    .local v8, "pm":Landroid/content/pm/PackageManager;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1;->val$builder3:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v15}, Landroid/text/SpannableStringBuilder;->clear()V
    :try_end_1d3
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_1d3} :catch_289

    .line 152
    :try_start_1d3
    sget-object v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    iget-object v15, v15, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    const/16 v16, 0x1000

    move/from16 v0, v16

    invoke-virtual {v8, v15, v0}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v15

    iget-object v9, v15, Landroid/content/pm/PackageInfo;->requestedPermissions:[Ljava/lang/String;

    .line 153
    if-eqz v9, :cond_326

    array-length v15, v9

    if-lez v15, :cond_326

    .line 154
    array-length v15, v9

    new-array v2, v15, [Ljava/lang/String;

    .line 155
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1ea
    array-length v15, v9

    if-ge v6, v15, :cond_326

    .line 156
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v16, v9, v6

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "\n"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 157
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1;->val$builder3:Landroid/text/SpannableStringBuilder;

    const-string v16, "#6699cc"

    const-string v17, "bold"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v14, v0, v1}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
    :try_end_215
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1d3 .. :try_end_215} :catch_322
    .catch Ljava/lang/NullPointerException; {:try_start_1d3 .. :try_end_215} :catch_289

    .line 158
    const/4 v10, 0x0

    .line 160
    .local v10, "result":Ljava/lang/String;
    :try_start_216
    aget-object v15, v9, v6

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v8, v15, v0}, Landroid/content/pm/PackageManager;->getPermissionInfo(Ljava/lang/String;I)Landroid/content/pm/PermissionInfo;

    move-result-object v15

    invoke-virtual {v15, v8}, Landroid/content/pm/PermissionInfo;->loadDescription(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v15

    invoke-interface {v15}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;
    :try_end_227
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_216 .. :try_end_227} :catch_309
    .catch Ljava/lang/Exception; {:try_start_216 .. :try_end_227} :catch_8bd
    .catch Ljava/lang/NullPointerException; {:try_start_216 .. :try_end_227} :catch_289

    move-result-object v10

    .line 164
    :goto_228
    if-nez v10, :cond_30d

    :try_start_22a
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const v16, 0x7f0701b9

    invoke-static/range {v16 .. v16}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "\n\n"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 167
    :goto_244
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1;->val$builder3:Landroid/text/SpannableStringBuilder;

    const-string v16, ""

    const-string v17, ""

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v14, v0, v1}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
    :try_end_257
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_22a .. :try_end_257} :catch_322
    .catch Ljava/lang/NullPointerException; {:try_start_22a .. :try_end_257} :catch_289

    .line 155
    add-int/lit8 v6, v6, 0x1

    goto :goto_1ea

    .line 101
    .end local v2    # "decripts":[Ljava/lang/String;
    .end local v6    # "i":I
    .end local v8    # "pm":Landroid/content/pm/PackageManager;
    .end local v9    # "requestedPermissions":[Ljava/lang/String;
    .end local v10    # "result":Ljava/lang/String;
    :cond_25a
    :try_start_25a
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const v16, 0x7f0700ed

    invoke-static/range {v16 .. v16}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "\n"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 102
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1;->val$builder:Landroid/text/SpannableStringBuilder;

    const-string v16, ""

    const-string v17, ""

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v14, v0, v1}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
    :try_end_287
    .catch Ljava/lang/NullPointerException; {:try_start_25a .. :try_end_287} :catch_289

    goto/16 :goto_4f

    .line 289
    .end local v7    # "ios":Landroid/text/SpannableString;
    :catch_289
    move-exception v3

    .line 290
    .local v3, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v3}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 292
    .end local v3    # "e":Ljava/lang/NullPointerException;
    :goto_28d
    return-void

    .line 124
    .restart local v7    # "ios":Landroid/text/SpannableString;
    :cond_28e
    :try_start_28e
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const v16, 0x7f0701fd

    invoke-static/range {v16 .. v16}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "\n"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 125
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1;->val$builder:Landroid/text/SpannableStringBuilder;

    const-string v16, ""

    const-string v17, ""

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v14, v0, v1}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto/16 :goto_15f

    .line 131
    :cond_2bd
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const v16, 0x7f0701fb

    invoke-static/range {v16 .. v16}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "\n"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 132
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1;->val$builder:Landroid/text/SpannableStringBuilder;

    const-string v16, ""

    const-string v17, ""

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v14, v0, v1}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto/16 :goto_193

    .line 138
    :cond_2ec
    const v15, 0x7f0701fe

    invoke-static {v15}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v14

    .line 139
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1;->val$builder:Landroid/text/SpannableStringBuilder;

    const v16, -0xff008d

    const-string v17, ""

    move/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v14, v0, v1}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;ILjava/lang/String;)Landroid/text/SpannableString;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
    :try_end_307
    .catch Ljava/lang/NullPointerException; {:try_start_28e .. :try_end_307} :catch_289

    goto/16 :goto_1b4

    .line 161
    .restart local v2    # "decripts":[Ljava/lang/String;
    .restart local v6    # "i":I
    .restart local v8    # "pm":Landroid/content/pm/PackageManager;
    .restart local v9    # "requestedPermissions":[Ljava/lang/String;
    .restart local v10    # "result":Ljava/lang/String;
    :catch_309
    move-exception v3

    .line 162
    .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v10, 0x0

    .line 163
    goto/16 :goto_228

    .line 165
    .end local v3    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_30d
    :try_start_30d
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "\n\n"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_31f
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_30d .. :try_end_31f} :catch_322
    .catch Ljava/lang/NullPointerException; {:try_start_30d .. :try_end_31f} :catch_289

    move-result-object v14

    goto/16 :goto_244

    .line 171
    .end local v6    # "i":I
    .end local v10    # "result":Ljava/lang/String;
    :catch_322
    move-exception v3

    .line 172
    .restart local v3    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_323
    invoke-virtual {v3}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 175
    .end local v3    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_326
    sget-object v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->handler:Landroid/os/Handler;

    new-instance v16, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1$3;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1$3;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1;)V

    invoke-virtual/range {v15 .. v16}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 181
    const-string v14, "Package name:\n"

    .line 182
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1;->val$builder2:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v15}, Landroid/text/SpannableStringBuilder;->clear()V

    .line 183
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1;->val$builder2:Landroid/text/SpannableStringBuilder;

    const-string v16, "#6699cc"

    const-string v17, "bold"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v14, v0, v1}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 185
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v16, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "\n"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 186
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1;->val$builder2:Landroid/text/SpannableStringBuilder;

    const-string v16, ""

    const-string v17, ""

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v14, v0, v1}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 188
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const v16, 0x7f0701f1

    invoke-static/range {v16 .. v16}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "\n"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 189
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1;->val$builder2:Landroid/text/SpannableStringBuilder;

    const-string v16, "#6699cc"

    const-string v17, "bold"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v14, v0, v1}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
    :try_end_3ab
    .catch Ljava/lang/NullPointerException; {:try_start_323 .. :try_end_3ab} :catch_289

    .line 191
    :try_start_3ab
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v16

    sget-object v17, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "\n"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 192
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1;->val$builder2:Landroid/text/SpannableStringBuilder;

    const-string v16, ""

    const-string v17, ""

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v14, v0, v1}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
    :try_end_3e9
    .catch Ljava/lang/Exception; {:try_start_3ab .. :try_end_3e9} :catch_8a7
    .catch Ljava/lang/NullPointerException; {:try_start_3ab .. :try_end_3e9} :catch_289

    .line 198
    :goto_3e9
    :try_start_3e9
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const v16, 0x7f0701f3

    invoke-static/range {v16 .. v16}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "\n"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 199
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1;->val$builder2:Landroid/text/SpannableStringBuilder;

    const-string v16, "#6699cc"

    const-string v17, "bold"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v14, v0, v1}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
    :try_end_416
    .catch Ljava/lang/NullPointerException; {:try_start_3e9 .. :try_end_416} :catch_289

    .line 202
    :try_start_416
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v15

    sget-object v16, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-virtual/range {v15 .. v17}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v15

    iget-object v15, v15, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v14, v15, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    .line 203
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1;->val$builder2:Landroid/text/SpannableStringBuilder;

    const-string v16, ""

    const-string v17, ""

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v14, v0, v1}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 205
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "\n"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const v16, 0x7f0701f7

    invoke-static/range {v16 .. v16}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "\n"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 206
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1;->val$builder2:Landroid/text/SpannableStringBuilder;

    const-string v16, "#6699cc"

    const-string v17, "bold"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v14, v0, v1}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 208
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v16

    sget-object v17, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-virtual/range {v16 .. v18}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v16

    move-object/from16 v0, v16

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "/"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 209
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1;->val$builder2:Landroid/text/SpannableStringBuilder;

    const-string v16, ""

    const-string v17, ""

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v14, v0, v1}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 211
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "\n"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const v16, 0x7f070202

    invoke-static/range {v16 .. v16}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 212
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1;->val$builder2:Landroid/text/SpannableStringBuilder;

    const-string v16, "#6699cc"

    const-string v17, "bold"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v14, v0, v1}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 214
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v15

    sget-object v16, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-virtual/range {v15 .. v17}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v15

    iget-object v14, v15, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    .line 215
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1;->val$builder2:Landroid/text/SpannableStringBuilder;

    const-string v16, ""

    const-string v17, ""

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v14, v0, v1}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 217
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "\n"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const v16, 0x7f0701f5

    invoke-static/range {v16 .. v16}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 218
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1;->val$builder2:Landroid/text/SpannableStringBuilder;

    const-string v16, "#6699cc"

    const-string v17, "bold"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v14, v0, v1}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 221
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, ""

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v16

    sget-object v17, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-virtual/range {v16 .. v18}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v16

    move-object/from16 v0, v16

    iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 222
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1;->val$builder2:Landroid/text/SpannableStringBuilder;

    const-string v16, ""

    const-string v17, ""

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v14, v0, v1}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 224
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "\n"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const v16, 0x7f070201

    invoke-static/range {v16 .. v16}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 225
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1;->val$builder2:Landroid/text/SpannableStringBuilder;

    const-string v16, "#6699cc"

    const-string v17, "bold"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v14, v0, v1}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 227
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, ""

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v16

    sget-object v17, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-virtual/range {v16 .. v18}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v16

    move-object/from16 v0, v16

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 228
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1;->val$builder2:Landroid/text/SpannableStringBuilder;

    const-string v16, ""

    const-string v17, ""

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v14, v0, v1}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 230
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "\n"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const v16, 0x7f07013c

    invoke-static/range {v16 .. v16}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 231
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1;->val$builder2:Landroid/text/SpannableStringBuilder;

    const-string v16, "#6699cc"

    const-string v17, "bold"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v14, v0, v1}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 232
    new-instance v11, Ljava/text/SimpleDateFormat;

    const-string v15, "yyyy-MM-dd HH:mm"

    invoke-direct {v11, v15}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 233
    .local v11, "sdf":Ljava/text/SimpleDateFormat;
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, ""

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    new-instance v16, Ljava/util/Date;

    sget-object v17, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    move-object/from16 v0, v17

    iget v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->updatetime:I

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-long v0, v0

    move-wide/from16 v17, v0

    const-wide/16 v19, 0x3e8

    mul-long v17, v17, v19

    invoke-direct/range {v16 .. v18}, Ljava/util/Date;-><init>(J)V

    move-object/from16 v0, v16

    invoke-virtual {v11, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 234
    sget-object v15, Ljava/lang/System;->out:Ljava/io/PrintStream;

    sget-object v16, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    move-object/from16 v0, v16

    iget v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->updatetime:I

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/io/PrintStream;->println(I)V

    .line 235
    sget-object v15, Ljava/lang/System;->out:Ljava/io/PrintStream;

    sget-object v16, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    move-object/from16 v0, v16

    iget v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->updatetime:I

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-long v0, v0

    move-wide/from16 v16, v0

    const-wide/16 v18, 0x3e8

    mul-long v16, v16, v18

    invoke-virtual/range {v15 .. v17}, Ljava/io/PrintStream;->println(J)V

    .line 236
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1;->val$builder2:Landroid/text/SpannableStringBuilder;

    const-string v16, ""

    const-string v17, ""

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v14, v0, v1}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 238
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "\n\n"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const v16, 0x7f070012

    invoke-static/range {v16 .. v16}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 239
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1;->val$builder2:Landroid/text/SpannableStringBuilder;

    const-string v16, "#6699cc"

    const-string v17, "bold"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v14, v0, v1}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 241
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "%.3f"

    const/16 v17, 0x1

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    new-instance v19, Ljava/io/File;

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v20

    sget-object v21, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-virtual/range {v20 .. v22}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v20

    move-object/from16 v0, v20

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-direct/range {v19 .. v20}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->length()J

    move-result-wide v19

    move-wide/from16 v0, v19

    long-to-float v0, v0

    move/from16 v19, v0

    const/high16 v20, 0x49800000    # 1048576.0f

    div-float v19, v19, v20

    invoke-static/range {v19 .. v19}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v19

    aput-object v19, v17, v18

    invoke-static/range {v16 .. v17}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " Mb"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 242
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1;->val$builder2:Landroid/text/SpannableStringBuilder;

    const-string v16, ""

    const-string v17, ""

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v14, v0, v1}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
    :try_end_736
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_416 .. :try_end_736} :catch_8ad
    .catch Ljava/lang/NullPointerException; {:try_start_416 .. :try_end_736} :catch_289

    .line 248
    .end local v11    # "sdf":Ljava/text/SimpleDateFormat;
    :goto_736
    :try_start_736
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "\n"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const v16, 0x7f0700da

    invoke-static/range {v16 .. v16}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 249
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1;->val$builder2:Landroid/text/SpannableStringBuilder;

    const-string v16, "#6699cc"

    const-string v17, "bold"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v14, v0, v1}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
    :try_end_769
    .catch Ljava/lang/Exception; {:try_start_736 .. :try_end_769} :catch_8b7
    .catch Ljava/lang/NullPointerException; {:try_start_736 .. :try_end_769} :catch_289

    .line 251
    const-wide/16 v12, 0x0

    .line 253
    .local v12, "size":J
    :try_start_76b
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v15

    sget-object v16, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-virtual/range {v15 .. v17}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v15

    iget-object v15, v15, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v15, v15, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    invoke-static {v15}, Lcom/chelpus/Utils;->getFileDalvikCache(Ljava/lang/String;)Ljava/io/File;

    move-result-object v15

    invoke-virtual {v15}, Ljava/io/File;->length()J
    :try_end_788
    .catch Ljava/lang/Exception; {:try_start_76b .. :try_end_788} :catch_8ba
    .catch Ljava/lang/NullPointerException; {:try_start_76b .. :try_end_788} :catch_289

    move-result-wide v12

    .line 257
    :goto_789
    :try_start_789
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "%.3f"

    const/16 v17, 0x1

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    long-to-float v0, v12

    move/from16 v19, v0

    const/high16 v20, 0x49800000    # 1048576.0f

    div-float v19, v19, v20

    invoke-static/range {v19 .. v19}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v19

    aput-object v19, v17, v18

    invoke-static/range {v16 .. v17}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " Mb"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 258
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1;->val$builder2:Landroid/text/SpannableStringBuilder;

    const-string v16, ""

    const-string v17, ""

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v14, v0, v1}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
    :try_end_7cc
    .catch Ljava/lang/Exception; {:try_start_789 .. :try_end_7cc} :catch_8b7
    .catch Ljava/lang/NullPointerException; {:try_start_789 .. :try_end_7cc} :catch_289

    .line 263
    .end local v12    # "size":J
    :goto_7cc
    :try_start_7cc
    sget-boolean v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->su:Z
    :try_end_7ce
    .catch Ljava/lang/NullPointerException; {:try_start_7cc .. :try_end_7ce} :catch_289

    if-eqz v15, :cond_897

    .line 265
    :try_start_7d0
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "\n"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const v16, 0x7f0700db

    invoke-static/range {v16 .. v16}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 266
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1;->val$builder2:Landroid/text/SpannableStringBuilder;

    const-string v16, "#6699cc"

    const-string v17, "bold"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v14, v0, v1}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 268
    const-string v12, ""
    :try_end_805
    .catch Ljava/lang/Exception; {:try_start_7d0 .. :try_end_805} :catch_8b3
    .catch Ljava/lang/NullPointerException; {:try_start_7d0 .. :try_end_805} :catch_289

    .line 270
    .local v12, "size":Ljava/lang/String;
    :try_start_805
    new-instance v15, Lcom/chelpus/Utils;

    const-string v16, ""

    invoke-direct/range {v15 .. v16}, Lcom/chelpus/Utils;-><init>(Ljava/lang/String;)V

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v19, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->dalvikruncommand:Ljava/lang/String;

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ".checkDataSize "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v19

    sget-object v20, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual/range {v19 .. v21}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    aput-object v18, v16, v17

    invoke-virtual/range {v15 .. v16}, Lcom/chelpus/Utils;->cmdRoot([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 271
    const-string v15, "SU Java-Code Running!\n"

    const-string v16, ""

    move-object/from16 v0, v16

    invoke-virtual {v12, v15, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    :try_end_85c
    .catch Ljava/lang/Exception; {:try_start_805 .. :try_end_85c} :catch_8b5
    .catch Ljava/lang/NullPointerException; {:try_start_805 .. :try_end_85c} :catch_289

    move-result-object v12

    .line 276
    :goto_85d
    :try_start_85d
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "\n"

    const-string v17, ""

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v16

    const-string v17, "\r"

    const-string v18, ""

    invoke-virtual/range {v16 .. v18}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " Mb"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 277
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1;->val$builder2:Landroid/text/SpannableStringBuilder;

    const-string v16, ""

    const-string v17, ""

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v14, v0, v1}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;
    :try_end_897
    .catch Ljava/lang/Exception; {:try_start_85d .. :try_end_897} :catch_8b3
    .catch Ljava/lang/NullPointerException; {:try_start_85d .. :try_end_897} :catch_289

    .line 282
    .end local v12    # "size":Ljava/lang/String;
    :cond_897
    :goto_897
    :try_start_897
    sget-object v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->handler:Landroid/os/Handler;

    new-instance v16, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1$4;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1$4;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1;)V

    invoke-virtual/range {v15 .. v16}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_28d

    .line 193
    :catch_8a7
    move-exception v5

    .line 195
    .local v5, "e3":Ljava/lang/Exception;
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_3e9

    .line 243
    .end local v5    # "e3":Ljava/lang/Exception;
    :catch_8ad
    move-exception v4

    .line 245
    .local v4, "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v4}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V
    :try_end_8b1
    .catch Ljava/lang/NullPointerException; {:try_start_897 .. :try_end_8b1} :catch_289

    goto/16 :goto_736

    .line 279
    .end local v4    # "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_8b3
    move-exception v15

    goto :goto_897

    .line 272
    .restart local v12    # "size":Ljava/lang/String;
    :catch_8b5
    move-exception v15

    goto :goto_85d

    .line 260
    .end local v12    # "size":Ljava/lang/String;
    :catch_8b7
    move-exception v15

    goto/16 :goto_7cc

    .line 254
    .local v12, "size":J
    :catch_8ba
    move-exception v15

    goto/16 :goto_789

    .line 163
    .end local v12    # "size":J
    .restart local v6    # "i":I
    .restart local v10    # "result":Ljava/lang/String;
    :catch_8bd
    move-exception v15

    goto/16 :goto_228
.end method
