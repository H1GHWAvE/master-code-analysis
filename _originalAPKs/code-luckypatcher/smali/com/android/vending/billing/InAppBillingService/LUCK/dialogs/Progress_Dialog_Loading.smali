.class public Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;
.super Ljava/lang/Object;
.source "Progress_Dialog_Loading.java"


# static fields
.field public static dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;


# instance fields
.field context:Landroid/support/v4/app/FragmentActivity;

.field dialog2:Landroid/app/Dialog;

.field fm:Landroid/support/v4/app/FragmentManager;

.field message:Ljava/lang/String;

.field title:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 25
    const/4 v0, 0x0

    sput-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    return-void
.end method

.method public constructor <init>()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    const-string v0, ""

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->message:Ljava/lang/String;

    .line 19
    const-string v0, ""

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->title:Ljava/lang/String;

    .line 20
    iput-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->fm:Landroid/support/v4/app/FragmentManager;

    .line 21
    iput-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->context:Landroid/support/v4/app/FragmentActivity;

    .line 26
    iput-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog2:Landroid/app/Dialog;

    .line 24
    return-void
.end method

.method public static newInstance()Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;
    .registers 1

    .prologue
    .line 28
    new-instance v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;

    invoke-direct {v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;-><init>()V

    .line 29
    .local v0, "f":Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;
    return-object v0
.end method


# virtual methods
.method public dismiss()V
    .registers 2

    .prologue
    .line 189
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog2:Landroid/app/Dialog;

    if-eqz v0, :cond_c

    .line 190
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog2:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 191
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog2:Landroid/app/Dialog;

    .line 193
    :cond_c
    return-void
.end method

.method public isShowing()Z
    .registers 2

    .prologue
    .line 155
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    if-eqz v0, :cond_e

    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    invoke-virtual {v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    .line 156
    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public onCreateDialog()Landroid/app/Dialog;
    .registers 3

    .prologue
    .line 43
    new-instance v0, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    .line 44
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    invoke-virtual {v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->setIncrementStyle()Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    .line 45
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->title:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_25

    const v0, 0x7f0700ef

    invoke-static {v0}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->title:Ljava/lang/String;

    .line 46
    :cond_25
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->setTitle(Ljava/lang/String;)Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    .line 47
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->message:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3f

    const v0, 0x7f070233

    invoke-static {v0}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->message:Ljava/lang/String;

    .line 48
    :cond_3f
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->message:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->setMessage(Ljava/lang/String;)Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    .line 49
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    const v1, 0x7f020033

    invoke-virtual {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->setIcon(I)Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    .line 50
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->setCancelable(Z)Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    .line 52
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    new-instance v1, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading$1;

    invoke-direct {v1, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading$1;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;)V

    invoke-virtual {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    .line 61
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    invoke-virtual {v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->create()Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method public setCancelable(Z)V
    .registers 3
    .param p1, "trig"    # Z

    .prologue
    .line 186
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog2:Landroid/app/Dialog;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog2:Landroid/app/Dialog;

    invoke-virtual {v0, p1}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 187
    :cond_9
    return-void
.end method

.method public setIndeterminate(ZLandroid/app/Activity;)V
    .registers 6
    .param p1, "indeterminate"    # Z
    .param p2, "activity"    # Landroid/app/Activity;

    .prologue
    .line 82
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    if-nez v1, :cond_7

    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->onCreateDialog()Landroid/app/Dialog;

    .line 83
    :cond_7
    if-eqz p1, :cond_2b

    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    const-string v2, ""

    invoke-virtual {v1, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->setProgressNumberFormat(Ljava/lang/String;)V

    .line 86
    :goto_10
    if-nez p1, :cond_33

    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    invoke-virtual {v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->setIncrementStyle()Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    .line 89
    :goto_17
    :try_start_17
    invoke-static {}, Lcom/chelpus/Utils;->onMainThread()Z

    move-result v1

    if-eqz v1, :cond_39

    .line 90
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    if-eqz v1, :cond_2a

    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->executePendingTransactions()Z
    :try_end_2a
    .catch Ljava/lang/Exception; {:try_start_17 .. :try_end_2a} :catch_44

    .line 103
    :cond_2a
    :goto_2a
    return-void

    .line 84
    :cond_2b
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    const-string v2, "%1d/%2d"

    invoke-virtual {v1, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->setProgressNumberFormat(Ljava/lang/String;)V

    goto :goto_10

    .line 87
    :cond_33
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    invoke-virtual {v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->setDefaultStyle()Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    goto :goto_17

    .line 92
    :cond_39
    :try_start_39
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v2, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading$3;

    invoke-direct {v2, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading$3;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;)V

    invoke-virtual {v1, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V
    :try_end_43
    .catch Ljava/lang/Exception; {:try_start_39 .. :try_end_43} :catch_44

    goto :goto_2a

    .line 99
    :catch_44
    move-exception v0

    .line 100
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2a
.end method

.method public setMax(I)V
    .registers 5
    .param p1, "max"    # I

    .prologue
    .line 106
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    if-nez v1, :cond_7

    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->onCreateDialog()Landroid/app/Dialog;

    .line 107
    :cond_7
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    invoke-virtual {v1, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->setMax(I)Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    .line 109
    :try_start_c
    invoke-static {}, Lcom/chelpus/Utils;->onMainThread()Z

    move-result v1

    if-eqz v1, :cond_20

    .line 110
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    if-eqz v1, :cond_1f

    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->executePendingTransactions()Z

    .line 120
    :cond_1f
    :goto_1f
    return-void

    .line 112
    :cond_20
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v2, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading$4;

    invoke-direct {v2, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading$4;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;)V

    invoke-virtual {v1, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V
    :try_end_2a
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_2a} :catch_2b

    goto :goto_1f

    .line 119
    :catch_2b
    move-exception v0

    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1f
.end method

.method public setMessage(Ljava/lang/String;)V
    .registers 5
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->message:Ljava/lang/String;

    .line 65
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    if-nez v1, :cond_9

    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->onCreateDialog()Landroid/app/Dialog;

    .line 66
    :cond_9
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    iget-object v2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->message:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->setMessage(Ljava/lang/String;)Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    .line 68
    :try_start_10
    invoke-static {}, Lcom/chelpus/Utils;->onMainThread()Z

    move-result v1

    if-eqz v1, :cond_24

    .line 69
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    if-eqz v1, :cond_23

    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->executePendingTransactions()Z

    .line 80
    :cond_23
    :goto_23
    return-void

    .line 71
    :cond_24
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v2, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading$2;

    invoke-direct {v2, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading$2;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;)V

    invoke-virtual {v1, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V
    :try_end_2e
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_2e} :catch_2f

    goto :goto_23

    .line 78
    :catch_2f
    move-exception v0

    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_23
.end method

.method public setProgress(I)V
    .registers 5
    .param p1, "progress"    # I

    .prologue
    .line 138
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    if-nez v1, :cond_7

    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->onCreateDialog()Landroid/app/Dialog;

    .line 139
    :cond_7
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    invoke-virtual {v1, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->setProgress(I)Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    .line 141
    :try_start_c
    invoke-static {}, Lcom/chelpus/Utils;->onMainThread()Z

    move-result v1

    if-eqz v1, :cond_20

    .line 142
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    if-eqz v1, :cond_1f

    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->executePendingTransactions()Z

    .line 152
    :cond_1f
    :goto_1f
    return-void

    .line 144
    :cond_20
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v2, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading$6;

    invoke-direct {v2, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading$6;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;)V

    invoke-virtual {v1, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V
    :try_end_2a
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_2a} :catch_2b

    goto :goto_1f

    .line 151
    :catch_2b
    move-exception v0

    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1f
.end method

.method public setProgressNumberFormat(Ljava/lang/String;)V
    .registers 5
    .param p1, "format"    # Ljava/lang/String;

    .prologue
    .line 122
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    if-nez v1, :cond_7

    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->onCreateDialog()Landroid/app/Dialog;

    .line 123
    :cond_7
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    invoke-virtual {v1, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->setProgressNumberFormat(Ljava/lang/String;)V

    .line 125
    :try_start_c
    invoke-static {}, Lcom/chelpus/Utils;->onMainThread()Z

    move-result v1

    if-eqz v1, :cond_20

    .line 126
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    if-eqz v1, :cond_1f

    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->executePendingTransactions()Z

    .line 136
    :cond_1f
    :goto_1f
    return-void

    .line 128
    :cond_20
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v2, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading$5;

    invoke-direct {v2, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading$5;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;)V

    invoke-virtual {v1, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V
    :try_end_2a
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_2a} :catch_2b

    goto :goto_1f

    .line 135
    :catch_2b
    move-exception v0

    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1f
.end method

.method public setTitle(Ljava/lang/String;)V
    .registers 5
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 161
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->title:Ljava/lang/String;

    .line 162
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    if-nez v1, :cond_9

    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->onCreateDialog()Landroid/app/Dialog;

    .line 163
    :cond_9
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    if-eqz v1, :cond_27

    .line 164
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog:Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    iget-object v2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->title:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;->setTitle(Ljava/lang/String;)Lcom/android/vending/billing/InAppBillingService/LUCK/ProgressDlg;

    .line 166
    :try_start_14
    invoke-static {}, Lcom/chelpus/Utils;->onMainThread()Z

    move-result v1

    if-eqz v1, :cond_28

    .line 167
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    if-eqz v1, :cond_27

    .line 168
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->executePendingTransactions()Z

    .line 184
    :cond_27
    :goto_27
    return-void

    .line 170
    :cond_28
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v2, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading$7;

    invoke-direct {v2, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading$7;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;)V

    invoke-virtual {v1, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V
    :try_end_32
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_32} :catch_33

    goto :goto_27

    .line 179
    :catch_33
    move-exception v0

    .line 180
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_27
.end method

.method public showDialog()V
    .registers 2

    .prologue
    .line 33
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog2:Landroid/app/Dialog;

    if-nez v0, :cond_a

    .line 34
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->onCreateDialog()Landroid/app/Dialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog2:Landroid/app/Dialog;

    .line 36
    :cond_a
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog2:Landroid/app/Dialog;

    if-eqz v0, :cond_13

    .line 37
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->dialog2:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 39
    :cond_13
    return-void
.end method
