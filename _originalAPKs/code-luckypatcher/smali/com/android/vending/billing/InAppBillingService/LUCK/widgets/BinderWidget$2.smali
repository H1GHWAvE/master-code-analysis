.class Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidget$2;
.super Ljava/lang/Object;
.source "BinderWidget.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidget;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidget;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$handler:Landroid/os/Handler;

.field final synthetic val$intent:Landroid/content/Intent;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidget;Landroid/content/Intent;Landroid/content/Context;Landroid/os/Handler;)V
    .registers 5
    .param p1, "this$0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidget;

    .prologue
    .line 179
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidget$2;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidget;

    iput-object p2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidget$2;->val$intent:Landroid/content/Intent;

    iput-object p3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidget$2;->val$context:Landroid/content/Context;

    iput-object p4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidget$2;->val$handler:Landroid/os/Handler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 19

    .prologue
    .line 182
    const/4 v12, 0x1

    sput-boolean v12, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->binder_process:Z

    .line 183
    invoke-static {}, Lcom/chelpus/Utils;->exitRoot()V

    .line 184
    const/4 v8, 0x0

    .line 185
    .local v8, "item":Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidget$2;->val$intent:Landroid/content/Intent;

    const-string v13, "appWidgetId"

    const/4 v14, -0x1

    invoke-virtual {v12, v13, v14}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    .line 186
    .local v5, "id":I
    const/4 v12, -0x1

    if-eq v5, v12, :cond_1f7

    .line 187
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidget$2;->val$context:Landroid/content/Context;

    invoke-static {v12, v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity;->loadTitlePref(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v12

    const-string v13, "NOT_SAVED_BIND"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_1f7

    .line 188
    new-instance v8, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;

    .end local v8    # "item":Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidget$2;->val$context:Landroid/content/Context;

    invoke-static {v12, v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity;->loadTitlePref(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v8, v12}, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;-><init>(Ljava/lang/String;)V

    .line 192
    .restart local v8    # "item":Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;
    new-instance v11, Landroid/widget/RemoteViews;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidget$2;->val$context:Landroid/content/Context;

    invoke-virtual {v12}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v12

    const v13, 0x7f04000d

    invoke-direct {v11, v12, v13}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 193
    .local v11, "remoteViews":Landroid/widget/RemoteViews;
    iget-object v12, v8, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;->TargetDir:Ljava/lang/String;

    const-string v13, "~chelpus_disabled~"

    const-string v14, ""

    invoke-virtual {v12, v13, v14}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v8, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;->TargetDir:Ljava/lang/String;

    .line 194
    iget-object v12, v8, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;->SourceDir:Ljava/lang/String;

    const-string v13, "~chelpus_disabled~"

    const-string v14, ""

    invoke-virtual {v12, v13, v14}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v8, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;->SourceDir:Ljava/lang/String;

    .line 196
    invoke-static {v8}, Lcom/chelpus/Utils;->checkBind(Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;)Z

    move-result v12

    if-nez v12, :cond_226

    .line 197
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidget$2;->val$context:Landroid/content/Context;

    invoke-static {v12}, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;->getBindes(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v1

    .line 198
    .local v1, "bindes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;>;"
    const/4 v3, 0x0

    .line 199
    .local v3, "found":Z
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_6d
    :goto_6d
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_d2

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;

    .line 200
    .local v7, "it":Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;
    iget-object v13, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;->SourceDir:Ljava/lang/String;

    const-string v14, "~chelpus_disabled~"

    const-string v15, ""

    invoke-virtual {v13, v14, v15}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    iget-object v14, v8, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;->SourceDir:Ljava/lang/String;

    const-string v15, "~chelpus_disabled~"

    const-string v16, ""

    invoke-virtual/range {v14 .. v16}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_6d

    iget-object v13, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;->TargetDir:Ljava/lang/String;

    const-string v14, "~chelpus_disabled~"

    const-string v15, ""

    invoke-virtual {v13, v14, v15}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    iget-object v14, v8, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;->TargetDir:Ljava/lang/String;

    const-string v15, "~chelpus_disabled~"

    const-string v16, ""

    invoke-virtual/range {v14 .. v16}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_6d

    .line 201
    iget-object v13, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;->TargetDir:Ljava/lang/String;

    const-string v14, "~chelpus_disabled~"

    const-string v15, ""

    invoke-virtual {v13, v14, v15}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    iput-object v13, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;->TargetDir:Ljava/lang/String;

    .line 202
    iget-object v13, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;->SourceDir:Ljava/lang/String;

    const-string v14, "~chelpus_disabled~"

    const-string v15, ""

    invoke-virtual {v13, v14, v15}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    iput-object v13, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;->SourceDir:Ljava/lang/String;

    .line 203
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidget$2;->val$context:Landroid/content/Context;

    invoke-virtual {v7}, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v5, v14}, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidgetConfigureActivity;->saveTitlePref(Landroid/content/Context;ILjava/lang/String;)V

    .line 204
    const/4 v3, 0x1

    goto :goto_6d

    .line 207
    .end local v7    # "it":Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;
    :cond_d2
    if-nez v3, :cond_13b

    .line 208
    sget-boolean v12, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->su:Z

    if-eqz v12, :cond_1f8

    .line 211
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "umount -f \'"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, v8, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;->TargetDir:Ljava/lang/String;

    const-string v14, "~chelpus_disabled~"

    const-string v15, ""

    invoke-virtual {v13, v14, v15}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "\'"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 212
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "umount -l \'"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, v8, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;->TargetDir:Ljava/lang/String;

    const-string v14, "~chelpus_disabled~"

    const-string v15, ""

    invoke-virtual {v13, v14, v15}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "\'"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 219
    :goto_124
    const v12, 0x7f0d002b

    const-string v13, "setBackgroundResource"

    const v14, 0x7f020050

    invoke-virtual {v11, v12, v13, v14}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 220
    const v12, 0x7f0d002a

    const-string v13, "#AAAAAA"

    invoke-static {v13}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v13

    invoke-virtual {v11, v12, v13}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 224
    :cond_13b
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidget$2;->val$context:Landroid/content/Context;

    invoke-static {v1, v12}, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;->savetoFile(Ljava/util/ArrayList;Landroid/content/Context;)V

    .line 227
    const-string v12, "mount"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "-o bind \'"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-object v14, v8, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;->SourceDir:Ljava/lang/String;

    const-string v15, "~chelpus_disabled~"

    const-string v16, ""

    invoke-virtual/range {v14 .. v16}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "\' \'"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-object v14, v8, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;->TargetDir:Ljava/lang/String;

    const-string v15, "~chelpus_disabled~"

    const-string v16, ""

    invoke-virtual/range {v14 .. v16}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "\'"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    iget-object v14, v8, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;->SourceDir:Ljava/lang/String;

    const-string v15, "~chelpus_disabled~"

    const-string v16, ""

    invoke-virtual/range {v14 .. v16}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    iget-object v15, v8, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;->TargetDir:Ljava/lang/String;

    const-string v16, "~chelpus_disabled~"

    const-string v17, ""

    invoke-virtual/range {v15 .. v17}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-static {v12, v13, v14, v15}, Lcom/chelpus/Utils;->verify_bind_and_run(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    :goto_192
    invoke-static {v8}, Lcom/chelpus/Utils;->checkBind(Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;)Z

    move-result v12

    if-eqz v12, :cond_3cf

    .line 274
    const v12, 0x7f0d002a

    const-string v13, "#00FF00"

    invoke-static {v13}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v13

    invoke-virtual {v11, v12, v13}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 275
    const v12, 0x7f0d002b

    const-string v13, "setBackgroundResource"

    const v14, 0x7f020051

    invoke-virtual {v11, v12, v13, v14}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 276
    move-object v2, v8

    .line 277
    .local v2, "fin_item":Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidget$2;->val$handler:Landroid/os/Handler;

    new-instance v13, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidget$2$1;

    move-object/from16 v0, p0

    invoke-direct {v13, v0, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidget$2$1;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidget$2;Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;)V

    invoke-virtual {v12, v13}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 294
    :goto_1be
    new-instance v10, Landroid/content/ComponentName;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidget$2;->val$context:Landroid/content/Context;

    const-class v13, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidget;

    invoke-direct {v10, v12, v13}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 295
    .local v10, "myWidget":Landroid/content/ComponentName;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidget$2;->val$context:Landroid/content/Context;

    invoke-static {v12}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v9

    .line 296
    .local v9, "manager":Landroid/appwidget/AppWidgetManager;
    invoke-virtual {v9, v5, v11}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 298
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidget$2;->val$context:Landroid/content/Context;

    invoke-static {v12}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v4

    .line 299
    .local v4, "gm":Landroid/appwidget/AppWidgetManager;
    new-instance v12, Landroid/content/ComponentName;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidget$2;->val$context:Landroid/content/Context;

    const-class v14, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidget;

    invoke-direct {v12, v13, v14}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v4, v12}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v6

    .line 300
    .local v6, "ids":[I
    sget-object v12, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidget;->widget:Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidget;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidget$2;->val$context:Landroid/content/Context;

    invoke-virtual {v12, v13, v4, v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidget;->onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    .line 302
    const/4 v12, 0x0

    sput-boolean v12, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->binder_process:Z

    .line 306
    .end local v1    # "bindes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;>;"
    .end local v2    # "fin_item":Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;
    .end local v3    # "found":Z
    .end local v4    # "gm":Landroid/appwidget/AppWidgetManager;
    .end local v6    # "ids":[I
    .end local v9    # "manager":Landroid/appwidget/AppWidgetManager;
    .end local v10    # "myWidget":Landroid/content/ComponentName;
    .end local v11    # "remoteViews":Landroid/widget/RemoteViews;
    :cond_1f7
    return-void

    .line 214
    .restart local v1    # "bindes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;>;"
    .restart local v3    # "found":Z
    .restart local v11    # "remoteViews":Landroid/widget/RemoteViews;
    :cond_1f8
    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/String;

    const/4 v13, 0x0

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "umount \'"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget-object v15, v8, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;->TargetDir:Ljava/lang/String;

    const-string v16, "~chelpus_disabled~"

    const-string v17, ""

    invoke-virtual/range {v15 .. v17}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "\'"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v12}, Lcom/chelpus/Utils;->cmd([Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_124

    .line 230
    .end local v1    # "bindes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;>;"
    .end local v3    # "found":Z
    :cond_226
    iget-object v12, v8, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;->TargetDir:Ljava/lang/String;

    const-string v13, "~chelpus_disabled~"

    const-string v14, ""

    invoke-virtual {v12, v13, v14}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v8, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;->TargetDir:Ljava/lang/String;

    .line 231
    iget-object v12, v8, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;->SourceDir:Ljava/lang/String;

    const-string v13, "~chelpus_disabled~"

    const-string v14, ""

    invoke-virtual {v12, v13, v14}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    iput-object v12, v8, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;->SourceDir:Ljava/lang/String;

    .line 232
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidget$2;->val$context:Landroid/content/Context;

    invoke-static {v12}, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;->getBindes(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v1

    .line 233
    .restart local v1    # "bindes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;>;"
    const/4 v3, 0x0

    .line 234
    .restart local v3    # "found":Z
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_24b
    :goto_24b
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_2b0

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;

    .line 235
    .restart local v7    # "it":Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;
    iget-object v13, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;->SourceDir:Ljava/lang/String;

    const-string v14, "~chelpus_disabled~"

    const-string v15, ""

    invoke-virtual {v13, v14, v15}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    iget-object v14, v8, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;->SourceDir:Ljava/lang/String;

    const-string v15, "~chelpus_disabled~"

    const-string v16, ""

    invoke-virtual/range {v14 .. v16}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_24b

    iget-object v13, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;->TargetDir:Ljava/lang/String;

    const-string v14, "~chelpus_disabled~"

    const-string v15, ""

    invoke-virtual {v13, v14, v15}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    iget-object v14, v8, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;->TargetDir:Ljava/lang/String;

    const-string v15, "~chelpus_disabled~"

    const-string v16, ""

    invoke-virtual/range {v14 .. v16}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_24b

    .line 236
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "~chelpus_disabled~"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-object v14, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;->TargetDir:Ljava/lang/String;

    const-string v15, "~chelpus_disabled~"

    const-string v16, ""

    invoke-virtual/range {v14 .. v16}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    iput-object v13, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;->TargetDir:Ljava/lang/String;

    .line 237
    iget-object v13, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;->SourceDir:Ljava/lang/String;

    iput-object v13, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;->SourceDir:Ljava/lang/String;

    .line 239
    const/4 v3, 0x1

    goto :goto_24b

    .line 242
    .end local v7    # "it":Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;
    :cond_2b0
    if-nez v3, :cond_348

    .line 243
    sget-boolean v12, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->su:Z

    if-eqz v12, :cond_31b

    .line 246
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "umount -f \'"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, v8, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;->TargetDir:Ljava/lang/String;

    const-string v14, "~chelpus_disabled~"

    const-string v15, ""

    invoke-virtual {v13, v14, v15}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "\'"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 247
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "umount -l \'"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, v8, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;->TargetDir:Ljava/lang/String;

    const-string v14, "~chelpus_disabled~"

    const-string v15, ""

    invoke-virtual {v13, v14, v15}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "\'"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 254
    :goto_302
    const v12, 0x7f0d002b

    const-string v13, "setBackgroundResource"

    const v14, 0x7f020050

    invoke-virtual {v11, v12, v13, v14}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 255
    const v12, 0x7f0d002a

    const-string v13, "#AAAAAA"

    invoke-static {v13}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v13

    invoke-virtual {v11, v12, v13}, Landroid/widget/RemoteViews;->setTextColor(II)V

    goto/16 :goto_192

    .line 249
    :cond_31b
    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/String;

    const/4 v13, 0x0

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "umount \'"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget-object v15, v8, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;->TargetDir:Ljava/lang/String;

    const-string v16, "~chelpus_disabled~"

    const-string v17, ""

    invoke-virtual/range {v15 .. v17}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "\'"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v12}, Lcom/chelpus/Utils;->cmd([Ljava/lang/String;)Ljava/lang/String;

    goto :goto_302

    .line 259
    :cond_348
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidget$2;->val$context:Landroid/content/Context;

    invoke-static {v1, v12}, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;->savetoFile(Ljava/util/ArrayList;Landroid/content/Context;)V

    .line 260
    sget-boolean v12, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->su:Z

    if-eqz v12, :cond_3a1

    .line 263
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "umount -f \'"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, v8, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;->TargetDir:Ljava/lang/String;

    const-string v14, "~chelpus_disabled~"

    const-string v15, ""

    invoke-virtual {v13, v14, v15}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "\'"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 264
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "umount -l \'"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, v8, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;->TargetDir:Ljava/lang/String;

    const-string v14, "~chelpus_disabled~"

    const-string v15, ""

    invoke-virtual {v13, v14, v15}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "\'"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    goto/16 :goto_192

    .line 266
    :cond_3a1
    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/String;

    const/4 v13, 0x0

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "umount \'"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget-object v15, v8, Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;->TargetDir:Ljava/lang/String;

    const-string v16, "~chelpus_disabled~"

    const-string v17, ""

    invoke-virtual/range {v15 .. v17}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "\'"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v12}, Lcom/chelpus/Utils;->cmd([Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_192

    .line 284
    :cond_3cf
    const v12, 0x7f0d002a

    const-string v13, "#FF0000"

    invoke-static {v13}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v13

    invoke-virtual {v11, v12, v13}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 285
    const v12, 0x7f0d002b

    const-string v13, "setBackgroundResource"

    const v14, 0x7f020050

    invoke-virtual {v11, v12, v13, v14}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 286
    move-object v2, v8

    .line 287
    .restart local v2    # "fin_item":Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidget$2;->val$handler:Landroid/os/Handler;

    new-instance v13, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidget$2$2;

    move-object/from16 v0, p0

    invoke-direct {v13, v0, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidget$2$2;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidget$2;Lcom/android/vending/billing/InAppBillingService/LUCK/BindItem;)V

    invoke-virtual {v12, v13}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_1be
.end method
