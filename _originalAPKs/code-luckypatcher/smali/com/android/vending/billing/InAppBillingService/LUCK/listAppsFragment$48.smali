.class Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$48;
.super Ljava/lang/Object;
.source "listAppsFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->changeDefaultDir()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

.field final synthetic val$ed:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;Landroid/widget/EditText;)V
    .registers 3
    .param p1, "this$0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    .prologue
    .line 6465
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$48;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iput-object p2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$48;->val$ed:Landroid/widget/EditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .registers 14
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 6470
    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$48;->val$ed:Landroid/widget/EditText;

    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    .line 6471
    .local v4, "newValue":Landroid/text/Editable;
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_100

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_100

    .line 6473
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    const-string v8, "\\s+"

    const-string v9, "."

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "\\/+"

    invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 6474
    .local v6, "tailsdir":[Ljava/lang/String;
    const-string v2, ""

    .line 6475
    .local v2, "data_dirs":Ljava/lang/String;
    array-length v8, v6

    const/4 v7, 0x0

    :goto_38
    if-ge v7, v8, :cond_5e

    aget-object v0, v6, v7

    .line 6476
    .local v0, "aTailsdir":Ljava/lang/String;
    const-string v9, ""

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_5b

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 6475
    :cond_5b
    add-int/lit8 v7, v7, 0x1

    goto :goto_38

    .line 6479
    .end local v0    # "aTailsdir":Ljava/lang/String;
    :cond_5e
    move-object v1, v2

    .line 6481
    .local v1, "data_dir":Ljava/lang/String;
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_ec

    .line 6482
    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$48;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    const/4 v8, 0x1

    invoke-virtual {v7, v8, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->testPath(ZLjava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_e0

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v7

    const-string v8, "sdcard"

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_e0

    .line 6483
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v7

    invoke-interface {v7}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    const-string v8, "path"

    invoke-interface {v7, v8, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    invoke-interface {v7}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 6484
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v7

    invoke-interface {v7}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    const-string v8, "manual_path"

    const/4 v9, 0x1

    invoke-interface {v7, v8, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    invoke-interface {v7}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 6485
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v7

    invoke-interface {v7}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    const-string v8, "path_changed"

    const/4 v9, 0x1

    invoke-interface {v7, v8, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    invoke-interface {v7}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 6488
    new-instance v5, Ljava/io/File;

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v7

    const-string v8, "basepath"

    const-string v9, "Noting"

    invoke-interface {v7, v8, v9}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 6489
    .local v5, "srcFolder":Ljava/io/File;
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 6490
    .local v3, "destFolder":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_e1

    .line 6491
    sget-object v7, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v8, "Directory does not exist."

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 6527
    .end local v1    # "data_dir":Ljava/lang/String;
    .end local v2    # "data_dirs":Ljava/lang/String;
    .end local v3    # "destFolder":Ljava/io/File;
    .end local v5    # "srcFolder":Ljava/io/File;
    .end local v6    # "tailsdir":[Ljava/lang/String;
    :cond_e0
    :goto_e0
    return-void

    .line 6493
    .restart local v1    # "data_dir":Ljava/lang/String;
    .restart local v2    # "data_dirs":Ljava/lang/String;
    .restart local v3    # "destFolder":Ljava/io/File;
    .restart local v5    # "srcFolder":Ljava/io/File;
    .restart local v6    # "tailsdir":[Ljava/lang/String;
    :cond_e1
    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$48;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v8, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$48$1;

    invoke-direct {v8, p0, v5, v3, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$48$1;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$48;Ljava/io/File;Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v7, v8}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runWithWait(Ljava/lang/Runnable;)V

    goto :goto_e0

    .line 6522
    .end local v3    # "destFolder":Ljava/io/File;
    .end local v5    # "srcFolder":Ljava/io/File;
    :cond_ec
    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$48;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    const v8, 0x7f070234

    invoke-static {v8}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v8

    const v9, 0x7f070105

    invoke-static {v9}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->showMessage(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_e0

    .line 6525
    .end local v1    # "data_dir":Ljava/lang/String;
    .end local v2    # "data_dirs":Ljava/lang/String;
    .end local v6    # "tailsdir":[Ljava/lang/String;
    :cond_100
    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$48;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    const v8, 0x7f070234

    invoke-static {v8}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v8

    const v9, 0x7f070104

    invoke-static {v9}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->showMessage(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_e0
.end method
