.class Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$9;
.super Ljava/lang/Object;
.source "LivepatchActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;)V
    .registers 2
    .param p1, "this$0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;

    .prologue
    .line 308
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$9;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 16
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 311
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$9;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;

    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$9;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;

    # getter for: Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->fileList:[Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$Item;
    invoke-static {v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->access$400(Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;)[Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$Item;

    move-result-object v6

    aget-object v6, v6, p3

    iget-object v6, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$Item;->file:Ljava/lang/String;

    # setter for: Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->chosenFile:Ljava/lang/String;
    invoke-static {v5, v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->access$302(Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 312
    new-instance v3, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$9;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;

    # getter for: Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->path:Ljava/io/File;
    invoke-static {v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->access$100(Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;)Ljava/io/File;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$9;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;

    # getter for: Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->chosenFile:Ljava/lang/String;
    invoke-static {v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->access$300(Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 313
    .local v3, "sel":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_98

    .line 314
    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$9;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    # setter for: Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->firstLvl:Ljava/lang/Boolean;
    invoke-static {v5, v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->access$502(Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    .line 317
    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$9;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;

    iget-object v5, v5, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->stri:Ljava/util/ArrayList;

    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$9;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;

    # getter for: Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->chosenFile:Ljava/lang/String;
    invoke-static {v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->access$300(Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 318
    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$9;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;

    const/4 v6, 0x0

    # setter for: Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->fileList:[Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$Item;
    invoke-static {v5, v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->access$402(Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;[Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$Item;)[Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$Item;

    .line 319
    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$9;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;

    new-instance v6, Ljava/io/File;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    # setter for: Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->path:Ljava/io/File;
    invoke-static {v5, v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->access$102(Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;Ljava/io/File;)Ljava/io/File;

    .line 321
    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$9;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;

    # invokes: Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->loadFileList()V
    invoke-static {v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->access$200(Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;)V

    .line 323
    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$9;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;

    const/4 v6, 0x3

    invoke-virtual {v5, v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->removeDialog(I)V

    .line 324
    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$9;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;

    const/4 v6, 0x3

    invoke-virtual {v5, v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->showDialog(I)V

    .line 325
    const-string v5, "F_PATH"

    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$9;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;

    # getter for: Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->path:Ljava/io/File;
    invoke-static {v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->access$100(Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;)Ljava/io/File;

    move-result-object v6

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 371
    :goto_97
    return-void

    .line 330
    :cond_98
    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$9;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;

    # getter for: Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->chosenFile:Ljava/lang/String;
    invoke-static {v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->access$300(Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "up"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_139

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_139

    .line 334
    :try_start_ac
    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$9;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;

    iget-object v5, v5, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->stri:Ljava/util/ArrayList;

    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$9;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;

    iget-object v6, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->stri:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 337
    .local v2, "s":Ljava/lang/String;
    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$9;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;

    new-instance v6, Ljava/io/File;

    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$9;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;

    # getter for: Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->path:Ljava/io/File;
    invoke-static {v7}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->access$100(Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;)Ljava/io/File;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$9;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;

    .line 338
    # getter for: Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->path:Ljava/io/File;
    invoke-static {v9}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->access$100(Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;)Ljava/io/File;

    move-result-object v9

    invoke-virtual {v9}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v9

    .line 337
    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    # setter for: Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->path:Ljava/io/File;
    invoke-static {v5, v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->access$102(Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;Ljava/io/File;)Ljava/io/File;

    .line 339
    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$9;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;

    const/4 v6, 0x0

    # setter for: Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->fileList:[Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$Item;
    invoke-static {v5, v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->access$402(Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;[Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$Item;)[Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$Item;
    :try_end_ed
    .catch Ljava/lang/Exception; {:try_start_ac .. :try_end_ed} :catch_123

    .line 347
    .end local v2    # "s":Ljava/lang/String;
    :goto_ed
    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$9;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;

    iget-object v5, v5, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->stri:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_101

    .line 348
    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$9;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    # setter for: Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->firstLvl:Ljava/lang/Boolean;
    invoke-static {v5, v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->access$502(Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    .line 350
    :cond_101
    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$9;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;

    # invokes: Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->loadFileList()V
    invoke-static {v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->access$200(Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;)V

    .line 352
    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$9;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;

    const/4 v6, 0x3

    invoke-virtual {v5, v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->removeDialog(I)V

    .line 353
    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$9;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;

    const/4 v6, 0x3

    invoke-virtual {v5, v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->showDialog(I)V

    .line 354
    const-string v5, "F_PATH"

    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$9;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;

    # getter for: Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->path:Ljava/io/File;
    invoke-static {v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->access$100(Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;)Ljava/io/File;

    move-result-object v6

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_97

    .line 340
    :catch_123
    move-exception v0

    .line 341
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 342
    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$9;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;

    iget-object v5, v5, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->stri:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 343
    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$9;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    # setter for: Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->firstLvl:Ljava/lang/Boolean;
    invoke-static {v5, v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->access$502(Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    goto :goto_ed

    .line 359
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_139
    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$9;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;

    # getter for: Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->path:Ljava/io/File;
    invoke-static {v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->access$100(Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;)Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    sput-object v5, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->selabpath:Ljava/lang/String;

    .line 360
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$9;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;

    # getter for: Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->path:Ljava/io/File;
    invoke-static {v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->access$100(Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;)Ljava/io/File;

    move-result-object v6

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$9;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;

    # getter for: Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->chosenFile:Ljava/lang/String;
    invoke-static {v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->access$300(Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    sput-object v5, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->selpath:Ljava/lang/String;

    .line 361
    const-string v5, "lib"

    sput-object v5, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->patch:Ljava/lang/String;

    .line 362
    const-string v5, "Help:\n\n - Pattern for original hex (length>3):\n  AA 1F ** 01 4C\n ** - any byte;\n\n - Pattern for replacement hex(length>3):\n  EA 2F ** ** **\n ** - does not change the byte;\n\nNumber of original bytes  and modified bytes must be equal; "

    sput-object v5, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->str:Ljava/lang/String;

    .line 363
    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$9;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;

    const/4 v6, 0x3

    invoke-virtual {v5, v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->removeDialog(I)V

    .line 364
    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$9;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;

    invoke-virtual {v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->getApplication()Landroid/app/Application;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 365
    .local v1, "r":Landroid/content/res/Resources;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Target is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->selpath:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ".\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 366
    .local v4, "str2":Ljava/lang/String;
    # getter for: Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->tv:Landroid/widget/TextView;
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->access$600()Landroid/widget/TextView;

    move-result-object v5

    const-string v6, "#ff00ff73"

    const-string v7, "bold"

    invoke-static {v4, v6, v7}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 367
    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$9;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    # setter for: Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->firstLvl:Ljava/lang/Boolean;
    invoke-static {v5, v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->access$502(Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    .line 368
    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$9;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;

    iget-object v5, v5, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->stri:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    goto/16 :goto_97
.end method
