.class public Lcom/android/vending/billing/InAppBillingService/LUCK/LogCollector;
.super Ljava/lang/Object;
.source "LogCollector.java"


# static fields
.field public static final LINE_SEPARATOR:Ljava/lang/String; = "\n"

.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private mLastLogs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mPackageName:Ljava/lang/String;

.field private mPattern:Ljava/util/regex/Pattern;

.field private mPrefs:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 56
    const-class v0, Lcom/android/vending/billing/InAppBillingService/LUCK/LogCollector;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/LogCollector;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 8

    .prologue
    const/4 v6, 0x0

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LogCollector;->mPackageName:Ljava/lang/String;

    .line 66
    const-string v1, "(.*)E\\/AndroidRuntime\\(\\s*\\d+\\)\\:\\s*at\\s%s.*"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LogCollector;->mPackageName:Ljava/lang/String;

    const-string v4, "."

    const-string v5, "\\."

    .line 67
    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    .line 66
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 68
    .local v0, "pattern":Ljava/lang/String;
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    iput-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LogCollector;->mPattern:Ljava/util/regex/Pattern;

    .line 69
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    .line 70
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v1

    const-string v2, "LogCollector"

    invoke-virtual {v1, v2, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LogCollector;->mPrefs:Landroid/content/SharedPreferences;

    .line 71
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LogCollector;->mLastLogs:Ljava/util/ArrayList;

    .line 72
    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LogCollector;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 73
    return-void
.end method

.method private collectLog(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Z)V
    .registers 23
    .param p2, "format"    # Ljava/lang/String;
    .param p3, "buffer"    # Ljava/lang/String;
    .param p4, "filterSpecs"    # [Ljava/lang/String;
    .param p5, "noRoot"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 113
    .local p1, "outLines":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->clear()V

    .line 115
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 117
    .local v11, "params":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-nez p2, :cond_c

    .line 118
    const-string p2, "time"

    .line 121
    :cond_c
    const-string v13, "-v"

    invoke-virtual {v11, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 122
    move-object/from16 v0, p2

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 124
    if-eqz p3, :cond_22

    .line 125
    const-string v13, "-b"

    invoke-virtual {v11, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 126
    move-object/from16 v0, p3

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 129
    :cond_22
    if-eqz p4, :cond_29

    .line 130
    move-object/from16 v0, p4

    invoke-static {v11, v0}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 135
    :cond_29
    :try_start_29
    sget-boolean v13, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->su:Z

    if-eqz v13, :cond_2f

    if-eqz p5, :cond_18e

    .line 136
    :cond_2f
    sget-object v13, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v14, "Collect logs no root."

    invoke-virtual {v13, v14}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 137
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 138
    .local v5, "commandLine":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v13, "logcat"

    invoke-virtual {v5, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 139
    const-string v13, "-d"

    invoke-virtual {v5, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 140
    invoke-virtual {v5, v11}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 141
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v13

    new-array v3, v13, [Ljava/lang/String;

    .line 142
    .local v3, "cmdLine":[Ljava/lang/String;
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "cmdLine":[Ljava/lang/String;
    check-cast v3, [Ljava/lang/String;

    .line 143
    .restart local v3    # "cmdLine":[Ljava/lang/String;
    sget-object v13, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    sget-object v14, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patchAct:Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;

    const-string v15, "sdcard"

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v14

    invoke-virtual {v14}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_82

    .line 144
    const/4 v13, 0x4

    new-array v4, v13, [Ljava/lang/String;

    const/4 v13, 0x0

    const-string v14, "logcat"

    aput-object v14, v4, v13

    const/4 v13, 0x1

    const-string v14, "-d"

    aput-object v14, v4, v13

    const/4 v13, 0x2

    const-string v14, "System.out:*"

    aput-object v14, v4, v13

    const/4 v13, 0x3

    const-string v14, "*:S"

    aput-object v14, v4, v13

    .line 145
    .local v4, "cmdLine2":[Ljava/lang/String;
    move-object v3, v4

    .line 147
    .end local v4    # "cmdLine2":[Ljava/lang/String;
    :cond_82
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v13

    invoke-virtual {v13, v3}, Ljava/lang/Runtime;->exec([Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v12

    .line 148
    .local v12, "process":Ljava/lang/Process;
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v13, Ljava/io/InputStreamReader;

    invoke-virtual {v12}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v2, v13}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 154
    .local v2, "bufferedReader":Ljava/io/BufferedReader;
    :goto_98
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v9

    .local v9, "line":Ljava/lang/String;
    if-eqz v9, :cond_116

    .line 155
    move-object/from16 v0, p1

    invoke-interface {v0, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_a3
    .catch Ljava/io/IOException; {:try_start_29 .. :try_end_a3} :catch_a4

    goto :goto_98

    .line 246
    .end local v2    # "bufferedReader":Ljava/io/BufferedReader;
    .end local v3    # "cmdLine":[Ljava/lang/String;
    .end local v5    # "commandLine":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v9    # "line":Ljava/lang/String;
    .end local v12    # "process":Ljava/lang/Process;
    :catch_a4
    move-exception v6

    .line 250
    .local v6, "e":Ljava/io/IOException;
    :try_start_a5
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v13

    const-string v14, "su"

    invoke-virtual {v13, v14}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v12

    .line 253
    .restart local v12    # "process":Ljava/lang/Process;
    new-instance v10, Ljava/io/DataOutputStream;

    invoke-virtual {v12}, Ljava/lang/Process;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v13

    invoke-direct {v10, v13}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 255
    .local v10, "os":Ljava/io/DataOutputStream;
    new-instance v8, Ljava/io/DataInputStream;

    invoke-virtual {v12}, Ljava/lang/Process;->getErrorStream()Ljava/io/InputStream;

    move-result-object v13

    invoke-direct {v8, v13}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 256
    .local v8, "errorGobbler":Ljava/io/DataInputStream;
    invoke-virtual {v8}, Ljava/io/DataInputStream;->available()I

    move-result v13

    new-array v1, v13, [B

    .line 257
    .local v1, "arrayErrorOfByte":[B
    invoke-virtual {v8, v1}, Ljava/io/DataInputStream;->read([B)I

    .line 258
    sget-object v13, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v14, Ljava/lang/String;

    invoke-direct {v14, v1}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v13, v14}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 260
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v13, Ljava/io/InputStreamReader;

    invoke-virtual {v12}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v2, v13}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 261
    .restart local v2    # "bufferedReader":Ljava/io/BufferedReader;
    sget-object v13, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    sget-object v14, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patchAct:Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;

    const-string v15, "sdcard"

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v14

    invoke-virtual {v14}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_2a2

    .line 262
    const-string v13, "logcat -d System.out:* *:S\n"

    invoke-virtual {v10, v13}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    .line 266
    :goto_fd
    invoke-virtual {v10}, Ljava/io/DataOutputStream;->flush()V

    .line 267
    invoke-virtual {v10}, Ljava/io/DataOutputStream;->close()V

    .line 273
    const-string v9, ""

    .line 274
    .restart local v9    # "line":Ljava/lang/String;
    :goto_105
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_2a9

    .line 275
    move-object/from16 v0, p1

    invoke-interface {v0, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_110
    .catch Ljava/io/IOException; {:try_start_a5 .. :try_end_110} :catch_111

    goto :goto_105

    .line 279
    .end local v1    # "arrayErrorOfByte":[B
    .end local v2    # "bufferedReader":Ljava/io/BufferedReader;
    .end local v8    # "errorGobbler":Ljava/io/DataInputStream;
    .end local v9    # "line":Ljava/lang/String;
    .end local v10    # "os":Ljava/io/DataOutputStream;
    .end local v12    # "process":Ljava/lang/Process;
    :catch_111
    move-exception v7

    .line 281
    .local v7, "e1":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    .line 285
    .end local v6    # "e":Ljava/io/IOException;
    .end local v7    # "e1":Ljava/io/IOException;
    :cond_115
    :goto_115
    return-void

    .line 157
    .restart local v2    # "bufferedReader":Ljava/io/BufferedReader;
    .restart local v3    # "cmdLine":[Ljava/lang/String;
    .restart local v5    # "commandLine":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v9    # "line":Ljava/lang/String;
    .restart local v12    # "process":Ljava/lang/Process;
    :cond_116
    :try_start_116
    invoke-virtual {v12}, Ljava/lang/Process;->destroy()V

    .line 158
    if-eqz v2, :cond_11e

    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V

    .line 213
    :cond_11e
    :goto_11e
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v13

    if-nez v13, :cond_115

    .line 215
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v13

    const-string v14, "su"

    invoke-virtual {v13, v14}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v12

    .line 218
    new-instance v10, Ljava/io/DataOutputStream;

    invoke-virtual {v12}, Ljava/lang/Process;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v13

    invoke-direct {v10, v13}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 220
    .restart local v10    # "os":Ljava/io/DataOutputStream;
    new-instance v8, Ljava/io/DataInputStream;

    invoke-virtual {v12}, Ljava/lang/Process;->getErrorStream()Ljava/io/InputStream;

    move-result-object v13

    invoke-direct {v8, v13}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 221
    .restart local v8    # "errorGobbler":Ljava/io/DataInputStream;
    invoke-virtual {v8}, Ljava/io/DataInputStream;->available()I

    move-result v13

    new-array v1, v13, [B

    .line 222
    .restart local v1    # "arrayErrorOfByte":[B
    invoke-virtual {v8, v1}, Ljava/io/DataInputStream;->read([B)I

    .line 223
    sget-object v13, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v14, Ljava/lang/String;

    invoke-direct {v14, v1}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v13, v14}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 225
    new-instance v2, Ljava/io/BufferedReader;

    .end local v2    # "bufferedReader":Ljava/io/BufferedReader;
    new-instance v13, Ljava/io/InputStreamReader;

    invoke-virtual {v12}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v2, v13}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 226
    .restart local v2    # "bufferedReader":Ljava/io/BufferedReader;
    sget-object v13, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    sget-object v14, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patchAct:Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;

    const-string v15, "sdcard"

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v14

    invoke-virtual {v14}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_291

    .line 227
    const-string v13, "logcat -d System.out:* *:S\n"

    invoke-virtual {v10, v13}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    .line 231
    :goto_17c
    invoke-virtual {v10}, Ljava/io/DataOutputStream;->flush()V

    .line 232
    invoke-virtual {v10}, Ljava/io/DataOutputStream;->close()V

    .line 239
    :goto_182
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_298

    .line 240
    move-object/from16 v0, p1

    invoke-interface {v0, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_182

    .line 160
    .end local v1    # "arrayErrorOfByte":[B
    .end local v2    # "bufferedReader":Ljava/io/BufferedReader;
    .end local v3    # "cmdLine":[Ljava/lang/String;
    .end local v5    # "commandLine":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v8    # "errorGobbler":Ljava/io/DataInputStream;
    .end local v9    # "line":Ljava/lang/String;
    .end local v10    # "os":Ljava/io/DataOutputStream;
    .end local v12    # "process":Ljava/lang/Process;
    :cond_18e
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v13

    const-string v14, "su"

    invoke-virtual {v13, v14}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v12

    .line 161
    .restart local v12    # "process":Ljava/lang/Process;
    new-instance v10, Ljava/io/DataOutputStream;

    invoke-virtual {v12}, Ljava/lang/Process;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v13

    invoke-direct {v10, v13}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 162
    .restart local v10    # "os":Ljava/io/DataOutputStream;
    new-instance v8, Ljava/io/DataInputStream;

    invoke-virtual {v12}, Ljava/lang/Process;->getErrorStream()Ljava/io/InputStream;

    move-result-object v13

    invoke-direct {v8, v13}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 163
    .restart local v8    # "errorGobbler":Ljava/io/DataInputStream;
    invoke-virtual {v8}, Ljava/io/DataInputStream;->available()I

    move-result v13

    new-array v1, v13, [B

    .line 164
    .restart local v1    # "arrayErrorOfByte":[B
    invoke-virtual {v8, v1}, Ljava/io/DataInputStream;->read([B)I

    .line 165
    sget-object v13, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v14, Ljava/lang/String;

    invoke-direct {v14, v1}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v13, v14}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 166
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v13, Ljava/io/InputStreamReader;

    invoke-virtual {v12}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v2, v13}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 167
    .restart local v2    # "bufferedReader":Ljava/io/BufferedReader;
    sget-object v13, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    sget-object v14, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patchAct:Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;

    const-string v15, "sdcard"

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v14

    invoke-virtual {v14}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_280

    .line 168
    const-string v13, "logcat -d System.out:* *:S\n"

    invoke-virtual {v10, v13}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    .line 172
    :goto_1e6
    invoke-virtual {v10}, Ljava/io/DataOutputStream;->flush()V

    .line 173
    invoke-virtual {v10}, Ljava/io/DataOutputStream;->close()V
    :try_end_1ec
    .catch Ljava/io/IOException; {:try_start_116 .. :try_end_1ec} :catch_a4

    .line 181
    :goto_1ec
    :try_start_1ec
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v9

    .restart local v9    # "line":Ljava/lang/String;
    if-eqz v9, :cond_1fc

    .line 182
    move-object/from16 v0, p1

    invoke-interface {v0, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1f7
    .catch Ljava/lang/Exception; {:try_start_1ec .. :try_end_1f7} :catch_1f8
    .catch Ljava/io/IOException; {:try_start_1ec .. :try_end_1f7} :catch_a4

    goto :goto_1ec

    .line 184
    .end local v9    # "line":Ljava/lang/String;
    :catch_1f8
    move-exception v6

    .line 185
    .local v6, "e":Ljava/lang/Exception;
    :try_start_1f9
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    .line 187
    .end local v6    # "e":Ljava/lang/Exception;
    :cond_1fc
    if-eqz v2, :cond_201

    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V

    .line 188
    :cond_201
    invoke-virtual {v12}, Ljava/lang/Process;->destroy()V

    .line 189
    sget-object v13, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v14, "Collect logs no root."

    invoke-virtual {v13, v14}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 190
    const-string v13, "\n\n\n*********************************** NO ROOT *******************************************************\n\n\n"

    move-object/from16 v0, p1

    invoke-interface {v0, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 191
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 192
    .restart local v5    # "commandLine":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v13, "logcat"

    invoke-virtual {v5, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 193
    const-string v13, "-d"

    invoke-virtual {v5, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 194
    invoke-virtual {v5, v11}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 195
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v13

    new-array v3, v13, [Ljava/lang/String;

    .line 196
    .restart local v3    # "cmdLine":[Ljava/lang/String;
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "cmdLine":[Ljava/lang/String;
    check-cast v3, [Ljava/lang/String;

    .line 197
    .restart local v3    # "cmdLine":[Ljava/lang/String;
    sget-object v13, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    sget-object v14, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patchAct:Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;

    const-string v15, "sdcard"

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v14

    invoke-virtual {v14}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_25e

    .line 198
    const/4 v13, 0x4

    new-array v4, v13, [Ljava/lang/String;

    const/4 v13, 0x0

    const-string v14, "logcat"

    aput-object v14, v4, v13

    const/4 v13, 0x1

    const-string v14, "-d"

    aput-object v14, v4, v13

    const/4 v13, 0x2

    const-string v14, "System.out:*"

    aput-object v14, v4, v13

    const/4 v13, 0x3

    const-string v14, "*:S"

    aput-object v14, v4, v13

    .line 199
    .restart local v4    # "cmdLine2":[Ljava/lang/String;
    move-object v3, v4

    .line 201
    .end local v4    # "cmdLine2":[Ljava/lang/String;
    :cond_25e
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v13

    invoke-virtual {v13, v3}, Ljava/lang/Runtime;->exec([Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v12

    .line 202
    new-instance v2, Ljava/io/BufferedReader;

    .end local v2    # "bufferedReader":Ljava/io/BufferedReader;
    new-instance v13, Ljava/io/InputStreamReader;

    invoke-virtual {v12}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v2, v13}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 207
    .restart local v2    # "bufferedReader":Ljava/io/BufferedReader;
    :goto_274
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v9

    .restart local v9    # "line":Ljava/lang/String;
    if-eqz v9, :cond_287

    .line 208
    move-object/from16 v0, p1

    invoke-interface {v0, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_274

    .line 170
    .end local v3    # "cmdLine":[Ljava/lang/String;
    .end local v5    # "commandLine":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v9    # "line":Ljava/lang/String;
    :cond_280
    const-string v13, "logcat -d -v time\n"

    invoke-virtual {v10, v13}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    goto/16 :goto_1e6

    .line 210
    .restart local v3    # "cmdLine":[Ljava/lang/String;
    .restart local v5    # "commandLine":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v9    # "line":Ljava/lang/String;
    :cond_287
    invoke-virtual {v12}, Ljava/lang/Process;->destroy()V

    .line 211
    if-eqz v2, :cond_11e

    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V

    goto/16 :goto_11e

    .line 229
    :cond_291
    const-string v13, "logcat -d\n"

    invoke-virtual {v10, v13}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    goto/16 :goto_17c

    .line 242
    :cond_298
    invoke-virtual {v12}, Ljava/lang/Process;->destroy()V

    .line 243
    if-eqz v2, :cond_115

    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_2a0
    .catch Ljava/io/IOException; {:try_start_1f9 .. :try_end_2a0} :catch_a4

    goto/16 :goto_115

    .line 264
    .end local v3    # "cmdLine":[Ljava/lang/String;
    .end local v5    # "commandLine":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v9    # "line":Ljava/lang/String;
    .local v6, "e":Ljava/io/IOException;
    :cond_2a2
    :try_start_2a2
    const-string v13, "logcat -d\n"

    invoke-virtual {v10, v13}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    goto/16 :goto_fd

    .line 277
    .restart local v9    # "line":Ljava/lang/String;
    :cond_2a9
    invoke-virtual {v12}, Ljava/lang/Process;->destroy()V
    :try_end_2ac
    .catch Ljava/io/IOException; {:try_start_2a2 .. :try_end_2ac} :catch_111

    goto/16 :goto_115
.end method

.method private collectPhoneInfo()Ljava/lang/String;
    .registers 5

    .prologue
    .line 329
    const-string v0, "Carrier:%s\nModel:%s\nFirmware:%s\n"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    sget-object v3, Landroid/os/Build;->BRAND:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    sget-object v3, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public collect(Landroid/content/Context;Z)Z
    .registers 20
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "writeToFile"    # Z

    .prologue
    .line 341
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/LogCollector;->mLastLogs:Ljava/util/ArrayList;

    .line 342
    .local v2, "lines":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    sget-boolean v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->su:Z

    if-eqz v1, :cond_94

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/LogCollector;->collectLog(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Z)V

    .line 344
    :goto_11
    if-eqz p2, :cond_13b

    .line 345
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_13b

    .line 347
    const/16 v16, 0x0

    .line 348
    .local v16, "sb":Ljava/lang/StringBuilder;
    const-string v15, ""

    .line 350
    .local v15, "preface":Ljava/lang/String;
    :try_start_1d
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    iget-object v15, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
    :try_end_2c
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1d .. :try_end_2c} :catch_9f

    .line 354
    :goto_2c
    new-instance v1, Ljava/lang/StringBuilder;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Lucky Patcher "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "\n"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    .line 356
    invoke-direct/range {p0 .. p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/LogCollector;->collectPhoneInfo()Ljava/lang/String;

    move-result-object v14

    .line 357
    .local v14, "phoneInfo":Ljava/lang/String;
    const-string v1, "\n"

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 361
    :goto_59
    :try_start_59
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5d
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_a4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .local v10, "line":Ljava/lang/String;
    const-string v3, "\n"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_74
    .catch Ljava/util/ConcurrentModificationException; {:try_start_59 .. :try_end_74} :catch_75

    goto :goto_5d

    .line 364
    .end local v10    # "line":Ljava/lang/String;
    :catch_75
    move-exception v8

    .line 365
    .local v8, "e":Ljava/util/ConcurrentModificationException;
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "LuckyPatcher (LogCollector): try again collect log."

    invoke-virtual {v1, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 366
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "\n"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    .line 367
    const-string v1, "\n"

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_59

    .line 343
    .end local v8    # "e":Ljava/util/ConcurrentModificationException;
    .end local v14    # "phoneInfo":Ljava/lang/String;
    .end local v15    # "preface":Ljava/lang/String;
    .end local v16    # "sb":Ljava/lang/StringBuilder;
    :cond_94
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/LogCollector;->collectLog(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Z)V

    goto/16 :goto_11

    .line 351
    .restart local v15    # "preface":Ljava/lang/String;
    .restart local v16    # "sb":Ljava/lang/StringBuilder;
    :catch_9f
    move-exception v8

    .line 352
    .local v8, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v8}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_2c

    .line 370
    .end local v8    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v14    # "phoneInfo":Ljava/lang/String;
    :cond_a4
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 372
    .local v7, "content":Ljava/lang/String;
    new-instance v12, Ljava/io/File;

    const-string v1, "abrakakdabra"

    invoke-direct {v12, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 376
    .local v12, "logfile":Ljava/io/File;
    :try_start_af
    new-instance v13, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "/Log/log.txt"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v13, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_c9
    .catch Ljava/io/IOException; {:try_start_af .. :try_end_c9} :catch_143

    .line 377
    .end local v12    # "logfile":Ljava/io/File;
    .local v13, "logfile":Ljava/io/File;
    :try_start_c9
    new-instance v11, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "/Log"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v11, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 378
    .local v11, "logdir":Ljava/io/File;
    invoke-virtual {v11}, Ljava/io/File;->mkdirs()Z

    .line 379
    invoke-virtual {v13}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_ef

    invoke-virtual {v13}, Ljava/io/File;->delete()Z

    .line 380
    :cond_ef
    new-instance v1, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/Log/log.zip"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_12c

    .line 381
    new-instance v1, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/Log/log.zip"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 382
    :cond_12c
    invoke-virtual {v13}, Ljava/io/File;->createNewFile()Z

    .line 383
    new-instance v9, Ljava/io/FileOutputStream;

    invoke-direct {v9, v13}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 385
    .local v9, "fout":Ljava/io/FileOutputStream;
    invoke-virtual {v7}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v9, v1}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_13b
    .catch Ljava/io/IOException; {:try_start_c9 .. :try_end_13b} :catch_14a

    .line 393
    .end local v7    # "content":Ljava/lang/String;
    .end local v9    # "fout":Ljava/io/FileOutputStream;
    .end local v11    # "logdir":Ljava/io/File;
    .end local v13    # "logfile":Ljava/io/File;
    .end local v14    # "phoneInfo":Ljava/lang/String;
    .end local v15    # "preface":Ljava/lang/String;
    .end local v16    # "sb":Ljava/lang/StringBuilder;
    :cond_13b
    :goto_13b
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_148

    const/4 v1, 0x1

    :goto_142
    return v1

    .line 387
    .restart local v7    # "content":Ljava/lang/String;
    .restart local v12    # "logfile":Ljava/io/File;
    .restart local v14    # "phoneInfo":Ljava/lang/String;
    .restart local v15    # "preface":Ljava/lang/String;
    .restart local v16    # "sb":Ljava/lang/StringBuilder;
    :catch_143
    move-exception v8

    .line 389
    .local v8, "e":Ljava/io/IOException;
    :goto_144
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_13b

    .line 393
    .end local v7    # "content":Ljava/lang/String;
    .end local v8    # "e":Ljava/io/IOException;
    .end local v12    # "logfile":Ljava/io/File;
    .end local v14    # "phoneInfo":Ljava/lang/String;
    .end local v15    # "preface":Ljava/lang/String;
    .end local v16    # "sb":Ljava/lang/StringBuilder;
    :cond_148
    const/4 v1, 0x0

    goto :goto_142

    .line 387
    .restart local v7    # "content":Ljava/lang/String;
    .restart local v13    # "logfile":Ljava/io/File;
    .restart local v14    # "phoneInfo":Ljava/lang/String;
    .restart local v15    # "preface":Ljava/lang/String;
    .restart local v16    # "sb":Ljava/lang/StringBuilder;
    :catch_14a
    move-exception v8

    move-object v12, v13

    .end local v13    # "logfile":Ljava/io/File;
    .restart local v12    # "logfile":Ljava/io/File;
    goto :goto_144
.end method

.method public hasForceCloseHappened()Z
    .registers 15

    .prologue
    .line 288
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v2, "*:E"

    aput-object v2, v4, v0

    .line 289
    .local v4, "filterSpecs":[Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 290
    .local v1, "lines":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    sget-boolean v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->su:Z

    if-eqz v0, :cond_5a

    const-string v2, "time"

    const/4 v3, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/LogCollector;->collectLog(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Z)V

    .line 293
    :goto_19
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_7c

    .line 294
    const/4 v8, 0x0

    .line 295
    .local v8, "forceClosedSinceLastCheck":Z
    const/4 v7, 0x0

    .line 296
    .local v7, "error_found":Z
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_25
    :goto_25
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_63

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 298
    .local v10, "line":Ljava/lang/String;
    iget-object v2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LogCollector;->mPattern:Ljava/util/regex/Pattern;

    invoke-virtual {v2, v10}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v11

    .line 299
    .local v11, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v11}, Ljava/util/regex/Matcher;->matches()Z

    move-result v9

    .line 300
    .local v9, "isMyStackTrace":Z
    iget-object v12, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LogCollector;->mPrefs:Landroid/content/SharedPreferences;

    .line 301
    .local v12, "prefs":Landroid/content/SharedPreferences;
    if-eqz v9, :cond_25

    .line 303
    const/4 v7, 0x1

    .line 304
    const/4 v2, 0x1

    invoke-virtual {v11, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v13

    .line 305
    .local v13, "timestamp":Ljava/lang/String;
    const/4 v2, 0x0

    invoke-interface {v12, v13, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    .line 309
    .local v6, "appeared":Z
    if-nez v6, :cond_25

    .line 311
    const/4 v8, 0x1

    .line 312
    invoke-interface {v12}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v2, v13, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_25

    .line 291
    .end local v6    # "appeared":Z
    .end local v7    # "error_found":Z
    .end local v8    # "forceClosedSinceLastCheck":Z
    .end local v9    # "isMyStackTrace":Z
    .end local v10    # "line":Ljava/lang/String;
    .end local v11    # "matcher":Ljava/util/regex/Matcher;
    .end local v12    # "prefs":Landroid/content/SharedPreferences;
    .end local v13    # "timestamp":Ljava/lang/String;
    :cond_5a
    const-string v2, "time"

    const/4 v3, 0x0

    const/4 v5, 0x1

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/LogCollector;->collectLog(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Z)V

    goto :goto_19

    .line 316
    .restart local v7    # "error_found":Z
    .restart local v8    # "forceClosedSinceLastCheck":Z
    :cond_63
    if-nez v8, :cond_7b

    if-nez v7, :cond_7b

    .line 317
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "Clear all FC logcat prefs..."

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 318
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LogCollector;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 325
    .end local v7    # "error_found":Z
    .end local v8    # "forceClosedSinceLastCheck":Z
    :cond_7b
    :goto_7b
    return v8

    .line 322
    :cond_7c
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "Clear all FC logcat prefs..."

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 323
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LogCollector;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 325
    const/4 v8, 0x0

    goto :goto_7b
.end method

.method public sendLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 22
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "email"    # Ljava/lang/String;
    .param p3, "subject"    # Ljava/lang/String;
    .param p4, "preface"    # Ljava/lang/String;

    .prologue
    .line 398
    new-instance v8, Ljava/io/File;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v14, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "/Log/log.txt"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v8, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 400
    .local v8, "logfile":Ljava/io/File;
    const-string v13, "file://"

    invoke-static {v13}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    .line 402
    .local v10, "uri":Landroid/net/Uri;
    :try_start_20
    new-instance v12, Lnet/lingala/zip4j/core/ZipFile;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v14, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "/Log/log.zip"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Lnet/lingala/zip4j/core/ZipFile;-><init>(Ljava/lang/String;)V

    .line 403
    .local v12, "zip":Lnet/lingala/zip4j/core/ZipFile;
    new-instance v9, Lnet/lingala/zip4j/model/ZipParameters;

    invoke-direct {v9}, Lnet/lingala/zip4j/model/ZipParameters;-><init>()V

    .line 405
    .local v9, "parameters":Lnet/lingala/zip4j/model/ZipParameters;
    const/16 v13, 0x8

    invoke-virtual {v9, v13}, Lnet/lingala/zip4j/model/ZipParameters;->setCompressionMethod(I)V

    .line 407
    const/4 v13, 0x5

    invoke-virtual {v9, v13}, Lnet/lingala/zip4j/model/ZipParameters;->setCompressionLevel(I)V

    .line 408
    invoke-virtual {v12, v8, v9}, Lnet/lingala/zip4j/core/ZipFile;->addFile(Ljava/io/File;Lnet/lingala/zip4j/model/ZipParameters;)V

    .line 409
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "file://"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    sget-object v14, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "/Log/log.zip"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_69
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_20 .. :try_end_69} :catch_1ab
    .catch Ljava/lang/Exception; {:try_start_20 .. :try_end_69} :catch_2f4

    move-result-object v10

    .line 417
    .end local v9    # "parameters":Lnet/lingala/zip4j/model/ZipParameters;
    .end local v12    # "zip":Lnet/lingala/zip4j/core/ZipFile;
    :goto_6a
    const-string v2, "not found"

    .line 418
    .local v2, "dalvikvm_file":Ljava/lang/String;
    const-string v1, ""

    .line 419
    .local v1, "content":Ljava/lang/String;
    const-string v13, "/system/bin/dalvikvm"

    invoke-static {v13}, Lcom/chelpus/Utils;->exists(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_78

    const-string v2, "/system/bin/dalvikvm"

    .line 420
    :cond_78
    const-string v13, "/system/bin/dalvikvm32"

    invoke-static {v13}, Lcom/chelpus/Utils;->exists(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_82

    const-string v2, "/system/bin/dalvikvm32"

    .line 421
    :cond_82
    sget-object v13, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    sget-object v14, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patchAct:Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;

    const-string v15, "sdcard"

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v14

    invoke-virtual {v14}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_1b1

    .line 422
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Email contain log.txt with bugs by Lucky Patcher! Please describe the Problem and tap to send email.\n\n\nRoot:"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    sget-boolean v14, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->su:Z

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "\n"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "Runtime: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-static {}, Lcom/chelpus/Utils;->getCurrentRuntimeValue()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "\n"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "DeviceID: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v14

    invoke-virtual {v14}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v14

    const-string v15, "android_id"

    invoke-static {v14, v15}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "\n"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    sget-object v14, Landroid/os/Build;->BOARD:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "\n"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    sget-object v14, Landroid/os/Build;->BRAND:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "\n"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    sget-object v14, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "\n"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    sget-object v14, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "\n"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    sget-object v14, Landroid/os/Build;->DISPLAY:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "\n"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    sget-object v14, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "\n"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "Lucky Patcher ver: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    sget-object v14, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->version:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "\n"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "LP files directory: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    sget-object v14, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "\n"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "dalvikvm file:"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "\n\n"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 427
    :goto_165
    :try_start_165
    invoke-static {}, Ljava/lang/System;->getenv()Ljava/util/Map;

    move-result-object v5

    .line 428
    .local v5, "envs":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v13

    invoke-interface {v13}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :goto_171
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_280

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 430
    .local v4, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, "\n"

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v15, ":"

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_1a9
    .catch Ljava/lang/Exception; {:try_start_165 .. :try_end_1a9} :catch_27c

    move-result-object v1

    .line 431
    goto :goto_171

    .line 411
    .end local v1    # "content":Ljava/lang/String;
    .end local v2    # "dalvikvm_file":Ljava/lang/String;
    .end local v4    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v5    # "envs":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :catch_1ab
    move-exception v3

    .line 413
    .local v3, "e":Lnet/lingala/zip4j/exception/ZipException;
    invoke-virtual {v3}, Lnet/lingala/zip4j/exception/ZipException;->printStackTrace()V

    goto/16 :goto_6a

    .line 424
    .end local v3    # "e":Lnet/lingala/zip4j/exception/ZipException;
    .restart local v1    # "content":Ljava/lang/String;
    .restart local v2    # "dalvikvm_file":Ljava/lang/String;
    :cond_1b1
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Email contain log.txt with bugs by Lucky Patcher! Please describe the Problem and tap to send email.\n\n\nRoot:"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    sget-boolean v14, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->su:Z

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "\n"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "Runtime: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-static {}, Lcom/chelpus/Utils;->getCurrentRuntimeValue()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "\n"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "DeviceID: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v14

    invoke-virtual {v14}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v14

    const-string v15, "android_id"

    invoke-static {v14, v15}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "\n"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    sget-object v14, Landroid/os/Build;->BOARD:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "\n"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    sget-object v14, Landroid/os/Build;->BRAND:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "\n"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    sget-object v14, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "\n"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    sget-object v14, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "\n"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    sget-object v14, Landroid/os/Build;->DISPLAY:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "\n"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    sget-object v14, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "\n"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "Lucky Patcher ver: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    sget-object v14, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->version:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "\n"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "LP files directory: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    sget-object v14, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "\n"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "dalvikvm file:"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "\n\n"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_165

    .line 433
    :catch_27c
    move-exception v3

    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    .line 434
    .end local v3    # "e":Ljava/lang/Exception;
    :cond_280
    new-instance v6, Landroid/content/Intent;

    const-string v13, "android.intent.action.SEND_MULTIPLE"

    invoke-direct {v6, v13}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 436
    .local v6, "intent":Landroid/content/Intent;
    const-string v13, "text/plain"

    invoke-virtual {v6, v13}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 437
    const-string v13, "android.intent.extra.EMAIL"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/String;

    const/4 v15, 0x0

    aput-object p2, v14, v15

    invoke-virtual {v6, v13, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 438
    const-string v13, "android.intent.extra.SUBJECT"

    move-object/from16 v0, p3

    invoke-virtual {v6, v13, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 439
    const-string v13, "android.intent.extra.TEXT"

    invoke-virtual {v6, v13, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 442
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 443
    .local v11, "uris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    invoke-virtual {v11, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 444
    new-instance v7, Ljava/io/File;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v14, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "/Log/Exception."

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    sget-object v14, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->version:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ".txt"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v7, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 445
    .local v7, "log":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v13

    if-eqz v13, :cond_2de

    invoke-static {v7}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v13

    invoke-virtual {v11, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 446
    :cond_2de
    const-string v13, "android.intent.extra.STREAM"

    invoke-virtual {v6, v13, v11}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 449
    :try_start_2e3
    sget-object v13, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patchAct:Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;

    const-string v14, "Send mail"

    invoke-static {v6, v14}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_2ee
    .catch Ljava/lang/RuntimeException; {:try_start_2e3 .. :try_end_2ee} :catch_2ef

    .line 451
    :goto_2ee
    return-void

    .line 450
    :catch_2ef
    move-exception v3

    .local v3, "e":Ljava/lang/RuntimeException;
    invoke-virtual {v3}, Ljava/lang/RuntimeException;->printStackTrace()V

    goto :goto_2ee

    .line 414
    .end local v1    # "content":Ljava/lang/String;
    .end local v2    # "dalvikvm_file":Ljava/lang/String;
    .end local v3    # "e":Ljava/lang/RuntimeException;
    .end local v6    # "intent":Landroid/content/Intent;
    .end local v7    # "log":Ljava/io/File;
    .end local v11    # "uris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    :catch_2f4
    move-exception v13

    goto/16 :goto_6a
.end method
