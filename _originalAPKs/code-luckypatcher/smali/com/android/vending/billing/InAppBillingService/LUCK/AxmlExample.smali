.class public Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample;
.super Ljava/lang/Object;
.source "AxmlExample.java"


# static fields
.field public static result:Z


# instance fields
.field activities:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field found1:Z

.field found2:Z

.field permissions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 217
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample;->result:Z

    return-void
.end method

.method public constructor <init>()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample;->permissions:Ljava/util/ArrayList;

    .line 64
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample;->activities:Ljava/util/ArrayList;

    .line 65
    iput-boolean v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample;->found1:Z

    iput-boolean v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample;->found2:Z

    return-void
.end method


# virtual methods
.method public changePackageName(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)Z
    .registers 12
    .param p1, "manifestFile"    # Ljava/io/File;
    .param p2, "oldPackageName"    # Ljava/lang/String;
    .param p3, "newPackageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 315
    const/4 v6, 0x0

    sput-boolean v6, Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample;->result:Z

    .line 316
    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v6

    long-to-int v6, v6

    new-array v3, v6, [B

    .line 318
    .local v3, "data1":[B
    :try_start_a
    new-instance v6, Ljava/io/FileInputStream;

    invoke-direct {v6, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {v6, v3}, Ljava/io/FileInputStream;->read([B)I
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_12} :catch_3c

    .line 323
    :goto_12
    new-instance v0, Lpxb/android/axml/AxmlReader;

    invoke-direct {v0, v3}, Lpxb/android/axml/AxmlReader;-><init>([B)V

    .line 324
    .local v0, "ar":Lpxb/android/axml/AxmlReader;
    new-instance v1, Lpxb/android/axml/AxmlWriter;

    invoke-direct {v1}, Lpxb/android/axml/AxmlWriter;-><init>()V

    .line 326
    .local v1, "aw":Lpxb/android/axml/AxmlWriter;
    new-instance v6, Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample$3;

    invoke-direct {v6, p0, v1, p2, p3}, Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample$3;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample;Lpxb/android/axml/NodeVisitor;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v6}, Lpxb/android/axml/AxmlReader;->accept(Lpxb/android/axml/AxmlVisitor;)V

    .line 449
    invoke-virtual {v1}, Lpxb/android/axml/AxmlWriter;->toByteArray()[B

    move-result-object v2

    .line 454
    .local v2, "data":[B
    :try_start_28
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    .line 455
    invoke-virtual {p1}, Ljava/io/File;->createNewFile()Z

    .line 458
    new-instance v5, Ljava/io/FileOutputStream;

    invoke-direct {v5, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 459
    .local v5, "fo":Ljava/io/FileOutputStream;
    invoke-virtual {v5, v2}, Ljava/io/FileOutputStream;->write([B)V

    .line 460
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_39
    .catch Ljava/io/IOException; {:try_start_28 .. :try_end_39} :catch_41

    .line 465
    .end local v5    # "fo":Ljava/io/FileOutputStream;
    :goto_39
    sget-boolean v6, Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample;->result:Z

    return v6

    .line 319
    .end local v0    # "ar":Lpxb/android/axml/AxmlReader;
    .end local v1    # "aw":Lpxb/android/axml/AxmlWriter;
    .end local v2    # "data":[B
    :catch_3c
    move-exception v4

    .line 320
    .local v4, "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_12

    .line 461
    .end local v4    # "e":Ljava/lang/Exception;
    .restart local v0    # "ar":Lpxb/android/axml/AxmlReader;
    .restart local v1    # "aw":Lpxb/android/axml/AxmlWriter;
    .restart local v2    # "data":[B
    :catch_41
    move-exception v4

    .line 463
    .local v4, "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_39
.end method

.method public changeTargetApi(Ljava/io/File;Ljava/lang/String;)Z
    .registers 11
    .param p1, "manifestFile"    # Ljava/io/File;
    .param p2, "targetApi"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 219
    const/4 v6, 0x0

    sput-boolean v6, Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample;->result:Z

    .line 220
    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v6

    long-to-int v6, v6

    new-array v3, v6, [B

    .line 222
    .local v3, "data1":[B
    :try_start_a
    new-instance v6, Ljava/io/FileInputStream;

    invoke-direct {v6, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {v6, v3}, Ljava/io/FileInputStream;->read([B)I
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_12} :catch_3c

    .line 227
    :goto_12
    new-instance v0, Lpxb/android/axml/AxmlReader;

    invoke-direct {v0, v3}, Lpxb/android/axml/AxmlReader;-><init>([B)V

    .line 228
    .local v0, "ar":Lpxb/android/axml/AxmlReader;
    new-instance v1, Lpxb/android/axml/AxmlWriter;

    invoke-direct {v1}, Lpxb/android/axml/AxmlWriter;-><init>()V

    .line 230
    .local v1, "aw":Lpxb/android/axml/AxmlWriter;
    new-instance v6, Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample$2;

    invoke-direct {v6, p0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample$2;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample;Lpxb/android/axml/NodeVisitor;)V

    invoke-virtual {v0, v6}, Lpxb/android/axml/AxmlReader;->accept(Lpxb/android/axml/AxmlVisitor;)V

    .line 296
    invoke-virtual {v1}, Lpxb/android/axml/AxmlWriter;->toByteArray()[B

    move-result-object v2

    .line 301
    .local v2, "data":[B
    :try_start_28
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    .line 302
    invoke-virtual {p1}, Ljava/io/File;->createNewFile()Z

    .line 305
    new-instance v5, Ljava/io/FileOutputStream;

    invoke-direct {v5, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 306
    .local v5, "fo":Ljava/io/FileOutputStream;
    invoke-virtual {v5, v2}, Ljava/io/FileOutputStream;->write([B)V

    .line 307
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_39
    .catch Ljava/io/IOException; {:try_start_28 .. :try_end_39} :catch_41

    .line 312
    .end local v5    # "fo":Ljava/io/FileOutputStream;
    :goto_39
    sget-boolean v6, Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample;->result:Z

    return v6

    .line 223
    .end local v0    # "ar":Lpxb/android/axml/AxmlReader;
    .end local v1    # "aw":Lpxb/android/axml/AxmlWriter;
    .end local v2    # "data":[B
    :catch_3c
    move-exception v4

    .line 224
    .local v4, "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_12

    .line 308
    .end local v4    # "e":Ljava/lang/Exception;
    .restart local v0    # "ar":Lpxb/android/axml/AxmlReader;
    .restart local v1    # "aw":Lpxb/android/axml/AxmlWriter;
    .restart local v2    # "data":[B
    :catch_41
    move-exception v4

    .line 310
    .local v4, "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_39
.end method

.method public disablePermisson(Ljava/io/File;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .registers 12
    .param p1, "manifestFile"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 68
    .local p2, "permiss":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .local p3, "activ":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iput-object p2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample;->permissions:Ljava/util/ArrayList;

    .line 69
    iput-object p3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample;->activities:Ljava/util/ArrayList;

    .line 70
    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v6

    long-to-int v6, v6

    new-array v3, v6, [B

    .line 72
    .local v3, "data1":[B
    :try_start_b
    new-instance v6, Ljava/io/FileInputStream;

    invoke-direct {v6, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {v6, v3}, Ljava/io/FileInputStream;->read([B)I
    :try_end_13
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_13} :catch_3b

    .line 77
    :goto_13
    new-instance v0, Lpxb/android/axml/AxmlReader;

    invoke-direct {v0, v3}, Lpxb/android/axml/AxmlReader;-><init>([B)V

    .line 78
    .local v0, "ar":Lpxb/android/axml/AxmlReader;
    new-instance v1, Lpxb/android/axml/AxmlWriter;

    invoke-direct {v1}, Lpxb/android/axml/AxmlWriter;-><init>()V

    .line 80
    .local v1, "aw":Lpxb/android/axml/AxmlWriter;
    new-instance v6, Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample$1;

    invoke-direct {v6, p0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample$1;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample;Lpxb/android/axml/NodeVisitor;)V

    invoke-virtual {v0, v6}, Lpxb/android/axml/AxmlReader;->accept(Lpxb/android/axml/AxmlVisitor;)V

    .line 199
    invoke-virtual {v1}, Lpxb/android/axml/AxmlWriter;->toByteArray()[B

    move-result-object v2

    .line 204
    .local v2, "data":[B
    :try_start_29
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    .line 205
    invoke-virtual {p1}, Ljava/io/File;->createNewFile()Z

    .line 208
    new-instance v5, Ljava/io/FileOutputStream;

    invoke-direct {v5, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 209
    .local v5, "fo":Ljava/io/FileOutputStream;
    invoke-virtual {v5, v2}, Ljava/io/FileOutputStream;->write([B)V

    .line 210
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_3a
    .catch Ljava/io/IOException; {:try_start_29 .. :try_end_3a} :catch_40

    .line 216
    .end local v5    # "fo":Ljava/io/FileOutputStream;
    :goto_3a
    return-void

    .line 73
    .end local v0    # "ar":Lpxb/android/axml/AxmlReader;
    .end local v1    # "aw":Lpxb/android/axml/AxmlWriter;
    .end local v2    # "data":[B
    :catch_3b
    move-exception v4

    .line 74
    .local v4, "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_13

    .line 211
    .end local v4    # "e":Ljava/lang/Exception;
    .restart local v0    # "ar":Lpxb/android/axml/AxmlReader;
    .restart local v1    # "aw":Lpxb/android/axml/AxmlWriter;
    .restart local v2    # "data":[B
    :catch_40
    move-exception v4

    .line 213
    .local v4, "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3a
.end method
