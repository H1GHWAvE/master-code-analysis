.class Lcom/android/vending/billing/InAppBillingService/LUCK/LuckyApp$1;
.super Ljava/lang/Object;
.source "LuckyApp.java"

# interfaces
.implements Ljava/lang/Thread$UncaughtExceptionHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/LuckyApp;->onCreate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/LuckyApp;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/LuckyApp;)V
    .registers 2
    .param p1, "this$0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/LuckyApp;

    .prologue
    .line 23
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LuckyApp$1;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/LuckyApp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V
    .registers 15
    .param p1, "arg0"    # Ljava/lang/Thread;
    .param p2, "arg1"    # Ljava/lang/Throwable;

    .prologue
    const/4 v11, 0x4

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 26
    sget-object v6, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "FATAL Exception LP "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p2}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 28
    :try_start_1f
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->init()V

    .line 29
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v6

    sget-object v7, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v7}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    invoke-virtual {v7}, Landroid/support/v4/app/FragmentActivity;->getPackageName()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v6

    iget-object v5, v6, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    .line 30
    .local v5, "ver":Ljava/lang/String;
    new-instance v3, Ljava/io/File;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/Log/Exception."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ".txt"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 31
    .local v3, "log":Ljava/io/File;
    new-instance v1, Ljava/io/StringWriter;

    invoke-direct {v1}, Ljava/io/StringWriter;-><init>()V

    .line 32
    .local v1, "errors":Ljava/io/StringWriter;
    new-instance v6, Ljava/io/PrintWriter;

    invoke-direct {v6, v1}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    invoke-virtual {p2, v6}, Ljava/lang/Throwable;->printStackTrace(Ljava/io/PrintWriter;)V

    .line 33
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Lucky Pacther ver. "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n\n "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/chelpus/Utils;->save_text_to_file(Ljava/io/File;Ljava/lang/String;)Z
    :try_end_8c
    .catch Ljava/lang/Exception; {:try_start_1f .. :try_end_8c} :catch_da

    .line 35
    .end local v1    # "errors":Ljava/io/StringWriter;
    .end local v3    # "log":Ljava/io/File;
    .end local v5    # "ver":Ljava/lang/String;
    :goto_8c
    invoke-virtual {p2}, Ljava/lang/Throwable;->printStackTrace()V

    .line 36
    const/4 v4, 0x0

    .line 37
    .local v4, "noStart":Z
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LuckyApp$1;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/LuckyApp;

    const-string v7, "config"

    invoke-virtual {v6, v7, v11}, Lcom/android/vending/billing/InAppBillingService/LUCK/LuckyApp;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v6

    const-string v7, "force_close"

    invoke-interface {v6, v7, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    if-eqz v6, :cond_a1

    .line 38
    const/4 v4, 0x1

    .line 39
    :cond_a1
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LuckyApp$1;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/LuckyApp;

    const-string v7, "config"

    invoke-virtual {v6, v7, v11}, Lcom/android/vending/billing/InAppBillingService/LUCK/LuckyApp;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v6

    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    const-string v7, "force_close"

    invoke-interface {v6, v7, v10}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 42
    :try_start_b6
    new-instance v3, Lcom/android/vending/billing/InAppBillingService/LUCK/LogCollector;

    invoke-direct {v3}, Lcom/android/vending/billing/InAppBillingService/LUCK/LogCollector;-><init>()V

    .line 43
    .local v3, "log":Lcom/android/vending/billing/InAppBillingService/LUCK/LogCollector;
    sget-object v6, Lcom/android/vending/billing/InAppBillingService/LUCK/LuckyApp;->instance:Landroid/content/Context;

    const/4 v7, 0x1

    invoke-virtual {v3, v6, v7}, Lcom/android/vending/billing/InAppBillingService/LUCK/LogCollector;->collect(Landroid/content/Context;Z)Z

    .line 45
    if-nez v4, :cond_d6

    .line 46
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v6

    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LuckyApp$1;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/LuckyApp;

    invoke-virtual {v7}, Lcom/android/vending/billing/InAppBillingService/LUCK/LuckyApp;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 47
    .local v2, "i":Landroid/content/Intent;
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LuckyApp$1;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/LuckyApp;

    invoke-virtual {v6, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/LuckyApp;->startActivity(Landroid/content/Intent;)V
    :try_end_d6
    .catch Ljava/lang/RuntimeException; {:try_start_b6 .. :try_end_d6} :catch_df
    .catch Ljava/lang/Exception; {:try_start_b6 .. :try_end_d6} :catch_e4

    .line 51
    .end local v2    # "i":Landroid/content/Intent;
    .end local v3    # "log":Lcom/android/vending/billing/InAppBillingService/LUCK/LogCollector;
    :cond_d6
    :goto_d6
    invoke-static {v9}, Ljava/lang/System;->exit(I)V

    .line 52
    return-void

    .line 34
    .end local v4    # "noStart":Z
    :catch_da
    move-exception v0

    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_8c

    .line 50
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v4    # "noStart":Z
    :catch_df
    move-exception v0

    .local v0, "e":Ljava/lang/RuntimeException;
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->printStackTrace()V

    goto :goto_d6

    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catch_e4
    move-exception v0

    .local v0, "e":Ljava/lang/Exception;
    invoke-static {v9}, Ljava/lang/System;->exit(I)V

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_d6
.end method
