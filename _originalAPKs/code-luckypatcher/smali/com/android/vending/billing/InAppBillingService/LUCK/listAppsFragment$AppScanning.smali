.class public Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning;
.super Ljava/lang/Object;
.source "listAppsFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "AppScanning"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;


# direct methods
.method public constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;)V
    .registers 2
    .param p1, "this$0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    .prologue
    .line 10772
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 12

    .prologue
    const/4 v7, 0x0

    .line 10780
    iget-object v8, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v9, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning$1;

    invoke-direct {v9, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning$1;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning;)V

    invoke-virtual {v8, v9}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V

    .line 10792
    const/4 v8, 0x0

    :try_start_c
    sput-object v8, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->data:Ljava/util/ArrayList;

    .line 10793
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    sput-object v8, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->data:Ljava/util/ArrayList;

    .line 10794
    sget-object v8, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->database:Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;

    if-nez v8, :cond_24

    .line 10795
    new-instance v8, Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v9

    invoke-direct {v8, v9}, Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;-><init>(Landroid/content/Context;)V

    sput-object v8, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->database:Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;
    :try_end_24
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_24} :catch_e8

    .line 10798
    :cond_24
    :try_start_24
    sget-boolean v8, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->sqlScanLite:Z

    if-eqz v8, :cond_32

    .line 10800
    iget-object v8, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v9, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning$2;

    invoke-direct {v9, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning$2;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning;)V

    invoke-virtual {v8, v9}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V
    :try_end_32
    .catch Ljava/lang/Exception; {:try_start_24 .. :try_end_32} :catch_144

    .line 10813
    :cond_32
    :goto_32
    const/4 v5, 0x0

    .line 10814
    .local v5, "skipIcon":Z
    :try_start_33
    sget-boolean v8, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->fast_start:Z

    if-eqz v8, :cond_38

    const/4 v5, 0x1

    .line 10816
    :cond_38
    if-nez v5, :cond_45

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v8

    const-string v9, "no_icon"

    const/4 v10, 0x0

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    .line 10817
    :cond_45
    sget v8, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->advancedFilter:I

    if-nez v8, :cond_dd

    .line 10818
    sget-object v8, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->database:Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;

    const/4 v9, 0x0

    invoke-virtual {v8, v9, v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;->getPackage(ZZ)Ljava/util/ArrayList;

    move-result-object v8

    sput-object v8, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->data:Ljava/util/ArrayList;

    .line 10822
    :goto_52
    sget-object v8, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->data:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-nez v8, :cond_119

    sget v8, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->advancedFilter:I

    if-nez v8, :cond_119

    .line 10823
    sget-object v8, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->data:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->clear()V

    .line 10824
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPackages()[Ljava/lang/String;

    move-result-object v3

    .line 10825
    .local v3, "packages":[Ljava/lang/String;
    iget-object v8, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    array-length v9, v3

    iput v9, v8, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->maxcount:I
    :try_end_6c
    .catch Ljava/lang/Exception; {:try_start_33 .. :try_end_6c} :catch_e8

    .line 10830
    :try_start_6c
    iget-object v8, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v9, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning$3;

    invoke-direct {v9, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning$3;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning;)V

    invoke-virtual {v8, v9}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V
    :try_end_76
    .catch Ljava/lang/Exception; {:try_start_6c .. :try_end_76} :catch_141

    .line 10851
    :goto_76
    :try_start_76
    iget-object v8, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    const/4 v9, 0x1

    iput v9, v8, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->count:I

    .line 10852
    const/4 v6, 0x0

    .line 10853
    .local v6, "sysshow":Z
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v8

    const-string v9, "systemapp"

    const/4 v10, 0x0

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z
    :try_end_86
    .catch Ljava/lang/Exception; {:try_start_76 .. :try_end_86} :catch_e8

    move-result v6

    .line 10856
    :try_start_87
    iget-object v8, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v9, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning$4;

    invoke-direct {v9, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning$4;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning;)V

    invoke-virtual {v8, v9}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V
    :try_end_91
    .catch Ljava/lang/Exception; {:try_start_87 .. :try_end_91} :catch_13e

    .line 10868
    :goto_91
    const/4 v1, 0x1

    .line 10869
    .local v1, "iconTrigger":Z
    :try_start_92
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v8

    const-string v9, "no_icon"

    const/4 v10, 0x0

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    if-eqz v8, :cond_a0

    const/4 v1, 0x0

    .line 10870
    :cond_a0
    array-length v8, v3

    :goto_a1
    if-ge v7, v8, :cond_10f

    aget-object v4, v3, v7
    :try_end_a5
    .catch Ljava/lang/Exception; {:try_start_92 .. :try_end_a5} :catch_e8

    .line 10873
    .local v4, "pkgname":Ljava/lang/String;
    :try_start_a5
    iget-object v9, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iget v10, v9, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->count:I

    add-int/lit8 v10, v10, 0x1

    iput v10, v9, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->count:I

    .line 10875
    new-instance v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v9

    sget v10, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->days:I

    invoke-direct {v2, v9, v4, v10, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;-><init>(Landroid/content/Context;Ljava/lang/String;IZ)V

    .line 10876
    .local v2, "local":Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    invoke-virtual {v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->saveItem()V

    .line 10880
    iget-boolean v9, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_ads:Z

    if-nez v9, :cond_cb

    iget-boolean v9, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_custom:Z

    if-nez v9, :cond_cb

    iget-boolean v9, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_lvl:Z

    if-nez v9, :cond_cb

    iget-boolean v9, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_manual:Z

    if-eqz v9, :cond_d0

    .line 10881
    :cond_cb
    sget-object v9, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->boot_pat:Ljava/util/ArrayList;

    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_d0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_a5 .. :try_end_d0} :catch_13c
    .catch Ljava/lang/Exception; {:try_start_a5 .. :try_end_d0} :catch_e8

    .line 10891
    .end local v2    # "local":Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    :cond_d0
    :goto_d0
    :try_start_d0
    iget-object v9, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v10, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning$5;

    invoke-direct {v10, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning$5;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning;)V

    invoke-virtual {v9, v10}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V

    .line 10870
    add-int/lit8 v7, v7, 0x1

    goto :goto_a1

    .line 10820
    .end local v1    # "iconTrigger":Z
    .end local v3    # "packages":[Ljava/lang/String;
    .end local v4    # "pkgname":Ljava/lang/String;
    .end local v6    # "sysshow":Z
    :cond_dd
    sget-object v8, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->database:Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;

    const/4 v9, 0x1

    invoke-virtual {v8, v9, v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;->getPackage(ZZ)Ljava/util/ArrayList;

    move-result-object v8

    sput-object v8, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->data:Ljava/util/ArrayList;
    :try_end_e6
    .catch Ljava/lang/Exception; {:try_start_d0 .. :try_end_e6} :catch_e8

    goto/16 :goto_52

    .line 10917
    .end local v5    # "skipIcon":Z
    :catch_e8
    move-exception v0

    .line 10918
    .local v0, "e":Ljava/lang/Exception;
    sget-object v7, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "LuckyPatcher (AppScanner): "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 10919
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 10921
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_104
    :goto_104
    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v8, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning$7;

    invoke-direct {v8, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning$7;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning;)V

    invoke-virtual {v7, v8}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V

    .line 10943
    return-void

    .line 10901
    .restart local v1    # "iconTrigger":Z
    .restart local v3    # "packages":[Ljava/lang/String;
    .restart local v5    # "skipIcon":Z
    .restart local v6    # "sysshow":Z
    :cond_10f
    :try_start_10f
    sget-object v7, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->database:Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;->getPackage(ZZ)Ljava/util/ArrayList;

    move-result-object v7

    sput-object v7, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->data:Ljava/util/ArrayList;

    .line 10903
    .end local v1    # "iconTrigger":Z
    .end local v3    # "packages":[Ljava/lang/String;
    .end local v6    # "sysshow":Z
    :cond_119
    sget-object v7, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->data:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-nez v7, :cond_104

    sget v7, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->advancedFilter:I

    if-eqz v7, :cond_104

    .line 10904
    const/4 v7, 0x0

    sput v7, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->advancedFilter:I

    .line 10905
    sget-boolean v7, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->adapterSelect:Z

    if-eqz v7, :cond_131

    .line 10906
    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v7}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->resetBatchOperation()V

    .line 10908
    :cond_131
    sget-object v7, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->handler:Landroid/os/Handler;

    new-instance v8, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning$6;

    invoke-direct {v8, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning$6;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$AppScanning;)V

    invoke-virtual {v7, v8}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_13b
    .catch Ljava/lang/Exception; {:try_start_10f .. :try_end_13b} :catch_e8

    goto :goto_104

    .line 10886
    .restart local v1    # "iconTrigger":Z
    .restart local v3    # "packages":[Ljava/lang/String;
    .restart local v4    # "pkgname":Ljava/lang/String;
    .restart local v6    # "sysshow":Z
    :catch_13c
    move-exception v9

    goto :goto_d0

    .line 10866
    .end local v1    # "iconTrigger":Z
    .end local v4    # "pkgname":Ljava/lang/String;
    :catch_13e
    move-exception v8

    goto/16 :goto_91

    .line 10847
    .end local v6    # "sysshow":Z
    :catch_141
    move-exception v8

    goto/16 :goto_76

    .line 10811
    .end local v3    # "packages":[Ljava/lang/String;
    .end local v5    # "skipIcon":Z
    :catch_144
    move-exception v8

    goto/16 :goto_32
.end method
