.class Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$UpdateVersion;
.super Landroid/os/AsyncTask;
.source "listAppsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "UpdateVersion"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;)V
    .registers 2
    .param p1, "this$0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    .prologue
    .line 10947
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$UpdateVersion;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Boolean;
    .registers 15
    .param p1, "params"    # [Ljava/lang/String;

    .prologue
    const/4 v12, 0x0

    const/4 v11, 0x1

    .line 10961
    iget-object v9, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$UpdateVersion;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    const-string v10, "http://chelpus.defcon5.biz/Version.txt"

    iput-object v10, v9, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->BuildVersionPath:Ljava/lang/String;

    .line 10962
    iget-object v9, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$UpdateVersion;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iput v12, v9, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->VersionCode:I

    .line 10964
    const/4 v6, 0x1

    .line 10965
    .local v6, "mirror":Z
    :goto_d
    if-eqz v6, :cond_7f

    .line 10967
    :try_start_f
    new-instance v8, Ljava/net/URL;

    iget-object v9, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$UpdateVersion;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iget-object v9, v9, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->BuildVersionPath:Ljava/lang/String;

    invoke-direct {v8, v9}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 10969
    .local v8, "u":Ljava/net/URL;
    invoke-virtual {v8}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v2

    check-cast v2, Ljava/net/HttpURLConnection;

    .line 10970
    .local v2, "c":Ljava/net/HttpURLConnection;
    const-string v9, "GET"

    invoke-virtual {v2, v9}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 10971
    const v9, 0xf4240

    invoke-virtual {v2, v9}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 10972
    const/4 v9, 0x0

    invoke-virtual {v2, v9}, Ljava/net/HttpURLConnection;->setUseCaches(Z)V

    .line 10973
    const-string v9, "Cache-Control"

    const-string v10, "no-cache"

    invoke-virtual {v2, v9, v10}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 10974
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->connect()V

    .line 10979
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v4

    .line 10981
    .local v4, "in":Ljava/io/InputStream;
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 10983
    .local v0, "baos":Ljava/io/ByteArrayOutputStream;
    const/16 v9, 0x2000

    new-array v1, v9, [B

    .line 10987
    .local v1, "buffer":[B
    const/4 v5, 0x0

    .line 10988
    .local v5, "len1":I
    :goto_45
    invoke-virtual {v4, v1}, Ljava/io/InputStream;->read([B)I

    move-result v5

    const/4 v9, -0x1

    if-eq v5, v9, :cond_5a

    .line 10989
    const/4 v9, 0x0

    invoke-virtual {v0, v1, v9, v5}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_50
    .catch Ljava/net/MalformedURLException; {:try_start_f .. :try_end_50} :catch_51
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_50} :catch_6b
    .catch Ljava/lang/NumberFormatException; {:try_start_f .. :try_end_50} :catch_74
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_50} :catch_7a

    goto :goto_45

    .line 10997
    .end local v0    # "baos":Ljava/io/ByteArrayOutputStream;
    .end local v1    # "buffer":[B
    .end local v2    # "c":Ljava/net/HttpURLConnection;
    .end local v4    # "in":Ljava/io/InputStream;
    .end local v5    # "len1":I
    .end local v8    # "u":Ljava/net/URL;
    :catch_51
    move-exception v3

    .line 10999
    .local v3, "e":Ljava/net/MalformedURLException;
    invoke-virtual {v3}, Ljava/net/MalformedURLException;->printStackTrace()V

    .line 11000
    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    .line 11031
    .end local v3    # "e":Ljava/net/MalformedURLException;
    :goto_59
    return-object v9

    .line 10991
    .restart local v0    # "baos":Ljava/io/ByteArrayOutputStream;
    .restart local v1    # "buffer":[B
    .restart local v2    # "c":Ljava/net/HttpURLConnection;
    .restart local v4    # "in":Ljava/io/InputStream;
    .restart local v5    # "len1":I
    .restart local v8    # "u":Ljava/net/URL;
    :cond_5a
    :try_start_5a
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;

    move-result-object v7

    .line 10993
    .local v7, "s2":Ljava/lang/String;
    iget-object v9, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$UpdateVersion;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    iput v10, v9, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->VersionCode:I

    .line 10996
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_69
    .catch Ljava/net/MalformedURLException; {:try_start_5a .. :try_end_69} :catch_51
    .catch Ljava/io/IOException; {:try_start_5a .. :try_end_69} :catch_6b
    .catch Ljava/lang/NumberFormatException; {:try_start_5a .. :try_end_69} :catch_74
    .catch Ljava/lang/Exception; {:try_start_5a .. :try_end_69} :catch_7a

    .line 11020
    .end local v0    # "baos":Ljava/io/ByteArrayOutputStream;
    .end local v1    # "buffer":[B
    .end local v2    # "c":Ljava/net/HttpURLConnection;
    .end local v4    # "in":Ljava/io/InputStream;
    .end local v5    # "len1":I
    .end local v7    # "s2":Ljava/lang/String;
    .end local v8    # "u":Ljava/net/URL;
    :goto_69
    const/4 v6, 0x0

    goto :goto_d

    .line 11001
    :catch_6b
    move-exception v3

    .line 11002
    .local v3, "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    .line 11003
    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    goto :goto_59

    .line 11005
    .end local v3    # "e":Ljava/io/IOException;
    :catch_74
    move-exception v3

    .line 11006
    .local v3, "e":Ljava/lang/NumberFormatException;
    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    goto :goto_59

    .line 11007
    .end local v3    # "e":Ljava/lang/NumberFormatException;
    :catch_7a
    move-exception v3

    .line 11008
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_69

    .line 11031
    .end local v3    # "e":Ljava/lang/Exception;
    :cond_7f
    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    goto :goto_59
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 10947
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$UpdateVersion;->doInBackground([Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .registers 7
    .param p1, "result"    # Ljava/lang/Boolean;

    .prologue
    const/16 v4, 0x3e7

    .line 11042
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 11043
    const/16 v1, 0xb

    invoke-static {v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->removeDialogLP(I)V

    .line 11047
    :try_start_a
    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$UpdateVersion;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iget v1, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->VersionCode:I

    sget v2, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->versionCodeLocal:I

    if-le v1, v2, :cond_25

    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$UpdateVersion;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iget v1, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->VersionCode:I

    if-ne v1, v4, :cond_38

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "999"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_38

    .line 11049
    :cond_25
    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$UpdateVersion;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    const v2, 0x7f07021b

    invoke-static {v2}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0700f4

    invoke-static {v3}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->showMessage(Ljava/lang/String;Ljava/lang/String;)V

    .line 11053
    :cond_38
    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$UpdateVersion;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iget v1, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->VersionCode:I

    sget v2, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->versionCodeLocal:I

    if-le v1, v2, :cond_53

    .line 11055
    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$UpdateVersion;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iget v1, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->VersionCode:I

    if-ne v1, v4, :cond_54

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "999"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_54

    .line 11069
    :cond_53
    :goto_53
    return-void

    .line 11057
    :cond_54
    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$UpdateVersion;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    # invokes: Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->updateapp()V
    invoke-static {v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->access$1700(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;)V

    .line 11058
    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$UpdateVersion;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iget v1, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->FalseHttp:I

    const/4 v2, 0x2

    if-lt v1, v2, :cond_53

    .line 11059
    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$UpdateVersion;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f070180

    invoke-static {v2}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V
    :try_end_75
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_a .. :try_end_75} :catch_76

    goto :goto_53

    .line 11063
    :catch_76
    move-exception v0

    .line 11065
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_53
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 2

    .prologue
    .line 10947
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$UpdateVersion;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected onPreExecute()V
    .registers 2

    .prologue
    .line 10952
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 10953
    const/16 v0, 0xb

    invoke-static {v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->showDialogLP(I)V

    .line 10954
    return-void
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .registers 2

    .prologue
    .line 10947
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$UpdateVersion;->onProgressUpdate([Ljava/lang/Void;)V

    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Void;)V
    .registers 2
    .param p1, "values"    # [Ljava/lang/Void;

    .prologue
    .line 11036
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    .line 11038
    return-void
.end method
