.class Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$35$1;
.super Ljava/lang/Object;
.source "All_Dialogs.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$35;->onClick(Landroid/content/DialogInterface;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$35;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$35;)V
    .registers 2
    .param p1, "this$1"    # Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$35;

    .prologue
    .line 1242
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$35$1;->this$1:Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$35;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 16

    .prologue
    const/4 v14, 0x1

    .line 1246
    sget-object v9, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patch_adapt:Landroid/widget/ArrayAdapter;

    invoke-virtual {v9}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v0

    .line 1247
    .local v0, "count":I
    new-instance v8, Lorg/json/JSONObject;

    invoke-direct {v8}, Lorg/json/JSONObject;-><init>()V

    .line 1248
    .local v8, "settings":Lorg/json/JSONObject;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_d
    if-ge v2, v0, :cond_10f

    .line 1249
    sget-object v9, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patch_adapt:Landroid/widget/ArrayAdapter;

    invoke-virtual {v9, v2}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;

    .line 1250
    .local v6, "patt":Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, ""

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-boolean v11, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;->Status:Z

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1251
    if-nez v2, :cond_3a

    .line 1253
    :try_start_33
    const-string v9, "patch1"

    iget-boolean v10, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;->Status:Z

    invoke-virtual {v8, v9, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;
    :try_end_3a
    .catch Lorg/json/JSONException; {:try_start_33 .. :try_end_3a} :catch_8e

    .line 1258
    :cond_3a
    :goto_3a
    if-ne v2, v14, :cond_43

    .line 1260
    :try_start_3c
    const-string v9, "patch2"

    iget-boolean v10, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;->Status:Z

    invoke-virtual {v8, v9, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;
    :try_end_43
    .catch Lorg/json/JSONException; {:try_start_3c .. :try_end_43} :catch_93

    .line 1265
    :cond_43
    :goto_43
    const/4 v9, 0x2

    if-ne v2, v9, :cond_4d

    .line 1267
    :try_start_46
    const-string v9, "patch3"

    iget-boolean v10, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;->Status:Z

    invoke-virtual {v8, v9, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;
    :try_end_4d
    .catch Lorg/json/JSONException; {:try_start_46 .. :try_end_4d} :catch_98

    .line 1272
    :cond_4d
    :goto_4d
    const/4 v9, 0x3

    if-ne v2, v9, :cond_81

    .line 1274
    :try_start_50
    const-string v9, "com.android.vending"

    const/4 v10, 0x0

    invoke-static {v9, v10}, Lcom/chelpus/Utils;->getPkgInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v7

    .line 1275
    .local v7, "pi":Landroid/content/pm/PackageInfo;
    if-eqz v7, :cond_7a

    .line 1276
    new-instance v5, Ljava/io/File;

    iget-object v9, v7, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v9, v9, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    const/4 v10, 0x0

    invoke-static {v9, v10}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v5, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_67
    .catch Lorg/json/JSONException; {:try_start_50 .. :try_end_67} :catch_103

    .line 1277
    .local v5, "odex":Ljava/io/File;
    const-wide/16 v3, 0x0

    .line 1279
    .local v3, "lenght":J
    :try_start_69
    invoke-virtual {v5}, Ljava/io/File;->length()J
    :try_end_6c
    .catch Ljava/lang/Exception; {:try_start_69 .. :try_end_6c} :catch_12f
    .catch Lorg/json/JSONException; {:try_start_69 .. :try_end_6c} :catch_103

    move-result-wide v3

    .line 1282
    :goto_6d
    const-wide/32 v9, 0x100000

    cmp-long v9, v3, v9

    if-gtz v9, :cond_7a

    const-wide/16 v9, 0x0

    cmp-long v9, v3, v9

    if-nez v9, :cond_9d

    .line 1295
    .end local v3    # "lenght":J
    .end local v5    # "odex":Ljava/io/File;
    :cond_7a
    :goto_7a
    :try_start_7a
    const-string v9, "patch4"

    iget-boolean v10, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;->Status:Z

    invoke-virtual {v8, v9, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;
    :try_end_81
    .catch Lorg/json/JSONException; {:try_start_7a .. :try_end_81} :catch_103

    .line 1300
    .end local v7    # "pi":Landroid/content/pm/PackageInfo;
    :cond_81
    :goto_81
    const/4 v9, 0x4

    if-ne v2, v9, :cond_8b

    .line 1302
    :try_start_84
    const-string v9, "hide"

    iget-boolean v10, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;->Status:Z

    invoke-virtual {v8, v9, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;
    :try_end_8b
    .catch Lorg/json/JSONException; {:try_start_84 .. :try_end_8b} :catch_109

    .line 1248
    :cond_8b
    :goto_8b
    add-int/lit8 v2, v2, 0x1

    goto :goto_d

    .line 1254
    :catch_8e
    move-exception v1

    .line 1255
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_3a

    .line 1261
    .end local v1    # "e":Lorg/json/JSONException;
    :catch_93
    move-exception v1

    .line 1262
    .restart local v1    # "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_43

    .line 1268
    .end local v1    # "e":Lorg/json/JSONException;
    :catch_98
    move-exception v1

    .line 1269
    .restart local v1    # "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_4d

    .line 1286
    .end local v1    # "e":Lorg/json/JSONException;
    .restart local v3    # "lenght":J
    .restart local v5    # "odex":Ljava/io/File;
    .restart local v7    # "pi":Landroid/content/pm/PackageInfo;
    :cond_9d
    :try_start_9d
    new-instance v9, Lcom/chelpus/Utils;

    const-string v10, ""

    invoke-direct {v9, v10}, Lcom/chelpus/Utils;-><init>(Ljava/lang/String;)V

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/String;

    const/4 v11, 0x0

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v13, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->dalvikruncommand:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ".pinfo "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, v7, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v13, v13, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    sget-object v13, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->toolfilesdir:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, v7, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v13, v13, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-static {v13}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " recovery"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v9, v10}, Lcom/chelpus/Utils;->cmdRoot([Ljava/lang/String;)Ljava/lang/String;

    .line 1287
    const/4 v9, 0x1

    invoke-static {v9}, Lcom/chelpus/Utils;->market_billing_services(Z)V

    .line 1288
    const/4 v9, 0x1

    invoke-static {v9}, Lcom/chelpus/Utils;->market_licensing_services(Z)V

    .line 1289
    const-string v9, "com.android.vending"

    invoke-static {v9}, Lcom/chelpus/Utils;->kill(Ljava/lang/String;)V
    :try_end_fb
    .catch Ljava/lang/Exception; {:try_start_9d .. :try_end_fb} :catch_fd
    .catch Lorg/json/JSONException; {:try_start_9d .. :try_end_fb} :catch_103

    goto/16 :goto_7a

    .line 1290
    :catch_fd
    move-exception v1

    .line 1291
    .local v1, "e":Ljava/lang/Exception;
    :try_start_fe
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_101
    .catch Lorg/json/JSONException; {:try_start_fe .. :try_end_101} :catch_103

    goto/16 :goto_7a

    .line 1296
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v3    # "lenght":J
    .end local v5    # "odex":Ljava/io/File;
    .end local v7    # "pi":Landroid/content/pm/PackageInfo;
    :catch_103
    move-exception v1

    .line 1297
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto/16 :goto_81

    .line 1303
    .end local v1    # "e":Lorg/json/JSONException;
    :catch_109
    move-exception v1

    .line 1304
    .restart local v1    # "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto/16 :goto_8b

    .line 1310
    .end local v1    # "e":Lorg/json/JSONException;
    .end local v6    # "patt":Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;
    :cond_10f
    :try_start_10f
    const-string v9, "module_on"

    iget-object v10, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$35$1;->this$1:Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$35;

    iget-object v10, v10, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$35;->val$chk_x:Landroid/widget/CheckBox;

    invoke-virtual {v10}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v10

    invoke-virtual {v8, v9, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;
    :try_end_11c
    .catch Lorg/json/JSONException; {:try_start_10f .. :try_end_11c} :catch_12a

    .line 1314
    :goto_11c
    new-instance v9, Ljava/lang/Thread;

    new-instance v10, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$35$1$1;

    invoke-direct {v10, p0, v8}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$35$1$1;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$35$1;Lorg/json/JSONObject;)V

    invoke-direct {v9, v10}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 1322
    invoke-virtual {v9}, Ljava/lang/Thread;->start()V

    .line 1324
    return-void

    .line 1311
    :catch_12a
    move-exception v1

    .line 1312
    .restart local v1    # "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_11c

    .line 1280
    .end local v1    # "e":Lorg/json/JSONException;
    .restart local v3    # "lenght":J
    .restart local v5    # "odex":Ljava/io/File;
    .restart local v6    # "patt":Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;
    .restart local v7    # "pi":Landroid/content/pm/PackageInfo;
    :catch_12f
    move-exception v9

    goto/16 :goto_6d
.end method
