.class Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$121;
.super Landroid/widget/ArrayAdapter;
.source "listAppsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getComponents()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/android/vending/billing/InAppBillingService/LUCK/Components;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;Landroid/content/Context;ILjava/util/List;)V
    .registers 5
    .param p1, "this$0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;
    .param p2, "x0"    # Landroid/content/Context;
    .param p3, "x1"    # I

    .prologue
    .line 14818
    .local p4, "x2":Ljava/util/List;, "Ljava/util/List<Lcom/android/vending/billing/InAppBillingService/LUCK/Components;>;"
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$121;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 15
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v9, 0x0

    const v10, 0x7f0701b9

    .line 14824
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v7

    const-string v8, "layout_inflater"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 14825
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const v7, 0x7f04001d

    invoke-virtual {v2, v7, p3, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    .line 14826
    .local v6, "view":Landroid/view/View;
    const v7, 0x7f0d0039

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 14828
    .local v5, "textView":Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$121;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getSizeText()I

    move-result v8

    invoke-virtual {v5, v7, v8}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 14829
    const/high16 v7, 0x40a00000    # 5.0f

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v8

    iget v8, v8, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v7, v8

    const/high16 v8, 0x3f000000    # 0.5f

    add-float/2addr v7, v8

    float-to-int v0, v7

    .line 14830
    .local v0, "dp5":I
    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    .line 14831
    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$121;->getItem(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;

    iget-boolean v7, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;->header:Z

    if-eqz v7, :cond_63

    .line 14832
    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$121;->getItem(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;

    iget-object v4, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;->Name:Ljava/lang/String;

    .line 14834
    .local v4, "str2":Ljava/lang/String;
    const-string v7, "#ffffffff"

    const-string v8, "bold"

    invoke-static {v4, v7, v8}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 14835
    const v7, -0xbbbbbc

    invoke-virtual {v6, v7}, Landroid/view/View;->setBackgroundColor(I)V

    .line 14917
    .end local v4    # "str2":Ljava/lang/String;
    :cond_62
    :goto_62
    return-object v6

    .line 14839
    :cond_63
    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$121;->getItem(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;

    iget-boolean v7, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;->activity:Z

    if-eqz v7, :cond_d6

    .line 14840
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$121;->getItem(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;

    iget-object v7, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;->Name:Ljava/lang/String;

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 14842
    .restart local v4    # "str2":Ljava/lang/String;
    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$121;->getItem(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;

    iget-boolean v7, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;->Status:Z

    if-eqz v7, :cond_1fd

    .line 14843
    const-string v7, "#ff00ffff"

    const-string v8, "bold"

    invoke-static {v4, v7, v8}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 14844
    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$121;->getItem(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;

    iget-object v7, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;->Name:Ljava/lang/String;

    invoke-static {v7}, Lcom/chelpus/Utils;->isAds(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_b6

    .line 14845
    const-string v7, "#ffffff00"

    const-string v8, "bold"

    invoke-static {v4, v7, v8}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 14851
    :cond_b6
    :goto_b6
    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$121;->getItem(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;

    iget-object v7, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;->Name:Ljava/lang/String;

    invoke-static {v7}, Lcom/chelpus/Utils;->isAds(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_20a

    .line 14852
    const v7, 0x7f070008

    invoke-static {v7}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v4

    .line 14855
    :goto_cb
    const-string v7, "#ff888888"

    const-string v8, "italic"

    invoke-static {v4, v7, v8}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 14857
    .end local v4    # "str2":Ljava/lang/String;
    :cond_d6
    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$121;->getItem(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;

    iget-boolean v7, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;->permission:Z

    if-eqz v7, :cond_14d

    .line 14858
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$121;->getItem(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;

    iget-object v7, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;->Name:Ljava/lang/String;

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 14860
    .restart local v4    # "str2":Ljava/lang/String;
    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$121;->getItem(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;

    iget-boolean v7, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;->Status:Z

    if-eqz v7, :cond_213

    .line 14861
    const-string v7, "#ff00ff00"

    const-string v8, "bold"

    invoke-static {v4, v7, v8}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 14867
    :goto_110
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 14869
    .local v3, "pm":Landroid/content/pm/PackageManager;
    :try_start_114
    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$121;->getItem(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;

    iget-object v7, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;->Name:Ljava/lang/String;

    const-string v8, "chelpa_per_"

    const-string v9, ""

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "chelpus_"

    const-string v9, ""

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v3, v7, v8}, Landroid/content/pm/PackageManager;->getPermissionInfo(Ljava/lang/String;I)Landroid/content/pm/PermissionInfo;

    move-result-object v7

    invoke-virtual {v7, v3}, Landroid/content/pm/PermissionInfo;->loadDescription(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-interface {v7}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    .line 14871
    if-nez v4, :cond_220

    .line 14872
    const v7, 0x7f0701b9

    invoke-static {v7}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v4

    .line 14873
    const-string v7, "#ff888888"

    const-string v8, "italic"

    invoke-static {v4, v7, v8}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V
    :try_end_14d
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_114 .. :try_end_14d} :catch_22d
    .catch Ljava/lang/NullPointerException; {:try_start_114 .. :try_end_14d} :catch_23f

    .line 14887
    .end local v3    # "pm":Landroid/content/pm/PackageManager;
    .end local v4    # "str2":Ljava/lang/String;
    :cond_14d
    :goto_14d
    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$121;->getItem(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;

    iget-boolean v7, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;->service:Z

    if-eqz v7, :cond_187

    .line 14888
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$121;->getItem(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;

    iget-object v7, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;->Name:Ljava/lang/String;

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 14890
    .restart local v4    # "str2":Ljava/lang/String;
    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$121;->getItem(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;

    iget-boolean v7, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;->Status:Z

    if-eqz v7, :cond_251

    .line 14891
    const-string v7, "#ff00ff00"

    const-string v8, "bold"

    invoke-static {v4, v7, v8}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 14897
    .end local v4    # "str2":Ljava/lang/String;
    :cond_187
    :goto_187
    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$121;->getItem(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;

    iget-boolean v7, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;->reciver:Z

    if-eqz v7, :cond_1c1

    .line 14898
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$121;->getItem(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;

    iget-object v7, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;->Name:Ljava/lang/String;

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 14900
    .restart local v4    # "str2":Ljava/lang/String;
    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$121;->getItem(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;

    iget-boolean v7, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;->Status:Z

    if-eqz v7, :cond_25e

    .line 14901
    const-string v7, "#ff00ffff"

    const-string v8, "bold"

    invoke-static {v4, v7, v8}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 14906
    .end local v4    # "str2":Ljava/lang/String;
    :cond_1c1
    :goto_1c1
    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$121;->getItem(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;

    iget-boolean v7, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;->provider:Z

    if-eqz v7, :cond_62

    .line 14907
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$121;->getItem(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;

    iget-object v7, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;->Name:Ljava/lang/String;

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 14909
    .restart local v4    # "str2":Ljava/lang/String;
    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$121;->getItem(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;

    iget-boolean v7, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/Components;->Status:Z

    if-eqz v7, :cond_26b

    .line 14910
    const-string v7, "#ff00ff00"

    const-string v8, "bold"

    invoke-static {v4, v7, v8}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_62

    .line 14848
    :cond_1fd
    const-string v7, "#ffff0000"

    const-string v8, "bold"

    invoke-static {v4, v7, v8}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_b6

    .line 14853
    :cond_20a
    const v7, 0x7f070009

    invoke-static {v7}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_cb

    .line 14863
    :cond_213
    const-string v7, "#ffff0000"

    const-string v8, "bold"

    invoke-static {v4, v7, v8}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_110

    .line 14875
    .restart local v3    # "pm":Landroid/content/pm/PackageManager;
    :cond_220
    :try_start_220
    const-string v7, "#ff888888"

    const-string v8, "italic"

    invoke-static {v4, v7, v8}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V
    :try_end_22b
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_220 .. :try_end_22b} :catch_22d
    .catch Ljava/lang/NullPointerException; {:try_start_220 .. :try_end_22b} :catch_23f

    goto/16 :goto_14d

    .line 14877
    :catch_22d
    move-exception v1

    .line 14879
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-static {v10}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v4

    .line 14880
    const-string v7, "#ff888888"

    const-string v8, "italic"

    invoke-static {v4, v7, v8}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    goto/16 :goto_14d

    .line 14881
    .end local v1    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_23f
    move-exception v1

    .line 14882
    .local v1, "e":Ljava/lang/NullPointerException;
    invoke-static {v10}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v4

    .line 14883
    const-string v7, "#ff888888"

    const-string v8, "italic"

    invoke-static {v4, v7, v8}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    goto/16 :goto_14d

    .line 14894
    .end local v1    # "e":Ljava/lang/NullPointerException;
    .end local v3    # "pm":Landroid/content/pm/PackageManager;
    :cond_251
    const-string v7, "#ffff0000"

    const-string v8, "bold"

    invoke-static {v4, v7, v8}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_187

    .line 14904
    :cond_25e
    const-string v7, "#ffff0000"

    const-string v8, "bold"

    invoke-static {v4, v7, v8}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1c1

    .line 14913
    :cond_26b
    const-string v7, "#ffff0000"

    const-string v8, "bold"

    invoke-static {v4, v7, v8}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_62
.end method
