.class Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter$2;
.super Landroid/widget/Filter;
.source "PkgListItemAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->getFilter()Landroid/widget/Filter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;)V
    .registers 2
    .param p1, "this$0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;

    .prologue
    .line 986
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter$2;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;

    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    return-void
.end method


# virtual methods
.method protected performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .registers 11
    .param p1, "constraint"    # Ljava/lang/CharSequence;

    .prologue
    .line 990
    new-instance v1, Landroid/widget/Filter$FilterResults;

    invoke-direct {v1}, Landroid/widget/Filter$FilterResults;-><init>()V

    .line 991
    .local v1, "oReturn":Landroid/widget/Filter$FilterResults;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 992
    .local v2, "results":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;>;"
    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter$2;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;

    iget-object v4, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->orig:[Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    if-nez v4, :cond_18

    .line 993
    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter$2;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;

    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter$2;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;

    iget-object v5, v5, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->data:[Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    iput-object v5, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->orig:[Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    .line 994
    :cond_18
    if-eqz p1, :cond_6a

    .line 995
    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter$2;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;

    iget-object v4, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->orig:[Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    if-eqz v4, :cond_5f

    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter$2;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;

    iget-object v4, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->orig:[Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    array-length v4, v4

    if-lez v4, :cond_5f

    .line 996
    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter$2;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;

    iget-object v5, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->orig:[Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    array-length v6, v5

    const/4 v4, 0x0

    :goto_2d
    if-ge v4, v6, :cond_5f

    aget-object v0, v5, v4

    .line 997
    .local v0, "g":Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    iget-object v7, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->name:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    .line 998
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_59

    iget-object v7, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    .line 999
    invoke-virtual {v7}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    .line 1000
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_5c

    .line 1001
    :cond_59
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 996
    :cond_5c
    add-int/lit8 v4, v4, 0x1

    goto :goto_2d

    .line 1004
    .end local v0    # "g":Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    :cond_5f
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v3, v4, [Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    .line 1005
    .local v3, "tmp":[Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 1006
    iput-object v3, v1, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 1008
    .end local v3    # "tmp":[Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    :cond_6a
    return-object v1
.end method

.method protected publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .registers 5
    .param p1, "constraint"    # Ljava/lang/CharSequence;
    .param p2, "results"    # Landroid/widget/Filter$FilterResults;

    .prologue
    .line 1014
    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter$2;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;

    iget-object v0, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    check-cast v0, [Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    check-cast v0, [Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    iput-object v0, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->data:[Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    .line 1015
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter$2;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;

    invoke-virtual {v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->notifyDataSetChanged()V

    .line 1016
    return-void
.end method
