.class public Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;
.super Landroid/app/Activity;
.source "LivepatchActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$Item;
    }
.end annotation


# static fields
.field private static final DIALOG_LOAD_FILE:I = 0x3

.field private static final SETTINGS_VIEWSIZE:Ljava/lang/String; = "viewsize"

.field private static final SETTINGS_VIEWSIZE_SMALL:I = 0x0

.field private static final TAG:Ljava/lang/String; = "F_PATH"

.field public static context:Landroid/content/Context;

.field static filter:Landroid/text/InputFilter;

.field static filter2:Landroid/text/InputFilter;

.field private static orhex:Landroid/widget/EditText;

.field public static patch:Ljava/lang/String;

.field private static rephex:Landroid/widget/EditText;

.field public static selabpath:Ljava/lang/String;

.field public static selpath:Ljava/lang/String;

.field public static str:Ljava/lang/String;

.field private static tv:Landroid/widget/TextView;


# instance fields
.field adapter:Landroid/widget/ListAdapter;

.field private chosenFile:Ljava/lang/String;

.field private fileList:[Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$Item;

.field private firstLvl:Ljava/lang/Boolean;

.field private path:Ljava/io/File;

.field public pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

.field start:I

.field stri:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 83
    const-string v0, ""

    sput-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->selpath:Ljava/lang/String;

    .line 84
    const-string v0, ""

    sput-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->selabpath:Ljava/lang/String;

    .line 85
    const-string v0, ""

    sput-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->patch:Ljava/lang/String;

    .line 88
    new-instance v0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$1;

    invoke-direct {v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$1;-><init>()V

    sput-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->filter:Landroid/text/InputFilter;

    .line 134
    new-instance v0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$2;

    invoke-direct {v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$2;-><init>()V

    sput-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->filter2:Landroid/text/InputFilter;

    return-void
.end method

.method public constructor <init>()V
    .registers 4

    .prologue
    .line 56
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 62
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->stri:Ljava/util/ArrayList;

    .line 65
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->firstLvl:Ljava/lang/Boolean;

    .line 70
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->path:Ljava/io/File;

    .line 81
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->start:I

    .line 657
    return-void
.end method

.method static synthetic access$000()Landroid/widget/EditText;
    .registers 1

    .prologue
    .line 56
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->rephex:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;)Ljava/io/File;
    .registers 2
    .param p0, "x0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->path:Ljava/io/File;

    return-object v0
.end method

.method static synthetic access$102(Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;Ljava/io/File;)Ljava/io/File;
    .registers 2
    .param p0, "x0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;
    .param p1, "x1"    # Ljava/io/File;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->path:Ljava/io/File;

    return-object p1
.end method

.method static synthetic access$200(Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;)V
    .registers 1
    .param p0, "x0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->loadFileList()V

    return-void
.end method

.method static synthetic access$300(Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;)Ljava/lang/String;
    .registers 2
    .param p0, "x0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->chosenFile:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$302(Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .param p0, "x0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->chosenFile:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$400(Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;)[Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$Item;
    .registers 2
    .param p0, "x0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->fileList:[Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$Item;

    return-object v0
.end method

.method static synthetic access$402(Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;[Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$Item;)[Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$Item;
    .registers 2
    .param p0, "x0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;
    .param p1, "x1"    # [Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$Item;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->fileList:[Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$Item;

    return-object p1
.end method

.method static synthetic access$502(Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .registers 2
    .param p0, "x0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;
    .param p1, "x1"    # Ljava/lang/Boolean;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->firstLvl:Ljava/lang/Boolean;

    return-object p1
.end method

.method static synthetic access$600()Landroid/widget/TextView;
    .registers 1

    .prologue
    .line 56
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->tv:Landroid/widget/TextView;

    return-object v0
.end method

.method private loadFileList()V
    .registers 15

    .prologue
    const v13, 0x7f020029

    const/4 v12, 0x1

    const/4 v5, 0x0

    .line 558
    new-array v7, v12, [Ljava/lang/String;

    const-string v0, "empty"

    aput-object v0, v7, v5

    .line 560
    .local v7, "fList":[Ljava/lang/String;
    :try_start_b
    new-instance v0, Lcom/chelpus/Utils;

    const-string v1, ""

    invoke-direct {v0, v1}, Lcom/chelpus/Utils;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "cd "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->path:Ljava/io/File;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "ls"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/chelpus/Utils;->cmdRoot([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 561
    .local v10, "str6":Ljava/lang/String;
    const-string v0, "\n"

    invoke-virtual {v10, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;
    :try_end_3b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_3b} :catch_64

    move-result-object v7

    .line 566
    .end local v10    # "str6":Ljava/lang/String;
    :goto_3c
    array-length v0, v7

    new-array v0, v0, [Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$Item;

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->fileList:[Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$Item;

    .line 567
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_42
    array-length v0, v7

    if-ge v8, v0, :cond_ab

    .line 569
    aget-object v0, v7, v8

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_69

    array-length v0, v7

    if-ne v0, v12, :cond_69

    .line 570
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->fileList:[Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$Item;

    new-instance v1, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$Item;

    const-string v2, "Up"

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v1, p0, v2, v3}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$Item;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;Ljava/lang/String;Ljava/lang/Integer;)V

    aput-object v1, v0, v8

    .line 567
    :goto_61
    add-int/lit8 v8, v8, 0x1

    goto :goto_42

    .line 562
    .end local v8    # "i":I
    :catch_64
    move-exception v6

    .line 563
    .local v6, "e":Ljava/lang/Exception;
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3c

    .line 572
    .end local v6    # "e":Ljava/lang/Exception;
    .restart local v8    # "i":I
    :cond_69
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->fileList:[Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$Item;

    new-instance v1, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$Item;

    aget-object v2, v7, v8

    const v3, 0x7f02002b

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v1, p0, v2, v3}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$Item;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;Ljava/lang/String;Ljava/lang/Integer;)V

    aput-object v1, v0, v8

    .line 575
    new-instance v9, Ljava/io/File;

    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->path:Ljava/io/File;

    aget-object v1, v7, v8

    invoke-direct {v9, v0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 578
    .local v9, "sel":Ljava/io/File;
    invoke-virtual {v9}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_9f

    .line 579
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->fileList:[Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$Item;

    aget-object v0, v0, v8

    const v1, 0x7f02002a

    iput v1, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$Item;->icon:I

    .line 580
    const-string v0, "DIRECTORY"

    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->fileList:[Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$Item;

    aget-object v1, v1, v8

    iget-object v1, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$Item;->file:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_61

    .line 582
    :cond_9f
    const-string v0, "FILE"

    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->fileList:[Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$Item;

    aget-object v1, v1, v8

    iget-object v1, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$Item;->file:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_61

    .line 587
    .end local v9    # "sel":Ljava/io/File;
    :cond_ab
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->firstLvl:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_c6

    .line 588
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->fileList:[Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$Item;

    array-length v0, v0

    if-ne v0, v12, :cond_d8

    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->fileList:[Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$Item;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$Item;->file:Ljava/lang/String;

    const-string v1, "Up"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d8

    .line 598
    :cond_c6
    :goto_c6
    new-instance v0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$11;

    const v3, 0x1090011

    const v4, 0x1020014

    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->fileList:[Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$Item;

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$11;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;Landroid/content/Context;II[Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$Item;)V

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->adapter:Landroid/widget/ListAdapter;

    .line 655
    return-void

    .line 590
    :cond_d8
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->fileList:[Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$Item;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x1

    new-array v11, v0, [Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$Item;

    .line 591
    .local v11, "temp":[Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$Item;
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->fileList:[Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$Item;

    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->fileList:[Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$Item;

    array-length v1, v1

    invoke-static {v0, v5, v11, v12, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 592
    new-instance v0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$Item;

    const-string v1, "Up"

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$Item;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;Ljava/lang/String;Ljava/lang/Integer;)V

    aput-object v0, v11, v5

    .line 593
    iput-object v11, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->fileList:[Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$Item;

    goto :goto_c6
.end method


# virtual methods
.method public backup_click()V
    .registers 9

    .prologue
    .line 523
    sget-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->patch:Ljava/lang/String;

    const-string v4, "dalvik"

    if-ne v3, v4, :cond_54

    .line 525
    :try_start_6
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->getApplication()Landroid/app/Application;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "Backup processing... Please wait..."

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 526
    new-instance v3, Lcom/chelpus/Utils;

    const-string v4, ""

    invoke-direct {v3, v4}, Lcom/chelpus/Utils;-><init>(Ljava/lang/String;)V

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->dalvikruncommand:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ".live_backup "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    iget-object v7, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v3, v4}, Lcom/chelpus/Utils;->cmdRoot([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 527
    .local v2, "str3":Ljava/lang/String;
    move-object v1, v2

    .line 528
    .local v1, "str2":Ljava/lang/String;
    sget-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->tv:Landroid/widget/TextView;

    const-string v4, "#ff00ff73"

    const-string v5, "bold"

    invoke-static {v1, v4, v5}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_54
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_54} :catch_127

    .line 534
    .end local v1    # "str2":Ljava/lang/String;
    .end local v2    # "str3":Ljava/lang/String;
    :cond_54
    :goto_54
    sget-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->patch:Ljava/lang/String;

    const-string v4, "lib"

    if-ne v3, v4, :cond_126

    .line 536
    :try_start_5a
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    iget-object v4, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    iget-object v3, v3, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    const-string v4, "/mnt/asec/"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_91

    .line 537
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    iget-object v4, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    iget-object v3, v3, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    const-string v4, "/pkg.apk"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "rw"

    invoke-static {v3, v4}, Lcom/chelpus/Utils;->remount(Ljava/lang/String;Ljava/lang/String;)Z

    .line 540
    :cond_91
    new-instance v3, Lcom/chelpus/Utils;

    const-string v4, ""

    invoke-direct {v3, v4}, Lcom/chelpus/Utils;-><init>(Ljava/lang/String;)V

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->dalvikruncommand:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ".live_backuplib "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->selpath:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v3, v4}, Lcom/chelpus/Utils;->cmdRoot([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->str:Ljava/lang/String;

    .line 542
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    iget-object v4, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    iget-object v3, v3, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    const-string v4, "/mnt/asec/"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_f6

    .line 543
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    iget-object v4, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    iget-object v3, v3, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    const-string v4, "/pkg.apk"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "ro"

    invoke-static {v3, v4}, Lcom/chelpus/Utils;->remount(Ljava/lang/String;Ljava/lang/String;)Z

    .line 545
    :cond_f6
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->str:Ljava/lang/String;

    .line 546
    .restart local v1    # "str2":Ljava/lang/String;
    sget-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->str:Ljava/lang/String;

    const-string v4, "Error"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_10f

    .line 547
    sget-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->tv:Landroid/widget/TextView;

    const-string v4, "#ffff0055"

    const-string v5, "bold"

    invoke-static {v1, v4, v5}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 548
    :cond_10f
    sget-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->str:Ljava/lang/String;

    const-string v4, "Error"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_126

    .line 549
    sget-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->tv:Landroid/widget/TextView;

    const-string v4, "#ff00ff73"

    const-string v5, "bold"

    invoke-static {v1, v4, v5}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_126
    .catch Ljava/lang/Exception; {:try_start_5a .. :try_end_126} :catch_12d

    .line 555
    .end local v1    # "str2":Ljava/lang/String;
    :cond_126
    :goto_126
    return-void

    .line 530
    :catch_127
    move-exception v0

    .line 531
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_54

    .line 550
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_12d
    move-exception v0

    .line 551
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_126
.end method

.method public launch_click()V
    .registers 6

    .prologue
    .line 398
    :try_start_0
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 399
    .local v1, "i":Landroid/content/Intent;
    invoke-virtual {p0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_f
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_f} :catch_10
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_f} :catch_15

    .line 403
    .end local v1    # "i":Landroid/content/Intent;
    :goto_f
    return-void

    .line 400
    :catch_10
    move-exception v0

    .local v0, "e":Ljava/lang/RuntimeException;
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->printStackTrace()V

    goto :goto_f

    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catch_15
    move-exception v0

    .line 401
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0700fd

    invoke-static {v3}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_f
.end method

.method public livepatch_click()V
    .registers 16

    .prologue
    const/4 v11, 0x0

    .line 406
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v12, "run patch"

    invoke-virtual {v10, v12}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 407
    const/4 v9, 0x1

    .line 409
    .local v9, "verif":Z
    sget-object v10, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->orhex:Landroid/widget/EditText;

    invoke-virtual {v10}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 410
    .local v3, "orighex":Ljava/lang/String;
    sget-object v10, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->rephex:Landroid/widget/EditText;

    invoke-virtual {v10}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    .line 412
    .local v6, "replhex":Ljava/lang/String;
    const-string v4, "([0-9A-Fa-f*?]){2}||([0-9A-Fa-f*?]){1}"

    .line 414
    .local v4, "pattern":Ljava/lang/String;
    if-eqz v3, :cond_96

    if-eqz v6, :cond_96

    .line 415
    const-string v10, "\\s+"

    invoke-virtual {v3, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 417
    .local v0, "chk":[Ljava/lang/String;
    array-length v12, v0

    move v10, v11

    :goto_2b
    if-ge v10, v12, :cond_4f

    aget-object v8, v0, v10

    .line 418
    .local v8, "tail":Ljava/lang/String;
    invoke-static {v4, v8}, Ljava/util/regex/Pattern;->matches(Ljava/lang/String;Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_4c

    .line 420
    const/4 v9, 0x0

    .line 421
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->getApplication()Landroid/app/Application;

    move-result-object v13

    invoke-virtual {v13}, Landroid/app/Application;->getBaseContext()Landroid/content/Context;

    move-result-object v13

    const v14, 0x7f070102

    invoke-static {v14}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14, v11}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v13

    invoke-virtual {v13}, Landroid/widget/Toast;->show()V

    .line 417
    :cond_4c
    add-int/lit8 v10, v10, 0x1

    goto :goto_2b

    .line 424
    .end local v8    # "tail":Ljava/lang/String;
    :cond_4f
    const-string v10, "\\s+"

    invoke-virtual {v6, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 425
    .local v1, "chk2":[Ljava/lang/String;
    array-length v12, v1

    move v10, v11

    :goto_57
    if-ge v10, v12, :cond_7b

    aget-object v8, v1, v10

    .line 426
    .restart local v8    # "tail":Ljava/lang/String;
    invoke-static {v4, v8}, Ljava/util/regex/Pattern;->matches(Ljava/lang/String;Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_78

    .line 428
    const/4 v9, 0x0

    .line 429
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->getApplication()Landroid/app/Application;

    move-result-object v13

    invoke-virtual {v13}, Landroid/app/Application;->getBaseContext()Landroid/content/Context;

    move-result-object v13

    const v14, 0x7f070106

    invoke-static {v14}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14, v11}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v13

    invoke-virtual {v13}, Landroid/widget/Toast;->show()V

    .line 425
    :cond_78
    add-int/lit8 v10, v10, 0x1

    goto :goto_57

    .line 432
    .end local v8    # "tail":Ljava/lang/String;
    :cond_7b
    array-length v10, v0

    array-length v12, v1

    if-eq v10, v12, :cond_96

    .line 433
    const/4 v9, 0x0

    .line 434
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->getApplication()Landroid/app/Application;

    move-result-object v10

    invoke-virtual {v10}, Landroid/app/Application;->getBaseContext()Landroid/content/Context;

    move-result-object v10

    const v12, 0x7f0700fe

    invoke-static {v12}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v12, v11}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v10

    invoke-virtual {v10}, Landroid/widget/Toast;->show()V

    .line 437
    .end local v0    # "chk":[Ljava/lang/String;
    .end local v1    # "chk2":[Ljava/lang/String;
    :cond_96
    if-eqz v9, :cond_258

    .line 438
    sget-object v10, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->patch:Ljava/lang/String;

    const-string v11, "dalvik"

    if-ne v10, v11, :cond_13c

    .line 440
    :try_start_9e
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->getApplication()Landroid/app/Application;

    move-result-object v10

    invoke-virtual {v10}, Landroid/app/Application;->getBaseContext()Landroid/content/Context;

    move-result-object v10

    const-string v11, "Search bytes... Please wait..."

    const/4 v12, 0x0

    invoke-static {v10, v11, v12}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v10

    invoke-virtual {v10}, Landroid/widget/Toast;->show()V

    .line 442
    new-instance v10, Lcom/chelpus/Utils;

    const-string v11, ""

    invoke-direct {v10, v11}, Lcom/chelpus/Utils;-><init>(Ljava/lang/String;)V

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/String;

    const/4 v12, 0x0

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v14, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->dalvikruncommand:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ".liverunpatch "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-object v14, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    iget-object v14, v14, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "\""

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "\""

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "\""

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "\""

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-virtual {v10, v11}, Lcom/chelpus/Utils;->cmdRoot([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    sput-object v10, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->str:Ljava/lang/String;

    .line 443
    sget-object v7, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->str:Ljava/lang/String;

    .line 444
    .local v7, "str2":Ljava/lang/String;
    sget-object v10, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->str:Ljava/lang/String;

    const-string v11, "Error"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_125

    .line 445
    sget-object v10, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->tv:Landroid/widget/TextView;

    const-string v11, "#ffff0055"

    const-string v12, "bold"

    invoke-static {v7, v11, v12}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 446
    :cond_125
    sget-object v10, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->str:Ljava/lang/String;

    const-string v11, "Error"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_13c

    .line 447
    sget-object v10, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->tv:Landroid/widget/TextView;

    const-string v11, "#ff00ff73"

    const-string v12, "bold"

    invoke-static {v7, v11, v12}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_13c
    .catch Ljava/lang/Exception; {:try_start_9e .. :try_end_13c} :catch_259

    .line 453
    .end local v7    # "str2":Ljava/lang/String;
    :cond_13c
    :goto_13c
    sget-object v10, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->patch:Ljava/lang/String;

    const-string v11, "lib"

    if-ne v10, v11, :cond_258

    .line 456
    :try_start_142
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->getApplication()Landroid/app/Application;

    move-result-object v10

    invoke-virtual {v10}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v10

    const-string v11, "Search bytes... Please wait..."

    const/4 v12, 0x0

    invoke-static {v10, v11, v12}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v10

    invoke-virtual {v10}, Landroid/widget/Toast;->show()V

    .line 458
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v10

    iget-object v11, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    iget-object v11, v11, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    const/4 v12, 0x0

    invoke-virtual {v10, v11, v12}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v10

    iget-object v10, v10, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v10, v10, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    const-string v11, "/mnt/asec/"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_18b

    .line 459
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v10

    iget-object v11, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    iget-object v11, v11, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    const/4 v12, 0x0

    invoke-virtual {v10, v11, v12}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v10

    iget-object v10, v10, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v10, v10, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    const-string v11, "/pkg.apk"

    const-string v12, ""

    invoke-virtual {v10, v11, v12}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v10

    const-string v11, "rw"

    invoke-static {v10, v11}, Lcom/chelpus/Utils;->remount(Ljava/lang/String;Ljava/lang/String;)Z

    .line 461
    :cond_18b
    new-instance v10, Lcom/chelpus/Utils;

    const-string v11, ""

    invoke-direct {v10, v11}, Lcom/chelpus/Utils;-><init>(Ljava/lang/String;)V

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/String;

    const/4 v12, 0x0

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v14, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->dalvikruncommand:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ".liverunpatchlib "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    sget-object v14, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->selpath:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "\""

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "\""

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "\""

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "\""

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-virtual {v10, v11}, Lcom/chelpus/Utils;->cmdRoot([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    sput-object v10, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->str:Ljava/lang/String;

    .line 463
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v10

    iget-object v11, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    iget-object v11, v11, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    const/4 v12, 0x0

    invoke-virtual {v10, v11, v12}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v10

    iget-object v10, v10, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v10, v10, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    const-string v11, "/mnt/asec/"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_21c

    .line 464
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v10

    iget-object v11, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    iget-object v11, v11, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    const/4 v12, 0x0

    invoke-virtual {v10, v11, v12}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v10

    iget-object v10, v10, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v10, v10, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    const-string v11, "/pkg.apk"

    const-string v12, ""

    invoke-virtual {v10, v11, v12}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v10

    const-string v11, "ro"

    invoke-static {v10, v11}, Lcom/chelpus/Utils;->remount(Ljava/lang/String;Ljava/lang/String;)Z

    .line 467
    :cond_21c
    sget-object v7, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->str:Ljava/lang/String;

    .line 468
    .restart local v7    # "str2":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->getApplication()Landroid/app/Application;

    move-result-object v10

    invoke-virtual {v10}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 470
    .local v5, "r":Landroid/content/res/Resources;
    sget-object v10, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->str:Ljava/lang/String;

    const-string v11, "Error"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_241

    .line 471
    sget-object v10, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->tv:Landroid/widget/TextView;

    const-string v11, "#ffff0055"

    const-string v12, "bold"

    invoke-static {v7, v11, v12}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 472
    :cond_241
    sget-object v10, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->str:Ljava/lang/String;

    const-string v11, "Error"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_258

    .line 473
    sget-object v10, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->tv:Landroid/widget/TextView;

    const-string v11, "#ff00ff73"

    const-string v12, "bold"

    invoke-static {v7, v11, v12}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_258
    .catch Ljava/lang/Exception; {:try_start_142 .. :try_end_258} :catch_25f

    .line 482
    .end local v5    # "r":Landroid/content/res/Resources;
    .end local v7    # "str2":Ljava/lang/String;
    :cond_258
    :goto_258
    return-void

    .line 449
    :catch_259
    move-exception v2

    .line 450
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_13c

    .line 476
    .end local v2    # "e":Ljava/lang/Exception;
    :catch_25f
    move-exception v2

    .line 477
    .restart local v2    # "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_258
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 14
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 181
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 182
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    sput-object v8, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->context:Landroid/content/Context;

    .line 183
    const v8, 0x7f04002f

    invoke-virtual {p0, v8}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->setContentView(I)V

    .line 184
    sget-object v8, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    iput-object v8, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    .line 187
    const v8, 0x7f0d00c4

    invoke-virtual {p0, v8}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    const v9, 0x7f0d00c5

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 188
    .local v1, "body":Landroid/widget/LinearLayout;
    const-string v8, "dalvik"

    sput-object v8, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->patch:Ljava/lang/String;

    .line 189
    const v8, 0x7f0d00be

    invoke-virtual {p0, v8}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/EditText;

    sput-object v8, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->orhex:Landroid/widget/EditText;

    .line 190
    const v8, 0x7f0d00bf

    invoke-virtual {p0, v8}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/EditText;

    sput-object v8, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->rephex:Landroid/widget/EditText;

    .line 192
    sget-object v8, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->orhex:Landroid/widget/EditText;

    const/4 v9, 0x1

    new-array v9, v9, [Landroid/text/InputFilter;

    const/4 v10, 0x0

    sget-object v11, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->filter:Landroid/text/InputFilter;

    aput-object v11, v9, v10

    invoke-virtual {v8, v9}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 193
    sget-object v8, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->orhex:Landroid/widget/EditText;

    new-instance v9, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$3;

    invoke-direct {v9, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$3;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;)V

    invoke-virtual {v8, v9}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 214
    const v8, 0x7f0d00c6

    invoke-virtual {v1, v8}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    sput-object v8, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->tv:Landroid/widget/TextView;

    .line 216
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->getApplication()Landroid/app/Application;

    move-result-object v8

    invoke-virtual {v8}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 217
    .local v4, "r":Landroid/content/res/Resources;
    const v8, 0x7f070178

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    sput-object v8, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->str:Ljava/lang/String;

    .line 218
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const v9, 0x7f0700de

    invoke-virtual {v4, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\n\n"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->str:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 219
    .local v7, "str2":Ljava/lang/String;
    sget-object v8, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->tv:Landroid/widget/TextView;

    const-string v9, "#ff00ff73"

    const-string v10, "bold"

    invoke-static {v7, v9, v10}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 220
    const v8, 0x7f0d00c0

    invoke-virtual {p0, v8}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    .line 221
    .local v3, "patchButton":Landroid/widget/Button;
    new-instance v8, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$4;

    invoke-direct {v8, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$4;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;)V

    invoke-virtual {v3, v8}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 227
    const v8, 0x7f0d00c3

    invoke-virtual {p0, v8}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    .line 228
    .local v2, "launchButton":Landroid/widget/Button;
    new-instance v8, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$5;

    invoke-direct {v8, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$5;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;)V

    invoke-virtual {v2, v8}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 234
    const v8, 0x7f0d00c1

    invoke-virtual {p0, v8}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 235
    .local v0, "backupButton":Landroid/widget/Button;
    new-instance v8, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$6;

    invoke-direct {v8, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$6;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;)V

    invoke-virtual {v0, v8}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 241
    const v8, 0x7f0d00c2

    invoke-virtual {p0, v8}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    .line 242
    .local v5, "restoreButton":Landroid/widget/Button;
    new-instance v8, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$7;

    invoke-direct {v8, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$7;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;)V

    invoke-virtual {v5, v8}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 248
    const v8, 0x7f0d00c7

    invoke-virtual {p0, v8}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/Button;

    .line 249
    .local v6, "selectButton":Landroid/widget/Button;
    new-instance v8, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$8;

    invoke-direct {v8, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$8;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;)V

    invoke-virtual {v6, v8}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 271
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .registers 7
    .param p1, "id"    # I

    .prologue
    const/high16 v4, -0x1000000

    .line 286
    packed-switch p1, :pswitch_data_5c

    .line 392
    const/4 v1, 0x0

    :goto_6
    return-object v1

    .line 290
    :pswitch_7
    new-instance v1, Landroid/app/Dialog;

    invoke-direct {v1, p0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 291
    .local v1, "dialog":Landroid/app/Dialog;
    new-instance v2, Landroid/widget/ListView;

    invoke-direct {v2, p0}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    .line 292
    .local v2, "modeList":Landroid/widget/ListView;
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 295
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->fileList:[Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$Item;

    if-nez v3, :cond_26

    .line 296
    const-string v3, "F_PATH"

    const-string v4, "No files loaded"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 297
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 298
    goto :goto_6

    .line 301
    :cond_26
    const-string v3, "Choose your file"

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 302
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->adapter:Landroid/widget/ListAdapter;

    if-eqz v3, :cond_48

    .line 304
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->adapter:Landroid/widget/ListAdapter;

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 305
    invoke-virtual {v2}, Landroid/widget/ListView;->invalidateViews()V

    .line 306
    invoke-virtual {v2, v4}, Landroid/widget/ListView;->setCacheColorHint(I)V

    .line 307
    invoke-virtual {v2, v4}, Landroid/widget/ListView;->setBackgroundColor(I)V

    .line 308
    new-instance v3, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$9;

    invoke-direct {v3, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$9;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 374
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 376
    :cond_48
    new-instance v3, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$10;

    invoke-direct {v3, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$10;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;)V

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const/4 v4, 0x1

    .line 384
    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 388
    goto :goto_6

    .line 286
    nop

    :pswitch_data_5c
    .packed-switch 0x3
        :pswitch_7
    .end packed-switch
.end method

.method public onRestart()V
    .registers 1

    .prologue
    .line 281
    invoke-super {p0}, Landroid/app/Activity;->onRestart()V

    .line 282
    return-void
.end method

.method public onStop()V
    .registers 1

    .prologue
    .line 276
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 277
    return-void
.end method

.method public restore_click()V
    .registers 9

    .prologue
    .line 485
    sget-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->patch:Ljava/lang/String;

    const-string v4, "dalvik"

    if-ne v3, v4, :cond_54

    .line 487
    :try_start_6
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->getApplication()Landroid/app/Application;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "Restore processing... Please wait..."

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 488
    new-instance v3, Lcom/chelpus/Utils;

    const-string v4, ""

    invoke-direct {v3, v4}, Lcom/chelpus/Utils;-><init>(Ljava/lang/String;)V

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->dalvikruncommand:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ".live_restore "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    iget-object v7, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v3, v4}, Lcom/chelpus/Utils;->cmdRoot([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 490
    .local v2, "str4":Ljava/lang/String;
    move-object v1, v2

    .line 491
    .local v1, "str2":Ljava/lang/String;
    sget-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->tv:Landroid/widget/TextView;

    const-string v4, "#ff00ff73"

    const-string v5, "bold"

    invoke-static {v1, v4, v5}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_54
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_54} :catch_127

    .line 497
    .end local v1    # "str2":Ljava/lang/String;
    .end local v2    # "str4":Ljava/lang/String;
    :cond_54
    :goto_54
    sget-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->patch:Ljava/lang/String;

    const-string v4, "lib"

    if-ne v3, v4, :cond_126

    .line 499
    :try_start_5a
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    iget-object v4, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    iget-object v3, v3, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    const-string v4, "/mnt/asec/"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_91

    .line 500
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    iget-object v4, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    iget-object v3, v3, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    const-string v4, "/pkg.apk"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "rw"

    invoke-static {v3, v4}, Lcom/chelpus/Utils;->remount(Ljava/lang/String;Ljava/lang/String;)Z

    .line 503
    :cond_91
    new-instance v3, Lcom/chelpus/Utils;

    const-string v4, ""

    invoke-direct {v3, v4}, Lcom/chelpus/Utils;-><init>(Ljava/lang/String;)V

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->dalvikruncommand:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ".live_restorelib "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->selpath:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v3, v4}, Lcom/chelpus/Utils;->cmdRoot([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->str:Ljava/lang/String;

    .line 505
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    iget-object v4, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    iget-object v3, v3, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    const-string v4, "/mnt/asec/"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_f6

    .line 506
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    iget-object v4, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    iget-object v3, v3, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    const-string v4, "/pkg.apk"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "ro"

    invoke-static {v3, v4}, Lcom/chelpus/Utils;->remount(Ljava/lang/String;Ljava/lang/String;)Z

    .line 508
    :cond_f6
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->str:Ljava/lang/String;

    .line 509
    .restart local v1    # "str2":Ljava/lang/String;
    sget-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->str:Ljava/lang/String;

    const-string v4, "Error"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_10f

    .line 510
    sget-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->tv:Landroid/widget/TextView;

    const-string v4, "#ffff0055"

    const-string v5, "bold"

    invoke-static {v1, v4, v5}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 511
    :cond_10f
    sget-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->str:Ljava/lang/String;

    const-string v4, "Error"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_126

    .line 512
    sget-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;->tv:Landroid/widget/TextView;

    const-string v4, "#ff00ff73"

    const-string v5, "bold"

    invoke-static {v1, v4, v5}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_126
    .catch Ljava/lang/Exception; {:try_start_5a .. :try_end_126} :catch_12d

    .line 520
    .end local v1    # "str2":Ljava/lang/String;
    :cond_126
    :goto_126
    return-void

    .line 493
    :catch_127
    move-exception v0

    .line 494
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_54

    .line 515
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_12d
    move-exception v0

    .line 516
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_126
.end method
