.class public Lcom/android/vending/billing/InAppBillingService/LUCK/FileApkListItem;
.super Ljava/lang/Object;
.source "FileApkListItem.java"


# instance fields
.field public backupfile:Ljava/io/File;

.field public icon:Landroid/graphics/drawable/Drawable;

.field public name:Ljava/lang/String;

.field public pkgName:Ljava/lang/String;

.field public versionCode:I

.field public versionName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/io/File;Z)V
    .registers 26
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "file"    # Ljava/io/File;
    .param p3, "getIcon"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 24
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 18
    const-string v3, "Bad file"

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/FileApkListItem;->name:Ljava/lang/String;

    .line 26
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/FileApkListItem;->backupfile:Ljava/io/File;

    .line 27
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v16

    .line 28
    .local v16, "pm":Landroid/content/pm/PackageManager;
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v3, v4}, Landroid/content/pm/PackageManager;->getPackageArchiveInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v13

    .line 29
    .local v13, "info":Landroid/content/pm/PackageInfo;
    if-eqz v13, :cond_b4

    .line 30
    iget-object v9, v13, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    .line 32
    .local v9, "appInfo":Landroid/content/pm/ApplicationInfo;
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x8

    if-lt v3, v4, :cond_34

    .line 33
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v9, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    .line 34
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v9, Landroid/content/pm/ApplicationInfo;->publicSourceDir:Ljava/lang/String;

    .line 38
    :cond_34
    iget-object v3, v13, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/FileApkListItem;->pkgName:Ljava/lang/String;

    .line 40
    move-object/from16 v0, v16

    invoke-virtual {v9, v0}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/FileApkListItem;->name:Ljava/lang/String;

    .line 41
    if-eqz p3, :cond_a2

    .line 42
    const/16 v20, 0x23

    .line 43
    .local v20, "scaleImage":I
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v0, v3, Landroid/util/DisplayMetrics;->density:F

    move/from16 v18, v0

    .line 44
    .local v18, "scale":F
    const/high16 v3, 0x420c0000    # 35.0f

    mul-float v3, v3, v18

    const/high16 v4, 0x3f000000    # 0.5f

    add-float/2addr v3, v4

    float-to-int v12, v3

    .line 45
    .local v12, "imgSize":I
    const/4 v2, 0x0

    .line 47
    .local v2, "iconka":Landroid/graphics/Bitmap;
    :try_start_61
    move-object/from16 v0, v16

    invoke-virtual {v9, v0}, Landroid/content/pm/ApplicationInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    check-cast v3, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;
    :try_end_6c
    .catch Ljava/lang/Exception; {:try_start_61 .. :try_end_6c} :catch_af

    move-result-object v2

    .line 52
    :goto_6d
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    .line 53
    .local v5, "width":I
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    .line 55
    .local v6, "height":I
    move v15, v12

    .line 56
    .local v15, "newWidth":I
    move v14, v12

    .line 57
    .local v14, "newHeight":I
    int-to-float v3, v15

    int-to-float v4, v5

    div-float v21, v3, v4

    .line 58
    .local v21, "scaleWidth":F
    int-to-float v3, v14

    int-to-float v4, v6

    div-float v19, v3, v4

    .line 60
    .local v19, "scaleHeight":F
    new-instance v7, Landroid/graphics/Matrix;

    invoke-direct {v7}, Landroid/graphics/Matrix;-><init>()V

    .line 61
    .local v7, "bMatrix":Landroid/graphics/Matrix;
    move/from16 v0, v21

    move/from16 v1, v19

    invoke-virtual {v7, v0, v1}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 63
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v8, 0x1

    invoke-static/range {v2 .. v8}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v17

    .line 64
    .local v17, "resizedBitmap":Landroid/graphics/Bitmap;
    new-instance v10, Landroid/graphics/drawable/BitmapDrawable;

    move-object/from16 v0, v17

    invoke-direct {v10, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    .line 66
    .local v10, "bmd":Landroid/graphics/drawable/BitmapDrawable;
    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/FileApkListItem;->icon:Landroid/graphics/drawable/Drawable;

    .line 67
    move-object/from16 v0, v16

    invoke-virtual {v9, v0}, Landroid/content/pm/ApplicationInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    .line 69
    .end local v2    # "iconka":Landroid/graphics/Bitmap;
    .end local v5    # "width":I
    .end local v6    # "height":I
    .end local v7    # "bMatrix":Landroid/graphics/Matrix;
    .end local v10    # "bmd":Landroid/graphics/drawable/BitmapDrawable;
    .end local v12    # "imgSize":I
    .end local v14    # "newHeight":I
    .end local v15    # "newWidth":I
    .end local v17    # "resizedBitmap":Landroid/graphics/Bitmap;
    .end local v18    # "scale":F
    .end local v19    # "scaleHeight":F
    .end local v20    # "scaleImage":I
    .end local v21    # "scaleWidth":F
    :cond_a2
    iget-object v3, v13, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/FileApkListItem;->versionName:Ljava/lang/String;

    .line 70
    iget v3, v13, Landroid/content/pm/PackageInfo;->versionCode:I

    move-object/from16 v0, p0

    iput v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/FileApkListItem;->versionCode:I

    .line 76
    return-void

    .line 48
    .restart local v2    # "iconka":Landroid/graphics/Bitmap;
    .restart local v12    # "imgSize":I
    .restart local v18    # "scale":F
    .restart local v20    # "scaleImage":I
    :catch_af
    move-exception v11

    .line 50
    .local v11, "e":Ljava/lang/Exception;
    invoke-virtual {v11}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_6d

    .line 73
    .end local v2    # "iconka":Landroid/graphics/Bitmap;
    .end local v9    # "appInfo":Landroid/content/pm/ApplicationInfo;
    .end local v11    # "e":Ljava/lang/Exception;
    .end local v12    # "imgSize":I
    .end local v18    # "scale":F
    .end local v20    # "scaleImage":I
    :cond_b4
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "oblom"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
.end method
