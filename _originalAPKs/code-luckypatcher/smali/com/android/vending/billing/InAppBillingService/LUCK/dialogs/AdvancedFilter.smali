.class public Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/AdvancedFilter;
.super Ljava/lang/Object;
.source "AdvancedFilter.java"


# static fields
.field public static final LOADING_PROGRESS_DIALOG:I = 0x17


# instance fields
.field adv:Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/AdvancedFilter;

.field dialog:Landroid/app/Dialog;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/AdvancedFilter;->adv:Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/AdvancedFilter;

    .line 32
    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/AdvancedFilter;->dialog:Landroid/app/Dialog;

    .line 29
    iput-object p0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/AdvancedFilter;->adv:Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/AdvancedFilter;

    .line 31
    return-void
.end method


# virtual methods
.method public dismiss()V
    .registers 2

    .prologue
    .line 135
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/AdvancedFilter;->dialog:Landroid/app/Dialog;

    if-eqz v0, :cond_c

    .line 136
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/AdvancedFilter;->dialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 137
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/AdvancedFilter;->dialog:Landroid/app/Dialog;

    .line 139
    :cond_c
    return-void
.end method

.method public onCreateDialog()Landroid/app/Dialog;
    .registers 27

    .prologue
    .line 44
    sget-object v23, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v24, "Market install Dialog create."

    invoke-virtual/range {v23 .. v24}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 45
    sget-object v23, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    if-eqz v23, :cond_13

    sget-object v23, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual/range {v23 .. v23}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v23

    if-nez v23, :cond_16

    :cond_13
    invoke-virtual/range {p0 .. p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/AdvancedFilter;->dismiss()V

    .line 46
    :cond_16
    sget-object v23, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual/range {v23 .. v23}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v23

    const v24, 0x7f040003

    const/16 v25, 0x0

    invoke-static/range {v23 .. v25}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    .line 47
    .local v4, "d":Landroid/widget/LinearLayout;
    const v23, 0x7f0d000b

    move/from16 v0, v23

    invoke-virtual {v4, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v23

    const v24, 0x7f0d000c

    invoke-virtual/range {v23 .. v24}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Landroid/widget/LinearLayout;

    .line 48
    .local v20, "scrbody":Landroid/widget/LinearLayout;
    const v23, 0x7f0d0031

    move-object/from16 v0, v20

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/TextView;

    .line 49
    .local v22, "text":Landroid/widget/TextView;
    const v23, 0x7f0d000e

    move-object/from16 v0, v20

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/RadioGroup;

    .line 50
    .local v18, "group":Landroid/widget/RadioGroup;
    invoke-virtual/range {v18 .. v18}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    .line 52
    const v23, 0x7f0d000f

    move-object/from16 v0, v18

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v19

    check-cast v19, Landroid/widget/RadioButton;

    .line 53
    .local v19, "nofilter":Landroid/widget/RadioButton;
    const v23, 0x7f070185

    invoke-static/range {v23 .. v23}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v19

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 54
    sget v23, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->advancedFilter:I

    if-eqz v23, :cond_80

    sget v23, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->advancedFilter:I

    const v24, 0x7f0d000f

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_89

    :cond_80
    const/16 v23, 0x1

    move-object/from16 v0, v19

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 55
    :cond_89
    const v23, 0x7f0d0010

    move-object/from16 v0, v18

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/RadioButton;

    .line 56
    .local v5, "filter0":Landroid/widget/RadioButton;
    const v23, 0x7f070111

    invoke-static/range {v23 .. v23}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v5, v0}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 57
    sget v23, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->advancedFilter:I

    const v24, 0x7f0d0010

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_b4

    const/16 v23, 0x1

    move/from16 v0, v23

    invoke-virtual {v5, v0}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 58
    :cond_b4
    const v23, 0x7f0d0011

    move-object/from16 v0, v18

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/RadioButton;

    .line 59
    .local v6, "filter1":Landroid/widget/RadioButton;
    const v23, 0x7f070112

    invoke-static/range {v23 .. v23}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v6, v0}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 60
    sget v23, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->advancedFilter:I

    const v24, 0x7f0d0011

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_df

    const/16 v23, 0x1

    move/from16 v0, v23

    invoke-virtual {v6, v0}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 61
    :cond_df
    const v23, 0x7f0d0012

    move-object/from16 v0, v18

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/RadioButton;

    .line 62
    .local v11, "filter2":Landroid/widget/RadioButton;
    const v23, 0x7f070117

    invoke-static/range {v23 .. v23}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v11, v0}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 63
    sget v23, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->advancedFilter:I

    const v24, 0x7f0d0012

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_10a

    const/16 v23, 0x1

    move/from16 v0, v23

    invoke-virtual {v11, v0}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 65
    :cond_10a
    const v23, 0x7f0d0013

    move-object/from16 v0, v18

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/RadioButton;

    .line 66
    .local v8, "filter11":Landroid/widget/RadioButton;
    const v23, 0x7f070114

    invoke-static/range {v23 .. v23}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v8, v0}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 67
    sget v23, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->advancedFilter:I

    const v24, 0x7f0d0013

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_135

    const/16 v23, 0x1

    move/from16 v0, v23

    invoke-virtual {v8, v0}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 68
    :cond_135
    const v23, 0x7f0d0014

    move-object/from16 v0, v18

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/RadioButton;

    .line 69
    .local v9, "filter12":Landroid/widget/RadioButton;
    const v23, 0x7f070115

    invoke-static/range {v23 .. v23}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v9, v0}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 70
    sget v23, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->advancedFilter:I

    const v24, 0x7f0d0014

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_160

    const/16 v23, 0x1

    move/from16 v0, v23

    invoke-virtual {v9, v0}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 71
    :cond_160
    const v23, 0x7f0d0015

    move-object/from16 v0, v18

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/RadioButton;

    .line 72
    .local v10, "filter13":Landroid/widget/RadioButton;
    const v23, 0x7f070116

    invoke-static/range {v23 .. v23}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v10, v0}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 73
    sget v23, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->advancedFilter:I

    const v24, 0x7f0d0015

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_18b

    const/16 v23, 0x1

    move/from16 v0, v23

    invoke-virtual {v10, v0}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 75
    :cond_18b
    const v23, 0x7f0d0016

    move-object/from16 v0, v18

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/RadioButton;

    .line 76
    .local v12, "filter4":Landroid/widget/RadioButton;
    const v23, 0x7f070119

    invoke-static/range {v23 .. v23}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v12, v0}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 77
    sget v23, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->advancedFilter:I

    const v24, 0x7f0d0016

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_1b6

    const/16 v23, 0x1

    move/from16 v0, v23

    invoke-virtual {v12, v0}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 78
    :cond_1b6
    const v23, 0x7f0d0017

    move-object/from16 v0, v18

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/RadioButton;

    .line 79
    .local v13, "filter5":Landroid/widget/RadioButton;
    const v23, 0x7f07011a

    invoke-static/range {v23 .. v23}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v13, v0}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 80
    sget v23, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->advancedFilter:I

    const v24, 0x7f0d0017

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_1e1

    const/16 v23, 0x1

    move/from16 v0, v23

    invoke-virtual {v13, v0}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 81
    :cond_1e1
    const v23, 0x7f0d0018

    move-object/from16 v0, v18

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/RadioButton;

    .line 82
    .local v14, "filter6":Landroid/widget/RadioButton;
    const v23, 0x7f07011b

    invoke-static/range {v23 .. v23}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v14, v0}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 83
    sget v23, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->advancedFilter:I

    const v24, 0x7f0d0018

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_20c

    const/16 v23, 0x1

    move/from16 v0, v23

    invoke-virtual {v14, v0}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 84
    :cond_20c
    const v23, 0x7f0d001a

    move-object/from16 v0, v18

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/RadioButton;

    .line 85
    .local v15, "filter7":Landroid/widget/RadioButton;
    const v23, 0x7f07011c

    invoke-static/range {v23 .. v23}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v15, v0}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 86
    sget v23, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->advancedFilter:I

    const v24, 0x7f0d001a

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_237

    const/16 v23, 0x1

    move/from16 v0, v23

    invoke-virtual {v15, v0}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 87
    :cond_237
    const v23, 0x7f0d001b

    move-object/from16 v0, v18

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/RadioButton;

    .line 88
    .local v16, "filter8":Landroid/widget/RadioButton;
    const v23, 0x7f07011d

    invoke-static/range {v23 .. v23}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v16

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 89
    sget v23, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->advancedFilter:I

    const v24, 0x7f0d001b

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_266

    const/16 v23, 0x1

    move-object/from16 v0, v16

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 90
    :cond_266
    const v23, 0x7f0d001c

    move-object/from16 v0, v18

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/RadioButton;

    .line 91
    .local v17, "filter9":Landroid/widget/RadioButton;
    const v23, 0x7f07011e

    invoke-static/range {v23 .. v23}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v17

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 92
    sget v23, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->advancedFilter:I

    const v24, 0x7f0d001c

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_295

    const/16 v23, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 93
    :cond_295
    const v23, 0x7f0d0019

    move-object/from16 v0, v18

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/RadioButton;

    .line 94
    .local v7, "filter10":Landroid/widget/RadioButton;
    const v23, 0x7f070113

    invoke-static/range {v23 .. v23}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v7, v0}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 95
    sget v23, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->advancedFilter:I

    const v24, 0x7f0d0019

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_2c0

    const/16 v23, 0x1

    move/from16 v0, v23

    invoke-virtual {v7, v0}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 97
    :cond_2c0
    new-instance v23, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/AdvancedFilter$1;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/AdvancedFilter$1;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/AdvancedFilter;)V

    move-object/from16 v0, v18

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 109
    const v23, 0x7f0d001d

    move/from16 v0, v23

    invoke-virtual {v4, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    .line 110
    .local v2, "button":Landroid/widget/Button;
    new-instance v23, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/AdvancedFilter$2;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/AdvancedFilter$2;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/AdvancedFilter;)V

    move-object/from16 v0, v23

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 116
    const v23, 0x7f0d001e

    move/from16 v0, v23

    invoke-virtual {v4, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    .line 117
    .local v3, "button2":Landroid/widget/Button;
    new-instance v23, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/AdvancedFilter$3;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/AdvancedFilter$3;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/AdvancedFilter;)V

    move-object/from16 v0, v23

    invoke-virtual {v3, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 123
    new-instance v23, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    sget-object v24, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual/range {v24 .. v24}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v24

    const/16 v25, 0x1

    invoke-direct/range {v23 .. v25}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;-><init>(Landroid/content/Context;Z)V

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setView(Landroid/view/View;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->create()Landroid/app/Dialog;

    move-result-object v21

    .line 124
    .local v21, "tempdialog":Landroid/app/Dialog;
    const/16 v23, 0x1

    move-object/from16 v0, v21

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 125
    new-instance v23, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/AdvancedFilter$4;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/AdvancedFilter$4;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/AdvancedFilter;)V

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 131
    return-object v21
.end method

.method public showDialog()V
    .registers 2

    .prologue
    .line 34
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/AdvancedFilter;->dialog:Landroid/app/Dialog;

    if-nez v0, :cond_a

    .line 35
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/AdvancedFilter;->onCreateDialog()Landroid/app/Dialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/AdvancedFilter;->dialog:Landroid/app/Dialog;

    .line 37
    :cond_a
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/AdvancedFilter;->dialog:Landroid/app/Dialog;

    if-eqz v0, :cond_13

    .line 38
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/AdvancedFilter;->dialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 40
    :cond_13
    return-void
.end method
