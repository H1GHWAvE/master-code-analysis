.class final Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$byNameApkFile;
.super Ljava/lang/Object;
.source "listAppsFragment.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "byNameApkFile"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/android/vending/billing/InAppBillingService/LUCK/FileApkListItem;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;)V
    .registers 2
    .param p1, "this$0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    .prologue
    .line 7348
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$byNameApkFile;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/android/vending/billing/InAppBillingService/LUCK/FileApkListItem;Lcom/android/vending/billing/InAppBillingService/LUCK/FileApkListItem;)I
    .registers 5
    .param p1, "a"    # Lcom/android/vending/billing/InAppBillingService/LUCK/FileApkListItem;
    .param p2, "b"    # Lcom/android/vending/billing/InAppBillingService/LUCK/FileApkListItem;

    .prologue
    .line 7350
    if-eqz p1, :cond_4

    if-nez p2, :cond_a

    .line 7351
    :cond_4
    new-instance v0, Ljava/lang/ClassCastException;

    invoke-direct {v0}, Ljava/lang/ClassCastException;-><init>()V

    throw v0

    .line 7353
    :cond_a
    iget-object v0, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/FileApkListItem;->pkgName:Ljava/lang/String;

    iget-object v1, p2, Lcom/android/vending/billing/InAppBillingService/LUCK/FileApkListItem;->pkgName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 7354
    iget v0, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/FileApkListItem;->versionCode:I

    iget v1, p2, Lcom/android/vending/billing/InAppBillingService/LUCK/FileApkListItem;->versionCode:I

    if-le v0, v1, :cond_1c

    const/4 v0, 0x1

    .line 7358
    :goto_1b
    return v0

    .line 7355
    :cond_1c
    iget v0, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/FileApkListItem;->versionCode:I

    iget v1, p2, Lcom/android/vending/billing/InAppBillingService/LUCK/FileApkListItem;->versionCode:I

    if-ge v0, v1, :cond_24

    const/4 v0, -0x1

    goto :goto_1b

    .line 7356
    :cond_24
    iget v0, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/FileApkListItem;->versionCode:I

    iget v1, p2, Lcom/android/vending/billing/InAppBillingService/LUCK/FileApkListItem;->versionCode:I

    if-ne v0, v1, :cond_2c

    const/4 v0, 0x0

    goto :goto_1b

    .line 7358
    :cond_2c
    iget-object v0, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/FileApkListItem;->name:Ljava/lang/String;

    iget-object v1, p2, Lcom/android/vending/billing/InAppBillingService/LUCK/FileApkListItem;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    goto :goto_1b
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 4

    .prologue
    .line 7348
    check-cast p1, Lcom/android/vending/billing/InAppBillingService/LUCK/FileApkListItem;

    check-cast p2, Lcom/android/vending/billing/InAppBillingService/LUCK/FileApkListItem;

    invoke-virtual {p0, p1, p2}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$byNameApkFile;->compare(Lcom/android/vending/billing/InAppBillingService/LUCK/FileApkListItem;Lcom/android/vending/billing/InAppBillingService/LUCK/FileApkListItem;)I

    move-result v0

    return v0
.end method
