.class public Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source "patchActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$byNameApkFile;
    }
.end annotation


# static fields
.field public static final APP_DIALOG:I = 0x6

.field public static final CONTEXT_DIALOG:I = 0x7

.field public static final CREATE_APK:I = 0x0

.field public static final CUSTOM2_DIALOG:I = 0xf

.field public static final CUSTOM_PATCH:I = 0x1

.field public static final DIALOG_REPORT_FORCE_CLOSE:I = 0x35f3ac

.field public static final MARKET_INSTALL_DIALOG:I = 0x1e

.field public static final PROGRESS_DIALOG2:I = 0xb

.field public static final RESTORE_FROM_BACKUP:I = 0x1c

.field private static final SETTINGS_ORIENT_LANDSCAPE:I = 0x1

.field private static final SETTINGS_ORIENT_PORTRET:I = 0x2

.field public static final SETTINGS_VIEWSIZE_DEFAULT:I

.field private static final SETTINGS_VIEWSIZE_SMALL:I

.field public static frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;


# instance fields
.field mIsRestoredToTop:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 90
    const/4 v0, 0x0

    sput-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 65
    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    .line 293
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->mIsRestoredToTop:Z

    .line 1599
    return-void
.end method


# virtual methods
.method public apply_click(Landroid/view/View;)V
    .registers 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 562
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    const/16 v0, 0xf

    invoke-static {v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->removeDialogLP(I)V

    .line 563
    sget v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->func:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_13

    .line 564
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    invoke-virtual {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->custompatch(Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;)V

    .line 565
    :cond_13
    sget v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->func:I

    if-nez v0, :cond_1c

    .line 566
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->createapkcustom()V

    .line 567
    :cond_1c
    return-void
.end method

.method public backup_click(Landroid/view/View;)V
    .registers 10
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 547
    :try_start_0
    const-string v2, ""

    .line 550
    .local v2, "str3":Ljava/lang/String;
    const-string v3, ""

    sput-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->str:Ljava/lang/String;

    .line 551
    new-instance v3, Lcom/chelpus/Utils;

    const-string v4, ""

    invoke-direct {v3, v4}, Lcom/chelpus/Utils;-><init>(Ljava/lang/String;)V

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->dalvikruncommand:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ".backup "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    iget-object v7, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v3, v4}, Lcom/chelpus/Utils;->cmdRoot([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 552
    move-object v1, v2

    .line 553
    .local v1, "str2":Ljava/lang/String;
    sget-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->tvt:Landroid/widget/TextView;

    const-string v4, "#ff00ff73"

    const-string v5, "bold"

    invoke-static {v1, v4, v5}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_42
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_42} :catch_43

    .line 558
    .end local v1    # "str2":Ljava/lang/String;
    .end local v2    # "str3":Ljava/lang/String;
    :goto_42
    return-void

    .line 555
    :catch_43
    move-exception v0

    .line 556
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_42
.end method

.method public cancel_click(Landroid/view/View;)V
    .registers 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 570
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    const/16 v0, 0xf

    invoke-static {v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->removeDialogLP(I)V

    .line 571
    return-void
.end method

.method public finish()V
    .registers 4

    .prologue
    .line 308
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 309
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-lt v1, v2, :cond_23

    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->isTaskRoot()Z

    move-result v1

    if-nez v1, :cond_23

    iget-boolean v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->mIsRestoredToTop:Z

    if-eqz v1, :cond_23

    .line 313
    const-string v1, "activity"

    invoke-virtual {p0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 314
    .local v0, "tasksManager":Landroid/app/ActivityManager;
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->getTaskId()I

    move-result v1

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/app/ActivityManager;->moveTaskToFront(II)V

    .line 316
    .end local v0    # "tasksManager":Landroid/app/ActivityManager;
    :cond_23
    return-void
.end method

.method public fixobject_click(Landroid/view/View;)V
    .registers 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 496
    :try_start_0
    sget-object v2, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    sget-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    invoke-virtual {v2, v3}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->odex(Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;)V

    .line 497
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f07000f

    invoke-static {v3}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 498
    .local v1, "str2":Ljava/lang/String;
    sget-object v2, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->tvt:Landroid/widget/TextView;

    const-string v3, "#ff00ff73"

    const-string v4, "bold"

    invoke-static {v1, v3, v4}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_2e
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_2e} :catch_2f

    .line 504
    .end local v1    # "str2":Ljava/lang/String;
    :goto_2e
    return-void

    .line 500
    :catch_2f
    move-exception v0

    .line 501
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 502
    const-string v2, "Error while saving file"

    const/4 v3, 0x1

    invoke-static {p0, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_2e
.end method

.method public launch_click(Landroid/view/View;)V
    .registers 7
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 457
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "vibration"

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_22

    .line 458
    sget-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    const-string v2, "vibrator"

    invoke-virtual {p0, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Vibrator;

    iput-object v2, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->vib:Landroid/os/Vibrator;

    .line 459
    sget-object v2, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iget-object v2, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->vib:Landroid/os/Vibrator;

    const-wide/16 v3, 0x32

    invoke-virtual {v2, v3, v4}, Landroid/os/Vibrator;->vibrate(J)V

    .line 462
    :cond_22
    sget-object v2, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    const/4 v2, 0x6

    invoke-static {v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->removeDialogLP(I)V

    .line 465
    :try_start_28
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "killall "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 466
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v2

    sget-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 467
    .local v1, "i":Landroid/content/Intent;
    invoke-virtual {p0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_51
    .catch Ljava/lang/RuntimeException; {:try_start_28 .. :try_end_51} :catch_52
    .catch Ljava/lang/Exception; {:try_start_28 .. :try_end_51} :catch_57

    .line 471
    .end local v1    # "i":Landroid/content/Intent;
    :goto_51
    return-void

    .line 468
    :catch_52
    move-exception v0

    .local v0, "e":Ljava/lang/RuntimeException;
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->printStackTrace()V

    goto :goto_51

    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catch_57
    move-exception v0

    .line 469
    .local v0, "e":Ljava/lang/Exception;
    const v2, 0x7f0700fd

    invoke-static {v2}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {p0, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_51
.end method

.method public mod_market_check(Landroid/view/View;)V
    .registers 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1613
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->connectToLicensing()V

    .line 1614
    return-void
.end method

.method public onBackPressed()V
    .registers 6

    .prologue
    .line 263
    :try_start_0
    sget-boolean v2, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->menu_open:Z

    if-eqz v2, :cond_a

    .line 264
    sget-object v2, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->hideMenu()V

    .line 292
    :goto_9
    return-void

    .line 267
    :cond_a
    sget-boolean v2, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->adapterSelect:Z

    if-eqz v2, :cond_1c

    .line 268
    sget-object v2, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->resetBatchOperation()V
    :try_end_13
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_13} :catch_14

    goto :goto_9

    .line 291
    :catch_14
    move-exception v1

    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onBackPressed()V

    goto :goto_9

    .line 272
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_1c
    :try_start_1c
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "confirm_exit"

    const/4 v4, 0x1

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_41

    .line 273
    new-instance v0, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$4;

    invoke-direct {v0, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$4;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;)V

    .line 287
    .local v0, "dialogClickListener":Landroid/content/DialogInterface$OnClickListener;
    const v2, 0x7f070019

    invoke-static {v2}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f070164

    invoke-static {v3}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v2, v3, v0, v0, v4}, Lcom/chelpus/Utils;->showDialogYesNo(Ljava/lang/String;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V

    goto :goto_9

    .line 289
    .end local v0    # "dialogClickListener":Landroid/content/DialogInterface$OnClickListener;
    :cond_41
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onBackPressed()V
    :try_end_44
    .catch Ljava/lang/Exception; {:try_start_1c .. :try_end_44} :catch_14

    goto :goto_9
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 12
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 94
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 95
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v5, "LuckyPatcher: create activity"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 99
    const-string v4, "config"

    invoke-virtual {p0, v4, v9}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    const-string v5, "force_close"

    invoke-interface {v4, v5, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_b2

    .line 100
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v5, "LP FC detected!"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 102
    :try_start_23
    const-string v4, "config"

    const/4 v5, 0x4

    invoke-virtual {p0, v4, v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string v5, "force_close"

    const/4 v6, 0x0

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 103
    new-instance v4, Lcom/android/vending/billing/InAppBillingService/LUCK/LogCollector;

    invoke-direct {v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/LogCollector;-><init>()V

    sput-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->mLogCollector:Lcom/android/vending/billing/InAppBillingService/LUCK/LogCollector;

    .line 105
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->init()V

    .line 106
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 108
    .local v2, "logbuilder":Landroid/app/AlertDialog$Builder;
    const v4, 0x7f0700fb

    invoke-static {v4}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v3

    .line 109
    .local v3, "message":Ljava/lang/String;
    const v4, 0x7f070234

    invoke-static {v4}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x1080027

    .line 110
    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    .line 111
    invoke-virtual {v4, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x7f070003

    .line 112
    invoke-static {v5}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$3;

    invoke-direct {v6, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$3;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;)V

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x7f07017b

    .line 166
    invoke-static {v5}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$2;

    invoke-direct {v6, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$2;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;)V

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    new-instance v5, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$1;

    invoke-direct {v5, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$1;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;)V

    .line 173
    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 181
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/AlertDialog;->show()V
    :try_end_93
    .catch Ljava/lang/Exception; {:try_start_23 .. :try_end_93} :catch_94

    .line 258
    .end local v2    # "logbuilder":Landroid/app/AlertDialog$Builder;
    .end local v3    # "message":Ljava/lang/String;
    :goto_93
    return-void

    .line 182
    :catch_94
    move-exception v1

    .line 183
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 184
    const-string v4, "config"

    invoke-virtual {p0, v4, v9}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string v5, "force_close"

    invoke-interface {v4, v5, v7}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 185
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->finish()V

    .line 186
    invoke-static {v7}, Ljava/lang/System;->exit(I)V

    goto :goto_93

    .line 189
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_b2
    sput-boolean v6, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->desktop_launch:Z

    .line 190
    sput-object p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patchAct:Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;

    .line 191
    sput-object p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->appcontext:Landroid/content/Context;

    .line 192
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v4

    const-string v5, "hide_title"

    invoke-interface {v4, v5, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_de

    .line 193
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 194
    .local v0, "api":I
    const/16 v4, 0xe

    if-lt v0, v4, :cond_d7

    invoke-static {p0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/ViewConfiguration;->hasPermanentMenuKey()Z

    move-result v4

    if-eqz v4, :cond_d7

    .line 195
    invoke-virtual {p0, v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->requestWindowFeature(I)Z

    .line 196
    :cond_d7
    const/16 v4, 0xa

    if-gt v0, v4, :cond_de

    invoke-virtual {p0, v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->requestWindowFeature(I)Z

    .line 201
    .end local v0    # "api":I
    :cond_de
    const v4, 0x7f040002

    invoke-virtual {p0, v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->setContentView(I)V

    .line 204
    const v4, 0x7f07003c

    :try_start_e7
    invoke-static {v4}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_f0

    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->finish()V
    :try_end_f0
    .catch Ljava/lang/Exception; {:try_start_e7 .. :try_end_f0} :catch_129

    .line 209
    :cond_f0
    :goto_f0
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v4

    const-string v5, "orientstion"

    invoke-interface {v4, v5, v8}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    if-ne v4, v6, :cond_ff

    .line 210
    invoke-virtual {p0, v7}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->setRequestedOrientation(I)V

    .line 212
    :cond_ff
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v4

    const-string v5, "orientstion"

    invoke-interface {v4, v5, v8}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_10f

    .line 213
    invoke-virtual {p0, v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->setRequestedOrientation(I)V

    .line 215
    :cond_10f
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v4

    const-string v5, "orientstion"

    invoke-interface {v4, v5, v8}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    if-ne v4, v8, :cond_11e

    .line 216
    invoke-virtual {p0, v9}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->setRequestedOrientation(I)V

    .line 243
    :cond_11e
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->getWindow()Landroid/view/Window;

    move-result-object v4

    const/16 v5, 0x80

    invoke-virtual {v4, v5}, Landroid/view/Window;->addFlags(I)V

    goto/16 :goto_93

    .line 205
    :catch_129
    move-exception v1

    .line 206
    .restart local v1    # "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 207
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->finish()V

    goto :goto_f0
.end method

.method public onMemoryLow()V
    .registers 3

    .prologue
    .line 346
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->goodMemory:Z

    .line 347
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "LuckyPatcher (onMemoryLow): started!"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 349
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .registers 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 296
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 297
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "Lucky Patcher: on new intent activity."

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 298
    sput-object p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patchAct:Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;

    .line 299
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    sput-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->handler:Landroid/os/Handler;

    .line 305
    return-void
.end method

.method public onPause()V
    .registers 4

    .prologue
    .line 327
    :try_start_0
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onPause()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_3} :catch_14

    .line 329
    :goto_3
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "Lucky Patcher: activity pause."

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 330
    sput-object p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patchAct:Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;

    .line 331
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    sput-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->handler:Landroid/os/Handler;

    .line 332
    return-void

    .line 328
    :catch_14
    move-exception v0

    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3
.end method

.method public onResume()V
    .registers 4

    .prologue
    .line 336
    :try_start_0
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onResume()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_3} :catch_14

    .line 338
    :goto_3
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "Lucky Patcher: activity resume."

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 339
    sput-object p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patchAct:Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;

    .line 340
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    sput-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->handler:Landroid/os/Handler;

    .line 341
    return-void

    .line 337
    :catch_14
    move-exception v0

    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3
.end method

.method public onStart()V
    .registers 3

    .prologue
    .line 319
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onStart()V

    .line 320
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "Lucky Patcher: start activity."

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 321
    sput-object p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patchAct:Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;

    .line 322
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    sput-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->handler:Landroid/os/Handler;

    .line 323
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .registers 14
    .param p1, "hasFocus"    # Z

    .prologue
    const-wide/32 v10, 0x100000

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 353
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onWindowFocusChanged(Z)V

    .line 355
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v5, "onWindowFocusChanged"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 356
    sput-object p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patchAct:Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;

    .line 357
    new-instance v4, Landroid/os/Handler;

    invoke-direct {v4}, Landroid/os/Handler;-><init>()V

    sput-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->handler:Landroid/os/Handler;

    .line 358
    sget-boolean v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->return_from_control_panel:Z

    if-eqz v4, :cond_56

    if-eqz p1, :cond_56

    .line 359
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->createExpandMenu()V

    .line 360
    sput-boolean v8, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->return_from_control_panel:Z

    .line 362
    sget-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    if-eqz v4, :cond_3a

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    sget-object v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    iget-object v5, v5, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    invoke-interface {v4, v5}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 364
    :cond_3a
    sget-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->plia:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;

    if-eqz v4, :cond_43

    sget-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->plia:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;

    invoke-virtual {v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->notifyDataSetChanged()V

    .line 365
    :cond_43
    sput-boolean v9, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->refresh:Z

    .line 366
    const/4 v4, 0x6

    invoke-static {v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->removeDialogLP(I)V

    .line 367
    new-instance v4, Ljava/lang/Thread;

    new-instance v5, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$5;

    invoke-direct {v5, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$5;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;)V

    invoke-direct {v4, v5}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 383
    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    .line 386
    :cond_56
    sget-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->plia:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;

    if-eqz v4, :cond_5f

    sget-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->plia:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;

    invoke-virtual {v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->notifyDataSetChanged()V

    .line 387
    :cond_5f
    new-instance v3, Landroid/app/ActivityManager$MemoryInfo;

    invoke-direct {v3}, Landroid/app/ActivityManager$MemoryInfo;-><init>()V

    .line 388
    .local v3, "mi":Landroid/app/ActivityManager$MemoryInfo;
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v4

    const-string v5, "activity"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 389
    .local v0, "activityManager":Landroid/app/ActivityManager;
    invoke-virtual {v0, v3}, Landroid/app/ActivityManager;->getMemoryInfo(Landroid/app/ActivityManager$MemoryInfo;)V

    .line 390
    iget-wide v4, v3, Landroid/app/ActivityManager$MemoryInfo;->availMem:J

    div-long v1, v4, v10

    .line 391
    .local v1, "availableMegs":J
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "LuckyPatcher "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->version:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " (FreeMemory): "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " lowMemory:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-boolean v6, v3, Landroid/app/ActivityManager$MemoryInfo;->lowMemory:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " TrashOld:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, v3, Landroid/app/ActivityManager$MemoryInfo;->threshold:J

    div-long/2addr v6, v10

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 392
    sget-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->firstrun:Ljava/lang/Boolean;

    if-eqz v4, :cond_c0

    sget-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->firstrun:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_d3

    :cond_c0
    sget-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->plia:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;

    if-eqz v4, :cond_d3

    if-eqz p1, :cond_d3

    .line 435
    sput-boolean v9, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->refresh:Z

    .line 436
    sget-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->plia:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;

    invoke-virtual {v4, v9}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->refreshPkgs(Z)V

    .line 437
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    sput-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->firstrun:Ljava/lang/Boolean;

    .line 439
    :cond_d3
    sget-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->plia:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;

    if-eqz v4, :cond_e2

    if-eqz p1, :cond_e2

    sget-boolean v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->refresh:Z

    if-eqz v4, :cond_e2

    sget-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->plia:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;

    invoke-virtual {v4, v8}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->refreshPkgs(Z)V

    .line 441
    :cond_e2
    return-void
.end method

.method public patch_click(Landroid/view/View;)V
    .registers 8
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 506
    sget v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->CurentSelect:I

    if-eqz v1, :cond_91

    .line 508
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    iget-object v1, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    invoke-static {v1}, Lcom/chelpus/Utils;->kill(Ljava/lang/String;)V

    .line 509
    const-string v1, ""

    sput-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->str:Ljava/lang/String;

    .line 510
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->plipack:Ljava/lang/String;

    invoke-static {v1}, Lcom/chelpus/Utils;->kill(Ljava/lang/String;)V

    .line 512
    new-instance v1, Lcom/chelpus/Utils;

    const-string v2, ""

    invoke-direct {v1, v2}, Lcom/chelpus/Utils;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->dalvikruncommand:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".nerorunpatch "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->plipack:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "object"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->CurentSelect:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/chelpus/Utils;->cmdRoot([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->str:Ljava/lang/String;

    .line 514
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->str:Ljava/lang/String;

    const-string v2, "Done"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_92

    .line 515
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Object N"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->CurentSelect:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const v2, 0x7f07010b

    invoke-static {v2}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 516
    .local v0, "str2":Ljava/lang/String;
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->tvt:Landroid/widget/TextView;

    const-string v2, "#ff00ff73"

    const-string v3, "bold"

    invoke-static {v0, v2, v3}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 526
    .end local v0    # "str2":Ljava/lang/String;
    :cond_91
    :goto_91
    return-void

    .line 520
    :cond_92
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Object N"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->CurentSelect:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const v2, 0x7f07010c

    invoke-static {v2}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 521
    .restart local v0    # "str2":Ljava/lang/String;
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->tvt:Landroid/widget/TextView;

    const-string v2, "#ffff0055"

    const-string v3, "bold"

    invoke-static {v0, v2, v3}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_91
.end method

.method public restore_click(Landroid/view/View;)V
    .registers 10
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 531
    :try_start_0
    const-string v2, ""

    .line 533
    .local v2, "str4":Ljava/lang/String;
    const-string v3, ""

    sput-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->str:Ljava/lang/String;

    .line 534
    new-instance v3, Lcom/chelpus/Utils;

    const-string v4, ""

    invoke-direct {v3, v4}, Lcom/chelpus/Utils;-><init>(Ljava/lang/String;)V

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->dalvikruncommand:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ".restore "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    iget-object v7, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v3, v4}, Lcom/chelpus/Utils;->cmdRoot([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 535
    move-object v1, v2

    .line 536
    .local v1, "str2":Ljava/lang/String;
    sget-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->tvt:Landroid/widget/TextView;

    const-string v4, "#ff00ff73"

    const-string v5, "bold"

    invoke-static {v1, v4, v5}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_42
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_42} :catch_43

    .line 542
    .end local v1    # "str2":Ljava/lang/String;
    .end local v2    # "str4":Ljava/lang/String;
    :goto_42
    return-void

    .line 538
    :catch_43
    move-exception v0

    .line 539
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_42
.end method

.method public saveobject_click(Landroid/view/View;)V
    .registers 10
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v7, 0x1

    .line 474
    :try_start_1
    new-instance v1, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    iget-object v5, v5, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".txt"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 475
    .local v1, "f":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_32

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 476
    :cond_32
    new-instance v2, Ljava/io/FileWriter;

    invoke-direct {v2, v1}, Ljava/io/FileWriter;-><init>(Ljava/io/File;)V

    .line 477
    .local v2, "fw":Ljava/io/FileWriter;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[BEGIN]\ngetActivity() Custom Patch generated by Luckypatcher the manual mode! For Object N"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->CurentSelect:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "...\n[CLASSES]\n{\"object\":\""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->CurentSelect:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\"}\n[ODEX]\n[END]\nApplication patched on object N"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->CurentSelect:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ". Please test...\nIf all works well. Make a \"Dalvik-cache Fix Apply\"."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    .line 478
    invoke-virtual {v2}, Ljava/io/FileWriter;->close()V

    .line 479
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Object N"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->CurentSelect:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const v5, 0x7f0701d3

    invoke-static {v5}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 480
    .local v3, "str2":Ljava/lang/String;
    sget-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->tvt:Landroid/widget/TextView;

    const-string v5, "#ff00ff73"

    const-string v6, "bold"

    invoke-static {v3, v5, v6}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_a3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_a3} :catch_a4
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_a3} :catch_b2

    .line 490
    .end local v1    # "f":Ljava/io/File;
    .end local v2    # "fw":Ljava/io/FileWriter;
    .end local v3    # "str2":Ljava/lang/String;
    :goto_a3
    return-void

    .line 482
    :catch_a4
    move-exception v0

    .line 483
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 484
    const-string v4, "Error while saving file"

    invoke-static {p0, v4, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto :goto_a3

    .line 485
    .end local v0    # "e":Ljava/io/IOException;
    :catch_b2
    move-exception v0

    .line 486
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 487
    const-string v4, "Error while saving file"

    invoke-static {p0, v4, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto :goto_a3
.end method

.method public show_file_explorer(Ljava/lang/String;)V
    .registers 13
    .param p1, "rootDir"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    const/4 v10, 0x0

    const v9, 0x7f0d0043

    .line 1452
    :try_start_5
    sget-object v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->plia:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;

    invoke-virtual {v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->onGroupCollapsedAll()V

    .line 1453
    const/4 v5, 0x0

    sput-object v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_d} :catch_6f

    .line 1457
    :goto_d
    const v5, 0x7f040026

    invoke-static {p0, v5, v6}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 1458
    .local v0, "d":Landroid/view/View;
    new-instance v5, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    const/4 v6, 0x1

    invoke-direct {v5, p0, v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;-><init>(Landroid/content/Context;Z)V

    invoke-virtual {v5, v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setView(Landroid/view/View;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->create()Landroid/app/Dialog;

    move-result-object v4

    .line 1459
    .local v4, "tempdialog":Landroid/app/Dialog;
    invoke-virtual {v4, v10}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 1460
    new-instance v5, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$28;

    invoke-direct {v5, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$28;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;)V

    invoke-virtual {v4, v5}, Landroid/app/Dialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 1482
    invoke-virtual {v4}, Landroid/app/Dialog;->show()V

    .line 1483
    sget-object v6, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    const v5, 0x7f0d0040

    invoke-virtual {v4, v5}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->myPath:Landroid/widget/TextView;

    .line 1485
    sget-object v5, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iput-object p1, v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->root:Ljava/lang/String;

    .line 1487
    invoke-virtual {v0, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ListView;

    .line 1488
    .local v3, "lv":Landroid/widget/ListView;
    new-instance v5, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$29;

    invoke-direct {v5, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$29;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;)V

    invoke-virtual {v3, v5}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1583
    sget-object v6, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v0, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ListView;

    iput-object v5, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->filebrowser:Landroid/widget/ListView;

    .line 1585
    :try_start_5b
    sget-object v6, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    sget-object v5, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iget-object v7, v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->root:Ljava/lang/String;

    const v5, 0x7f0d0043

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ListView;

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v5, v8}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getDir(Ljava/lang/String;Landroid/widget/ListView;Z)V
    :try_end_6e
    .catch Ljava/lang/Exception; {:try_start_5b .. :try_end_6e} :catch_74

    .line 1597
    :goto_6e
    return-void

    .line 1454
    .end local v0    # "d":Landroid/view/View;
    .end local v3    # "lv":Landroid/widget/ListView;
    .end local v4    # "tempdialog":Landroid/app/Dialog;
    :catch_6f
    move-exception v1

    .line 1455
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_d

    .line 1586
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v0    # "d":Landroid/view/View;
    .restart local v3    # "lv":Landroid/widget/ListView;
    .restart local v4    # "tempdialog":Landroid/app/Dialog;
    :catch_74
    move-exception v1

    .line 1588
    .restart local v1    # "e":Ljava/lang/Exception;
    :try_start_75
    sget-object v5, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v6, Ljava/io/File;

    sget-object v7, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->root:Ljava/lang/String;

    .line 1589
    sget-object v6, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    sget-object v5, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iget-object v7, v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->root:Ljava/lang/String;

    const v5, 0x7f0d0043

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ListView;

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v5, v8}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getDir(Ljava/lang/String;Landroid/widget/ListView;Z)V
    :try_end_97
    .catch Ljava/lang/Exception; {:try_start_75 .. :try_end_97} :catch_98

    goto :goto_6e

    .line 1590
    :catch_98
    move-exception v2

    .line 1591
    .local v2, "e1":Ljava/lang/Exception;
    sget-object v5, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    sget-object v6, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    iput-object v6, v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->root:Ljava/lang/String;

    .line 1592
    sget-object v6, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    sget-object v5, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iget-object v7, v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->root:Ljava/lang/String;

    invoke-virtual {v0, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ListView;

    invoke-virtual {v6, v7, v5, v10}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getDir(Ljava/lang/String;Landroid/widget/ListView;Z)V

    goto :goto_6e
.end method

.method public toolbar_backups_click(Landroid/view/View;)V
    .registers 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1146
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/Backup/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    .line 1147
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    const/16 v1, 0xb

    invoke-static {v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->showDialogLP(I)V

    .line 1148
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->progress2:Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2;->setCancelable(Z)V

    .line 1149
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->progress2:Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2;

    const v2, 0x7f070233

    invoke-static {v2}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_2;->setMessage(Ljava/lang/String;)V

    .line 1151
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$25;

    invoke-direct {v1, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$25;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 1287
    .local v0, "t":Ljava/lang/Thread;
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setPriority(I)V

    .line 1288
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 1290
    return-void
.end method

.method public toolbar_market_install_click(Landroid/view/View;)V
    .registers 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/16 v1, 0x1e

    .line 581
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-static {v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->removeDialogLP(I)V

    .line 582
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-static {v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->showDialogLP(I)V

    .line 584
    return-void
.end method

.method public toolbar_menu_click(Landroid/view/View;)V
    .registers 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 575
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->openOptionsMenu()V

    .line 577
    return-void
.end method

.method public toolbar_rebuild_click(Landroid/view/View;)V
    .registers 14
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v7, 0x0

    const/4 v11, 0x1

    const v10, 0x7f0d0043

    .line 1295
    :try_start_5
    sget-object v6, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->plia:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;

    invoke-virtual {v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->onGroupCollapsedAll()V

    .line 1296
    const/4 v6, 0x0

    sput-object v6, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_d} :catch_b4

    .line 1300
    :goto_d
    const v6, 0x7f040026

    invoke-static {p0, v6, v7}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 1301
    .local v0, "d":Landroid/view/View;
    new-instance v6, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    invoke-direct {v6, p0, v11}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;-><init>(Landroid/content/Context;Z)V

    invoke-virtual {v6, v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setView(Landroid/view/View;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->create()Landroid/app/Dialog;

    move-result-object v5

    .line 1302
    .local v5, "tempdialog":Landroid/app/Dialog;
    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 1303
    new-instance v6, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$26;

    invoke-direct {v6, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$26;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;)V

    invoke-virtual {v5, v6}, Landroid/app/Dialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 1326
    invoke-virtual {v5}, Landroid/app/Dialog;->show()V

    .line 1327
    sget-object v7, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    const v6, 0x7f0d0040

    invoke-virtual {v5, v6}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->myPath:Landroid/widget/TextView;

    .line 1328
    new-instance v6, Ljava/io/File;

    sget-object v7, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->extStorageDirectory:Ljava/lang/String;

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v4

    .line 1330
    .local v4, "storages_path":Ljava/lang/String;
    :goto_4a
    :try_start_4a
    new-instance v6, Ljava/io/File;

    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_84

    .line 1331
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v4

    .line 1332
    sget-object v6, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Parent directory:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_7f
    .catch Ljava/lang/Exception; {:try_start_4a .. :try_end_7f} :catch_80

    goto :goto_4a

    .line 1334
    :catch_80
    move-exception v1

    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 1335
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_84
    sget-object v6, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iput-object v4, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->root:Ljava/lang/String;

    .line 1337
    invoke-virtual {v0, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ListView;

    .line 1338
    .local v3, "lv":Landroid/widget/ListView;
    new-instance v6, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$27;

    invoke-direct {v6, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$27;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;)V

    invoke-virtual {v3, v6}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1433
    sget-object v7, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v0, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ListView;

    iput-object v6, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->filebrowser:Landroid/widget/ListView;

    .line 1435
    :try_start_a0
    sget-object v7, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    sget-object v6, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iget-object v8, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->root:Ljava/lang/String;

    const v6, 0x7f0d0043

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ListView;

    const/4 v9, 0x1

    invoke-virtual {v7, v8, v6, v9}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getDir(Ljava/lang/String;Landroid/widget/ListView;Z)V
    :try_end_b3
    .catch Ljava/lang/Exception; {:try_start_a0 .. :try_end_b3} :catch_ba

    .line 1447
    :goto_b3
    return-void

    .line 1297
    .end local v0    # "d":Landroid/view/View;
    .end local v3    # "lv":Landroid/widget/ListView;
    .end local v4    # "storages_path":Ljava/lang/String;
    .end local v5    # "tempdialog":Landroid/app/Dialog;
    :catch_b4
    move-exception v1

    .line 1298
    .restart local v1    # "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_d

    .line 1436
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v0    # "d":Landroid/view/View;
    .restart local v3    # "lv":Landroid/widget/ListView;
    .restart local v4    # "storages_path":Ljava/lang/String;
    .restart local v5    # "tempdialog":Landroid/app/Dialog;
    :catch_ba
    move-exception v1

    .line 1438
    .restart local v1    # "e":Ljava/lang/Exception;
    :try_start_bb
    sget-object v6, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v7, Ljava/io/File;

    sget-object v8, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->root:Ljava/lang/String;

    .line 1439
    sget-object v7, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    sget-object v6, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iget-object v8, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->root:Ljava/lang/String;

    const v6, 0x7f0d0043

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ListView;

    const/4 v9, 0x1

    invoke-virtual {v7, v8, v6, v9}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getDir(Ljava/lang/String;Landroid/widget/ListView;Z)V
    :try_end_dd
    .catch Ljava/lang/Exception; {:try_start_bb .. :try_end_dd} :catch_de

    goto :goto_b3

    .line 1440
    :catch_de
    move-exception v2

    .line 1441
    .local v2, "e1":Ljava/lang/Exception;
    sget-object v6, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    sget-object v7, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    iput-object v7, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->root:Ljava/lang/String;

    .line 1442
    sget-object v7, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    sget-object v6, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iget-object v8, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->root:Ljava/lang/String;

    invoke-virtual {v0, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ListView;

    invoke-virtual {v7, v8, v6, v11}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getDir(Ljava/lang/String;Landroid/widget/ListView;Z)V

    goto :goto_b3
.end method

.method public toolbar_refresh_click(Landroid/view/View;)V
    .registers 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1124
    :try_start_0
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 1125
    .local v1, "ft":Landroid/support/v4/app/FragmentTransaction;
    sget-object v2, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    sget-object v2, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->filter:Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/FilterFragment;

    if-nez v2, :cond_25

    .line 1126
    sget-object v2, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v2, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/FilterFragment;

    invoke-direct {v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/FilterFragment;-><init>()V

    sput-object v2, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->filter:Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/FilterFragment;

    .line 1127
    const v2, 0x7f0d00d0

    sget-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    sget-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->filter:Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/FilterFragment;

    invoke-virtual {v1, v2, v3}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 1129
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 1139
    .end local v1    # "ft":Landroid/support/v4/app/FragmentTransaction;
    :goto_24
    return-void

    .line 1131
    .restart local v1    # "ft":Landroid/support/v4/app/FragmentTransaction;
    :cond_25
    sget-object v2, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    sget-object v2, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->filter:Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/FilterFragment;

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 1132
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 1133
    sget-object v2, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    sget-object v2, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->filter:Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/FilterFragment;

    invoke-virtual {v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/FilterFragment;->onDestroy()V

    .line 1134
    sget-object v2, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    const/4 v2, 0x0

    sput-object v2, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->filter:Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/FilterFragment;

    .line 1135
    sget-object v2, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    sget-object v2, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->plia:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;

    invoke-virtual {v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->getFilter()Landroid/widget/Filter;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/Filter;->filter(Ljava/lang/CharSequence;)V

    .line 1136
    sget-object v2, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    sget-object v2, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->plia:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;

    invoke-virtual {v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->notifyDataSetChanged()V
    :try_end_4f
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_4f} :catch_50

    goto :goto_24

    .line 1138
    .end local v1    # "ft":Landroid/support/v4/app/FragmentTransaction;
    :catch_50
    move-exception v0

    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_24
.end method

.method public toolbar_settings_click()V
    .registers 12

    .prologue
    .line 972
    sget-boolean v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->menu_open:Z

    if-eqz v0, :cond_a

    .line 973
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->hideMenu()V

    .line 1118
    :cond_9
    :goto_9
    return-void

    .line 977
    :cond_a
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 978
    .local v10, "s":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;>;"
    new-instance v0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    const v1, 0x7f07001b

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;-><init>(ILjava/util/List;Z)V

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 979
    new-instance v0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    const v1, 0x7f07022f

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const/4 v3, 0x1

    const/4 v4, 0x1

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;-><init>(ILjava/util/List;IZ)V

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 980
    new-instance v0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    const v1, 0x7f070216

    new-instance v2, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$11;

    invoke-direct {v2, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$11;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;)V

    const/4 v3, 0x2

    const/4 v4, 0x1

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;-><init>(ILjava/util/List;IZ)V

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 984
    new-instance v0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    const v1, 0x7f070196

    new-instance v2, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$12;

    invoke-direct {v2, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$12;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;)V

    const/4 v3, 0x2

    const/4 v4, 0x1

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;-><init>(ILjava/util/List;IZ)V

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 988
    new-instance v0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    const v1, 0x7f0701ed

    new-instance v2, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$13;

    invoke-direct {v2, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$13;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;)V

    const/4 v3, 0x2

    const/4 v4, 0x1

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;-><init>(ILjava/util/List;IZ)V

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 992
    new-instance v0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    const v1, 0x7f070110

    const v2, 0x7f07011f

    new-instance v3, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$14;

    invoke-direct {v3, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$14;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;)V

    const/4 v4, 0x2

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;-><init>(IILjava/util/List;IZ)V

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 995
    new-instance v0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    const v1, 0x7f070197

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const/4 v3, 0x1

    const/4 v4, 0x1

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;-><init>(ILjava/util/List;IZ)V

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 996
    new-instance v0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    const v1, 0x7f070145

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const/4 v3, 0x2

    const/4 v4, 0x1

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;-><init>(ILjava/util/List;IZ)V

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 997
    new-instance v0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    const v1, 0x7f0701e2

    const v2, 0x7f0701e3

    new-instance v3, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$15;

    invoke-direct {v3, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$15;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;)V

    const/4 v4, 0x2

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;-><init>(IILjava/util/List;IZ)V

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1000
    new-instance v0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    const v1, 0x7f0700e3

    const v2, 0x7f0700e4

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    const/4 v4, 0x2

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;-><init>(IILjava/util/List;IZ)V

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1001
    new-instance v0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    const v1, 0x7f0700dc

    const v2, 0x7f0700dd

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    const/4 v4, 0x2

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;-><init>(IILjava/util/List;IZ)V

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1003
    new-instance v0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    const v1, 0x7f0701df

    const v2, 0x7f0701e0

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    const/4 v4, 0x3

    const/4 v5, 0x1

    const-string v6, "confirm_exit"

    const/4 v7, 0x1

    new-instance v8, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$16;

    invoke-direct {v8, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$16;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;)V

    invoke-direct/range {v0 .. v8}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;-><init>(IILjava/util/List;IZLjava/lang/String;ZLjava/lang/Runnable;)V

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1017
    new-instance v0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    const v1, 0x7f07010d

    const v2, 0x7f07010e

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    const/4 v4, 0x3

    const/4 v5, 0x1

    const-string v6, "fast_start"

    const/4 v7, 0x0

    new-instance v8, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$17;

    invoke-direct {v8, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$17;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;)V

    invoke-direct/range {v0 .. v8}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;-><init>(IILjava/util/List;IZLjava/lang/String;ZLjava/lang/Runnable;)V

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1031
    new-instance v0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    const v1, 0x7f07017d

    const v2, 0x7f07017e

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    const/4 v4, 0x3

    const/4 v5, 0x1

    const-string v6, "no_icon"

    const/4 v7, 0x0

    new-instance v8, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$18;

    invoke-direct {v8, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$18;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;)V

    invoke-direct/range {v0 .. v8}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;-><init>(IILjava/util/List;IZLjava/lang/String;ZLjava/lang/Runnable;)V

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1043
    new-instance v0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    const v1, 0x7f070010

    const v2, 0x7f070013

    new-instance v3, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$19;

    invoke-direct {v3, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$19;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;)V

    const/4 v4, 0x2

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;-><init>(IILjava/util/List;IZ)V

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1046
    new-instance v0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    const v1, 0x7f070041

    const v2, 0x7f070042

    new-instance v3, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$20;

    invoke-direct {v3, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$20;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;)V

    const/4 v4, 0x2

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;-><init>(IILjava/util/List;IZ)V

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1050
    new-instance v0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    const v1, 0x7f07012e

    const v2, 0x7f07012f

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    const/4 v4, 0x3

    const/4 v5, 0x1

    const-string v6, "hide_notify"

    const/4 v7, 0x0

    new-instance v8, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$21;

    invoke-direct {v8, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$21;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;)V

    invoke-direct/range {v0 .. v8}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;-><init>(IILjava/util/List;IZLjava/lang/String;ZLjava/lang/Runnable;)V

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1064
    new-instance v0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    const v1, 0x7f070130

    const v2, 0x7f070131

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    const/4 v4, 0x3

    const/4 v5, 0x1

    const-string v6, "hide_title"

    const/4 v7, 0x0

    new-instance v8, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$22;

    invoke-direct {v8, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$22;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;)V

    invoke-direct/range {v0 .. v8}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;-><init>(IILjava/util/List;IZLjava/lang/String;ZLjava/lang/Runnable;)V

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1078
    new-instance v0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    const v1, 0x7f0700e7

    const v2, 0x7f0700e8

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    const/4 v4, 0x3

    const/4 v5, 0x1

    const-string v6, "disable_autoupdate"

    const/4 v7, 0x0

    new-instance v8, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$23;

    invoke-direct {v8, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$23;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;)V

    invoke-direct/range {v0 .. v8}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;-><init>(IILjava/util/List;IZLjava/lang/String;ZLjava/lang/Runnable;)V

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1092
    new-instance v0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    const v1, 0x7f07022a

    const v2, 0x7f07022b

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    const/4 v4, 0x3

    const/4 v5, 0x1

    const-string v6, "vibration"

    const/4 v7, 0x0

    new-instance v8, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$24;

    invoke-direct {v8, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$24;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;)V

    invoke-direct/range {v0 .. v8}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;-><init>(IILjava/util/List;IZLjava/lang/String;ZLjava/lang/Runnable;)V

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1104
    new-instance v0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    const v1, 0x7f07012b

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const/4 v3, 0x2

    const/4 v4, 0x1

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;-><init>(ILjava/util/List;IZ)V

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1105
    new-instance v0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    const v1, 0x7f070226

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const/4 v3, 0x2

    const/4 v4, 0x1

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;-><init>(ILjava/util/List;IZ)V

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1106
    new-instance v0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    const v1, 0x7f0701d9

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const/4 v3, 0x2

    const/4 v4, 0x1

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;-><init>(ILjava/util/List;IZ)V

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1107
    new-instance v0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    const v1, 0x7f070006

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const/4 v3, 0x2

    const/4 v4, 0x1

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;-><init>(ILjava/util/List;IZ)V

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1110
    :try_start_21d
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->menu_adapter:Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;

    if-eqz v0, :cond_9

    .line 1111
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->menu_adapter:Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;

    invoke-virtual {v0, v10}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->add(Ljava/util/ArrayList;)V

    .line 1112
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->showMenu()V
    :try_end_22b
    .catch Ljava/lang/Exception; {:try_start_21d .. :try_end_22b} :catch_22d

    goto/16 :goto_9

    .line 1114
    :catch_22d
    move-exception v9

    .line 1115
    .local v9, "e":Ljava/lang/Exception;
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_9
.end method

.method public toolbar_switchers_click(Landroid/view/View;)V
    .registers 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 587
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$6;

    invoke-direct {v1, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$6;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 881
    .local v0, "t":Ljava/lang/Thread;
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setPriority(I)V

    .line 882
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 883
    return-void
.end method

.method public toolbar_system_utils_click(Landroid/view/View;)V
    .registers 11
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const v8, 0x7f07021c

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 885
    sget-boolean v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->menu_open:Z

    if-eqz v3, :cond_f

    .line 886
    sget-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v3}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->hideMenu()V

    .line 970
    :cond_e
    :goto_e
    return-void

    .line 890
    :cond_f
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 891
    .local v1, "s":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;>;"
    new-instance v3, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    const v4, 0x7f07001b

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v3, v4, v5, v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;-><init>(ILjava/util/List;Z)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 892
    sget-boolean v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->su:Z

    if-eqz v3, :cond_6c

    .line 894
    new-instance v3, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    const v4, 0x7f07003b

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v3, v4, v5, v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;-><init>(ILjava/util/List;Z)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 896
    new-instance v3, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    const v4, 0x7f07004b

    new-instance v5, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$7;

    invoke-direct {v5, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$7;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;)V

    invoke-direct {v3, v4, v5, v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;-><init>(ILjava/util/List;Z)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 904
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v3

    new-instance v4, Landroid/content/Intent;

    const-string v5, "com.google.android.gms.ads.identifier.service.START"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4, v7}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v2

    .line 905
    .local v2, "serv":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    if-eqz v2, :cond_154

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_154

    .line 906
    new-instance v3, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    new-instance v4, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$8;

    invoke-direct {v4, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$8;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;)V

    invoke-direct {v3, v8, v4, v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;-><init>(ILjava/util/List;Z)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 927
    .end local v2    # "serv":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    :cond_6c
    :goto_6c
    new-instance v3, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    const v4, 0x7f0701c4

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v3, v4, v5, v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;-><init>(ILjava/util/List;Z)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 928
    sget-boolean v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->su:Z

    if-eqz v3, :cond_110

    .line 930
    new-instance v3, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    const v4, 0x7f0700e5

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v3, v4, v5, v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;-><init>(ILjava/util/List;Z)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 932
    new-instance v3, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    const v4, 0x7f0700bf

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v3, v4, v5, v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;-><init>(ILjava/util/List;Z)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 934
    invoke-static {}, Lcom/chelpus/Utils;->isXposedEnabled()Z

    move-result v3

    if-eqz v3, :cond_b6

    new-instance v3, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    const v4, 0x7f070239

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v3, v4, v5, v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;-><init>(ILjava/util/List;Z)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 936
    :cond_b6
    new-instance v3, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    const v4, 0x7f07016a

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v3, v4, v5, v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;-><init>(ILjava/util/List;Z)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 939
    :try_start_c6
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string v4, "com.android.vending"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_d0
    .catch Ljava/lang/Exception; {:try_start_c6 .. :try_end_d0} :catch_163

    .line 943
    :cond_d0
    :goto_d0
    new-instance v3, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    const v4, 0x7f070150

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v3, v4, v5, v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;-><init>(ILjava/util/List;Z)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 945
    new-instance v3, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    const v4, 0x7f070190

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v3, v4, v5, v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;-><init>(ILjava/util/List;Z)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 947
    new-instance v3, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    const v4, 0x7f0701c6

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v3, v4, v5, v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;-><init>(ILjava/util/List;Z)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 949
    new-instance v3, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    const v4, 0x7f070044

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v3, v4, v5, v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;-><init>(ILjava/util/List;Z)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 951
    :cond_110
    sget v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->api:I

    const/16 v4, 0x13

    if-lt v3, v4, :cond_11a

    sget-boolean v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->su:Z

    if-eqz v3, :cond_12a

    .line 952
    :cond_11a
    new-instance v3, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    const v4, 0x7f0701da

    new-instance v5, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$10;

    invoke-direct {v5, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$10;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;)V

    invoke-direct {v3, v4, v5, v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;-><init>(ILjava/util/List;Z)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 957
    :cond_12a
    sget-boolean v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->su:Z

    if-eqz v3, :cond_13e

    .line 958
    new-instance v3, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    const v4, 0x7f0701be

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v3, v4, v5, v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;-><init>(ILjava/util/List;Z)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 962
    :cond_13e
    :try_start_13e
    sget-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->menu_adapter:Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;

    if-eqz v3, :cond_e

    .line 963
    sget-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->menu_adapter:Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;

    invoke-virtual {v3, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->add(Ljava/util/ArrayList;)V

    .line 964
    sget-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v3}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->showMenu()V
    :try_end_14c
    .catch Ljava/lang/Exception; {:try_start_13e .. :try_end_14c} :catch_14e

    goto/16 :goto_e

    .line 966
    :catch_14e
    move-exception v0

    .line 967
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_e

    .line 914
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v2    # "serv":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    :cond_154
    new-instance v3, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    new-instance v4, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$9;

    invoke-direct {v4, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$9;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;)V

    invoke-direct {v3, v8, v4, v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;-><init>(ILjava/util/List;Z)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_6c

    .line 940
    .end local v2    # "serv":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    :catch_163
    move-exception v0

    .line 941
    .restart local v0    # "e":Ljava/lang/Exception;
    sget-boolean v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->su:Z

    if-eqz v3, :cond_d0

    new-instance v3, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    const v4, 0x7f070263

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v3, v4, v5, v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;-><init>(ILjava/util/List;Z)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_d0
.end method
