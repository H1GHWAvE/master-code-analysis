.class final Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$byPkgStatus;
.super Ljava/lang/Object;
.source "listAppsFragment.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "byPkgStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;)V
    .registers 2
    .param p1, "this$0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    .prologue
    .line 6807
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$byPkgStatus;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;)I
    .registers 9
    .param p1, "a"    # Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    .param p2, "b"    # Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    .prologue
    .line 6809
    if-eqz p1, :cond_4

    if-nez p2, :cond_a

    .line 6810
    :cond_4
    new-instance v4, Ljava/lang/ClassCastException;

    invoke-direct {v4}, Ljava/lang/ClassCastException;-><init>()V

    throw v4

    .line 6813
    :cond_a
    iget v4, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->stored:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iget v5, p2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->stored:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/Integer;->compareTo(Ljava/lang/Integer;)I

    move-result v1

    .line 6814
    .local v1, "ret":I
    if-eqz v1, :cond_1d

    .line 6837
    .end local v1    # "ret":I
    :goto_1c
    return v1

    .line 6817
    .restart local v1    # "ret":I
    :cond_1d
    iget v4, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->stored:I

    if-nez v4, :cond_62

    .line 6818
    const/16 v2, 0xff

    .line 6819
    .local v2, "statA":I
    const/16 v3, 0xff

    .line 6820
    .local v3, "statB":I
    iget-boolean v4, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->ads:Z

    if-nez v4, :cond_2e

    iget-boolean v4, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->lvl:Z

    if-nez v4, :cond_2e

    const/4 v2, 0x4

    .line 6821
    :cond_2e
    iget-boolean v4, p2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->ads:Z

    if-nez v4, :cond_37

    iget-boolean v4, p2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->lvl:Z

    if-nez v4, :cond_37

    const/4 v3, 0x4

    .line 6822
    :cond_37
    iget-boolean v4, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->ads:Z

    if-eqz v4, :cond_3c

    const/4 v2, 0x3

    .line 6823
    :cond_3c
    iget-boolean v4, p2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->ads:Z

    if-eqz v4, :cond_41

    const/4 v3, 0x3

    .line 6824
    :cond_41
    iget-boolean v4, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->lvl:Z

    if-eqz v4, :cond_46

    const/4 v2, 0x2

    .line 6825
    :cond_46
    iget-boolean v4, p2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->lvl:Z

    if-eqz v4, :cond_4b

    const/4 v3, 0x2

    .line 6826
    :cond_4b
    iget-boolean v4, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->custom:Z

    if-eqz v4, :cond_50

    const/4 v2, 0x1

    .line 6827
    :cond_50
    iget-boolean v4, p2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->custom:Z

    if-eqz v4, :cond_55

    const/4 v3, 0x1

    .line 6828
    :cond_55
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/Integer;->compareTo(Ljava/lang/Integer;)I

    move-result v1

    goto :goto_1c

    .line 6834
    .end local v2    # "statA":I
    .end local v3    # "statB":I
    :cond_62
    :try_start_62
    invoke-virtual {p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I
    :try_end_6d
    .catch Ljava/lang/NullPointerException; {:try_start_62 .. :try_end_6d} :catch_6f

    move-result v1

    goto :goto_1c

    .line 6835
    :catch_6f
    move-exception v0

    .line 6836
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 6837
    const/4 v1, 0x0

    goto :goto_1c
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 4

    .prologue
    .line 6807
    check-cast p1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    check-cast p2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    invoke-virtual {p0, p1, p2}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$byPkgStatus;->compare(Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;)I

    move-result v0

    return v0
.end method
