.class Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$38;
.super Ljava/lang/Object;
.source "listAppsFragment.java"

# interfaces
.implements Landroid/widget/ExpandableListView$OnGroupClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->populateAdapter(Ljava/util/ArrayList;Ljava/util/Comparator;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;)V
    .registers 2
    .param p1, "this$0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    .prologue
    .line 6137
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$38;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGroupClick(Landroid/widget/ExpandableListView;Landroid/view/View;IJ)Z
    .registers 11
    .param p1, "parent"    # Landroid/widget/ExpandableListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "groupPosition"    # I
    .param p4, "id"    # J

    .prologue
    const/4 v2, 0x1

    .line 6140
    sget-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->menu_adapter:Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;

    invoke-virtual {v3, p3}, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItemAdapter;->getGroup(I)Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;

    move-result-object v1

    .line 6141
    .local v1, "item":Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;
    iget-object v3, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->childs:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-eqz v3, :cond_13

    iget v3, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->type:I

    if-ne v3, v2, :cond_65

    .line 6142
    :cond_13
    iget v3, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->type:I

    packed-switch v3, :pswitch_data_68

    .line 6177
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$38;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iget v4, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->punkt_menu:I

    invoke-virtual {v3, v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runId(I)V

    .line 6185
    :cond_1f
    :goto_1f
    :pswitch_1f
    return v2

    .line 6146
    :pswitch_20
    iget v3, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->punkt_menu:I

    sparse-switch v3, :sswitch_data_72

    goto :goto_1f

    .line 6166
    :sswitch_26
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$38;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v3}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->showAbout()V

    goto :goto_1f

    .line 6148
    :sswitch_2c
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$38;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v3}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->selectLanguage()V

    goto :goto_1f

    .line 6151
    :sswitch_32
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$38;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v3}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->changeDefaultDir()V

    goto :goto_1f

    .line 6154
    :sswitch_38
    new-instance v0, Landroid/content/Intent;

    sget-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patchAct:Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;

    const-class v4, Lcom/android/vending/billing/InAppBillingService/LUCK/HelpActivity;

    invoke-direct {v0, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 6155
    .local v0, "intent":Landroid/content/Intent;
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$38;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v3, v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_1f

    .line 6158
    .end local v0    # "intent":Landroid/content/Intent;
    :sswitch_47
    sget-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    if-eqz v3, :cond_1f

    .line 6159
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$38;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v3}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runUpdate()V

    goto :goto_1f

    .line 6163
    :sswitch_51
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$38;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v3}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->sendLog()V

    goto :goto_1f

    .line 6169
    :sswitch_57
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$38;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v3}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->changeDayOnUp()V

    goto :goto_1f

    .line 6174
    :pswitch_5d
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$38;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iget-object v4, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->runCode:Ljava/lang/Runnable;

    invoke-virtual {v3, v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V

    goto :goto_1f

    .line 6185
    :cond_65
    const/4 v2, 0x0

    goto :goto_1f

    .line 6142
    nop

    :pswitch_data_68
    .packed-switch 0x1
        :pswitch_1f
        :pswitch_20
        :pswitch_5d
    .end packed-switch

    .line 6146
    :sswitch_data_72
    .sparse-switch
        0x7f070006 -> :sswitch_26
        0x7f0700dc -> :sswitch_57
        0x7f0700e3 -> :sswitch_32
        0x7f07012b -> :sswitch_38
        0x7f070145 -> :sswitch_2c
        0x7f0701d9 -> :sswitch_51
        0x7f070226 -> :sswitch_47
    .end sparse-switch
.end method
