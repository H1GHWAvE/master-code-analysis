.class public Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Patch_Dialog;
.super Ljava/lang/Object;
.source "Patch_Dialog.java"


# instance fields
.field dialog:Landroid/app/Dialog;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Patch_Dialog;->dialog:Landroid/app/Dialog;

    .line 26
    return-void
.end method


# virtual methods
.method public dismiss()V
    .registers 2

    .prologue
    .line 92
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Patch_Dialog;->dialog:Landroid/app/Dialog;

    if-eqz v0, :cond_c

    .line 93
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Patch_Dialog;->dialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 94
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Patch_Dialog;->dialog:Landroid/app/Dialog;

    .line 96
    :cond_c
    return-void
.end method

.method public onCreateDialog()Landroid/app/Dialog;
    .registers 9

    .prologue
    const/4 v7, 0x0

    .line 39
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v6, "Patch Dialog create."

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 40
    sget-object v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    if-eqz v5, :cond_14

    sget-object v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    if-nez v5, :cond_17

    :cond_14
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Patch_Dialog;->dismiss()V

    .line 41
    :cond_17
    sget-object v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    const v6, 0x7f040031

    invoke-static {v5, v6, v7}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 43
    .local v2, "d":Landroid/widget/LinearLayout;
    const v5, 0x7f0d00c4

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    const v6, 0x7f0d00c5

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 44
    .local v0, "body":Landroid/widget/LinearLayout;
    const v5, 0x7f0d00c8

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 46
    .local v4, "tv":Landroid/widget/TextView;
    :try_start_3f
    sget-object v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    const/4 v5, 0x0

    invoke-static {v4, v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patch_dialog_text_builder(Landroid/widget/TextView;Z)V
    :try_end_45
    .catch Ljava/lang/Exception; {:try_start_3f .. :try_end_45} :catch_85

    .line 55
    :goto_45
    new-instance v1, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    sget-object v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-direct {v1, v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;-><init>(Landroid/content/Context;)V

    .line 57
    .local v1, "builder":Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    const v5, 0x7f02002c

    invoke-virtual {v1, v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setIcon(I)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    .line 58
    const v5, 0x7f070002

    invoke-static {v5}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setTitle(Ljava/lang/String;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    .line 59
    const/4 v5, 0x1

    invoke-virtual {v1, v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setCancelable(Z)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    move-result-object v5

    const v6, 0x104000a

    .line 60
    invoke-static {v6}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v6

    .line 59
    invoke-virtual {v5, v6, v7}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setPositiveButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    move-result-object v5

    const v6, 0x7f070146

    new-instance v7, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Patch_Dialog$1;

    invoke-direct {v7, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Patch_Dialog$1;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Patch_Dialog;)V

    .line 60
    invoke-virtual {v5, v6, v7}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    move-result-object v5

    .line 84
    invoke-virtual {v5, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setView(Landroid/view/View;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    move-result-object v5

    .line 88
    invoke-virtual {v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->create()Landroid/app/Dialog;

    move-result-object v5

    return-object v5

    .line 47
    .end local v1    # "builder":Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    :catch_85
    move-exception v3

    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_45
.end method

.method public showDialog()V
    .registers 2

    .prologue
    .line 29
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Patch_Dialog;->dialog:Landroid/app/Dialog;

    if-nez v0, :cond_a

    .line 30
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Patch_Dialog;->onCreateDialog()Landroid/app/Dialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Patch_Dialog;->dialog:Landroid/app/Dialog;

    .line 32
    :cond_a
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Patch_Dialog;->dialog:Landroid/app/Dialog;

    if-eqz v0, :cond_13

    .line 33
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Patch_Dialog;->dialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 35
    :cond_13
    return-void
.end method
