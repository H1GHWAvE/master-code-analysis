.class Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$24;
.super Ljava/lang/Object;
.source "All_Dialogs.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;->onCreateDialog()Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;)V
    .registers 2
    .param p1, "this$0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;

    .prologue
    .line 790
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$24;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .registers 10
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v6, 0x3

    .line 795
    const-string v3, "1"

    .line 796
    .local v3, "result":Ljava/lang/String;
    sget-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patch_adapt:Landroid/widget/ArrayAdapter;

    invoke-virtual {v4}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v0

    .line 797
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_a
    if-ge v1, v0, :cond_9a

    .line 798
    sget-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patch_adapt:Landroid/widget/ArrayAdapter;

    invoke-virtual {v4, v1}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/vending/billing/InAppBillingService/LUCK/Patterns;

    .line 799
    .local v2, "patt":Lcom/android/vending/billing/InAppBillingService/LUCK/Patterns;
    iget-boolean v4, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/Patterns;->Status:Z

    if-eqz v4, :cond_37

    if-ge v1, v6, :cond_37

    .line 800
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "pattern"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 802
    :cond_37
    if-ne v1, v6, :cond_56

    iget-boolean v4, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/Patterns;->Status:Z

    if-eqz v4, :cond_56

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "copyDC"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 803
    :cond_56
    const/4 v4, 0x4

    if-ne v1, v4, :cond_76

    iget-boolean v4, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/Patterns;->Status:Z

    if-eqz v4, :cond_76

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "backup"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 804
    :cond_76
    const/4 v4, 0x5

    if-ne v1, v4, :cond_96

    iget-boolean v4, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/Patterns;->Status:Z

    if-eqz v4, :cond_96

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "deleteDC"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 797
    :cond_96
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_a

    .line 808
    .end local v2    # "patt":Lcom/android/vending/billing/InAppBillingService/LUCK/Patterns;
    :cond_9a
    sget-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    sget-object v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    invoke-virtual {v4, v5, v3}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->support(Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;Ljava/lang/String;)V

    .line 810
    return-void
.end method
