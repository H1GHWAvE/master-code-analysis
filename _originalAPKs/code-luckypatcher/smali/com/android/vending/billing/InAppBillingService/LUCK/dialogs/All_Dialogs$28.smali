.class Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$28;
.super Ljava/lang/Object;
.source "All_Dialogs.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;->onCreateDialog()Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;)V
    .registers 2
    .param p1, "this$0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;

    .prologue
    .line 916
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$28;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .registers 9
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 921
    const-string v3, "1"

    .line 922
    .local v3, "result":Ljava/lang/String;
    sget-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patch_adapt:Landroid/widget/ArrayAdapter;

    invoke-virtual {v4}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v0

    .line 923
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_9
    if-ge v1, v0, :cond_a8

    .line 924
    sget-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patch_adapt:Landroid/widget/ArrayAdapter;

    invoke-virtual {v4, v1}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/vending/billing/InAppBillingService/LUCK/Patterns;

    .line 925
    .local v2, "patt":Lcom/android/vending/billing/InAppBillingService/LUCK/Patterns;
    if-nez v1, :cond_1b

    iget-boolean v4, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/Patterns;->Status:Z

    if-eqz v4, :cond_1b

    .line 926
    const-string v3, "pattern1_pattern2_pattern3_pattern5_"

    .line 927
    :cond_1b
    const/4 v4, 0x1

    if-ne v1, v4, :cond_24

    iget-boolean v4, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/Patterns;->Status:Z

    if-eqz v4, :cond_24

    .line 928
    const-string v3, "pattern1_pattern2_pattern3_pattern6_"

    .line 929
    :cond_24
    const/4 v4, 0x2

    if-ne v1, v4, :cond_44

    iget-boolean v4, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/Patterns;->Status:Z

    if-eqz v4, :cond_44

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "pattern4"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 930
    :cond_44
    const/4 v4, 0x3

    if-ne v1, v4, :cond_64

    iget-boolean v4, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/Patterns;->Status:Z

    if-eqz v4, :cond_64

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "amazon"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 931
    :cond_64
    const/4 v4, 0x4

    if-ne v1, v4, :cond_84

    iget-boolean v4, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/Patterns;->Status:Z

    if-eqz v4, :cond_84

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "samsung"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 932
    :cond_84
    const/4 v4, 0x5

    if-ne v1, v4, :cond_a4

    iget-boolean v4, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/Patterns;->Status:Z

    if-eqz v4, :cond_a4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "dependencies"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 923
    :cond_a4
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_9

    .line 938
    .end local v2    # "patt":Lcom/android/vending/billing/InAppBillingService/LUCK/Patterns;
    :cond_a8
    sget-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->rebuldApk:Ljava/lang/String;

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b8

    .line 939
    sget-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v4, v3}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->createapklvl(Ljava/lang/String;)V

    .line 943
    :goto_b7
    return-void

    .line 941
    :cond_b8
    sget-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v4, v3}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->toolbar_createapklvl(Ljava/lang/String;)V

    goto :goto_b7
.end method
