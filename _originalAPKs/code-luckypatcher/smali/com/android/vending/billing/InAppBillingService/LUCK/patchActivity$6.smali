.class Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$6;
.super Ljava/lang/Object;
.source "patchActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->toolbar_switchers_click(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;)V
    .registers 2
    .param p1, "this$0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;

    .prologue
    .line 587
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$6;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 13

    .prologue
    const v11, 0x7f070261

    const/4 v10, 0x0

    .line 591
    const/4 v3, 0x0

    .line 593
    .local v3, "info":Landroid/content/pm/PackageInfo;
    :try_start_5
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v7

    const-string v8, "com.android.vending"

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_f
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_5 .. :try_end_f} :catch_ca
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_f} :catch_d0

    move-result-object v3

    .line 599
    :goto_10
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 600
    .local v4, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const v7, 0x7f070147

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 601
    sget-boolean v7, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->su:Z

    if-eqz v7, :cond_2f

    if-eqz v3, :cond_2f

    const v7, 0x7f0700eb

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 602
    :cond_2f
    const v7, 0x7f070028

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 603
    sget-boolean v7, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->su:Z

    if-eqz v7, :cond_77

    if-eqz v3, :cond_77

    .line 604
    const v7, 0x7f0700e9

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 606
    invoke-static {}, Lcom/chelpus/Utils;->checkCoreJarPatch11()Z

    move-result v7

    if-eqz v7, :cond_77

    invoke-static {}, Lcom/chelpus/Utils;->checkCoreJarPatch12()Z

    move-result v7

    if-eqz v7, :cond_77

    .line 607
    invoke-static {}, Lcom/chelpus/Utils;->isXposedEnabled()Z

    move-result v7

    if-eqz v7, :cond_db

    .line 608
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 609
    .local v5, "s":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;>;"
    const/4 v6, 0x0

    .line 611
    .local v6, "settings":Lorg/json/JSONObject;
    :try_start_61
    invoke-static {}, Lcom/chelpus/Utils;->readXposedParamBoolean()Lorg/json/JSONObject;
    :try_end_64
    .catch Lorg/json/JSONException; {:try_start_61 .. :try_end_64} :catch_d6

    move-result-object v6

    .line 616
    :goto_65
    const/4 v0, 0x0

    .line 617
    .local v0, "Xposed4":Z
    if-eqz v6, :cond_6e

    .line 618
    const-string v7, "patch4"

    invoke-virtual {v6, v7, v10}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 621
    :cond_6e
    if-nez v0, :cond_77

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 628
    .end local v0    # "Xposed4":Z
    .end local v5    # "s":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;>;"
    .end local v6    # "settings":Lorg/json/JSONObject;
    :cond_77
    :goto_77
    const v7, 0x7f070209

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 629
    const v7, 0x7f07020b

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 630
    sget-boolean v7, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->su:Z

    if-eqz v7, :cond_ad

    .line 631
    const v7, 0x7f07020d

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 632
    const v7, 0x7f070211

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 633
    const v7, 0x7f07020f

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 638
    :cond_ad
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-eqz v7, :cond_e3

    .line 639
    new-instance v7, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$6$1;

    iget-object v8, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$6;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;

    const v9, 0x7f040039

    invoke-direct {v7, p0, v8, v9, v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$6$1;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$6;Landroid/content/Context;ILjava/util/List;)V

    sput-object v7, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->adapt:Landroid/widget/ArrayAdapter;

    .line 860
    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$6;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;

    new-instance v8, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$6$2;

    invoke-direct {v8, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$6$2;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$6;)V

    invoke-virtual {v7, v8}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 878
    :goto_c9
    return-void

    .line 594
    .end local v4    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :catch_ca
    move-exception v2

    .line 596
    .local v2, "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v2}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto/16 :goto_10

    .line 597
    .end local v2    # "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_d0
    move-exception v2

    .local v2, "e1":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v2}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto/16 :goto_10

    .line 613
    .end local v2    # "e1":Ljava/lang/IllegalArgumentException;
    .restart local v4    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .restart local v5    # "s":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;>;"
    .restart local v6    # "settings":Lorg/json/JSONObject;
    :catch_d6
    move-exception v1

    .line 614
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_65

    .line 623
    .end local v1    # "e":Lorg/json/JSONException;
    .end local v5    # "s":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;>;"
    .end local v6    # "settings":Lorg/json/JSONObject;
    :cond_db
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_77

    .line 870
    :cond_e3
    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$6;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;

    new-instance v8, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$6$3;

    invoke-direct {v8, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$6$3;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity$6;)V

    invoke-virtual {v7, v8}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_c9
.end method
