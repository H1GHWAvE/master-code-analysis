.class Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$3;
.super Landroid/widget/ArrayAdapter;
.source "BinderActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;->getDir(Ljava/lang/String;Landroid/widget/ListView;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$ItemFile;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;Landroid/content/Context;ILjava/util/List;)V
    .registers 5
    .param p1, "this$0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;
    .param p2, "x0"    # Landroid/content/Context;
    .param p3, "x1"    # I

    .prologue
    .line 659
    .local p4, "x2":Ljava/util/List;, "Ljava/util/List<Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$ItemFile;>;"
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$3;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;

    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 14
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v9, 0x1

    .line 665
    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$3;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$ItemFile;

    .line 667
    .local v0, "current":Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$ItemFile;
    move-object v4, p2

    .line 670
    .local v4, "row":Landroid/view/View;
    if-nez v4, :cond_18

    .line 672
    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$3;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;

    invoke-virtual {v7}, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    .line 673
    .local v3, "inflater":Landroid/view/LayoutInflater;
    const v7, 0x7f040027

    const/4 v8, 0x0

    invoke-virtual {v3, v7, p3, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 684
    .end local v3    # "inflater":Landroid/view/LayoutInflater;
    :cond_18
    const v7, 0x7f0d009b

    invoke-virtual {v4, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 685
    .local v5, "textView":Landroid/widget/TextView;
    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$3;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;

    iget-object v7, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;->context:Landroid/content/Context;

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getSizeText()I

    move-result v8

    invoke-virtual {v5, v7, v8}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 687
    iget-object v7, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$ItemFile;->file:Ljava/lang/String;

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 688
    const v7, 0x7f0d009a

    invoke-virtual {v4, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 690
    .local v2, "icon":Landroid/widget/ImageView;
    if-eqz p1, :cond_3e

    if-ne p1, v9, :cond_60

    :cond_3e
    const/4 v7, 0x1

    :try_start_3f
    invoke-virtual {p0, v7}, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$3;->getItem(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$ItemFile;

    iget-object v7, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$ItemFile;->file:Ljava/lang/String;

    const-string v8, "../"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_60

    .line 691
    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$3;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;

    invoke-virtual {v7}, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f020029

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 706
    :goto_5f
    return-object v4

    .line 693
    :cond_60
    new-instance v7, Ljava/io/File;

    iget-object v8, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$ItemFile;->full:Ljava/lang/String;

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/io/File;->isDirectory()Z

    move-result v7

    if-eqz v7, :cond_83

    .line 694
    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$3;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;

    invoke-virtual {v7}, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f02002a

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    :try_end_7d
    .catch Ljava/lang/Exception; {:try_start_3f .. :try_end_7d} :catch_7e
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3f .. :try_end_7d} :catch_94

    goto :goto_5f

    .line 699
    :catch_7e
    move-exception v1

    .line 700
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_5f

    .line 696
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_83
    :try_start_83
    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$3;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;

    invoke-virtual {v7}, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f02002b

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    :try_end_93
    .catch Ljava/lang/Exception; {:try_start_83 .. :try_end_93} :catch_7e
    .catch Ljava/lang/OutOfMemoryError; {:try_start_83 .. :try_end_93} :catch_94

    goto :goto_5f

    .line 701
    :catch_94
    move-exception v6

    .line 702
    .local v6, "u":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v6}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    .line 703
    invoke-static {}, Ljava/lang/System;->gc()V

    goto :goto_5f
.end method
