.class public Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog;
.super Ljava/lang/Object;
.source "App_Dialog.java"


# instance fields
.field dialog:Landroid/app/Dialog;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog;->dialog:Landroid/app/Dialog;

    .line 38
    return-void
.end method


# virtual methods
.method public dismiss()V
    .registers 2

    .prologue
    .line 306
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog;->dialog:Landroid/app/Dialog;

    if-eqz v0, :cond_c

    .line 307
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog;->dialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 308
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog;->dialog:Landroid/app/Dialog;

    .line 310
    :cond_c
    return-void
.end method

.method public onCreateDialog()Landroid/app/Dialog;
    .registers 20

    .prologue
    .line 50
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "App Dialog create."

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 51
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    if-eqz v1, :cond_13

    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-nez v1, :cond_16

    :cond_13
    invoke-virtual/range {p0 .. p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog;->dismiss()V

    .line 52
    :cond_16
    new-instance v5, Landroid/text/SpannableStringBuilder;

    invoke-direct {v5}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 53
    .local v5, "builder":Landroid/text/SpannableStringBuilder;
    new-instance v8, Landroid/text/SpannableStringBuilder;

    invoke-direct {v8}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 54
    .local v8, "builder2":Landroid/text/SpannableStringBuilder;
    new-instance v6, Landroid/text/SpannableStringBuilder;

    invoke-direct {v6}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 59
    .local v6, "builder3":Landroid/text/SpannableStringBuilder;
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f040008

    const/16 v18, 0x0

    move-object/from16 v0, v18

    invoke-static {v1, v2, v0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/LinearLayout;

    .line 61
    .local v12, "d":Landroid/widget/LinearLayout;
    const v1, 0x7f0d002f

    invoke-virtual {v12, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0d000c

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/LinearLayout;

    .line 63
    .local v16, "scrbody":Landroid/widget/LinearLayout;
    const v1, 0x7f0d0030

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/ImageView;

    .line 64
    .local v15, "img":Landroid/widget/ImageView;
    const v1, 0x7f0d0031

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 65
    .local v3, "txt":Landroid/widget/TextView;
    const v1, 0x7f0d000d

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 66
    .local v4, "txt2":Landroid/widget/TextView;
    const v1, 0x7f0d0034

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 67
    .local v9, "txt3":Landroid/widget/TextView;
    const v1, 0x7f0d0037

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 68
    .local v7, "txt4":Landroid/widget/TextView;
    const v1, 0x7f0d0033

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/ProgressBar;

    .line 72
    .local v10, "bar":Landroid/widget/ProgressBar;
    :try_start_8a
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v1

    sget-object v2, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    iget-object v2, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getApplicationIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v15, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    :try_end_99
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_8a .. :try_end_99} :catch_d2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_8a .. :try_end_99} :catch_d7
    .catch Ljava/lang/Exception; {:try_start_8a .. :try_end_99} :catch_dc

    .line 82
    :goto_99
    new-instance v13, Ljava/lang/Thread;

    new-instance v1, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1;

    move-object/from16 v2, p0

    invoke-direct/range {v1 .. v10}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$1;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/text/SpannableStringBuilder;Landroid/text/SpannableStringBuilder;Landroid/widget/TextView;Landroid/text/SpannableStringBuilder;Landroid/widget/TextView;Landroid/widget/ProgressBar;)V

    invoke-direct {v13, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 294
    .local v13, "data":Ljava/lang/Thread;
    invoke-virtual {v13}, Ljava/lang/Thread;->start()V

    .line 295
    new-instance v1, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    sget-object v2, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const/16 v18, 0x1

    move/from16 v0, v18

    invoke-direct {v1, v2, v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;-><init>(Landroid/content/Context;Z)V

    invoke-virtual {v1, v12}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setView(Landroid/view/View;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->create()Landroid/app/Dialog;

    move-result-object v17

    .line 296
    .local v17, "tempdialog":Landroid/app/Dialog;
    const/4 v1, 0x1

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 297
    new-instance v1, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$2;

    move-object/from16 v0, p0

    invoke-direct {v1, v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog$2;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog;)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 303
    return-object v17

    .line 73
    .end local v13    # "data":Ljava/lang/Thread;
    .end local v17    # "tempdialog":Landroid/app/Dialog;
    :catch_d2
    move-exception v14

    .line 75
    .local v14, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_d3
    invoke-virtual {v14}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V
    :try_end_d6
    .catch Ljava/lang/OutOfMemoryError; {:try_start_d3 .. :try_end_d6} :catch_d7
    .catch Ljava/lang/Exception; {:try_start_d3 .. :try_end_d6} :catch_dc

    goto :goto_99

    .line 77
    .end local v14    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_d7
    move-exception v11

    .line 78
    .local v11, "E":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v11}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    goto :goto_99

    .line 79
    .end local v11    # "E":Ljava/lang/OutOfMemoryError;
    :catch_dc
    move-exception v14

    .line 80
    .local v14, "e":Ljava/lang/Exception;
    invoke-virtual {v14}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_99
.end method

.method public showDialog()V
    .registers 2

    .prologue
    .line 41
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog;->dialog:Landroid/app/Dialog;

    if-nez v0, :cond_a

    .line 42
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog;->onCreateDialog()Landroid/app/Dialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog;->dialog:Landroid/app/Dialog;

    .line 44
    :cond_a
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog;->dialog:Landroid/app/Dialog;

    if-eqz v0, :cond_13

    .line 45
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/App_Dialog;->dialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 47
    :cond_13
    return-void
.end method
