.class Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$85;
.super Landroid/widget/ArrayAdapter;
.source "listAppsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->contextselpatchlvl(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/android/vending/billing/InAppBillingService/LUCK/Patterns;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

.field txtStatus:Landroid/widget/TextView;

.field txtTitle:Landroid/widget/TextView;

.field final synthetic val$installpatch:Z


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;Landroid/content/Context;ILjava/util/List;Z)V
    .registers 6
    .param p1, "this$0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;
    .param p2, "x0"    # Landroid/content/Context;
    .param p3, "x1"    # I

    .prologue
    .line 10002
    .local p4, "x2":Ljava/util/List;, "Ljava/util/List<Lcom/android/vending/billing/InAppBillingService/LUCK/Patterns;>;"
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$85;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iput-boolean p5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$85;->val$installpatch:Z

    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 12
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 10009
    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$85;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/vending/billing/InAppBillingService/LUCK/Patterns;

    .line 10010
    .local v2, "p":Lcom/android/vending/billing/InAppBillingService/LUCK/Patterns;
    move-object v3, p2

    .line 10017
    .local v3, "row":Landroid/view/View;
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v5

    const-string v6, "layout_inflater"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 10018
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v5, 0x7f040038

    const/4 v6, 0x0

    invoke-virtual {v1, v5, p3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 10020
    const v5, 0x7f0d004e

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$85;->txtTitle:Landroid/widget/TextView;

    .line 10021
    const v5, 0x7f0d004f

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$85;->txtStatus:Landroid/widget/TextView;

    .line 10024
    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$85;->txtTitle:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$85;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getSizeText()I

    move-result v7

    invoke-virtual {v5, v6, v7}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 10025
    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$85;->txtStatus:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$85;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getSizeText()I

    move-result v7

    invoke-virtual {v5, v6, v7}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 10028
    const v5, 0x7f0d0080

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 10029
    .local v0, "chk":Landroid/widget/CheckBox;
    iget-boolean v5, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/Patterns;->Status:Z

    invoke-virtual {v0, v5}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 10030
    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 10040
    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$85;->txtStatus:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$85;->getContext()Landroid/content/Context;

    move-result-object v6

    const v7, 0x1030046

    invoke-virtual {v5, v6, v7}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 10041
    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$85;->txtStatus:Landroid/widget/TextView;

    const v6, -0x777778

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 10042
    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$85;->txtTitle:Landroid/widget/TextView;

    const/4 v6, -0x1

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 10043
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$85;->txtTitle:Landroid/widget/TextView;

    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$85;->getItem(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/vending/billing/InAppBillingService/LUCK/Patterns;

    iget-object v5, v5, Lcom/android/vending/billing/InAppBillingService/LUCK/Patterns;->Name:Ljava/lang/String;

    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 10044
    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$85;->txtTitle:Landroid/widget/TextView;

    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-virtual {v5, v6, v7}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 10046
    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$85;->getItem(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/vending/billing/InAppBillingService/LUCK/Patterns;

    iget-object v4, v5, Lcom/android/vending/billing/InAppBillingService/LUCK/Patterns;->Name:Ljava/lang/String;

    .line 10048
    .local v4, "str2":Ljava/lang/String;
    const/4 v5, 0x6

    if-eq p1, v5, :cond_99

    const/4 v5, 0x7

    if-ne p1, v5, :cond_11e

    :cond_99
    iget-boolean v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$85;->val$installpatch:Z

    if-eqz v5, :cond_11e

    .line 10049
    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$85;->txtTitle:Landroid/widget/TextView;

    const-string v6, "#ffffff00"

    const-string v7, "bold"

    invoke-static {v4, v6, v7}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 10060
    :goto_aa
    if-nez p1, :cond_b3

    const v5, 0x7f0700a9

    invoke-static {v5}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v4

    .line 10061
    :cond_b3
    const/4 v5, 0x1

    if-ne p1, v5, :cond_bd

    const v5, 0x7f0700ab

    invoke-static {v5}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v4

    .line 10062
    :cond_bd
    const/4 v5, 0x2

    if-ne p1, v5, :cond_c7

    const v5, 0x7f0700ad

    invoke-static {v5}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v4

    .line 10063
    :cond_c7
    const/4 v5, 0x3

    if-ne p1, v5, :cond_d1

    const v5, 0x7f0700a6

    invoke-static {v5}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v4

    .line 10064
    :cond_d1
    const/4 v5, 0x4

    if-ne p1, v5, :cond_db

    const v5, 0x7f0700b5

    invoke-static {v5}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v4

    .line 10065
    :cond_db
    const/4 v5, 0x5

    if-ne p1, v5, :cond_e5

    const v5, 0x7f0700a7

    invoke-static {v5}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v4

    .line 10066
    :cond_e5
    const/4 v5, 0x6

    if-ne p1, v5, :cond_f3

    iget-boolean v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$85;->val$installpatch:Z

    if-eqz v5, :cond_f3

    .line 10067
    const v5, 0x7f0700af

    invoke-static {v5}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v4

    .line 10068
    :cond_f3
    const/4 v5, 0x7

    if-ne p1, v5, :cond_101

    iget-boolean v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$85;->val$installpatch:Z

    if-eqz v5, :cond_101

    .line 10069
    const v5, 0x7f0700b1

    invoke-static {v5}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v4

    .line 10070
    :cond_101
    const/16 v5, 0x8

    if-ne p1, v5, :cond_110

    iget-boolean v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$85;->val$installpatch:Z

    if-eqz v5, :cond_110

    .line 10071
    const v5, 0x7f0700b3

    invoke-static {v5}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v4

    .line 10072
    :cond_110
    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$85;->txtStatus:Landroid/widget/TextView;

    const-string v6, "#ff888888"

    const-string v7, "italic"

    invoke-static {v4, v6, v7}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 10076
    return-object v3

    .line 10051
    :cond_11e
    const/16 v5, 0x8

    if-ne p1, v5, :cond_135

    iget-boolean v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$85;->val$installpatch:Z

    if-eqz v5, :cond_135

    .line 10052
    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$85;->txtTitle:Landroid/widget/TextView;

    const-string v6, "#ffff0000"

    const-string v7, "bold"

    invoke-static {v4, v6, v7}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_aa

    .line 10054
    :cond_135
    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$85;->txtTitle:Landroid/widget/TextView;

    const-string v6, "#ff00ff00"

    const-string v7, "bold"

    invoke-static {v4, v6, v7}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_aa
.end method
