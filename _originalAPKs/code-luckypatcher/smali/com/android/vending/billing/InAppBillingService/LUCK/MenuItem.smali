.class public Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;
.super Ljava/lang/Object;
.source "MenuItem.java"


# instance fields
.field public booleanPref:Ljava/lang/String;

.field public booleanPrefDef:Z

.field public childs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public icon:Z

.field public punkt_menu:I

.field public punkt_menu_descr:I

.field public runCode:Ljava/lang/Runnable;

.field public type:I

.field public typeChild:I


# direct methods
.method public constructor <init>(IILjava/util/List;IIZ)V
    .registers 9
    .param p1, "group"    # I
    .param p2, "descr"    # I
    .param p4, "type"    # I
    .param p5, "typeChield"    # I
    .param p6, "hasIcon"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;IIZ)V"
        }
    .end annotation

    .prologue
    .local p3, "childs_menu":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->punkt_menu_descr:I

    .line 12
    iput-boolean v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->icon:Z

    .line 13
    iput v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->type:I

    .line 14
    iput v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->typeChild:I

    .line 15
    iput-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->runCode:Ljava/lang/Runnable;

    .line 16
    iput-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->booleanPref:Ljava/lang/String;

    .line 17
    iput-boolean v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->booleanPrefDef:Z

    .line 57
    iput p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->punkt_menu:I

    .line 58
    iput-object p3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->childs:Ljava/util/List;

    .line 59
    iput-boolean p6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->icon:Z

    .line 60
    iput p4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->type:I

    .line 61
    iput p5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->typeChild:I

    .line 62
    iput p2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->punkt_menu_descr:I

    .line 63
    return-void
.end method

.method public constructor <init>(IILjava/util/List;IZ)V
    .registers 8
    .param p1, "group"    # I
    .param p2, "descr"    # I
    .param p4, "type"    # I
    .param p5, "hasIcon"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;IZ)V"
        }
    .end annotation

    .prologue
    .local p3, "childs_menu":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->punkt_menu_descr:I

    .line 12
    iput-boolean v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->icon:Z

    .line 13
    iput v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->type:I

    .line 14
    iput v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->typeChild:I

    .line 15
    iput-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->runCode:Ljava/lang/Runnable;

    .line 16
    iput-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->booleanPref:Ljava/lang/String;

    .line 17
    iput-boolean v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->booleanPrefDef:Z

    .line 32
    iput p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->punkt_menu:I

    .line 33
    iput-object p3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->childs:Ljava/util/List;

    .line 34
    iput-boolean p5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->icon:Z

    .line 35
    iput p4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->type:I

    .line 36
    iput p2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->punkt_menu_descr:I

    .line 38
    return-void
.end method

.method public constructor <init>(IILjava/util/List;IZLjava/lang/String;ZLjava/lang/Runnable;)V
    .registers 11
    .param p1, "group"    # I
    .param p2, "descr"    # I
    .param p4, "type"    # I
    .param p5, "hasIcon"    # Z
    .param p6, "booleanPref"    # Ljava/lang/String;
    .param p7, "defBoolPref"    # Z
    .param p8, "runCode"    # Ljava/lang/Runnable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;IZ",
            "Ljava/lang/String;",
            "Z",
            "Ljava/lang/Runnable;",
            ")V"
        }
    .end annotation

    .prologue
    .local p3, "childs_menu":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->punkt_menu_descr:I

    .line 12
    iput-boolean v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->icon:Z

    .line 13
    iput v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->type:I

    .line 14
    iput v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->typeChild:I

    .line 15
    iput-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->runCode:Ljava/lang/Runnable;

    .line 16
    iput-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->booleanPref:Ljava/lang/String;

    .line 17
    iput-boolean v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->booleanPrefDef:Z

    .line 40
    iput p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->punkt_menu:I

    .line 41
    iput-object p3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->childs:Ljava/util/List;

    .line 42
    iput-boolean p5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->icon:Z

    .line 43
    iput p4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->type:I

    .line 44
    iput p2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->punkt_menu_descr:I

    .line 45
    iput-object p8, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->runCode:Ljava/lang/Runnable;

    .line 46
    iput-object p6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->booleanPref:Ljava/lang/String;

    .line 47
    iput-boolean p7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->booleanPrefDef:Z

    .line 48
    return-void
.end method

.method public constructor <init>(ILjava/util/List;IIZ)V
    .registers 8
    .param p1, "group"    # I
    .param p3, "type"    # I
    .param p4, "typeChield"    # I
    .param p5, "hasIcon"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;IIZ)V"
        }
    .end annotation

    .prologue
    .local p2, "childs_menu":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->punkt_menu_descr:I

    .line 12
    iput-boolean v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->icon:Z

    .line 13
    iput v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->type:I

    .line 14
    iput v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->typeChild:I

    .line 15
    iput-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->runCode:Ljava/lang/Runnable;

    .line 16
    iput-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->booleanPref:Ljava/lang/String;

    .line 17
    iput-boolean v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->booleanPrefDef:Z

    .line 50
    iput p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->punkt_menu:I

    .line 51
    iput-object p2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->childs:Ljava/util/List;

    .line 52
    iput-boolean p5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->icon:Z

    .line 53
    iput p3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->type:I

    .line 54
    iput p4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->typeChild:I

    .line 55
    return-void
.end method

.method public constructor <init>(ILjava/util/List;IZ)V
    .registers 7
    .param p1, "group"    # I
    .param p3, "type"    # I
    .param p4, "hasIcon"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;IZ)V"
        }
    .end annotation

    .prologue
    .local p2, "childs_menu":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->punkt_menu_descr:I

    .line 12
    iput-boolean v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->icon:Z

    .line 13
    iput v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->type:I

    .line 14
    iput v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->typeChild:I

    .line 15
    iput-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->runCode:Ljava/lang/Runnable;

    .line 16
    iput-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->booleanPref:Ljava/lang/String;

    .line 17
    iput-boolean v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->booleanPrefDef:Z

    .line 25
    iput p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->punkt_menu:I

    .line 26
    iput-object p2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->childs:Ljava/util/List;

    .line 27
    iput-boolean p4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->icon:Z

    .line 28
    iput p3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->type:I

    .line 30
    return-void
.end method

.method public constructor <init>(ILjava/util/List;Z)V
    .registers 6
    .param p1, "group"    # I
    .param p3, "hasIcon"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .local p2, "childs_menu":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->punkt_menu_descr:I

    .line 12
    iput-boolean v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->icon:Z

    .line 13
    iput v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->type:I

    .line 14
    iput v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->typeChild:I

    .line 15
    iput-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->runCode:Ljava/lang/Runnable;

    .line 16
    iput-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->booleanPref:Ljava/lang/String;

    .line 17
    iput-boolean v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->booleanPrefDef:Z

    .line 20
    iput p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->punkt_menu:I

    .line 21
    iput-object p2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->childs:Ljava/util/List;

    .line 22
    iput-boolean p3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/MenuItem;->icon:Z

    .line 23
    return-void
.end method
