.class public Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Menu_Dialog_Static;
.super Ljava/lang/Object;
.source "Menu_Dialog_Static.java"


# instance fields
.field dialog:Landroid/app/Dialog;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Menu_Dialog_Static;->dialog:Landroid/app/Dialog;

    .line 32
    return-void
.end method


# virtual methods
.method public dismiss()V
    .registers 2

    .prologue
    .line 97
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Menu_Dialog_Static;->dialog:Landroid/app/Dialog;

    if-eqz v0, :cond_c

    .line 98
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Menu_Dialog_Static;->dialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 99
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Menu_Dialog_Static;->dialog:Landroid/app/Dialog;

    .line 101
    :cond_c
    return-void
.end method

.method public onCreateDialog()Landroid/app/Dialog;
    .registers 5

    .prologue
    .line 46
    :try_start_0
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "Menu Dialog create."

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 47
    sget-object v2, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    if-eqz v2, :cond_13

    sget-object v2, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    if-nez v2, :cond_16

    :cond_13
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Menu_Dialog_Static;->dismiss()V

    .line 49
    :cond_16
    new-instance v0, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    sget-object v2, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;-><init>(Landroid/content/Context;)V

    .line 51
    .local v0, "builder5":Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    const v2, 0x7f0701de

    new-instance v3, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Menu_Dialog_Static$1;

    invoke-direct {v3, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Menu_Dialog_Static$1;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Menu_Dialog_Static;)V

    invoke-virtual {v0, v2, v3}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    .line 57
    sget-object v2, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->adapt:Landroid/widget/ArrayAdapter;

    if-eqz v2, :cond_44

    .line 60
    sget-object v2, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->adapt:Landroid/widget/ArrayAdapter;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/ArrayAdapter;->setNotifyOnChange(Z)V

    .line 61
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setAdapterNotClose(Z)V

    .line 62
    sget-object v2, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->adapt:Landroid/widget/ArrayAdapter;

    new-instance v3, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Menu_Dialog_Static$2;

    invoke-direct {v3, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Menu_Dialog_Static$2;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Menu_Dialog_Static;)V

    invoke-virtual {v0, v2, v3}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setAdapter(Landroid/widget/ArrayAdapter;Landroid/widget/AdapterView$OnItemClickListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    .line 81
    :cond_44
    new-instance v2, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Menu_Dialog_Static$3;

    invoke-direct {v2, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Menu_Dialog_Static$3;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Menu_Dialog_Static;)V

    invoke-virtual {v0, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    .line 91
    invoke-virtual {v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->create()Landroid/app/Dialog;
    :try_end_4f
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_4f} :catch_51

    move-result-object v2

    .line 93
    .end local v0    # "builder5":Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;
    :goto_50
    return-object v2

    .line 92
    :catch_51
    move-exception v1

    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 93
    const/4 v2, 0x0

    goto :goto_50
.end method

.method public showDialog()V
    .registers 2

    .prologue
    .line 35
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Menu_Dialog_Static;->dialog:Landroid/app/Dialog;

    if-nez v0, :cond_a

    .line 36
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Menu_Dialog_Static;->onCreateDialog()Landroid/app/Dialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Menu_Dialog_Static;->dialog:Landroid/app/Dialog;

    .line 38
    :cond_a
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Menu_Dialog_Static;->dialog:Landroid/app/Dialog;

    if-eqz v0, :cond_13

    .line 39
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Menu_Dialog_Static;->dialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 41
    :cond_13
    return-void
.end method
