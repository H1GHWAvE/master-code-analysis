.class public Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadFile;
.super Landroid/os/AsyncTask;
.source "listAppsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "DownloadFile"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field cacheFound:Z

.field corruptdownload:Z

.field downloadedFound:Z

.field filename:Ljava/lang/String;

.field internet_not_found:Z

.field numbytes:I

.field on_post_run_cached:Ljava/lang/Runnable;

.field on_post_run_downloaded:Ljava/lang/Runnable;

.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;


# direct methods
.method public constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;)V
    .registers 4
    .param p1, "this$0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 11622
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadFile;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 11623
    iput v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadFile;->numbytes:I

    .line 11624
    iput-boolean v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadFile;->corruptdownload:Z

    .line 11625
    iput-boolean v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadFile;->internet_not_found:Z

    .line 11626
    iput-boolean v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadFile;->cacheFound:Z

    .line 11627
    iput-boolean v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadFile;->downloadedFound:Z

    .line 11628
    const-string v0, "mod.market4.apk"

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadFile;->filename:Ljava/lang/String;

    .line 11629
    iput-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadFile;->on_post_run_cached:Ljava/lang/Runnable;

    .line 11630
    iput-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadFile;->on_post_run_downloaded:Ljava/lang/Runnable;

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Boolean;
    .registers 23
    .param p1, "params"    # [Ljava/lang/String;

    .prologue
    .line 11639
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "http://chelpus.defcon5.biz/"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const/16 v16, 0x0

    aget-object v16, p1, v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 11640
    .local v14, "urlpath":Ljava/lang/String;
    const/4 v15, 0x0

    aget-object v15, p1, v15

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadFile;->filename:Ljava/lang/String;

    .line 11642
    :try_start_1e
    new-instance v15, Ljava/net/URL;

    invoke-direct {v15, v14}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 11643
    invoke-virtual {v15}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v5

    check-cast v5, Ljava/net/HttpURLConnection;

    .line 11644
    .local v5, "con":Ljava/net/HttpURLConnection;
    const-string v15, "HEAD"

    invoke-virtual {v5, v15}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 11645
    const-string v15, "Cache-Control"

    const-string v16, "no-cache"

    move-object/from16 v0, v16

    invoke-virtual {v5, v15, v0}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 11646
    const v15, 0xf4240

    invoke-virtual {v5, v15}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 11647
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->connect()V

    .line 11648
    const-string v15, "Content-length"

    invoke-virtual {v5, v15}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v15

    move-object/from16 v0, p0

    iput v15, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadFile;->numbytes:I

    .line 11649
    sget-object v15, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v16, "%s bytes found, %s Mb"

    const/16 v17, 0x2

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadFile;->numbytes:I

    move/from16 v19, v0

    .line 11650
    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    aput-object v19, v17, v18

    const/16 v18, 0x1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadFile;->numbytes:I

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    const/high16 v20, 0x49800000    # 1048576.0f

    div-float v19, v19, v20

    invoke-static/range {v19 .. v19}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v19

    aput-object v19, v17, v18

    .line 11649
    invoke-static/range {v16 .. v17}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 11651
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_89
    .catch Ljava/lang/Exception; {:try_start_1e .. :try_end_89} :catch_256

    .line 11658
    .end local v5    # "con":Ljava/net/HttpURLConnection;
    :goto_89
    sget-object v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->progress_loading:Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;

    if-eqz v15, :cond_a6

    sget-object v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->progress_loading:Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;

    invoke-virtual {v15}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->isShowing()Z

    move-result v15

    if-eqz v15, :cond_a6

    sget-object v15, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->progress_loading:Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadFile;->numbytes:I

    move/from16 v16, v0

    move/from16 v0, v16

    div-int/lit16 v0, v0, 0x400

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->setMax(I)V

    .line 11659
    :cond_a6
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput-boolean v15, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadFile;->cacheFound:Z

    .line 11660
    new-instance v15, Ljava/io/File;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v17, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "/Download/"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const/16 v17, 0x0

    aget-object v17, p1, v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-direct/range {v15 .. v16}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v15}, Ljava/io/File;->exists()Z

    move-result v15

    if-eqz v15, :cond_14f

    .line 11661
    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadFile;->internet_not_found:Z

    if-nez v15, :cond_266

    .line 11662
    sget-object v15, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v16, Ljava/io/File;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v18, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "/Download/"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const/16 v18, 0x0

    aget-object v18, p1, v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-direct/range {v16 .. v17}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->length()J

    move-result-wide v16

    invoke-virtual/range {v15 .. v17}, Ljava/io/PrintStream;->println(J)V

    .line 11663
    sget-object v15, Ljava/lang/System;->out:Ljava/io/PrintStream;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadFile;->numbytes:I

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/io/PrintStream;->println(I)V

    .line 11664
    new-instance v15, Ljava/io/File;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v17, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "/Download/"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const/16 v17, 0x0

    aget-object v17, p1, v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-direct/range {v15 .. v16}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v15}, Ljava/io/File;->length()J

    move-result-wide v15

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadFile;->numbytes:I

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-long v0, v0

    move-wide/from16 v17, v0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_14a

    move-object/from16 v0, p0

    iget v15, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadFile;->numbytes:I

    if-nez v15, :cond_14f

    .line 11665
    :cond_14a
    const/4 v15, 0x1

    move-object/from16 v0, p0

    iput-boolean v15, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadFile;->cacheFound:Z

    .line 11671
    :cond_14f
    :goto_14f
    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadFile;->internet_not_found:Z

    if-nez v15, :cond_2a5

    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadFile;->cacheFound:Z

    if-nez v15, :cond_2a5

    .line 11673
    :try_start_15b
    new-instance v13, Ljava/net/URL;

    invoke-direct {v13, v14}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 11675
    .local v13, "url":Ljava/net/URL;
    invoke-virtual {v13}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v4

    check-cast v4, Ljava/net/HttpURLConnection;

    .line 11676
    .local v4, "c":Ljava/net/HttpURLConnection;
    const-string v15, "GET"

    invoke-virtual {v4, v15}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 11677
    const v15, 0xf4240

    invoke-virtual {v4, v15}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 11678
    const/4 v15, 0x0

    invoke-virtual {v4, v15}, Ljava/net/HttpURLConnection;->setUseCaches(Z)V

    .line 11679
    const-string v15, "Cache-Control"

    const-string v16, "no-cache"

    move-object/from16 v0, v16

    invoke-virtual {v4, v15, v0}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 11680
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->connect()V

    .line 11682
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v16, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "/Download/"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 11683
    .local v2, "PATH":Ljava/lang/String;
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 11684
    .local v7, "file":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v15

    if-nez v15, :cond_1a4

    .line 11685
    invoke-virtual {v7}, Ljava/io/File;->mkdirs()Z

    .line 11687
    :cond_1a4
    new-instance v11, Ljava/io/File;

    const/4 v15, 0x0

    aget-object v15, p1, v15

    invoke-direct {v11, v7, v15}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 11688
    .local v11, "outputFile":Ljava/io/File;
    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    move-result v15

    if-eqz v15, :cond_1b5

    invoke-virtual {v11}, Ljava/io/File;->delete()Z

    .line 11689
    :cond_1b5
    new-instance v8, Ljava/io/FileOutputStream;

    invoke-direct {v8, v11}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 11690
    .local v8, "fos":Ljava/io/FileOutputStream;
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v9

    .line 11691
    .local v9, "is":Ljava/io/InputStream;
    const/16 v15, 0x2000

    new-array v3, v15, [B

    .line 11692
    .local v3, "buffer":[B
    const/4 v12, 0x0

    .line 11693
    .local v12, "size":I
    const/4 v10, 0x0

    .line 11694
    .local v10, "len1":I
    :goto_1c4
    invoke-virtual {v9, v3}, Ljava/io/InputStream;->read([B)I

    move-result v10

    const/4 v15, -0x1

    if-eq v10, v15, :cond_26d

    .line 11695
    add-int/2addr v12, v10

    .line 11696
    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Integer;

    const/16 v16, 0x0

    div-int/lit16 v0, v12, 0x400

    move/from16 v17, v0

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadFile;->publishProgress([Ljava/lang/Object;)V

    .line 11697
    const/4 v15, 0x0

    invoke-virtual {v8, v3, v15, v10}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_1e4
    .catch Ljava/io/IOException; {:try_start_15b .. :try_end_1e4} :catch_1e5

    goto :goto_1c4

    .line 11701
    .end local v2    # "PATH":Ljava/lang/String;
    .end local v3    # "buffer":[B
    .end local v4    # "c":Ljava/net/HttpURLConnection;
    .end local v7    # "file":Ljava/io/File;
    .end local v8    # "fos":Ljava/io/FileOutputStream;
    .end local v9    # "is":Ljava/io/InputStream;
    .end local v10    # "len1":I
    .end local v11    # "outputFile":Ljava/io/File;
    .end local v12    # "size":I
    .end local v13    # "url":Ljava/net/URL;
    :catch_1e5
    move-exception v6

    .line 11702
    .local v6, "e":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    .line 11705
    .end local v6    # "e":Ljava/io/IOException;
    :goto_1e9
    new-instance v15, Ljava/io/File;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v17, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "/Download/"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const/16 v17, 0x0

    aget-object v17, p1, v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-direct/range {v15 .. v16}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v15}, Ljava/io/File;->exists()Z

    move-result v15

    if-eqz v15, :cond_250

    .line 11706
    new-instance v15, Ljava/io/File;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v17, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "/Download/"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const/16 v17, 0x0

    aget-object v17, p1, v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-direct/range {v15 .. v16}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v15}, Ljava/io/File;->length()J

    move-result-wide v15

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadFile;->numbytes:I

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-long v0, v0

    move-wide/from16 v17, v0

    cmp-long v15, v15, v17

    if-nez v15, :cond_275

    .line 11707
    const/4 v15, 0x1

    move-object/from16 v0, p0

    iput-boolean v15, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadFile;->cacheFound:Z

    .line 11708
    const/4 v15, 0x1

    move-object/from16 v0, p0

    iput-boolean v15, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadFile;->downloadedFound:Z

    .line 11720
    :cond_250
    :goto_250
    const/4 v15, 0x1

    invoke-static {v15}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v15

    return-object v15

    .line 11652
    :catch_256
    move-exception v6

    .line 11653
    .local v6, "e":Ljava/lang/Exception;
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    .line 11654
    const/4 v15, 0x1

    move-object/from16 v0, p0

    iput-boolean v15, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadFile;->internet_not_found:Z

    .line 11655
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput v15, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadFile;->numbytes:I

    goto/16 :goto_89

    .line 11668
    .end local v6    # "e":Ljava/lang/Exception;
    :cond_266
    const/4 v15, 0x1

    move-object/from16 v0, p0

    iput-boolean v15, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadFile;->cacheFound:Z

    goto/16 :goto_14f

    .line 11699
    .restart local v2    # "PATH":Ljava/lang/String;
    .restart local v3    # "buffer":[B
    .restart local v4    # "c":Ljava/net/HttpURLConnection;
    .restart local v7    # "file":Ljava/io/File;
    .restart local v8    # "fos":Ljava/io/FileOutputStream;
    .restart local v9    # "is":Ljava/io/InputStream;
    .restart local v10    # "len1":I
    .restart local v11    # "outputFile":Ljava/io/File;
    .restart local v12    # "size":I
    .restart local v13    # "url":Ljava/net/URL;
    :cond_26d
    :try_start_26d
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->close()V

    .line 11700
    invoke-virtual {v9}, Ljava/io/InputStream;->close()V
    :try_end_273
    .catch Ljava/io/IOException; {:try_start_26d .. :try_end_273} :catch_1e5

    goto/16 :goto_1e9

    .line 11710
    .end local v2    # "PATH":Ljava/lang/String;
    .end local v3    # "buffer":[B
    .end local v4    # "c":Ljava/net/HttpURLConnection;
    .end local v7    # "file":Ljava/io/File;
    .end local v8    # "fos":Ljava/io/FileOutputStream;
    .end local v9    # "is":Ljava/io/InputStream;
    .end local v10    # "len1":I
    .end local v11    # "outputFile":Ljava/io/File;
    .end local v12    # "size":I
    .end local v13    # "url":Ljava/net/URL;
    :cond_275
    new-instance v15, Ljava/io/File;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v17, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "/Download/"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const/16 v17, 0x0

    aget-object v17, p1, v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-direct/range {v15 .. v16}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v15}, Ljava/io/File;->delete()Z

    .line 11711
    const/4 v15, 0x1

    move-object/from16 v0, p0

    iput-boolean v15, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadFile;->corruptdownload:Z

    .line 11712
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput-boolean v15, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadFile;->cacheFound:Z

    goto :goto_250

    .line 11716
    :cond_2a5
    new-instance v15, Ljava/io/File;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v17, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "/Download/"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const/16 v17, 0x0

    aget-object v17, p1, v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-direct/range {v15 .. v16}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v15}, Ljava/io/File;->exists()Z

    move-result v15

    if-eqz v15, :cond_250

    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadFile;->cacheFound:Z

    if-nez v15, :cond_250

    .line 11717
    new-instance v15, Ljava/io/File;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v17, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "/Download/"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const/16 v17, 0x0

    aget-object v17, p1, v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-direct/range {v15 .. v16}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v15}, Ljava/io/File;->delete()Z

    goto/16 :goto_250
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 11622
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadFile;->doInBackground([Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .registers 8
    .param p1, "result"    # Ljava/lang/Boolean;

    .prologue
    const v5, 0x7f0700c0

    const v4, 0x7f070234

    .line 11733
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 11734
    const/16 v1, 0x17

    invoke-static {v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->removeDialogLP(I)V

    .line 11735
    iget-boolean v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadFile;->corruptdownload:Z

    if-eqz v1, :cond_1f

    .line 11736
    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadFile;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-static {v4}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v5}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->showMessage(Ljava/lang/String;Ljava/lang/String;)V

    .line 11738
    :cond_1f
    iget-boolean v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadFile;->internet_not_found:Z

    if-eqz v1, :cond_37

    iget-boolean v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadFile;->cacheFound:Z

    if-nez v1, :cond_37

    .line 11739
    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadFile;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-static {v4}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f070141

    invoke-static {v3}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->showMessage(Ljava/lang/String;Ljava/lang/String;)V

    .line 11741
    :cond_37
    iget-boolean v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadFile;->cacheFound:Z

    if-eqz v1, :cond_7f

    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/Download/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadFile;->filename:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_7f

    .line 11743
    :try_start_61
    iget-boolean v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadFile;->cacheFound:Z

    if-eqz v1, :cond_72

    iget-boolean v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadFile;->downloadedFound:Z

    if-nez v1, :cond_72

    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadFile;->on_post_run_cached:Ljava/lang/Runnable;

    if-eqz v1, :cond_72

    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadFile;->on_post_run_cached:Ljava/lang/Runnable;

    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    .line 11744
    :cond_72
    iget-boolean v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadFile;->downloadedFound:Z

    if-eqz v1, :cond_7f

    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadFile;->on_post_run_downloaded:Ljava/lang/Runnable;

    if-eqz v1, :cond_7f

    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadFile;->on_post_run_downloaded:Ljava/lang/Runnable;

    invoke-interface {v1}, Ljava/lang/Runnable;->run()V
    :try_end_7f
    .catch Ljava/lang/Exception; {:try_start_61 .. :try_end_7f} :catch_80

    .line 11752
    :cond_7f
    :goto_7f
    return-void

    .line 11745
    :catch_80
    move-exception v0

    .line 11746
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 11747
    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadFile;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-static {v4}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v5}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->showMessage(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_7f
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 2

    .prologue
    .line 11622
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadFile;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected onPreExecute()V
    .registers 2

    .prologue
    .line 11633
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 11634
    const/16 v0, 0x17

    invoke-static {v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->showDialogLP(I)V

    .line 11635
    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Integer;)V
    .registers 4
    .param p1, "values"    # [Ljava/lang/Integer;

    .prologue
    .line 11725
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    .line 11727
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->progress_loading:Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;

    if-eqz v0, :cond_1b

    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->progress_loading:Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;

    invoke-virtual {v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 11728
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->progress_loading:Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;

    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->setProgress(I)V

    .line 11729
    :cond_1b
    return-void
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .registers 2

    .prologue
    .line 11622
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadFile;->onProgressUpdate([Ljava/lang/Integer;)V

    return-void
.end method
