.class Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$26;
.super Ljava/lang/Object;
.source "SetPrefs.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field dialog6:Landroid/app/Dialog;

.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;)V
    .registers 2
    .param p1, "this$0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;

    .prologue
    .line 983
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$26;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .registers 11
    .param p1, "preference"    # Landroid/preference/Preference;

    .prologue
    const/4 v8, 0x1

    const/high16 v7, -0x1000000

    .line 987
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    .line 988
    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$26;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;

    invoke-virtual {v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/AssetManager;->getLocales()[Ljava/lang/String;

    move-result-object v2

    .line 989
    .local v2, "langs":[Ljava/lang/String;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 990
    .local v4, "s":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v5, "en_US"

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 991
    const-string v5, "ar"

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 992
    const-string v5, "bg"

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 993
    const-string v5, "cs"

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 994
    const-string v5, "da"

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 995
    const-string v5, "de"

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 996
    const-string v5, "el"

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 997
    const-string v5, "es"

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 998
    const-string v5, "fa"

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 999
    const-string v5, "fi"

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1000
    const-string v5, "fr"

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1001
    const-string v5, "he"

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1002
    const-string v5, "hi"

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1003
    const-string v5, "hu"

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1004
    const-string v5, "id"

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1005
    const-string v5, "in"

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1006
    const-string v5, "it"

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1007
    const-string v5, "iw"

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1008
    const-string v5, "ja"

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1009
    const-string v5, "km"

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1010
    const-string v5, "km_KH"

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1011
    const-string v5, "ko"

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1012
    const-string v5, "lt"

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1013
    const-string v5, "ml"

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1014
    const-string v5, "my"

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1015
    const-string v5, "nl"

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1016
    const-string v5, "pl"

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1017
    const-string v5, "pt"

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1018
    const-string v5, "pt_BR"

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1019
    const-string v5, "ro"

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1020
    const-string v5, "ru"

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1021
    const-string v5, "sk"

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1022
    const-string v5, "tr"

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1023
    const-string v5, "ug"

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1024
    const-string v5, "uk"

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1025
    const-string v5, "vi"

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1026
    const-string v5, "zh_CN"

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1027
    const-string v5, "zh_HK"

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1028
    const-string v5, "zh_TW"

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1057
    new-instance v0, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$26$1;

    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$26;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;

    iget-object v5, v5, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->context:Landroid/content/Context;

    const v6, 0x7f04001d

    invoke-direct {v0, p0, v5, v6, v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$26$1;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$26;Landroid/content/Context;ILjava/util/List;)V

    .line 1073
    .local v0, "adapt2":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    new-instance v3, Landroid/widget/ListView;

    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$26;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;

    iget-object v5, v5, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->context:Landroid/content/Context;

    invoke-direct {v3, v5}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    .line 1074
    .local v3, "modeList":Landroid/widget/ListView;
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$26;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;

    iget-object v5, v5, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->context:Landroid/content/Context;

    invoke-direct {v1, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1076
    .local v1, "builder9":Landroid/app/AlertDialog$Builder;
    if-eqz v0, :cond_112

    .line 1078
    invoke-virtual {v0, v8}, Landroid/widget/ArrayAdapter;->setNotifyOnChange(Z)V

    .line 1079
    invoke-virtual {v3, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1080
    invoke-virtual {v3}, Landroid/widget/ListView;->invalidateViews()V

    .line 1081
    invoke-virtual {v3, v7}, Landroid/widget/ListView;->setCacheColorHint(I)V

    .line 1082
    invoke-virtual {v3, v7}, Landroid/widget/ListView;->setBackgroundColor(I)V

    .line 1083
    new-instance v5, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$26$2;

    invoke-direct {v5, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$26$2;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$26;)V

    invoke-virtual {v3, v5}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1115
    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 1119
    :cond_112
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v5

    iput-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$26;->dialog6:Landroid/app/Dialog;

    .line 1121
    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$26;->dialog6:Landroid/app/Dialog;

    invoke-virtual {v5}, Landroid/app/Dialog;->show()V

    .line 1124
    return v8
.end method
