.class final Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$1;
.super Ljava/lang/Object;
.source "LivepatchActivity.java"

# interfaces
.implements Landroid/text/InputFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private getPattern()Ljava/lang/String;
    .registers 3

    .prologue
    .line 126
    new-instance v1, Ljava/text/DecimalFormatSymbols;

    invoke-direct {v1}, Ljava/text/DecimalFormatSymbols;-><init>()V

    .line 128
    const-string v0, "([0-9A-Fa-f*?]){2}||([0-9A-Fa-f*?]){1}"

    .line 130
    .local v0, "pattern":Ljava/lang/String;
    return-object v0
.end method


# virtual methods
.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .registers 14
    .param p1, "source"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "end"    # I
    .param p4, "dest"    # Landroid/text/Spanned;
    .param p5, "dstart"    # I
    .param p6, "dend"    # I

    .prologue
    .line 93
    :try_start_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 95
    .local v0, "checkedText":Ljava/lang/String;
    invoke-direct {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/LivepatchActivity$1;->getPattern()Ljava/lang/String;

    move-result-object v3

    .line 97
    .local v3, "pattern":Ljava/lang/String;
    const-string v5, "([\\s]){2}"

    invoke-static {v5}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    .line 98
    .local v4, "pattern2":Ljava/util/regex/Pattern;
    invoke-virtual {v4, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 99
    .local v2, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z

    move-result v5

    if-eqz v5, :cond_30

    .line 100
    const-string v5, ""

    .line 121
    .end local v0    # "checkedText":Ljava/lang/String;
    .end local v2    # "matcher":Ljava/util/regex/Matcher;
    .end local v3    # "pattern":Ljava/lang/String;
    .end local v4    # "pattern2":Ljava/util/regex/Pattern;
    :goto_2f
    return-object v5

    .line 102
    .restart local v0    # "checkedText":Ljava/lang/String;
    .restart local v2    # "matcher":Ljava/util/regex/Matcher;
    .restart local v3    # "pattern":Ljava/lang/String;
    .restart local v4    # "pattern2":Ljava/util/regex/Pattern;
    :cond_30
    const-string v5, "([\\s]){2}"

    invoke-static {v5, v0}, Ljava/util/regex/Pattern;->matches(Ljava/lang/String;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3b

    .line 105
    const-string v5, ""
    :try_end_3a
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_3a} :catch_3d

    goto :goto_2f

    .line 119
    :cond_3b
    const/4 v5, 0x0

    goto :goto_2f

    .line 120
    .end local v0    # "checkedText":Ljava/lang/String;
    .end local v2    # "matcher":Ljava/util/regex/Matcher;
    .end local v3    # "pattern":Ljava/lang/String;
    .end local v4    # "pattern2":Ljava/util/regex/Pattern;
    :catch_3d
    move-exception v1

    .line 121
    .local v1, "e":Ljava/lang/Exception;
    const-string v5, ""

    goto :goto_2f
.end method
