.class Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter$1;
.super Ljava/lang/Object;
.source "PkgListItemAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->getGroupView(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;)V
    .registers 2
    .param p1, "this$0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;

    .prologue
    .line 219
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter$1;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const v3, 0x7f0d00d3

    .line 222
    iget-object v2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter$1;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v2, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->getGroup(I)Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    move-result-object v0

    .line 223
    .local v0, "it":Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    iget-boolean v1, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->selected:Z

    if-nez v1, :cond_51

    .line 224
    iget-object v2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter$1;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v2, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->getGroup(I)Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    move-result-object v1

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->selected:Z

    .line 225
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    iget-boolean v2, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->selected:Z

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Z)V

    .line 226
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->selectedApps:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_46

    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patchAct:Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;

    invoke-virtual {v1, v3}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    sget v2, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->adapterSelectType:I

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(I)V

    .line 227
    :cond_46
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->selectedApps:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 236
    :goto_4b
    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter$1;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;

    invoke-virtual {v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->notifyDataSetChanged()V

    .line 238
    return-void

    .line 230
    :cond_51
    iget-object v2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter$1;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v2, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->getGroup(I)Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    move-result-object v1

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->selected:Z

    .line 231
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    iget-boolean v2, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->selected:Z

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Z)V

    .line 232
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->selectedApps:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 233
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->selectedApps:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_8a

    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patchAct:Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;

    invoke-virtual {v1, v3}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    const v2, 0x7f070040

    invoke-static {v2}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 234
    :cond_8a
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    sget-object v2, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->selectedApps:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(I)V

    goto :goto_4b
.end method
