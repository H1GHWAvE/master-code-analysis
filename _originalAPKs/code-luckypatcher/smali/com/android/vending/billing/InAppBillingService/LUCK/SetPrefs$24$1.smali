.class Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$24$1;
.super Landroid/os/AsyncTask;
.source "SetPrefs.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$24;->onPreferenceClick(Landroid/preference/Preference;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field pd:Landroid/app/ProgressDialog;

.field final synthetic this$1:Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$24;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$24;)V
    .registers 2
    .param p1, "this$1"    # Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$24;

    .prologue
    .line 878
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$24$1;->this$1:Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$24;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .registers 6
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 883
    :try_start_0
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->mLogCollector:Lcom/android/vending/billing/InAppBillingService/LUCK/LogCollector;

    if-nez v1, :cond_b

    new-instance v1, Lcom/android/vending/billing/InAppBillingService/LUCK/LogCollector;

    invoke-direct {v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/LogCollector;-><init>()V

    sput-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->mLogCollector:Lcom/android/vending/billing/InAppBillingService/LUCK/LogCollector;

    .line 884
    :cond_b
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->mLogCollector:Lcom/android/vending/billing/InAppBillingService/LUCK/LogCollector;

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/android/vending/billing/InAppBillingService/LUCK/LogCollector;->collect(Landroid/content/Context;Z)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_19
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_19} :catch_1b

    move-result-object v1

    .line 887
    :goto_1a
    return-object v1

    .line 885
    :catch_1b
    move-exception v0

    .line 886
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 887
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_1a
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 878
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$24$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .registers 12
    .param p1, "result"    # Ljava/lang/Boolean;

    .prologue
    .line 905
    :try_start_0
    iget-object v2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$24$1;->pd:Landroid/app/ProgressDialog;

    if-eqz v2, :cond_11

    iget-object v2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$24$1;->pd:Landroid/app/ProgressDialog;

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_11

    iget-object v2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$24$1;->pd:Landroid/app/ProgressDialog;

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_11} :catch_4d

    .line 907
    :cond_11
    :goto_11
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_57

    .line 909
    :try_start_17
    sget-object v2, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->mLogCollector:Lcom/android/vending/billing/InAppBillingService/LUCK/LogCollector;

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v3

    const-string v4, "lp.chelpus@gmail.com"

    const-string v5, "Error Log"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Lucky Patcher "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v7

    sget-object v8, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v8}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v8

    invoke-virtual {v8}, Landroid/support/v4/app/FragmentActivity;->getPackageName()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v7

    iget-object v7, v7, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/LogCollector;->sendLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4c
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_17 .. :try_end_4c} :catch_52

    .line 925
    :goto_4c
    return-void

    .line 906
    :catch_4d
    move-exception v0

    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_11

    .line 910
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_52
    move-exception v0

    .line 912
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_4c

    .line 916
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_57
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$24$1;->this$1:Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$24;

    iget-object v2, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$24;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;

    iget-object v2, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->context:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 917
    .local v1, "logbuilder":Landroid/app/AlertDialog$Builder;
    const-string v2, "Error"

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0700f9

    .line 918
    invoke-static {v3}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const-string v3, "OK"

    const/4 v4, 0x0

    .line 919
    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 920
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    goto :goto_4c
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 2

    .prologue
    .line 878
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$24$1;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected onPreExecute()V
    .registers 3

    .prologue
    .line 894
    new-instance v0, Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$24$1;->this$1:Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$24;

    iget-object v1, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$24;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;

    iget-object v1, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->context:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$24$1;->pd:Landroid/app/ProgressDialog;

    .line 895
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$24$1;->pd:Landroid/app/ProgressDialog;

    const-string v1, "Progress"

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 896
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$24$1;->pd:Landroid/app/ProgressDialog;

    const v1, 0x7f070047

    invoke-static {v1}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 897
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$24$1;->pd:Landroid/app/ProgressDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 898
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$24$1;->pd:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 900
    return-void
.end method
