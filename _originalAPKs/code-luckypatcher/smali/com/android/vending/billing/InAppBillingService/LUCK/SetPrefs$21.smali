.class Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$21;
.super Ljava/lang/Object;
.source "SetPrefs.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field progress:Landroid/app/ProgressDialog;

.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;)V
    .registers 3
    .param p1, "this$0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;

    .prologue
    .line 761
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$21;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 762
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$21;->progress:Landroid/app/ProgressDialog;

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .registers 16
    .param p1, "arg0"    # Landroid/preference/Preference;
    .param p2, "newValue"    # Ljava/lang/Object;

    .prologue
    .line 766
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v9

    const-string v10, "config"

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v5

    .line 768
    .local v5, "settings":Landroid/content/SharedPreferences;
    iget-object v9, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$21;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;

    const-string v10, "path"

    invoke-virtual {v9, v10}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v9

    if-ne p1, v9, :cond_138

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v10, "/"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_138

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v10, "/"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_138

    .line 770
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    const-string v10, "\\s+"

    const-string v11, "."

    invoke-virtual {v9, v10, v11}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v10, "\\/+"

    invoke-virtual {v9, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 771
    .local v8, "tailsdir":[Ljava/lang/String;
    const-string v2, ""

    .line 772
    .local v2, "data_dirs":Ljava/lang/String;
    array-length v10, v8

    const/4 v9, 0x0

    :goto_47
    if-ge v9, v10, :cond_6d

    aget-object v0, v8, v9

    .line 773
    .local v0, "aTailsdir":Ljava/lang/String;
    const-string v11, ""

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_6a

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 772
    :cond_6a
    add-int/lit8 v9, v9, 0x1

    goto :goto_47

    .line 776
    .end local v0    # "aTailsdir":Ljava/lang/String;
    :cond_6d
    move-object v1, v2

    .line 778
    .local v1, "data_dir":Ljava/lang/String;
    new-instance v9, Ljava/io/File;

    invoke-direct {v9, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v9

    if-nez v9, :cond_121

    .line 779
    iget-object v9, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$21;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;

    const/4 v10, 0x1

    invoke-virtual {v9, v10, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->testPath(ZLjava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_136

    iget-object v9, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$21;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;

    const-string v10, "sdcard"

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v9

    invoke-virtual {v9}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_136

    .line 780
    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v9

    const-string v10, "path"

    invoke-interface {v9, v10, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v9

    invoke-interface {v9}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 781
    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v9

    const-string v10, "manual_path"

    const/4 v11, 0x1

    invoke-interface {v9, v10, v11}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v9

    invoke-interface {v9}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 782
    invoke-virtual {p1}, Landroid/preference/Preference;->getEditor()Landroid/content/SharedPreferences$Editor;

    move-result-object v9

    const-string v10, "path"

    invoke-interface {v9, v10, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v9

    invoke-interface {v9}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 783
    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v9

    const-string v10, "path_changed"

    const/4 v11, 0x1

    invoke-interface {v9, v10, v11}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v9

    invoke-interface {v9}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 786
    new-instance v6, Ljava/io/File;

    const-string v9, "basepath"

    const-string v10, "Noting"

    invoke-interface {v5, v9, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v6, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 787
    .local v6, "srcFolder":Ljava/io/File;
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 788
    .local v3, "destFolder":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v9

    if-nez v9, :cond_ec

    .line 789
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Directory does not exist."

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 838
    :goto_ea
    const/4 v9, 0x1

    .line 849
    .end local v1    # "data_dir":Ljava/lang/String;
    .end local v2    # "data_dirs":Ljava/lang/String;
    .end local v3    # "destFolder":Ljava/io/File;
    .end local v6    # "srcFolder":Ljava/io/File;
    .end local v8    # "tailsdir":[Ljava/lang/String;
    :goto_eb
    return v9

    .line 792
    .restart local v1    # "data_dir":Ljava/lang/String;
    .restart local v2    # "data_dirs":Ljava/lang/String;
    .restart local v3    # "destFolder":Ljava/io/File;
    .restart local v6    # "srcFolder":Ljava/io/File;
    .restart local v8    # "tailsdir":[Ljava/lang/String;
    :cond_ec
    new-instance v9, Landroid/app/ProgressDialog;

    iget-object v10, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$21;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;

    invoke-direct {v9, v10}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v9, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$21;->progress:Landroid/app/ProgressDialog;

    .line 793
    iget-object v9, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$21;->progress:Landroid/app/ProgressDialog;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 794
    iget-object v9, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$21;->progress:Landroid/app/ProgressDialog;

    iget-object v10, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$21;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;

    const v11, 0x7f070233

    invoke-virtual {v10, v11}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 795
    iget-object v9, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$21;->progress:Landroid/app/ProgressDialog;

    invoke-virtual {v9}, Landroid/app/ProgressDialog;->show()V

    .line 796
    new-instance v4, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$21$1;

    invoke-direct {v4, p0, v5, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$21$1;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$21;Landroid/content/SharedPreferences;Ljava/lang/String;)V

    .line 818
    .local v4, "handler":Landroid/os/Handler;
    new-instance v7, Ljava/lang/Thread;

    new-instance v9, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$21$2;

    invoke-direct {v9, p0, v6, v3, v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$21$2;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$21;Ljava/io/File;Ljava/io/File;Landroid/os/Handler;)V

    invoke-direct {v7, v9}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 835
    .local v7, "t":Ljava/lang/Thread;
    invoke-virtual {v7}, Ljava/lang/Thread;->start()V

    goto :goto_ea

    .line 844
    .end local v3    # "destFolder":Ljava/io/File;
    .end local v4    # "handler":Landroid/os/Handler;
    .end local v6    # "srcFolder":Ljava/io/File;
    .end local v7    # "t":Ljava/lang/Thread;
    :cond_121
    iget-object v9, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$21;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;

    iget-object v10, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$21;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;

    const v11, 0x7f070234

    invoke-static {v11}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v11

    const v12, 0x7f070105

    invoke-static {v12}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v12

    # invokes: Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->showMessage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v9, v10, v11, v12}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->access$000(Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 849
    .end local v1    # "data_dir":Ljava/lang/String;
    .end local v2    # "data_dirs":Ljava/lang/String;
    .end local v8    # "tailsdir":[Ljava/lang/String;
    :cond_136
    :goto_136
    const/4 v9, 0x0

    goto :goto_eb

    .line 847
    :cond_138
    iget-object v9, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$21;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;

    iget-object v10, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$21;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;

    const v11, 0x7f070234

    invoke-static {v11}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v11

    const v12, 0x7f070104

    invoke-static {v12}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v12

    # invokes: Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->showMessage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v9, v10, v11, v12}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->access$000(Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_136
.end method
