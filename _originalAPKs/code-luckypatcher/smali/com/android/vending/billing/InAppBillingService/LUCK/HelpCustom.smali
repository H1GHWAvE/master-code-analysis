.class public Lcom/android/vending/billing/InAppBillingService/LUCK/HelpCustom;
.super Landroid/app/Activity;
.source "HelpCustom.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 11
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .registers 11
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 14
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 15
    new-instance v5, Landroid/webkit/WebView;

    invoke-direct {v5, p0}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    .line 16
    .local v5, "webview":Landroid/webkit/WebView;
    invoke-virtual {p0, v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/HelpCustom;->setContentView(Landroid/view/View;)V

    .line 17
    invoke-virtual {v5}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v6

    const/16 v7, 0xe

    invoke-virtual {v6, v7}, Landroid/webkit/WebSettings;->setDefaultFontSize(I)V

    .line 18
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/HelpCustom;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f060009

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v3

    .line 19
    .local v3, "inputStream":Ljava/io/InputStream;
    const/16 v6, 0x200

    new-array v1, v6, [B

    .line 20
    .local v1, "buffer":[B
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 22
    .local v0, "baos":Ljava/io/ByteArrayOutputStream;
    const/4 v4, 0x0

    .line 24
    .local v4, "len1":I
    :goto_29
    :try_start_29
    invoke-virtual {v3, v1}, Ljava/io/InputStream;->read([B)I

    move-result v4

    const/4 v6, -0x1

    if-eq v4, v6, :cond_3a

    .line 25
    const/4 v6, 0x0

    invoke-virtual {v0, v1, v6, v4}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_34
    .catch Ljava/io/IOException; {:try_start_29 .. :try_end_34} :catch_35

    goto :goto_29

    .line 29
    :catch_35
    move-exception v2

    .line 31
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 35
    .end local v2    # "e":Ljava/io/IOException;
    :goto_39
    return-void

    .line 27
    :cond_3a
    :try_start_3a
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v7, "text/html; charset=UTF-8"

    const/4 v8, 0x0

    invoke-virtual {v5, v6, v7, v8}, Landroid/webkit/WebView;->loadData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_44
    .catch Ljava/io/IOException; {:try_start_3a .. :try_end_44} :catch_35

    goto :goto_39
.end method
