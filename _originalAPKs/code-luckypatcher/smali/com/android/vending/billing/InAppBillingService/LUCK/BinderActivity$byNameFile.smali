.class final Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$byNameFile;
.super Ljava/lang/Object;
.source "BinderActivity.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "byNameFile"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$ItemFile;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;


# direct methods
.method private constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;)V
    .registers 2

    .prologue
    .line 715
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$byNameFile;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$1;)V
    .registers 3
    .param p1, "x0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;
    .param p2, "x1"    # Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$1;

    .prologue
    .line 715
    invoke-direct {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$byNameFile;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity;)V

    return-void
.end method


# virtual methods
.method public compare(Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$ItemFile;Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$ItemFile;)I
    .registers 6
    .param p1, "a"    # Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$ItemFile;
    .param p2, "b"    # Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$ItemFile;

    .prologue
    .line 718
    if-eqz p1, :cond_4

    if-nez p2, :cond_d

    .line 719
    :cond_4
    :try_start_4
    new-instance v1, Ljava/lang/ClassCastException;

    invoke-direct {v1}, Ljava/lang/ClassCastException;-><init>()V

    throw v1

    .line 733
    :catch_a
    move-exception v0

    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    .line 734
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_c
    return v1

    .line 721
    :cond_d
    new-instance v1, Ljava/io/File;

    iget-object v2, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$ItemFile;->full:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_30

    new-instance v1, Ljava/io/File;

    iget-object v2, p2, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$ItemFile;->full:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_30

    .line 722
    iget-object v1, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$ItemFile;->full:Ljava/lang/String;

    iget-object v2, p2, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$ItemFile;->full:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    goto :goto_c

    .line 724
    :cond_30
    new-instance v1, Ljava/io/File;

    iget-object v2, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$ItemFile;->full:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_4c

    new-instance v1, Ljava/io/File;

    iget-object v2, p2, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$ItemFile;->full:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-nez v1, :cond_4c

    .line 725
    const/4 v1, -0x1

    goto :goto_c

    .line 727
    :cond_4c
    new-instance v1, Ljava/io/File;

    iget-object v2, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$ItemFile;->full:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-nez v1, :cond_6f

    new-instance v1, Ljava/io/File;

    iget-object v2, p2, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$ItemFile;->full:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-nez v1, :cond_6f

    .line 728
    iget-object v1, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$ItemFile;->full:Ljava/lang/String;

    iget-object v2, p2, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$ItemFile;->full:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    goto :goto_c

    .line 730
    :cond_6f
    new-instance v1, Ljava/io/File;

    iget-object v2, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$ItemFile;->full:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-nez v1, :cond_8b

    new-instance v1, Ljava/io/File;

    iget-object v2, p2, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$ItemFile;->full:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z
    :try_end_86
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_86} :catch_a

    move-result v1

    if-eqz v1, :cond_8b

    .line 731
    const/4 v1, 0x1

    goto :goto_c

    .line 734
    :cond_8b
    iget-object v1, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$ItemFile;->full:Ljava/lang/String;

    iget-object v2, p2, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$ItemFile;->full:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    goto/16 :goto_c
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 4

    .prologue
    .line 715
    check-cast p1, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$ItemFile;

    check-cast p2, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$ItemFile;

    invoke-virtual {p0, p1, p2}, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$byNameFile;->compare(Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$ItemFile;Lcom/android/vending/billing/InAppBillingService/LUCK/BinderActivity$ItemFile;)I

    move-result v0

    return v0
.end method
