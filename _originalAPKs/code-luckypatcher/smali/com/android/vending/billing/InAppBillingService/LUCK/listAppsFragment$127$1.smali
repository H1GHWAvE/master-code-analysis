.class Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$127$1;
.super Ljava/lang/Object;
.source "listAppsFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$127;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$127;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$127;)V
    .registers 2
    .param p1, "this$1"    # Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$127;

    .prologue
    .line 16252
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$127$1;->this$1:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$127;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 12

    .prologue
    .line 16255
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 16256
    .local v2, "commands":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v6, ""

    .line 16257
    .local v6, "statics":Ljava/lang/String;
    sget-boolean v7, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->su:Z

    if-eqz v7, :cond_ae

    .line 16258
    const-string v6, "pm install -r -s -i com.android.vending "

    .line 16260
    :goto_d
    new-instance v5, Ljava/util/ArrayList;

    sget-object v7, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->selectedApps:Ljava/util/ArrayList;

    invoke-direct {v5, v7}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 16261
    .local v5, "selApps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;>;"
    sget-object v7, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->selectedApps:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->clear()V

    .line 16263
    :try_start_19
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_1d
    :goto_1d
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_89

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    .line 16264
    .local v4, "item":Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    const-string v0, ""
    :try_end_2b
    .catch Ljava/lang/Exception; {:try_start_19 .. :try_end_2b} :catch_85

    .line 16266
    .local v0, "apk_file":Ljava/lang/String;
    :try_start_2b
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v8

    iget-object v9, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v8

    iget-object v8, v8, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v8, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    .line 16267
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "chmod 644 "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 16268
    sget-object v8, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Start move to sdcard"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_68
    .catch Ljava/lang/Exception; {:try_start_2b .. :try_end_68} :catch_b6

    .line 16271
    :goto_68
    :try_start_68
    const-string v8, ""

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_1d

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_84
    .catch Ljava/lang/Exception; {:try_start_68 .. :try_end_84} :catch_85

    goto :goto_1d

    .line 16273
    .end local v0    # "apk_file":Ljava/lang/String;
    .end local v4    # "item":Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    :catch_85
    move-exception v3

    .line 16274
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    .line 16276
    .end local v3    # "e":Ljava/lang/Exception;
    :cond_89
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lez v7, :cond_a6

    .line 16277
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v7

    new-array v1, v7, [Ljava/lang/String;

    .line 16278
    .local v1, "com":[Ljava/lang/String;
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 16279
    sget-boolean v7, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->su:Z

    if-eqz v7, :cond_b2

    new-instance v7, Lcom/chelpus/Utils;

    const-string v8, ""

    invoke-direct {v7, v8}, Lcom/chelpus/Utils;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Lcom/chelpus/Utils;->cmdRoot([Ljava/lang/String;)Ljava/lang/String;

    .line 16282
    .end local v1    # "com":[Ljava/lang/String;
    :cond_a6
    :goto_a6
    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$127$1;->this$1:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$127;

    iget-object v7, v7, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$127;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v7}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->resetBatchOperation()V

    .line 16284
    return-void

    .line 16259
    .end local v5    # "selApps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;>;"
    :cond_ae
    const-string v6, "pm install -r -s "

    goto/16 :goto_d

    .line 16280
    .restart local v1    # "com":[Ljava/lang/String;
    .restart local v5    # "selApps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;>;"
    :cond_b2
    invoke-static {v1}, Lcom/chelpus/Utils;->cmd([Ljava/lang/String;)Ljava/lang/String;

    goto :goto_a6

    .line 16269
    .end local v1    # "com":[Ljava/lang/String;
    .restart local v0    # "apk_file":Ljava/lang/String;
    .restart local v4    # "item":Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    :catch_b6
    move-exception v8

    goto :goto_68
.end method
