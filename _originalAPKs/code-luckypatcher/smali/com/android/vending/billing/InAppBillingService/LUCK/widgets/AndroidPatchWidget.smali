.class public Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AndroidPatchWidget;
.super Landroid/appwidget/AppWidgetProvider;
.source "AndroidPatchWidget.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/appwidget/AppWidgetProvider;-><init>()V

    return-void
.end method

.method static updateAppWidget(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;I)V
    .registers 19
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appWidgetManager"    # Landroid/appwidget/AppWidgetManager;
    .param p2, "appWidgetId"    # I

    .prologue
    .line 47
    const v13, 0x7f07025e

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 49
    .local v12, "widgetText":Ljava/lang/CharSequence;
    new-instance v11, Landroid/widget/RemoteViews;

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v13

    const v14, 0x7f040005

    invoke-direct {v11, v13, v14}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 50
    .local v11, "views":Landroid/widget/RemoteViews;
    const/4 v3, 0x0

    .line 51
    .local v3, "i":I
    const/4 v7, 0x0

    .local v7, "patch1":Z
    const/4 v8, 0x0

    .local v8, "patch2":Z
    const/4 v9, 0x0

    .line 52
    .local v9, "patch3":Z
    const-string v10, ""

    .line 53
    .local v10, "str":Ljava/lang/String;
    invoke-static {}, Lcom/chelpus/Utils;->checkCoreJarPatch11()Z

    move-result v13

    if-eqz v13, :cond_24

    .line 54
    add-int/lit8 v3, v3, 0x1

    .line 55
    const/4 v7, 0x1

    .line 57
    :cond_24
    invoke-static {}, Lcom/chelpus/Utils;->checkCoreJarPatch12()Z

    move-result v13

    if-eqz v13, :cond_2d

    .line 58
    add-int/lit8 v3, v3, 0x1

    .line 59
    const/4 v7, 0x1

    .line 61
    :cond_2d
    if-lez v3, :cond_16a

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const v14, 0x7f070074

    invoke-static {v14}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "\n("

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "/2 patched)"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 63
    :goto_53
    invoke-static {}, Lcom/chelpus/Utils;->checkCoreJarPatch20()Z

    move-result v13

    if-eqz v13, :cond_186

    .line 64
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "\n"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const v14, 0x7f070076

    invoke-static {v14}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "\n(patched)"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 65
    const/4 v8, 0x1

    .line 68
    :goto_7e
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v13

    invoke-static {v13}, Lcom/chelpus/Utils;->checkCoreJarPatch30(Landroid/content/pm/PackageManager;)Z

    move-result v13

    if-eqz v13, :cond_1ac

    .line 69
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "\n"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const v14, 0x7f070078

    invoke-static {v14}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "\n(patched)"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 70
    const/4 v9, 0x1

    .line 73
    :goto_ad
    const/4 v4, 0x0

    .line 74
    .local v4, "offset":I
    new-instance v2, Landroid/text/SpannableString;

    invoke-direct {v2, v10}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 75
    .local v2, "color_str":Landroid/text/SpannableString;
    if-eqz v7, :cond_1d2

    .line 76
    const v13, 0x7f070074

    invoke-static {v13}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v13

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, " ("

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "/2 patched)"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v14

    add-int v4, v13, v14

    .line 77
    new-instance v13, Landroid/text/style/ForegroundColorSpan;

    const v14, -0xff0100

    invoke-direct {v13, v14}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const v14, 0x7f070074

    invoke-static {v14}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v14

    const/4 v15, 0x0

    invoke-virtual {v2, v13, v14, v4, v15}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 83
    :goto_f6
    const/4 v5, 0x0

    .line 84
    .local v5, "offset2":I
    if-eqz v8, :cond_1fd

    .line 85
    const v13, 0x7f070076

    invoke-static {v13}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v13

    add-int/2addr v13, v4

    add-int/lit8 v13, v13, 0x1

    const-string v14, " (patched)"

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v14

    add-int v5, v13, v14

    .line 86
    new-instance v13, Landroid/text/style/ForegroundColorSpan;

    const v14, -0xff0100

    invoke-direct {v13, v14}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const v14, 0x7f070076

    invoke-static {v14}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v14

    add-int/2addr v14, v4

    add-int/lit8 v14, v14, 0x1

    const/4 v15, 0x0

    invoke-virtual {v2, v13, v14, v5, v15}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 92
    :goto_129
    const/4 v6, 0x0

    .line 93
    .local v6, "offset3":I
    if-eqz v9, :cond_22e

    .line 94
    const v13, 0x7f070078

    invoke-static {v13}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v13

    add-int/2addr v13, v5

    add-int/lit8 v13, v13, 0x1

    const-string v14, " (patched)"

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v14

    add-int v6, v13, v14

    .line 95
    new-instance v13, Landroid/text/style/ForegroundColorSpan;

    const v14, -0xff0100

    invoke-direct {v13, v14}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const v14, 0x7f070078

    invoke-static {v14}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v14

    add-int/2addr v14, v5

    add-int/lit8 v14, v14, 0x1

    const/4 v15, 0x0

    invoke-virtual {v2, v13, v14, v6, v15}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 102
    :goto_15c
    const v13, 0x7f0d002a

    invoke-virtual {v11, v13, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 105
    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-virtual {v0, v1, v11}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 106
    return-void

    .line 62
    .end local v2    # "color_str":Landroid/text/SpannableString;
    .end local v4    # "offset":I
    .end local v5    # "offset2":I
    .end local v6    # "offset3":I
    :cond_16a
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const v14, 0x7f070074

    invoke-static {v14}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "\n(not patched)"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_53

    .line 67
    :cond_186
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "\n"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const v14, 0x7f070076

    invoke-static {v14}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "\n(not patched)"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_7e

    .line 72
    :cond_1ac
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "\n"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const v14, 0x7f070078

    invoke-static {v14}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "\n(not patched)"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_ad

    .line 80
    .restart local v2    # "color_str":Landroid/text/SpannableString;
    .restart local v4    # "offset":I
    :cond_1d2
    const v13, 0x7f070074

    invoke-static {v13}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v13

    const-string v14, " (not patched)"

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v14

    add-int v4, v13, v14

    .line 81
    new-instance v13, Landroid/text/style/ForegroundColorSpan;

    const/high16 v14, -0x10000

    invoke-direct {v13, v14}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const v14, 0x7f070074

    invoke-static {v14}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v14

    const/4 v15, 0x0

    invoke-virtual {v2, v13, v14, v4, v15}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    goto/16 :goto_f6

    .line 89
    .restart local v5    # "offset2":I
    :cond_1fd
    const v13, 0x7f070076

    invoke-static {v13}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v13

    add-int/lit8 v13, v13, 0x1

    add-int/2addr v13, v4

    const-string v14, " (not patched)"

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v14

    add-int v5, v13, v14

    .line 90
    new-instance v13, Landroid/text/style/ForegroundColorSpan;

    const/high16 v14, -0x10000

    invoke-direct {v13, v14}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const v14, 0x7f070076

    invoke-static {v14}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v14

    add-int/lit8 v14, v14, 0x1

    add-int/2addr v14, v4

    const/4 v15, 0x0

    invoke-virtual {v2, v13, v14, v5, v15}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    goto/16 :goto_129

    .line 98
    .restart local v6    # "offset3":I
    :cond_22e
    const v13, 0x7f070078

    invoke-static {v13}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v13

    add-int/2addr v13, v5

    add-int/lit8 v13, v13, 0x1

    const-string v14, " (not patched)"

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v14

    add-int v6, v13, v14

    .line 99
    sget-object v13, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v13, v4}, Ljava/io/PrintStream;->println(I)V

    sget-object v13, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v13, v5}, Ljava/io/PrintStream;->println(I)V

    sget-object v13, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v14

    invoke-virtual {v13, v14}, Ljava/io/PrintStream;->println(I)V

    .line 100
    new-instance v13, Landroid/text/style/ForegroundColorSpan;

    const/high16 v14, -0x10000

    invoke-direct {v13, v14}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const v14, 0x7f070078

    invoke-static {v14}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v14

    add-int/2addr v14, v5

    add-int/lit8 v14, v14, 0x1

    const/4 v15, 0x0

    invoke-virtual {v2, v13, v14, v6, v15}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    goto/16 :goto_15c
.end method


# virtual methods
.method public onDisabled(Landroid/content/Context;)V
    .registers 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 42
    return-void
.end method

.method public onEnabled(Landroid/content/Context;)V
    .registers 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 36
    return-void
.end method

.method public onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .registers 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appWidgetManager"    # Landroid/appwidget/AppWidgetManager;
    .param p3, "appWidgetIds"    # [I

    .prologue
    .line 21
    new-instance v2, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f040005

    invoke-direct {v2, v3, v4}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 24
    .local v2, "remoteViews":Landroid/widget/RemoteViews;
    array-length v0, p3

    .line 25
    .local v0, "N":I
    array-length v4, p3

    const/4 v3, 0x0

    :goto_f
    if-ge v3, v4, :cond_19

    aget v1, p3, v3

    .line 26
    .local v1, "appWidgetId":I
    invoke-static {p1, p2, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AndroidPatchWidget;->updateAppWidget(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;I)V

    .line 25
    add-int/lit8 v3, v3, 0x1

    goto :goto_f

    .line 29
    .end local v1    # "appWidgetId":I
    :cond_19
    return-void
.end method
