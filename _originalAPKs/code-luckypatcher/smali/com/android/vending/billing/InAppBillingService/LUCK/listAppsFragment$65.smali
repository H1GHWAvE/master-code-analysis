.class Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$65;
.super Ljava/lang/Object;
.source "listAppsFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->dexopt_app(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

.field final synthetic val$appdir:Ljava/lang/String;

.field final synthetic val$pkgName:Ljava/lang/String;

.field final synthetic val$uid:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .param p1, "this$0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    .prologue
    .line 8595
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$65;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iput-object p2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$65;->val$appdir:Ljava/lang/String;

    iput-object p3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$65;->val$uid:Ljava/lang/String;

    iput-object p4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$65;->val$pkgName:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 12

    .prologue
    const/16 v10, 0xb

    .line 8601
    :try_start_2
    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$65;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentActivity;->getFilesDir()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    .line 8602
    .local v2, "filesdir":Ljava/lang/String;
    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$65;->val$appdir:Ljava/lang/String;

    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$65;->val$uid:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/chelpus/Utils;->getOdexForCreate(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 8603
    .local v0, "dexopt":Ljava/lang/String;
    const-string v4, ""

    .line 8605
    .local v4, "uid":Ljava/lang/String;
    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$65;->val$appdir:Ljava/lang/String;

    const-string v6, "/system/"

    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1e9

    .line 8606
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v5

    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$65;->val$pkgName:Ljava/lang/String;

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v5

    iget-object v5, v5, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v5, v5, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    .line 8611
    :goto_37
    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$65;->val$appdir:Ljava/lang/String;

    const-string v6, "rw"

    invoke-static {v5, v6}, Lcom/chelpus/Utils;->remount(Ljava/lang/String;Ljava/lang/String;)Z

    .line 8615
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_67

    .line 8616
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    .line 8617
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "rm "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 8619
    :cond_67
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_79

    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v6, "Ne udalos udalit"

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 8620
    :cond_79
    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$65;->val$pkgName:Ljava/lang/String;

    invoke-static {v5}, Lcom/chelpus/Utils;->kill(Ljava/lang/String;)V

    .line 8622
    invoke-static {}, Lcom/chelpus/Utils;->dexoptcopy()V

    .line 8623
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "chown 0.0 "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/dexopt-wrapper"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 8624
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "chown 0:0 "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/dexopt-wrapper"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 8625
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "chmod 777 "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/dexopt-wrapper"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 8627
    new-instance v5, Lcom/chelpus/Utils;

    const-string v6, ""

    invoke-direct {v5, v6}, Lcom/chelpus/Utils;-><init>(Ljava/lang/String;)V

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->dalvikruncommand:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ".optimizedex "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$65;->val$appdir:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {}, Lcom/chelpus/Utils;->getCurrentRuntimeValue()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v5, v6}, Lcom/chelpus/Utils;->cmdRoot([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 8628
    .local v3, "result":Ljava/lang/String;
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v5, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 8629
    const-string v5, "chelpus_return_1"

    invoke-virtual {v3, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_12e

    const-string v5, "chelpus_return_4"

    invoke-virtual {v3, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_164

    .line 8630
    :cond_12e
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    .line 8631
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "rm "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 8632
    const/16 v5, 0xb

    invoke-static {v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->removeDialogLP(I)V

    .line 8633
    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$65;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    const v6, 0x7f070234

    invoke-static {v6}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v6

    const v7, 0x7f070101

    invoke-static {v7}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->showMessage(Ljava/lang/String;Ljava/lang/String;)V

    .line 8636
    :cond_164
    const-string v5, "chelpus_return_10"

    invoke-virtual {v3, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_17b

    .line 8637
    const/16 v5, 0xb

    invoke-static {v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->removeDialogLP(I)V

    .line 8638
    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$65;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v6, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$65$1;

    invoke-direct {v6, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$65$1;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$65;)V

    invoke-virtual {v5, v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V

    .line 8647
    :cond_17b
    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$65;->val$appdir:Ljava/lang/String;

    const-string v6, "/system/"

    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1ed

    .line 8648
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "chmod 0644 "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 8649
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "chown 1000."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 8650
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "chown 1000:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V
    :try_end_1db
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_1db} :catch_230

    .line 8666
    .end local v0    # "dexopt":Ljava/lang/String;
    .end local v2    # "filesdir":Ljava/lang/String;
    .end local v3    # "result":Ljava/lang/String;
    .end local v4    # "uid":Ljava/lang/String;
    :goto_1db
    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$65;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v6, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$65$3;

    invoke-direct {v6, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$65$3;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$65;)V

    invoke-virtual {v5, v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V

    .line 8673
    invoke-static {v10}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->removeDialogLP(I)V

    .line 8674
    return-void

    .line 8608
    .restart local v0    # "dexopt":Ljava/lang/String;
    .restart local v2    # "filesdir":Ljava/lang/String;
    .restart local v4    # "uid":Ljava/lang/String;
    :cond_1e9
    :try_start_1e9
    const-string v4, "0"

    goto/16 :goto_37

    .line 8652
    .restart local v3    # "result":Ljava/lang/String;
    :cond_1ed
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "chmod 0644 "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 8653
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "chown 0.0 "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 8654
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "chown 0:0 "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V
    :try_end_22f
    .catch Ljava/lang/Exception; {:try_start_1e9 .. :try_end_22f} :catch_230

    goto :goto_1db

    .line 8657
    .end local v0    # "dexopt":Ljava/lang/String;
    .end local v2    # "filesdir":Ljava/lang/String;
    .end local v3    # "result":Ljava/lang/String;
    .end local v4    # "uid":Ljava/lang/String;
    :catch_230
    move-exception v1

    .line 8658
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 8659
    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$65;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v6, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$65$2;

    invoke-direct {v6, p0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$65$2;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$65;Ljava/lang/Exception;)V

    invoke-virtual {v5, v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V

    goto :goto_1db
.end method
