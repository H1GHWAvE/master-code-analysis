.class Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$5;
.super Ljava/lang/Object;
.source "SetPrefs.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;)V
    .registers 2
    .param p1, "this$0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;

    .prologue
    .line 271
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$5;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .registers 14
    .param p1, "arg0"    # Landroid/preference/Preference;
    .param p2, "newValue"    # Ljava/lang/Object;

    .prologue
    const/high16 v10, 0x20000

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 274
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v8

    const-string v9, "config"

    invoke-virtual {v8, v9, v7}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v5

    .line 276
    .local v5, "settings":Landroid/content/SharedPreferences;
    iget-object v8, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$5;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;

    const-string v9, "language"

    invoke-virtual {v8, v9}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v8

    if-ne p1, v8, :cond_82

    const-string v8, "1"

    invoke-virtual {p2, v8}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_82

    .line 277
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v7

    iget-object v2, v7, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 279
    .local v2, "appLoc":Ljava/util/Locale;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v7

    invoke-static {v7}, Ljava/util/Locale;->setDefault(Ljava/util/Locale;)V

    .line 280
    new-instance v0, Landroid/content/res/Configuration;

    invoke-direct {v0}, Landroid/content/res/Configuration;-><init>()V

    .line 281
    .local v0, "appConfig":Landroid/content/res/Configuration;
    iput-object v2, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 282
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v7

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v8

    invoke-virtual {v7, v0, v8}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    .line 284
    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    const-string v8, "language"

    invoke-interface {v7, v8, v6}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    invoke-interface {v7}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 285
    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$5;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;

    invoke-virtual {v7}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->getIntent()Landroid/content/Intent;

    move-result-object v4

    .line 287
    .local v4, "intent":Landroid/content/Intent;
    invoke-virtual {v4, v10}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 288
    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$5;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;

    invoke-virtual {v7}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->finish()V

    .line 290
    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$5;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;

    invoke-virtual {v7, v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->startActivity(Landroid/content/Intent;)V

    .line 291
    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    const-string v8, "settings_change"

    invoke-interface {v7, v8, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    invoke-interface {v7}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 292
    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    const-string v8, "lang_change"

    invoke-interface {v7, v8, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    invoke-interface {v7}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 319
    .end local v0    # "appConfig":Landroid/content/res/Configuration;
    .end local v2    # "appLoc":Ljava/util/Locale;
    .end local v4    # "intent":Landroid/content/Intent;
    :goto_81
    return v6

    .line 297
    :cond_82
    iget-object v8, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$5;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;

    const-string v9, "language"

    invoke-virtual {v8, v9}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v8

    if-ne p1, v8, :cond_f0

    const-string v8, "2"

    invoke-virtual {p2, v8}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_f0

    .line 298
    new-instance v3, Ljava/util/Locale;

    const-string v7, "en"

    invoke-direct {v3, v7}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    .line 299
    .local v3, "appLoc2":Ljava/util/Locale;
    invoke-static {v3}, Ljava/util/Locale;->setDefault(Ljava/util/Locale;)V

    .line 300
    new-instance v1, Landroid/content/res/Configuration;

    invoke-direct {v1}, Landroid/content/res/Configuration;-><init>()V

    .line 301
    .local v1, "appConfig2":Landroid/content/res/Configuration;
    iput-object v3, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 302
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v7

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v8

    invoke-virtual {v7, v1, v8}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    .line 304
    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    const-string v8, "language"

    const/4 v9, 0x2

    invoke-interface {v7, v8, v9}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    invoke-interface {v7}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 305
    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$5;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;

    invoke-virtual {v7}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->getIntent()Landroid/content/Intent;

    move-result-object v4

    .line 307
    .restart local v4    # "intent":Landroid/content/Intent;
    invoke-virtual {v4, v10}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 308
    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$5;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;

    invoke-virtual {v7}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->finish()V

    .line 310
    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$5;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;

    invoke-virtual {v7, v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->startActivity(Landroid/content/Intent;)V

    .line 311
    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    const-string v8, "settings_change"

    invoke-interface {v7, v8, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    invoke-interface {v7}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 312
    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    const-string v8, "lang_change"

    invoke-interface {v7, v8, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    invoke-interface {v7}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_81

    .end local v1    # "appConfig2":Landroid/content/res/Configuration;
    .end local v3    # "appLoc2":Ljava/util/Locale;
    .end local v4    # "intent":Landroid/content/Intent;
    :cond_f0
    move v6, v7

    .line 319
    goto :goto_81
.end method
