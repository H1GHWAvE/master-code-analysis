.class Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadVersion;
.super Landroid/os/AsyncTask;
.source "listAppsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DownloadVersion"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field corruptdownload:Z

.field host_down:Z

.field numbytes:I

.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;)V
    .registers 3
    .param p1, "this$0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    .prologue
    const/4 v0, 0x0

    .line 11480
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadVersion;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 11481
    iput v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadVersion;->numbytes:I

    .line 11482
    iput-boolean v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadVersion;->corruptdownload:Z

    .line 11483
    iput-boolean v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadVersion;->host_down:Z

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Boolean;
    .registers 26
    .param p1, "params"    # [Ljava/lang/String;

    .prologue
    .line 11495
    :try_start_0
    new-instance v18, Ljava/net/URL;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadVersion;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->urlpath:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-direct/range {v18 .. v19}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 11496
    invoke-virtual/range {v18 .. v18}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v5

    check-cast v5, Ljava/net/HttpURLConnection;

    .line 11497
    .local v5, "con":Ljava/net/HttpURLConnection;
    const-string v18, "HEAD"

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 11498
    const v18, 0xf4240

    move/from16 v0, v18

    invoke-virtual {v5, v0}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 11499
    const-string v18, "Cache-Control"

    const-string v19, "no-cache"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v5, v0, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 11500
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->connect()V

    .line 11501
    const-string v18, "Content-length"

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v18

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadVersion;->numbytes:I

    .line 11502
    sget-object v18, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v19, "%s bytes found, %s Mb"

    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadVersion;->numbytes:I

    move/from16 v22, v0

    .line 11503
    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    aput-object v22, v20, v21

    const/16 v21, 0x1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadVersion;->numbytes:I

    move/from16 v22, v0

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    const/high16 v23, 0x49800000    # 1048576.0f

    div-float v22, v22, v23

    invoke-static/range {v22 .. v22}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v22

    aput-object v22, v20, v21

    .line 11502
    invoke-static/range {v19 .. v20}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 11504
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_81
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_81} :catch_2ec

    .line 11509
    .end local v5    # "con":Ljava/net/HttpURLConnection;
    :goto_81
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadVersion;->numbytes:I

    move/from16 v18, v0

    if-eqz v18, :cond_34a

    .line 11510
    sget-object v18, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->progress_loading:Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;

    if-eqz v18, :cond_a6

    sget-object v18, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->progress_loading:Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;

    invoke-virtual/range {v18 .. v18}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->isShowing()Z

    move-result v18

    if-eqz v18, :cond_a6

    sget-object v18, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->progress_loading:Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadVersion;->numbytes:I

    move/from16 v19, v0

    move/from16 v0, v19

    div-int/lit16 v0, v0, 0x400

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->setMax(I)V

    .line 11513
    :cond_a6
    :try_start_a6
    new-instance v17, Ljava/net/URL;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadVersion;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->urlpath:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-direct/range {v17 .. v18}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 11515
    .local v17, "url":Ljava/net/URL;
    invoke-virtual/range {v17 .. v17}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v4

    check-cast v4, Ljava/net/HttpURLConnection;

    .line 11516
    .local v4, "c":Ljava/net/HttpURLConnection;
    const-string v18, "GET"

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 11517
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/net/HttpURLConnection;->setUseCaches(Z)V

    .line 11518
    const v18, 0x927c0

    move/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 11519
    const-string v18, "Cache-Control"

    const-string v19, "no-cache"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v4, v0, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 11520
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->connect()V

    .line 11522
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v19, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "/Download/"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 11523
    .local v2, "PATH":Ljava/lang/String;
    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 11524
    .local v8, "file":Ljava/io/File;
    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v18

    if-nez v18, :cond_104

    .line 11525
    invoke-virtual {v8}, Ljava/io/File;->mkdirs()Z

    .line 11527
    :cond_104
    new-instance v13, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadVersion;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->ApkName:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-direct {v13, v8, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 11528
    .local v13, "outputFile":Ljava/io/File;
    invoke-virtual {v13}, Ljava/io/File;->exists()Z

    move-result v18

    if-eqz v18, :cond_120

    invoke-virtual {v13}, Ljava/io/File;->delete()Z

    .line 11529
    :cond_120
    new-instance v9, Ljava/io/FileOutputStream;

    invoke-direct {v9, v13}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 11531
    .local v9, "fos":Ljava/io/FileOutputStream;
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v11

    .line 11533
    .local v11, "is":Ljava/io/InputStream;
    const/16 v18, 0x2000

    move/from16 v0, v18

    new-array v3, v0, [B

    .line 11534
    .local v3, "buffer":[B
    const/4 v15, 0x0

    .line 11535
    .local v15, "size":I
    const/4 v12, 0x0

    .line 11536
    .local v12, "len1":I
    :goto_131
    invoke-virtual {v11, v3}, Ljava/io/InputStream;->read([B)I

    move-result v12

    const/16 v18, -0x1

    move/from16 v0, v18

    if-eq v12, v0, :cond_2fa

    .line 11537
    add-int/2addr v15, v12

    .line 11538
    const/16 v18, 0x1

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Integer;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    div-int/lit16 v0, v15, 0x400

    move/from16 v20, v0

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    aput-object v20, v18, v19

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadVersion;->publishProgress([Ljava/lang/Object;)V

    .line 11539
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v9, v3, v0, v12}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_15e
    .catch Ljava/io/IOException; {:try_start_a6 .. :try_end_15e} :catch_15f

    goto :goto_131

    .line 11543
    .end local v2    # "PATH":Ljava/lang/String;
    .end local v3    # "buffer":[B
    .end local v4    # "c":Ljava/net/HttpURLConnection;
    .end local v8    # "file":Ljava/io/File;
    .end local v9    # "fos":Ljava/io/FileOutputStream;
    .end local v11    # "is":Ljava/io/InputStream;
    .end local v12    # "len1":I
    .end local v13    # "outputFile":Ljava/io/File;
    .end local v15    # "size":I
    .end local v17    # "url":Ljava/net/URL;
    :catch_15f
    move-exception v6

    .line 11544
    .local v6, "e":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    .line 11547
    .end local v6    # "e":Ljava/io/IOException;
    :goto_163
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadVersion;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->PackageName:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v14

    .line 11548
    .local v14, "packageURI":Landroid/net/Uri;
    new-instance v10, Landroid/content/Intent;

    const-string v18, "android.intent.action.VIEW"

    move-object/from16 v0, v18

    invoke-direct {v10, v0, v14}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 11551
    .local v10, "intent":Landroid/content/Intent;
    :try_start_17c
    new-instance v16, Ljava/net/URL;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "http://chelpus.defcon5.biz/link_counter.php?ver="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadVersion;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->VersionCode:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 11553
    .local v16, "u":Ljava/net/URL;
    invoke-virtual/range {v16 .. v16}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v4

    check-cast v4, Ljava/net/HttpURLConnection;

    .line 11554
    .restart local v4    # "c":Ljava/net/HttpURLConnection;
    const-string v18, "GET"

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 11555
    const v18, 0xf4240

    move/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 11556
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/net/HttpURLConnection;->setUseCaches(Z)V

    .line 11557
    const-string v18, "Cache-Control"

    const-string v19, "no-cache"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v4, v0, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 11558
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->connect()V

    .line 11559
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    .line 11560
    new-instance v18, Ljava/io/ByteArrayOutputStream;

    invoke-direct/range {v18 .. v18}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 11561
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_1d9
    .catch Ljava/net/MalformedURLException; {:try_start_17c .. :try_end_1d9} :catch_302
    .catch Ljava/io/IOException; {:try_start_17c .. :try_end_1d9} :catch_308
    .catch Ljava/lang/NumberFormatException; {:try_start_17c .. :try_end_1d9} :catch_353
    .catch Ljava/lang/Exception; {:try_start_17c .. :try_end_1d9} :catch_30e

    .line 11570
    .end local v4    # "c":Ljava/net/HttpURLConnection;
    .end local v16    # "u":Ljava/net/URL;
    :goto_1d9
    new-instance v18, Ljava/io/File;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v20, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "/Download/"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadVersion;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->ApkName:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->exists()Z

    move-result v18

    if-eqz v18, :cond_2e5

    .line 11571
    new-instance v18, Ljava/io/File;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v20, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "/Download/"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadVersion;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->ApkName:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->length()J

    move-result-wide v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadVersion;->numbytes:I

    move/from16 v20, v0

    move/from16 v0, v20

    int-to-long v0, v0

    move-wide/from16 v20, v0

    cmp-long v18, v18, v20

    if-nez v18, :cond_314

    .line 11572
    new-instance v18, Ljava/io/File;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v20, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "/Download/"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadVersion;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->ApkName:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static/range {v18 .. v18}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v18

    const-string v19, "application/vnd.android.package-archive"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 11575
    const/high16 v18, 0x20000

    move/from16 v0, v18

    invoke-virtual {v10, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 11577
    sget-object v18, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patchAct:Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->startActivity(Landroid/content/Intent;)V

    .line 11578
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadVersion;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->VersionCode:I

    move/from16 v18, v0

    const/16 v19, 0x3e7

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_2e5

    .line 11580
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    .line 11581
    .local v7, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v18, "999"

    const/16 v19, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-interface {v7, v0, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 11582
    invoke-interface {v7}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 11584
    new-instance v18, Lcom/chelpus/Utils;

    const-string v19, ""

    invoke-direct/range {v18 .. v19}, Lcom/chelpus/Utils;-><init>(Ljava/lang/String;)V

    const/16 v19, 0x1

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "pm uninstall -k "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    sget-object v22, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->luckyPackage:Ljava/lang/String;

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    aput-object v21, v19, v20

    invoke-virtual/range {v18 .. v19}, Lcom/chelpus/Utils;->cmdRoot([Ljava/lang/String;)Ljava/lang/String;

    .line 11585
    sget-object v18, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->luckyPackage:Ljava/lang/String;

    invoke-static/range {v18 .. v18}, Lcom/chelpus/Utils;->kill(Ljava/lang/String;)V

    .line 11596
    .end local v7    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v10    # "intent":Landroid/content/Intent;
    .end local v14    # "packageURI":Landroid/net/Uri;
    :cond_2e5
    :goto_2e5
    const/16 v18, 0x1

    invoke-static/range {v18 .. v18}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v18

    return-object v18

    .line 11505
    :catch_2ec
    move-exception v6

    .line 11506
    .local v6, "e":Ljava/lang/Exception;
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadVersion;->numbytes:I

    .line 11507
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_81

    .line 11541
    .end local v6    # "e":Ljava/lang/Exception;
    .restart local v2    # "PATH":Ljava/lang/String;
    .restart local v3    # "buffer":[B
    .restart local v4    # "c":Ljava/net/HttpURLConnection;
    .restart local v8    # "file":Ljava/io/File;
    .restart local v9    # "fos":Ljava/io/FileOutputStream;
    .restart local v11    # "is":Ljava/io/InputStream;
    .restart local v12    # "len1":I
    .restart local v13    # "outputFile":Ljava/io/File;
    .restart local v15    # "size":I
    .restart local v17    # "url":Ljava/net/URL;
    :cond_2fa
    :try_start_2fa
    invoke-virtual {v9}, Ljava/io/FileOutputStream;->close()V

    .line 11542
    invoke-virtual {v11}, Ljava/io/InputStream;->close()V
    :try_end_300
    .catch Ljava/io/IOException; {:try_start_2fa .. :try_end_300} :catch_15f

    goto/16 :goto_163

    .line 11562
    .end local v2    # "PATH":Ljava/lang/String;
    .end local v3    # "buffer":[B
    .end local v4    # "c":Ljava/net/HttpURLConnection;
    .end local v8    # "file":Ljava/io/File;
    .end local v9    # "fos":Ljava/io/FileOutputStream;
    .end local v11    # "is":Ljava/io/InputStream;
    .end local v12    # "len1":I
    .end local v13    # "outputFile":Ljava/io/File;
    .end local v15    # "size":I
    .end local v17    # "url":Ljava/net/URL;
    .restart local v10    # "intent":Landroid/content/Intent;
    .restart local v14    # "packageURI":Landroid/net/Uri;
    :catch_302
    move-exception v6

    .line 11563
    .local v6, "e":Ljava/net/MalformedURLException;
    invoke-virtual {v6}, Ljava/net/MalformedURLException;->printStackTrace()V

    goto/16 :goto_1d9

    .line 11564
    .end local v6    # "e":Ljava/net/MalformedURLException;
    :catch_308
    move-exception v6

    .line 11565
    .local v6, "e":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_1d9

    .line 11567
    .end local v6    # "e":Ljava/io/IOException;
    :catch_30e
    move-exception v6

    .line 11568
    .local v6, "e":Ljava/lang/Exception;
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_1d9

    .line 11588
    .end local v6    # "e":Ljava/lang/Exception;
    :cond_314
    new-instance v18, Ljava/io/File;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v20, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "/Download/"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadVersion;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->ApkName:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->delete()Z

    .line 11589
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadVersion;->corruptdownload:Z

    goto :goto_2e5

    .line 11594
    .end local v10    # "intent":Landroid/content/Intent;
    .end local v14    # "packageURI":Landroid/net/Uri;
    :cond_34a
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadVersion;->host_down:Z

    goto :goto_2e5

    .line 11566
    .restart local v10    # "intent":Landroid/content/Intent;
    .restart local v14    # "packageURI":Landroid/net/Uri;
    :catch_353
    move-exception v18

    goto/16 :goto_1d9
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 11480
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadVersion;->doInBackground([Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .registers 6
    .param p1, "result"    # Ljava/lang/Boolean;

    .prologue
    const v3, 0x7f070234

    .line 11610
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 11611
    const/16 v0, 0x17

    invoke-static {v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->removeDialogLP(I)V

    .line 11612
    iget-boolean v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadVersion;->corruptdownload:Z

    if-eqz v0, :cond_1f

    .line 11613
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadVersion;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-static {v3}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0700c0

    invoke-static {v2}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->showMessage(Ljava/lang/String;Ljava/lang/String;)V

    .line 11615
    :cond_1f
    iget-boolean v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadVersion;->host_down:Z

    if-eqz v0, :cond_33

    .line 11616
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadVersion;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-static {v3}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f070132

    invoke-static {v2}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->showMessage(Ljava/lang/String;Ljava/lang/String;)V

    .line 11619
    :cond_33
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 2

    .prologue
    .line 11480
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadVersion;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected onPreExecute()V
    .registers 2

    .prologue
    .line 11487
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 11488
    const/16 v0, 0x17

    invoke-static {v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->showDialogLP(I)V

    .line 11490
    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Integer;)V
    .registers 4
    .param p1, "values"    # [Ljava/lang/Integer;

    .prologue
    .line 11601
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    .line 11603
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->progress_loading:Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;

    if-eqz v0, :cond_13

    .line 11604
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->progress_loading:Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;

    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Progress_Dialog_Loading;->setProgress(I)V

    .line 11606
    :cond_13
    return-void
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .registers 2

    .prologue
    .line 11480
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$DownloadVersion;->onProgressUpdate([Ljava/lang/Integer;)V

    return-void
.end method
