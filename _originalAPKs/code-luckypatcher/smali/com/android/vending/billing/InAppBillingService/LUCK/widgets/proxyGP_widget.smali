.class public Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/proxyGP_widget;
.super Landroid/appwidget/AppWidgetProvider;
.source "proxyGP_widget.java"


# static fields
.field public static ACTION_WIDGET_RECEIVER:Ljava/lang/String;

.field public static ACTION_WIDGET_RECEIVER_Updater:Ljava/lang/String;


# instance fields
.field cont:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 33
    const-string v0, "ActionReceiverProxyGPWidget"

    sput-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/proxyGP_widget;->ACTION_WIDGET_RECEIVER:Ljava/lang/String;

    .line 34
    const-string v0, "ActionReceiverWidgetProxyGPUpdate"

    sput-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/proxyGP_widget;->ACTION_WIDGET_RECEIVER_Updater:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/appwidget/AppWidgetProvider;-><init>()V

    .line 41
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/proxyGP_widget;->cont:Landroid/content/Context;

    return-void
.end method

.method public static pushWidgetUpdate(Landroid/content/Context;Landroid/widget/RemoteViews;)V
    .registers 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "remoteViews"    # Landroid/widget/RemoteViews;

    .prologue
    .line 37
    new-instance v1, Landroid/content/ComponentName;

    const-class v2, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/proxyGP_widget;

    invoke-direct {v1, p0, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 38
    .local v1, "myWidget":Landroid/content/ComponentName;
    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    .line 39
    .local v0, "manager":Landroid/appwidget/AppWidgetManager;
    invoke-virtual {v0, v1, p1}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(Landroid/content/ComponentName;Landroid/widget/RemoteViews;)V

    .line 40
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 32
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 98
    const/16 v25, 0x0

    invoke-static/range {v25 .. v25}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v25

    sput-object v25, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->startUnderRoot:Ljava/lang/Boolean;

    .line 99
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    .line 100
    .local v5, "action":Ljava/lang/String;
    sget-object v25, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/proxyGP_widget;->ACTION_WIDGET_RECEIVER:Ljava/lang/String;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_22d

    .line 101
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->init()V

    .line 102
    new-instance v12, Landroid/os/Handler;

    invoke-direct {v12}, Landroid/os/Handler;-><init>()V

    .line 103
    .local v12, "handler":Landroid/os/Handler;
    new-instance v17, Landroid/widget/RemoteViews;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v25

    const v26, 0x7f04003f

    move-object/from16 v0, v17

    move-object/from16 v1, v25

    move/from16 v2, v26

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 104
    .local v17, "remoteViews":Landroid/widget/RemoteViews;
    const v25, 0x7f0d00f0

    const-string v26, ""

    move-object/from16 v0, v17

    move/from16 v1, v25

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 105
    const v25, 0x7f0d00f1

    const/16 v26, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 106
    invoke-static/range {p1 .. p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v10

    .line 107
    .local v10, "gm":Landroid/appwidget/AppWidgetManager;
    new-instance v25, Landroid/content/ComponentName;

    const-class v26, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/proxyGP_widget;

    move-object/from16 v0, v25

    move-object/from16 v1, p1

    move-object/from16 v2, v26

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    move-object/from16 v0, v25

    invoke-virtual {v10, v0}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v13

    .line 108
    .local v13, "ids":[I
    move-object/from16 v0, v17

    invoke-virtual {v10, v13, v0}, Landroid/appwidget/AppWidgetManager;->updateAppWidget([ILandroid/widget/RemoteViews;)V

    .line 109
    sget-boolean v25, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->su:Z

    if-eqz v25, :cond_2eb

    invoke-static {}, Lcom/chelpus/Utils;->checkCoreJarPatch11()Z

    move-result v25

    if-eqz v25, :cond_2eb

    invoke-static {}, Lcom/chelpus/Utils;->checkCoreJarPatch12()Z

    move-result v25

    if-eqz v25, :cond_2eb

    .line 110
    const/4 v11, 0x0

    .line 112
    .local v11, "gp":Landroid/content/pm/PackageInfo;
    :try_start_79
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v25

    const-string v26, "com.android.vending"

    const/16 v27, 0x0

    invoke-virtual/range {v25 .. v27}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_84
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_79 .. :try_end_84} :catch_25d

    move-result-object v11

    .line 116
    :goto_85
    if-eqz v11, :cond_22d

    .line 117
    iget-object v0, v11, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    move-object/from16 v25, v0

    invoke-static/range {v25 .. v25}, Lcom/chelpus/Utils;->getFileDalvikCache(Ljava/lang/String;)Ljava/io/File;

    move-result-object v6

    .line 118
    .local v6, "dc":Ljava/io/File;
    new-instance v16, Ljava/io/File;

    iget-object v0, v11, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    move-object/from16 v25, v0

    const/16 v26, 0x1

    invoke-static/range {v25 .. v26}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v16

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 120
    .local v16, "odex":Ljava/io/File;
    const/16 v19, 0x0

    .line 121
    .local v19, "run":Z
    invoke-static {}, Lcom/chelpus/Utils;->isXposedEnabled()Z

    move-result v25

    if-eqz v25, :cond_277

    .line 122
    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    .line 123
    .local v20, "s":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;>;"
    const/16 v21, 0x0

    .line 125
    .local v21, "settings":Lorg/json/JSONObject;
    :try_start_bd
    invoke-static {}, Lcom/chelpus/Utils;->readXposedParamBoolean()Lorg/json/JSONObject;
    :try_end_c0
    .catch Lorg/json/JSONException; {:try_start_bd .. :try_end_c0} :catch_271

    move-result-object v21

    .line 130
    :goto_c1
    const/4 v4, 0x0

    .line 131
    .local v4, "Xposed4":Z
    if-eqz v21, :cond_d2

    .line 132
    const-string v25, "patch4"

    const/16 v26, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, v25

    move/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v4

    .line 135
    :cond_d2
    if-nez v4, :cond_d6

    const/16 v19, 0x1

    .line 139
    .end local v4    # "Xposed4":Z
    .end local v20    # "s":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;>;"
    .end local v21    # "settings":Lorg/json/JSONObject;
    :cond_d6
    :goto_d6
    if-eqz v19, :cond_2c4

    .line 140
    new-instance v25, Ljava/io/File;

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "/p.info"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-direct/range {v25 .. v26}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v25 .. v25}, Ljava/io/File;->exists()Z

    move-result v25

    if-nez v25, :cond_1ed

    .line 142
    new-instance v23, Ljava/io/File;

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v26, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->toolfilesdir:Ljava/lang/String;

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, "/"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, "p.apk"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v23

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 143
    .local v23, "tempopt":Ljava/io/File;
    const v18, 0x7f06000b

    .line 145
    .local v18, "resFile":I
    invoke-static/range {v18 .. v18}, Lcom/chelpus/Utils;->getRawLength(I)J

    move-result-wide v14

    .line 147
    .local v14, "lenght":J
    invoke-virtual/range {v23 .. v23}, Ljava/io/File;->exists()Z

    move-result v25

    if-eqz v25, :cond_137

    invoke-virtual/range {v23 .. v23}, Ljava/io/File;->length()J

    move-result-wide v25

    cmp-long v25, v25, v14

    if-eqz v25, :cond_1ed

    .line 148
    :cond_137
    invoke-virtual/range {v23 .. v23}, Ljava/io/File;->length()J

    move-result-wide v25

    cmp-long v25, v25, v14

    if-eqz v25, :cond_170

    .line 149
    sget-object v25, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "LuckyPatcher: p.info version updated. "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, " "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v23 .. v23}, Ljava/io/File;->length()J

    move-result-wide v27

    invoke-virtual/range {v26 .. v28}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 150
    invoke-virtual/range {v23 .. v23}, Ljava/io/File;->exists()Z

    move-result v25

    if-eqz v25, :cond_170

    invoke-virtual/range {v23 .. v23}, Ljava/io/File;->delete()Z

    .line 153
    :cond_170
    :try_start_170
    new-instance v25, Ljava/io/File;

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v27, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->toolfilesdir:Ljava/lang/String;

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "/p.apk"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-direct/range {v25 .. v26}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move/from16 v0, v18

    move-object/from16 v1, v25

    invoke-static {v0, v1}, Lcom/chelpus/Utils;->getRawToFile(ILjava/io/File;)Z
    :try_end_191
    .catch Ljava/lang/Exception; {:try_start_170 .. :try_end_191} :catch_318

    .line 160
    :goto_191
    :try_start_191
    new-instance v25, Ljava/io/File;

    invoke-virtual/range {v23 .. v23}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v26

    invoke-direct/range {v25 .. v26}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/16 v26, 0xdff

    invoke-static/range {v25 .. v26}, Lcom/chelpus/Utils;->chmod(Ljava/io/File;I)I
    :try_end_19f
    .catch Ljava/lang/Exception; {:try_start_191 .. :try_end_19f} :catch_27b

    .line 167
    :goto_19f
    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "chmod 06777 "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v23 .. v23}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v25 .. v25}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 168
    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "chown 0.0 "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v23 .. v23}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v25 .. v25}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 169
    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "chown 0:0 "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v23 .. v23}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v25 .. v25}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 175
    .end local v14    # "lenght":J
    .end local v18    # "resFile":I
    .end local v23    # "tempopt":Ljava/io/File;
    :cond_1ed
    const-wide/16 v14, 0x0

    .line 177
    .restart local v14    # "lenght":J
    :try_start_1ef
    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->length()J
    :try_end_1f2
    .catch Ljava/lang/Exception; {:try_start_1ef .. :try_end_1f2} :catch_288

    move-result-wide v14

    .line 185
    :goto_1f3
    const-wide/16 v25, 0x0

    cmp-long v25, v14, v25

    if-eqz v25, :cond_295

    const-wide/32 v25, 0x100000

    cmp-long v25, v14, v25

    if-gez v25, :cond_295

    .line 186
    iget-object v0, v11, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    move-object/from16 v22, v0

    .line 187
    .local v22, "sourceDir":Ljava/lang/String;
    iget-object v0, v11, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    move/from16 v25, v0

    invoke-static/range {v25 .. v25}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v24

    .line 188
    .local v24, "uid":Ljava/lang/String;
    new-instance v25, Ljava/lang/Thread;

    new-instance v26, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/proxyGP_widget$1;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, v22

    move-object/from16 v3, v24

    invoke-direct {v0, v1, v2, v3, v12}, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/proxyGP_widget$1;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/proxyGP_widget;Ljava/lang/String;Ljava/lang/String;Landroid/os/Handler;)V

    invoke-direct/range {v25 .. v26}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 217
    invoke-virtual/range {v25 .. v25}, Ljava/lang/Thread;->start()V

    .line 282
    .end local v6    # "dc":Ljava/io/File;
    .end local v10    # "gm":Landroid/appwidget/AppWidgetManager;
    .end local v11    # "gp":Landroid/content/pm/PackageInfo;
    .end local v12    # "handler":Landroid/os/Handler;
    .end local v13    # "ids":[I
    .end local v14    # "lenght":J
    .end local v16    # "odex":Ljava/io/File;
    .end local v17    # "remoteViews":Landroid/widget/RemoteViews;
    .end local v19    # "run":Z
    .end local v22    # "sourceDir":Ljava/lang/String;
    .end local v24    # "uid":Ljava/lang/String;
    :cond_22d
    :goto_22d
    sget-object v25, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/proxyGP_widget;->ACTION_WIDGET_RECEIVER_Updater:Ljava/lang/String;

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_259

    .line 284
    const/16 v25, 0x1

    :try_start_239
    sput-boolean v25, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->appDisabler:Z

    .line 285
    invoke-static/range {p1 .. p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v10

    .line 286
    .restart local v10    # "gm":Landroid/appwidget/AppWidgetManager;
    new-instance v25, Landroid/content/ComponentName;

    const-class v26, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/proxyGP_widget;

    move-object/from16 v0, v25

    move-object/from16 v1, p1

    move-object/from16 v2, v26

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    move-object/from16 v0, v25

    invoke-virtual {v10, v0}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v13

    .line 287
    .restart local v13    # "ids":[I
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v10, v13}, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/proxyGP_widget;->onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    :try_end_259
    .catch Ljava/lang/Exception; {:try_start_239 .. :try_end_259} :catch_312

    .line 292
    .end local v10    # "gm":Landroid/appwidget/AppWidgetManager;
    .end local v13    # "ids":[I
    :cond_259
    :goto_259
    invoke-super/range {p0 .. p2}, Landroid/appwidget/AppWidgetProvider;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    .line 293
    return-void

    .line 113
    .restart local v10    # "gm":Landroid/appwidget/AppWidgetManager;
    .restart local v11    # "gp":Landroid/content/pm/PackageInfo;
    .restart local v12    # "handler":Landroid/os/Handler;
    .restart local v13    # "ids":[I
    .restart local v17    # "remoteViews":Landroid/widget/RemoteViews;
    :catch_25d
    move-exception v7

    .line 114
    .local v7, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v25, "Google Play not installed."

    const/16 v26, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    move/from16 v2, v26

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Landroid/widget/Toast;->show()V

    goto/16 :goto_85

    .line 127
    .end local v7    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v6    # "dc":Ljava/io/File;
    .restart local v16    # "odex":Ljava/io/File;
    .restart local v19    # "run":Z
    .restart local v20    # "s":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;>;"
    .restart local v21    # "settings":Lorg/json/JSONObject;
    :catch_271
    move-exception v7

    .line 128
    .local v7, "e":Lorg/json/JSONException;
    invoke-virtual {v7}, Lorg/json/JSONException;->printStackTrace()V

    goto/16 :goto_c1

    .line 137
    .end local v7    # "e":Lorg/json/JSONException;
    .end local v20    # "s":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;>;"
    .end local v21    # "settings":Lorg/json/JSONObject;
    :cond_277
    const/16 v19, 0x1

    goto/16 :goto_d6

    .line 161
    .restart local v14    # "lenght":J
    .restart local v18    # "resFile":I
    .restart local v23    # "tempopt":Ljava/io/File;
    :catch_27b
    move-exception v9

    .line 163
    .local v9, "e2":Ljava/lang/Exception;
    sget-object v25, Ljava/lang/System;->out:Ljava/io/PrintStream;

    move-object/from16 v0, v25

    invoke-virtual {v0, v9}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    .line 164
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_19f

    .line 178
    .end local v9    # "e2":Ljava/lang/Exception;
    .end local v18    # "resFile":I
    .end local v23    # "tempopt":Ljava/io/File;
    :catch_288
    move-exception v7

    .line 180
    .local v7, "e":Ljava/lang/Exception;
    :try_start_289
    invoke-virtual {v6}, Ljava/io/File;->length()J
    :try_end_28c
    .catch Ljava/lang/Exception; {:try_start_289 .. :try_end_28c} :catch_28f

    move-result-wide v14

    goto/16 :goto_1f3

    .line 181
    :catch_28f
    move-exception v8

    .line 182
    .local v8, "e1":Ljava/lang/Exception;
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_1f3

    .line 221
    .end local v7    # "e":Ljava/lang/Exception;
    .end local v8    # "e1":Ljava/lang/Exception;
    :cond_295
    iget-object v0, v11, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    move-object/from16 v22, v0

    .line 222
    .restart local v22    # "sourceDir":Ljava/lang/String;
    iget-object v0, v11, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    move/from16 v25, v0

    invoke-static/range {v25 .. v25}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v24

    .line 224
    .restart local v24    # "uid":Ljava/lang/String;
    new-instance v25, Ljava/lang/Thread;

    new-instance v26, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/proxyGP_widget$2;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, v22

    move-object/from16 v3, v24

    invoke-direct {v0, v1, v2, v3, v12}, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/proxyGP_widget$2;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/proxyGP_widget;Ljava/lang/String;Ljava/lang/String;Landroid/os/Handler;)V

    invoke-direct/range {v25 .. v26}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 263
    invoke-virtual/range {v25 .. v25}, Ljava/lang/Thread;->start()V

    goto/16 :goto_22d

    .line 269
    .end local v14    # "lenght":J
    .end local v22    # "sourceDir":Ljava/lang/String;
    .end local v24    # "uid":Ljava/lang/String;
    :cond_2c4
    const v25, 0x7f070266

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v25

    const/16 v26, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    move/from16 v2, v26

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Landroid/widget/Toast;->show()V

    .line 270
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v25

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-virtual {v0, v1, v10, v13}, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/proxyGP_widget;->onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    goto/16 :goto_22d

    .line 275
    .end local v6    # "dc":Ljava/io/File;
    .end local v11    # "gp":Landroid/content/pm/PackageInfo;
    .end local v16    # "odex":Ljava/io/File;
    .end local v19    # "run":Z
    :cond_2eb
    const v25, 0x7f070267

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v25

    const/16 v26, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    move/from16 v2, v26

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Landroid/widget/Toast;->show()V

    .line 276
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v25

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-virtual {v0, v1, v10, v13}, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/proxyGP_widget;->onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    goto/16 :goto_22d

    .line 288
    .end local v10    # "gm":Landroid/appwidget/AppWidgetManager;
    .end local v12    # "handler":Landroid/os/Handler;
    .end local v13    # "ids":[I
    .end local v17    # "remoteViews":Landroid/widget/RemoteViews;
    :catch_312
    move-exception v7

    .restart local v7    # "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_259

    .line 154
    .end local v7    # "e":Ljava/lang/Exception;
    .restart local v6    # "dc":Ljava/io/File;
    .restart local v10    # "gm":Landroid/appwidget/AppWidgetManager;
    .restart local v11    # "gp":Landroid/content/pm/PackageInfo;
    .restart local v12    # "handler":Landroid/os/Handler;
    .restart local v13    # "ids":[I
    .restart local v14    # "lenght":J
    .restart local v16    # "odex":Ljava/io/File;
    .restart local v17    # "remoteViews":Landroid/widget/RemoteViews;
    .restart local v18    # "resFile":I
    .restart local v19    # "run":Z
    .restart local v23    # "tempopt":Ljava/io/File;
    :catch_318
    move-exception v25

    goto/16 :goto_191
.end method

.method public onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .registers 19
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appWidgetManager"    # Landroid/appwidget/AppWidgetManager;
    .param p3, "appWidgetIds"    # [I

    .prologue
    .line 45
    move-object/from16 v0, p1

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/proxyGP_widget;->cont:Landroid/content/Context;

    .line 46
    const/4 v12, 0x0

    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v12

    sput-object v12, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->startUnderRoot:Ljava/lang/Boolean;

    .line 47
    new-instance v11, Landroid/widget/RemoteViews;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v12

    const v13, 0x7f04003f

    invoke-direct {v11, v12, v13}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 50
    .local v11, "remoteViews":Landroid/widget/RemoteViews;
    new-instance v3, Landroid/content/Intent;

    const-class v12, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/proxyGP_widget;

    move-object/from16 v0, p1

    invoke-direct {v3, v0, v12}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 51
    .local v3, "active":Landroid/content/Intent;
    sget-object v12, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/proxyGP_widget;->ACTION_WIDGET_RECEIVER:Ljava/lang/String;

    invoke-virtual {v3, v12}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 52
    const-string v12, "msg"

    const-string v13, "Hello Habrahabr"

    invoke-virtual {v3, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 55
    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v12, v3, v13}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 58
    .local v2, "actionPendingIntent":Landroid/app/PendingIntent;
    const v12, 0x7f0d00f0

    invoke-virtual {v11, v12, v2}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 59
    const v12, 0x7f0d00f0

    const-string v13, "Proxy GP"

    invoke-virtual {v11, v12, v13}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 60
    const v12, 0x7f0d00f1

    const/16 v13, 0x8

    invoke-virtual {v11, v12, v13}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 61
    const/4 v7, 0x0

    .line 63
    .local v7, "gp":Landroid/content/pm/PackageInfo;
    :try_start_4b
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v12

    const-string v13, "com.android.vending"

    const/4 v14, 0x0

    invoke-virtual {v12, v13, v14}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_55
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_4b .. :try_end_55} :catch_95

    move-result-object v7

    .line 68
    :goto_56
    if-eqz v7, :cond_bb

    .line 69
    iget-object v12, v7, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v12, v12, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    invoke-static {v12}, Lcom/chelpus/Utils;->getFileDalvikCache(Ljava/lang/String;)Ljava/io/File;

    move-result-object v4

    .line 70
    .local v4, "dc":Ljava/io/File;
    new-instance v10, Ljava/io/File;

    iget-object v12, v7, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v12, v12, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    const/4 v13, 0x1

    invoke-static {v12, v13}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v10, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 71
    .local v10, "odex":Ljava/io/File;
    const-wide/16 v8, 0x0

    .line 73
    .local v8, "lenght":J
    :try_start_70
    invoke-virtual {v10}, Ljava/io/File;->length()J
    :try_end_73
    .catch Ljava/lang/Exception; {:try_start_70 .. :try_end_73} :catch_a3

    move-result-wide v8

    .line 81
    :goto_74
    const-wide/16 v12, 0x0

    cmp-long v12, v8, v12

    if-eqz v12, :cond_ae

    const-wide/32 v12, 0x100000

    cmp-long v12, v8, v12

    if-gez v12, :cond_ae

    .line 82
    const v12, 0x7f0d00f0

    const-string v13, "#00FF00"

    invoke-static {v13}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v13

    invoke-virtual {v11, v12, v13}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 90
    .end local v4    # "dc":Ljava/io/File;
    .end local v8    # "lenght":J
    .end local v10    # "odex":Ljava/io/File;
    :goto_8d
    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-virtual {v0, v1, v11}, Landroid/appwidget/AppWidgetManager;->updateAppWidget([ILandroid/widget/RemoteViews;)V

    .line 92
    return-void

    .line 64
    :catch_95
    move-exception v5

    .line 65
    .local v5, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v12, "Google Play not installed."

    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v12, v13}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v12

    invoke-virtual {v12}, Landroid/widget/Toast;->show()V

    goto :goto_56

    .line 74
    .end local v5    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v4    # "dc":Ljava/io/File;
    .restart local v8    # "lenght":J
    .restart local v10    # "odex":Ljava/io/File;
    :catch_a3
    move-exception v5

    .line 76
    .local v5, "e":Ljava/lang/Exception;
    :try_start_a4
    invoke-virtual {v4}, Ljava/io/File;->length()J
    :try_end_a7
    .catch Ljava/lang/Exception; {:try_start_a4 .. :try_end_a7} :catch_a9

    move-result-wide v8

    goto :goto_74

    .line 77
    :catch_a9
    move-exception v6

    .line 78
    .local v6, "e1":Ljava/lang/Exception;
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_74

    .line 84
    .end local v5    # "e":Ljava/lang/Exception;
    .end local v6    # "e1":Ljava/lang/Exception;
    :cond_ae
    const v12, 0x7f0d00f0

    const-string v13, "#FF0000"

    invoke-static {v13}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v13

    invoke-virtual {v11, v12, v13}, Landroid/widget/RemoteViews;->setTextColor(II)V

    goto :goto_8d

    .line 87
    .end local v4    # "dc":Ljava/io/File;
    .end local v8    # "lenght":J
    .end local v10    # "odex":Ljava/io/File;
    :cond_bb
    const v12, 0x7f0d00f0

    const-string v13, "#FF0000"

    invoke-static {v13}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v13

    invoke-virtual {v11, v12, v13}, Landroid/widget/RemoteViews;->setTextColor(II)V

    goto :goto_8d
.end method
