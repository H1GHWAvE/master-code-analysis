.class Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$116;
.super Ljava/lang/Object;
.source "listAppsFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->apkpermissions(Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;Ljava/util/ArrayList;Ljava/util/ArrayList;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;)V
    .registers 2
    .param p1, "this$0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    .prologue
    .line 13975
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$116;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 38

    .prologue
    .line 13978
    sget-object v19, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    .line 13979
    .local v19, "pli":Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    const/4 v14, 0x0

    .line 13980
    .local v14, "filesDir":Ljava/lang/String;
    const/4 v11, 0x0

    .line 13982
    .local v11, "error":Z
    :try_start_4
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v31

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    invoke-virtual/range {v31 .. v33}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v31

    move-object/from16 v0, v31

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-object v14, v0, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;
    :try_end_1e
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_4 .. :try_end_1e} :catch_84
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_1e} :catch_8a

    .line 13991
    :goto_1e
    if-nez v11, :cond_49e

    .line 13992
    const-string v31, "/mnt/asec/"

    move-object/from16 v0, v31

    invoke-virtual {v14, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v31

    if-eqz v31, :cond_3b

    .line 13994
    const-string v31, "/pkg.apk"

    const-string v32, ""

    move-object/from16 v0, v31

    move-object/from16 v1, v32

    invoke-virtual {v14, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v31

    const-string v32, "rw"

    invoke-static/range {v31 .. v32}, Lcom/chelpus/Utils;->remount(Ljava/lang/String;Ljava/lang/String;)Z

    .line 13996
    :cond_3b
    const-string v31, "/system/"

    move-object/from16 v0, v31

    invoke-virtual {v14, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v31

    if-eqz v31, :cond_4c

    .line 13998
    const-string v31, "/system"

    const-string v32, "rw"

    invoke-static/range {v31 .. v32}, Lcom/chelpus/Utils;->remount(Ljava/lang/String;Ljava/lang/String;)Z

    .line 14000
    :cond_4c
    new-instance v31, Ljava/lang/StringBuilder;

    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    const-string v32, "chmod 644 "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v31 .. v31}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 14001
    new-instance v31, Ljava/io/File;

    move-object/from16 v0, v31

    invoke-direct {v0, v14}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v31 .. v31}, Ljava/io/File;->exists()Z

    move-result v31

    if-nez v31, :cond_90

    .line 14003
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$116;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    move-object/from16 v31, v0

    new-instance v32, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$116$1;

    move-object/from16 v0, v32

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$116$1;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$116;)V

    invoke-virtual/range {v31 .. v32}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V

    .line 14215
    :goto_83
    return-void

    .line 13983
    :catch_84
    move-exception v10

    .line 13985
    .local v10, "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v10}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 13986
    const/4 v11, 0x1

    .line 13990
    goto :goto_1e

    .line 13987
    .end local v10    # "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_8a
    move-exception v9

    .line 13988
    .local v9, "e":Ljava/lang/Exception;
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V

    .line 13989
    const/4 v11, 0x1

    goto :goto_1e

    .line 14013
    .end local v9    # "e":Ljava/lang/Exception;
    :cond_90
    const/4 v13, 0x0

    .line 14015
    .local v13, "extrFile":Ljava/lang/String;
    :try_start_91
    new-instance v15, Ljava/io/FileInputStream;

    invoke-direct {v15, v14}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 14016
    .local v15, "fin":Ljava/io/FileInputStream;
    new-instance v29, Ljava/util/zip/ZipInputStream;

    move-object/from16 v0, v29

    invoke-direct {v0, v15}, Ljava/util/zip/ZipInputStream;-><init>(Ljava/io/InputStream;)V

    .line 14017
    .local v29, "zin":Ljava/util/zip/ZipInputStream;
    const/16 v28, 0x0

    .line 14018
    .local v28, "ze":Ljava/util/zip/ZipEntry;
    :cond_9f
    :goto_9f
    invoke-virtual/range {v29 .. v29}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    move-result-object v28

    if-eqz v28, :cond_186

    .line 14023
    invoke-virtual/range {v28 .. v28}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v31

    const-string v32, "AndroidManifest.xml"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_9f

    .line 14024
    new-instance v31, Ljava/lang/StringBuilder;

    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v32, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, "/Modified/"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, "AndroidManifest.xml"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 14025
    new-instance v16, Ljava/io/FileOutputStream;

    move-object/from16 v0, v16

    invoke-direct {v0, v13}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 14027
    .local v16, "fout":Ljava/io/FileOutputStream;
    const/16 v31, 0x2000

    move/from16 v0, v31

    new-array v6, v0, [B

    .line 14029
    .local v6, "buffer":[B
    :goto_d9
    move-object/from16 v0, v29

    invoke-virtual {v0, v6}, Ljava/util/zip/ZipInputStream;->read([B)I

    move-result v17

    .local v17, "length":I
    const/16 v31, -0x1

    move/from16 v0, v17

    move/from16 v1, v31

    if-eq v0, v1, :cond_17e

    .line 14030
    const/16 v31, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v31

    move/from16 v2, v17

    invoke-virtual {v0, v6, v1, v2}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_f2
    .catch Ljava/lang/Exception; {:try_start_91 .. :try_end_f2} :catch_f3

    goto :goto_d9

    .line 14040
    .end local v6    # "buffer":[B
    .end local v15    # "fin":Ljava/io/FileInputStream;
    .end local v16    # "fout":Ljava/io/FileOutputStream;
    .end local v17    # "length":I
    .end local v28    # "ze":Ljava/util/zip/ZipEntry;
    .end local v29    # "zin":Ljava/util/zip/ZipInputStream;
    :catch_f3
    move-exception v9

    .line 14042
    .restart local v9    # "e":Ljava/lang/Exception;
    :try_start_f4
    new-instance v30, Lnet/lingala/zip4j/core/ZipFile;

    move-object/from16 v0, v30

    invoke-direct {v0, v14}, Lnet/lingala/zip4j/core/ZipFile;-><init>(Ljava/lang/String;)V

    .line 14046
    .local v30, "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    new-instance v31, Ljava/lang/StringBuilder;

    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v32, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, "/Modified/"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, "AndroidManifest.xml"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 14047
    const-string v31, "AndroidManifest.xml"

    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v33, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, "/Modified/"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    invoke-virtual/range {v30 .. v32}, Lnet/lingala/zip4j/core/ZipFile;->extractFile(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_130
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_f4 .. :try_end_130} :catch_18d
    .catch Ljava/lang/Exception; {:try_start_f4 .. :try_end_130} :catch_1c6

    .line 14058
    .end local v30    # "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    :goto_130
    sget-object v31, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    const-string v33, "Decompress unzip "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    invoke-virtual/range {v31 .. v32}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 14060
    .end local v9    # "e":Ljava/lang/Exception;
    :goto_14a
    new-instance v31, Ljava/io/File;

    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v33, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, "/Modified/AndroidManifest.xml"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    invoke-direct/range {v31 .. v32}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v31 .. v31}, Ljava/io/File;->exists()Z

    move-result v31

    if-nez v31, :cond_1ff

    .line 14062
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$116;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    move-object/from16 v31, v0

    new-instance v32, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$116$2;

    move-object/from16 v0, v32

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$116$2;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$116;)V

    invoke-virtual/range {v31 .. v32}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V

    goto/16 :goto_83

    .line 14032
    .restart local v6    # "buffer":[B
    .restart local v15    # "fin":Ljava/io/FileInputStream;
    .restart local v16    # "fout":Ljava/io/FileOutputStream;
    .restart local v17    # "length":I
    .restart local v28    # "ze":Ljava/util/zip/ZipEntry;
    .restart local v29    # "zin":Ljava/util/zip/ZipInputStream;
    :cond_17e
    :try_start_17e
    invoke-virtual/range {v29 .. v29}, Ljava/util/zip/ZipInputStream;->closeEntry()V

    .line 14033
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileOutputStream;->close()V

    goto/16 :goto_9f

    .line 14038
    .end local v6    # "buffer":[B
    .end local v16    # "fout":Ljava/io/FileOutputStream;
    .end local v17    # "length":I
    :cond_186
    invoke-virtual/range {v29 .. v29}, Ljava/util/zip/ZipInputStream;->close()V

    .line 14039
    invoke-virtual {v15}, Ljava/io/FileInputStream;->close()V
    :try_end_18c
    .catch Ljava/lang/Exception; {:try_start_17e .. :try_end_18c} :catch_f3

    goto :goto_14a

    .line 14050
    .end local v15    # "fin":Ljava/io/FileInputStream;
    .end local v28    # "ze":Ljava/util/zip/ZipEntry;
    .end local v29    # "zin":Ljava/util/zip/ZipInputStream;
    .restart local v9    # "e":Ljava/lang/Exception;
    :catch_18d
    move-exception v10

    .line 14051
    .local v10, "e1":Lnet/lingala/zip4j/exception/ZipException;
    sget-object v31, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    const-string v33, "Error classes.dex decompress! "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    invoke-virtual/range {v31 .. v32}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 14052
    sget-object v31, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    const-string v33, "Exception e1"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual {v9}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    invoke-virtual/range {v31 .. v32}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_130

    .line 14053
    .end local v10    # "e1":Lnet/lingala/zip4j/exception/ZipException;
    :catch_1c6
    move-exception v10

    .line 14054
    .local v10, "e1":Ljava/lang/Exception;
    sget-object v31, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    const-string v33, "Error classes.dex decompress! "

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    invoke-virtual/range {v31 .. v32}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 14055
    sget-object v31, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    const-string v33, "Exception e1"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual {v9}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    invoke-virtual/range {v31 .. v32}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_130

    .line 14072
    .end local v9    # "e":Ljava/lang/Exception;
    .end local v10    # "e1":Ljava/lang/Exception;
    :cond_1ff
    new-instance v12, Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample;

    invoke-direct {v12}, Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample;-><init>()V

    .line 14074
    .local v12, "ex":Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample;
    new-instance v18, Ljava/io/File;

    move-object/from16 v0, v18

    invoke-direct {v0, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 14077
    .local v18, "manifestFile":Ljava/io/File;
    :try_start_20b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$116;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    move-object/from16 v31, v0

    # getter for: Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->permissions:Ljava/util/ArrayList;
    invoke-static/range {v31 .. v31}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->access$2200(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;)Ljava/util/ArrayList;

    move-result-object v31

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$116;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    move-object/from16 v32, v0

    # getter for: Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->activities:Ljava/util/ArrayList;
    invoke-static/range {v32 .. v32}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->access$2300(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;)Ljava/util/ArrayList;

    move-result-object v32

    move-object/from16 v0, v18

    move-object/from16 v1, v31

    move-object/from16 v2, v32

    invoke-virtual {v12, v0, v1, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample;->disablePermisson(Ljava/io/File;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    :try_end_228
    .catch Ljava/io/IOException; {:try_start_20b .. :try_end_228} :catch_372
    .catch Ljava/lang/Exception; {:try_start_20b .. :try_end_228} :catch_378

    .line 14116
    :goto_228
    new-instance v24, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;

    invoke-direct/range {v24 .. v24}, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;-><init>()V

    .line 14117
    .local v24, "signer":Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;
    new-instance v31, Ljava/lang/StringBuilder;

    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v32, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, "/Modified/tmp.apk"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    .line 14120
    .local v27, "tmpapk":Ljava/lang/String;
    :try_start_242
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$116;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    move-object/from16 v31, v0

    # invokes: Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getSignatureKeys()V
    invoke-static/range {v31 .. v31}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->access$1300(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;)V

    .line 14123
    new-instance v31, Ljava/io/File;

    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v33, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, "/Modified/Keys/"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, "testkey.x509.pem"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    invoke-direct/range {v31 .. v32}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v31 .. v31}, Ljava/io/File;->toURI()Ljava/net/URI;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/net/URI;->toURL()Ljava/net/URL;

    move-result-object v8

    .line 14124
    .local v8, "certUrl":Ljava/net/URL;
    move-object/from16 v0, v24

    invoke-virtual {v0, v8}, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->readPublicKey(Ljava/net/URL;)Ljava/security/cert/X509Certificate;

    move-result-object v7

    .line 14125
    .local v7, "cert":Ljava/security/cert/X509Certificate;
    new-instance v31, Ljava/io/File;

    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v33, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, "/Modified/Keys/"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, "testkey.sbt"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    invoke-direct/range {v31 .. v32}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v31 .. v31}, Ljava/io/File;->toURI()Ljava/net/URI;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/net/URI;->toURL()Ljava/net/URL;

    move-result-object v22

    .line 14126
    .local v22, "sbtUrl":Ljava/net/URL;
    move-object/from16 v0, v24

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->readContentAsBytes(Ljava/net/URL;)[B

    move-result-object v23

    .line 14127
    .local v23, "sigBlockTemplate":[B
    new-instance v31, Ljava/io/File;

    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v33, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, "/Modified/Keys/"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, "testkey.pk8"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    invoke-direct/range {v31 .. v32}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v31 .. v31}, Ljava/io/File;->toURI()Ljava/net/URI;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/net/URI;->toURL()Ljava/net/URL;

    move-result-object v21

    .line 14128
    .local v21, "privateKeyUrl":Ljava/net/URL;
    const-string v31, "lp"

    move-object/from16 v0, v24

    move-object/from16 v1, v21

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->readPrivateKey(Ljava/net/URL;Ljava/lang/String;)Ljava/security/PrivateKey;

    move-result-object v20

    .line 14129
    .local v20, "privateKey":Ljava/security/PrivateKey;
    const-string v31, "custom"

    move-object/from16 v0, v24

    move-object/from16 v1, v31

    move-object/from16 v2, v20

    move-object/from16 v3, v23

    invoke-virtual {v0, v1, v7, v2, v3}, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->setKeys(Ljava/lang/String;Ljava/security/cert/X509Certificate;Ljava/security/PrivateKey;[B)V

    .line 14130
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 14131
    .local v4, "add_files":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;>;"
    new-instance v31, Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;

    new-instance v32, Ljava/lang/StringBuilder;

    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v33, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, "/Modified/AndroidManifest.xml"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    new-instance v33, Ljava/lang/StringBuilder;

    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v34, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    const-string v34, "/Modified/"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-direct/range {v31 .. v33}, Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v31

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 14133
    move-object/from16 v0, v24

    move-object/from16 v1, v27

    invoke-virtual {v0, v14, v1, v4}, Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;->signZip(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 14136
    new-instance v31, Ljava/io/File;

    move-object/from16 v0, v31

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v31 .. v31}, Ljava/io/File;->exists()Z

    move-result v31

    if-nez v31, :cond_390

    .line 14138
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$116;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    move-object/from16 v31, v0

    new-instance v32, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$116$4;

    move-object/from16 v0, v32

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$116$4;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$116;)V

    invoke-virtual/range {v31 .. v32}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V
    :try_end_34b
    .catch Ljava/lang/Throwable; {:try_start_242 .. :try_end_34b} :catch_34d

    goto/16 :goto_83

    .line 14148
    .end local v4    # "add_files":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;>;"
    .end local v7    # "cert":Ljava/security/cert/X509Certificate;
    .end local v8    # "certUrl":Ljava/net/URL;
    .end local v20    # "privateKey":Ljava/security/PrivateKey;
    .end local v21    # "privateKeyUrl":Ljava/net/URL;
    .end local v22    # "sbtUrl":Ljava/net/URL;
    .end local v23    # "sigBlockTemplate":[B
    :catch_34d
    move-exception v26

    .line 14149
    .local v26, "t":Ljava/lang/Throwable;
    new-instance v5, Ljava/io/File;

    move-object/from16 v0, v27

    invoke-direct {v5, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 14150
    .local v5, "apkcrk":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v31

    if-eqz v31, :cond_35e

    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    .line 14152
    :cond_35e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$116;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    move-object/from16 v31, v0

    new-instance v32, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$116$5;

    move-object/from16 v0, v32

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$116$5;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$116;)V

    invoke-virtual/range {v31 .. v32}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V

    goto/16 :goto_83

    .line 14078
    .end local v5    # "apkcrk":Ljava/io/File;
    .end local v24    # "signer":Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;
    .end local v26    # "t":Ljava/lang/Throwable;
    .end local v27    # "tmpapk":Ljava/lang/String;
    :catch_372
    move-exception v9

    .line 14080
    .local v9, "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_228

    .line 14081
    .end local v9    # "e":Ljava/io/IOException;
    :catch_378
    move-exception v9

    .line 14082
    .local v9, "e":Ljava/lang/Exception;
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V

    .line 14084
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$116;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    move-object/from16 v31, v0

    new-instance v32, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$116$3;

    move-object/from16 v0, v32

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$116$3;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$116;)V

    invoke-virtual/range {v31 .. v32}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V

    goto/16 :goto_83

    .line 14163
    .end local v9    # "e":Ljava/lang/Exception;
    .restart local v4    # "add_files":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;>;"
    .restart local v7    # "cert":Ljava/security/cert/X509Certificate;
    .restart local v8    # "certUrl":Ljava/net/URL;
    .restart local v20    # "privateKey":Ljava/security/PrivateKey;
    .restart local v21    # "privateKeyUrl":Ljava/net/URL;
    .restart local v22    # "sbtUrl":Ljava/net/URL;
    .restart local v23    # "sigBlockTemplate":[B
    .restart local v24    # "signer":Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;
    .restart local v27    # "tmpapk":Ljava/lang/String;
    :cond_390
    new-instance v31, Ljava/io/File;

    move-object/from16 v0, v31

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v31 .. v31}, Ljava/io/File;->exists()Z

    move-result v31

    if-eqz v31, :cond_47c

    .line 14165
    :try_start_39f
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v31

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    invoke-virtual/range {v31 .. v33}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v31

    move-object/from16 v0, v31

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    move-object/from16 v25, v0

    .line 14166
    .local v25, "srcDir":Ljava/lang/String;
    const-string v31, "/mnt/asec/"

    move-object/from16 v0, v25

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v31

    if-eqz v31, :cond_3d8

    .line 14167
    const-string v31, "/pkg.apk"

    const-string v32, ""

    move-object/from16 v0, v31

    move-object/from16 v1, v32

    invoke-virtual {v14, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v31

    const-string v32, "rw"

    invoke-static/range {v31 .. v32}, Lcom/chelpus/Utils;->remount(Ljava/lang/String;Ljava/lang/String;)Z

    .line 14168
    :cond_3d8
    new-instance v31, Lcom/chelpus/Utils;

    const-string v32, ""

    invoke-direct/range {v31 .. v32}, Lcom/chelpus/Utils;-><init>(Ljava/lang/String;)V

    const/16 v32, 0x1

    move/from16 v0, v32

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    new-instance v34, Ljava/lang/StringBuilder;

    invoke-direct/range {v34 .. v34}, Ljava/lang/StringBuilder;-><init>()V

    const-string v35, "rm "

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    const/16 v35, 0x0

    move-object/from16 v0, v25

    move/from16 v1, v35

    invoke-static {v0, v1}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v35

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v34

    aput-object v34, v32, v33

    invoke-virtual/range {v31 .. v32}, Lcom/chelpus/Utils;->cmdRoot([Ljava/lang/String;)Ljava/lang/String;
    :try_end_40b
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_39f .. :try_end_40b} :catch_490
    .catch Ljava/lang/Exception; {:try_start_39f .. :try_end_40b} :catch_497

    .line 14177
    .end local v25    # "srcDir":Ljava/lang/String;
    :goto_40b
    sget-object v31, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v32, Lcom/chelpus/Utils;

    const-string v33, ""

    invoke-direct/range {v32 .. v33}, Lcom/chelpus/Utils;-><init>(Ljava/lang/String;)V

    const/16 v33, 0x2

    move/from16 v0, v33

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v33, v0

    const/16 v34, 0x0

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string v36, "pm uninstall "

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    move-object/from16 v36, v0

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    aput-object v35, v33, v34

    const/16 v34, 0x1

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string v36, "pm install -r -i com.android.vending \'"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    const-string v36, "\'"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    aput-object v35, v33, v34

    invoke-virtual/range {v32 .. v33}, Lcom/chelpus/Utils;->cmdRoot([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v32

    invoke-virtual/range {v31 .. v32}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 14178
    new-instance v31, Ljava/io/File;

    move-object/from16 v0, v31

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v31 .. v31}, Ljava/io/File;->exists()Z

    move-result v31

    if-eqz v31, :cond_47c

    new-instance v31, Ljava/io/File;

    move-object/from16 v0, v31

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v31 .. v31}, Ljava/io/File;->delete()Z

    .line 14195
    .end local v4    # "add_files":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;>;"
    .end local v7    # "cert":Ljava/security/cert/X509Certificate;
    .end local v8    # "certUrl":Ljava/net/URL;
    .end local v12    # "ex":Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample;
    .end local v13    # "extrFile":Ljava/lang/String;
    .end local v18    # "manifestFile":Ljava/io/File;
    .end local v20    # "privateKey":Ljava/security/PrivateKey;
    .end local v21    # "privateKeyUrl":Ljava/net/URL;
    .end local v22    # "sbtUrl":Ljava/net/URL;
    .end local v23    # "sigBlockTemplate":[B
    .end local v24    # "signer":Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;
    .end local v27    # "tmpapk":Ljava/lang/String;
    :cond_47c
    :goto_47c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$116;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    move-object/from16 v31, v0

    new-instance v32, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$116$7;

    move-object/from16 v0, v32

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$116$7;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$116;)V

    invoke-virtual/range {v31 .. v32}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V

    goto/16 :goto_83

    .line 14169
    .restart local v4    # "add_files":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;>;"
    .restart local v7    # "cert":Ljava/security/cert/X509Certificate;
    .restart local v8    # "certUrl":Ljava/net/URL;
    .restart local v12    # "ex":Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample;
    .restart local v13    # "extrFile":Ljava/lang/String;
    .restart local v18    # "manifestFile":Ljava/io/File;
    .restart local v20    # "privateKey":Ljava/security/PrivateKey;
    .restart local v21    # "privateKeyUrl":Ljava/net/URL;
    .restart local v22    # "sbtUrl":Ljava/net/URL;
    .restart local v23    # "sigBlockTemplate":[B
    .restart local v24    # "signer":Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;
    .restart local v27    # "tmpapk":Ljava/lang/String;
    :catch_490
    move-exception v10

    .line 14171
    .local v10, "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v10}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 14172
    const/4 v11, 0x1

    .line 14176
    goto/16 :goto_40b

    .line 14173
    .end local v10    # "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_497
    move-exception v9

    .line 14174
    .restart local v9    # "e":Ljava/lang/Exception;
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V

    .line 14175
    const/4 v11, 0x1

    goto/16 :goto_40b

    .line 14184
    .end local v4    # "add_files":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/AddFilesItem;>;"
    .end local v7    # "cert":Ljava/security/cert/X509Certificate;
    .end local v8    # "certUrl":Ljava/net/URL;
    .end local v9    # "e":Ljava/lang/Exception;
    .end local v12    # "ex":Lcom/android/vending/billing/InAppBillingService/LUCK/AxmlExample;
    .end local v13    # "extrFile":Ljava/lang/String;
    .end local v18    # "manifestFile":Ljava/io/File;
    .end local v20    # "privateKey":Ljava/security/PrivateKey;
    .end local v21    # "privateKeyUrl":Ljava/net/URL;
    .end local v22    # "sbtUrl":Ljava/net/URL;
    .end local v23    # "sigBlockTemplate":[B
    .end local v24    # "signer":Lcom/android/vending/billing/InAppBillingService/LUCK/ZipSignerLP;
    .end local v27    # "tmpapk":Ljava/lang/String;
    :cond_49e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$116;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    move-object/from16 v31, v0

    new-instance v32, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$116$6;

    move-object/from16 v0, v32

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$116$6;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$116;)V

    invoke-virtual/range {v31 .. v32}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V

    goto :goto_47c
.end method
