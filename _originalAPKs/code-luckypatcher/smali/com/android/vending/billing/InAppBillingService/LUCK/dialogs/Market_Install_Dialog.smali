.class public Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Market_Install_Dialog;
.super Ljava/lang/Object;
.source "Market_Install_Dialog.java"


# static fields
.field public static final LOADING_PROGRESS_DIALOG:I = 0x17


# instance fields
.field dialog:Landroid/app/Dialog;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Market_Install_Dialog;->dialog:Landroid/app/Dialog;

    .line 43
    return-void
.end method


# virtual methods
.method public dismiss()V
    .registers 2

    .prologue
    .line 346
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Market_Install_Dialog;->dialog:Landroid/app/Dialog;

    if-eqz v0, :cond_c

    .line 347
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Market_Install_Dialog;->dialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 348
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Market_Install_Dialog;->dialog:Landroid/app/Dialog;

    .line 350
    :cond_c
    return-void
.end method

.method public onCreateDialog()Landroid/app/Dialog;
    .registers 25

    .prologue
    .line 56
    sget-object v21, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v22, "Market install Dialog create."

    invoke-virtual/range {v21 .. v22}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 57
    sget-object v21, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    if-eqz v21, :cond_13

    sget-object v21, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual/range {v21 .. v21}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v21

    if-nez v21, :cond_16

    :cond_13
    invoke-virtual/range {p0 .. p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Market_Install_Dialog;->dismiss()V

    .line 58
    :cond_16
    sget-object v21, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual/range {v21 .. v21}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v21

    const v22, 0x7f04002a

    const/16 v23, 0x0

    invoke-static/range {v21 .. v23}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout;

    .line 59
    .local v6, "d":Landroid/widget/LinearLayout;
    const v21, 0x7f0d00a7

    move/from16 v0, v21

    invoke-virtual {v6, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v21

    const v22, 0x7f0d000c

    invoke-virtual/range {v21 .. v22}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/LinearLayout;

    .line 60
    .local v17, "scrbody":Landroid/widget/LinearLayout;
    const v21, 0x7f0d00b4

    move-object/from16 v0, v17

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Landroid/widget/TextView;

    .line 61
    .local v20, "text":Landroid/widget/TextView;
    const v21, 0x7f0d000e

    move-object/from16 v0, v17

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/RadioGroup;

    .line 62
    .local v8, "group":Landroid/widget/RadioGroup;
    invoke-virtual {v8}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    .line 63
    const-string v21, "mod.market53.apk"

    sput-object v21, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->verMarket:Ljava/lang/String;

    .line 64
    const v21, 0x7f0d00ab

    move/from16 v0, v21

    invoke-virtual {v8, v0}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/RadioButton;

    .line 65
    .local v9, "rb4":Landroid/widget/RadioButton;
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const v22, 0x7f070155

    invoke-static/range {v22 .. v22}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " 4.1.6 (Android 2.2 and UP)"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v9, v0}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 66
    const v21, 0x7f0d00b1

    move/from16 v0, v21

    invoke-virtual {v8, v0}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/RadioButton;

    .line 67
    .local v12, "rb5":Landroid/widget/RadioButton;
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const v22, 0x7f070156

    invoke-static/range {v22 .. v22}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " 4.1.6 (Android 2.2 and UP)"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v12, v0}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 68
    const v21, 0x7f0d00aa

    move/from16 v0, v21

    invoke-virtual {v8, v0}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/RadioButton;

    .line 69
    .local v10, "rb44":Landroid/widget/RadioButton;
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const v22, 0x7f070155

    invoke-static/range {v22 .. v22}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " 4.9.13 (Android 2.2 and UP)"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v10, v0}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 70
    const v21, 0x7f0d00b0

    move/from16 v0, v21

    invoke-virtual {v8, v0}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/RadioButton;

    .line 71
    .local v11, "rb44o":Landroid/widget/RadioButton;
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const v22, 0x7f070156

    invoke-static/range {v22 .. v22}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " 4.9.13 (Android 2.2 and UP)"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v11, v0}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 72
    const v21, 0x7f0d00a9

    move/from16 v0, v21

    invoke-virtual {v8, v0}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/RadioButton;

    .line 73
    .local v13, "rb50":Landroid/widget/RadioButton;
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const v22, 0x7f070155

    invoke-static/range {v22 .. v22}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " 5.1.11 (Android 2.2 and UP)"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v13, v0}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 74
    const v21, 0x7f0d00af

    move/from16 v0, v21

    invoke-virtual {v8, v0}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/RadioButton;

    .line 75
    .local v14, "rb50o":Landroid/widget/RadioButton;
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const v22, 0x7f070156

    invoke-static/range {v22 .. v22}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " 5.1.11 (Android 2.2 and UP)"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v14, v0}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 76
    const v21, 0x7f0d00a8

    move/from16 v0, v21

    invoke-virtual {v8, v0}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/RadioButton;

    .line 77
    .local v15, "rb53":Landroid/widget/RadioButton;
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const v22, 0x7f070155

    invoke-static/range {v22 .. v22}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " 5.3.6 (Android 2.2 and UP)"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v15, v0}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 78
    const v21, 0x7f0d00ae

    move/from16 v0, v21

    invoke-virtual {v8, v0}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/RadioButton;

    .line 79
    .local v16, "rb53o":Landroid/widget/RadioButton;
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const v22, 0x7f070156

    invoke-static/range {v22 .. v22}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " 5.3.6 (Android 2.2 and UP)"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 81
    new-instance v21, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Market_Install_Dialog$1;

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Market_Install_Dialog$1;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Market_Install_Dialog;)V

    move-object/from16 v0, v21

    invoke-virtual {v8, v0}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 127
    const v21, 0x7f0d00b6

    move/from16 v0, v21

    invoke-virtual {v6, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    .line 128
    .local v4, "button":Landroid/widget/Button;
    const v21, 0x7f0d00b5

    move/from16 v0, v21

    invoke-virtual {v6, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    .line 129
    .local v5, "chk":Landroid/widget/CheckBox;
    const/16 v21, 0x0

    sput-boolean v21, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->asUser:Z

    .line 130
    const/16 v21, 0x0

    sput-boolean v21, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->asNeedUser:Z

    .line 133
    :try_start_1d8
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v21

    const-string v22, "com.android.vending"

    const/16 v23, 0x0

    invoke-virtual/range {v21 .. v23}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    .line 134
    .local v3, "ai":Landroid/content/pm/ApplicationInfo;
    invoke-static {}, Lcom/chelpus/Utils;->checkCoreJarPatch11()Z

    move-result v21

    if-eqz v21, :cond_2aa

    invoke-static {}, Lcom/chelpus/Utils;->checkCoreJarPatch12()Z

    move-result v21

    if-eqz v21, :cond_2aa

    invoke-static {}, Lcom/chelpus/Utils;->checkCoreJarPatch20()Z

    move-result v21

    if-eqz v21, :cond_2aa

    iget v0, v3, Landroid/content/pm/ApplicationInfo;->flags:I

    move/from16 v21, v0

    and-int/lit8 v21, v21, 0x1

    if-eqz v21, :cond_2aa

    .line 135
    const/16 v21, 0x1

    move/from16 v0, v21

    invoke-virtual {v5, v0}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 136
    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v5, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 137
    const/16 v21, 0x0

    sput-boolean v21, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->asUser:Z

    .line 138
    const/16 v21, 0x1

    sput-boolean v21, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->asNeedUser:Z
    :try_end_214
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1d8 .. :try_end_214} :catch_2c2
    .catch Ljava/lang/Exception; {:try_start_1d8 .. :try_end_214} :catch_2db

    .line 157
    .end local v3    # "ai":Landroid/content/pm/ApplicationInfo;
    :goto_214
    new-instance v21, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Market_Install_Dialog$2;

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Market_Install_Dialog$2;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Market_Install_Dialog;)V

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 176
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "  "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const v22, 0x7f070141

    invoke-static/range {v22 .. v22}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    .line 178
    .local v18, "str2":Ljava/lang/String;
    const v21, 0xff00

    const-string v22, "bold"

    move-object/from16 v0, v18

    move/from16 v1, v21

    move-object/from16 v2, v22

    invoke-static {v0, v1, v2}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;ILjava/lang/String;)Landroid/text/SpannableString;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 179
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "\n\n"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const v22, 0x7f07014f

    invoke-static/range {v22 .. v22}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 180
    new-instance v21, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Market_Install_Dialog$3;

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Market_Install_Dialog$3;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Market_Install_Dialog;)V

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 333
    new-instance v21, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    sget-object v22, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual/range {v22 .. v22}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getContext()Landroid/support/v4/app/FragmentActivity;

    move-result-object v22

    const/16 v23, 0x1

    invoke-direct/range {v21 .. v23}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;-><init>(Landroid/content/Context;Z)V

    move-object/from16 v0, v21

    invoke-virtual {v0, v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->setView(Landroid/view/View;)Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/android/vending/billing/InAppBillingService/LUCK/AlertDlg;->create()Landroid/app/Dialog;

    move-result-object v19

    .line 334
    .local v19, "tempdialog":Landroid/app/Dialog;
    const/16 v21, 0x1

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 335
    new-instance v21, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Market_Install_Dialog$4;

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Market_Install_Dialog$4;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Market_Install_Dialog;)V

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 341
    return-object v19

    .line 140
    .end local v18    # "str2":Ljava/lang/String;
    .end local v19    # "tempdialog":Landroid/app/Dialog;
    .restart local v3    # "ai":Landroid/content/pm/ApplicationInfo;
    :cond_2aa
    const/16 v21, 0x0

    :try_start_2ac
    sput-boolean v21, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->asUser:Z

    .line 141
    const/16 v21, 0x0

    sput-boolean v21, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->asNeedUser:Z

    .line 142
    const/16 v21, 0x1

    move/from16 v0, v21

    invoke-virtual {v5, v0}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 143
    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v5, v0}, Landroid/widget/CheckBox;->setChecked(Z)V
    :try_end_2c0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2ac .. :try_end_2c0} :catch_2c2
    .catch Ljava/lang/Exception; {:try_start_2ac .. :try_end_2c0} :catch_2db

    goto/16 :goto_214

    .line 145
    .end local v3    # "ai":Landroid/content/pm/ApplicationInfo;
    :catch_2c2
    move-exception v7

    .line 146
    .local v7, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/16 v21, 0x0

    sput-boolean v21, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->asUser:Z

    .line 147
    const/16 v21, 0x0

    sput-boolean v21, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->asNeedUser:Z

    .line 148
    const/16 v21, 0x1

    move/from16 v0, v21

    invoke-virtual {v5, v0}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 149
    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v5, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto/16 :goto_214

    .line 150
    .end local v7    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_2db
    move-exception v7

    .line 151
    .local v7, "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    .line 152
    const/16 v21, 0x0

    sput-boolean v21, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->asUser:Z

    .line 153
    const/16 v21, 0x0

    sput-boolean v21, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->asNeedUser:Z

    .line 154
    const/16 v21, 0x1

    move/from16 v0, v21

    invoke-virtual {v5, v0}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 155
    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v5, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto/16 :goto_214
.end method

.method public showDialog()V
    .registers 2

    .prologue
    .line 46
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Market_Install_Dialog;->dialog:Landroid/app/Dialog;

    if-nez v0, :cond_a

    .line 47
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Market_Install_Dialog;->onCreateDialog()Landroid/app/Dialog;

    move-result-object v0

    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Market_Install_Dialog;->dialog:Landroid/app/Dialog;

    .line 49
    :cond_a
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Market_Install_Dialog;->dialog:Landroid/app/Dialog;

    if-eqz v0, :cond_13

    .line 50
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/Market_Install_Dialog;->dialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 52
    :cond_13
    return-void
.end method
