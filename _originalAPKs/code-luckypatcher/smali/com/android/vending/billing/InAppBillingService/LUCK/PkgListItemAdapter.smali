.class public Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;
.super Landroid/widget/BaseExpandableListAdapter;
.source "PkgListItemAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter$Refresh_Packages;
    }
.end annotation


# static fields
.field public static final TEXT_DEFAULT:I = 0x0

.field public static final TEXT_LARGE:I = 0x2

.field public static final TEXT_MEDIUM:I = 0x1

.field public static final TEXT_SMALL:I


# instance fields
.field Ready:Z

.field add:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;",
            ">;"
        }
    .end annotation
.end field

.field public childMenu:[I

.field public childMenuNoRoot:[I

.field public childMenuSystem:[I

.field private context:Landroid/content/Context;

.field public data:[Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

.field del:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;",
            ">;"
        }
    .end annotation
.end field

.field private disabled:Landroid/graphics/drawable/Drawable;

.field private imgIcon:Landroid/widget/ImageView;

.field private layoutResourceId:I

.field private on_top_txt:Landroid/widget/TextView;

.field public orig:[Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

.field private size:I

.field public sorter:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;",
            ">;"
        }
    .end annotation
.end field

.field private txtStatus:Landroid/widget/TextView;

.field private txtTitle:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;IILjava/util/List;)V
    .registers 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layout"    # I
    .param p3, "s"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "II",
            "Ljava/util/List",
            "<",
            "Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p4, "p":Ljava/util/List;, "Ljava/util/List<Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;>;"
    const/4 v3, 0x0

    .line 77
    invoke-direct {p0}, Landroid/widget/BaseExpandableListAdapter;-><init>()V

    .line 46
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->Ready:Z

    .line 50
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020020

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->disabled:Landroid/graphics/drawable/Drawable;

    .line 57
    iput-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->childMenu:[I

    .line 58
    iput-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->childMenuNoRoot:[I

    .line 59
    iput-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->childMenuSystem:[I

    .line 79
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->context:Landroid/content/Context;

    .line 80
    iput p2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->layoutResourceId:I

    .line 82
    iput p3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->size:I

    .line 83
    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    invoke-interface {p4, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    check-cast v1, [Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    iput-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->data:[Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    .line 84
    sput-object p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->plia:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;

    .line 85
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->disabled:Landroid/graphics/drawable/Drawable;

    .line 86
    .local v0, "iconka":Landroid/graphics/drawable/Drawable;
    const-string v1, "#fe6c00"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 87
    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->disabled:Landroid/graphics/drawable/Drawable;

    .line 89
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/util/List;)V
    .registers 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layout"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p3, "p":Ljava/util/List;, "Ljava/util/List<Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;>;"
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 64
    invoke-direct {p0}, Landroid/widget/BaseExpandableListAdapter;-><init>()V

    .line 46
    iput-boolean v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->Ready:Z

    .line 50
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020020

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->disabled:Landroid/graphics/drawable/Drawable;

    .line 57
    iput-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->childMenu:[I

    .line 58
    iput-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->childMenuNoRoot:[I

    .line 59
    iput-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->childMenuSystem:[I

    .line 66
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->context:Landroid/content/Context;

    .line 67
    iput p2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->layoutResourceId:I

    .line 69
    iput v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->size:I

    .line 70
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    invoke-interface {p3, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    check-cast v1, [Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    iput-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->data:[Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    .line 71
    sput-object p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->plia:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;

    .line 72
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->disabled:Landroid/graphics/drawable/Drawable;

    .line 73
    .local v0, "iconka":Landroid/graphics/drawable/Drawable;
    const-string v1, "#fe6c00"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 74
    iput-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->disabled:Landroid/graphics/drawable/Drawable;

    .line 75
    return-void
.end method

.method static synthetic access$000(Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;)Landroid/content/Context;
    .registers 2
    .param p0, "x0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->context:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public add(Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;)V
    .registers 6
    .param p1, "item"    # Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    .prologue
    .line 577
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "add "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 578
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->data:[Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 579
    .local v0, "tmp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;>;"
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 580
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    iput-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->data:[Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    .line 581
    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->data:[Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 582
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->sort()V

    .line 583
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->notifyDataSetChanged()V

    .line 584
    return-void
.end method

.method public checkItem(Ljava/lang/String;)Z
    .registers 9
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 606
    :try_start_1
    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->data:[Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    array-length v5, v4

    move v3, v2

    :goto_5
    if-ge v3, v5, :cond_12

    aget-object v0, v4, v3

    .line 607
    .local v0, "aData":Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    iget-object v6, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    invoke-virtual {v6, p1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_e} :catch_16

    move-result v6

    if-eqz v6, :cond_13

    .line 608
    const/4 v2, 0x1

    .line 614
    .end local v0    # "aData":Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    :cond_12
    :goto_12
    return v2

    .line 606
    .restart local v0    # "aData":Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    :cond_13
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    .line 611
    .end local v0    # "aData":Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    :catch_16
    move-exception v1

    .line 612
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_12
.end method

.method public getChild(II)Ljava/lang/Integer;
    .registers 4
    .param p1, "arg0"    # I
    .param p2, "arg1"    # I

    .prologue
    .line 94
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->childMenu:[I

    aget v0, v0, p2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getChild(II)Ljava/lang/Object;
    .registers 4

    .prologue
    .line 36
    invoke-virtual {p0, p1, p2}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->getChild(II)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getChildId(II)J
    .registers 5
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I

    .prologue
    .line 99
    mul-int/lit8 v0, p1, 0xa

    add-int/2addr v0, p2

    int-to-long v0, v0

    return-wide v0
.end method

.method public getChildView(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 13
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I
    .param p3, "isLastChild"    # Z
    .param p4, "convertView"    # Landroid/view/View;
    .param p5, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const v6, 0x7f02003f

    const v5, 0x7f020041

    .line 933
    if-nez p4, :cond_1a

    .line 934
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->context:Landroid/content/Context;

    const-string v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 935
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v3, 0x7f04001c

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p4

    .line 938
    .end local v1    # "inflater":Landroid/view/LayoutInflater;
    :cond_1a
    const v3, 0x7f0d007e

    invoke-virtual {p4, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 940
    .local v2, "textChild":Landroid/widget/TextView;
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->context:Landroid/content/Context;

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getSizeText()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 941
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->context:Landroid/content/Context;

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getSizeText()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 948
    invoke-virtual {p0, p1, p2}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->getChild(II)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v3}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 950
    const v3, 0x7f0d007d

    invoke-virtual {p4, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 951
    .local v0, "image":Landroid/widget/ImageView;
    invoke-virtual {p0, p1, p2}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->getChild(II)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    sparse-switch v3, :sswitch_data_d2

    .line 983
    :goto_58
    return-object p4

    .line 953
    :sswitch_59
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f02004c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_58

    .line 956
    :sswitch_68
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_58

    .line 959
    :sswitch_74
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_58

    .line 962
    :sswitch_80
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f02004a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_58

    .line 965
    :sswitch_8f
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f020019

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_58

    .line 968
    :sswitch_9e
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_58

    .line 971
    :sswitch_aa
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_58

    .line 974
    :sswitch_b6
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_58

    .line 977
    :sswitch_c2
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f020035

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_58

    .line 951
    nop

    :sswitch_data_d2
    .sparse-switch
        0x7f07000d -> :sswitch_68
        0x7f070016 -> :sswitch_b6
        0x7f070017 -> :sswitch_c2
        0x7f070045 -> :sswitch_8f
        0x7f070068 -> :sswitch_74
        0x7f07008a -> :sswitch_59
        0x7f070172 -> :sswitch_aa
        0x7f070174 -> :sswitch_9e
        0x7f070223 -> :sswitch_80
    .end sparse-switch
.end method

.method public getChildrenCount(I)I
    .registers 12
    .param p1, "groupPosition"    # I

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x6

    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 104
    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->getGroup(I)Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    move-result-object v3

    if-eqz v3, :cond_fd

    .line 105
    const/4 v0, 0x0

    .line 109
    .local v0, "directory":Ljava/lang/String;
    :try_start_c
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->getGroup(I)Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    move-result-object v4

    iget-object v4, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    iget-object v3, v3, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v3, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;
    :try_end_1f
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_c .. :try_end_1f} :catch_102
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_1f} :catch_108

    .line 119
    :goto_1f
    sget-boolean v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->su:Z

    if-eqz v3, :cond_76

    if-eqz v0, :cond_76

    const-string v3, "/system/"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_76

    .line 122
    const/16 v3, 0x8

    new-array v3, v3, [I

    iput-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->childMenu:[I

    .line 123
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->childMenu:[I

    const v4, 0x7f070017

    aput v4, v3, v2

    .line 124
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->childMenu:[I

    const v4, 0x7f07008a

    aput v4, v3, v6

    .line 125
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->childMenu:[I

    const v4, 0x7f07000d

    aput v4, v3, v8

    .line 126
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->childMenu:[I

    const v4, 0x7f070068

    aput v4, v3, v9

    .line 127
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->childMenu:[I

    const/4 v4, 0x4

    const v5, 0x7f070223

    aput v5, v3, v4

    .line 128
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->childMenu:[I

    const/4 v4, 0x5

    const v5, 0x7f070045

    aput v5, v3, v4

    .line 129
    const-string v3, "/mnt/asec/"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_10e

    .line 130
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->childMenu:[I

    const v4, 0x7f070174

    aput v4, v3, v7

    .line 132
    :goto_6e
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->childMenu:[I

    const/4 v4, 0x7

    const v5, 0x7f070016

    aput v5, v3, v4

    .line 134
    :cond_76
    sget-boolean v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->su:Z

    if-eqz v3, :cond_bc

    if-eqz v0, :cond_bc

    const-string v3, "/system/"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_bc

    .line 135
    const/4 v3, 0x7

    new-array v3, v3, [I

    iput-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->childMenu:[I

    .line 136
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->childMenu:[I

    const v4, 0x7f070017

    aput v4, v3, v2

    .line 137
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->childMenu:[I

    const v4, 0x7f07008a

    aput v4, v3, v6

    .line 138
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->childMenu:[I

    const v4, 0x7f07000d

    aput v4, v3, v8

    .line 139
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->childMenu:[I

    const v4, 0x7f070068

    aput v4, v3, v9

    .line 140
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->childMenu:[I

    const/4 v4, 0x4

    const v5, 0x7f070223

    aput v5, v3, v4

    .line 141
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->childMenu:[I

    const/4 v4, 0x5

    const v5, 0x7f070045

    aput v5, v3, v4

    .line 142
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->childMenu:[I

    const v4, 0x7f070016

    aput v4, v3, v7

    .line 144
    :cond_bc
    sget-boolean v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->su:Z

    if-nez v3, :cond_f0

    .line 145
    new-array v3, v7, [I

    iput-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->childMenu:[I

    .line 146
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->childMenu:[I

    const v4, 0x7f070017

    aput v4, v3, v2

    .line 147
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->childMenu:[I

    const v4, 0x7f07008a

    aput v4, v3, v6

    .line 148
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->childMenu:[I

    const v4, 0x7f07000d

    aput v4, v3, v8

    .line 149
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->childMenu:[I

    const v4, 0x7f070068

    aput v4, v3, v9

    .line 150
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->childMenu:[I

    const/4 v4, 0x4

    const v5, 0x7f070223

    aput v5, v3, v4

    .line 151
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->childMenu:[I

    const/4 v4, 0x5

    const v5, 0x7f070016

    aput v5, v3, v4

    .line 153
    :cond_f0
    if-nez v0, :cond_fd

    .line 154
    new-array v3, v6, [I

    iput-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->childMenu:[I

    .line 155
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->childMenu:[I

    const v4, 0x7f07023d

    aput v4, v3, v2

    .line 159
    .end local v0    # "directory":Ljava/lang/String;
    :cond_fd
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->childMenu:[I

    if-nez v3, :cond_117

    .line 160
    :goto_101
    return v2

    .line 110
    .restart local v0    # "directory":Ljava/lang/String;
    :catch_102
    move-exception v1

    .line 112
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_103
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V
    :try_end_106
    .catch Ljava/lang/Exception; {:try_start_103 .. :try_end_106} :catch_108

    goto/16 :goto_1f

    .line 116
    .end local v1    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_108
    move-exception v1

    .line 117
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_1f

    .line 131
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_10e
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->childMenu:[I

    const v4, 0x7f070172

    aput v4, v3, v7

    goto/16 :goto_6e

    .line 160
    .end local v0    # "directory":Ljava/lang/String;
    :cond_117
    iget-object v2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->childMenu:[I

    array-length v2, v2

    goto :goto_101
.end method

.method public getFilter()Landroid/widget/Filter;
    .registers 2

    .prologue
    .line 986
    new-instance v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter$2;

    invoke-direct {v0, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter$2;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;)V

    return-object v0
.end method

.method public getGroup(I)Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    .registers 3
    .param p1, "groupPosition"    # I

    .prologue
    .line 634
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->data:[Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public bridge synthetic getGroup(I)Ljava/lang/Object;
    .registers 3

    .prologue
    .line 36
    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->getGroup(I)Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    move-result-object v0

    return-object v0
.end method

.method public getGroupCount()I
    .registers 2

    .prologue
    .line 170
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->data:[Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    array-length v0, v0

    return v0
.end method

.method public getGroupId(I)J
    .registers 4
    .param p1, "groupPosition"    # I

    .prologue
    .line 175
    int-to-long v0, p1

    return-wide v0
.end method

.method public getGroupView(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 47
    .param p1, "groupPosition"    # I
    .param p2, "isExpanded"    # Z
    .param p3, "convertView"    # Landroid/view/View;
    .param p4, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 181
    :try_start_0
    invoke-virtual/range {p0 .. p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->getGroup(I)Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    move-result-object v27

    .line 182
    .local v27, "p":Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    if-eqz v27, :cond_c

    move-object/from16 v0, v27

    iget-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->hidden:Z

    if-eqz v3, :cond_18

    .line 183
    :cond_c
    new-instance v29, Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->context:Landroid/content/Context;

    move-object/from16 v0, v29

    invoke-direct {v0, v3}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 509
    .end local v27    # "p":Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    :cond_17
    :goto_17
    return-object v29

    .line 186
    .restart local v27    # "p":Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    :cond_18
    move-object/from16 v29, p3

    .line 189
    .local v29, "row":Landroid/view/View;
    if-nez v29, :cond_35

    .line 191
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->context:Landroid/content/Context;

    const-string v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Landroid/view/LayoutInflater;

    .line 193
    .local v23, "inflater":Landroid/view/LayoutInflater;
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->layoutResourceId:I

    const/4 v4, 0x0

    move-object/from16 v0, v23

    move-object/from16 v1, p4

    invoke-virtual {v0, v3, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v29

    .line 203
    .end local v23    # "inflater":Landroid/view/LayoutInflater;
    :cond_35
    const v3, 0x7f0d004e

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->txtTitle:Landroid/widget/TextView;

    .line 204
    const v3, 0x7f0d004f

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->txtStatus:Landroid/widget/TextView;

    .line 205
    const v3, 0x7f0d0038

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->imgIcon:Landroid/widget/ImageView;

    .line 206
    const v3, 0x7f0d00cc

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->on_top_txt:Landroid/widget/TextView;

    .line 207
    const v3, 0x7f0d0080

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/CheckBox;

    .line 208
    .local v16, "chk":Landroid/widget/CheckBox;
    const v3, 0x7f0d00cb

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/CheckBox;

    .line 209
    .local v17, "chk2":Landroid/widget/CheckBox;
    const v3, 0x7f0d00cd

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/TextView;

    .line 210
    .local v18, "chk3":Landroid/widget/TextView;
    const v3, 0x7f0d00ca

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v34

    check-cast v34, Landroid/widget/CheckBox;

    .line 211
    .local v34, "select":Landroid/widget/CheckBox;
    sget-boolean v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->adapterSelect:Z

    if-eqz v3, :cond_2f3

    .line 213
    const/4 v3, 0x0

    move-object/from16 v0, v34

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 214
    const/16 v3, 0x8

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 215
    const/16 v3, 0x8

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 216
    const/16 v3, 0x8

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 217
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->on_top_txt:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 218
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v34

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setTag(Ljava/lang/Object;)V

    .line 219
    new-instance v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter$1;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter$1;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;)V

    move-object/from16 v0, v34

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 249
    :cond_da
    :goto_da
    move-object/from16 v0, v27

    iget-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->selected:Z

    if-eqz v3, :cond_32a

    const/4 v3, 0x1

    move-object/from16 v0, v34

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 252
    :goto_e6
    move-object/from16 v0, v27

    iget-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->modified:Z

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 253
    const/4 v3, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 254
    move-object/from16 v0, v27

    iget-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->system:Z

    if-nez v3, :cond_343

    .line 255
    move-object/from16 v0, v27

    iget-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->on_sd:Z

    if-eqz v3, :cond_332

    .line 256
    const-string v3, "SD"

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 257
    const/16 v3, -0x100

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 273
    :goto_10f
    const/4 v3, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setClickable(Z)V

    .line 274
    move-object/from16 v0, v27

    iget-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->odex:Z

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 275
    const/4 v3, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 278
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->txtTitle:Landroid/widget/TextView;

    move-object/from16 v0, v27

    iget-object v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_12f
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_12f} :catch_31c

    .line 287
    :try_start_12f
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v3

    const-string v4, "no_icon"

    const/4 v8, 0x0

    invoke-interface {v3, v4, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_1ca

    .line 288
    sget-boolean v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->goodMemory:Z

    if-eqz v3, :cond_3f3

    .line 290
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->imgIcon:Landroid/widget/ImageView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 291
    move-object/from16 v0, v27

    iget-object v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->icon:Landroid/graphics/drawable/Drawable;

    if-nez v3, :cond_1b9

    .line 293
    const/16 v32, 0x23

    .line 294
    .local v32, "scaleImage":I
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v0, v3, Landroid/util/DisplayMetrics;->density:F

    move/from16 v30, v0
    :try_end_15c
    .catch Ljava/lang/OutOfMemoryError; {:try_start_12f .. :try_end_15c} :catch_384
    .catch Ljava/lang/Exception; {:try_start_12f .. :try_end_15c} :catch_3a8

    .line 295
    .local v30, "scale":F
    const/high16 v3, 0x420c0000    # 35.0f

    mul-float v3, v3, v30

    const/high16 v4, 0x3f000000    # 0.5f

    add-float/2addr v3, v4

    float-to-int v0, v3

    move/from16 v22, v0

    .line 297
    .local v22, "imgSize":I
    const/4 v2, 0x0

    .line 299
    .local v2, "iconka":Landroid/graphics/Bitmap;
    :try_start_167
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v3

    move-object/from16 v0, v27

    iget-object v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/content/pm/PackageManager;->getApplicationIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    check-cast v3, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;
    :try_end_178
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_167 .. :try_end_178} :catch_37e
    .catch Ljava/lang/OutOfMemoryError; {:try_start_167 .. :try_end_178} :catch_384
    .catch Ljava/lang/Exception; {:try_start_167 .. :try_end_178} :catch_3a8

    move-result-object v2

    .line 304
    :goto_179
    :try_start_179
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    .line 305
    .local v5, "width":I
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    .line 307
    .local v6, "height":I
    move/from16 v26, v22

    .line 308
    .local v26, "newWidth":I
    move/from16 v25, v22

    .line 309
    .local v25, "newHeight":I
    move/from16 v0, v26

    int-to-float v3, v0

    int-to-float v4, v5

    div-float v33, v3, v4

    .line 310
    .local v33, "scaleWidth":F
    move/from16 v0, v25

    int-to-float v3, v0

    int-to-float v4, v6

    div-float v31, v3, v4

    .line 312
    .local v31, "scaleHeight":F
    new-instance v7, Landroid/graphics/Matrix;

    invoke-direct {v7}, Landroid/graphics/Matrix;-><init>()V

    .line 313
    .local v7, "bMatrix":Landroid/graphics/Matrix;
    move/from16 v0, v33

    move/from16 v1, v31

    invoke-virtual {v7, v0, v1}, Landroid/graphics/Matrix;->postScale(FF)Z
    :try_end_19d
    .catch Ljava/lang/OutOfMemoryError; {:try_start_179 .. :try_end_19d} :catch_384
    .catch Ljava/lang/Exception; {:try_start_179 .. :try_end_19d} :catch_3a8

    .line 314
    const/16 v28, 0x0

    .line 316
    .local v28, "resizedBitmap":Landroid/graphics/Bitmap;
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v8, 0x1

    :try_start_1a2
    invoke-static/range {v2 .. v8}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v28

    .line 317
    new-instance v15, Landroid/graphics/drawable/BitmapDrawable;

    move-object/from16 v0, v28

    invoke-direct {v15, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    .line 319
    .local v15, "bmd":Landroid/graphics/drawable/BitmapDrawable;
    move-object/from16 v0, v27

    iput-object v15, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->icon:Landroid/graphics/drawable/Drawable;
    :try_end_1b1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1a2 .. :try_end_1b1} :catch_39f
    .catch Ljava/lang/Exception; {:try_start_1a2 .. :try_end_1b1} :catch_3a8

    .line 322
    const/4 v2, 0x0

    .line 323
    const/4 v15, 0x0

    .line 325
    const/4 v7, 0x0

    .line 333
    .end local v15    # "bmd":Landroid/graphics/drawable/BitmapDrawable;
    :goto_1b4
    const/16 v28, 0x0

    .line 335
    :try_start_1b6
    invoke-virtual/range {v27 .. v27}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->saveItem()V

    .line 337
    .end local v2    # "iconka":Landroid/graphics/Bitmap;
    .end local v5    # "width":I
    .end local v6    # "height":I
    .end local v7    # "bMatrix":Landroid/graphics/Matrix;
    .end local v22    # "imgSize":I
    .end local v25    # "newHeight":I
    .end local v26    # "newWidth":I
    .end local v28    # "resizedBitmap":Landroid/graphics/Bitmap;
    .end local v30    # "scale":F
    .end local v31    # "scaleHeight":F
    .end local v32    # "scaleImage":I
    .end local v33    # "scaleWidth":F
    :cond_1b9
    move-object/from16 v0, v27

    iget-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->enable:Z

    if-nez v3, :cond_3e6

    .line 338
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->imgIcon:Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->disabled:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    :try_end_1ca
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1b6 .. :try_end_1ca} :catch_384
    .catch Ljava/lang/Exception; {:try_start_1b6 .. :try_end_1ca} :catch_3a8

    .line 367
    :cond_1ca
    :goto_1ca
    :try_start_1ca
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->txtTitle:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->context:Landroid/content/Context;

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getSizeText()I

    move-result v8

    invoke-virtual {v3, v4, v8}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 368
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->txtStatus:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->context:Landroid/content/Context;

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getSizeText()I

    move-result v8

    invoke-virtual {v3, v4, v8}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 375
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->txtStatus:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->context:Landroid/content/Context;

    const v8, 0x1030046

    invoke-virtual {v3, v4, v8}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 377
    const-string v19, "#ffcc7943"

    .line 379
    .local v19, "color":Ljava/lang/String;
    move-object/from16 v0, v27

    iget v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->stored:I

    packed-switch v3, :pswitch_data_600

    .line 394
    :goto_1ff
    :pswitch_1ff
    move-object/from16 v0, v27

    iget v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->stored:I

    if-nez v3, :cond_213

    move-object/from16 v0, v27

    iget-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->ads:Z

    if-nez v3, :cond_213

    move-object/from16 v0, v27

    iget-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->lvl:Z

    if-nez v3, :cond_213

    const-string v19, "#ffff0055"

    .line 395
    :cond_213
    move-object/from16 v0, v27

    iget v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->stored:I

    if-nez v3, :cond_221

    move-object/from16 v0, v27

    iget-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->ads:Z

    if-eqz v3, :cond_221

    const-string v19, "#ff00ffff"

    .line 396
    :cond_221
    move-object/from16 v0, v27

    iget v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->stored:I

    if-nez v3, :cond_22f

    move-object/from16 v0, v27

    iget-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->lvl:Z

    if-eqz v3, :cond_22f

    const-string v19, "#ff00ff73"

    .line 398
    :cond_22f
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->txtTitle:Landroid/widget/TextView;

    invoke-static/range {v19 .. v19}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 400
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->txtStatus:Landroid/widget/TextView;

    const v4, -0x777778

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 411
    const-string v35, ""

    .line 413
    .local v35, "statusi":Ljava/lang/String;
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v3

    const-string v4, "force_language"

    const-string v8, "default"

    invoke-interface {v3, v4, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    .line 416
    .local v24, "lng":Ljava/lang/String;
    const-string v3, "default"

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_419

    .line 417
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget-object v14, v3, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 419
    .local v14, "appLoc3":Ljava/util/Locale;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Locale;->setDefault(Ljava/util/Locale;)V

    .line 420
    new-instance v12, Landroid/content/res/Configuration;

    invoke-direct {v12}, Landroid/content/res/Configuration;-><init>()V

    .line 421
    .local v12, "appConfig3":Landroid/content/res/Configuration;
    iput-object v14, v12, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;
    :try_end_274
    .catch Ljava/lang/Exception; {:try_start_1ca .. :try_end_274} :catch_31c

    .line 423
    :try_start_274
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v3

    .line 424
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    .line 423
    invoke-virtual {v3, v12, v4}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V
    :try_end_283
    .catch Ljava/lang/Exception; {:try_start_274 .. :try_end_283} :catch_413

    .line 444
    .end local v12    # "appConfig3":Landroid/content/res/Configuration;
    .end local v14    # "appLoc3":Ljava/util/Locale;
    :goto_283
    :try_start_283
    move-object/from16 v0, v27

    iget-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->ads:Z

    if-eqz v3, :cond_290

    const v3, 0x7f0701f2

    invoke-static {v3}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v35

    .line 445
    :cond_290
    move-object/from16 v0, v27

    iget-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->lvl:Z

    if-nez v3, :cond_478

    .line 446
    move-object/from16 v0, v27

    iget-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->ads:Z

    if-nez v3, :cond_2a9

    .line 447
    move-object/from16 v0, v27

    iget-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->billing:Z

    if-nez v3, :cond_2a9

    .line 448
    const v3, 0x7f0701fc

    invoke-static {v3}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v35

    .line 457
    :cond_2a9
    :goto_2a9
    move-object/from16 v0, v27

    iget-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->custom:Z

    if-eqz v3, :cond_2b6

    const v3, 0x7f0701f6

    invoke-static {v3}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v35

    .line 458
    :cond_2b6
    move-object/from16 v0, v27

    iget-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->custom:Z

    if-eqz v3, :cond_2c5

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->txtTitle:Landroid/widget/TextView;

    const/16 v4, -0x100

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 460
    :cond_2c5
    const/16 v21, 0x0

    .local v21, "i":I
    :goto_2c7
    sget-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->bootlist:[Ljava/lang/String;

    array-length v3, v3

    move/from16 v0, v21

    if-ge v0, v3, :cond_4a9

    .line 461
    move-object/from16 v0, v27

    iget-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_ads:Z

    if-nez v3, :cond_2e6

    move-object/from16 v0, v27

    iget-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_custom:Z

    if-nez v3, :cond_2e6

    move-object/from16 v0, v27

    iget-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_lvl:Z

    if-nez v3, :cond_2e6

    move-object/from16 v0, v27

    iget-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_manual:Z

    if-eqz v3, :cond_2f0

    .line 462
    :cond_2e6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->txtTitle:Landroid/widget/TextView;

    const v4, -0xff01

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 460
    :cond_2f0
    add-int/lit8 v21, v21, 0x1

    goto :goto_2c7

    .line 241
    .end local v19    # "color":Ljava/lang/String;
    .end local v21    # "i":I
    .end local v24    # "lng":Ljava/lang/String;
    .end local v35    # "statusi":Ljava/lang/String;
    :cond_2f3
    invoke-virtual/range {v34 .. v34}, Landroid/widget/CheckBox;->getVisibility()I

    move-result v3

    if-nez v3, :cond_da

    .line 242
    const/16 v3, 0x8

    move-object/from16 v0, v34

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 243
    const/4 v3, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 244
    const/4 v3, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 245
    const/4 v3, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 246
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->on_top_txt:Landroid/widget/TextView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V
    :try_end_31a
    .catch Ljava/lang/Exception; {:try_start_283 .. :try_end_31a} :catch_31c

    goto/16 :goto_da

    .line 509
    .end local v16    # "chk":Landroid/widget/CheckBox;
    .end local v17    # "chk2":Landroid/widget/CheckBox;
    .end local v18    # "chk3":Landroid/widget/TextView;
    .end local v27    # "p":Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    .end local v29    # "row":Landroid/view/View;
    .end local v34    # "select":Landroid/widget/CheckBox;
    :catch_31c
    move-exception v20

    .local v20, "e":Ljava/lang/Exception;
    new-instance v29, Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->context:Landroid/content/Context;

    move-object/from16 v0, v29

    invoke-direct {v0, v3}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    goto/16 :goto_17

    .line 250
    .end local v20    # "e":Ljava/lang/Exception;
    .restart local v16    # "chk":Landroid/widget/CheckBox;
    .restart local v17    # "chk2":Landroid/widget/CheckBox;
    .restart local v18    # "chk3":Landroid/widget/TextView;
    .restart local v27    # "p":Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    .restart local v29    # "row":Landroid/view/View;
    .restart local v34    # "select":Landroid/widget/CheckBox;
    :cond_32a
    const/4 v3, 0x0

    :try_start_32b
    move-object/from16 v0, v34

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto/16 :goto_e6

    .line 259
    :cond_332
    const-string v3, "INT"

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 260
    const v3, -0xff0100

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_10f

    .line 263
    :cond_343
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v3

    move-object/from16 v0, v27

    iget-object v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    const/4 v8, 0x0

    invoke-virtual {v3, v4, v8}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    iget-object v3, v3, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v10, v3, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    .line 264
    .local v10, "apk":Ljava/lang/String;
    const-string v3, "/system"

    invoke-virtual {v10, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_36d

    .line 265
    const-string v3, "SYS"

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 266
    const v3, -0xff01

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_10f

    .line 268
    :cond_36d
    const-string v3, "INT"

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 269
    const v3, -0xff0100

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V
    :try_end_37c
    .catch Ljava/lang/Exception; {:try_start_32b .. :try_end_37c} :catch_31c

    goto/16 :goto_10f

    .line 300
    .end local v10    # "apk":Ljava/lang/String;
    .restart local v2    # "iconka":Landroid/graphics/Bitmap;
    .restart local v22    # "imgSize":I
    .restart local v30    # "scale":F
    .restart local v32    # "scaleImage":I
    :catch_37e
    move-exception v20

    .line 302
    .local v20, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_37f
    invoke-virtual/range {v20 .. v20}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V
    :try_end_382
    .catch Ljava/lang/OutOfMemoryError; {:try_start_37f .. :try_end_382} :catch_384
    .catch Ljava/lang/Exception; {:try_start_37f .. :try_end_382} :catch_3a8

    goto/16 :goto_179

    .line 347
    .end local v2    # "iconka":Landroid/graphics/Bitmap;
    .end local v20    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v22    # "imgSize":I
    .end local v30    # "scale":F
    .end local v32    # "scaleImage":I
    :catch_384
    move-exception v9

    .line 348
    .local v9, "E":Ljava/lang/OutOfMemoryError;
    :try_start_385
    invoke-virtual {v9}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    .line 349
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 350
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "no_icon"

    const/4 v8, 0x1

    invoke-interface {v3, v4, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_39d
    .catch Ljava/lang/Exception; {:try_start_385 .. :try_end_39d} :catch_31c

    goto/16 :goto_1ca

    .line 327
    .end local v9    # "E":Ljava/lang/OutOfMemoryError;
    .restart local v2    # "iconka":Landroid/graphics/Bitmap;
    .restart local v5    # "width":I
    .restart local v6    # "height":I
    .restart local v7    # "bMatrix":Landroid/graphics/Matrix;
    .restart local v22    # "imgSize":I
    .restart local v25    # "newHeight":I
    .restart local v26    # "newWidth":I
    .restart local v28    # "resizedBitmap":Landroid/graphics/Bitmap;
    .restart local v30    # "scale":F
    .restart local v31    # "scaleHeight":F
    .restart local v32    # "scaleImage":I
    .restart local v33    # "scaleWidth":F
    :catch_39f
    move-exception v20

    .line 328
    .local v20, "e":Ljava/lang/OutOfMemoryError;
    :try_start_3a0
    invoke-virtual/range {v20 .. v20}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    .line 331
    const/4 v3, 0x0

    sput-boolean v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->goodMemory:Z
    :try_end_3a6
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3a0 .. :try_end_3a6} :catch_384
    .catch Ljava/lang/Exception; {:try_start_3a0 .. :try_end_3a6} :catch_3a8

    goto/16 :goto_1b4

    .line 352
    .end local v2    # "iconka":Landroid/graphics/Bitmap;
    .end local v5    # "width":I
    .end local v6    # "height":I
    .end local v7    # "bMatrix":Landroid/graphics/Matrix;
    .end local v20    # "e":Ljava/lang/OutOfMemoryError;
    .end local v22    # "imgSize":I
    .end local v25    # "newHeight":I
    .end local v26    # "newWidth":I
    .end local v28    # "resizedBitmap":Landroid/graphics/Bitmap;
    .end local v30    # "scale":F
    .end local v31    # "scaleHeight":F
    .end local v32    # "scaleImage":I
    .end local v33    # "scaleWidth":F
    :catch_3a8
    move-exception v20

    .line 353
    .local v20, "e":Ljava/lang/Exception;
    :try_start_3a9
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Exception;->printStackTrace()V

    .line 356
    move-object/from16 v0, v27

    iget-object v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->name:Ljava/lang/String;

    if-eqz v3, :cond_3be

    move-object/from16 v0, v27

    iget-object v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->name:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3ca

    .line 357
    :cond_3be
    move-object/from16 v0, v27

    iget-object v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->remove(Ljava/lang/String;)V

    .line 358
    invoke-virtual/range {p0 .. p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->notifyDataSetChanged()V

    .line 360
    :cond_3ca
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "LuckyPatcher(PackageListItemAdapter): "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_3e4
    .catch Ljava/lang/Exception; {:try_start_3a9 .. :try_end_3e4} :catch_31c

    goto/16 :goto_1ca

    .line 340
    .end local v20    # "e":Ljava/lang/Exception;
    :cond_3e6
    :try_start_3e6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->imgIcon:Landroid/widget/ImageView;

    move-object/from16 v0, v27

    iget-object v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->icon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1ca

    .line 344
    :cond_3f3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->context:Landroid/content/Context;

    const-string v4, "Out of memory! Icon not loaded!"

    const/4 v8, 0x0

    invoke-static {v3, v4, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V
    :try_end_401
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3e6 .. :try_end_401} :catch_384
    .catch Ljava/lang/Exception; {:try_start_3e6 .. :try_end_401} :catch_3a8

    goto/16 :goto_1ca

    .line 382
    .restart local v19    # "color":Ljava/lang/String;
    :pswitch_403
    :try_start_403
    const-string v19, "#fff0e442"

    .line 383
    goto/16 :goto_1ff

    .line 385
    :pswitch_407
    const-string v19, "#ff00ff73"

    .line 386
    goto/16 :goto_1ff

    .line 388
    :pswitch_40b
    const-string v19, "#ff00ffff"

    .line 389
    goto/16 :goto_1ff

    .line 391
    :pswitch_40f
    const-string v19, "#ffff0055"

    goto/16 :goto_1ff

    .line 425
    .restart local v12    # "appConfig3":Landroid/content/res/Configuration;
    .restart local v14    # "appLoc3":Ljava/util/Locale;
    .restart local v24    # "lng":Ljava/lang/String;
    .restart local v35    # "statusi":Ljava/lang/String;
    :catch_413
    move-exception v20

    .line 426
    .restart local v20    # "e":Ljava/lang/Exception;
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_283

    .line 429
    .end local v12    # "appConfig3":Landroid/content/res/Configuration;
    .end local v14    # "appLoc3":Ljava/util/Locale;
    .end local v20    # "e":Ljava/lang/Exception;
    :cond_419
    const/4 v13, 0x0

    .line 430
    .local v13, "appLoc2":Ljava/util/Locale;
    const-string v3, "_"

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v36

    .line 431
    .local v36, "tails":[Ljava/lang/String;
    move-object/from16 v0, v36

    array-length v3, v0

    const/4 v4, 0x1

    if-ne v3, v4, :cond_430

    new-instance v13, Ljava/util/Locale;

    .end local v13    # "appLoc2":Ljava/util/Locale;
    const/4 v3, 0x0

    aget-object v3, v36, v3

    invoke-direct {v13, v3}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    .line 432
    .restart local v13    # "appLoc2":Ljava/util/Locale;
    :cond_430
    move-object/from16 v0, v36

    array-length v3, v0

    const/4 v4, 0x2

    if-ne v3, v4, :cond_443

    new-instance v13, Ljava/util/Locale;

    .end local v13    # "appLoc2":Ljava/util/Locale;
    const/4 v3, 0x0

    aget-object v3, v36, v3

    const/4 v4, 0x1

    aget-object v4, v36, v4

    const-string v8, ""

    invoke-direct {v13, v3, v4, v8}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 433
    .restart local v13    # "appLoc2":Ljava/util/Locale;
    :cond_443
    move-object/from16 v0, v36

    array-length v3, v0

    const/4 v4, 0x3

    if-ne v3, v4, :cond_457

    new-instance v13, Ljava/util/Locale;

    .end local v13    # "appLoc2":Ljava/util/Locale;
    const/4 v3, 0x0

    aget-object v3, v36, v3

    const/4 v4, 0x1

    aget-object v4, v36, v4

    const/4 v8, 0x2

    aget-object v8, v36, v8

    invoke-direct {v13, v3, v4, v8}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 434
    .restart local v13    # "appLoc2":Ljava/util/Locale;
    :cond_457
    invoke-static {v13}, Ljava/util/Locale;->setDefault(Ljava/util/Locale;)V

    .line 435
    new-instance v11, Landroid/content/res/Configuration;

    invoke-direct {v11}, Landroid/content/res/Configuration;-><init>()V

    .line 436
    .local v11, "appConfig2":Landroid/content/res/Configuration;
    iput-object v13, v11, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;
    :try_end_461
    .catch Ljava/lang/Exception; {:try_start_403 .. :try_end_461} :catch_31c

    .line 438
    :try_start_461
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v3

    .line 439
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    .line 438
    invoke-virtual {v3, v11, v4}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V
    :try_end_470
    .catch Ljava/lang/Exception; {:try_start_461 .. :try_end_470} :catch_472

    goto/16 :goto_283

    .line 440
    :catch_472
    move-exception v20

    .line 441
    .restart local v20    # "e":Ljava/lang/Exception;
    :try_start_473
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_283

    .line 451
    .end local v11    # "appConfig2":Landroid/content/res/Configuration;
    .end local v13    # "appLoc2":Ljava/util/Locale;
    .end local v20    # "e":Ljava/lang/Exception;
    .end local v36    # "tails":[Ljava/lang/String;
    :cond_478
    move-object/from16 v0, v27

    iget-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->ads:Z

    if-nez v3, :cond_487

    .line 452
    const v3, 0x7f0701f9

    invoke-static {v3}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v35

    goto/16 :goto_2a9

    .line 454
    :cond_487
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v35

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const v4, 0x7f0701f9

    invoke-static {v4}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    goto/16 :goto_2a9

    .line 463
    .restart local v21    # "i":I
    :cond_4a9
    move-object/from16 v0, v27

    iget-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->billing:Z

    if-eqz v3, :cond_4df

    .line 464
    const-string v3, ""

    move-object/from16 v0, v35

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5cc

    const v3, 0x7f0701f4

    invoke-static {v3}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v35

    .line 466
    :goto_4c0
    move-object/from16 v0, v27

    iget-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->lvl:Z

    if-nez v3, :cond_5ee

    const/4 v3, 0x1

    move v4, v3

    :goto_4c8
    move-object/from16 v0, v27

    iget-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->custom:Z

    if-nez v3, :cond_5f2

    const/4 v3, 0x1

    :goto_4cf
    and-int/2addr v3, v4

    if-eqz v3, :cond_4df

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->txtTitle:Landroid/widget/TextView;

    const-string v4, "#c5b5ff"

    invoke-static {v4}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 468
    :cond_4df
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->txtStatus:Landroid/widget/TextView;

    move-object/from16 v0, v35

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 474
    move-object/from16 v0, v27

    iget-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->system:Z

    if-eqz v3, :cond_4f8

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->txtTitle:Landroid/widget/TextView;

    const v4, -0x279e9

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 476
    :cond_4f8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->txtTitle:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v37

    .line 477
    .local v37, "tf":Landroid/graphics/Typeface;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->txtTitle:Landroid/widget/TextView;

    const/4 v4, 0x0

    move-object/from16 v0, v37

    invoke-virtual {v3, v0, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 480
    move-object/from16 v0, v27

    iget-boolean v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->enable:Z

    if-nez v3, :cond_524

    .line 481
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->txtTitle:Landroid/widget/TextView;

    const v4, -0x777778

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 482
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->txtStatus:Landroid/widget/TextView;

    const v4, -0x777778

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 484
    :cond_524
    move-object/from16 v0, v27

    iget v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->stored:I

    if-nez v3, :cond_5f5

    .line 486
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->on_top_txt:Landroid/widget/TextView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 487
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->on_top_txt:Landroid/widget/TextView;

    const v4, 0x7f070194

    invoke-static {v4}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 499
    :goto_540
    sget-boolean v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->adapterSelect:Z

    if-eqz v3, :cond_17

    .line 500
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->on_top_txt:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 501
    sget v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->adapterSelectType:I

    const v4, 0x7f07003f

    if-eq v3, v4, :cond_55b

    sget v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->adapterSelectType:I

    const v4, 0x7f070243

    if-ne v3, v4, :cond_17

    .line 503
    :cond_55b
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->on_top_txt:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 504
    new-instance v10, Ljava/io/File;

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v3

    move-object/from16 v0, v27

    iget-object v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    const/4 v8, 0x0

    invoke-virtual {v3, v4, v8}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    iget-object v3, v3, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    invoke-direct {v10, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 505
    .local v10, "apk":Ljava/io/File;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->txtStatus:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const v8, 0x7f070012

    invoke-static {v8}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v8, " "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v8, "%.3f"

    const/16 v38, 0x1

    move/from16 v0, v38

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v38, v0

    const/16 v39, 0x0

    invoke-virtual {v10}, Ljava/io/File;->length()J

    move-result-wide v40

    move-wide/from16 v0, v40

    long-to-float v0, v0

    move/from16 v40, v0

    const/high16 v41, 0x49800000    # 1048576.0f

    div-float v40, v40, v41

    invoke-static/range {v40 .. v40}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v40

    aput-object v40, v38, v39

    move-object/from16 v0, v38

    invoke-static {v8, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v8, " Mb"

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_17

    .line 465
    .end local v10    # "apk":Ljava/io/File;
    .end local v37    # "tf":Landroid/graphics/Typeface;
    :cond_5cc
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v35

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const v4, 0x7f0701f4

    invoke-static {v4}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    goto/16 :goto_4c0

    .line 466
    :cond_5ee
    const/4 v3, 0x0

    move v4, v3

    goto/16 :goto_4c8

    :cond_5f2
    const/4 v3, 0x0

    goto/16 :goto_4cf

    .line 490
    .restart local v37    # "tf":Landroid/graphics/Typeface;
    :cond_5f5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->on_top_txt:Landroid/widget/TextView;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V
    :try_end_5fd
    .catch Ljava/lang/Exception; {:try_start_473 .. :try_end_5fd} :catch_31c

    goto/16 :goto_540

    .line 379
    nop

    :pswitch_data_600
    .packed-switch 0x1
        :pswitch_403
        :pswitch_407
        :pswitch_1ff
        :pswitch_40b
        :pswitch_1ff
        :pswitch_40f
    .end packed-switch
.end method

.method public getItem(I)Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    .registers 3
    .param p1, "groupPosition"    # I

    .prologue
    .line 165
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->data:[Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getItem(Ljava/lang/String;)Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    .registers 7
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 596
    iget-object v2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->data:[Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_4
    if-ge v1, v3, :cond_14

    aget-object v0, v2, v1

    .line 597
    .local v0, "aData":Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    iget-object v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    invoke-virtual {v4, p1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_11

    .line 601
    .end local v0    # "aData":Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    :goto_10
    return-object v0

    .line 596
    .restart local v0    # "aData":Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    :cond_11
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 601
    .end local v0    # "aData":Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    :cond_14
    const/4 v0, 0x0

    goto :goto_10
.end method

.method public hasStableIds()Z
    .registers 2

    .prologue
    .line 532
    const/4 v0, 0x1

    return v0
.end method

.method public isChildSelectable(II)Z
    .registers 4
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I

    .prologue
    .line 547
    const/4 v0, 0x1

    return v0
.end method

.method public notifyDataSetChanged()V
    .registers 1

    .prologue
    .line 627
    invoke-super {p0}, Landroid/widget/BaseExpandableListAdapter;->notifyDataSetChanged()V

    .line 630
    return-void
.end method

.method public notifyDataSetChanged(Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;)V
    .registers 4
    .param p1, "pli"    # Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    .prologue
    .line 619
    invoke-super {p0}, Landroid/widget/BaseExpandableListAdapter;->notifyDataSetChanged()V

    .line 621
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->database:Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;

    if-nez v0, :cond_10

    new-instance v0, Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;

    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->context:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->database:Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;

    .line 622
    :cond_10
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->database:Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;

    invoke-virtual {v0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/DatabaseHelper;->savePackage(Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;)V

    .line 624
    return-void
.end method

.method public onGroupCollapsed(I)V
    .registers 2
    .param p1, "groupPosition"    # I

    .prologue
    .line 515
    invoke-super {p0, p1}, Landroid/widget/BaseExpandableListAdapter;->onGroupCollapsed(I)V

    .line 516
    return-void
.end method

.method public onGroupCollapsedAll()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 535
    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->data:[Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    array-length v1, v1

    if-lez v1, :cond_18

    .line 537
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_7
    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->data:[Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    array-length v1, v1

    if-ge v0, v1, :cond_16

    .line 538
    sput-object v2, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    .line 539
    sget-object v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->lv:Landroid/widget/ExpandableListView;

    invoke-virtual {v1, v0}, Landroid/widget/ExpandableListView;->collapseGroup(I)Z

    .line 537
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 542
    :cond_16
    sput-object v2, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    .line 544
    .end local v0    # "i":I
    :cond_18
    return-void
.end method

.method public onGroupExpanded(I)V
    .registers 4
    .param p1, "groupPosition"    # I

    .prologue
    .line 521
    sget v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->lastExpandedGroupPosition:I

    if-eq p1, v0, :cond_b

    .line 522
    sget-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->lv:Landroid/widget/ExpandableListView;

    sget v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->lastExpandedGroupPosition:I

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->collapseGroup(I)Z

    .line 525
    :cond_b
    invoke-super {p0, p1}, Landroid/widget/BaseExpandableListAdapter;->onGroupExpanded(I)V

    .line 526
    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->getGroup(I)Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    move-result-object v0

    sput-object v0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->pli:Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    .line 527
    sput p1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->lastExpandedGroupPosition:I

    .line 528
    return-void
.end method

.method public refreshPkgs(Z)V
    .registers 6
    .param p1, "saveTrigger"    # Z

    .prologue
    const/4 v3, 0x1

    .line 638
    sget-boolean v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->addapps:Z

    if-nez v1, :cond_23

    sget-boolean v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->refresh:Z

    if-eqz v1, :cond_23

    .line 639
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "LuckyPatcher: start refreshPackages."

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 640
    sput-boolean v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->addapps:Z

    .line 641
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter$Refresh_Packages;

    invoke-direct {v1, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter$Refresh_Packages;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 642
    .local v0, "refresh":Ljava/lang/Thread;
    invoke-virtual {v0, v3}, Ljava/lang/Thread;->setPriority(I)V

    .line 643
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 653
    .end local v0    # "refresh":Ljava/lang/Thread;
    :goto_22
    return-void

    .line 647
    :cond_23
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "LuckyPatcher: finalize refreshPackages."

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 650
    const/4 v1, 0x0

    sput-boolean v1, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->refresh:Z

    goto :goto_22
.end method

.method public remove(Ljava/lang/String;)V
    .registers 6
    .param p1, "pkgName"    # Ljava/lang/String;

    .prologue
    .line 556
    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->checkItem(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_32

    .line 558
    :try_start_6
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->data:[Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    new-array v2, v3, [Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    .line 559
    .local v2, "tmp":[Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    const/4 v1, 0x0

    .line 560
    .local v1, "j":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_f
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->data:[Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    array-length v3, v3

    if-ge v0, v3, :cond_33

    .line 561
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->data:[Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    aget-object v3, v3, v0

    iget-object v3, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2b

    .line 562
    iget-object v3, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->data:[Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    aget-object v3, v3, v0

    aput-object v3, v2, v1

    .line 563
    add-int/lit8 v1, v1, 0x1

    .line 560
    :goto_28
    add-int/lit8 v0, v0, 0x1

    goto :goto_f

    .line 565
    :cond_2b
    sget-object v3, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->lv:Landroid/widget/ExpandableListView;

    invoke-virtual {v3, v0}, Landroid/widget/ExpandableListView;->collapseGroup(I)Z

    goto :goto_28

    .line 571
    .end local v0    # "i":I
    .end local v1    # "j":I
    .end local v2    # "tmp":[Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    :catch_31
    move-exception v3

    .line 574
    :cond_32
    :goto_32
    return-void

    .line 568
    .restart local v0    # "i":I
    .restart local v1    # "j":I
    .restart local v2    # "tmp":[Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    :cond_33
    iput-object v2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->data:[Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    .line 570
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->notifyDataSetChanged()V
    :try_end_38
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_38} :catch_31

    goto :goto_32
.end method

.method public setTextSize(I)V
    .registers 2
    .param p1, "s"    # I

    .prologue
    .line 551
    iput p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->size:I

    .line 552
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->notifyDataSetChanged()V

    .line 553
    return-void
.end method

.method public size()I
    .registers 2

    .prologue
    .line 878
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->data:[Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    array-length v0, v0

    return v0
.end method

.method public sort()V
    .registers 4

    .prologue
    .line 588
    :try_start_0
    iget-object v1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->data:[Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    iget-object v2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->sorter:Ljava/util/Comparator;

    invoke-static {v1, v2}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_7} :catch_8

    .line 593
    :goto_7
    return-void

    .line 589
    :catch_8
    move-exception v0

    .line 590
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_7
.end method

.method public updateItem(Ljava/lang/String;)V
    .registers 10
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 884
    :try_start_1
    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->data:[Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    array-length v6, v5

    :goto_4
    if-ge v4, v6, :cond_8d

    aget-object v0, v5, v4

    .line 885
    .local v0, "aData":Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    iget-object v7, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    invoke-virtual {v7, p1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_af

    .line 886
    new-instance v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItemAdapter;->context:Landroid/content/Context;

    sget v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->days:I

    const/4 v6, 0x0

    invoke-direct {v2, v4, p1, v5, v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;-><init>(Landroid/content/Context;Ljava/lang/String;IZ)V

    .line 888
    .local v2, "item":Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    invoke-virtual {v2, v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->equalsPli(Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;)Z

    move-result v4

    if-nez v4, :cond_8d

    .line 889
    invoke-virtual {v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->saveItem()V

    .line 890
    iget-object v4, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    iput-object v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    .line 891
    iget-object v4, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->name:Ljava/lang/String;

    iput-object v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->name:Ljava/lang/String;

    .line 892
    iget v4, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->storepref:I

    iput v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->storepref:I

    .line 893
    iget v4, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->stored:I

    iput v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->stored:I

    .line 894
    iget-boolean v4, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->hidden:Z

    iput-boolean v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->hidden:Z

    .line 896
    iget-boolean v4, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_ads:Z

    iput-boolean v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_ads:Z

    .line 897
    iget-boolean v4, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_lvl:Z

    iput-boolean v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_lvl:Z

    .line 898
    iget-boolean v4, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_custom:Z

    iput-boolean v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_custom:Z

    .line 899
    iget-boolean v4, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_manual:Z

    iput-boolean v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->boot_manual:Z

    .line 900
    iget-boolean v4, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->custom:Z

    iput-boolean v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->custom:Z

    .line 901
    iget-object v4, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    invoke-static {v4}, Lcom/chelpus/Utils;->isInstalledOnSdCard(Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->on_sd:Z

    .line 902
    iget-boolean v4, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->lvl:Z

    iput-boolean v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->lvl:Z

    .line 903
    iget-boolean v4, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->ads:Z

    iput-boolean v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->ads:Z

    .line 904
    iget-boolean v4, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->billing:Z

    iput-boolean v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->billing:Z

    .line 905
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getConfig()Landroid/content/SharedPreferences;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v4, p1, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_6d

    .line 906
    const/4 v4, 0x1

    iput-boolean v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->modified:Z

    .line 907
    :cond_6d
    iget-boolean v4, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->system:Z

    iput-boolean v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->system:Z

    .line 908
    iget v4, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->updatetime:I

    iput v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->updatetime:I

    .line 911
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, p1, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v4

    iget-object v3, v4, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    .line 913
    .local v3, "pkgdir":Ljava/lang/String;
    invoke-static {v3}, Lcom/chelpus/Utils;->isOdex(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_8e

    .line 914
    const/4 v4, 0x1

    iput-boolean v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->odex:Z

    .line 918
    :goto_89
    iget-boolean v4, v2, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->enable:Z

    iput-boolean v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->enable:Z

    .line 929
    .end local v0    # "aData":Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    .end local v2    # "item":Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    .end local v3    # "pkgdir":Ljava/lang/String;
    :cond_8d
    :goto_8d
    return-void

    .line 916
    .restart local v0    # "aData":Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    .restart local v2    # "item":Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    .restart local v3    # "pkgdir":Ljava/lang/String;
    :cond_8e
    const/4 v4, 0x0

    iput-boolean v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->odex:Z
    :try_end_91
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_91} :catch_92

    goto :goto_89

    .line 924
    .end local v0    # "aData":Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    .end local v2    # "item":Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    .end local v3    # "pkgdir":Ljava/lang/String;
    :catch_92
    move-exception v1

    .line 925
    .local v1, "e":Ljava/lang/Exception;
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "LuckyPatcher (updateItem PkgListItemAdapter):"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 926
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_8d

    .line 884
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v0    # "aData":Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    :cond_af
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_4
.end method
