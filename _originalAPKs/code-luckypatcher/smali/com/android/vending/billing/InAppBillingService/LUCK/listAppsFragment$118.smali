.class Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$118;
.super Landroid/widget/ArrayAdapter;
.source "listAppsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getpermissions()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/android/vending/billing/InAppBillingService/LUCK/Perm;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;Landroid/content/Context;ILjava/util/List;)V
    .registers 5
    .param p1, "this$0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;
    .param p2, "x0"    # Landroid/content/Context;
    .param p3, "x1"    # I

    .prologue
    .line 14585
    .local p4, "x2":Ljava/util/List;, "Ljava/util/List<Lcom/android/vending/billing/InAppBillingService/LUCK/Perm;>;"
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$118;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 14
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 14589
    invoke-super {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 14590
    .local v5, "view":Landroid/view/View;
    const v6, 0x7f0d0039

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 14592
    .local v4, "textView":Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$118;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getSizeText()I

    move-result v7

    invoke-virtual {v4, v6, v7}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 14598
    const/high16 v6, 0x40a00000    # 5.0f

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v7

    iget v7, v7, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v6, v7

    const/high16 v7, 0x3f000000    # 0.5f

    add-float/2addr v6, v7

    float-to-int v0, v6

    .line 14599
    .local v0, "dp5":I
    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    .line 14604
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$118;->getItem(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/vending/billing/InAppBillingService/LUCK/Perm;

    iget-object v6, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/Perm;->Name:Ljava/lang/String;

    const-string v8, "disabled_"

    const-string v9, ""

    invoke-virtual {v6, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v6

    const-string v8, "chelpa_per_"

    const-string v9, ""

    invoke-virtual {v6, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v6

    const-string v8, "chelpus_"

    const-string v9, ""

    invoke-virtual {v6, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v6

    const-string v8, "android.permission."

    const-string v9, ""

    invoke-virtual {v6, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v6

    const-string v8, "com.android.vending."

    const-string v9, ""

    invoke-virtual {v6, v8, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 14606
    .local v3, "str2":Ljava/lang/String;
    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$118;->getItem(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/vending/billing/InAppBillingService/LUCK/Perm;

    iget-boolean v6, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/Perm;->Status:Z

    if-eqz v6, :cond_13e

    .line 14607
    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$118;->getItem(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/vending/billing/InAppBillingService/LUCK/Perm;

    iget-boolean v6, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/Perm;->Status:Z

    if-eqz v6, :cond_131

    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$118;->getItem(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/vending/billing/InAppBillingService/LUCK/Perm;

    iget-object v6, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/Perm;->Name:Ljava/lang/String;

    const-string v7, "chelpus_"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_131

    .line 14608
    const-string v6, "#ff00ffff"

    const-string v7, "bold"

    invoke-static {v3, v6, v7}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 14609
    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$118;->getItem(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/vending/billing/InAppBillingService/LUCK/Perm;

    iget-object v6, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/Perm;->Name:Ljava/lang/String;

    invoke-static {v6}, Lcom/chelpus/Utils;->isAds(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_b7

    .line 14610
    const-string v6, "#ffffff00"

    const-string v7, "bold"

    invoke-static {v3, v6, v7}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 14616
    :cond_b7
    :goto_b7
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 14619
    .local v2, "pm":Landroid/content/pm/PackageManager;
    :try_start_bb
    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$118;->getItem(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/vending/billing/InAppBillingService/LUCK/Perm;

    iget-object v6, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/Perm;->Name:Ljava/lang/String;

    const-string v7, "chelpa_per_"

    const-string v8, ""

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "chelpus_"

    const-string v8, ""

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v2, v6, v7}, Landroid/content/pm/PackageManager;->getPermissionInfo(Ljava/lang/String;I)Landroid/content/pm/PermissionInfo;

    move-result-object v6

    invoke-virtual {v6, v2}, Landroid/content/pm/PermissionInfo;->loadDescription(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    .line 14621
    if-nez v3, :cond_14b

    .line 14622
    const v6, 0x7f0701b9

    invoke-static {v6}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v3

    .line 14623
    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$118;->getItem(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/vending/billing/InAppBillingService/LUCK/Perm;

    iget-object v6, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/Perm;->Name:Ljava/lang/String;

    const-string v7, "chelpus_"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_100

    .line 14624
    const v6, 0x7f070009

    invoke-static {v6}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v3

    .line 14625
    :cond_100
    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$118;->getItem(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/vending/billing/InAppBillingService/LUCK/Perm;

    iget-object v6, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/Perm;->Name:Ljava/lang/String;

    const-string v7, "chelpus_"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_125

    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$118;->getItem(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/vending/billing/InAppBillingService/LUCK/Perm;

    iget-object v6, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/Perm;->Name:Ljava/lang/String;

    invoke-static {v6}, Lcom/chelpus/Utils;->isAds(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_125

    .line 14626
    const v6, 0x7f070008

    invoke-static {v6}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v3

    .line 14627
    :cond_125
    const-string v6, "#ff888888"

    const-string v7, "italic"

    invoke-static {v3, v6, v7}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V
    :try_end_130
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_bb .. :try_end_130} :catch_157
    .catch Ljava/lang/NullPointerException; {:try_start_bb .. :try_end_130} :catch_1a7

    .line 14651
    :goto_130
    return-object v5

    .line 14612
    .end local v2    # "pm":Landroid/content/pm/PackageManager;
    :cond_131
    const-string v6, "#ff00ff00"

    const-string v7, "bold"

    invoke-static {v3, v6, v7}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_b7

    .line 14614
    :cond_13e
    const-string v6, "#ffff0000"

    const-string v7, "bold"

    invoke-static {v3, v6, v7}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_b7

    .line 14629
    .restart local v2    # "pm":Landroid/content/pm/PackageManager;
    :cond_14b
    :try_start_14b
    const-string v6, "#ff888888"

    const-string v7, "italic"

    invoke-static {v3, v6, v7}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V
    :try_end_156
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_14b .. :try_end_156} :catch_157
    .catch Ljava/lang/NullPointerException; {:try_start_14b .. :try_end_156} :catch_1a7

    goto :goto_130

    .line 14631
    :catch_157
    move-exception v1

    .line 14633
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const v6, 0x7f0701b9

    invoke-static {v6}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v3

    .line 14634
    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$118;->getItem(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/vending/billing/InAppBillingService/LUCK/Perm;

    iget-object v6, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/Perm;->Name:Ljava/lang/String;

    const-string v7, "chelpus_"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_176

    .line 14635
    const v6, 0x7f070009

    invoke-static {v6}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v3

    .line 14636
    :cond_176
    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$118;->getItem(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/vending/billing/InAppBillingService/LUCK/Perm;

    iget-object v6, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/Perm;->Name:Ljava/lang/String;

    const-string v7, "chelpus_"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_19b

    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$118;->getItem(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/vending/billing/InAppBillingService/LUCK/Perm;

    iget-object v6, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/Perm;->Name:Ljava/lang/String;

    invoke-static {v6}, Lcom/chelpus/Utils;->isAds(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_19b

    .line 14637
    const v6, 0x7f070008

    invoke-static {v6}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v3

    .line 14639
    :cond_19b
    const-string v6, "#ff888888"

    const-string v7, "italic"

    invoke-static {v3, v6, v7}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    goto :goto_130

    .line 14640
    .end local v1    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_1a7
    move-exception v1

    .line 14641
    .local v1, "e":Ljava/lang/NullPointerException;
    const v6, 0x7f0701b9

    invoke-static {v6}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v3

    .line 14642
    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$118;->getItem(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/vending/billing/InAppBillingService/LUCK/Perm;

    iget-object v6, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/Perm;->Name:Ljava/lang/String;

    const-string v7, "chelpus_"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1c6

    .line 14643
    const v6, 0x7f070009

    invoke-static {v6}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v3

    .line 14644
    :cond_1c6
    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$118;->getItem(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/vending/billing/InAppBillingService/LUCK/Perm;

    iget-object v6, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/Perm;->Name:Ljava/lang/String;

    const-string v7, "chelpus_"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1eb

    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$118;->getItem(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/vending/billing/InAppBillingService/LUCK/Perm;

    iget-object v6, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/Perm;->Name:Ljava/lang/String;

    invoke-static {v6}, Lcom/chelpus/Utils;->isAds(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1eb

    .line 14645
    const v6, 0x7f070008

    invoke-static {v6}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v3

    .line 14647
    :cond_1eb
    const-string v6, "#ff888888"

    const-string v7, "italic"

    invoke-static {v3, v6, v7}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    goto/16 :goto_130
.end method
