.class Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity$1;
.super Landroid/widget/ArrayAdapter;
.source "AppDisablerWidgetConfigureActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/PkgItem;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity;Landroid/content/Context;I[Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/PkgItem;)V
    .registers 5
    .param p1, "this$0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity;
    .param p2, "x0"    # Landroid/content/Context;
    .param p3, "x1"    # I
    .param p4, "x2"    # [Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/PkgItem;

    .prologue
    .line 111
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity$1;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity;

    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 32
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 114
    move-object/from16 v21, p2

    .line 117
    .local v21, "row":Landroid/view/View;
    if-nez v21, :cond_1a

    .line 119
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v3

    const-string v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Landroid/view/LayoutInflater;

    .line 120
    .local v15, "inflater":Landroid/view/LayoutInflater;
    const v3, 0x7f04002e

    const/4 v4, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v15, v3, v0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v21

    .line 124
    .end local v15    # "inflater":Landroid/view/LayoutInflater;
    :cond_1a
    const v3, 0x7f0d0039

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v26

    check-cast v26, Landroid/widget/TextView;

    .line 125
    .local v26, "textView":Landroid/widget/TextView;
    const v3, 0x7f0d0038

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/ImageView;

    .line 126
    .local v13, "icon":Landroid/widget/ImageView;
    const/4 v3, 0x0

    invoke-virtual {v13, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 129
    invoke-virtual/range {p0 .. p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity$1;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getSizeText()I

    move-result v4

    move-object/from16 v0, v26

    invoke-virtual {v0, v3, v4}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 132
    const/4 v3, -0x1

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 133
    invoke-virtual/range {p0 .. p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/AppDisablerWidgetConfigureActivity$1;->getItem(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/PkgItem;

    .line 136
    .local v16, "item":Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/PkgItem;
    :try_start_4d
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v19

    .line 137
    .local v19, "pm":Landroid/content/pm/PackageManager;
    const/4 v9, 0x0

    .line 139
    .local v9, "appInfo":Landroid/content/pm/ApplicationInfo;
    const/16 v24, 0x19

    .line 140
    .local v24, "scaleImage":I
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v0, v3, Landroid/util/DisplayMetrics;->density:F

    move/from16 v22, v0
    :try_end_60
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4d .. :try_end_60} :catch_f8
    .catch Ljava/lang/Exception; {:try_start_4d .. :try_end_60} :catch_105

    .line 141
    .local v22, "scale":F
    const/high16 v3, 0x41c80000    # 25.0f

    mul-float v3, v3, v22

    const/high16 v4, 0x3f000000    # 0.5f

    add-float/2addr v3, v4

    float-to-int v14, v3

    .line 143
    .local v14, "imgSize":I
    const/4 v2, 0x0

    .line 145
    .local v2, "iconka":Landroid/graphics/Bitmap;
    :try_start_69
    move-object/from16 v0, v16

    iget-object v3, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/PkgItem;->package_name:Ljava/lang/String;

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Landroid/content/pm/PackageManager;->getApplicationIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    check-cast v3, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    .line 146
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    .line 147
    .local v5, "width":I
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    .line 149
    .local v6, "height":I
    move/from16 v18, v14

    .line 150
    .local v18, "newWidth":I
    move/from16 v17, v14

    .line 151
    .local v17, "newHeight":I
    move/from16 v0, v18

    int-to-float v3, v0

    int-to-float v4, v5

    div-float v25, v3, v4

    .line 152
    .local v25, "scaleWidth":F
    move/from16 v0, v17

    int-to-float v3, v0

    int-to-float v4, v6

    div-float v23, v3, v4

    .line 154
    .local v23, "scaleHeight":F
    new-instance v7, Landroid/graphics/Matrix;

    invoke-direct {v7}, Landroid/graphics/Matrix;-><init>()V

    .line 155
    .local v7, "bMatrix":Landroid/graphics/Matrix;
    move/from16 v0, v25

    move/from16 v1, v23

    invoke-virtual {v7, v0, v1}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 157
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v8, 0x1

    invoke-static/range {v2 .. v8}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v20

    .line 158
    .local v20, "resizedBitmap":Landroid/graphics/Bitmap;
    new-instance v10, Landroid/graphics/drawable/BitmapDrawable;

    move-object/from16 v0, v20

    invoke-direct {v10, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    .line 161
    .local v10, "bmd":Landroid/graphics/drawable/BitmapDrawable;
    invoke-virtual {v13, v10}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    :try_end_ae
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_69 .. :try_end_ae} :catch_f3
    .catch Ljava/lang/Exception; {:try_start_69 .. :try_end_ae} :catch_100
    .catch Ljava/lang/OutOfMemoryError; {:try_start_69 .. :try_end_ae} :catch_10a

    .line 163
    const/4 v2, 0x0

    .line 164
    const/4 v10, 0x0

    .line 166
    const/4 v7, 0x0

    .line 187
    .end local v2    # "iconka":Landroid/graphics/Bitmap;
    .end local v5    # "width":I
    .end local v6    # "height":I
    .end local v7    # "bMatrix":Landroid/graphics/Matrix;
    .end local v9    # "appInfo":Landroid/content/pm/ApplicationInfo;
    .end local v10    # "bmd":Landroid/graphics/drawable/BitmapDrawable;
    .end local v14    # "imgSize":I
    .end local v17    # "newHeight":I
    .end local v18    # "newWidth":I
    .end local v19    # "pm":Landroid/content/pm/PackageManager;
    .end local v20    # "resizedBitmap":Landroid/graphics/Bitmap;
    .end local v22    # "scale":F
    .end local v23    # "scaleHeight":F
    .end local v24    # "scaleImage":I
    .end local v25    # "scaleWidth":F
    :goto_b1
    const/high16 v3, 0x40a00000    # 5.0f

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v3, v4

    const/high16 v4, 0x3f000000    # 0.5f

    add-float/2addr v3, v4

    float-to-int v11, v3

    .line 188
    .local v11, "dp5":I
    move-object/from16 v0, v26

    invoke-virtual {v0, v11}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    .line 191
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v16

    iget-object v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/PkgItem;->label:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v16

    iget-object v4, v0, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/PkgItem;->package_name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 192
    const/4 v3, 0x0

    const/4 v4, 0x1

    move-object/from16 v0, v26

    invoke-virtual {v0, v3, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 193
    return-object v21

    .line 169
    .end local v11    # "dp5":I
    .restart local v2    # "iconka":Landroid/graphics/Bitmap;
    .restart local v9    # "appInfo":Landroid/content/pm/ApplicationInfo;
    .restart local v14    # "imgSize":I
    .restart local v19    # "pm":Landroid/content/pm/PackageManager;
    .restart local v22    # "scale":F
    .restart local v24    # "scaleImage":I
    :catch_f3
    move-exception v12

    .line 171
    .local v12, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_f4
    invoke-virtual {v12}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V
    :try_end_f7
    .catch Ljava/lang/OutOfMemoryError; {:try_start_f4 .. :try_end_f7} :catch_f8
    .catch Ljava/lang/Exception; {:try_start_f4 .. :try_end_f7} :catch_105

    goto :goto_b1

    .line 180
    .end local v2    # "iconka":Landroid/graphics/Bitmap;
    .end local v9    # "appInfo":Landroid/content/pm/ApplicationInfo;
    .end local v12    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v14    # "imgSize":I
    .end local v19    # "pm":Landroid/content/pm/PackageManager;
    .end local v22    # "scale":F
    .end local v24    # "scaleImage":I
    :catch_f8
    move-exception v27

    .line 181
    .local v27, "u":Ljava/lang/OutOfMemoryError;
    invoke-virtual/range {v27 .. v27}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    .line 182
    invoke-static {}, Ljava/lang/System;->gc()V

    goto :goto_b1

    .line 172
    .end local v27    # "u":Ljava/lang/OutOfMemoryError;
    .restart local v2    # "iconka":Landroid/graphics/Bitmap;
    .restart local v9    # "appInfo":Landroid/content/pm/ApplicationInfo;
    .restart local v14    # "imgSize":I
    .restart local v19    # "pm":Landroid/content/pm/PackageManager;
    .restart local v22    # "scale":F
    .restart local v24    # "scaleImage":I
    :catch_100
    move-exception v12

    .line 173
    .local v12, "e":Ljava/lang/Exception;
    :try_start_101
    invoke-virtual {v12}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_104
    .catch Ljava/lang/OutOfMemoryError; {:try_start_101 .. :try_end_104} :catch_f8
    .catch Ljava/lang/Exception; {:try_start_101 .. :try_end_104} :catch_105

    goto :goto_b1

    .line 183
    .end local v2    # "iconka":Landroid/graphics/Bitmap;
    .end local v9    # "appInfo":Landroid/content/pm/ApplicationInfo;
    .end local v12    # "e":Ljava/lang/Exception;
    .end local v14    # "imgSize":I
    .end local v19    # "pm":Landroid/content/pm/PackageManager;
    .end local v22    # "scale":F
    .end local v24    # "scaleImage":I
    :catch_105
    move-exception v12

    .line 184
    .restart local v12    # "e":Ljava/lang/Exception;
    invoke-virtual {v12}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_b1

    .line 174
    .end local v12    # "e":Ljava/lang/Exception;
    .restart local v2    # "iconka":Landroid/graphics/Bitmap;
    .restart local v9    # "appInfo":Landroid/content/pm/ApplicationInfo;
    .restart local v14    # "imgSize":I
    .restart local v19    # "pm":Landroid/content/pm/PackageManager;
    .restart local v22    # "scale":F
    .restart local v24    # "scaleImage":I
    :catch_10a
    move-exception v12

    .line 175
    .local v12, "e":Ljava/lang/OutOfMemoryError;
    goto :goto_b1
.end method
