.class Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$129$1;
.super Ljava/lang/Object;
.source "listAppsFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$129;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$129;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$129;)V
    .registers 2
    .param p1, "this$1"    # Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$129;

    .prologue
    .line 16337
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$129$1;->this$1:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$129;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 11

    .prologue
    .line 16340
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 16341
    .local v2, "commands":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v5, ""

    .line 16342
    .local v5, "statics":Ljava/lang/String;
    const-string v5, "pm uninstall "

    .line 16343
    new-instance v4, Ljava/util/ArrayList;

    sget-object v6, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->selectedApps:Ljava/util/ArrayList;

    invoke-direct {v4, v6}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 16344
    .local v4, "selApps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;>;"
    sget-object v6, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->selectedApps:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    .line 16345
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_19
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_56

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;

    .line 16346
    .local v3, "item":Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    const-string v0, ""

    .line 16347
    .local v0, "apk_file":Ljava/lang/String;
    sget-object v7, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Uninstall"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 16348
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;->pkgName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_19

    .line 16350
    .end local v0    # "apk_file":Ljava/lang/String;
    .end local v3    # "item":Lcom/android/vending/billing/InAppBillingService/LUCK/PkgListItem;
    :cond_56
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lez v6, :cond_73

    .line 16351
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v6

    new-array v1, v6, [Ljava/lang/String;

    .line 16352
    .local v1, "com":[Ljava/lang/String;
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 16353
    sget-boolean v6, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->su:Z

    if-eqz v6, :cond_7b

    new-instance v6, Lcom/chelpus/Utils;

    const-string v7, ""

    invoke-direct {v6, v7}, Lcom/chelpus/Utils;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Lcom/chelpus/Utils;->cmdRoot([Ljava/lang/String;)Ljava/lang/String;

    .line 16356
    .end local v1    # "com":[Ljava/lang/String;
    :cond_73
    :goto_73
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$129$1;->this$1:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$129;

    iget-object v6, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$129;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->resetBatchOperation()V

    .line 16358
    return-void

    .line 16354
    .restart local v1    # "com":[Ljava/lang/String;
    :cond_7b
    invoke-static {v1}, Lcom/chelpus/Utils;->cmd([Ljava/lang/String;)Ljava/lang/String;

    goto :goto_73
.end method
