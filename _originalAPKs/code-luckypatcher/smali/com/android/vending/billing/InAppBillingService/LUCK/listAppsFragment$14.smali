.class Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$14;
.super Ljava/lang/Object;
.source "listAppsFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->corepatch(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

.field final synthetic val$result:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;Ljava/lang/String;)V
    .registers 3
    .param p1, "this$0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    .prologue
    .line 2134
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$14;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iput-object p2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$14;->val$result:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 12

    .prologue
    const-wide/16 v9, 0x0

    const/4 v4, 0x0

    .line 2137
    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$14;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v6, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$14$1;

    invoke-direct {v6, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$14$1;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$14;)V

    invoke-virtual {v5, v6}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V

    .line 2146
    :try_start_d
    sget-boolean v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patchOnlyDalvikCore:Z

    if-eqz v5, :cond_11d

    .line 2147
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v5, "patch only dalvik cache mode"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2148
    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$14;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    const-string v5, ""

    iput-object v5, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->dalvikcache:Ljava/lang/String;

    .line 2149
    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$14;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    const-string v5, ""

    iput-object v5, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->dalvikcache2:Ljava/lang/String;

    .line 2150
    const-string v4, ""

    sput-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->result_core_patch:Ljava/lang/String;

    .line 2151
    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$14;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    const-string v5, "/system/framework/core.jar"

    invoke-static {v5}, Lcom/chelpus/Utils;->getFileDalvikCache(Ljava/lang/String;)Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->dalvikcache:Ljava/lang/String;

    .line 2152
    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$14;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iget-object v4, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->dalvikcache:Ljava/lang/String;

    if-nez v4, :cond_46

    .line 2153
    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$14;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$14$2;

    invoke-direct {v5, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$14$2;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$14;)V

    invoke-virtual {v4, v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V

    .line 2160
    :cond_46
    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$14;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    const-string v5, "/system/framework/services.jar"

    invoke-static {v5}, Lcom/chelpus/Utils;->getFileDalvikCache(Ljava/lang/String;)Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->dalvikcache2:Ljava/lang/String;

    .line 2161
    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$14;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iget-object v4, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->dalvikcache2:Ljava/lang/String;

    if-nez v4, :cond_64

    .line 2162
    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$14;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$14$3;

    invoke-direct {v5, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$14$3;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$14;)V

    invoke-virtual {v4, v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V

    .line 2168
    :cond_64
    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$14;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iget-object v4, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->dalvikcache:Ljava/lang/String;

    if-eqz v4, :cond_10d

    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$14;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iget-object v4, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->dalvikcache2:Ljava/lang/String;

    if-eqz v4, :cond_10d

    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$14;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iget-object v4, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->dalvikcache:Ljava/lang/String;

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_10d

    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$14;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iget-object v4, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->dalvikcache2:Ljava/lang/String;

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_10d

    .line 2169
    new-instance v4, Lcom/chelpus/Utils;

    const-string v5, ""

    invoke-direct {v4, v5}, Lcom/chelpus/Utils;-><init>(Ljava/lang/String;)V

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->dalvikruncommand:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ".corepatch "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$14;->val$result:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$14;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iget-object v8, v8, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->dalvikcache:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$14;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iget-object v8, v8, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->dalvikcache2:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " OnlyDalvik"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v4, v5}, Lcom/chelpus/Utils;->cmdRoot([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    sput-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->str:Ljava/lang/String;

    .line 2170
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    sget-object v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->str:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2171
    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$14;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$14$4;

    invoke-direct {v5, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$14$4;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$14;)V

    invoke-virtual {v4, v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V

    .line 2193
    :goto_fb
    sget-boolean v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->install_market_to_system:Z

    if-eqz v4, :cond_102

    invoke-static {}, Lcom/chelpus/Utils;->reboot()V
    :try_end_102
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_102} :catch_118

    .line 2334
    :cond_102
    :goto_102
    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$14;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$14$9;

    invoke-direct {v5, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$14$9;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$14;)V

    invoke-virtual {v4, v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V

    .line 2341
    return-void

    .line 2187
    :cond_10d
    :try_start_10d
    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$14;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$14$5;

    invoke-direct {v5, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$14$5;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$14;)V

    invoke-virtual {v4, v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V
    :try_end_117
    .catch Ljava/lang/Exception; {:try_start_10d .. :try_end_117} :catch_118

    goto :goto_fb

    .line 2331
    :catch_118
    move-exception v0

    .line 2332
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_102

    .line 2195
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_11d
    :try_start_11d
    invoke-static {}, Lcom/chelpus/Utils;->turn_off_patch_on_boot_all()V

    .line 2196
    const-string v5, "/system/framework/core.jar"

    invoke-static {v5}, Lcom/chelpus/Utils;->isOdex(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_158

    new-instance v5, Ljava/io/File;

    const-string v6, "/system/framework/core.jar"

    const/4 v7, 0x0

    invoke-static {v6, v7}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v5

    cmp-long v5, v5, v9

    if-eqz v5, :cond_158

    const-string v5, "/system/framework/services.jar"

    invoke-static {v5}, Lcom/chelpus/Utils;->isOdex(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_158

    new-instance v5, Ljava/io/File;

    const-string v6, "system/framework/services.jar"

    const/4 v7, 0x0

    invoke-static {v6, v7}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v5

    cmp-long v5, v5, v9

    if-nez v5, :cond_430

    .line 2197
    :cond_158
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v6, "start odex framework"

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2198
    const-string v5, "/system"

    const-string v6, "rw"

    invoke-static {v5, v6}, Lcom/chelpus/Utils;->remount(Ljava/lang/String;Ljava/lang/String;)Z

    .line 2199
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "chattr -ai "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/system/framework/core.jar"

    const/4 v7, 0x0

    invoke-static {v6, v7}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 2200
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "chattr -ai "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/system/framework/services.jar"

    const/4 v7, 0x0

    invoke-static {v6, v7}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 2201
    const-string v5, "chattr -ai /system/framework/core.jar"

    invoke-static {v5}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 2202
    const-string v5, "chattr -ai /system/framework/services.jar"

    invoke-static {v5}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 2203
    const-string v5, "chattr -ai /system/framework/core-libart.jar"

    invoke-static {v5}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 2204
    const/4 v5, 0x6

    new-array v2, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "/data/dalvik-cache/"

    aput-object v6, v2, v5

    const/4 v5, 0x1

    const-string v6, "/data/dalvik-cache/arm/"

    aput-object v6, v2, v5

    const/4 v5, 0x2

    const-string v6, "/sd-ext/data/dalvik-cache/"

    aput-object v6, v2, v5

    const/4 v5, 0x3

    const-string v6, "/cache/dalvik-cache/"

    aput-object v6, v2, v5

    const/4 v5, 0x4

    const-string v6, "/sd-ext/data/cache/dalvik-cache/"

    aput-object v6, v2, v5

    const/4 v5, 0x5

    const-string v6, "/data/cache/dalvik-cache/"

    aput-object v6, v2, v5

    .line 2205
    .local v2, "strings2":[Ljava/lang/String;
    array-length v5, v2

    :goto_1d1
    if-ge v4, v5, :cond_33c

    aget-object v3, v2, v4

    .line 2206
    .local v3, "tail":Ljava/lang/String;
    sget-object v6, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "LuckyPatcher: search dalvik-cache! "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "system@framework@core.jar@classes.dex"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2207
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "system@framework@core.jar@classes.dex"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/chelpus/Utils;->exists(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_231

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "system@framework@services.jar@classes.dex"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/chelpus/Utils;->exists(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_231

    invoke-static {}, Lcom/chelpus/Utils;->getCurrentRuntimeValue()Ljava/lang/String;

    move-result-object v6

    const-string v7, "ART"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3f8

    .line 2208
    :cond_231
    invoke-static {}, Lcom/chelpus/Utils;->getCurrentRuntimeValue()Ljava/lang/String;

    move-result-object v4

    const-string v5, "ART"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3d8

    .line 2209
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v5, "LuckyPatcher: art patch"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2213
    :goto_244
    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$14;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "system@framework@core.jar@classes.dex"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->dalvikcache:Ljava/lang/String;

    .line 2214
    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$14;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "system@framework@services.jar@classes.dex"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->dalvikcache2:Ljava/lang/String;

    .line 2215
    const-string v4, ""

    sput-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->str:Ljava/lang/String;

    .line 2216
    const-string v4, "/system/framework/core.jar"

    invoke-static {v4}, Lcom/chelpus/Utils;->isOdex(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_292

    new-instance v4, Ljava/io/File;

    const-string v5, "/system/framework/core.jar"

    const/4 v6, 0x0

    invoke-static {v5, v6}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v4

    cmp-long v4, v4, v9

    if-eqz v4, :cond_29d

    .line 2217
    :cond_292
    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$14;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    const-string v5, "/system/framework/core.jar"

    const/4 v6, 0x0

    invoke-static {v5, v6}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->dalvikcache:Ljava/lang/String;

    .line 2218
    :cond_29d
    const-string v4, "/system/framework/services.jar"

    invoke-static {v4}, Lcom/chelpus/Utils;->isOdex(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2b9

    new-instance v4, Ljava/io/File;

    const-string v5, "/system/framework/services.jar"

    const/4 v6, 0x0

    invoke-static {v5, v6}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v4

    cmp-long v4, v4, v9

    if-eqz v4, :cond_2c4

    .line 2219
    :cond_2b9
    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$14;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    const-string v5, "/system/framework/services.jar"

    const/4 v6, 0x0

    invoke-static {v5, v6}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->dalvikcache2:Ljava/lang/String;

    .line 2220
    :cond_2c4
    const-string v4, "/system"

    const-string v5, "rw"

    invoke-static {v4, v5}, Lcom/chelpus/Utils;->remount(Ljava/lang/String;Ljava/lang/String;)Z

    .line 2222
    new-instance v4, Lcom/chelpus/Utils;

    const-string v5, ""

    invoke-direct {v4, v5}, Lcom/chelpus/Utils;-><init>(Ljava/lang/String;)V

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->dalvikruncommand:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ".corepatch "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$14;->val$result:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$14;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iget-object v8, v8, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->dalvikcache:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$14;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    iget-object v8, v8, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->dalvikcache2:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Lcom/chelpus/Utils;->getCurrentRuntimeValue()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v4, v5}, Lcom/chelpus/Utils;->cmdRoot([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    sput-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->str:Ljava/lang/String;

    .line 2223
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    sget-object v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->str:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2230
    .end local v3    # "tail":Ljava/lang/String;
    :cond_33c
    const-string v4, "/system/framework/core.patched"

    invoke-static {v4}, Lcom/chelpus/Utils;->exists(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_34c

    const-string v4, "/data/data/core.odex"

    invoke-static {v4}, Lcom/chelpus/Utils;->exists(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_401

    .line 2231
    :cond_34c
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v5, "Result code for found core.patched"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2232
    const-string v1, ""

    .line 2233
    .local v1, "patched_core":Ljava/lang/String;
    const-string v4, "/system/framework/core.patched"

    invoke-static {v4}, Lcom/chelpus/Utils;->exists(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_35f

    .line 2234
    const-string v1, "/system/framework/core.patched"

    .line 2235
    :cond_35f
    const-string v4, "/data/data/core.odex"

    invoke-static {v4}, Lcom/chelpus/Utils;->exists(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_369

    .line 2236
    const-string v1, "/data/data/core.odex"

    .line 2237
    :cond_369
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "chmod 0644 "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 2238
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "chown 0.0 "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 2239
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "chown 0:0 "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 2241
    const-string v4, "/system/framework/services.jar"

    invoke-static {v4}, Lcom/chelpus/Utils;->exists(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3c2

    .line 2242
    const-string v4, "chmod 0644 /system/framework/services.jar"

    invoke-static {v4}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 2243
    const-string v4, "chown 0.0 /system/framework/services.jar"

    invoke-static {v4}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 2244
    const-string v4, "chown 0:0 /system/framework/services.jar"

    invoke-static {v4}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 2246
    :cond_3c2
    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$14;->val$result:Ljava/lang/String;

    const-string v5, "restore"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3fc

    .line 2247
    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$14;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$14$6;

    invoke-direct {v5, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$14$6;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$14;)V

    invoke-virtual {v4, v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V

    goto/16 :goto_102

    .line 2211
    .end local v1    # "patched_core":Ljava/lang/String;
    .restart local v3    # "tail":Ljava/lang/String;
    :cond_3d8
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "LuckyPatcher: found dalvik-cache! "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "system@framework@core.jar@classes.dex"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_244

    .line 2205
    :cond_3f8
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_1d1

    .line 2294
    .end local v3    # "tail":Ljava/lang/String;
    .restart local v1    # "patched_core":Ljava/lang/String;
    :cond_3fc
    invoke-static {}, Lcom/chelpus/Utils;->reboot()V

    goto/16 :goto_102

    .line 2297
    .end local v1    # "patched_core":Ljava/lang/String;
    :cond_401
    sget-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->str:Ljava/lang/String;

    const-string v5, "LuckyPatcher: odex not equal lenght packed! Not enougth space in /system/!"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_415

    .line 2298
    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$14;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$14$7;

    invoke-direct {v5, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$14$7;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$14;)V

    invoke-virtual {v4, v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V

    .line 2304
    :cond_415
    sget-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->str:Ljava/lang/String;

    const-string v5, "LuckyPatcher: odex not equal lenght patched! Not enougth space in /system/!"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_42b

    .line 2305
    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$14;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    new-instance v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$14$8;

    invoke-direct {v5, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$14$8;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$14;)V

    invoke-virtual {v4, v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->runToMain(Ljava/lang/Runnable;)V

    goto/16 :goto_102

    .line 2311
    :cond_42b
    invoke-static {}, Lcom/chelpus/Utils;->reboot()V

    goto/16 :goto_102

    .line 2316
    .end local v2    # "strings2":[Ljava/lang/String;
    :cond_430
    const-string v4, ""

    sput-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->str:Ljava/lang/String;

    .line 2318
    const-string v4, "/system"

    const-string v5, "rw"

    invoke-static {v4, v5}, Lcom/chelpus/Utils;->remount(Ljava/lang/String;Ljava/lang/String;)Z

    .line 2319
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "chattr -ai "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/system/framework/core.jar"

    const/4 v6, 0x0

    invoke-static {v5, v6}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 2320
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "chattr -ai "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/system/framework/services.jar"

    const/4 v6, 0x0

    invoke-static {v5, v6}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 2321
    const-string v4, "chattr -ai /system/framework/core.jar"

    invoke-static {v4}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 2322
    const-string v4, "chattr -ai /system/framework/services.jar"

    invoke-static {v4}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 2323
    const-string v4, "chattr -ai /system/framework/core-libart.jar"

    invoke-static {v4}, Lcom/chelpus/Utils;->run_all(Ljava/lang/String;)V

    .line 2325
    new-instance v4, Lcom/chelpus/Utils;

    const-string v5, ""

    invoke-direct {v4, v5}, Lcom/chelpus/Utils;-><init>(Ljava/lang/String;)V

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->dalvikruncommand:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ".corepatch "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$14;->val$result:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/system/framework/core.jar"

    const/4 v9, 0x0

    invoke-static {v8, v9}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/system/framework/services.jar"

    const/4 v9, 0x0

    invoke-static {v8, v9}, Lcom/chelpus/Utils;->getPlaceForOdex(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Lcom/chelpus/Utils;->getCurrentRuntimeValue()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v4, v5}, Lcom/chelpus/Utils;->cmdRoot([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    sput-object v4, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->str:Ljava/lang/String;

    .line 2326
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    sget-object v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->str:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2327
    invoke-static {}, Lcom/chelpus/Utils;->reboot()V
    :try_end_4fe
    .catch Ljava/lang/Exception; {:try_start_11d .. :try_end_4fe} :catch_118

    goto/16 :goto_102
.end method
