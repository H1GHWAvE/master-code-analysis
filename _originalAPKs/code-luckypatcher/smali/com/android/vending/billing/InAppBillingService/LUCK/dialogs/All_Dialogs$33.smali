.class Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$33;
.super Ljava/lang/Object;
.source "All_Dialogs.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;->onCreateDialog()Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;)V
    .registers 2
    .param p1, "this$0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;

    .prologue
    .line 1143
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs$33;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/dialogs/All_Dialogs;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .registers 13
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 1148
    const-string v7, "patch"

    .line 1149
    .local v7, "result":Ljava/lang/String;
    sget-object v8, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patch_adapt:Landroid/widget/ArrayAdapter;

    invoke-virtual {v8}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v3

    .line 1150
    .local v3, "count":I
    const/4 v0, 0x0

    .local v0, "a":Z
    const/4 v1, 0x0

    .local v1, "b":Z
    const/4 v2, 0x0

    .local v2, "c":Z
    const/4 v4, 0x0

    .line 1151
    .local v4, "d":Z
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_d
    if-ge v5, v3, :cond_b6

    .line 1152
    sget-object v8, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patch_adapt:Landroid/widget/ArrayAdapter;

    invoke-virtual {v8, v5}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;

    .line 1153
    .local v6, "patt":Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;
    if-nez v5, :cond_31

    iget-boolean v8, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;->Status:Z

    if-eqz v8, :cond_31

    .line 1154
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "_patch1"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 1155
    const/4 v0, 0x1

    .line 1157
    :cond_31
    const/4 v8, 0x1

    if-ne v5, v8, :cond_4c

    iget-boolean v8, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;->Status:Z

    if-eqz v8, :cond_4c

    .line 1158
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "_patch2"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 1159
    const/4 v1, 0x1

    .line 1161
    :cond_4c
    const/4 v8, 0x2

    if-ne v5, v8, :cond_67

    iget-boolean v8, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;->Status:Z

    if-eqz v8, :cond_67

    .line 1162
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "_patch3"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 1163
    const/4 v2, 0x1

    .line 1165
    :cond_67
    const/4 v8, 0x3

    if-ne v5, v8, :cond_82

    iget-boolean v8, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;->Status:Z

    if-eqz v8, :cond_82

    .line 1166
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "_patch4"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 1167
    const/4 v2, 0x1

    .line 1169
    :cond_82
    const/4 v8, 0x4

    if-ne v5, v8, :cond_8c

    iget-boolean v8, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;->Status:Z

    if-eqz v8, :cond_8c

    .line 1170
    const-string v7, "restoreCore"

    .line 1171
    const/4 v4, 0x1

    .line 1173
    :cond_8c
    const/4 v8, 0x5

    if-ne v5, v8, :cond_af

    iget-boolean v8, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;->Status:Z

    if-eqz v8, :cond_af

    .line 1174
    const-string v8, "restoreCore"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_b3

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "_restoreServices"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 1176
    :goto_ae
    const/4 v4, 0x1

    .line 1151
    :cond_af
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_d

    .line 1175
    :cond_b3
    const-string v7, "restoreServices"

    goto :goto_ae

    .line 1180
    .end local v6    # "patt":Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;
    :cond_b6
    if-nez v0, :cond_be

    if-nez v1, :cond_be

    if-nez v2, :cond_be

    if-eqz v4, :cond_c3

    .line 1181
    :cond_be
    sget-object v8, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    invoke-virtual {v8, v7}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->corepatch(Ljava/lang/String;)V

    .line 1186
    :cond_c3
    return-void
.end method
