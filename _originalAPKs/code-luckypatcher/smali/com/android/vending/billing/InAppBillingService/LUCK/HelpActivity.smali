.class public Lcom/android/vending/billing/InAppBillingService/LUCK/HelpActivity;
.super Landroid/app/Activity;
.source "HelpActivity.java"


# instance fields
.field public context:Landroid/content/Context;

.field mLocalActivityManager:Landroid/app/LocalActivityManager;

.field tabHost:Landroid/widget/TabHost;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private static createTabView(Landroid/content/Context;Ljava/lang/String;)Landroid/view/View;
    .registers 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 75
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f04003c

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 76
    .local v1, "view":Landroid/view/View;
    const v2, 0x7f0d00eb

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 77
    .local v0, "tv":Landroid/widget/TextView;
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 78
    return-object v1
.end method

.method private initTabs(Landroid/os/Bundle;)V
    .registers 12
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v9, 0x1020012

    const/4 v8, 0x0

    .line 44
    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/HelpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 45
    .local v1, "res":Landroid/content/res/Resources;
    invoke-virtual {p0, v9}, Lcom/android/vending/billing/InAppBillingService/LUCK/HelpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TabHost;

    iput-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/HelpActivity;->tabHost:Landroid/widget/TabHost;

    .line 46
    new-instance v6, Landroid/app/LocalActivityManager;

    invoke-direct {v6, p0, v8}, Landroid/app/LocalActivityManager;-><init>(Landroid/app/Activity;Z)V

    iput-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/HelpActivity;->mLocalActivityManager:Landroid/app/LocalActivityManager;

    .line 47
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/HelpActivity;->mLocalActivityManager:Landroid/app/LocalActivityManager;

    invoke-virtual {v6, p1}, Landroid/app/LocalActivityManager;->dispatchCreate(Landroid/os/Bundle;)V

    .line 48
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/HelpActivity;->tabHost:Landroid/widget/TabHost;

    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/HelpActivity;->mLocalActivityManager:Landroid/app/LocalActivityManager;

    invoke-virtual {v6, v7}, Landroid/widget/TabHost;->setup(Landroid/app/LocalActivityManager;)V

    .line 54
    invoke-virtual {p0, v9}, Lcom/android/vending/billing/InAppBillingService/LUCK/HelpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TabHost;

    .line 55
    .local v3, "tabHost":Landroid/widget/TabHost;
    invoke-virtual {v3}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v6

    const v7, 0x7f020055

    invoke-virtual {v6, v7}, Landroid/widget/TabWidget;->setDividerDrawable(I)V

    .line 56
    invoke-virtual {v3}, Landroid/widget/TabHost;->getContext()Landroid/content/Context;

    move-result-object v6

    const v7, 0x7f07012c

    invoke-virtual {v1, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/vending/billing/InAppBillingService/LUCK/HelpActivity;->createTabView(Landroid/content/Context;Ljava/lang/String;)Landroid/view/View;

    move-result-object v4

    .line 57
    .local v4, "tabview1":Landroid/view/View;
    invoke-virtual {v3}, Landroid/widget/TabHost;->getContext()Landroid/content/Context;

    move-result-object v6

    const v7, 0x7f07012d

    invoke-virtual {v1, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/vending/billing/InAppBillingService/LUCK/HelpActivity;->createTabView(Landroid/content/Context;Ljava/lang/String;)Landroid/view/View;

    move-result-object v5

    .line 58
    .local v5, "tabview2":Landroid/view/View;
    new-instance v6, Landroid/content/Intent;

    invoke-direct {v6}, Landroid/content/Intent;-><init>()V

    const-class v7, Lcom/android/vending/billing/InAppBillingService/LUCK/HelpCommon;

    invoke-virtual {v6, p0, v7}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 60
    .local v0, "intent":Landroid/content/Intent;
    const-string v6, "Common"

    invoke-virtual {v3, v6}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v6

    invoke-virtual {v6, v4}, Landroid/widget/TabHost$TabSpec;->setIndicator(Landroid/view/View;)Landroid/widget/TabHost$TabSpec;

    move-result-object v6

    invoke-virtual {v6, v0}, Landroid/widget/TabHost$TabSpec;->setContent(Landroid/content/Intent;)Landroid/widget/TabHost$TabSpec;

    move-result-object v2

    .line 61
    .local v2, "spec":Landroid/widget/TabHost$TabSpec;
    invoke-virtual {v3, v2}, Landroid/widget/TabHost;->addTab(Landroid/widget/TabHost$TabSpec;)V

    .line 63
    new-instance v6, Landroid/content/Intent;

    invoke-direct {v6}, Landroid/content/Intent;-><init>()V

    const-class v7, Lcom/android/vending/billing/InAppBillingService/LUCK/HelpCustom;

    invoke-virtual {v6, p0, v7}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 65
    const-string v6, "Create"

    invoke-virtual {v3, v6}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v6

    invoke-virtual {v6, v5}, Landroid/widget/TabHost$TabSpec;->setIndicator(Landroid/view/View;)Landroid/widget/TabHost$TabSpec;

    move-result-object v6

    invoke-virtual {v6, v0}, Landroid/widget/TabHost$TabSpec;->setContent(Landroid/content/Intent;)Landroid/widget/TabHost$TabSpec;

    move-result-object v2

    .line 66
    invoke-virtual {v3, v2}, Landroid/widget/TabHost;->addTab(Landroid/widget/TabHost$TabSpec;)V

    .line 68
    invoke-virtual {v3, v8}, Landroid/widget/TabHost;->setCurrentTab(I)V

    .line 69
    invoke-virtual {v3}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/widget/TabWidget;->setCurrentTab(I)V

    .line 72
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 22
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 26
    const v0, 0x7f04002d

    invoke-virtual {p0, v0}, Lcom/android/vending/billing/InAppBillingService/LUCK/HelpActivity;->setContentView(I)V

    .line 27
    invoke-direct {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/HelpActivity;->initTabs(Landroid/os/Bundle;)V

    .line 28
    return-void
.end method

.method protected onPause()V
    .registers 3

    .prologue
    .line 38
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/HelpActivity;->mLocalActivityManager:Landroid/app/LocalActivityManager;

    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/HelpActivity;->isFinishing()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/LocalActivityManager;->dispatchPause(Z)V

    .line 39
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 40
    return-void
.end method

.method protected onResume()V
    .registers 2

    .prologue
    .line 32
    iget-object v0, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/HelpActivity;->mLocalActivityManager:Landroid/app/LocalActivityManager;

    invoke-virtual {v0}, Landroid/app/LocalActivityManager;->dispatchResume()V

    .line 33
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 34
    return-void
.end method
