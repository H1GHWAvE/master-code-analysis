.class Lcom/android/vending/billing/InAppBillingService/LUCK/BinderLuckyPatcher$1;
.super Ljava/lang/Object;
.source "BinderLuckyPatcher.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/BinderLuckyPatcher;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/BinderLuckyPatcher;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/BinderLuckyPatcher;Landroid/content/Context;)V
    .registers 3
    .param p1, "this$0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/BinderLuckyPatcher;

    .prologue
    .line 28
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderLuckyPatcher$1;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/BinderLuckyPatcher;

    iput-object p2, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderLuckyPatcher$1;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 15

    .prologue
    const/4 v13, 0x0

    .line 31
    new-instance v0, Ljava/io/File;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v10, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderLuckyPatcher$1;->val$context:Landroid/content/Context;

    const-string v11, "binder"

    invoke-virtual {v10, v11, v13}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/bind.txt"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v0, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 33
    .local v0, "bindfile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_a9

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v9

    const-wide/16 v11, 0x0

    cmp-long v9, v9, v11

    if-lez v9, :cond_a9

    .line 34
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "LuckyPatcher binder start!"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 38
    :try_start_38
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v9

    if-nez v9, :cond_41

    .line 40
    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z

    .line 43
    :cond_41
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 44
    .local v4, "fis":Ljava/io/FileInputStream;
    new-instance v6, Ljava/io/InputStreamReader;

    const-string v9, "UTF-8"

    invoke-direct {v6, v4, v9}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 45
    .local v6, "isr":Ljava/io/InputStreamReader;
    new-instance v1, Ljava/io/BufferedReader;

    invoke-direct {v1, v6}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 46
    .local v1, "br":Ljava/io/BufferedReader;
    const-string v2, ""

    .line 48
    .local v2, "data":Ljava/lang/String;
    const-string v8, ""

    .line 49
    .local v8, "temp":Ljava/lang/String;
    :cond_56
    :goto_56
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_aa

    .line 50
    move-object v8, v2

    .line 51
    const-string v9, ";"

    invoke-virtual {v8, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 52
    .local v7, "tails":[Ljava/lang/String;
    array-length v9, v7

    const/4 v10, 0x2

    if-ne v9, v10, :cond_56

    .line 53
    const-string v9, "mount"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "-o bind \'"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const/4 v11, 0x0

    aget-object v11, v7, v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "\' \'"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const/4 v11, 0x1

    aget-object v11, v7, v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "\'"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    aget-object v11, v7, v11

    const/4 v12, 0x1

    aget-object v12, v7, v12

    invoke-static {v9, v10, v11, v12}, Lcom/chelpus/Utils;->verify_bind_and_run(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_9b
    .catch Ljava/io/FileNotFoundException; {:try_start_38 .. :try_end_9b} :catch_9c
    .catch Ljava/io/IOException; {:try_start_38 .. :try_end_9b} :catch_c1

    goto :goto_56

    .line 61
    .end local v1    # "br":Ljava/io/BufferedReader;
    .end local v2    # "data":Ljava/lang/String;
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .end local v6    # "isr":Ljava/io/InputStreamReader;
    .end local v7    # "tails":[Ljava/lang/String;
    .end local v8    # "temp":Ljava/lang/String;
    :catch_9c
    move-exception v3

    .line 62
    .local v3, "e":Ljava/io/FileNotFoundException;
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "Not found bind.txt"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 66
    .end local v3    # "e":Ljava/io/FileNotFoundException;
    :goto_a4
    sput-boolean v13, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patchOnBoot:Z

    .line 67
    invoke-static {}, Lcom/chelpus/Utils;->exit()V

    .line 69
    :cond_a9
    return-void

    .line 57
    .restart local v1    # "br":Ljava/io/BufferedReader;
    .restart local v2    # "data":Ljava/lang/String;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v6    # "isr":Ljava/io/InputStreamReader;
    .restart local v8    # "temp":Ljava/lang/String;
    :cond_aa
    :try_start_aa
    new-instance v5, Landroid/content/Intent;

    iget-object v9, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderLuckyPatcher$1;->val$context:Landroid/content/Context;

    const-class v10, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidget;

    invoke-direct {v5, v9, v10}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 58
    .local v5, "i":Landroid/content/Intent;
    sget-object v9, Lcom/android/vending/billing/InAppBillingService/LUCK/widgets/BinderWidget;->ACTION_WIDGET_RECEIVER_Updater:Ljava/lang/String;

    invoke-virtual {v5, v9}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 59
    iget-object v9, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/BinderLuckyPatcher$1;->val$context:Landroid/content/Context;

    invoke-virtual {v9, v5}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 60
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_c0
    .catch Ljava/io/FileNotFoundException; {:try_start_aa .. :try_end_c0} :catch_9c
    .catch Ljava/io/IOException; {:try_start_aa .. :try_end_c0} :catch_c1

    goto :goto_a4

    .line 63
    .end local v1    # "br":Ljava/io/BufferedReader;
    .end local v2    # "data":Ljava/lang/String;
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .end local v5    # "i":Landroid/content/Intent;
    .end local v6    # "isr":Ljava/io/InputStreamReader;
    .end local v8    # "temp":Ljava/lang/String;
    :catch_c1
    move-exception v3

    .line 64
    .local v3, "e":Ljava/io/IOException;
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, ""

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_a4
.end method
