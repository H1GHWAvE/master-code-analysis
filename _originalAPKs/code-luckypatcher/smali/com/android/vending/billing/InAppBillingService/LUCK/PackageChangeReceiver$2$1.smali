.class Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$2$1;
.super Ljava/lang/Object;
.source "PackageChangeReceiver.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$2;->onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$2;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$2;)V
    .registers 2
    .param p1, "this$1"    # Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$2;

    .prologue
    .line 598
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$2$1;->this$1:Lcom/android/vending/billing/InAppBillingService/LUCK/PackageChangeReceiver$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 9
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/high16 v6, 0x10000000

    const/high16 v5, 0x20000

    .line 602
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x9

    if-lt v3, v4, :cond_38

    .line 604
    const-string v3, "package:com.android.vending"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 605
    .local v2, "packageURI":Landroid/net/Uri;
    new-instance v0, Landroid/content/Intent;

    const-string v3, "android.settings.APPLICATION_DETAILS_SETTINGS"

    invoke-direct {v0, v3, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 606
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {v0, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 607
    invoke-virtual {v0, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 608
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 619
    .end local v2    # "packageURI":Landroid/net/Uri;
    :goto_24
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v3

    const-string v4, "window"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    .line 620
    .local v1, "manager":Landroid/view/WindowManager;
    invoke-virtual {p1}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v3

    invoke-interface {v1, v3}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 621
    return-void

    .line 611
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "manager":Landroid/view/WindowManager;
    :cond_38
    new-instance v0, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 612
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v3, "com.android.settings"

    const-string v4, "com.android.settings.InstalledAppDetails"

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 613
    const-string v3, "com.android.settings.ApplicationPkgName"

    const-string v4, "com.android.vending"

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 614
    const-string v3, "pkg"

    const-string v4, "com.android.vending"

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 615
    invoke-virtual {v0, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 616
    invoke-virtual {v0, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 617
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_24
.end method
