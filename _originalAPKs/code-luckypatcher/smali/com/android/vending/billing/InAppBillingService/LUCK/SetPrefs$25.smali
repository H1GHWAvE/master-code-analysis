.class Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$25;
.super Ljava/lang/Object;
.source "SetPrefs.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;)V
    .registers 2
    .param p1, "this$0"    # Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;

    .prologue
    .line 933
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$25;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .registers 10
    .param p1, "preference"    # Landroid/preference/Preference;

    .prologue
    const/4 v6, 0x0

    const/4 v7, 0x1

    .line 936
    iget-object v4, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$25;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;

    iget-object v4, v4, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->context:Landroid/content/Context;

    const/high16 v5, 0x7f040000

    invoke-static {v4, v5, v6}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 937
    .local v1, "d":Landroid/widget/LinearLayout;
    const v4, 0x7f0d0001

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    const v5, 0x7f0d0003

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 940
    .local v0, "body":Landroid/widget/LinearLayout;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const v5, 0x7f070229

    invoke-static {v5}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->version:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 941
    .local v2, "str2":Ljava/lang/String;
    const v4, 0x7f0d0004

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 942
    .local v3, "tv":Landroid/widget/TextView;
    const-string v4, "#ffffffff"

    const-string v5, "bold"

    invoke-static {v2, v4, v5}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 943
    const-string v2, "----------------------------------\n\n"

    .line 944
    const-string v4, "#ffffffff"

    const-string v5, "bold"

    invoke-static {v2, v4, v5}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 945
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const v5, 0x7f070004

    invoke-static {v5}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->basepath:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 946
    const-string v4, "#ffffffff"

    const-string v5, "bold"

    invoke-static {v2, v4, v5}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 948
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const v5, 0x7f0701bd

    invoke-static {v5}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 949
    const-string v4, "#ffff0000"

    const-string v5, "bold"

    invoke-static {v2, v4, v5}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 951
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const v5, 0x7f070005

    invoke-static {v5}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 952
    const-string v4, "#ffffff00"

    const-string v5, "bold"

    invoke-static {v2, v4, v5}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 956
    const-string v2, "\nThanks to:\nUsed source code: pyler, Mikanoshi@4pda.ru\nSite WebIconSet.com - Smile icons\np0izn@xda - English translation\nJang Seung Hun - Korean translation\nDerDownloader7,Simon Plantone - German translation\nAndrew Q., Simoconfa - Italian translation\ntrBilimBEKo,Z.Zeynalov,Noss_bar - Turkish translation\nSpoetnic - Dutch translation\n\u67d2\u6708\u96ea@BNB520,\u5356\u840c\u5b50,Hiapk-\u5929\u4f7f - Simplified Chinese translation\neauland, foXaCe - French translation\nrhrarhra@Mobilism.org, alireza_simkesh@XDA - Persian translation\nAlftronics & TheCh - Portuguese translation\nCaio Fons\u00eaca - Portuguese (Brasil) translation\n\u6f22\u9ed1\u7389,Hiapk-\u5929\u4f7f - Traditional Chinese translation\ns_h - Hebrew translation\nXcooM - Polish translation\nGizmo[SK],pyler - Slovak translation\nHarmeet Singh - Hindi translation\nPROXIMO - Bulgarian translation\nCulum - Indonesian translation\nslycog@XDA - Custom Patch Help translate\nCheng Sokdara (homi3kh) - Khmer translation\nrkononenko, masterlist@forpda, Volodiimr - Ukrainian translation\nJeduRocks@XDA - Spanish translation\nPaul Diosteanu - Romanian translation\ngidano - Hungarian translation\nOrbit09, Renek - Czech translation\ni_Droidi@Twitter - Arabic translation\nbobo1704 - Greek translation\nJack_Rover - Finnish translation\nWan Mohammad - Malay translation\nNguy\u1ec5n \u0110\u1ee9c L\u01b0\u1ee3ng - Vietnamese translation\nSOORAJ SR(fb.com/rsoorajs) - Malayalam translation\nEkberjan - Uighur translation\nevildog1 - Danish translation\nHzy980512 - Japanese translation\nNihar Patel) Gujarati translation\nChelpus, maksnogin - Russian translation\nIcons to menu - Sergey Kamashki\nTemplate for ADS - And123\n\nSupport by email: lp.chelpus@gmail.com\nGood Luck!\nChelpuS"

    .line 957
    const-string v4, "#ffffffff"

    const-string v5, "bold"

    invoke-static {v2, v4, v5}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 960
    new-instance v4, Landroid/app/AlertDialog$Builder;

    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$25;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;

    iget-object v5, v5, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->context:Landroid/content/Context;

    invoke-direct {v4, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v5, 0x7f070007

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x108009b

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x104000a

    .line 961
    invoke-static {v5}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v5

    .line 960
    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const-string v5, "Changelog"

    new-instance v6, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$25$2;

    invoke-direct {v6, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$25$2;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$25;)V

    .line 961
    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    .line 971
    invoke-virtual {v4, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    new-instance v5, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$25$1;

    invoke-direct {v5, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$25$1;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$25;)V

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    .line 979
    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/AlertDialog;->show()V

    .line 980
    return v7
.end method
