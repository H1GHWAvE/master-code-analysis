.class Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$26$2;
.super Ljava/lang/Object;
.source "SetPrefs.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$26;->onPreferenceClick(Landroid/preference/Preference;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$26;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$26;)V
    .registers 2
    .param p1, "this$1"    # Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$26;

    .prologue
    .line 1083
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$26$2;->this$1:Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$26;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 14
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 1086
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$26$2;->this$1:Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$26;

    iget-object v5, v5, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$26;->dialog6:Landroid/app/Dialog;

    invoke-virtual {v5}, Landroid/app/Dialog;->isShowing()Z

    move-result v5

    if-eqz v5, :cond_11

    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$26$2;->this$1:Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$26;

    iget-object v5, v5, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$26;->dialog6:Landroid/app/Dialog;

    invoke-virtual {v5}, Landroid/app/Dialog;->dismiss()V

    .line 1087
    :cond_11
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v5

    const-string v6, "config"

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 1088
    .local v3, "settings":Landroid/content/SharedPreferences;
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    const-string v6, "_"

    invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 1089
    .local v4, "tails":[Ljava/lang/String;
    const/4 v1, 0x0

    .line 1090
    .local v1, "appLoc2":Ljava/util/Locale;
    array-length v5, v4

    const/4 v6, 0x1

    if-ne v5, v6, :cond_35

    new-instance v1, Ljava/util/Locale;

    .end local v1    # "appLoc2":Ljava/util/Locale;
    const/4 v5, 0x0

    aget-object v5, v4, v5

    invoke-direct {v1, v5}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    .line 1091
    .restart local v1    # "appLoc2":Ljava/util/Locale;
    :cond_35
    array-length v5, v4

    const/4 v6, 0x2

    if-ne v5, v6, :cond_46

    new-instance v1, Ljava/util/Locale;

    .end local v1    # "appLoc2":Ljava/util/Locale;
    const/4 v5, 0x0

    aget-object v5, v4, v5

    const/4 v6, 0x1

    aget-object v6, v4, v6

    const-string v7, ""

    invoke-direct {v1, v5, v6, v7}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1092
    .restart local v1    # "appLoc2":Ljava/util/Locale;
    :cond_46
    array-length v5, v4

    const/4 v6, 0x3

    if-ne v5, v6, :cond_58

    .line 1093
    new-instance v1, Ljava/util/Locale;

    .end local v1    # "appLoc2":Ljava/util/Locale;
    const/4 v5, 0x0

    aget-object v5, v4, v5

    const/4 v6, 0x1

    aget-object v6, v4, v6

    const/4 v7, 0x2

    aget-object v7, v4, v7

    invoke-direct {v1, v5, v6, v7}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1095
    .restart local v1    # "appLoc2":Ljava/util/Locale;
    :cond_58
    invoke-static {v1}, Ljava/util/Locale;->setDefault(Ljava/util/Locale;)V

    .line 1096
    new-instance v0, Landroid/content/res/Configuration;

    invoke-direct {v0}, Landroid/content/res/Configuration;-><init>()V

    .line 1097
    .local v0, "appConfig2":Landroid/content/res/Configuration;
    iput-object v1, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 1098
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v5

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getRes()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v6

    invoke-virtual {v5, v0, v6}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    .line 1100
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    const-string v7, "force_language"

    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-interface {v6, v7, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1101
    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$26$2;->this$1:Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$26;

    iget-object v5, v5, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$26;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;

    invoke-virtual {v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 1103
    .local v2, "intent":Landroid/content/Intent;
    const/high16 v5, 0x20000

    invoke-virtual {v2, v5}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1104
    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$26$2;->this$1:Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$26;

    iget-object v5, v5, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$26;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;

    invoke-virtual {v5}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->finish()V

    .line 1106
    iget-object v5, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$26$2;->this$1:Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$26;

    iget-object v5, v5, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs$26;->this$0:Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;

    invoke-virtual {v5, v2}, Lcom/android/vending/billing/InAppBillingService/LUCK/SetPrefs;->startActivity(Landroid/content/Intent;)V

    .line 1107
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    const-string v6, "settings_change"

    const/4 v7, 0x1

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1108
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    const-string v6, "lang_change"

    const/4 v7, 0x1

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1110
    return-void
.end method
