.class Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1$1;
.super Landroid/widget/ArrayAdapter;
.source "listAppsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$2:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1;

.field txtStatus:Landroid/widget/TextView;

.field txtStatusPatch:Landroid/widget/TextView;

.field txtTitle:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1;Landroid/content/Context;ILjava/util/List;)V
    .registers 5
    .param p1, "this$2"    # Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1;
    .param p2, "x0"    # Landroid/content/Context;
    .param p3, "x1"    # I

    .prologue
    .line 9349
    .local p4, "x2":Ljava/util/List;, "Ljava/util/List<Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;>;"
    iput-object p1, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1$1;->this$2:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1;

    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 14
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 9356
    sget-boolean v6, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->xposedNotify:Z

    if-eqz v6, :cond_13

    .line 9357
    sget-object v6, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->frag:Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;

    sget-object v6, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->patchAct:Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;

    new-instance v7, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1$1$1;

    invoke-direct {v7, p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1$1$1;-><init>(Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1$1;)V

    invoke-virtual {v6, v7}, Lcom/android/vending/billing/InAppBillingService/LUCK/patchActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 9365
    const/4 v6, 0x0

    sput-boolean v6, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->xposedNotify:Z

    .line 9367
    :cond_13
    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1$1;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;

    .line 9368
    .local v3, "p":Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;
    move-object v4, p2

    .line 9375
    .local v4, "row":Landroid/view/View;
    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getInstance()Landroid/content/Context;

    move-result-object v6

    const-string v7, "layout_inflater"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 9376
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const v6, 0x7f04001f

    const/4 v7, 0x0

    invoke-virtual {v2, v6, p3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 9378
    const v6, 0x7f0d004e

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1$1;->txtTitle:Landroid/widget/TextView;

    .line 9379
    const v6, 0x7f0d004f

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1$1;->txtStatus:Landroid/widget/TextView;

    .line 9380
    const v6, 0x7f0d0081

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1$1;->txtStatusPatch:Landroid/widget/TextView;

    .line 9382
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1$1;->txtTitle:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1$1;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getSizeText()I

    move-result v8

    invoke-virtual {v6, v7, v8}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 9383
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1$1;->txtStatus:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1$1;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getSizeText()I

    move-result v8

    invoke-virtual {v6, v7, v8}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 9384
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1$1;->txtStatusPatch:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1$1;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getSizeText()I

    move-result v8

    invoke-virtual {v6, v7, v8}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 9386
    const v6, 0x7f0d0080

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 9387
    .local v0, "chk":Landroid/widget/CheckBox;
    iget-boolean v6, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;->Status:Z

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 9388
    if-nez p1, :cond_e8

    .line 9389
    invoke-static {}, Lcom/chelpus/Utils;->checkCoreJarPatch11()Z

    move-result v6

    if-eqz v6, :cond_24a

    .line 9391
    const/4 v1, 0x0

    .line 9392
    .local v1, "i":I
    invoke-static {}, Lcom/chelpus/Utils;->checkCoreJarPatch11()Z

    move-result v6

    if-eqz v6, :cond_95

    add-int/lit8 v1, v1, 0x1

    .line 9393
    :cond_95
    invoke-static {}, Lcom/chelpus/Utils;->checkCoreJarPatch12()Z

    move-result v6

    if-eqz v6, :cond_9d

    add-int/lit8 v1, v1, 0x1

    .line 9394
    :cond_9d
    const/4 v6, 0x2

    if-ne v1, v6, :cond_c6

    .line 9395
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 9396
    const/4 v6, 0x1

    iput-boolean v6, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;->disabled:Z

    .line 9397
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1$1;->txtStatusPatch:Landroid/widget/TextView;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "2/2 "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const v8, 0x7f0700bc

    invoke-static {v8}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 9399
    :cond_c6
    const/4 v6, 0x1

    if-ne v1, v6, :cond_e8

    .line 9400
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1$1;->txtStatusPatch:Landroid/widget/TextView;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "1/2 "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const v8, 0x7f0700bc

    invoke-static {v8}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 9411
    .end local v1    # "i":I
    :cond_e8
    :goto_e8
    const/4 v6, 0x1

    if-ne p1, v6, :cond_104

    .line 9412
    invoke-static {}, Lcom/chelpus/Utils;->checkCoreJarPatch20()Z

    move-result v6

    if-eqz v6, :cond_26a

    .line 9413
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 9414
    const/4 v6, 0x1

    iput-boolean v6, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;->disabled:Z

    .line 9415
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1$1;->txtStatusPatch:Landroid/widget/TextView;

    const v7, 0x7f0700bc

    invoke-static {v7}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 9424
    :cond_104
    :goto_104
    const/4 v6, 0x2

    if-ne p1, v6, :cond_128

    .line 9425
    sget-boolean v6, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->filePatch3:Z

    if-nez v6, :cond_115

    invoke-static {}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->getPkgMng()Landroid/content/pm/PackageManager;

    move-result-object v6

    invoke-static {v6}, Lcom/chelpus/Utils;->checkCoreJarPatch30(Landroid/content/pm/PackageManager;)Z

    move-result v6

    if-eqz v6, :cond_28a

    .line 9426
    :cond_115
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 9427
    const/4 v6, 0x1

    iput-boolean v6, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;->disabled:Z

    .line 9428
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1$1;->txtStatusPatch:Landroid/widget/TextView;

    const v7, 0x7f0700bc

    invoke-static {v7}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 9437
    :cond_128
    :goto_128
    const/4 v6, 0x3

    if-ne p1, v6, :cond_173

    .line 9438
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 9440
    sget-boolean v6, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->filePatch3:Z

    if-nez v6, :cond_139

    invoke-static {}, Lcom/chelpus/Utils;->checkCoreJarPatch40()Z

    move-result v6

    if-eqz v6, :cond_2aa

    .line 9441
    :cond_139
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 9442
    const/4 v6, 0x1

    iput-boolean v6, v3, Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;->disabled:Z

    .line 9443
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1$1;->txtStatusPatch:Landroid/widget/TextView;

    const v7, 0x7f0700bc

    invoke-static {v7}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 9451
    :goto_14c
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1$1;->txtStatusPatch:Landroid/widget/TextView;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const v8, 0x7f07007e

    invoke-static {v8}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const-string v8, "#FF0000"

    const-string v9, "normal"

    invoke-static {v7, v8, v9}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 9453
    :cond_173
    const/4 v6, 0x4

    if-ne p1, v6, :cond_18a

    .line 9454
    const v6, 0x7f070082

    invoke-static {v6}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v5

    .line 9455
    .local v5, "str2":Ljava/lang/String;
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1$1;->txtStatusPatch:Landroid/widget/TextView;

    const-string v7, "#ff888888"

    const-string v8, "italic"

    invoke-static {v5, v7, v8}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 9458
    .end local v5    # "str2":Ljava/lang/String;
    :cond_18a
    const/4 v6, 0x5

    if-ne p1, v6, :cond_1a1

    .line 9459
    const v6, 0x7f070081

    invoke-static {v6}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v5

    .line 9460
    .restart local v5    # "str2":Ljava/lang/String;
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1$1;->txtStatusPatch:Landroid/widget/TextView;

    const-string v7, "#ff888888"

    const-string v8, "italic"

    invoke-static {v5, v7, v8}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 9463
    .end local v5    # "str2":Ljava/lang/String;
    :cond_1a1
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 9474
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1$1;->txtStatus:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1$1;->getContext()Landroid/content/Context;

    move-result-object v7

    const v8, 0x1030046

    invoke-virtual {v6, v7, v8}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 9475
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1$1;->txtStatus:Landroid/widget/TextView;

    const v7, -0x777778

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setTextColor(I)V

    .line 9476
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1$1;->txtTitle:Landroid/widget/TextView;

    const/4 v7, -0x1

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setTextColor(I)V

    .line 9477
    iget-object v7, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1$1;->txtTitle:Landroid/widget/TextView;

    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1$1;->getItem(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;

    iget-object v6, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;->Name:Ljava/lang/String;

    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 9478
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1$1;->txtTitle:Landroid/widget/TextView;

    const/4 v7, 0x0

    const/4 v8, 0x1

    invoke-virtual {v6, v7, v8}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 9480
    invoke-virtual {p0, p1}, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1$1;->getItem(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;

    iget-object v5, v6, Lcom/android/vending/billing/InAppBillingService/LUCK/CoreItem;->Name:Ljava/lang/String;

    .line 9482
    .restart local v5    # "str2":Ljava/lang/String;
    if-eqz p1, :cond_1e0

    const/4 v6, 0x1

    if-ne p1, v6, :cond_2ca

    .line 9483
    :cond_1e0
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1$1;->txtTitle:Landroid/widget/TextView;

    const-string v7, "#ff00ff00"

    const-string v8, "bold"

    invoke-static {v5, v7, v8}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 9494
    :goto_1ed
    if-nez p1, :cond_1f6

    .line 9495
    const v6, 0x7f070075

    invoke-static {v6}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v5

    .line 9496
    :cond_1f6
    const/4 v6, 0x1

    if-ne p1, v6, :cond_200

    .line 9497
    const v6, 0x7f070077

    invoke-static {v6}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v5

    .line 9498
    :cond_200
    const/4 v6, 0x2

    if-ne p1, v6, :cond_228

    .line 9499
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const v7, 0x7f070079

    invoke-static {v7}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const v7, 0x7f07007a

    invoke-static {v7}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 9500
    :cond_228
    const/4 v6, 0x3

    if-ne p1, v6, :cond_232

    .line 9501
    const v6, 0x7f07007c

    invoke-static {v6}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v5

    .line 9502
    :cond_232
    const/4 v6, 0x4

    if-ne p1, v6, :cond_237

    const-string v5, ""

    .line 9503
    :cond_237
    const/4 v6, 0x5

    if-ne p1, v6, :cond_23c

    const-string v5, ""

    .line 9504
    :cond_23c
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1$1;->txtStatus:Landroid/widget/TextView;

    const-string v7, "#ff888888"

    const-string v8, "italic"

    invoke-static {v5, v7, v8}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 9508
    return-object v4

    .line 9403
    .end local v5    # "str2":Ljava/lang/String;
    :cond_24a
    sget-boolean v6, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->fileNotSpace:Z

    if-eqz v6, :cond_25c

    .line 9404
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1$1;->txtStatusPatch:Landroid/widget/TextView;

    const v7, 0x7f0700be

    invoke-static {v7}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_e8

    .line 9406
    :cond_25c
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1$1;->txtStatusPatch:Landroid/widget/TextView;

    const v7, 0x7f0700bd

    invoke-static {v7}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_e8

    .line 9417
    :cond_26a
    sget-boolean v6, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->fileNotSpace:Z

    if-eqz v6, :cond_27c

    .line 9418
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1$1;->txtStatusPatch:Landroid/widget/TextView;

    const v7, 0x7f0700be

    invoke-static {v7}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_104

    .line 9420
    :cond_27c
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1$1;->txtStatusPatch:Landroid/widget/TextView;

    const v7, 0x7f0700bd

    invoke-static {v7}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_104

    .line 9430
    :cond_28a
    sget-boolean v6, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->fileNotSpace:Z

    if-eqz v6, :cond_29c

    .line 9431
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1$1;->txtStatusPatch:Landroid/widget/TextView;

    const v7, 0x7f0700be

    invoke-static {v7}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_128

    .line 9433
    :cond_29c
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1$1;->txtStatusPatch:Landroid/widget/TextView;

    const v7, 0x7f0700bd

    invoke-static {v7}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_128

    .line 9445
    :cond_2aa
    sget-boolean v6, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment;->fileNotSpace:Z

    if-eqz v6, :cond_2bc

    .line 9446
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1$1;->txtStatusPatch:Landroid/widget/TextView;

    const v7, 0x7f0700be

    invoke-static {v7}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_14c

    .line 9448
    :cond_2bc
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1$1;->txtStatusPatch:Landroid/widget/TextView;

    const v7, 0x7f0700bd

    invoke-static {v7}, Lcom/chelpus/Utils;->getText(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_14c

    .line 9485
    .restart local v5    # "str2":Ljava/lang/String;
    :cond_2ca
    const/4 v6, 0x4

    if-eq p1, v6, :cond_2d0

    const/4 v6, 0x5

    if-ne p1, v6, :cond_2df

    .line 9486
    :cond_2d0
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1$1;->txtTitle:Landroid/widget/TextView;

    const-string v7, "#ffff0000"

    const-string v8, "bold"

    invoke-static {v5, v7, v8}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1ed

    .line 9488
    :cond_2df
    iget-object v6, p0, Lcom/android/vending/billing/InAppBillingService/LUCK/listAppsFragment$77$1$1;->txtTitle:Landroid/widget/TextView;

    const-string v7, "#ff00ff00"

    const-string v8, "bold"

    invoke-static {v5, v7, v8}, Lcom/chelpus/Utils;->getColoredText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1ed
.end method
