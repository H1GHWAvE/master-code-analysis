.class public abstract Lkellinwood/logging/AbstractLogger;
.super Ljava/lang/Object;
.source "AbstractLogger.java"

# interfaces
.implements Lkellinwood/logging/LoggerInterface;


# instance fields
.field protected category:Ljava/lang/String;

.field dateFormat:Ljava/text/SimpleDateFormat;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 4
    .param p1, "category"    # Ljava/lang/String;

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM-dd hh:mm:ss"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lkellinwood/logging/AbstractLogger;->dateFormat:Ljava/text/SimpleDateFormat;

    .line 30
    iput-object p1, p0, Lkellinwood/logging/AbstractLogger;->category:Ljava/lang/String;

    .line 31
    return-void
.end method


# virtual methods
.method public debug(Ljava/lang/String;)V
    .registers 4
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 52
    const-string v0, "DEBUG"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Lkellinwood/logging/AbstractLogger;->writeFixNullMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 53
    return-void
.end method

.method public debug(Ljava/lang/String;Ljava/lang/Throwable;)V
    .registers 4
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 48
    const-string v0, "DEBUG"

    invoke-virtual {p0, v0, p1, p2}, Lkellinwood/logging/AbstractLogger;->writeFixNullMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 49
    return-void
.end method

.method public error(Ljava/lang/String;)V
    .registers 4
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 60
    const-string v0, "ERROR"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Lkellinwood/logging/AbstractLogger;->writeFixNullMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 61
    return-void
.end method

.method public error(Ljava/lang/String;Ljava/lang/Throwable;)V
    .registers 4
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 56
    const-string v0, "ERROR"

    invoke-virtual {p0, v0, p1, p2}, Lkellinwood/logging/AbstractLogger;->writeFixNullMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 57
    return-void
.end method

.method protected format(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 8
    .param p1, "level"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 34
    const-string v0, "%s %s %s: %s\n"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lkellinwood/logging/AbstractLogger;->dateFormat:Ljava/text/SimpleDateFormat;

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    invoke-virtual {v3, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lkellinwood/logging/AbstractLogger;->category:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    aput-object p2, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public info(Ljava/lang/String;)V
    .registers 4
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 68
    const-string v0, "INFO"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Lkellinwood/logging/AbstractLogger;->writeFixNullMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 69
    return-void
.end method

.method public info(Ljava/lang/String;Ljava/lang/Throwable;)V
    .registers 4
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 64
    const-string v0, "INFO"

    invoke-virtual {p0, v0, p1, p2}, Lkellinwood/logging/AbstractLogger;->writeFixNullMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 65
    return-void
.end method

.method public isDebugEnabled()Z
    .registers 2

    .prologue
    .line 80
    const/4 v0, 0x1

    return v0
.end method

.method public isErrorEnabled()Z
    .registers 2

    .prologue
    .line 84
    const/4 v0, 0x1

    return v0
.end method

.method public isInfoEnabled()Z
    .registers 2

    .prologue
    .line 88
    const/4 v0, 0x1

    return v0
.end method

.method public isWarningEnabled()Z
    .registers 2

    .prologue
    .line 92
    const/4 v0, 0x1

    return v0
.end method

.method public warning(Ljava/lang/String;)V
    .registers 4
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 76
    const-string v0, "WARNING"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Lkellinwood/logging/AbstractLogger;->writeFixNullMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 77
    return-void
.end method

.method public warning(Ljava/lang/String;Ljava/lang/Throwable;)V
    .registers 4
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 72
    const-string v0, "WARNING"

    invoke-virtual {p0, v0, p1, p2}, Lkellinwood/logging/AbstractLogger;->writeFixNullMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 73
    return-void
.end method

.method protected abstract write(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
.end method

.method protected writeFixNullMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .registers 5
    .param p1, "level"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;
    .param p3, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 40
    if-nez p2, :cond_c

    .line 41
    if-eqz p3, :cond_10

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p2

    .line 44
    :cond_c
    :goto_c
    invoke-virtual {p0, p1, p2, p3}, Lkellinwood/logging/AbstractLogger;->write(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 45
    return-void

    .line 42
    :cond_10
    const-string p2, "null"

    goto :goto_c
.end method
