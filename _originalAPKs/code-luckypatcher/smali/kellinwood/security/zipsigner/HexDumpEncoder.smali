.class public Lkellinwood/security/zipsigner/HexDumpEncoder;
.super Ljava/lang/Object;
.source "HexDumpEncoder.java"


# static fields
.field static encoder:Lkellinwood/security/zipsigner/HexEncoder;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 29
    new-instance v0, Lkellinwood/security/zipsigner/HexEncoder;

    invoke-direct {v0}, Lkellinwood/security/zipsigner/HexEncoder;-><init>()V

    sput-object v0, Lkellinwood/security/zipsigner/HexDumpEncoder;->encoder:Lkellinwood/security/zipsigner/HexEncoder;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static encode([B)Ljava/lang/String;
    .registers 16
    .param p0, "data"    # [B

    .prologue
    .line 34
    :try_start_0
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 35
    .local v0, "baos":Ljava/io/ByteArrayOutputStream;
    sget-object v11, Lkellinwood/security/zipsigner/HexDumpEncoder;->encoder:Lkellinwood/security/zipsigner/HexEncoder;

    const/4 v12, 0x0

    array-length v13, p0

    invoke-virtual {v11, p0, v12, v13, v0}, Lkellinwood/security/zipsigner/HexEncoder;->encode([BIILjava/io/OutputStream;)I

    .line 36
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    .line 38
    .local v3, "hex":[B
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 39
    .local v4, "hexDumpOut":Ljava/lang/StringBuilder;
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_16
    array-length v11, v3

    if-ge v6, v11, :cond_d2

    .line 41
    add-int/lit8 v11, v6, 0x20

    array-length v12, v3

    invoke-static {v11, v12}, Ljava/lang/Math;->min(II)I

    move-result v9

    .line 43
    .local v9, "max":I
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 44
    .local v5, "hexOut":Ljava/lang/StringBuilder;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 46
    .local v1, "chrOut":Ljava/lang/StringBuilder;
    const-string v11, "%08x: "

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    div-int/lit8 v14, v6, 0x2

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v11, v12}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 48
    move v7, v6

    .local v7, "j":I
    :goto_40
    if-ge v7, v9, :cond_aa

    .line 49
    aget-byte v11, v3, v7

    int-to-char v11, v11

    invoke-static {v11}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v11

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 50
    add-int/lit8 v11, v7, 0x1

    aget-byte v11, v3, v11

    int-to-char v11, v11

    invoke-static {v11}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v11

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 51
    add-int/lit8 v11, v7, 0x2

    rem-int/lit8 v11, v11, 0x4

    if-nez v11, :cond_63

    const/16 v11, 0x20

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 53
    :cond_63
    div-int/lit8 v11, v7, 0x2

    aget-byte v2, p0, v11

    .line 54
    .local v2, "dataChar":I
    const/16 v11, 0x20

    if-lt v2, v11, :cond_7a

    const/16 v11, 0x7f

    if-ge v2, v11, :cond_7a

    int-to-char v11, v2

    invoke-static {v11}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v11

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 48
    :goto_77
    add-int/lit8 v7, v7, 0x2

    goto :goto_40

    .line 55
    :cond_7a
    const/16 v11, 0x2e

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
    :try_end_7f
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_7f} :catch_80

    goto :goto_77

    .line 68
    .end local v0    # "baos":Ljava/io/ByteArrayOutputStream;
    .end local v1    # "chrOut":Ljava/lang/StringBuilder;
    .end local v2    # "dataChar":I
    .end local v3    # "hex":[B
    .end local v4    # "hexDumpOut":Ljava/lang/StringBuilder;
    .end local v5    # "hexOut":Ljava/lang/StringBuilder;
    .end local v6    # "i":I
    .end local v7    # "j":I
    .end local v9    # "max":I
    :catch_80
    move-exception v10

    .line 69
    .local v10, "x":Ljava/io/IOException;
    new-instance v11, Ljava/lang/IllegalStateException;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ": "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v10}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v11

    .line 59
    .end local v10    # "x":Ljava/io/IOException;
    .restart local v0    # "baos":Ljava/io/ByteArrayOutputStream;
    .restart local v1    # "chrOut":Ljava/lang/StringBuilder;
    .restart local v3    # "hex":[B
    .restart local v4    # "hexDumpOut":Ljava/lang/StringBuilder;
    .restart local v5    # "hexOut":Ljava/lang/StringBuilder;
    .restart local v6    # "i":I
    .restart local v7    # "j":I
    .restart local v9    # "max":I
    :cond_aa
    :try_start_aa
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 60
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v8

    .local v8, "k":I
    :goto_b5
    const/16 v11, 0x32

    if-ge v8, v11, :cond_c1

    const/16 v11, 0x20

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    add-int/lit8 v8, v8, 0x1

    goto :goto_b5

    .line 61
    :cond_c1
    const-string v11, "  "

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 62
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 63
    const-string v11, "\n"

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 39
    add-int/lit8 v6, v6, 0x20

    goto/16 :goto_16

    .line 66
    .end local v1    # "chrOut":Ljava/lang/StringBuilder;
    .end local v5    # "hexOut":Ljava/lang/StringBuilder;
    .end local v7    # "j":I
    .end local v8    # "k":I
    .end local v9    # "max":I
    :cond_d2
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_d5
    .catch Ljava/io/IOException; {:try_start_aa .. :try_end_d5} :catch_80

    move-result-object v11

    return-object v11
.end method
