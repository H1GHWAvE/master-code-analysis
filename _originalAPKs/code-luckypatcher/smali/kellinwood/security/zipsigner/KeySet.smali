.class public Lkellinwood/security/zipsigner/KeySet;
.super Ljava/lang/Object;
.source "KeySet.java"


# instance fields
.field name:Ljava/lang/String;

.field privateKey:Ljava/security/PrivateKey;

.field publicKey:Ljava/security/cert/X509Certificate;

.field sigBlockTemplate:[B

.field signatureAlgorithm:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object v0, p0, Lkellinwood/security/zipsigner/KeySet;->publicKey:Ljava/security/cert/X509Certificate;

    .line 31
    iput-object v0, p0, Lkellinwood/security/zipsigner/KeySet;->privateKey:Ljava/security/PrivateKey;

    .line 34
    iput-object v0, p0, Lkellinwood/security/zipsigner/KeySet;->sigBlockTemplate:[B

    .line 36
    const-string v0, "SHA1withRSA"

    iput-object v0, p0, Lkellinwood/security/zipsigner/KeySet;->signatureAlgorithm:Ljava/lang/String;

    .line 39
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/security/cert/X509Certificate;Ljava/security/PrivateKey;Ljava/lang/String;[B)V
    .registers 7
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "publicKey"    # Ljava/security/cert/X509Certificate;
    .param p3, "privateKey"    # Ljava/security/PrivateKey;
    .param p4, "signatureAlgorithm"    # Ljava/lang/String;
    .param p5, "sigBlockTemplate"    # [B

    .prologue
    const/4 v0, 0x0

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object v0, p0, Lkellinwood/security/zipsigner/KeySet;->publicKey:Ljava/security/cert/X509Certificate;

    .line 31
    iput-object v0, p0, Lkellinwood/security/zipsigner/KeySet;->privateKey:Ljava/security/PrivateKey;

    .line 34
    iput-object v0, p0, Lkellinwood/security/zipsigner/KeySet;->sigBlockTemplate:[B

    .line 36
    const-string v0, "SHA1withRSA"

    iput-object v0, p0, Lkellinwood/security/zipsigner/KeySet;->signatureAlgorithm:Ljava/lang/String;

    .line 51
    iput-object p1, p0, Lkellinwood/security/zipsigner/KeySet;->name:Ljava/lang/String;

    .line 52
    iput-object p2, p0, Lkellinwood/security/zipsigner/KeySet;->publicKey:Ljava/security/cert/X509Certificate;

    .line 53
    iput-object p3, p0, Lkellinwood/security/zipsigner/KeySet;->privateKey:Ljava/security/PrivateKey;

    .line 54
    if-eqz p4, :cond_18

    iput-object p4, p0, Lkellinwood/security/zipsigner/KeySet;->signatureAlgorithm:Ljava/lang/String;

    .line 55
    :cond_18
    iput-object p5, p0, Lkellinwood/security/zipsigner/KeySet;->sigBlockTemplate:[B

    .line 56
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/security/cert/X509Certificate;Ljava/security/PrivateKey;[B)V
    .registers 6
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "publicKey"    # Ljava/security/cert/X509Certificate;
    .param p3, "privateKey"    # Ljava/security/PrivateKey;
    .param p4, "sigBlockTemplate"    # [B

    .prologue
    const/4 v0, 0x0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object v0, p0, Lkellinwood/security/zipsigner/KeySet;->publicKey:Ljava/security/cert/X509Certificate;

    .line 31
    iput-object v0, p0, Lkellinwood/security/zipsigner/KeySet;->privateKey:Ljava/security/PrivateKey;

    .line 34
    iput-object v0, p0, Lkellinwood/security/zipsigner/KeySet;->sigBlockTemplate:[B

    .line 36
    const-string v0, "SHA1withRSA"

    iput-object v0, p0, Lkellinwood/security/zipsigner/KeySet;->signatureAlgorithm:Ljava/lang/String;

    .line 43
    iput-object p1, p0, Lkellinwood/security/zipsigner/KeySet;->name:Ljava/lang/String;

    .line 44
    iput-object p2, p0, Lkellinwood/security/zipsigner/KeySet;->publicKey:Ljava/security/cert/X509Certificate;

    .line 45
    iput-object p3, p0, Lkellinwood/security/zipsigner/KeySet;->privateKey:Ljava/security/PrivateKey;

    .line 46
    iput-object p4, p0, Lkellinwood/security/zipsigner/KeySet;->sigBlockTemplate:[B

    .line 47
    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .registers 2

    .prologue
    .line 59
    iget-object v0, p0, Lkellinwood/security/zipsigner/KeySet;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getPrivateKey()Ljava/security/PrivateKey;
    .registers 2

    .prologue
    .line 75
    iget-object v0, p0, Lkellinwood/security/zipsigner/KeySet;->privateKey:Ljava/security/PrivateKey;

    return-object v0
.end method

.method public getPublicKey()Ljava/security/cert/X509Certificate;
    .registers 2

    .prologue
    .line 67
    iget-object v0, p0, Lkellinwood/security/zipsigner/KeySet;->publicKey:Ljava/security/cert/X509Certificate;

    return-object v0
.end method

.method public getSigBlockTemplate()[B
    .registers 2

    .prologue
    .line 83
    iget-object v0, p0, Lkellinwood/security/zipsigner/KeySet;->sigBlockTemplate:[B

    return-object v0
.end method

.method public getSignatureAlgorithm()Ljava/lang/String;
    .registers 2

    .prologue
    .line 91
    iget-object v0, p0, Lkellinwood/security/zipsigner/KeySet;->signatureAlgorithm:Ljava/lang/String;

    return-object v0
.end method

.method public setName(Ljava/lang/String;)V
    .registers 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lkellinwood/security/zipsigner/KeySet;->name:Ljava/lang/String;

    .line 64
    return-void
.end method

.method public setPrivateKey(Ljava/security/PrivateKey;)V
    .registers 2
    .param p1, "privateKey"    # Ljava/security/PrivateKey;

    .prologue
    .line 79
    iput-object p1, p0, Lkellinwood/security/zipsigner/KeySet;->privateKey:Ljava/security/PrivateKey;

    .line 80
    return-void
.end method

.method public setPublicKey(Ljava/security/cert/X509Certificate;)V
    .registers 2
    .param p1, "publicKey"    # Ljava/security/cert/X509Certificate;

    .prologue
    .line 71
    iput-object p1, p0, Lkellinwood/security/zipsigner/KeySet;->publicKey:Ljava/security/cert/X509Certificate;

    .line 72
    return-void
.end method

.method public setSigBlockTemplate([B)V
    .registers 2
    .param p1, "sigBlockTemplate"    # [B

    .prologue
    .line 87
    iput-object p1, p0, Lkellinwood/security/zipsigner/KeySet;->sigBlockTemplate:[B

    .line 88
    return-void
.end method

.method public setSignatureAlgorithm(Ljava/lang/String;)V
    .registers 2
    .param p1, "signatureAlgorithm"    # Ljava/lang/String;

    .prologue
    .line 95
    if-nez p1, :cond_5

    const-string p1, "SHA1withRSA"

    .line 97
    :goto_4
    return-void

    .line 96
    :cond_5
    iput-object p1, p0, Lkellinwood/security/zipsigner/KeySet;->signatureAlgorithm:Ljava/lang/String;

    goto :goto_4
.end method
