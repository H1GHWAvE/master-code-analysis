.class public Lkellinwood/security/zipsigner/DefaultResourceAdapter;
.super Ljava/lang/Object;
.source "DefaultResourceAdapter.java"

# interfaces
.implements Lkellinwood/security/zipsigner/ResourceAdapter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkellinwood/security/zipsigner/DefaultResourceAdapter$1;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    return-void
.end method


# virtual methods
.method public varargs getString(Lkellinwood/security/zipsigner/ResourceAdapter$Item;[Ljava/lang/Object;)Ljava/lang/String;
    .registers 8
    .param p1, "item"    # Lkellinwood/security/zipsigner/ResourceAdapter$Item;
    .param p2, "args"    # [Ljava/lang/Object;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 11
    sget-object v0, Lkellinwood/security/zipsigner/DefaultResourceAdapter$1;->$SwitchMap$kellinwood$security$zipsigner$ResourceAdapter$Item:[I

    invoke-virtual {p1}, Lkellinwood/security/zipsigner/ResourceAdapter$Item;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_60

    .line 29
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown item "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 13
    :pswitch_26
    const-string v0, "Input and output files are the same.  Specify a different name for the output."

    .line 27
    :goto_28
    return-object v0

    .line 15
    :pswitch_29
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unable to auto-select key for signing "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    aget-object v1, p2, v3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_28

    .line 17
    :pswitch_3f
    const-string v0, "Loading certificate and private key"

    goto :goto_28

    .line 19
    :pswitch_42
    const-string v0, "Parsing the input\'s central directory"

    goto :goto_28

    .line 21
    :pswitch_45
    const-string v0, "Generating manifest"

    goto :goto_28

    .line 23
    :pswitch_48
    const-string v0, "Generating signature file"

    goto :goto_28

    .line 25
    :pswitch_4b
    const-string v0, "Generating signature block file"

    goto :goto_28

    .line 27
    :pswitch_4e
    const-string v0, "Copying zip entry %d of %d"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    aget-object v2, p2, v3

    aput-object v2, v1, v3

    aget-object v2, p2, v4

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_28

    .line 11
    :pswitch_data_60
    .packed-switch 0x1
        :pswitch_26
        :pswitch_29
        :pswitch_3f
        :pswitch_42
        :pswitch_45
        :pswitch_48
        :pswitch_4b
        :pswitch_4e
    .end packed-switch
.end method
