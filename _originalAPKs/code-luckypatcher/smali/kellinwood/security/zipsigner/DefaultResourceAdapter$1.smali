.class synthetic Lkellinwood/security/zipsigner/DefaultResourceAdapter$1;
.super Ljava/lang/Object;
.source "DefaultResourceAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkellinwood/security/zipsigner/DefaultResourceAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$kellinwood$security$zipsigner$ResourceAdapter$Item:[I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 11
    invoke-static {}, Lkellinwood/security/zipsigner/ResourceAdapter$Item;->values()[Lkellinwood/security/zipsigner/ResourceAdapter$Item;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lkellinwood/security/zipsigner/DefaultResourceAdapter$1;->$SwitchMap$kellinwood$security$zipsigner$ResourceAdapter$Item:[I

    :try_start_9
    sget-object v0, Lkellinwood/security/zipsigner/DefaultResourceAdapter$1;->$SwitchMap$kellinwood$security$zipsigner$ResourceAdapter$Item:[I

    sget-object v1, Lkellinwood/security/zipsigner/ResourceAdapter$Item;->INPUT_SAME_AS_OUTPUT_ERROR:Lkellinwood/security/zipsigner/ResourceAdapter$Item;

    invoke-virtual {v1}, Lkellinwood/security/zipsigner/ResourceAdapter$Item;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_14
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_14} :catch_71

    :goto_14
    :try_start_14
    sget-object v0, Lkellinwood/security/zipsigner/DefaultResourceAdapter$1;->$SwitchMap$kellinwood$security$zipsigner$ResourceAdapter$Item:[I

    sget-object v1, Lkellinwood/security/zipsigner/ResourceAdapter$Item;->AUTO_KEY_SELECTION_ERROR:Lkellinwood/security/zipsigner/ResourceAdapter$Item;

    invoke-virtual {v1}, Lkellinwood/security/zipsigner/ResourceAdapter$Item;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_14 .. :try_end_1f} :catch_6f

    :goto_1f
    :try_start_1f
    sget-object v0, Lkellinwood/security/zipsigner/DefaultResourceAdapter$1;->$SwitchMap$kellinwood$security$zipsigner$ResourceAdapter$Item:[I

    sget-object v1, Lkellinwood/security/zipsigner/ResourceAdapter$Item;->LOADING_CERTIFICATE_AND_KEY:Lkellinwood/security/zipsigner/ResourceAdapter$Item;

    invoke-virtual {v1}, Lkellinwood/security/zipsigner/ResourceAdapter$Item;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1f .. :try_end_2a} :catch_6d

    :goto_2a
    :try_start_2a
    sget-object v0, Lkellinwood/security/zipsigner/DefaultResourceAdapter$1;->$SwitchMap$kellinwood$security$zipsigner$ResourceAdapter$Item:[I

    sget-object v1, Lkellinwood/security/zipsigner/ResourceAdapter$Item;->PARSING_CENTRAL_DIRECTORY:Lkellinwood/security/zipsigner/ResourceAdapter$Item;

    invoke-virtual {v1}, Lkellinwood/security/zipsigner/ResourceAdapter$Item;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_35
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2a .. :try_end_35} :catch_6b

    :goto_35
    :try_start_35
    sget-object v0, Lkellinwood/security/zipsigner/DefaultResourceAdapter$1;->$SwitchMap$kellinwood$security$zipsigner$ResourceAdapter$Item:[I

    sget-object v1, Lkellinwood/security/zipsigner/ResourceAdapter$Item;->GENERATING_MANIFEST:Lkellinwood/security/zipsigner/ResourceAdapter$Item;

    invoke-virtual {v1}, Lkellinwood/security/zipsigner/ResourceAdapter$Item;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_40
    .catch Ljava/lang/NoSuchFieldError; {:try_start_35 .. :try_end_40} :catch_69

    :goto_40
    :try_start_40
    sget-object v0, Lkellinwood/security/zipsigner/DefaultResourceAdapter$1;->$SwitchMap$kellinwood$security$zipsigner$ResourceAdapter$Item:[I

    sget-object v1, Lkellinwood/security/zipsigner/ResourceAdapter$Item;->GENERATING_SIGNATURE_FILE:Lkellinwood/security/zipsigner/ResourceAdapter$Item;

    invoke-virtual {v1}, Lkellinwood/security/zipsigner/ResourceAdapter$Item;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_4b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_40 .. :try_end_4b} :catch_67

    :goto_4b
    :try_start_4b
    sget-object v0, Lkellinwood/security/zipsigner/DefaultResourceAdapter$1;->$SwitchMap$kellinwood$security$zipsigner$ResourceAdapter$Item:[I

    sget-object v1, Lkellinwood/security/zipsigner/ResourceAdapter$Item;->GENERATING_SIGNATURE_BLOCK:Lkellinwood/security/zipsigner/ResourceAdapter$Item;

    invoke-virtual {v1}, Lkellinwood/security/zipsigner/ResourceAdapter$Item;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_56
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4b .. :try_end_56} :catch_65

    :goto_56
    :try_start_56
    sget-object v0, Lkellinwood/security/zipsigner/DefaultResourceAdapter$1;->$SwitchMap$kellinwood$security$zipsigner$ResourceAdapter$Item:[I

    sget-object v1, Lkellinwood/security/zipsigner/ResourceAdapter$Item;->COPYING_ZIP_ENTRY:Lkellinwood/security/zipsigner/ResourceAdapter$Item;

    invoke-virtual {v1}, Lkellinwood/security/zipsigner/ResourceAdapter$Item;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_62
    .catch Ljava/lang/NoSuchFieldError; {:try_start_56 .. :try_end_62} :catch_63

    :goto_62
    return-void

    :catch_63
    move-exception v0

    goto :goto_62

    :catch_65
    move-exception v0

    goto :goto_56

    :catch_67
    move-exception v0

    goto :goto_4b

    :catch_69
    move-exception v0

    goto :goto_40

    :catch_6b
    move-exception v0

    goto :goto_35

    :catch_6d
    move-exception v0

    goto :goto_2a

    :catch_6f
    move-exception v0

    goto :goto_1f

    :catch_71
    move-exception v0

    goto :goto_14
.end method
