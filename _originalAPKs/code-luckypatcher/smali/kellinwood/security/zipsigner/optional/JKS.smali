.class public Lkellinwood/security/zipsigner/optional/JKS;
.super Ljava/security/KeyStoreSpi;
.source "JKS.java"


# static fields
.field private static final MAGIC:I = -0x1120113

.field private static final PRIVATE_KEY:I = 0x1

.field private static final TRUSTED_CERT:I = 0x2


# instance fields
.field private final aliases:Ljava/util/Vector;

.field private final certChains:Ljava/util/HashMap;

.field private final dates:Ljava/util/HashMap;

.field private final privateKeys:Ljava/util/HashMap;

.field private final trustedCerts:Ljava/util/HashMap;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 174
    invoke-direct {p0}, Ljava/security/KeyStoreSpi;-><init>()V

    .line 175
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lkellinwood/security/zipsigner/optional/JKS;->aliases:Ljava/util/Vector;

    .line 176
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lkellinwood/security/zipsigner/optional/JKS;->trustedCerts:Ljava/util/HashMap;

    .line 177
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lkellinwood/security/zipsigner/optional/JKS;->privateKeys:Ljava/util/HashMap;

    .line 178
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lkellinwood/security/zipsigner/optional/JKS;->certChains:Ljava/util/HashMap;

    .line 179
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lkellinwood/security/zipsigner/optional/JKS;->dates:Ljava/util/HashMap;

    .line 181
    return-void
.end method

.method private static charsToBytes([C)[B
    .registers 6
    .param p0, "passwd"    # [C

    .prologue
    .line 540
    array-length v4, p0

    mul-int/lit8 v4, v4, 0x2

    new-array v0, v4, [B

    .line 541
    .local v0, "buf":[B
    const/4 v1, 0x0

    .local v1, "i":I
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_7
    array-length v4, p0

    if-ge v1, v4, :cond_1d

    .line 543
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "j":I
    .local v3, "j":I
    aget-char v4, p0, v1

    ushr-int/lit8 v4, v4, 0x8

    int-to-byte v4, v4

    aput-byte v4, v0, v2

    .line 544
    add-int/lit8 v2, v3, 0x1

    .end local v3    # "j":I
    .restart local v2    # "j":I
    aget-char v4, p0, v1

    int-to-byte v4, v4

    aput-byte v4, v0, v3

    .line 541
    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    .line 546
    :cond_1d
    return-object v0
.end method

.method private static decryptKey([B[B)[B
    .registers 14
    .param p0, "encryptedPKI"    # [B
    .param p1, "passwd"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/UnrecoverableKeyException;
        }
    .end annotation

    .prologue
    .line 464
    :try_start_0
    new-instance v3, Ljavax/crypto/EncryptedPrivateKeyInfo;

    invoke-direct {v3, p0}, Ljavax/crypto/EncryptedPrivateKeyInfo;-><init>([B)V

    .line 466
    .local v3, "epki":Ljavax/crypto/EncryptedPrivateKeyInfo;
    invoke-virtual {v3}, Ljavax/crypto/EncryptedPrivateKeyInfo;->getEncryptedData()[B

    move-result-object v2

    .line 467
    .local v2, "encr":[B
    const/16 v9, 0x14

    new-array v6, v9, [B

    .line 468
    .local v6, "keystream":[B
    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v11, 0x14

    invoke-static {v2, v9, v6, v10, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 469
    const/16 v9, 0x14

    new-array v0, v9, [B

    .line 470
    .local v0, "check":[B
    array-length v9, v2

    add-int/lit8 v9, v9, -0x14

    const/4 v10, 0x0

    const/16 v11, 0x14

    invoke-static {v2, v9, v0, v10, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 471
    array-length v9, v2

    add-int/lit8 v9, v9, -0x28

    new-array v5, v9, [B

    .line 472
    .local v5, "key":[B
    const-string v9, "SHA1"

    invoke-static {v9}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v7

    .line 473
    .local v7, "sha":Ljava/security/MessageDigest;
    const/4 v1, 0x0

    .line 474
    .local v1, "count":I
    :cond_2d
    array-length v9, v5

    if-ge v1, v9, :cond_54

    .line 476
    invoke-virtual {v7}, Ljava/security/MessageDigest;->reset()V

    .line 477
    invoke-virtual {v7, p1}, Ljava/security/MessageDigest;->update([B)V

    .line 478
    invoke-virtual {v7, v6}, Ljava/security/MessageDigest;->update([B)V

    .line 479
    const/4 v9, 0x0

    array-length v10, v6

    invoke-virtual {v7, v6, v9, v10}, Ljava/security/MessageDigest;->digest([BII)I

    .line 480
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_3f
    array-length v9, v6

    if-ge v4, v9, :cond_2d

    array-length v9, v5

    if-ge v1, v9, :cond_2d

    .line 482
    aget-byte v9, v6, v4

    add-int/lit8 v10, v1, 0x14

    aget-byte v10, v2, v10

    xor-int/2addr v9, v10

    int-to-byte v9, v9

    aput-byte v9, v5, v1

    .line 483
    add-int/lit8 v1, v1, 0x1

    .line 480
    add-int/lit8 v4, v4, 0x1

    goto :goto_3f

    .line 486
    .end local v4    # "i":I
    :cond_54
    invoke-virtual {v7}, Ljava/security/MessageDigest;->reset()V

    .line 487
    invoke-virtual {v7, p1}, Ljava/security/MessageDigest;->update([B)V

    .line 488
    invoke-virtual {v7, v5}, Ljava/security/MessageDigest;->update([B)V

    .line 489
    invoke-virtual {v7}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v9

    invoke-static {v0, v9}, Ljava/security/MessageDigest;->isEqual([B[B)Z

    move-result v9

    if-nez v9, :cond_7a

    .line 490
    new-instance v9, Ljava/security/UnrecoverableKeyException;

    const-string v10, "checksum mismatch"

    invoke-direct {v9, v10}, Ljava/security/UnrecoverableKeyException;-><init>(Ljava/lang/String;)V

    throw v9
    :try_end_6f
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_6f} :catch_6f

    .line 493
    .end local v0    # "check":[B
    .end local v1    # "count":I
    .end local v2    # "encr":[B
    .end local v3    # "epki":Ljavax/crypto/EncryptedPrivateKeyInfo;
    .end local v5    # "key":[B
    .end local v6    # "keystream":[B
    .end local v7    # "sha":Ljava/security/MessageDigest;
    :catch_6f
    move-exception v8

    .line 495
    .local v8, "x":Ljava/lang/Exception;
    new-instance v9, Ljava/security/UnrecoverableKeyException;

    invoke-virtual {v8}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/security/UnrecoverableKeyException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 491
    .end local v8    # "x":Ljava/lang/Exception;
    .restart local v0    # "check":[B
    .restart local v1    # "count":I
    .restart local v2    # "encr":[B
    .restart local v3    # "epki":Ljavax/crypto/EncryptedPrivateKeyInfo;
    .restart local v5    # "key":[B
    .restart local v6    # "keystream":[B
    .restart local v7    # "sha":Ljava/security/MessageDigest;
    :cond_7a
    return-object v5
.end method

.method private static encryptKey(Ljava/security/Key;[B)[B
    .registers 13
    .param p0, "key"    # Ljava/security/Key;
    .param p1, "passwd"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/KeyStoreException;
        }
    .end annotation

    .prologue
    .line 504
    :try_start_0
    const-string v8, "SHA1"

    invoke-static {v8}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v6

    .line 505
    .local v6, "sha":Ljava/security/MessageDigest;
    const-string v8, "SHA1PRNG"

    invoke-static {v8}, Ljava/security/SecureRandom;->getInstance(Ljava/lang/String;)Ljava/security/SecureRandom;

    move-result-object v5

    .line 506
    .local v5, "rand":Ljava/security/SecureRandom;
    invoke-interface {p0}, Ljava/security/Key;->getEncoded()[B

    move-result-object v3

    .line 507
    .local v3, "k":[B
    array-length v8, v3

    add-int/lit8 v8, v8, 0x28

    new-array v1, v8, [B

    .line 508
    .local v1, "encrypted":[B
    const/16 v8, 0x14

    invoke-static {v8}, Ljava/security/SecureRandom;->getSeed(I)[B

    move-result-object v4

    .line 509
    .local v4, "keystream":[B
    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x14

    invoke-static {v4, v8, v1, v9, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 510
    const/4 v0, 0x0

    .line 511
    .local v0, "count":I
    :cond_23
    array-length v8, v3

    if-ge v0, v8, :cond_4a

    .line 513
    invoke-virtual {v6}, Ljava/security/MessageDigest;->reset()V

    .line 514
    invoke-virtual {v6, p1}, Ljava/security/MessageDigest;->update([B)V

    .line 515
    invoke-virtual {v6, v4}, Ljava/security/MessageDigest;->update([B)V

    .line 516
    const/4 v8, 0x0

    array-length v9, v4

    invoke-virtual {v6, v4, v8, v9}, Ljava/security/MessageDigest;->digest([BII)I

    .line 517
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_35
    array-length v8, v4

    if-ge v2, v8, :cond_23

    array-length v8, v3

    if-ge v0, v8, :cond_23

    .line 519
    add-int/lit8 v8, v0, 0x14

    aget-byte v9, v4, v2

    aget-byte v10, v3, v0

    xor-int/2addr v9, v10

    int-to-byte v9, v9

    aput-byte v9, v1, v8

    .line 520
    add-int/lit8 v0, v0, 0x1

    .line 517
    add-int/lit8 v2, v2, 0x1

    goto :goto_35

    .line 523
    .end local v2    # "i":I
    :cond_4a
    invoke-virtual {v6}, Ljava/security/MessageDigest;->reset()V

    .line 524
    invoke-virtual {v6, p1}, Ljava/security/MessageDigest;->update([B)V

    .line 525
    invoke-virtual {v6, v3}, Ljava/security/MessageDigest;->update([B)V

    .line 526
    array-length v8, v1

    add-int/lit8 v8, v8, -0x14

    const/16 v9, 0x14

    invoke-virtual {v6, v1, v8, v9}, Ljava/security/MessageDigest;->digest([BII)I

    .line 529
    new-instance v8, Ljavax/crypto/EncryptedPrivateKeyInfo;

    const-string v9, "1.3.6.1.4.1.42.2.17.1.1"

    invoke-direct {v8, v9, v1}, Ljavax/crypto/EncryptedPrivateKeyInfo;-><init>(Ljava/lang/String;[B)V

    invoke-virtual {v8}, Ljavax/crypto/EncryptedPrivateKeyInfo;->getEncoded()[B
    :try_end_65
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_65} :catch_67

    move-result-object v8

    return-object v8

    .line 532
    .end local v0    # "count":I
    .end local v1    # "encrypted":[B
    .end local v3    # "k":[B
    .end local v4    # "keystream":[B
    .end local v5    # "rand":Ljava/security/SecureRandom;
    .end local v6    # "sha":Ljava/security/MessageDigest;
    :catch_67
    move-exception v7

    .line 534
    .local v7, "x":Ljava/lang/Exception;
    new-instance v8, Ljava/security/KeyStoreException;

    invoke-virtual {v7}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/security/KeyStoreException;-><init>(Ljava/lang/String;)V

    throw v8
.end method

.method private static readCert(Ljava/io/DataInputStream;)Ljava/security/cert/Certificate;
    .registers 6
    .param p0, "in"    # Ljava/io/DataInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/cert/CertificateException;,
            Ljava/security/NoSuchAlgorithmException;
        }
    .end annotation

    .prologue
    .line 442
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v3

    .line 443
    .local v3, "type":Ljava/lang/String;
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readInt()I

    move-result v2

    .line 444
    .local v2, "len":I
    new-array v0, v2, [B

    .line 445
    .local v0, "encoded":[B
    invoke-virtual {p0, v0}, Ljava/io/DataInputStream;->read([B)I

    .line 446
    invoke-static {v3}, Ljava/security/cert/CertificateFactory;->getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;

    move-result-object v1

    .line 447
    .local v1, "factory":Ljava/security/cert/CertificateFactory;
    new-instance v4, Ljava/io/ByteArrayInputStream;

    invoke-direct {v4, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-virtual {v1, v4}, Ljava/security/cert/CertificateFactory;->generateCertificate(Ljava/io/InputStream;)Ljava/security/cert/Certificate;

    move-result-object v4

    return-object v4
.end method

.method private static writeCert(Ljava/io/DataOutputStream;Ljava/security/cert/Certificate;)V
    .registers 4
    .param p0, "dout"    # Ljava/io/DataOutputStream;
    .param p1, "cert"    # Ljava/security/cert/Certificate;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/cert/CertificateException;
        }
    .end annotation

    .prologue
    .line 453
    invoke-virtual {p1}, Ljava/security/cert/Certificate;->getType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 454
    invoke-virtual {p1}, Ljava/security/cert/Certificate;->getEncoded()[B

    move-result-object v0

    .line 455
    .local v0, "b":[B
    array-length v1, v0

    invoke-virtual {p0, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 456
    invoke-virtual {p0, v0}, Ljava/io/DataOutputStream;->write([B)V

    .line 457
    return-void
.end method


# virtual methods
.method public engineAliases()Ljava/util/Enumeration;
    .registers 2

    .prologue
    .line 306
    iget-object v0, p0, Lkellinwood/security/zipsigner/optional/JKS;->aliases:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v0

    return-object v0
.end method

.method public engineContainsAlias(Ljava/lang/String;)Z
    .registers 3
    .param p1, "alias"    # Ljava/lang/String;

    .prologue
    .line 311
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    .line 312
    iget-object v0, p0, Lkellinwood/security/zipsigner/optional/JKS;->aliases:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public engineDeleteEntry(Ljava/lang/String;)V
    .registers 3
    .param p1, "alias"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/KeyStoreException;
        }
    .end annotation

    .prologue
    .line 300
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    .line 301
    iget-object v0, p0, Lkellinwood/security/zipsigner/optional/JKS;->aliases:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->remove(Ljava/lang/Object;)Z

    .line 302
    return-void
.end method

.method public engineGetCertificate(Ljava/lang/String;)Ljava/security/cert/Certificate;
    .registers 4
    .param p1, "alias"    # Ljava/lang/String;

    .prologue
    .line 222
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    .line 223
    invoke-virtual {p0, p1}, Lkellinwood/security/zipsigner/optional/JKS;->engineIsKeyEntry(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1e

    .line 224
    iget-object v1, p0, Lkellinwood/security/zipsigner/optional/JKS;->certChains:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/security/cert/Certificate;

    move-object v0, v1

    check-cast v0, [Ljava/security/cert/Certificate;

    .line 225
    .local v0, "certChain":[Ljava/security/cert/Certificate;
    if-eqz v0, :cond_1e

    array-length v1, v0

    if-lez v1, :cond_1e

    const/4 v1, 0x0

    aget-object v1, v0, v1

    .line 227
    .end local v0    # "certChain":[Ljava/security/cert/Certificate;
    :goto_1d
    return-object v1

    :cond_1e
    iget-object v1, p0, Lkellinwood/security/zipsigner/optional/JKS;->trustedCerts:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/security/cert/Certificate;

    goto :goto_1d
.end method

.method public engineGetCertificateAlias(Ljava/security/cert/Certificate;)Ljava/lang/String;
    .registers 5
    .param p1, "cert"    # Ljava/security/cert/Certificate;

    .prologue
    .line 334
    iget-object v2, p0, Lkellinwood/security/zipsigner/optional/JKS;->trustedCerts:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "keys":Ljava/util/Iterator;
    :cond_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_23

    .line 336
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 337
    .local v0, "alias":Ljava/lang/String;
    iget-object v2, p0, Lkellinwood/security/zipsigner/optional/JKS;->trustedCerts:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/security/cert/Certificate;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 340
    .end local v0    # "alias":Ljava/lang/String;
    :goto_22
    return-object v0

    :cond_23
    const/4 v0, 0x0

    goto :goto_22
.end method

.method public engineGetCertificateChain(Ljava/lang/String;)[Ljava/security/cert/Certificate;
    .registers 3
    .param p1, "alias"    # Ljava/lang/String;

    .prologue
    .line 216
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    .line 217
    iget-object v0, p0, Lkellinwood/security/zipsigner/optional/JKS;->certChains:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/security/cert/Certificate;

    check-cast v0, [Ljava/security/cert/Certificate;

    return-object v0
.end method

.method public engineGetCreationDate(Ljava/lang/String;)Ljava/util/Date;
    .registers 3
    .param p1, "alias"    # Ljava/lang/String;

    .prologue
    .line 232
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    .line 233
    iget-object v0, p0, Lkellinwood/security/zipsigner/optional/JKS;->dates:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Date;

    return-object v0
.end method

.method public engineGetKey(Ljava/lang/String;[C)Ljava/security/Key;
    .registers 9
    .param p1, "alias"    # Ljava/lang/String;
    .param p2, "password"    # [C
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;,
            Ljava/security/UnrecoverableKeyException;
        }
    .end annotation

    .prologue
    .line 189
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    .line 191
    iget-object v4, p0, Lkellinwood/security/zipsigner/optional/JKS;->privateKeys:Ljava/util/HashMap;

    invoke-virtual {v4, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_e

    .line 192
    const/4 v4, 0x0

    .line 211
    :goto_d
    return-object v4

    .line 193
    :cond_e
    iget-object v4, p0, Lkellinwood/security/zipsigner/optional/JKS;->privateKeys:Ljava/util/HashMap;

    invoke-virtual {v4, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [B

    check-cast v4, [B

    invoke-static {p2}, Lkellinwood/security/zipsigner/optional/JKS;->charsToBytes([C)[B

    move-result-object v5

    invoke-static {v4, v5}, Lkellinwood/security/zipsigner/optional/JKS;->decryptKey([B[B)[B

    move-result-object v2

    .line 195
    .local v2, "key":[B
    invoke-virtual {p0, p1}, Lkellinwood/security/zipsigner/optional/JKS;->engineGetCertificateChain(Ljava/lang/String;)[Ljava/security/cert/Certificate;

    move-result-object v0

    .line 196
    .local v0, "chain":[Ljava/security/cert/Certificate;
    array-length v4, v0

    if-lez v4, :cond_4b

    .line 201
    const/4 v4, 0x0

    :try_start_28
    aget-object v4, v0, v4

    invoke-virtual {v4}, Ljava/security/cert/Certificate;->getPublicKey()Ljava/security/PublicKey;

    move-result-object v4

    invoke-interface {v4}, Ljava/security/PublicKey;->getAlgorithm()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/security/KeyFactory;->getInstance(Ljava/lang/String;)Ljava/security/KeyFactory;

    move-result-object v1

    .line 203
    .local v1, "fact":Ljava/security/KeyFactory;
    new-instance v4, Ljava/security/spec/PKCS8EncodedKeySpec;

    invoke-direct {v4, v2}, Ljava/security/spec/PKCS8EncodedKeySpec;-><init>([B)V

    invoke-virtual {v1, v4}, Ljava/security/KeyFactory;->generatePrivate(Ljava/security/spec/KeySpec;)Ljava/security/PrivateKey;
    :try_end_3e
    .catch Ljava/security/spec/InvalidKeySpecException; {:try_start_28 .. :try_end_3e} :catch_40

    move-result-object v4

    goto :goto_d

    .line 205
    .end local v1    # "fact":Ljava/security/KeyFactory;
    :catch_40
    move-exception v3

    .line 207
    .local v3, "x":Ljava/security/spec/InvalidKeySpecException;
    new-instance v4, Ljava/security/UnrecoverableKeyException;

    invoke-virtual {v3}, Ljava/security/spec/InvalidKeySpecException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/security/UnrecoverableKeyException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 211
    .end local v3    # "x":Ljava/security/spec/InvalidKeySpecException;
    :cond_4b
    new-instance v4, Ljavax/crypto/spec/SecretKeySpec;

    invoke-direct {v4, v2, p1}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    goto :goto_d
.end method

.method public engineIsCertificateEntry(Ljava/lang/String;)Z
    .registers 3
    .param p1, "alias"    # Ljava/lang/String;

    .prologue
    .line 328
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    .line 329
    iget-object v0, p0, Lkellinwood/security/zipsigner/optional/JKS;->trustedCerts:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public engineIsKeyEntry(Ljava/lang/String;)Z
    .registers 3
    .param p1, "alias"    # Ljava/lang/String;

    .prologue
    .line 322
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    .line 323
    iget-object v0, p0, Lkellinwood/security/zipsigner/optional/JKS;->privateKeys:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public engineLoad(Ljava/io/InputStream;[C)V
    .registers 20
    .param p1, "in"    # Ljava/io/InputStream;
    .param p2, "passwd"    # [C
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/NoSuchAlgorithmException;,
            Ljava/security/cert/CertificateException;
        }
    .end annotation

    .prologue
    .line 384
    const-string v13, "SHA"

    invoke-static {v13}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v10

    .line 385
    .local v10, "md":Ljava/security/MessageDigest;
    if-eqz p2, :cond_f

    invoke-static/range {p2 .. p2}, Lkellinwood/security/zipsigner/optional/JKS;->charsToBytes([C)[B

    move-result-object v13

    invoke-virtual {v10, v13}, Ljava/security/MessageDigest;->update([B)V

    .line 386
    :cond_f
    const-string v13, "Mighty Aphrodite"

    const-string v14, "UTF-8"

    invoke-virtual {v13, v14}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v13

    invoke-virtual {v10, v13}, Ljava/security/MessageDigest;->update([B)V

    .line 387
    move-object/from16 v0, p0

    iget-object v13, v0, Lkellinwood/security/zipsigner/optional/JKS;->aliases:Ljava/util/Vector;

    invoke-virtual {v13}, Ljava/util/Vector;->clear()V

    .line 388
    move-object/from16 v0, p0

    iget-object v13, v0, Lkellinwood/security/zipsigner/optional/JKS;->trustedCerts:Ljava/util/HashMap;

    invoke-virtual {v13}, Ljava/util/HashMap;->clear()V

    .line 389
    move-object/from16 v0, p0

    iget-object v13, v0, Lkellinwood/security/zipsigner/optional/JKS;->privateKeys:Ljava/util/HashMap;

    invoke-virtual {v13}, Ljava/util/HashMap;->clear()V

    .line 390
    move-object/from16 v0, p0

    iget-object v13, v0, Lkellinwood/security/zipsigner/optional/JKS;->certChains:Ljava/util/HashMap;

    invoke-virtual {v13}, Ljava/util/HashMap;->clear()V

    .line 391
    move-object/from16 v0, p0

    iget-object v13, v0, Lkellinwood/security/zipsigner/optional/JKS;->dates:Ljava/util/HashMap;

    invoke-virtual {v13}, Ljava/util/HashMap;->clear()V

    .line 392
    if-nez p1, :cond_40

    .line 434
    :cond_3f
    return-void

    .line 393
    :cond_40
    new-instance v4, Ljava/io/DataInputStream;

    new-instance v13, Ljava/security/DigestInputStream;

    move-object/from16 v0, p1

    invoke-direct {v13, v0, v10}, Ljava/security/DigestInputStream;-><init>(Ljava/io/InputStream;Ljava/security/MessageDigest;)V

    invoke-direct {v4, v13}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 394
    .local v4, "din":Ljava/io/DataInputStream;
    invoke-virtual {v4}, Ljava/io/DataInputStream;->readInt()I

    move-result v13

    const v14, -0x1120113

    if-eq v13, v14, :cond_5d

    .line 395
    new-instance v13, Ljava/io/IOException;

    const-string v14, "not a JavaKeyStore"

    invoke-direct {v13, v14}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v13

    .line 396
    :cond_5d
    invoke-virtual {v4}, Ljava/io/DataInputStream;->readInt()I

    .line 397
    invoke-virtual {v4}, Ljava/io/DataInputStream;->readInt()I

    move-result v11

    .line 398
    .local v11, "n":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lkellinwood/security/zipsigner/optional/JKS;->aliases:Ljava/util/Vector;

    invoke-virtual {v13, v11}, Ljava/util/Vector;->ensureCapacity(I)V

    .line 399
    if-gez v11, :cond_75

    .line 400
    new-instance v13, Ljava/io/IOException;

    const-string v14, "negative entry count"

    invoke-direct {v13, v14}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v13

    .line 401
    :cond_75
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_76
    if-ge v7, v11, :cond_da

    .line 403
    invoke-virtual {v4}, Ljava/io/DataInputStream;->readInt()I

    move-result v12

    .line 404
    .local v12, "type":I
    invoke-virtual {v4}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v1

    .line 405
    .local v1, "alias":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v13, v0, Lkellinwood/security/zipsigner/optional/JKS;->aliases:Ljava/util/Vector;

    invoke-virtual {v13, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 406
    move-object/from16 v0, p0

    iget-object v13, v0, Lkellinwood/security/zipsigner/optional/JKS;->dates:Ljava/util/HashMap;

    new-instance v14, Ljava/util/Date;

    invoke-virtual {v4}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v15

    invoke-direct/range {v14 .. v16}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v13, v1, v14}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 407
    packed-switch v12, :pswitch_data_f6

    .line 426
    new-instance v13, Ljava/io/IOException;

    const-string v14, "malformed key store"

    invoke-direct {v13, v14}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v13

    .line 410
    :pswitch_a2
    invoke-virtual {v4}, Ljava/io/DataInputStream;->readInt()I

    move-result v9

    .line 411
    .local v9, "len":I
    new-array v5, v9, [B

    .line 412
    .local v5, "encoded":[B
    invoke-virtual {v4, v5}, Ljava/io/DataInputStream;->read([B)I

    .line 413
    move-object/from16 v0, p0

    iget-object v13, v0, Lkellinwood/security/zipsigner/optional/JKS;->privateKeys:Ljava/util/HashMap;

    invoke-virtual {v13, v1, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 414
    invoke-virtual {v4}, Ljava/io/DataInputStream;->readInt()I

    move-result v3

    .line 415
    .local v3, "count":I
    new-array v2, v3, [Ljava/security/cert/Certificate;

    .line 416
    .local v2, "chain":[Ljava/security/cert/Certificate;
    const/4 v8, 0x0

    .local v8, "j":I
    :goto_b9
    if-ge v8, v3, :cond_c4

    .line 417
    invoke-static {v4}, Lkellinwood/security/zipsigner/optional/JKS;->readCert(Ljava/io/DataInputStream;)Ljava/security/cert/Certificate;

    move-result-object v13

    aput-object v13, v2, v8

    .line 416
    add-int/lit8 v8, v8, 0x1

    goto :goto_b9

    .line 418
    :cond_c4
    move-object/from16 v0, p0

    iget-object v13, v0, Lkellinwood/security/zipsigner/optional/JKS;->certChains:Ljava/util/HashMap;

    invoke-virtual {v13, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 401
    .end local v2    # "chain":[Ljava/security/cert/Certificate;
    .end local v3    # "count":I
    .end local v5    # "encoded":[B
    .end local v8    # "j":I
    .end local v9    # "len":I
    :goto_cb
    add-int/lit8 v7, v7, 0x1

    goto :goto_76

    .line 422
    :pswitch_ce
    move-object/from16 v0, p0

    iget-object v13, v0, Lkellinwood/security/zipsigner/optional/JKS;->trustedCerts:Ljava/util/HashMap;

    invoke-static {v4}, Lkellinwood/security/zipsigner/optional/JKS;->readCert(Ljava/io/DataInputStream;)Ljava/security/cert/Certificate;

    move-result-object v14

    invoke-virtual {v13, v1, v14}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_cb

    .line 430
    .end local v1    # "alias":Ljava/lang/String;
    .end local v12    # "type":I
    :cond_da
    const/16 v13, 0x14

    new-array v6, v13, [B

    .line 431
    .local v6, "hash":[B
    invoke-virtual {v4, v6}, Ljava/io/DataInputStream;->read([B)I

    .line 432
    if-eqz p2, :cond_3f

    invoke-virtual {v10}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v13

    invoke-static {v6, v13}, Ljava/security/MessageDigest;->isEqual([B[B)Z

    move-result v13

    if-eqz v13, :cond_3f

    .line 433
    new-instance v13, Ljava/io/IOException;

    const-string v14, "signature not verified"

    invoke-direct {v13, v14}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v13

    .line 407
    nop

    :pswitch_data_f6
    .packed-switch 0x1
        :pswitch_a2
        :pswitch_ce
    .end packed-switch
.end method

.method public engineSetCertificateEntry(Ljava/lang/String;Ljava/security/cert/Certificate;)V
    .registers 6
    .param p1, "alias"    # Ljava/lang/String;
    .param p2, "cert"    # Ljava/security/cert/Certificate;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/KeyStoreException;
        }
    .end annotation

    .prologue
    .line 285
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    .line 286
    iget-object v0, p0, Lkellinwood/security/zipsigner/optional/JKS;->privateKeys:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 287
    new-instance v0, Ljava/security/KeyStoreException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\" is a private key entry"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/security/KeyStoreException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 288
    :cond_2b
    if-nez p2, :cond_33

    .line 289
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 290
    :cond_33
    iget-object v0, p0, Lkellinwood/security/zipsigner/optional/JKS;->trustedCerts:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 291
    iget-object v0, p0, Lkellinwood/security/zipsigner/optional/JKS;->aliases:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4f

    .line 293
    iget-object v0, p0, Lkellinwood/security/zipsigner/optional/JKS;->dates:Ljava/util/HashMap;

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 294
    iget-object v0, p0, Lkellinwood/security/zipsigner/optional/JKS;->aliases:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 296
    :cond_4f
    return-void
.end method

.method public engineSetKeyEntry(Ljava/lang/String;Ljava/security/Key;[C[Ljava/security/cert/Certificate;)V
    .registers 8
    .param p1, "alias"    # Ljava/lang/String;
    .param p2, "key"    # Ljava/security/Key;
    .param p3, "passwd"    # [C
    .param p4, "certChain"    # [Ljava/security/cert/Certificate;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/KeyStoreException;
        }
    .end annotation

    .prologue
    .line 241
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    .line 242
    iget-object v0, p0, Lkellinwood/security/zipsigner/optional/JKS;->trustedCerts:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 243
    new-instance v0, Ljava/security/KeyStoreException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is a trusted certificate entry"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/security/KeyStoreException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 244
    :cond_2b
    iget-object v0, p0, Lkellinwood/security/zipsigner/optional/JKS;->privateKeys:Ljava/util/HashMap;

    invoke-static {p3}, Lkellinwood/security/zipsigner/optional/JKS;->charsToBytes([C)[B

    move-result-object v1

    invoke-static {p2, v1}, Lkellinwood/security/zipsigner/optional/JKS;->encryptKey(Ljava/security/Key;[B)[B

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 245
    if-eqz p4, :cond_57

    .line 246
    iget-object v0, p0, Lkellinwood/security/zipsigner/optional/JKS;->certChains:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 249
    :goto_3f
    iget-object v0, p0, Lkellinwood/security/zipsigner/optional/JKS;->aliases:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_56

    .line 251
    iget-object v0, p0, Lkellinwood/security/zipsigner/optional/JKS;->dates:Ljava/util/HashMap;

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 252
    iget-object v0, p0, Lkellinwood/security/zipsigner/optional/JKS;->aliases:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 254
    :cond_56
    return-void

    .line 248
    :cond_57
    iget-object v0, p0, Lkellinwood/security/zipsigner/optional/JKS;->certChains:Ljava/util/HashMap;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/security/cert/Certificate;

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3f
.end method

.method public engineSetKeyEntry(Ljava/lang/String;[B[Ljava/security/cert/Certificate;)V
    .registers 8
    .param p1, "alias"    # Ljava/lang/String;
    .param p2, "encodedKey"    # [B
    .param p3, "certChain"    # [Ljava/security/cert/Certificate;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/KeyStoreException;
        }
    .end annotation

    .prologue
    .line 259
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    .line 260
    iget-object v1, p0, Lkellinwood/security/zipsigner/optional/JKS;->trustedCerts:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2b

    .line 261
    new-instance v1, Ljava/security/KeyStoreException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\" is a trusted certificate entry"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/security/KeyStoreException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 264
    :cond_2b
    :try_start_2b
    new-instance v1, Ljavax/crypto/EncryptedPrivateKeyInfo;

    invoke-direct {v1, p2}, Ljavax/crypto/EncryptedPrivateKeyInfo;-><init>([B)V
    :try_end_30
    .catch Ljava/io/IOException; {:try_start_2b .. :try_end_30} :catch_54

    .line 270
    iget-object v1, p0, Lkellinwood/security/zipsigner/optional/JKS;->privateKeys:Ljava/util/HashMap;

    invoke-virtual {v1, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 271
    if-eqz p3, :cond_5d

    .line 272
    iget-object v1, p0, Lkellinwood/security/zipsigner/optional/JKS;->certChains:Ljava/util/HashMap;

    invoke-virtual {v1, p1, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 275
    :goto_3c
    iget-object v1, p0, Lkellinwood/security/zipsigner/optional/JKS;->aliases:Ljava/util/Vector;

    invoke-virtual {v1, p1}, Ljava/util/Vector;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_53

    .line 277
    iget-object v1, p0, Lkellinwood/security/zipsigner/optional/JKS;->dates:Ljava/util/HashMap;

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v1, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 278
    iget-object v1, p0, Lkellinwood/security/zipsigner/optional/JKS;->aliases:Ljava/util/Vector;

    invoke-virtual {v1, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 280
    :cond_53
    return-void

    .line 266
    :catch_54
    move-exception v0

    .line 268
    .local v0, "ioe":Ljava/io/IOException;
    new-instance v1, Ljava/security/KeyStoreException;

    const-string v2, "encoded key is not an EncryptedPrivateKeyInfo"

    invoke-direct {v1, v2}, Ljava/security/KeyStoreException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 274
    .end local v0    # "ioe":Ljava/io/IOException;
    :cond_5d
    iget-object v1, p0, Lkellinwood/security/zipsigner/optional/JKS;->certChains:Ljava/util/HashMap;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/security/cert/Certificate;

    invoke-virtual {v1, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3c
.end method

.method public engineSize()I
    .registers 2

    .prologue
    .line 317
    iget-object v0, p0, Lkellinwood/security/zipsigner/optional/JKS;->aliases:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    return v0
.end method

.method public engineStore(Ljava/io/OutputStream;[C)V
    .registers 14
    .param p1, "out"    # Ljava/io/OutputStream;
    .param p2, "passwd"    # [C
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/NoSuchAlgorithmException;,
            Ljava/security/cert/CertificateException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x2

    .line 346
    const-string v8, "SHA1"

    invoke-static {v8}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v7

    .line 347
    .local v7, "md":Ljava/security/MessageDigest;
    invoke-static {p2}, Lkellinwood/security/zipsigner/optional/JKS;->charsToBytes([C)[B

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/security/MessageDigest;->update([B)V

    .line 348
    const-string v8, "Mighty Aphrodite"

    const-string v9, "UTF-8"

    invoke-virtual {v8, v9}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/security/MessageDigest;->update([B)V

    .line 349
    new-instance v3, Ljava/io/DataOutputStream;

    new-instance v8, Ljava/security/DigestOutputStream;

    invoke-direct {v8, p1, v7}, Ljava/security/DigestOutputStream;-><init>(Ljava/io/OutputStream;Ljava/security/MessageDigest;)V

    invoke-direct {v3, v8}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 350
    .local v3, "dout":Ljava/io/DataOutputStream;
    const v8, -0x1120113

    invoke-virtual {v3, v8}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 351
    invoke-virtual {v3, v10}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 352
    iget-object v8, p0, Lkellinwood/security/zipsigner/optional/JKS;->aliases:Ljava/util/Vector;

    invoke-virtual {v8}, Ljava/util/Vector;->size()I

    move-result v8

    invoke-virtual {v3, v8}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 353
    iget-object v8, p0, Lkellinwood/security/zipsigner/optional/JKS;->aliases:Ljava/util/Vector;

    invoke-virtual {v8}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v4

    .local v4, "e":Ljava/util/Enumeration;
    :cond_3b
    :goto_3b
    invoke-interface {v4}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v8

    if-eqz v8, :cond_b3

    .line 355
    invoke-interface {v4}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 356
    .local v0, "alias":Ljava/lang/String;
    iget-object v8, p0, Lkellinwood/security/zipsigner/optional/JKS;->trustedCerts:Ljava/util/HashMap;

    invoke-virtual {v8, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_70

    .line 358
    invoke-virtual {v3, v10}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 359
    invoke-virtual {v3, v0}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 360
    iget-object v8, p0, Lkellinwood/security/zipsigner/optional/JKS;->dates:Ljava/util/HashMap;

    invoke-virtual {v8, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/Date;

    invoke-virtual {v8}, Ljava/util/Date;->getTime()J

    move-result-wide v8

    invoke-virtual {v3, v8, v9}, Ljava/io/DataOutputStream;->writeLong(J)V

    .line 361
    iget-object v8, p0, Lkellinwood/security/zipsigner/optional/JKS;->trustedCerts:Ljava/util/HashMap;

    invoke-virtual {v8, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/security/cert/Certificate;

    invoke-static {v3, v8}, Lkellinwood/security/zipsigner/optional/JKS;->writeCert(Ljava/io/DataOutputStream;Ljava/security/cert/Certificate;)V

    goto :goto_3b

    .line 365
    :cond_70
    const/4 v8, 0x1

    invoke-virtual {v3, v8}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 366
    invoke-virtual {v3, v0}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 367
    iget-object v8, p0, Lkellinwood/security/zipsigner/optional/JKS;->dates:Ljava/util/HashMap;

    invoke-virtual {v8, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/Date;

    invoke-virtual {v8}, Ljava/util/Date;->getTime()J

    move-result-wide v8

    invoke-virtual {v3, v8, v9}, Ljava/io/DataOutputStream;->writeLong(J)V

    .line 368
    iget-object v8, p0, Lkellinwood/security/zipsigner/optional/JKS;->privateKeys:Ljava/util/HashMap;

    invoke-virtual {v8, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [B

    move-object v6, v8

    check-cast v6, [B

    .line 369
    .local v6, "key":[B
    array-length v8, v6

    invoke-virtual {v3, v8}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 370
    invoke-virtual {v3, v6}, Ljava/io/DataOutputStream;->write([B)V

    .line 371
    iget-object v8, p0, Lkellinwood/security/zipsigner/optional/JKS;->certChains:Ljava/util/HashMap;

    invoke-virtual {v8, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [Ljava/security/cert/Certificate;

    move-object v1, v8

    check-cast v1, [Ljava/security/cert/Certificate;

    .line 372
    .local v1, "chain":[Ljava/security/cert/Certificate;
    array-length v8, v1

    invoke-virtual {v3, v8}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 373
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_a8
    array-length v8, v1

    if-ge v5, v8, :cond_3b

    .line 374
    aget-object v8, v1, v5

    invoke-static {v3, v8}, Lkellinwood/security/zipsigner/optional/JKS;->writeCert(Ljava/io/DataOutputStream;Ljava/security/cert/Certificate;)V

    .line 373
    add-int/lit8 v5, v5, 0x1

    goto :goto_a8

    .line 377
    .end local v0    # "alias":Ljava/lang/String;
    .end local v1    # "chain":[Ljava/security/cert/Certificate;
    .end local v5    # "i":I
    .end local v6    # "key":[B
    :cond_b3
    invoke-virtual {v7}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v2

    .line 378
    .local v2, "digest":[B
    invoke-virtual {v3, v2}, Ljava/io/DataOutputStream;->write([B)V

    .line 379
    return-void
.end method
