.class public Lkellinwood/security/zipsigner/optional/SignatureBlockGenerator;
.super Ljava/lang/Object;
.source "SignatureBlockGenerator.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static generate(Lkellinwood/security/zipsigner/KeySet;[B)[B
    .registers 16
    .param p0, "keySet"    # Lkellinwood/security/zipsigner/KeySet;
    .param p1, "content"    # [B

    .prologue
    .line 27
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 28
    .local v0, "certList":Ljava/util/List;
    new-instance v7, Lorg/spongycastle/cms/CMSProcessableByteArray;

    invoke-direct {v7, p1}, Lorg/spongycastle/cms/CMSProcessableByteArray;-><init>([B)V

    .line 30
    .local v7, "msg":Lorg/spongycastle/cms/CMSTypedData;
    invoke-virtual {p0}, Lkellinwood/security/zipsigner/KeySet;->getPublicKey()Ljava/security/cert/X509Certificate;

    move-result-object v12

    invoke-interface {v0, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 32
    new-instance v1, Lorg/spongycastle/cert/jcajce/JcaCertStore;

    invoke-direct {v1, v0}, Lorg/spongycastle/cert/jcajce/JcaCertStore;-><init>(Ljava/util/Collection;)V

    .line 34
    .local v1, "certs":Lorg/spongycastle/util/Store;
    new-instance v3, Lorg/spongycastle/cms/CMSSignedDataGenerator;

    invoke-direct {v3}, Lorg/spongycastle/cms/CMSSignedDataGenerator;-><init>()V

    .line 36
    .local v3, "gen":Lorg/spongycastle/cms/CMSSignedDataGenerator;
    new-instance v12, Lorg/spongycastle/operator/jcajce/JcaContentSignerBuilder;

    invoke-virtual {p0}, Lkellinwood/security/zipsigner/KeySet;->getSignatureAlgorithm()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Lorg/spongycastle/operator/jcajce/JcaContentSignerBuilder;-><init>(Ljava/lang/String;)V

    const-string v13, "SC"

    invoke-virtual {v12, v13}, Lorg/spongycastle/operator/jcajce/JcaContentSignerBuilder;->setProvider(Ljava/lang/String;)Lorg/spongycastle/operator/jcajce/JcaContentSignerBuilder;

    move-result-object v4

    .line 37
    .local v4, "jcaContentSignerBuilder":Lorg/spongycastle/operator/jcajce/JcaContentSignerBuilder;
    invoke-virtual {p0}, Lkellinwood/security/zipsigner/KeySet;->getPrivateKey()Ljava/security/PrivateKey;

    move-result-object v12

    invoke-virtual {v4, v12}, Lorg/spongycastle/operator/jcajce/JcaContentSignerBuilder;->build(Ljava/security/PrivateKey;)Lorg/spongycastle/operator/ContentSigner;

    move-result-object v8

    .line 39
    .local v8, "sha1Signer":Lorg/spongycastle/operator/ContentSigner;
    new-instance v12, Lorg/spongycastle/operator/jcajce/JcaDigestCalculatorProviderBuilder;

    invoke-direct {v12}, Lorg/spongycastle/operator/jcajce/JcaDigestCalculatorProviderBuilder;-><init>()V

    const-string v13, "SC"

    invoke-virtual {v12, v13}, Lorg/spongycastle/operator/jcajce/JcaDigestCalculatorProviderBuilder;->setProvider(Ljava/lang/String;)Lorg/spongycastle/operator/jcajce/JcaDigestCalculatorProviderBuilder;

    move-result-object v5

    .line 40
    .local v5, "jcaDigestCalculatorProviderBuilder":Lorg/spongycastle/operator/jcajce/JcaDigestCalculatorProviderBuilder;
    invoke-virtual {v5}, Lorg/spongycastle/operator/jcajce/JcaDigestCalculatorProviderBuilder;->build()Lorg/spongycastle/operator/DigestCalculatorProvider;

    move-result-object v2

    .line 42
    .local v2, "digestCalculatorProvider":Lorg/spongycastle/operator/DigestCalculatorProvider;
    new-instance v6, Lorg/spongycastle/cms/jcajce/JcaSignerInfoGeneratorBuilder;

    invoke-direct {v6, v2}, Lorg/spongycastle/cms/jcajce/JcaSignerInfoGeneratorBuilder;-><init>(Lorg/spongycastle/operator/DigestCalculatorProvider;)V

    .line 43
    .local v6, "jcaSignerInfoGeneratorBuilder":Lorg/spongycastle/cms/jcajce/JcaSignerInfoGeneratorBuilder;
    const/4 v12, 0x1

    invoke-virtual {v6, v12}, Lorg/spongycastle/cms/jcajce/JcaSignerInfoGeneratorBuilder;->setDirectSignature(Z)Lorg/spongycastle/cms/jcajce/JcaSignerInfoGeneratorBuilder;

    .line 44
    invoke-virtual {p0}, Lkellinwood/security/zipsigner/KeySet;->getPublicKey()Ljava/security/cert/X509Certificate;

    move-result-object v12

    invoke-virtual {v6, v8, v12}, Lorg/spongycastle/cms/jcajce/JcaSignerInfoGeneratorBuilder;->build(Lorg/spongycastle/operator/ContentSigner;Ljava/security/cert/X509Certificate;)Lorg/spongycastle/cms/SignerInfoGenerator;

    move-result-object v10

    .line 46
    .local v10, "signerInfoGenerator":Lorg/spongycastle/cms/SignerInfoGenerator;
    invoke-virtual {v3, v10}, Lorg/spongycastle/cms/CMSSignedDataGenerator;->addSignerInfoGenerator(Lorg/spongycastle/cms/SignerInfoGenerator;)V

    .line 48
    invoke-virtual {v3, v1}, Lorg/spongycastle/cms/CMSSignedDataGenerator;->addCertificates(Lorg/spongycastle/util/Store;)V

    .line 50
    const/4 v12, 0x0

    invoke-virtual {v3, v7, v12}, Lorg/spongycastle/cms/CMSSignedDataGenerator;->generate(Lorg/spongycastle/cms/CMSTypedData;Z)Lorg/spongycastle/cms/CMSSignedData;

    move-result-object v9

    .line 51
    .local v9, "sigData":Lorg/spongycastle/cms/CMSSignedData;
    invoke-virtual {v9}, Lorg/spongycastle/cms/CMSSignedData;->toASN1Structure()Lorg/spongycastle/asn1/cms/ContentInfo;

    move-result-object v12

    const-string v13, "DER"

    invoke-virtual {v12, v13}, Lorg/spongycastle/asn1/cms/ContentInfo;->getEncoded(Ljava/lang/String;)[B
    :try_end_66
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_66} :catch_68

    move-result-object v12

    return-object v12

    .line 53
    .end local v0    # "certList":Ljava/util/List;
    .end local v1    # "certs":Lorg/spongycastle/util/Store;
    .end local v2    # "digestCalculatorProvider":Lorg/spongycastle/operator/DigestCalculatorProvider;
    .end local v3    # "gen":Lorg/spongycastle/cms/CMSSignedDataGenerator;
    .end local v4    # "jcaContentSignerBuilder":Lorg/spongycastle/operator/jcajce/JcaContentSignerBuilder;
    .end local v5    # "jcaDigestCalculatorProviderBuilder":Lorg/spongycastle/operator/jcajce/JcaDigestCalculatorProviderBuilder;
    .end local v6    # "jcaSignerInfoGeneratorBuilder":Lorg/spongycastle/cms/jcajce/JcaSignerInfoGeneratorBuilder;
    .end local v7    # "msg":Lorg/spongycastle/cms/CMSTypedData;
    .end local v8    # "sha1Signer":Lorg/spongycastle/operator/ContentSigner;
    .end local v9    # "sigData":Lorg/spongycastle/cms/CMSSignedData;
    .end local v10    # "signerInfoGenerator":Lorg/spongycastle/cms/SignerInfoGenerator;
    :catch_68
    move-exception v11

    .line 54
    .local v11, "x":Ljava/lang/Exception;
    new-instance v12, Ljava/lang/RuntimeException;

    invoke-virtual {v11}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13, v11}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v12
.end method
