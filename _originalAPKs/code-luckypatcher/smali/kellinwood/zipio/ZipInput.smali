.class public Lkellinwood/zipio/ZipInput;
.super Ljava/lang/Object;
.source "ZipInput.java"


# static fields
.field static log:Lkellinwood/logging/LoggerInterface;


# instance fields
.field centralEnd:Lkellinwood/zipio/CentralEnd;

.field fileLength:J

.field in:Ljava/io/RandomAccessFile;

.field public inputFilename:Ljava/lang/String;

.field manifest:Ljava/util/jar/Manifest;

.field scanIterations:I

.field zioEntries:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lkellinwood/zipio/ZioEntry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 5
    .param p1, "filename"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    const/4 v0, 0x0

    iput-object v0, p0, Lkellinwood/zipio/ZipInput;->in:Ljava/io/RandomAccessFile;

    .line 48
    const/4 v0, 0x0

    iput v0, p0, Lkellinwood/zipio/ZipInput;->scanIterations:I

    .line 50
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lkellinwood/zipio/ZipInput;->zioEntries:Ljava/util/Map;

    .line 56
    iput-object p1, p0, Lkellinwood/zipio/ZipInput;->inputFilename:Ljava/lang/String;

    .line 57
    new-instance v0, Ljava/io/RandomAccessFile;

    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lkellinwood/zipio/ZipInput;->inputFilename:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const-string v2, "r"

    invoke-direct {v0, v1, v2}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, Lkellinwood/zipio/ZipInput;->in:Ljava/io/RandomAccessFile;

    .line 58
    iget-object v0, p0, Lkellinwood/zipio/ZipInput;->in:Ljava/io/RandomAccessFile;

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v0

    iput-wide v0, p0, Lkellinwood/zipio/ZipInput;->fileLength:J

    .line 59
    return-void
.end method

.method private doRead()V
    .registers 12

    .prologue
    .line 150
    const/16 v6, 0x100

    :try_start_2
    invoke-virtual {p0, v6}, Lkellinwood/zipio/ZipInput;->scanForEOCDR(I)J

    move-result-wide v3

    .line 151
    .local v3, "posEOCDR":J
    iget-object v6, p0, Lkellinwood/zipio/ZipInput;->in:Ljava/io/RandomAccessFile;

    invoke-virtual {v6, v3, v4}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 152
    invoke-static {p0}, Lkellinwood/zipio/CentralEnd;->read(Lkellinwood/zipio/ZipInput;)Lkellinwood/zipio/CentralEnd;

    move-result-object v6

    iput-object v6, p0, Lkellinwood/zipio/ZipInput;->centralEnd:Lkellinwood/zipio/CentralEnd;

    .line 154
    invoke-static {}, Lkellinwood/zipio/ZipInput;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v6

    invoke-interface {v6}, Lkellinwood/logging/LoggerInterface;->isDebugEnabled()Z

    move-result v0

    .line 155
    .local v0, "debug":Z
    if-eqz v0, :cond_77

    .line 156
    invoke-static {}, Lkellinwood/zipio/ZipInput;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v6

    const-string v7, "EOCD found in %d iterations"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget v10, p0, Lkellinwood/zipio/ZipInput;->scanIterations:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 157
    invoke-static {}, Lkellinwood/zipio/ZipInput;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v6

    const-string v7, "Directory entries=%d, size=%d, offset=%d/0x%08x"

    const/4 v8, 0x4

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-object v10, p0, Lkellinwood/zipio/ZipInput;->centralEnd:Lkellinwood/zipio/CentralEnd;

    iget-short v10, v10, Lkellinwood/zipio/CentralEnd;->totalCentralEntries:S

    invoke-static {v10}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    iget-object v10, p0, Lkellinwood/zipio/ZipInput;->centralEnd:Lkellinwood/zipio/CentralEnd;

    iget v10, v10, Lkellinwood/zipio/CentralEnd;->centralDirectorySize:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x2

    iget-object v10, p0, Lkellinwood/zipio/ZipInput;->centralEnd:Lkellinwood/zipio/CentralEnd;

    iget v10, v10, Lkellinwood/zipio/CentralEnd;->centralStartOffset:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x3

    iget-object v10, p0, Lkellinwood/zipio/ZipInput;->centralEnd:Lkellinwood/zipio/CentralEnd;

    iget v10, v10, Lkellinwood/zipio/CentralEnd;->centralStartOffset:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Lkellinwood/logging/LoggerInterface;->debug(Ljava/lang/String;)V

    .line 160
    invoke-static {}, Lkellinwood/zipio/ZipInput;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v6

    invoke-static {v6}, Lkellinwood/zipio/ZipListingHelper;->listHeader(Lkellinwood/logging/LoggerInterface;)V

    .line 163
    :cond_77
    iget-object v6, p0, Lkellinwood/zipio/ZipInput;->in:Ljava/io/RandomAccessFile;

    iget-object v7, p0, Lkellinwood/zipio/ZipInput;->centralEnd:Lkellinwood/zipio/CentralEnd;

    iget v7, v7, Lkellinwood/zipio/CentralEnd;->centralStartOffset:I

    int-to-long v7, v7

    invoke-virtual {v6, v7, v8}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 165
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_82
    iget-object v6, p0, Lkellinwood/zipio/ZipInput;->centralEnd:Lkellinwood/zipio/CentralEnd;

    iget-short v6, v6, Lkellinwood/zipio/CentralEnd;->totalCentralEntries:S

    if-ge v2, v6, :cond_a5

    .line 166
    invoke-static {p0}, Lkellinwood/zipio/ZioEntry;->read(Lkellinwood/zipio/ZipInput;)Lkellinwood/zipio/ZioEntry;

    move-result-object v1

    .line 167
    .local v1, "entry":Lkellinwood/zipio/ZioEntry;
    iget-object v6, p0, Lkellinwood/zipio/ZipInput;->zioEntries:Ljava/util/Map;

    invoke-virtual {v1}, Lkellinwood/zipio/ZioEntry;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 168
    if-eqz v0, :cond_9e

    invoke-static {}, Lkellinwood/zipio/ZipInput;->getLogger()Lkellinwood/logging/LoggerInterface;

    move-result-object v6

    invoke-static {v6, v1}, Lkellinwood/zipio/ZipListingHelper;->listEntry(Lkellinwood/logging/LoggerInterface;Lkellinwood/zipio/ZioEntry;)V
    :try_end_9e
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_9e} :catch_a1

    .line 165
    :cond_9e
    add-int/lit8 v2, v2, 0x1

    goto :goto_82

    .line 172
    .end local v0    # "debug":Z
    .end local v1    # "entry":Lkellinwood/zipio/ZioEntry;
    .end local v2    # "i":I
    .end local v3    # "posEOCDR":J
    :catch_a1
    move-exception v5

    .line 173
    .local v5, "t":Ljava/lang/Throwable;
    invoke-virtual {v5}, Ljava/lang/Throwable;->printStackTrace()V

    .line 175
    .end local v5    # "t":Ljava/lang/Throwable;
    :cond_a5
    return-void
.end method

.method private static getLogger()Lkellinwood/logging/LoggerInterface;
    .registers 1

    .prologue
    .line 62
    sget-object v0, Lkellinwood/zipio/ZipInput;->log:Lkellinwood/logging/LoggerInterface;

    if-nez v0, :cond_10

    const-class v0, Lkellinwood/zipio/ZipInput;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lkellinwood/logging/LoggerManager;->getLogger(Ljava/lang/String;)Lkellinwood/logging/LoggerInterface;

    move-result-object v0

    sput-object v0, Lkellinwood/zipio/ZipInput;->log:Lkellinwood/logging/LoggerInterface;

    .line 63
    :cond_10
    sget-object v0, Lkellinwood/zipio/ZipInput;->log:Lkellinwood/logging/LoggerInterface;

    return-object v0
.end method

.method public static read(Ljava/lang/String;)Lkellinwood/zipio/ZipInput;
    .registers 2
    .param p0, "filename"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 75
    new-instance v0, Lkellinwood/zipio/ZipInput;

    invoke-direct {v0, p0}, Lkellinwood/zipio/ZipInput;-><init>(Ljava/lang/String;)V

    .line 76
    .local v0, "zipInput":Lkellinwood/zipio/ZipInput;
    invoke-direct {v0}, Lkellinwood/zipio/ZipInput;->doRead()V

    .line 77
    return-object v0
.end method


# virtual methods
.method public close()V
    .registers 2

    .prologue
    .line 178
    iget-object v0, p0, Lkellinwood/zipio/ZipInput;->in:Ljava/io/RandomAccessFile;

    if-eqz v0, :cond_9

    :try_start_4
    iget-object v0, p0, Lkellinwood/zipio/ZipInput;->in:Ljava/io/RandomAccessFile;

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->close()V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_9} :catch_a

    .line 179
    :cond_9
    :goto_9
    return-void

    .line 178
    :catch_a
    move-exception v0

    goto :goto_9
.end method

.method public getEntries()Ljava/util/Map;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lkellinwood/zipio/ZioEntry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 86
    iget-object v0, p0, Lkellinwood/zipio/ZipInput;->zioEntries:Ljava/util/Map;

    return-object v0
.end method

.method public getEntry(Ljava/lang/String;)Lkellinwood/zipio/ZioEntry;
    .registers 3
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    .line 82
    iget-object v0, p0, Lkellinwood/zipio/ZipInput;->zioEntries:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkellinwood/zipio/ZioEntry;

    return-object v0
.end method

.method public getFileLength()J
    .registers 3

    .prologue
    .line 71
    iget-wide v0, p0, Lkellinwood/zipio/ZipInput;->fileLength:J

    return-wide v0
.end method

.method public getFilePointer()J
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 182
    iget-object v0, p0, Lkellinwood/zipio/ZipInput;->in:Ljava/io/RandomAccessFile;

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v0

    return-wide v0
.end method

.method public getFilename()Ljava/lang/String;
    .registers 2

    .prologue
    .line 67
    iget-object v0, p0, Lkellinwood/zipio/ZipInput;->inputFilename:Ljava/lang/String;

    return-object v0
.end method

.method public getManifest()Ljava/util/jar/Manifest;
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 111
    iget-object v1, p0, Lkellinwood/zipio/ZipInput;->manifest:Ljava/util/jar/Manifest;

    if-nez v1, :cond_1b

    .line 112
    iget-object v1, p0, Lkellinwood/zipio/ZipInput;->zioEntries:Ljava/util/Map;

    const-string v2, "META-INF/MANIFEST.MF"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkellinwood/zipio/ZioEntry;

    .line 113
    .local v0, "e":Lkellinwood/zipio/ZioEntry;
    if-eqz v0, :cond_1b

    .line 114
    new-instance v1, Ljava/util/jar/Manifest;

    invoke-virtual {v0}, Lkellinwood/zipio/ZioEntry;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/jar/Manifest;-><init>(Ljava/io/InputStream;)V

    iput-object v1, p0, Lkellinwood/zipio/ZipInput;->manifest:Ljava/util/jar/Manifest;

    .line 117
    .end local v0    # "e":Lkellinwood/zipio/ZioEntry;
    :cond_1b
    iget-object v1, p0, Lkellinwood/zipio/ZipInput;->manifest:Ljava/util/jar/Manifest;

    return-object v1
.end method

.method public list(Ljava/lang/String;)Ljava/util/Collection;
    .registers 11
    .param p1, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    .line 95
    const-string v5, "/"

    invoke-virtual {p1, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_11

    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v6, "Invalid path -- does not end with \'/\'"

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 97
    :cond_11
    const-string v5, "/"

    invoke-virtual {p1, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1d

    invoke-virtual {p1, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    .line 99
    :cond_1d
    const-string v5, "^%s([^/]+/?).*"

    new-array v6, v8, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object p1, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    .line 101
    .local v4, "p":Ljava/util/regex/Pattern;
    new-instance v3, Ljava/util/TreeSet;

    invoke-direct {v3}, Ljava/util/TreeSet;-><init>()V

    .line 103
    .local v3, "names":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iget-object v5, p0, Lkellinwood/zipio/ZipInput;->zioEntries:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_3b
    :goto_3b
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_59

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 104
    .local v2, "name":Ljava/lang/String;
    invoke-virtual {v4, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 105
    .local v1, "m":Ljava/util/regex/Matcher;
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v5

    if-eqz v5, :cond_3b

    invoke-virtual {v1, v8}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_3b

    .line 107
    .end local v1    # "m":Ljava/util/regex/Matcher;
    .end local v2    # "name":Ljava/lang/String;
    :cond_59
    return-object v3
.end method

.method public read([BII)I
    .registers 5
    .param p1, "b"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 228
    iget-object v0, p0, Lkellinwood/zipio/ZipInput;->in:Ljava/io/RandomAccessFile;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/RandomAccessFile;->read([BII)I

    move-result v0

    return v0
.end method

.method public readByte()B
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 190
    iget-object v0, p0, Lkellinwood/zipio/ZipInput;->in:Ljava/io/RandomAccessFile;

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->readByte()B

    move-result v0

    return v0
.end method

.method public readBytes(I)[B
    .registers 5
    .param p1, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 220
    new-array v0, p1, [B

    .line 221
    .local v0, "buffer":[B
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_3
    if-ge v1, p1, :cond_10

    .line 222
    iget-object v2, p0, Lkellinwood/zipio/ZipInput;->in:Ljava/io/RandomAccessFile;

    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->readByte()B

    move-result v2

    aput-byte v2, v0, v1

    .line 221
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 224
    :cond_10
    return-object v0
.end method

.method public readInt()I
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 194
    const/4 v1, 0x0

    .line 195
    .local v1, "result":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    const/4 v2, 0x4

    if-ge v0, v2, :cond_12

    .line 196
    iget-object v2, p0, Lkellinwood/zipio/ZipInput;->in:Ljava/io/RandomAccessFile;

    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->readUnsignedByte()I

    move-result v2

    mul-int/lit8 v3, v0, 0x8

    shl-int/2addr v2, v3

    or-int/2addr v1, v2

    .line 195
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 198
    :cond_12
    return v1
.end method

.method public readShort()S
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 202
    const/4 v1, 0x0

    .line 203
    .local v1, "result":S
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    const/4 v2, 0x2

    if-ge v0, v2, :cond_13

    .line 204
    iget-object v2, p0, Lkellinwood/zipio/ZipInput;->in:Ljava/io/RandomAccessFile;

    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->readUnsignedByte()I

    move-result v2

    mul-int/lit8 v3, v0, 0x8

    shl-int/2addr v2, v3

    or-int/2addr v2, v1

    int-to-short v1, v2

    .line 203
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 206
    :cond_13
    return v1
.end method

.method public readString(I)Ljava/lang/String;
    .registers 5
    .param p1, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 211
    new-array v0, p1, [B

    .line 212
    .local v0, "buffer":[B
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_3
    if-ge v1, p1, :cond_10

    .line 213
    iget-object v2, p0, Lkellinwood/zipio/ZipInput;->in:Ljava/io/RandomAccessFile;

    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->readByte()B

    move-result v2

    aput-byte v2, v0, v1

    .line 212
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 215
    :cond_10
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>([B)V

    return-object v2
.end method

.method public scanForEOCDR(I)J
    .registers 10
    .param p1, "size"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 125
    int-to-long v3, p1

    iget-wide v5, p0, Lkellinwood/zipio/ZipInput;->fileLength:J

    cmp-long v3, v3, v5

    if-gtz v3, :cond_b

    const/high16 v3, 0x10000

    if-le p1, v3, :cond_26

    :cond_b
    new-instance v3, Ljava/lang/IllegalStateException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "End of central directory not found in "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lkellinwood/zipio/ZipInput;->inputFilename:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 127
    :cond_26
    iget-wide v3, p0, Lkellinwood/zipio/ZipInput;->fileLength:J

    int-to-long v5, p1

    invoke-static {v3, v4, v5, v6}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v3

    long-to-int v2, v3

    .line 129
    .local v2, "scanSize":I
    new-array v1, v2, [B

    .line 131
    .local v1, "scanBuf":[B
    iget-object v3, p0, Lkellinwood/zipio/ZipInput;->in:Ljava/io/RandomAccessFile;

    iget-wide v4, p0, Lkellinwood/zipio/ZipInput;->fileLength:J

    int-to-long v6, v2

    sub-long/2addr v4, v6

    invoke-virtual {v3, v4, v5}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 133
    iget-object v3, p0, Lkellinwood/zipio/ZipInput;->in:Ljava/io/RandomAccessFile;

    invoke-virtual {v3, v1}, Ljava/io/RandomAccessFile;->readFully([B)V

    .line 135
    add-int/lit8 v0, v2, -0x16

    .local v0, "i":I
    :goto_40
    if-ltz v0, :cond_6e

    .line 136
    iget v3, p0, Lkellinwood/zipio/ZipInput;->scanIterations:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lkellinwood/zipio/ZipInput;->scanIterations:I

    .line 137
    aget-byte v3, v1, v0

    const/16 v4, 0x50

    if-ne v3, v4, :cond_6b

    add-int/lit8 v3, v0, 0x1

    aget-byte v3, v1, v3

    const/16 v4, 0x4b

    if-ne v3, v4, :cond_6b

    add-int/lit8 v3, v0, 0x2

    aget-byte v3, v1, v3

    const/4 v4, 0x5

    if-ne v3, v4, :cond_6b

    add-int/lit8 v3, v0, 0x3

    aget-byte v3, v1, v3

    const/4 v4, 0x6

    if-ne v3, v4, :cond_6b

    .line 138
    iget-wide v3, p0, Lkellinwood/zipio/ZipInput;->fileLength:J

    int-to-long v5, v2

    sub-long/2addr v3, v5

    int-to-long v5, v0

    add-long/2addr v3, v5

    .line 142
    :goto_6a
    return-wide v3

    .line 135
    :cond_6b
    add-int/lit8 v0, v0, -0x1

    goto :goto_40

    .line 142
    :cond_6e
    mul-int/lit8 v3, p1, 0x2

    invoke-virtual {p0, v3}, Lkellinwood/zipio/ZipInput;->scanForEOCDR(I)J

    move-result-wide v3

    goto :goto_6a
.end method

.method public seek(J)V
    .registers 4
    .param p1, "position"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 186
    iget-object v0, p0, Lkellinwood/zipio/ZipInput;->in:Ljava/io/RandomAccessFile;

    invoke-virtual {v0, p1, p2}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 187
    return-void
.end method
