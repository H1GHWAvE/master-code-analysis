.class public Lorg/tukaani/xz/common/EncoderUtil;
.super Lorg/tukaani/xz/common/Util;
.source "EncoderUtil.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 16
    invoke-direct {p0}, Lorg/tukaani/xz/common/Util;-><init>()V

    return-void
.end method

.method public static encodeVLI(Ljava/io/OutputStream;J)V
    .registers 7
    .param p0, "out"    # Ljava/io/OutputStream;
    .param p1, "num"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v2, 0x80

    .line 29
    :goto_2
    cmp-long v0, p1, v2

    if-ltz v0, :cond_10

    .line 30
    or-long v0, p1, v2

    long-to-int v0, v0

    int-to-byte v0, v0

    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write(I)V

    .line 31
    const/4 v0, 0x7

    ushr-long/2addr p1, v0

    goto :goto_2

    .line 34
    :cond_10
    long-to-int v0, p1

    int-to-byte v0, v0

    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write(I)V

    .line 35
    return-void
.end method

.method public static writeCRC32(Ljava/io/OutputStream;[B)V
    .registers 8
    .param p0, "out"    # Ljava/io/OutputStream;
    .param p1, "buf"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 19
    new-instance v0, Ljava/util/zip/CRC32;

    invoke-direct {v0}, Ljava/util/zip/CRC32;-><init>()V

    .line 20
    .local v0, "crc32":Ljava/util/zip/CRC32;
    invoke-virtual {v0, p1}, Ljava/util/zip/CRC32;->update([B)V

    .line 21
    invoke-virtual {v0}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide v2

    .line 23
    .local v2, "value":J
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_d
    const/4 v4, 0x4

    if-ge v1, v4, :cond_1c

    .line 24
    mul-int/lit8 v4, v1, 0x8

    ushr-long v4, v2, v4

    long-to-int v4, v4

    int-to-byte v4, v4

    invoke-virtual {p0, v4}, Ljava/io/OutputStream;->write(I)V

    .line 23
    add-int/lit8 v1, v1, 0x1

    goto :goto_d

    .line 25
    :cond_1c
    return-void
.end method
