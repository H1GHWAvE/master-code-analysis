.class Lorg/tukaani/xz/LZMA2OutputStream;
.super Lorg/tukaani/xz/FinishableOutputStream;
.source "LZMA2OutputStream.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field static final COMPRESSED_SIZE_MAX:I = 0x10000


# instance fields
.field private dictResetNeeded:Z

.field private exception:Ljava/io/IOException;

.field private finished:Z

.field private final lz:Lorg/tukaani/xz/lz/LZEncoder;

.field private final lzma:Lorg/tukaani/xz/lzma/LZMAEncoder;

.field private out:Lorg/tukaani/xz/FinishableOutputStream;

.field private final outData:Ljava/io/DataOutputStream;

.field private pendingSize:I

.field private final props:I

.field private propsNeeded:Z

.field private final rc:Lorg/tukaani/xz/rangecoder/RangeEncoder;

.field private stateResetNeeded:Z

.field private final tempBuf:[B


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 19
    const-class v0, Lorg/tukaani/xz/LZMA2OutputStream;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_c

    const/4 v0, 0x1

    :goto_9
    sput-boolean v0, Lorg/tukaani/xz/LZMA2OutputStream;->$assertionsDisabled:Z

    return-void

    :cond_c
    const/4 v0, 0x0

    goto :goto_9
.end method

.method constructor <init>(Lorg/tukaani/xz/FinishableOutputStream;Lorg/tukaani/xz/LZMA2Options;)V
    .registers 15
    .param p1, "out"    # Lorg/tukaani/xz/FinishableOutputStream;
    .param p2, "options"    # Lorg/tukaani/xz/LZMA2Options;

    .prologue
    const/4 v11, 0x0

    const/4 v1, 0x1

    .line 54
    invoke-direct {p0}, Lorg/tukaani/xz/FinishableOutputStream;-><init>()V

    .line 30
    iput-boolean v1, p0, Lorg/tukaani/xz/LZMA2OutputStream;->dictResetNeeded:Z

    .line 31
    iput-boolean v1, p0, Lorg/tukaani/xz/LZMA2OutputStream;->stateResetNeeded:Z

    .line 32
    iput-boolean v1, p0, Lorg/tukaani/xz/LZMA2OutputStream;->propsNeeded:Z

    .line 34
    iput v11, p0, Lorg/tukaani/xz/LZMA2OutputStream;->pendingSize:I

    .line 35
    iput-boolean v11, p0, Lorg/tukaani/xz/LZMA2OutputStream;->finished:Z

    .line 36
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/tukaani/xz/LZMA2OutputStream;->exception:Ljava/io/IOException;

    .line 38
    new-array v0, v1, [B

    iput-object v0, p0, Lorg/tukaani/xz/LZMA2OutputStream;->tempBuf:[B

    .line 55
    if-nez p1, :cond_1e

    .line 56
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 58
    :cond_1e
    iput-object p1, p0, Lorg/tukaani/xz/LZMA2OutputStream;->out:Lorg/tukaani/xz/FinishableOutputStream;

    .line 59
    new-instance v0, Ljava/io/DataOutputStream;

    invoke-direct {v0, p1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v0, p0, Lorg/tukaani/xz/LZMA2OutputStream;->outData:Ljava/io/DataOutputStream;

    .line 60
    new-instance v0, Lorg/tukaani/xz/rangecoder/RangeEncoder;

    const/high16 v1, 0x10000

    invoke-direct {v0, v1}, Lorg/tukaani/xz/rangecoder/RangeEncoder;-><init>(I)V

    iput-object v0, p0, Lorg/tukaani/xz/LZMA2OutputStream;->rc:Lorg/tukaani/xz/rangecoder/RangeEncoder;

    .line 62
    invoke-virtual {p2}, Lorg/tukaani/xz/LZMA2Options;->getDictSize()I

    move-result v5

    .line 63
    .local v5, "dictSize":I
    invoke-static {v5}, Lorg/tukaani/xz/LZMA2OutputStream;->getExtraSizeBefore(I)I

    move-result v6

    .line 64
    .local v6, "extraSizeBefore":I
    iget-object v0, p0, Lorg/tukaani/xz/LZMA2OutputStream;->rc:Lorg/tukaani/xz/rangecoder/RangeEncoder;

    .line 65
    invoke-virtual {p2}, Lorg/tukaani/xz/LZMA2Options;->getLc()I

    move-result v1

    invoke-virtual {p2}, Lorg/tukaani/xz/LZMA2Options;->getLp()I

    move-result v2

    invoke-virtual {p2}, Lorg/tukaani/xz/LZMA2Options;->getPb()I

    move-result v3

    .line 66
    invoke-virtual {p2}, Lorg/tukaani/xz/LZMA2Options;->getMode()I

    move-result v4

    .line 67
    invoke-virtual {p2}, Lorg/tukaani/xz/LZMA2Options;->getNiceLen()I

    move-result v7

    .line 68
    invoke-virtual {p2}, Lorg/tukaani/xz/LZMA2Options;->getMatchFinder()I

    move-result v8

    invoke-virtual {p2}, Lorg/tukaani/xz/LZMA2Options;->getDepthLimit()I

    move-result v9

    .line 64
    invoke-static/range {v0 .. v9}, Lorg/tukaani/xz/lzma/LZMAEncoder;->getInstance(Lorg/tukaani/xz/rangecoder/RangeEncoder;IIIIIIIII)Lorg/tukaani/xz/lzma/LZMAEncoder;

    move-result-object v0

    iput-object v0, p0, Lorg/tukaani/xz/LZMA2OutputStream;->lzma:Lorg/tukaani/xz/lzma/LZMAEncoder;

    .line 70
    iget-object v0, p0, Lorg/tukaani/xz/LZMA2OutputStream;->lzma:Lorg/tukaani/xz/lzma/LZMAEncoder;

    invoke-virtual {v0}, Lorg/tukaani/xz/lzma/LZMAEncoder;->getLZEncoder()Lorg/tukaani/xz/lz/LZEncoder;

    move-result-object v0

    iput-object v0, p0, Lorg/tukaani/xz/LZMA2OutputStream;->lz:Lorg/tukaani/xz/lz/LZEncoder;

    .line 72
    invoke-virtual {p2}, Lorg/tukaani/xz/LZMA2Options;->getPresetDict()[B

    move-result-object v10

    .line 73
    .local v10, "presetDict":[B
    if-eqz v10, :cond_74

    array-length v0, v10

    if-lez v0, :cond_74

    .line 74
    iget-object v0, p0, Lorg/tukaani/xz/LZMA2OutputStream;->lz:Lorg/tukaani/xz/lz/LZEncoder;

    invoke-virtual {v0, v5, v10}, Lorg/tukaani/xz/lz/LZEncoder;->setPresetDict(I[B)V

    .line 75
    iput-boolean v11, p0, Lorg/tukaani/xz/LZMA2OutputStream;->dictResetNeeded:Z

    .line 78
    :cond_74
    invoke-virtual {p2}, Lorg/tukaani/xz/LZMA2Options;->getPb()I

    move-result v0

    mul-int/lit8 v0, v0, 0x5

    invoke-virtual {p2}, Lorg/tukaani/xz/LZMA2Options;->getLp()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x9

    invoke-virtual {p2}, Lorg/tukaani/xz/LZMA2Options;->getLc()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lorg/tukaani/xz/LZMA2OutputStream;->props:I

    .line 79
    return-void
.end method

.method private static getExtraSizeBefore(I)I
    .registers 2
    .param p0, "dictSize"    # I

    .prologue
    const/high16 v0, 0x10000

    .line 41
    if-le v0, p0, :cond_6

    sub-int/2addr v0, p0

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method static getMemoryUsage(Lorg/tukaani/xz/LZMA2Options;)I
    .registers 5
    .param p0, "options"    # Lorg/tukaani/xz/LZMA2Options;

    .prologue
    .line 47
    invoke-virtual {p0}, Lorg/tukaani/xz/LZMA2Options;->getDictSize()I

    move-result v0

    .line 48
    .local v0, "dictSize":I
    invoke-static {v0}, Lorg/tukaani/xz/LZMA2OutputStream;->getExtraSizeBefore(I)I

    move-result v1

    .line 49
    .local v1, "extraSizeBefore":I
    invoke-virtual {p0}, Lorg/tukaani/xz/LZMA2Options;->getMode()I

    move-result v2

    .line 51
    invoke-virtual {p0}, Lorg/tukaani/xz/LZMA2Options;->getMatchFinder()I

    move-result v3

    .line 49
    invoke-static {v2, v0, v1, v3}, Lorg/tukaani/xz/lzma/LZMAEncoder;->getMemoryUsage(IIII)I

    move-result v2

    add-int/lit8 v2, v2, 0x46

    return v2
.end method

.method private writeChunk()V
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 113
    iget-object v2, p0, Lorg/tukaani/xz/LZMA2OutputStream;->rc:Lorg/tukaani/xz/rangecoder/RangeEncoder;

    invoke-virtual {v2}, Lorg/tukaani/xz/rangecoder/RangeEncoder;->finish()I

    move-result v0

    .line 114
    .local v0, "compressedSize":I
    iget-object v2, p0, Lorg/tukaani/xz/LZMA2OutputStream;->lzma:Lorg/tukaani/xz/lzma/LZMAEncoder;

    invoke-virtual {v2}, Lorg/tukaani/xz/lzma/LZMAEncoder;->getUncompressedSize()I

    move-result v1

    .line 116
    .local v1, "uncompressedSize":I
    sget-boolean v2, Lorg/tukaani/xz/LZMA2OutputStream;->$assertionsDisabled:Z

    if-nez v2, :cond_18

    if-gtz v0, :cond_18

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v0}, Ljava/lang/AssertionError;-><init>(I)V

    throw v2

    .line 117
    :cond_18
    sget-boolean v2, Lorg/tukaani/xz/LZMA2OutputStream;->$assertionsDisabled:Z

    if-nez v2, :cond_24

    if-gtz v1, :cond_24

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(I)V

    throw v2

    .line 121
    :cond_24
    add-int/lit8 v2, v0, 0x2

    if-ge v2, v1, :cond_3b

    .line 122
    invoke-direct {p0, v1, v0}, Lorg/tukaani/xz/LZMA2OutputStream;->writeLZMA(II)V

    .line 130
    :goto_2b
    iget v2, p0, Lorg/tukaani/xz/LZMA2OutputStream;->pendingSize:I

    sub-int/2addr v2, v1

    iput v2, p0, Lorg/tukaani/xz/LZMA2OutputStream;->pendingSize:I

    .line 131
    iget-object v2, p0, Lorg/tukaani/xz/LZMA2OutputStream;->lzma:Lorg/tukaani/xz/lzma/LZMAEncoder;

    invoke-virtual {v2}, Lorg/tukaani/xz/lzma/LZMAEncoder;->resetUncompressedSize()V

    .line 132
    iget-object v2, p0, Lorg/tukaani/xz/LZMA2OutputStream;->rc:Lorg/tukaani/xz/rangecoder/RangeEncoder;

    invoke-virtual {v2}, Lorg/tukaani/xz/rangecoder/RangeEncoder;->reset()V

    .line 133
    return-void

    .line 124
    :cond_3b
    iget-object v2, p0, Lorg/tukaani/xz/LZMA2OutputStream;->lzma:Lorg/tukaani/xz/lzma/LZMAEncoder;

    invoke-virtual {v2}, Lorg/tukaani/xz/lzma/LZMAEncoder;->reset()V

    .line 125
    iget-object v2, p0, Lorg/tukaani/xz/LZMA2OutputStream;->lzma:Lorg/tukaani/xz/lzma/LZMAEncoder;

    invoke-virtual {v2}, Lorg/tukaani/xz/lzma/LZMAEncoder;->getUncompressedSize()I

    move-result v1

    .line 126
    sget-boolean v2, Lorg/tukaani/xz/LZMA2OutputStream;->$assertionsDisabled:Z

    if-nez v2, :cond_52

    if-gtz v1, :cond_52

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v1}, Ljava/lang/AssertionError;-><init>(I)V

    throw v2

    .line 127
    :cond_52
    invoke-direct {p0, v1}, Lorg/tukaani/xz/LZMA2OutputStream;->writeUncompressed(I)V

    goto :goto_2b
.end method

.method private writeEndMarker()V
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 181
    sget-boolean v1, Lorg/tukaani/xz/LZMA2OutputStream;->$assertionsDisabled:Z

    if-nez v1, :cond_e

    iget-boolean v1, p0, Lorg/tukaani/xz/LZMA2OutputStream;->finished:Z

    if-eqz v1, :cond_e

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 183
    :cond_e
    iget-object v1, p0, Lorg/tukaani/xz/LZMA2OutputStream;->exception:Ljava/io/IOException;

    if-eqz v1, :cond_15

    .line 184
    iget-object v1, p0, Lorg/tukaani/xz/LZMA2OutputStream;->exception:Ljava/io/IOException;

    throw v1

    .line 186
    :cond_15
    iget-object v1, p0, Lorg/tukaani/xz/LZMA2OutputStream;->lz:Lorg/tukaani/xz/lz/LZEncoder;

    invoke-virtual {v1}, Lorg/tukaani/xz/lz/LZEncoder;->setFinishing()V

    .line 189
    :goto_1a
    :try_start_1a
    iget v1, p0, Lorg/tukaani/xz/LZMA2OutputStream;->pendingSize:I

    if-lez v1, :cond_2b

    .line 190
    iget-object v1, p0, Lorg/tukaani/xz/LZMA2OutputStream;->lzma:Lorg/tukaani/xz/lzma/LZMAEncoder;

    invoke-virtual {v1}, Lorg/tukaani/xz/lzma/LZMAEncoder;->encodeForLZMA2()Z

    .line 191
    invoke-direct {p0}, Lorg/tukaani/xz/LZMA2OutputStream;->writeChunk()V
    :try_end_26
    .catch Ljava/io/IOException; {:try_start_1a .. :try_end_26} :catch_27

    goto :goto_1a

    .line 195
    :catch_27
    move-exception v0

    .line 196
    .local v0, "e":Ljava/io/IOException;
    iput-object v0, p0, Lorg/tukaani/xz/LZMA2OutputStream;->exception:Ljava/io/IOException;

    .line 197
    throw v0

    .line 194
    .end local v0    # "e":Ljava/io/IOException;
    :cond_2b
    :try_start_2b
    iget-object v1, p0, Lorg/tukaani/xz/LZMA2OutputStream;->out:Lorg/tukaani/xz/FinishableOutputStream;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lorg/tukaani/xz/FinishableOutputStream;->write(I)V
    :try_end_31
    .catch Ljava/io/IOException; {:try_start_2b .. :try_end_31} :catch_27

    .line 200
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/tukaani/xz/LZMA2OutputStream;->finished:Z

    .line 201
    return-void
.end method

.method private writeLZMA(II)V
    .registers 7
    .param p1, "uncompressedSize"    # I
    .param p2, "compressedSize"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 139
    iget-boolean v1, p0, Lorg/tukaani/xz/LZMA2OutputStream;->propsNeeded:Z

    if-eqz v1, :cond_3f

    .line 140
    iget-boolean v1, p0, Lorg/tukaani/xz/LZMA2OutputStream;->dictResetNeeded:Z

    if-eqz v1, :cond_3c

    .line 141
    const/16 v0, 0xe0

    .line 151
    .local v0, "control":I
    :goto_b
    add-int/lit8 v1, p1, -0x1

    ushr-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    .line 152
    iget-object v1, p0, Lorg/tukaani/xz/LZMA2OutputStream;->outData:Ljava/io/DataOutputStream;

    invoke-virtual {v1, v0}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 154
    iget-object v1, p0, Lorg/tukaani/xz/LZMA2OutputStream;->outData:Ljava/io/DataOutputStream;

    add-int/lit8 v2, p1, -0x1

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 155
    iget-object v1, p0, Lorg/tukaani/xz/LZMA2OutputStream;->outData:Ljava/io/DataOutputStream;

    add-int/lit8 v2, p2, -0x1

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 157
    iget-boolean v1, p0, Lorg/tukaani/xz/LZMA2OutputStream;->propsNeeded:Z

    if-eqz v1, :cond_2e

    .line 158
    iget-object v1, p0, Lorg/tukaani/xz/LZMA2OutputStream;->outData:Ljava/io/DataOutputStream;

    iget v2, p0, Lorg/tukaani/xz/LZMA2OutputStream;->props:I

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 160
    :cond_2e
    iget-object v1, p0, Lorg/tukaani/xz/LZMA2OutputStream;->rc:Lorg/tukaani/xz/rangecoder/RangeEncoder;

    iget-object v2, p0, Lorg/tukaani/xz/LZMA2OutputStream;->out:Lorg/tukaani/xz/FinishableOutputStream;

    invoke-virtual {v1, v2}, Lorg/tukaani/xz/rangecoder/RangeEncoder;->write(Ljava/io/OutputStream;)V

    .line 162
    iput-boolean v3, p0, Lorg/tukaani/xz/LZMA2OutputStream;->propsNeeded:Z

    .line 163
    iput-boolean v3, p0, Lorg/tukaani/xz/LZMA2OutputStream;->stateResetNeeded:Z

    .line 164
    iput-boolean v3, p0, Lorg/tukaani/xz/LZMA2OutputStream;->dictResetNeeded:Z

    .line 165
    return-void

    .line 143
    .end local v0    # "control":I
    :cond_3c
    const/16 v0, 0xc0

    .restart local v0    # "control":I
    goto :goto_b

    .line 145
    .end local v0    # "control":I
    :cond_3f
    iget-boolean v1, p0, Lorg/tukaani/xz/LZMA2OutputStream;->stateResetNeeded:Z

    if-eqz v1, :cond_46

    .line 146
    const/16 v0, 0xa0

    .restart local v0    # "control":I
    goto :goto_b

    .line 148
    .end local v0    # "control":I
    :cond_46
    const/16 v0, 0x80

    .restart local v0    # "control":I
    goto :goto_b
.end method

.method private writeUncompressed(I)V
    .registers 6
    .param p1, "uncompressedSize"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 168
    :goto_1
    if-lez p1, :cond_28

    .line 169
    const/high16 v1, 0x10000

    invoke-static {p1, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 170
    .local v0, "chunkSize":I
    iget-object v3, p0, Lorg/tukaani/xz/LZMA2OutputStream;->outData:Ljava/io/DataOutputStream;

    iget-boolean v1, p0, Lorg/tukaani/xz/LZMA2OutputStream;->dictResetNeeded:Z

    if-eqz v1, :cond_26

    move v1, v2

    :goto_10
    invoke-virtual {v3, v1}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 171
    iget-object v1, p0, Lorg/tukaani/xz/LZMA2OutputStream;->outData:Ljava/io/DataOutputStream;

    add-int/lit8 v3, v0, -0x1

    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 172
    iget-object v1, p0, Lorg/tukaani/xz/LZMA2OutputStream;->lz:Lorg/tukaani/xz/lz/LZEncoder;

    iget-object v3, p0, Lorg/tukaani/xz/LZMA2OutputStream;->out:Lorg/tukaani/xz/FinishableOutputStream;

    invoke-virtual {v1, v3, p1, v0}, Lorg/tukaani/xz/lz/LZEncoder;->copyUncompressed(Ljava/io/OutputStream;II)V

    .line 173
    sub-int/2addr p1, v0

    .line 174
    const/4 v1, 0x0

    iput-boolean v1, p0, Lorg/tukaani/xz/LZMA2OutputStream;->dictResetNeeded:Z

    goto :goto_1

    .line 170
    :cond_26
    const/4 v1, 0x2

    goto :goto_10

    .line 177
    .end local v0    # "chunkSize":I
    :cond_28
    iput-boolean v2, p0, Lorg/tukaani/xz/LZMA2OutputStream;->stateResetNeeded:Z

    .line 178
    return-void
.end method


# virtual methods
.method public close()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 241
    iget-object v1, p0, Lorg/tukaani/xz/LZMA2OutputStream;->out:Lorg/tukaani/xz/FinishableOutputStream;

    if-eqz v1, :cond_13

    .line 242
    iget-boolean v1, p0, Lorg/tukaani/xz/LZMA2OutputStream;->finished:Z

    if-nez v1, :cond_b

    .line 244
    :try_start_8
    invoke-direct {p0}, Lorg/tukaani/xz/LZMA2OutputStream;->writeEndMarker()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_b} :catch_22

    .line 249
    :cond_b
    :goto_b
    :try_start_b
    iget-object v1, p0, Lorg/tukaani/xz/LZMA2OutputStream;->out:Lorg/tukaani/xz/FinishableOutputStream;

    invoke-virtual {v1}, Lorg/tukaani/xz/FinishableOutputStream;->close()V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_10} :catch_1a

    .line 255
    :cond_10
    :goto_10
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/tukaani/xz/LZMA2OutputStream;->out:Lorg/tukaani/xz/FinishableOutputStream;

    .line 258
    :cond_13
    iget-object v1, p0, Lorg/tukaani/xz/LZMA2OutputStream;->exception:Ljava/io/IOException;

    if-eqz v1, :cond_24

    .line 259
    iget-object v1, p0, Lorg/tukaani/xz/LZMA2OutputStream;->exception:Ljava/io/IOException;

    throw v1

    .line 250
    :catch_1a
    move-exception v0

    .line 251
    .local v0, "e":Ljava/io/IOException;
    iget-object v1, p0, Lorg/tukaani/xz/LZMA2OutputStream;->exception:Ljava/io/IOException;

    if-nez v1, :cond_10

    .line 252
    iput-object v0, p0, Lorg/tukaani/xz/LZMA2OutputStream;->exception:Ljava/io/IOException;

    goto :goto_10

    .line 245
    .end local v0    # "e":Ljava/io/IOException;
    :catch_22
    move-exception v1

    goto :goto_b

    .line 260
    :cond_24
    return-void
.end method

.method public finish()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 226
    iget-boolean v1, p0, Lorg/tukaani/xz/LZMA2OutputStream;->finished:Z

    if-nez v1, :cond_f

    .line 227
    invoke-direct {p0}, Lorg/tukaani/xz/LZMA2OutputStream;->writeEndMarker()V

    .line 230
    :try_start_7
    iget-object v1, p0, Lorg/tukaani/xz/LZMA2OutputStream;->out:Lorg/tukaani/xz/FinishableOutputStream;

    invoke-virtual {v1}, Lorg/tukaani/xz/FinishableOutputStream;->finish()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_c} :catch_10

    .line 236
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/tukaani/xz/LZMA2OutputStream;->finished:Z

    .line 238
    :cond_f
    return-void

    .line 231
    :catch_10
    move-exception v0

    .line 232
    .local v0, "e":Ljava/io/IOException;
    iput-object v0, p0, Lorg/tukaani/xz/LZMA2OutputStream;->exception:Ljava/io/IOException;

    .line 233
    throw v0
.end method

.method public flush()V
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 204
    iget-object v1, p0, Lorg/tukaani/xz/LZMA2OutputStream;->exception:Ljava/io/IOException;

    if-eqz v1, :cond_7

    .line 205
    iget-object v1, p0, Lorg/tukaani/xz/LZMA2OutputStream;->exception:Ljava/io/IOException;

    throw v1

    .line 207
    :cond_7
    iget-boolean v1, p0, Lorg/tukaani/xz/LZMA2OutputStream;->finished:Z

    if-eqz v1, :cond_13

    .line 208
    new-instance v1, Lorg/tukaani/xz/XZIOException;

    const-string v2, "Stream finished or closed"

    invoke-direct {v1, v2}, Lorg/tukaani/xz/XZIOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 211
    :cond_13
    :try_start_13
    iget-object v1, p0, Lorg/tukaani/xz/LZMA2OutputStream;->lz:Lorg/tukaani/xz/lz/LZEncoder;

    invoke-virtual {v1}, Lorg/tukaani/xz/lz/LZEncoder;->setFlushing()V

    .line 213
    :goto_18
    iget v1, p0, Lorg/tukaani/xz/LZMA2OutputStream;->pendingSize:I

    if-lez v1, :cond_29

    .line 214
    iget-object v1, p0, Lorg/tukaani/xz/LZMA2OutputStream;->lzma:Lorg/tukaani/xz/lzma/LZMAEncoder;

    invoke-virtual {v1}, Lorg/tukaani/xz/lzma/LZMAEncoder;->encodeForLZMA2()Z

    .line 215
    invoke-direct {p0}, Lorg/tukaani/xz/LZMA2OutputStream;->writeChunk()V
    :try_end_24
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_24} :catch_25

    goto :goto_18

    .line 219
    :catch_25
    move-exception v0

    .line 220
    .local v0, "e":Ljava/io/IOException;
    iput-object v0, p0, Lorg/tukaani/xz/LZMA2OutputStream;->exception:Ljava/io/IOException;

    .line 221
    throw v0

    .line 218
    .end local v0    # "e":Ljava/io/IOException;
    :cond_29
    :try_start_29
    iget-object v1, p0, Lorg/tukaani/xz/LZMA2OutputStream;->out:Lorg/tukaani/xz/FinishableOutputStream;

    invoke-virtual {v1}, Lorg/tukaani/xz/FinishableOutputStream;->flush()V
    :try_end_2e
    .catch Ljava/io/IOException; {:try_start_29 .. :try_end_2e} :catch_25

    .line 223
    return-void
.end method

.method public write(I)V
    .registers 5
    .param p1, "b"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 82
    iget-object v0, p0, Lorg/tukaani/xz/LZMA2OutputStream;->tempBuf:[B

    int-to-byte v1, p1

    aput-byte v1, v0, v2

    .line 83
    iget-object v0, p0, Lorg/tukaani/xz/LZMA2OutputStream;->tempBuf:[B

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v2, v1}, Lorg/tukaani/xz/LZMA2OutputStream;->write([BII)V

    .line 84
    return-void
.end method

.method public write([BII)V
    .registers 8
    .param p1, "buf"    # [B
    .param p2, "off"    # I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 87
    if-ltz p2, :cond_d

    if-ltz p3, :cond_d

    add-int v2, p2, p3

    if-ltz v2, :cond_d

    add-int v2, p2, p3

    array-length v3, p1

    if-le v2, v3, :cond_13

    .line 88
    :cond_d
    new-instance v2, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v2}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v2

    .line 90
    :cond_13
    iget-object v2, p0, Lorg/tukaani/xz/LZMA2OutputStream;->exception:Ljava/io/IOException;

    if-eqz v2, :cond_1a

    .line 91
    iget-object v2, p0, Lorg/tukaani/xz/LZMA2OutputStream;->exception:Ljava/io/IOException;

    throw v2

    .line 93
    :cond_1a
    iget-boolean v2, p0, Lorg/tukaani/xz/LZMA2OutputStream;->finished:Z

    if-eqz v2, :cond_26

    .line 94
    new-instance v2, Lorg/tukaani/xz/XZIOException;

    const-string v3, "Stream finished or closed"

    invoke-direct {v2, v3}, Lorg/tukaani/xz/XZIOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 97
    :cond_26
    :goto_26
    if-lez p3, :cond_45

    .line 98
    :try_start_28
    iget-object v2, p0, Lorg/tukaani/xz/LZMA2OutputStream;->lz:Lorg/tukaani/xz/lz/LZEncoder;

    invoke-virtual {v2, p1, p2, p3}, Lorg/tukaani/xz/lz/LZEncoder;->fillWindow([BII)I

    move-result v1

    .line 99
    .local v1, "used":I
    add-int/2addr p2, v1

    .line 100
    sub-int/2addr p3, v1

    .line 101
    iget v2, p0, Lorg/tukaani/xz/LZMA2OutputStream;->pendingSize:I

    add-int/2addr v2, v1

    iput v2, p0, Lorg/tukaani/xz/LZMA2OutputStream;->pendingSize:I

    .line 103
    iget-object v2, p0, Lorg/tukaani/xz/LZMA2OutputStream;->lzma:Lorg/tukaani/xz/lzma/LZMAEncoder;

    invoke-virtual {v2}, Lorg/tukaani/xz/lzma/LZMAEncoder;->encodeForLZMA2()Z

    move-result v2

    if-eqz v2, :cond_26

    .line 104
    invoke-direct {p0}, Lorg/tukaani/xz/LZMA2OutputStream;->writeChunk()V
    :try_end_40
    .catch Ljava/io/IOException; {:try_start_28 .. :try_end_40} :catch_41

    goto :goto_26

    .line 106
    .end local v1    # "used":I
    :catch_41
    move-exception v0

    .line 107
    .local v0, "e":Ljava/io/IOException;
    iput-object v0, p0, Lorg/tukaani/xz/LZMA2OutputStream;->exception:Ljava/io/IOException;

    .line 108
    throw v0

    .line 110
    .end local v0    # "e":Ljava/io/IOException;
    :cond_45
    return-void
.end method
