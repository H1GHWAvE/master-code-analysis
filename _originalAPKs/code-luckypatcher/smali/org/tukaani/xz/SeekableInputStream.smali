.class public abstract Lorg/tukaani/xz/SeekableInputStream;
.super Ljava/io/InputStream;
.source "SeekableInputStream.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract length()J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract position()J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract seek(J)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public skip(J)J
    .registers 10
    .param p1, "n"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    .line 42
    cmp-long v6, p1, v4

    if-gtz v6, :cond_7

    .line 54
    :cond_6
    :goto_6
    return-wide v4

    .line 45
    :cond_7
    invoke-virtual {p0}, Lorg/tukaani/xz/SeekableInputStream;->length()J

    move-result-wide v2

    .line 46
    .local v2, "size":J
    invoke-virtual {p0}, Lorg/tukaani/xz/SeekableInputStream;->position()J

    move-result-wide v0

    .line 47
    .local v0, "pos":J
    cmp-long v6, v0, v2

    if-gez v6, :cond_6

    .line 50
    sub-long v4, v2, v0

    cmp-long v4, v4, p1

    if-gez v4, :cond_1b

    .line 51
    sub-long p1, v2, v0

    .line 53
    :cond_1b
    add-long v4, v0, p1

    invoke-virtual {p0, v4, v5}, Lorg/tukaani/xz/SeekableInputStream;->seek(J)V

    move-wide v4, p1

    .line 54
    goto :goto_6
.end method
