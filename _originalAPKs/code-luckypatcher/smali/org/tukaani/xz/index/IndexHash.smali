.class public Lorg/tukaani/xz/index/IndexHash;
.super Lorg/tukaani/xz/index/IndexBase;
.source "IndexHash.java"


# instance fields
.field private hash:Lorg/tukaani/xz/check/Check;


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    .line 26
    new-instance v1, Lorg/tukaani/xz/CorruptedInputException;

    invoke-direct {v1}, Lorg/tukaani/xz/CorruptedInputException;-><init>()V

    invoke-direct {p0, v1}, Lorg/tukaani/xz/index/IndexBase;-><init>(Lorg/tukaani/xz/XZIOException;)V

    .line 29
    :try_start_8
    new-instance v1, Lorg/tukaani/xz/check/SHA256;

    invoke-direct {v1}, Lorg/tukaani/xz/check/SHA256;-><init>()V

    iput-object v1, p0, Lorg/tukaani/xz/index/IndexHash;->hash:Lorg/tukaani/xz/check/Check;
    :try_end_f
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_8 .. :try_end_f} :catch_10

    .line 33
    :goto_f
    return-void

    .line 30
    :catch_10
    move-exception v0

    .line 31
    .local v0, "e":Ljava/security/NoSuchAlgorithmException;
    new-instance v1, Lorg/tukaani/xz/check/CRC32;

    invoke-direct {v1}, Lorg/tukaani/xz/check/CRC32;-><init>()V

    iput-object v1, p0, Lorg/tukaani/xz/index/IndexHash;->hash:Lorg/tukaani/xz/check/Check;

    goto :goto_f
.end method


# virtual methods
.method public add(JJ)V
    .registers 8
    .param p1, "unpaddedSize"    # J
    .param p3, "uncompressedSize"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/tukaani/xz/XZIOException;
        }
    .end annotation

    .prologue
    .line 37
    invoke-super {p0, p1, p2, p3, p4}, Lorg/tukaani/xz/index/IndexBase;->add(JJ)V

    .line 39
    const/16 v1, 0x10

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 40
    .local v0, "buf":Ljava/nio/ByteBuffer;
    invoke-virtual {v0, p1, p2}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    .line 41
    invoke-virtual {v0, p3, p4}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    .line 42
    iget-object v1, p0, Lorg/tukaani/xz/index/IndexHash;->hash:Lorg/tukaani/xz/check/Check;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/tukaani/xz/check/Check;->update([B)V

    .line 43
    return-void
.end method

.method public bridge synthetic getIndexSize()J
    .registers 3

    .prologue
    .line 22
    invoke-super {p0}, Lorg/tukaani/xz/index/IndexBase;->getIndexSize()J

    move-result-wide v0

    return-wide v0
.end method

.method public bridge synthetic getStreamSize()J
    .registers 3

    .prologue
    .line 22
    invoke-super {p0}, Lorg/tukaani/xz/index/IndexBase;->getStreamSize()J

    move-result-wide v0

    return-wide v0
.end method

.method public validate(Ljava/io/InputStream;)V
    .registers 23
    .param p1, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 48
    new-instance v2, Ljava/util/zip/CRC32;

    invoke-direct {v2}, Ljava/util/zip/CRC32;-><init>()V

    .line 49
    .local v2, "crc32":Ljava/util/zip/CRC32;
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/util/zip/CRC32;->update(I)V

    .line 50
    new-instance v6, Ljava/util/zip/CheckedInputStream;

    move-object/from16 v0, p1

    invoke-direct {v6, v0, v2}, Ljava/util/zip/CheckedInputStream;-><init>(Ljava/io/InputStream;Ljava/util/zip/Checksum;)V

    .line 53
    .local v6, "inChecked":Ljava/util/zip/CheckedInputStream;
    invoke-static {v6}, Lorg/tukaani/xz/common/DecoderUtil;->decodeVLI(Ljava/io/InputStream;)J

    move-result-wide v9

    .line 54
    .local v9, "storedRecordCount":J
    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/tukaani/xz/index/IndexHash;->recordCount:J

    move-wide/from16 v17, v0

    cmp-long v17, v9, v17

    if-eqz v17, :cond_29

    .line 55
    new-instance v17, Lorg/tukaani/xz/CorruptedInputException;

    const-string v18, "XZ Index is corrupt"

    invoke-direct/range {v17 .. v18}, Lorg/tukaani/xz/CorruptedInputException;-><init>(Ljava/lang/String;)V

    throw v17

    .line 59
    :cond_29
    new-instance v8, Lorg/tukaani/xz/index/IndexHash;

    invoke-direct {v8}, Lorg/tukaani/xz/index/IndexHash;-><init>()V

    .line 60
    .local v8, "stored":Lorg/tukaani/xz/index/IndexHash;
    const-wide/16 v4, 0x0

    .local v4, "i":J
    :goto_30
    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/tukaani/xz/index/IndexHash;->recordCount:J

    move-wide/from16 v17, v0

    cmp-long v17, v4, v17

    if-gez v17, :cond_85

    .line 61
    invoke-static {v6}, Lorg/tukaani/xz/common/DecoderUtil;->decodeVLI(Ljava/io/InputStream;)J

    move-result-wide v13

    .line 62
    .local v13, "unpaddedSize":J
    invoke-static {v6}, Lorg/tukaani/xz/common/DecoderUtil;->decodeVLI(Ljava/io/InputStream;)J

    move-result-wide v11

    .line 65
    .local v11, "uncompressedSize":J
    :try_start_42
    invoke-virtual {v8, v13, v14, v11, v12}, Lorg/tukaani/xz/index/IndexHash;->add(JJ)V
    :try_end_45
    .catch Lorg/tukaani/xz/XZIOException; {:try_start_42 .. :try_end_45} :catch_77

    .line 70
    iget-wide v0, v8, Lorg/tukaani/xz/index/IndexHash;->blocksSum:J

    move-wide/from16 v17, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/tukaani/xz/index/IndexHash;->blocksSum:J

    move-wide/from16 v19, v0

    cmp-long v17, v17, v19

    if-gtz v17, :cond_6f

    iget-wide v0, v8, Lorg/tukaani/xz/index/IndexHash;->uncompressedSum:J

    move-wide/from16 v17, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/tukaani/xz/index/IndexHash;->uncompressedSum:J

    move-wide/from16 v19, v0

    cmp-long v17, v17, v19

    if-gtz v17, :cond_6f

    iget-wide v0, v8, Lorg/tukaani/xz/index/IndexHash;->indexListSize:J

    move-wide/from16 v17, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/tukaani/xz/index/IndexHash;->indexListSize:J

    move-wide/from16 v19, v0

    cmp-long v17, v17, v19

    if-lez v17, :cond_80

    .line 73
    :cond_6f
    new-instance v17, Lorg/tukaani/xz/CorruptedInputException;

    const-string v18, "XZ Index is corrupt"

    invoke-direct/range {v17 .. v18}, Lorg/tukaani/xz/CorruptedInputException;-><init>(Ljava/lang/String;)V

    throw v17

    .line 66
    :catch_77
    move-exception v3

    .line 67
    .local v3, "e":Lorg/tukaani/xz/XZIOException;
    new-instance v17, Lorg/tukaani/xz/CorruptedInputException;

    const-string v18, "XZ Index is corrupt"

    invoke-direct/range {v17 .. v18}, Lorg/tukaani/xz/CorruptedInputException;-><init>(Ljava/lang/String;)V

    throw v17

    .line 60
    .end local v3    # "e":Lorg/tukaani/xz/XZIOException;
    :cond_80
    const-wide/16 v17, 0x1

    add-long v4, v4, v17

    goto :goto_30

    .line 76
    .end local v11    # "uncompressedSize":J
    .end local v13    # "unpaddedSize":J
    :cond_85
    iget-wide v0, v8, Lorg/tukaani/xz/index/IndexHash;->blocksSum:J

    move-wide/from16 v17, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/tukaani/xz/index/IndexHash;->blocksSum:J

    move-wide/from16 v19, v0

    cmp-long v17, v17, v19

    if-nez v17, :cond_c7

    iget-wide v0, v8, Lorg/tukaani/xz/index/IndexHash;->uncompressedSum:J

    move-wide/from16 v17, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/tukaani/xz/index/IndexHash;->uncompressedSum:J

    move-wide/from16 v19, v0

    cmp-long v17, v17, v19

    if-nez v17, :cond_c7

    iget-wide v0, v8, Lorg/tukaani/xz/index/IndexHash;->indexListSize:J

    move-wide/from16 v17, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/tukaani/xz/index/IndexHash;->indexListSize:J

    move-wide/from16 v19, v0

    cmp-long v17, v17, v19

    if-nez v17, :cond_c7

    iget-object v0, v8, Lorg/tukaani/xz/index/IndexHash;->hash:Lorg/tukaani/xz/check/Check;

    move-object/from16 v17, v0

    .line 79
    invoke-virtual/range {v17 .. v17}, Lorg/tukaani/xz/check/Check;->finish()[B

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/tukaani/xz/index/IndexHash;->hash:Lorg/tukaani/xz/check/Check;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lorg/tukaani/xz/check/Check;->finish()[B

    move-result-object v18

    invoke-static/range {v17 .. v18}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v17

    if-nez v17, :cond_cf

    .line 80
    :cond_c7
    new-instance v17, Lorg/tukaani/xz/CorruptedInputException;

    const-string v18, "XZ Index is corrupt"

    invoke-direct/range {v17 .. v18}, Lorg/tukaani/xz/CorruptedInputException;-><init>(Ljava/lang/String;)V

    throw v17

    .line 83
    :cond_cf
    new-instance v7, Ljava/io/DataInputStream;

    invoke-direct {v7, v6}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 84
    .local v7, "inData":Ljava/io/DataInputStream;
    invoke-virtual/range {p0 .. p0}, Lorg/tukaani/xz/index/IndexHash;->getIndexPaddingSize()I

    move-result v4

    .local v4, "i":I
    :goto_d8
    if-lez v4, :cond_eb

    .line 85
    invoke-virtual {v7}, Ljava/io/DataInputStream;->readUnsignedByte()I

    move-result v17

    if-eqz v17, :cond_e8

    .line 86
    new-instance v17, Lorg/tukaani/xz/CorruptedInputException;

    const-string v18, "XZ Index is corrupt"

    invoke-direct/range {v17 .. v18}, Lorg/tukaani/xz/CorruptedInputException;-><init>(Ljava/lang/String;)V

    throw v17

    .line 84
    :cond_e8
    add-int/lit8 v4, v4, -0x1

    goto :goto_d8

    .line 89
    :cond_eb
    invoke-virtual {v2}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide v15

    .line 90
    .local v15, "value":J
    const/4 v4, 0x0

    :goto_f0
    const/16 v17, 0x4

    move/from16 v0, v17

    if-ge v4, v0, :cond_116

    .line 91
    mul-int/lit8 v17, v4, 0x8

    ushr-long v17, v15, v17

    const-wide/16 v19, 0xff

    and-long v17, v17, v19

    invoke-virtual {v7}, Ljava/io/DataInputStream;->readUnsignedByte()I

    move-result v19

    move/from16 v0, v19

    int-to-long v0, v0

    move-wide/from16 v19, v0

    cmp-long v17, v17, v19

    if-eqz v17, :cond_113

    .line 92
    new-instance v17, Lorg/tukaani/xz/CorruptedInputException;

    const-string v18, "XZ Index is corrupt"

    invoke-direct/range {v17 .. v18}, Lorg/tukaani/xz/CorruptedInputException;-><init>(Ljava/lang/String;)V

    throw v17

    .line 90
    :cond_113
    add-int/lit8 v4, v4, 0x1

    goto :goto_f0

    .line 93
    :cond_116
    return-void
.end method
