.class public Lorg/tukaani/xz/index/IndexDecoder;
.super Lorg/tukaani/xz/index/IndexBase;
.source "IndexDecoder.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private compressedOffset:J

.field private largestBlockSize:J

.field private final memoryUsage:I

.field private recordOffset:I

.field private final streamFlags:Lorg/tukaani/xz/common/StreamFlags;

.field private final streamPadding:J

.field private final uncompressed:[J

.field private uncompressedOffset:J

.field private final unpadded:[J


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 22
    const-class v0, Lorg/tukaani/xz/index/IndexDecoder;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_c

    const/4 v0, 0x1

    :goto_9
    sput-boolean v0, Lorg/tukaani/xz/index/IndexDecoder;->$assertionsDisabled:Z

    return-void

    :cond_c
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public constructor <init>(Lorg/tukaani/xz/SeekableInputStream;Lorg/tukaani/xz/common/StreamFlags;JI)V
    .registers 30
    .param p1, "in"    # Lorg/tukaani/xz/SeekableInputStream;
    .param p2, "streamFooterFlags"    # Lorg/tukaani/xz/common/StreamFlags;
    .param p3, "streamPadding"    # J
    .param p5, "memoryLimit"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 44
    new-instance v20, Lorg/tukaani/xz/CorruptedInputException;

    const-string v21, "XZ Index is corrupt"

    invoke-direct/range {v20 .. v21}, Lorg/tukaani/xz/CorruptedInputException;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lorg/tukaani/xz/index/IndexBase;-><init>(Lorg/tukaani/xz/XZIOException;)V

    .line 33
    const-wide/16 v20, 0x0

    move-wide/from16 v0, v20

    move-object/from16 v2, p0

    iput-wide v0, v2, Lorg/tukaani/xz/index/IndexDecoder;->largestBlockSize:J

    .line 37
    const/16 v20, 0x0

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tukaani/xz/index/IndexDecoder;->recordOffset:I

    .line 38
    const-wide/16 v20, 0x0

    move-wide/from16 v0, v20

    move-object/from16 v2, p0

    iput-wide v0, v2, Lorg/tukaani/xz/index/IndexDecoder;->compressedOffset:J

    .line 39
    const-wide/16 v20, 0x0

    move-wide/from16 v0, v20

    move-object/from16 v2, p0

    iput-wide v0, v2, Lorg/tukaani/xz/index/IndexDecoder;->uncompressedOffset:J

    .line 45
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/tukaani/xz/index/IndexDecoder;->streamFlags:Lorg/tukaani/xz/common/StreamFlags;

    .line 46
    move-wide/from16 v0, p3

    move-object/from16 v2, p0

    iput-wide v0, v2, Lorg/tukaani/xz/index/IndexDecoder;->streamPadding:J

    .line 50
    invoke-virtual/range {p1 .. p1}, Lorg/tukaani/xz/SeekableInputStream;->position()J

    move-result-wide v20

    move-object/from16 v0, p2

    iget-wide v0, v0, Lorg/tukaani/xz/common/StreamFlags;->backwardSize:J

    move-wide/from16 v22, v0

    add-long v20, v20, v22

    const-wide/16 v22, 0x4

    sub-long v7, v20, v22

    .line 52
    .local v7, "endPos":J
    new-instance v5, Ljava/util/zip/CRC32;

    invoke-direct {v5}, Ljava/util/zip/CRC32;-><init>()V

    .line 53
    .local v5, "crc32":Ljava/util/zip/CRC32;
    new-instance v10, Ljava/util/zip/CheckedInputStream;

    move-object/from16 v0, p1

    invoke-direct {v10, v0, v5}, Ljava/util/zip/CheckedInputStream;-><init>(Ljava/io/InputStream;Ljava/util/zip/Checksum;)V

    .line 56
    .local v10, "inChecked":Ljava/util/zip/CheckedInputStream;
    invoke-virtual {v10}, Ljava/util/zip/CheckedInputStream;->read()I

    move-result v20

    if-eqz v20, :cond_64

    .line 57
    new-instance v20, Lorg/tukaani/xz/CorruptedInputException;

    const-string v21, "XZ Index is corrupt"

    invoke-direct/range {v20 .. v21}, Lorg/tukaani/xz/CorruptedInputException;-><init>(Ljava/lang/String;)V

    throw v20

    .line 61
    :cond_64
    :try_start_64
    invoke-static {v10}, Lorg/tukaani/xz/common/DecoderUtil;->decodeVLI(Ljava/io/InputStream;)J

    move-result-wide v3

    .line 67
    .local v3, "count":J
    move-object/from16 v0, p2

    iget-wide v0, v0, Lorg/tukaani/xz/common/StreamFlags;->backwardSize:J

    move-wide/from16 v20, v0

    const-wide/16 v22, 0x2

    div-long v20, v20, v22

    cmp-long v20, v3, v20

    if-ltz v20, :cond_87

    .line 68
    new-instance v20, Lorg/tukaani/xz/CorruptedInputException;

    const-string v21, "XZ Index is corrupt"

    invoke-direct/range {v20 .. v21}, Lorg/tukaani/xz/CorruptedInputException;-><init>(Ljava/lang/String;)V

    throw v20
    :try_end_7e
    .catch Ljava/io/EOFException; {:try_start_64 .. :try_end_7e} :catch_7e

    .line 111
    .end local v3    # "count":J
    :catch_7e
    move-exception v6

    .line 114
    .local v6, "e":Ljava/io/EOFException;
    new-instance v20, Lorg/tukaani/xz/CorruptedInputException;

    const-string v21, "XZ Index is corrupt"

    invoke-direct/range {v20 .. v21}, Lorg/tukaani/xz/CorruptedInputException;-><init>(Ljava/lang/String;)V

    throw v20

    .line 72
    .end local v6    # "e":Ljava/io/EOFException;
    .restart local v3    # "count":J
    :cond_87
    const-wide/32 v20, 0x7fffffff

    cmp-long v20, v3, v20

    if-lez v20, :cond_96

    .line 73
    :try_start_8e
    new-instance v20, Lorg/tukaani/xz/UnsupportedOptionsException;

    const-string v21, "XZ Index has over 2147483647 Records"

    invoke-direct/range {v20 .. v21}, Lorg/tukaani/xz/UnsupportedOptionsException;-><init>(Ljava/lang/String;)V

    throw v20

    .line 78
    :cond_96
    const-wide/16 v20, 0x10

    mul-long v20, v20, v3

    const-wide/16 v22, 0x3ff

    add-long v20, v20, v22

    const-wide/16 v22, 0x400

    div-long v20, v20, v22

    move-wide/from16 v0, v20

    long-to-int v0, v0

    move/from16 v20, v0

    add-int/lit8 v20, v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tukaani/xz/index/IndexDecoder;->memoryUsage:I

    .line 79
    if-ltz p5, :cond_cf

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tukaani/xz/index/IndexDecoder;->memoryUsage:I

    move/from16 v20, v0

    move/from16 v0, v20

    move/from16 v1, p5

    if-le v0, v1, :cond_cf

    .line 80
    new-instance v20, Lorg/tukaani/xz/MemoryLimitException;

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tukaani/xz/index/IndexDecoder;->memoryUsage:I

    move/from16 v21, v0

    move-object/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, p5

    invoke-direct {v0, v1, v2}, Lorg/tukaani/xz/MemoryLimitException;-><init>(II)V

    throw v20

    .line 83
    :cond_cf
    long-to-int v0, v3

    move/from16 v20, v0

    move/from16 v0, v20

    new-array v0, v0, [J

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/tukaani/xz/index/IndexDecoder;->unpadded:[J

    .line 84
    long-to-int v0, v3

    move/from16 v20, v0

    move/from16 v0, v20

    new-array v0, v0, [J

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/tukaani/xz/index/IndexDecoder;->uncompressed:[J

    .line 85
    const/4 v13, 0x0

    .line 88
    .local v13, "record":I
    long-to-int v9, v3

    .local v9, "i":I
    :goto_ef
    if-lez v9, :cond_15a

    .line 90
    invoke-static {v10}, Lorg/tukaani/xz/common/DecoderUtil;->decodeVLI(Ljava/io/InputStream;)J

    move-result-wide v16

    .line 91
    .local v16, "unpaddedSize":J
    invoke-static {v10}, Lorg/tukaani/xz/common/DecoderUtil;->decodeVLI(Ljava/io/InputStream;)J

    move-result-wide v14

    .line 97
    .local v14, "uncompressedSize":J
    invoke-virtual/range {p1 .. p1}, Lorg/tukaani/xz/SeekableInputStream;->position()J

    move-result-wide v20

    cmp-long v20, v20, v7

    if-lez v20, :cond_109

    .line 98
    new-instance v20, Lorg/tukaani/xz/CorruptedInputException;

    const-string v21, "XZ Index is corrupt"

    invoke-direct/range {v20 .. v21}, Lorg/tukaani/xz/CorruptedInputException;-><init>(Ljava/lang/String;)V

    throw v20

    .line 101
    :cond_109
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/tukaani/xz/index/IndexDecoder;->unpadded:[J

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/tukaani/xz/index/IndexDecoder;->blocksSum:J

    move-wide/from16 v21, v0

    add-long v21, v21, v16

    aput-wide v21, v20, v13

    .line 102
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/tukaani/xz/index/IndexDecoder;->uncompressed:[J

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/tukaani/xz/index/IndexDecoder;->uncompressedSum:J

    move-wide/from16 v21, v0

    add-long v21, v21, v14

    aput-wide v21, v20, v13

    .line 103
    add-int/lit8 v13, v13, 0x1

    .line 104
    move-object/from16 v0, p0

    move-wide/from16 v1, v16

    invoke-super {v0, v1, v2, v14, v15}, Lorg/tukaani/xz/index/IndexBase;->add(JJ)V

    .line 105
    sget-boolean v20, Lorg/tukaani/xz/index/IndexDecoder;->$assertionsDisabled:Z

    if-nez v20, :cond_149

    int-to-long v0, v13

    move-wide/from16 v20, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/tukaani/xz/index/IndexDecoder;->recordCount:J

    move-wide/from16 v22, v0

    cmp-long v20, v20, v22

    if-eqz v20, :cond_149

    new-instance v20, Ljava/lang/AssertionError;

    invoke-direct/range {v20 .. v20}, Ljava/lang/AssertionError;-><init>()V

    throw v20

    .line 108
    :cond_149
    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/tukaani/xz/index/IndexDecoder;->largestBlockSize:J

    move-wide/from16 v20, v0

    cmp-long v20, v20, v14

    if-gez v20, :cond_157

    .line 109
    move-object/from16 v0, p0

    iput-wide v14, v0, Lorg/tukaani/xz/index/IndexDecoder;->largestBlockSize:J
    :try_end_157
    .catch Ljava/io/EOFException; {:try_start_8e .. :try_end_157} :catch_7e

    .line 88
    :cond_157
    add-int/lit8 v9, v9, -0x1

    goto :goto_ef

    .line 119
    .end local v14    # "uncompressedSize":J
    .end local v16    # "unpaddedSize":J
    :cond_15a
    invoke-virtual/range {p0 .. p0}, Lorg/tukaani/xz/index/IndexDecoder;->getIndexPaddingSize()I

    move-result v11

    .line 120
    .local v11, "indexPaddingSize":I
    invoke-virtual/range {p1 .. p1}, Lorg/tukaani/xz/SeekableInputStream;->position()J

    move-result-wide v20

    int-to-long v0, v11

    move-wide/from16 v22, v0

    add-long v20, v20, v22

    cmp-long v20, v20, v7

    if-eqz v20, :cond_173

    .line 121
    new-instance v20, Lorg/tukaani/xz/CorruptedInputException;

    const-string v21, "XZ Index is corrupt"

    invoke-direct/range {v20 .. v21}, Lorg/tukaani/xz/CorruptedInputException;-><init>(Ljava/lang/String;)V

    throw v20

    :cond_173
    move v12, v11

    .line 124
    .end local v11    # "indexPaddingSize":I
    .local v12, "indexPaddingSize":I
    add-int/lit8 v11, v12, -0x1

    .end local v12    # "indexPaddingSize":I
    .restart local v11    # "indexPaddingSize":I
    if-lez v12, :cond_186

    .line 125
    invoke-virtual {v10}, Ljava/util/zip/CheckedInputStream;->read()I

    move-result v20

    if-eqz v20, :cond_173

    .line 126
    new-instance v20, Lorg/tukaani/xz/CorruptedInputException;

    const-string v21, "XZ Index is corrupt"

    invoke-direct/range {v20 .. v21}, Lorg/tukaani/xz/CorruptedInputException;-><init>(Ljava/lang/String;)V

    throw v20

    .line 129
    :cond_186
    invoke-virtual {v5}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide v18

    .line 130
    .local v18, "value":J
    const/4 v9, 0x0

    :goto_18b
    const/16 v20, 0x4

    move/from16 v0, v20

    if-ge v9, v0, :cond_1b1

    .line 131
    mul-int/lit8 v20, v9, 0x8

    ushr-long v20, v18, v20

    const-wide/16 v22, 0xff

    and-long v20, v20, v22

    invoke-virtual/range {p1 .. p1}, Lorg/tukaani/xz/SeekableInputStream;->read()I

    move-result v22

    move/from16 v0, v22

    int-to-long v0, v0

    move-wide/from16 v22, v0

    cmp-long v20, v20, v22

    if-eqz v20, :cond_1ae

    .line 132
    new-instance v20, Lorg/tukaani/xz/CorruptedInputException;

    const-string v21, "XZ Index is corrupt"

    invoke-direct/range {v20 .. v21}, Lorg/tukaani/xz/CorruptedInputException;-><init>(Ljava/lang/String;)V

    throw v20

    .line 130
    :cond_1ae
    add-int/lit8 v9, v9, 0x1

    goto :goto_18b

    .line 133
    :cond_1b1
    return-void
.end method


# virtual methods
.method public bridge synthetic getIndexSize()J
    .registers 3

    .prologue
    .line 22
    invoke-super {p0}, Lorg/tukaani/xz/index/IndexBase;->getIndexSize()J

    move-result-wide v0

    return-wide v0
.end method

.method public getLargestBlockSize()J
    .registers 3

    .prologue
    .line 164
    iget-wide v0, p0, Lorg/tukaani/xz/index/IndexDecoder;->largestBlockSize:J

    return-wide v0
.end method

.method public getMemoryUsage()I
    .registers 2

    .prologue
    .line 146
    iget v0, p0, Lorg/tukaani/xz/index/IndexDecoder;->memoryUsage:I

    return v0
.end method

.method public getRecordCount()I
    .registers 3

    .prologue
    .line 156
    iget-wide v0, p0, Lorg/tukaani/xz/index/IndexDecoder;->recordCount:J

    long-to-int v0, v0

    return v0
.end method

.method public getStreamFlags()Lorg/tukaani/xz/common/StreamFlags;
    .registers 2

    .prologue
    .line 150
    iget-object v0, p0, Lorg/tukaani/xz/index/IndexDecoder;->streamFlags:Lorg/tukaani/xz/common/StreamFlags;

    return-object v0
.end method

.method public bridge synthetic getStreamSize()J
    .registers 3

    .prologue
    .line 22
    invoke-super {p0}, Lorg/tukaani/xz/index/IndexBase;->getStreamSize()J

    move-result-wide v0

    return-wide v0
.end method

.method public getUncompressedSize()J
    .registers 3

    .prologue
    .line 160
    iget-wide v0, p0, Lorg/tukaani/xz/index/IndexDecoder;->uncompressedSum:J

    return-wide v0
.end method

.method public hasRecord(I)Z
    .registers 8
    .param p1, "blockNumber"    # I

    .prologue
    .line 173
    iget v0, p0, Lorg/tukaani/xz/index/IndexDecoder;->recordOffset:I

    if-lt p1, v0, :cond_11

    int-to-long v0, p1

    iget v2, p0, Lorg/tukaani/xz/index/IndexDecoder;->recordOffset:I

    int-to-long v2, v2

    iget-wide v4, p0, Lorg/tukaani/xz/index/IndexDecoder;->recordCount:J

    add-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-gez v0, :cond_11

    const/4 v0, 0x1

    :goto_10
    return v0

    :cond_11
    const/4 v0, 0x0

    goto :goto_10
.end method

.method public hasUncompressedOffset(J)Z
    .registers 7
    .param p1, "pos"    # J

    .prologue
    .line 168
    iget-wide v0, p0, Lorg/tukaani/xz/index/IndexDecoder;->uncompressedOffset:J

    cmp-long v0, p1, v0

    if-ltz v0, :cond_11

    iget-wide v0, p0, Lorg/tukaani/xz/index/IndexDecoder;->uncompressedOffset:J

    iget-wide v2, p0, Lorg/tukaani/xz/index/IndexDecoder;->uncompressedSum:J

    add-long/2addr v0, v2

    cmp-long v0, p1, v0

    if-gez v0, :cond_11

    const/4 v0, 0x1

    :goto_10
    return v0

    :cond_11
    const/4 v0, 0x0

    goto :goto_10
.end method

.method public locateBlock(Lorg/tukaani/xz/index/BlockInfo;J)V
    .registers 9
    .param p1, "info"    # Lorg/tukaani/xz/index/BlockInfo;
    .param p2, "target"    # J

    .prologue
    .line 178
    sget-boolean v3, Lorg/tukaani/xz/index/IndexDecoder;->$assertionsDisabled:Z

    if-nez v3, :cond_10

    iget-wide v3, p0, Lorg/tukaani/xz/index/IndexDecoder;->uncompressedOffset:J

    cmp-long v3, p2, v3

    if-gez v3, :cond_10

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 179
    :cond_10
    iget-wide v3, p0, Lorg/tukaani/xz/index/IndexDecoder;->uncompressedOffset:J

    sub-long/2addr p2, v3

    .line 180
    sget-boolean v3, Lorg/tukaani/xz/index/IndexDecoder;->$assertionsDisabled:Z

    if-nez v3, :cond_23

    iget-wide v3, p0, Lorg/tukaani/xz/index/IndexDecoder;->uncompressedSum:J

    cmp-long v3, p2, v3

    if-ltz v3, :cond_23

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 182
    :cond_23
    const/4 v1, 0x0

    .line 183
    .local v1, "left":I
    iget-object v3, p0, Lorg/tukaani/xz/index/IndexDecoder;->unpadded:[J

    array-length v3, v3

    add-int/lit8 v2, v3, -0x1

    .line 185
    .local v2, "right":I
    :goto_29
    if-ge v1, v2, :cond_3e

    .line 186
    sub-int v3, v2, v1

    div-int/lit8 v3, v3, 0x2

    add-int v0, v1, v3

    .line 188
    .local v0, "i":I
    iget-object v3, p0, Lorg/tukaani/xz/index/IndexDecoder;->uncompressed:[J

    aget-wide v3, v3, v0

    cmp-long v3, v3, p2

    if-gtz v3, :cond_3c

    .line 189
    add-int/lit8 v1, v0, 0x1

    goto :goto_29

    .line 191
    :cond_3c
    move v2, v0

    goto :goto_29

    .line 194
    .end local v0    # "i":I
    :cond_3e
    iget v3, p0, Lorg/tukaani/xz/index/IndexDecoder;->recordOffset:I

    add-int/2addr v3, v1

    invoke-virtual {p0, p1, v3}, Lorg/tukaani/xz/index/IndexDecoder;->setBlockInfo(Lorg/tukaani/xz/index/BlockInfo;I)V

    .line 195
    return-void
.end method

.method public setBlockInfo(Lorg/tukaani/xz/index/BlockInfo;I)V
    .registers 10
    .param p1, "info"    # Lorg/tukaani/xz/index/BlockInfo;
    .param p2, "blockNumber"    # I

    .prologue
    const-wide/16 v5, 0x0

    .line 200
    sget-boolean v1, Lorg/tukaani/xz/index/IndexDecoder;->$assertionsDisabled:Z

    if-nez v1, :cond_10

    iget v1, p0, Lorg/tukaani/xz/index/IndexDecoder;->recordOffset:I

    if-ge p2, v1, :cond_10

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 201
    :cond_10
    sget-boolean v1, Lorg/tukaani/xz/index/IndexDecoder;->$assertionsDisabled:Z

    if-nez v1, :cond_25

    iget v1, p0, Lorg/tukaani/xz/index/IndexDecoder;->recordOffset:I

    sub-int v1, p2, v1

    int-to-long v1, v1

    iget-wide v3, p0, Lorg/tukaani/xz/index/IndexDecoder;->recordCount:J

    cmp-long v1, v1, v3

    if-ltz v1, :cond_25

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 203
    :cond_25
    iput-object p0, p1, Lorg/tukaani/xz/index/BlockInfo;->index:Lorg/tukaani/xz/index/IndexDecoder;

    .line 204
    iput p2, p1, Lorg/tukaani/xz/index/BlockInfo;->blockNumber:I

    .line 206
    iget v1, p0, Lorg/tukaani/xz/index/IndexDecoder;->recordOffset:I

    sub-int v0, p2, v1

    .line 208
    .local v0, "pos":I
    if-nez v0, :cond_57

    .line 209
    iput-wide v5, p1, Lorg/tukaani/xz/index/BlockInfo;->compressedOffset:J

    .line 210
    iput-wide v5, p1, Lorg/tukaani/xz/index/BlockInfo;->uncompressedOffset:J

    .line 216
    :goto_33
    iget-object v1, p0, Lorg/tukaani/xz/index/IndexDecoder;->unpadded:[J

    aget-wide v1, v1, v0

    iget-wide v3, p1, Lorg/tukaani/xz/index/BlockInfo;->compressedOffset:J

    sub-long/2addr v1, v3

    iput-wide v1, p1, Lorg/tukaani/xz/index/BlockInfo;->unpaddedSize:J

    .line 217
    iget-object v1, p0, Lorg/tukaani/xz/index/IndexDecoder;->uncompressed:[J

    aget-wide v1, v1, v0

    iget-wide v3, p1, Lorg/tukaani/xz/index/BlockInfo;->uncompressedOffset:J

    sub-long/2addr v1, v3

    iput-wide v1, p1, Lorg/tukaani/xz/index/BlockInfo;->uncompressedSize:J

    .line 219
    iget-wide v1, p1, Lorg/tukaani/xz/index/BlockInfo;->compressedOffset:J

    iget-wide v3, p0, Lorg/tukaani/xz/index/IndexDecoder;->compressedOffset:J

    const-wide/16 v5, 0xc

    add-long/2addr v3, v5

    add-long/2addr v1, v3

    iput-wide v1, p1, Lorg/tukaani/xz/index/BlockInfo;->compressedOffset:J

    .line 221
    iget-wide v1, p1, Lorg/tukaani/xz/index/BlockInfo;->uncompressedOffset:J

    iget-wide v3, p0, Lorg/tukaani/xz/index/IndexDecoder;->uncompressedOffset:J

    add-long/2addr v1, v3

    iput-wide v1, p1, Lorg/tukaani/xz/index/BlockInfo;->uncompressedOffset:J

    .line 222
    return-void

    .line 212
    :cond_57
    iget-object v1, p0, Lorg/tukaani/xz/index/IndexDecoder;->unpadded:[J

    add-int/lit8 v2, v0, -0x1

    aget-wide v1, v1, v2

    const-wide/16 v3, 0x3

    add-long/2addr v1, v3

    const-wide/16 v3, -0x4

    and-long/2addr v1, v3

    iput-wide v1, p1, Lorg/tukaani/xz/index/BlockInfo;->compressedOffset:J

    .line 213
    iget-object v1, p0, Lorg/tukaani/xz/index/IndexDecoder;->uncompressed:[J

    add-int/lit8 v2, v0, -0x1

    aget-wide v1, v1, v2

    iput-wide v1, p1, Lorg/tukaani/xz/index/BlockInfo;->uncompressedOffset:J

    goto :goto_33
.end method

.method public setOffsets(Lorg/tukaani/xz/index/IndexDecoder;)V
    .registers 6
    .param p1, "prev"    # Lorg/tukaani/xz/index/IndexDecoder;

    .prologue
    .line 138
    iget v0, p1, Lorg/tukaani/xz/index/IndexDecoder;->recordOffset:I

    iget-wide v1, p1, Lorg/tukaani/xz/index/IndexDecoder;->recordCount:J

    long-to-int v1, v1

    add-int/2addr v0, v1

    iput v0, p0, Lorg/tukaani/xz/index/IndexDecoder;->recordOffset:I

    .line 139
    iget-wide v0, p1, Lorg/tukaani/xz/index/IndexDecoder;->compressedOffset:J

    .line 140
    invoke-virtual {p1}, Lorg/tukaani/xz/index/IndexDecoder;->getStreamSize()J

    move-result-wide v2

    add-long/2addr v0, v2

    iget-wide v2, p1, Lorg/tukaani/xz/index/IndexDecoder;->streamPadding:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lorg/tukaani/xz/index/IndexDecoder;->compressedOffset:J

    .line 141
    sget-boolean v0, Lorg/tukaani/xz/index/IndexDecoder;->$assertionsDisabled:Z

    if-nez v0, :cond_29

    iget-wide v0, p0, Lorg/tukaani/xz/index/IndexDecoder;->compressedOffset:J

    const-wide/16 v2, 0x3

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_29

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 142
    :cond_29
    iget-wide v0, p1, Lorg/tukaani/xz/index/IndexDecoder;->uncompressedOffset:J

    iget-wide v2, p1, Lorg/tukaani/xz/index/IndexDecoder;->uncompressedSum:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lorg/tukaani/xz/index/IndexDecoder;->uncompressedOffset:J

    .line 143
    return-void
.end method
