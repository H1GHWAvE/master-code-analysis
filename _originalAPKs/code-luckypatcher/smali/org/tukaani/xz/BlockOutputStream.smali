.class Lorg/tukaani/xz/BlockOutputStream;
.super Lorg/tukaani/xz/FinishableOutputStream;
.source "BlockOutputStream.java"


# instance fields
.field private final check:Lorg/tukaani/xz/check/Check;

.field private final compressedSizeLimit:J

.field private filterChain:Lorg/tukaani/xz/FinishableOutputStream;

.field private final headerSize:I

.field private final out:Ljava/io/OutputStream;

.field private final outCounted:Lorg/tukaani/xz/CountingOutputStream;

.field private final tempBuf:[B

.field private uncompressedSize:J


# direct methods
.method public constructor <init>(Ljava/io/OutputStream;[Lorg/tukaani/xz/FilterEncoder;Lorg/tukaani/xz/check/Check;)V
    .registers 12
    .param p1, "out"    # Ljava/io/OutputStream;
    .param p2, "filters"    # [Lorg/tukaani/xz/FilterEncoder;
    .param p3, "check"    # Lorg/tukaani/xz/check/Check;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 31
    invoke-direct {p0}, Lorg/tukaani/xz/FinishableOutputStream;-><init>()V

    .line 26
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lorg/tukaani/xz/BlockOutputStream;->uncompressedSize:J

    .line 28
    const/4 v4, 0x1

    new-array v4, v4, [B

    iput-object v4, p0, Lorg/tukaani/xz/BlockOutputStream;->tempBuf:[B

    .line 32
    iput-object p1, p0, Lorg/tukaani/xz/BlockOutputStream;->out:Ljava/io/OutputStream;

    .line 33
    iput-object p3, p0, Lorg/tukaani/xz/BlockOutputStream;->check:Lorg/tukaani/xz/check/Check;

    .line 36
    new-instance v4, Lorg/tukaani/xz/CountingOutputStream;

    invoke-direct {v4, p1}, Lorg/tukaani/xz/CountingOutputStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v4, p0, Lorg/tukaani/xz/BlockOutputStream;->outCounted:Lorg/tukaani/xz/CountingOutputStream;

    .line 37
    iget-object v4, p0, Lorg/tukaani/xz/BlockOutputStream;->outCounted:Lorg/tukaani/xz/CountingOutputStream;

    iput-object v4, p0, Lorg/tukaani/xz/BlockOutputStream;->filterChain:Lorg/tukaani/xz/FinishableOutputStream;

    .line 38
    array-length v4, p2

    add-int/lit8 v3, v4, -0x1

    .local v3, "i":I
    :goto_1f
    if-ltz v3, :cond_2e

    .line 39
    aget-object v4, p2, v3

    iget-object v5, p0, Lorg/tukaani/xz/BlockOutputStream;->filterChain:Lorg/tukaani/xz/FinishableOutputStream;

    invoke-interface {v4, v5}, Lorg/tukaani/xz/FilterEncoder;->getOutputStream(Lorg/tukaani/xz/FinishableOutputStream;)Lorg/tukaani/xz/FinishableOutputStream;

    move-result-object v4

    iput-object v4, p0, Lorg/tukaani/xz/BlockOutputStream;->filterChain:Lorg/tukaani/xz/FinishableOutputStream;

    .line 38
    add-int/lit8 v3, v3, -0x1

    goto :goto_1f

    .line 42
    :cond_2e
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 46
    .local v1, "bufStream":Ljava/io/ByteArrayOutputStream;
    invoke-virtual {v1, v6}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 50
    array-length v4, p2

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v1, v4}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 53
    const/4 v3, 0x0

    :goto_3d
    array-length v4, p2

    if-ge v3, v4, :cond_5a

    .line 54
    aget-object v4, p2, v3

    invoke-interface {v4}, Lorg/tukaani/xz/FilterEncoder;->getFilterID()J

    move-result-wide v4

    invoke-static {v1, v4, v5}, Lorg/tukaani/xz/common/EncoderUtil;->encodeVLI(Ljava/io/OutputStream;J)V

    .line 55
    aget-object v4, p2, v3

    invoke-interface {v4}, Lorg/tukaani/xz/FilterEncoder;->getFilterProps()[B

    move-result-object v2

    .line 56
    .local v2, "filterProps":[B
    array-length v4, v2

    int-to-long v4, v4

    invoke-static {v1, v4, v5}, Lorg/tukaani/xz/common/EncoderUtil;->encodeVLI(Ljava/io/OutputStream;J)V

    .line 57
    invoke-virtual {v1, v2}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 53
    add-int/lit8 v3, v3, 0x1

    goto :goto_3d

    .line 61
    .end local v2    # "filterProps":[B
    :cond_5a
    :goto_5a
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v4

    and-int/lit8 v4, v4, 0x3

    if-eqz v4, :cond_66

    .line 62
    invoke-virtual {v1, v6}, Ljava/io/ByteArrayOutputStream;->write(I)V

    goto :goto_5a

    .line 64
    :cond_66
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 68
    .local v0, "buf":[B
    array-length v4, v0

    add-int/lit8 v4, v4, 0x4

    iput v4, p0, Lorg/tukaani/xz/BlockOutputStream;->headerSize:I

    .line 71
    iget v4, p0, Lorg/tukaani/xz/BlockOutputStream;->headerSize:I

    const/16 v5, 0x400

    if-le v4, v5, :cond_7b

    .line 72
    new-instance v4, Lorg/tukaani/xz/UnsupportedOptionsException;

    invoke-direct {v4}, Lorg/tukaani/xz/UnsupportedOptionsException;-><init>()V

    throw v4

    .line 75
    :cond_7b
    array-length v4, v0

    div-int/lit8 v4, v4, 0x4

    int-to-byte v4, v4

    aput-byte v4, v0, v6

    .line 78
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 79
    invoke-static {p1, v0}, Lorg/tukaani/xz/common/EncoderUtil;->writeCRC32(Ljava/io/OutputStream;[B)V

    .line 83
    const-wide v4, 0x7ffffffffffffffcL

    iget v6, p0, Lorg/tukaani/xz/BlockOutputStream;->headerSize:I

    int-to-long v6, v6

    sub-long/2addr v4, v6

    .line 84
    invoke-virtual {p3}, Lorg/tukaani/xz/check/Check;->getSize()I

    move-result v6

    int-to-long v6, v6

    sub-long/2addr v4, v6

    iput-wide v4, p0, Lorg/tukaani/xz/BlockOutputStream;->compressedSizeLimit:J

    .line 85
    return-void
.end method

.method private validate()V
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    .line 118
    iget-object v2, p0, Lorg/tukaani/xz/BlockOutputStream;->outCounted:Lorg/tukaani/xz/CountingOutputStream;

    invoke-virtual {v2}, Lorg/tukaani/xz/CountingOutputStream;->getSize()J

    move-result-wide v0

    .line 122
    .local v0, "compressedSize":J
    cmp-long v2, v0, v4

    if-ltz v2, :cond_18

    iget-wide v2, p0, Lorg/tukaani/xz/BlockOutputStream;->compressedSizeLimit:J

    cmp-long v2, v0, v2

    if-gtz v2, :cond_18

    iget-wide v2, p0, Lorg/tukaani/xz/BlockOutputStream;->uncompressedSize:J

    cmp-long v2, v2, v4

    if-gez v2, :cond_20

    .line 124
    :cond_18
    new-instance v2, Lorg/tukaani/xz/XZIOException;

    const-string v3, "XZ Stream has grown too big"

    invoke-direct {v2, v3}, Lorg/tukaani/xz/XZIOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 125
    :cond_20
    return-void
.end method


# virtual methods
.method public finish()V
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 106
    iget-object v2, p0, Lorg/tukaani/xz/BlockOutputStream;->filterChain:Lorg/tukaani/xz/FinishableOutputStream;

    invoke-virtual {v2}, Lorg/tukaani/xz/FinishableOutputStream;->finish()V

    .line 107
    invoke-direct {p0}, Lorg/tukaani/xz/BlockOutputStream;->validate()V

    .line 110
    iget-object v2, p0, Lorg/tukaani/xz/BlockOutputStream;->outCounted:Lorg/tukaani/xz/CountingOutputStream;

    invoke-virtual {v2}, Lorg/tukaani/xz/CountingOutputStream;->getSize()J

    move-result-wide v0

    .local v0, "i":J
    :goto_e
    const-wide/16 v2, 0x3

    and-long/2addr v2, v0

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_21

    .line 111
    iget-object v2, p0, Lorg/tukaani/xz/BlockOutputStream;->out:Ljava/io/OutputStream;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/io/OutputStream;->write(I)V

    .line 110
    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    goto :goto_e

    .line 114
    :cond_21
    iget-object v2, p0, Lorg/tukaani/xz/BlockOutputStream;->out:Ljava/io/OutputStream;

    iget-object v3, p0, Lorg/tukaani/xz/BlockOutputStream;->check:Lorg/tukaani/xz/check/Check;

    invoke-virtual {v3}, Lorg/tukaani/xz/check/Check;->finish()[B

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/OutputStream;->write([B)V

    .line 115
    return-void
.end method

.method public flush()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 100
    iget-object v0, p0, Lorg/tukaani/xz/BlockOutputStream;->filterChain:Lorg/tukaani/xz/FinishableOutputStream;

    invoke-virtual {v0}, Lorg/tukaani/xz/FinishableOutputStream;->flush()V

    .line 101
    invoke-direct {p0}, Lorg/tukaani/xz/BlockOutputStream;->validate()V

    .line 102
    return-void
.end method

.method public getUncompressedSize()J
    .registers 3

    .prologue
    .line 132
    iget-wide v0, p0, Lorg/tukaani/xz/BlockOutputStream;->uncompressedSize:J

    return-wide v0
.end method

.method public getUnpaddedSize()J
    .registers 5

    .prologue
    .line 128
    iget v0, p0, Lorg/tukaani/xz/BlockOutputStream;->headerSize:I

    int-to-long v0, v0

    iget-object v2, p0, Lorg/tukaani/xz/BlockOutputStream;->outCounted:Lorg/tukaani/xz/CountingOutputStream;

    invoke-virtual {v2}, Lorg/tukaani/xz/CountingOutputStream;->getSize()J

    move-result-wide v2

    add-long/2addr v0, v2

    iget-object v2, p0, Lorg/tukaani/xz/BlockOutputStream;->check:Lorg/tukaani/xz/check/Check;

    invoke-virtual {v2}, Lorg/tukaani/xz/check/Check;->getSize()I

    move-result v2

    int-to-long v2, v2

    add-long/2addr v0, v2

    return-wide v0
.end method

.method public write(I)V
    .registers 5
    .param p1, "b"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 88
    iget-object v0, p0, Lorg/tukaani/xz/BlockOutputStream;->tempBuf:[B

    int-to-byte v1, p1

    aput-byte v1, v0, v2

    .line 89
    iget-object v0, p0, Lorg/tukaani/xz/BlockOutputStream;->tempBuf:[B

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v2, v1}, Lorg/tukaani/xz/BlockOutputStream;->write([BII)V

    .line 90
    return-void
.end method

.method public write([BII)V
    .registers 8
    .param p1, "buf"    # [B
    .param p2, "off"    # I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 93
    iget-object v0, p0, Lorg/tukaani/xz/BlockOutputStream;->filterChain:Lorg/tukaani/xz/FinishableOutputStream;

    invoke-virtual {v0, p1, p2, p3}, Lorg/tukaani/xz/FinishableOutputStream;->write([BII)V

    .line 94
    iget-object v0, p0, Lorg/tukaani/xz/BlockOutputStream;->check:Lorg/tukaani/xz/check/Check;

    invoke-virtual {v0, p1, p2, p3}, Lorg/tukaani/xz/check/Check;->update([BII)V

    .line 95
    iget-wide v0, p0, Lorg/tukaani/xz/BlockOutputStream;->uncompressedSize:J

    int-to-long v2, p3

    add-long/2addr v0, v2

    iput-wide v0, p0, Lorg/tukaani/xz/BlockOutputStream;->uncompressedSize:J

    .line 96
    invoke-direct {p0}, Lorg/tukaani/xz/BlockOutputStream;->validate()V

    .line 97
    return-void
.end method
