.class Lorg/tukaani/xz/SimpleOutputStream;
.super Lorg/tukaani/xz/FinishableOutputStream;
.source "SimpleOutputStream.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final FILTER_BUF_SIZE:I = 0x1000


# instance fields
.field private exception:Ljava/io/IOException;

.field private final filterBuf:[B

.field private finished:Z

.field private out:Lorg/tukaani/xz/FinishableOutputStream;

.field private pos:I

.field private final simpleFilter:Lorg/tukaani/xz/simple/SimpleFilter;

.field private final tempBuf:[B

.field private unfiltered:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 15
    const-class v0, Lorg/tukaani/xz/SimpleOutputStream;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_c

    const/4 v0, 0x1

    :goto_9
    sput-boolean v0, Lorg/tukaani/xz/SimpleOutputStream;->$assertionsDisabled:Z

    return-void

    :cond_c
    const/4 v0, 0x0

    goto :goto_9
.end method

.method constructor <init>(Lorg/tukaani/xz/FinishableOutputStream;Lorg/tukaani/xz/simple/SimpleFilter;)V
    .registers 5
    .param p1, "out"    # Lorg/tukaani/xz/FinishableOutputStream;
    .param p2, "simpleFilter"    # Lorg/tukaani/xz/simple/SimpleFilter;

    .prologue
    const/4 v1, 0x0

    .line 35
    invoke-direct {p0}, Lorg/tukaani/xz/FinishableOutputStream;-><init>()V

    .line 21
    const/16 v0, 0x1000

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/tukaani/xz/SimpleOutputStream;->filterBuf:[B

    .line 22
    iput v1, p0, Lorg/tukaani/xz/SimpleOutputStream;->pos:I

    .line 23
    iput v1, p0, Lorg/tukaani/xz/SimpleOutputStream;->unfiltered:I

    .line 25
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/tukaani/xz/SimpleOutputStream;->exception:Ljava/io/IOException;

    .line 26
    iput-boolean v1, p0, Lorg/tukaani/xz/SimpleOutputStream;->finished:Z

    .line 28
    const/4 v0, 0x1

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/tukaani/xz/SimpleOutputStream;->tempBuf:[B

    .line 36
    if-nez p1, :cond_20

    .line 37
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 39
    :cond_20
    iput-object p1, p0, Lorg/tukaani/xz/SimpleOutputStream;->out:Lorg/tukaani/xz/FinishableOutputStream;

    .line 40
    iput-object p2, p0, Lorg/tukaani/xz/SimpleOutputStream;->simpleFilter:Lorg/tukaani/xz/simple/SimpleFilter;

    .line 41
    return-void
.end method

.method static getMemoryUsage()I
    .registers 1

    .prologue
    .line 31
    const/4 v0, 0x5

    return v0
.end method

.method private writePending()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 92
    sget-boolean v1, Lorg/tukaani/xz/SimpleOutputStream;->$assertionsDisabled:Z

    if-nez v1, :cond_e

    iget-boolean v1, p0, Lorg/tukaani/xz/SimpleOutputStream;->finished:Z

    if-eqz v1, :cond_e

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 94
    :cond_e
    iget-object v1, p0, Lorg/tukaani/xz/SimpleOutputStream;->exception:Ljava/io/IOException;

    if-eqz v1, :cond_15

    .line 95
    iget-object v1, p0, Lorg/tukaani/xz/SimpleOutputStream;->exception:Ljava/io/IOException;

    throw v1

    .line 98
    :cond_15
    :try_start_15
    iget-object v1, p0, Lorg/tukaani/xz/SimpleOutputStream;->out:Lorg/tukaani/xz/FinishableOutputStream;

    iget-object v2, p0, Lorg/tukaani/xz/SimpleOutputStream;->filterBuf:[B

    iget v3, p0, Lorg/tukaani/xz/SimpleOutputStream;->pos:I

    iget v4, p0, Lorg/tukaani/xz/SimpleOutputStream;->unfiltered:I

    invoke-virtual {v1, v2, v3, v4}, Lorg/tukaani/xz/FinishableOutputStream;->write([BII)V
    :try_end_20
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_20} :catch_24

    .line 104
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/tukaani/xz/SimpleOutputStream;->finished:Z

    .line 105
    return-void

    .line 99
    :catch_24
    move-exception v0

    .line 100
    .local v0, "e":Ljava/io/IOException;
    iput-object v0, p0, Lorg/tukaani/xz/SimpleOutputStream;->exception:Ljava/io/IOException;

    .line 101
    throw v0
.end method


# virtual methods
.method public close()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 126
    iget-object v1, p0, Lorg/tukaani/xz/SimpleOutputStream;->out:Lorg/tukaani/xz/FinishableOutputStream;

    if-eqz v1, :cond_13

    .line 127
    iget-boolean v1, p0, Lorg/tukaani/xz/SimpleOutputStream;->finished:Z

    if-nez v1, :cond_b

    .line 132
    :try_start_8
    invoke-direct {p0}, Lorg/tukaani/xz/SimpleOutputStream;->writePending()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_b} :catch_22

    .line 137
    :cond_b
    :goto_b
    :try_start_b
    iget-object v1, p0, Lorg/tukaani/xz/SimpleOutputStream;->out:Lorg/tukaani/xz/FinishableOutputStream;

    invoke-virtual {v1}, Lorg/tukaani/xz/FinishableOutputStream;->close()V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_10} :catch_1a

    .line 145
    :cond_10
    :goto_10
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/tukaani/xz/SimpleOutputStream;->out:Lorg/tukaani/xz/FinishableOutputStream;

    .line 148
    :cond_13
    iget-object v1, p0, Lorg/tukaani/xz/SimpleOutputStream;->exception:Ljava/io/IOException;

    if-eqz v1, :cond_24

    .line 149
    iget-object v1, p0, Lorg/tukaani/xz/SimpleOutputStream;->exception:Ljava/io/IOException;

    throw v1

    .line 138
    :catch_1a
    move-exception v0

    .line 141
    .local v0, "e":Ljava/io/IOException;
    iget-object v1, p0, Lorg/tukaani/xz/SimpleOutputStream;->exception:Ljava/io/IOException;

    if-nez v1, :cond_10

    .line 142
    iput-object v0, p0, Lorg/tukaani/xz/SimpleOutputStream;->exception:Ljava/io/IOException;

    goto :goto_10

    .line 133
    .end local v0    # "e":Ljava/io/IOException;
    :catch_22
    move-exception v1

    goto :goto_b

    .line 150
    :cond_24
    return-void
.end method

.method public finish()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 112
    iget-boolean v1, p0, Lorg/tukaani/xz/SimpleOutputStream;->finished:Z

    if-nez v1, :cond_c

    .line 114
    invoke-direct {p0}, Lorg/tukaani/xz/SimpleOutputStream;->writePending()V

    .line 117
    :try_start_7
    iget-object v1, p0, Lorg/tukaani/xz/SimpleOutputStream;->out:Lorg/tukaani/xz/FinishableOutputStream;

    invoke-virtual {v1}, Lorg/tukaani/xz/FinishableOutputStream;->finish()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_c} :catch_d

    .line 123
    :cond_c
    return-void

    .line 118
    :catch_d
    move-exception v0

    .line 119
    .local v0, "e":Ljava/io/IOException;
    iput-object v0, p0, Lorg/tukaani/xz/SimpleOutputStream;->exception:Ljava/io/IOException;

    .line 120
    throw v0
.end method

.method public flush()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 108
    new-instance v0, Lorg/tukaani/xz/UnsupportedOptionsException;

    const-string v1, "Flushing is not supported"

    invoke-direct {v0, v1}, Lorg/tukaani/xz/UnsupportedOptionsException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public write(I)V
    .registers 5
    .param p1, "b"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 44
    iget-object v0, p0, Lorg/tukaani/xz/SimpleOutputStream;->tempBuf:[B

    int-to-byte v1, p1

    aput-byte v1, v0, v2

    .line 45
    iget-object v0, p0, Lorg/tukaani/xz/SimpleOutputStream;->tempBuf:[B

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v2, v1}, Lorg/tukaani/xz/SimpleOutputStream;->write([BII)V

    .line 46
    return-void
.end method

.method public write([BII)V
    .registers 12
    .param p1, "buf"    # [B
    .param p2, "off"    # I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 49
    if-ltz p2, :cond_e

    if-ltz p3, :cond_e

    add-int v3, p2, p3

    if-ltz v3, :cond_e

    add-int v3, p2, p3

    array-length v4, p1

    if-le v3, v4, :cond_14

    .line 50
    :cond_e
    new-instance v3, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v3}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v3

    .line 52
    :cond_14
    iget-object v3, p0, Lorg/tukaani/xz/SimpleOutputStream;->exception:Ljava/io/IOException;

    if-eqz v3, :cond_1b

    .line 53
    iget-object v3, p0, Lorg/tukaani/xz/SimpleOutputStream;->exception:Ljava/io/IOException;

    throw v3

    .line 55
    :cond_1b
    iget-boolean v3, p0, Lorg/tukaani/xz/SimpleOutputStream;->finished:Z

    if-eqz v3, :cond_50

    .line 56
    new-instance v3, Lorg/tukaani/xz/XZIOException;

    const-string v4, "Stream finished or closed"

    invoke-direct {v3, v4}, Lorg/tukaani/xz/XZIOException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 69
    .local v0, "copySize":I
    .local v2, "filtered":I
    :cond_27
    iget v3, p0, Lorg/tukaani/xz/SimpleOutputStream;->unfiltered:I

    sub-int/2addr v3, v2

    iput v3, p0, Lorg/tukaani/xz/SimpleOutputStream;->unfiltered:I

    .line 73
    :try_start_2c
    iget-object v3, p0, Lorg/tukaani/xz/SimpleOutputStream;->out:Lorg/tukaani/xz/FinishableOutputStream;

    iget-object v4, p0, Lorg/tukaani/xz/SimpleOutputStream;->filterBuf:[B

    iget v5, p0, Lorg/tukaani/xz/SimpleOutputStream;->pos:I

    invoke-virtual {v3, v4, v5, v2}, Lorg/tukaani/xz/FinishableOutputStream;->write([BII)V
    :try_end_35
    .catch Ljava/io/IOException; {:try_start_2c .. :try_end_35} :catch_88

    .line 79
    iget v3, p0, Lorg/tukaani/xz/SimpleOutputStream;->pos:I

    add-int/2addr v3, v2

    iput v3, p0, Lorg/tukaani/xz/SimpleOutputStream;->pos:I

    .line 84
    iget v3, p0, Lorg/tukaani/xz/SimpleOutputStream;->pos:I

    iget v4, p0, Lorg/tukaani/xz/SimpleOutputStream;->unfiltered:I

    add-int/2addr v3, v4

    const/16 v4, 0x1000

    if-ne v3, v4, :cond_50

    .line 85
    iget-object v3, p0, Lorg/tukaani/xz/SimpleOutputStream;->filterBuf:[B

    iget v4, p0, Lorg/tukaani/xz/SimpleOutputStream;->pos:I

    iget-object v5, p0, Lorg/tukaani/xz/SimpleOutputStream;->filterBuf:[B

    iget v6, p0, Lorg/tukaani/xz/SimpleOutputStream;->unfiltered:I

    invoke-static {v3, v4, v5, v7, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 86
    iput v7, p0, Lorg/tukaani/xz/SimpleOutputStream;->pos:I

    .line 58
    .end local v0    # "copySize":I
    .end local v2    # "filtered":I
    :cond_50
    if-lez p3, :cond_8c

    .line 60
    iget v3, p0, Lorg/tukaani/xz/SimpleOutputStream;->pos:I

    iget v4, p0, Lorg/tukaani/xz/SimpleOutputStream;->unfiltered:I

    add-int/2addr v3, v4

    rsub-int v3, v3, 0x1000

    invoke-static {p3, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 61
    .restart local v0    # "copySize":I
    iget-object v3, p0, Lorg/tukaani/xz/SimpleOutputStream;->filterBuf:[B

    iget v4, p0, Lorg/tukaani/xz/SimpleOutputStream;->pos:I

    iget v5, p0, Lorg/tukaani/xz/SimpleOutputStream;->unfiltered:I

    add-int/2addr v4, v5

    invoke-static {p1, p2, v3, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 62
    add-int/2addr p2, v0

    .line 63
    sub-int/2addr p3, v0

    .line 64
    iget v3, p0, Lorg/tukaani/xz/SimpleOutputStream;->unfiltered:I

    add-int/2addr v3, v0

    iput v3, p0, Lorg/tukaani/xz/SimpleOutputStream;->unfiltered:I

    .line 67
    iget-object v3, p0, Lorg/tukaani/xz/SimpleOutputStream;->simpleFilter:Lorg/tukaani/xz/simple/SimpleFilter;

    iget-object v4, p0, Lorg/tukaani/xz/SimpleOutputStream;->filterBuf:[B

    iget v5, p0, Lorg/tukaani/xz/SimpleOutputStream;->pos:I

    iget v6, p0, Lorg/tukaani/xz/SimpleOutputStream;->unfiltered:I

    invoke-interface {v3, v4, v5, v6}, Lorg/tukaani/xz/simple/SimpleFilter;->code([BII)I

    move-result v2

    .line 68
    .restart local v2    # "filtered":I
    sget-boolean v3, Lorg/tukaani/xz/SimpleOutputStream;->$assertionsDisabled:Z

    if-nez v3, :cond_27

    iget v3, p0, Lorg/tukaani/xz/SimpleOutputStream;->unfiltered:I

    if-le v2, v3, :cond_27

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 74
    :catch_88
    move-exception v1

    .line 75
    .local v1, "e":Ljava/io/IOException;
    iput-object v1, p0, Lorg/tukaani/xz/SimpleOutputStream;->exception:Ljava/io/IOException;

    .line 76
    throw v1

    .line 89
    .end local v0    # "copySize":I
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "filtered":I
    :cond_8c
    return-void
.end method
