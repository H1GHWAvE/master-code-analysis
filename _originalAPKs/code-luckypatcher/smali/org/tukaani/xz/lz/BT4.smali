.class final Lorg/tukaani/xz/lz/BT4;
.super Lorg/tukaani/xz/lz/LZEncoder;
.source "BT4.java"


# instance fields
.field private cyclicPos:I

.field private final cyclicSize:I

.field private final depthLimit:I

.field private final hash:Lorg/tukaani/xz/lz/Hash234;

.field private lzPos:I

.field private final matches:Lorg/tukaani/xz/lz/Matches;

.field private final tree:[I


# direct methods
.method constructor <init>(IIIIII)V
    .registers 9
    .param p1, "dictSize"    # I
    .param p2, "beforeSizeMin"    # I
    .param p3, "readAheadMax"    # I
    .param p4, "niceLen"    # I
    .param p5, "matchLenMax"    # I
    .param p6, "depthLimit"    # I

    .prologue
    .line 29
    invoke-direct/range {p0 .. p5}, Lorg/tukaani/xz/lz/LZEncoder;-><init>(IIIII)V

    .line 20
    const/4 v0, -0x1

    iput v0, p0, Lorg/tukaani/xz/lz/BT4;->cyclicPos:I

    .line 31
    add-int/lit8 v0, p1, 0x1

    iput v0, p0, Lorg/tukaani/xz/lz/BT4;->cyclicSize:I

    .line 32
    iget v0, p0, Lorg/tukaani/xz/lz/BT4;->cyclicSize:I

    iput v0, p0, Lorg/tukaani/xz/lz/BT4;->lzPos:I

    .line 34
    new-instance v0, Lorg/tukaani/xz/lz/Hash234;

    invoke-direct {v0, p1}, Lorg/tukaani/xz/lz/Hash234;-><init>(I)V

    iput-object v0, p0, Lorg/tukaani/xz/lz/BT4;->hash:Lorg/tukaani/xz/lz/Hash234;

    .line 35
    iget v0, p0, Lorg/tukaani/xz/lz/BT4;->cyclicSize:I

    mul-int/lit8 v0, v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/tukaani/xz/lz/BT4;->tree:[I

    .line 40
    new-instance v0, Lorg/tukaani/xz/lz/Matches;

    add-int/lit8 v1, p4, -0x1

    invoke-direct {v0, v1}, Lorg/tukaani/xz/lz/Matches;-><init>(I)V

    iput-object v0, p0, Lorg/tukaani/xz/lz/BT4;->matches:Lorg/tukaani/xz/lz/Matches;

    .line 42
    if-lez p6, :cond_2b

    .end local p6    # "depthLimit":I
    :goto_28
    iput p6, p0, Lorg/tukaani/xz/lz/BT4;->depthLimit:I

    .line 43
    return-void

    .line 42
    .restart local p6    # "depthLimit":I
    :cond_2b
    div-int/lit8 v0, p4, 0x2

    add-int/lit8 p6, v0, 0x10

    goto :goto_28
.end method

.method static getMemoryUsage(I)I
    .registers 3
    .param p0, "dictSize"    # I

    .prologue
    .line 24
    invoke-static {p0}, Lorg/tukaani/xz/lz/Hash234;->getMemoryUsage(I)I

    move-result v0

    div-int/lit16 v1, p0, 0x80

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0xa

    return v0
.end method

.method private movePos()I
    .registers 6

    .prologue
    const v4, 0x7fffffff

    .line 46
    iget v2, p0, Lorg/tukaani/xz/lz/BT4;->niceLen:I

    const/4 v3, 0x4

    invoke-virtual {p0, v2, v3}, Lorg/tukaani/xz/lz/BT4;->movePos(II)I

    move-result v0

    .line 48
    .local v0, "avail":I
    if-eqz v0, :cond_34

    .line 49
    iget v2, p0, Lorg/tukaani/xz/lz/BT4;->lzPos:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/tukaani/xz/lz/BT4;->lzPos:I

    if-ne v2, v4, :cond_27

    .line 50
    iget v2, p0, Lorg/tukaani/xz/lz/BT4;->cyclicSize:I

    sub-int v1, v4, v2

    .line 51
    .local v1, "normalizationOffset":I
    iget-object v2, p0, Lorg/tukaani/xz/lz/BT4;->hash:Lorg/tukaani/xz/lz/Hash234;

    invoke-virtual {v2, v1}, Lorg/tukaani/xz/lz/Hash234;->normalize(I)V

    .line 52
    iget-object v2, p0, Lorg/tukaani/xz/lz/BT4;->tree:[I

    invoke-static {v2, v1}, Lorg/tukaani/xz/lz/BT4;->normalize([II)V

    .line 53
    iget v2, p0, Lorg/tukaani/xz/lz/BT4;->lzPos:I

    sub-int/2addr v2, v1

    iput v2, p0, Lorg/tukaani/xz/lz/BT4;->lzPos:I

    .line 56
    .end local v1    # "normalizationOffset":I
    :cond_27
    iget v2, p0, Lorg/tukaani/xz/lz/BT4;->cyclicPos:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/tukaani/xz/lz/BT4;->cyclicPos:I

    iget v3, p0, Lorg/tukaani/xz/lz/BT4;->cyclicSize:I

    if-ne v2, v3, :cond_34

    .line 57
    const/4 v2, 0x0

    iput v2, p0, Lorg/tukaani/xz/lz/BT4;->cyclicPos:I

    .line 60
    :cond_34
    return v0
.end method

.method private skip(II)V
    .registers 16
    .param p1, "niceLenLimit"    # I
    .param p2, "currentMatch"    # I

    .prologue
    const/4 v10, 0x0

    .line 188
    iget v1, p0, Lorg/tukaani/xz/lz/BT4;->depthLimit:I

    .line 190
    .local v1, "depth":I
    iget v9, p0, Lorg/tukaani/xz/lz/BT4;->cyclicPos:I

    shl-int/lit8 v9, v9, 0x1

    add-int/lit8 v7, v9, 0x1

    .line 191
    .local v7, "ptr0":I
    iget v9, p0, Lorg/tukaani/xz/lz/BT4;->cyclicPos:I

    shl-int/lit8 v8, v9, 0x1

    .line 192
    .local v8, "ptr1":I
    const/4 v4, 0x0

    .line 193
    .local v4, "len0":I
    const/4 v5, 0x0

    .line 196
    .local v5, "len1":I
    :goto_f
    iget v9, p0, Lorg/tukaani/xz/lz/BT4;->lzPos:I

    sub-int v0, v9, p2

    .line 198
    .local v0, "delta":I
    add-int/lit8 v2, v1, -0x1

    .end local v1    # "depth":I
    .local v2, "depth":I
    if-eqz v1, :cond_1b

    iget v9, p0, Lorg/tukaani/xz/lz/BT4;->cyclicSize:I

    if-lt v0, v9, :cond_24

    .line 199
    :cond_1b
    iget-object v9, p0, Lorg/tukaani/xz/lz/BT4;->tree:[I

    aput v10, v9, v7

    .line 200
    iget-object v9, p0, Lorg/tukaani/xz/lz/BT4;->tree:[I

    aput v10, v9, v8

    .line 216
    :goto_23
    return-void

    .line 204
    :cond_24
    iget v9, p0, Lorg/tukaani/xz/lz/BT4;->cyclicPos:I

    sub-int v11, v9, v0

    iget v9, p0, Lorg/tukaani/xz/lz/BT4;->cyclicPos:I

    if-le v0, v9, :cond_5d

    iget v9, p0, Lorg/tukaani/xz/lz/BT4;->cyclicSize:I

    :goto_2e
    add-int/2addr v9, v11

    shl-int/lit8 v6, v9, 0x1

    .line 206
    .local v6, "pair":I
    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 208
    .local v3, "len":I
    iget-object v9, p0, Lorg/tukaani/xz/lz/BT4;->buf:[B

    iget v11, p0, Lorg/tukaani/xz/lz/BT4;->readPos:I

    add-int/2addr v11, v3

    sub-int/2addr v11, v0

    aget-byte v9, v9, v11

    iget-object v11, p0, Lorg/tukaani/xz/lz/BT4;->buf:[B

    iget v12, p0, Lorg/tukaani/xz/lz/BT4;->readPos:I

    add-int/2addr v12, v3

    aget-byte v11, v11, v12

    if-ne v9, v11, :cond_70

    .line 213
    :cond_46
    add-int/lit8 v3, v3, 0x1

    if-ne v3, p1, :cond_5f

    .line 214
    iget-object v9, p0, Lorg/tukaani/xz/lz/BT4;->tree:[I

    iget-object v10, p0, Lorg/tukaani/xz/lz/BT4;->tree:[I

    aget v10, v10, v6

    aput v10, v9, v8

    .line 215
    iget-object v9, p0, Lorg/tukaani/xz/lz/BT4;->tree:[I

    iget-object v10, p0, Lorg/tukaani/xz/lz/BT4;->tree:[I

    add-int/lit8 v11, v6, 0x1

    aget v10, v10, v11

    aput v10, v9, v7

    goto :goto_23

    .end local v3    # "len":I
    .end local v6    # "pair":I
    :cond_5d
    move v9, v10

    .line 204
    goto :goto_2e

    .line 218
    .restart local v3    # "len":I
    .restart local v6    # "pair":I
    :cond_5f
    iget-object v9, p0, Lorg/tukaani/xz/lz/BT4;->buf:[B

    iget v11, p0, Lorg/tukaani/xz/lz/BT4;->readPos:I

    add-int/2addr v11, v3

    sub-int/2addr v11, v0

    aget-byte v9, v9, v11

    iget-object v11, p0, Lorg/tukaani/xz/lz/BT4;->buf:[B

    iget v12, p0, Lorg/tukaani/xz/lz/BT4;->readPos:I

    add-int/2addr v12, v3

    aget-byte v11, v11, v12

    if-eq v9, v11, :cond_46

    .line 221
    :cond_70
    iget-object v9, p0, Lorg/tukaani/xz/lz/BT4;->buf:[B

    iget v11, p0, Lorg/tukaani/xz/lz/BT4;->readPos:I

    add-int/2addr v11, v3

    sub-int/2addr v11, v0

    aget-byte v9, v9, v11

    and-int/lit16 v9, v9, 0xff

    iget-object v11, p0, Lorg/tukaani/xz/lz/BT4;->buf:[B

    iget v12, p0, Lorg/tukaani/xz/lz/BT4;->readPos:I

    add-int/2addr v12, v3

    aget-byte v11, v11, v12

    and-int/lit16 v11, v11, 0xff

    if-ge v9, v11, :cond_93

    .line 223
    iget-object v9, p0, Lorg/tukaani/xz/lz/BT4;->tree:[I

    aput p2, v9, v8

    .line 224
    add-int/lit8 v8, v6, 0x1

    .line 225
    iget-object v9, p0, Lorg/tukaani/xz/lz/BT4;->tree:[I

    aget p2, v9, v8

    .line 226
    move v5, v3

    :goto_90
    move v1, v2

    .line 233
    .end local v2    # "depth":I
    .restart local v1    # "depth":I
    goto/16 :goto_f

    .line 228
    .end local v1    # "depth":I
    .restart local v2    # "depth":I
    :cond_93
    iget-object v9, p0, Lorg/tukaani/xz/lz/BT4;->tree:[I

    aput p2, v9, v7

    .line 229
    move v7, v6

    .line 230
    iget-object v9, p0, Lorg/tukaani/xz/lz/BT4;->tree:[I

    aget p2, v9, v7

    .line 231
    move v4, v3

    goto :goto_90
.end method


# virtual methods
.method public getMatches()Lorg/tukaani/xz/lz/Matches;
    .registers 23

    .prologue
    .line 64
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/tukaani/xz/lz/BT4;->matches:Lorg/tukaani/xz/lz/Matches;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    move/from16 v0, v19

    move-object/from16 v1, v18

    iput v0, v1, Lorg/tukaani/xz/lz/Matches;->count:I

    .line 66
    move-object/from16 v0, p0

    iget v13, v0, Lorg/tukaani/xz/lz/BT4;->matchLenMax:I

    .line 67
    .local v13, "matchLenLimit":I
    move-object/from16 v0, p0

    iget v14, v0, Lorg/tukaani/xz/lz/BT4;->niceLen:I

    .line 68
    .local v14, "niceLenLimit":I
    invoke-direct/range {p0 .. p0}, Lorg/tukaani/xz/lz/BT4;->movePos()I

    move-result v2

    .line 70
    .local v2, "avail":I
    if-ge v2, v13, :cond_29

    .line 71
    if-nez v2, :cond_25

    .line 72
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/tukaani/xz/lz/BT4;->matches:Lorg/tukaani/xz/lz/Matches;

    move-object/from16 v18, v0

    .line 167
    :goto_24
    return-object v18

    .line 74
    :cond_25
    move v13, v2

    .line 75
    if-le v14, v2, :cond_29

    .line 76
    move v14, v2

    .line 79
    :cond_29
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/tukaani/xz/lz/BT4;->hash:Lorg/tukaani/xz/lz/Hash234;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/tukaani/xz/lz/BT4;->buf:[B

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tukaani/xz/lz/BT4;->readPos:I

    move/from16 v20, v0

    invoke-virtual/range {v18 .. v20}, Lorg/tukaani/xz/lz/Hash234;->calcHashes([BI)V

    .line 80
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tukaani/xz/lz/BT4;->lzPos:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/tukaani/xz/lz/BT4;->hash:Lorg/tukaani/xz/lz/Hash234;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lorg/tukaani/xz/lz/Hash234;->getHash2Pos()I

    move-result v19

    sub-int v5, v18, v19

    .line 81
    .local v5, "delta2":I
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tukaani/xz/lz/BT4;->lzPos:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/tukaani/xz/lz/BT4;->hash:Lorg/tukaani/xz/lz/Hash234;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lorg/tukaani/xz/lz/Hash234;->getHash3Pos()I

    move-result v19

    sub-int v6, v18, v19

    .line 82
    .local v6, "delta3":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/tukaani/xz/lz/BT4;->hash:Lorg/tukaani/xz/lz/Hash234;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lorg/tukaani/xz/lz/Hash234;->getHash4Pos()I

    move-result v3

    .line 83
    .local v3, "currentMatch":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/tukaani/xz/lz/BT4;->hash:Lorg/tukaani/xz/lz/Hash234;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tukaani/xz/lz/BT4;->lzPos:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Lorg/tukaani/xz/lz/Hash234;->updateTables(I)V

    .line 85
    const/4 v12, 0x0

    .line 91
    .local v12, "lenBest":I
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tukaani/xz/lz/BT4;->cyclicSize:I

    move/from16 v18, v0

    move/from16 v0, v18

    if-ge v5, v0, :cond_dd

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/tukaani/xz/lz/BT4;->buf:[B

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tukaani/xz/lz/BT4;->readPos:I

    move/from16 v19, v0

    sub-int v19, v19, v5

    aget-byte v18, v18, v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/tukaani/xz/lz/BT4;->buf:[B

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tukaani/xz/lz/BT4;->readPos:I

    move/from16 v20, v0

    aget-byte v19, v19, v20

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_dd

    .line 92
    const/4 v12, 0x2

    .line 93
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/tukaani/xz/lz/BT4;->matches:Lorg/tukaani/xz/lz/Matches;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lorg/tukaani/xz/lz/Matches;->len:[I

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const/16 v20, 0x2

    aput v20, v18, v19

    .line 94
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/tukaani/xz/lz/BT4;->matches:Lorg/tukaani/xz/lz/Matches;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lorg/tukaani/xz/lz/Matches;->dist:[I

    move-object/from16 v18, v0

    const/16 v19, 0x0

    add-int/lit8 v20, v5, -0x1

    aput v20, v18, v19

    .line 95
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/tukaani/xz/lz/BT4;->matches:Lorg/tukaani/xz/lz/Matches;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    move/from16 v0, v19

    move-object/from16 v1, v18

    iput v0, v1, Lorg/tukaani/xz/lz/Matches;->count:I

    .line 102
    :cond_dd
    if-eq v5, v6, :cond_133

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tukaani/xz/lz/BT4;->cyclicSize:I

    move/from16 v18, v0

    move/from16 v0, v18

    if-ge v6, v0, :cond_133

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/tukaani/xz/lz/BT4;->buf:[B

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tukaani/xz/lz/BT4;->readPos:I

    move/from16 v19, v0

    sub-int v19, v19, v6

    aget-byte v18, v18, v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/tukaani/xz/lz/BT4;->buf:[B

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tukaani/xz/lz/BT4;->readPos:I

    move/from16 v20, v0

    aget-byte v19, v19, v20

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_133

    .line 104
    const/4 v12, 0x3

    .line 105
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/tukaani/xz/lz/BT4;->matches:Lorg/tukaani/xz/lz/Matches;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lorg/tukaani/xz/lz/Matches;->dist:[I

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/tukaani/xz/lz/BT4;->matches:Lorg/tukaani/xz/lz/Matches;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lorg/tukaani/xz/lz/Matches;->count:I

    move/from16 v20, v0

    add-int/lit8 v21, v20, 0x1

    move/from16 v0, v21

    move-object/from16 v1, v19

    iput v0, v1, Lorg/tukaani/xz/lz/Matches;->count:I

    add-int/lit8 v19, v6, -0x1

    aput v19, v18, v20

    .line 106
    move v5, v6

    .line 110
    :cond_133
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/tukaani/xz/lz/BT4;->matches:Lorg/tukaani/xz/lz/Matches;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Lorg/tukaani/xz/lz/Matches;->count:I

    move/from16 v18, v0

    if-lez v18, :cond_199

    .line 111
    :goto_141
    if-ge v12, v13, :cond_16e

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/tukaani/xz/lz/BT4;->buf:[B

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tukaani/xz/lz/BT4;->readPos:I

    move/from16 v19, v0

    add-int v19, v19, v12

    sub-int v19, v19, v5

    aget-byte v18, v18, v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/tukaani/xz/lz/BT4;->buf:[B

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tukaani/xz/lz/BT4;->readPos:I

    move/from16 v20, v0

    add-int v20, v20, v12

    aget-byte v19, v19, v20

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_16e

    .line 113
    add-int/lit8 v12, v12, 0x1

    goto :goto_141

    .line 115
    :cond_16e
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/tukaani/xz/lz/BT4;->matches:Lorg/tukaani/xz/lz/Matches;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lorg/tukaani/xz/lz/Matches;->len:[I

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/tukaani/xz/lz/BT4;->matches:Lorg/tukaani/xz/lz/Matches;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lorg/tukaani/xz/lz/Matches;->count:I

    move/from16 v19, v0

    add-int/lit8 v19, v19, -0x1

    aput v12, v18, v19

    .line 119
    if-lt v12, v14, :cond_199

    .line 120
    move-object/from16 v0, p0

    invoke-direct {v0, v14, v3}, Lorg/tukaani/xz/lz/BT4;->skip(II)V

    .line 121
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/tukaani/xz/lz/BT4;->matches:Lorg/tukaani/xz/lz/Matches;

    move-object/from16 v18, v0

    goto/16 :goto_24

    .line 127
    :cond_199
    const/16 v18, 0x3

    move/from16 v0, v18

    if-ge v12, v0, :cond_1a0

    .line 128
    const/4 v12, 0x3

    .line 130
    :cond_1a0
    move-object/from16 v0, p0

    iget v7, v0, Lorg/tukaani/xz/lz/BT4;->depthLimit:I

    .line 132
    .local v7, "depth":I
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tukaani/xz/lz/BT4;->cyclicPos:I

    move/from16 v18, v0

    shl-int/lit8 v18, v18, 0x1

    add-int/lit8 v16, v18, 0x1

    .line 133
    .local v16, "ptr0":I
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tukaani/xz/lz/BT4;->cyclicPos:I

    move/from16 v18, v0

    shl-int/lit8 v17, v18, 0x1

    .line 134
    .local v17, "ptr1":I
    const/4 v10, 0x0

    .line 135
    .local v10, "len0":I
    const/4 v11, 0x0

    .line 138
    .local v11, "len1":I
    :goto_1b8
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tukaani/xz/lz/BT4;->lzPos:I

    move/from16 v18, v0

    sub-int v4, v18, v3

    .line 143
    .local v4, "delta":I
    add-int/lit8 v8, v7, -0x1

    .end local v7    # "depth":I
    .local v8, "depth":I
    if-eqz v7, :cond_1ce

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tukaani/xz/lz/BT4;->cyclicSize:I

    move/from16 v18, v0

    move/from16 v0, v18

    if-lt v4, v0, :cond_1ea

    .line 144
    :cond_1ce
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/tukaani/xz/lz/BT4;->tree:[I

    move-object/from16 v18, v0

    const/16 v19, 0x0

    aput v19, v18, v16

    .line 145
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/tukaani/xz/lz/BT4;->tree:[I

    move-object/from16 v18, v0

    const/16 v19, 0x0

    aput v19, v18, v17

    .line 146
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/tukaani/xz/lz/BT4;->matches:Lorg/tukaani/xz/lz/Matches;

    move-object/from16 v18, v0

    goto/16 :goto_24

    .line 149
    :cond_1ea
    move-object/from16 v0, p0

    iget v0, v0, Lorg/tukaani/xz/lz/BT4;->cyclicPos:I

    move/from16 v18, v0

    sub-int v19, v18, v4

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tukaani/xz/lz/BT4;->cyclicPos:I

    move/from16 v18, v0

    move/from16 v0, v18

    if-le v4, v0, :cond_2d7

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tukaani/xz/lz/BT4;->cyclicSize:I

    move/from16 v18, v0

    :goto_202
    add-int v18, v18, v19

    shl-int/lit8 v15, v18, 0x1

    .line 151
    .local v15, "pair":I
    invoke-static {v10, v11}, Ljava/lang/Math;->min(II)I

    move-result v9

    .line 153
    .local v9, "len":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/tukaani/xz/lz/BT4;->buf:[B

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tukaani/xz/lz/BT4;->readPos:I

    move/from16 v19, v0

    add-int v19, v19, v9

    sub-int v19, v19, v4

    aget-byte v18, v18, v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/tukaani/xz/lz/BT4;->buf:[B

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tukaani/xz/lz/BT4;->readPos:I

    move/from16 v20, v0

    add-int v20, v20, v9

    aget-byte v19, v19, v20

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_2db

    .line 154
    :cond_232
    add-int/lit8 v9, v9, 0x1

    if-ge v9, v13, :cond_25e

    .line 155
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/tukaani/xz/lz/BT4;->buf:[B

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tukaani/xz/lz/BT4;->readPos:I

    move/from16 v19, v0

    add-int v19, v19, v9

    sub-int v19, v19, v4

    aget-byte v18, v18, v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/tukaani/xz/lz/BT4;->buf:[B

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tukaani/xz/lz/BT4;->readPos:I

    move/from16 v20, v0

    add-int v20, v20, v9

    aget-byte v19, v19, v20

    move/from16 v0, v18

    move/from16 v1, v19

    if-eq v0, v1, :cond_232

    .line 158
    :cond_25e
    if-le v9, v12, :cond_2db

    .line 159
    move v12, v9

    .line 160
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/tukaani/xz/lz/BT4;->matches:Lorg/tukaani/xz/lz/Matches;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lorg/tukaani/xz/lz/Matches;->len:[I

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/tukaani/xz/lz/BT4;->matches:Lorg/tukaani/xz/lz/Matches;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lorg/tukaani/xz/lz/Matches;->count:I

    move/from16 v19, v0

    aput v9, v18, v19

    .line 161
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/tukaani/xz/lz/BT4;->matches:Lorg/tukaani/xz/lz/Matches;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lorg/tukaani/xz/lz/Matches;->dist:[I

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/tukaani/xz/lz/BT4;->matches:Lorg/tukaani/xz/lz/Matches;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lorg/tukaani/xz/lz/Matches;->count:I

    move/from16 v19, v0

    add-int/lit8 v20, v4, -0x1

    aput v20, v18, v19

    .line 162
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/tukaani/xz/lz/BT4;->matches:Lorg/tukaani/xz/lz/Matches;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Lorg/tukaani/xz/lz/Matches;->count:I

    move/from16 v19, v0

    add-int/lit8 v19, v19, 0x1

    move/from16 v0, v19

    move-object/from16 v1, v18

    iput v0, v1, Lorg/tukaani/xz/lz/Matches;->count:I

    .line 164
    if-lt v9, v14, :cond_2db

    .line 165
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/tukaani/xz/lz/BT4;->tree:[I

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/tukaani/xz/lz/BT4;->tree:[I

    move-object/from16 v19, v0

    aget v19, v19, v15

    aput v19, v18, v17

    .line 166
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/tukaani/xz/lz/BT4;->tree:[I

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/tukaani/xz/lz/BT4;->tree:[I

    move-object/from16 v19, v0

    add-int/lit8 v20, v15, 0x1

    aget v19, v19, v20

    aput v19, v18, v16

    .line 167
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/tukaani/xz/lz/BT4;->matches:Lorg/tukaani/xz/lz/Matches;

    move-object/from16 v18, v0

    goto/16 :goto_24

    .line 149
    .end local v9    # "len":I
    .end local v15    # "pair":I
    :cond_2d7
    const/16 v18, 0x0

    goto/16 :goto_202

    .line 172
    .restart local v9    # "len":I
    .restart local v15    # "pair":I
    :cond_2db
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/tukaani/xz/lz/BT4;->buf:[B

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tukaani/xz/lz/BT4;->readPos:I

    move/from16 v19, v0

    add-int v19, v19, v9

    sub-int v19, v19, v4

    aget-byte v18, v18, v19

    move/from16 v0, v18

    and-int/lit16 v0, v0, 0xff

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/tukaani/xz/lz/BT4;->buf:[B

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tukaani/xz/lz/BT4;->readPos:I

    move/from16 v20, v0

    add-int v20, v20, v9

    aget-byte v19, v19, v20

    move/from16 v0, v19

    and-int/lit16 v0, v0, 0xff

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_325

    .line 174
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/tukaani/xz/lz/BT4;->tree:[I

    move-object/from16 v18, v0

    aput v3, v18, v17

    .line 175
    add-int/lit8 v17, v15, 0x1

    .line 176
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/tukaani/xz/lz/BT4;->tree:[I

    move-object/from16 v18, v0

    aget v3, v18, v17

    .line 177
    move v11, v9

    :goto_322
    move v7, v8

    .line 184
    .end local v8    # "depth":I
    .restart local v7    # "depth":I
    goto/16 :goto_1b8

    .line 179
    .end local v7    # "depth":I
    .restart local v8    # "depth":I
    :cond_325
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/tukaani/xz/lz/BT4;->tree:[I

    move-object/from16 v18, v0

    aput v3, v18, v16

    .line 180
    move/from16 v16, v15

    .line 181
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/tukaani/xz/lz/BT4;->tree:[I

    move-object/from16 v18, v0

    aget v3, v18, v16

    .line 182
    move v10, v9

    goto :goto_322
.end method

.method public skip(I)V
    .registers 9
    .param p1, "len"    # I

    .prologue
    .line 237
    move v2, p1

    .end local p1    # "len":I
    .local v2, "len":I
    :goto_1
    add-int/lit8 p1, v2, -0x1

    .end local v2    # "len":I
    .restart local p1    # "len":I
    if-lez v2, :cond_2d

    .line 238
    iget v3, p0, Lorg/tukaani/xz/lz/BT4;->niceLen:I

    .line 239
    .local v3, "niceLenLimit":I
    invoke-direct {p0}, Lorg/tukaani/xz/lz/BT4;->movePos()I

    move-result v0

    .line 241
    .local v0, "avail":I
    if-ge v0, v3, :cond_12

    .line 242
    if-nez v0, :cond_11

    move v2, p1

    .line 243
    .end local p1    # "len":I
    .restart local v2    # "len":I
    goto :goto_1

    .line 245
    .end local v2    # "len":I
    .restart local p1    # "len":I
    :cond_11
    move v3, v0

    .line 248
    :cond_12
    iget-object v4, p0, Lorg/tukaani/xz/lz/BT4;->hash:Lorg/tukaani/xz/lz/Hash234;

    iget-object v5, p0, Lorg/tukaani/xz/lz/BT4;->buf:[B

    iget v6, p0, Lorg/tukaani/xz/lz/BT4;->readPos:I

    invoke-virtual {v4, v5, v6}, Lorg/tukaani/xz/lz/Hash234;->calcHashes([BI)V

    .line 249
    iget-object v4, p0, Lorg/tukaani/xz/lz/BT4;->hash:Lorg/tukaani/xz/lz/Hash234;

    invoke-virtual {v4}, Lorg/tukaani/xz/lz/Hash234;->getHash4Pos()I

    move-result v1

    .line 250
    .local v1, "currentMatch":I
    iget-object v4, p0, Lorg/tukaani/xz/lz/BT4;->hash:Lorg/tukaani/xz/lz/Hash234;

    iget v5, p0, Lorg/tukaani/xz/lz/BT4;->lzPos:I

    invoke-virtual {v4, v5}, Lorg/tukaani/xz/lz/Hash234;->updateTables(I)V

    .line 252
    invoke-direct {p0, v3, v1}, Lorg/tukaani/xz/lz/BT4;->skip(II)V

    move v2, p1

    .line 253
    .end local p1    # "len":I
    .restart local v2    # "len":I
    goto :goto_1

    .line 254
    .end local v0    # "avail":I
    .end local v1    # "currentMatch":I
    .end local v2    # "len":I
    .end local v3    # "niceLenLimit":I
    .restart local p1    # "len":I
    :cond_2d
    return-void
.end method
