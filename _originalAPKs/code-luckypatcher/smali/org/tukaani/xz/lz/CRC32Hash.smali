.class Lorg/tukaani/xz/lz/CRC32Hash;
.super Ljava/lang/Object;
.source "CRC32Hash.java"


# static fields
.field private static final CRC32_POLY:I = -0x12477ce0

.field static final crcTable:[I


# direct methods
.method static constructor <clinit>()V
    .registers 6

    .prologue
    const/16 v5, 0x100

    .line 19
    new-array v3, v5, [I

    sput-object v3, Lorg/tukaani/xz/lz/CRC32Hash;->crcTable:[I

    .line 22
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_7
    if-ge v0, v5, :cond_27

    .line 23
    move v2, v0

    .line 25
    .local v2, "r":I
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_b
    const/16 v3, 0x8

    if-ge v1, v3, :cond_20

    .line 26
    and-int/lit8 v3, v2, 0x1

    if-eqz v3, :cond_1d

    .line 27
    ushr-int/lit8 v3, v2, 0x1

    const v4, -0x12477ce0

    xor-int v2, v3, v4

    .line 25
    :goto_1a
    add-int/lit8 v1, v1, 0x1

    goto :goto_b

    .line 29
    :cond_1d
    ushr-int/lit8 v2, v2, 0x1

    goto :goto_1a

    .line 32
    :cond_20
    sget-object v3, Lorg/tukaani/xz/lz/CRC32Hash;->crcTable:[I

    aput v2, v3, v0

    .line 22
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 34
    .end local v1    # "j":I
    .end local v2    # "r":I
    :cond_27
    return-void
.end method

.method constructor <init>()V
    .registers 1

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
