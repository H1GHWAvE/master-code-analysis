.class public abstract Lorg/tukaani/xz/lz/LZEncoder;
.super Ljava/lang/Object;
.source "LZEncoder.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final MF_BT4:I = 0x14

.field public static final MF_HC4:I = 0x4


# instance fields
.field final buf:[B

.field private finishing:Z

.field private final keepSizeAfter:I

.field private final keepSizeBefore:I

.field final matchLenMax:I

.field final niceLen:I

.field private pendingSize:I

.field private readLimit:I

.field readPos:I

.field private writePos:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 16
    const-class v0, Lorg/tukaani/xz/lz/LZEncoder;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_c

    const/4 v0, 0x1

    :goto_9
    sput-boolean v0, Lorg/tukaani/xz/lz/LZEncoder;->$assertionsDisabled:Z

    return-void

    :cond_c
    const/4 v0, 0x0

    goto :goto_9
.end method

.method constructor <init>(IIIII)V
    .registers 8
    .param p1, "dictSize"    # I
    .param p2, "extraSizeBefore"    # I
    .param p3, "extraSizeAfter"    # I
    .param p4, "niceLen"    # I
    .param p5, "matchLenMax"    # I

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 137
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput v1, p0, Lorg/tukaani/xz/lz/LZEncoder;->readPos:I

    .line 40
    iput v1, p0, Lorg/tukaani/xz/lz/LZEncoder;->readLimit:I

    .line 41
    iput-boolean v0, p0, Lorg/tukaani/xz/lz/LZEncoder;->finishing:Z

    .line 42
    iput v0, p0, Lorg/tukaani/xz/lz/LZEncoder;->writePos:I

    .line 43
    iput v0, p0, Lorg/tukaani/xz/lz/LZEncoder;->pendingSize:I

    .line 138
    invoke-static {p1, p2, p3, p5}, Lorg/tukaani/xz/lz/LZEncoder;->getBufSize(IIII)I

    move-result v0

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/tukaani/xz/lz/LZEncoder;->buf:[B

    .line 141
    add-int v0, p2, p1

    iput v0, p0, Lorg/tukaani/xz/lz/LZEncoder;->keepSizeBefore:I

    .line 142
    add-int v0, p3, p5

    iput v0, p0, Lorg/tukaani/xz/lz/LZEncoder;->keepSizeAfter:I

    .line 144
    iput p5, p0, Lorg/tukaani/xz/lz/LZEncoder;->matchLenMax:I

    .line 145
    iput p4, p0, Lorg/tukaani/xz/lz/LZEncoder;->niceLen:I

    .line 146
    return-void
.end method

.method private static getBufSize(IIII)I
    .registers 9
    .param p0, "dictSize"    # I
    .param p1, "extraSizeBefore"    # I
    .param p2, "extraSizeAfter"    # I
    .param p3, "matchLenMax"    # I

    .prologue
    .line 60
    add-int v1, p1, p0

    .line 61
    .local v1, "keepSizeBefore":I
    add-int v0, p2, p3

    .line 62
    .local v0, "keepSizeAfter":I
    div-int/lit8 v3, p0, 0x2

    const/high16 v4, 0x40000

    add-int/2addr v3, v4

    const/high16 v4, 0x20000000

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 63
    .local v2, "reserveSize":I
    add-int v3, v1, v0

    add-int/2addr v3, v2

    return v3
.end method

.method public static getInstance(IIIIIII)Lorg/tukaani/xz/lz/LZEncoder;
    .registers 14
    .param p0, "dictSize"    # I
    .param p1, "extraSizeBefore"    # I
    .param p2, "extraSizeAfter"    # I
    .param p3, "niceLen"    # I
    .param p4, "matchLenMax"    # I
    .param p5, "mf"    # I
    .param p6, "depthLimit"    # I

    .prologue
    .line 120
    sparse-switch p5, :sswitch_data_22

    .line 130
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 122
    :sswitch_9
    new-instance v0, Lorg/tukaani/xz/lz/HC4;

    move v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p6

    invoke-direct/range {v0 .. v6}, Lorg/tukaani/xz/lz/HC4;-><init>(IIIIII)V

    .line 126
    :goto_14
    return-object v0

    :sswitch_15
    new-instance v0, Lorg/tukaani/xz/lz/BT4;

    move v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p6

    invoke-direct/range {v0 .. v6}, Lorg/tukaani/xz/lz/BT4;-><init>(IIIIII)V

    goto :goto_14

    .line 120
    nop

    :sswitch_data_22
    .sparse-switch
        0x4 -> :sswitch_9
        0x14 -> :sswitch_15
    .end sparse-switch
.end method

.method public static getMemoryUsage(IIIII)I
    .registers 7
    .param p0, "dictSize"    # I
    .param p1, "extraSizeBefore"    # I
    .param p2, "extraSizeAfter"    # I
    .param p3, "matchLenMax"    # I
    .param p4, "mf"    # I

    .prologue
    .line 74
    invoke-static {p0, p1, p2, p3}, Lorg/tukaani/xz/lz/LZEncoder;->getBufSize(IIII)I

    move-result v1

    div-int/lit16 v1, v1, 0x400

    add-int/lit8 v0, v1, 0xa

    .line 77
    .local v0, "m":I
    sparse-switch p4, :sswitch_data_1e

    .line 87
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v1

    .line 79
    :sswitch_11
    invoke-static {p0}, Lorg/tukaani/xz/lz/HC4;->getMemoryUsage(I)I

    move-result v1

    add-int/2addr v0, v1

    .line 90
    :goto_16
    return v0

    .line 83
    :sswitch_17
    invoke-static {p0}, Lorg/tukaani/xz/lz/BT4;->getMemoryUsage(I)I

    move-result v1

    add-int/2addr v0, v1

    .line 84
    goto :goto_16

    .line 77
    nop

    :sswitch_data_1e
    .sparse-switch
        0x4 -> :sswitch_11
        0x14 -> :sswitch_17
    .end sparse-switch
.end method

.method private moveWindow()V
    .registers 6

    .prologue
    .line 176
    iget v2, p0, Lorg/tukaani/xz/lz/LZEncoder;->readPos:I

    add-int/lit8 v2, v2, 0x1

    iget v3, p0, Lorg/tukaani/xz/lz/LZEncoder;->keepSizeBefore:I

    sub-int/2addr v2, v3

    and-int/lit8 v0, v2, -0x10

    .line 177
    .local v0, "moveOffset":I
    iget v2, p0, Lorg/tukaani/xz/lz/LZEncoder;->writePos:I

    sub-int v1, v2, v0

    .line 178
    .local v1, "moveSize":I
    iget-object v2, p0, Lorg/tukaani/xz/lz/LZEncoder;->buf:[B

    iget-object v3, p0, Lorg/tukaani/xz/lz/LZEncoder;->buf:[B

    const/4 v4, 0x0

    invoke-static {v2, v0, v3, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 180
    iget v2, p0, Lorg/tukaani/xz/lz/LZEncoder;->readPos:I

    sub-int/2addr v2, v0

    iput v2, p0, Lorg/tukaani/xz/lz/LZEncoder;->readPos:I

    .line 181
    iget v2, p0, Lorg/tukaani/xz/lz/LZEncoder;->readLimit:I

    sub-int/2addr v2, v0

    iput v2, p0, Lorg/tukaani/xz/lz/LZEncoder;->readLimit:I

    .line 182
    iget v2, p0, Lorg/tukaani/xz/lz/LZEncoder;->writePos:I

    sub-int/2addr v2, v0

    iput v2, p0, Lorg/tukaani/xz/lz/LZEncoder;->writePos:I

    .line 183
    return-void
.end method

.method static normalize([II)V
    .registers 4
    .param p0, "positions"    # [I
    .param p1, "normalizationOffset"    # I

    .prologue
    .line 46
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v1, p0

    if-ge v0, v1, :cond_14

    .line 47
    aget v1, p0, v0

    if-gt v1, p1, :cond_e

    .line 48
    const/4 v1, 0x0

    aput v1, p0, v0

    .line 46
    :goto_b
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 50
    :cond_e
    aget v1, p0, v0

    sub-int/2addr v1, p1

    aput v1, p0, v0

    goto :goto_b

    .line 52
    :cond_14
    return-void
.end method

.method private processPendingBytes()V
    .registers 4

    .prologue
    .line 228
    iget v1, p0, Lorg/tukaani/xz/lz/LZEncoder;->pendingSize:I

    if-lez v1, :cond_27

    iget v1, p0, Lorg/tukaani/xz/lz/LZEncoder;->readPos:I

    iget v2, p0, Lorg/tukaani/xz/lz/LZEncoder;->readLimit:I

    if-ge v1, v2, :cond_27

    .line 229
    iget v1, p0, Lorg/tukaani/xz/lz/LZEncoder;->readPos:I

    iget v2, p0, Lorg/tukaani/xz/lz/LZEncoder;->pendingSize:I

    sub-int/2addr v1, v2

    iput v1, p0, Lorg/tukaani/xz/lz/LZEncoder;->readPos:I

    .line 230
    iget v0, p0, Lorg/tukaani/xz/lz/LZEncoder;->pendingSize:I

    .line 231
    .local v0, "oldPendingSize":I
    const/4 v1, 0x0

    iput v1, p0, Lorg/tukaani/xz/lz/LZEncoder;->pendingSize:I

    .line 232
    invoke-virtual {p0, v0}, Lorg/tukaani/xz/lz/LZEncoder;->skip(I)V

    .line 233
    sget-boolean v1, Lorg/tukaani/xz/lz/LZEncoder;->$assertionsDisabled:Z

    if-nez v1, :cond_27

    iget v1, p0, Lorg/tukaani/xz/lz/LZEncoder;->pendingSize:I

    if-lt v1, v0, :cond_27

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 235
    .end local v0    # "oldPendingSize":I
    :cond_27
    return-void
.end method


# virtual methods
.method public copyUncompressed(Ljava/io/OutputStream;II)V
    .registers 6
    .param p1, "out"    # Ljava/io/OutputStream;
    .param p2, "backward"    # I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 274
    iget-object v0, p0, Lorg/tukaani/xz/lz/LZEncoder;->buf:[B

    iget v1, p0, Lorg/tukaani/xz/lz/LZEncoder;->readPos:I

    add-int/lit8 v1, v1, 0x1

    sub-int/2addr v1, p2

    invoke-virtual {p1, v0, v1, p3}, Ljava/io/OutputStream;->write([BII)V

    .line 275
    return-void
.end method

.method public fillWindow([BII)I
    .registers 7
    .param p1, "in"    # [B
    .param p2, "off"    # I
    .param p3, "len"    # I

    .prologue
    .line 189
    sget-boolean v0, Lorg/tukaani/xz/lz/LZEncoder;->$assertionsDisabled:Z

    if-nez v0, :cond_e

    iget-boolean v0, p0, Lorg/tukaani/xz/lz/LZEncoder;->finishing:Z

    if-eqz v0, :cond_e

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 192
    :cond_e
    iget v0, p0, Lorg/tukaani/xz/lz/LZEncoder;->readPos:I

    iget-object v1, p0, Lorg/tukaani/xz/lz/LZEncoder;->buf:[B

    array-length v1, v1

    iget v2, p0, Lorg/tukaani/xz/lz/LZEncoder;->keepSizeAfter:I

    sub-int/2addr v1, v2

    if-lt v0, v1, :cond_1b

    .line 193
    invoke-direct {p0}, Lorg/tukaani/xz/lz/LZEncoder;->moveWindow()V

    .line 197
    :cond_1b
    iget-object v0, p0, Lorg/tukaani/xz/lz/LZEncoder;->buf:[B

    array-length v0, v0

    iget v1, p0, Lorg/tukaani/xz/lz/LZEncoder;->writePos:I

    sub-int/2addr v0, v1

    if-le p3, v0, :cond_2a

    .line 198
    iget-object v0, p0, Lorg/tukaani/xz/lz/LZEncoder;->buf:[B

    array-length v0, v0

    iget v1, p0, Lorg/tukaani/xz/lz/LZEncoder;->writePos:I

    sub-int p3, v0, v1

    .line 200
    :cond_2a
    iget-object v0, p0, Lorg/tukaani/xz/lz/LZEncoder;->buf:[B

    iget v1, p0, Lorg/tukaani/xz/lz/LZEncoder;->writePos:I

    invoke-static {p1, p2, v0, v1, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 201
    iget v0, p0, Lorg/tukaani/xz/lz/LZEncoder;->writePos:I

    add-int/2addr v0, p3

    iput v0, p0, Lorg/tukaani/xz/lz/LZEncoder;->writePos:I

    .line 205
    iget v0, p0, Lorg/tukaani/xz/lz/LZEncoder;->writePos:I

    iget v1, p0, Lorg/tukaani/xz/lz/LZEncoder;->keepSizeAfter:I

    if-lt v0, v1, :cond_43

    .line 206
    iget v0, p0, Lorg/tukaani/xz/lz/LZEncoder;->writePos:I

    iget v1, p0, Lorg/tukaani/xz/lz/LZEncoder;->keepSizeAfter:I

    sub-int/2addr v0, v1

    iput v0, p0, Lorg/tukaani/xz/lz/LZEncoder;->readLimit:I

    .line 208
    :cond_43
    invoke-direct {p0}, Lorg/tukaani/xz/lz/LZEncoder;->processPendingBytes()V

    .line 212
    return p3
.end method

.method public getAvail()I
    .registers 3

    .prologue
    .line 285
    sget-boolean v0, Lorg/tukaani/xz/lz/LZEncoder;->$assertionsDisabled:Z

    if-nez v0, :cond_10

    invoke-virtual {p0}, Lorg/tukaani/xz/lz/LZEncoder;->isStarted()Z

    move-result v0

    if-nez v0, :cond_10

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 286
    :cond_10
    iget v0, p0, Lorg/tukaani/xz/lz/LZEncoder;->writePos:I

    iget v1, p0, Lorg/tukaani/xz/lz/LZEncoder;->readPos:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public getByte(I)I
    .registers 4
    .param p1, "backward"    # I

    .prologue
    .line 307
    iget-object v0, p0, Lorg/tukaani/xz/lz/LZEncoder;->buf:[B

    iget v1, p0, Lorg/tukaani/xz/lz/LZEncoder;->readPos:I

    sub-int/2addr v1, p1

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    return v0
.end method

.method public getByte(II)I
    .registers 5
    .param p1, "forward"    # I
    .param p2, "backward"    # I

    .prologue
    .line 316
    iget-object v0, p0, Lorg/tukaani/xz/lz/LZEncoder;->buf:[B

    iget v1, p0, Lorg/tukaani/xz/lz/LZEncoder;->readPos:I

    add-int/2addr v1, p1

    sub-int/2addr v1, p2

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    return v0
.end method

.method public getMatchLen(II)I
    .registers 8
    .param p1, "dist"    # I
    .param p2, "lenLimit"    # I

    .prologue
    .line 328
    iget v2, p0, Lorg/tukaani/xz/lz/LZEncoder;->readPos:I

    sub-int/2addr v2, p1

    add-int/lit8 v0, v2, -0x1

    .line 329
    .local v0, "backPos":I
    const/4 v1, 0x0

    .line 331
    .local v1, "len":I
    :goto_6
    if-ge v1, p2, :cond_1a

    iget-object v2, p0, Lorg/tukaani/xz/lz/LZEncoder;->buf:[B

    iget v3, p0, Lorg/tukaani/xz/lz/LZEncoder;->readPos:I

    add-int/2addr v3, v1

    aget-byte v2, v2, v3

    iget-object v3, p0, Lorg/tukaani/xz/lz/LZEncoder;->buf:[B

    add-int v4, v0, v1

    aget-byte v3, v3, v4

    if-ne v2, v3, :cond_1a

    .line 332
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 334
    :cond_1a
    return v1
.end method

.method public getMatchLen(III)I
    .registers 10
    .param p1, "forward"    # I
    .param p2, "dist"    # I
    .param p3, "lenLimit"    # I

    .prologue
    .line 347
    iget v3, p0, Lorg/tukaani/xz/lz/LZEncoder;->readPos:I

    add-int v1, v3, p1

    .line 348
    .local v1, "curPos":I
    sub-int v3, v1, p2

    add-int/lit8 v0, v3, -0x1

    .line 349
    .local v0, "backPos":I
    const/4 v2, 0x0

    .line 351
    .local v2, "len":I
    :goto_9
    if-ge v2, p3, :cond_1c

    iget-object v3, p0, Lorg/tukaani/xz/lz/LZEncoder;->buf:[B

    add-int v4, v1, v2

    aget-byte v3, v3, v4

    iget-object v4, p0, Lorg/tukaani/xz/lz/LZEncoder;->buf:[B

    add-int v5, v0, v2

    aget-byte v4, v4, v5

    if-ne v3, v4, :cond_1c

    .line 352
    add-int/lit8 v2, v2, 0x1

    goto :goto_9

    .line 354
    :cond_1c
    return v2
.end method

.method public abstract getMatches()Lorg/tukaani/xz/lz/Matches;
.end method

.method public getPos()I
    .registers 2

    .prologue
    .line 294
    iget v0, p0, Lorg/tukaani/xz/lz/LZEncoder;->readPos:I

    return v0
.end method

.method public hasEnoughData(I)Z
    .registers 4
    .param p1, "alreadyReadLen"    # I

    .prologue
    .line 269
    iget v0, p0, Lorg/tukaani/xz/lz/LZEncoder;->readPos:I

    sub-int/2addr v0, p1

    iget v1, p0, Lorg/tukaani/xz/lz/LZEncoder;->readLimit:I

    if-ge v0, v1, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public isStarted()Z
    .registers 3

    .prologue
    .line 242
    iget v0, p0, Lorg/tukaani/xz/lz/LZEncoder;->readPos:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_7

    const/4 v0, 0x1

    :goto_6
    return v0

    :cond_7
    const/4 v0, 0x0

    goto :goto_6
.end method

.method movePos(II)I
    .registers 6
    .param p1, "requiredForFlushing"    # I
    .param p2, "requiredForFinishing"    # I

    .prologue
    .line 395
    sget-boolean v1, Lorg/tukaani/xz/lz/LZEncoder;->$assertionsDisabled:Z

    if-nez v1, :cond_c

    if-ge p1, p2, :cond_c

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 397
    :cond_c
    iget v1, p0, Lorg/tukaani/xz/lz/LZEncoder;->readPos:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/tukaani/xz/lz/LZEncoder;->readPos:I

    .line 398
    iget v1, p0, Lorg/tukaani/xz/lz/LZEncoder;->writePos:I

    iget v2, p0, Lorg/tukaani/xz/lz/LZEncoder;->readPos:I

    sub-int v0, v1, v2

    .line 400
    .local v0, "avail":I
    if-ge v0, p1, :cond_27

    .line 401
    if-lt v0, p2, :cond_20

    iget-boolean v1, p0, Lorg/tukaani/xz/lz/LZEncoder;->finishing:Z

    if-nez v1, :cond_27

    .line 402
    :cond_20
    iget v1, p0, Lorg/tukaani/xz/lz/LZEncoder;->pendingSize:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/tukaani/xz/lz/LZEncoder;->pendingSize:I

    .line 403
    const/4 v0, 0x0

    .line 407
    :cond_27
    return v0
.end method

.method public setFinishing()V
    .registers 2

    .prologue
    .line 259
    iget v0, p0, Lorg/tukaani/xz/lz/LZEncoder;->writePos:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/tukaani/xz/lz/LZEncoder;->readLimit:I

    .line 260
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/tukaani/xz/lz/LZEncoder;->finishing:Z

    .line 261
    invoke-direct {p0}, Lorg/tukaani/xz/lz/LZEncoder;->processPendingBytes()V

    .line 262
    return-void
.end method

.method public setFlushing()V
    .registers 2

    .prologue
    .line 250
    iget v0, p0, Lorg/tukaani/xz/lz/LZEncoder;->writePos:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/tukaani/xz/lz/LZEncoder;->readLimit:I

    .line 251
    invoke-direct {p0}, Lorg/tukaani/xz/lz/LZEncoder;->processPendingBytes()V

    .line 252
    return-void
.end method

.method public setPresetDict(I[B)V
    .registers 7
    .param p1, "dictSize"    # I
    .param p2, "presetDict"    # [B

    .prologue
    .line 154
    sget-boolean v2, Lorg/tukaani/xz/lz/LZEncoder;->$assertionsDisabled:Z

    if-nez v2, :cond_10

    invoke-virtual {p0}, Lorg/tukaani/xz/lz/LZEncoder;->isStarted()Z

    move-result v2

    if-eqz v2, :cond_10

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 155
    :cond_10
    sget-boolean v2, Lorg/tukaani/xz/lz/LZEncoder;->$assertionsDisabled:Z

    if-nez v2, :cond_1e

    iget v2, p0, Lorg/tukaani/xz/lz/LZEncoder;->writePos:I

    if-eqz v2, :cond_1e

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 157
    :cond_1e
    if-eqz p2, :cond_36

    .line 160
    array-length v2, p2

    invoke-static {v2, p1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 161
    .local v0, "copySize":I
    array-length v2, p2

    sub-int v1, v2, v0

    .line 162
    .local v1, "offset":I
    iget-object v2, p0, Lorg/tukaani/xz/lz/LZEncoder;->buf:[B

    const/4 v3, 0x0

    invoke-static {p2, v1, v2, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 163
    iget v2, p0, Lorg/tukaani/xz/lz/LZEncoder;->writePos:I

    add-int/2addr v2, v0

    iput v2, p0, Lorg/tukaani/xz/lz/LZEncoder;->writePos:I

    .line 164
    invoke-virtual {p0, v0}, Lorg/tukaani/xz/lz/LZEncoder;->skip(I)V

    .line 166
    .end local v0    # "copySize":I
    .end local v1    # "offset":I
    :cond_36
    return-void
.end method

.method public abstract skip(I)V
.end method

.method public verifyMatches(Lorg/tukaani/xz/lz/Matches;)Z
    .registers 6
    .param p1, "matches"    # Lorg/tukaani/xz/lz/Matches;

    .prologue
    .line 368
    invoke-virtual {p0}, Lorg/tukaani/xz/lz/LZEncoder;->getAvail()I

    move-result v2

    iget v3, p0, Lorg/tukaani/xz/lz/LZEncoder;->matchLenMax:I

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 370
    .local v1, "lenLimit":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_b
    iget v2, p1, Lorg/tukaani/xz/lz/Matches;->count:I

    if-ge v0, v2, :cond_22

    .line 371
    iget-object v2, p1, Lorg/tukaani/xz/lz/Matches;->dist:[I

    aget v2, v2, v0

    invoke-virtual {p0, v2, v1}, Lorg/tukaani/xz/lz/LZEncoder;->getMatchLen(II)I

    move-result v2

    iget-object v3, p1, Lorg/tukaani/xz/lz/Matches;->len:[I

    aget v3, v3, v0

    if-eq v2, v3, :cond_1f

    .line 372
    const/4 v2, 0x0

    .line 374
    :goto_1e
    return v2

    .line 370
    :cond_1f
    add-int/lit8 v0, v0, 0x1

    goto :goto_b

    .line 374
    :cond_22
    const/4 v2, 0x1

    goto :goto_1e
.end method
