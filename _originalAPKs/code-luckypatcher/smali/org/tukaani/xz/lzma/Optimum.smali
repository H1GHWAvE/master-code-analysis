.class final Lorg/tukaani/xz/lzma/Optimum;
.super Ljava/lang/Object;
.source "Optimum.java"


# static fields
.field private static final INFINITY_PRICE:I = 0x40000000


# instance fields
.field backPrev:I

.field backPrev2:I

.field hasPrev2:Z

.field optPrev:I

.field optPrev2:I

.field prev1IsLiteral:Z

.field price:I

.field final reps:[I

.field final state:Lorg/tukaani/xz/lzma/State;


# direct methods
.method constructor <init>()V
    .registers 2

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    new-instance v0, Lorg/tukaani/xz/lzma/State;

    invoke-direct {v0}, Lorg/tukaani/xz/lzma/State;-><init>()V

    iput-object v0, p0, Lorg/tukaani/xz/lzma/Optimum;->state:Lorg/tukaani/xz/lzma/State;

    .line 17
    const/4 v0, 0x4

    new-array v0, v0, [I

    iput-object v0, p0, Lorg/tukaani/xz/lzma/Optimum;->reps:[I

    return-void
.end method


# virtual methods
.method reset()V
    .registers 2

    .prologue
    .line 36
    const/high16 v0, 0x40000000    # 2.0f

    iput v0, p0, Lorg/tukaani/xz/lzma/Optimum;->price:I

    .line 37
    return-void
.end method

.method set1(III)V
    .registers 5
    .param p1, "newPrice"    # I
    .param p2, "optCur"    # I
    .param p3, "back"    # I

    .prologue
    .line 43
    iput p1, p0, Lorg/tukaani/xz/lzma/Optimum;->price:I

    .line 44
    iput p2, p0, Lorg/tukaani/xz/lzma/Optimum;->optPrev:I

    .line 45
    iput p3, p0, Lorg/tukaani/xz/lzma/Optimum;->backPrev:I

    .line 46
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/tukaani/xz/lzma/Optimum;->prev1IsLiteral:Z

    .line 47
    return-void
.end method

.method set2(III)V
    .registers 5
    .param p1, "newPrice"    # I
    .param p2, "optCur"    # I
    .param p3, "back"    # I

    .prologue
    .line 53
    iput p1, p0, Lorg/tukaani/xz/lzma/Optimum;->price:I

    .line 54
    add-int/lit8 v0, p2, 0x1

    iput v0, p0, Lorg/tukaani/xz/lzma/Optimum;->optPrev:I

    .line 55
    iput p3, p0, Lorg/tukaani/xz/lzma/Optimum;->backPrev:I

    .line 56
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/tukaani/xz/lzma/Optimum;->prev1IsLiteral:Z

    .line 57
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/tukaani/xz/lzma/Optimum;->hasPrev2:Z

    .line 58
    return-void
.end method

.method set3(IIIII)V
    .registers 8
    .param p1, "newPrice"    # I
    .param p2, "optCur"    # I
    .param p3, "back2"    # I
    .param p4, "len2"    # I
    .param p5, "back"    # I

    .prologue
    const/4 v1, 0x1

    .line 65
    iput p1, p0, Lorg/tukaani/xz/lzma/Optimum;->price:I

    .line 66
    add-int v0, p2, p4

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/tukaani/xz/lzma/Optimum;->optPrev:I

    .line 67
    iput p5, p0, Lorg/tukaani/xz/lzma/Optimum;->backPrev:I

    .line 68
    iput-boolean v1, p0, Lorg/tukaani/xz/lzma/Optimum;->prev1IsLiteral:Z

    .line 69
    iput-boolean v1, p0, Lorg/tukaani/xz/lzma/Optimum;->hasPrev2:Z

    .line 70
    iput p2, p0, Lorg/tukaani/xz/lzma/Optimum;->optPrev2:I

    .line 71
    iput p3, p0, Lorg/tukaani/xz/lzma/Optimum;->backPrev2:I

    .line 72
    return-void
.end method
