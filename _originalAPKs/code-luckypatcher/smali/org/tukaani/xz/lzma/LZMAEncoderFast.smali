.class final Lorg/tukaani/xz/lzma/LZMAEncoderFast;
.super Lorg/tukaani/xz/lzma/LZMAEncoder;
.source "LZMAEncoderFast.java"


# static fields
.field private static EXTRA_SIZE_AFTER:I

.field private static EXTRA_SIZE_BEFORE:I


# instance fields
.field private matches:Lorg/tukaani/xz/lz/Matches;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 18
    const/4 v0, 0x1

    sput v0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->EXTRA_SIZE_BEFORE:I

    .line 19
    const/16 v0, 0x110

    sput v0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->EXTRA_SIZE_AFTER:I

    return-void
.end method

.method constructor <init>(Lorg/tukaani/xz/rangecoder/RangeEncoder;IIIIIIII)V
    .registers 18
    .param p1, "rc"    # Lorg/tukaani/xz/rangecoder/RangeEncoder;
    .param p2, "lc"    # I
    .param p3, "lp"    # I
    .param p4, "pb"    # I
    .param p5, "dictSize"    # I
    .param p6, "extraSizeBefore"    # I
    .param p7, "niceLen"    # I
    .param p8, "mf"    # I
    .param p9, "depthLimit"    # I

    .prologue
    .line 32
    sget v0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->EXTRA_SIZE_BEFORE:I

    .line 33
    invoke-static {p6, v0}, Ljava/lang/Math;->max(II)I

    move-result v1

    sget v2, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->EXTRA_SIZE_AFTER:I

    const/16 v4, 0x111

    move v0, p5

    move v3, p7

    move/from16 v5, p8

    move/from16 v6, p9

    .line 32
    invoke-static/range {v0 .. v6}, Lorg/tukaani/xz/lz/LZEncoder;->getInstance(IIIIIII)Lorg/tukaani/xz/lz/LZEncoder;

    move-result-object v2

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move v7, p7

    invoke-direct/range {v0 .. v7}, Lorg/tukaani/xz/lzma/LZMAEncoder;-><init>(Lorg/tukaani/xz/rangecoder/RangeEncoder;Lorg/tukaani/xz/lz/LZEncoder;IIIII)V

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->matches:Lorg/tukaani/xz/lz/Matches;

    .line 39
    return-void
.end method

.method private changePair(II)Z
    .registers 4
    .param p1, "smallDist"    # I
    .param p2, "bigDist"    # I

    .prologue
    .line 42
    ushr-int/lit8 v0, p2, 0x7

    if-ge p1, v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method static getMemoryUsage(III)I
    .registers 6
    .param p0, "dictSize"    # I
    .param p1, "extraSizeBefore"    # I
    .param p2, "mf"    # I

    .prologue
    .line 24
    sget v0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->EXTRA_SIZE_BEFORE:I

    .line 25
    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    sget v1, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->EXTRA_SIZE_AFTER:I

    const/16 v2, 0x111

    .line 24
    invoke-static {p0, v0, v1, v2, p2}, Lorg/tukaani/xz/lz/LZEncoder;->getMemoryUsage(IIIII)I

    move-result v0

    return v0
.end method


# virtual methods
.method getNextSymbol()I
    .registers 16

    .prologue
    const/4 v14, 0x4

    const/4 v12, -0x1

    const/4 v10, 0x1

    const/4 v13, 0x2

    .line 49
    iget v11, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->readAhead:I

    if-ne v11, v12, :cond_e

    .line 50
    invoke-virtual {p0}, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->getMatches()Lorg/tukaani/xz/lz/Matches;

    move-result-object v11

    iput-object v11, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->matches:Lorg/tukaani/xz/lz/Matches;

    .line 52
    :cond_e
    iput v12, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->back:I

    .line 58
    iget-object v11, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->lz:Lorg/tukaani/xz/lz/LZEncoder;

    invoke-virtual {v11}, Lorg/tukaani/xz/lz/LZEncoder;->getAvail()I

    move-result v11

    const/16 v12, 0x111

    invoke-static {v11, v12}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 59
    .local v0, "avail":I
    if-ge v0, v13, :cond_20

    move v6, v10

    .line 149
    :goto_1f
    return v6

    .line 63
    :cond_20
    const/4 v2, 0x0

    .line 64
    .local v2, "bestRepLen":I
    const/4 v1, 0x0

    .line 65
    .local v1, "bestRepIndex":I
    const/4 v9, 0x0

    .local v9, "rep":I
    :goto_23
    if-ge v9, v14, :cond_46

    .line 66
    iget-object v11, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->lz:Lorg/tukaani/xz/lz/LZEncoder;

    iget-object v12, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->reps:[I

    aget v12, v12, v9

    invoke-virtual {v11, v12, v0}, Lorg/tukaani/xz/lz/LZEncoder;->getMatchLen(II)I

    move-result v3

    .line 67
    .local v3, "len":I
    if-ge v3, v13, :cond_34

    .line 65
    :cond_31
    :goto_31
    add-int/lit8 v9, v9, 0x1

    goto :goto_23

    .line 71
    :cond_34
    iget v11, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->niceLen:I

    if-lt v3, v11, :cond_41

    .line 72
    iput v9, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->back:I

    .line 73
    add-int/lit8 v10, v3, -0x1

    invoke-virtual {p0, v10}, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->skip(I)V

    move v6, v3

    .line 74
    goto :goto_1f

    .line 78
    :cond_41
    if-le v3, v2, :cond_31

    .line 79
    move v1, v9

    .line 80
    move v2, v3

    goto :goto_31

    .line 84
    .end local v3    # "len":I
    :cond_46
    const/4 v6, 0x0

    .line 85
    .local v6, "mainLen":I
    const/4 v5, 0x0

    .line 87
    .local v5, "mainDist":I
    iget-object v11, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->matches:Lorg/tukaani/xz/lz/Matches;

    iget v11, v11, Lorg/tukaani/xz/lz/Matches;->count:I

    if-lez v11, :cond_c3

    .line 88
    iget-object v11, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->matches:Lorg/tukaani/xz/lz/Matches;

    iget-object v11, v11, Lorg/tukaani/xz/lz/Matches;->len:[I

    iget-object v12, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->matches:Lorg/tukaani/xz/lz/Matches;

    iget v12, v12, Lorg/tukaani/xz/lz/Matches;->count:I

    add-int/lit8 v12, v12, -0x1

    aget v6, v11, v12

    .line 89
    iget-object v11, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->matches:Lorg/tukaani/xz/lz/Matches;

    iget-object v11, v11, Lorg/tukaani/xz/lz/Matches;->dist:[I

    iget-object v12, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->matches:Lorg/tukaani/xz/lz/Matches;

    iget v12, v12, Lorg/tukaani/xz/lz/Matches;->count:I

    add-int/lit8 v12, v12, -0x1

    aget v5, v11, v12

    .line 91
    iget v11, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->niceLen:I

    if-lt v6, v11, :cond_94

    .line 92
    add-int/lit8 v10, v5, 0x4

    iput v10, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->back:I

    .line 93
    add-int/lit8 v10, v6, -0x1

    invoke-virtual {p0, v10}, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->skip(I)V

    goto :goto_1f

    .line 102
    :cond_74
    iget-object v11, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->matches:Lorg/tukaani/xz/lz/Matches;

    iget v12, v11, Lorg/tukaani/xz/lz/Matches;->count:I

    add-int/lit8 v12, v12, -0x1

    iput v12, v11, Lorg/tukaani/xz/lz/Matches;->count:I

    .line 103
    iget-object v11, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->matches:Lorg/tukaani/xz/lz/Matches;

    iget-object v11, v11, Lorg/tukaani/xz/lz/Matches;->len:[I

    iget-object v12, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->matches:Lorg/tukaani/xz/lz/Matches;

    iget v12, v12, Lorg/tukaani/xz/lz/Matches;->count:I

    add-int/lit8 v12, v12, -0x1

    aget v6, v11, v12

    .line 104
    iget-object v11, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->matches:Lorg/tukaani/xz/lz/Matches;

    iget-object v11, v11, Lorg/tukaani/xz/lz/Matches;->dist:[I

    iget-object v12, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->matches:Lorg/tukaani/xz/lz/Matches;

    iget v12, v12, Lorg/tukaani/xz/lz/Matches;->count:I

    add-int/lit8 v12, v12, -0x1

    aget v5, v11, v12

    .line 97
    :cond_94
    iget-object v11, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->matches:Lorg/tukaani/xz/lz/Matches;

    iget v11, v11, Lorg/tukaani/xz/lz/Matches;->count:I

    if-le v11, v10, :cond_bc

    iget-object v11, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->matches:Lorg/tukaani/xz/lz/Matches;

    iget-object v11, v11, Lorg/tukaani/xz/lz/Matches;->len:[I

    iget-object v12, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->matches:Lorg/tukaani/xz/lz/Matches;

    iget v12, v12, Lorg/tukaani/xz/lz/Matches;->count:I

    add-int/lit8 v12, v12, -0x2

    aget v11, v11, v12

    add-int/lit8 v11, v11, 0x1

    if-ne v6, v11, :cond_bc

    .line 99
    iget-object v11, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->matches:Lorg/tukaani/xz/lz/Matches;

    iget-object v11, v11, Lorg/tukaani/xz/lz/Matches;->dist:[I

    iget-object v12, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->matches:Lorg/tukaani/xz/lz/Matches;

    iget v12, v12, Lorg/tukaani/xz/lz/Matches;->count:I

    add-int/lit8 v12, v12, -0x2

    aget v11, v11, v12

    invoke-direct {p0, v11, v5}, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->changePair(II)Z

    move-result v11

    if-nez v11, :cond_74

    .line 107
    :cond_bc
    if-ne v6, v13, :cond_c3

    const/16 v11, 0x80

    if-lt v5, v11, :cond_c3

    .line 108
    const/4 v6, 0x1

    .line 111
    :cond_c3
    if-lt v2, v13, :cond_e4

    .line 112
    add-int/lit8 v11, v2, 0x1

    if-ge v11, v6, :cond_da

    add-int/lit8 v11, v2, 0x2

    if-lt v11, v6, :cond_d1

    const/16 v11, 0x200

    if-ge v5, v11, :cond_da

    :cond_d1
    add-int/lit8 v11, v2, 0x3

    if-lt v11, v6, :cond_e4

    const v11, 0x8000

    if-lt v5, v11, :cond_e4

    .line 115
    :cond_da
    iput v1, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->back:I

    .line 116
    add-int/lit8 v10, v2, -0x1

    invoke-virtual {p0, v10}, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->skip(I)V

    move v6, v2

    .line 117
    goto/16 :goto_1f

    .line 121
    :cond_e4
    if-lt v6, v13, :cond_e8

    if-gt v0, v13, :cond_eb

    :cond_e8
    move v6, v10

    .line 122
    goto/16 :goto_1f

    .line 126
    :cond_eb
    invoke-virtual {p0}, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->getMatches()Lorg/tukaani/xz/lz/Matches;

    move-result-object v11

    iput-object v11, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->matches:Lorg/tukaani/xz/lz/Matches;

    .line 128
    iget-object v11, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->matches:Lorg/tukaani/xz/lz/Matches;

    iget v11, v11, Lorg/tukaani/xz/lz/Matches;->count:I

    if-lez v11, :cond_131

    .line 129
    iget-object v11, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->matches:Lorg/tukaani/xz/lz/Matches;

    iget-object v11, v11, Lorg/tukaani/xz/lz/Matches;->len:[I

    iget-object v12, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->matches:Lorg/tukaani/xz/lz/Matches;

    iget v12, v12, Lorg/tukaani/xz/lz/Matches;->count:I

    add-int/lit8 v12, v12, -0x1

    aget v8, v11, v12

    .line 130
    .local v8, "newLen":I
    iget-object v11, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->matches:Lorg/tukaani/xz/lz/Matches;

    iget-object v11, v11, Lorg/tukaani/xz/lz/Matches;->dist:[I

    iget-object v12, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->matches:Lorg/tukaani/xz/lz/Matches;

    iget v12, v12, Lorg/tukaani/xz/lz/Matches;->count:I

    add-int/lit8 v12, v12, -0x1

    aget v7, v11, v12

    .line 132
    .local v7, "newDist":I
    if-lt v8, v6, :cond_113

    if-lt v7, v5, :cond_12e

    :cond_113
    add-int/lit8 v11, v6, 0x1

    if-ne v8, v11, :cond_11d

    .line 134
    invoke-direct {p0, v5, v7}, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->changePair(II)Z

    move-result v11

    if-eqz v11, :cond_12e

    :cond_11d
    add-int/lit8 v11, v6, 0x1

    if-gt v8, v11, :cond_12e

    add-int/lit8 v11, v8, 0x1

    if-lt v11, v6, :cond_131

    const/4 v11, 0x3

    if-lt v6, v11, :cond_131

    .line 138
    invoke-direct {p0, v7, v5}, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->changePair(II)Z

    move-result v11

    if-eqz v11, :cond_131

    :cond_12e
    move v6, v10

    .line 139
    goto/16 :goto_1f

    .line 142
    .end local v7    # "newDist":I
    .end local v8    # "newLen":I
    :cond_131
    add-int/lit8 v11, v6, -0x1

    invoke-static {v11, v13}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 143
    .local v4, "limit":I
    const/4 v9, 0x0

    :goto_138
    if-ge v9, v14, :cond_14c

    .line 144
    iget-object v11, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->lz:Lorg/tukaani/xz/lz/LZEncoder;

    iget-object v12, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->reps:[I

    aget v12, v12, v9

    invoke-virtual {v11, v12, v4}, Lorg/tukaani/xz/lz/LZEncoder;->getMatchLen(II)I

    move-result v11

    if-ne v11, v4, :cond_149

    move v6, v10

    .line 145
    goto/16 :goto_1f

    .line 143
    :cond_149
    add-int/lit8 v9, v9, 0x1

    goto :goto_138

    .line 147
    :cond_14c
    add-int/lit8 v10, v5, 0x4

    iput v10, p0, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->back:I

    .line 148
    add-int/lit8 v10, v6, -0x2

    invoke-virtual {p0, v10}, Lorg/tukaani/xz/lzma/LZMAEncoderFast;->skip(I)V

    goto/16 :goto_1f
.end method
