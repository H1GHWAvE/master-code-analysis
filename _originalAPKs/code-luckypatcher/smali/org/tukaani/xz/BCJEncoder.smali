.class Lorg/tukaani/xz/BCJEncoder;
.super Lorg/tukaani/xz/BCJCoder;
.source "BCJEncoder.java"

# interfaces
.implements Lorg/tukaani/xz/FilterEncoder;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final filterID:J

.field private final options:Lorg/tukaani/xz/BCJOptions;

.field private final props:[B


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 12
    const-class v0, Lorg/tukaani/xz/BCJEncoder;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_c

    const/4 v0, 0x1

    :goto_9
    sput-boolean v0, Lorg/tukaani/xz/BCJEncoder;->$assertionsDisabled:Z

    return-void

    :cond_c
    const/4 v0, 0x0

    goto :goto_9
.end method

.method constructor <init>(Lorg/tukaani/xz/BCJOptions;J)V
    .registers 9
    .param p1, "options"    # Lorg/tukaani/xz/BCJOptions;
    .param p2, "filterID"    # J

    .prologue
    const/4 v4, 0x4

    .line 17
    invoke-direct {p0}, Lorg/tukaani/xz/BCJCoder;-><init>()V

    .line 18
    sget-boolean v2, Lorg/tukaani/xz/BCJEncoder;->$assertionsDisabled:Z

    if-nez v2, :cond_14

    invoke-static {p2, p3}, Lorg/tukaani/xz/BCJEncoder;->isBCJFilterID(J)Z

    move-result v2

    if-nez v2, :cond_14

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 19
    :cond_14
    invoke-virtual {p1}, Lorg/tukaani/xz/BCJOptions;->getStartOffset()I

    move-result v1

    .line 21
    .local v1, "startOffset":I
    if-nez v1, :cond_2a

    .line 22
    const/4 v2, 0x0

    new-array v2, v2, [B

    iput-object v2, p0, Lorg/tukaani/xz/BCJEncoder;->props:[B

    .line 29
    :cond_1f
    iput-wide p2, p0, Lorg/tukaani/xz/BCJEncoder;->filterID:J

    .line 30
    invoke-virtual {p1}, Lorg/tukaani/xz/BCJOptions;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/tukaani/xz/BCJOptions;

    iput-object v2, p0, Lorg/tukaani/xz/BCJEncoder;->options:Lorg/tukaani/xz/BCJOptions;

    .line 31
    return-void

    .line 24
    :cond_2a
    new-array v2, v4, [B

    iput-object v2, p0, Lorg/tukaani/xz/BCJEncoder;->props:[B

    .line 25
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2f
    if-ge v0, v4, :cond_1f

    .line 26
    iget-object v2, p0, Lorg/tukaani/xz/BCJEncoder;->props:[B

    mul-int/lit8 v3, v0, 0x8

    ushr-int v3, v1, v3

    int-to-byte v3, v3

    aput-byte v3, v2, v0

    .line 25
    add-int/lit8 v0, v0, 0x1

    goto :goto_2f
.end method


# virtual methods
.method public getFilterID()J
    .registers 3

    .prologue
    .line 34
    iget-wide v0, p0, Lorg/tukaani/xz/BCJEncoder;->filterID:J

    return-wide v0
.end method

.method public getFilterProps()[B
    .registers 2

    .prologue
    .line 38
    iget-object v0, p0, Lorg/tukaani/xz/BCJEncoder;->props:[B

    return-object v0
.end method

.method public getOutputStream(Lorg/tukaani/xz/FinishableOutputStream;)Lorg/tukaani/xz/FinishableOutputStream;
    .registers 3
    .param p1, "out"    # Lorg/tukaani/xz/FinishableOutputStream;

    .prologue
    .line 46
    iget-object v0, p0, Lorg/tukaani/xz/BCJEncoder;->options:Lorg/tukaani/xz/BCJOptions;

    invoke-virtual {v0, p1}, Lorg/tukaani/xz/BCJOptions;->getOutputStream(Lorg/tukaani/xz/FinishableOutputStream;)Lorg/tukaani/xz/FinishableOutputStream;

    move-result-object v0

    return-object v0
.end method

.method public supportsFlushing()Z
    .registers 2

    .prologue
    .line 42
    const/4 v0, 0x0

    return v0
.end method
