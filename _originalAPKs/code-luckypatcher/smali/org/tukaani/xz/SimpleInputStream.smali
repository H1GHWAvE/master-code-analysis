.class Lorg/tukaani/xz/SimpleInputStream;
.super Ljava/io/InputStream;
.source "SimpleInputStream.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final FILTER_BUF_SIZE:I = 0x1000


# instance fields
.field private endReached:Z

.field private exception:Ljava/io/IOException;

.field private final filterBuf:[B

.field private filtered:I

.field private in:Ljava/io/InputStream;

.field private pos:I

.field private final simpleFilter:Lorg/tukaani/xz/simple/SimpleFilter;

.field private final tempBuf:[B

.field private unfiltered:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 16
    const-class v0, Lorg/tukaani/xz/SimpleInputStream;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_c

    const/4 v0, 0x1

    :goto_9
    sput-boolean v0, Lorg/tukaani/xz/SimpleInputStream;->$assertionsDisabled:Z

    return-void

    :cond_c
    const/4 v0, 0x0

    goto :goto_9
.end method

.method constructor <init>(Ljava/io/InputStream;Lorg/tukaani/xz/simple/SimpleFilter;)V
    .registers 5
    .param p1, "in"    # Ljava/io/InputStream;
    .param p2, "simpleFilter"    # Lorg/tukaani/xz/simple/SimpleFilter;

    .prologue
    const/4 v1, 0x0

    .line 36
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 22
    const/16 v0, 0x1000

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/tukaani/xz/SimpleInputStream;->filterBuf:[B

    .line 23
    iput v1, p0, Lorg/tukaani/xz/SimpleInputStream;->pos:I

    .line 24
    iput v1, p0, Lorg/tukaani/xz/SimpleInputStream;->filtered:I

    .line 25
    iput v1, p0, Lorg/tukaani/xz/SimpleInputStream;->unfiltered:I

    .line 27
    iput-boolean v1, p0, Lorg/tukaani/xz/SimpleInputStream;->endReached:Z

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/tukaani/xz/SimpleInputStream;->exception:Ljava/io/IOException;

    .line 30
    const/4 v0, 0x1

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/tukaani/xz/SimpleInputStream;->tempBuf:[B

    .line 39
    if-nez p1, :cond_22

    .line 40
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 44
    :cond_22
    sget-boolean v0, Lorg/tukaani/xz/SimpleInputStream;->$assertionsDisabled:Z

    if-nez v0, :cond_2e

    if-nez p2, :cond_2e

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 46
    :cond_2e
    iput-object p1, p0, Lorg/tukaani/xz/SimpleInputStream;->in:Ljava/io/InputStream;

    .line 47
    iput-object p2, p0, Lorg/tukaani/xz/SimpleInputStream;->simpleFilter:Lorg/tukaani/xz/simple/SimpleFilter;

    .line 48
    return-void
.end method

.method static getMemoryUsage()I
    .registers 1

    .prologue
    .line 33
    const/4 v0, 0x5

    return v0
.end method


# virtual methods
.method public available()I
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 120
    iget-object v0, p0, Lorg/tukaani/xz/SimpleInputStream;->in:Ljava/io/InputStream;

    if-nez v0, :cond_c

    .line 121
    new-instance v0, Lorg/tukaani/xz/XZIOException;

    const-string v1, "Stream closed"

    invoke-direct {v0, v1}, Lorg/tukaani/xz/XZIOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 123
    :cond_c
    iget-object v0, p0, Lorg/tukaani/xz/SimpleInputStream;->exception:Ljava/io/IOException;

    if-eqz v0, :cond_13

    .line 124
    iget-object v0, p0, Lorg/tukaani/xz/SimpleInputStream;->exception:Ljava/io/IOException;

    throw v0

    .line 126
    :cond_13
    iget v0, p0, Lorg/tukaani/xz/SimpleInputStream;->filtered:I

    return v0
.end method

.method public close()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 130
    iget-object v0, p0, Lorg/tukaani/xz/SimpleInputStream;->in:Ljava/io/InputStream;

    if-eqz v0, :cond_c

    .line 132
    :try_start_5
    iget-object v0, p0, Lorg/tukaani/xz/SimpleInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_a
    .catchall {:try_start_5 .. :try_end_a} :catchall_d

    .line 134
    iput-object v1, p0, Lorg/tukaani/xz/SimpleInputStream;->in:Ljava/io/InputStream;

    .line 137
    :cond_c
    return-void

    .line 134
    :catchall_d
    move-exception v0

    iput-object v1, p0, Lorg/tukaani/xz/SimpleInputStream;->in:Ljava/io/InputStream;

    throw v0
.end method

.method public read()I
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v0, -0x1

    .line 51
    iget-object v1, p0, Lorg/tukaani/xz/SimpleInputStream;->tempBuf:[B

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v3, v2}, Lorg/tukaani/xz/SimpleInputStream;->read([BII)I

    move-result v1

    if-ne v1, v0, :cond_c

    :goto_b
    return v0

    :cond_c
    iget-object v0, p0, Lorg/tukaani/xz/SimpleInputStream;->tempBuf:[B

    aget-byte v0, v0, v3

    and-int/lit16 v0, v0, 0xff

    goto :goto_b
.end method

.method public read([BII)I
    .registers 15
    .param p1, "buf"    # [B
    .param p2, "off"    # I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x0

    .line 55
    if-ltz p2, :cond_f

    if-ltz p3, :cond_f

    add-int v5, p2, p3

    if-ltz v5, :cond_f

    add-int v5, p2, p3

    array-length v6, p1

    if-le v5, v6, :cond_15

    .line 56
    :cond_f
    new-instance v4, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v4}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v4

    .line 58
    :cond_15
    if-nez p3, :cond_18

    .line 90
    :cond_17
    :goto_17
    return v3

    .line 61
    :cond_18
    iget-object v5, p0, Lorg/tukaani/xz/SimpleInputStream;->in:Ljava/io/InputStream;

    if-nez v5, :cond_24

    .line 62
    new-instance v4, Lorg/tukaani/xz/XZIOException;

    const-string v5, "Stream closed"

    invoke-direct {v4, v5}, Lorg/tukaani/xz/XZIOException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 64
    :cond_24
    iget-object v5, p0, Lorg/tukaani/xz/SimpleInputStream;->exception:Ljava/io/IOException;

    if-eqz v5, :cond_2b

    .line 65
    iget-object v4, p0, Lorg/tukaani/xz/SimpleInputStream;->exception:Ljava/io/IOException;

    throw v4

    .line 68
    :cond_2b
    const/4 v3, 0x0

    .line 72
    .local v3, "size":I
    :goto_2c
    :try_start_2c
    iget v5, p0, Lorg/tukaani/xz/SimpleInputStream;->filtered:I

    invoke-static {v5, p3}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 73
    .local v0, "copySize":I
    iget-object v5, p0, Lorg/tukaani/xz/SimpleInputStream;->filterBuf:[B

    iget v6, p0, Lorg/tukaani/xz/SimpleInputStream;->pos:I

    invoke-static {v5, v6, p1, p2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 74
    iget v5, p0, Lorg/tukaani/xz/SimpleInputStream;->pos:I

    add-int/2addr v5, v0

    iput v5, p0, Lorg/tukaani/xz/SimpleInputStream;->pos:I

    .line 75
    iget v5, p0, Lorg/tukaani/xz/SimpleInputStream;->filtered:I

    sub-int/2addr v5, v0

    iput v5, p0, Lorg/tukaani/xz/SimpleInputStream;->filtered:I

    .line 76
    add-int/2addr p2, v0

    .line 77
    sub-int/2addr p3, v0

    .line 78
    add-int/2addr v3, v0

    .line 83
    iget v5, p0, Lorg/tukaani/xz/SimpleInputStream;->pos:I

    iget v6, p0, Lorg/tukaani/xz/SimpleInputStream;->filtered:I

    add-int/2addr v5, v6

    iget v6, p0, Lorg/tukaani/xz/SimpleInputStream;->unfiltered:I

    add-int/2addr v5, v6

    const/16 v6, 0x1000

    if-ne v5, v6, :cond_64

    .line 84
    iget-object v5, p0, Lorg/tukaani/xz/SimpleInputStream;->filterBuf:[B

    iget v6, p0, Lorg/tukaani/xz/SimpleInputStream;->pos:I

    iget-object v7, p0, Lorg/tukaani/xz/SimpleInputStream;->filterBuf:[B

    const/4 v8, 0x0

    iget v9, p0, Lorg/tukaani/xz/SimpleInputStream;->filtered:I

    iget v10, p0, Lorg/tukaani/xz/SimpleInputStream;->unfiltered:I

    add-int/2addr v9, v10

    invoke-static {v5, v6, v7, v8, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 86
    const/4 v5, 0x0

    iput v5, p0, Lorg/tukaani/xz/SimpleInputStream;->pos:I

    .line 89
    :cond_64
    if-eqz p3, :cond_6a

    iget-boolean v5, p0, Lorg/tukaani/xz/SimpleInputStream;->endReached:Z

    if-eqz v5, :cond_6e

    .line 90
    :cond_6a
    if-gtz v3, :cond_17

    move v3, v4

    goto :goto_17

    .line 92
    :cond_6e
    sget-boolean v5, Lorg/tukaani/xz/SimpleInputStream;->$assertionsDisabled:Z

    if-nez v5, :cond_80

    iget v5, p0, Lorg/tukaani/xz/SimpleInputStream;->filtered:I

    if-eqz v5, :cond_80

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4
    :try_end_7c
    .catch Ljava/io/IOException; {:try_start_2c .. :try_end_7c} :catch_7c

    .line 113
    .end local v0    # "copySize":I
    :catch_7c
    move-exception v1

    .line 114
    .local v1, "e":Ljava/io/IOException;
    iput-object v1, p0, Lorg/tukaani/xz/SimpleInputStream;->exception:Ljava/io/IOException;

    .line 115
    throw v1

    .line 95
    .end local v1    # "e":Ljava/io/IOException;
    .restart local v0    # "copySize":I
    :cond_80
    :try_start_80
    iget v5, p0, Lorg/tukaani/xz/SimpleInputStream;->pos:I

    iget v6, p0, Lorg/tukaani/xz/SimpleInputStream;->filtered:I

    add-int/2addr v5, v6

    iget v6, p0, Lorg/tukaani/xz/SimpleInputStream;->unfiltered:I

    add-int/2addr v5, v6

    rsub-int v2, v5, 0x1000

    .line 96
    .local v2, "inSize":I
    iget-object v5, p0, Lorg/tukaani/xz/SimpleInputStream;->in:Ljava/io/InputStream;

    iget-object v6, p0, Lorg/tukaani/xz/SimpleInputStream;->filterBuf:[B

    iget v7, p0, Lorg/tukaani/xz/SimpleInputStream;->pos:I

    iget v8, p0, Lorg/tukaani/xz/SimpleInputStream;->filtered:I

    add-int/2addr v7, v8

    iget v8, p0, Lorg/tukaani/xz/SimpleInputStream;->unfiltered:I

    add-int/2addr v7, v8

    invoke-virtual {v5, v6, v7, v2}, Ljava/io/InputStream;->read([BII)I

    move-result v2

    .line 99
    if-ne v2, v4, :cond_a7

    .line 102
    const/4 v5, 0x1

    iput-boolean v5, p0, Lorg/tukaani/xz/SimpleInputStream;->endReached:Z

    .line 103
    iget v5, p0, Lorg/tukaani/xz/SimpleInputStream;->unfiltered:I

    iput v5, p0, Lorg/tukaani/xz/SimpleInputStream;->filtered:I

    .line 104
    const/4 v5, 0x0

    iput v5, p0, Lorg/tukaani/xz/SimpleInputStream;->unfiltered:I

    goto :goto_2c

    .line 107
    :cond_a7
    iget v5, p0, Lorg/tukaani/xz/SimpleInputStream;->unfiltered:I

    add-int/2addr v5, v2

    iput v5, p0, Lorg/tukaani/xz/SimpleInputStream;->unfiltered:I

    .line 108
    iget-object v5, p0, Lorg/tukaani/xz/SimpleInputStream;->simpleFilter:Lorg/tukaani/xz/simple/SimpleFilter;

    iget-object v6, p0, Lorg/tukaani/xz/SimpleInputStream;->filterBuf:[B

    iget v7, p0, Lorg/tukaani/xz/SimpleInputStream;->pos:I

    iget v8, p0, Lorg/tukaani/xz/SimpleInputStream;->unfiltered:I

    invoke-interface {v5, v6, v7, v8}, Lorg/tukaani/xz/simple/SimpleFilter;->code([BII)I

    move-result v5

    iput v5, p0, Lorg/tukaani/xz/SimpleInputStream;->filtered:I

    .line 109
    sget-boolean v5, Lorg/tukaani/xz/SimpleInputStream;->$assertionsDisabled:Z

    if-nez v5, :cond_ca

    iget v5, p0, Lorg/tukaani/xz/SimpleInputStream;->filtered:I

    iget v6, p0, Lorg/tukaani/xz/SimpleInputStream;->unfiltered:I

    if-le v5, v6, :cond_ca

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 110
    :cond_ca
    iget v5, p0, Lorg/tukaani/xz/SimpleInputStream;->unfiltered:I

    iget v6, p0, Lorg/tukaani/xz/SimpleInputStream;->filtered:I

    sub-int/2addr v5, v6

    iput v5, p0, Lorg/tukaani/xz/SimpleInputStream;->unfiltered:I
    :try_end_d1
    .catch Ljava/io/IOException; {:try_start_80 .. :try_end_d1} :catch_7c

    goto/16 :goto_2c
.end method
