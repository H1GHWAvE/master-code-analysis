.class public Lorg/tukaani/xz/SeekableXZInputStream;
.super Lorg/tukaani/xz/SeekableInputStream;
.source "SeekableXZInputStream.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private blockCount:I

.field private blockDecoder:Lorg/tukaani/xz/BlockInputStream;

.field private check:Lorg/tukaani/xz/check/Check;

.field private checkTypes:I

.field private final curBlockInfo:Lorg/tukaani/xz/index/BlockInfo;

.field private curPos:J

.field private endReached:Z

.field private exception:Ljava/io/IOException;

.field private in:Lorg/tukaani/xz/SeekableInputStream;

.field private indexMemoryUsage:I

.field private largestBlockSize:J

.field private final memoryLimit:I

.field private final queriedBlockInfo:Lorg/tukaani/xz/index/BlockInfo;

.field private seekNeeded:Z

.field private seekPos:J

.field private final streams:Ljava/util/ArrayList;

.field private final tempBuf:[B

.field private uncompressedSize:J


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 76
    const-class v0, Lorg/tukaani/xz/SeekableXZInputStream;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_c

    const/4 v0, 0x1

    :goto_9
    sput-boolean v0, Lorg/tukaani/xz/SeekableXZInputStream;->$assertionsDisabled:Z

    return-void

    :cond_c
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public constructor <init>(Lorg/tukaani/xz/SeekableInputStream;)V
    .registers 3
    .param p1, "in"    # Lorg/tukaani/xz/SeekableInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 204
    const/4 v0, -0x1

    invoke-direct {p0, p1, v0}, Lorg/tukaani/xz/SeekableXZInputStream;-><init>(Lorg/tukaani/xz/SeekableInputStream;I)V

    .line 205
    return-void
.end method

.method public constructor <init>(Lorg/tukaani/xz/SeekableInputStream;I)V
    .registers 28
    .param p1, "in"    # Lorg/tukaani/xz/SeekableInputStream;
    .param p2, "memoryLimit"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 241
    invoke-direct/range {p0 .. p0}, Lorg/tukaani/xz/SeekableInputStream;-><init>()V

    .line 93
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput v4, v0, Lorg/tukaani/xz/SeekableXZInputStream;->indexMemoryUsage:I

    .line 100
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Lorg/tukaani/xz/SeekableXZInputStream;->streams:Ljava/util/ArrayList;

    .line 105
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput v4, v0, Lorg/tukaani/xz/SeekableXZInputStream;->checkTypes:I

    .line 110
    const-wide/16 v21, 0x0

    move-wide/from16 v0, v21

    move-object/from16 v2, p0

    iput-wide v0, v2, Lorg/tukaani/xz/SeekableXZInputStream;->uncompressedSize:J

    .line 115
    const-wide/16 v21, 0x0

    move-wide/from16 v0, v21

    move-object/from16 v2, p0

    iput-wide v0, v2, Lorg/tukaani/xz/SeekableXZInputStream;->largestBlockSize:J

    .line 120
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput v4, v0, Lorg/tukaani/xz/SeekableXZInputStream;->blockCount:I

    .line 143
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lorg/tukaani/xz/SeekableXZInputStream;->blockDecoder:Lorg/tukaani/xz/BlockInputStream;

    .line 148
    const-wide/16 v21, 0x0

    move-wide/from16 v0, v21

    move-object/from16 v2, p0

    iput-wide v0, v2, Lorg/tukaani/xz/SeekableXZInputStream;->curPos:J

    .line 159
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lorg/tukaani/xz/SeekableXZInputStream;->seekNeeded:Z

    .line 165
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lorg/tukaani/xz/SeekableXZInputStream;->endReached:Z

    .line 170
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lorg/tukaani/xz/SeekableXZInputStream;->exception:Ljava/io/IOException;

    .line 176
    const/4 v4, 0x1

    new-array v4, v4, [B

    move-object/from16 v0, p0

    iput-object v4, v0, Lorg/tukaani/xz/SeekableXZInputStream;->tempBuf:[B

    .line 242
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/tukaani/xz/SeekableXZInputStream;->in:Lorg/tukaani/xz/SeekableInputStream;

    .line 243
    new-instance v14, Ljava/io/DataInputStream;

    move-object/from16 v0, p1

    invoke-direct {v14, v0}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 247
    .local v14, "inData":Ljava/io/DataInputStream;
    const-wide/16 v21, 0x0

    move-object/from16 v0, p1

    move-wide/from16 v1, v21

    invoke-virtual {v0, v1, v2}, Lorg/tukaani/xz/SeekableInputStream;->seek(J)V

    .line 248
    sget-object v4, Lorg/tukaani/xz/XZ;->HEADER_MAGIC:[B

    array-length v4, v4

    new-array v9, v4, [B

    .line 249
    .local v9, "buf":[B
    invoke-virtual {v14, v9}, Ljava/io/DataInputStream;->readFully([B)V

    .line 250
    sget-object v4, Lorg/tukaani/xz/XZ;->HEADER_MAGIC:[B

    invoke-static {v9, v4}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v4

    if-nez v4, :cond_7a

    .line 251
    new-instance v4, Lorg/tukaani/xz/XZFormatException;

    invoke-direct {v4}, Lorg/tukaani/xz/XZFormatException;-><init>()V

    throw v4

    .line 255
    :cond_7a
    invoke-virtual/range {p1 .. p1}, Lorg/tukaani/xz/SeekableInputStream;->length()J

    move-result-wide v17

    .line 256
    .local v17, "pos":J
    const-wide/16 v21, 0x3

    and-long v21, v21, v17

    const-wide/16 v23, 0x0

    cmp-long v4, v21, v23

    if-eqz v4, :cond_90

    .line 257
    new-instance v4, Lorg/tukaani/xz/CorruptedInputException;

    const-string v8, "XZ file size is not a multiple of 4 bytes"

    invoke-direct {v4, v8}, Lorg/tukaani/xz/CorruptedInputException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 261
    :cond_90
    const/16 v4, 0xc

    new-array v9, v4, [B

    .line 262
    const-wide/16 v6, 0x0

    .line 264
    .local v6, "streamPadding":J
    :goto_96
    const-wide/16 v21, 0x0

    cmp-long v4, v17, v21

    if-lez v4, :cond_201

    .line 265
    const-wide/16 v21, 0xc

    cmp-long v4, v17, v21

    if-gez v4, :cond_a8

    .line 266
    new-instance v4, Lorg/tukaani/xz/CorruptedInputException;

    invoke-direct {v4}, Lorg/tukaani/xz/CorruptedInputException;-><init>()V

    throw v4

    .line 269
    :cond_a8
    const-wide/16 v21, 0xc

    sub-long v21, v17, v21

    move-object/from16 v0, p1

    move-wide/from16 v1, v21

    invoke-virtual {v0, v1, v2}, Lorg/tukaani/xz/SeekableInputStream;->seek(J)V

    .line 270
    invoke-virtual {v14, v9}, Ljava/io/DataInputStream;->readFully([B)V

    .line 275
    const/16 v4, 0x8

    aget-byte v4, v9, v4

    if-nez v4, :cond_d7

    const/16 v4, 0x9

    aget-byte v4, v9, v4

    if-nez v4, :cond_d7

    const/16 v4, 0xa

    aget-byte v4, v9, v4

    if-nez v4, :cond_d7

    const/16 v4, 0xb

    aget-byte v4, v9, v4

    if-nez v4, :cond_d7

    .line 277
    const-wide/16 v21, 0x4

    add-long v6, v6, v21

    .line 278
    const-wide/16 v21, 0x4

    sub-long v17, v17, v21

    .line 279
    goto :goto_96

    .line 283
    :cond_d7
    const-wide/16 v21, 0xc

    sub-long v17, v17, v21

    .line 287
    invoke-static {v9}, Lorg/tukaani/xz/common/DecoderUtil;->decodeStreamFooter([B)Lorg/tukaani/xz/common/StreamFlags;

    move-result-object v5

    .line 288
    .local v5, "streamFooter":Lorg/tukaani/xz/common/StreamFlags;
    iget-wide v0, v5, Lorg/tukaani/xz/common/StreamFlags;->backwardSize:J

    move-wide/from16 v21, v0

    cmp-long v4, v21, v17

    if-ltz v4, :cond_ef

    .line 289
    new-instance v4, Lorg/tukaani/xz/CorruptedInputException;

    const-string v8, "Backward Size in XZ Stream Footer is too big"

    invoke-direct {v4, v8}, Lorg/tukaani/xz/CorruptedInputException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 294
    :cond_ef
    iget v4, v5, Lorg/tukaani/xz/common/StreamFlags;->checkType:I

    invoke-static {v4}, Lorg/tukaani/xz/check/Check;->getInstance(I)Lorg/tukaani/xz/check/Check;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lorg/tukaani/xz/SeekableXZInputStream;->check:Lorg/tukaani/xz/check/Check;

    .line 297
    move-object/from16 v0, p0

    iget v4, v0, Lorg/tukaani/xz/SeekableXZInputStream;->checkTypes:I

    const/4 v8, 0x1

    iget v0, v5, Lorg/tukaani/xz/common/StreamFlags;->checkType:I

    move/from16 v21, v0

    shl-int v8, v8, v21

    or-int/2addr v4, v8

    move-object/from16 v0, p0

    iput v4, v0, Lorg/tukaani/xz/SeekableXZInputStream;->checkTypes:I

    .line 300
    iget-wide v0, v5, Lorg/tukaani/xz/common/StreamFlags;->backwardSize:J

    move-wide/from16 v21, v0

    sub-long v21, v17, v21

    move-object/from16 v0, p1

    move-wide/from16 v1, v21

    invoke-virtual {v0, v1, v2}, Lorg/tukaani/xz/SeekableInputStream;->seek(J)V

    .line 305
    :try_start_116
    new-instance v3, Lorg/tukaani/xz/index/IndexDecoder;

    move-object/from16 v4, p1

    move/from16 v8, p2

    invoke-direct/range {v3 .. v8}, Lorg/tukaani/xz/index/IndexDecoder;-><init>(Lorg/tukaani/xz/SeekableInputStream;Lorg/tukaani/xz/common/StreamFlags;JI)V
    :try_end_11f
    .catch Lorg/tukaani/xz/MemoryLimitException; {:try_start_116 .. :try_end_11f} :catch_140

    .line 317
    .local v3, "index":Lorg/tukaani/xz/index/IndexDecoder;
    move-object/from16 v0, p0

    iget v4, v0, Lorg/tukaani/xz/SeekableXZInputStream;->indexMemoryUsage:I

    invoke-virtual {v3}, Lorg/tukaani/xz/index/IndexDecoder;->getMemoryUsage()I

    move-result v8

    add-int/2addr v4, v8

    move-object/from16 v0, p0

    iput v4, v0, Lorg/tukaani/xz/SeekableXZInputStream;->indexMemoryUsage:I

    .line 318
    if-ltz p2, :cond_169

    .line 319
    invoke-virtual {v3}, Lorg/tukaani/xz/index/IndexDecoder;->getMemoryUsage()I

    move-result v4

    sub-int p2, p2, v4

    .line 320
    sget-boolean v4, Lorg/tukaani/xz/SeekableXZInputStream;->$assertionsDisabled:Z

    if-nez v4, :cond_169

    if-gez p2, :cond_169

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 307
    .end local v3    # "index":Lorg/tukaani/xz/index/IndexDecoder;
    :catch_140
    move-exception v11

    .line 310
    .local v11, "e":Lorg/tukaani/xz/MemoryLimitException;
    sget-boolean v4, Lorg/tukaani/xz/SeekableXZInputStream;->$assertionsDisabled:Z

    if-nez v4, :cond_14d

    if-gez p2, :cond_14d

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 311
    :cond_14d
    new-instance v4, Lorg/tukaani/xz/MemoryLimitException;

    .line 312
    invoke-virtual {v11}, Lorg/tukaani/xz/MemoryLimitException;->getMemoryNeeded()I

    move-result v8

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tukaani/xz/SeekableXZInputStream;->indexMemoryUsage:I

    move/from16 v21, v0

    add-int v8, v8, v21

    move-object/from16 v0, p0

    iget v0, v0, Lorg/tukaani/xz/SeekableXZInputStream;->indexMemoryUsage:I

    move/from16 v21, v0

    add-int v21, v21, p2

    move/from16 v0, v21

    invoke-direct {v4, v8, v0}, Lorg/tukaani/xz/MemoryLimitException;-><init>(II)V

    throw v4

    .line 324
    .end local v11    # "e":Lorg/tukaani/xz/MemoryLimitException;
    .restart local v3    # "index":Lorg/tukaani/xz/index/IndexDecoder;
    :cond_169
    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/tukaani/xz/SeekableXZInputStream;->largestBlockSize:J

    move-wide/from16 v21, v0

    invoke-virtual {v3}, Lorg/tukaani/xz/index/IndexDecoder;->getLargestBlockSize()J

    move-result-wide v23

    cmp-long v4, v21, v23

    if-gez v4, :cond_181

    .line 325
    invoke-virtual {v3}, Lorg/tukaani/xz/index/IndexDecoder;->getLargestBlockSize()J

    move-result-wide v21

    move-wide/from16 v0, v21

    move-object/from16 v2, p0

    iput-wide v0, v2, Lorg/tukaani/xz/SeekableXZInputStream;->largestBlockSize:J

    .line 329
    :cond_181
    invoke-virtual {v3}, Lorg/tukaani/xz/index/IndexDecoder;->getStreamSize()J

    move-result-wide v21

    const-wide/16 v23, 0xc

    sub-long v15, v21, v23

    .line 330
    .local v15, "off":J
    cmp-long v4, v17, v15

    if-gez v4, :cond_195

    .line 331
    new-instance v4, Lorg/tukaani/xz/CorruptedInputException;

    const-string v8, "XZ Index indicates too big compressed size for the XZ Stream"

    invoke-direct {v4, v8}, Lorg/tukaani/xz/CorruptedInputException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 335
    :cond_195
    sub-long v17, v17, v15

    .line 336
    move-object/from16 v0, p1

    move-wide/from16 v1, v17

    invoke-virtual {v0, v1, v2}, Lorg/tukaani/xz/SeekableInputStream;->seek(J)V

    .line 339
    invoke-virtual {v14, v9}, Ljava/io/DataInputStream;->readFully([B)V

    .line 340
    invoke-static {v9}, Lorg/tukaani/xz/common/DecoderUtil;->decodeStreamHeader([B)Lorg/tukaani/xz/common/StreamFlags;

    move-result-object v20

    .line 343
    .local v20, "streamHeader":Lorg/tukaani/xz/common/StreamFlags;
    move-object/from16 v0, v20

    invoke-static {v0, v5}, Lorg/tukaani/xz/common/DecoderUtil;->areStreamFlagsEqual(Lorg/tukaani/xz/common/StreamFlags;Lorg/tukaani/xz/common/StreamFlags;)Z

    move-result v4

    if-nez v4, :cond_1b5

    .line 344
    new-instance v4, Lorg/tukaani/xz/CorruptedInputException;

    const-string v8, "XZ Stream Footer does not match Stream Header"

    invoke-direct {v4, v8}, Lorg/tukaani/xz/CorruptedInputException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 349
    :cond_1b5
    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/tukaani/xz/SeekableXZInputStream;->uncompressedSize:J

    move-wide/from16 v21, v0

    invoke-virtual {v3}, Lorg/tukaani/xz/index/IndexDecoder;->getUncompressedSize()J

    move-result-wide v23

    add-long v21, v21, v23

    move-wide/from16 v0, v21

    move-object/from16 v2, p0

    iput-wide v0, v2, Lorg/tukaani/xz/SeekableXZInputStream;->uncompressedSize:J

    .line 350
    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/tukaani/xz/SeekableXZInputStream;->uncompressedSize:J

    move-wide/from16 v21, v0

    const-wide/16 v23, 0x0

    cmp-long v4, v21, v23

    if-gez v4, :cond_1db

    .line 351
    new-instance v4, Lorg/tukaani/xz/UnsupportedOptionsException;

    const-string v8, "XZ file is too big"

    invoke-direct {v4, v8}, Lorg/tukaani/xz/UnsupportedOptionsException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 354
    :cond_1db
    move-object/from16 v0, p0

    iget v4, v0, Lorg/tukaani/xz/SeekableXZInputStream;->blockCount:I

    invoke-virtual {v3}, Lorg/tukaani/xz/index/IndexDecoder;->getRecordCount()I

    move-result v8

    add-int/2addr v4, v8

    move-object/from16 v0, p0

    iput v4, v0, Lorg/tukaani/xz/SeekableXZInputStream;->blockCount:I

    .line 355
    move-object/from16 v0, p0

    iget v4, v0, Lorg/tukaani/xz/SeekableXZInputStream;->blockCount:I

    if-gez v4, :cond_1f6

    .line 356
    new-instance v4, Lorg/tukaani/xz/UnsupportedOptionsException;

    const-string v8, "XZ file has over 2147483647 Blocks"

    invoke-direct {v4, v8}, Lorg/tukaani/xz/UnsupportedOptionsException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 360
    :cond_1f6
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/tukaani/xz/SeekableXZInputStream;->streams:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 363
    const-wide/16 v6, 0x0

    .line 364
    goto/16 :goto_96

    .line 366
    .end local v3    # "index":Lorg/tukaani/xz/index/IndexDecoder;
    .end local v5    # "streamFooter":Lorg/tukaani/xz/common/StreamFlags;
    .end local v15    # "off":J
    .end local v20    # "streamHeader":Lorg/tukaani/xz/common/StreamFlags;
    :cond_201
    sget-boolean v4, Lorg/tukaani/xz/SeekableXZInputStream;->$assertionsDisabled:Z

    if-nez v4, :cond_211

    const-wide/16 v21, 0x0

    cmp-long v4, v17, v21

    if-eqz v4, :cond_211

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 369
    :cond_211
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput v0, v1, Lorg/tukaani/xz/SeekableXZInputStream;->memoryLimit:I

    .line 374
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/tukaani/xz/SeekableXZInputStream;->streams:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/tukaani/xz/SeekableXZInputStream;->streams:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lorg/tukaani/xz/index/IndexDecoder;

    .line 375
    .local v19, "prev":Lorg/tukaani/xz/index/IndexDecoder;
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/tukaani/xz/SeekableXZInputStream;->streams:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v13, v4, -0x2

    .local v13, "i":I
    :goto_235
    if-ltz v13, :cond_24b

    .line 376
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/tukaani/xz/SeekableXZInputStream;->streams:Ljava/util/ArrayList;

    invoke-virtual {v4, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lorg/tukaani/xz/index/IndexDecoder;

    .line 377
    .local v10, "cur":Lorg/tukaani/xz/index/IndexDecoder;
    move-object/from16 v0, v19

    invoke-virtual {v10, v0}, Lorg/tukaani/xz/index/IndexDecoder;->setOffsets(Lorg/tukaani/xz/index/IndexDecoder;)V

    .line 378
    move-object/from16 v19, v10

    .line 375
    add-int/lit8 v13, v13, -0x1

    goto :goto_235

    .line 385
    .end local v10    # "cur":Lorg/tukaani/xz/index/IndexDecoder;
    :cond_24b
    move-object/from16 v0, p0

    iget-object v4, v0, Lorg/tukaani/xz/SeekableXZInputStream;->streams:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v8, v0, Lorg/tukaani/xz/SeekableXZInputStream;->streams:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lorg/tukaani/xz/index/IndexDecoder;

    .line 386
    .local v12, "first":Lorg/tukaani/xz/index/IndexDecoder;
    new-instance v4, Lorg/tukaani/xz/index/BlockInfo;

    invoke-direct {v4, v12}, Lorg/tukaani/xz/index/BlockInfo;-><init>(Lorg/tukaani/xz/index/IndexDecoder;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lorg/tukaani/xz/SeekableXZInputStream;->curBlockInfo:Lorg/tukaani/xz/index/BlockInfo;

    .line 390
    new-instance v4, Lorg/tukaani/xz/index/BlockInfo;

    invoke-direct {v4, v12}, Lorg/tukaani/xz/index/BlockInfo;-><init>(Lorg/tukaani/xz/index/IndexDecoder;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lorg/tukaani/xz/SeekableXZInputStream;->queriedBlockInfo:Lorg/tukaani/xz/index/BlockInfo;

    .line 391
    return-void
.end method

.method private initBlockDecoder()V
    .registers 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 881
    const/4 v0, 0x0

    :try_start_1
    iput-object v0, p0, Lorg/tukaani/xz/SeekableXZInputStream;->blockDecoder:Lorg/tukaani/xz/BlockInputStream;

    .line 882
    new-instance v0, Lorg/tukaani/xz/BlockInputStream;

    iget-object v1, p0, Lorg/tukaani/xz/SeekableXZInputStream;->in:Lorg/tukaani/xz/SeekableInputStream;

    iget-object v2, p0, Lorg/tukaani/xz/SeekableXZInputStream;->check:Lorg/tukaani/xz/check/Check;

    iget v3, p0, Lorg/tukaani/xz/SeekableXZInputStream;->memoryLimit:I

    iget-object v4, p0, Lorg/tukaani/xz/SeekableXZInputStream;->curBlockInfo:Lorg/tukaani/xz/index/BlockInfo;

    iget-wide v4, v4, Lorg/tukaani/xz/index/BlockInfo;->unpaddedSize:J

    iget-object v6, p0, Lorg/tukaani/xz/SeekableXZInputStream;->curBlockInfo:Lorg/tukaani/xz/index/BlockInfo;

    iget-wide v6, v6, Lorg/tukaani/xz/index/BlockInfo;->uncompressedSize:J

    invoke-direct/range {v0 .. v7}, Lorg/tukaani/xz/BlockInputStream;-><init>(Ljava/io/InputStream;Lorg/tukaani/xz/check/Check;IJJ)V

    iput-object v0, p0, Lorg/tukaani/xz/SeekableXZInputStream;->blockDecoder:Lorg/tukaani/xz/BlockInputStream;
    :try_end_18
    .catch Lorg/tukaani/xz/MemoryLimitException; {:try_start_1 .. :try_end_18} :catch_19
    .catch Lorg/tukaani/xz/IndexIndicatorException; {:try_start_1 .. :try_end_18} :catch_3a

    .line 895
    return-void

    .line 884
    :catch_19
    move-exception v8

    .line 887
    .local v8, "e":Lorg/tukaani/xz/MemoryLimitException;
    sget-boolean v0, Lorg/tukaani/xz/SeekableXZInputStream;->$assertionsDisabled:Z

    if-nez v0, :cond_28

    iget v0, p0, Lorg/tukaani/xz/SeekableXZInputStream;->memoryLimit:I

    if-gez v0, :cond_28

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 888
    :cond_28
    new-instance v0, Lorg/tukaani/xz/MemoryLimitException;

    .line 889
    invoke-virtual {v8}, Lorg/tukaani/xz/MemoryLimitException;->getMemoryNeeded()I

    move-result v1

    iget v2, p0, Lorg/tukaani/xz/SeekableXZInputStream;->indexMemoryUsage:I

    add-int/2addr v1, v2

    iget v2, p0, Lorg/tukaani/xz/SeekableXZInputStream;->memoryLimit:I

    iget v3, p0, Lorg/tukaani/xz/SeekableXZInputStream;->indexMemoryUsage:I

    add-int/2addr v2, v3

    invoke-direct {v0, v1, v2}, Lorg/tukaani/xz/MemoryLimitException;-><init>(II)V

    throw v0

    .line 891
    .end local v8    # "e":Lorg/tukaani/xz/MemoryLimitException;
    :catch_3a
    move-exception v8

    .line 893
    .local v8, "e":Lorg/tukaani/xz/IndexIndicatorException;
    new-instance v0, Lorg/tukaani/xz/CorruptedInputException;

    invoke-direct {v0}, Lorg/tukaani/xz/CorruptedInputException;-><init>()V

    throw v0
.end method

.method private locateBlockByNumber(Lorg/tukaani/xz/index/BlockInfo;I)V
    .registers 8
    .param p1, "info"    # Lorg/tukaani/xz/index/BlockInfo;
    .param p2, "blockNumber"    # I

    .prologue
    .line 854
    if-ltz p2, :cond_6

    iget v2, p0, Lorg/tukaani/xz/SeekableXZInputStream;->blockCount:I

    if-lt p2, v2, :cond_1f

    .line 855
    :cond_6
    new-instance v2, Ljava/lang/IndexOutOfBoundsException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Invalid XZ Block number: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 859
    :cond_1f
    iget v2, p1, Lorg/tukaani/xz/index/BlockInfo;->blockNumber:I

    if-ne v2, p2, :cond_24

    .line 868
    :goto_23
    return-void

    .line 864
    :cond_24
    const/4 v0, 0x0

    .line 865
    .local v0, "i":I
    :goto_25
    iget-object v2, p0, Lorg/tukaani/xz/SeekableXZInputStream;->streams:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/tukaani/xz/index/IndexDecoder;

    .line 866
    .local v1, "index":Lorg/tukaani/xz/index/IndexDecoder;
    invoke-virtual {v1, p2}, Lorg/tukaani/xz/index/IndexDecoder;->hasRecord(I)Z

    move-result v2

    if-eqz v2, :cond_37

    .line 867
    invoke-virtual {v1, p1, p2}, Lorg/tukaani/xz/index/IndexDecoder;->setBlockInfo(Lorg/tukaani/xz/index/BlockInfo;I)V

    goto :goto_23

    .line 864
    :cond_37
    add-int/lit8 v0, v0, 0x1

    goto :goto_25
.end method

.method private locateBlockByPos(Lorg/tukaani/xz/index/BlockInfo;J)V
    .registers 12
    .param p1, "info"    # Lorg/tukaani/xz/index/BlockInfo;
    .param p2, "pos"    # J

    .prologue
    const-wide/16 v6, 0x0

    .line 827
    cmp-long v2, p2, v6

    if-ltz v2, :cond_c

    iget-wide v2, p0, Lorg/tukaani/xz/SeekableXZInputStream;->uncompressedSize:J

    cmp-long v2, p2, v2

    if-ltz v2, :cond_25

    .line 828
    :cond_c
    new-instance v2, Ljava/lang/IndexOutOfBoundsException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Invalid uncompressed position: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 833
    :cond_25
    const/4 v0, 0x0

    .line 834
    .local v0, "i":I
    :goto_26
    iget-object v2, p0, Lorg/tukaani/xz/SeekableXZInputStream;->streams:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/tukaani/xz/index/IndexDecoder;

    .line 835
    .local v1, "index":Lorg/tukaani/xz/index/IndexDecoder;
    invoke-virtual {v1, p2, p3}, Lorg/tukaani/xz/index/IndexDecoder;->hasUncompressedOffset(J)Z

    move-result v2

    if-eqz v2, :cond_4a

    .line 840
    invoke-virtual {v1, p1, p2, p3}, Lorg/tukaani/xz/index/IndexDecoder;->locateBlock(Lorg/tukaani/xz/index/BlockInfo;J)V

    .line 842
    sget-boolean v2, Lorg/tukaani/xz/SeekableXZInputStream;->$assertionsDisabled:Z

    if-nez v2, :cond_4d

    iget-wide v2, p1, Lorg/tukaani/xz/index/BlockInfo;->compressedOffset:J

    const-wide/16 v4, 0x3

    and-long/2addr v2, v4

    cmp-long v2, v2, v6

    if-eqz v2, :cond_4d

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 833
    :cond_4a
    add-int/lit8 v0, v0, 0x1

    goto :goto_26

    .line 843
    :cond_4d
    sget-boolean v2, Lorg/tukaani/xz/SeekableXZInputStream;->$assertionsDisabled:Z

    if-nez v2, :cond_5d

    iget-wide v2, p1, Lorg/tukaani/xz/index/BlockInfo;->uncompressedSize:J

    cmp-long v2, v2, v6

    if-gtz v2, :cond_5d

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 844
    :cond_5d
    sget-boolean v2, Lorg/tukaani/xz/SeekableXZInputStream;->$assertionsDisabled:Z

    if-nez v2, :cond_6d

    iget-wide v2, p1, Lorg/tukaani/xz/index/BlockInfo;->uncompressedOffset:J

    cmp-long v2, p2, v2

    if-gez v2, :cond_6d

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 845
    :cond_6d
    sget-boolean v2, Lorg/tukaani/xz/SeekableXZInputStream;->$assertionsDisabled:Z

    if-nez v2, :cond_80

    iget-wide v2, p1, Lorg/tukaani/xz/index/BlockInfo;->uncompressedOffset:J

    iget-wide v4, p1, Lorg/tukaani/xz/index/BlockInfo;->uncompressedSize:J

    add-long/2addr v2, v4

    cmp-long v2, p2, v2

    if-ltz v2, :cond_80

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 846
    :cond_80
    return-void
.end method

.method private seek()V
    .registers 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 761
    iget-boolean v2, p0, Lorg/tukaani/xz/SeekableXZInputStream;->seekNeeded:Z

    if-nez v2, :cond_1a

    .line 762
    iget-object v2, p0, Lorg/tukaani/xz/SeekableXZInputStream;->curBlockInfo:Lorg/tukaani/xz/index/BlockInfo;

    invoke-virtual {v2}, Lorg/tukaani/xz/index/BlockInfo;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_16

    .line 763
    iget-object v2, p0, Lorg/tukaani/xz/SeekableXZInputStream;->curBlockInfo:Lorg/tukaani/xz/index/BlockInfo;

    invoke-virtual {v2}, Lorg/tukaani/xz/index/BlockInfo;->setNext()V

    .line 764
    invoke-direct {p0}, Lorg/tukaani/xz/SeekableXZInputStream;->initBlockDecoder()V

    .line 821
    :cond_15
    :goto_15
    return-void

    .line 768
    :cond_16
    iget-wide v2, p0, Lorg/tukaani/xz/SeekableXZInputStream;->curPos:J

    iput-wide v2, p0, Lorg/tukaani/xz/SeekableXZInputStream;->seekPos:J

    .line 771
    :cond_1a
    iput-boolean v6, p0, Lorg/tukaani/xz/SeekableXZInputStream;->seekNeeded:Z

    .line 774
    iget-wide v2, p0, Lorg/tukaani/xz/SeekableXZInputStream;->seekPos:J

    iget-wide v4, p0, Lorg/tukaani/xz/SeekableXZInputStream;->uncompressedSize:J

    cmp-long v2, v2, v4

    if-ltz v2, :cond_2f

    .line 775
    iget-wide v2, p0, Lorg/tukaani/xz/SeekableXZInputStream;->seekPos:J

    iput-wide v2, p0, Lorg/tukaani/xz/SeekableXZInputStream;->curPos:J

    .line 776
    const/4 v2, 0x0

    iput-object v2, p0, Lorg/tukaani/xz/SeekableXZInputStream;->blockDecoder:Lorg/tukaani/xz/BlockInputStream;

    .line 777
    const/4 v2, 0x1

    iput-boolean v2, p0, Lorg/tukaani/xz/SeekableXZInputStream;->endReached:Z

    goto :goto_15

    .line 781
    :cond_2f
    iput-boolean v6, p0, Lorg/tukaani/xz/SeekableXZInputStream;->endReached:Z

    .line 784
    iget-object v2, p0, Lorg/tukaani/xz/SeekableXZInputStream;->curBlockInfo:Lorg/tukaani/xz/index/BlockInfo;

    iget-wide v3, p0, Lorg/tukaani/xz/SeekableXZInputStream;->seekPos:J

    invoke-direct {p0, v2, v3, v4}, Lorg/tukaani/xz/SeekableXZInputStream;->locateBlockByPos(Lorg/tukaani/xz/index/BlockInfo;J)V

    .line 796
    iget-wide v2, p0, Lorg/tukaani/xz/SeekableXZInputStream;->curPos:J

    iget-object v4, p0, Lorg/tukaani/xz/SeekableXZInputStream;->curBlockInfo:Lorg/tukaani/xz/index/BlockInfo;

    iget-wide v4, v4, Lorg/tukaani/xz/index/BlockInfo;->uncompressedOffset:J

    cmp-long v2, v2, v4

    if-lez v2, :cond_4a

    iget-wide v2, p0, Lorg/tukaani/xz/SeekableXZInputStream;->curPos:J

    iget-wide v4, p0, Lorg/tukaani/xz/SeekableXZInputStream;->seekPos:J

    cmp-long v2, v2, v4

    if-lez v2, :cond_68

    .line 798
    :cond_4a
    iget-object v2, p0, Lorg/tukaani/xz/SeekableXZInputStream;->in:Lorg/tukaani/xz/SeekableInputStream;

    iget-object v3, p0, Lorg/tukaani/xz/SeekableXZInputStream;->curBlockInfo:Lorg/tukaani/xz/index/BlockInfo;

    iget-wide v3, v3, Lorg/tukaani/xz/index/BlockInfo;->compressedOffset:J

    invoke-virtual {v2, v3, v4}, Lorg/tukaani/xz/SeekableInputStream;->seek(J)V

    .line 802
    iget-object v2, p0, Lorg/tukaani/xz/SeekableXZInputStream;->curBlockInfo:Lorg/tukaani/xz/index/BlockInfo;

    invoke-virtual {v2}, Lorg/tukaani/xz/index/BlockInfo;->getCheckType()I

    move-result v2

    invoke-static {v2}, Lorg/tukaani/xz/check/Check;->getInstance(I)Lorg/tukaani/xz/check/Check;

    move-result-object v2

    iput-object v2, p0, Lorg/tukaani/xz/SeekableXZInputStream;->check:Lorg/tukaani/xz/check/Check;

    .line 805
    invoke-direct {p0}, Lorg/tukaani/xz/SeekableXZInputStream;->initBlockDecoder()V

    .line 806
    iget-object v2, p0, Lorg/tukaani/xz/SeekableXZInputStream;->curBlockInfo:Lorg/tukaani/xz/index/BlockInfo;

    iget-wide v2, v2, Lorg/tukaani/xz/index/BlockInfo;->uncompressedOffset:J

    iput-wide v2, p0, Lorg/tukaani/xz/SeekableXZInputStream;->curPos:J

    .line 811
    :cond_68
    iget-wide v2, p0, Lorg/tukaani/xz/SeekableXZInputStream;->seekPos:J

    iget-wide v4, p0, Lorg/tukaani/xz/SeekableXZInputStream;->curPos:J

    cmp-long v2, v2, v4

    if-lez v2, :cond_15

    .line 815
    iget-wide v2, p0, Lorg/tukaani/xz/SeekableXZInputStream;->seekPos:J

    iget-wide v4, p0, Lorg/tukaani/xz/SeekableXZInputStream;->curPos:J

    sub-long v0, v2, v4

    .line 816
    .local v0, "skipAmount":J
    iget-object v2, p0, Lorg/tukaani/xz/SeekableXZInputStream;->blockDecoder:Lorg/tukaani/xz/BlockInputStream;

    invoke-virtual {v2, v0, v1}, Lorg/tukaani/xz/BlockInputStream;->skip(J)J

    move-result-wide v2

    cmp-long v2, v2, v0

    if-eqz v2, :cond_86

    .line 817
    new-instance v2, Lorg/tukaani/xz/CorruptedInputException;

    invoke-direct {v2}, Lorg/tukaani/xz/CorruptedInputException;-><init>()V

    throw v2

    .line 819
    :cond_86
    iget-wide v2, p0, Lorg/tukaani/xz/SeekableXZInputStream;->seekPos:J

    iput-wide v2, p0, Lorg/tukaani/xz/SeekableXZInputStream;->curPos:J

    goto :goto_15
.end method


# virtual methods
.method public available()I
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 654
    iget-object v0, p0, Lorg/tukaani/xz/SeekableXZInputStream;->in:Lorg/tukaani/xz/SeekableInputStream;

    if-nez v0, :cond_c

    .line 655
    new-instance v0, Lorg/tukaani/xz/XZIOException;

    const-string v1, "Stream closed"

    invoke-direct {v0, v1}, Lorg/tukaani/xz/XZIOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 657
    :cond_c
    iget-object v0, p0, Lorg/tukaani/xz/SeekableXZInputStream;->exception:Ljava/io/IOException;

    if-eqz v0, :cond_13

    .line 658
    iget-object v0, p0, Lorg/tukaani/xz/SeekableXZInputStream;->exception:Ljava/io/IOException;

    throw v0

    .line 660
    :cond_13
    iget-boolean v0, p0, Lorg/tukaani/xz/SeekableXZInputStream;->endReached:Z

    if-nez v0, :cond_1f

    iget-boolean v0, p0, Lorg/tukaani/xz/SeekableXZInputStream;->seekNeeded:Z

    if-nez v0, :cond_1f

    iget-object v0, p0, Lorg/tukaani/xz/SeekableXZInputStream;->blockDecoder:Lorg/tukaani/xz/BlockInputStream;

    if-nez v0, :cond_21

    .line 661
    :cond_1f
    const/4 v0, 0x0

    .line 663
    :goto_20
    return v0

    :cond_21
    iget-object v0, p0, Lorg/tukaani/xz/SeekableXZInputStream;->blockDecoder:Lorg/tukaani/xz/BlockInputStream;

    invoke-virtual {v0}, Lorg/tukaani/xz/BlockInputStream;->available()I

    move-result v0

    goto :goto_20
.end method

.method public close()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 673
    iget-object v0, p0, Lorg/tukaani/xz/SeekableXZInputStream;->in:Lorg/tukaani/xz/SeekableInputStream;

    if-eqz v0, :cond_c

    .line 675
    :try_start_5
    iget-object v0, p0, Lorg/tukaani/xz/SeekableXZInputStream;->in:Lorg/tukaani/xz/SeekableInputStream;

    invoke-virtual {v0}, Lorg/tukaani/xz/SeekableInputStream;->close()V
    :try_end_a
    .catchall {:try_start_5 .. :try_end_a} :catchall_d

    .line 677
    iput-object v1, p0, Lorg/tukaani/xz/SeekableXZInputStream;->in:Lorg/tukaani/xz/SeekableInputStream;

    .line 680
    :cond_c
    return-void

    .line 677
    :catchall_d
    move-exception v0

    iput-object v1, p0, Lorg/tukaani/xz/SeekableXZInputStream;->in:Lorg/tukaani/xz/SeekableInputStream;

    throw v0
.end method

.method public getBlockCheckType(I)I
    .registers 3
    .param p1, "blockNumber"    # I

    .prologue
    .line 519
    iget-object v0, p0, Lorg/tukaani/xz/SeekableXZInputStream;->queriedBlockInfo:Lorg/tukaani/xz/index/BlockInfo;

    invoke-direct {p0, v0, p1}, Lorg/tukaani/xz/SeekableXZInputStream;->locateBlockByNumber(Lorg/tukaani/xz/index/BlockInfo;I)V

    .line 520
    iget-object v0, p0, Lorg/tukaani/xz/SeekableXZInputStream;->queriedBlockInfo:Lorg/tukaani/xz/index/BlockInfo;

    invoke-virtual {v0}, Lorg/tukaani/xz/index/BlockInfo;->getCheckType()I

    move-result v0

    return v0
.end method

.method public getBlockCompPos(I)J
    .registers 4
    .param p1, "blockNumber"    # I

    .prologue
    .line 487
    iget-object v0, p0, Lorg/tukaani/xz/SeekableXZInputStream;->queriedBlockInfo:Lorg/tukaani/xz/index/BlockInfo;

    invoke-direct {p0, v0, p1}, Lorg/tukaani/xz/SeekableXZInputStream;->locateBlockByNumber(Lorg/tukaani/xz/index/BlockInfo;I)V

    .line 488
    iget-object v0, p0, Lorg/tukaani/xz/SeekableXZInputStream;->queriedBlockInfo:Lorg/tukaani/xz/index/BlockInfo;

    iget-wide v0, v0, Lorg/tukaani/xz/index/BlockInfo;->compressedOffset:J

    return-wide v0
.end method

.method public getBlockCompSize(I)J
    .registers 6
    .param p1, "blockNumber"    # I

    .prologue
    .line 503
    iget-object v0, p0, Lorg/tukaani/xz/SeekableXZInputStream;->queriedBlockInfo:Lorg/tukaani/xz/index/BlockInfo;

    invoke-direct {p0, v0, p1}, Lorg/tukaani/xz/SeekableXZInputStream;->locateBlockByNumber(Lorg/tukaani/xz/index/BlockInfo;I)V

    .line 504
    iget-object v0, p0, Lorg/tukaani/xz/SeekableXZInputStream;->queriedBlockInfo:Lorg/tukaani/xz/index/BlockInfo;

    iget-wide v0, v0, Lorg/tukaani/xz/index/BlockInfo;->unpaddedSize:J

    const-wide/16 v2, 0x3

    add-long/2addr v0, v2

    const-wide/16 v2, -0x4

    and-long/2addr v0, v2

    return-wide v0
.end method

.method public getBlockCount()I
    .registers 2

    .prologue
    .line 444
    iget v0, p0, Lorg/tukaani/xz/SeekableXZInputStream;->blockCount:I

    return v0
.end method

.method public getBlockNumber(J)I
    .registers 4
    .param p1, "pos"    # J

    .prologue
    .line 534
    iget-object v0, p0, Lorg/tukaani/xz/SeekableXZInputStream;->queriedBlockInfo:Lorg/tukaani/xz/index/BlockInfo;

    invoke-direct {p0, v0, p1, p2}, Lorg/tukaani/xz/SeekableXZInputStream;->locateBlockByPos(Lorg/tukaani/xz/index/BlockInfo;J)V

    .line 535
    iget-object v0, p0, Lorg/tukaani/xz/SeekableXZInputStream;->queriedBlockInfo:Lorg/tukaani/xz/index/BlockInfo;

    iget v0, v0, Lorg/tukaani/xz/index/BlockInfo;->blockNumber:I

    return v0
.end method

.method public getBlockPos(I)J
    .registers 4
    .param p1, "blockNumber"    # I

    .prologue
    .line 457
    iget-object v0, p0, Lorg/tukaani/xz/SeekableXZInputStream;->queriedBlockInfo:Lorg/tukaani/xz/index/BlockInfo;

    invoke-direct {p0, v0, p1}, Lorg/tukaani/xz/SeekableXZInputStream;->locateBlockByNumber(Lorg/tukaani/xz/index/BlockInfo;I)V

    .line 458
    iget-object v0, p0, Lorg/tukaani/xz/SeekableXZInputStream;->queriedBlockInfo:Lorg/tukaani/xz/index/BlockInfo;

    iget-wide v0, v0, Lorg/tukaani/xz/index/BlockInfo;->uncompressedOffset:J

    return-wide v0
.end method

.method public getBlockSize(I)J
    .registers 4
    .param p1, "blockNumber"    # I

    .prologue
    .line 471
    iget-object v0, p0, Lorg/tukaani/xz/SeekableXZInputStream;->queriedBlockInfo:Lorg/tukaani/xz/index/BlockInfo;

    invoke-direct {p0, v0, p1}, Lorg/tukaani/xz/SeekableXZInputStream;->locateBlockByNumber(Lorg/tukaani/xz/index/BlockInfo;I)V

    .line 472
    iget-object v0, p0, Lorg/tukaani/xz/SeekableXZInputStream;->queriedBlockInfo:Lorg/tukaani/xz/index/BlockInfo;

    iget-wide v0, v0, Lorg/tukaani/xz/index/BlockInfo;->uncompressedSize:J

    return-wide v0
.end method

.method public getCheckTypes()I
    .registers 2

    .prologue
    .line 404
    iget v0, p0, Lorg/tukaani/xz/SeekableXZInputStream;->checkTypes:I

    return v0
.end method

.method public getIndexMemoryUsage()I
    .registers 2

    .prologue
    .line 414
    iget v0, p0, Lorg/tukaani/xz/SeekableXZInputStream;->indexMemoryUsage:I

    return v0
.end method

.method public getLargestBlockSize()J
    .registers 3

    .prologue
    .line 426
    iget-wide v0, p0, Lorg/tukaani/xz/SeekableXZInputStream;->largestBlockSize:J

    return-wide v0
.end method

.method public getStreamCount()I
    .registers 2

    .prologue
    .line 435
    iget-object v0, p0, Lorg/tukaani/xz/SeekableXZInputStream;->streams:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public length()J
    .registers 3

    .prologue
    .line 687
    iget-wide v0, p0, Lorg/tukaani/xz/SeekableXZInputStream;->uncompressedSize:J

    return-wide v0
.end method

.method public position()J
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 696
    iget-object v0, p0, Lorg/tukaani/xz/SeekableXZInputStream;->in:Lorg/tukaani/xz/SeekableInputStream;

    if-nez v0, :cond_c

    .line 697
    new-instance v0, Lorg/tukaani/xz/XZIOException;

    const-string v1, "Stream closed"

    invoke-direct {v0, v1}, Lorg/tukaani/xz/XZIOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 699
    :cond_c
    iget-boolean v0, p0, Lorg/tukaani/xz/SeekableXZInputStream;->seekNeeded:Z

    if-eqz v0, :cond_13

    iget-wide v0, p0, Lorg/tukaani/xz/SeekableXZInputStream;->seekPos:J

    :goto_12
    return-wide v0

    :cond_13
    iget-wide v0, p0, Lorg/tukaani/xz/SeekableXZInputStream;->curPos:J

    goto :goto_12
.end method

.method public read()I
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v0, -0x1

    .line 553
    iget-object v1, p0, Lorg/tukaani/xz/SeekableXZInputStream;->tempBuf:[B

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v3, v2}, Lorg/tukaani/xz/SeekableXZInputStream;->read([BII)I

    move-result v1

    if-ne v1, v0, :cond_c

    :goto_b
    return v0

    :cond_c
    iget-object v0, p0, Lorg/tukaani/xz/SeekableXZInputStream;->tempBuf:[B

    aget-byte v0, v0, v3

    and-int/lit16 v0, v0, 0xff

    goto :goto_b
.end method

.method public read([BII)I
    .registers 12
    .param p1, "buf"    # [B
    .param p2, "off"    # I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, -0x1

    .line 588
    if-ltz p2, :cond_e

    if-ltz p3, :cond_e

    add-int v4, p2, p3

    if-ltz v4, :cond_e

    add-int v4, p2, p3

    array-length v5, p1

    if-le v4, v5, :cond_14

    .line 589
    :cond_e
    new-instance v3, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v3}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v3

    .line 591
    :cond_14
    if-nez p3, :cond_18

    .line 592
    const/4 v2, 0x0

    .line 639
    :cond_17
    :goto_17
    return v2

    .line 594
    :cond_18
    iget-object v4, p0, Lorg/tukaani/xz/SeekableXZInputStream;->in:Lorg/tukaani/xz/SeekableInputStream;

    if-nez v4, :cond_24

    .line 595
    new-instance v3, Lorg/tukaani/xz/XZIOException;

    const-string v4, "Stream closed"

    invoke-direct {v3, v4}, Lorg/tukaani/xz/XZIOException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 597
    :cond_24
    iget-object v4, p0, Lorg/tukaani/xz/SeekableXZInputStream;->exception:Ljava/io/IOException;

    if-eqz v4, :cond_2b

    .line 598
    iget-object v3, p0, Lorg/tukaani/xz/SeekableXZInputStream;->exception:Ljava/io/IOException;

    throw v3

    .line 600
    :cond_2b
    const/4 v2, 0x0

    .line 603
    .local v2, "size":I
    :try_start_2c
    iget-boolean v4, p0, Lorg/tukaani/xz/SeekableXZInputStream;->seekNeeded:Z

    if-eqz v4, :cond_33

    .line 604
    invoke-direct {p0}, Lorg/tukaani/xz/SeekableXZInputStream;->seek()V

    .line 606
    :cond_33
    iget-boolean v4, p0, Lorg/tukaani/xz/SeekableXZInputStream;->endReached:Z

    if-eqz v4, :cond_4a

    move v2, v3

    .line 607
    goto :goto_17

    .line 616
    :cond_39
    iget-object v4, p0, Lorg/tukaani/xz/SeekableXZInputStream;->blockDecoder:Lorg/tukaani/xz/BlockInputStream;

    invoke-virtual {v4, p1, p2, p3}, Lorg/tukaani/xz/BlockInputStream;->read([BII)I

    move-result v1

    .line 618
    .local v1, "ret":I
    if-lez v1, :cond_58

    .line 619
    iget-wide v4, p0, Lorg/tukaani/xz/SeekableXZInputStream;->curPos:J

    int-to-long v6, v1

    add-long/2addr v4, v6

    iput-wide v4, p0, Lorg/tukaani/xz/SeekableXZInputStream;->curPos:J

    .line 620
    add-int/2addr v2, v1

    .line 621
    add-int/2addr p2, v1

    .line 622
    sub-int/2addr p3, v1

    .line 609
    .end local v1    # "ret":I
    :cond_4a
    :goto_4a
    if-lez p3, :cond_17

    .line 610
    iget-object v4, p0, Lorg/tukaani/xz/SeekableXZInputStream;->blockDecoder:Lorg/tukaani/xz/BlockInputStream;

    if-nez v4, :cond_39

    .line 611
    invoke-direct {p0}, Lorg/tukaani/xz/SeekableXZInputStream;->seek()V

    .line 612
    iget-boolean v4, p0, Lorg/tukaani/xz/SeekableXZInputStream;->endReached:Z

    if-eqz v4, :cond_39

    goto :goto_17

    .line 623
    .restart local v1    # "ret":I
    :cond_58
    if-ne v1, v3, :cond_4a

    .line 624
    const/4 v4, 0x0

    iput-object v4, p0, Lorg/tukaani/xz/SeekableXZInputStream;->blockDecoder:Lorg/tukaani/xz/BlockInputStream;
    :try_end_5d
    .catch Ljava/io/IOException; {:try_start_2c .. :try_end_5d} :catch_5e

    goto :goto_4a

    .line 627
    .end local v1    # "ret":I
    :catch_5e
    move-exception v0

    .line 631
    .local v0, "e":Ljava/io/IOException;
    instance-of v3, v0, Ljava/io/EOFException;

    if-eqz v3, :cond_68

    .line 632
    new-instance v0, Lorg/tukaani/xz/CorruptedInputException;

    .end local v0    # "e":Ljava/io/IOException;
    invoke-direct {v0}, Lorg/tukaani/xz/CorruptedInputException;-><init>()V

    .line 634
    .restart local v0    # "e":Ljava/io/IOException;
    :cond_68
    iput-object v0, p0, Lorg/tukaani/xz/SeekableXZInputStream;->exception:Ljava/io/IOException;

    .line 635
    if-nez v2, :cond_17

    .line 636
    throw v0
.end method

.method public seek(J)V
    .registers 6
    .param p1, "pos"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 719
    iget-object v0, p0, Lorg/tukaani/xz/SeekableXZInputStream;->in:Lorg/tukaani/xz/SeekableInputStream;

    if-nez v0, :cond_c

    .line 720
    new-instance v0, Lorg/tukaani/xz/XZIOException;

    const-string v1, "Stream closed"

    invoke-direct {v0, v1}, Lorg/tukaani/xz/XZIOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 722
    :cond_c
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gez v0, :cond_2b

    .line 723
    new-instance v0, Lorg/tukaani/xz/XZIOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Negative seek position: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/tukaani/xz/XZIOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 725
    :cond_2b
    iput-wide p1, p0, Lorg/tukaani/xz/SeekableXZInputStream;->seekPos:J

    .line 726
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/tukaani/xz/SeekableXZInputStream;->seekNeeded:Z

    .line 727
    return-void
.end method

.method public seekToBlock(I)V
    .registers 5
    .param p1, "blockNumber"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 740
    iget-object v0, p0, Lorg/tukaani/xz/SeekableXZInputStream;->in:Lorg/tukaani/xz/SeekableInputStream;

    if-nez v0, :cond_c

    .line 741
    new-instance v0, Lorg/tukaani/xz/XZIOException;

    const-string v1, "Stream closed"

    invoke-direct {v0, v1}, Lorg/tukaani/xz/XZIOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 743
    :cond_c
    if-ltz p1, :cond_12

    iget v0, p0, Lorg/tukaani/xz/SeekableXZInputStream;->blockCount:I

    if-lt p1, v0, :cond_2b

    .line 744
    :cond_12
    new-instance v0, Lorg/tukaani/xz/XZIOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid XZ Block number: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/tukaani/xz/XZIOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 749
    :cond_2b
    invoke-virtual {p0, p1}, Lorg/tukaani/xz/SeekableXZInputStream;->getBlockPos(I)J

    move-result-wide v0

    iput-wide v0, p0, Lorg/tukaani/xz/SeekableXZInputStream;->seekPos:J

    .line 750
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/tukaani/xz/SeekableXZInputStream;->seekNeeded:Z

    .line 751
    return-void
.end method
