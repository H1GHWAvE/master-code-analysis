.class public final Lorg/tukaani/xz/simple/X86;
.super Ljava/lang/Object;
.source "X86.java"

# interfaces
.implements Lorg/tukaani/xz/simple/SimpleFilter;


# static fields
.field private static final MASK_TO_ALLOWED_STATUS:[Z

.field private static final MASK_TO_BIT_NUMBER:[I


# instance fields
.field private final isEncoder:Z

.field private pos:I

.field private prevMask:I


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    const/16 v1, 0x8

    .line 14
    new-array v0, v1, [Z

    fill-array-data v0, :array_12

    sput-object v0, Lorg/tukaani/xz/simple/X86;->MASK_TO_ALLOWED_STATUS:[Z

    .line 17
    new-array v0, v1, [I

    fill-array-data v0, :array_1a

    sput-object v0, Lorg/tukaani/xz/simple/X86;->MASK_TO_BIT_NUMBER:[I

    return-void

    .line 14
    nop

    :array_12
    .array-data 1
        0x1t
        0x1t
        0x1t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 17
    :array_1a
    .array-data 4
        0x0
        0x1
        0x2
        0x2
        0x3
        0x3
        0x3
        0x3
    .end array-data
.end method

.method public constructor <init>(ZI)V
    .registers 4
    .param p1, "isEncoder"    # Z
    .param p2, "startPos"    # I

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const/4 v0, 0x0

    iput v0, p0, Lorg/tukaani/xz/simple/X86;->prevMask:I

    .line 29
    iput-boolean p1, p0, Lorg/tukaani/xz/simple/X86;->isEncoder:Z

    .line 30
    add-int/lit8 v0, p2, 0x5

    iput v0, p0, Lorg/tukaani/xz/simple/X86;->pos:I

    .line 31
    return-void
.end method

.method private static test86MSByte(B)Z
    .registers 3
    .param p0, "b"    # B

    .prologue
    .line 24
    and-int/lit16 v0, p0, 0xff

    .line 25
    .local v0, "i":I
    if-eqz v0, :cond_8

    const/16 v1, 0xff

    if-ne v0, v1, :cond_a

    :cond_8
    const/4 v1, 0x1

    :goto_9
    return v1

    :cond_a
    const/4 v1, 0x0

    goto :goto_9
.end method


# virtual methods
.method public code([BII)I
    .registers 14
    .param p1, "buf"    # [B
    .param p2, "off"    # I
    .param p3, "len"    # I

    .prologue
    const/4 v6, 0x0

    .line 34
    add-int/lit8 v4, p2, -0x1

    .line 35
    .local v4, "prevPos":I
    add-int v7, p2, p3

    add-int/lit8 v1, v7, -0x5

    .line 38
    .local v1, "end":I
    move v2, p2

    .local v2, "i":I
    :goto_8
    if-gt v2, v1, :cond_da

    .line 39
    aget-byte v7, p1, v2

    and-int/lit16 v7, v7, 0xfe

    const/16 v8, 0xe8

    if-eq v7, v8, :cond_15

    .line 38
    :goto_12
    add-int/lit8 v2, v2, 0x1

    goto :goto_8

    .line 42
    :cond_15
    sub-int v4, v2, v4

    .line 43
    and-int/lit8 v7, v4, -0x4

    if-eqz v7, :cond_7b

    .line 44
    iput v6, p0, Lorg/tukaani/xz/simple/X86;->prevMask:I

    .line 57
    :cond_1d
    move v4, v2

    .line 59
    add-int/lit8 v7, v2, 0x4

    aget-byte v7, p1, v7

    invoke-static {v7}, Lorg/tukaani/xz/simple/X86;->test86MSByte(B)Z

    move-result v7

    if-eqz v7, :cond_d0

    .line 60
    add-int/lit8 v7, v2, 0x1

    aget-byte v7, p1, v7

    and-int/lit16 v7, v7, 0xff

    add-int/lit8 v8, v2, 0x2

    aget-byte v8, p1, v8

    and-int/lit16 v8, v8, 0xff

    shl-int/lit8 v8, v8, 0x8

    or-int/2addr v7, v8

    add-int/lit8 v8, v2, 0x3

    aget-byte v8, p1, v8

    and-int/lit16 v8, v8, 0xff

    shl-int/lit8 v8, v8, 0x10

    or-int/2addr v7, v8

    add-int/lit8 v8, v2, 0x4

    aget-byte v8, p1, v8

    and-int/lit16 v8, v8, 0xff

    shl-int/lit8 v8, v8, 0x18

    or-int v5, v7, v8

    .line 66
    .local v5, "src":I
    :goto_4a
    iget-boolean v7, p0, Lorg/tukaani/xz/simple/X86;->isEncoder:Z

    if-eqz v7, :cond_ac

    .line 67
    iget v7, p0, Lorg/tukaani/xz/simple/X86;->pos:I

    add-int/2addr v7, v2

    sub-int/2addr v7, p2

    add-int v0, v5, v7

    .line 71
    .local v0, "dest":I
    :goto_54
    iget v7, p0, Lorg/tukaani/xz/simple/X86;->prevMask:I

    if-nez v7, :cond_b3

    .line 81
    :cond_58
    add-int/lit8 v7, v2, 0x1

    int-to-byte v8, v0

    aput-byte v8, p1, v7

    .line 82
    add-int/lit8 v7, v2, 0x2

    ushr-int/lit8 v8, v0, 0x8

    int-to-byte v8, v8

    aput-byte v8, p1, v7

    .line 83
    add-int/lit8 v7, v2, 0x3

    ushr-int/lit8 v8, v0, 0x10

    int-to-byte v8, v8

    aput-byte v8, p1, v7

    .line 84
    add-int/lit8 v7, v2, 0x4

    ushr-int/lit8 v8, v0, 0x18

    and-int/lit8 v8, v8, 0x1

    add-int/lit8 v8, v8, -0x1

    xor-int/lit8 v8, v8, -0x1

    int-to-byte v8, v8

    aput-byte v8, p1, v7

    .line 85
    add-int/lit8 v2, v2, 0x4

    .line 86
    goto :goto_12

    .line 46
    .end local v0    # "dest":I
    .end local v5    # "src":I
    :cond_7b
    iget v7, p0, Lorg/tukaani/xz/simple/X86;->prevMask:I

    add-int/lit8 v8, v4, -0x1

    shl-int/2addr v7, v8

    and-int/lit8 v7, v7, 0x7

    iput v7, p0, Lorg/tukaani/xz/simple/X86;->prevMask:I

    .line 47
    iget v7, p0, Lorg/tukaani/xz/simple/X86;->prevMask:I

    if-eqz v7, :cond_1d

    .line 48
    sget-object v7, Lorg/tukaani/xz/simple/X86;->MASK_TO_ALLOWED_STATUS:[Z

    iget v8, p0, Lorg/tukaani/xz/simple/X86;->prevMask:I

    aget-boolean v7, v7, v8

    if-eqz v7, :cond_a1

    add-int/lit8 v7, v2, 0x4

    sget-object v8, Lorg/tukaani/xz/simple/X86;->MASK_TO_BIT_NUMBER:[I

    iget v9, p0, Lorg/tukaani/xz/simple/X86;->prevMask:I

    aget v8, v8, v9

    sub-int/2addr v7, v8

    aget-byte v7, p1, v7

    invoke-static {v7}, Lorg/tukaani/xz/simple/X86;->test86MSByte(B)Z

    move-result v7

    if-eqz v7, :cond_1d

    .line 50
    :cond_a1
    move v4, v2

    .line 51
    iget v7, p0, Lorg/tukaani/xz/simple/X86;->prevMask:I

    shl-int/lit8 v7, v7, 0x1

    or-int/lit8 v7, v7, 0x1

    iput v7, p0, Lorg/tukaani/xz/simple/X86;->prevMask:I

    goto/16 :goto_12

    .line 69
    .restart local v5    # "src":I
    :cond_ac
    iget v7, p0, Lorg/tukaani/xz/simple/X86;->pos:I

    add-int/2addr v7, v2

    sub-int/2addr v7, p2

    sub-int v0, v5, v7

    .restart local v0    # "dest":I
    goto :goto_54

    .line 74
    :cond_b3
    sget-object v7, Lorg/tukaani/xz/simple/X86;->MASK_TO_BIT_NUMBER:[I

    iget v8, p0, Lorg/tukaani/xz/simple/X86;->prevMask:I

    aget v7, v7, v8

    mul-int/lit8 v3, v7, 0x8

    .line 75
    .local v3, "index":I
    rsub-int/lit8 v7, v3, 0x18

    ushr-int v7, v0, v7

    int-to-byte v7, v7

    invoke-static {v7}, Lorg/tukaani/xz/simple/X86;->test86MSByte(B)Z

    move-result v7

    if-eqz v7, :cond_58

    .line 78
    const/4 v7, 0x1

    rsub-int/lit8 v8, v3, 0x20

    shl-int/2addr v7, v8

    add-int/lit8 v7, v7, -0x1

    xor-int v5, v0, v7

    .line 79
    goto/16 :goto_4a

    .line 87
    .end local v0    # "dest":I
    .end local v3    # "index":I
    .end local v5    # "src":I
    :cond_d0
    iget v7, p0, Lorg/tukaani/xz/simple/X86;->prevMask:I

    shl-int/lit8 v7, v7, 0x1

    or-int/lit8 v7, v7, 0x1

    iput v7, p0, Lorg/tukaani/xz/simple/X86;->prevMask:I

    goto/16 :goto_12

    .line 91
    :cond_da
    sub-int v4, v2, v4

    .line 92
    and-int/lit8 v7, v4, -0x4

    if-eqz v7, :cond_e9

    :goto_e0
    iput v6, p0, Lorg/tukaani/xz/simple/X86;->prevMask:I

    .line 94
    sub-int/2addr v2, p2

    .line 95
    iget v6, p0, Lorg/tukaani/xz/simple/X86;->pos:I

    add-int/2addr v6, v2

    iput v6, p0, Lorg/tukaani/xz/simple/X86;->pos:I

    .line 96
    return v2

    .line 92
    :cond_e9
    iget v6, p0, Lorg/tukaani/xz/simple/X86;->prevMask:I

    add-int/lit8 v7, v4, -0x1

    shl-int/2addr v6, v7

    goto :goto_e0
.end method
