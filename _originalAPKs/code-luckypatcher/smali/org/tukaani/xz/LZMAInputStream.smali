.class public Lorg/tukaani/xz/LZMAInputStream;
.super Ljava/io/InputStream;
.source "LZMAInputStream.java"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final DICT_SIZE_MAX:I = 0x7ffffff0


# instance fields
.field private endReached:Z

.field private exception:Ljava/io/IOException;

.field private in:Ljava/io/InputStream;

.field private lz:Lorg/tukaani/xz/lz/LZDecoder;

.field private lzma:Lorg/tukaani/xz/lzma/LZMADecoder;

.field private rc:Lorg/tukaani/xz/rangecoder/RangeDecoderFromStream;

.field private remainingSize:J

.field private final tempBuf:[B


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 37
    const-class v0, Lorg/tukaani/xz/LZMAInputStream;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_c

    const/4 v0, 0x1

    :goto_9
    sput-boolean v0, Lorg/tukaani/xz/LZMAInputStream;->$assertionsDisabled:Z

    return-void

    :cond_c
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .registers 3
    .param p1, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 182
    const/4 v0, -0x1

    invoke-direct {p0, p1, v0}, Lorg/tukaani/xz/LZMAInputStream;-><init>(Ljava/io/InputStream;I)V

    .line 183
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;I)V
    .registers 14
    .param p1, "in"    # Ljava/io/InputStream;
    .param p2, "memoryLimit"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 216
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 54
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/tukaani/xz/LZMAInputStream;->endReached:Z

    .line 56
    const/4 v0, 0x1

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/tukaani/xz/LZMAInputStream;->tempBuf:[B

    .line 64
    iput-object v6, p0, Lorg/tukaani/xz/LZMAInputStream;->exception:Ljava/io/IOException;

    .line 217
    new-instance v8, Ljava/io/DataInputStream;

    invoke-direct {v8, p1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 220
    .local v8, "inData":Ljava/io/DataInputStream;
    invoke-virtual {v8}, Ljava/io/DataInputStream;->readByte()B

    move-result v4

    .line 223
    .local v4, "propsByte":B
    const/4 v5, 0x0

    .line 224
    .local v5, "dictSize":I
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_19
    const/4 v0, 0x4

    if-ge v7, v0, :cond_27

    .line 225
    invoke-virtual {v8}, Ljava/io/DataInputStream;->readUnsignedByte()I

    move-result v0

    mul-int/lit8 v1, v7, 0x8

    shl-int/2addr v0, v1

    or-int/2addr v5, v0

    .line 224
    add-int/lit8 v7, v7, 0x1

    goto :goto_19

    .line 231
    :cond_27
    const-wide/16 v2, 0x0

    .line 232
    .local v2, "uncompSize":J
    const/4 v7, 0x0

    :goto_2a
    const/16 v0, 0x8

    if-ge v7, v0, :cond_3a

    .line 233
    invoke-virtual {v8}, Ljava/io/DataInputStream;->readUnsignedByte()I

    move-result v0

    int-to-long v0, v0

    mul-int/lit8 v10, v7, 0x8

    shl-long/2addr v0, v10

    or-long/2addr v2, v0

    .line 232
    add-int/lit8 v7, v7, 0x1

    goto :goto_2a

    .line 236
    :cond_3a
    invoke-static {v5, v4}, Lorg/tukaani/xz/LZMAInputStream;->getMemoryUsage(IB)I

    move-result v9

    .line 237
    .local v9, "memoryNeeded":I
    const/4 v0, -0x1

    if-eq p2, v0, :cond_49

    if-le v9, p2, :cond_49

    .line 238
    new-instance v0, Lorg/tukaani/xz/MemoryLimitException;

    invoke-direct {v0, v9, p2}, Lorg/tukaani/xz/MemoryLimitException;-><init>(II)V

    throw v0

    :cond_49
    move-object v0, p0

    move-object v1, p1

    .line 240
    invoke-direct/range {v0 .. v6}, Lorg/tukaani/xz/LZMAInputStream;->initialize(Ljava/io/InputStream;JBI[B)V

    .line 241
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;JBI)V
    .registers 13
    .param p1, "in"    # Ljava/io/InputStream;
    .param p2, "uncompSize"    # J
    .param p4, "propsByte"    # B
    .param p5, "dictSize"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 290
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 54
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/tukaani/xz/LZMAInputStream;->endReached:Z

    .line 56
    const/4 v0, 0x1

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/tukaani/xz/LZMAInputStream;->tempBuf:[B

    .line 64
    iput-object v6, p0, Lorg/tukaani/xz/LZMAInputStream;->exception:Ljava/io/IOException;

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move v4, p4

    move v5, p5

    .line 291
    invoke-direct/range {v0 .. v6}, Lorg/tukaani/xz/LZMAInputStream;->initialize(Ljava/io/InputStream;JBI[B)V

    .line 292
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;JBI[B)V
    .registers 8
    .param p1, "in"    # Ljava/io/InputStream;
    .param p2, "uncompSize"    # J
    .param p4, "propsByte"    # B
    .param p5, "dictSize"    # I
    .param p6, "presetDict"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 328
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 54
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/tukaani/xz/LZMAInputStream;->endReached:Z

    .line 56
    const/4 v0, 0x1

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/tukaani/xz/LZMAInputStream;->tempBuf:[B

    .line 64
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/tukaani/xz/LZMAInputStream;->exception:Ljava/io/IOException;

    .line 329
    invoke-direct/range {p0 .. p6}, Lorg/tukaani/xz/LZMAInputStream;->initialize(Ljava/io/InputStream;JBI[B)V

    .line 330
    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;JIIII[B)V
    .registers 10
    .param p1, "in"    # Ljava/io/InputStream;
    .param p2, "uncompSize"    # J
    .param p4, "lc"    # I
    .param p5, "lp"    # I
    .param p6, "pb"    # I
    .param p7, "dictSize"    # I
    .param p8, "presetDict"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 367
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 54
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/tukaani/xz/LZMAInputStream;->endReached:Z

    .line 56
    const/4 v0, 0x1

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/tukaani/xz/LZMAInputStream;->tempBuf:[B

    .line 64
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/tukaani/xz/LZMAInputStream;->exception:Ljava/io/IOException;

    .line 368
    invoke-direct/range {p0 .. p8}, Lorg/tukaani/xz/LZMAInputStream;->initialize(Ljava/io/InputStream;JIIII[B)V

    .line 369
    return-void
.end method

.method private static getDictSize(I)I
    .registers 3
    .param p0, "dictSize"    # I

    .prologue
    .line 136
    if-ltz p0, :cond_7

    const v0, 0x7ffffff0

    if-le p0, v0, :cond_f

    .line 137
    :cond_7
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "LZMA dictionary is too big for this implementation"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 150
    :cond_f
    const/16 v0, 0x1000

    if-ge p0, v0, :cond_15

    .line 151
    const/16 p0, 0x1000

    .line 155
    :cond_15
    add-int/lit8 v0, p0, 0xf

    and-int/lit8 v0, v0, -0x10

    return v0
.end method

.method public static getMemoryUsage(IB)I
    .registers 7
    .param p0, "dictSize"    # I
    .param p1, "propsByte"    # B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/tukaani/xz/UnsupportedOptionsException;,
            Lorg/tukaani/xz/CorruptedInputException;
        }
    .end annotation

    .prologue
    .line 89
    if-ltz p0, :cond_7

    const v3, 0x7ffffff0

    if-le p0, v3, :cond_f

    .line 90
    :cond_7
    new-instance v3, Lorg/tukaani/xz/UnsupportedOptionsException;

    const-string v4, "LZMA dictionary is too big for this implementation"

    invoke-direct {v3, v4}, Lorg/tukaani/xz/UnsupportedOptionsException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 93
    :cond_f
    and-int/lit16 v2, p1, 0xff

    .line 94
    .local v2, "props":I
    const/16 v3, 0xe0

    if-le v2, v3, :cond_1d

    .line 95
    new-instance v3, Lorg/tukaani/xz/CorruptedInputException;

    const-string v4, "Invalid LZMA properties byte"

    invoke-direct {v3, v4}, Lorg/tukaani/xz/CorruptedInputException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 97
    :cond_1d
    rem-int/lit8 v2, v2, 0x2d

    .line 98
    div-int/lit8 v1, v2, 0x9

    .line 99
    .local v1, "lp":I
    mul-int/lit8 v3, v1, 0x9

    sub-int v0, v2, v3

    .line 101
    .local v0, "lc":I
    invoke-static {p0, v0, v1}, Lorg/tukaani/xz/LZMAInputStream;->getMemoryUsage(III)I

    move-result v3

    return v3
.end method

.method public static getMemoryUsage(III)I
    .registers 6
    .param p0, "dictSize"    # I
    .param p1, "lc"    # I
    .param p2, "lp"    # I

    .prologue
    .line 121
    if-ltz p1, :cond_b

    const/16 v0, 0x8

    if-gt p1, v0, :cond_b

    if-ltz p2, :cond_b

    const/4 v0, 0x4

    if-le p2, v0, :cond_13

    .line 122
    :cond_b
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid lc or lp"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 131
    :cond_13
    invoke-static {p0}, Lorg/tukaani/xz/LZMAInputStream;->getDictSize(I)I

    move-result v0

    div-int/lit16 v0, v0, 0x400

    add-int/lit8 v0, v0, 0xa

    const/16 v1, 0x600

    add-int v2, p1, p2

    shl-int/2addr v1, v2

    div-int/lit16 v1, v1, 0x400

    add-int/2addr v0, v1

    return v0
.end method

.method private initialize(Ljava/io/InputStream;JBI[B)V
    .registers 17
    .param p1, "in"    # Ljava/io/InputStream;
    .param p2, "uncompSize"    # J
    .param p4, "propsByte"    # B
    .param p5, "dictSize"    # I
    .param p6, "presetDict"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 376
    const-wide/16 v0, -0x1

    cmp-long v0, p2, v0

    if-gez v0, :cond_e

    .line 377
    new-instance v0, Lorg/tukaani/xz/UnsupportedOptionsException;

    const-string v1, "Uncompressed size is too big"

    invoke-direct {v0, v1}, Lorg/tukaani/xz/UnsupportedOptionsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 382
    :cond_e
    and-int/lit16 v9, p4, 0xff

    .line 383
    .local v9, "props":I
    const/16 v0, 0xe0

    if-le v9, v0, :cond_1c

    .line 384
    new-instance v0, Lorg/tukaani/xz/CorruptedInputException;

    const-string v1, "Invalid LZMA properties byte"

    invoke-direct {v0, v1}, Lorg/tukaani/xz/CorruptedInputException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 386
    :cond_1c
    div-int/lit8 v6, v9, 0x2d

    .line 387
    .local v6, "pb":I
    mul-int/lit8 v0, v6, 0x9

    mul-int/lit8 v0, v0, 0x5

    sub-int/2addr v9, v0

    .line 388
    div-int/lit8 v5, v9, 0x9

    .line 389
    .local v5, "lp":I
    mul-int/lit8 v0, v5, 0x9

    sub-int v4, v9, v0

    .line 393
    .local v4, "lc":I
    if-ltz p5, :cond_30

    const v0, 0x7ffffff0

    if-le p5, v0, :cond_38

    .line 394
    :cond_30
    new-instance v0, Lorg/tukaani/xz/UnsupportedOptionsException;

    const-string v1, "LZMA dictionary is too big for this implementation"

    invoke-direct {v0, v1}, Lorg/tukaani/xz/UnsupportedOptionsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_38
    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move v7, p5

    move-object/from16 v8, p6

    .line 397
    invoke-direct/range {v0 .. v8}, Lorg/tukaani/xz/LZMAInputStream;->initialize(Ljava/io/InputStream;JIIII[B)V

    .line 398
    return-void
.end method

.method private initialize(Ljava/io/InputStream;JIIII[B)V
    .registers 15
    .param p1, "in"    # Ljava/io/InputStream;
    .param p2, "uncompSize"    # J
    .param p4, "lc"    # I
    .param p5, "lp"    # I
    .param p6, "pb"    # I
    .param p7, "dictSize"    # I
    .param p8, "presetDict"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x4

    .line 406
    const-wide/16 v0, -0x1

    cmp-long v0, p2, v0

    if-ltz v0, :cond_15

    if-ltz p4, :cond_15

    const/16 v0, 0x8

    if-gt p4, v0, :cond_15

    if-ltz p5, :cond_15

    if-gt p5, v2, :cond_15

    if-ltz p6, :cond_15

    if-le p6, v2, :cond_1b

    .line 408
    :cond_15
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 410
    :cond_1b
    iput-object p1, p0, Lorg/tukaani/xz/LZMAInputStream;->in:Ljava/io/InputStream;

    .line 414
    invoke-static {p7}, Lorg/tukaani/xz/LZMAInputStream;->getDictSize(I)I

    move-result p7

    .line 415
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-ltz v0, :cond_31

    int-to-long v0, p7

    cmp-long v0, v0, p2

    if-lez v0, :cond_31

    .line 416
    long-to-int v0, p2

    invoke-static {v0}, Lorg/tukaani/xz/LZMAInputStream;->getDictSize(I)I

    move-result p7

    .line 418
    :cond_31
    new-instance v0, Lorg/tukaani/xz/lz/LZDecoder;

    invoke-static {p7}, Lorg/tukaani/xz/LZMAInputStream;->getDictSize(I)I

    move-result v1

    invoke-direct {v0, v1, p8}, Lorg/tukaani/xz/lz/LZDecoder;-><init>(I[B)V

    iput-object v0, p0, Lorg/tukaani/xz/LZMAInputStream;->lz:Lorg/tukaani/xz/lz/LZDecoder;

    .line 419
    new-instance v0, Lorg/tukaani/xz/rangecoder/RangeDecoderFromStream;

    invoke-direct {v0, p1}, Lorg/tukaani/xz/rangecoder/RangeDecoderFromStream;-><init>(Ljava/io/InputStream;)V

    iput-object v0, p0, Lorg/tukaani/xz/LZMAInputStream;->rc:Lorg/tukaani/xz/rangecoder/RangeDecoderFromStream;

    .line 420
    new-instance v0, Lorg/tukaani/xz/lzma/LZMADecoder;

    iget-object v1, p0, Lorg/tukaani/xz/LZMAInputStream;->lz:Lorg/tukaani/xz/lz/LZDecoder;

    iget-object v2, p0, Lorg/tukaani/xz/LZMAInputStream;->rc:Lorg/tukaani/xz/rangecoder/RangeDecoderFromStream;

    move v3, p4

    move v4, p5

    move v5, p6

    invoke-direct/range {v0 .. v5}, Lorg/tukaani/xz/lzma/LZMADecoder;-><init>(Lorg/tukaani/xz/lz/LZDecoder;Lorg/tukaani/xz/rangecoder/RangeDecoder;III)V

    iput-object v0, p0, Lorg/tukaani/xz/LZMAInputStream;->lzma:Lorg/tukaani/xz/lzma/LZMADecoder;

    .line 421
    iput-wide p2, p0, Lorg/tukaani/xz/LZMAInputStream;->remainingSize:J

    .line 422
    return-void
.end method


# virtual methods
.method public close()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 561
    iget-object v0, p0, Lorg/tukaani/xz/LZMAInputStream;->in:Ljava/io/InputStream;

    if-eqz v0, :cond_c

    .line 563
    :try_start_5
    iget-object v0, p0, Lorg/tukaani/xz/LZMAInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_a
    .catchall {:try_start_5 .. :try_end_a} :catchall_d

    .line 565
    iput-object v1, p0, Lorg/tukaani/xz/LZMAInputStream;->in:Ljava/io/InputStream;

    .line 568
    :cond_c
    return-void

    .line 565
    :catchall_d
    move-exception v0

    iput-object v1, p0, Lorg/tukaani/xz/LZMAInputStream;->in:Ljava/io/InputStream;

    throw v0
.end method

.method public read()I
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v0, -0x1

    .line 444
    iget-object v1, p0, Lorg/tukaani/xz/LZMAInputStream;->tempBuf:[B

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v3, v2}, Lorg/tukaani/xz/LZMAInputStream;->read([BII)I

    move-result v1

    if-ne v1, v0, :cond_c

    :goto_b
    return v0

    :cond_c
    iget-object v0, p0, Lorg/tukaani/xz/LZMAInputStream;->tempBuf:[B

    aget-byte v0, v0, v3

    and-int/lit16 v0, v0, 0xff

    goto :goto_b
.end method

.method public read([BII)I
    .registers 15
    .param p1, "buf"    # [B
    .param p2, "off"    # I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, -0x1

    const-wide/16 v9, 0x0

    .line 471
    if-ltz p2, :cond_10

    if-ltz p3, :cond_10

    add-int v5, p2, p3

    if-ltz v5, :cond_10

    add-int v5, p2, p3

    array-length v6, p1

    if-le v5, v6, :cond_16

    .line 472
    :cond_10
    new-instance v4, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v4}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v4

    .line 474
    :cond_16
    if-nez p3, :cond_1a

    .line 475
    const/4 v3, 0x0

    .line 546
    :cond_19
    :goto_19
    return v3

    .line 477
    :cond_1a
    iget-object v5, p0, Lorg/tukaani/xz/LZMAInputStream;->in:Ljava/io/InputStream;

    if-nez v5, :cond_26

    .line 478
    new-instance v4, Lorg/tukaani/xz/XZIOException;

    const-string v5, "Stream closed"

    invoke-direct {v4, v5}, Lorg/tukaani/xz/XZIOException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 480
    :cond_26
    iget-object v5, p0, Lorg/tukaani/xz/LZMAInputStream;->exception:Ljava/io/IOException;

    if-eqz v5, :cond_2d

    .line 481
    iget-object v4, p0, Lorg/tukaani/xz/LZMAInputStream;->exception:Ljava/io/IOException;

    throw v4

    .line 483
    :cond_2d
    iget-boolean v5, p0, Lorg/tukaani/xz/LZMAInputStream;->endReached:Z

    if-eqz v5, :cond_33

    move v3, v4

    .line 484
    goto :goto_19

    .line 487
    :cond_33
    const/4 v3, 0x0

    .line 489
    .local v3, "size":I
    :cond_34
    if-lez p3, :cond_19

    .line 493
    move v1, p3

    .line 494
    .local v1, "copySizeMax":I
    :try_start_37
    iget-wide v5, p0, Lorg/tukaani/xz/LZMAInputStream;->remainingSize:J

    cmp-long v5, v5, v9

    if-ltz v5, :cond_47

    iget-wide v5, p0, Lorg/tukaani/xz/LZMAInputStream;->remainingSize:J

    int-to-long v7, p3

    cmp-long v5, v5, v7

    if-gez v5, :cond_47

    .line 495
    iget-wide v5, p0, Lorg/tukaani/xz/LZMAInputStream;->remainingSize:J

    long-to-int v1, v5

    .line 497
    :cond_47
    iget-object v5, p0, Lorg/tukaani/xz/LZMAInputStream;->lz:Lorg/tukaani/xz/lz/LZDecoder;

    invoke-virtual {v5, v1}, Lorg/tukaani/xz/lz/LZDecoder;->setLimit(I)V
    :try_end_4c
    .catch Ljava/io/IOException; {:try_start_37 .. :try_end_4c} :catch_76

    .line 501
    :try_start_4c
    iget-object v5, p0, Lorg/tukaani/xz/LZMAInputStream;->lzma:Lorg/tukaani/xz/lzma/LZMADecoder;

    invoke-virtual {v5}, Lorg/tukaani/xz/lzma/LZMADecoder;->decode()V
    :try_end_51
    .catch Lorg/tukaani/xz/CorruptedInputException; {:try_start_4c .. :try_end_51} :catch_7a
    .catch Ljava/io/IOException; {:try_start_4c .. :try_end_51} :catch_76

    .line 520
    :goto_51
    :try_start_51
    iget-object v5, p0, Lorg/tukaani/xz/LZMAInputStream;->lz:Lorg/tukaani/xz/lz/LZDecoder;

    invoke-virtual {v5, p1, p2}, Lorg/tukaani/xz/lz/LZDecoder;->flush([BI)I

    move-result v0

    .line 521
    .local v0, "copiedSize":I
    add-int/2addr p2, v0

    .line 522
    sub-int/2addr p3, v0

    .line 523
    add-int/2addr v3, v0

    .line 525
    iget-wide v5, p0, Lorg/tukaani/xz/LZMAInputStream;->remainingSize:J

    cmp-long v5, v5, v9

    if-ltz v5, :cond_9e

    .line 527
    iget-wide v5, p0, Lorg/tukaani/xz/LZMAInputStream;->remainingSize:J

    int-to-long v7, v0

    sub-long/2addr v5, v7

    iput-wide v5, p0, Lorg/tukaani/xz/LZMAInputStream;->remainingSize:J

    .line 528
    sget-boolean v5, Lorg/tukaani/xz/LZMAInputStream;->$assertionsDisabled:Z

    if-nez v5, :cond_95

    iget-wide v5, p0, Lorg/tukaani/xz/LZMAInputStream;->remainingSize:J

    cmp-long v5, v5, v9

    if-gez v5, :cond_95

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4
    :try_end_76
    .catch Ljava/io/IOException; {:try_start_51 .. :try_end_76} :catch_76

    .line 548
    .end local v0    # "copiedSize":I
    :catch_76
    move-exception v2

    .line 549
    .local v2, "e":Ljava/io/IOException;
    iput-object v2, p0, Lorg/tukaani/xz/LZMAInputStream;->exception:Ljava/io/IOException;

    .line 550
    throw v2

    .line 502
    .end local v2    # "e":Ljava/io/IOException;
    :catch_7a
    move-exception v2

    .line 507
    .local v2, "e":Lorg/tukaani/xz/CorruptedInputException;
    :try_start_7b
    iget-wide v5, p0, Lorg/tukaani/xz/LZMAInputStream;->remainingSize:J

    const-wide/16 v7, -0x1

    cmp-long v5, v5, v7

    if-nez v5, :cond_8b

    iget-object v5, p0, Lorg/tukaani/xz/LZMAInputStream;->lzma:Lorg/tukaani/xz/lzma/LZMADecoder;

    invoke-virtual {v5}, Lorg/tukaani/xz/lzma/LZMADecoder;->endMarkerDetected()Z

    move-result v5

    if-nez v5, :cond_8c

    .line 508
    :cond_8b
    throw v2

    .line 510
    :cond_8c
    const/4 v5, 0x1

    iput-boolean v5, p0, Lorg/tukaani/xz/LZMAInputStream;->endReached:Z

    .line 516
    iget-object v5, p0, Lorg/tukaani/xz/LZMAInputStream;->rc:Lorg/tukaani/xz/rangecoder/RangeDecoderFromStream;

    invoke-virtual {v5}, Lorg/tukaani/xz/rangecoder/RangeDecoderFromStream;->normalize()V

    goto :goto_51

    .line 530
    .end local v2    # "e":Lorg/tukaani/xz/CorruptedInputException;
    .restart local v0    # "copiedSize":I
    :cond_95
    iget-wide v5, p0, Lorg/tukaani/xz/LZMAInputStream;->remainingSize:J

    cmp-long v5, v5, v9

    if-nez v5, :cond_9e

    .line 531
    const/4 v5, 0x1

    iput-boolean v5, p0, Lorg/tukaani/xz/LZMAInputStream;->endReached:Z

    .line 534
    :cond_9e
    iget-boolean v5, p0, Lorg/tukaani/xz/LZMAInputStream;->endReached:Z

    if-eqz v5, :cond_34

    .line 539
    iget-object v5, p0, Lorg/tukaani/xz/LZMAInputStream;->rc:Lorg/tukaani/xz/rangecoder/RangeDecoderFromStream;

    invoke-virtual {v5}, Lorg/tukaani/xz/rangecoder/RangeDecoderFromStream;->isFinished()Z

    move-result v5

    if-eqz v5, :cond_b2

    iget-object v5, p0, Lorg/tukaani/xz/LZMAInputStream;->lz:Lorg/tukaani/xz/lz/LZDecoder;

    invoke-virtual {v5}, Lorg/tukaani/xz/lz/LZDecoder;->hasPending()Z

    move-result v5

    if-eqz v5, :cond_b8

    .line 540
    :cond_b2
    new-instance v4, Lorg/tukaani/xz/CorruptedInputException;

    invoke-direct {v4}, Lorg/tukaani/xz/CorruptedInputException;-><init>()V

    throw v4
    :try_end_b8
    .catch Ljava/io/IOException; {:try_start_7b .. :try_end_b8} :catch_76

    .line 542
    :cond_b8
    if-nez v3, :cond_19

    move v3, v4

    goto/16 :goto_19
.end method
