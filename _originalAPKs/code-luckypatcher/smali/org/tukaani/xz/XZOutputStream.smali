.class public Lorg/tukaani/xz/XZOutputStream;
.super Lorg/tukaani/xz/FinishableOutputStream;
.source "XZOutputStream.java"


# instance fields
.field private blockEncoder:Lorg/tukaani/xz/BlockOutputStream;

.field private final check:Lorg/tukaani/xz/check/Check;

.field private exception:Ljava/io/IOException;

.field private filters:[Lorg/tukaani/xz/FilterEncoder;

.field private filtersSupportFlushing:Z

.field private finished:Z

.field private final index:Lorg/tukaani/xz/index/IndexEncoder;

.field private out:Ljava/io/OutputStream;

.field private final streamFlags:Lorg/tukaani/xz/common/StreamFlags;

.field private final tempBuf:[B


# direct methods
.method public constructor <init>(Ljava/io/OutputStream;Lorg/tukaani/xz/FilterOptions;)V
    .registers 4
    .param p1, "out"    # Ljava/io/OutputStream;
    .param p2, "filterOptions"    # Lorg/tukaani/xz/FilterOptions;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 94
    const/4 v0, 0x4

    invoke-direct {p0, p1, p2, v0}, Lorg/tukaani/xz/XZOutputStream;-><init>(Ljava/io/OutputStream;Lorg/tukaani/xz/FilterOptions;I)V

    .line 95
    return-void
.end method

.method public constructor <init>(Ljava/io/OutputStream;Lorg/tukaani/xz/FilterOptions;I)V
    .registers 6
    .param p1, "out"    # Ljava/io/OutputStream;
    .param p2, "filterOptions"    # Lorg/tukaani/xz/FilterOptions;
    .param p3, "checkType"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 119
    const/4 v0, 0x1

    new-array v0, v0, [Lorg/tukaani/xz/FilterOptions;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    invoke-direct {p0, p1, v0, p3}, Lorg/tukaani/xz/XZOutputStream;-><init>(Ljava/io/OutputStream;[Lorg/tukaani/xz/FilterOptions;I)V

    .line 120
    return-void
.end method

.method public constructor <init>(Ljava/io/OutputStream;[Lorg/tukaani/xz/FilterOptions;)V
    .registers 4
    .param p1, "out"    # Ljava/io/OutputStream;
    .param p2, "filterOptions"    # [Lorg/tukaani/xz/FilterOptions;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 140
    const/4 v0, 0x4

    invoke-direct {p0, p1, p2, v0}, Lorg/tukaani/xz/XZOutputStream;-><init>(Ljava/io/OutputStream;[Lorg/tukaani/xz/FilterOptions;I)V

    .line 141
    return-void
.end method

.method public constructor <init>(Ljava/io/OutputStream;[Lorg/tukaani/xz/FilterOptions;I)V
    .registers 6
    .param p1, "out"    # Ljava/io/OutputStream;
    .param p2, "filterOptions"    # [Lorg/tukaani/xz/FilterOptions;
    .param p3, "checkType"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 162
    invoke-direct {p0}, Lorg/tukaani/xz/FinishableOutputStream;-><init>()V

    .line 56
    new-instance v0, Lorg/tukaani/xz/common/StreamFlags;

    invoke-direct {v0}, Lorg/tukaani/xz/common/StreamFlags;-><init>()V

    iput-object v0, p0, Lorg/tukaani/xz/XZOutputStream;->streamFlags:Lorg/tukaani/xz/common/StreamFlags;

    .line 58
    new-instance v0, Lorg/tukaani/xz/index/IndexEncoder;

    invoke-direct {v0}, Lorg/tukaani/xz/index/IndexEncoder;-><init>()V

    iput-object v0, p0, Lorg/tukaani/xz/XZOutputStream;->index:Lorg/tukaani/xz/index/IndexEncoder;

    .line 60
    iput-object v1, p0, Lorg/tukaani/xz/XZOutputStream;->blockEncoder:Lorg/tukaani/xz/BlockOutputStream;

    .line 70
    iput-object v1, p0, Lorg/tukaani/xz/XZOutputStream;->exception:Ljava/io/IOException;

    .line 71
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/tukaani/xz/XZOutputStream;->finished:Z

    .line 73
    const/4 v0, 0x1

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/tukaani/xz/XZOutputStream;->tempBuf:[B

    .line 163
    iput-object p1, p0, Lorg/tukaani/xz/XZOutputStream;->out:Ljava/io/OutputStream;

    .line 164
    invoke-virtual {p0, p2}, Lorg/tukaani/xz/XZOutputStream;->updateFilters([Lorg/tukaani/xz/FilterOptions;)V

    .line 166
    iget-object v0, p0, Lorg/tukaani/xz/XZOutputStream;->streamFlags:Lorg/tukaani/xz/common/StreamFlags;

    iput p3, v0, Lorg/tukaani/xz/common/StreamFlags;->checkType:I

    .line 167
    invoke-static {p3}, Lorg/tukaani/xz/check/Check;->getInstance(I)Lorg/tukaani/xz/check/Check;

    move-result-object v0

    iput-object v0, p0, Lorg/tukaani/xz/XZOutputStream;->check:Lorg/tukaani/xz/check/Check;

    .line 169
    invoke-direct {p0}, Lorg/tukaani/xz/XZOutputStream;->encodeStreamHeader()V

    .line 170
    return-void
.end method

.method private encodeStreamFlags([BI)V
    .registers 5
    .param p1, "buf"    # [B
    .param p2, "off"    # I

    .prologue
    .line 462
    const/4 v0, 0x0

    aput-byte v0, p1, p2

    .line 463
    add-int/lit8 v0, p2, 0x1

    iget-object v1, p0, Lorg/tukaani/xz/XZOutputStream;->streamFlags:Lorg/tukaani/xz/common/StreamFlags;

    iget v1, v1, Lorg/tukaani/xz/common/StreamFlags;->checkType:I

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    .line 464
    return-void
.end method

.method private encodeStreamFooter()V
    .registers 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x4

    .line 477
    const/4 v4, 0x6

    new-array v2, v4, [B

    .line 478
    .local v2, "buf":[B
    iget-object v4, p0, Lorg/tukaani/xz/XZOutputStream;->index:Lorg/tukaani/xz/index/IndexEncoder;

    invoke-virtual {v4}, Lorg/tukaani/xz/index/IndexEncoder;->getIndexSize()J

    move-result-wide v4

    const-wide/16 v6, 0x4

    div-long/2addr v4, v6

    const-wide/16 v6, 0x1

    sub-long v0, v4, v6

    .line 479
    .local v0, "backwardSize":J
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_12
    if-ge v3, v8, :cond_1f

    .line 480
    mul-int/lit8 v4, v3, 0x8

    ushr-long v4, v0, v4

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    .line 479
    add-int/lit8 v3, v3, 0x1

    goto :goto_12

    .line 482
    :cond_1f
    invoke-direct {p0, v2, v8}, Lorg/tukaani/xz/XZOutputStream;->encodeStreamFlags([BI)V

    .line 484
    iget-object v4, p0, Lorg/tukaani/xz/XZOutputStream;->out:Ljava/io/OutputStream;

    invoke-static {v4, v2}, Lorg/tukaani/xz/common/EncoderUtil;->writeCRC32(Ljava/io/OutputStream;[B)V

    .line 485
    iget-object v4, p0, Lorg/tukaani/xz/XZOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v4, v2}, Ljava/io/OutputStream;->write([B)V

    .line 486
    iget-object v4, p0, Lorg/tukaani/xz/XZOutputStream;->out:Ljava/io/OutputStream;

    sget-object v5, Lorg/tukaani/xz/XZ;->FOOTER_MAGIC:[B

    invoke-virtual {v4, v5}, Ljava/io/OutputStream;->write([B)V

    .line 487
    return-void
.end method

.method private encodeStreamHeader()V
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 467
    iget-object v1, p0, Lorg/tukaani/xz/XZOutputStream;->out:Ljava/io/OutputStream;

    sget-object v2, Lorg/tukaani/xz/XZ;->HEADER_MAGIC:[B

    invoke-virtual {v1, v2}, Ljava/io/OutputStream;->write([B)V

    .line 469
    const/4 v1, 0x2

    new-array v0, v1, [B

    .line 470
    .local v0, "buf":[B
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lorg/tukaani/xz/XZOutputStream;->encodeStreamFlags([BI)V

    .line 471
    iget-object v1, p0, Lorg/tukaani/xz/XZOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 473
    iget-object v1, p0, Lorg/tukaani/xz/XZOutputStream;->out:Ljava/io/OutputStream;

    invoke-static {v1, v0}, Lorg/tukaani/xz/common/EncoderUtil;->writeCRC32(Ljava/io/OutputStream;[B)V

    .line 474
    return-void
.end method


# virtual methods
.method public close()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 437
    iget-object v1, p0, Lorg/tukaani/xz/XZOutputStream;->out:Ljava/io/OutputStream;

    if-eqz v1, :cond_f

    .line 442
    :try_start_4
    invoke-virtual {p0}, Lorg/tukaani/xz/XZOutputStream;->finish()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_7} :catch_1e

    .line 446
    :goto_7
    :try_start_7
    iget-object v1, p0, Lorg/tukaani/xz/XZOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_c} :catch_16

    .line 454
    :cond_c
    :goto_c
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/tukaani/xz/XZOutputStream;->out:Ljava/io/OutputStream;

    .line 457
    :cond_f
    iget-object v1, p0, Lorg/tukaani/xz/XZOutputStream;->exception:Ljava/io/IOException;

    if-eqz v1, :cond_20

    .line 458
    iget-object v1, p0, Lorg/tukaani/xz/XZOutputStream;->exception:Ljava/io/IOException;

    throw v1

    .line 447
    :catch_16
    move-exception v0

    .line 450
    .local v0, "e":Ljava/io/IOException;
    iget-object v1, p0, Lorg/tukaani/xz/XZOutputStream;->exception:Ljava/io/IOException;

    if-nez v1, :cond_c

    .line 451
    iput-object v0, p0, Lorg/tukaani/xz/XZOutputStream;->exception:Ljava/io/IOException;

    goto :goto_c

    .line 443
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1e
    move-exception v1

    goto :goto_7

    .line 459
    :cond_20
    return-void
.end method

.method public endBlock()V
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 315
    iget-object v1, p0, Lorg/tukaani/xz/XZOutputStream;->exception:Ljava/io/IOException;

    if-eqz v1, :cond_7

    .line 316
    iget-object v1, p0, Lorg/tukaani/xz/XZOutputStream;->exception:Ljava/io/IOException;

    throw v1

    .line 318
    :cond_7
    iget-boolean v1, p0, Lorg/tukaani/xz/XZOutputStream;->finished:Z

    if-eqz v1, :cond_13

    .line 319
    new-instance v1, Lorg/tukaani/xz/XZIOException;

    const-string v2, "Stream finished or closed"

    invoke-direct {v1, v2}, Lorg/tukaani/xz/XZIOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 324
    :cond_13
    iget-object v1, p0, Lorg/tukaani/xz/XZOutputStream;->blockEncoder:Lorg/tukaani/xz/BlockOutputStream;

    if-eqz v1, :cond_30

    .line 326
    :try_start_17
    iget-object v1, p0, Lorg/tukaani/xz/XZOutputStream;->blockEncoder:Lorg/tukaani/xz/BlockOutputStream;

    invoke-virtual {v1}, Lorg/tukaani/xz/BlockOutputStream;->finish()V

    .line 327
    iget-object v1, p0, Lorg/tukaani/xz/XZOutputStream;->index:Lorg/tukaani/xz/index/IndexEncoder;

    iget-object v2, p0, Lorg/tukaani/xz/XZOutputStream;->blockEncoder:Lorg/tukaani/xz/BlockOutputStream;

    invoke-virtual {v2}, Lorg/tukaani/xz/BlockOutputStream;->getUnpaddedSize()J

    move-result-wide v2

    iget-object v4, p0, Lorg/tukaani/xz/XZOutputStream;->blockEncoder:Lorg/tukaani/xz/BlockOutputStream;

    .line 328
    invoke-virtual {v4}, Lorg/tukaani/xz/BlockOutputStream;->getUncompressedSize()J

    move-result-wide v4

    .line 327
    invoke-virtual {v1, v2, v3, v4, v5}, Lorg/tukaani/xz/index/IndexEncoder;->add(JJ)V

    .line 329
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/tukaani/xz/XZOutputStream;->blockEncoder:Lorg/tukaani/xz/BlockOutputStream;
    :try_end_30
    .catch Ljava/io/IOException; {:try_start_17 .. :try_end_30} :catch_31

    .line 335
    :cond_30
    return-void

    .line 330
    :catch_31
    move-exception v0

    .line 331
    .local v0, "e":Ljava/io/IOException;
    iput-object v0, p0, Lorg/tukaani/xz/XZOutputStream;->exception:Ljava/io/IOException;

    .line 332
    throw v0
.end method

.method public finish()V
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 404
    iget-boolean v1, p0, Lorg/tukaani/xz/XZOutputStream;->finished:Z

    if-nez v1, :cond_14

    .line 407
    invoke-virtual {p0}, Lorg/tukaani/xz/XZOutputStream;->endBlock()V

    .line 410
    :try_start_7
    iget-object v1, p0, Lorg/tukaani/xz/XZOutputStream;->index:Lorg/tukaani/xz/index/IndexEncoder;

    iget-object v2, p0, Lorg/tukaani/xz/XZOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v1, v2}, Lorg/tukaani/xz/index/IndexEncoder;->encode(Ljava/io/OutputStream;)V

    .line 411
    invoke-direct {p0}, Lorg/tukaani/xz/XZOutputStream;->encodeStreamFooter()V
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_11} :catch_15

    .line 420
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/tukaani/xz/XZOutputStream;->finished:Z

    .line 422
    :cond_14
    return-void

    .line 412
    :catch_15
    move-exception v0

    .line 413
    .local v0, "e":Ljava/io/IOException;
    iput-object v0, p0, Lorg/tukaani/xz/XZOutputStream;->exception:Ljava/io/IOException;

    .line 414
    throw v0
.end method

.method public flush()V
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 360
    iget-object v1, p0, Lorg/tukaani/xz/XZOutputStream;->exception:Ljava/io/IOException;

    if-eqz v1, :cond_7

    .line 361
    iget-object v1, p0, Lorg/tukaani/xz/XZOutputStream;->exception:Ljava/io/IOException;

    throw v1

    .line 363
    :cond_7
    iget-boolean v1, p0, Lorg/tukaani/xz/XZOutputStream;->finished:Z

    if-eqz v1, :cond_13

    .line 364
    new-instance v1, Lorg/tukaani/xz/XZIOException;

    const-string v2, "Stream finished or closed"

    invoke-direct {v1, v2}, Lorg/tukaani/xz/XZIOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 367
    :cond_13
    :try_start_13
    iget-object v1, p0, Lorg/tukaani/xz/XZOutputStream;->blockEncoder:Lorg/tukaani/xz/BlockOutputStream;

    if-eqz v1, :cond_2e

    .line 368
    iget-boolean v1, p0, Lorg/tukaani/xz/XZOutputStream;->filtersSupportFlushing:Z

    if-eqz v1, :cond_21

    .line 371
    iget-object v1, p0, Lorg/tukaani/xz/XZOutputStream;->blockEncoder:Lorg/tukaani/xz/BlockOutputStream;

    invoke-virtual {v1}, Lorg/tukaani/xz/BlockOutputStream;->flush()V

    .line 383
    :goto_20
    return-void

    .line 373
    :cond_21
    invoke-virtual {p0}, Lorg/tukaani/xz/XZOutputStream;->endBlock()V

    .line 374
    iget-object v1, p0, Lorg/tukaani/xz/XZOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v1}, Ljava/io/OutputStream;->flush()V
    :try_end_29
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_29} :catch_2a

    goto :goto_20

    .line 379
    :catch_2a
    move-exception v0

    .line 380
    .local v0, "e":Ljava/io/IOException;
    iput-object v0, p0, Lorg/tukaani/xz/XZOutputStream;->exception:Ljava/io/IOException;

    .line 381
    throw v0

    .line 377
    .end local v0    # "e":Ljava/io/IOException;
    :cond_2e
    :try_start_2e
    iget-object v1, p0, Lorg/tukaani/xz/XZOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v1}, Ljava/io/OutputStream;->flush()V
    :try_end_33
    .catch Ljava/io/IOException; {:try_start_2e .. :try_end_33} :catch_2a

    goto :goto_20
.end method

.method public updateFilters(Lorg/tukaani/xz/FilterOptions;)V
    .registers 4
    .param p1, "filterOptions"    # Lorg/tukaani/xz/FilterOptions;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/tukaani/xz/XZIOException;
        }
    .end annotation

    .prologue
    .line 186
    const/4 v1, 0x1

    new-array v0, v1, [Lorg/tukaani/xz/FilterOptions;

    .line 187
    .local v0, "opts":[Lorg/tukaani/xz/FilterOptions;
    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 188
    invoke-virtual {p0, v0}, Lorg/tukaani/xz/XZOutputStream;->updateFilters([Lorg/tukaani/xz/FilterOptions;)V

    .line 189
    return-void
.end method

.method public updateFilters([Lorg/tukaani/xz/FilterOptions;)V
    .registers 7
    .param p1, "filterOptions"    # [Lorg/tukaani/xz/FilterOptions;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/tukaani/xz/XZIOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 208
    iget-object v2, p0, Lorg/tukaani/xz/XZOutputStream;->blockEncoder:Lorg/tukaani/xz/BlockOutputStream;

    if-eqz v2, :cond_d

    .line 209
    new-instance v2, Lorg/tukaani/xz/UnsupportedOptionsException;

    const-string v3, "Changing filter options in the middle of a XZ Block not implemented"

    invoke-direct {v2, v3}, Lorg/tukaani/xz/UnsupportedOptionsException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 212
    :cond_d
    array-length v2, p1

    if-lt v2, v4, :cond_14

    array-length v2, p1

    const/4 v3, 0x4

    if-le v2, v3, :cond_1c

    .line 213
    :cond_14
    new-instance v2, Lorg/tukaani/xz/UnsupportedOptionsException;

    const-string v3, "XZ filter chain must be 1-4 filters"

    invoke-direct {v2, v3}, Lorg/tukaani/xz/UnsupportedOptionsException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 216
    :cond_1c
    iput-boolean v4, p0, Lorg/tukaani/xz/XZOutputStream;->filtersSupportFlushing:Z

    .line 217
    array-length v2, p1

    new-array v1, v2, [Lorg/tukaani/xz/FilterEncoder;

    .line 218
    .local v1, "newFilters":[Lorg/tukaani/xz/FilterEncoder;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_22
    array-length v2, p1

    if-ge v0, v2, :cond_3b

    .line 219
    aget-object v2, p1, v0

    invoke-virtual {v2}, Lorg/tukaani/xz/FilterOptions;->getFilterEncoder()Lorg/tukaani/xz/FilterEncoder;

    move-result-object v2

    aput-object v2, v1, v0

    .line 220
    iget-boolean v2, p0, Lorg/tukaani/xz/XZOutputStream;->filtersSupportFlushing:Z

    aget-object v3, v1, v0

    invoke-interface {v3}, Lorg/tukaani/xz/FilterEncoder;->supportsFlushing()Z

    move-result v3

    and-int/2addr v2, v3

    iput-boolean v2, p0, Lorg/tukaani/xz/XZOutputStream;->filtersSupportFlushing:Z

    .line 218
    add-int/lit8 v0, v0, 0x1

    goto :goto_22

    .line 223
    :cond_3b
    invoke-static {v1}, Lorg/tukaani/xz/RawCoder;->validate([Lorg/tukaani/xz/FilterCoder;)V

    .line 224
    iput-object v1, p0, Lorg/tukaani/xz/XZOutputStream;->filters:[Lorg/tukaani/xz/FilterEncoder;

    .line 225
    return-void
.end method

.method public write(I)V
    .registers 5
    .param p1, "b"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 240
    iget-object v0, p0, Lorg/tukaani/xz/XZOutputStream;->tempBuf:[B

    int-to-byte v1, p1

    aput-byte v1, v0, v2

    .line 241
    iget-object v0, p0, Lorg/tukaani/xz/XZOutputStream;->tempBuf:[B

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v2, v1}, Lorg/tukaani/xz/XZOutputStream;->write([BII)V

    .line 242
    return-void
.end method

.method public write([BII)V
    .registers 9
    .param p1, "buf"    # [B
    .param p2, "off"    # I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 269
    if-ltz p2, :cond_d

    if-ltz p3, :cond_d

    add-int v1, p2, p3

    if-ltz v1, :cond_d

    add-int v1, p2, p3

    array-length v2, p1

    if-le v1, v2, :cond_13

    .line 270
    :cond_d
    new-instance v1, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v1}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v1

    .line 272
    :cond_13
    iget-object v1, p0, Lorg/tukaani/xz/XZOutputStream;->exception:Ljava/io/IOException;

    if-eqz v1, :cond_1a

    .line 273
    iget-object v1, p0, Lorg/tukaani/xz/XZOutputStream;->exception:Ljava/io/IOException;

    throw v1

    .line 275
    :cond_1a
    iget-boolean v1, p0, Lorg/tukaani/xz/XZOutputStream;->finished:Z

    if-eqz v1, :cond_26

    .line 276
    new-instance v1, Lorg/tukaani/xz/XZIOException;

    const-string v2, "Stream finished or closed"

    invoke-direct {v1, v2}, Lorg/tukaani/xz/XZIOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 279
    :cond_26
    :try_start_26
    iget-object v1, p0, Lorg/tukaani/xz/XZOutputStream;->blockEncoder:Lorg/tukaani/xz/BlockOutputStream;

    if-nez v1, :cond_37

    .line 280
    new-instance v1, Lorg/tukaani/xz/BlockOutputStream;

    iget-object v2, p0, Lorg/tukaani/xz/XZOutputStream;->out:Ljava/io/OutputStream;

    iget-object v3, p0, Lorg/tukaani/xz/XZOutputStream;->filters:[Lorg/tukaani/xz/FilterEncoder;

    iget-object v4, p0, Lorg/tukaani/xz/XZOutputStream;->check:Lorg/tukaani/xz/check/Check;

    invoke-direct {v1, v2, v3, v4}, Lorg/tukaani/xz/BlockOutputStream;-><init>(Ljava/io/OutputStream;[Lorg/tukaani/xz/FilterEncoder;Lorg/tukaani/xz/check/Check;)V

    iput-object v1, p0, Lorg/tukaani/xz/XZOutputStream;->blockEncoder:Lorg/tukaani/xz/BlockOutputStream;

    .line 282
    :cond_37
    iget-object v1, p0, Lorg/tukaani/xz/XZOutputStream;->blockEncoder:Lorg/tukaani/xz/BlockOutputStream;

    invoke-virtual {v1, p1, p2, p3}, Lorg/tukaani/xz/BlockOutputStream;->write([BII)V
    :try_end_3c
    .catch Ljava/io/IOException; {:try_start_26 .. :try_end_3c} :catch_3d

    .line 287
    return-void

    .line 283
    :catch_3d
    move-exception v0

    .line 284
    .local v0, "e":Ljava/io/IOException;
    iput-object v0, p0, Lorg/tukaani/xz/XZOutputStream;->exception:Ljava/io/IOException;

    .line 285
    throw v0
.end method
