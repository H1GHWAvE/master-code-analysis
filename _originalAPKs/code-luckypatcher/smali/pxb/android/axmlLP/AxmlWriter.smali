.class public Lpxb/android/axmlLP/AxmlWriter;
.super Lpxb/android/axmlLP/AxmlVisitor;
.source "AxmlWriter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lpxb/android/axmlLP/AxmlWriter$Ns;,
        Lpxb/android/axmlLP/AxmlWriter$NodeImpl;,
        Lpxb/android/axmlLP/AxmlWriter$Attr;
    }
.end annotation


# instance fields
.field private firsts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lpxb/android/axmlLP/AxmlWriter$NodeImpl;",
            ">;"
        }
    .end annotation
.end field

.field private nses:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lpxb/android/axmlLP/AxmlWriter$Ns;",
            ">;"
        }
    .end annotation
.end field

.field private otherString:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lpxb/android/axmlLP/StringItem;",
            ">;"
        }
    .end annotation
.end field

.field private resourceId2Str:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lpxb/android/axmlLP/StringItem;",
            ">;"
        }
    .end annotation
.end field

.field private resourceIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private resourceString:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lpxb/android/axmlLP/StringItem;",
            ">;"
        }
    .end annotation
.end field

.field private stringItems:Lpxb/android/axmlLP/StringItems;


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    .line 37
    invoke-direct {p0}, Lpxb/android/axmlLP/AxmlVisitor;-><init>()V

    .line 229
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lpxb/android/axmlLP/AxmlWriter;->firsts:Ljava/util/List;

    .line 231
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lpxb/android/axmlLP/AxmlWriter;->nses:Ljava/util/Map;

    .line 233
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lpxb/android/axmlLP/AxmlWriter;->otherString:Ljava/util/List;

    .line 235
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lpxb/android/axmlLP/AxmlWriter;->resourceId2Str:Ljava/util/Map;

    .line 237
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lpxb/android/axmlLP/AxmlWriter;->resourceIds:Ljava/util/List;

    .line 239
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lpxb/android/axmlLP/AxmlWriter;->resourceString:Ljava/util/List;

    .line 241
    new-instance v0, Lpxb/android/axmlLP/StringItems;

    invoke-direct {v0}, Lpxb/android/axmlLP/StringItems;-><init>()V

    iput-object v0, p0, Lpxb/android/axmlLP/AxmlWriter;->stringItems:Lpxb/android/axmlLP/StringItems;

    return-void
.end method

.method private prepare()I
    .registers 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v13, 0x0

    const/4 v12, 0x0

    .line 264
    iget-object v7, p0, Lpxb/android/axmlLP/AxmlWriter;->nses:Ljava/util/Map;

    invoke-interface {v7}, Ljava/util/Map;->size()I

    move-result v7

    mul-int/lit8 v7, v7, 0x18

    mul-int/lit8 v5, v7, 0x2

    .line 265
    .local v5, "size":I
    iget-object v7, p0, Lpxb/android/axmlLP/AxmlWriter;->firsts:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_12
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_24

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lpxb/android/axmlLP/AxmlWriter$NodeImpl;

    .line 266
    .local v3, "first":Lpxb/android/axmlLP/AxmlWriter$NodeImpl;
    invoke-virtual {v3, p0}, Lpxb/android/axmlLP/AxmlWriter$NodeImpl;->prepare(Lpxb/android/axmlLP/AxmlWriter;)I

    move-result v8

    add-int/2addr v5, v8

    .line 267
    goto :goto_12

    .line 269
    .end local v3    # "first":Lpxb/android/axmlLP/AxmlWriter$NodeImpl;
    :cond_24
    const/4 v0, 0x0

    .line 270
    .local v0, "a":I
    iget-object v7, p0, Lpxb/android/axmlLP/AxmlWriter;->nses:Ljava/util/Map;

    invoke-interface {v7}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_2f
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_7e

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 271
    .local v2, "e":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lpxb/android/axmlLP/AxmlWriter$Ns;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lpxb/android/axmlLP/AxmlWriter$Ns;

    .line 272
    .local v4, "ns":Lpxb/android/axmlLP/AxmlWriter$Ns;
    if-nez v4, :cond_6d

    .line 273
    new-instance v4, Lpxb/android/axmlLP/AxmlWriter$Ns;

    .end local v4    # "ns":Lpxb/android/axmlLP/AxmlWriter$Ns;
    new-instance v9, Lpxb/android/axmlLP/StringItem;

    const-string v7, "axml_auto_%02d"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    add-int/lit8 v1, v0, 0x1

    .end local v0    # "a":I
    .local v1, "a":I
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v10, v12

    invoke-static {v7, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v9, v7}, Lpxb/android/axmlLP/StringItem;-><init>(Ljava/lang/String;)V

    new-instance v10, Lpxb/android/axmlLP/StringItem;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-direct {v10, v7}, Lpxb/android/axmlLP/StringItem;-><init>(Ljava/lang/String;)V

    invoke-direct {v4, v9, v10, v12}, Lpxb/android/axmlLP/AxmlWriter$Ns;-><init>(Lpxb/android/axmlLP/StringItem;Lpxb/android/axmlLP/StringItem;I)V

    .line 274
    .restart local v4    # "ns":Lpxb/android/axmlLP/AxmlWriter$Ns;
    invoke-interface {v2, v4}, Ljava/util/Map$Entry;->setValue(Ljava/lang/Object;)Ljava/lang/Object;

    move v0, v1

    .line 276
    .end local v1    # "a":I
    .restart local v0    # "a":I
    :cond_6d
    iget-object v7, v4, Lpxb/android/axmlLP/AxmlWriter$Ns;->prefix:Lpxb/android/axmlLP/StringItem;

    invoke-virtual {p0, v7}, Lpxb/android/axmlLP/AxmlWriter;->update(Lpxb/android/axmlLP/StringItem;)Lpxb/android/axmlLP/StringItem;

    move-result-object v7

    iput-object v7, v4, Lpxb/android/axmlLP/AxmlWriter$Ns;->prefix:Lpxb/android/axmlLP/StringItem;

    .line 277
    iget-object v7, v4, Lpxb/android/axmlLP/AxmlWriter$Ns;->uri:Lpxb/android/axmlLP/StringItem;

    invoke-virtual {p0, v7}, Lpxb/android/axmlLP/AxmlWriter;->update(Lpxb/android/axmlLP/StringItem;)Lpxb/android/axmlLP/StringItem;

    move-result-object v7

    iput-object v7, v4, Lpxb/android/axmlLP/AxmlWriter$Ns;->uri:Lpxb/android/axmlLP/StringItem;

    goto :goto_2f

    .line 280
    .end local v2    # "e":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lpxb/android/axmlLP/AxmlWriter$Ns;>;"
    .end local v4    # "ns":Lpxb/android/axmlLP/AxmlWriter$Ns;
    :cond_7e
    iget-object v7, p0, Lpxb/android/axmlLP/AxmlWriter;->stringItems:Lpxb/android/axmlLP/StringItems;

    iget-object v8, p0, Lpxb/android/axmlLP/AxmlWriter;->resourceString:Ljava/util/List;

    invoke-virtual {v7, v8}, Lpxb/android/axmlLP/StringItems;->addAll(Ljava/util/Collection;)Z

    .line 281
    iput-object v13, p0, Lpxb/android/axmlLP/AxmlWriter;->resourceString:Ljava/util/List;

    .line 282
    iget-object v7, p0, Lpxb/android/axmlLP/AxmlWriter;->stringItems:Lpxb/android/axmlLP/StringItems;

    iget-object v8, p0, Lpxb/android/axmlLP/AxmlWriter;->otherString:Ljava/util/List;

    invoke-virtual {v7, v8}, Lpxb/android/axmlLP/StringItems;->addAll(Ljava/util/Collection;)Z

    .line 283
    iput-object v13, p0, Lpxb/android/axmlLP/AxmlWriter;->otherString:Ljava/util/List;

    .line 284
    iget-object v7, p0, Lpxb/android/axmlLP/AxmlWriter;->stringItems:Lpxb/android/axmlLP/StringItems;

    invoke-virtual {v7}, Lpxb/android/axmlLP/StringItems;->prepare()V

    .line 285
    iget-object v7, p0, Lpxb/android/axmlLP/AxmlWriter;->stringItems:Lpxb/android/axmlLP/StringItems;

    invoke-virtual {v7}, Lpxb/android/axmlLP/StringItems;->getSize()I

    move-result v6

    .line 286
    .local v6, "stringSize":I
    rem-int/lit8 v7, v6, 0x4

    if-eqz v7, :cond_a4

    .line 287
    rem-int/lit8 v7, v6, 0x4

    rsub-int/lit8 v7, v7, 0x4

    add-int/2addr v6, v7

    .line 289
    :cond_a4
    add-int/lit8 v7, v6, 0x8

    add-int/2addr v5, v7

    .line 290
    iget-object v7, p0, Lpxb/android/axmlLP/AxmlWriter;->resourceIds:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    mul-int/lit8 v7, v7, 0x4

    add-int/lit8 v7, v7, 0x8

    add-int/2addr v5, v7

    .line 291
    return v5
.end method


# virtual methods
.method public end()V
    .registers 1

    .prologue
    .line 248
    return-void
.end method

.method public first(Ljava/lang/String;Ljava/lang/String;)Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;
    .registers 5
    .param p1, "ns"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 252
    new-instance v0, Lpxb/android/axmlLP/AxmlWriter$NodeImpl;

    invoke-direct {v0, p1, p2}, Lpxb/android/axmlLP/AxmlWriter$NodeImpl;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    .local v0, "first":Lpxb/android/axmlLP/AxmlWriter$NodeImpl;
    iget-object v1, p0, Lpxb/android/axmlLP/AxmlWriter;->firsts:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 254
    return-object v0
.end method

.method public ns(Ljava/lang/String;Ljava/lang/String;I)V
    .registers 8
    .param p1, "prefix"    # Ljava/lang/String;
    .param p2, "uri"    # Ljava/lang/String;
    .param p3, "ln"    # I

    .prologue
    .line 259
    iget-object v0, p0, Lpxb/android/axmlLP/AxmlWriter;->nses:Ljava/util/Map;

    new-instance v1, Lpxb/android/axmlLP/AxmlWriter$Ns;

    new-instance v2, Lpxb/android/axmlLP/StringItem;

    invoke-direct {v2, p1}, Lpxb/android/axmlLP/StringItem;-><init>(Ljava/lang/String;)V

    new-instance v3, Lpxb/android/axmlLP/StringItem;

    invoke-direct {v3, p2}, Lpxb/android/axmlLP/StringItem;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2, v3, p3}, Lpxb/android/axmlLP/AxmlWriter$Ns;-><init>(Lpxb/android/axmlLP/StringItem;Lpxb/android/axmlLP/StringItem;I)V

    invoke-interface {v0, p2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 260
    return-void
.end method

.method public toByteArray()[B
    .registers 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v13, 0x18

    const/4 v12, -0x1

    .line 295
    new-instance v4, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v4}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 297
    .local v4, "os":Ljava/io/ByteArrayOutputStream;
    new-instance v5, Lcom/googlecode/dex2jar/reader/io/LeDataOut;

    invoke-direct {v5, v4}, Lcom/googlecode/dex2jar/reader/io/LeDataOut;-><init>(Ljava/io/OutputStream;)V

    .line 298
    .local v5, "out":Lcom/googlecode/dex2jar/reader/io/DataOut;
    invoke-direct {p0}, Lpxb/android/axmlLP/AxmlWriter;->prepare()I

    move-result v7

    .line 299
    .local v7, "size":I
    const v10, 0x80003

    invoke-interface {v5, v10}, Lcom/googlecode/dex2jar/reader/io/DataOut;->writeInt(I)V

    .line 300
    add-int/lit8 v10, v7, 0x8

    invoke-interface {v5, v10}, Lcom/googlecode/dex2jar/reader/io/DataOut;->writeInt(I)V

    .line 302
    iget-object v10, p0, Lpxb/android/axmlLP/AxmlWriter;->stringItems:Lpxb/android/axmlLP/StringItems;

    invoke-virtual {v10}, Lpxb/android/axmlLP/StringItems;->getSize()I

    move-result v9

    .line 303
    .local v9, "stringSize":I
    const/4 v6, 0x0

    .line 304
    .local v6, "padding":I
    rem-int/lit8 v10, v9, 0x4

    if-eqz v10, :cond_2b

    .line 305
    rem-int/lit8 v10, v9, 0x4

    rsub-int/lit8 v6, v10, 0x4

    .line 307
    :cond_2b
    const v10, 0x1c0001

    invoke-interface {v5, v10}, Lcom/googlecode/dex2jar/reader/io/DataOut;->writeInt(I)V

    .line 308
    add-int v10, v9, v6

    add-int/lit8 v10, v10, 0x8

    invoke-interface {v5, v10}, Lcom/googlecode/dex2jar/reader/io/DataOut;->writeInt(I)V

    .line 309
    iget-object v10, p0, Lpxb/android/axmlLP/AxmlWriter;->stringItems:Lpxb/android/axmlLP/StringItems;

    invoke-virtual {v10, v5}, Lpxb/android/axmlLP/StringItems;->write(Lcom/googlecode/dex2jar/reader/io/DataOut;)V

    .line 310
    new-array v10, v6, [B

    invoke-interface {v5, v10}, Lcom/googlecode/dex2jar/reader/io/DataOut;->writeBytes([B)V

    .line 312
    const v10, 0x80180

    invoke-interface {v5, v10}, Lcom/googlecode/dex2jar/reader/io/DataOut;->writeInt(I)V

    .line 313
    iget-object v10, p0, Lpxb/android/axmlLP/AxmlWriter;->resourceIds:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    mul-int/lit8 v10, v10, 0x4

    add-int/lit8 v10, v10, 0x8

    invoke-interface {v5, v10}, Lcom/googlecode/dex2jar/reader/io/DataOut;->writeInt(I)V

    .line 314
    iget-object v10, p0, Lpxb/android/axmlLP/AxmlWriter;->resourceIds:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_5b
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_6f

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 315
    .local v2, "i":Ljava/lang/Integer;
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v11

    invoke-interface {v5, v11}, Lcom/googlecode/dex2jar/reader/io/DataOut;->writeInt(I)V

    goto :goto_5b

    .line 318
    .end local v2    # "i":Ljava/lang/Integer;
    :cond_6f
    new-instance v8, Ljava/util/Stack;

    invoke-direct {v8}, Ljava/util/Stack;-><init>()V

    .line 319
    .local v8, "stack":Ljava/util/Stack;, "Ljava/util/Stack<Lpxb/android/axmlLP/AxmlWriter$Ns;>;"
    iget-object v10, p0, Lpxb/android/axmlLP/AxmlWriter;->nses:Ljava/util/Map;

    invoke-interface {v10}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_7e
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_b1

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 320
    .local v0, "e":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lpxb/android/axmlLP/AxmlWriter$Ns;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lpxb/android/axmlLP/AxmlWriter$Ns;

    .line 321
    .local v3, "ns":Lpxb/android/axmlLP/AxmlWriter$Ns;
    invoke-virtual {v8, v3}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 322
    const v11, 0x100100

    invoke-interface {v5, v11}, Lcom/googlecode/dex2jar/reader/io/DataOut;->writeInt(I)V

    .line 323
    invoke-interface {v5, v13}, Lcom/googlecode/dex2jar/reader/io/DataOut;->writeInt(I)V

    .line 324
    invoke-interface {v5, v12}, Lcom/googlecode/dex2jar/reader/io/DataOut;->writeInt(I)V

    .line 325
    invoke-interface {v5, v12}, Lcom/googlecode/dex2jar/reader/io/DataOut;->writeInt(I)V

    .line 326
    iget-object v11, v3, Lpxb/android/axmlLP/AxmlWriter$Ns;->prefix:Lpxb/android/axmlLP/StringItem;

    iget v11, v11, Lpxb/android/axmlLP/StringItem;->index:I

    invoke-interface {v5, v11}, Lcom/googlecode/dex2jar/reader/io/DataOut;->writeInt(I)V

    .line 327
    iget-object v11, v3, Lpxb/android/axmlLP/AxmlWriter$Ns;->uri:Lpxb/android/axmlLP/StringItem;

    iget v11, v11, Lpxb/android/axmlLP/StringItem;->index:I

    invoke-interface {v5, v11}, Lcom/googlecode/dex2jar/reader/io/DataOut;->writeInt(I)V

    goto :goto_7e

    .line 330
    .end local v0    # "e":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lpxb/android/axmlLP/AxmlWriter$Ns;>;"
    .end local v3    # "ns":Lpxb/android/axmlLP/AxmlWriter$Ns;
    :cond_b1
    iget-object v10, p0, Lpxb/android/axmlLP/AxmlWriter;->firsts:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_b7
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_c7

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lpxb/android/axmlLP/AxmlWriter$NodeImpl;

    .line 331
    .local v1, "first":Lpxb/android/axmlLP/AxmlWriter$NodeImpl;
    invoke-virtual {v1, v5}, Lpxb/android/axmlLP/AxmlWriter$NodeImpl;->write(Lcom/googlecode/dex2jar/reader/io/DataOut;)V

    goto :goto_b7

    .line 334
    .end local v1    # "first":Lpxb/android/axmlLP/AxmlWriter$NodeImpl;
    :cond_c7
    :goto_c7
    invoke-virtual {v8}, Ljava/util/Stack;->size()I

    move-result v10

    if-lez v10, :cond_f3

    .line 335
    invoke-virtual {v8}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lpxb/android/axmlLP/AxmlWriter$Ns;

    .line 336
    .restart local v3    # "ns":Lpxb/android/axmlLP/AxmlWriter$Ns;
    const v10, 0x100101

    invoke-interface {v5, v10}, Lcom/googlecode/dex2jar/reader/io/DataOut;->writeInt(I)V

    .line 337
    invoke-interface {v5, v13}, Lcom/googlecode/dex2jar/reader/io/DataOut;->writeInt(I)V

    .line 338
    iget v10, v3, Lpxb/android/axmlLP/AxmlWriter$Ns;->ln:I

    invoke-interface {v5, v10}, Lcom/googlecode/dex2jar/reader/io/DataOut;->writeInt(I)V

    .line 339
    invoke-interface {v5, v12}, Lcom/googlecode/dex2jar/reader/io/DataOut;->writeInt(I)V

    .line 340
    iget-object v10, v3, Lpxb/android/axmlLP/AxmlWriter$Ns;->prefix:Lpxb/android/axmlLP/StringItem;

    iget v10, v10, Lpxb/android/axmlLP/StringItem;->index:I

    invoke-interface {v5, v10}, Lcom/googlecode/dex2jar/reader/io/DataOut;->writeInt(I)V

    .line 341
    iget-object v10, v3, Lpxb/android/axmlLP/AxmlWriter$Ns;->uri:Lpxb/android/axmlLP/StringItem;

    iget v10, v10, Lpxb/android/axmlLP/StringItem;->index:I

    invoke-interface {v5, v10}, Lcom/googlecode/dex2jar/reader/io/DataOut;->writeInt(I)V

    goto :goto_c7

    .line 343
    .end local v3    # "ns":Lpxb/android/axmlLP/AxmlWriter$Ns;
    :cond_f3
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v10

    return-object v10
.end method

.method update(Lpxb/android/axmlLP/StringItem;)Lpxb/android/axmlLP/StringItem;
    .registers 5
    .param p1, "item"    # Lpxb/android/axmlLP/StringItem;

    .prologue
    .line 347
    if-nez p1, :cond_4

    .line 348
    const/4 v0, 0x0

    .line 355
    :goto_3
    return-object v0

    .line 349
    :cond_4
    iget-object v2, p0, Lpxb/android/axmlLP/AxmlWriter;->otherString:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 350
    .local v1, "i":I
    if-gez v1, :cond_19

    .line 351
    new-instance v0, Lpxb/android/axmlLP/StringItem;

    iget-object v2, p1, Lpxb/android/axmlLP/StringItem;->data:Ljava/lang/String;

    invoke-direct {v0, v2}, Lpxb/android/axmlLP/StringItem;-><init>(Ljava/lang/String;)V

    .line 352
    .local v0, "copy":Lpxb/android/axmlLP/StringItem;
    iget-object v2, p0, Lpxb/android/axmlLP/AxmlWriter;->otherString:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 355
    .end local v0    # "copy":Lpxb/android/axmlLP/StringItem;
    :cond_19
    iget-object v2, p0, Lpxb/android/axmlLP/AxmlWriter;->otherString:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lpxb/android/axmlLP/StringItem;

    move-object v0, v2

    goto :goto_3
.end method

.method updateNs(Lpxb/android/axmlLP/StringItem;)Lpxb/android/axmlLP/StringItem;
    .registers 5
    .param p1, "item"    # Lpxb/android/axmlLP/StringItem;

    .prologue
    const/4 v1, 0x0

    .line 360
    if-nez p1, :cond_4

    .line 367
    :goto_3
    return-object v1

    .line 363
    :cond_4
    iget-object v0, p1, Lpxb/android/axmlLP/StringItem;->data:Ljava/lang/String;

    .line 364
    .local v0, "ns":Ljava/lang/String;
    iget-object v2, p0, Lpxb/android/axmlLP/AxmlWriter;->nses:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_13

    .line 365
    iget-object v2, p0, Lpxb/android/axmlLP/AxmlWriter;->nses:Ljava/util/Map;

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 367
    :cond_13
    invoke-virtual {p0, p1}, Lpxb/android/axmlLP/AxmlWriter;->update(Lpxb/android/axmlLP/StringItem;)Lpxb/android/axmlLP/StringItem;

    move-result-object v1

    goto :goto_3
.end method

.method updateWithResourceId(Lpxb/android/axmlLP/StringItem;I)Lpxb/android/axmlLP/StringItem;
    .registers 7
    .param p1, "name"    # Lpxb/android/axmlLP/StringItem;
    .param p2, "resourceId"    # I

    .prologue
    .line 371
    iget-object v2, p0, Lpxb/android/axmlLP/AxmlWriter;->resourceId2Str:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lpxb/android/axmlLP/StringItem;

    .line 372
    .local v1, "item":Lpxb/android/axmlLP/StringItem;
    if-eqz v1, :cond_f

    .line 379
    .end local v1    # "item":Lpxb/android/axmlLP/StringItem;
    :goto_e
    return-object v1

    .line 375
    .restart local v1    # "item":Lpxb/android/axmlLP/StringItem;
    :cond_f
    new-instance v0, Lpxb/android/axmlLP/StringItem;

    iget-object v2, p1, Lpxb/android/axmlLP/StringItem;->data:Ljava/lang/String;

    invoke-direct {v0, v2}, Lpxb/android/axmlLP/StringItem;-><init>(Ljava/lang/String;)V

    .line 376
    .local v0, "copy":Lpxb/android/axmlLP/StringItem;
    iget-object v2, p0, Lpxb/android/axmlLP/AxmlWriter;->resourceIds:Ljava/util/List;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 377
    iget-object v2, p0, Lpxb/android/axmlLP/AxmlWriter;->resourceString:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 378
    iget-object v2, p0, Lpxb/android/axmlLP/AxmlWriter;->resourceId2Str:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v0

    .line 379
    goto :goto_e
.end method
