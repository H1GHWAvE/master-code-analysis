.class Lpxb/android/axmlLP/AxmlWriter$NodeImpl;
.super Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;
.source "AxmlWriter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lpxb/android/axmlLP/AxmlWriter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "NodeImpl"
.end annotation


# instance fields
.field private attrs:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lpxb/android/axmlLP/AxmlWriter$Attr;",
            ">;"
        }
    .end annotation
.end field

.field private children:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lpxb/android/axmlLP/AxmlWriter$NodeImpl;",
            ">;"
        }
    .end annotation
.end field

.field private line:I

.field private name:Lpxb/android/axmlLP/StringItem;

.field private ns:Lpxb/android/axmlLP/StringItem;

.field private text:Lpxb/android/axmlLP/StringItem;

.field private textLineNumber:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .param p1, "ns"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 79
    invoke-direct {p0, v1}, Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;-><init>(Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;)V

    .line 70
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lpxb/android/axmlLP/AxmlWriter$NodeImpl;->attrs:Ljava/util/Map;

    .line 71
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lpxb/android/axmlLP/AxmlWriter$NodeImpl;->children:Ljava/util/List;

    .line 80
    if-nez p1, :cond_1c

    move-object v0, v1

    :goto_15
    iput-object v0, p0, Lpxb/android/axmlLP/AxmlWriter$NodeImpl;->ns:Lpxb/android/axmlLP/StringItem;

    .line 81
    if-nez p2, :cond_22

    :goto_19
    iput-object v1, p0, Lpxb/android/axmlLP/AxmlWriter$NodeImpl;->name:Lpxb/android/axmlLP/StringItem;

    .line 82
    return-void

    .line 80
    :cond_1c
    new-instance v0, Lpxb/android/axmlLP/StringItem;

    invoke-direct {v0, p1}, Lpxb/android/axmlLP/StringItem;-><init>(Ljava/lang/String;)V

    goto :goto_15

    .line 81
    :cond_22
    new-instance v1, Lpxb/android/axmlLP/StringItem;

    invoke-direct {v1, p2}, Lpxb/android/axmlLP/StringItem;-><init>(Ljava/lang/String;)V

    goto :goto_19
.end method


# virtual methods
.method public attr(Ljava/lang/String;Ljava/lang/String;IILjava/lang/Object;)V
    .registers 14
    .param p1, "ns"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "resourceId"    # I
    .param p4, "type"    # I
    .param p5, "value"    # Ljava/lang/Object;

    .prologue
    .line 86
    if-nez p2, :cond_a

    .line 87
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "name can\'t be null"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 89
    :cond_a
    iget-object v6, p0, Lpxb/android/axmlLP/AxmlWriter$NodeImpl;->attrs:Ljava/util/Map;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    if-nez p1, :cond_44

    const-string v0, "zzz"

    :goto_15
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    new-instance v0, Lpxb/android/axmlLP/AxmlWriter$Attr;

    if-nez p1, :cond_46

    const/4 v1, 0x0

    :goto_2c
    new-instance v2, Lpxb/android/axmlLP/StringItem;

    invoke-direct {v2, p2}, Lpxb/android/axmlLP/StringItem;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x3

    if-ne p4, v3, :cond_4c

    new-instance v5, Lpxb/android/axmlLP/StringItem;

    check-cast p5, Ljava/lang/String;

    .end local p5    # "value":Ljava/lang/Object;
    invoke-direct {v5, p5}, Lpxb/android/axmlLP/StringItem;-><init>(Ljava/lang/String;)V

    :goto_3b
    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lpxb/android/axmlLP/AxmlWriter$Attr;-><init>(Lpxb/android/axmlLP/StringItem;Lpxb/android/axmlLP/StringItem;IILjava/lang/Object;)V

    invoke-interface {v6, v7, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    return-void

    .restart local p5    # "value":Ljava/lang/Object;
    :cond_44
    move-object v0, p1

    .line 89
    goto :goto_15

    :cond_46
    new-instance v1, Lpxb/android/axmlLP/StringItem;

    invoke-direct {v1, p1}, Lpxb/android/axmlLP/StringItem;-><init>(Ljava/lang/String;)V

    goto :goto_2c

    :cond_4c
    move-object v5, p5

    goto :goto_3b
.end method

.method public child(Ljava/lang/String;Ljava/lang/String;)Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;
    .registers 5
    .param p1, "ns"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 96
    new-instance v0, Lpxb/android/axmlLP/AxmlWriter$NodeImpl;

    invoke-direct {v0, p1, p2}, Lpxb/android/axmlLP/AxmlWriter$NodeImpl;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    .local v0, "child":Lpxb/android/axmlLP/AxmlWriter$NodeImpl;
    iget-object v1, p0, Lpxb/android/axmlLP/AxmlWriter$NodeImpl;->children:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 98
    return-object v0
.end method

.method public end()V
    .registers 1

    .prologue
    .line 103
    return-void
.end method

.method public line(I)V
    .registers 2
    .param p1, "ln"    # I

    .prologue
    .line 107
    iput p1, p0, Lpxb/android/axmlLP/AxmlWriter$NodeImpl;->line:I

    .line 108
    return-void
.end method

.method public prepare(Lpxb/android/axmlLP/AxmlWriter;)I
    .registers 7
    .param p1, "axmlWriter"    # Lpxb/android/axmlLP/AxmlWriter;

    .prologue
    .line 111
    iget-object v3, p0, Lpxb/android/axmlLP/AxmlWriter$NodeImpl;->ns:Lpxb/android/axmlLP/StringItem;

    invoke-virtual {p1, v3}, Lpxb/android/axmlLP/AxmlWriter;->updateNs(Lpxb/android/axmlLP/StringItem;)Lpxb/android/axmlLP/StringItem;

    move-result-object v3

    iput-object v3, p0, Lpxb/android/axmlLP/AxmlWriter$NodeImpl;->ns:Lpxb/android/axmlLP/StringItem;

    .line 112
    iget-object v3, p0, Lpxb/android/axmlLP/AxmlWriter$NodeImpl;->name:Lpxb/android/axmlLP/StringItem;

    invoke-virtual {p1, v3}, Lpxb/android/axmlLP/AxmlWriter;->update(Lpxb/android/axmlLP/StringItem;)Lpxb/android/axmlLP/StringItem;

    move-result-object v3

    iput-object v3, p0, Lpxb/android/axmlLP/AxmlWriter$NodeImpl;->name:Lpxb/android/axmlLP/StringItem;

    .line 114
    invoke-virtual {p0}, Lpxb/android/axmlLP/AxmlWriter$NodeImpl;->sortedAttrs()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_18
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_28

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpxb/android/axmlLP/AxmlWriter$Attr;

    .line 115
    .local v0, "attr":Lpxb/android/axmlLP/AxmlWriter$Attr;
    invoke-virtual {v0, p1}, Lpxb/android/axmlLP/AxmlWriter$Attr;->prepare(Lpxb/android/axmlLP/AxmlWriter;)V

    goto :goto_18

    .line 117
    .end local v0    # "attr":Lpxb/android/axmlLP/AxmlWriter$Attr;
    :cond_28
    iget-object v3, p0, Lpxb/android/axmlLP/AxmlWriter$NodeImpl;->text:Lpxb/android/axmlLP/StringItem;

    invoke-virtual {p1, v3}, Lpxb/android/axmlLP/AxmlWriter;->update(Lpxb/android/axmlLP/StringItem;)Lpxb/android/axmlLP/StringItem;

    move-result-object v3

    iput-object v3, p0, Lpxb/android/axmlLP/AxmlWriter$NodeImpl;->text:Lpxb/android/axmlLP/StringItem;

    .line 118
    iget-object v3, p0, Lpxb/android/axmlLP/AxmlWriter$NodeImpl;->attrs:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v3

    mul-int/lit8 v3, v3, 0x14

    add-int/lit8 v2, v3, 0x3c

    .line 120
    .local v2, "size":I
    iget-object v3, p0, Lpxb/android/axmlLP/AxmlWriter$NodeImpl;->children:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_40
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_52

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lpxb/android/axmlLP/AxmlWriter$NodeImpl;

    .line 121
    .local v1, "child":Lpxb/android/axmlLP/AxmlWriter$NodeImpl;
    invoke-virtual {v1, p1}, Lpxb/android/axmlLP/AxmlWriter$NodeImpl;->prepare(Lpxb/android/axmlLP/AxmlWriter;)I

    move-result v4

    add-int/2addr v2, v4

    .line 122
    goto :goto_40

    .line 123
    .end local v1    # "child":Lpxb/android/axmlLP/AxmlWriter$NodeImpl;
    :cond_52
    iget-object v3, p0, Lpxb/android/axmlLP/AxmlWriter$NodeImpl;->text:Lpxb/android/axmlLP/StringItem;

    if-eqz v3, :cond_58

    .line 124
    add-int/lit8 v2, v2, 0x1c

    .line 126
    :cond_58
    return v2
.end method

.method sortedAttrs()Ljava/util/List;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lpxb/android/axmlLP/AxmlWriter$Attr;",
            ">;"
        }
    .end annotation

    .prologue
    .line 130
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lpxb/android/axmlLP/AxmlWriter$NodeImpl;->attrs:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 131
    .local v0, "lAttrs":Ljava/util/List;, "Ljava/util/List<Lpxb/android/axmlLP/AxmlWriter$Attr;>;"
    new-instance v1, Lpxb/android/axmlLP/AxmlWriter$NodeImpl$1;

    invoke-direct {v1, p0}, Lpxb/android/axmlLP/AxmlWriter$NodeImpl$1;-><init>(Lpxb/android/axmlLP/AxmlWriter$NodeImpl;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 154
    return-object v0
.end method

.method public text(ILjava/lang/String;)V
    .registers 4
    .param p1, "ln"    # I
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 159
    new-instance v0, Lpxb/android/axmlLP/StringItem;

    invoke-direct {v0, p2}, Lpxb/android/axmlLP/StringItem;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lpxb/android/axmlLP/AxmlWriter$NodeImpl;->text:Lpxb/android/axmlLP/StringItem;

    .line 160
    iput p1, p0, Lpxb/android/axmlLP/AxmlWriter$NodeImpl;->textLineNumber:I

    .line 161
    return-void
.end method

.method write(Lcom/googlecode/dex2jar/reader/io/DataOut;)V
    .registers 9
    .param p1, "out"    # Lcom/googlecode/dex2jar/reader/io/DataOut;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v4, -0x1

    .line 165
    const v3, 0x100102

    invoke-interface {p1, v3}, Lcom/googlecode/dex2jar/reader/io/DataOut;->writeInt(I)V

    .line 166
    iget-object v3, p0, Lpxb/android/axmlLP/AxmlWriter$NodeImpl;->attrs:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v3

    mul-int/lit8 v3, v3, 0x14

    add-int/lit8 v3, v3, 0x24

    invoke-interface {p1, v3}, Lcom/googlecode/dex2jar/reader/io/DataOut;->writeInt(I)V

    .line 167
    iget v3, p0, Lpxb/android/axmlLP/AxmlWriter$NodeImpl;->line:I

    invoke-interface {p1, v3}, Lcom/googlecode/dex2jar/reader/io/DataOut;->writeInt(I)V

    .line 168
    invoke-interface {p1, v4}, Lcom/googlecode/dex2jar/reader/io/DataOut;->writeInt(I)V

    .line 169
    iget-object v3, p0, Lpxb/android/axmlLP/AxmlWriter$NodeImpl;->ns:Lpxb/android/axmlLP/StringItem;

    if-eqz v3, :cond_92

    iget-object v3, p0, Lpxb/android/axmlLP/AxmlWriter$NodeImpl;->ns:Lpxb/android/axmlLP/StringItem;

    iget v3, v3, Lpxb/android/axmlLP/StringItem;->index:I

    :goto_25
    invoke-interface {p1, v3}, Lcom/googlecode/dex2jar/reader/io/DataOut;->writeInt(I)V

    .line 170
    iget-object v3, p0, Lpxb/android/axmlLP/AxmlWriter$NodeImpl;->name:Lpxb/android/axmlLP/StringItem;

    iget v3, v3, Lpxb/android/axmlLP/StringItem;->index:I

    invoke-interface {p1, v3}, Lcom/googlecode/dex2jar/reader/io/DataOut;->writeInt(I)V

    .line 171
    const v3, 0x140014

    invoke-interface {p1, v3}, Lcom/googlecode/dex2jar/reader/io/DataOut;->writeInt(I)V

    .line 172
    iget-object v3, p0, Lpxb/android/axmlLP/AxmlWriter$NodeImpl;->attrs:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v3

    invoke-interface {p1, v3}, Lcom/googlecode/dex2jar/reader/io/DataOut;->writeShort(I)V

    .line 173
    invoke-interface {p1, v5}, Lcom/googlecode/dex2jar/reader/io/DataOut;->writeShort(I)V

    .line 174
    invoke-interface {p1, v5}, Lcom/googlecode/dex2jar/reader/io/DataOut;->writeShort(I)V

    .line 175
    invoke-interface {p1, v5}, Lcom/googlecode/dex2jar/reader/io/DataOut;->writeShort(I)V

    .line 176
    invoke-virtual {p0}, Lpxb/android/axmlLP/AxmlWriter$NodeImpl;->sortedAttrs()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_4f
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_ba

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpxb/android/axmlLP/AxmlWriter$Attr;

    .line 177
    .local v0, "attr":Lpxb/android/axmlLP/AxmlWriter$Attr;
    iget-object v3, v0, Lpxb/android/axmlLP/AxmlWriter$Attr;->ns:Lpxb/android/axmlLP/StringItem;

    if-nez v3, :cond_94

    move v3, v4

    :goto_60
    invoke-interface {p1, v3}, Lcom/googlecode/dex2jar/reader/io/DataOut;->writeInt(I)V

    .line 178
    iget-object v3, v0, Lpxb/android/axmlLP/AxmlWriter$Attr;->name:Lpxb/android/axmlLP/StringItem;

    iget v3, v3, Lpxb/android/axmlLP/StringItem;->index:I

    invoke-interface {p1, v3}, Lcom/googlecode/dex2jar/reader/io/DataOut;->writeInt(I)V

    .line 179
    iget-object v3, v0, Lpxb/android/axmlLP/AxmlWriter$Attr;->value:Ljava/lang/Object;

    instance-of v3, v3, Lpxb/android/axmlLP/StringItem;

    if-eqz v3, :cond_99

    iget-object v3, v0, Lpxb/android/axmlLP/AxmlWriter$Attr;->value:Ljava/lang/Object;

    check-cast v3, Lpxb/android/axmlLP/StringItem;

    iget v3, v3, Lpxb/android/axmlLP/StringItem;->index:I

    :goto_76
    invoke-interface {p1, v3}, Lcom/googlecode/dex2jar/reader/io/DataOut;->writeInt(I)V

    .line 180
    iget v3, v0, Lpxb/android/axmlLP/AxmlWriter$Attr;->type:I

    shl-int/lit8 v3, v3, 0x18

    or-int/lit8 v3, v3, 0x8

    invoke-interface {p1, v3}, Lcom/googlecode/dex2jar/reader/io/DataOut;->writeInt(I)V

    .line 181
    iget-object v2, v0, Lpxb/android/axmlLP/AxmlWriter$Attr;->value:Ljava/lang/Object;

    .line 182
    .local v2, "v":Ljava/lang/Object;
    instance-of v3, v2, Lpxb/android/axmlLP/StringItem;

    if-eqz v3, :cond_9b

    .line 183
    iget-object v3, v0, Lpxb/android/axmlLP/AxmlWriter$Attr;->value:Ljava/lang/Object;

    check-cast v3, Lpxb/android/axmlLP/StringItem;

    iget v3, v3, Lpxb/android/axmlLP/StringItem;->index:I

    invoke-interface {p1, v3}, Lcom/googlecode/dex2jar/reader/io/DataOut;->writeInt(I)V

    goto :goto_4f

    .end local v0    # "attr":Lpxb/android/axmlLP/AxmlWriter$Attr;
    .end local v2    # "v":Ljava/lang/Object;
    :cond_92
    move v3, v4

    .line 169
    goto :goto_25

    .line 177
    .restart local v0    # "attr":Lpxb/android/axmlLP/AxmlWriter$Attr;
    :cond_94
    iget-object v3, v0, Lpxb/android/axmlLP/AxmlWriter$Attr;->ns:Lpxb/android/axmlLP/StringItem;

    iget v3, v3, Lpxb/android/axmlLP/StringItem;->index:I

    goto :goto_60

    :cond_99
    move v3, v4

    .line 179
    goto :goto_76

    .line 184
    .restart local v2    # "v":Ljava/lang/Object;
    :cond_9b
    instance-of v3, v2, Ljava/lang/Boolean;

    if-eqz v3, :cond_ae

    .line 185
    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v3, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_ac

    move v3, v4

    :goto_a8
    invoke-interface {p1, v3}, Lcom/googlecode/dex2jar/reader/io/DataOut;->writeInt(I)V

    goto :goto_4f

    :cond_ac
    move v3, v5

    goto :goto_a8

    .line 187
    :cond_ae
    iget-object v3, v0, Lpxb/android/axmlLP/AxmlWriter$Attr;->value:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-interface {p1, v3}, Lcom/googlecode/dex2jar/reader/io/DataOut;->writeInt(I)V

    goto :goto_4f

    .line 191
    .end local v0    # "attr":Lpxb/android/axmlLP/AxmlWriter$Attr;
    .end local v2    # "v":Ljava/lang/Object;
    :cond_ba
    iget-object v3, p0, Lpxb/android/axmlLP/AxmlWriter$NodeImpl;->text:Lpxb/android/axmlLP/StringItem;

    if-eqz v3, :cond_e0

    .line 192
    const v3, 0x100104

    invoke-interface {p1, v3}, Lcom/googlecode/dex2jar/reader/io/DataOut;->writeInt(I)V

    .line 193
    const/16 v3, 0x1c

    invoke-interface {p1, v3}, Lcom/googlecode/dex2jar/reader/io/DataOut;->writeInt(I)V

    .line 194
    iget v3, p0, Lpxb/android/axmlLP/AxmlWriter$NodeImpl;->textLineNumber:I

    invoke-interface {p1, v3}, Lcom/googlecode/dex2jar/reader/io/DataOut;->writeInt(I)V

    .line 195
    invoke-interface {p1, v4}, Lcom/googlecode/dex2jar/reader/io/DataOut;->writeInt(I)V

    .line 196
    iget-object v3, p0, Lpxb/android/axmlLP/AxmlWriter$NodeImpl;->text:Lpxb/android/axmlLP/StringItem;

    iget v3, v3, Lpxb/android/axmlLP/StringItem;->index:I

    invoke-interface {p1, v3}, Lcom/googlecode/dex2jar/reader/io/DataOut;->writeInt(I)V

    .line 197
    const/16 v3, 0x8

    invoke-interface {p1, v3}, Lcom/googlecode/dex2jar/reader/io/DataOut;->writeInt(I)V

    .line 198
    invoke-interface {p1, v5}, Lcom/googlecode/dex2jar/reader/io/DataOut;->writeInt(I)V

    .line 202
    :cond_e0
    iget-object v3, p0, Lpxb/android/axmlLP/AxmlWriter$NodeImpl;->children:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_e6
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_f6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lpxb/android/axmlLP/AxmlWriter$NodeImpl;

    .line 203
    .local v1, "child":Lpxb/android/axmlLP/AxmlWriter$NodeImpl;
    invoke-virtual {v1, p1}, Lpxb/android/axmlLP/AxmlWriter$NodeImpl;->write(Lcom/googlecode/dex2jar/reader/io/DataOut;)V

    goto :goto_e6

    .line 207
    .end local v1    # "child":Lpxb/android/axmlLP/AxmlWriter$NodeImpl;
    :cond_f6
    const v3, 0x100103

    invoke-interface {p1, v3}, Lcom/googlecode/dex2jar/reader/io/DataOut;->writeInt(I)V

    .line 208
    const/16 v3, 0x18

    invoke-interface {p1, v3}, Lcom/googlecode/dex2jar/reader/io/DataOut;->writeInt(I)V

    .line 209
    invoke-interface {p1, v4}, Lcom/googlecode/dex2jar/reader/io/DataOut;->writeInt(I)V

    .line 210
    invoke-interface {p1, v4}, Lcom/googlecode/dex2jar/reader/io/DataOut;->writeInt(I)V

    .line 211
    iget-object v3, p0, Lpxb/android/axmlLP/AxmlWriter$NodeImpl;->ns:Lpxb/android/axmlLP/StringItem;

    if-eqz v3, :cond_10f

    iget-object v3, p0, Lpxb/android/axmlLP/AxmlWriter$NodeImpl;->ns:Lpxb/android/axmlLP/StringItem;

    iget v4, v3, Lpxb/android/axmlLP/StringItem;->index:I

    :cond_10f
    invoke-interface {p1, v4}, Lcom/googlecode/dex2jar/reader/io/DataOut;->writeInt(I)V

    .line 212
    iget-object v3, p0, Lpxb/android/axmlLP/AxmlWriter$NodeImpl;->name:Lpxb/android/axmlLP/StringItem;

    iget v3, v3, Lpxb/android/axmlLP/StringItem;->index:I

    invoke-interface {p1, v3}, Lcom/googlecode/dex2jar/reader/io/DataOut;->writeInt(I)V

    .line 213
    return-void
.end method
