.class Lpxb/android/axmlLP/AxmlWriter$NodeImpl$1;
.super Ljava/lang/Object;
.source "AxmlWriter.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lpxb/android/axmlLP/AxmlWriter$NodeImpl;->sortedAttrs()Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lpxb/android/axmlLP/AxmlWriter$Attr;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lpxb/android/axmlLP/AxmlWriter$NodeImpl;


# direct methods
.method constructor <init>(Lpxb/android/axmlLP/AxmlWriter$NodeImpl;)V
    .registers 2
    .param p1, "this$0"    # Lpxb/android/axmlLP/AxmlWriter$NodeImpl;

    .prologue
    .line 131
    iput-object p1, p0, Lpxb/android/axmlLP/AxmlWriter$NodeImpl$1;->this$0:Lpxb/android/axmlLP/AxmlWriter$NodeImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 4

    .prologue
    .line 131
    check-cast p1, Lpxb/android/axmlLP/AxmlWriter$Attr;

    check-cast p2, Lpxb/android/axmlLP/AxmlWriter$Attr;

    invoke-virtual {p0, p1, p2}, Lpxb/android/axmlLP/AxmlWriter$NodeImpl$1;->compare(Lpxb/android/axmlLP/AxmlWriter$Attr;Lpxb/android/axmlLP/AxmlWriter$Attr;)I

    move-result v0

    return v0
.end method

.method public compare(Lpxb/android/axmlLP/AxmlWriter$Attr;Lpxb/android/axmlLP/AxmlWriter$Attr;)I
    .registers 6
    .param p1, "a"    # Lpxb/android/axmlLP/AxmlWriter$Attr;
    .param p2, "b"    # Lpxb/android/axmlLP/AxmlWriter$Attr;

    .prologue
    .line 134
    iget-object v1, p1, Lpxb/android/axmlLP/AxmlWriter$Attr;->ns:Lpxb/android/axmlLP/StringItem;

    if-nez v1, :cond_17

    .line 135
    iget-object v1, p2, Lpxb/android/axmlLP/AxmlWriter$Attr;->ns:Lpxb/android/axmlLP/StringItem;

    if-nez v1, :cond_15

    .line 136
    iget-object v1, p2, Lpxb/android/axmlLP/AxmlWriter$Attr;->name:Lpxb/android/axmlLP/StringItem;

    iget-object v1, v1, Lpxb/android/axmlLP/StringItem;->data:Ljava/lang/String;

    iget-object v2, p1, Lpxb/android/axmlLP/AxmlWriter$Attr;->name:Lpxb/android/axmlLP/StringItem;

    iget-object v2, v2, Lpxb/android/axmlLP/StringItem;->data:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    .line 150
    :cond_14
    :goto_14
    return v0

    .line 138
    :cond_15
    const/4 v0, 0x1

    goto :goto_14

    .line 140
    :cond_17
    iget-object v1, p2, Lpxb/android/axmlLP/AxmlWriter$Attr;->ns:Lpxb/android/axmlLP/StringItem;

    if-nez v1, :cond_1d

    .line 141
    const/4 v0, -0x1

    goto :goto_14

    .line 143
    :cond_1d
    iget-object v1, p1, Lpxb/android/axmlLP/AxmlWriter$Attr;->ns:Lpxb/android/axmlLP/StringItem;

    iget-object v1, v1, Lpxb/android/axmlLP/StringItem;->data:Ljava/lang/String;

    iget-object v2, p2, Lpxb/android/axmlLP/AxmlWriter$Attr;->ns:Lpxb/android/axmlLP/StringItem;

    iget-object v2, v2, Lpxb/android/axmlLP/StringItem;->data:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    .line 144
    .local v0, "x":I
    if-nez v0, :cond_14

    .line 145
    iget v1, p1, Lpxb/android/axmlLP/AxmlWriter$Attr;->resourceId:I

    iget v2, p2, Lpxb/android/axmlLP/AxmlWriter$Attr;->resourceId:I

    sub-int v0, v1, v2

    .line 146
    if-nez v0, :cond_14

    .line 147
    iget-object v1, p1, Lpxb/android/axmlLP/AxmlWriter$Attr;->name:Lpxb/android/axmlLP/StringItem;

    iget-object v1, v1, Lpxb/android/axmlLP/StringItem;->data:Ljava/lang/String;

    iget-object v2, p2, Lpxb/android/axmlLP/AxmlWriter$Attr;->name:Lpxb/android/axmlLP/StringItem;

    iget-object v2, v2, Lpxb/android/axmlLP/StringItem;->data:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_14
.end method
