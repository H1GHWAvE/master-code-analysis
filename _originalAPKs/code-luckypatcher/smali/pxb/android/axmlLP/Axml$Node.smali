.class public Lpxb/android/axmlLP/Axml$Node;
.super Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;
.source "Axml.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lpxb/android/axmlLP/Axml;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Node"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lpxb/android/axmlLP/Axml$Node$Text;,
        Lpxb/android/axmlLP/Axml$Node$Attr;
    }
.end annotation


# instance fields
.field public attrs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lpxb/android/axmlLP/Axml$Node$Attr;",
            ">;"
        }
    .end annotation
.end field

.field public children:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lpxb/android/axmlLP/Axml$Node;",
            ">;"
        }
    .end annotation
.end field

.field public ln:Ljava/lang/Integer;

.field public name:Ljava/lang/String;

.field public ns:Ljava/lang/String;

.field public text:Lpxb/android/axmlLP/Axml$Node$Text;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 23
    invoke-direct {p0}, Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;-><init>()V

    .line 43
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lpxb/android/axmlLP/Axml$Node;->attrs:Ljava/util/List;

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lpxb/android/axmlLP/Axml$Node;->children:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public accept(Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;)V
    .registers 5
    .param p1, "nodeVisitor"    # Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;

    .prologue
    .line 50
    iget-object v1, p0, Lpxb/android/axmlLP/Axml$Node;->ns:Ljava/lang/String;

    iget-object v2, p0, Lpxb/android/axmlLP/Axml$Node;->name:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;->child(Ljava/lang/String;Ljava/lang/String;)Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;

    move-result-object v0

    .line 51
    .local v0, "nodeVisitor2":Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;
    invoke-virtual {p0, v0}, Lpxb/android/axmlLP/Axml$Node;->acceptB(Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;)V

    .line 52
    invoke-virtual {v0}, Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;->end()V

    .line 53
    return-void
.end method

.method public acceptB(Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;)V
    .registers 6
    .param p1, "nodeVisitor"    # Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;

    .prologue
    .line 56
    iget-object v2, p0, Lpxb/android/axmlLP/Axml$Node;->text:Lpxb/android/axmlLP/Axml$Node$Text;

    if-eqz v2, :cond_9

    .line 57
    iget-object v2, p0, Lpxb/android/axmlLP/Axml$Node;->text:Lpxb/android/axmlLP/Axml$Node$Text;

    invoke-virtual {v2, p1}, Lpxb/android/axmlLP/Axml$Node$Text;->accept(Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;)V

    .line 59
    :cond_9
    iget-object v2, p0, Lpxb/android/axmlLP/Axml$Node;->attrs:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_f
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1f

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpxb/android/axmlLP/Axml$Node$Attr;

    .line 60
    .local v0, "a":Lpxb/android/axmlLP/Axml$Node$Attr;
    invoke-virtual {v0, p1}, Lpxb/android/axmlLP/Axml$Node$Attr;->accept(Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;)V

    goto :goto_f

    .line 62
    .end local v0    # "a":Lpxb/android/axmlLP/Axml$Node$Attr;
    :cond_1f
    iget-object v2, p0, Lpxb/android/axmlLP/Axml$Node;->ln:Ljava/lang/Integer;

    if-eqz v2, :cond_2c

    .line 63
    iget-object v2, p0, Lpxb/android/axmlLP/Axml$Node;->ln:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v2}, Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;->line(I)V

    .line 65
    :cond_2c
    iget-object v2, p0, Lpxb/android/axmlLP/Axml$Node;->children:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_32
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_42

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lpxb/android/axmlLP/Axml$Node;

    .line 66
    .local v1, "c":Lpxb/android/axmlLP/Axml$Node;
    invoke-virtual {v1, p1}, Lpxb/android/axmlLP/Axml$Node;->accept(Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;)V

    goto :goto_32

    .line 68
    .end local v1    # "c":Lpxb/android/axmlLP/Axml$Node;
    :cond_42
    return-void
.end method

.method public attr(Ljava/lang/String;Ljava/lang/String;IILjava/lang/Object;)V
    .registers 8
    .param p1, "ns"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "resourceId"    # I
    .param p4, "type"    # I
    .param p5, "obj"    # Ljava/lang/Object;

    .prologue
    .line 72
    new-instance v0, Lpxb/android/axmlLP/Axml$Node$Attr;

    invoke-direct {v0}, Lpxb/android/axmlLP/Axml$Node$Attr;-><init>()V

    .line 73
    .local v0, "attr":Lpxb/android/axmlLP/Axml$Node$Attr;
    iput-object p2, v0, Lpxb/android/axmlLP/Axml$Node$Attr;->name:Ljava/lang/String;

    .line 74
    iput-object p1, v0, Lpxb/android/axmlLP/Axml$Node$Attr;->ns:Ljava/lang/String;

    .line 75
    iput p3, v0, Lpxb/android/axmlLP/Axml$Node$Attr;->resourceId:I

    .line 76
    iput p4, v0, Lpxb/android/axmlLP/Axml$Node$Attr;->type:I

    .line 77
    iput-object p5, v0, Lpxb/android/axmlLP/Axml$Node$Attr;->value:Ljava/lang/Object;

    .line 78
    iget-object v1, p0, Lpxb/android/axmlLP/Axml$Node;->attrs:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 79
    return-void
.end method

.method public child(Ljava/lang/String;Ljava/lang/String;)Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;
    .registers 5
    .param p1, "ns"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 83
    new-instance v0, Lpxb/android/axmlLP/Axml$Node;

    invoke-direct {v0}, Lpxb/android/axmlLP/Axml$Node;-><init>()V

    .line 84
    .local v0, "node":Lpxb/android/axmlLP/Axml$Node;
    iput-object p2, v0, Lpxb/android/axmlLP/Axml$Node;->name:Ljava/lang/String;

    .line 85
    iput-object p1, v0, Lpxb/android/axmlLP/Axml$Node;->ns:Ljava/lang/String;

    .line 86
    iget-object v1, p0, Lpxb/android/axmlLP/Axml$Node;->children:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 87
    return-object v0
.end method

.method public line(I)V
    .registers 3
    .param p1, "ln"    # I

    .prologue
    .line 92
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lpxb/android/axmlLP/Axml$Node;->ln:Ljava/lang/Integer;

    .line 93
    return-void
.end method

.method public text(ILjava/lang/String;)V
    .registers 4
    .param p1, "lineNumber"    # I
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 97
    new-instance v0, Lpxb/android/axmlLP/Axml$Node$Text;

    invoke-direct {v0}, Lpxb/android/axmlLP/Axml$Node$Text;-><init>()V

    .line 98
    .local v0, "text":Lpxb/android/axmlLP/Axml$Node$Text;
    iput p1, v0, Lpxb/android/axmlLP/Axml$Node$Text;->ln:I

    .line 99
    iput-object p2, v0, Lpxb/android/axmlLP/Axml$Node$Text;->text:Ljava/lang/String;

    .line 100
    iput-object v0, p0, Lpxb/android/axmlLP/Axml$Node;->text:Lpxb/android/axmlLP/Axml$Node$Text;

    .line 101
    return-void
.end method
