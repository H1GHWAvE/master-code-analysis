.class Lpxb/android/axmlLP/AxmlWriter$Attr;
.super Ljava/lang/Object;
.source "AxmlWriter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lpxb/android/axmlLP/AxmlWriter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Attr"
.end annotation


# instance fields
.field public name:Lpxb/android/axmlLP/StringItem;

.field public ns:Lpxb/android/axmlLP/StringItem;

.field public resourceId:I

.field public type:I

.field public value:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lpxb/android/axmlLP/StringItem;Lpxb/android/axmlLP/StringItem;IILjava/lang/Object;)V
    .registers 6
    .param p1, "ns"    # Lpxb/android/axmlLP/StringItem;
    .param p2, "name"    # Lpxb/android/axmlLP/StringItem;
    .param p3, "resourceId"    # I
    .param p4, "type"    # I
    .param p5, "value"    # Ljava/lang/Object;

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lpxb/android/axmlLP/AxmlWriter$Attr;->ns:Lpxb/android/axmlLP/StringItem;

    .line 48
    iput-object p2, p0, Lpxb/android/axmlLP/AxmlWriter$Attr;->name:Lpxb/android/axmlLP/StringItem;

    .line 49
    iput p3, p0, Lpxb/android/axmlLP/AxmlWriter$Attr;->resourceId:I

    .line 50
    iput p4, p0, Lpxb/android/axmlLP/AxmlWriter$Attr;->type:I

    .line 51
    iput-object p5, p0, Lpxb/android/axmlLP/AxmlWriter$Attr;->value:Ljava/lang/Object;

    .line 52
    return-void
.end method


# virtual methods
.method public prepare(Lpxb/android/axmlLP/AxmlWriter;)V
    .registers 4
    .param p1, "axmlWriter"    # Lpxb/android/axmlLP/AxmlWriter;

    .prologue
    .line 55
    iget-object v0, p0, Lpxb/android/axmlLP/AxmlWriter$Attr;->ns:Lpxb/android/axmlLP/StringItem;

    invoke-virtual {p1, v0}, Lpxb/android/axmlLP/AxmlWriter;->updateNs(Lpxb/android/axmlLP/StringItem;)Lpxb/android/axmlLP/StringItem;

    move-result-object v0

    iput-object v0, p0, Lpxb/android/axmlLP/AxmlWriter$Attr;->ns:Lpxb/android/axmlLP/StringItem;

    .line 56
    iget-object v0, p0, Lpxb/android/axmlLP/AxmlWriter$Attr;->name:Lpxb/android/axmlLP/StringItem;

    if-eqz v0, :cond_1b

    .line 57
    iget v0, p0, Lpxb/android/axmlLP/AxmlWriter$Attr;->resourceId:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_2c

    .line 58
    iget-object v0, p0, Lpxb/android/axmlLP/AxmlWriter$Attr;->name:Lpxb/android/axmlLP/StringItem;

    iget v1, p0, Lpxb/android/axmlLP/AxmlWriter$Attr;->resourceId:I

    invoke-virtual {p1, v0, v1}, Lpxb/android/axmlLP/AxmlWriter;->updateWithResourceId(Lpxb/android/axmlLP/StringItem;I)Lpxb/android/axmlLP/StringItem;

    move-result-object v0

    iput-object v0, p0, Lpxb/android/axmlLP/AxmlWriter$Attr;->name:Lpxb/android/axmlLP/StringItem;

    .line 63
    :cond_1b
    :goto_1b
    iget-object v0, p0, Lpxb/android/axmlLP/AxmlWriter$Attr;->value:Ljava/lang/Object;

    instance-of v0, v0, Lpxb/android/axmlLP/StringItem;

    if-eqz v0, :cond_2b

    .line 64
    iget-object v0, p0, Lpxb/android/axmlLP/AxmlWriter$Attr;->value:Ljava/lang/Object;

    check-cast v0, Lpxb/android/axmlLP/StringItem;

    invoke-virtual {p1, v0}, Lpxb/android/axmlLP/AxmlWriter;->update(Lpxb/android/axmlLP/StringItem;)Lpxb/android/axmlLP/StringItem;

    move-result-object v0

    iput-object v0, p0, Lpxb/android/axmlLP/AxmlWriter$Attr;->value:Ljava/lang/Object;

    .line 66
    :cond_2b
    return-void

    .line 60
    :cond_2c
    iget-object v0, p0, Lpxb/android/axmlLP/AxmlWriter$Attr;->name:Lpxb/android/axmlLP/StringItem;

    invoke-virtual {p1, v0}, Lpxb/android/axmlLP/AxmlWriter;->update(Lpxb/android/axmlLP/StringItem;)Lpxb/android/axmlLP/StringItem;

    move-result-object v0

    iput-object v0, p0, Lpxb/android/axmlLP/AxmlWriter$Attr;->name:Lpxb/android/axmlLP/StringItem;

    goto :goto_1b
.end method
