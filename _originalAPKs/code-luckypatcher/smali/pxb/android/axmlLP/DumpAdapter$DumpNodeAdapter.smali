.class public Lpxb/android/axmlLP/DumpAdapter$DumpNodeAdapter;
.super Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;
.source "DumpAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lpxb/android/axmlLP/DumpAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DumpNodeAdapter"
.end annotation


# instance fields
.field protected deep:I

.field protected nses:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;)V
    .registers 3
    .param p1, "nv"    # Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;-><init>(Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;)V

    .line 41
    const/4 v0, 0x0

    iput v0, p0, Lpxb/android/axmlLP/DumpAdapter$DumpNodeAdapter;->deep:I

    .line 42
    const/4 v0, 0x0

    iput-object v0, p0, Lpxb/android/axmlLP/DumpAdapter$DumpNodeAdapter;->nses:Ljava/util/Map;

    .line 43
    return-void
.end method

.method public constructor <init>(Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;ILjava/util/Map;)V
    .registers 4
    .param p1, "nv"    # Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;
    .param p2, "x"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;",
            "I",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 46
    .local p3, "nses":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0, p1}, Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;-><init>(Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;)V

    .line 47
    iput p2, p0, Lpxb/android/axmlLP/DumpAdapter$DumpNodeAdapter;->deep:I

    .line 48
    iput-object p3, p0, Lpxb/android/axmlLP/DumpAdapter$DumpNodeAdapter;->nses:Ljava/util/Map;

    .line 49
    return-void
.end method


# virtual methods
.method public attr(Ljava/lang/String;Ljava/lang/String;IILjava/lang/Object;)V
    .registers 14
    .param p1, "ns"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "resourceId"    # I
    .param p4, "type"    # I
    .param p5, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 53
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_4
    iget v1, p0, Lpxb/android/axmlLP/DumpAdapter$DumpNodeAdapter;->deep:I

    if-ge v0, v1, :cond_12

    .line 54
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "  "

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 53
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 56
    :cond_12
    if-eqz p1, :cond_27

    .line 57
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "%s:"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-virtual {p0, p1}, Lpxb/android/axmlLP/DumpAdapter$DumpNodeAdapter;->getPrefix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 59
    :cond_27
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v1, p2}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 60
    const/4 v1, -0x1

    if-eq p3, v1, :cond_42

    .line 61
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "(%08x)"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 63
    :cond_42
    instance-of v1, p5, Ljava/lang/String;

    if-eqz v1, :cond_64

    .line 64
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "=[%08x]\"%s\""

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    aput-object p5, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 70
    :goto_5b
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v1}, Ljava/io/PrintStream;->println()V

    .line 71
    invoke-super/range {p0 .. p5}, Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;->attr(Ljava/lang/String;Ljava/lang/String;IILjava/lang/Object;)V

    .line 72
    return-void

    .line 65
    :cond_64
    instance-of v1, p5, Ljava/lang/Boolean;

    if-eqz v1, :cond_7e

    .line 66
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "=[%08x]\"%b\""

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    aput-object p5, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    goto :goto_5b

    .line 68
    :cond_7e
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "=[%08x]%08x"

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    aput-object p5, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    goto :goto_5b
.end method

.method public child(Ljava/lang/String;Ljava/lang/String;)Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;
    .registers 8
    .param p1, "ns"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 76
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget v2, p0, Lpxb/android/axmlLP/DumpAdapter$DumpNodeAdapter;->deep:I

    if-ge v0, v2, :cond_f

    .line 77
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "  "

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 76
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 79
    :cond_f
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "<"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 80
    if-eqz p1, :cond_34

    .line 81
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, p1}, Lpxb/android/axmlLP/DumpAdapter$DumpNodeAdapter;->getPrefix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 83
    :cond_34
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v2, p2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 84
    invoke-super {p0, p1, p2}, Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;->child(Ljava/lang/String;Ljava/lang/String;)Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;

    move-result-object v1

    .line 85
    .local v1, "nv":Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;
    if-eqz v1, :cond_4b

    .line 86
    new-instance v2, Lpxb/android/axmlLP/DumpAdapter$DumpNodeAdapter;

    iget v3, p0, Lpxb/android/axmlLP/DumpAdapter$DumpNodeAdapter;->deep:I

    add-int/lit8 v3, v3, 0x1

    iget-object v4, p0, Lpxb/android/axmlLP/DumpAdapter$DumpNodeAdapter;->nses:Ljava/util/Map;

    invoke-direct {v2, v1, v3, v4}, Lpxb/android/axmlLP/DumpAdapter$DumpNodeAdapter;-><init>(Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;ILjava/util/Map;)V

    .line 88
    :goto_4a
    return-object v2

    :cond_4b
    const/4 v2, 0x0

    goto :goto_4a
.end method

.method protected getPrefix(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    .line 92
    iget-object v1, p0, Lpxb/android/axmlLP/DumpAdapter$DumpNodeAdapter;->nses:Ljava/util/Map;

    if-eqz v1, :cond_f

    .line 93
    iget-object v1, p0, Lpxb/android/axmlLP/DumpAdapter$DumpNodeAdapter;->nses:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 94
    .local v0, "prefix":Ljava/lang/String;
    if-eqz v0, :cond_f

    .line 98
    .end local v0    # "prefix":Ljava/lang/String;
    :goto_e
    return-object v0

    :cond_f
    move-object v0, p1

    goto :goto_e
.end method

.method public text(ILjava/lang/String;)V
    .registers 6
    .param p1, "ln"    # I
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 103
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget v1, p0, Lpxb/android/axmlLP/DumpAdapter$DumpNodeAdapter;->deep:I

    add-int/lit8 v1, v1, 0x1

    if-ge v0, v1, :cond_11

    .line 104
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "  "

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 103
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 106
    :cond_11
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v1, p2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 107
    invoke-super {p0, p1, p2}, Lpxb/android/axmlLP/AxmlVisitor$NodeVisitor;->text(ILjava/lang/String;)V

    .line 108
    return-void
.end method
