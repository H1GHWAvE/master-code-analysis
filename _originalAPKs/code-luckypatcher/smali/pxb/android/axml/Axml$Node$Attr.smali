.class public Lpxb/android/axml/Axml$Node$Attr;
.super Ljava/lang/Object;
.source "Axml.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lpxb/android/axml/Axml$Node;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Attr"
.end annotation


# instance fields
.field public name:Ljava/lang/String;

.field public ns:Ljava/lang/String;

.field public resourceId:I

.field public type:I

.field public value:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public accept(Lpxb/android/axml/NodeVisitor;)V
    .registers 8
    .param p1, "nodeVisitor"    # Lpxb/android/axml/NodeVisitor;

    .prologue
    .line 30
    iget-object v1, p0, Lpxb/android/axml/Axml$Node$Attr;->ns:Ljava/lang/String;

    iget-object v2, p0, Lpxb/android/axml/Axml$Node$Attr;->name:Ljava/lang/String;

    iget v3, p0, Lpxb/android/axml/Axml$Node$Attr;->resourceId:I

    iget v4, p0, Lpxb/android/axml/Axml$Node$Attr;->type:I

    iget-object v5, p0, Lpxb/android/axml/Axml$Node$Attr;->value:Ljava/lang/Object;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Lpxb/android/axml/NodeVisitor;->attr(Ljava/lang/String;Ljava/lang/String;IILjava/lang/Object;)V

    .line 31
    return-void
.end method
