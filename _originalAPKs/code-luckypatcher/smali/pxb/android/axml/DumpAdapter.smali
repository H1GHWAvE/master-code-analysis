.class public Lpxb/android/axml/DumpAdapter;
.super Lpxb/android/axml/AxmlVisitor;
.source "DumpAdapter.java"


# instance fields
.field protected deep:I

.field protected nses:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 31
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lpxb/android/axml/DumpAdapter;-><init>(Lpxb/android/axml/NodeVisitor;)V

    .line 32
    return-void
.end method

.method public constructor <init>(Lpxb/android/axml/NodeVisitor;)V
    .registers 4
    .param p1, "nv"    # Lpxb/android/axml/NodeVisitor;

    .prologue
    .line 35
    const/4 v0, 0x0

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-direct {p0, p1, v0, v1}, Lpxb/android/axml/DumpAdapter;-><init>(Lpxb/android/axml/NodeVisitor;ILjava/util/Map;)V

    .line 36
    return-void
.end method

.method public constructor <init>(Lpxb/android/axml/NodeVisitor;ILjava/util/Map;)V
    .registers 4
    .param p1, "nv"    # Lpxb/android/axml/NodeVisitor;
    .param p2, "x"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lpxb/android/axml/NodeVisitor;",
            "I",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 39
    .local p3, "nses":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0, p1}, Lpxb/android/axml/AxmlVisitor;-><init>(Lpxb/android/axml/NodeVisitor;)V

    .line 40
    iput p2, p0, Lpxb/android/axml/DumpAdapter;->deep:I

    .line 41
    iput-object p3, p0, Lpxb/android/axml/DumpAdapter;->nses:Ljava/util/Map;

    .line 42
    return-void
.end method


# virtual methods
.method public attr(Ljava/lang/String;Ljava/lang/String;IILjava/lang/Object;)V
    .registers 15
    .param p1, "ns"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "resourceId"    # I
    .param p4, "type"    # I
    .param p5, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 46
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_4
    iget v2, p0, Lpxb/android/axml/DumpAdapter;->deep:I

    if-ge v0, v2, :cond_12

    .line 47
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "  "

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 46
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 49
    :cond_12
    if-eqz p1, :cond_27

    .line 50
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "%s:"

    new-array v4, v6, [Ljava/lang/Object;

    invoke-virtual {p0, p1}, Lpxb/android/axml/DumpAdapter;->getPrefix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 52
    :cond_27
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v2, p2}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 53
    const/4 v2, -0x1

    if-eq p3, v2, :cond_42

    .line 54
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "(%08x)"

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 56
    :cond_42
    instance-of v2, p5, Ljava/lang/String;

    if-eqz v2, :cond_64

    .line 57
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "=[%08x]\"%s\""

    new-array v4, v8, [Ljava/lang/Object;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    aput-object p5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 68
    :goto_5b
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v2}, Ljava/io/PrintStream;->println()V

    .line 69
    invoke-super/range {p0 .. p5}, Lpxb/android/axml/AxmlVisitor;->attr(Ljava/lang/String;Ljava/lang/String;IILjava/lang/Object;)V

    .line 70
    return-void

    .line 58
    :cond_64
    instance-of v2, p5, Ljava/lang/Boolean;

    if-eqz v2, :cond_7e

    .line 59
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "=[%08x]\"%b\""

    new-array v4, v8, [Ljava/lang/Object;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    aput-object p5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    goto :goto_5b

    .line 60
    :cond_7e
    instance-of v2, p5, Lpxb/android/axml/ValueWrapper;

    if-eqz v2, :cond_a6

    move-object v1, p5

    .line 61
    check-cast v1, Lpxb/android/axml/ValueWrapper;

    .line 62
    .local v1, "w":Lpxb/android/axml/ValueWrapper;
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "=[%08x]@%08x, raw: \"%s\""

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    iget v5, v1, Lpxb/android/axml/ValueWrapper;->ref:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    iget-object v5, v1, Lpxb/android/axml/ValueWrapper;->raw:Ljava/lang/String;

    aput-object v5, v4, v8

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    goto :goto_5b

    .line 63
    .end local v1    # "w":Lpxb/android/axml/ValueWrapper;
    :cond_a6
    if-ne p4, v6, :cond_be

    .line 64
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "=[%08x]@%08x"

    new-array v4, v8, [Ljava/lang/Object;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    aput-object p5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    goto :goto_5b

    .line 66
    :cond_be
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "=[%08x]%08x"

    new-array v4, v8, [Ljava/lang/Object;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    aput-object p5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    goto :goto_5b
.end method

.method public child(Ljava/lang/String;Ljava/lang/String;)Lpxb/android/axml/NodeVisitor;
    .registers 8
    .param p1, "ns"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 74
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget v2, p0, Lpxb/android/axml/DumpAdapter;->deep:I

    if-ge v0, v2, :cond_f

    .line 75
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "  "

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 74
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 77
    :cond_f
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "<"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 78
    if-eqz p1, :cond_34

    .line 79
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, p1}, Lpxb/android/axml/DumpAdapter;->getPrefix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 81
    :cond_34
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v2, p2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 82
    invoke-super {p0, p1, p2}, Lpxb/android/axml/AxmlVisitor;->child(Ljava/lang/String;Ljava/lang/String;)Lpxb/android/axml/NodeVisitor;

    move-result-object v1

    .line 83
    .local v1, "nv":Lpxb/android/axml/NodeVisitor;
    if-eqz v1, :cond_4b

    .line 84
    new-instance v2, Lpxb/android/axml/DumpAdapter;

    iget v3, p0, Lpxb/android/axml/DumpAdapter;->deep:I

    add-int/lit8 v3, v3, 0x1

    iget-object v4, p0, Lpxb/android/axml/DumpAdapter;->nses:Ljava/util/Map;

    invoke-direct {v2, v1, v3, v4}, Lpxb/android/axml/DumpAdapter;-><init>(Lpxb/android/axml/NodeVisitor;ILjava/util/Map;)V

    .line 86
    :goto_4a
    return-object v2

    :cond_4b
    const/4 v2, 0x0

    goto :goto_4a
.end method

.method protected getPrefix(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    .line 90
    iget-object v1, p0, Lpxb/android/axml/DumpAdapter;->nses:Ljava/util/Map;

    if-eqz v1, :cond_f

    .line 91
    iget-object v1, p0, Lpxb/android/axml/DumpAdapter;->nses:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 92
    .local v0, "prefix":Ljava/lang/String;
    if-eqz v0, :cond_f

    .line 96
    .end local v0    # "prefix":Ljava/lang/String;
    :goto_e
    return-object v0

    :cond_f
    move-object v0, p1

    goto :goto_e
.end method

.method public ns(Ljava/lang/String;Ljava/lang/String;I)V
    .registers 7
    .param p1, "prefix"    # Ljava/lang/String;
    .param p2, "uri"    # Ljava/lang/String;
    .param p3, "ln"    # I

    .prologue
    .line 101
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 102
    iget-object v0, p0, Lpxb/android/axml/DumpAdapter;->nses:Ljava/util/Map;

    invoke-interface {v0, p2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    invoke-super {p0, p1, p2, p3}, Lpxb/android/axml/AxmlVisitor;->ns(Ljava/lang/String;Ljava/lang/String;I)V

    .line 104
    return-void
.end method

.method public text(ILjava/lang/String;)V
    .registers 6
    .param p1, "ln"    # I
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 108
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget v1, p0, Lpxb/android/axml/DumpAdapter;->deep:I

    add-int/lit8 v1, v1, 0x1

    if-ge v0, v1, :cond_11

    .line 109
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "  "

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 108
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 111
    :cond_11
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "T: "

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 112
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v1, p2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 113
    invoke-super {p0, p1, p2}, Lpxb/android/axml/AxmlVisitor;->text(ILjava/lang/String;)V

    .line 114
    return-void
.end method
