.class public Lpxb/android/axml/AxmlParser;
.super Ljava/lang/Object;
.source "AxmlParser.java"

# interfaces
.implements Lpxb/android/ResConst;


# static fields
.field public static final END_FILE:I = 0x7

.field public static final END_NS:I = 0x5

.field public static final END_TAG:I = 0x3

.field public static final START_FILE:I = 0x1

.field public static final START_NS:I = 0x4

.field public static final START_TAG:I = 0x2

.field public static final TEXT:I = 0x6


# instance fields
.field private attributeCount:I

.field private attrs:Ljava/nio/IntBuffer;

.field private classAttribute:I

.field private fileSize:I

.field private idAttribute:I

.field private in:Ljava/nio/ByteBuffer;

.field private lineNumber:I

.field private nameIdx:I

.field private nsIdx:I

.field private prefixIdx:I

.field private resourceIds:[I

.field private strings:[Ljava/lang/String;

.field private styleAttribute:I

.field private textIdx:I


# direct methods
.method public constructor <init>(Ljava/nio/ByteBuffer;)V
    .registers 3
    .param p1, "in"    # Ljava/nio/ByteBuffer;

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    const/4 v0, -0x1

    iput v0, p0, Lpxb/android/axml/AxmlParser;->fileSize:I

    .line 77
    sget-object v0, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lpxb/android/axml/AxmlParser;->in:Ljava/nio/ByteBuffer;

    .line 78
    return-void
.end method

.method public constructor <init>([B)V
    .registers 3
    .param p1, "data"    # [B

    .prologue
    .line 72
    invoke-static {p1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-direct {p0, v0}, Lpxb/android/axml/AxmlParser;-><init>(Ljava/nio/ByteBuffer;)V

    .line 73
    return-void
.end method


# virtual methods
.method public getAttrCount()I
    .registers 2

    .prologue
    .line 81
    iget v0, p0, Lpxb/android/axml/AxmlParser;->attributeCount:I

    return v0
.end method

.method public getAttrName(I)Ljava/lang/String;
    .registers 5
    .param p1, "i"    # I

    .prologue
    .line 89
    iget-object v1, p0, Lpxb/android/axml/AxmlParser;->attrs:Ljava/nio/IntBuffer;

    mul-int/lit8 v2, p1, 0x5

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/nio/IntBuffer;->get(I)I

    move-result v0

    .line 90
    .local v0, "idx":I
    iget-object v1, p0, Lpxb/android/axml/AxmlParser;->strings:[Ljava/lang/String;

    aget-object v1, v1, v0

    return-object v1
.end method

.method public getAttrNs(I)Ljava/lang/String;
    .registers 5
    .param p1, "i"    # I

    .prologue
    .line 95
    iget-object v1, p0, Lpxb/android/axml/AxmlParser;->attrs:Ljava/nio/IntBuffer;

    mul-int/lit8 v2, p1, 0x5

    add-int/lit8 v2, v2, 0x0

    invoke-virtual {v1, v2}, Ljava/nio/IntBuffer;->get(I)I

    move-result v0

    .line 96
    .local v0, "idx":I
    if-ltz v0, :cond_11

    iget-object v1, p0, Lpxb/android/axml/AxmlParser;->strings:[Ljava/lang/String;

    aget-object v1, v1, v0

    :goto_10
    return-object v1

    :cond_11
    const/4 v1, 0x0

    goto :goto_10
.end method

.method getAttrRawString(I)Ljava/lang/String;
    .registers 5
    .param p1, "i"    # I

    .prologue
    .line 100
    iget-object v1, p0, Lpxb/android/axml/AxmlParser;->attrs:Ljava/nio/IntBuffer;

    mul-int/lit8 v2, p1, 0x5

    add-int/lit8 v2, v2, 0x2

    invoke-virtual {v1, v2}, Ljava/nio/IntBuffer;->get(I)I

    move-result v0

    .line 101
    .local v0, "idx":I
    if-ltz v0, :cond_11

    .line 102
    iget-object v1, p0, Lpxb/android/axml/AxmlParser;->strings:[Ljava/lang/String;

    aget-object v1, v1, v0

    .line 104
    :goto_10
    return-object v1

    :cond_11
    const/4 v1, 0x0

    goto :goto_10
.end method

.method public getAttrResId(I)I
    .registers 5
    .param p1, "i"    # I

    .prologue
    .line 108
    iget-object v1, p0, Lpxb/android/axml/AxmlParser;->resourceIds:[I

    if-eqz v1, :cond_1a

    .line 109
    iget-object v1, p0, Lpxb/android/axml/AxmlParser;->attrs:Ljava/nio/IntBuffer;

    mul-int/lit8 v2, p1, 0x5

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/nio/IntBuffer;->get(I)I

    move-result v0

    .line 110
    .local v0, "idx":I
    if-ltz v0, :cond_1a

    iget-object v1, p0, Lpxb/android/axml/AxmlParser;->resourceIds:[I

    array-length v1, v1

    if-ge v0, v1, :cond_1a

    .line 111
    iget-object v1, p0, Lpxb/android/axml/AxmlParser;->resourceIds:[I

    aget v1, v1, v0

    .line 114
    .end local v0    # "idx":I
    :goto_19
    return v1

    :cond_1a
    const/4 v1, -0x1

    goto :goto_19
.end method

.method public getAttrType(I)I
    .registers 4
    .param p1, "i"    # I

    .prologue
    .line 118
    iget-object v0, p0, Lpxb/android/axml/AxmlParser;->attrs:Ljava/nio/IntBuffer;

    mul-int/lit8 v1, p1, 0x5

    add-int/lit8 v1, v1, 0x3

    invoke-virtual {v0, v1}, Ljava/nio/IntBuffer;->get(I)I

    move-result v0

    shr-int/lit8 v0, v0, 0x18

    return v0
.end method

.method public getAttrValue(I)Ljava/lang/Object;
    .registers 5
    .param p1, "i"    # I

    .prologue
    .line 122
    iget-object v1, p0, Lpxb/android/axml/AxmlParser;->attrs:Ljava/nio/IntBuffer;

    mul-int/lit8 v2, p1, 0x5

    add-int/lit8 v2, v2, 0x4

    invoke-virtual {v1, v2}, Ljava/nio/IntBuffer;->get(I)I

    move-result v0

    .line 124
    .local v0, "v":I
    iget v1, p0, Lpxb/android/axml/AxmlParser;->idAttribute:I

    if-ne p1, v1, :cond_17

    .line 125
    invoke-virtual {p0, p1}, Lpxb/android/axml/AxmlParser;->getAttrRawString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lpxb/android/axml/ValueWrapper;->wrapId(ILjava/lang/String;)Lpxb/android/axml/ValueWrapper;

    move-result-object v1

    .line 138
    :goto_16
    return-object v1

    .line 126
    :cond_17
    iget v1, p0, Lpxb/android/axml/AxmlParser;->styleAttribute:I

    if-ne p1, v1, :cond_24

    .line 127
    invoke-virtual {p0, p1}, Lpxb/android/axml/AxmlParser;->getAttrRawString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lpxb/android/axml/ValueWrapper;->wrapStyle(ILjava/lang/String;)Lpxb/android/axml/ValueWrapper;

    move-result-object v1

    goto :goto_16

    .line 128
    :cond_24
    iget v1, p0, Lpxb/android/axml/AxmlParser;->classAttribute:I

    if-ne p1, v1, :cond_31

    .line 129
    invoke-virtual {p0, p1}, Lpxb/android/axml/AxmlParser;->getAttrRawString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lpxb/android/axml/ValueWrapper;->wrapClass(ILjava/lang/String;)Lpxb/android/axml/ValueWrapper;

    move-result-object v1

    goto :goto_16

    .line 132
    :cond_31
    invoke-virtual {p0, p1}, Lpxb/android/axml/AxmlParser;->getAttrType(I)I

    move-result v1

    sparse-switch v1, :sswitch_data_4c

    .line 138
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_16

    .line 134
    :sswitch_3d
    iget-object v1, p0, Lpxb/android/axml/AxmlParser;->strings:[Ljava/lang/String;

    aget-object v1, v1, v0

    goto :goto_16

    .line 136
    :sswitch_42
    if-eqz v0, :cond_4a

    const/4 v1, 0x1

    :goto_45
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_16

    :cond_4a
    const/4 v1, 0x0

    goto :goto_45

    .line 132
    :sswitch_data_4c
    .sparse-switch
        0x3 -> :sswitch_3d
        0x12 -> :sswitch_42
    .end sparse-switch
.end method

.method public getAttributeCount()I
    .registers 2

    .prologue
    .line 85
    iget v0, p0, Lpxb/android/axml/AxmlParser;->attributeCount:I

    return v0
.end method

.method public getLineNumber()I
    .registers 2

    .prologue
    .line 143
    iget v0, p0, Lpxb/android/axml/AxmlParser;->lineNumber:I

    return v0
.end method

.method public getName()Ljava/lang/String;
    .registers 3

    .prologue
    .line 147
    iget-object v0, p0, Lpxb/android/axml/AxmlParser;->strings:[Ljava/lang/String;

    iget v1, p0, Lpxb/android/axml/AxmlParser;->nameIdx:I

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getNamespacePrefix()Ljava/lang/String;
    .registers 3

    .prologue
    .line 151
    iget-object v0, p0, Lpxb/android/axml/AxmlParser;->strings:[Ljava/lang/String;

    iget v1, p0, Lpxb/android/axml/AxmlParser;->prefixIdx:I

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getNamespaceUri()Ljava/lang/String;
    .registers 3

    .prologue
    .line 155
    iget v0, p0, Lpxb/android/axml/AxmlParser;->nsIdx:I

    if-ltz v0, :cond_b

    iget-object v0, p0, Lpxb/android/axml/AxmlParser;->strings:[Ljava/lang/String;

    iget v1, p0, Lpxb/android/axml/AxmlParser;->nsIdx:I

    aget-object v0, v0, v1

    :goto_a
    return-object v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public getText()Ljava/lang/String;
    .registers 3

    .prologue
    .line 159
    iget-object v0, p0, Lpxb/android/axml/AxmlParser;->strings:[Ljava/lang/String;

    iget v1, p0, Lpxb/android/axml/AxmlParser;->textIdx:I

    aget-object v0, v0, v1

    return-object v0
.end method

.method public next()I
    .registers 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const v9, 0xffff

    .line 163
    iget v7, p0, Lpxb/android/axml/AxmlParser;->fileSize:I

    if-gez v7, :cond_22

    .line 164
    iget-object v7, p0, Lpxb/android/axml/AxmlParser;->in:Ljava/nio/ByteBuffer;

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v7

    and-int v6, v7, v9

    .line 165
    .local v6, "type":I
    const/4 v7, 0x3

    if-eq v6, v7, :cond_18

    .line 166
    new-instance v7, Ljava/lang/RuntimeException;

    invoke-direct {v7}, Ljava/lang/RuntimeException;-><init>()V

    throw v7

    .line 168
    :cond_18
    iget-object v7, p0, Lpxb/android/axml/AxmlParser;->in:Ljava/nio/ByteBuffer;

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v7

    iput v7, p0, Lpxb/android/axml/AxmlParser;->fileSize:I

    .line 169
    const/4 v1, 0x1

    .line 272
    .end local v6    # "type":I
    :goto_21
    return v1

    .line 171
    :cond_22
    const/4 v1, -0x1

    .line 172
    .local v1, "event":I
    iget-object v7, p0, Lpxb/android/axml/AxmlParser;->in:Ljava/nio/ByteBuffer;

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->position()I

    move-result v4

    .local v4, "p":I
    :goto_29
    iget v7, p0, Lpxb/android/axml/AxmlParser;->fileSize:I

    if-ge v4, v7, :cond_138

    .line 173
    iget-object v7, p0, Lpxb/android/axml/AxmlParser;->in:Ljava/nio/ByteBuffer;

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v7

    and-int v6, v7, v9

    .line 174
    .restart local v6    # "type":I
    iget-object v7, p0, Lpxb/android/axml/AxmlParser;->in:Ljava/nio/ByteBuffer;

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v5

    .line 175
    .local v5, "size":I
    sparse-switch v6, :sswitch_data_13c

    .line 267
    new-instance v7, Ljava/lang/RuntimeException;

    invoke-direct {v7}, Ljava/lang/RuntimeException;-><init>()V

    throw v7

    .line 178
    :sswitch_44
    iget-object v7, p0, Lpxb/android/axml/AxmlParser;->in:Ljava/nio/ByteBuffer;

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v7

    iput v7, p0, Lpxb/android/axml/AxmlParser;->lineNumber:I

    .line 179
    iget-object v7, p0, Lpxb/android/axml/AxmlParser;->in:Ljava/nio/ByteBuffer;

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->getInt()I

    .line 180
    iget-object v7, p0, Lpxb/android/axml/AxmlParser;->in:Ljava/nio/ByteBuffer;

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v7

    iput v7, p0, Lpxb/android/axml/AxmlParser;->nsIdx:I

    .line 181
    iget-object v7, p0, Lpxb/android/axml/AxmlParser;->in:Ljava/nio/ByteBuffer;

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v7

    iput v7, p0, Lpxb/android/axml/AxmlParser;->nameIdx:I

    .line 182
    iget-object v7, p0, Lpxb/android/axml/AxmlParser;->in:Ljava/nio/ByteBuffer;

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v2

    .line 183
    .local v2, "flag":I
    const v7, 0x140014

    if-eq v2, v7, :cond_72

    .line 184
    new-instance v7, Ljava/lang/RuntimeException;

    invoke-direct {v7}, Ljava/lang/RuntimeException;-><init>()V

    throw v7

    .line 188
    :cond_72
    iget-object v7, p0, Lpxb/android/axml/AxmlParser;->in:Ljava/nio/ByteBuffer;

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v7

    and-int/2addr v7, v9

    iput v7, p0, Lpxb/android/axml/AxmlParser;->attributeCount:I

    .line 189
    iget-object v7, p0, Lpxb/android/axml/AxmlParser;->in:Ljava/nio/ByteBuffer;

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v7

    and-int/2addr v7, v9

    add-int/lit8 v7, v7, -0x1

    iput v7, p0, Lpxb/android/axml/AxmlParser;->idAttribute:I

    .line 190
    iget-object v7, p0, Lpxb/android/axml/AxmlParser;->in:Ljava/nio/ByteBuffer;

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v7

    and-int/2addr v7, v9

    add-int/lit8 v7, v7, -0x1

    iput v7, p0, Lpxb/android/axml/AxmlParser;->classAttribute:I

    .line 191
    iget-object v7, p0, Lpxb/android/axml/AxmlParser;->in:Ljava/nio/ByteBuffer;

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v7

    and-int/2addr v7, v9

    add-int/lit8 v7, v7, -0x1

    iput v7, p0, Lpxb/android/axml/AxmlParser;->styleAttribute:I

    .line 193
    iget-object v7, p0, Lpxb/android/axml/AxmlParser;->in:Ljava/nio/ByteBuffer;

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->asIntBuffer()Ljava/nio/IntBuffer;

    move-result-object v7

    iput-object v7, p0, Lpxb/android/axml/AxmlParser;->attrs:Ljava/nio/IntBuffer;

    .line 225
    const/4 v1, 0x2

    .line 269
    .end local v2    # "flag":I
    :goto_a5
    iget-object v7, p0, Lpxb/android/axml/AxmlParser;->in:Ljava/nio/ByteBuffer;

    add-int v8, v4, v5

    invoke-virtual {v7, v8}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    goto/16 :goto_21

    .line 229
    :sswitch_ae
    iget-object v7, p0, Lpxb/android/axml/AxmlParser;->in:Ljava/nio/ByteBuffer;

    add-int v8, v4, v5

    invoke-virtual {v7, v8}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 230
    const/4 v1, 0x3

    .line 232
    goto :goto_a5

    .line 234
    :sswitch_b7
    iget-object v7, p0, Lpxb/android/axml/AxmlParser;->in:Ljava/nio/ByteBuffer;

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v7

    iput v7, p0, Lpxb/android/axml/AxmlParser;->lineNumber:I

    .line 235
    iget-object v7, p0, Lpxb/android/axml/AxmlParser;->in:Ljava/nio/ByteBuffer;

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->getInt()I

    .line 236
    iget-object v7, p0, Lpxb/android/axml/AxmlParser;->in:Ljava/nio/ByteBuffer;

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v7

    iput v7, p0, Lpxb/android/axml/AxmlParser;->prefixIdx:I

    .line 237
    iget-object v7, p0, Lpxb/android/axml/AxmlParser;->in:Ljava/nio/ByteBuffer;

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v7

    iput v7, p0, Lpxb/android/axml/AxmlParser;->nsIdx:I

    .line 238
    const/4 v1, 0x4

    .line 239
    goto :goto_a5

    .line 241
    :sswitch_d6
    iget-object v7, p0, Lpxb/android/axml/AxmlParser;->in:Ljava/nio/ByteBuffer;

    add-int v8, v4, v5

    invoke-virtual {v7, v8}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 242
    const/4 v1, 0x5

    .line 243
    goto :goto_a5

    .line 245
    :sswitch_df
    iget-object v7, p0, Lpxb/android/axml/AxmlParser;->in:Ljava/nio/ByteBuffer;

    invoke-static {v7}, Lpxb/android/StringItems;->read(Ljava/nio/ByteBuffer;)[Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lpxb/android/axml/AxmlParser;->strings:[Ljava/lang/String;

    .line 246
    iget-object v7, p0, Lpxb/android/axml/AxmlParser;->in:Ljava/nio/ByteBuffer;

    add-int v8, v4, v5

    invoke-virtual {v7, v8}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 172
    :goto_ee
    iget-object v7, p0, Lpxb/android/axml/AxmlParser;->in:Ljava/nio/ByteBuffer;

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->position()I

    move-result v4

    goto/16 :goto_29

    .line 249
    :sswitch_f6
    div-int/lit8 v7, v5, 0x4

    add-int/lit8 v0, v7, -0x2

    .line 250
    .local v0, "count":I
    new-array v7, v0, [I

    iput-object v7, p0, Lpxb/android/axml/AxmlParser;->resourceIds:[I

    .line 251
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_ff
    if-ge v3, v0, :cond_10e

    .line 252
    iget-object v7, p0, Lpxb/android/axml/AxmlParser;->resourceIds:[I

    iget-object v8, p0, Lpxb/android/axml/AxmlParser;->in:Ljava/nio/ByteBuffer;

    invoke-virtual {v8}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v8

    aput v8, v7, v3

    .line 251
    add-int/lit8 v3, v3, 0x1

    goto :goto_ff

    .line 254
    :cond_10e
    iget-object v7, p0, Lpxb/android/axml/AxmlParser;->in:Ljava/nio/ByteBuffer;

    add-int v8, v4, v5

    invoke-virtual {v7, v8}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    goto :goto_ee

    .line 257
    .end local v0    # "count":I
    .end local v3    # "i":I
    :sswitch_116
    iget-object v7, p0, Lpxb/android/axml/AxmlParser;->in:Ljava/nio/ByteBuffer;

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v7

    iput v7, p0, Lpxb/android/axml/AxmlParser;->lineNumber:I

    .line 258
    iget-object v7, p0, Lpxb/android/axml/AxmlParser;->in:Ljava/nio/ByteBuffer;

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->getInt()I

    .line 259
    iget-object v7, p0, Lpxb/android/axml/AxmlParser;->in:Ljava/nio/ByteBuffer;

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v7

    iput v7, p0, Lpxb/android/axml/AxmlParser;->textIdx:I

    .line 261
    iget-object v7, p0, Lpxb/android/axml/AxmlParser;->in:Ljava/nio/ByteBuffer;

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->getInt()I

    .line 262
    iget-object v7, p0, Lpxb/android/axml/AxmlParser;->in:Ljava/nio/ByteBuffer;

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->getInt()I

    .line 264
    const/4 v1, 0x6

    .line 265
    goto/16 :goto_a5

    .line 272
    .end local v5    # "size":I
    .end local v6    # "type":I
    :cond_138
    const/4 v1, 0x7

    goto/16 :goto_21

    .line 175
    nop

    :sswitch_data_13c
    .sparse-switch
        0x1 -> :sswitch_df
        0x100 -> :sswitch_b7
        0x101 -> :sswitch_d6
        0x102 -> :sswitch_44
        0x103 -> :sswitch_ae
        0x104 -> :sswitch_116
        0x180 -> :sswitch_f6
    .end sparse-switch
.end method
