.class final Lpxb/android/axml/AxmlWriter$1;
.super Ljava/lang/Object;
.source "AxmlWriter.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lpxb/android/axml/AxmlWriter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lpxb/android/axml/AxmlWriter$Attr;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 4
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 48
    check-cast p1, Lpxb/android/axml/AxmlWriter$Attr;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lpxb/android/axml/AxmlWriter$Attr;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lpxb/android/axml/AxmlWriter$1;->compare(Lpxb/android/axml/AxmlWriter$Attr;Lpxb/android/axml/AxmlWriter$Attr;)I

    move-result v0

    return v0
.end method

.method public compare(Lpxb/android/axml/AxmlWriter$Attr;Lpxb/android/axml/AxmlWriter$Attr;)I
    .registers 10
    .param p1, "a"    # Lpxb/android/axml/AxmlWriter$Attr;
    .param p2, "b"    # Lpxb/android/axml/AxmlWriter$Attr;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 52
    iget v5, p1, Lpxb/android/axml/AxmlWriter$Attr;->resourceId:I

    iget v6, p2, Lpxb/android/axml/AxmlWriter$Attr;->resourceId:I

    sub-int v2, v5, v6

    .line 53
    .local v2, "x":I
    if-nez v2, :cond_27

    .line 54
    iget-object v5, p1, Lpxb/android/axml/AxmlWriter$Attr;->name:Lpxb/android/StringItem;

    iget-object v5, v5, Lpxb/android/StringItem;->data:Ljava/lang/String;

    iget-object v6, p2, Lpxb/android/axml/AxmlWriter$Attr;->name:Lpxb/android/StringItem;

    iget-object v6, v6, Lpxb/android/StringItem;->data:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    .line 55
    if-nez v2, :cond_27

    .line 56
    iget-object v5, p1, Lpxb/android/axml/AxmlWriter$Attr;->ns:Lpxb/android/StringItem;

    if-nez v5, :cond_28

    move v0, v3

    .line 57
    .local v0, "aNsIsnull":Z
    :goto_1d
    iget-object v5, p2, Lpxb/android/axml/AxmlWriter$Attr;->ns:Lpxb/android/StringItem;

    if-nez v5, :cond_2a

    move v1, v3

    .line 58
    .local v1, "bNsIsnull":Z
    :goto_22
    if-eqz v0, :cond_2e

    .line 59
    if-eqz v1, :cond_2c

    .line 60
    const/4 v2, 0x0

    .line 74
    .end local v0    # "aNsIsnull":Z
    .end local v1    # "bNsIsnull":Z
    :cond_27
    :goto_27
    return v2

    :cond_28
    move v0, v4

    .line 56
    goto :goto_1d

    .restart local v0    # "aNsIsnull":Z
    :cond_2a
    move v1, v4

    .line 57
    goto :goto_22

    .line 62
    .restart local v1    # "bNsIsnull":Z
    :cond_2c
    const/4 v2, -0x1

    goto :goto_27

    .line 65
    :cond_2e
    if-eqz v1, :cond_32

    .line 66
    const/4 v2, 0x1

    goto :goto_27

    .line 68
    :cond_32
    iget-object v3, p1, Lpxb/android/axml/AxmlWriter$Attr;->ns:Lpxb/android/StringItem;

    iget-object v3, v3, Lpxb/android/StringItem;->data:Ljava/lang/String;

    iget-object v4, p2, Lpxb/android/axml/AxmlWriter$Attr;->ns:Lpxb/android/StringItem;

    iget-object v4, v4, Lpxb/android/StringItem;->data:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    goto :goto_27
.end method
