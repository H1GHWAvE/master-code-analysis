.class Lpxb/android/axml/AxmlWriter$Attr;
.super Ljava/lang/Object;
.source "AxmlWriter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lpxb/android/axml/AxmlWriter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Attr"
.end annotation


# instance fields
.field public index:I

.field public name:Lpxb/android/StringItem;

.field public ns:Lpxb/android/StringItem;

.field public raw:Lpxb/android/StringItem;

.field public resourceId:I

.field public type:I

.field public value:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lpxb/android/StringItem;Lpxb/android/StringItem;I)V
    .registers 4
    .param p1, "ns"    # Lpxb/android/StringItem;
    .param p2, "name"    # Lpxb/android/StringItem;
    .param p3, "resourceId"    # I

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    iput-object p1, p0, Lpxb/android/axml/AxmlWriter$Attr;->ns:Lpxb/android/StringItem;

    .line 91
    iput-object p2, p0, Lpxb/android/axml/AxmlWriter$Attr;->name:Lpxb/android/StringItem;

    .line 92
    iput p3, p0, Lpxb/android/axml/AxmlWriter$Attr;->resourceId:I

    .line 93
    return-void
.end method


# virtual methods
.method public prepare(Lpxb/android/axml/AxmlWriter;)V
    .registers 4
    .param p1, "axmlWriter"    # Lpxb/android/axml/AxmlWriter;

    .prologue
    .line 96
    iget-object v0, p0, Lpxb/android/axml/AxmlWriter$Attr;->ns:Lpxb/android/StringItem;

    invoke-virtual {p1, v0}, Lpxb/android/axml/AxmlWriter;->updateNs(Lpxb/android/StringItem;)Lpxb/android/StringItem;

    move-result-object v0

    iput-object v0, p0, Lpxb/android/axml/AxmlWriter$Attr;->ns:Lpxb/android/StringItem;

    .line 97
    iget-object v0, p0, Lpxb/android/axml/AxmlWriter$Attr;->name:Lpxb/android/StringItem;

    if-eqz v0, :cond_1b

    .line 98
    iget v0, p0, Lpxb/android/axml/AxmlWriter$Attr;->resourceId:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_38

    .line 99
    iget-object v0, p0, Lpxb/android/axml/AxmlWriter$Attr;->name:Lpxb/android/StringItem;

    iget v1, p0, Lpxb/android/axml/AxmlWriter$Attr;->resourceId:I

    invoke-virtual {p1, v0, v1}, Lpxb/android/axml/AxmlWriter;->updateWithResourceId(Lpxb/android/StringItem;I)Lpxb/android/StringItem;

    move-result-object v0

    iput-object v0, p0, Lpxb/android/axml/AxmlWriter$Attr;->name:Lpxb/android/StringItem;

    .line 104
    :cond_1b
    :goto_1b
    iget-object v0, p0, Lpxb/android/axml/AxmlWriter$Attr;->value:Ljava/lang/Object;

    instance-of v0, v0, Lpxb/android/StringItem;

    if-eqz v0, :cond_2b

    .line 105
    iget-object v0, p0, Lpxb/android/axml/AxmlWriter$Attr;->value:Ljava/lang/Object;

    check-cast v0, Lpxb/android/StringItem;

    invoke-virtual {p1, v0}, Lpxb/android/axml/AxmlWriter;->update(Lpxb/android/StringItem;)Lpxb/android/StringItem;

    move-result-object v0

    iput-object v0, p0, Lpxb/android/axml/AxmlWriter$Attr;->value:Ljava/lang/Object;

    .line 107
    :cond_2b
    iget-object v0, p0, Lpxb/android/axml/AxmlWriter$Attr;->raw:Lpxb/android/StringItem;

    if-eqz v0, :cond_37

    .line 108
    iget-object v0, p0, Lpxb/android/axml/AxmlWriter$Attr;->raw:Lpxb/android/StringItem;

    invoke-virtual {p1, v0}, Lpxb/android/axml/AxmlWriter;->update(Lpxb/android/StringItem;)Lpxb/android/StringItem;

    move-result-object v0

    iput-object v0, p0, Lpxb/android/axml/AxmlWriter$Attr;->raw:Lpxb/android/StringItem;

    .line 110
    :cond_37
    return-void

    .line 101
    :cond_38
    iget-object v0, p0, Lpxb/android/axml/AxmlWriter$Attr;->name:Lpxb/android/StringItem;

    invoke-virtual {p1, v0}, Lpxb/android/axml/AxmlWriter;->update(Lpxb/android/StringItem;)Lpxb/android/StringItem;

    move-result-object v0

    iput-object v0, p0, Lpxb/android/axml/AxmlWriter$Attr;->name:Lpxb/android/StringItem;

    goto :goto_1b
.end method
