.class public Lpxb/android/arsc/BagValue;
.super Ljava/lang/Object;
.source "BagValue.java"


# instance fields
.field public map:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/util/Map$Entry",
            "<",
            "Ljava/lang/Integer;",
            "Lpxb/android/arsc/Value;",
            ">;>;"
        }
    .end annotation
.end field

.field public final parent:I


# direct methods
.method public constructor <init>(I)V
    .registers 3
    .param p1, "parent"    # I

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lpxb/android/arsc/BagValue;->map:Ljava/util/List;

    .line 28
    iput p1, p0, Lpxb/android/arsc/BagValue;->parent:I

    .line 29
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .registers 7
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 33
    if-ne p0, p1, :cond_5

    .line 47
    :cond_4
    :goto_4
    return v1

    .line 35
    :cond_5
    if-nez p1, :cond_9

    move v1, v2

    .line 36
    goto :goto_4

    .line 37
    :cond_9
    instance-of v3, p1, Lpxb/android/arsc/BagValue;

    if-nez v3, :cond_f

    move v1, v2

    .line 38
    goto :goto_4

    :cond_f
    move-object v0, p1

    .line 39
    check-cast v0, Lpxb/android/arsc/BagValue;

    .line 40
    .local v0, "other":Lpxb/android/arsc/BagValue;
    iget-object v3, p0, Lpxb/android/arsc/BagValue;->map:Ljava/util/List;

    if-nez v3, :cond_1c

    .line 41
    iget-object v3, v0, Lpxb/android/arsc/BagValue;->map:Ljava/util/List;

    if-eqz v3, :cond_28

    move v1, v2

    .line 42
    goto :goto_4

    .line 43
    :cond_1c
    iget-object v3, p0, Lpxb/android/arsc/BagValue;->map:Ljava/util/List;

    iget-object v4, v0, Lpxb/android/arsc/BagValue;->map:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_28

    move v1, v2

    .line 44
    goto :goto_4

    .line 45
    :cond_28
    iget v3, p0, Lpxb/android/arsc/BagValue;->parent:I

    iget v4, v0, Lpxb/android/arsc/BagValue;->parent:I

    if-eq v3, v4, :cond_4

    move v1, v2

    .line 46
    goto :goto_4
.end method

.method public hashCode()I
    .registers 5

    .prologue
    .line 52
    const/16 v0, 0x1f

    .line 53
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 54
    .local v1, "result":I
    iget-object v2, p0, Lpxb/android/arsc/BagValue;->map:Ljava/util/List;

    if-nez v2, :cond_11

    const/4 v2, 0x0

    :goto_8
    add-int/lit8 v1, v2, 0x1f

    .line 55
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lpxb/android/arsc/BagValue;->parent:I

    add-int v1, v2, v3

    .line 56
    return v1

    .line 54
    :cond_11
    iget-object v2, p0, Lpxb/android/arsc/BagValue;->map:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->hashCode()I

    move-result v2

    goto :goto_8
.end method

.method public toString()Ljava/lang/String;
    .registers 10

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 60
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 61
    .local v2, "sb":Ljava/lang/StringBuilder;
    const-string v3, "{bag%08x"

    new-array v4, v8, [Ljava/lang/Object;

    iget v5, p0, Lpxb/android/arsc/BagValue;->parent:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 62
    iget-object v3, p0, Lpxb/android/arsc/BagValue;->map:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_20
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_50

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 63
    .local v0, "e":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lpxb/android/arsc/Value;>;"
    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "0x%08x"

    new-array v5, v8, [Ljava/lang/Object;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64
    const-string v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 65
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_20

    .line 68
    .end local v0    # "e":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lpxb/android/arsc/Value;>;"
    :cond_50
    const-string v3, "}"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method
