.class public Lpxb/android/arsc/ArscWriter;
.super Ljava/lang/Object;
.source "ArscWriter.java"

# interfaces
.implements Lpxb/android/ResConst;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lpxb/android/arsc/ArscWriter$1;,
        Lpxb/android/arsc/ArscWriter$PkgCtx;
    }
.end annotation


# instance fields
.field private ctxs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lpxb/android/arsc/ArscWriter$PkgCtx;",
            ">;"
        }
    .end annotation
.end field

.field private pkgs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lpxb/android/arsc/Pkg;",
            ">;"
        }
    .end annotation
.end field

.field private strTable:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lpxb/android/StringItem;",
            ">;"
        }
    .end annotation
.end field

.field private strTable0:Lpxb/android/StringItems;


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lpxb/android/arsc/Pkg;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 85
    .local p1, "pkgs":Ljava/util/List;, "Ljava/util/List<Lpxb/android/arsc/Pkg;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lpxb/android/arsc/ArscWriter;->ctxs:Ljava/util/List;

    .line 82
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lpxb/android/arsc/ArscWriter;->strTable:Ljava/util/Map;

    .line 83
    new-instance v0, Lpxb/android/StringItems;

    invoke-direct {v0}, Lpxb/android/StringItems;-><init>()V

    iput-object v0, p0, Lpxb/android/arsc/ArscWriter;->strTable0:Lpxb/android/StringItems;

    .line 86
    iput-object p1, p0, Lpxb/android/arsc/ArscWriter;->pkgs:Ljava/util/List;

    .line 87
    return-void
.end method

.method private static varargs D(Ljava/lang/String;[Ljava/lang/Object;)V
    .registers 2
    .param p0, "fmt"    # Ljava/lang/String;
    .param p1, "args"    # [Ljava/lang/Object;

    .prologue
    .line 78
    return-void
.end method

.method private addString(Ljava/lang/String;)V
    .registers 4
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 103
    iget-object v1, p0, Lpxb/android/arsc/ArscWriter;->strTable:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 109
    :goto_8
    return-void

    .line 106
    :cond_9
    new-instance v0, Lpxb/android/StringItem;

    invoke-direct {v0, p1}, Lpxb/android/StringItem;-><init>(Ljava/lang/String;)V

    .line 107
    .local v0, "stringItem":Lpxb/android/StringItem;
    iget-object v1, p0, Lpxb/android/arsc/ArscWriter;->strTable:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    iget-object v1, p0, Lpxb/android/arsc/ArscWriter;->strTable0:Lpxb/android/StringItems;

    invoke-virtual {v1, v0}, Lpxb/android/StringItems;->add(Ljava/lang/Object;)Z

    goto :goto_8
.end method

.method private count()I
    .registers 20

    .prologue
    .line 113
    const/4 v13, 0x0

    .line 115
    .local v13, "size":I
    add-int/lit8 v13, v13, 0xc

    .line 117
    move-object/from16 v0, p0

    iget-object v0, v0, Lpxb/android/arsc/ArscWriter;->strTable0:Lpxb/android/StringItems;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lpxb/android/StringItems;->getSize()I

    move-result v15

    .line 118
    .local v15, "stringSize":I
    rem-int/lit8 v17, v15, 0x4

    if-eqz v17, :cond_17

    .line 119
    rem-int/lit8 v17, v15, 0x4

    rsub-int/lit8 v17, v17, 0x4

    add-int v15, v15, v17

    .line 121
    :cond_17
    add-int/lit8 v17, v15, 0x8

    add-int/lit8 v13, v17, 0xc

    .line 123
    move-object/from16 v0, p0

    iget-object v0, v0, Lpxb/android/arsc/ArscWriter;->ctxs:Ljava/util/List;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_25
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_144

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lpxb/android/arsc/ArscWriter$PkgCtx;

    .line 124
    .local v5, "ctx":Lpxb/android/arsc/ArscWriter$PkgCtx;
    iput v13, v5, Lpxb/android/arsc/ArscWriter$PkgCtx;->offset:I

    .line 125
    const/4 v12, 0x0

    .line 126
    .local v12, "pkgSize":I
    add-int/lit16 v12, v12, 0x10c

    .line 127
    add-int/lit8 v12, v12, 0x10

    .line 129
    iput v12, v5, Lpxb/android/arsc/ArscWriter$PkgCtx;->typeStringOff:I

    .line 131
    iget-object v0, v5, Lpxb/android/arsc/ArscWriter$PkgCtx;->typeNames0:Lpxb/android/StringItems;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lpxb/android/StringItems;->getSize()I

    move-result v15

    .line 132
    rem-int/lit8 v17, v15, 0x4

    if-eqz v17, :cond_4c

    .line 133
    rem-int/lit8 v17, v15, 0x4

    rsub-int/lit8 v17, v17, 0x4

    add-int v15, v15, v17

    .line 135
    :cond_4c
    add-int/lit8 v17, v15, 0x8

    move/from16 v0, v17

    add-int/lit16 v12, v0, 0x11c

    .line 138
    iput v12, v5, Lpxb/android/arsc/ArscWriter$PkgCtx;->keyStringOff:I

    .line 141
    iget-object v0, v5, Lpxb/android/arsc/ArscWriter$PkgCtx;->keyNames0:Lpxb/android/StringItems;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lpxb/android/StringItems;->getSize()I

    move-result v15

    .line 142
    rem-int/lit8 v17, v15, 0x4

    if-eqz v17, :cond_66

    .line 143
    rem-int/lit8 v17, v15, 0x4

    rsub-int/lit8 v17, v17, 0x4

    add-int v15, v15, v17

    .line 145
    :cond_66
    add-int/lit8 v17, v15, 0x8

    add-int v12, v12, v17

    .line 148
    iget-object v0, v5, Lpxb/android/arsc/ArscWriter$PkgCtx;->pkg:Lpxb/android/arsc/Pkg;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lpxb/android/arsc/Pkg;->types:Ljava/util/TreeMap;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/TreeMap;->values()Ljava/util/Collection;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_7c
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_13f

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lpxb/android/arsc/Type;

    .line 149
    .local v16, "type":Lpxb/android/arsc/Type;
    add-int v17, v13, v12

    move/from16 v0, v17

    move-object/from16 v1, v16

    iput v0, v1, Lpxb/android/arsc/Type;->wPosition:I

    .line 150
    move-object/from16 v0, v16

    iget-object v0, v0, Lpxb/android/arsc/Type;->specs:[Lpxb/android/arsc/ResSpec;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v17, v0

    mul-int/lit8 v17, v17, 0x4

    add-int/lit8 v17, v17, 0x10

    add-int v12, v12, v17

    .line 153
    move-object/from16 v0, v16

    iget-object v0, v0, Lpxb/android/arsc/Type;->configs:Ljava/util/List;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_ab
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_7c

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lpxb/android/arsc/Config;

    .line 154
    .local v3, "config":Lpxb/android/arsc/Config;
    add-int v17, v12, v13

    move/from16 v0, v17

    iput v0, v3, Lpxb/android/arsc/Config;->wPosition:I

    .line 155
    move v4, v12

    .line 156
    .local v4, "configBasePostion":I
    add-int/lit8 v12, v12, 0x14

    .line 157
    iget-object v0, v3, Lpxb/android/arsc/Config;->id:[B

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    array-length v14, v0

    .line 158
    .local v14, "size0":I
    rem-int/lit8 v17, v14, 0x4

    if-eqz v17, :cond_d1

    .line 159
    rem-int/lit8 v17, v14, 0x4

    rsub-int/lit8 v17, v17, 0x4

    add-int v14, v14, v17

    .line 161
    :cond_d1
    add-int/2addr v12, v14

    .line 163
    sub-int v17, v12, v4

    const/16 v18, 0x38

    move/from16 v0, v17

    move/from16 v1, v18

    if-le v0, v1, :cond_e4

    .line 164
    new-instance v17, Ljava/lang/RuntimeException;

    const-string v18, "config id  too big"

    invoke-direct/range {v17 .. v18}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v17

    .line 166
    :cond_e4
    add-int/lit8 v12, v4, 0x38

    .line 169
    iget v0, v3, Lpxb/android/arsc/Config;->entryCount:I

    move/from16 v17, v0

    mul-int/lit8 v17, v17, 0x4

    add-int v12, v12, v17

    .line 170
    sub-int v17, v12, v4

    move/from16 v0, v17

    iput v0, v3, Lpxb/android/arsc/Config;->wEntryStart:I

    .line 171
    move v7, v12

    .line 172
    .local v7, "entryBase":I
    iget-object v0, v3, Lpxb/android/arsc/Config;->resources:Ljava/util/Map;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .local v11, "i$":Ljava/util/Iterator;
    :goto_101
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_137

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lpxb/android/arsc/ResEntry;

    .line 173
    .local v6, "e":Lpxb/android/arsc/ResEntry;
    sub-int v17, v12, v7

    move/from16 v0, v17

    iput v0, v6, Lpxb/android/arsc/ResEntry;->wOffset:I

    .line 174
    add-int/lit8 v12, v12, 0x8

    .line 175
    iget-object v0, v6, Lpxb/android/arsc/ResEntry;->value:Ljava/lang/Object;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    instance-of v0, v0, Lpxb/android/arsc/BagValue;

    move/from16 v17, v0

    if-eqz v17, :cond_134

    .line 176
    iget-object v2, v6, Lpxb/android/arsc/ResEntry;->value:Ljava/lang/Object;

    check-cast v2, Lpxb/android/arsc/BagValue;

    .line 177
    .local v2, "big":Lpxb/android/arsc/BagValue;
    iget-object v0, v2, Lpxb/android/arsc/BagValue;->map:Ljava/util/List;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v17

    mul-int/lit8 v17, v17, 0xc

    add-int/lit8 v17, v17, 0x8

    add-int v12, v12, v17

    .line 178
    goto :goto_101

    .line 179
    .end local v2    # "big":Lpxb/android/arsc/BagValue;
    :cond_134
    add-int/lit8 v12, v12, 0x8

    goto :goto_101

    .line 182
    .end local v6    # "e":Lpxb/android/arsc/ResEntry;
    :cond_137
    sub-int v17, v12, v4

    move/from16 v0, v17

    iput v0, v3, Lpxb/android/arsc/Config;->wChunkSize:I

    goto/16 :goto_ab

    .line 185
    .end local v3    # "config":Lpxb/android/arsc/Config;
    .end local v4    # "configBasePostion":I
    .end local v7    # "entryBase":I
    .end local v11    # "i$":Ljava/util/Iterator;
    .end local v14    # "size0":I
    .end local v16    # "type":Lpxb/android/arsc/Type;
    :cond_13f
    iput v12, v5, Lpxb/android/arsc/ArscWriter$PkgCtx;->pkgSize:I

    .line 186
    add-int/2addr v13, v12

    .line 187
    goto/16 :goto_25

    .line 189
    .end local v5    # "ctx":Lpxb/android/arsc/ArscWriter$PkgCtx;
    .end local v12    # "pkgSize":I
    :cond_144
    return v13
.end method

.method public static varargs main([Ljava/lang/String;)V
    .registers 6
    .param p0, "args"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 90
    array-length v3, p0

    const/4 v4, 0x2

    if-ge v3, v4, :cond_c

    .line 91
    sget-object v3, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string v4, "asrc-write-test in.arsc out.arsc"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 100
    :goto_b
    return-void

    .line 94
    :cond_c
    new-instance v3, Ljava/io/File;

    const/4 v4, 0x0

    aget-object v4, p0, v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Lpxb/android/axml/Util;->readFile(Ljava/io/File;)[B

    move-result-object v0

    .line 95
    .local v0, "data":[B
    new-instance v3, Lpxb/android/arsc/ArscParser;

    invoke-direct {v3, v0}, Lpxb/android/arsc/ArscParser;-><init>([B)V

    invoke-virtual {v3}, Lpxb/android/arsc/ArscParser;->parse()Ljava/util/List;

    move-result-object v2

    .line 97
    .local v2, "pkgs":Ljava/util/List;, "Ljava/util/List<Lpxb/android/arsc/Pkg;>;"
    new-instance v3, Lpxb/android/arsc/ArscWriter;

    invoke-direct {v3, v2}, Lpxb/android/arsc/ArscWriter;-><init>(Ljava/util/List;)V

    invoke-virtual {v3}, Lpxb/android/arsc/ArscWriter;->toByteArray()[B

    move-result-object v1

    .line 99
    .local v1, "data2":[B
    new-instance v3, Ljava/io/File;

    const/4 v4, 0x1

    aget-object v4, p0, v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v1, v3}, Lpxb/android/axml/Util;->writeFile([BLjava/io/File;)V

    goto :goto_b
.end method

.method private prepare()Ljava/util/List;
    .registers 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lpxb/android/arsc/ArscWriter$PkgCtx;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 193
    iget-object v13, p0, Lpxb/android/arsc/ArscWriter;->pkgs:Ljava/util/List;

    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_6
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_9b

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lpxb/android/arsc/Pkg;

    .line 194
    .local v10, "pkg":Lpxb/android/arsc/Pkg;
    new-instance v2, Lpxb/android/arsc/ArscWriter$PkgCtx;

    const/4 v13, 0x0

    invoke-direct {v2, v13}, Lpxb/android/arsc/ArscWriter$PkgCtx;-><init>(Lpxb/android/arsc/ArscWriter$1;)V

    .line 195
    .local v2, "ctx":Lpxb/android/arsc/ArscWriter$PkgCtx;
    iput-object v10, v2, Lpxb/android/arsc/ArscWriter$PkgCtx;->pkg:Lpxb/android/arsc/Pkg;

    .line 196
    iget-object v13, p0, Lpxb/android/arsc/ArscWriter;->ctxs:Ljava/util/List;

    invoke-interface {v13, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 198
    iget-object v13, v10, Lpxb/android/arsc/Pkg;->types:Ljava/util/TreeMap;

    invoke-virtual {v13}, Ljava/util/TreeMap;->values()Ljava/util/Collection;

    move-result-object v13

    invoke-interface {v13}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_29
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_88

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lpxb/android/arsc/Type;

    .line 199
    .local v12, "type":Lpxb/android/arsc/Type;
    iget v13, v12, Lpxb/android/arsc/Type;->id:I

    add-int/lit8 v13, v13, -0x1

    iget-object v14, v12, Lpxb/android/arsc/Type;->name:Ljava/lang/String;

    invoke-virtual {v2, v13, v14}, Lpxb/android/arsc/ArscWriter$PkgCtx;->addTypeName(ILjava/lang/String;)V

    .line 200
    iget-object v0, v12, Lpxb/android/arsc/Type;->specs:[Lpxb/android/arsc/ResSpec;

    .local v0, "arr$":[Lpxb/android/arsc/ResSpec;
    array-length v8, v0

    .local v8, "len$":I
    const/4 v6, 0x0

    .local v6, "i$":I
    :goto_42
    if-ge v6, v8, :cond_4e

    aget-object v11, v0, v6

    .line 201
    .local v11, "spec":Lpxb/android/arsc/ResSpec;
    iget-object v13, v11, Lpxb/android/arsc/ResSpec;->name:Ljava/lang/String;

    invoke-virtual {v2, v13}, Lpxb/android/arsc/ArscWriter$PkgCtx;->addKeyName(Ljava/lang/String;)V

    .line 200
    add-int/lit8 v6, v6, 0x1

    goto :goto_42

    .line 203
    .end local v11    # "spec":Lpxb/android/arsc/ResSpec;
    :cond_4e
    iget-object v13, v12, Lpxb/android/arsc/Type;->configs:Ljava/util/List;

    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .end local v6    # "i$":I
    :cond_54
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_29

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lpxb/android/arsc/Config;

    .line 204
    .local v1, "config":Lpxb/android/arsc/Config;
    iget-object v13, v1, Lpxb/android/arsc/Config;->resources:Ljava/util/Map;

    invoke-interface {v13}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v13

    invoke-interface {v13}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :goto_6a
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_54

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lpxb/android/arsc/ResEntry;

    .line 205
    .local v3, "e":Lpxb/android/arsc/ResEntry;
    iget-object v9, v3, Lpxb/android/arsc/ResEntry;->value:Ljava/lang/Object;

    .line 206
    .local v9, "object":Ljava/lang/Object;
    instance-of v13, v9, Lpxb/android/arsc/BagValue;

    if-eqz v13, :cond_82

    .line 207
    check-cast v9, Lpxb/android/arsc/BagValue;

    .end local v9    # "object":Ljava/lang/Object;
    invoke-direct {p0, v9}, Lpxb/android/arsc/ArscWriter;->travelBagValue(Lpxb/android/arsc/BagValue;)V

    goto :goto_6a

    .line 209
    .restart local v9    # "object":Ljava/lang/Object;
    :cond_82
    check-cast v9, Lpxb/android/arsc/Value;

    .end local v9    # "object":Ljava/lang/Object;
    invoke-direct {p0, v9}, Lpxb/android/arsc/ArscWriter;->travelValue(Lpxb/android/arsc/Value;)V

    goto :goto_6a

    .line 214
    .end local v0    # "arr$":[Lpxb/android/arsc/ResSpec;
    .end local v1    # "config":Lpxb/android/arsc/Config;
    .end local v3    # "e":Lpxb/android/arsc/ResEntry;
    .end local v7    # "i$":Ljava/util/Iterator;
    .end local v8    # "len$":I
    .end local v12    # "type":Lpxb/android/arsc/Type;
    :cond_88
    iget-object v13, v2, Lpxb/android/arsc/ArscWriter$PkgCtx;->keyNames0:Lpxb/android/StringItems;

    invoke-virtual {v13}, Lpxb/android/StringItems;->prepare()V

    .line 215
    iget-object v13, v2, Lpxb/android/arsc/ArscWriter$PkgCtx;->typeNames0:Lpxb/android/StringItems;

    iget-object v14, v2, Lpxb/android/arsc/ArscWriter$PkgCtx;->typeNames:Ljava/util/List;

    invoke-virtual {v13, v14}, Lpxb/android/StringItems;->addAll(Ljava/util/Collection;)Z

    .line 216
    iget-object v13, v2, Lpxb/android/arsc/ArscWriter$PkgCtx;->typeNames0:Lpxb/android/StringItems;

    invoke-virtual {v13}, Lpxb/android/StringItems;->prepare()V

    goto/16 :goto_6

    .line 218
    .end local v2    # "ctx":Lpxb/android/arsc/ArscWriter$PkgCtx;
    .end local v10    # "pkg":Lpxb/android/arsc/Pkg;
    :cond_9b
    iget-object v13, p0, Lpxb/android/arsc/ArscWriter;->strTable0:Lpxb/android/StringItems;

    invoke-virtual {v13}, Lpxb/android/StringItems;->prepare()V

    .line 219
    iget-object v13, p0, Lpxb/android/arsc/ArscWriter;->ctxs:Ljava/util/List;

    return-object v13
.end method

.method private travelBagValue(Lpxb/android/arsc/BagValue;)V
    .registers 5
    .param p1, "bag"    # Lpxb/android/arsc/BagValue;

    .prologue
    .line 231
    iget-object v2, p1, Lpxb/android/arsc/BagValue;->map:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 232
    .local v0, "e":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lpxb/android/arsc/Value;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lpxb/android/arsc/Value;

    invoke-direct {p0, v2}, Lpxb/android/arsc/ArscWriter;->travelValue(Lpxb/android/arsc/Value;)V

    goto :goto_6

    .line 234
    .end local v0    # "e":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lpxb/android/arsc/Value;>;"
    :cond_1c
    return-void
.end method

.method private travelValue(Lpxb/android/arsc/Value;)V
    .registers 3
    .param p1, "v"    # Lpxb/android/arsc/Value;

    .prologue
    .line 237
    iget-object v0, p1, Lpxb/android/arsc/Value;->raw:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 238
    iget-object v0, p1, Lpxb/android/arsc/Value;->raw:Ljava/lang/String;

    invoke-direct {p0, v0}, Lpxb/android/arsc/ArscWriter;->addString(Ljava/lang/String;)V

    .line 240
    :cond_9
    return-void
.end method

.method private write(Ljava/nio/ByteBuffer;I)V
    .registers 34
    .param p1, "out"    # Ljava/nio/ByteBuffer;
    .param p2, "size"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 243
    const v27, 0xc0002

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 244
    invoke-virtual/range {p1 .. p2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 245
    move-object/from16 v0, p0

    iget-object v0, v0, Lpxb/android/arsc/ArscWriter;->ctxs:Ljava/util/List;

    move-object/from16 v27, v0

    invoke-interface/range {v27 .. v27}, Ljava/util/List;->size()I

    move-result v27

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 248
    move-object/from16 v0, p0

    iget-object v0, v0, Lpxb/android/arsc/ArscWriter;->strTable0:Lpxb/android/StringItems;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lpxb/android/StringItems;->getSize()I

    move-result v24

    .line 249
    .local v24, "stringSize":I
    const/16 v20, 0x0

    .line 250
    .local v20, "padding":I
    rem-int/lit8 v27, v24, 0x4

    if-eqz v27, :cond_32

    .line 251
    rem-int/lit8 v27, v24, 0x4

    rsub-int/lit8 v20, v27, 0x4

    .line 253
    :cond_32
    const v27, 0x1c0001

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 254
    add-int v27, v24, v20

    add-int/lit8 v27, v27, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 255
    move-object/from16 v0, p0

    iget-object v0, v0, Lpxb/android/arsc/ArscWriter;->strTable0:Lpxb/android/StringItems;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lpxb/android/StringItems;->write(Ljava/nio/ByteBuffer;)V

    .line 256
    move/from16 v0, v20

    new-array v0, v0, [B

    move-object/from16 v27, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 259
    move-object/from16 v0, p0

    iget-object v0, v0, Lpxb/android/arsc/ArscWriter;->ctxs:Ljava/util/List;

    move-object/from16 v27, v0

    invoke-interface/range {v27 .. v27}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_6b
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v27

    if-eqz v27, :cond_4c1

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lpxb/android/arsc/ArscWriter$PkgCtx;

    .line 260
    .local v21, "pctx":Lpxb/android/arsc/ArscWriter$PkgCtx;
    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v27

    move-object/from16 v0, v21

    iget v0, v0, Lpxb/android/arsc/ArscWriter$PkgCtx;->offset:I

    move/from16 v28, v0

    move/from16 v0, v27

    move/from16 v1, v28

    if-eq v0, v1, :cond_8d

    .line 261
    new-instance v27, Ljava/lang/RuntimeException;

    invoke-direct/range {v27 .. v27}, Ljava/lang/RuntimeException;-><init>()V

    throw v27

    .line 263
    :cond_8d
    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v5

    .line 264
    .local v5, "basePosition":I
    const v27, 0x11c0200

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 265
    move-object/from16 v0, v21

    iget v0, v0, Lpxb/android/arsc/ArscWriter$PkgCtx;->pkgSize:I

    move/from16 v27, v0

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 266
    move-object/from16 v0, v21

    iget-object v0, v0, Lpxb/android/arsc/ArscWriter$PkgCtx;->pkg:Lpxb/android/arsc/Pkg;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget v0, v0, Lpxb/android/arsc/Pkg;->id:I

    move/from16 v27, v0

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 267
    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v19

    .line 268
    .local v19, "p":I
    move-object/from16 v0, v21

    iget-object v0, v0, Lpxb/android/arsc/ArscWriter$PkgCtx;->pkg:Lpxb/android/arsc/Pkg;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget-object v0, v0, Lpxb/android/arsc/Pkg;->name:Ljava/lang/String;

    move-object/from16 v27, v0

    const-string v28, "UTF-16LE"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 269
    move/from16 v0, v19

    add-int/lit16 v0, v0, 0x100

    move/from16 v27, v0

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 271
    move-object/from16 v0, v21

    iget v0, v0, Lpxb/android/arsc/ArscWriter$PkgCtx;->typeStringOff:I

    move/from16 v27, v0

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 272
    move-object/from16 v0, v21

    iget-object v0, v0, Lpxb/android/arsc/ArscWriter$PkgCtx;->typeNames0:Lpxb/android/StringItems;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lpxb/android/StringItems;->size()I

    move-result v27

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 274
    move-object/from16 v0, v21

    iget v0, v0, Lpxb/android/arsc/ArscWriter$PkgCtx;->keyStringOff:I

    move/from16 v27, v0

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 275
    move-object/from16 v0, v21

    iget-object v0, v0, Lpxb/android/arsc/ArscWriter$PkgCtx;->keyNames0:Lpxb/android/StringItems;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lpxb/android/StringItems;->size()I

    move-result v27

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 278
    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v27

    sub-int v27, v27, v5

    move-object/from16 v0, v21

    iget v0, v0, Lpxb/android/arsc/ArscWriter$PkgCtx;->typeStringOff:I

    move/from16 v28, v0

    move/from16 v0, v27

    move/from16 v1, v28

    if-eq v0, v1, :cond_139

    .line 279
    new-instance v27, Ljava/lang/RuntimeException;

    invoke-direct/range {v27 .. v27}, Ljava/lang/RuntimeException;-><init>()V

    throw v27

    .line 281
    :cond_139
    move-object/from16 v0, v21

    iget-object v0, v0, Lpxb/android/arsc/ArscWriter$PkgCtx;->typeNames0:Lpxb/android/StringItems;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lpxb/android/StringItems;->getSize()I

    move-result v24

    .line 282
    const/16 v20, 0x0

    .line 283
    rem-int/lit8 v27, v24, 0x4

    if-eqz v27, :cond_14d

    .line 284
    rem-int/lit8 v27, v24, 0x4

    rsub-int/lit8 v20, v27, 0x4

    .line 286
    :cond_14d
    const v27, 0x1c0001

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 287
    add-int v27, v24, v20

    add-int/lit8 v27, v27, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 288
    move-object/from16 v0, v21

    iget-object v0, v0, Lpxb/android/arsc/ArscWriter$PkgCtx;->typeNames0:Lpxb/android/StringItems;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lpxb/android/StringItems;->write(Ljava/nio/ByteBuffer;)V

    .line 289
    move/from16 v0, v20

    new-array v0, v0, [B

    move-object/from16 v27, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 293
    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v27

    sub-int v27, v27, v5

    move-object/from16 v0, v21

    iget v0, v0, Lpxb/android/arsc/ArscWriter$PkgCtx;->keyStringOff:I

    move/from16 v28, v0

    move/from16 v0, v27

    move/from16 v1, v28

    if-eq v0, v1, :cond_194

    .line 294
    new-instance v27, Ljava/lang/RuntimeException;

    invoke-direct/range {v27 .. v27}, Ljava/lang/RuntimeException;-><init>()V

    throw v27

    .line 296
    :cond_194
    move-object/from16 v0, v21

    iget-object v0, v0, Lpxb/android/arsc/ArscWriter$PkgCtx;->keyNames0:Lpxb/android/StringItems;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lpxb/android/StringItems;->getSize()I

    move-result v24

    .line 297
    const/16 v20, 0x0

    .line 298
    rem-int/lit8 v27, v24, 0x4

    if-eqz v27, :cond_1a8

    .line 299
    rem-int/lit8 v27, v24, 0x4

    rsub-int/lit8 v20, v27, 0x4

    .line 301
    :cond_1a8
    const v27, 0x1c0001

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 302
    add-int v27, v24, v20

    add-int/lit8 v27, v27, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 303
    move-object/from16 v0, v21

    iget-object v0, v0, Lpxb/android/arsc/ArscWriter$PkgCtx;->keyNames0:Lpxb/android/StringItems;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lpxb/android/StringItems;->write(Ljava/nio/ByteBuffer;)V

    .line 304
    move/from16 v0, v20

    new-array v0, v0, [B

    move-object/from16 v27, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 307
    move-object/from16 v0, v21

    iget-object v0, v0, Lpxb/android/arsc/ArscWriter$PkgCtx;->pkg:Lpxb/android/arsc/Pkg;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget-object v0, v0, Lpxb/android/arsc/Pkg;->types:Ljava/util/TreeMap;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Ljava/util/TreeMap;->values()Ljava/util/Collection;

    move-result-object v27

    invoke-interface/range {v27 .. v27}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :cond_1eb
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v27

    if-eqz v27, :cond_6b

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Lpxb/android/arsc/Type;

    .line 308
    .local v25, "t":Lpxb/android/arsc/Type;
    const-string v27, "[%08x]write spec"

    const/16 v28, 0x2

    move/from16 v0, v28

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v28, v0

    const/16 v29, 0x0

    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v30

    invoke-static/range {v30 .. v30}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v30

    aput-object v30, v28, v29

    const/16 v29, 0x1

    move-object/from16 v0, v25

    iget-object v0, v0, Lpxb/android/arsc/Type;->name:Ljava/lang/String;

    move-object/from16 v30, v0

    aput-object v30, v28, v29

    invoke-static/range {v27 .. v28}, Lpxb/android/arsc/ArscWriter;->D(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 309
    move-object/from16 v0, v25

    iget v0, v0, Lpxb/android/arsc/Type;->wPosition:I

    move/from16 v27, v0

    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v28

    move/from16 v0, v27

    move/from16 v1, v28

    if-eq v0, v1, :cond_230

    .line 310
    new-instance v27, Ljava/lang/RuntimeException;

    invoke-direct/range {v27 .. v27}, Ljava/lang/RuntimeException;-><init>()V

    throw v27

    .line 312
    :cond_230
    const v27, 0x100202

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 313
    move-object/from16 v0, v25

    iget-object v0, v0, Lpxb/android/arsc/Type;->specs:[Lpxb/android/arsc/ResSpec;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    array-length v0, v0

    move/from16 v27, v0

    mul-int/lit8 v27, v27, 0x4

    add-int/lit8 v27, v27, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 315
    move-object/from16 v0, v25

    iget v0, v0, Lpxb/android/arsc/Type;->id:I

    move/from16 v27, v0

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 316
    move-object/from16 v0, v25

    iget-object v0, v0, Lpxb/android/arsc/Type;->specs:[Lpxb/android/arsc/ResSpec;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    array-length v0, v0

    move/from16 v27, v0

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 317
    move-object/from16 v0, v25

    iget-object v3, v0, Lpxb/android/arsc/Type;->specs:[Lpxb/android/arsc/ResSpec;

    .local v3, "arr$":[Lpxb/android/arsc/ResSpec;
    array-length v0, v3

    move/from16 v18, v0

    .local v18, "len$":I
    const/4 v14, 0x0

    .local v14, "i$":I
    :goto_277
    move/from16 v0, v18

    if-ge v14, v0, :cond_28d

    aget-object v23, v3, v14

    .line 318
    .local v23, "spec":Lpxb/android/arsc/ResSpec;
    move-object/from16 v0, v23

    iget v0, v0, Lpxb/android/arsc/ResSpec;->flags:I

    move/from16 v27, v0

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 317
    add-int/lit8 v14, v14, 0x1

    goto :goto_277

    .line 321
    .end local v23    # "spec":Lpxb/android/arsc/ResSpec;
    :cond_28d
    move-object/from16 v0, v25

    iget-object v0, v0, Lpxb/android/arsc/Type;->configs:Ljava/util/List;

    move-object/from16 v27, v0

    invoke-interface/range {v27 .. v27}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    .end local v14    # "i$":I
    :cond_297
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v27

    if-eqz v27, :cond_1eb

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lpxb/android/arsc/Config;

    .line 322
    .local v6, "config":Lpxb/android/arsc/Config;
    const-string v27, "[%08x]write config"

    const/16 v28, 0x1

    move/from16 v0, v28

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v28, v0

    const/16 v29, 0x0

    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v30

    invoke-static/range {v30 .. v30}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v30

    aput-object v30, v28, v29

    invoke-static/range {v27 .. v28}, Lpxb/android/arsc/ArscWriter;->D(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 323
    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v26

    .line 324
    .local v26, "typeConfigPosition":I
    iget v0, v6, Lpxb/android/arsc/Config;->wPosition:I

    move/from16 v27, v0

    move/from16 v0, v27

    move/from16 v1, v26

    if-eq v0, v1, :cond_2d0

    .line 325
    new-instance v27, Ljava/lang/RuntimeException;

    invoke-direct/range {v27 .. v27}, Ljava/lang/RuntimeException;-><init>()V

    throw v27

    .line 327
    :cond_2d0
    const v27, 0x380201

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 328
    iget v0, v6, Lpxb/android/arsc/Config;->wChunkSize:I

    move/from16 v27, v0

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 330
    move-object/from16 v0, v25

    iget v0, v0, Lpxb/android/arsc/Type;->id:I

    move/from16 v27, v0

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 331
    move-object/from16 v0, v25

    iget-object v0, v0, Lpxb/android/arsc/Type;->specs:[Lpxb/android/arsc/ResSpec;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    array-length v0, v0

    move/from16 v27, v0

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 332
    iget v0, v6, Lpxb/android/arsc/Config;->wEntryStart:I

    move/from16 v27, v0

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 334
    const-string v27, "[%08x]write config ids"

    const/16 v28, 0x1

    move/from16 v0, v28

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v28, v0

    const/16 v29, 0x0

    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v30

    invoke-static/range {v30 .. v30}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v30

    aput-object v30, v28, v29

    invoke-static/range {v27 .. v28}, Lpxb/android/arsc/ArscWriter;->D(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 335
    iget-object v0, v6, Lpxb/android/arsc/Config;->id:[B

    move-object/from16 v27, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 337
    iget-object v0, v6, Lpxb/android/arsc/Config;->id:[B

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    array-length v0, v0

    move/from16 v22, v0

    .line 338
    .local v22, "size0":I
    const/16 v20, 0x0

    .line 339
    rem-int/lit8 v27, v22, 0x4

    if-eqz v27, :cond_346

    .line 340
    rem-int/lit8 v27, v22, 0x4

    rsub-int/lit8 v20, v27, 0x4

    .line 342
    :cond_346
    move/from16 v0, v20

    new-array v0, v0, [B

    move-object/from16 v27, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 344
    add-int/lit8 v27, v26, 0x38

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 346
    const-string v27, "[%08x]write config entry offsets"

    const/16 v28, 0x1

    move/from16 v0, v28

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v28, v0

    const/16 v29, 0x0

    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v30

    invoke-static/range {v30 .. v30}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v30

    aput-object v30, v28, v29

    invoke-static/range {v27 .. v28}, Lpxb/android/arsc/ArscWriter;->D(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 347
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_376
    iget v0, v6, Lpxb/android/arsc/Config;->entryCount:I

    move/from16 v27, v0

    move/from16 v0, v27

    if-ge v11, v0, :cond_3a6

    .line 348
    iget-object v0, v6, Lpxb/android/arsc/Config;->resources:Ljava/util/Map;

    move-object/from16 v27, v0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    invoke-interface/range {v27 .. v28}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lpxb/android/arsc/ResEntry;

    .line 349
    .local v8, "entry":Lpxb/android/arsc/ResEntry;
    if-nez v8, :cond_39a

    .line 350
    const/16 v27, -0x1

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 347
    :goto_397
    add-int/lit8 v11, v11, 0x1

    goto :goto_376

    .line 352
    :cond_39a
    iget v0, v8, Lpxb/android/arsc/ResEntry;->wOffset:I

    move/from16 v27, v0

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    goto :goto_397

    .line 356
    .end local v8    # "entry":Lpxb/android/arsc/ResEntry;
    :cond_3a6
    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v27

    sub-int v27, v27, v26

    iget v0, v6, Lpxb/android/arsc/Config;->wEntryStart:I

    move/from16 v28, v0

    move/from16 v0, v27

    move/from16 v1, v28

    if-eq v0, v1, :cond_3bc

    .line 357
    new-instance v27, Ljava/lang/RuntimeException;

    invoke-direct/range {v27 .. v27}, Ljava/lang/RuntimeException;-><init>()V

    throw v27

    .line 359
    :cond_3bc
    const-string v27, "[%08x]write config entrys"

    const/16 v28, 0x1

    move/from16 v0, v28

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v28, v0

    const/16 v29, 0x0

    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v30

    invoke-static/range {v30 .. v30}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v30

    aput-object v30, v28, v29

    invoke-static/range {v27 .. v28}, Lpxb/android/arsc/ArscWriter;->D(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 360
    iget-object v0, v6, Lpxb/android/arsc/Config;->resources:Ljava/util/Map;

    move-object/from16 v27, v0

    invoke-interface/range {v27 .. v27}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v27

    invoke-interface/range {v27 .. v27}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :cond_3e1
    :goto_3e1
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v27

    if-eqz v27, :cond_297

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lpxb/android/arsc/ResEntry;

    .line 361
    .local v7, "e":Lpxb/android/arsc/ResEntry;
    const-string v27, "[%08x]ResTable_entry"

    const/16 v28, 0x1

    move/from16 v0, v28

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v28, v0

    const/16 v29, 0x0

    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v30

    invoke-static/range {v30 .. v30}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v30

    aput-object v30, v28, v29

    invoke-static/range {v27 .. v28}, Lpxb/android/arsc/ArscWriter;->D(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 362
    iget-object v0, v7, Lpxb/android/arsc/ResEntry;->value:Ljava/lang/Object;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    instance-of v0, v0, Lpxb/android/arsc/BagValue;

    move/from16 v17, v0

    .line 363
    .local v17, "isBag":Z
    if-eqz v17, :cond_4a8

    const/16 v27, 0x10

    :goto_414
    move/from16 v0, v27

    int-to-short v0, v0

    move/from16 v27, v0

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 364
    iget v10, v7, Lpxb/android/arsc/ResEntry;->flag:I

    .line 365
    .local v10, "flag":I
    if-eqz v17, :cond_4ac

    .line 366
    or-int/lit8 v10, v10, 0x1

    .line 370
    :goto_426
    int-to-short v0, v10

    move/from16 v27, v0

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 371
    move-object/from16 v0, v21

    iget-object v0, v0, Lpxb/android/arsc/ArscWriter$PkgCtx;->keyNames:Ljava/util/Map;

    move-object/from16 v27, v0

    iget-object v0, v7, Lpxb/android/arsc/ResEntry;->spec:Lpxb/android/arsc/ResSpec;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lpxb/android/arsc/ResSpec;->name:Ljava/lang/String;

    move-object/from16 v28, v0

    invoke-interface/range {v27 .. v28}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Lpxb/android/StringItem;

    move-object/from16 v0, v27

    iget v0, v0, Lpxb/android/StringItem;->index:I

    move/from16 v27, v0

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 372
    if-eqz v17, :cond_4b0

    .line 373
    iget-object v4, v7, Lpxb/android/arsc/ResEntry;->value:Ljava/lang/Object;

    check-cast v4, Lpxb/android/arsc/BagValue;

    .line 374
    .local v4, "bag":Lpxb/android/arsc/BagValue;
    iget v0, v4, Lpxb/android/arsc/BagValue;->parent:I

    move/from16 v27, v0

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 375
    iget-object v0, v4, Lpxb/android/arsc/BagValue;->map:Ljava/util/List;

    move-object/from16 v27, v0

    invoke-interface/range {v27 .. v27}, Ljava/util/List;->size()I

    move-result v27

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 376
    iget-object v0, v4, Lpxb/android/arsc/BagValue;->map:Ljava/util/List;

    move-object/from16 v27, v0

    invoke-interface/range {v27 .. v27}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v16

    .local v16, "i$":Ljava/util/Iterator;
    :goto_47b
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v27

    if-eqz v27, :cond_3e1

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/Map$Entry;

    .line 377
    .local v9, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lpxb/android/arsc/Value;>;"
    invoke-interface {v9}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Ljava/lang/Integer;

    invoke-virtual/range {v27 .. v27}, Ljava/lang/Integer;->intValue()I

    move-result v27

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 378
    invoke-interface {v9}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Lpxb/android/arsc/Value;

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lpxb/android/arsc/ArscWriter;->writeValue(Lpxb/android/arsc/Value;Ljava/nio/ByteBuffer;)V

    goto :goto_47b

    .line 363
    .end local v4    # "bag":Lpxb/android/arsc/BagValue;
    .end local v9    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lpxb/android/arsc/Value;>;"
    .end local v10    # "flag":I
    .end local v16    # "i$":Ljava/util/Iterator;
    :cond_4a8
    const/16 v27, 0x8

    goto/16 :goto_414

    .line 368
    .restart local v10    # "flag":I
    :cond_4ac
    and-int/lit8 v10, v10, -0x2

    goto/16 :goto_426

    .line 381
    :cond_4b0
    iget-object v0, v7, Lpxb/android/arsc/ResEntry;->value:Ljava/lang/Object;

    move-object/from16 v27, v0

    check-cast v27, Lpxb/android/arsc/Value;

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lpxb/android/arsc/ArscWriter;->writeValue(Lpxb/android/arsc/Value;Ljava/nio/ByteBuffer;)V

    goto/16 :goto_3e1

    .line 387
    .end local v3    # "arr$":[Lpxb/android/arsc/ResSpec;
    .end local v5    # "basePosition":I
    .end local v6    # "config":Lpxb/android/arsc/Config;
    .end local v7    # "e":Lpxb/android/arsc/ResEntry;
    .end local v10    # "flag":I
    .end local v11    # "i":I
    .end local v17    # "isBag":Z
    .end local v18    # "len$":I
    .end local v19    # "p":I
    .end local v21    # "pctx":Lpxb/android/arsc/ArscWriter$PkgCtx;
    .end local v22    # "size0":I
    .end local v25    # "t":Lpxb/android/arsc/Type;
    .end local v26    # "typeConfigPosition":I
    :cond_4c1
    return-void
.end method

.method private writeValue(Lpxb/android/arsc/Value;Ljava/nio/ByteBuffer;)V
    .registers 5
    .param p1, "value"    # Lpxb/android/arsc/Value;
    .param p2, "out"    # Ljava/nio/ByteBuffer;

    .prologue
    .line 390
    const/16 v0, 0x8

    invoke-virtual {p2, v0}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 391
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 392
    iget v0, p1, Lpxb/android/arsc/Value;->type:I

    int-to-byte v0, v0

    invoke-virtual {p2, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 393
    iget v0, p1, Lpxb/android/arsc/Value;->type:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_24

    .line 394
    iget-object v0, p0, Lpxb/android/arsc/ArscWriter;->strTable:Ljava/util/Map;

    iget-object v1, p1, Lpxb/android/arsc/Value;->raw:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpxb/android/StringItem;

    iget v0, v0, Lpxb/android/StringItem;->index:I

    invoke-virtual {p2, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 398
    :goto_23
    return-void

    .line 396
    :cond_24
    iget v0, p1, Lpxb/android/arsc/Value;->data:I

    invoke-virtual {p2, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    goto :goto_23
.end method


# virtual methods
.method public toByteArray()[B
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 223
    invoke-direct {p0}, Lpxb/android/arsc/ArscWriter;->prepare()Ljava/util/List;

    .line 224
    invoke-direct {p0}, Lpxb/android/arsc/ArscWriter;->count()I

    move-result v1

    .line 225
    .local v1, "size":I
    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    sget-object v3, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 226
    .local v0, "out":Ljava/nio/ByteBuffer;
    invoke-direct {p0, v0, v1}, Lpxb/android/arsc/ArscWriter;->write(Ljava/nio/ByteBuffer;I)V

    .line 227
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v2

    return-object v2
.end method
