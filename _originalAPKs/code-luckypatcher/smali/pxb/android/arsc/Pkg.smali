.class public Lpxb/android/arsc/Pkg;
.super Ljava/lang/Object;
.source "Pkg.java"


# instance fields
.field public final id:I

.field public name:Ljava/lang/String;

.field public types:Ljava/util/TreeMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Integer;",
            "Lpxb/android/arsc/Type;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(ILjava/lang/String;)V
    .registers 4
    .param p1, "id"    # I
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lpxb/android/arsc/Pkg;->types:Ljava/util/TreeMap;

    .line 27
    iput p1, p0, Lpxb/android/arsc/Pkg;->id:I

    .line 28
    iput-object p2, p0, Lpxb/android/arsc/Pkg;->name:Ljava/lang/String;

    .line 29
    return-void
.end method


# virtual methods
.method public getType(ILjava/lang/String;I)Lpxb/android/arsc/Type;
    .registers 7
    .param p1, "tid"    # I
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "entrySize"    # I

    .prologue
    .line 32
    iget-object v1, p0, Lpxb/android/arsc/Pkg;->types:Ljava/util/TreeMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpxb/android/arsc/Type;

    .line 33
    .local v0, "type":Lpxb/android/arsc/Type;
    if-eqz v0, :cond_2f

    .line 34
    if-eqz p2, :cond_45

    .line 35
    iget-object v1, v0, Lpxb/android/arsc/Type;->name:Ljava/lang/String;

    if-nez v1, :cond_21

    .line 36
    iput-object p2, v0, Lpxb/android/arsc/Type;->name:Ljava/lang/String;

    .line 40
    :cond_16
    iget-object v1, v0, Lpxb/android/arsc/Type;->specs:[Lpxb/android/arsc/ResSpec;

    array-length v1, v1

    if-eq v1, p3, :cond_45

    .line 41
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1}, Ljava/lang/RuntimeException;-><init>()V

    throw v1

    .line 37
    :cond_21
    iget-object v1, v0, Lpxb/android/arsc/Type;->name:Ljava/lang/String;

    invoke-virtual {p2, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_16

    .line 38
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1}, Ljava/lang/RuntimeException;-><init>()V

    throw v1

    .line 45
    :cond_2f
    new-instance v0, Lpxb/android/arsc/Type;

    .end local v0    # "type":Lpxb/android/arsc/Type;
    invoke-direct {v0}, Lpxb/android/arsc/Type;-><init>()V

    .line 46
    .restart local v0    # "type":Lpxb/android/arsc/Type;
    iput p1, v0, Lpxb/android/arsc/Type;->id:I

    .line 47
    iput-object p2, v0, Lpxb/android/arsc/Type;->name:Ljava/lang/String;

    .line 48
    new-array v1, p3, [Lpxb/android/arsc/ResSpec;

    iput-object v1, v0, Lpxb/android/arsc/Type;->specs:[Lpxb/android/arsc/ResSpec;

    .line 49
    iget-object v1, p0, Lpxb/android/arsc/Pkg;->types:Ljava/util/TreeMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    :cond_45
    return-object v0
.end method
