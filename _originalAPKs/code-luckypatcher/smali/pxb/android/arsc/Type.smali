.class public Lpxb/android/arsc/Type;
.super Ljava/lang/Object;
.source "Type.java"


# instance fields
.field public configs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lpxb/android/arsc/Config;",
            ">;"
        }
    .end annotation
.end field

.field public id:I

.field public name:Ljava/lang/String;

.field public specs:[Lpxb/android/arsc/ResSpec;

.field wPosition:I


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lpxb/android/arsc/Type;->configs:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public addConfig(Lpxb/android/arsc/Config;)V
    .registers 4
    .param p1, "config"    # Lpxb/android/arsc/Config;

    .prologue
    .line 29
    iget v0, p1, Lpxb/android/arsc/Config;->entryCount:I

    iget-object v1, p0, Lpxb/android/arsc/Type;->specs:[Lpxb/android/arsc/ResSpec;

    array-length v1, v1

    if-eq v0, v1, :cond_d

    .line 30
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0}, Ljava/lang/RuntimeException;-><init>()V

    throw v0

    .line 32
    :cond_d
    iget-object v0, p0, Lpxb/android/arsc/Type;->configs:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 33
    return-void
.end method

.method public getSpec(I)Lpxb/android/arsc/ResSpec;
    .registers 4
    .param p1, "resId"    # I

    .prologue
    .line 36
    iget-object v1, p0, Lpxb/android/arsc/Type;->specs:[Lpxb/android/arsc/ResSpec;

    aget-object v0, v1, p1

    .line 37
    .local v0, "res":Lpxb/android/arsc/ResSpec;
    if-nez v0, :cond_f

    .line 38
    new-instance v0, Lpxb/android/arsc/ResSpec;

    .end local v0    # "res":Lpxb/android/arsc/ResSpec;
    invoke-direct {v0, p1}, Lpxb/android/arsc/ResSpec;-><init>(I)V

    .line 39
    .restart local v0    # "res":Lpxb/android/arsc/ResSpec;
    iget-object v1, p0, Lpxb/android/arsc/Type;->specs:[Lpxb/android/arsc/ResSpec;

    aput-object v0, v1, p1

    .line 41
    :cond_f
    return-object v0
.end method
