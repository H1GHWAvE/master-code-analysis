.class public Lnet/lingala/zip4j/zip/ZipEngine;
.super Ljava/lang/Object;
.source "ZipEngine.java"


# instance fields
.field private zipModel:Lnet/lingala/zip4j/model/ZipModel;


# direct methods
.method public constructor <init>(Lnet/lingala/zip4j/model/ZipModel;)V
    .registers 4
    .param p1, "zipModel"    # Lnet/lingala/zip4j/model/ZipModel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/lingala/zip4j/exception/ZipException;
        }
    .end annotation

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    if-nez p1, :cond_d

    .line 49
    new-instance v0, Lnet/lingala/zip4j/exception/ZipException;

    const-string v1, "zip model is null in ZipEngine constructor"

    invoke-direct {v0, v1}, Lnet/lingala/zip4j/exception/ZipException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 52
    :cond_d
    iput-object p1, p0, Lnet/lingala/zip4j/zip/ZipEngine;->zipModel:Lnet/lingala/zip4j/model/ZipModel;

    .line 53
    return-void
.end method

.method static synthetic access$000(Lnet/lingala/zip4j/zip/ZipEngine;Ljava/util/ArrayList;Lnet/lingala/zip4j/model/ZipParameters;Lnet/lingala/zip4j/progress/ProgressMonitor;)V
    .registers 4
    .param p0, "x0"    # Lnet/lingala/zip4j/zip/ZipEngine;
    .param p1, "x1"    # Ljava/util/ArrayList;
    .param p2, "x2"    # Lnet/lingala/zip4j/model/ZipParameters;
    .param p3, "x3"    # Lnet/lingala/zip4j/progress/ProgressMonitor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/lingala/zip4j/exception/ZipException;
        }
    .end annotation

    .prologue
    .line 42
    invoke-direct {p0, p1, p2, p3}, Lnet/lingala/zip4j/zip/ZipEngine;->initAddFiles(Ljava/util/ArrayList;Lnet/lingala/zip4j/model/ZipParameters;Lnet/lingala/zip4j/progress/ProgressMonitor;)V

    return-void
.end method

.method private calculateTotalWork(Ljava/util/ArrayList;Lnet/lingala/zip4j/model/ZipParameters;)J
    .registers 12
    .param p1, "fileList"    # Ljava/util/ArrayList;
    .param p2, "parameters"    # Lnet/lingala/zip4j/model/ZipParameters;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/lingala/zip4j/exception/ZipException;
        }
    .end annotation

    .prologue
    .line 455
    if-nez p1, :cond_a

    .line 456
    new-instance v5, Lnet/lingala/zip4j/exception/ZipException;

    const-string v6, "file list is null, cannot calculate total work"

    invoke-direct {v5, v6}, Lnet/lingala/zip4j/exception/ZipException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 459
    :cond_a
    const-wide/16 v3, 0x0

    .line 461
    .local v3, "totalWork":J
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_d
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v1, v5, :cond_a8

    .line 462
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    instance-of v5, v5, Ljava/io/File;

    if-eqz v5, :cond_98

    .line 463
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_98

    .line 464
    invoke-virtual {p2}, Lnet/lingala/zip4j/model/ZipParameters;->isEncryptFiles()Z

    move-result v5

    if-eqz v5, :cond_9c

    .line 465
    invoke-virtual {p2}, Lnet/lingala/zip4j/model/ZipParameters;->getEncryptionMethod()I

    move-result v5

    if-nez v5, :cond_9c

    .line 466
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/io/File;

    invoke-static {v5}, Lnet/lingala/zip4j/util/Zip4jUtil;->getFileLengh(Ljava/io/File;)J

    move-result-wide v5

    const-wide/16 v7, 0x2

    mul-long/2addr v5, v7

    add-long/2addr v3, v5

    .line 471
    :goto_41
    iget-object v5, p0, Lnet/lingala/zip4j/zip/ZipEngine;->zipModel:Lnet/lingala/zip4j/model/ZipModel;

    invoke-virtual {v5}, Lnet/lingala/zip4j/model/ZipModel;->getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;

    move-result-object v5

    if-eqz v5, :cond_98

    iget-object v5, p0, Lnet/lingala/zip4j/zip/ZipEngine;->zipModel:Lnet/lingala/zip4j/model/ZipModel;

    .line 472
    invoke-virtual {v5}, Lnet/lingala/zip4j/model/ZipModel;->getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;

    move-result-object v5

    invoke-virtual {v5}, Lnet/lingala/zip4j/model/CentralDirectory;->getFileHeaders()Ljava/util/ArrayList;

    move-result-object v5

    if-eqz v5, :cond_98

    iget-object v5, p0, Lnet/lingala/zip4j/zip/ZipEngine;->zipModel:Lnet/lingala/zip4j/model/ZipModel;

    .line 473
    invoke-virtual {v5}, Lnet/lingala/zip4j/model/ZipModel;->getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;

    move-result-object v5

    invoke-virtual {v5}, Lnet/lingala/zip4j/model/CentralDirectory;->getFileHeaders()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_98

    .line 475
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2}, Lnet/lingala/zip4j/model/ZipParameters;->getRootFolderInZip()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2}, Lnet/lingala/zip4j/model/ZipParameters;->getDefaultFolderPath()Ljava/lang/String;

    move-result-object v7

    .line 474
    invoke-static {v5, v6, v7}, Lnet/lingala/zip4j/util/Zip4jUtil;->getRelativeFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 476
    .local v2, "relativeFileName":Ljava/lang/String;
    iget-object v5, p0, Lnet/lingala/zip4j/zip/ZipEngine;->zipModel:Lnet/lingala/zip4j/model/ZipModel;

    invoke-static {v5, v2}, Lnet/lingala/zip4j/util/Zip4jUtil;->getFileHeader(Lnet/lingala/zip4j/model/ZipModel;Ljava/lang/String;)Lnet/lingala/zip4j/model/FileHeader;

    move-result-object v0

    .line 477
    .local v0, "fileHeader":Lnet/lingala/zip4j/model/FileHeader;
    if-eqz v0, :cond_98

    .line 478
    new-instance v5, Ljava/io/File;

    iget-object v6, p0, Lnet/lingala/zip4j/zip/ZipEngine;->zipModel:Lnet/lingala/zip4j/model/ZipModel;

    invoke-virtual {v6}, Lnet/lingala/zip4j/model/ZipModel;->getZipFile()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v5}, Lnet/lingala/zip4j/util/Zip4jUtil;->getFileLengh(Ljava/io/File;)J

    move-result-wide v5

    invoke-virtual {v0}, Lnet/lingala/zip4j/model/FileHeader;->getCompressedSize()J

    move-result-wide v7

    sub-long/2addr v5, v7

    add-long/2addr v3, v5

    .line 461
    .end local v0    # "fileHeader":Lnet/lingala/zip4j/model/FileHeader;
    .end local v2    # "relativeFileName":Ljava/lang/String;
    :cond_98
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_d

    .line 468
    :cond_9c
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/io/File;

    invoke-static {v5}, Lnet/lingala/zip4j/util/Zip4jUtil;->getFileLengh(Ljava/io/File;)J

    move-result-wide v5

    add-long/2addr v3, v5

    goto :goto_41

    .line 485
    :cond_a8
    return-wide v3
.end method

.method private checkParameters(Lnet/lingala/zip4j/model/ZipParameters;)V
    .registers 5
    .param p1, "parameters"    # Lnet/lingala/zip4j/model/ZipParameters;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/lingala/zip4j/exception/ZipException;
        }
    .end annotation

    .prologue
    const/16 v1, 0x8

    const/4 v2, -0x1

    .line 305
    if-nez p1, :cond_d

    .line 306
    new-instance v0, Lnet/lingala/zip4j/exception/ZipException;

    const-string v1, "cannot validate zip parameters"

    invoke-direct {v0, v1}, Lnet/lingala/zip4j/exception/ZipException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 309
    :cond_d
    invoke-virtual {p1}, Lnet/lingala/zip4j/model/ZipParameters;->getCompressionMethod()I

    move-result v0

    if-eqz v0, :cond_21

    .line 310
    invoke-virtual {p1}, Lnet/lingala/zip4j/model/ZipParameters;->getCompressionMethod()I

    move-result v0

    if-eq v0, v1, :cond_21

    .line 311
    new-instance v0, Lnet/lingala/zip4j/exception/ZipException;

    const-string v1, "unsupported compression type"

    invoke-direct {v0, v1}, Lnet/lingala/zip4j/exception/ZipException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 314
    :cond_21
    invoke-virtual {p1}, Lnet/lingala/zip4j/model/ZipParameters;->getCompressionMethod()I

    move-result v0

    if-ne v0, v1, :cond_3d

    .line 315
    invoke-virtual {p1}, Lnet/lingala/zip4j/model/ZipParameters;->getCompressionLevel()I

    move-result v0

    if-gez v0, :cond_3d

    invoke-virtual {p1}, Lnet/lingala/zip4j/model/ZipParameters;->getCompressionLevel()I

    move-result v0

    const/16 v1, 0x9

    if-le v0, v1, :cond_3d

    .line 316
    new-instance v0, Lnet/lingala/zip4j/exception/ZipException;

    const-string v1, "invalid compression level. compression level dor deflate should be in the range of 0-9"

    invoke-direct {v0, v1}, Lnet/lingala/zip4j/exception/ZipException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 320
    :cond_3d
    invoke-virtual {p1}, Lnet/lingala/zip4j/model/ZipParameters;->isEncryptFiles()Z

    move-result v0

    if-eqz v0, :cond_6e

    .line 321
    invoke-virtual {p1}, Lnet/lingala/zip4j/model/ZipParameters;->getEncryptionMethod()I

    move-result v0

    if-eqz v0, :cond_59

    .line 322
    invoke-virtual {p1}, Lnet/lingala/zip4j/model/ZipParameters;->getEncryptionMethod()I

    move-result v0

    const/16 v1, 0x63

    if-eq v0, v1, :cond_59

    .line 323
    new-instance v0, Lnet/lingala/zip4j/exception/ZipException;

    const-string v1, "unsupported encryption method"

    invoke-direct {v0, v1}, Lnet/lingala/zip4j/exception/ZipException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 326
    :cond_59
    invoke-virtual {p1}, Lnet/lingala/zip4j/model/ZipParameters;->getPassword()[C

    move-result-object v0

    if-eqz v0, :cond_66

    invoke-virtual {p1}, Lnet/lingala/zip4j/model/ZipParameters;->getPassword()[C

    move-result-object v0

    array-length v0, v0

    if-gtz v0, :cond_74

    .line 327
    :cond_66
    new-instance v0, Lnet/lingala/zip4j/exception/ZipException;

    const-string v1, "input password is empty or null"

    invoke-direct {v0, v1}, Lnet/lingala/zip4j/exception/ZipException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 330
    :cond_6e
    invoke-virtual {p1, v2}, Lnet/lingala/zip4j/model/ZipParameters;->setAesKeyStrength(I)V

    .line 331
    invoke-virtual {p1, v2}, Lnet/lingala/zip4j/model/ZipParameters;->setEncryptionMethod(I)V

    .line 334
    :cond_74
    return-void
.end method

.method private createEndOfCentralDirectoryRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 445
    new-instance v0, Lnet/lingala/zip4j/model/EndCentralDirRecord;

    invoke-direct {v0}, Lnet/lingala/zip4j/model/EndCentralDirRecord;-><init>()V

    .line 446
    .local v0, "endCentralDirRecord":Lnet/lingala/zip4j/model/EndCentralDirRecord;
    const-wide/32 v1, 0x6054b50

    invoke-virtual {v0, v1, v2}, Lnet/lingala/zip4j/model/EndCentralDirRecord;->setSignature(J)V

    .line 447
    invoke-virtual {v0, v3}, Lnet/lingala/zip4j/model/EndCentralDirRecord;->setNoOfThisDisk(I)V

    .line 448
    invoke-virtual {v0, v3}, Lnet/lingala/zip4j/model/EndCentralDirRecord;->setTotNoOfEntriesInCentralDir(I)V

    .line 449
    invoke-virtual {v0, v3}, Lnet/lingala/zip4j/model/EndCentralDirRecord;->setTotNoOfEntriesInCentralDirOnThisDisk(I)V

    .line 450
    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Lnet/lingala/zip4j/model/EndCentralDirRecord;->setOffsetOfStartOfCentralDir(J)V

    .line 451
    return-object v0
.end method

.method private initAddFiles(Ljava/util/ArrayList;Lnet/lingala/zip4j/model/ZipParameters;Lnet/lingala/zip4j/progress/ProgressMonitor;)V
    .registers 22
    .param p1, "fileList"    # Ljava/util/ArrayList;
    .param p2, "parameters"    # Lnet/lingala/zip4j/model/ZipParameters;
    .param p3, "progressMonitor"    # Lnet/lingala/zip4j/progress/ProgressMonitor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/lingala/zip4j/exception/ZipException;
        }
    .end annotation

    .prologue
    .line 92
    if-eqz p1, :cond_4

    if-nez p2, :cond_c

    .line 93
    :cond_4
    new-instance v14, Lnet/lingala/zip4j/exception/ZipException;

    const-string v15, "one of the input parameters is null when adding files"

    invoke-direct {v14, v15}, Lnet/lingala/zip4j/exception/ZipException;-><init>(Ljava/lang/String;)V

    throw v14

    .line 96
    :cond_c
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v14

    if-gtz v14, :cond_1a

    .line 97
    new-instance v14, Lnet/lingala/zip4j/exception/ZipException;

    const-string v15, "no files to add"

    invoke-direct {v14, v15}, Lnet/lingala/zip4j/exception/ZipException;-><init>(Ljava/lang/String;)V

    throw v14

    .line 100
    :cond_1a
    move-object/from16 v0, p0

    iget-object v14, v0, Lnet/lingala/zip4j/zip/ZipEngine;->zipModel:Lnet/lingala/zip4j/model/ZipModel;

    invoke-virtual {v14}, Lnet/lingala/zip4j/model/ZipModel;->getEndCentralDirRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;

    move-result-object v14

    if-nez v14, :cond_2f

    .line 101
    move-object/from16 v0, p0

    iget-object v14, v0, Lnet/lingala/zip4j/zip/ZipEngine;->zipModel:Lnet/lingala/zip4j/model/ZipModel;

    invoke-direct/range {p0 .. p0}, Lnet/lingala/zip4j/zip/ZipEngine;->createEndOfCentralDirectoryRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;

    move-result-object v15

    invoke-virtual {v14, v15}, Lnet/lingala/zip4j/model/ZipModel;->setEndCentralDirRecord(Lnet/lingala/zip4j/model/EndCentralDirRecord;)V

    .line 104
    :cond_2f
    const/4 v9, 0x0

    .line 105
    .local v9, "outputStream":Lnet/lingala/zip4j/io/ZipOutputStream;
    const/4 v6, 0x0

    .line 107
    .local v6, "inputStream":Ljava/io/InputStream;
    :try_start_31
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lnet/lingala/zip4j/zip/ZipEngine;->checkParameters(Lnet/lingala/zip4j/model/ZipParameters;)V

    .line 109
    invoke-direct/range {p0 .. p3}, Lnet/lingala/zip4j/zip/ZipEngine;->removeFilesIfExists(Ljava/util/ArrayList;Lnet/lingala/zip4j/model/ZipParameters;Lnet/lingala/zip4j/progress/ProgressMonitor;)V

    .line 111
    move-object/from16 v0, p0

    iget-object v14, v0, Lnet/lingala/zip4j/zip/ZipEngine;->zipModel:Lnet/lingala/zip4j/model/ZipModel;

    invoke-virtual {v14}, Lnet/lingala/zip4j/model/ZipModel;->getZipFile()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lnet/lingala/zip4j/util/Zip4jUtil;->checkFileExists(Ljava/lang/String;)Z

    move-result v8

    .line 113
    .local v8, "isZipFileAlreadExists":Z
    new-instance v13, Lnet/lingala/zip4j/io/SplitOutputStream;

    new-instance v14, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v15, v0, Lnet/lingala/zip4j/zip/ZipEngine;->zipModel:Lnet/lingala/zip4j/model/ZipModel;

    invoke-virtual {v15}, Lnet/lingala/zip4j/model/ZipModel;->getZipFile()Ljava/lang/String;

    move-result-object v15

    invoke-direct {v14, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v15, v0, Lnet/lingala/zip4j/zip/ZipEngine;->zipModel:Lnet/lingala/zip4j/model/ZipModel;

    invoke-virtual {v15}, Lnet/lingala/zip4j/model/ZipModel;->getSplitLength()J

    move-result-wide v15

    invoke-direct/range {v13 .. v16}, Lnet/lingala/zip4j/io/SplitOutputStream;-><init>(Ljava/io/File;J)V

    .line 114
    .local v13, "splitOutputStream":Lnet/lingala/zip4j/io/SplitOutputStream;
    new-instance v10, Lnet/lingala/zip4j/io/ZipOutputStream;

    move-object/from16 v0, p0

    iget-object v14, v0, Lnet/lingala/zip4j/zip/ZipEngine;->zipModel:Lnet/lingala/zip4j/model/ZipModel;

    invoke-direct {v10, v13, v14}, Lnet/lingala/zip4j/io/ZipOutputStream;-><init>(Ljava/io/OutputStream;Lnet/lingala/zip4j/model/ZipModel;)V
    :try_end_6a
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_31 .. :try_end_6a} :catch_22d
    .catch Ljava/lang/Exception; {:try_start_31 .. :try_end_6a} :catch_227
    .catchall {:try_start_31 .. :try_end_6a} :catchall_86

    .line 116
    .end local v9    # "outputStream":Lnet/lingala/zip4j/io/ZipOutputStream;
    .local v10, "outputStream":Lnet/lingala/zip4j/io/ZipOutputStream;
    if-eqz v8, :cond_a1

    .line 117
    :try_start_6c
    move-object/from16 v0, p0

    iget-object v14, v0, Lnet/lingala/zip4j/zip/ZipEngine;->zipModel:Lnet/lingala/zip4j/model/ZipModel;

    invoke-virtual {v14}, Lnet/lingala/zip4j/model/ZipModel;->getEndCentralDirRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;

    move-result-object v14

    if-nez v14, :cond_92

    .line 118
    new-instance v14, Lnet/lingala/zip4j/exception/ZipException;

    const-string v15, "invalid end of central directory record"

    invoke-direct {v14, v15}, Lnet/lingala/zip4j/exception/ZipException;-><init>(Ljava/lang/String;)V

    throw v14
    :try_end_7e
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_6c .. :try_end_7e} :catch_7e
    .catch Ljava/lang/Exception; {:try_start_6c .. :try_end_7e} :catch_1dc
    .catchall {:try_start_6c .. :try_end_7e} :catchall_1f3

    .line 184
    :catch_7e
    move-exception v2

    move-object v9, v10

    .line 185
    .end local v8    # "isZipFileAlreadExists":Z
    .end local v10    # "outputStream":Lnet/lingala/zip4j/io/ZipOutputStream;
    .end local v13    # "splitOutputStream":Lnet/lingala/zip4j/io/SplitOutputStream;
    .local v2, "e":Lnet/lingala/zip4j/exception/ZipException;
    .restart local v9    # "outputStream":Lnet/lingala/zip4j/io/ZipOutputStream;
    :goto_80
    :try_start_80
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Lnet/lingala/zip4j/progress/ProgressMonitor;->endProgressMonitorError(Ljava/lang/Throwable;)V

    .line 186
    throw v2
    :try_end_86
    .catchall {:try_start_80 .. :try_end_86} :catchall_86

    .line 191
    .end local v2    # "e":Lnet/lingala/zip4j/exception/ZipException;
    :catchall_86
    move-exception v14

    :goto_87
    if-eqz v6, :cond_8c

    .line 193
    :try_start_89
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_8c
    .catch Ljava/io/IOException; {:try_start_89 .. :try_end_8c} :catch_21c

    .line 198
    :cond_8c
    :goto_8c
    if-eqz v9, :cond_91

    .line 200
    :try_start_8e
    invoke-virtual {v9}, Lnet/lingala/zip4j/io/ZipOutputStream;->close()V
    :try_end_91
    .catch Ljava/io/IOException; {:try_start_8e .. :try_end_91} :catch_21f

    .line 202
    :cond_91
    :goto_91
    throw v14

    .line 120
    .end local v9    # "outputStream":Lnet/lingala/zip4j/io/ZipOutputStream;
    .restart local v8    # "isZipFileAlreadExists":Z
    .restart local v10    # "outputStream":Lnet/lingala/zip4j/io/ZipOutputStream;
    .restart local v13    # "splitOutputStream":Lnet/lingala/zip4j/io/SplitOutputStream;
    :cond_92
    :try_start_92
    move-object/from16 v0, p0

    iget-object v14, v0, Lnet/lingala/zip4j/zip/ZipEngine;->zipModel:Lnet/lingala/zip4j/model/ZipModel;

    invoke-virtual {v14}, Lnet/lingala/zip4j/model/ZipModel;->getEndCentralDirRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;

    move-result-object v14

    invoke-virtual {v14}, Lnet/lingala/zip4j/model/EndCentralDirRecord;->getOffsetOfStartOfCentralDir()J

    move-result-wide v14

    invoke-virtual {v13, v14, v15}, Lnet/lingala/zip4j/io/SplitOutputStream;->seek(J)V

    .line 122
    :cond_a1
    const/16 v14, 0x1000

    new-array v11, v14, [B
    :try_end_a5
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_92 .. :try_end_a5} :catch_7e
    .catch Ljava/lang/Exception; {:try_start_92 .. :try_end_a5} :catch_1dc
    .catchall {:try_start_92 .. :try_end_a5} :catchall_1f3

    .line 123
    .local v11, "readBuff":[B
    const/4 v12, -0x1

    .line 124
    .local v12, "readLen":I
    const/4 v5, 0x0

    .local v5, "i":I
    move-object v7, v6

    .end local v6    # "inputStream":Ljava/io/InputStream;
    .local v7, "inputStream":Ljava/io/InputStream;
    :goto_a8
    :try_start_a8
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v14

    if-ge v5, v14, :cond_1f7

    .line 126
    invoke-virtual/range {p3 .. p3}, Lnet/lingala/zip4j/progress/ProgressMonitor;->isCancelAllTasks()Z

    move-result v14

    if-eqz v14, :cond_cc

    .line 127
    const/4 v14, 0x3

    move-object/from16 v0, p3

    invoke-virtual {v0, v14}, Lnet/lingala/zip4j/progress/ProgressMonitor;->setResult(I)V

    .line 128
    const/4 v14, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v14}, Lnet/lingala/zip4j/progress/ProgressMonitor;->setState(I)V
    :try_end_c0
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_a8 .. :try_end_c0} :catch_230
    .catch Ljava/lang/Exception; {:try_start_a8 .. :try_end_c0} :catch_229
    .catchall {:try_start_a8 .. :try_end_c0} :catchall_222

    .line 191
    if-eqz v7, :cond_c5

    .line 193
    :try_start_c2
    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_c5
    .catch Ljava/io/IOException; {:try_start_c2 .. :try_end_c5} :catch_20a

    .line 198
    :cond_c5
    :goto_c5
    if-eqz v10, :cond_ca

    .line 200
    :try_start_c7
    invoke-virtual {v10}, Lnet/lingala/zip4j/io/ZipOutputStream;->close()V
    :try_end_ca
    .catch Ljava/io/IOException; {:try_start_c7 .. :try_end_ca} :catch_20d

    :cond_ca
    :goto_ca
    move-object v6, v7

    .line 205
    .end local v7    # "inputStream":Ljava/io/InputStream;
    .restart local v6    # "inputStream":Ljava/io/InputStream;
    :cond_cb
    :goto_cb
    return-void

    .line 132
    .end local v6    # "inputStream":Ljava/io/InputStream;
    .restart local v7    # "inputStream":Ljava/io/InputStream;
    :cond_cc
    :try_start_cc
    invoke-virtual/range {p2 .. p2}, Lnet/lingala/zip4j/model/ZipParameters;->clone()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lnet/lingala/zip4j/model/ZipParameters;

    .line 134
    .local v4, "fileParameters":Lnet/lingala/zip4j/model/ZipParameters;
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/io/File;

    invoke-virtual {v14}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p3

    invoke-virtual {v0, v14}, Lnet/lingala/zip4j/progress/ProgressMonitor;->setFileName(Ljava/lang/String;)V

    .line 136
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/io/File;

    invoke-virtual {v14}, Ljava/io/File;->isDirectory()Z

    move-result v14

    if-nez v14, :cond_153

    .line 137
    invoke-virtual {v4}, Lnet/lingala/zip4j/model/ZipParameters;->isEncryptFiles()Z

    move-result v14

    if-eqz v14, :cond_13d

    invoke-virtual {v4}, Lnet/lingala/zip4j/model/ZipParameters;->getEncryptionMethod()I

    move-result v14

    if-nez v14, :cond_13d

    .line 138
    const/4 v14, 0x3

    move-object/from16 v0, p3

    invoke-virtual {v0, v14}, Lnet/lingala/zip4j/progress/ProgressMonitor;->setCurrentOperation(I)V

    .line 139
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/io/File;

    invoke-virtual {v14}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p3

    invoke-static {v14, v0}, Lnet/lingala/zip4j/util/CRCUtil;->computeFileCRC(Ljava/lang/String;Lnet/lingala/zip4j/progress/ProgressMonitor;)J

    move-result-wide v14

    long-to-int v14, v14

    invoke-virtual {v4, v14}, Lnet/lingala/zip4j/model/ZipParameters;->setSourceFileCRC(I)V

    .line 140
    const/4 v14, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v14}, Lnet/lingala/zip4j/progress/ProgressMonitor;->setCurrentOperation(I)V

    .line 142
    invoke-virtual/range {p3 .. p3}, Lnet/lingala/zip4j/progress/ProgressMonitor;->isCancelAllTasks()Z

    move-result v14

    if-eqz v14, :cond_13d

    .line 143
    const/4 v14, 0x3

    move-object/from16 v0, p3

    invoke-virtual {v0, v14}, Lnet/lingala/zip4j/progress/ProgressMonitor;->setResult(I)V

    .line 144
    const/4 v14, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v14}, Lnet/lingala/zip4j/progress/ProgressMonitor;->setState(I)V
    :try_end_131
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_cc .. :try_end_131} :catch_230
    .catch Ljava/lang/Exception; {:try_start_cc .. :try_end_131} :catch_229
    .catchall {:try_start_cc .. :try_end_131} :catchall_222

    .line 191
    if-eqz v7, :cond_136

    .line 193
    :try_start_133
    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_136
    .catch Ljava/io/IOException; {:try_start_133 .. :try_end_136} :catch_210

    .line 198
    :cond_136
    :goto_136
    if-eqz v10, :cond_13b

    .line 200
    :try_start_138
    invoke-virtual {v10}, Lnet/lingala/zip4j/io/ZipOutputStream;->close()V
    :try_end_13b
    .catch Ljava/io/IOException; {:try_start_138 .. :try_end_13b} :catch_213

    :cond_13b
    :goto_13b
    move-object v6, v7

    .line 145
    .end local v7    # "inputStream":Ljava/io/InputStream;
    .restart local v6    # "inputStream":Ljava/io/InputStream;
    goto :goto_cb

    .line 149
    .end local v6    # "inputStream":Ljava/io/InputStream;
    .restart local v7    # "inputStream":Ljava/io/InputStream;
    :cond_13d
    :try_start_13d
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/io/File;

    invoke-static {v14}, Lnet/lingala/zip4j/util/Zip4jUtil;->getFileLengh(Ljava/io/File;)J

    move-result-wide v14

    const-wide/16 v16, 0x0

    cmp-long v14, v14, v16

    if-nez v14, :cond_153

    .line 150
    const/4 v14, 0x0

    invoke-virtual {v4, v14}, Lnet/lingala/zip4j/model/ZipParameters;->setCompressionMethod(I)V

    .line 153
    :cond_153
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/io/File;

    .line 154
    .local v3, "f":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->isFile()Z

    move-result v14

    if-nez v14, :cond_16d

    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v14

    if-nez v14, :cond_16d

    move-object v6, v7

    .line 124
    .end local v7    # "inputStream":Ljava/io/InputStream;
    .restart local v6    # "inputStream":Ljava/io/InputStream;
    :cond_168
    :goto_168
    add-int/lit8 v5, v5, 0x1

    move-object v7, v6

    .end local v6    # "inputStream":Ljava/io/InputStream;
    .restart local v7    # "inputStream":Ljava/io/InputStream;
    goto/16 :goto_a8

    .line 155
    :cond_16d
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/io/File;

    invoke-virtual {v10, v14, v4}, Lnet/lingala/zip4j/io/ZipOutputStream;->putNextEntry(Ljava/io/File;Lnet/lingala/zip4j/model/ZipParameters;)V

    .line 156
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/io/File;

    invoke-virtual {v14}, Ljava/io/File;->isDirectory()Z

    move-result v14

    if-eqz v14, :cond_18b

    .line 157
    invoke-virtual {v10}, Lnet/lingala/zip4j/io/ZipOutputStream;->closeEntry()V

    move-object v6, v7

    .line 158
    .end local v7    # "inputStream":Ljava/io/InputStream;
    .restart local v6    # "inputStream":Ljava/io/InputStream;
    goto :goto_168

    .line 160
    .end local v6    # "inputStream":Ljava/io/InputStream;
    .restart local v7    # "inputStream":Ljava/io/InputStream;
    :cond_18b
    sget-object v15, Ljava/lang/System;->out:Ljava/io/PrintStream;

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/io/File;

    invoke-virtual {v14}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v15, v14}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 162
    new-instance v6, Ljava/io/FileInputStream;

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/io/File;

    invoke-direct {v6, v14}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_1a9
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_13d .. :try_end_1a9} :catch_230
    .catch Ljava/lang/Exception; {:try_start_13d .. :try_end_1a9} :catch_229
    .catchall {:try_start_13d .. :try_end_1a9} :catchall_222

    .line 164
    .end local v7    # "inputStream":Ljava/io/InputStream;
    .restart local v6    # "inputStream":Ljava/io/InputStream;
    :goto_1a9
    :try_start_1a9
    invoke-virtual {v6, v11}, Ljava/io/InputStream;->read([B)I

    move-result v12

    const/4 v14, -0x1

    if-eq v12, v14, :cond_1e9

    .line 165
    invoke-virtual/range {p3 .. p3}, Lnet/lingala/zip4j/progress/ProgressMonitor;->isCancelAllTasks()Z

    move-result v14

    if-eqz v14, :cond_1d1

    .line 166
    const/4 v14, 0x3

    move-object/from16 v0, p3

    invoke-virtual {v0, v14}, Lnet/lingala/zip4j/progress/ProgressMonitor;->setResult(I)V

    .line 167
    const/4 v14, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v14}, Lnet/lingala/zip4j/progress/ProgressMonitor;->setState(I)V
    :try_end_1c2
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_1a9 .. :try_end_1c2} :catch_7e
    .catch Ljava/lang/Exception; {:try_start_1a9 .. :try_end_1c2} :catch_1dc
    .catchall {:try_start_1a9 .. :try_end_1c2} :catchall_1f3

    .line 191
    if-eqz v6, :cond_1c7

    .line 193
    :try_start_1c4
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_1c7
    .catch Ljava/io/IOException; {:try_start_1c4 .. :try_end_1c7} :catch_216

    .line 198
    :cond_1c7
    :goto_1c7
    if-eqz v10, :cond_cb

    .line 200
    :try_start_1c9
    invoke-virtual {v10}, Lnet/lingala/zip4j/io/ZipOutputStream;->close()V
    :try_end_1cc
    .catch Ljava/io/IOException; {:try_start_1c9 .. :try_end_1cc} :catch_1ce

    goto/16 :goto_cb

    .line 201
    :catch_1ce
    move-exception v14

    goto/16 :goto_cb

    .line 171
    :cond_1d1
    const/4 v14, 0x0

    :try_start_1d2
    invoke-virtual {v10, v11, v14, v12}, Lnet/lingala/zip4j/io/ZipOutputStream;->write([BII)V

    .line 172
    int-to-long v14, v12

    move-object/from16 v0, p3

    invoke-virtual {v0, v14, v15}, Lnet/lingala/zip4j/progress/ProgressMonitor;->updateWorkCompleted(J)V
    :try_end_1db
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_1d2 .. :try_end_1db} :catch_7e
    .catch Ljava/lang/Exception; {:try_start_1d2 .. :try_end_1db} :catch_1dc
    .catchall {:try_start_1d2 .. :try_end_1db} :catchall_1f3

    goto :goto_1a9

    .line 187
    .end local v3    # "f":Ljava/io/File;
    .end local v4    # "fileParameters":Lnet/lingala/zip4j/model/ZipParameters;
    .end local v5    # "i":I
    .end local v11    # "readBuff":[B
    .end local v12    # "readLen":I
    :catch_1dc
    move-exception v2

    move-object v9, v10

    .line 188
    .end local v8    # "isZipFileAlreadExists":Z
    .end local v10    # "outputStream":Lnet/lingala/zip4j/io/ZipOutputStream;
    .end local v13    # "splitOutputStream":Lnet/lingala/zip4j/io/SplitOutputStream;
    .local v2, "e":Ljava/lang/Exception;
    .restart local v9    # "outputStream":Lnet/lingala/zip4j/io/ZipOutputStream;
    :goto_1de
    :try_start_1de
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Lnet/lingala/zip4j/progress/ProgressMonitor;->endProgressMonitorError(Ljava/lang/Throwable;)V

    .line 189
    new-instance v14, Lnet/lingala/zip4j/exception/ZipException;

    invoke-direct {v14, v2}, Lnet/lingala/zip4j/exception/ZipException;-><init>(Ljava/lang/Throwable;)V

    throw v14
    :try_end_1e9
    .catchall {:try_start_1de .. :try_end_1e9} :catchall_86

    .line 175
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v9    # "outputStream":Lnet/lingala/zip4j/io/ZipOutputStream;
    .restart local v3    # "f":Ljava/io/File;
    .restart local v4    # "fileParameters":Lnet/lingala/zip4j/model/ZipParameters;
    .restart local v5    # "i":I
    .restart local v8    # "isZipFileAlreadExists":Z
    .restart local v10    # "outputStream":Lnet/lingala/zip4j/io/ZipOutputStream;
    .restart local v11    # "readBuff":[B
    .restart local v12    # "readLen":I
    .restart local v13    # "splitOutputStream":Lnet/lingala/zip4j/io/SplitOutputStream;
    :cond_1e9
    :try_start_1e9
    invoke-virtual {v10}, Lnet/lingala/zip4j/io/ZipOutputStream;->closeEntry()V

    .line 177
    if-eqz v6, :cond_168

    .line 178
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_1f1
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_1e9 .. :try_end_1f1} :catch_7e
    .catch Ljava/lang/Exception; {:try_start_1e9 .. :try_end_1f1} :catch_1dc
    .catchall {:try_start_1e9 .. :try_end_1f1} :catchall_1f3

    goto/16 :goto_168

    .line 191
    .end local v3    # "f":Ljava/io/File;
    .end local v4    # "fileParameters":Lnet/lingala/zip4j/model/ZipParameters;
    .end local v5    # "i":I
    .end local v11    # "readBuff":[B
    .end local v12    # "readLen":I
    :catchall_1f3
    move-exception v14

    move-object v9, v10

    .end local v10    # "outputStream":Lnet/lingala/zip4j/io/ZipOutputStream;
    .restart local v9    # "outputStream":Lnet/lingala/zip4j/io/ZipOutputStream;
    goto/16 :goto_87

    .line 182
    .end local v6    # "inputStream":Ljava/io/InputStream;
    .end local v9    # "outputStream":Lnet/lingala/zip4j/io/ZipOutputStream;
    .restart local v5    # "i":I
    .restart local v7    # "inputStream":Ljava/io/InputStream;
    .restart local v10    # "outputStream":Lnet/lingala/zip4j/io/ZipOutputStream;
    .restart local v11    # "readBuff":[B
    .restart local v12    # "readLen":I
    :cond_1f7
    :try_start_1f7
    invoke-virtual {v10}, Lnet/lingala/zip4j/io/ZipOutputStream;->finish()V

    .line 183
    invoke-virtual/range {p3 .. p3}, Lnet/lingala/zip4j/progress/ProgressMonitor;->endProgressMonitorSuccess()V
    :try_end_1fd
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_1f7 .. :try_end_1fd} :catch_230
    .catch Ljava/lang/Exception; {:try_start_1f7 .. :try_end_1fd} :catch_229
    .catchall {:try_start_1f7 .. :try_end_1fd} :catchall_222

    .line 191
    if-eqz v7, :cond_202

    .line 193
    :try_start_1ff
    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_202
    .catch Ljava/io/IOException; {:try_start_1ff .. :try_end_202} :catch_218

    .line 198
    :cond_202
    :goto_202
    if-eqz v10, :cond_207

    .line 200
    :try_start_204
    invoke-virtual {v10}, Lnet/lingala/zip4j/io/ZipOutputStream;->close()V
    :try_end_207
    .catch Ljava/io/IOException; {:try_start_204 .. :try_end_207} :catch_21a

    :cond_207
    :goto_207
    move-object v6, v7

    .line 205
    .end local v7    # "inputStream":Ljava/io/InputStream;
    .restart local v6    # "inputStream":Ljava/io/InputStream;
    goto/16 :goto_cb

    .line 194
    .end local v6    # "inputStream":Ljava/io/InputStream;
    .restart local v7    # "inputStream":Ljava/io/InputStream;
    :catch_20a
    move-exception v14

    goto/16 :goto_c5

    .line 201
    :catch_20d
    move-exception v14

    goto/16 :goto_ca

    .line 194
    .restart local v4    # "fileParameters":Lnet/lingala/zip4j/model/ZipParameters;
    :catch_210
    move-exception v14

    goto/16 :goto_136

    .line 201
    :catch_213
    move-exception v14

    goto/16 :goto_13b

    .line 194
    .end local v7    # "inputStream":Ljava/io/InputStream;
    .restart local v3    # "f":Ljava/io/File;
    .restart local v6    # "inputStream":Ljava/io/InputStream;
    :catch_216
    move-exception v14

    goto :goto_1c7

    .end local v3    # "f":Ljava/io/File;
    .end local v4    # "fileParameters":Lnet/lingala/zip4j/model/ZipParameters;
    .end local v6    # "inputStream":Ljava/io/InputStream;
    .restart local v7    # "inputStream":Ljava/io/InputStream;
    :catch_218
    move-exception v14

    goto :goto_202

    .line 201
    :catch_21a
    move-exception v14

    goto :goto_207

    .line 194
    .end local v5    # "i":I
    .end local v7    # "inputStream":Ljava/io/InputStream;
    .end local v8    # "isZipFileAlreadExists":Z
    .end local v10    # "outputStream":Lnet/lingala/zip4j/io/ZipOutputStream;
    .end local v11    # "readBuff":[B
    .end local v12    # "readLen":I
    .end local v13    # "splitOutputStream":Lnet/lingala/zip4j/io/SplitOutputStream;
    .restart local v6    # "inputStream":Ljava/io/InputStream;
    .restart local v9    # "outputStream":Lnet/lingala/zip4j/io/ZipOutputStream;
    :catch_21c
    move-exception v15

    goto/16 :goto_8c

    .line 201
    :catch_21f
    move-exception v15

    goto/16 :goto_91

    .line 191
    .end local v6    # "inputStream":Ljava/io/InputStream;
    .end local v9    # "outputStream":Lnet/lingala/zip4j/io/ZipOutputStream;
    .restart local v5    # "i":I
    .restart local v7    # "inputStream":Ljava/io/InputStream;
    .restart local v8    # "isZipFileAlreadExists":Z
    .restart local v10    # "outputStream":Lnet/lingala/zip4j/io/ZipOutputStream;
    .restart local v11    # "readBuff":[B
    .restart local v12    # "readLen":I
    .restart local v13    # "splitOutputStream":Lnet/lingala/zip4j/io/SplitOutputStream;
    :catchall_222
    move-exception v14

    move-object v6, v7

    .end local v7    # "inputStream":Ljava/io/InputStream;
    .restart local v6    # "inputStream":Ljava/io/InputStream;
    move-object v9, v10

    .end local v10    # "outputStream":Lnet/lingala/zip4j/io/ZipOutputStream;
    .restart local v9    # "outputStream":Lnet/lingala/zip4j/io/ZipOutputStream;
    goto/16 :goto_87

    .line 187
    .end local v5    # "i":I
    .end local v8    # "isZipFileAlreadExists":Z
    .end local v11    # "readBuff":[B
    .end local v12    # "readLen":I
    .end local v13    # "splitOutputStream":Lnet/lingala/zip4j/io/SplitOutputStream;
    :catch_227
    move-exception v2

    goto :goto_1de

    .end local v6    # "inputStream":Ljava/io/InputStream;
    .end local v9    # "outputStream":Lnet/lingala/zip4j/io/ZipOutputStream;
    .restart local v5    # "i":I
    .restart local v7    # "inputStream":Ljava/io/InputStream;
    .restart local v8    # "isZipFileAlreadExists":Z
    .restart local v10    # "outputStream":Lnet/lingala/zip4j/io/ZipOutputStream;
    .restart local v11    # "readBuff":[B
    .restart local v12    # "readLen":I
    .restart local v13    # "splitOutputStream":Lnet/lingala/zip4j/io/SplitOutputStream;
    :catch_229
    move-exception v2

    move-object v6, v7

    .end local v7    # "inputStream":Ljava/io/InputStream;
    .restart local v6    # "inputStream":Ljava/io/InputStream;
    move-object v9, v10

    .end local v10    # "outputStream":Lnet/lingala/zip4j/io/ZipOutputStream;
    .restart local v9    # "outputStream":Lnet/lingala/zip4j/io/ZipOutputStream;
    goto :goto_1de

    .line 184
    .end local v5    # "i":I
    .end local v8    # "isZipFileAlreadExists":Z
    .end local v11    # "readBuff":[B
    .end local v12    # "readLen":I
    .end local v13    # "splitOutputStream":Lnet/lingala/zip4j/io/SplitOutputStream;
    :catch_22d
    move-exception v2

    goto/16 :goto_80

    .end local v6    # "inputStream":Ljava/io/InputStream;
    .end local v9    # "outputStream":Lnet/lingala/zip4j/io/ZipOutputStream;
    .restart local v5    # "i":I
    .restart local v7    # "inputStream":Ljava/io/InputStream;
    .restart local v8    # "isZipFileAlreadExists":Z
    .restart local v10    # "outputStream":Lnet/lingala/zip4j/io/ZipOutputStream;
    .restart local v11    # "readBuff":[B
    .restart local v12    # "readLen":I
    .restart local v13    # "splitOutputStream":Lnet/lingala/zip4j/io/SplitOutputStream;
    :catch_230
    move-exception v2

    move-object v6, v7

    .end local v7    # "inputStream":Ljava/io/InputStream;
    .restart local v6    # "inputStream":Ljava/io/InputStream;
    move-object v9, v10

    .end local v10    # "outputStream":Lnet/lingala/zip4j/io/ZipOutputStream;
    .restart local v9    # "outputStream":Lnet/lingala/zip4j/io/ZipOutputStream;
    goto/16 :goto_80
.end method

.method private prepareFileOutputStream()Ljava/io/RandomAccessFile;
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/lingala/zip4j/exception/ZipException;
        }
    .end annotation

    .prologue
    .line 428
    iget-object v3, p0, Lnet/lingala/zip4j/zip/ZipEngine;->zipModel:Lnet/lingala/zip4j/model/ZipModel;

    invoke-virtual {v3}, Lnet/lingala/zip4j/model/ZipModel;->getZipFile()Ljava/lang/String;

    move-result-object v2

    .line 429
    .local v2, "outPath":Ljava/lang/String;
    invoke-static {v2}, Lnet/lingala/zip4j/util/Zip4jUtil;->isStringNotNullAndNotEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_14

    .line 430
    new-instance v3, Lnet/lingala/zip4j/exception/ZipException;

    const-string v4, "invalid output path"

    invoke-direct {v3, v4}, Lnet/lingala/zip4j/exception/ZipException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 434
    :cond_14
    :try_start_14
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 435
    .local v1, "outFile":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_2a

    .line 436
    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->mkdirs()Z

    .line 438
    :cond_2a
    new-instance v3, Ljava/io/RandomAccessFile;

    const-string v4, "rw"

    invoke-direct {v3, v1, v4}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_31
    .catch Ljava/io/FileNotFoundException; {:try_start_14 .. :try_end_31} :catch_32

    return-object v3

    .line 439
    .end local v1    # "outFile":Ljava/io/File;
    :catch_32
    move-exception v0

    .line 440
    .local v0, "e":Ljava/io/FileNotFoundException;
    new-instance v3, Lnet/lingala/zip4j/exception/ZipException;

    invoke-direct {v3, v0}, Lnet/lingala/zip4j/exception/ZipException;-><init>(Ljava/lang/Throwable;)V

    throw v3
.end method

.method private removeFilesIfExists(Ljava/util/ArrayList;Lnet/lingala/zip4j/model/ZipParameters;Lnet/lingala/zip4j/progress/ProgressMonitor;)V
    .registers 18
    .param p1, "fileList"    # Ljava/util/ArrayList;
    .param p2, "parameters"    # Lnet/lingala/zip4j/model/ZipParameters;
    .param p3, "progressMonitor"    # Lnet/lingala/zip4j/progress/ProgressMonitor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/lingala/zip4j/exception/ZipException;
        }
    .end annotation

    .prologue
    .line 349
    iget-object v11, p0, Lnet/lingala/zip4j/zip/ZipEngine;->zipModel:Lnet/lingala/zip4j/model/ZipModel;

    if-eqz v11, :cond_28

    iget-object v11, p0, Lnet/lingala/zip4j/zip/ZipEngine;->zipModel:Lnet/lingala/zip4j/model/ZipModel;

    invoke-virtual {v11}, Lnet/lingala/zip4j/model/ZipModel;->getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;

    move-result-object v11

    if-eqz v11, :cond_28

    iget-object v11, p0, Lnet/lingala/zip4j/zip/ZipEngine;->zipModel:Lnet/lingala/zip4j/model/ZipModel;

    .line 350
    invoke-virtual {v11}, Lnet/lingala/zip4j/model/ZipModel;->getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;

    move-result-object v11

    invoke-virtual {v11}, Lnet/lingala/zip4j/model/CentralDirectory;->getFileHeaders()Ljava/util/ArrayList;

    move-result-object v11

    if-eqz v11, :cond_28

    iget-object v11, p0, Lnet/lingala/zip4j/zip/ZipEngine;->zipModel:Lnet/lingala/zip4j/model/ZipModel;

    .line 351
    invoke-virtual {v11}, Lnet/lingala/zip4j/model/ZipModel;->getCentralDirectory()Lnet/lingala/zip4j/model/CentralDirectory;

    move-result-object v11

    invoke-virtual {v11}, Lnet/lingala/zip4j/model/CentralDirectory;->getFileHeaders()Ljava/util/ArrayList;

    move-result-object v11

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v11

    if-gtz v11, :cond_29

    .line 425
    :cond_28
    :goto_28
    return-void

    .line 355
    :cond_29
    const/4 v9, 0x0

    .line 358
    .local v9, "outputStream":Ljava/io/RandomAccessFile;
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_2b
    :try_start_2b
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v11

    if-ge v6, v11, :cond_d3

    .line 359
    invoke-virtual {p1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/io/File;

    .line 361
    .local v3, "file":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v11

    .line 362
    invoke-virtual/range {p2 .. p2}, Lnet/lingala/zip4j/model/ZipParameters;->getRootFolderInZip()Ljava/lang/String;

    move-result-object v12

    invoke-virtual/range {p2 .. p2}, Lnet/lingala/zip4j/model/ZipParameters;->getDefaultFolderPath()Ljava/lang/String;

    move-result-object v13

    .line 361
    invoke-static {v11, v12, v13}, Lnet/lingala/zip4j/util/Zip4jUtil;->getRelativeFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 364
    .local v5, "fileName":Ljava/lang/String;
    iget-object v11, p0, Lnet/lingala/zip4j/zip/ZipEngine;->zipModel:Lnet/lingala/zip4j/model/ZipModel;

    invoke-static {v11, v5}, Lnet/lingala/zip4j/util/Zip4jUtil;->getFileHeader(Lnet/lingala/zip4j/model/ZipModel;Ljava/lang/String;)Lnet/lingala/zip4j/model/FileHeader;

    move-result-object v4

    .line 365
    .local v4, "fileHeader":Lnet/lingala/zip4j/model/FileHeader;
    if-eqz v4, :cond_af

    .line 367
    if-eqz v9, :cond_55

    .line 368
    invoke-virtual {v9}, Ljava/io/RandomAccessFile;->close()V

    .line 369
    const/4 v9, 0x0

    .line 372
    :cond_55
    new-instance v1, Lnet/lingala/zip4j/util/ArchiveMaintainer;

    invoke-direct {v1}, Lnet/lingala/zip4j/util/ArchiveMaintainer;-><init>()V

    .line 373
    .local v1, "archiveMaintainer":Lnet/lingala/zip4j/util/ArchiveMaintainer;
    const/4 v11, 0x2

    move-object/from16 v0, p3

    invoke-virtual {v0, v11}, Lnet/lingala/zip4j/progress/ProgressMonitor;->setCurrentOperation(I)V

    .line 374
    iget-object v11, p0, Lnet/lingala/zip4j/zip/ZipEngine;->zipModel:Lnet/lingala/zip4j/model/ZipModel;

    move-object/from16 v0, p3

    invoke-virtual {v1, v11, v4, v0}, Lnet/lingala/zip4j/util/ArchiveMaintainer;->initRemoveZipFile(Lnet/lingala/zip4j/model/ZipModel;Lnet/lingala/zip4j/model/FileHeader;Lnet/lingala/zip4j/progress/ProgressMonitor;)Ljava/util/HashMap;

    move-result-object v10

    .line 377
    .local v10, "retMap":Ljava/util/HashMap;
    invoke-virtual/range {p3 .. p3}, Lnet/lingala/zip4j/progress/ProgressMonitor;->isCancelAllTasks()Z

    move-result v11

    if-eqz v11, :cond_82

    .line 378
    const/4 v11, 0x3

    move-object/from16 v0, p3

    invoke-virtual {v0, v11}, Lnet/lingala/zip4j/progress/ProgressMonitor;->setResult(I)V

    .line 379
    const/4 v11, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v11}, Lnet/lingala/zip4j/progress/ProgressMonitor;->setState(I)V
    :try_end_7a
    .catch Ljava/io/IOException; {:try_start_2b .. :try_end_7a} :catch_bc
    .catchall {:try_start_2b .. :try_end_7a} :catchall_c3

    .line 417
    if-eqz v9, :cond_28

    .line 419
    :try_start_7c
    invoke-virtual {v9}, Ljava/io/RandomAccessFile;->close()V
    :try_end_7f
    .catch Ljava/io/IOException; {:try_start_7c .. :try_end_7f} :catch_80

    goto :goto_28

    .line 420
    :catch_80
    move-exception v11

    goto :goto_28

    .line 383
    :cond_82
    const/4 v11, 0x0

    .line 384
    :try_start_83
    move-object/from16 v0, p3

    invoke-virtual {v0, v11}, Lnet/lingala/zip4j/progress/ProgressMonitor;->setCurrentOperation(I)V

    .line 386
    if-nez v9, :cond_af

    .line 387
    invoke-direct {p0}, Lnet/lingala/zip4j/zip/ZipEngine;->prepareFileOutputStream()Ljava/io/RandomAccessFile;

    move-result-object v9

    .line 389
    if-eqz v10, :cond_af

    .line 390
    const-string v11, "offsetCentralDir"

    invoke-virtual {v10, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_95
    .catch Ljava/io/IOException; {:try_start_83 .. :try_end_95} :catch_bc
    .catchall {:try_start_83 .. :try_end_95} :catchall_c3

    move-result-object v11

    if-eqz v11, :cond_af

    .line 391
    const-wide/16 v7, -0x1

    .line 393
    .local v7, "offsetCentralDir":J
    :try_start_9a
    const-string v11, "offsetCentralDir"

    .line 395
    invoke-virtual {v10, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    .line 394
    invoke-static {v11}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_a5
    .catch Ljava/lang/NumberFormatException; {:try_start_9a .. :try_end_a5} :catch_b3
    .catch Ljava/lang/Exception; {:try_start_9a .. :try_end_a5} :catch_ca
    .catch Ljava/io/IOException; {:try_start_9a .. :try_end_a5} :catch_bc
    .catchall {:try_start_9a .. :try_end_a5} :catchall_c3

    move-result-wide v7

    .line 406
    const-wide/16 v11, 0x0

    cmp-long v11, v7, v11

    if-ltz v11, :cond_af

    .line 407
    :try_start_ac
    invoke-virtual {v9, v7, v8}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 358
    .end local v1    # "archiveMaintainer":Lnet/lingala/zip4j/util/ArchiveMaintainer;
    .end local v7    # "offsetCentralDir":J
    .end local v10    # "retMap":Ljava/util/HashMap;
    :cond_af
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_2b

    .line 396
    .restart local v1    # "archiveMaintainer":Lnet/lingala/zip4j/util/ArchiveMaintainer;
    .restart local v7    # "offsetCentralDir":J
    .restart local v10    # "retMap":Ljava/util/HashMap;
    :catch_b3
    move-exception v2

    .line 397
    .local v2, "e":Ljava/lang/NumberFormatException;
    new-instance v11, Lnet/lingala/zip4j/exception/ZipException;

    const-string v12, "NumberFormatException while parsing offset central directory. Cannot update already existing file header"

    invoke-direct {v11, v12}, Lnet/lingala/zip4j/exception/ZipException;-><init>(Ljava/lang/String;)V

    throw v11
    :try_end_bc
    .catch Ljava/io/IOException; {:try_start_ac .. :try_end_bc} :catch_bc
    .catchall {:try_start_ac .. :try_end_bc} :catchall_c3

    .line 414
    .end local v1    # "archiveMaintainer":Lnet/lingala/zip4j/util/ArchiveMaintainer;
    .end local v2    # "e":Ljava/lang/NumberFormatException;
    .end local v3    # "file":Ljava/io/File;
    .end local v4    # "fileHeader":Lnet/lingala/zip4j/model/FileHeader;
    .end local v5    # "fileName":Ljava/lang/String;
    .end local v7    # "offsetCentralDir":J
    .end local v10    # "retMap":Ljava/util/HashMap;
    :catch_bc
    move-exception v2

    .line 415
    .local v2, "e":Ljava/io/IOException;
    :try_start_bd
    new-instance v11, Lnet/lingala/zip4j/exception/ZipException;

    invoke-direct {v11, v2}, Lnet/lingala/zip4j/exception/ZipException;-><init>(Ljava/lang/Throwable;)V

    throw v11
    :try_end_c3
    .catchall {:try_start_bd .. :try_end_c3} :catchall_c3

    .line 417
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_c3
    move-exception v11

    if-eqz v9, :cond_c9

    .line 419
    :try_start_c6
    invoke-virtual {v9}, Ljava/io/RandomAccessFile;->close()V
    :try_end_c9
    .catch Ljava/io/IOException; {:try_start_c6 .. :try_end_c9} :catch_dd

    .line 422
    :cond_c9
    :goto_c9
    throw v11

    .line 400
    .restart local v1    # "archiveMaintainer":Lnet/lingala/zip4j/util/ArchiveMaintainer;
    .restart local v3    # "file":Ljava/io/File;
    .restart local v4    # "fileHeader":Lnet/lingala/zip4j/model/FileHeader;
    .restart local v5    # "fileName":Ljava/lang/String;
    .restart local v7    # "offsetCentralDir":J
    .restart local v10    # "retMap":Ljava/util/HashMap;
    :catch_ca
    move-exception v2

    .line 401
    .local v2, "e":Ljava/lang/Exception;
    :try_start_cb
    new-instance v11, Lnet/lingala/zip4j/exception/ZipException;

    const-string v12, "Error while parsing offset central directory. Cannot update already existing file header"

    invoke-direct {v11, v12}, Lnet/lingala/zip4j/exception/ZipException;-><init>(Ljava/lang/String;)V

    throw v11
    :try_end_d3
    .catch Ljava/io/IOException; {:try_start_cb .. :try_end_d3} :catch_bc
    .catchall {:try_start_cb .. :try_end_d3} :catchall_c3

    .line 417
    .end local v1    # "archiveMaintainer":Lnet/lingala/zip4j/util/ArchiveMaintainer;
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v3    # "file":Ljava/io/File;
    .end local v4    # "fileHeader":Lnet/lingala/zip4j/model/FileHeader;
    .end local v5    # "fileName":Ljava/lang/String;
    .end local v7    # "offsetCentralDir":J
    .end local v10    # "retMap":Ljava/util/HashMap;
    :cond_d3
    if-eqz v9, :cond_28

    .line 419
    :try_start_d5
    invoke-virtual {v9}, Ljava/io/RandomAccessFile;->close()V
    :try_end_d8
    .catch Ljava/io/IOException; {:try_start_d5 .. :try_end_d8} :catch_da

    goto/16 :goto_28

    .line 420
    :catch_da
    move-exception v11

    goto/16 :goto_28

    :catch_dd
    move-exception v12

    goto :goto_c9
.end method


# virtual methods
.method public addFiles(Ljava/util/ArrayList;Lnet/lingala/zip4j/model/ZipParameters;Lnet/lingala/zip4j/progress/ProgressMonitor;Z)V
    .registers 11
    .param p1, "fileList"    # Ljava/util/ArrayList;
    .param p2, "parameters"    # Lnet/lingala/zip4j/model/ZipParameters;
    .param p3, "progressMonitor"    # Lnet/lingala/zip4j/progress/ProgressMonitor;
    .param p4, "runInThread"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/lingala/zip4j/exception/ZipException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 58
    if-eqz p1, :cond_6

    if-nez p2, :cond_e

    .line 59
    :cond_6
    new-instance v1, Lnet/lingala/zip4j/exception/ZipException;

    const-string v2, "one of the input parameters is null when adding files"

    invoke-direct {v1, v2}, Lnet/lingala/zip4j/exception/ZipException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 62
    :cond_e
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-gtz v1, :cond_1c

    .line 63
    new-instance v1, Lnet/lingala/zip4j/exception/ZipException;

    const-string v2, "no files to add"

    invoke-direct {v1, v2}, Lnet/lingala/zip4j/exception/ZipException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 66
    :cond_1c
    invoke-virtual {p3, v3}, Lnet/lingala/zip4j/progress/ProgressMonitor;->setCurrentOperation(I)V

    .line 67
    invoke-virtual {p3, v2}, Lnet/lingala/zip4j/progress/ProgressMonitor;->setState(I)V

    .line 68
    invoke-virtual {p3, v2}, Lnet/lingala/zip4j/progress/ProgressMonitor;->setResult(I)V

    .line 70
    if-eqz p4, :cond_4a

    .line 71
    invoke-direct {p0, p1, p2}, Lnet/lingala/zip4j/zip/ZipEngine;->calculateTotalWork(Ljava/util/ArrayList;Lnet/lingala/zip4j/model/ZipParameters;)J

    move-result-wide v1

    invoke-virtual {p3, v1, v2}, Lnet/lingala/zip4j/progress/ProgressMonitor;->setTotalWork(J)V

    .line 72
    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1}, Lnet/lingala/zip4j/progress/ProgressMonitor;->setFileName(Ljava/lang/String;)V

    .line 74
    new-instance v0, Lnet/lingala/zip4j/zip/ZipEngine$1;

    const-string v2, "Zip4j"

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lnet/lingala/zip4j/zip/ZipEngine$1;-><init>(Lnet/lingala/zip4j/zip/ZipEngine;Ljava/lang/String;Ljava/util/ArrayList;Lnet/lingala/zip4j/model/ZipParameters;Lnet/lingala/zip4j/progress/ProgressMonitor;)V

    .line 82
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 87
    .end local v0    # "thread":Ljava/lang/Thread;
    :goto_49
    return-void

    .line 85
    :cond_4a
    invoke-direct {p0, p1, p2, p3}, Lnet/lingala/zip4j/zip/ZipEngine;->initAddFiles(Ljava/util/ArrayList;Lnet/lingala/zip4j/model/ZipParameters;Lnet/lingala/zip4j/progress/ProgressMonitor;)V

    goto :goto_49
.end method

.method public addFolderToZip(Ljava/io/File;Lnet/lingala/zip4j/model/ZipParameters;Lnet/lingala/zip4j/progress/ProgressMonitor;Z)V
    .registers 10
    .param p1, "file"    # Ljava/io/File;
    .param p2, "parameters"    # Lnet/lingala/zip4j/model/ZipParameters;
    .param p3, "progressMonitor"    # Lnet/lingala/zip4j/progress/ProgressMonitor;
    .param p4, "runInThread"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/lingala/zip4j/exception/ZipException;
        }
    .end annotation

    .prologue
    .line 261
    if-eqz p1, :cond_4

    if-nez p2, :cond_c

    .line 262
    :cond_4
    new-instance v2, Lnet/lingala/zip4j/exception/ZipException;

    const-string v3, "one of the input parameters is null, cannot add folder to zip"

    invoke-direct {v2, v3}, Lnet/lingala/zip4j/exception/ZipException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 265
    :cond_c
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lnet/lingala/zip4j/util/Zip4jUtil;->checkFileExists(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1e

    .line 266
    new-instance v2, Lnet/lingala/zip4j/exception/ZipException;

    const-string v3, "input folder does not exist"

    invoke-direct {v2, v3}, Lnet/lingala/zip4j/exception/ZipException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 269
    :cond_1e
    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-nez v2, :cond_2c

    .line 270
    new-instance v2, Lnet/lingala/zip4j/exception/ZipException;

    const-string v3, "input file is not a folder, user addFileToZip method to add files"

    invoke-direct {v2, v3}, Lnet/lingala/zip4j/exception/ZipException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 273
    :cond_2c
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lnet/lingala/zip4j/util/Zip4jUtil;->checkFileReadAccess(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_53

    .line 274
    new-instance v2, Lnet/lingala/zip4j/exception/ZipException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "cannot read folder: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lnet/lingala/zip4j/exception/ZipException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 277
    :cond_53
    const/4 v1, 0x0

    .line 278
    .local v1, "rootFolderPath":Ljava/lang/String;
    invoke-virtual {p2}, Lnet/lingala/zip4j/model/ZipParameters;->isIncludeRootFolder()Z

    move-result v2

    if-eqz v2, :cond_aa

    .line 279
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_98

    .line 280
    invoke-virtual {p1}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v2

    if-eqz v2, :cond_95

    invoke-virtual {p1}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    .line 288
    :goto_76
    invoke-virtual {p2, v1}, Lnet/lingala/zip4j/model/ZipParameters;->setDefaultFolderPath(Ljava/lang/String;)V

    .line 290
    invoke-virtual {p2}, Lnet/lingala/zip4j/model/ZipParameters;->isReadHiddenFiles()Z

    move-result v2

    invoke-static {p1, v2}, Lnet/lingala/zip4j/util/Zip4jUtil;->getFilesInDirectoryRec(Ljava/io/File;Z)Ljava/util/ArrayList;

    move-result-object v0

    .line 292
    .local v0, "fileList":Ljava/util/ArrayList;
    invoke-virtual {p2}, Lnet/lingala/zip4j/model/ZipParameters;->isIncludeRootFolder()Z

    move-result v2

    if-eqz v2, :cond_91

    .line 293
    if-nez v0, :cond_8e

    .line 294
    new-instance v0, Ljava/util/ArrayList;

    .end local v0    # "fileList":Ljava/util/ArrayList;
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 296
    .restart local v0    # "fileList":Ljava/util/ArrayList;
    :cond_8e
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 299
    :cond_91
    invoke-virtual {p0, v0, p2, p3, p4}, Lnet/lingala/zip4j/zip/ZipEngine;->addFiles(Ljava/util/ArrayList;Lnet/lingala/zip4j/model/ZipParameters;Lnet/lingala/zip4j/progress/ProgressMonitor;Z)V

    .line 300
    return-void

    .line 280
    .end local v0    # "fileList":Ljava/util/ArrayList;
    :cond_95
    const-string v1, ""

    goto :goto_76

    .line 282
    :cond_98
    invoke-virtual {p1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v2

    if-eqz v2, :cond_a7

    invoke-virtual {p1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    :goto_a6
    goto :goto_76

    :cond_a7
    const-string v1, ""

    goto :goto_a6

    .line 285
    :cond_aa
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    goto :goto_76
.end method

.method public addStreamToZip(Ljava/io/InputStream;Lnet/lingala/zip4j/model/ZipParameters;)V
    .registers 13
    .param p1, "inputStream"    # Ljava/io/InputStream;
    .param p2, "parameters"    # Lnet/lingala/zip4j/model/ZipParameters;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/lingala/zip4j/exception/ZipException;
        }
    .end annotation

    .prologue
    .line 208
    if-eqz p1, :cond_4

    if-nez p2, :cond_c

    .line 209
    :cond_4
    new-instance v7, Lnet/lingala/zip4j/exception/ZipException;

    const-string v8, "one of the input parameters is null, cannot add stream to zip"

    invoke-direct {v7, v8}, Lnet/lingala/zip4j/exception/ZipException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 212
    :cond_c
    const/4 v2, 0x0

    .line 215
    .local v2, "outputStream":Lnet/lingala/zip4j/io/ZipOutputStream;
    :try_start_d
    invoke-direct {p0, p2}, Lnet/lingala/zip4j/zip/ZipEngine;->checkParameters(Lnet/lingala/zip4j/model/ZipParameters;)V

    .line 217
    iget-object v7, p0, Lnet/lingala/zip4j/zip/ZipEngine;->zipModel:Lnet/lingala/zip4j/model/ZipModel;

    invoke-virtual {v7}, Lnet/lingala/zip4j/model/ZipModel;->getZipFile()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lnet/lingala/zip4j/util/Zip4jUtil;->checkFileExists(Ljava/lang/String;)Z

    move-result v1

    .line 219
    .local v1, "isZipFileAlreadExists":Z
    new-instance v6, Lnet/lingala/zip4j/io/SplitOutputStream;

    new-instance v7, Ljava/io/File;

    iget-object v8, p0, Lnet/lingala/zip4j/zip/ZipEngine;->zipModel:Lnet/lingala/zip4j/model/ZipModel;

    invoke-virtual {v8}, Lnet/lingala/zip4j/model/ZipModel;->getZipFile()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lnet/lingala/zip4j/zip/ZipEngine;->zipModel:Lnet/lingala/zip4j/model/ZipModel;

    invoke-virtual {v8}, Lnet/lingala/zip4j/model/ZipModel;->getSplitLength()J

    move-result-wide v8

    invoke-direct {v6, v7, v8, v9}, Lnet/lingala/zip4j/io/SplitOutputStream;-><init>(Ljava/io/File;J)V

    .line 220
    .local v6, "splitOutputStream":Lnet/lingala/zip4j/io/SplitOutputStream;
    new-instance v3, Lnet/lingala/zip4j/io/ZipOutputStream;

    iget-object v7, p0, Lnet/lingala/zip4j/zip/ZipEngine;->zipModel:Lnet/lingala/zip4j/model/ZipModel;

    invoke-direct {v3, v6, v7}, Lnet/lingala/zip4j/io/ZipOutputStream;-><init>(Ljava/io/OutputStream;Lnet/lingala/zip4j/model/ZipModel;)V
    :try_end_37
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_d .. :try_end_37} :catch_aa
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_37} :catch_a8
    .catchall {:try_start_d .. :try_end_37} :catchall_4c

    .line 222
    .end local v2    # "outputStream":Lnet/lingala/zip4j/io/ZipOutputStream;
    .local v3, "outputStream":Lnet/lingala/zip4j/io/ZipOutputStream;
    if-eqz v1, :cond_60

    .line 223
    :try_start_39
    iget-object v7, p0, Lnet/lingala/zip4j/zip/ZipEngine;->zipModel:Lnet/lingala/zip4j/model/ZipModel;

    invoke-virtual {v7}, Lnet/lingala/zip4j/model/ZipModel;->getEndCentralDirRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;

    move-result-object v7

    if-nez v7, :cond_53

    .line 224
    new-instance v7, Lnet/lingala/zip4j/exception/ZipException;

    const-string v8, "invalid end of central directory record"

    invoke-direct {v7, v8}, Lnet/lingala/zip4j/exception/ZipException;-><init>(Ljava/lang/String;)V

    throw v7
    :try_end_49
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_39 .. :try_end_49} :catch_49
    .catch Ljava/lang/Exception; {:try_start_39 .. :try_end_49} :catch_8d
    .catchall {:try_start_39 .. :try_end_49} :catchall_a5

    .line 244
    :catch_49
    move-exception v0

    move-object v2, v3

    .line 245
    .end local v1    # "isZipFileAlreadExists":Z
    .end local v3    # "outputStream":Lnet/lingala/zip4j/io/ZipOutputStream;
    .end local v6    # "splitOutputStream":Lnet/lingala/zip4j/io/SplitOutputStream;
    .local v0, "e":Lnet/lingala/zip4j/exception/ZipException;
    .restart local v2    # "outputStream":Lnet/lingala/zip4j/io/ZipOutputStream;
    :goto_4b
    :try_start_4b
    throw v0
    :try_end_4c
    .catchall {:try_start_4b .. :try_end_4c} :catchall_4c

    .line 249
    .end local v0    # "e":Lnet/lingala/zip4j/exception/ZipException;
    :catchall_4c
    move-exception v7

    :goto_4d
    if-eqz v2, :cond_52

    .line 251
    :try_start_4f
    invoke-virtual {v2}, Lnet/lingala/zip4j/io/ZipOutputStream;->close()V
    :try_end_52
    .catch Ljava/io/IOException; {:try_start_4f .. :try_end_52} :catch_a3

    .line 254
    :cond_52
    :goto_52
    throw v7

    .line 226
    .end local v2    # "outputStream":Lnet/lingala/zip4j/io/ZipOutputStream;
    .restart local v1    # "isZipFileAlreadExists":Z
    .restart local v3    # "outputStream":Lnet/lingala/zip4j/io/ZipOutputStream;
    .restart local v6    # "splitOutputStream":Lnet/lingala/zip4j/io/SplitOutputStream;
    :cond_53
    :try_start_53
    iget-object v7, p0, Lnet/lingala/zip4j/zip/ZipEngine;->zipModel:Lnet/lingala/zip4j/model/ZipModel;

    invoke-virtual {v7}, Lnet/lingala/zip4j/model/ZipModel;->getEndCentralDirRecord()Lnet/lingala/zip4j/model/EndCentralDirRecord;

    move-result-object v7

    invoke-virtual {v7}, Lnet/lingala/zip4j/model/EndCentralDirRecord;->getOffsetOfStartOfCentralDir()J

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Lnet/lingala/zip4j/io/SplitOutputStream;->seek(J)V

    .line 229
    :cond_60
    const/16 v7, 0x1000

    new-array v4, v7, [B

    .line 230
    .local v4, "readBuff":[B
    const/4 v5, -0x1

    .line 232
    .local v5, "readLen":I
    const/4 v7, 0x0

    invoke-virtual {v3, v7, p2}, Lnet/lingala/zip4j/io/ZipOutputStream;->putNextEntry(Ljava/io/File;Lnet/lingala/zip4j/model/ZipParameters;)V

    .line 234
    invoke-virtual {p2}, Lnet/lingala/zip4j/model/ZipParameters;->getFileNameInZip()Ljava/lang/String;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_95

    .line 235
    invoke-virtual {p2}, Lnet/lingala/zip4j/model/ZipParameters;->getFileNameInZip()Ljava/lang/String;

    move-result-object v7

    const-string v8, "\\"

    invoke-virtual {v7, v8}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_95

    .line 236
    :goto_81
    invoke-virtual {p1, v4}, Ljava/io/InputStream;->read([B)I

    move-result v5

    const/4 v7, -0x1

    if-eq v5, v7, :cond_95

    .line 237
    const/4 v7, 0x0

    invoke-virtual {v3, v4, v7, v5}, Lnet/lingala/zip4j/io/ZipOutputStream;->write([BII)V
    :try_end_8c
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_53 .. :try_end_8c} :catch_49
    .catch Ljava/lang/Exception; {:try_start_53 .. :try_end_8c} :catch_8d
    .catchall {:try_start_53 .. :try_end_8c} :catchall_a5

    goto :goto_81

    .line 246
    .end local v4    # "readBuff":[B
    .end local v5    # "readLen":I
    :catch_8d
    move-exception v0

    move-object v2, v3

    .line 247
    .end local v1    # "isZipFileAlreadExists":Z
    .end local v3    # "outputStream":Lnet/lingala/zip4j/io/ZipOutputStream;
    .end local v6    # "splitOutputStream":Lnet/lingala/zip4j/io/SplitOutputStream;
    .local v0, "e":Ljava/lang/Exception;
    .restart local v2    # "outputStream":Lnet/lingala/zip4j/io/ZipOutputStream;
    :goto_8f
    :try_start_8f
    new-instance v7, Lnet/lingala/zip4j/exception/ZipException;

    invoke-direct {v7, v0}, Lnet/lingala/zip4j/exception/ZipException;-><init>(Ljava/lang/Throwable;)V

    throw v7
    :try_end_95
    .catchall {:try_start_8f .. :try_end_95} :catchall_4c

    .line 241
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v2    # "outputStream":Lnet/lingala/zip4j/io/ZipOutputStream;
    .restart local v1    # "isZipFileAlreadExists":Z
    .restart local v3    # "outputStream":Lnet/lingala/zip4j/io/ZipOutputStream;
    .restart local v4    # "readBuff":[B
    .restart local v5    # "readLen":I
    .restart local v6    # "splitOutputStream":Lnet/lingala/zip4j/io/SplitOutputStream;
    :cond_95
    :try_start_95
    invoke-virtual {v3}, Lnet/lingala/zip4j/io/ZipOutputStream;->closeEntry()V

    .line 242
    invoke-virtual {v3}, Lnet/lingala/zip4j/io/ZipOutputStream;->finish()V
    :try_end_9b
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_95 .. :try_end_9b} :catch_49
    .catch Ljava/lang/Exception; {:try_start_95 .. :try_end_9b} :catch_8d
    .catchall {:try_start_95 .. :try_end_9b} :catchall_a5

    .line 249
    if-eqz v3, :cond_a0

    .line 251
    :try_start_9d
    invoke-virtual {v3}, Lnet/lingala/zip4j/io/ZipOutputStream;->close()V
    :try_end_a0
    .catch Ljava/io/IOException; {:try_start_9d .. :try_end_a0} :catch_a1

    .line 257
    :cond_a0
    :goto_a0
    return-void

    .line 252
    :catch_a1
    move-exception v7

    goto :goto_a0

    .end local v1    # "isZipFileAlreadExists":Z
    .end local v3    # "outputStream":Lnet/lingala/zip4j/io/ZipOutputStream;
    .end local v4    # "readBuff":[B
    .end local v5    # "readLen":I
    .end local v6    # "splitOutputStream":Lnet/lingala/zip4j/io/SplitOutputStream;
    .restart local v2    # "outputStream":Lnet/lingala/zip4j/io/ZipOutputStream;
    :catch_a3
    move-exception v8

    goto :goto_52

    .line 249
    .end local v2    # "outputStream":Lnet/lingala/zip4j/io/ZipOutputStream;
    .restart local v1    # "isZipFileAlreadExists":Z
    .restart local v3    # "outputStream":Lnet/lingala/zip4j/io/ZipOutputStream;
    .restart local v6    # "splitOutputStream":Lnet/lingala/zip4j/io/SplitOutputStream;
    :catchall_a5
    move-exception v7

    move-object v2, v3

    .end local v3    # "outputStream":Lnet/lingala/zip4j/io/ZipOutputStream;
    .restart local v2    # "outputStream":Lnet/lingala/zip4j/io/ZipOutputStream;
    goto :goto_4d

    .line 246
    .end local v1    # "isZipFileAlreadExists":Z
    .end local v6    # "splitOutputStream":Lnet/lingala/zip4j/io/SplitOutputStream;
    :catch_a8
    move-exception v0

    goto :goto_8f

    .line 244
    :catch_aa
    move-exception v0

    goto :goto_4b
.end method
