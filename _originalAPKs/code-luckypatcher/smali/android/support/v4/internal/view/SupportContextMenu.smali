.class public interface abstract Landroid/support/v4/internal/view/SupportContextMenu;
.super Ljava/lang/Object;
.source "SupportContextMenu.java"

# interfaces
.implements Landroid/support/v4/internal/view/SupportMenu;
.implements Landroid/view/ContextMenu;


# virtual methods
.method public abstract clearHeader()V
.end method

.method public abstract setHeaderIcon(I)Landroid/support/v4/internal/view/SupportContextMenu;
.end method

.method public abstract setHeaderIcon(Landroid/graphics/drawable/Drawable;)Landroid/support/v4/internal/view/SupportContextMenu;
.end method

.method public abstract setHeaderTitle(I)Landroid/support/v4/internal/view/SupportContextMenu;
.end method

.method public abstract setHeaderTitle(Ljava/lang/CharSequence;)Landroid/support/v4/internal/view/SupportContextMenu;
.end method

.method public abstract setHeaderView(Landroid/view/View;)Landroid/support/v4/internal/view/SupportContextMenu;
.end method
