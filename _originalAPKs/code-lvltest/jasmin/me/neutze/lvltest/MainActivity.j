.bytecode 50.0
.class public synchronized me/neutze/lvltest/MainActivity
.super android/app/Activity
.inner class inner me/neutze/lvltest/MainActivity$1
.inner class inner me/neutze/lvltest/MainActivity$2
.inner class private MyLicenseCheckerCallback inner me/neutze/lvltest/MainActivity$MyLicenseCheckerCallback outer me/neutze/lvltest/MainActivity

.field private static final 'BASE64_PUBLIC_KEY' Ljava/lang/String; = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAo/iY2nt0OvE0DsOj0TBg+ue1s7NDjyzL+XXV8bz4EaWWIayFHFaMjJ6yX/XsL1uLpE6WqvVFrFwjcnGx4uP4YHpE3eTKMkV4ubMdUCEKkGu4t4NxnS46rOdEn8kouLx2katPF2HN5padsDgDj+51p0CEDewG3A2QST+EQK/fhds0TTP/nOgv8Qm8P7l0iayf6KbzY8E8oI422Ep7xqz1uFxmQn0VXETZnjcTh6SaA6k67lxk+vVHwDB44zgFp9HBGRAZC5gQEji483AyQCKs5z9WaFo2StEDiyeI11v+LT/bTEVhZZAGQHdR8nuJxh1Zfw6anuHcTtTfsuNzU7wn/wIDAQAB"

.field private static final 'SALT' [B

.field private 'android_id' Ljava/lang/String;

.field private 'deviceId' Ljava/lang/String;

.field private 'mCheckLicenseButton' Landroid/widget/Button;

.field private 'mChecker' Lcom/google/android/vending/licensing/LicenseChecker;

.field private 'mHandler' Landroid/os/Handler;

.field private 'mLicenseCheckerCallback' Lme/neutze/lvltest/MainActivity$MyLicenseCheckerCallback;

.field private 'mObsfuscator' Lcom/google/android/vending/licensing/AESObfuscator;

.field private 'mStatusText' Landroid/widget/TextView;

.method static <clinit>()V
bipush 20
newarray byte
dup
iconst_0
ldc_w -20
bastore
dup
iconst_1
ldc_w 30
bastore
dup
iconst_2
ldc_w 50
bastore
dup
iconst_3
ldc_w -70
bastore
dup
iconst_4
ldc_w 33
bastore
dup
iconst_5
ldc_w -100
bastore
dup
bipush 6
ldc_w 32
bastore
dup
bipush 7
ldc_w -90
bastore
dup
bipush 8
ldc_w -88
bastore
dup
bipush 9
ldc_w 104
bastore
dup
bipush 10
ldc_w 12
bastore
dup
bipush 11
ldc_w -10
bastore
dup
bipush 12
ldc_w 72
bastore
dup
bipush 13
ldc_w -34
bastore
dup
bipush 14
ldc_w 115
bastore
dup
bipush 15
ldc_w 21
bastore
dup
bipush 16
ldc_w 62
bastore
dup
bipush 17
ldc_w 35
bastore
dup
bipush 18
ldc_w -12
bastore
dup
bipush 19
ldc_w 97
bastore
putstatic me/neutze/lvltest/MainActivity/SALT [B
return
.limit locals 0
.limit stack 4
.end method

.method public <init>()V
aload 0
invokespecial android/app/Activity/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method static synthetic access$102(Lme/neutze/lvltest/MainActivity;Landroid/os/Handler;)Landroid/os/Handler;
aload 0
aload 1
putfield me/neutze/lvltest/MainActivity/mHandler Landroid/os/Handler;
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method static synthetic access$200(Lme/neutze/lvltest/MainActivity;)V
aload 0
invokespecial me/neutze/lvltest/MainActivity/doCheck()V
return
.limit locals 1
.limit stack 1
.end method

.method static synthetic access$300(Lme/neutze/lvltest/MainActivity;)Landroid/widget/TextView;
aload 0
getfield me/neutze/lvltest/MainActivity/mStatusText Landroid/widget/TextView;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic access$400(Lme/neutze/lvltest/MainActivity;)Landroid/widget/Button;
aload 0
getfield me/neutze/lvltest/MainActivity/mCheckLicenseButton Landroid/widget/Button;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic access$500(Lme/neutze/lvltest/MainActivity;Ljava/lang/String;)V
aload 0
aload 1
invokespecial me/neutze/lvltest/MainActivity/displayResult(Ljava/lang/String;)V
return
.limit locals 2
.limit stack 2
.end method

.method private displayResult(Ljava/lang/String;)V
aload 0
getfield me/neutze/lvltest/MainActivity/mHandler Landroid/os/Handler;
new me/neutze/lvltest/MainActivity$2
dup
aload 0
aload 1
invokespecial me/neutze/lvltest/MainActivity$2/<init>(Lme/neutze/lvltest/MainActivity;Ljava/lang/String;)V
invokevirtual android/os/Handler/post(Ljava/lang/Runnable;)Z
pop
return
.limit locals 2
.limit stack 5
.end method

.method private doCheck()V
aload 0
getfield me/neutze/lvltest/MainActivity/mCheckLicenseButton Landroid/widget/Button;
iconst_0
invokevirtual android/widget/Button/setEnabled(Z)V
aload 0
iconst_1
invokevirtual me/neutze/lvltest/MainActivity/setProgressBarIndeterminateVisibility(Z)V
aload 0
getfield me/neutze/lvltest/MainActivity/mStatusText Landroid/widget/TextView;
ldc_w 2131099651
invokevirtual android/widget/TextView/setText(I)V
aload 0
getfield me/neutze/lvltest/MainActivity/mChecker Lcom/google/android/vending/licensing/LicenseChecker;
aload 0
getfield me/neutze/lvltest/MainActivity/mLicenseCheckerCallback Lme/neutze/lvltest/MainActivity$MyLicenseCheckerCallback;
invokevirtual com/google/android/vending/licensing/LicenseChecker/checkAccess(Lcom/google/android/vending/licensing/LicenseCheckerCallback;)V
return
.limit locals 1
.limit stack 2
.end method

.method public onCreate(Landroid/os/Bundle;)V
.catch java/lang/NoSuchFieldException from L0 to L1 using L2
aload 0
aload 1
invokespecial android/app/Activity/onCreate(Landroid/os/Bundle;)V
aload 0
ldc_w 2130903040
invokevirtual me/neutze/lvltest/MainActivity/setContentView(I)V
aload 0
aload 0
ldc_w 2131230720
invokevirtual me/neutze/lvltest/MainActivity/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
putfield me/neutze/lvltest/MainActivity/mStatusText Landroid/widget/TextView;
aload 0
aload 0
ldc_w 2131230721
invokevirtual me/neutze/lvltest/MainActivity/findViewById(I)Landroid/view/View;
checkcast android/widget/Button
putfield me/neutze/lvltest/MainActivity/mCheckLicenseButton Landroid/widget/Button;
aload 0
aload 0
invokevirtual me/neutze/lvltest/MainActivity/getContentResolver()Landroid/content/ContentResolver;
ldc "android_id"
invokestatic android/provider/Settings$Secure/getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;
putfield me/neutze/lvltest/MainActivity/deviceId Ljava/lang/String;
aload 0
new com/google/android/vending/licensing/AESObfuscator
dup
getstatic me/neutze/lvltest/MainActivity/SALT [B
aload 0
invokevirtual me/neutze/lvltest/MainActivity/getPackageName()Ljava/lang/String;
aload 0
getfield me/neutze/lvltest/MainActivity/android_id Ljava/lang/String;
invokespecial com/google/android/vending/licensing/AESObfuscator/<init>([BLjava/lang/String;Ljava/lang/String;)V
putfield me/neutze/lvltest/MainActivity/mObsfuscator Lcom/google/android/vending/licensing/AESObfuscator;
new com/google/android/vending/licensing/ServerManagedPolicy
dup
aload 0
aload 0
getfield me/neutze/lvltest/MainActivity/mObsfuscator Lcom/google/android/vending/licensing/AESObfuscator;
invokespecial com/google/android/vending/licensing/ServerManagedPolicy/<init>(Landroid/content/Context;Lcom/google/android/vending/licensing/Obfuscator;)V
astore 1
aload 0
new me/neutze/lvltest/MainActivity$MyLicenseCheckerCallback
dup
aload 0
aconst_null
invokespecial me/neutze/lvltest/MainActivity$MyLicenseCheckerCallback/<init>(Lme/neutze/lvltest/MainActivity;Lme/neutze/lvltest/MainActivity$1;)V
putfield me/neutze/lvltest/MainActivity/mLicenseCheckerCallback Lme/neutze/lvltest/MainActivity$MyLicenseCheckerCallback;
aload 0
new com/google/android/vending/licensing/LicenseChecker
dup
aload 0
aload 1
ldc "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAo/iY2nt0OvE0DsOj0TBg+ue1s7NDjyzL+XXV8bz4EaWWIayFHFaMjJ6yX/XsL1uLpE6WqvVFrFwjcnGx4uP4YHpE3eTKMkV4ubMdUCEKkGu4t4NxnS46rOdEn8kouLx2katPF2HN5padsDgDj+51p0CEDewG3A2QST+EQK/fhds0TTP/nOgv8Qm8P7l0iayf6KbzY8E8oI422Ep7xqz1uFxmQn0VXETZnjcTh6SaA6k67lxk+vVHwDB44zgFp9HBGRAZC5gQEji483AyQCKs5z9WaFo2StEDiyeI11v+LT/bTEVhZZAGQHdR8nuJxh1Zfw6anuHcTtTfsuNzU7wn/wIDAQAB"
invokespecial com/google/android/vending/licensing/LicenseChecker/<init>(Landroid/content/Context;Lcom/google/android/vending/licensing/Policy;Ljava/lang/String;)V
putfield me/neutze/lvltest/MainActivity/mChecker Lcom/google/android/vending/licensing/LicenseChecker;
L0:
ldc "JOHANNES"
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "getField "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc com/google/android/vending/licensing/LicenseChecker
ldc "mPolicy"
invokevirtual java/lang/Class/getField(Ljava/lang/String;)Ljava/lang/reflect/Field;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
L1:
aload 0
getfield me/neutze/lvltest/MainActivity/mCheckLicenseButton Landroid/widget/Button;
new me/neutze/lvltest/MainActivity$1
dup
aload 0
invokespecial me/neutze/lvltest/MainActivity$1/<init>(Lme/neutze/lvltest/MainActivity;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
return
L2:
astore 1
aload 1
invokevirtual java/lang/NoSuchFieldException/printStackTrace()V
goto L1
.limit locals 2
.limit stack 6
.end method

.method protected onDestroy()V
aload 0
invokespecial android/app/Activity/onDestroy()V
aload 0
getfield me/neutze/lvltest/MainActivity/mChecker Lcom/google/android/vending/licensing/LicenseChecker;
invokevirtual com/google/android/vending/licensing/LicenseChecker/onDestroy()V
return
.limit locals 1
.limit stack 1
.end method
