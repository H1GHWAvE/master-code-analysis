.class public Lme/neutze/lvltest/MainActivity;
.super Landroid/app/Activity;
.source "MainActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lme/neutze/lvltest/MainActivity$MyLicenseCheckerCallback;
    }
.end annotation


# static fields
.field private static final BASE64_PUBLIC_KEY:Ljava/lang/String; = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAo/iY2nt0OvE0DsOj0TBg+ue1s7NDjyzL+XXV8bz4EaWWIayFHFaMjJ6yX/XsL1uLpE6WqvVFrFwjcnGx4uP4YHpE3eTKMkV4ubMdUCEKkGu4t4NxnS46rOdEn8kouLx2katPF2HN5padsDgDj+51p0CEDewG3A2QST+EQK/fhds0TTP/nOgv8Qm8P7l0iayf6KbzY8E8oI422Ep7xqz1uFxmQn0VXETZnjcTh6SaA6k67lxk+vVHwDB44zgFp9HBGRAZC5gQEji483AyQCKs5z9WaFo2StEDiyeI11v+LT/bTEVhZZAGQHdR8nuJxh1Zfw6anuHcTtTfsuNzU7wn/wIDAQAB"

.field private static final SALT:[B


# instance fields
.field private android_id:Ljava/lang/String;

.field private deviceId:Ljava/lang/String;

.field private mCheckLicenseButton:Landroid/widget/Button;

.field private mChecker:Lcom/google/android/vending/licensing/LicenseChecker;

.field private mHandler:Landroid/os/Handler;

.field private mLicenseCheckerCallback:Lme/neutze/lvltest/MainActivity$MyLicenseCheckerCallback;

.field private mObsfuscator:Lcom/google/android/vending/licensing/AESObfuscator;

.field private mStatusText:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 22
    const/16 v0, 0x14

    new-array v0, v0, [B

    fill-array-data v0, :array_a

    sput-object v0, Lme/neutze/lvltest/MainActivity;->SALT:[B

    return-void

    :array_a
    .array-data 1
        -0x14t
        0x1et
        0x32t
        -0x46t
        0x21t
        -0x64t
        0x20t
        -0x5at
        -0x58t
        0x68t
        0xct
        -0xat
        0x48t
        -0x22t
        0x73t
        0x15t
        0x3et
        0x23t
        -0xct
        0x61t
    .end array-data
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic access$102(Lme/neutze/lvltest/MainActivity;Landroid/os/Handler;)Landroid/os/Handler;
    .registers 2
    .param p0, "x0"    # Lme/neutze/lvltest/MainActivity;
    .param p1, "x1"    # Landroid/os/Handler;

    .prologue
    .line 18
    iput-object p1, p0, Lme/neutze/lvltest/MainActivity;->mHandler:Landroid/os/Handler;

    return-object p1
.end method

.method static synthetic access$200(Lme/neutze/lvltest/MainActivity;)V
    .registers 1
    .param p0, "x0"    # Lme/neutze/lvltest/MainActivity;

    .prologue
    .line 18
    invoke-direct {p0}, Lme/neutze/lvltest/MainActivity;->doCheck()V

    return-void
.end method

.method static synthetic access$300(Lme/neutze/lvltest/MainActivity;)Landroid/widget/TextView;
    .registers 2
    .param p0, "x0"    # Lme/neutze/lvltest/MainActivity;

    .prologue
    .line 18
    iget-object v0, p0, Lme/neutze/lvltest/MainActivity;->mStatusText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$400(Lme/neutze/lvltest/MainActivity;)Landroid/widget/Button;
    .registers 2
    .param p0, "x0"    # Lme/neutze/lvltest/MainActivity;

    .prologue
    .line 18
    iget-object v0, p0, Lme/neutze/lvltest/MainActivity;->mCheckLicenseButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$500(Lme/neutze/lvltest/MainActivity;Ljava/lang/String;)V
    .registers 2
    .param p0, "x0"    # Lme/neutze/lvltest/MainActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lme/neutze/lvltest/MainActivity;->displayResult(Ljava/lang/String;)V

    return-void
.end method

.method private displayResult(Ljava/lang/String;)V
    .registers 4
    .param p1, "result"    # Ljava/lang/String;

    .prologue
    .line 77
    iget-object v0, p0, Lme/neutze/lvltest/MainActivity;->mHandler:Landroid/os/Handler;

    new-instance v1, Lme/neutze/lvltest/MainActivity$2;

    invoke-direct {v1, p0, p1}, Lme/neutze/lvltest/MainActivity$2;-><init>(Lme/neutze/lvltest/MainActivity;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 84
    return-void
.end method

.method private doCheck()V
    .registers 3

    .prologue
    .line 64
    iget-object v0, p0, Lme/neutze/lvltest/MainActivity;->mCheckLicenseButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 65
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lme/neutze/lvltest/MainActivity;->setProgressBarIndeterminateVisibility(Z)V

    .line 66
    iget-object v0, p0, Lme/neutze/lvltest/MainActivity;->mStatusText:Landroid/widget/TextView;

    const v1, 0x7f060003

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 67
    iget-object v0, p0, Lme/neutze/lvltest/MainActivity;->mChecker:Lcom/google/android/vending/licensing/LicenseChecker;

    iget-object v1, p0, Lme/neutze/lvltest/MainActivity;->mLicenseCheckerCallback:Lme/neutze/lvltest/MainActivity$MyLicenseCheckerCallback;

    invoke-virtual {v0, v1}, Lcom/google/android/vending/licensing/LicenseChecker;->checkAccess(Lcom/google/android/vending/licensing/LicenseCheckerCallback;)V

    .line 68
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .registers 8
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    .line 37
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 38
    const/high16 v2, 0x7f030000

    invoke-virtual {p0, v2}, Lme/neutze/lvltest/MainActivity;->setContentView(I)V

    .line 40
    const/high16 v2, 0x7f080000

    invoke-virtual {p0, v2}, Lme/neutze/lvltest/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lme/neutze/lvltest/MainActivity;->mStatusText:Landroid/widget/TextView;

    .line 41
    const v2, 0x7f080001

    invoke-virtual {p0, v2}, Lme/neutze/lvltest/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lme/neutze/lvltest/MainActivity;->mCheckLicenseButton:Landroid/widget/Button;

    .line 43
    invoke-virtual {p0}, Lme/neutze/lvltest/MainActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "android_id"

    invoke-static {v2, v3}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lme/neutze/lvltest/MainActivity;->deviceId:Ljava/lang/String;

    .line 44
    new-instance v2, Lcom/google/android/vending/licensing/AESObfuscator;

    sget-object v3, Lme/neutze/lvltest/MainActivity;->SALT:[B

    invoke-virtual {p0}, Lme/neutze/lvltest/MainActivity;->getPackageName()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lme/neutze/lvltest/MainActivity;->android_id:Ljava/lang/String;

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/vending/licensing/AESObfuscator;-><init>([BLjava/lang/String;Ljava/lang/String;)V

    iput-object v2, p0, Lme/neutze/lvltest/MainActivity;->mObsfuscator:Lcom/google/android/vending/licensing/AESObfuscator;

    .line 45
    new-instance v1, Lcom/google/android/vending/licensing/ServerManagedPolicy;

    iget-object v2, p0, Lme/neutze/lvltest/MainActivity;->mObsfuscator:Lcom/google/android/vending/licensing/AESObfuscator;

    invoke-direct {v1, p0, v2}, Lcom/google/android/vending/licensing/ServerManagedPolicy;-><init>(Landroid/content/Context;Lcom/google/android/vending/licensing/Obfuscator;)V

    .line 46
    .local v1, "serverPolicy":Lcom/google/android/vending/licensing/ServerManagedPolicy;
    new-instance v2, Lme/neutze/lvltest/MainActivity$MyLicenseCheckerCallback;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lme/neutze/lvltest/MainActivity$MyLicenseCheckerCallback;-><init>(Lme/neutze/lvltest/MainActivity;Lme/neutze/lvltest/MainActivity$1;)V

    iput-object v2, p0, Lme/neutze/lvltest/MainActivity;->mLicenseCheckerCallback:Lme/neutze/lvltest/MainActivity$MyLicenseCheckerCallback;

    .line 47
    new-instance v2, Lcom/google/android/vending/licensing/LicenseChecker;

    const-string v3, "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAo/iY2nt0OvE0DsOj0TBg+ue1s7NDjyzL+XXV8bz4EaWWIayFHFaMjJ6yX/XsL1uLpE6WqvVFrFwjcnGx4uP4YHpE3eTKMkV4ubMdUCEKkGu4t4NxnS46rOdEn8kouLx2katPF2HN5padsDgDj+51p0CEDewG3A2QST+EQK/fhds0TTP/nOgv8Qm8P7l0iayf6KbzY8E8oI422Ep7xqz1uFxmQn0VXETZnjcTh6SaA6k67lxk+vVHwDB44zgFp9HBGRAZC5gQEji483AyQCKs5z9WaFo2StEDiyeI11v+LT/bTEVhZZAGQHdR8nuJxh1Zfw6anuHcTtTfsuNzU7wn/wIDAQAB"

    invoke-direct {v2, p0, v1, v3}, Lcom/google/android/vending/licensing/LicenseChecker;-><init>(Landroid/content/Context;Lcom/google/android/vending/licensing/Policy;Ljava/lang/String;)V

    iput-object v2, p0, Lme/neutze/lvltest/MainActivity;->mChecker:Lcom/google/android/vending/licensing/LicenseChecker;

    .line 49
    :try_start_50
    const-string v2, "JOHANNES"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getField "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-class v4, Lcom/google/android/vending/licensing/LicenseChecker;

    const-string v5, "mPolicy"

    invoke-virtual {v4, v5}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_70
    .catch Ljava/lang/NoSuchFieldException; {:try_start_50 .. :try_end_70} :catch_7b

    .line 54
    :goto_70
    iget-object v2, p0, Lme/neutze/lvltest/MainActivity;->mCheckLicenseButton:Landroid/widget/Button;

    new-instance v3, Lme/neutze/lvltest/MainActivity$1;

    invoke-direct {v3, p0}, Lme/neutze/lvltest/MainActivity$1;-><init>(Lme/neutze/lvltest/MainActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 61
    return-void

    .line 50
    :catch_7b
    move-exception v0

    .line 51
    .local v0, "e":Ljava/lang/NoSuchFieldException;
    invoke-virtual {v0}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    goto :goto_70
.end method

.method protected onDestroy()V
    .registers 2

    .prologue
    .line 72
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 73
    iget-object v0, p0, Lme/neutze/lvltest/MainActivity;->mChecker:Lcom/google/android/vending/licensing/LicenseChecker;

    invoke-virtual {v0}, Lcom/google/android/vending/licensing/LicenseChecker;->onDestroy()V

    .line 74
    return-void
.end method
