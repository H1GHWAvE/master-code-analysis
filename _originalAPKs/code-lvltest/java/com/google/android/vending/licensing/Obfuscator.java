package com.google.android.vending.licensing;
public interface Obfuscator {

    public abstract String obfuscate();

    public abstract String unobfuscate();
}
