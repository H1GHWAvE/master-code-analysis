package com.google.android.vending.licensing;
public interface ILicensingService implements android.os.IInterface {

    public abstract void checkLicense();
}
