package com.google.android.vending.licensing;
public class PreferenceObfuscator {
    private static final String TAG = "PreferenceObfuscator";
    private android.content.SharedPreferences$Editor mEditor;
    private final com.google.android.vending.licensing.Obfuscator mObfuscator;
    private final android.content.SharedPreferences mPreferences;

    public PreferenceObfuscator(android.content.SharedPreferences p2, com.google.android.vending.licensing.Obfuscator p3)
    {
        this.mPreferences = p2;
        this.mObfuscator = p3;
        this.mEditor = 0;
        return;
    }

    public void commit()
    {
        if (this.mEditor != null) {
            this.mEditor.commit();
            this.mEditor = 0;
        }
        return;
    }

    public String getString(String p7, String p8)
    {
        String v1;
        String v2 = this.mPreferences.getString(p7, 0);
        if (v2 == null) {
            v1 = p8;
        } else {
            try {
                v1 = this.mObfuscator.unobfuscate(v2, p7);
            } catch (com.google.android.vending.licensing.ValidationException v0) {
                android.util.Log.w("PreferenceObfuscator", new StringBuilder().append("Validation error while reading preference: ").append(p7).toString());
                v1 = p8;
            }
        }
        return v1;
    }

    public void putString(String p3, String p4)
    {
        if (this.mEditor == null) {
            this.mEditor = this.mPreferences.edit();
        }
        this.mEditor.putString(p3, this.mObfuscator.obfuscate(p4, p3));
        return;
    }
}
