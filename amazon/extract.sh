#!/bin/bash

#remove old files
rm -rf ~/Desktop/master_thesis/sourcefiles/amazon/code-amazontest/java/
rm -rf ~/Desktop/master_thesis/sourcefiles/amazon/code-amazontest/smali/
rm -rf ~/Desktop/master_thesis/sourcefiles/amazon/code-amazontest/jasmin
rm -rf ~/Desktop/master_thesis/sourcefiles/amazon/code-amazontest/jar/lvltest.jar

#androguard
python ~/Desktop/master_thesis/sourcefiles/_tools/androdd.py -i ~/Desktop/master_thesis/sourcefiles/amazon/apk-amazontest/amazontest.apk -o ~/Desktop/master_thesis/sourcefiles/amazon/code-amazontest/java/

#baksmali
java -jar ~/Desktop/master_thesis/sourcefiles/_tools/baksmali.jar -x ~/Desktop/master_thesis/sourcefiles/amazon/apk-amazontest/amazontest.apk -o ~/Desktop/master_thesis/sourcefiles/amazon/code-amazontest/smali/

#jasmin
bash ~/Desktop/master_thesis/sourcefiles/_tools/_dex2jar-2.0/d2j-dex2jar.sh -f -o ~/Desktop/master_thesis/sourcefiles/amazon/code-amazontest/jar/lvltest.jar ~/Desktop/master_thesis/sourcefiles/amazon/apk-amazontest/amazontest.apk

#jar
bash ~/Desktop/master_thesis/sourcefiles/_tools/_dex2jar-2.0/d2j-jar2jasmin.sh -f -o ~/Desktop/master_thesis/sourcefiles/amazon/code-amazontest/jasmin ~/Desktop/master_thesis/sourcefiles/amazon/code-amazontest/jar/lvltest.jar
