.class public Lme/neutze/license/MainActivity;
.super Landroid/app/Activity;
.source "MainActivity.java"


# instance fields
.field private mCheckLicenseButton:Landroid/widget/Button;

.field private mHandler:Landroid/os/Handler;

.field private mStatusText:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 13
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic access$002(Lme/neutze/license/MainActivity;Landroid/os/Handler;)Landroid/os/Handler;
    .registers 2
    .param p0, "x0"    # Lme/neutze/license/MainActivity;
    .param p1, "x1"    # Landroid/os/Handler;

    .prologue
    .line 13
    iput-object p1, p0, Lme/neutze/license/MainActivity;->mHandler:Landroid/os/Handler;

    return-object p1
.end method

.method static synthetic access$100(Lme/neutze/license/MainActivity;)V
    .registers 1
    .param p0, "x0"    # Lme/neutze/license/MainActivity;

    .prologue
    .line 13
    invoke-direct {p0}, Lme/neutze/license/MainActivity;->doCheck()V

    return-void
.end method

.method static synthetic access$200(Lme/neutze/license/MainActivity;)Landroid/widget/TextView;
    .registers 2
    .param p0, "x0"    # Lme/neutze/license/MainActivity;

    .prologue
    .line 13
    iget-object v0, p0, Lme/neutze/license/MainActivity;->mStatusText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$300(Lme/neutze/license/MainActivity;)Landroid/widget/Button;
    .registers 2
    .param p0, "x0"    # Lme/neutze/license/MainActivity;

    .prologue
    .line 13
    iget-object v0, p0, Lme/neutze/license/MainActivity;->mCheckLicenseButton:Landroid/widget/Button;

    return-object v0
.end method

.method private displayResult(Ljava/lang/String;)V
    .registers 6
    .param p1, "result"    # Ljava/lang/String;

    .prologue
    .line 50
    iget-object v0, p0, Lme/neutze/license/MainActivity;->mHandler:Landroid/os/Handler;

    new-instance v1, Lme/neutze/license/MainActivity$2;

    invoke-direct {v1, p0, p1}, Lme/neutze/license/MainActivity$2;-><init>(Lme/neutze/license/MainActivity;Ljava/lang/String;)V

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 58
    return-void
.end method

.method private doCheck()V
    .registers 3

    .prologue
    .line 38
    iget-object v0, p0, Lme/neutze/license/MainActivity;->mCheckLicenseButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 39
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lme/neutze/license/MainActivity;->setProgressBarIndeterminateVisibility(Z)V

    .line 40
    iget-object v0, p0, Lme/neutze/license/MainActivity;->mStatusText:Landroid/widget/TextView;

    const v1, 0x7f060003

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 41
    const-string v0, "amazon does not check here"

    invoke-direct {p0, v0}, Lme/neutze/license/MainActivity;->displayResult(Ljava/lang/String;)V

    .line 42
    return-void
.end method

.method private onCreateMainActivity(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    .line 21
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 22
    const/high16 v0, 0x7f030000

    invoke-virtual {p0, v0}, Lme/neutze/license/MainActivity;->setContentView(I)V

    .line 24
    const/high16 v0, 0x7f080000

    invoke-virtual {p0, v0}, Lme/neutze/license/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lme/neutze/license/MainActivity;->mStatusText:Landroid/widget/TextView;

    .line 25
    const v0, 0x7f080001

    invoke-virtual {p0, v0}, Lme/neutze/license/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lme/neutze/license/MainActivity;->mCheckLicenseButton:Landroid/widget/Button;

    .line 28
    iget-object v0, p0, Lme/neutze/license/MainActivity;->mCheckLicenseButton:Landroid/widget/Button;

    new-instance v1, Lme/neutze/license/MainActivity$1;

    invoke-direct {v1, p0}, Lme/neutze/license/MainActivity$1;-><init>(Lme/neutze/license/MainActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 35
    return-void
.end method

.method private onDestroyMainActivity()V
    .registers 1

    .prologue
    .line 46
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 47
    return-void
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .registers 5
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    invoke-static {p0, p1, p2, p3}, Lcom/amazon/android/Kiwi;->onActivityResult(Landroid/app/Activity;IILandroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_7

    :goto_6
    return-void

    :cond_7
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_6
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 3

    invoke-direct {p0, p1}, Lme/neutze/license/MainActivity;->onCreateMainActivity(Landroid/os/Bundle;)V

    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/amazon/android/Kiwi;->onCreate(Landroid/app/Activity;Z)V

    return-void
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .registers 4

    invoke-static {p0, p1}, Lcom/amazon/android/Kiwi;->onCreateDialog(Landroid/app/Activity;I)Landroid/app/Dialog;

    move-result-object v0

    .local v0, "dialog":Landroid/app/Dialog;
    if-eqz v0, :cond_8

    move-object v1, v0

    :goto_7
    return-object v1

    :cond_8
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v1

    goto :goto_7
.end method

.method public onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .registers 4

    invoke-static {p0, p1}, Lcom/amazon/android/Kiwi;->onCreateDialog(Landroid/app/Activity;I)Landroid/app/Dialog;

    move-result-object v0

    .local v0, "dialog":Landroid/app/Dialog;
    if-eqz v0, :cond_8

    move-object p0, v0

    :goto_7
    return-object p0

    :cond_8
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object p0

    goto :goto_7
.end method

.method public onDestroy()V
    .registers 1

    invoke-direct {p0}, Lme/neutze/license/MainActivity;->onDestroyMainActivity()V

    invoke-static {p0}, Lcom/amazon/android/Kiwi;->onDestroy(Landroid/app/Activity;)V

    return-void
.end method

.method public onPause()V
    .registers 1

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    invoke-static {p0}, Lcom/amazon/android/Kiwi;->onPause(Landroid/app/Activity;)V

    return-void
.end method

.method public onResume()V
    .registers 1

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    invoke-static {p0}, Lcom/amazon/android/Kiwi;->onResume(Landroid/app/Activity;)V

    return-void
.end method

.method public onStart()V
    .registers 1

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    invoke-static {p0}, Lcom/amazon/android/Kiwi;->onStart(Landroid/app/Activity;)V

    return-void
.end method

.method public onStop()V
    .registers 1

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    invoke-static {p0}, Lcom/amazon/android/Kiwi;->onStop(Landroid/app/Activity;)V

    return-void
.end method
