.class public abstract Lcom/amazon/venezia/command/c;
.super Landroid/os/Binder;

# interfaces
.implements Lcom/amazon/venezia/command/r;


# direct methods
.method public constructor <init>()V
    .registers 2

    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    const-string v0, "com.amazon.venezia.command.DecisionResult"

    invoke-virtual {p0, p0, v0}, Lcom/amazon/venezia/command/c;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v2, 0x0

    const/4 v3, 0x1

    const-string v1, "com.amazon.venezia.command.DecisionResult"

    sparse-switch p1, :sswitch_data_e0

    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    :goto_b
    return v0

    :sswitch_c
    const-string v0, "com.amazon.venezia.command.DecisionResult"

    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    move v0, v3

    goto :goto_b

    :sswitch_13
    const-string v0, "com.amazon.venezia.command.DecisionResult"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/amazon/venezia/command/c;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    move v0, v3

    goto :goto_b

    :sswitch_24
    const-string v0, "com.amazon.venezia.command.DecisionResult"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/amazon/venezia/command/c;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    move v0, v3

    goto :goto_b

    :sswitch_35
    const-string v0, "com.amazon.venezia.command.DecisionResult"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/amazon/venezia/command/c;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    move v0, v3

    goto :goto_b

    :sswitch_46
    const-string v0, "com.amazon.venezia.command.DecisionResult"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/amazon/venezia/command/c;->d()J

    move-result-wide v0

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    invoke-virtual {p3, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    move v0, v3

    goto :goto_b

    :sswitch_57
    const-string v0, "com.amazon.venezia.command.DecisionResult"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    if-nez v1, :cond_6b

    move-object v0, v2

    :goto_63
    invoke-virtual {p0, v0}, Lcom/amazon/venezia/command/c;->a(Lcom/amazon/venezia/command/s;)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v3

    goto :goto_b

    :cond_6b
    const-string v0, "com.amazon.venezia.command.DecisionExpirationContext"

    invoke-interface {v1, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_7a

    instance-of v2, v0, Lcom/amazon/venezia/command/s;

    if-eqz v2, :cond_7a

    check-cast v0, Lcom/amazon/venezia/command/s;

    goto :goto_63

    :cond_7a
    new-instance v0, Lcom/amazon/venezia/command/u;

    invoke-direct {v0, v1}, Lcom/amazon/venezia/command/u;-><init>(Landroid/os/IBinder;)V

    goto :goto_63

    :sswitch_80
    const-string v0, "com.amazon.venezia.command.DecisionResult"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/amazon/venezia/command/c;->e()Lcom/amazon/venezia/command/n;

    move-result-object v0

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v0, :cond_98

    invoke-interface {v0}, Lcom/amazon/venezia/command/n;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_92
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    move v0, v3

    goto/16 :goto_b

    :cond_98
    move-object v0, v2

    goto :goto_92

    :sswitch_9a
    const-string v0, "com.amazon.venezia.command.DecisionResult"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/amazon/venezia/command/c;->f()Lcom/amazon/venezia/command/n;

    move-result-object v0

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v0, :cond_b2

    invoke-interface {v0}, Lcom/amazon/venezia/command/n;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_ac
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    move v0, v3

    goto/16 :goto_b

    :cond_b2
    move-object v0, v2

    goto :goto_ac

    :sswitch_b4
    const-string v0, "com.amazon.venezia.command.DecisionResult"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/amazon/venezia/command/c;->g()Lcom/amazon/venezia/command/n;

    move-result-object v0

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v0, :cond_cc

    invoke-interface {v0}, Lcom/amazon/venezia/command/n;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_c6
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    move v0, v3

    goto/16 :goto_b

    :cond_cc
    move-object v0, v2

    goto :goto_c6

    :sswitch_ce
    const-string v0, "com.amazon.venezia.command.DecisionResult"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/amazon/venezia/command/c;->h()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    move v0, v3

    goto/16 :goto_b

    :sswitch_data_e0
    .sparse-switch
        0x1 -> :sswitch_13
        0x2 -> :sswitch_24
        0x3 -> :sswitch_35
        0x4 -> :sswitch_46
        0x5 -> :sswitch_57
        0x6 -> :sswitch_80
        0x7 -> :sswitch_9a
        0x8 -> :sswitch_b4
        0x9 -> :sswitch_ce
        0x5f4e5446 -> :sswitch_c
    .end sparse-switch
.end method
