.class public abstract Lcom/amazon/venezia/command/o;
.super Landroid/os/Binder;

# interfaces
.implements Lcom/amazon/venezia/command/f;


# direct methods
.method public constructor <init>()V
    .registers 2

    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    const-string v0, "com.amazon.venezia.command.ResultCallback"

    invoke-virtual {p0, p0, v0}, Lcom/amazon/venezia/command/o;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v2, 0x0

    const/4 v3, 0x1

    const-string v1, "com.amazon.venezia.command.ResultCallback"

    sparse-switch p1, :sswitch_data_b8

    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    :goto_b
    return v0

    :sswitch_c
    const-string v0, "com.amazon.venezia.command.ResultCallback"

    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    move v0, v3

    goto :goto_b

    :sswitch_13
    const-string v0, "com.amazon.venezia.command.ResultCallback"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    if-nez v1, :cond_27

    move-object v0, v2

    :goto_1f
    invoke-virtual {p0, v0}, Lcom/amazon/venezia/command/o;->a(Lcom/amazon/venezia/command/SuccessResult;)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v3

    goto :goto_b

    :cond_27
    const-string v0, "com.amazon.venezia.command.SuccessResult"

    invoke-interface {v1, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_36

    instance-of v2, v0, Lcom/amazon/venezia/command/SuccessResult;

    if-eqz v2, :cond_36

    check-cast v0, Lcom/amazon/venezia/command/SuccessResult;

    goto :goto_1f

    :cond_36
    new-instance v0, Lcom/amazon/venezia/command/x;

    invoke-direct {v0, v1}, Lcom/amazon/venezia/command/x;-><init>(Landroid/os/IBinder;)V

    goto :goto_1f

    :sswitch_3c
    const-string v0, "com.amazon.venezia.command.ResultCallback"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    if-nez v1, :cond_50

    move-object v0, v2

    :goto_48
    invoke-virtual {p0, v0}, Lcom/amazon/venezia/command/o;->a(Lcom/amazon/venezia/command/FailureResult;)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v3

    goto :goto_b

    :cond_50
    const-string v0, "com.amazon.venezia.command.FailureResult"

    invoke-interface {v1, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_5f

    instance-of v2, v0, Lcom/amazon/venezia/command/FailureResult;

    if-eqz v2, :cond_5f

    check-cast v0, Lcom/amazon/venezia/command/FailureResult;

    goto :goto_48

    :cond_5f
    new-instance v0, Lcom/amazon/venezia/command/ab;

    invoke-direct {v0, v1}, Lcom/amazon/venezia/command/ab;-><init>(Landroid/os/IBinder;)V

    goto :goto_48

    :sswitch_65
    const-string v0, "com.amazon.venezia.command.ResultCallback"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    if-nez v1, :cond_79

    move-object v0, v2

    :goto_71
    invoke-virtual {p0, v0}, Lcom/amazon/venezia/command/o;->a(Lcom/amazon/venezia/command/r;)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v3

    goto :goto_b

    :cond_79
    const-string v0, "com.amazon.venezia.command.DecisionResult"

    invoke-interface {v1, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_88

    instance-of v2, v0, Lcom/amazon/venezia/command/r;

    if-eqz v2, :cond_88

    check-cast v0, Lcom/amazon/venezia/command/r;

    goto :goto_71

    :cond_88
    new-instance v0, Lcom/amazon/venezia/command/aa;

    invoke-direct {v0, v1}, Lcom/amazon/venezia/command/aa;-><init>(Landroid/os/IBinder;)V

    goto :goto_71

    :sswitch_8e
    const-string v0, "com.amazon.venezia.command.ResultCallback"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    if-nez v1, :cond_a3

    move-object v0, v2

    :goto_9a
    invoke-virtual {p0, v0}, Lcom/amazon/venezia/command/o;->a(Lcom/amazon/venezia/command/k;)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v3

    goto/16 :goto_b

    :cond_a3
    const-string v0, "com.amazon.venezia.command.ExceptionResult"

    invoke-interface {v1, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_b2

    instance-of v2, v0, Lcom/amazon/venezia/command/k;

    if-eqz v2, :cond_b2

    check-cast v0, Lcom/amazon/venezia/command/k;

    goto :goto_9a

    :cond_b2
    new-instance v0, Lcom/amazon/venezia/command/v;

    invoke-direct {v0, v1}, Lcom/amazon/venezia/command/v;-><init>(Landroid/os/IBinder;)V

    goto :goto_9a

    :sswitch_data_b8
    .sparse-switch
        0x1 -> :sswitch_13
        0x2 -> :sswitch_3c
        0x3 -> :sswitch_65
        0x4 -> :sswitch_8e
        0x5f4e5446 -> :sswitch_c
    .end sparse-switch
.end method
