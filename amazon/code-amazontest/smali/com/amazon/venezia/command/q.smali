.class public abstract Lcom/amazon/venezia/command/q;
.super Landroid/os/Binder;

# interfaces
.implements Lcom/amazon/venezia/command/h;


# direct methods
.method public constructor <init>()V
    .registers 2

    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    const-string v0, "com.amazon.venezia.command.CommandService"

    invoke-virtual {p0, p0, v0}, Lcom/amazon/venezia/command/q;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    return-void
.end method

.method public static a(Landroid/os/IBinder;)Lcom/amazon/venezia/command/h;
    .registers 4

    if-nez p0, :cond_4

    const/4 v1, 0x0

    :goto_3
    return-object v1

    :cond_4
    const-string v1, "com.amazon.venezia.command.CommandService"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v1

    if-eqz v1, :cond_16

    instance-of v2, v1, Lcom/amazon/venezia/command/h;

    if-eqz v2, :cond_16

    move-object v0, v1

    check-cast v0, Lcom/amazon/venezia/command/h;

    move-object p0, v0

    move-object v1, p0

    goto :goto_3

    :cond_16
    new-instance v1, Lcom/amazon/venezia/command/l;

    invoke-direct {v1, p0}, Lcom/amazon/venezia/command/l;-><init>(Landroid/os/IBinder;)V

    goto :goto_3
.end method


# virtual methods
.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v3, 0x0

    const/4 v4, 0x1

    const-string v1, "com.amazon.venezia.command.CommandService"

    sparse-switch p1, :sswitch_data_5a

    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    :goto_b
    return v0

    :sswitch_c
    const-string v0, "com.amazon.venezia.command.CommandService"

    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    move v0, v4

    goto :goto_b

    :sswitch_13
    const-string v0, "com.amazon.venezia.command.CommandService"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    if-nez v1, :cond_2e

    move-object v1, v3

    :goto_1f
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v2

    if-nez v2, :cond_45

    move-object v0, v3

    :goto_26
    invoke-virtual {p0, v1, v0}, Lcom/amazon/venezia/command/q;->a(Lcom/amazon/venezia/command/w;Lcom/amazon/venezia/command/f;)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v4

    goto :goto_b

    :cond_2e
    const-string v0, "com.amazon.venezia.command.Command"

    invoke-interface {v1, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_3e

    instance-of v2, v0, Lcom/amazon/venezia/command/w;

    if-eqz v2, :cond_3e

    check-cast v0, Lcom/amazon/venezia/command/w;

    move-object v1, v0

    goto :goto_1f

    :cond_3e
    new-instance v0, Lcom/amazon/venezia/command/d;

    invoke-direct {v0, v1}, Lcom/amazon/venezia/command/d;-><init>(Landroid/os/IBinder;)V

    move-object v1, v0

    goto :goto_1f

    :cond_45
    const-string v0, "com.amazon.venezia.command.ResultCallback"

    invoke-interface {v2, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_54

    instance-of v3, v0, Lcom/amazon/venezia/command/f;

    if-eqz v3, :cond_54

    check-cast v0, Lcom/amazon/venezia/command/f;

    goto :goto_26

    :cond_54
    new-instance v0, Lcom/amazon/venezia/command/t;

    invoke-direct {v0, v2}, Lcom/amazon/venezia/command/t;-><init>(Landroid/os/IBinder;)V

    goto :goto_26

    :sswitch_data_5a
    .sparse-switch
        0x1 -> :sswitch_13
        0x5f4e5446 -> :sswitch_c
    .end sparse-switch
.end method
