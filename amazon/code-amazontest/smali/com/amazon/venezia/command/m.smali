.class public abstract Lcom/amazon/venezia/command/m;
.super Landroid/os/Binder;

# interfaces
.implements Lcom/amazon/venezia/command/n;


# direct methods
.method public constructor <init>()V
    .registers 2

    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    const-string v0, "com.amazon.venezia.command.Choice"

    invoke-virtual {p0, p0, v0}, Lcom/amazon/venezia/command/m;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    return-void
.end method

.method public static a(Landroid/os/IBinder;)Lcom/amazon/venezia/command/n;
    .registers 4

    if-nez p0, :cond_4

    const/4 v1, 0x0

    :goto_3
    return-object v1

    :cond_4
    const-string v1, "com.amazon.venezia.command.Choice"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v1

    if-eqz v1, :cond_16

    instance-of v2, v1, Lcom/amazon/venezia/command/n;

    if-eqz v2, :cond_16

    move-object v0, v1

    check-cast v0, Lcom/amazon/venezia/command/n;

    move-object p0, v0

    move-object v1, p0

    goto :goto_3

    :cond_16
    new-instance v1, Lcom/amazon/venezia/command/a;

    invoke-direct {v1, p0}, Lcom/amazon/venezia/command/a;-><init>(Landroid/os/IBinder;)V

    goto :goto_3
.end method


# virtual methods
.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v3, 0x1

    const-string v1, "com.amazon.venezia.command.Choice"

    sparse-switch p1, :sswitch_data_78

    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    :goto_a
    return v0

    :sswitch_b
    const-string v0, "com.amazon.venezia.command.Choice"

    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    move v0, v3

    goto :goto_a

    :sswitch_12
    const-string v0, "com.amazon.venezia.command.Choice"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/amazon/venezia/command/m;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    move v0, v3

    goto :goto_a

    :sswitch_23
    const-string v0, "com.amazon.venezia.command.Choice"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/amazon/venezia/command/m;->b()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v0, :cond_39

    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v0, p3, v3}, Landroid/content/Intent;->writeToParcel(Landroid/os/Parcel;I)V

    :goto_37
    move v0, v3

    goto :goto_a

    :cond_39
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_37

    :sswitch_3e
    const-string v0, "com.amazon.venezia.command.Choice"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    if-nez v1, :cond_52

    const/4 v0, 0x0

    :goto_4a
    invoke-virtual {p0, v0}, Lcom/amazon/venezia/command/m;->a(Lcom/amazon/venezia/command/y;)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v3

    goto :goto_a

    :cond_52
    const-string v0, "com.amazon.venezia.command.ChoiceContext"

    invoke-interface {v1, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_61

    instance-of v2, v0, Lcom/amazon/venezia/command/y;

    if-eqz v2, :cond_61

    check-cast v0, Lcom/amazon/venezia/command/y;

    goto :goto_4a

    :cond_61
    new-instance v0, Lcom/amazon/venezia/command/j;

    invoke-direct {v0, v1}, Lcom/amazon/venezia/command/j;-><init>(Landroid/os/IBinder;)V

    goto :goto_4a

    :sswitch_67
    const-string v0, "com.amazon.venezia.command.Choice"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/amazon/venezia/command/m;->c()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    move v0, v3

    goto :goto_a

    :sswitch_data_78
    .sparse-switch
        0x1 -> :sswitch_12
        0x2 -> :sswitch_23
        0x3 -> :sswitch_3e
        0x4 -> :sswitch_67
        0x5f4e5446 -> :sswitch_b
    .end sparse-switch
.end method
