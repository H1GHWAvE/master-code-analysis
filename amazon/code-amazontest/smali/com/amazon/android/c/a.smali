.class public final Lcom/amazon/android/c/a;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/amazon/android/c/d;
.implements Lcom/amazon/android/framework/resource/b;
.implements Ljava/lang/Thread$UncaughtExceptionHandler;


# static fields
.field private static final a:Lcom/amazon/android/framework/util/KiwiLogger;


# instance fields
.field private b:Lcom/amazon/android/n/g;
    .annotation runtime Lcom/amazon/android/framework/resource/Resource;
    .end annotation
.end field

.field private c:Lcom/amazon/android/g/c;
    .annotation runtime Lcom/amazon/android/framework/resource/Resource;
    .end annotation
.end field

.field private d:Landroid/app/Application;
    .annotation runtime Lcom/amazon/android/framework/resource/Resource;
    .end annotation
.end field

.field private e:Ljava/lang/Thread$UncaughtExceptionHandler;

.field private f:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    new-instance v0, Lcom/amazon/android/framework/util/KiwiLogger;

    const-string v1, "CrashManagerImpl"

    invoke-direct {v0, v1}, Lcom/amazon/android/framework/util/KiwiLogger;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/amazon/android/c/a;->a:Lcom/amazon/android/framework/util/KiwiLogger;

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/amazon/android/c/a;->f:Ljava/util/Map;

    return-void
.end method

.method static synthetic a(Lcom/amazon/android/c/a;)Ljava/util/Map;
    .registers 2

    iget-object v0, p0, Lcom/amazon/android/c/a;->f:Ljava/util/Map;

    return-object v0
.end method

.method private declared-synchronized a(Ljava/lang/String;)V
    .registers 7

    monitor-enter p0

    :try_start_1
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    const v1, 0x1869f

    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "s-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".amzst"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_25
    .catchall {:try_start_1 .. :try_end_25} :catchall_4a

    move-result-object v0

    const/4 v1, 0x0

    :try_start_27
    iget-object v2, p0, Lcom/amazon/android/c/a;->d:Landroid/app/Application;

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Landroid/app/Application;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;
    :try_end_2d
    .catch Ljava/lang/Exception; {:try_start_27 .. :try_end_2d} :catch_3a
    .catchall {:try_start_27 .. :try_end_2d} :catchall_4d

    move-result-object v0

    :try_start_2e
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_35
    .catch Ljava/lang/Exception; {:try_start_2e .. :try_end_35} :catch_57
    .catchall {:try_start_2e .. :try_end_35} :catchall_52

    :try_start_35
    invoke-static {v0}, Lcom/amazon/android/framework/util/a;->a(Ljava/io/OutputStream;)V
    :try_end_38
    .catchall {:try_start_35 .. :try_end_38} :catchall_4a

    :goto_38
    monitor-exit p0

    return-void

    :catch_3a
    move-exception v0

    :goto_3b
    :try_start_3b
    sget-boolean v2, Lcom/amazon/android/framework/util/KiwiLogger;->ERROR_ON:Z

    if-eqz v2, :cond_46

    sget-object v2, Lcom/amazon/android/c/a;->a:Lcom/amazon/android/framework/util/KiwiLogger;

    const-string v3, "Coud not save crash report to file"

    invoke-virtual {v2, v3, v0}, Lcom/amazon/android/framework/util/KiwiLogger;->error(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_46
    .catchall {:try_start_3b .. :try_end_46} :catchall_4d

    :cond_46
    :try_start_46
    invoke-static {v1}, Lcom/amazon/android/framework/util/a;->a(Ljava/io/OutputStream;)V
    :try_end_49
    .catchall {:try_start_46 .. :try_end_49} :catchall_4a

    goto :goto_38

    :catchall_4a
    move-exception v0

    monitor-exit p0

    throw v0

    :catchall_4d
    move-exception v0

    :goto_4e
    :try_start_4e
    invoke-static {v1}, Lcom/amazon/android/framework/util/a;->a(Ljava/io/OutputStream;)V

    throw v0
    :try_end_52
    .catchall {:try_start_4e .. :try_end_52} :catchall_4a

    :catchall_52
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    goto :goto_4e

    :catch_57
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    goto :goto_3b
.end method

.method private b(Ljava/lang/String;)Lcom/amazon/android/c/b;
    .registers 5

    :try_start_0
    invoke-static {p1}, Lcom/amazon/android/c/a;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/amazon/android/c/a;->c:Lcom/amazon/android/g/c;

    invoke-interface {v1, v0}, Lcom/amazon/android/g/c;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/amazon/android/u/a;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/amazon/android/c/b;
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_10} :catch_12

    move-object v0, p0

    :goto_11
    return-object v0

    :catch_12
    move-exception v0

    sget-boolean v0, Lcom/amazon/android/framework/util/KiwiLogger;->ERROR_ON:Z

    if-eqz v0, :cond_2f

    sget-object v0, Lcom/amazon/android/c/a;->a:Lcom/amazon/android/framework/util/KiwiLogger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to load crash report: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amazon/android/framework/util/KiwiLogger;->error(Ljava/lang/String;)V

    :cond_2f
    const/4 v0, 0x0

    goto :goto_11
.end method

.method private b()Z
    .registers 3

    iget-object v0, p0, Lcom/amazon/android/c/a;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    const/4 v1, 0x5

    if-lt v0, v1, :cond_b

    const/4 v0, 0x1

    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method private static c(Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x0

    :try_start_6
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v3, Ljava/io/FileReader;

    invoke-direct {v3, p0}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_10
    .catchall {:try_start_6 .. :try_end_10} :catchall_2c

    :goto_10
    :try_start_10
    invoke-virtual {v2}, Ljava/io/BufferedReader;->ready()Z

    move-result v1

    if-eqz v1, :cond_24

    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1d
    .catchall {:try_start_10 .. :try_end_1d} :catchall_1e

    goto :goto_10

    :catchall_1e
    move-exception v0

    move-object v1, v2

    :goto_20
    invoke-static {v1}, Lcom/amazon/android/framework/util/a;->a(Ljava/io/Reader;)V

    throw v0

    :cond_24
    invoke-static {v2}, Lcom/amazon/android/framework/util/a;->a(Ljava/io/Reader;)V

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :catchall_2c
    move-exception v0

    goto :goto_20
.end method

.method private static d(Ljava/lang/String;)V
    .registers 5

    :try_start_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_8} :catch_9

    :cond_8
    :goto_8
    return-void

    :catch_9
    move-exception v0

    sget-boolean v1, Lcom/amazon/android/framework/util/KiwiLogger;->ERROR_ON:Z

    if-eqz v1, :cond_8

    sget-object v1, Lcom/amazon/android/c/a;->a:Lcom/amazon/android/framework/util/KiwiLogger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cannot delete file: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/amazon/android/framework/util/KiwiLogger;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_8
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .registers 8

    const-string v6, "/"

    invoke-direct {p0}, Lcom/amazon/android/c/a;->b()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    :cond_c
    return-object v0

    :cond_d
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/amazon/android/c/a;->d:Landroid/app/Application;

    invoke-virtual {v3}, Landroid/app/Application;->getFilesDir()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v2, Lcom/amazon/android/c/c;

    invoke-direct {v2, p0}, Lcom/amazon/android/c/c;-><init>(Lcom/amazon/android/c/a;)V

    invoke-virtual {v1, v2}, Ljava/io/File;->list(Ljava/io/FilenameFilter;)[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    :goto_3e
    array-length v3, v1

    if-ge v2, v3, :cond_c

    invoke-direct {p0}, Lcom/amazon/android/c/a;->b()Z

    move-result v3

    if-nez v3, :cond_c

    aget-object v3, v1, v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/amazon/android/c/a;->d:Landroid/app/Application;

    invoke-virtual {v5}, Landroid/app/Application;->getFilesDir()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/amazon/android/c/a;->b(Ljava/lang/String;)Lcom/amazon/android/c/b;

    move-result-object v4

    if-eqz v4, :cond_7b

    iget-object v5, p0, Lcom/amazon/android/c/a;->f:Ljava/util/Map;

    invoke-interface {v5, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_78
    add-int/lit8 v2, v2, 0x1

    goto :goto_3e

    :cond_7b
    invoke-static {v3}, Lcom/amazon/android/c/a;->d(Ljava/lang/String;)V

    goto :goto_78
.end method

.method public final a(Ljava/util/List;)V
    .registers 5

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_21

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amazon/android/c/b;

    iget-object v1, p0, Lcom/amazon/android/c/a;->f:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/amazon/android/c/a;->d(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/amazon/android/c/a;->f:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    :cond_21
    return-void
.end method

.method public final onResourcesPopulated()V
    .registers 3

    invoke-static {}, Lcom/amazon/android/d/a;->a()V

    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v0

    instance-of v0, v0, Lcom/amazon/android/c/d;

    if-nez v0, :cond_1f

    sget-boolean v0, Lcom/amazon/android/framework/util/KiwiLogger;->TRACE_ON:Z

    if-eqz v0, :cond_16

    sget-object v0, Lcom/amazon/android/c/a;->a:Lcom/amazon/android/framework/util/KiwiLogger;

    const-string v1, "Registering Crash Handler"

    invoke-virtual {v0, v1}, Lcom/amazon/android/framework/util/KiwiLogger;->trace(Ljava/lang/String;)V

    :cond_16
    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v0

    iput-object v0, p0, Lcom/amazon/android/c/a;->e:Ljava/lang/Thread$UncaughtExceptionHandler;

    invoke-static {p0}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    :cond_1f
    return-void
.end method

.method public final uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V
    .registers 6

    sget-boolean v0, Lcom/amazon/android/framework/util/KiwiLogger;->TRACE_ON:Z

    if-eqz v0, :cond_b

    sget-object v0, Lcom/amazon/android/c/a;->a:Lcom/amazon/android/framework/util/KiwiLogger;

    const-string v1, "Crash detected"

    invoke-virtual {v0, v1, p2}, Lcom/amazon/android/framework/util/KiwiLogger;->trace(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_b
    :try_start_b
    new-instance v0, Lcom/amazon/android/c/b;

    iget-object v1, p0, Lcom/amazon/android/c/a;->d:Landroid/app/Application;

    invoke-direct {v0, v1, p2}, Lcom/amazon/android/c/b;-><init>(Landroid/app/Application;Ljava/lang/Throwable;)V

    invoke-static {v0}, Lcom/amazon/android/u/a;->a(Ljava/io/Serializable;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/amazon/android/c/a;->c:Lcom/amazon/android/g/c;

    invoke-interface {v1, v0}, Lcom/amazon/android/g/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/amazon/android/c/a;->a(Ljava/lang/String;)V
    :try_end_1f
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_1f} :catch_3e

    :cond_1f
    :goto_1f
    :try_start_1f
    iget-object v0, p0, Lcom/amazon/android/c/a;->b:Lcom/amazon/android/n/g;

    new-instance v1, Lcom/amazon/android/a/b;

    invoke-direct {v1}, Lcom/amazon/android/a/b;-><init>()V

    invoke-interface {v0, v1}, Lcom/amazon/android/n/g;->a(Lcom/amazon/android/n/d;)V
    :try_end_29
    .catch Ljava/lang/Throwable; {:try_start_1f .. :try_end_29} :catch_4b

    :cond_29
    :goto_29
    sget-boolean v0, Lcom/amazon/android/framework/util/KiwiLogger;->TRACE_ON:Z

    if-eqz v0, :cond_34

    sget-object v0, Lcom/amazon/android/c/a;->a:Lcom/amazon/android/framework/util/KiwiLogger;

    const-string v1, "Calling previous handler"

    invoke-virtual {v0, v1}, Lcom/amazon/android/framework/util/KiwiLogger;->trace(Ljava/lang/String;)V

    :cond_34
    iget-object v0, p0, Lcom/amazon/android/c/a;->e:Ljava/lang/Thread$UncaughtExceptionHandler;

    if-eqz v0, :cond_3d

    iget-object v0, p0, Lcom/amazon/android/c/a;->e:Ljava/lang/Thread$UncaughtExceptionHandler;

    invoke-interface {v0, p1, p2}, Ljava/lang/Thread$UncaughtExceptionHandler;->uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V

    :cond_3d
    return-void

    :catch_3e
    move-exception v0

    :try_start_3f
    sget-boolean v1, Lcom/amazon/android/framework/util/KiwiLogger;->ERROR_ON:Z

    if-eqz v1, :cond_1f

    sget-object v1, Lcom/amazon/android/c/a;->a:Lcom/amazon/android/framework/util/KiwiLogger;

    const-string v2, "Could not handle uncaught exception"

    invoke-virtual {v1, v2, v0}, Lcom/amazon/android/framework/util/KiwiLogger;->error(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4a
    .catch Ljava/lang/Throwable; {:try_start_3f .. :try_end_4a} :catch_4b

    goto :goto_1f

    :catch_4b
    move-exception v0

    sget-boolean v1, Lcom/amazon/android/framework/util/KiwiLogger;->TRACE_ON:Z

    if-eqz v1, :cond_29

    sget-object v1, Lcom/amazon/android/c/a;->a:Lcom/amazon/android/framework/util/KiwiLogger;

    const-string v2, "Error occured while handling exception"

    invoke-virtual {v1, v2, v0}, Lcom/amazon/android/framework/util/KiwiLogger;->trace(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_29
.end method
