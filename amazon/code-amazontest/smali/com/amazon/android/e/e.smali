.class final Lcom/amazon/android/e/e;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/amazon/android/n/c;


# instance fields
.field private synthetic a:Lcom/amazon/android/e/c;


# direct methods
.method constructor <init>(Lcom/amazon/android/e/c;)V
    .registers 2

    iput-object p1, p0, Lcom/amazon/android/e/e;->a:Lcom/amazon/android/e/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/amazon/android/n/f;
    .registers 2

    sget-object v0, Lcom/amazon/android/j/c;->c:Lcom/amazon/android/j/c;

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/amazon/android/n/d;)V
    .registers 6

    check-cast p1, Lcom/amazon/android/j/b;

    iget-object v0, p0, Lcom/amazon/android/e/e;->a:Lcom/amazon/android/e/c;

    invoke-static {v0}, Lcom/amazon/android/e/c;->a(Lcom/amazon/android/e/c;)Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/amazon/android/e/a;

    if-eqz p0, :cond_45

    iget-object v0, p1, Lcom/amazon/android/j/b;->a:Landroid/app/Activity;

    invoke-static {}, Lcom/amazon/android/e/c;->a()Lcom/amazon/android/framework/util/KiwiLogger;

    move-result-object v1

    const-string v2, "Context changed while awaiting result!"

    invoke-virtual {v1, v2}, Lcom/amazon/android/framework/util/KiwiLogger;->error(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/amazon/android/e/a;->b:Landroid/app/Activity;

    if-eqz v1, :cond_42

    invoke-static {}, Lcom/amazon/android/e/c;->a()Lcom/amazon/android/framework/util/KiwiLogger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Finishing activity from old context: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/amazon/android/e/a;->b:Landroid/app/Activity;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/amazon/android/framework/util/KiwiLogger;->error(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/amazon/android/e/a;->b:Landroid/app/Activity;

    iget v2, p0, Lcom/amazon/android/e/a;->a:I

    invoke-virtual {v1, v2}, Landroid/app/Activity;->finishActivity(I)V

    :cond_42
    invoke-virtual {p0, v0}, Lcom/amazon/android/e/a;->a(Landroid/app/Activity;)V

    :cond_45
    return-void
.end method

.method public final b()Lcom/amazon/android/n/a;
    .registers 2

    sget-object v0, Lcom/amazon/android/n/a;->b:Lcom/amazon/android/n/a;

    return-object v0
.end method
