.class public Lcom/amazon/android/framework/prompt/PromptManagerImpl;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/amazon/android/framework/prompt/PromptManager;
.implements Lcom/amazon/android/framework/resource/b;


# static fields
.field public static final LOGGER:Lcom/amazon/android/framework/util/KiwiLogger;


# instance fields
.field private contextManager:Lcom/amazon/android/framework/context/ContextManager;
    .annotation runtime Lcom/amazon/android/framework/resource/Resource;
    .end annotation
.end field

.field private eventManager:Lcom/amazon/android/n/g;
    .annotation runtime Lcom/amazon/android/framework/resource/Resource;
    .end annotation
.end field

.field private final finished:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private pending:Ljava/util/Set;

.field private resourceManager:Lcom/amazon/android/framework/resource/a;
    .annotation runtime Lcom/amazon/android/framework/resource/Resource;
    .end annotation
.end field

.field private showing:Lcom/amazon/android/framework/prompt/Prompt;

.field private taskManager:Lcom/amazon/android/framework/task/TaskManager;
    .annotation runtime Lcom/amazon/android/framework/resource/Resource;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 2

    new-instance v0, Lcom/amazon/android/framework/util/KiwiLogger;

    const-string v1, "PromptManagerImpl"

    invoke-direct {v0, v1}, Lcom/amazon/android/framework/util/KiwiLogger;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/amazon/android/framework/prompt/PromptManagerImpl;->LOGGER:Lcom/amazon/android/framework/util/KiwiLogger;

    return-void
.end method

.method public constructor <init>()V
    .registers 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/amazon/android/framework/prompt/PromptManagerImpl;->pending:Ljava/util/Set;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/amazon/android/framework/prompt/PromptManagerImpl;->finished:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method

.method static synthetic a(Lcom/amazon/android/framework/prompt/PromptManagerImpl;)V
    .registers 1

    invoke-direct {p0}, Lcom/amazon/android/framework/prompt/PromptManagerImpl;->finish()V

    return-void
.end method

.method static synthetic a(Lcom/amazon/android/framework/prompt/PromptManagerImpl;Landroid/app/Activity;)V
    .registers 2

    invoke-direct {p0, p1}, Lcom/amazon/android/framework/prompt/PromptManagerImpl;->onResume(Landroid/app/Activity;)V

    return-void
.end method

.method static synthetic a(Lcom/amazon/android/framework/prompt/PromptManagerImpl;Lcom/amazon/android/framework/prompt/Prompt;)V
    .registers 2

    invoke-direct {p0, p1}, Lcom/amazon/android/framework/prompt/PromptManagerImpl;->presentImpl(Lcom/amazon/android/framework/prompt/Prompt;)V

    return-void
.end method

.method static synthetic b(Lcom/amazon/android/framework/prompt/PromptManagerImpl;)Lcom/amazon/android/framework/prompt/Prompt;
    .registers 2

    iget-object v0, p0, Lcom/amazon/android/framework/prompt/PromptManagerImpl;->showing:Lcom/amazon/android/framework/prompt/Prompt;

    return-object v0
.end method

.method static synthetic b(Lcom/amazon/android/framework/prompt/PromptManagerImpl;Lcom/amazon/android/framework/prompt/Prompt;)V
    .registers 2

    invoke-direct {p0, p1}, Lcom/amazon/android/framework/prompt/PromptManagerImpl;->removeExpiredPrompt(Lcom/amazon/android/framework/prompt/Prompt;)V

    return-void
.end method

.method private finish()V
    .registers 4

    iget-object v0, p0, Lcom/amazon/android/framework/prompt/PromptManagerImpl;->finished:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-nez v0, :cond_b

    :cond_a
    :goto_a
    return-void

    :cond_b
    sget-boolean v0, Lcom/amazon/android/framework/util/KiwiLogger;->TRACE_ON:Z

    if-eqz v0, :cond_16

    sget-object v0, Lcom/amazon/android/framework/prompt/PromptManagerImpl;->LOGGER:Lcom/amazon/android/framework/util/KiwiLogger;

    const-string v1, "PromptManager finishing...."

    invoke-virtual {v0, v1}, Lcom/amazon/android/framework/util/KiwiLogger;->trace(Ljava/lang/String;)V

    :cond_16
    iget-object v0, p0, Lcom/amazon/android/framework/prompt/PromptManagerImpl;->pending:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1c
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2f

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amazon/android/framework/prompt/Prompt;

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    invoke-virtual {v0}, Lcom/amazon/android/framework/prompt/Prompt;->expire()V

    goto :goto_1c

    :cond_2f
    iget-object v0, p0, Lcom/amazon/android/framework/prompt/PromptManagerImpl;->showing:Lcom/amazon/android/framework/prompt/Prompt;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/amazon/android/framework/prompt/PromptManagerImpl;->showing:Lcom/amazon/android/framework/prompt/Prompt;

    invoke-virtual {v0}, Lcom/amazon/android/framework/prompt/Prompt;->dismiss()Z

    goto :goto_a
.end method

.method private getNextPending()Lcom/amazon/android/framework/prompt/Prompt;
    .registers 2

    iget-object v0, p0, Lcom/amazon/android/framework/prompt/PromptManagerImpl;->pending:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_a

    const/4 v0, 0x0

    :goto_9
    return-object v0

    :cond_a
    iget-object v0, p0, Lcom/amazon/android/framework/prompt/PromptManagerImpl;->pending:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/amazon/android/framework/prompt/Prompt;

    move-object v0, p0

    goto :goto_9
.end method

.method private onResume(Landroid/app/Activity;)V
    .registers 3

    iget-object v0, p0, Lcom/amazon/android/framework/prompt/PromptManagerImpl;->showing:Lcom/amazon/android/framework/prompt/Prompt;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/amazon/android/framework/prompt/PromptManagerImpl;->showing:Lcom/amazon/android/framework/prompt/Prompt;

    invoke-direct {p0, v0, p1}, Lcom/amazon/android/framework/prompt/PromptManagerImpl;->show(Lcom/amazon/android/framework/prompt/Prompt;Landroid/app/Activity;)V

    :goto_9
    return-void

    :cond_a
    invoke-direct {p0, p1}, Lcom/amazon/android/framework/prompt/PromptManagerImpl;->presentNextPending(Landroid/app/Activity;)V

    goto :goto_9
.end method

.method private presentImpl(Lcom/amazon/android/framework/prompt/Prompt;)V
    .registers 5

    iget-object v0, p0, Lcom/amazon/android/framework/prompt/PromptManagerImpl;->finished:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_34

    sget-boolean v0, Lcom/amazon/android/framework/util/KiwiLogger;->ERROR_ON:Z

    if-eqz v0, :cond_30

    sget-object v0, Lcom/amazon/android/framework/prompt/PromptManagerImpl;->LOGGER:Lcom/amazon/android/framework/util/KiwiLogger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Prompt: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " presented after app"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " destruction expiring it now!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amazon/android/framework/util/KiwiLogger;->error(Ljava/lang/String;)V

    :cond_30
    invoke-virtual {p1}, Lcom/amazon/android/framework/prompt/Prompt;->expire()V

    :cond_33
    :goto_33
    return-void

    :cond_34
    sget-boolean v0, Lcom/amazon/android/framework/util/KiwiLogger;->TRACE_ON:Z

    if-eqz v0, :cond_50

    sget-object v0, Lcom/amazon/android/framework/prompt/PromptManagerImpl;->LOGGER:Lcom/amazon/android/framework/util/KiwiLogger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Presening Prompt: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amazon/android/framework/util/KiwiLogger;->trace(Ljava/lang/String;)V

    :cond_50
    invoke-virtual {p1, p0}, Lcom/amazon/android/framework/prompt/Prompt;->register(Lcom/amazon/android/i/d;)V

    iget-object v0, p0, Lcom/amazon/android/framework/prompt/PromptManagerImpl;->pending:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/amazon/android/framework/prompt/PromptManagerImpl;->showing:Lcom/amazon/android/framework/prompt/Prompt;

    if-eqz v0, :cond_68

    sget-boolean v0, Lcom/amazon/android/framework/util/KiwiLogger;->TRACE_ON:Z

    if-eqz v0, :cond_33

    sget-object v0, Lcom/amazon/android/framework/prompt/PromptManagerImpl;->LOGGER:Lcom/amazon/android/framework/util/KiwiLogger;

    const-string v1, "Dialog currently showing, not presenting given dialog"

    invoke-virtual {v0, v1}, Lcom/amazon/android/framework/util/KiwiLogger;->trace(Ljava/lang/String;)V

    goto :goto_33

    :cond_68
    iget-object v0, p0, Lcom/amazon/android/framework/prompt/PromptManagerImpl;->contextManager:Lcom/amazon/android/framework/context/ContextManager;

    invoke-interface {v0}, Lcom/amazon/android/framework/context/ContextManager;->getVisible()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_33

    invoke-direct {p0, v0}, Lcom/amazon/android/framework/prompt/PromptManagerImpl;->presentNextPending(Landroid/app/Activity;)V

    goto :goto_33
.end method

.method private presentNextPending(Landroid/app/Activity;)V
    .registers 3

    invoke-direct {p0}, Lcom/amazon/android/framework/prompt/PromptManagerImpl;->getNextPending()Lcom/amazon/android/framework/prompt/Prompt;

    move-result-object v0

    if-nez v0, :cond_7

    :goto_6
    return-void

    :cond_7
    invoke-direct {p0, v0, p1}, Lcom/amazon/android/framework/prompt/PromptManagerImpl;->show(Lcom/amazon/android/framework/prompt/Prompt;Landroid/app/Activity;)V

    goto :goto_6
.end method

.method private registerActivityResumedListener()V
    .registers 3

    iget-object v0, p0, Lcom/amazon/android/framework/prompt/PromptManagerImpl;->eventManager:Lcom/amazon/android/n/g;

    new-instance v1, Lcom/amazon/android/framework/prompt/f;

    invoke-direct {v1, p0}, Lcom/amazon/android/framework/prompt/f;-><init>(Lcom/amazon/android/framework/prompt/PromptManagerImpl;)V

    invoke-interface {v0, v1}, Lcom/amazon/android/n/g;->a(Lcom/amazon/android/n/c;)V

    return-void
.end method

.method private registerAppDestructionListener()V
    .registers 3

    new-instance v0, Lcom/amazon/android/framework/prompt/i;

    invoke-direct {v0, p0}, Lcom/amazon/android/framework/prompt/i;-><init>(Lcom/amazon/android/framework/prompt/PromptManagerImpl;)V

    iget-object v1, p0, Lcom/amazon/android/framework/prompt/PromptManagerImpl;->eventManager:Lcom/amazon/android/n/g;

    invoke-interface {v1, v0}, Lcom/amazon/android/n/g;->a(Lcom/amazon/android/n/c;)V

    return-void
.end method

.method private registerTestModeListener()V
    .registers 3

    new-instance v0, Lcom/amazon/android/framework/prompt/j;

    invoke-direct {v0, p0}, Lcom/amazon/android/framework/prompt/j;-><init>(Lcom/amazon/android/framework/prompt/PromptManagerImpl;)V

    iget-object v1, p0, Lcom/amazon/android/framework/prompt/PromptManagerImpl;->eventManager:Lcom/amazon/android/n/g;

    invoke-interface {v1, v0}, Lcom/amazon/android/n/g;->a(Lcom/amazon/android/n/c;)V

    return-void
.end method

.method private removeExpiredPrompt(Lcom/amazon/android/framework/prompt/Prompt;)V
    .registers 3

    iget-object v0, p0, Lcom/amazon/android/framework/prompt/PromptManagerImpl;->pending:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/amazon/android/framework/prompt/PromptManagerImpl;->showing:Lcom/amazon/android/framework/prompt/Prompt;

    if-ne v0, p1, :cond_17

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amazon/android/framework/prompt/PromptManagerImpl;->showing:Lcom/amazon/android/framework/prompt/Prompt;

    iget-object v0, p0, Lcom/amazon/android/framework/prompt/PromptManagerImpl;->contextManager:Lcom/amazon/android/framework/context/ContextManager;

    invoke-interface {v0}, Lcom/amazon/android/framework/context/ContextManager;->getVisible()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_17

    invoke-direct {p0, v0}, Lcom/amazon/android/framework/prompt/PromptManagerImpl;->presentNextPending(Landroid/app/Activity;)V

    :cond_17
    return-void
.end method

.method private show(Lcom/amazon/android/framework/prompt/Prompt;Landroid/app/Activity;)V
    .registers 3

    iput-object p1, p0, Lcom/amazon/android/framework/prompt/PromptManagerImpl;->showing:Lcom/amazon/android/framework/prompt/Prompt;

    invoke-virtual {p1, p2}, Lcom/amazon/android/framework/prompt/Prompt;->show(Landroid/app/Activity;)V

    return-void
.end method


# virtual methods
.method public observe(Lcom/amazon/android/framework/prompt/Prompt;)V
    .registers 5

    new-instance v0, Lcom/amazon/android/framework/prompt/e;

    invoke-direct {v0, p0, p1}, Lcom/amazon/android/framework/prompt/e;-><init>(Lcom/amazon/android/framework/prompt/PromptManagerImpl;Lcom/amazon/android/framework/prompt/Prompt;)V

    iget-object v1, p0, Lcom/amazon/android/framework/prompt/PromptManagerImpl;->taskManager:Lcom/amazon/android/framework/task/TaskManager;

    sget-object v2, Lcom/amazon/android/framework/task/pipeline/TaskPipelineId;->FOREGROUND:Lcom/amazon/android/framework/task/pipeline/TaskPipelineId;

    invoke-interface {v1, v2, v0}, Lcom/amazon/android/framework/task/TaskManager;->enqueue(Lcom/amazon/android/framework/task/pipeline/TaskPipelineId;Lcom/amazon/android/framework/task/Task;)V

    return-void
.end method

.method public bridge synthetic observe(Lcom/amazon/android/i/b;)V
    .registers 2

    check-cast p1, Lcom/amazon/android/framework/prompt/Prompt;

    invoke-virtual {p0, p1}, Lcom/amazon/android/framework/prompt/PromptManagerImpl;->observe(Lcom/amazon/android/framework/prompt/Prompt;)V

    return-void
.end method

.method public onCreateDialog(Landroid/app/Activity;I)Landroid/app/Dialog;
    .registers 7

    const/4 v3, 0x0

    sget-boolean v0, Lcom/amazon/android/framework/util/KiwiLogger;->TRACE_ON:Z

    if-eqz v0, :cond_27

    sget-object v0, Lcom/amazon/android/framework/prompt/PromptManagerImpl;->LOGGER:Lcom/amazon/android/framework/util/KiwiLogger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCreateDialog, id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", activity: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amazon/android/framework/util/KiwiLogger;->trace(Ljava/lang/String;)V

    :cond_27
    iget-object v0, p0, Lcom/amazon/android/framework/prompt/PromptManagerImpl;->showing:Lcom/amazon/android/framework/prompt/Prompt;

    if-nez v0, :cond_38

    sget-boolean v0, Lcom/amazon/android/framework/util/KiwiLogger;->TRACE_ON:Z

    if-eqz v0, :cond_36

    sget-object v0, Lcom/amazon/android/framework/prompt/PromptManagerImpl;->LOGGER:Lcom/amazon/android/framework/util/KiwiLogger;

    const-string v1, "Showing dialog is null, returning"

    invoke-virtual {v0, v1}, Lcom/amazon/android/framework/util/KiwiLogger;->trace(Ljava/lang/String;)V

    :cond_36
    move-object v0, v3

    :goto_37
    return-object v0

    :cond_38
    iget-object v0, p0, Lcom/amazon/android/framework/prompt/PromptManagerImpl;->showing:Lcom/amazon/android/framework/prompt/Prompt;

    invoke-virtual {v0}, Lcom/amazon/android/framework/prompt/Prompt;->getIdentifier()I

    move-result v0

    if-eq v0, p2, :cond_64

    sget-boolean v0, Lcom/amazon/android/framework/util/KiwiLogger;->TRACE_ON:Z

    if-eqz v0, :cond_62

    sget-object v0, Lcom/amazon/android/framework/prompt/PromptManagerImpl;->LOGGER:Lcom/amazon/android/framework/util/KiwiLogger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Showing dialog id does not match given id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", returning"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amazon/android/framework/util/KiwiLogger;->trace(Ljava/lang/String;)V

    :cond_62
    move-object v0, v3

    goto :goto_37

    :cond_64
    sget-boolean v0, Lcom/amazon/android/framework/util/KiwiLogger;->TRACE_ON:Z

    if-eqz v0, :cond_82

    sget-object v0, Lcom/amazon/android/framework/prompt/PromptManagerImpl;->LOGGER:Lcom/amazon/android/framework/util/KiwiLogger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Creating dialog prompt: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/amazon/android/framework/prompt/PromptManagerImpl;->showing:Lcom/amazon/android/framework/prompt/Prompt;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amazon/android/framework/util/KiwiLogger;->trace(Ljava/lang/String;)V

    :cond_82
    iget-object v0, p0, Lcom/amazon/android/framework/prompt/PromptManagerImpl;->showing:Lcom/amazon/android/framework/prompt/Prompt;

    invoke-virtual {v0, p1}, Lcom/amazon/android/framework/prompt/Prompt;->create(Landroid/app/Activity;)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_37
.end method

.method public onResourcesPopulated()V
    .registers 1

    invoke-direct {p0}, Lcom/amazon/android/framework/prompt/PromptManagerImpl;->registerActivityResumedListener()V

    invoke-direct {p0}, Lcom/amazon/android/framework/prompt/PromptManagerImpl;->registerAppDestructionListener()V

    invoke-direct {p0}, Lcom/amazon/android/framework/prompt/PromptManagerImpl;->registerTestModeListener()V

    return-void
.end method

.method public onWindowFocusChanged(Landroid/app/Activity;Z)V
    .registers 4

    iget-object v0, p0, Lcom/amazon/android/framework/prompt/PromptManagerImpl;->showing:Lcom/amazon/android/framework/prompt/Prompt;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/amazon/android/framework/prompt/PromptManagerImpl;->showing:Lcom/amazon/android/framework/prompt/Prompt;

    invoke-virtual {v0, p1, p2}, Lcom/amazon/android/framework/prompt/Prompt;->onFocusChanged(Landroid/app/Activity;Z)V

    :cond_9
    return-void
.end method

.method public present(Lcom/amazon/android/framework/prompt/Prompt;)V
    .registers 5

    sget-boolean v0, Lcom/amazon/android/framework/util/KiwiLogger;->TRACE_ON:Z

    if-eqz v0, :cond_1c

    sget-object v0, Lcom/amazon/android/framework/prompt/PromptManagerImpl;->LOGGER:Lcom/amazon/android/framework/util/KiwiLogger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Scheduling presentation: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amazon/android/framework/util/KiwiLogger;->trace(Ljava/lang/String;)V

    :cond_1c
    iget-object v0, p0, Lcom/amazon/android/framework/prompt/PromptManagerImpl;->resourceManager:Lcom/amazon/android/framework/resource/a;

    invoke-interface {v0, p1}, Lcom/amazon/android/framework/resource/a;->b(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/amazon/android/framework/prompt/PromptManagerImpl;->finished:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_55

    sget-boolean v0, Lcom/amazon/android/framework/util/KiwiLogger;->ERROR_ON:Z

    if-eqz v0, :cond_51

    sget-object v0, Lcom/amazon/android/framework/prompt/PromptManagerImpl;->LOGGER:Lcom/amazon/android/framework/util/KiwiLogger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Prompt: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " presented after app"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " destruction expiring it now!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amazon/android/framework/util/KiwiLogger;->error(Ljava/lang/String;)V

    :cond_51
    invoke-virtual {p1}, Lcom/amazon/android/framework/prompt/Prompt;->expire()V

    :goto_54
    return-void

    :cond_55
    new-instance v0, Lcom/amazon/android/framework/prompt/h;

    invoke-direct {v0, p0, p1}, Lcom/amazon/android/framework/prompt/h;-><init>(Lcom/amazon/android/framework/prompt/PromptManagerImpl;Lcom/amazon/android/framework/prompt/Prompt;)V

    iget-object v1, p0, Lcom/amazon/android/framework/prompt/PromptManagerImpl;->taskManager:Lcom/amazon/android/framework/task/TaskManager;

    sget-object v2, Lcom/amazon/android/framework/task/pipeline/TaskPipelineId;->FOREGROUND:Lcom/amazon/android/framework/task/pipeline/TaskPipelineId;

    invoke-interface {v1, v2, v0}, Lcom/amazon/android/framework/task/TaskManager;->enqueue(Lcom/amazon/android/framework/task/pipeline/TaskPipelineId;Lcom/amazon/android/framework/task/Task;)V

    goto :goto_54
.end method
