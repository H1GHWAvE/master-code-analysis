.class public final Lcom/amazon/android/q/c;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/amazon/android/q/d;


# static fields
.field private static final a:Lcom/amazon/android/framework/util/KiwiLogger;


# instance fields
.field private b:Lcom/amazon/android/q/a;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    new-instance v0, Lcom/amazon/android/framework/util/KiwiLogger;

    const-string v1, "MetricsManagerImpl"

    invoke-direct {v0, v1}, Lcom/amazon/android/framework/util/KiwiLogger;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/amazon/android/q/c;->a:Lcom/amazon/android/framework/util/KiwiLogger;

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/amazon/android/q/a;

    invoke-direct {v0}, Lcom/amazon/android/q/a;-><init>()V

    iput-object v0, p0, Lcom/amazon/android/q/c;->b:Lcom/amazon/android/q/a;

    return-void
.end method


# virtual methods
.method public final declared-synchronized a()Lcom/amazon/android/q/a;
    .registers 3

    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/amazon/android/q/c;->b:Lcom/amazon/android/q/a;

    invoke-virtual {v0}, Lcom/amazon/android/q/a;->a()Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/amazon/android/q/c;->b:Lcom/amazon/android/q/a;
    :try_end_b
    .catchall {:try_start_1 .. :try_end_b} :catchall_17

    :goto_b
    monitor-exit p0

    return-object v0

    :cond_d
    :try_start_d
    iget-object v0, p0, Lcom/amazon/android/q/c;->b:Lcom/amazon/android/q/a;

    new-instance v1, Lcom/amazon/android/q/a;

    invoke-direct {v1}, Lcom/amazon/android/q/a;-><init>()V

    iput-object v1, p0, Lcom/amazon/android/q/c;->b:Lcom/amazon/android/q/a;
    :try_end_16
    .catchall {:try_start_d .. :try_end_16} :catchall_17

    goto :goto_b

    :catchall_17
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/amazon/android/q/b;)V
    .registers 5

    monitor-enter p0

    :try_start_1
    sget-boolean v0, Lcom/amazon/android/framework/util/KiwiLogger;->TRACE_ON:Z

    if-eqz v0, :cond_1d

    sget-object v0, Lcom/amazon/android/q/c;->a:Lcom/amazon/android/framework/util/KiwiLogger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Recording Metric: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amazon/android/framework/util/KiwiLogger;->trace(Ljava/lang/String;)V

    :cond_1d
    iget-object v0, p0, Lcom/amazon/android/q/c;->b:Lcom/amazon/android/q/a;

    iget-object v0, v0, Lcom/amazon/android/q/a;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_24
    .catchall {:try_start_1 .. :try_end_24} :catchall_26

    monitor-exit p0

    return-void

    :catchall_26
    move-exception v0

    monitor-exit p0

    throw v0
.end method
