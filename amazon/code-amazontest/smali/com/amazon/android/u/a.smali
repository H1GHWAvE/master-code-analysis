.class public final Lcom/amazon/android/u/a;
.super Ljava/lang/Object;


# static fields
.field private static final a:Lcom/amazon/android/framework/util/KiwiLogger;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    new-instance v0, Lcom/amazon/android/framework/util/KiwiLogger;

    const-string v1, "Serializer"

    invoke-direct {v0, v1}, Lcom/amazon/android/framework/util/KiwiLogger;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/amazon/android/u/a;->a:Lcom/amazon/android/framework/util/KiwiLogger;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/Object;
    .registers 7

    const/4 v5, 0x0

    if-eqz p0, :cond_9

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_b

    :cond_9
    move-object v0, v5

    :goto_a
    return-object v0

    :cond_b
    :try_start_b
    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-static {v0}, Lcom/amazon/mas/kiwi/util/Base64;->decode([B)[B
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_12} :catch_25

    move-result-object v0

    :try_start_13
    new-instance v1, Ljava/io/ObjectInputStream;

    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-direct {v2, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v1, v2}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_1d
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_1d} :catch_33
    .catchall {:try_start_13 .. :try_end_1d} :catchall_56

    :try_start_1d
    invoke-virtual {v1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;
    :try_end_20
    .catch Ljava/lang/Exception; {:try_start_1d .. :try_end_20} :catch_5e
    .catchall {:try_start_1d .. :try_end_20} :catchall_5c

    move-result-object v0

    invoke-static {v1}, Lcom/amazon/android/framework/util/a;->a(Ljava/io/InputStream;)V

    goto :goto_a

    :catch_25
    move-exception v0

    sget-boolean v1, Lcom/amazon/android/framework/util/KiwiLogger;->ERROR_ON:Z

    if-eqz v1, :cond_31

    sget-object v1, Lcom/amazon/android/u/a;->a:Lcom/amazon/android/framework/util/KiwiLogger;

    const-string v2, "Could not decode string"

    invoke-virtual {v1, v2, v0}, Lcom/amazon/android/framework/util/KiwiLogger;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_31
    move-object v0, v5

    goto :goto_a

    :catch_33
    move-exception v0

    move-object v1, v5

    :goto_35
    :try_start_35
    sget-boolean v2, Lcom/amazon/android/framework/util/KiwiLogger;->ERROR_ON:Z

    if-eqz v2, :cond_51

    sget-object v2, Lcom/amazon/android/u/a;->a:Lcom/amazon/android/framework/util/KiwiLogger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Could not read object from string: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Lcom/amazon/android/framework/util/KiwiLogger;->error(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_51
    .catchall {:try_start_35 .. :try_end_51} :catchall_5c

    :cond_51
    invoke-static {v1}, Lcom/amazon/android/framework/util/a;->a(Ljava/io/InputStream;)V

    move-object v0, v5

    goto :goto_a

    :catchall_56
    move-exception v0

    move-object v1, v5

    :goto_58
    invoke-static {v1}, Lcom/amazon/android/framework/util/a;->a(Ljava/io/InputStream;)V

    throw v0

    :catchall_5c
    move-exception v0

    goto :goto_58

    :catch_5e
    move-exception v0

    goto :goto_35
.end method

.method public static a(Ljava/io/Serializable;)Ljava/lang/String;
    .registers 7

    const/4 v5, 0x0

    if-nez p0, :cond_5

    move-object v0, v5

    :goto_4
    return-object v0

    :cond_5
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    :try_start_a
    new-instance v1, Ljava/io/ObjectOutputStream;

    invoke-direct {v1, v0}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_f} :catch_1e
    .catchall {:try_start_a .. :try_end_f} :catchall_41

    :try_start_f
    invoke-virtual {v1, p0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-static {v0}, Lcom/amazon/mas/kiwi/util/Base64;->encodeBytes([B)Ljava/lang/String;
    :try_end_19
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_19} :catch_49
    .catchall {:try_start_f .. :try_end_19} :catchall_47

    move-result-object v0

    invoke-static {v1}, Lcom/amazon/android/framework/util/a;->a(Ljava/io/OutputStream;)V

    goto :goto_4

    :catch_1e
    move-exception v0

    move-object v1, v5

    :goto_20
    :try_start_20
    sget-boolean v2, Lcom/amazon/android/framework/util/KiwiLogger;->ERROR_ON:Z

    if-eqz v2, :cond_3c

    sget-object v2, Lcom/amazon/android/u/a;->a:Lcom/amazon/android/framework/util/KiwiLogger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Could not serialize object: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Lcom/amazon/android/framework/util/KiwiLogger;->error(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3c
    .catchall {:try_start_20 .. :try_end_3c} :catchall_47

    :cond_3c
    invoke-static {v1}, Lcom/amazon/android/framework/util/a;->a(Ljava/io/OutputStream;)V

    move-object v0, v5

    goto :goto_4

    :catchall_41
    move-exception v0

    move-object v1, v5

    :goto_43
    invoke-static {v1}, Lcom/amazon/android/framework/util/a;->a(Ljava/io/OutputStream;)V

    throw v0

    :catchall_47
    move-exception v0

    goto :goto_43

    :catch_49
    move-exception v0

    goto :goto_20
.end method
