.class public abstract Lcom/amazon/android/l/c;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/amazon/android/l/a;


# instance fields
.field private workflow:Lcom/amazon/android/l/b;


# direct methods
.method public constructor <init>()V
    .registers 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method protected final isWorkflowChild()Z
    .registers 2

    iget-object v0, p0, Lcom/amazon/android/l/c;->workflow:Lcom/amazon/android/l/b;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method protected final quitParentWorkflow()V
    .registers 3

    invoke-virtual {p0}, Lcom/amazon/android/l/c;->isWorkflowChild()Z

    move-result v0

    const-string v1, "task is no a workflow child"

    invoke-static {v0, v1}, Lcom/amazon/android/d/a;->a(ZLjava/lang/String;)V

    iget-object v0, p0, Lcom/amazon/android/l/c;->workflow:Lcom/amazon/android/l/b;

    invoke-virtual {v0}, Lcom/amazon/android/l/b;->c()V

    return-void
.end method

.method public final setWorkflow(Lcom/amazon/android/l/b;)V
    .registers 4

    const-string v0, "workflow"

    invoke-static {p1, v0}, Lcom/amazon/android/d/a;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/amazon/android/l/c;->workflow:Lcom/amazon/android/l/b;

    const-string v1, "workflow instance can only be set once"

    if-nez v0, :cond_12

    const/4 v0, 0x1

    :goto_c
    invoke-static {v0, v1}, Lcom/amazon/android/d/a;->a(ZLjava/lang/String;)V

    iput-object p1, p0, Lcom/amazon/android/l/c;->workflow:Lcom/amazon/android/l/b;

    return-void

    :cond_12
    const/4 v0, 0x0

    goto :goto_c
.end method
