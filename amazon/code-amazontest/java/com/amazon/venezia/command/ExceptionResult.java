package com.amazon.venezia.command;
public interface ExceptionResult implements android.os.IInterface {

    public abstract String getErrorCode();

    public abstract java.util.Map getExtensionData();
}
