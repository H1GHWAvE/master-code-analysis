package com.amazon.venezia.command;
 class Choice$Stub$Proxy implements com.amazon.venezia.command.Choice {
    private android.os.IBinder mRemote;

    Choice$Stub$Proxy(android.os.IBinder p1)
    {
        this.mRemote = p1;
        return;
    }

    public android.os.IBinder asBinder()
    {
        return this.mRemote;
    }

    public void choose(com.amazon.venezia.command.ChoiceContext p6)
    {
        android.os.Parcel v0 = android.os.Parcel.obtain();
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            android.os.IBinder v2_1;
            v0.writeInterfaceToken("com.amazon.venezia.command.Choice");
        } catch (android.os.IBinder v2_2) {
            v1.recycle();
            v0.recycle();
            throw v2_2;
        }
        if (p6 == null) {
            v2_1 = 0;
        } else {
            v2_1 = p6.asBinder();
        }
        v0.writeStrongBinder(v2_1);
        this.mRemote.transact(3, v0, v1, 0);
        v1.readException();
        v1.recycle();
        v0.recycle();
        return;
    }

    public String getDisplayableName()
    {
        android.os.Parcel v0 = android.os.Parcel.obtain();
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            v0.writeInterfaceToken("com.amazon.venezia.command.Choice");
            this.mRemote.transact(1, v0, v1, 0);
            v1.readException();
            String v2 = v1.readString();
            v1.recycle();
            v0.recycle();
            return v2;
        } catch (Throwable v3_2) {
            v1.recycle();
            v0.recycle();
            throw v3_2;
        }
    }

    public java.util.Map getExtensionData()
    {
        android.os.Parcel v0 = android.os.Parcel.obtain();
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            v0.writeInterfaceToken("com.amazon.venezia.command.Choice");
            this.mRemote.transact(4, v0, v1, 0);
            v1.readException();
            java.util.HashMap v2 = v1.readHashMap(this.getClass().getClassLoader());
            v1.recycle();
            v0.recycle();
            return v2;
        } catch (Throwable v4_3) {
            v1.recycle();
            v0.recycle();
            throw v4_3;
        }
    }

    public android.content.Intent getIntent()
    {
        android.os.Parcel v0 = android.os.Parcel.obtain();
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            int v2_0;
            v0.writeInterfaceToken("com.amazon.venezia.command.Choice");
            this.mRemote.transact(2, v0, v1, 0);
            v1.readException();
        } catch (android.os.Parcelable$Creator v3_4) {
            v1.recycle();
            v0.recycle();
            throw v3_4;
        }
        if (v1.readInt() == 0) {
            v2_0 = 0;
        } else {
            v2_0 = ((android.content.Intent) android.content.Intent.CREATOR.createFromParcel(v1));
        }
        v1.recycle();
        v0.recycle();
        return v2_0;
    }

    public String getInterfaceDescriptor()
    {
        return "com.amazon.venezia.command.Choice";
    }
}
