package com.amazon.venezia.command;
public interface ResultCallback implements android.os.IInterface {

    public abstract void onDecide();

    public abstract void onException();

    public abstract void onFailure();

    public abstract void onSuccess();
}
