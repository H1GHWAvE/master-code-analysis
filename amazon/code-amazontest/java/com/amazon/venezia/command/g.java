package com.amazon.venezia.command;
public abstract class g extends android.os.Binder implements com.amazon.venezia.command.y {

    public g()
    {
        this.attachInterface(this, "com.amazon.venezia.command.ChoiceContext");
        return;
    }

    public boolean onTransact(int p4, android.os.Parcel p5, android.os.Parcel p6, int p7)
    {
        int v0_0;
        switch (p4) {
            case 1:
                p5.enforceInterface("com.amazon.venezia.command.ChoiceContext");
                int v0_1 = this.a();
                p6.writeNoException();
                p6.writeMap(v0_1);
                v0_0 = 1;
                break;
            case 1598968902:
                p6.writeString("com.amazon.venezia.command.ChoiceContext");
                v0_0 = 1;
                break;
            default:
                v0_0 = super.onTransact(p4, p5, p6, p7);
        }
        return v0_0;
    }
}
