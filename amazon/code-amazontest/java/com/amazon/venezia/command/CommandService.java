package com.amazon.venezia.command;
public interface CommandService implements android.os.IInterface {

    public abstract void execute();
}
