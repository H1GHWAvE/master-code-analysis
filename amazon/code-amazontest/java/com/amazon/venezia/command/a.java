package com.amazon.venezia.command;
final class a implements com.amazon.venezia.command.n {
    private android.os.IBinder a;

    a(android.os.IBinder p1)
    {
        this.a = p1;
        return;
    }

    public final String a()
    {
        android.os.Parcel v0 = android.os.Parcel.obtain();
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            v0.writeInterfaceToken("com.amazon.venezia.command.Choice");
            this.a.transact(1, v0, v1, 0);
            v1.readException();
            Throwable v2_2 = v1.readString();
            v1.recycle();
            v0.recycle();
            return v2_2;
        } catch (Throwable v2_3) {
            v1.recycle();
            v0.recycle();
            throw v2_3;
        }
    }

    public final void a(com.amazon.venezia.command.y p6)
    {
        android.os.Parcel v0 = android.os.Parcel.obtain();
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            android.os.IBinder v2_1;
            v0.writeInterfaceToken("com.amazon.venezia.command.Choice");
        } catch (android.os.IBinder v2_2) {
            v1.recycle();
            v0.recycle();
            throw v2_2;
        }
        if (p6 == null) {
            v2_1 = 0;
        } else {
            v2_1 = p6.asBinder();
        }
        v0.writeStrongBinder(v2_1);
        this.a.transact(3, v0, v1, 0);
        v1.readException();
        v1.recycle();
        v0.recycle();
        return;
    }

    public final android.os.IBinder asBinder()
    {
        return this.a;
    }

    public final android.content.Intent b()
    {
        android.os.Parcel v0 = android.os.Parcel.obtain();
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            int v2_4;
            v0.writeInterfaceToken("com.amazon.venezia.command.Choice");
            this.a.transact(2, v0, v1, 0);
            v1.readException();
        } catch (int v2_3) {
            v1.recycle();
            v0.recycle();
            throw v2_3;
        }
        if (v1.readInt() == 0) {
            v2_4 = 0;
        } else {
            v2_4 = ((android.content.Intent) android.content.Intent.CREATOR.createFromParcel(v1));
        }
        v1.recycle();
        v0.recycle();
        return v2_4;
    }

    public final java.util.Map c()
    {
        android.os.Parcel v0 = android.os.Parcel.obtain();
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            v0.writeInterfaceToken("com.amazon.venezia.command.Choice");
            this.a.transact(4, v0, v1, 0);
            v1.readException();
            Throwable v2_4 = v1.readHashMap(this.getClass().getClassLoader());
            v1.recycle();
            v0.recycle();
            return v2_4;
        } catch (Throwable v2_5) {
            v1.recycle();
            v0.recycle();
            throw v2_5;
        }
    }
}
