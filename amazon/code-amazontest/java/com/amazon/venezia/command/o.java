package com.amazon.venezia.command;
public abstract class o extends android.os.Binder implements com.amazon.venezia.command.f {

    public o()
    {
        this.attachInterface(this, "com.amazon.venezia.command.ResultCallback");
        return;
    }

    public android.os.IBinder asBinder()
    {
        return this;
    }

    public boolean onTransact(int p5, android.os.Parcel p6, android.os.Parcel p7, int p8)
    {
        com.amazon.venezia.command.k v0_0;
        switch (p5) {
            case 1:
                com.amazon.venezia.command.k v0_16;
                p6.enforceInterface("com.amazon.venezia.command.ResultCallback");
                android.os.IBinder v1_4 = p6.readStrongBinder();
                if (v1_4 != null) {
                    com.amazon.venezia.command.k v0_14 = v1_4.queryLocalInterface("com.amazon.venezia.command.SuccessResult");
                    if ((v0_14 == null) || (!(v0_14 instanceof com.amazon.venezia.command.SuccessResult))) {
                        v0_16 = new com.amazon.venezia.command.x(v1_4);
                    } else {
                        v0_16 = ((com.amazon.venezia.command.SuccessResult) v0_14);
                    }
                } else {
                    v0_16 = 0;
                }
                this.a(v0_16);
                p7.writeNoException();
                v0_0 = 1;
                break;
            case 2:
                com.amazon.venezia.command.k v0_12;
                p6.enforceInterface("com.amazon.venezia.command.ResultCallback");
                android.os.IBinder v1_3 = p6.readStrongBinder();
                if (v1_3 != null) {
                    com.amazon.venezia.command.k v0_10 = v1_3.queryLocalInterface("com.amazon.venezia.command.FailureResult");
                    if ((v0_10 == null) || (!(v0_10 instanceof com.amazon.venezia.command.FailureResult))) {
                        v0_12 = new com.amazon.venezia.command.ab(v1_3);
                    } else {
                        v0_12 = ((com.amazon.venezia.command.FailureResult) v0_10);
                    }
                } else {
                    v0_12 = 0;
                }
                this.a(v0_12);
                p7.writeNoException();
                v0_0 = 1;
                break;
            case 3:
                com.amazon.venezia.command.k v0_8;
                p6.enforceInterface("com.amazon.venezia.command.ResultCallback");
                android.os.IBinder v1_2 = p6.readStrongBinder();
                if (v1_2 != null) {
                    com.amazon.venezia.command.k v0_6 = v1_2.queryLocalInterface("com.amazon.venezia.command.DecisionResult");
                    if ((v0_6 == null) || (!(v0_6 instanceof com.amazon.venezia.command.r))) {
                        v0_8 = new com.amazon.venezia.command.aa(v1_2);
                    } else {
                        v0_8 = ((com.amazon.venezia.command.r) v0_6);
                    }
                } else {
                    v0_8 = 0;
                }
                this.a(v0_8);
                p7.writeNoException();
                v0_0 = 1;
                break;
            case 4:
                com.amazon.venezia.command.k v0_4;
                p6.enforceInterface("com.amazon.venezia.command.ResultCallback");
                android.os.IBinder v1_1 = p6.readStrongBinder();
                if (v1_1 != null) {
                    com.amazon.venezia.command.k v0_2 = v1_1.queryLocalInterface("com.amazon.venezia.command.ExceptionResult");
                    if ((v0_2 == null) || (!(v0_2 instanceof com.amazon.venezia.command.k))) {
                        v0_4 = new com.amazon.venezia.command.v(v1_1);
                    } else {
                        v0_4 = ((com.amazon.venezia.command.k) v0_2);
                    }
                } else {
                    v0_4 = 0;
                }
                this.a(v0_4);
                p7.writeNoException();
                v0_0 = 1;
                break;
            case 1598968902:
                p7.writeString("com.amazon.venezia.command.ResultCallback");
                v0_0 = 1;
                break;
            default:
                v0_0 = super.onTransact(p5, p6, p7, p8);
        }
        return v0_0;
    }
}
