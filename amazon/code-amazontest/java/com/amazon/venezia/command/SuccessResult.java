package com.amazon.venezia.command;
public interface SuccessResult implements android.os.IInterface {

    public abstract String getAuthToken();

    public abstract java.util.Map getData();
}
