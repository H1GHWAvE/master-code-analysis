package com.amazon.venezia.command;
 class CommandService$Stub$Proxy implements com.amazon.venezia.command.CommandService {
    private android.os.IBinder mRemote;

    CommandService$Stub$Proxy(android.os.IBinder p1)
    {
        this.mRemote = p1;
        return;
    }

    public android.os.IBinder asBinder()
    {
        return this.mRemote;
    }

    public void execute(com.amazon.venezia.command.Command p6, com.amazon.venezia.command.ResultCallback p7)
    {
        android.os.Parcel v0 = android.os.Parcel.obtain();
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            android.os.IBinder v2_1;
            v0.writeInterfaceToken("com.amazon.venezia.command.CommandService");
        } catch (android.os.IBinder v2_3) {
            v1.recycle();
            v0.recycle();
            throw v2_3;
        }
        if (p6 == null) {
            v2_1 = 0;
        } else {
            v2_1 = p6.asBinder();
        }
        android.os.IBinder v2_2;
        v0.writeStrongBinder(v2_1);
        if (p7 == null) {
            v2_2 = 0;
        } else {
            v2_2 = p7.asBinder();
        }
        v0.writeStrongBinder(v2_2);
        this.mRemote.transact(1, v0, v1, 0);
        v1.readException();
        v1.recycle();
        v0.recycle();
        return;
    }

    public String getInterfaceDescriptor()
    {
        return "com.amazon.venezia.command.CommandService";
    }
}
