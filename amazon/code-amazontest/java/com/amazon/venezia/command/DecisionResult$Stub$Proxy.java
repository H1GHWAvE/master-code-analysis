package com.amazon.venezia.command;
 class DecisionResult$Stub$Proxy implements com.amazon.venezia.command.DecisionResult {
    private android.os.IBinder mRemote;

    DecisionResult$Stub$Proxy(android.os.IBinder p1)
    {
        this.mRemote = p1;
        return;
    }

    public android.os.IBinder asBinder()
    {
        return this.mRemote;
    }

    public void expire(com.amazon.venezia.command.DecisionExpirationContext p6)
    {
        android.os.Parcel v0 = android.os.Parcel.obtain();
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            android.os.IBinder v2_1;
            v0.writeInterfaceToken("com.amazon.venezia.command.DecisionResult");
        } catch (android.os.IBinder v2_2) {
            v1.recycle();
            v0.recycle();
            throw v2_2;
        }
        if (p6 == null) {
            v2_1 = 0;
        } else {
            v2_1 = p6.asBinder();
        }
        v0.writeStrongBinder(v2_1);
        this.mRemote.transact(5, v0, v1, 0);
        v1.readException();
        v1.recycle();
        v0.recycle();
        return;
    }

    public String getAuthToken()
    {
        android.os.Parcel v0 = android.os.Parcel.obtain();
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            v0.writeInterfaceToken("com.amazon.venezia.command.DecisionResult");
            this.mRemote.transact(1, v0, v1, 0);
            v1.readException();
            String v2 = v1.readString();
            v1.recycle();
            v0.recycle();
            return v2;
        } catch (Throwable v3_2) {
            v1.recycle();
            v0.recycle();
            throw v3_2;
        }
    }

    public long getDecisionDurationInSeconds()
    {
        android.os.Parcel v0 = android.os.Parcel.obtain();
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            v0.writeInterfaceToken("com.amazon.venezia.command.DecisionResult");
            this.mRemote.transact(4, v0, v1, 0);
            v1.readException();
            long v2 = v1.readLong();
            v1.recycle();
            v0.recycle();
            return v2;
        } catch (Throwable v4_2) {
            v1.recycle();
            v0.recycle();
            throw v4_2;
        }
    }

    public String getDescription()
    {
        android.os.Parcel v0 = android.os.Parcel.obtain();
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            v0.writeInterfaceToken("com.amazon.venezia.command.DecisionResult");
            this.mRemote.transact(3, v0, v1, 0);
            v1.readException();
            String v2 = v1.readString();
            v1.recycle();
            v0.recycle();
            return v2;
        } catch (Throwable v3_2) {
            v1.recycle();
            v0.recycle();
            throw v3_2;
        }
    }

    public String getDisplayableName()
    {
        android.os.Parcel v0 = android.os.Parcel.obtain();
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            v0.writeInterfaceToken("com.amazon.venezia.command.DecisionResult");
            this.mRemote.transact(2, v0, v1, 0);
            v1.readException();
            String v2 = v1.readString();
            v1.recycle();
            v0.recycle();
            return v2;
        } catch (Throwable v3_2) {
            v1.recycle();
            v0.recycle();
            throw v3_2;
        }
    }

    public java.util.Map getExtensionData()
    {
        android.os.Parcel v0 = android.os.Parcel.obtain();
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            v0.writeInterfaceToken("com.amazon.venezia.command.DecisionResult");
            this.mRemote.transact(9, v0, v1, 0);
            v1.readException();
            java.util.HashMap v2 = v1.readHashMap(this.getClass().getClassLoader());
            v1.recycle();
            v0.recycle();
            return v2;
        } catch (Throwable v4_3) {
            v1.recycle();
            v0.recycle();
            throw v4_3;
        }
    }

    public String getInterfaceDescriptor()
    {
        return "com.amazon.venezia.command.DecisionResult";
    }

    public com.amazon.venezia.command.Choice getNegativeChoice()
    {
        android.os.Parcel v0 = android.os.Parcel.obtain();
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            v0.writeInterfaceToken("com.amazon.venezia.command.DecisionResult");
            this.mRemote.transact(8, v0, v1, 0);
            v1.readException();
            com.amazon.venezia.command.Choice v2 = com.amazon.venezia.command.Choice$Stub.asInterface(v1.readStrongBinder());
            v1.recycle();
            v0.recycle();
            return v2;
        } catch (Throwable v3_3) {
            v1.recycle();
            v0.recycle();
            throw v3_3;
        }
    }

    public com.amazon.venezia.command.Choice getNeutralChoice()
    {
        android.os.Parcel v0 = android.os.Parcel.obtain();
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            v0.writeInterfaceToken("com.amazon.venezia.command.DecisionResult");
            this.mRemote.transact(7, v0, v1, 0);
            v1.readException();
            com.amazon.venezia.command.Choice v2 = com.amazon.venezia.command.Choice$Stub.asInterface(v1.readStrongBinder());
            v1.recycle();
            v0.recycle();
            return v2;
        } catch (Throwable v3_3) {
            v1.recycle();
            v0.recycle();
            throw v3_3;
        }
    }

    public com.amazon.venezia.command.Choice getPositiveChoice()
    {
        android.os.Parcel v0 = android.os.Parcel.obtain();
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            v0.writeInterfaceToken("com.amazon.venezia.command.DecisionResult");
            this.mRemote.transact(6, v0, v1, 0);
            v1.readException();
            com.amazon.venezia.command.Choice v2 = com.amazon.venezia.command.Choice$Stub.asInterface(v1.readStrongBinder());
            v1.recycle();
            v0.recycle();
            return v2;
        } catch (Throwable v3_3) {
            v1.recycle();
            v0.recycle();
            throw v3_3;
        }
    }
}
