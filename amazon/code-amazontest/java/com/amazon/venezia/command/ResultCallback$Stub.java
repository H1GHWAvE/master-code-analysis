package com.amazon.venezia.command;
public abstract class ResultCallback$Stub extends android.os.Binder implements com.amazon.venezia.command.ResultCallback {
    private static final String DESCRIPTOR = "com.amazon.venezia.command.ResultCallback";
    static final int TRANSACTION_onDecide = 3;
    static final int TRANSACTION_onException = 4;
    static final int TRANSACTION_onFailure = 2;
    static final int TRANSACTION_onSuccess = 1;

    public ResultCallback$Stub()
    {
        this.attachInterface(this, "com.amazon.venezia.command.ResultCallback");
        return;
    }

    public static com.amazon.venezia.command.ResultCallback asInterface(android.os.IBinder p2)
    {
        com.amazon.venezia.command.ResultCallback v1_3;
        if (p2 != null) {
            com.amazon.venezia.command.ResultCallback v0_0 = p2.queryLocalInterface("com.amazon.venezia.command.ResultCallback");
            if ((v0_0 == null) || (!(v0_0 instanceof com.amazon.venezia.command.ResultCallback))) {
                v1_3 = new com.amazon.venezia.command.ResultCallback$Stub$Proxy(p2);
            } else {
                v1_3 = ((com.amazon.venezia.command.ResultCallback) v0_0);
            }
        } else {
            v1_3 = 0;
        }
        return v1_3;
    }

    public android.os.IBinder asBinder()
    {
        return this;
    }

    public boolean onTransact(int p5, android.os.Parcel p6, android.os.Parcel p7, int p8)
    {
        int v1_0;
        switch (p5) {
            case 1:
                p6.enforceInterface("com.amazon.venezia.command.ResultCallback");
                this.onSuccess(com.amazon.venezia.command.SuccessResult$Stub.asInterface(p6.readStrongBinder()));
                p7.writeNoException();
                v1_0 = 1;
                break;
            case 2:
                p6.enforceInterface("com.amazon.venezia.command.ResultCallback");
                this.onFailure(com.amazon.venezia.command.FailureResult$Stub.asInterface(p6.readStrongBinder()));
                p7.writeNoException();
                v1_0 = 1;
                break;
            case 3:
                p6.enforceInterface("com.amazon.venezia.command.ResultCallback");
                this.onDecide(com.amazon.venezia.command.DecisionResult$Stub.asInterface(p6.readStrongBinder()));
                p7.writeNoException();
                v1_0 = 1;
                break;
            case 4:
                p6.enforceInterface("com.amazon.venezia.command.ResultCallback");
                this.onException(com.amazon.venezia.command.ExceptionResult$Stub.asInterface(p6.readStrongBinder()));
                p7.writeNoException();
                v1_0 = 1;
                break;
            case 1598968902:
                p7.writeString("com.amazon.venezia.command.ResultCallback");
                v1_0 = 1;
                break;
            default:
                v1_0 = super.onTransact(p5, p6, p7, p8);
        }
        return v1_0;
    }
}
