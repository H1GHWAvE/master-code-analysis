package com.amazon.venezia.command;
final class u implements com.amazon.venezia.command.s {
    private android.os.IBinder a;

    u(android.os.IBinder p1)
    {
        this.a = p1;
        return;
    }

    public final String a()
    {
        android.os.Parcel v0 = android.os.Parcel.obtain();
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            v0.writeInterfaceToken("com.amazon.venezia.command.DecisionExpirationContext");
            this.a.transact(1, v0, v1, 0);
            v1.readException();
            Throwable v2_2 = v1.readString();
            v1.recycle();
            v0.recycle();
            return v2_2;
        } catch (Throwable v2_3) {
            v1.recycle();
            v0.recycle();
            throw v2_3;
        }
    }

    public final android.os.IBinder asBinder()
    {
        return this.a;
    }

    public final java.util.Map b()
    {
        android.os.Parcel v0 = android.os.Parcel.obtain();
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            v0.writeInterfaceToken("com.amazon.venezia.command.DecisionExpirationContext");
            this.a.transact(2, v0, v1, 0);
            v1.readException();
            Throwable v2_4 = v1.readHashMap(this.getClass().getClassLoader());
            v1.recycle();
            v0.recycle();
            return v2_4;
        } catch (Throwable v2_5) {
            v1.recycle();
            v0.recycle();
            throw v2_5;
        }
    }
}
