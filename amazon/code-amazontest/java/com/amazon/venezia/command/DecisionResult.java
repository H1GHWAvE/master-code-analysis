package com.amazon.venezia.command;
public interface DecisionResult implements android.os.IInterface {

    public abstract void expire();

    public abstract String getAuthToken();

    public abstract long getDecisionDurationInSeconds();

    public abstract String getDescription();

    public abstract String getDisplayableName();

    public abstract java.util.Map getExtensionData();

    public abstract com.amazon.venezia.command.Choice getNegativeChoice();

    public abstract com.amazon.venezia.command.Choice getNeutralChoice();

    public abstract com.amazon.venezia.command.Choice getPositiveChoice();
}
