package com.amazon.venezia.command;
final class aa implements com.amazon.venezia.command.r {
    private android.os.IBinder a;

    aa(android.os.IBinder p1)
    {
        this.a = p1;
        return;
    }

    public final String a()
    {
        android.os.Parcel v0 = android.os.Parcel.obtain();
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            v0.writeInterfaceToken("com.amazon.venezia.command.DecisionResult");
            this.a.transact(1, v0, v1, 0);
            v1.readException();
            Throwable v2_2 = v1.readString();
            v1.recycle();
            v0.recycle();
            return v2_2;
        } catch (Throwable v2_3) {
            v1.recycle();
            v0.recycle();
            throw v2_3;
        }
    }

    public final void a(com.amazon.venezia.command.s p6)
    {
        android.os.Parcel v0 = android.os.Parcel.obtain();
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            android.os.IBinder v2_1;
            v0.writeInterfaceToken("com.amazon.venezia.command.DecisionResult");
        } catch (android.os.IBinder v2_2) {
            v1.recycle();
            v0.recycle();
            throw v2_2;
        }
        if (p6 == null) {
            v2_1 = 0;
        } else {
            v2_1 = p6.asBinder();
        }
        v0.writeStrongBinder(v2_1);
        this.a.transact(5, v0, v1, 0);
        v1.readException();
        v1.recycle();
        v0.recycle();
        return;
    }

    public final android.os.IBinder asBinder()
    {
        return this.a;
    }

    public final String b()
    {
        android.os.Parcel v0 = android.os.Parcel.obtain();
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            v0.writeInterfaceToken("com.amazon.venezia.command.DecisionResult");
            this.a.transact(2, v0, v1, 0);
            v1.readException();
            Throwable v2_2 = v1.readString();
            v1.recycle();
            v0.recycle();
            return v2_2;
        } catch (Throwable v2_3) {
            v1.recycle();
            v0.recycle();
            throw v2_3;
        }
    }

    public final String c()
    {
        android.os.Parcel v0 = android.os.Parcel.obtain();
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            v0.writeInterfaceToken("com.amazon.venezia.command.DecisionResult");
            this.a.transact(3, v0, v1, 0);
            v1.readException();
            Throwable v2_2 = v1.readString();
            v1.recycle();
            v0.recycle();
            return v2_2;
        } catch (Throwable v2_3) {
            v1.recycle();
            v0.recycle();
            throw v2_3;
        }
    }

    public final long d()
    {
        android.os.Parcel v0 = android.os.Parcel.obtain();
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            v0.writeInterfaceToken("com.amazon.venezia.command.DecisionResult");
            this.a.transact(4, v0, v1, 0);
            v1.readException();
            Throwable v2_2 = v1.readLong();
            v1.recycle();
            v0.recycle();
            return v2_2;
        } catch (Throwable v2_3) {
            v1.recycle();
            v0.recycle();
            throw v2_3;
        }
    }

    public final com.amazon.venezia.command.n e()
    {
        android.os.Parcel v0 = android.os.Parcel.obtain();
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            v0.writeInterfaceToken("com.amazon.venezia.command.DecisionResult");
            this.a.transact(6, v0, v1, 0);
            v1.readException();
            Throwable v2_3 = com.amazon.venezia.command.m.a(v1.readStrongBinder());
            v1.recycle();
            v0.recycle();
            return v2_3;
        } catch (Throwable v2_4) {
            v1.recycle();
            v0.recycle();
            throw v2_4;
        }
    }

    public final com.amazon.venezia.command.n f()
    {
        android.os.Parcel v0 = android.os.Parcel.obtain();
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            v0.writeInterfaceToken("com.amazon.venezia.command.DecisionResult");
            this.a.transact(7, v0, v1, 0);
            v1.readException();
            Throwable v2_3 = com.amazon.venezia.command.m.a(v1.readStrongBinder());
            v1.recycle();
            v0.recycle();
            return v2_3;
        } catch (Throwable v2_4) {
            v1.recycle();
            v0.recycle();
            throw v2_4;
        }
    }

    public final com.amazon.venezia.command.n g()
    {
        android.os.Parcel v0 = android.os.Parcel.obtain();
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            v0.writeInterfaceToken("com.amazon.venezia.command.DecisionResult");
            this.a.transact(8, v0, v1, 0);
            v1.readException();
            Throwable v2_3 = com.amazon.venezia.command.m.a(v1.readStrongBinder());
            v1.recycle();
            v0.recycle();
            return v2_3;
        } catch (Throwable v2_4) {
            v1.recycle();
            v0.recycle();
            throw v2_4;
        }
    }

    public final java.util.Map h()
    {
        android.os.Parcel v0 = android.os.Parcel.obtain();
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            v0.writeInterfaceToken("com.amazon.venezia.command.DecisionResult");
            this.a.transact(9, v0, v1, 0);
            v1.readException();
            Throwable v2_4 = v1.readHashMap(this.getClass().getClassLoader());
            v1.recycle();
            v0.recycle();
            return v2_4;
        } catch (Throwable v2_5) {
            v1.recycle();
            v0.recycle();
            throw v2_5;
        }
    }
}
