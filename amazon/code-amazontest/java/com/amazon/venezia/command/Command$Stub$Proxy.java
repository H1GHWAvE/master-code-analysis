package com.amazon.venezia.command;
 class Command$Stub$Proxy implements com.amazon.venezia.command.Command {
    private android.os.IBinder mRemote;

    Command$Stub$Proxy(android.os.IBinder p1)
    {
        this.mRemote = p1;
        return;
    }

    public android.os.IBinder asBinder()
    {
        return this.mRemote;
    }

    public java.util.Map getData()
    {
        android.os.Parcel v0 = android.os.Parcel.obtain();
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            v0.writeInterfaceToken("com.amazon.venezia.command.Command");
            this.mRemote.transact(4, v0, v1, 0);
            v1.readException();
            java.util.HashMap v2 = v1.readHashMap(this.getClass().getClassLoader());
            v1.recycle();
            v0.recycle();
            return v2;
        } catch (Throwable v4_3) {
            v1.recycle();
            v0.recycle();
            throw v4_3;
        }
    }

    public String getInterfaceDescriptor()
    {
        return "com.amazon.venezia.command.Command";
    }

    public String getName()
    {
        android.os.Parcel v0 = android.os.Parcel.obtain();
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            v0.writeInterfaceToken("com.amazon.venezia.command.Command");
            this.mRemote.transact(1, v0, v1, 0);
            v1.readException();
            String v2 = v1.readString();
            v1.recycle();
            v0.recycle();
            return v2;
        } catch (Throwable v3_2) {
            v1.recycle();
            v0.recycle();
            throw v3_2;
        }
    }

    public String getPackageName()
    {
        android.os.Parcel v0 = android.os.Parcel.obtain();
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            v0.writeInterfaceToken("com.amazon.venezia.command.Command");
            this.mRemote.transact(3, v0, v1, 0);
            v1.readException();
            String v2 = v1.readString();
            v1.recycle();
            v0.recycle();
            return v2;
        } catch (Throwable v3_2) {
            v1.recycle();
            v0.recycle();
            throw v3_2;
        }
    }

    public String getVersion()
    {
        android.os.Parcel v0 = android.os.Parcel.obtain();
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            v0.writeInterfaceToken("com.amazon.venezia.command.Command");
            this.mRemote.transact(2, v0, v1, 0);
            v1.readException();
            String v2 = v1.readString();
            v1.recycle();
            v0.recycle();
            return v2;
        } catch (Throwable v3_2) {
            v1.recycle();
            v0.recycle();
            throw v3_2;
        }
    }
}
