package com.amazon.venezia.command;
public interface Choice implements android.os.IInterface {

    public abstract void choose();

    public abstract String getDisplayableName();

    public abstract java.util.Map getExtensionData();

    public abstract android.content.Intent getIntent();
}
