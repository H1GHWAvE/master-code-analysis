package com.amazon.venezia.command;
public abstract class FailureResult$Stub extends android.os.Binder implements com.amazon.venezia.command.FailureResult {
    private static final String DESCRIPTOR = "com.amazon.venezia.command.FailureResult";
    static final int TRANSACTION_getAuthToken = 1;
    static final int TRANSACTION_getButtonLabel = 4;
    static final int TRANSACTION_getDisplayableMessage = 3;
    static final int TRANSACTION_getDisplayableName = 2;
    static final int TRANSACTION_getExtensionData = 6;
    static final int TRANSACTION_show = 5;

    public FailureResult$Stub()
    {
        this.attachInterface(this, "com.amazon.venezia.command.FailureResult");
        return;
    }

    public static com.amazon.venezia.command.FailureResult asInterface(android.os.IBinder p2)
    {
        com.amazon.venezia.command.FailureResult v1_3;
        if (p2 != null) {
            com.amazon.venezia.command.FailureResult v0_0 = p2.queryLocalInterface("com.amazon.venezia.command.FailureResult");
            if ((v0_0 == null) || (!(v0_0 instanceof com.amazon.venezia.command.FailureResult))) {
                v1_3 = new com.amazon.venezia.command.FailureResult$Stub$Proxy(p2);
            } else {
                v1_3 = ((com.amazon.venezia.command.FailureResult) v0_0);
            }
        } else {
            v1_3 = 0;
        }
        return v1_3;
    }

    public android.os.IBinder asBinder()
    {
        return this;
    }

    public boolean onTransact(int p5, android.os.Parcel p6, android.os.Parcel p7, int p8)
    {
        int v1_0;
        switch (p5) {
            case 1:
                p6.enforceInterface("com.amazon.venezia.command.FailureResult");
                java.util.Map v0_5 = this.getAuthToken();
                p7.writeNoException();
                p7.writeString(v0_5);
                v1_0 = 1;
                break;
            case 2:
                p6.enforceInterface("com.amazon.venezia.command.FailureResult");
                java.util.Map v0_4 = this.getDisplayableName();
                p7.writeNoException();
                p7.writeString(v0_4);
                v1_0 = 1;
                break;
            case 3:
                p6.enforceInterface("com.amazon.venezia.command.FailureResult");
                java.util.Map v0_3 = this.getDisplayableMessage();
                p7.writeNoException();
                p7.writeString(v0_3);
                v1_0 = 1;
                break;
            case 4:
                p6.enforceInterface("com.amazon.venezia.command.FailureResult");
                java.util.Map v0_2 = this.getButtonLabel();
                p7.writeNoException();
                p7.writeString(v0_2);
                v1_0 = 1;
                break;
            case 5:
                int v1_1;
                p6.enforceInterface("com.amazon.venezia.command.FailureResult");
                java.util.Map v0_1 = this.show();
                p7.writeNoException();
                if (v0_1 == null) {
                    v1_1 = 0;
                } else {
                    v1_1 = 1;
                }
                p7.writeInt(v1_1);
                v1_0 = 1;
                break;
            case 6:
                p6.enforceInterface("com.amazon.venezia.command.FailureResult");
                java.util.Map v0_0 = this.getExtensionData();
                p7.writeNoException();
                p7.writeMap(v0_0);
                v1_0 = 1;
                break;
            case 1598968902:
                p7.writeString("com.amazon.venezia.command.FailureResult");
                v1_0 = 1;
                break;
            default:
                v1_0 = super.onTransact(p5, p6, p7, p8);
        }
        return v1_0;
    }
}
