package com.amazon.venezia.command;
public abstract class b extends android.os.Binder implements com.amazon.venezia.command.FailureResult {

    public b()
    {
        this.attachInterface(this, "com.amazon.venezia.command.FailureResult");
        return;
    }

    public boolean onTransact(int p4, android.os.Parcel p5, android.os.Parcel p6, int p7)
    {
        int v0_0;
        switch (p4) {
            case 1:
                p5.enforceInterface("com.amazon.venezia.command.FailureResult");
                int v0_7 = this.getAuthToken();
                p6.writeNoException();
                p6.writeString(v0_7);
                v0_0 = 1;
                break;
            case 2:
                p5.enforceInterface("com.amazon.venezia.command.FailureResult");
                int v0_6 = this.getDisplayableName();
                p6.writeNoException();
                p6.writeString(v0_6);
                v0_0 = 1;
                break;
            case 3:
                p5.enforceInterface("com.amazon.venezia.command.FailureResult");
                int v0_5 = this.getDisplayableMessage();
                p6.writeNoException();
                p6.writeString(v0_5);
                v0_0 = 1;
                break;
            case 4:
                p5.enforceInterface("com.amazon.venezia.command.FailureResult");
                int v0_4 = this.getButtonLabel();
                p6.writeNoException();
                p6.writeString(v0_4);
                v0_0 = 1;
                break;
            case 5:
                int v0_3;
                p5.enforceInterface("com.amazon.venezia.command.FailureResult");
                int v0_2 = this.show();
                p6.writeNoException();
                if (v0_2 == 0) {
                    v0_3 = 0;
                } else {
                    v0_3 = 1;
                }
                p6.writeInt(v0_3);
                v0_0 = 1;
                break;
            case 6:
                p5.enforceInterface("com.amazon.venezia.command.FailureResult");
                int v0_1 = this.getExtensionData();
                p6.writeNoException();
                p6.writeMap(v0_1);
                v0_0 = 1;
                break;
            case 1598968902:
                p6.writeString("com.amazon.venezia.command.FailureResult");
                v0_0 = 1;
                break;
            default:
                v0_0 = super.onTransact(p4, p5, p6, p7);
        }
        return v0_0;
    }
}
