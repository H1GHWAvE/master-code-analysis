package com.amazon.venezia.command;
public abstract class CommandService$Stub extends android.os.Binder implements com.amazon.venezia.command.CommandService {
    private static final String DESCRIPTOR = "com.amazon.venezia.command.CommandService";
    static final int TRANSACTION_execute = 1;

    public CommandService$Stub()
    {
        this.attachInterface(this, "com.amazon.venezia.command.CommandService");
        return;
    }

    public static com.amazon.venezia.command.CommandService asInterface(android.os.IBinder p2)
    {
        com.amazon.venezia.command.CommandService v1_3;
        if (p2 != null) {
            com.amazon.venezia.command.CommandService v0_0 = p2.queryLocalInterface("com.amazon.venezia.command.CommandService");
            if ((v0_0 == null) || (!(v0_0 instanceof com.amazon.venezia.command.CommandService))) {
                v1_3 = new com.amazon.venezia.command.CommandService$Stub$Proxy(p2);
            } else {
                v1_3 = ((com.amazon.venezia.command.CommandService) v0_0);
            }
        } else {
            v1_3 = 0;
        }
        return v1_3;
    }

    public android.os.IBinder asBinder()
    {
        return this;
    }

    public boolean onTransact(int p6, android.os.Parcel p7, android.os.Parcel p8, int p9)
    {
        int v2_0;
        switch (p6) {
            case 1:
                p7.enforceInterface("com.amazon.venezia.command.CommandService");
                this.execute(com.amazon.venezia.command.Command$Stub.asInterface(p7.readStrongBinder()), com.amazon.venezia.command.ResultCallback$Stub.asInterface(p7.readStrongBinder()));
                p8.writeNoException();
                v2_0 = 1;
                break;
            case 1598968902:
                p8.writeString("com.amazon.venezia.command.CommandService");
                v2_0 = 1;
                break;
            default:
                v2_0 = super.onTransact(p6, p7, p8, p9);
        }
        return v2_0;
    }
}
