package com.amazon.venezia.command;
public interface Command implements android.os.IInterface {

    public abstract java.util.Map getData();

    public abstract String getName();

    public abstract String getPackageName();

    public abstract String getVersion();
}
