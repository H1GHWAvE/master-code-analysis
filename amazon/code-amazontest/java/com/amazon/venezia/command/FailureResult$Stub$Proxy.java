package com.amazon.venezia.command;
 class FailureResult$Stub$Proxy implements com.amazon.venezia.command.FailureResult {
    private android.os.IBinder mRemote;

    FailureResult$Stub$Proxy(android.os.IBinder p1)
    {
        this.mRemote = p1;
        return;
    }

    public android.os.IBinder asBinder()
    {
        return this.mRemote;
    }

    public String getAuthToken()
    {
        android.os.Parcel v0 = android.os.Parcel.obtain();
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            v0.writeInterfaceToken("com.amazon.venezia.command.FailureResult");
            this.mRemote.transact(1, v0, v1, 0);
            v1.readException();
            String v2 = v1.readString();
            v1.recycle();
            v0.recycle();
            return v2;
        } catch (Throwable v3_2) {
            v1.recycle();
            v0.recycle();
            throw v3_2;
        }
    }

    public String getButtonLabel()
    {
        android.os.Parcel v0 = android.os.Parcel.obtain();
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            v0.writeInterfaceToken("com.amazon.venezia.command.FailureResult");
            this.mRemote.transact(4, v0, v1, 0);
            v1.readException();
            String v2 = v1.readString();
            v1.recycle();
            v0.recycle();
            return v2;
        } catch (Throwable v3_2) {
            v1.recycle();
            v0.recycle();
            throw v3_2;
        }
    }

    public String getDisplayableMessage()
    {
        android.os.Parcel v0 = android.os.Parcel.obtain();
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            v0.writeInterfaceToken("com.amazon.venezia.command.FailureResult");
            this.mRemote.transact(3, v0, v1, 0);
            v1.readException();
            String v2 = v1.readString();
            v1.recycle();
            v0.recycle();
            return v2;
        } catch (Throwable v3_2) {
            v1.recycle();
            v0.recycle();
            throw v3_2;
        }
    }

    public String getDisplayableName()
    {
        android.os.Parcel v0 = android.os.Parcel.obtain();
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            v0.writeInterfaceToken("com.amazon.venezia.command.FailureResult");
            this.mRemote.transact(2, v0, v1, 0);
            v1.readException();
            String v2 = v1.readString();
            v1.recycle();
            v0.recycle();
            return v2;
        } catch (Throwable v3_2) {
            v1.recycle();
            v0.recycle();
            throw v3_2;
        }
    }

    public java.util.Map getExtensionData()
    {
        android.os.Parcel v0 = android.os.Parcel.obtain();
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            v0.writeInterfaceToken("com.amazon.venezia.command.FailureResult");
            this.mRemote.transact(6, v0, v1, 0);
            v1.readException();
            java.util.HashMap v2 = v1.readHashMap(this.getClass().getClassLoader());
            v1.recycle();
            v0.recycle();
            return v2;
        } catch (Throwable v4_3) {
            v1.recycle();
            v0.recycle();
            throw v4_3;
        }
    }

    public String getInterfaceDescriptor()
    {
        return "com.amazon.venezia.command.FailureResult";
    }

    public boolean show()
    {
        android.os.Parcel v0 = android.os.Parcel.obtain();
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            int v2;
            v0.writeInterfaceToken("com.amazon.venezia.command.FailureResult");
            this.mRemote.transact(5, v0, v1, 0);
            v1.readException();
        } catch (int v3_3) {
            v1.recycle();
            v0.recycle();
            throw v3_3;
        }
        if (v1.readInt() == 0) {
            v2 = 0;
        } else {
            v2 = 1;
        }
        v1.recycle();
        v0.recycle();
        return v2;
    }
}
