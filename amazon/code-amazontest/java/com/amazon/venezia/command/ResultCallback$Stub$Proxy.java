package com.amazon.venezia.command;
 class ResultCallback$Stub$Proxy implements com.amazon.venezia.command.ResultCallback {
    private android.os.IBinder mRemote;

    ResultCallback$Stub$Proxy(android.os.IBinder p1)
    {
        this.mRemote = p1;
        return;
    }

    public android.os.IBinder asBinder()
    {
        return this.mRemote;
    }

    public String getInterfaceDescriptor()
    {
        return "com.amazon.venezia.command.ResultCallback";
    }

    public void onDecide(com.amazon.venezia.command.DecisionResult p6)
    {
        android.os.Parcel v0 = android.os.Parcel.obtain();
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            android.os.IBinder v2_1;
            v0.writeInterfaceToken("com.amazon.venezia.command.ResultCallback");
        } catch (android.os.IBinder v2_2) {
            v1.recycle();
            v0.recycle();
            throw v2_2;
        }
        if (p6 == null) {
            v2_1 = 0;
        } else {
            v2_1 = p6.asBinder();
        }
        v0.writeStrongBinder(v2_1);
        this.mRemote.transact(3, v0, v1, 0);
        v1.readException();
        v1.recycle();
        v0.recycle();
        return;
    }

    public void onException(com.amazon.venezia.command.ExceptionResult p6)
    {
        android.os.Parcel v0 = android.os.Parcel.obtain();
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            android.os.IBinder v2_1;
            v0.writeInterfaceToken("com.amazon.venezia.command.ResultCallback");
        } catch (android.os.IBinder v2_2) {
            v1.recycle();
            v0.recycle();
            throw v2_2;
        }
        if (p6 == null) {
            v2_1 = 0;
        } else {
            v2_1 = p6.asBinder();
        }
        v0.writeStrongBinder(v2_1);
        this.mRemote.transact(4, v0, v1, 0);
        v1.readException();
        v1.recycle();
        v0.recycle();
        return;
    }

    public void onFailure(com.amazon.venezia.command.FailureResult p6)
    {
        android.os.Parcel v0 = android.os.Parcel.obtain();
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            android.os.IBinder v2_1;
            v0.writeInterfaceToken("com.amazon.venezia.command.ResultCallback");
        } catch (android.os.IBinder v2_2) {
            v1.recycle();
            v0.recycle();
            throw v2_2;
        }
        if (p6 == null) {
            v2_1 = 0;
        } else {
            v2_1 = p6.asBinder();
        }
        v0.writeStrongBinder(v2_1);
        this.mRemote.transact(2, v0, v1, 0);
        v1.readException();
        v1.recycle();
        v0.recycle();
        return;
    }

    public void onSuccess(com.amazon.venezia.command.SuccessResult p6)
    {
        android.os.Parcel v0 = android.os.Parcel.obtain();
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            android.os.IBinder v2_1;
            v0.writeInterfaceToken("com.amazon.venezia.command.ResultCallback");
        } catch (android.os.IBinder v2_2) {
            v1.recycle();
            v0.recycle();
            throw v2_2;
        }
        if (p6 == null) {
            v2_1 = 0;
        } else {
            v2_1 = p6.asBinder();
        }
        v0.writeStrongBinder(v2_1);
        this.mRemote.transact(1, v0, v1, 0);
        v1.readException();
        v1.recycle();
        v0.recycle();
        return;
    }
}
