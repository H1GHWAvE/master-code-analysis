package com.amazon.venezia.command;
public abstract class m extends android.os.Binder implements com.amazon.venezia.command.n {

    public m()
    {
        this.attachInterface(this, "com.amazon.venezia.command.Choice");
        return;
    }

    public static com.amazon.venezia.command.n a(android.os.IBinder p3)
    {
        com.amazon.venezia.command.n v1_3;
        if (p3 != null) {
            com.amazon.venezia.command.n v1_1 = p3.queryLocalInterface("com.amazon.venezia.command.Choice");
            if ((v1_1 == null) || (!(v1_1 instanceof com.amazon.venezia.command.n))) {
                v1_3 = new com.amazon.venezia.command.a(p3);
            } else {
                v1_3 = ((com.amazon.venezia.command.n) v1_1);
            }
        } else {
            v1_3 = 0;
        }
        return v1_3;
    }

    public boolean onTransact(int p5, android.os.Parcel p6, android.os.Parcel p7, int p8)
    {
        com.amazon.venezia.command.y v0_0;
        switch (p5) {
            case 1:
                p6.enforceInterface("com.amazon.venezia.command.Choice");
                com.amazon.venezia.command.y v0_8 = this.a();
                p7.writeNoException();
                p7.writeString(v0_8);
                v0_0 = 1;
                break;
            case 2:
                p6.enforceInterface("com.amazon.venezia.command.Choice");
                com.amazon.venezia.command.y v0_6 = this.b();
                p7.writeNoException();
                if (v0_6 == null) {
                    p7.writeInt(0);
                } else {
                    p7.writeInt(1);
                    v0_6.writeToParcel(p7, 1);
                }
                v0_0 = 1;
                break;
            case 3:
                com.amazon.venezia.command.y v0_5;
                p6.enforceInterface("com.amazon.venezia.command.Choice");
                android.os.IBinder v1_1 = p6.readStrongBinder();
                if (v1_1 != null) {
                    com.amazon.venezia.command.y v0_3 = v1_1.queryLocalInterface("com.amazon.venezia.command.ChoiceContext");
                    if ((v0_3 == null) || (!(v0_3 instanceof com.amazon.venezia.command.y))) {
                        v0_5 = new com.amazon.venezia.command.j(v1_1);
                    } else {
                        v0_5 = ((com.amazon.venezia.command.y) v0_3);
                    }
                } else {
                    v0_5 = 0;
                }
                this.a(v0_5);
                p7.writeNoException();
                v0_0 = 1;
                break;
            case 4:
                p6.enforceInterface("com.amazon.venezia.command.Choice");
                com.amazon.venezia.command.y v0_1 = this.c();
                p7.writeNoException();
                p7.writeMap(v0_1);
                v0_0 = 1;
                break;
            case 1598968902:
                p7.writeString("com.amazon.venezia.command.Choice");
                v0_0 = 1;
                break;
            default:
                v0_0 = super.onTransact(p5, p6, p7, p8);
        }
        return v0_0;
    }
}
