package com.amazon.venezia.command;
public abstract class Choice$Stub extends android.os.Binder implements com.amazon.venezia.command.Choice {
    private static final String DESCRIPTOR = "com.amazon.venezia.command.Choice";
    static final int TRANSACTION_choose = 3;
    static final int TRANSACTION_getDisplayableName = 1;
    static final int TRANSACTION_getExtensionData = 4;
    static final int TRANSACTION_getIntent = 2;

    public Choice$Stub()
    {
        this.attachInterface(this, "com.amazon.venezia.command.Choice");
        return;
    }

    public static com.amazon.venezia.command.Choice asInterface(android.os.IBinder p2)
    {
        com.amazon.venezia.command.Choice v1_3;
        if (p2 != null) {
            com.amazon.venezia.command.Choice v0_0 = p2.queryLocalInterface("com.amazon.venezia.command.Choice");
            if ((v0_0 == null) || (!(v0_0 instanceof com.amazon.venezia.command.Choice))) {
                v1_3 = new com.amazon.venezia.command.Choice$Stub$Proxy(p2);
            } else {
                v1_3 = ((com.amazon.venezia.command.Choice) v0_0);
            }
        } else {
            v1_3 = 0;
        }
        return v1_3;
    }

    public android.os.IBinder asBinder()
    {
        return this;
    }

    public boolean onTransact(int p6, android.os.Parcel p7, android.os.Parcel p8, int p9)
    {
        int v2_0;
        switch (p6) {
            case 1:
                p7.enforceInterface("com.amazon.venezia.command.Choice");
                java.util.Map v1_2 = this.getDisplayableName();
                p8.writeNoException();
                p8.writeString(v1_2);
                v2_0 = 1;
                break;
            case 2:
                p7.enforceInterface("com.amazon.venezia.command.Choice");
                java.util.Map v1_1 = this.getIntent();
                p8.writeNoException();
                if (v1_1 == null) {
                    p8.writeInt(0);
                } else {
                    p8.writeInt(1);
                    v1_1.writeToParcel(p8, 1);
                }
                v2_0 = 1;
                break;
            case 3:
                p7.enforceInterface("com.amazon.venezia.command.Choice");
                this.choose(com.amazon.venezia.command.ChoiceContext$Stub.asInterface(p7.readStrongBinder()));
                p8.writeNoException();
                v2_0 = 1;
                break;
            case 4:
                p7.enforceInterface("com.amazon.venezia.command.Choice");
                java.util.Map v1_0 = this.getExtensionData();
                p8.writeNoException();
                p8.writeMap(v1_0);
                v2_0 = 1;
                break;
            case 1598968902:
                p8.writeString("com.amazon.venezia.command.Choice");
                v2_0 = 1;
                break;
            default:
                v2_0 = super.onTransact(p6, p7, p8, p9);
        }
        return v2_0;
    }
}
