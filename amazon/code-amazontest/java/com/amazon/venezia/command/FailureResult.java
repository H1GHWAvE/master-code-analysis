package com.amazon.venezia.command;
public interface FailureResult implements android.os.IInterface {

    public abstract String getAuthToken();

    public abstract String getButtonLabel();

    public abstract String getDisplayableMessage();

    public abstract String getDisplayableName();

    public abstract java.util.Map getExtensionData();

    public abstract boolean show();
}
