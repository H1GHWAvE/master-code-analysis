package com.amazon.venezia.command;
public abstract class c extends android.os.Binder implements com.amazon.venezia.command.r {

    public c()
    {
        this.attachInterface(this, "com.amazon.venezia.command.DecisionResult");
        return;
    }

    public boolean onTransact(int p5, android.os.Parcel p6, android.os.Parcel p7, int p8)
    {
        com.amazon.venezia.command.s v0_0;
        switch (p5) {
            case 1:
                p6.enforceInterface("com.amazon.venezia.command.DecisionResult");
                com.amazon.venezia.command.s v0_15 = this.a();
                p7.writeNoException();
                p7.writeString(v0_15);
                v0_0 = 1;
                break;
            case 2:
                p6.enforceInterface("com.amazon.venezia.command.DecisionResult");
                com.amazon.venezia.command.s v0_14 = this.b();
                p7.writeNoException();
                p7.writeString(v0_14);
                v0_0 = 1;
                break;
            case 3:
                p6.enforceInterface("com.amazon.venezia.command.DecisionResult");
                com.amazon.venezia.command.s v0_13 = this.c();
                p7.writeNoException();
                p7.writeString(v0_13);
                v0_0 = 1;
                break;
            case 4:
                p6.enforceInterface("com.amazon.venezia.command.DecisionResult");
                com.amazon.venezia.command.s v0_12 = this.d();
                p7.writeNoException();
                p7.writeLong(v0_12);
                v0_0 = 1;
                break;
            case 5:
                com.amazon.venezia.command.s v0_11;
                p6.enforceInterface("com.amazon.venezia.command.DecisionResult");
                android.os.IBinder v1_1 = p6.readStrongBinder();
                if (v1_1 != null) {
                    com.amazon.venezia.command.s v0_9 = v1_1.queryLocalInterface("com.amazon.venezia.command.DecisionExpirationContext");
                    if ((v0_9 == null) || (!(v0_9 instanceof com.amazon.venezia.command.s))) {
                        v0_11 = new com.amazon.venezia.command.u(v1_1);
                    } else {
                        v0_11 = ((com.amazon.venezia.command.s) v0_9);
                    }
                } else {
                    v0_11 = 0;
                }
                this.a(v0_11);
                p7.writeNoException();
                v0_0 = 1;
                break;
            case 6:
                com.amazon.venezia.command.s v0_7;
                p6.enforceInterface("com.amazon.venezia.command.DecisionResult");
                com.amazon.venezia.command.s v0_6 = this.e();
                p7.writeNoException();
                if (v0_6 == null) {
                    v0_7 = 0;
                } else {
                    v0_7 = v0_6.asBinder();
                }
                p7.writeStrongBinder(v0_7);
                v0_0 = 1;
                break;
            case 7:
                com.amazon.venezia.command.s v0_5;
                p6.enforceInterface("com.amazon.venezia.command.DecisionResult");
                com.amazon.venezia.command.s v0_4 = this.f();
                p7.writeNoException();
                if (v0_4 == null) {
                    v0_5 = 0;
                } else {
                    v0_5 = v0_4.asBinder();
                }
                p7.writeStrongBinder(v0_5);
                v0_0 = 1;
                break;
            case 8:
                com.amazon.venezia.command.s v0_3;
                p6.enforceInterface("com.amazon.venezia.command.DecisionResult");
                com.amazon.venezia.command.s v0_2 = this.g();
                p7.writeNoException();
                if (v0_2 == null) {
                    v0_3 = 0;
                } else {
                    v0_3 = v0_2.asBinder();
                }
                p7.writeStrongBinder(v0_3);
                v0_0 = 1;
                break;
            case 9:
                p6.enforceInterface("com.amazon.venezia.command.DecisionResult");
                com.amazon.venezia.command.s v0_1 = this.h();
                p7.writeNoException();
                p7.writeMap(v0_1);
                v0_0 = 1;
                break;
            case 1598968902:
                p7.writeString("com.amazon.venezia.command.DecisionResult");
                v0_0 = 1;
                break;
            default:
                v0_0 = super.onTransact(p5, p6, p7, p8);
        }
        return v0_0;
    }
}
