package com.amazon.venezia.command;
public abstract class DecisionResult$Stub extends android.os.Binder implements com.amazon.venezia.command.DecisionResult {
    private static final String DESCRIPTOR = "com.amazon.venezia.command.DecisionResult";
    static final int TRANSACTION_expire = 5;
    static final int TRANSACTION_getAuthToken = 1;
    static final int TRANSACTION_getDecisionDurationInSeconds = 4;
    static final int TRANSACTION_getDescription = 3;
    static final int TRANSACTION_getDisplayableName = 2;
    static final int TRANSACTION_getExtensionData = 9;
    static final int TRANSACTION_getNegativeChoice = 8;
    static final int TRANSACTION_getNeutralChoice = 7;
    static final int TRANSACTION_getPositiveChoice = 6;

    public DecisionResult$Stub()
    {
        this.attachInterface(this, "com.amazon.venezia.command.DecisionResult");
        return;
    }

    public static com.amazon.venezia.command.DecisionResult asInterface(android.os.IBinder p2)
    {
        com.amazon.venezia.command.DecisionResult v1_3;
        if (p2 != null) {
            com.amazon.venezia.command.DecisionResult v0_0 = p2.queryLocalInterface("com.amazon.venezia.command.DecisionResult");
            if ((v0_0 == null) || (!(v0_0 instanceof com.amazon.venezia.command.DecisionResult))) {
                v1_3 = new com.amazon.venezia.command.DecisionResult$Stub$Proxy(p2);
            } else {
                v1_3 = ((com.amazon.venezia.command.DecisionResult) v0_0);
            }
        } else {
            v1_3 = 0;
        }
        return v1_3;
    }

    public android.os.IBinder asBinder()
    {
        return this;
    }

    public boolean onTransact(int p8, android.os.Parcel p9, android.os.Parcel p10, int p11)
    {
        int v3_0;
        switch (p8) {
            case 1:
                p9.enforceInterface("com.amazon.venezia.command.DecisionResult");
                java.util.Map v1_7 = this.getAuthToken();
                p10.writeNoException();
                p10.writeString(v1_7);
                v3_0 = 1;
                break;
            case 2:
                p9.enforceInterface("com.amazon.venezia.command.DecisionResult");
                java.util.Map v1_6 = this.getDisplayableName();
                p10.writeNoException();
                p10.writeString(v1_6);
                v3_0 = 1;
                break;
            case 3:
                p9.enforceInterface("com.amazon.venezia.command.DecisionResult");
                java.util.Map v1_5 = this.getDescription();
                p10.writeNoException();
                p10.writeString(v1_5);
                v3_0 = 1;
                break;
            case 4:
                p9.enforceInterface("com.amazon.venezia.command.DecisionResult");
                java.util.Map v1_4 = this.getDecisionDurationInSeconds();
                p10.writeNoException();
                p10.writeLong(v1_4);
                v3_0 = 1;
                break;
            case 5:
                p9.enforceInterface("com.amazon.venezia.command.DecisionResult");
                this.expire(com.amazon.venezia.command.DecisionExpirationContext$Stub.asInterface(p9.readStrongBinder()));
                p10.writeNoException();
                v3_0 = 1;
                break;
            case 6:
                int v3_3;
                p9.enforceInterface("com.amazon.venezia.command.DecisionResult");
                java.util.Map v1_3 = this.getPositiveChoice();
                p10.writeNoException();
                if (v1_3 == null) {
                    v3_3 = 0;
                } else {
                    v3_3 = v1_3.asBinder();
                }
                p10.writeStrongBinder(v3_3);
                v3_0 = 1;
                break;
            case 7:
                int v3_2;
                p9.enforceInterface("com.amazon.venezia.command.DecisionResult");
                java.util.Map v1_2 = this.getNeutralChoice();
                p10.writeNoException();
                if (v1_2 == null) {
                    v3_2 = 0;
                } else {
                    v3_2 = v1_2.asBinder();
                }
                p10.writeStrongBinder(v3_2);
                v3_0 = 1;
                break;
            case 8:
                int v3_1;
                p9.enforceInterface("com.amazon.venezia.command.DecisionResult");
                java.util.Map v1_1 = this.getNegativeChoice();
                p10.writeNoException();
                if (v1_1 == null) {
                    v3_1 = 0;
                } else {
                    v3_1 = v1_1.asBinder();
                }
                p10.writeStrongBinder(v3_1);
                v3_0 = 1;
                break;
            case 9:
                p9.enforceInterface("com.amazon.venezia.command.DecisionResult");
                java.util.Map v1_0 = this.getExtensionData();
                p10.writeNoException();
                p10.writeMap(v1_0);
                v3_0 = 1;
                break;
            case 1598968902:
                p10.writeString("com.amazon.venezia.command.DecisionResult");
                v3_0 = 1;
                break;
            default:
                v3_0 = super.onTransact(p8, p9, p10, p11);
        }
        return v3_0;
    }
}
