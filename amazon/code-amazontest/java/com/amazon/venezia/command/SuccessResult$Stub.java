package com.amazon.venezia.command;
public abstract class SuccessResult$Stub extends android.os.Binder implements com.amazon.venezia.command.SuccessResult {
    private static final String DESCRIPTOR = "com.amazon.venezia.command.SuccessResult";
    static final int TRANSACTION_getAuthToken = 1;
    static final int TRANSACTION_getData = 2;

    public SuccessResult$Stub()
    {
        this.attachInterface(this, "com.amazon.venezia.command.SuccessResult");
        return;
    }

    public static com.amazon.venezia.command.SuccessResult asInterface(android.os.IBinder p2)
    {
        com.amazon.venezia.command.SuccessResult v1_3;
        if (p2 != null) {
            com.amazon.venezia.command.SuccessResult v0_0 = p2.queryLocalInterface("com.amazon.venezia.command.SuccessResult");
            if ((v0_0 == null) || (!(v0_0 instanceof com.amazon.venezia.command.SuccessResult))) {
                v1_3 = new com.amazon.venezia.command.SuccessResult$Stub$Proxy(p2);
            } else {
                v1_3 = ((com.amazon.venezia.command.SuccessResult) v0_0);
            }
        } else {
            v1_3 = 0;
        }
        return v1_3;
    }

    public android.os.IBinder asBinder()
    {
        return this;
    }

    public boolean onTransact(int p5, android.os.Parcel p6, android.os.Parcel p7, int p8)
    {
        int v1;
        switch (p5) {
            case 1:
                p6.enforceInterface("com.amazon.venezia.command.SuccessResult");
                java.util.Map v0_1 = this.getAuthToken();
                p7.writeNoException();
                p7.writeString(v0_1);
                v1 = 1;
                break;
            case 2:
                p6.enforceInterface("com.amazon.venezia.command.SuccessResult");
                java.util.Map v0_0 = this.getData();
                p7.writeNoException();
                p7.writeMap(v0_0);
                v1 = 1;
                break;
            case 1598968902:
                p7.writeString("com.amazon.venezia.command.SuccessResult");
                v1 = 1;
                break;
            default:
                v1 = super.onTransact(p5, p6, p7, p8);
        }
        return v1;
    }
}
