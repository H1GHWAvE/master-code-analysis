package com.amazon.venezia.command;
public interface DecisionExpirationContext implements android.os.IInterface {

    public abstract java.util.Map getExtensionData();

    public abstract String getReason();
}
