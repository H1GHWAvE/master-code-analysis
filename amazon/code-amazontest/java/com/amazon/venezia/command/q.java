package com.amazon.venezia.command;
public abstract class q extends android.os.Binder implements com.amazon.venezia.command.h {

    public q()
    {
        this.attachInterface(this, "com.amazon.venezia.command.CommandService");
        return;
    }

    public static com.amazon.venezia.command.h a(android.os.IBinder p3)
    {
        com.amazon.venezia.command.h v1_3;
        if (p3 != null) {
            com.amazon.venezia.command.h v1_1 = p3.queryLocalInterface("com.amazon.venezia.command.CommandService");
            if ((v1_1 == null) || (!(v1_1 instanceof com.amazon.venezia.command.h))) {
                v1_3 = new com.amazon.venezia.command.l(p3);
            } else {
                v1_3 = ((com.amazon.venezia.command.h) v1_1);
            }
        } else {
            v1_3 = 0;
        }
        return v1_3;
    }

    public boolean onTransact(int p6, android.os.Parcel p7, android.os.Parcel p8, int p9)
    {
        com.amazon.venezia.command.f v0_0;
        switch (p6) {
            case 1:
                com.amazon.venezia.command.w v1_2;
                p7.enforceInterface("com.amazon.venezia.command.CommandService");
                com.amazon.venezia.command.w v1_1 = p7.readStrongBinder();
                if (v1_1 != null) {
                    com.amazon.venezia.command.f v0_2 = v1_1.queryLocalInterface("com.amazon.venezia.command.Command");
                    if ((v0_2 == null) || (!(v0_2 instanceof com.amazon.venezia.command.w))) {
                        v1_2 = new com.amazon.venezia.command.d(v1_1);
                    } else {
                        v1_2 = ((com.amazon.venezia.command.w) v0_2);
                    }
                } else {
                    v1_2 = 0;
                }
                com.amazon.venezia.command.f v0_9;
                boolean v2_1 = p7.readStrongBinder();
                if (v2_1) {
                    com.amazon.venezia.command.f v0_7 = v2_1.queryLocalInterface("com.amazon.venezia.command.ResultCallback");
                    if ((v0_7 == null) || (!(v0_7 instanceof com.amazon.venezia.command.f))) {
                        v0_9 = new com.amazon.venezia.command.t(v2_1);
                    } else {
                        v0_9 = ((com.amazon.venezia.command.f) v0_7);
                    }
                } else {
                    v0_9 = 0;
                }
                this.a(v1_2, v0_9);
                p8.writeNoException();
                v0_0 = 1;
                break;
            case 1598968902:
                p8.writeString("com.amazon.venezia.command.CommandService");
                v0_0 = 1;
                break;
            default:
                v0_0 = super.onTransact(p6, p7, p8, p9);
        }
        return v0_0;
    }
}
