package com.amazon.venezia.command;
final class ab implements com.amazon.venezia.command.FailureResult {
    private android.os.IBinder a;

    ab(android.os.IBinder p1)
    {
        this.a = p1;
        return;
    }

    public final android.os.IBinder asBinder()
    {
        return this.a;
    }

    public final String getAuthToken()
    {
        android.os.Parcel v0 = android.os.Parcel.obtain();
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            v0.writeInterfaceToken("com.amazon.venezia.command.FailureResult");
            this.a.transact(1, v0, v1, 0);
            v1.readException();
            Throwable v2_2 = v1.readString();
            v1.recycle();
            v0.recycle();
            return v2_2;
        } catch (Throwable v2_3) {
            v1.recycle();
            v0.recycle();
            throw v2_3;
        }
    }

    public final String getButtonLabel()
    {
        android.os.Parcel v0 = android.os.Parcel.obtain();
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            v0.writeInterfaceToken("com.amazon.venezia.command.FailureResult");
            this.a.transact(4, v0, v1, 0);
            v1.readException();
            Throwable v2_2 = v1.readString();
            v1.recycle();
            v0.recycle();
            return v2_2;
        } catch (Throwable v2_3) {
            v1.recycle();
            v0.recycle();
            throw v2_3;
        }
    }

    public final String getDisplayableMessage()
    {
        android.os.Parcel v0 = android.os.Parcel.obtain();
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            v0.writeInterfaceToken("com.amazon.venezia.command.FailureResult");
            this.a.transact(3, v0, v1, 0);
            v1.readException();
            Throwable v2_2 = v1.readString();
            v1.recycle();
            v0.recycle();
            return v2_2;
        } catch (Throwable v2_3) {
            v1.recycle();
            v0.recycle();
            throw v2_3;
        }
    }

    public final String getDisplayableName()
    {
        android.os.Parcel v0 = android.os.Parcel.obtain();
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            v0.writeInterfaceToken("com.amazon.venezia.command.FailureResult");
            this.a.transact(2, v0, v1, 0);
            v1.readException();
            Throwable v2_2 = v1.readString();
            v1.recycle();
            v0.recycle();
            return v2_2;
        } catch (Throwable v2_3) {
            v1.recycle();
            v0.recycle();
            throw v2_3;
        }
    }

    public final java.util.Map getExtensionData()
    {
        android.os.Parcel v0 = android.os.Parcel.obtain();
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            v0.writeInterfaceToken("com.amazon.venezia.command.FailureResult");
            this.a.transact(6, v0, v1, 0);
            v1.readException();
            Throwable v2_4 = v1.readHashMap(this.getClass().getClassLoader());
            v1.recycle();
            v0.recycle();
            return v2_4;
        } catch (Throwable v2_5) {
            v1.recycle();
            v0.recycle();
            throw v2_5;
        }
    }

    public final boolean show()
    {
        android.os.Parcel v0 = android.os.Parcel.obtain();
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            int v2_4;
            v0.writeInterfaceToken("com.amazon.venezia.command.FailureResult");
            this.a.transact(5, v0, v1, 0);
            v1.readException();
        } catch (int v2_3) {
            v1.recycle();
            v0.recycle();
            throw v2_3;
        }
        if (v1.readInt() == 0) {
            v2_4 = 0;
        } else {
            v2_4 = 1;
        }
        v1.recycle();
        v0.recycle();
        return v2_4;
    }
}
