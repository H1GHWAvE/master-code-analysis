package com.amazon.venezia.service.verify;
public abstract class IApplicationVerificationService$Stub extends android.os.Binder implements com.amazon.venezia.service.verify.IApplicationVerificationService {
    private static final String DESCRIPTOR = "com.amazon.venezia.service.verify.IApplicationVerificationService";
    static final int TRANSACTION_getAmazonId = 3;
    static final int TRANSACTION_getDeviceId = 4;
    static final int TRANSACTION_getToken = 2;
    static final int TRANSACTION_reportVerificationResults = 1;

    public IApplicationVerificationService$Stub()
    {
        this.attachInterface(this, "com.amazon.venezia.service.verify.IApplicationVerificationService");
        return;
    }

    public static com.amazon.venezia.service.verify.IApplicationVerificationService asInterface(android.os.IBinder p2)
    {
        com.amazon.venezia.service.verify.IApplicationVerificationService v1_3;
        if (p2 != null) {
            com.amazon.venezia.service.verify.IApplicationVerificationService v0_0 = p2.queryLocalInterface("com.amazon.venezia.service.verify.IApplicationVerificationService");
            if ((v0_0 == null) || (!(v0_0 instanceof com.amazon.venezia.service.verify.IApplicationVerificationService))) {
                v1_3 = new com.amazon.venezia.service.verify.IApplicationVerificationService$Stub$Proxy(p2);
            } else {
                v1_3 = ((com.amazon.venezia.service.verify.IApplicationVerificationService) v0_0);
            }
        } else {
            v1_3 = 0;
        }
        return v1_3;
    }

    public android.os.IBinder asBinder()
    {
        return this;
    }

    public boolean onTransact(int p8, android.os.Parcel p9, android.os.Parcel p10, int p11)
    {
        int v4_0;
        switch (p8) {
            case 1:
                int v1;
                p9.enforceInterface("com.amazon.venezia.service.verify.IApplicationVerificationService");
                String v0_1 = p9.readString();
                if (p9.readInt() == 0) {
                    v1 = 0;
                } else {
                    v1 = 1;
                }
                this.reportVerificationResults(v0_1, v1, p9.readString());
                p10.writeNoException();
                v4_0 = 1;
                break;
            case 2:
                p9.enforceInterface("com.amazon.venezia.service.verify.IApplicationVerificationService");
                String v3_2 = this.getToken(p9.readString());
                p10.writeNoException();
                p10.writeString(v3_2);
                v4_0 = 1;
                break;
            case 3:
                p9.enforceInterface("com.amazon.venezia.service.verify.IApplicationVerificationService");
                String v3_1 = this.getAmazonId();
                p10.writeNoException();
                p10.writeString(v3_1);
                v4_0 = 1;
                break;
            case 4:
                p9.enforceInterface("com.amazon.venezia.service.verify.IApplicationVerificationService");
                String v3_0 = this.getDeviceId();
                p10.writeNoException();
                p10.writeString(v3_0);
                v4_0 = 1;
                break;
            case 1598968902:
                p10.writeString("com.amazon.venezia.service.verify.IApplicationVerificationService");
                v4_0 = 1;
                break;
            default:
                v4_0 = super.onTransact(p8, p9, p10, p11);
        }
        return v4_0;
    }
}
