package com.amazon.venezia.service.verify;
public interface IApplicationVerificationService implements android.os.IInterface {

    public abstract String getAmazonId();

    public abstract String getDeviceId();

    public abstract String getToken();

    public abstract void reportVerificationResults();
}
