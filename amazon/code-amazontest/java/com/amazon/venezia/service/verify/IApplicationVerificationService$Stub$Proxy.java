package com.amazon.venezia.service.verify;
 class IApplicationVerificationService$Stub$Proxy implements com.amazon.venezia.service.verify.IApplicationVerificationService {
    private android.os.IBinder mRemote;

    IApplicationVerificationService$Stub$Proxy(android.os.IBinder p1)
    {
        this.mRemote = p1;
        return;
    }

    public android.os.IBinder asBinder()
    {
        return this.mRemote;
    }

    public String getAmazonId()
    {
        android.os.Parcel v0 = android.os.Parcel.obtain();
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            v0.writeInterfaceToken("com.amazon.venezia.service.verify.IApplicationVerificationService");
            this.mRemote.transact(3, v0, v1, 0);
            v1.readException();
            String v2 = v1.readString();
            v1.recycle();
            v0.recycle();
            return v2;
        } catch (Throwable v3_2) {
            v1.recycle();
            v0.recycle();
            throw v3_2;
        }
    }

    public String getDeviceId()
    {
        android.os.Parcel v0 = android.os.Parcel.obtain();
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            v0.writeInterfaceToken("com.amazon.venezia.service.verify.IApplicationVerificationService");
            this.mRemote.transact(4, v0, v1, 0);
            v1.readException();
            String v2 = v1.readString();
            v1.recycle();
            v0.recycle();
            return v2;
        } catch (Throwable v3_2) {
            v1.recycle();
            v0.recycle();
            throw v3_2;
        }
    }

    public String getInterfaceDescriptor()
    {
        return "com.amazon.venezia.service.verify.IApplicationVerificationService";
    }

    public String getToken(String p7)
    {
        android.os.Parcel v0 = android.os.Parcel.obtain();
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            v0.writeInterfaceToken("com.amazon.venezia.service.verify.IApplicationVerificationService");
            v0.writeString(p7);
            this.mRemote.transact(2, v0, v1, 0);
            v1.readException();
            String v2 = v1.readString();
            v1.recycle();
            v0.recycle();
            return v2;
        } catch (Throwable v3_2) {
            v1.recycle();
            v0.recycle();
            throw v3_2;
        }
    }

    public void reportVerificationResults(String p6, boolean p7, String p8)
    {
        android.os.Parcel v0 = android.os.Parcel.obtain();
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            android.os.IBinder v2_1;
            v0.writeInterfaceToken("com.amazon.venezia.service.verify.IApplicationVerificationService");
            v0.writeString(p6);
        } catch (android.os.IBinder v2_2) {
            v1.recycle();
            v0.recycle();
            throw v2_2;
        }
        if (!p7) {
            v2_1 = 0;
        } else {
            v2_1 = 1;
        }
        v0.writeInt(v2_1);
        v0.writeString(p8);
        this.mRemote.transact(1, v0, v1, 0);
        v1.readException();
        v1.recycle();
        v0.recycle();
        return;
    }
}
