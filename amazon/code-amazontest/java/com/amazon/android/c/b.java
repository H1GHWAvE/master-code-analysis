package com.amazon.android.c;
public final class b implements java.io.Serializable {
    private static final com.amazon.android.framework.util.KiwiLogger a = None;
    private static final long serialVersionUID = 1;
    private final java.util.HashMap b;

    static b()
    {
        com.amazon.android.c.b.a = new com.amazon.android.framework.util.KiwiLogger("CrashReport");
        return;
    }

    public b(android.app.Application p7, Throwable p8)
    {
        this.b = new java.util.LinkedHashMap();
        try {
            this.b.put("crashTime", new java.util.Date().toString());
            java.util.HashMap v0_3 = com.amazon.android.c.b.a(p7);
        } catch (java.util.HashMap v0_31) {
            if (!com.amazon.android.framework.util.KiwiLogger.ERROR_ON) {
                return;
            } else {
                com.amazon.android.c.b.a.error("Error collection crash report details", v0_31);
                return;
            }
        }
        if (v0_3 != null) {
            this.b.put("packageVersionName", v0_3.versionName);
            this.b.put("packageName", v0_3.packageName);
            this.b.put("packageFilePath", p7.getFilesDir().getAbsolutePath());
        }
        this.b.put("deviceModel", android.os.Build.MODEL);
        this.b.put("androidVersion", android.os.Build$VERSION.RELEASE);
        this.b.put("deviceBoard", android.os.Build.BOARD);
        this.b.put("deviceBrand", android.os.Build.BRAND);
        this.b.put("deviceDisplay", android.os.Build.DISPLAY);
        this.b.put("deviceFingerPrint", android.os.Build.FINGERPRINT);
        this.b.put("deviceHost", android.os.Build.HOST);
        this.b.put("deviceId", android.os.Build.ID);
        this.b.put("deviceManufacturer", android.os.Build.MANUFACTURER);
        this.b.put("deviceProduct", android.os.Build.PRODUCT);
        this.b.put("deviceTags", android.os.Build.TAGS);
        this.b.put("deviceTime", Long.toString(android.os.Build.TIME));
        this.b.put("deviceType", android.os.Build.TYPE);
        this.b.put("deviceUser", android.os.Build.USER);
        java.util.HashMap v0_20 = this.b;
        long v3_2 = new android.os.StatFs(android.os.Environment.getDataDirectory().getPath());
        v0_20.put("totalInternalMemorySize", Long.toString((((long) v3_2.getBlockCount()) * ((long) v3_2.getBlockSize()))));
        java.util.HashMap v0_21 = this.b;
        long v3_4 = new android.os.StatFs(android.os.Environment.getDataDirectory().getPath());
        v0_21.put("availableInternalMemorySize", Long.toString((((long) v3_4.getAvailableBlocks()) * ((long) v3_4.getBlockSize()))));
        java.util.HashMap v0_24 = ((android.app.ActivityManager) p7.getSystemService("activity"));
        if (v0_24 != null) {
            String v1_21 = new android.app.ActivityManager$MemoryInfo();
            v0_24.getMemoryInfo(v1_21);
            this.b.put("memLowFlag", Boolean.toString(v1_21.lowMemory));
            this.b.put("memLowThreshold", Long.toString(v1_21.threshold));
        }
        this.b.put("nativeHeapSize", Long.toString(android.os.Debug.getNativeHeapSize()));
        this.b.put("nativeHeapFreeSize", Long.toString(android.os.Debug.getNativeHeapAllocatedSize()));
        this.b.put("threadAllocCount", Long.toString(((long) android.os.Debug.getThreadAllocCount())));
        this.b.put("threadAllocSize", Long.toString(((long) android.os.Debug.getThreadAllocSize())));
        this.a(p8);
        this.b();
        this.c();
        return;
    }

    private static android.content.pm.PackageInfo a(android.app.Application p3)
    {
        try {
            int v0_1 = p3.getPackageManager().getPackageInfo(p3.getPackageName(), 0);
        } catch (int v0_2) {
            if (com.amazon.android.framework.util.KiwiLogger.ERROR_ON) {
                com.amazon.android.c.b.a.error("Unable to fetch package info", v0_2);
            }
            v0_1 = 0;
        }
        return v0_1;
    }

    private void a(Throwable p6)
    {
        String v0_1 = new StringBuilder();
        java.util.HashMap v1_1 = new java.io.StringWriter();
        String v2_1 = new java.io.PrintWriter(v1_1);
        p6.printStackTrace(v2_1);
        v0_1.append(v1_1.toString());
        v0_1.append("\n");
        Throwable v3_2 = p6.getCause();
        while (v3_2 != null) {
            v3_2.printStackTrace(v2_1);
            v0_1.append(v1_1.toString());
            v3_2 = v3_2.getCause();
            v0_1.append("\n\n");
        }
        v2_1.close();
        this.b.put("stackTrace", v0_1.toString());
        return;
    }

    private void b()
    {
        String v2_1 = new StringBuilder();
        java.util.Iterator v3 = Thread.getAllStackTraces().entrySet().iterator();
        while (v3.hasNext()) {
            String v0_5 = ((java.util.Map$Entry) v3.next());
            int v1_2 = ((Thread) v0_5.getKey());
            String v0_7 = ((StackTraceElement[]) v0_5.getValue());
            v2_1.append(new StringBuilder().append("Thread : ").append(v1_2.getId()).toString());
            if (!com.amazon.android.framework.util.b.a(v1_2.getName())) {
                v2_1.append(new StringBuilder().append("/").append(v1_2.getName()).toString());
            }
            v2_1.append("\n");
            v2_1.append(new StringBuilder().append("isAlive : ").append(v1_2.isAlive()).append("\n").toString());
            v2_1.append(new StringBuilder().append("isInterrupted : ").append(v1_2.isInterrupted()).append("\n").toString());
            v2_1.append(new StringBuilder().append("isDaemon : ").append(v1_2.isDaemon()).append("\n").toString());
            int v1_7 = 0;
            while (v1_7 < v0_7.length) {
                v2_1.append(new StringBuilder().append("\tat ").append(v0_7[v1_7]).append("\n").toString());
                v1_7++;
            }
            v2_1.append("\n\n");
        }
        this.b.put("threadDump", v2_1.toString());
        return;
    }

    private void c()
    {
        try {
            com.amazon.android.framework.util.KiwiLogger v1_1 = new StringBuilder();
            v1_1.append(((String) this.b.get("packageName"))).append(((String) this.b.get("packageVersionName"))).append(((String) this.b.get("androidVersion")));
            String v0_11 = ((String) this.b.get("stackTrace"));
        } catch (String v0_18) {
            if (!com.amazon.android.framework.util.KiwiLogger.ERROR_ON) {
                return;
            } else {
                com.amazon.android.c.b.a.error("Error capturing crash id", v0_18);
                return;
            }
        }
        if (v0_11 != null) {
            String v0_12 = java.util.regex.Pattern.compile("([a-zA-Z0-9_.]+(Exception|Error))|(at\\s.*\\(.*\\))").matcher(v0_11);
            while (v0_12.find()) {
                v1_1.append(v0_12.group());
            }
        }
        this.b.put("crashId", new java.math.BigInteger(java.security.MessageDigest.getInstance("SHA1").digest(v1_1.toString().getBytes("UTF-8"))).abs().toString(16));
        return;
    }

    public final java.util.Map a()
    {
        return this.b;
    }
}
