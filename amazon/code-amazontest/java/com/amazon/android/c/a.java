package com.amazon.android.c;
public final class a implements com.amazon.android.c.d, com.amazon.android.framework.resource.b, java.lang.Thread$UncaughtExceptionHandler {
    private static final com.amazon.android.framework.util.KiwiLogger a;
    private com.amazon.android.n.g b;
    private com.amazon.android.g.c c;
    private android.app.Application d;
    private Thread$UncaughtExceptionHandler e;
    private java.util.Map f;

    static a()
    {
        com.amazon.android.c.a.a = new com.amazon.android.framework.util.KiwiLogger("CrashManagerImpl");
        return;
    }

    public a()
    {
        this.f = new java.util.HashMap();
        return;
    }

    static synthetic java.util.Map a(com.amazon.android.c.a p1)
    {
        return p1.f;
    }

    private declared_synchronized void a(String p6)
    {
        try {
            Throwable v1_5 = 0;
            try {
                Exception v0_6 = this.d.openFileOutput(new StringBuilder().append("s-").append(new java.util.Random().nextInt(99999)).append(".amzst").toString(), 0);
            } catch (Exception v0_7) {
                if (com.amazon.android.framework.util.KiwiLogger.ERROR_ON) {
                    com.amazon.android.c.a.a.error("Coud not save crash report to file", v0_7);
                }
                com.amazon.android.framework.util.a.a(v1_5);
                return;
            } catch (Exception v0_8) {
                com.amazon.android.framework.util.a.a(v1_5);
                throw v0_8;
            }
            try {
                v0_6.write(p6.getBytes());
            } catch (Throwable v1_8) {
                v1_5 = v0_6;
                v0_8 = v1_8;
            } catch (Throwable v1_7) {
                v1_5 = v0_6;
                v0_7 = v1_7;
            }
            com.amazon.android.framework.util.a.a(v0_6);
            return;
        } catch (Exception v0_9) {
            throw v0_9;
        }
    }

    private com.amazon.android.c.b b(String p4)
    {
        try {
            int v0_2 = ((com.amazon.android.c.b) com.amazon.android.u.a.a(this.c.b(com.amazon.android.c.a.c(p4))));
        } catch (int v0) {
            if (com.amazon.android.framework.util.KiwiLogger.ERROR_ON) {
                com.amazon.android.c.a.a.error(new StringBuilder().append("Failed to load crash report: ").append(p4).toString());
            }
            v0_2 = 0;
        }
        return v0_2;
    }

    private boolean b()
    {
        int v0_2;
        if (this.f.size() < 5) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    private static String c(String p4)
    {
        String v0_1 = new StringBuilder();
        String v1_0 = 0;
        try {
            java.io.BufferedReader v2_1 = new java.io.BufferedReader(new java.io.FileReader(p4));
            try {
                while (v2_1.ready()) {
                    v0_1.append(v2_1.readLine());
                }
            } catch (String v0_2) {
                v1_0 = v2_1;
                com.amazon.android.framework.util.a.a(v1_0);
                throw v0_2;
            }
            com.amazon.android.framework.util.a.a(v2_1);
            return v0_1.toString();
        } catch (String v0_2) {
        }
    }

    private static void d(String p4)
    {
        try {
            new java.io.File(p4).delete();
        } catch (Exception v0_2) {
            if (!com.amazon.android.framework.util.KiwiLogger.ERROR_ON) {
            } else {
                com.amazon.android.c.a.a.error(new StringBuilder().append("Cannot delete file: ").append(p4).toString(), v0_2);
            }
        }
        return;
    }

    public final java.util.List a()
    {
        java.util.ArrayList v0_2;
        if (!this.b()) {
            v0_2 = new java.util.ArrayList();
            String[] v1_2 = new java.io.File(new StringBuilder().append(this.d.getFilesDir().getAbsolutePath()).append("/").toString()).list(new com.amazon.android.c.c(this));
            int v2_7 = 0;
            while ((v2_7 < v1_2.length) && (!this.b())) {
                String v3_7 = new StringBuilder().append(this.d.getFilesDir().getAbsolutePath()).append("/").append(v1_2[v2_7]).toString();
                com.amazon.android.c.b v4_4 = this.b(v3_7);
                if (v4_4 == null) {
                    com.amazon.android.c.a.d(v3_7);
                } else {
                    this.f.put(v4_4, v3_7);
                    v0_2.add(v4_4);
                }
                v2_7++;
            }
        } else {
            v0_2 = java.util.Collections.emptyList();
        }
        return v0_2;
    }

    public final void a(java.util.List p4)
    {
        java.util.Iterator v2 = p4.iterator();
        while (v2.hasNext()) {
            com.amazon.android.c.b v0_2 = ((com.amazon.android.c.b) v2.next());
            com.amazon.android.c.a.d(((String) this.f.get(v0_2)));
            this.f.remove(v0_2);
        }
        return;
    }

    public final void onResourcesPopulated()
    {
        com.amazon.android.d.a.a();
        if (!(Thread.getDefaultUncaughtExceptionHandler() instanceof com.amazon.android.c.d)) {
            if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                com.amazon.android.c.a.a.trace("Registering Crash Handler");
            }
            this.e = Thread.getDefaultUncaughtExceptionHandler();
            Thread.setDefaultUncaughtExceptionHandler(this);
        }
        return;
    }

    public final void uncaughtException(Thread p4, Throwable p5)
    {
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.c.a.a.trace("Crash detected", p5);
        }
        try {
            this.a(this.c.a(com.amazon.android.u.a.a(new com.amazon.android.c.b(this.d, p5))));
        } catch (Thread$UncaughtExceptionHandler v0_6) {
            if (!com.amazon.android.framework.util.KiwiLogger.ERROR_ON) {
            } else {
                com.amazon.android.c.a.a.error("Could not handle uncaught exception", v0_6);
            }
        } catch (Thread$UncaughtExceptionHandler v0_8) {
            if (!com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                    com.amazon.android.c.a.a.trace("Calling previous handler");
                }
                if (this.e != null) {
                    this.e.uncaughtException(p4, p5);
                }
                return;
            } else {
                com.amazon.android.c.a.a.trace("Error occured while handling exception", v0_8);
            }
        }
        this.b.a(new com.amazon.android.a.b());
    }
}
