package com.amazon.android.g;
public final class a implements com.amazon.android.framework.resource.b, com.amazon.android.g.c {
    private static final com.amazon.android.framework.util.KiwiLogger a;
    private static final java.util.Comparator b;
    private android.app.Application c;
    private javax.crypto.spec.SecretKeySpec d;

    static a()
    {
        com.amazon.android.g.a.a = new com.amazon.android.framework.util.KiwiLogger("ObfuscationManagerImpl");
        com.amazon.android.g.a.b = new com.amazon.android.g.b();
        return;
    }

    public a()
    {
        return;
    }

    private javax.crypto.spec.SecretKeySpec a()
    {
        int v0_1 = new StringBuilder();
        v0_1.append(this.c.getPackageName());
        v0_1.append(android.provider.Settings$Secure.getString(this.c.getContentResolver(), "android_id"));
        v0_1.append(android.os.Build.FINGERPRINT);
        v0_1.append(android.os.Build.BRAND);
        v0_1.append(android.os.Build.BOARD);
        v0_1.append(android.os.Build.MODEL);
        int v0_2 = v0_1.toString();
        try {
            com.amazon.android.framework.util.KiwiLogger v1_9 = com.amazon.android.g.a.b();
            String v2_2 = javax.crypto.KeyGenerator.getInstance("AES");
            v1_9.setSeed(com.amazon.android.g.a.a("SHA-256", com.amazon.android.g.a.a("MD5", v0_2.getBytes())));
            v2_2.init(128, v1_9);
            int v0_9 = new javax.crypto.spec.SecretKeySpec(v2_2.generateKey().getEncoded(), "AES");
        } catch (int v0_11) {
            if (!com.amazon.android.framework.util.KiwiLogger.ERROR_ON) {
                v0_9 = 0;
            } else {
                com.amazon.android.g.a.a.error(new StringBuilder().append("Unable to create KeySpec: ").append(v0_11).toString(), v0_11);
            }
        } catch (int v0_10) {
            if (!com.amazon.android.framework.util.KiwiLogger.ERROR_ON) {
            } else {
                com.amazon.android.g.a.a.error("Unable to find appropriate provider: ", v0_10);
            }
        }
        return v0_9;
    }

    private static byte[] a(String p4, byte[] p5)
    {
        try {
            int v0_0 = java.security.MessageDigest.getInstance(p4);
            v0_0.update(p5);
            int v0_1 = v0_0.digest();
        } catch (int v0_2) {
            if (com.amazon.android.framework.util.KiwiLogger.ERROR_ON) {
                com.amazon.android.g.a.a.error(new StringBuilder().append("Failed to create MessageDigest: ").append(v0_2).toString(), v0_2);
            }
            v0_1 = 0;
        }
        return v0_1;
    }

    private byte[] a(byte[] p5)
    {
        try {
            int v0_1 = javax.crypto.Cipher.getInstance("AES");
            v0_1.init(1, this.d);
            int v0_2 = v0_1.doFinal(p5);
        } catch (int v0_3) {
            if (com.amazon.android.framework.util.KiwiLogger.ERROR_ON) {
                com.amazon.android.g.a.a.error(new StringBuilder().append("Error obfuscating data: ").append(v0_3).toString(), v0_3);
            }
            v0_2 = 0;
        }
        return v0_2;
    }

    private static java.security.SecureRandom b()
    {
        java.security.SecureRandom v0_1;
        if (android.os.Build$VERSION.SDK_INT < 17) {
            v0_1 = java.security.SecureRandom.getInstance("SHA1PRNG");
        } else {
            try {
                v0_1 = java.security.SecureRandom.getInstance("SHA1PRNG", "Crypto");
            } catch (java.security.SecureRandom v0) {
                java.security.SecureRandom v0_4 = java.security.Security.getProviders("SecureRandom.SHA1PRNG");
                java.util.Arrays.sort(v0_4, com.amazon.android.g.a.b);
                String v1_4 = new byte[8];
                v1_4 = {0, 1, 2, 3, 4, 5, 6, 7};
                byte[] v2 = new byte[128];
                byte[] v3_1 = new byte[128];
                int v4 = v0_4.length;
                int v5 = 0;
            }
        }
        return v0_1;
    }

    private byte[] b(byte[] p5)
    {
        try {
            int v0_1 = javax.crypto.Cipher.getInstance("AES");
            v0_1.init(2, this.d);
            int v0_2 = v0_1.doFinal(p5);
        } catch (int v0_3) {
            if (com.amazon.android.framework.util.KiwiLogger.ERROR_ON) {
                com.amazon.android.g.a.a.error(new StringBuilder().append("Error unobfuscating data: ").append(v0_3).toString(), v0_3);
            }
            v0_2 = 0;
        }
        return v0_2;
    }

    private static byte[] c(String p7)
    {
        int v0 = p7.length();
        byte[] v1_1 = new byte[(v0 / 2)];
        int v2 = 0;
        while (v2 < v0) {
            v1_1[(v2 / 2)] = ((byte) ((Character.digit(p7.charAt(v2), 16) << 4) | Character.digit(p7.charAt((v2 + 1)), 16)));
            v2 += 2;
        }
        return v1_1;
    }

    public final String a(String p8)
    {
        String v0_4;
        if (p8 != null) {
            if (this.d != null) {
                try {
                    String v0_3 = this.a(p8.getBytes("UTF-8"));
                    StringBuffer v1_1 = new StringBuffer((v0_3.length * 2));
                    int v2_2 = 0;
                } catch (String v0) {
                    v0_4 = 0;
                }
                while (v2_2 < v0_3.length) {
                    char v3_1 = v0_3[v2_2];
                    v1_1.append("0123456789ABCDEF".charAt(((v3_1 >> 4) & 15))).append("0123456789ABCDEF".charAt((v3_1 & 15)));
                    v2_2++;
                }
                v0_4 = v1_1.toString();
            } else {
                v0_4 = 0;
            }
        } else {
            v0_4 = 0;
        }
        return v0_4;
    }

    public final String b(String p5)
    {
        int v0_2;
        if (p5 != null) {
            if (this.d != null) {
                try {
                    v0_2 = new String(this.b(com.amazon.android.g.a.c(p5)), "UTF-8");
                } catch (int v0) {
                    v0_2 = 0;
                }
            } else {
                v0_2 = 0;
            }
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    public final void onResourcesPopulated()
    {
        this.d = this.a();
        return;
    }
}
