package com.amazon.android.framework.prompt;
final class e implements com.amazon.android.framework.task.Task {
    private synthetic com.amazon.android.framework.prompt.Prompt a;
    private synthetic com.amazon.android.framework.prompt.PromptManagerImpl b;

    e(com.amazon.android.framework.prompt.PromptManagerImpl p1, com.amazon.android.framework.prompt.Prompt p2)
    {
        this.b = p1;
        this.a = p2;
        return;
    }

    public final void execute()
    {
        com.amazon.android.framework.prompt.PromptManagerImpl.b(this.b, this.a);
        return;
    }

    public final String toString()
    {
        return new StringBuilder().append("PromptManager:removeExpiredPrompt: ").append(this.a).toString();
    }
}
