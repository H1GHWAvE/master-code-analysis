package com.amazon.android.framework.prompt;
public abstract class Prompt extends com.amazon.android.i.c {
    private static final com.amazon.android.framework.util.KiwiLogger LOGGER;
    private android.app.Activity context;
    private com.amazon.android.o.a dataStore;
    private android.app.Dialog dialog;
    private final java.util.concurrent.atomic.AtomicBoolean dismissed;
    private final int identifier;
    private com.amazon.android.framework.prompt.d manualExpirationReason;

    static Prompt()
    {
        com.amazon.android.framework.prompt.Prompt.LOGGER = new com.amazon.android.framework.util.KiwiLogger("Prompt");
        return;
    }

    public Prompt()
    {
        this.dismissed = new java.util.concurrent.atomic.AtomicBoolean(0);
        this.identifier = this.createIdentifier();
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.framework.prompt.Prompt.LOGGER.trace(new StringBuilder().append("Creating Prompt: ").append(this.identifier).toString());
        }
        return;
    }

    private int createIdentifier()
    {
        int v0_3 = (new java.util.Random().nextInt(2146249079) + 1234567);
        if (v0_3 <= 1234567) {
            v0_3 = 1234567;
        }
        return v0_3;
    }

    private void dismissDialog()
    {
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.framework.prompt.Prompt.LOGGER.error(new StringBuilder().append("Dismissing dialog: ").append(this.identifier).toString());
        }
        try {
            this.context.dismissDialog(this.identifier);
            this.context.removeDialog(this.identifier);
        } catch (com.amazon.android.framework.util.KiwiLogger v0) {
            if (!com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            } else {
                com.amazon.android.framework.prompt.Prompt.LOGGER.error(new StringBuilder().append("Unable to remove dialog: ").append(this.identifier).toString());
            }
        }
        this.context = 0;
        this.dialog = 0;
        return;
    }

    private void expire(com.amazon.android.framework.prompt.d p4)
    {
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.framework.prompt.Prompt.LOGGER.trace(new StringBuilder().append("Expiring prompt pre-maturely: id: ").append(this.getIdentifier()).append(", prompt: ").append(this).append(",").append(", reason: ").append(p4).toString());
        }
        this.manualExpirationReason = p4;
        this.expire();
        return;
    }

    private boolean isCompatible(android.app.Activity p3)
    {
        boolean v0_2;
        if (!this.dataStore.b("TEST_MODE")) {
            v0_2 = this.doCompatibilityCheck(p3);
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    private void showDialog(android.app.Activity p4)
    {
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.framework.prompt.Prompt.LOGGER.trace(new StringBuilder().append("Showing prompt, id: ").append(this.getIdentifier()).append(", prompt: ").append(this).append(", activity: ").append(p4).toString());
        }
        if (this.context != null) {
            this.dismissDialog();
        }
        p4.showDialog(this.getIdentifier());
        return;
    }

    public final android.app.Dialog create(android.app.Activity p3)
    {
        this.context = p3;
        this.dialog = this.doCreate(p3);
        this.dialog.setCancelable(0);
        this.dialog.setOnKeyListener(new com.amazon.android.framework.prompt.b(this));
        return this.dialog;
    }

    protected final boolean dismiss()
    {
        com.amazon.android.d.a.a();
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.framework.prompt.Prompt.LOGGER.trace(new StringBuilder().append("Dismissing Prompt: ").append(this.identifier).toString());
        }
        int v0_5;
        if (this.dismissed.compareAndSet(0, 1)) {
            if (this.context != null) {
                this.dismissDialog();
            }
            this.discard();
            v0_5 = 1;
        } else {
            if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                com.amazon.android.framework.prompt.Prompt.LOGGER.error("Prompt has already been dismissed");
            }
            v0_5 = 0;
        }
        return v0_5;
    }

    protected boolean doCompatibilityCheck(android.app.Activity p2)
    {
        return 1;
    }

    protected abstract android.app.Dialog doCreate();

    protected final void doExpiration()
    {
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.framework.prompt.Prompt.LOGGER.trace(new StringBuilder().append("Expiring prompt: ").append(this).toString());
        }
        this.taskManager.enqueue(com.amazon.android.framework.task.pipeline.TaskPipelineId.FOREGROUND, new com.amazon.android.framework.prompt.a(this));
        this.doExpiration(this.getExpirationReason());
        return;
    }

    protected abstract void doExpiration();

    protected com.amazon.android.framework.prompt.d getExpirationReason()
    {
        com.amazon.android.framework.prompt.d v0_2;
        if (this.isExpired()) {
            if (this.manualExpirationReason != null) {
                v0_2 = this.manualExpirationReason;
            } else {
                v0_2 = com.amazon.android.framework.prompt.d.b;
            }
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    public int getIdentifier()
    {
        return this.identifier;
    }

    public void onFocusChanged(android.app.Activity p3, boolean p4)
    {
        if (p3 == this.context) {
            if ((p4) && (!this.dialog.isShowing())) {
                if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                    com.amazon.android.framework.prompt.Prompt.LOGGER.trace("showing dialog because it was not showing");
                }
                this.dialog.show();
            }
        } else {
            if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                com.amazon.android.framework.prompt.Prompt.LOGGER.trace("Unrecognized context");
            }
        }
        return;
    }

    public final void show(android.app.Activity p2)
    {
        com.amazon.android.d.a.a(p2, "activity");
        com.amazon.android.d.a.a();
        if (!this.isCompatible(p2)) {
            this.expire(com.amazon.android.framework.prompt.d.a);
        } else {
            this.showDialog(p2);
        }
        return;
    }
}
