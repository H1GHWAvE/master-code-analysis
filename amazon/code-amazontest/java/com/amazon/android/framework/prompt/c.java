package com.amazon.android.framework.prompt;
public final class c extends com.amazon.android.framework.prompt.SimplePrompt implements com.amazon.android.framework.resource.b {
    private static final com.amazon.android.framework.util.KiwiLogger a;
    private com.amazon.android.framework.context.ContextManager b;

    static c()
    {
        com.amazon.android.framework.prompt.c.a = new com.amazon.android.framework.util.KiwiLogger("ShutdownPrompt");
        return;
    }

    public c(com.amazon.android.framework.prompt.PromptContent p1)
    {
        this(p1);
        return;
    }

    protected final void doAction()
    {
        this.b.finishActivities();
        com.amazon.android.framework.prompt.c.a.test("license verification failed");
        com.amazon.android.framework.prompt.c.a.test("Killing application");
        return;
    }

    protected final long getExpirationDurationInSeconds()
    {
        return 31536000;
    }

    public final void onResourcesPopulatedImpl()
    {
        this.b.stopServices();
        return;
    }

    public final String toString()
    {
        return "ShutdownPrompt";
    }
}
