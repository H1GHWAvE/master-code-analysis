package com.amazon.android.framework.prompt;
public abstract class SimplePrompt extends com.amazon.android.framework.prompt.Prompt {
    protected final com.amazon.android.framework.prompt.PromptContent content;

    public SimplePrompt(com.amazon.android.framework.prompt.PromptContent p2)
    {
        com.amazon.android.d.a.a(p2, "content");
        this.content = p2;
        return;
    }

    protected abstract void doAction();

    protected boolean doCompatibilityCheck(android.app.Activity p2)
    {
        return this.content.isVisible();
    }

    public final android.app.Dialog doCreate(android.app.Activity p5)
    {
        android.app.AlertDialog v0_1 = new android.app.AlertDialog$Builder(p5);
        v0_1.setTitle(this.content.getTitle()).setMessage(this.content.getMessage()).setCancelable(0).setNeutralButton(this.content.getButtonLabel(), new com.amazon.android.framework.prompt.g(this));
        return v0_1.create();
    }

    protected void doExpiration(com.amazon.android.framework.prompt.d p1)
    {
        this.doAction();
        return;
    }
}
