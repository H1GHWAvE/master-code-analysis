package com.amazon.android.framework.prompt;
public final enum class d extends java.lang.Enum {
    public static final enum com.amazon.android.framework.prompt.d a;
    public static final enum com.amazon.android.framework.prompt.d b;
    private static final synthetic com.amazon.android.framework.prompt.d[] c;

    static d()
    {
        com.amazon.android.framework.prompt.d.a = new com.amazon.android.framework.prompt.d("NOT_COMPATIBLE", 0);
        com.amazon.android.framework.prompt.d.b = new com.amazon.android.framework.prompt.d("EXPIRATION_DURATION_ELAPSED", 1);
        com.amazon.android.framework.prompt.d[] v0_5 = new com.amazon.android.framework.prompt.d[2];
        v0_5[0] = com.amazon.android.framework.prompt.d.a;
        v0_5[1] = com.amazon.android.framework.prompt.d.b;
        com.amazon.android.framework.prompt.d.c = v0_5;
        return;
    }

    private d(String p1, int p2)
    {
        this(p1, p2);
        return;
    }

    public static com.amazon.android.framework.prompt.d valueOf(String p1)
    {
        return ((com.amazon.android.framework.prompt.d) Enum.valueOf(com.amazon.android.framework.prompt.d, p1));
    }

    public static com.amazon.android.framework.prompt.d[] values()
    {
        return ((com.amazon.android.framework.prompt.d[]) com.amazon.android.framework.prompt.d.c.clone());
    }
}
