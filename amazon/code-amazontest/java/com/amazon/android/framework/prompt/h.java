package com.amazon.android.framework.prompt;
final class h implements com.amazon.android.framework.task.Task {
    private synthetic com.amazon.android.framework.prompt.Prompt a;
    private synthetic com.amazon.android.framework.prompt.PromptManagerImpl b;

    h(com.amazon.android.framework.prompt.PromptManagerImpl p1, com.amazon.android.framework.prompt.Prompt p2)
    {
        this.b = p1;
        this.a = p2;
        return;
    }

    public final void execute()
    {
        com.amazon.android.framework.prompt.PromptManagerImpl.a(this.b, this.a);
        return;
    }

    public final String toString()
    {
        return new StringBuilder().append("Prompt Presentation on Main Thread: ").append(this.a).append(", ").append(this.a.getExpiration()).toString();
    }
}
