package com.amazon.android.framework.prompt;
public interface PromptManager implements com.amazon.android.i.d {

    public abstract android.app.Dialog onCreateDialog();

    public abstract void onWindowFocusChanged();

    public abstract void present();
}
