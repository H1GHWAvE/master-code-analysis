package com.amazon.android.framework.prompt;
public class PromptManagerImpl implements com.amazon.android.framework.prompt.PromptManager, com.amazon.android.framework.resource.b {
    public static final com.amazon.android.framework.util.KiwiLogger LOGGER;
    private com.amazon.android.framework.context.ContextManager contextManager;
    private com.amazon.android.n.g eventManager;
    private final java.util.concurrent.atomic.AtomicBoolean finished;
    private java.util.Set pending;
    private com.amazon.android.framework.resource.a resourceManager;
    private com.amazon.android.framework.prompt.Prompt showing;
    private com.amazon.android.framework.task.TaskManager taskManager;

    static PromptManagerImpl()
    {
        com.amazon.android.framework.prompt.PromptManagerImpl.LOGGER = new com.amazon.android.framework.util.KiwiLogger("PromptManagerImpl");
        return;
    }

    public PromptManagerImpl()
    {
        this.pending = new java.util.LinkedHashSet();
        this.finished = new java.util.concurrent.atomic.AtomicBoolean(0);
        return;
    }

    static synthetic void a(com.amazon.android.framework.prompt.PromptManagerImpl p0)
    {
        p0.finish();
        return;
    }

    static synthetic void a(com.amazon.android.framework.prompt.PromptManagerImpl p0, android.app.Activity p1)
    {
        p0.onResume(p1);
        return;
    }

    static synthetic void a(com.amazon.android.framework.prompt.PromptManagerImpl p0, com.amazon.android.framework.prompt.Prompt p1)
    {
        p0.presentImpl(p1);
        return;
    }

    static synthetic com.amazon.android.framework.prompt.Prompt b(com.amazon.android.framework.prompt.PromptManagerImpl p1)
    {
        return p1.showing;
    }

    static synthetic void b(com.amazon.android.framework.prompt.PromptManagerImpl p0, com.amazon.android.framework.prompt.Prompt p1)
    {
        p0.removeExpiredPrompt(p1);
        return;
    }

    private void finish()
    {
        if (this.finished.compareAndSet(0, 1)) {
            if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                com.amazon.android.framework.prompt.PromptManagerImpl.LOGGER.trace("PromptManager finishing....");
            }
            java.util.Iterator v1_2 = this.pending.iterator();
            while (v1_2.hasNext()) {
                com.amazon.android.framework.prompt.Prompt v0_9 = ((com.amazon.android.framework.prompt.Prompt) v1_2.next());
                v1_2.remove();
                v0_9.expire();
            }
            if (this.showing != null) {
                this.showing.dismiss();
            }
        }
        return;
    }

    private com.amazon.android.framework.prompt.Prompt getNextPending()
    {
        com.amazon.android.framework.prompt.Prompt v0_4;
        if (!this.pending.isEmpty()) {
            v0_4 = ((com.amazon.android.framework.prompt.Prompt) this.pending.iterator().next());
        } else {
            v0_4 = 0;
        }
        return v0_4;
    }

    private void onResume(android.app.Activity p2)
    {
        if (this.showing == null) {
            this.presentNextPending(p2);
        } else {
            this.show(this.showing, p2);
        }
        return;
    }

    private void presentImpl(com.amazon.android.framework.prompt.Prompt p4)
    {
        if (!this.finished.get()) {
            if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                com.amazon.android.framework.prompt.PromptManagerImpl.LOGGER.trace(new StringBuilder().append("Presening Prompt: ").append(p4).toString());
            }
            p4.register(this);
            this.pending.add(p4);
            if (this.showing == null) {
                com.amazon.android.framework.util.KiwiLogger v0_7 = this.contextManager.getVisible();
                if (v0_7 != null) {
                    this.presentNextPending(v0_7);
                }
            } else {
                if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                    com.amazon.android.framework.prompt.PromptManagerImpl.LOGGER.trace("Dialog currently showing, not presenting given dialog");
                }
            }
        } else {
            if (com.amazon.android.framework.util.KiwiLogger.ERROR_ON) {
                com.amazon.android.framework.prompt.PromptManagerImpl.LOGGER.error(new StringBuilder().append("Prompt: ").append(p4).append(" presented after app").append(" destruction expiring it now!").toString());
            }
            p4.expire();
        }
        return;
    }

    private void presentNextPending(android.app.Activity p2)
    {
        com.amazon.android.framework.prompt.Prompt v0 = this.getNextPending();
        if (v0 != null) {
            this.show(v0, p2);
        }
        return;
    }

    private void registerActivityResumedListener()
    {
        this.eventManager.a(new com.amazon.android.framework.prompt.f(this));
        return;
    }

    private void registerAppDestructionListener()
    {
        this.eventManager.a(new com.amazon.android.framework.prompt.i(this));
        return;
    }

    private void registerTestModeListener()
    {
        this.eventManager.a(new com.amazon.android.framework.prompt.j(this));
        return;
    }

    private void removeExpiredPrompt(com.amazon.android.framework.prompt.Prompt p2)
    {
        this.pending.remove(p2);
        if (this.showing == p2) {
            this.showing = 0;
            android.app.Activity v0_4 = this.contextManager.getVisible();
            if (v0_4 != null) {
                this.presentNextPending(v0_4);
            }
        }
        return;
    }

    private void show(com.amazon.android.framework.prompt.Prompt p1, android.app.Activity p2)
    {
        this.showing = p1;
        p1.show(p2);
        return;
    }

    public void observe(com.amazon.android.framework.prompt.Prompt p4)
    {
        this.taskManager.enqueue(com.amazon.android.framework.task.pipeline.TaskPipelineId.FOREGROUND, new com.amazon.android.framework.prompt.e(this, p4));
        return;
    }

    public bridge synthetic void observe(com.amazon.android.i.b p1)
    {
        this.observe(((com.amazon.android.framework.prompt.Prompt) p1));
        return;
    }

    public android.app.Dialog onCreateDialog(android.app.Activity p5, int p6)
    {
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.framework.prompt.PromptManagerImpl.LOGGER.trace(new StringBuilder().append("onCreateDialog, id: ").append(p6).append(", activity: ").append(p5).toString());
        }
        android.app.Dialog v0_8;
        if (this.showing != null) {
            if (this.showing.getIdentifier() == p6) {
                if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                    com.amazon.android.framework.prompt.PromptManagerImpl.LOGGER.trace(new StringBuilder().append("Creating dialog prompt: ").append(this.showing).toString());
                }
                v0_8 = this.showing.create(p5);
            } else {
                if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                    com.amazon.android.framework.prompt.PromptManagerImpl.LOGGER.trace(new StringBuilder().append("Showing dialog id does not match given id: ").append(p6).append(", returning").toString());
                }
                v0_8 = 0;
            }
        } else {
            if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                com.amazon.android.framework.prompt.PromptManagerImpl.LOGGER.trace("Showing dialog is null, returning");
            }
            v0_8 = 0;
        }
        return v0_8;
    }

    public void onResourcesPopulated()
    {
        this.registerActivityResumedListener();
        this.registerAppDestructionListener();
        this.registerTestModeListener();
        return;
    }

    public void onWindowFocusChanged(android.app.Activity p2, boolean p3)
    {
        if (this.showing != null) {
            this.showing.onFocusChanged(p2, p3);
        }
        return;
    }

    public void present(com.amazon.android.framework.prompt.Prompt p4)
    {
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.framework.prompt.PromptManagerImpl.LOGGER.trace(new StringBuilder().append("Scheduling presentation: ").append(p4).toString());
        }
        this.resourceManager.b(p4);
        if (!this.finished.get()) {
            this.taskManager.enqueue(com.amazon.android.framework.task.pipeline.TaskPipelineId.FOREGROUND, new com.amazon.android.framework.prompt.h(this, p4));
        } else {
            if (com.amazon.android.framework.util.KiwiLogger.ERROR_ON) {
                com.amazon.android.framework.prompt.PromptManagerImpl.LOGGER.error(new StringBuilder().append("Prompt: ").append(p4).append(" presented after app").append(" destruction expiring it now!").toString());
            }
            p4.expire();
        }
        return;
    }
}
