package com.amazon.android.framework.prompt;
public class PromptContent {
    private final String buttonLabel;
    private final String message;
    private final boolean shouldDownload;
    private final String title;
    private final boolean visible;

    public PromptContent(String p7, String p8, String p9, boolean p10)
    {
        this(p7, p8, p9, p10, 0);
        return;
    }

    public PromptContent(String p1, String p2, String p3, boolean p4, boolean p5)
    {
        this.title = p1;
        this.message = p2;
        this.buttonLabel = p3;
        this.visible = p4;
        this.shouldDownload = p5;
        return;
    }

    public String getButtonLabel()
    {
        return this.buttonLabel;
    }

    public String getMessage()
    {
        return this.message;
    }

    public String getTitle()
    {
        return this.title;
    }

    public boolean isVisible()
    {
        return this.visible;
    }

    public boolean shouldDownload()
    {
        return this.shouldDownload;
    }

    public String toString()
    {
        String v0_1 = new StringBuilder("PromptContent: [ title:");
        v0_1.append(this.title).append(", message: ").append(this.message).append(", label: ").append(this.buttonLabel).append(", visible: ").append(this.visible).append(", shouldDownload: ").append(this.shouldDownload).append("]");
        return v0_1.toString();
    }
}
