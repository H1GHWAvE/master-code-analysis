package com.amazon.android.framework.exception;
public abstract class KiwiException extends java.lang.Exception {
    private static final long serialVersionUID = 1;
    private final String context;
    private final String reason;
    private final String type;

    public KiwiException(String p2)
    {
        this(p2, 0);
        return;
    }

    public KiwiException(String p2, String p3)
    {
        this(p2, p3, 0);
        return;
    }

    public KiwiException(String p4, String p5, String p6)
    {
        this(new StringBuilder().append(p4).append(": ").append(p5).append(": ").append(p6).toString());
        this.type = p4;
        this.reason = p5;
        this.context = p6;
        return;
    }

    public KiwiException(String p2, String p3, Throwable p4)
    {
        this(p2, p3, com.amazon.android.framework.exception.KiwiException.getContext(p4));
        return;
    }

    public KiwiException(String p2, Throwable p3)
    {
        this(p2, com.amazon.android.framework.exception.KiwiException.getName(p3), p3);
        return;
    }

    private static String getContext(Throwable p5)
    {
        String v0_2;
        if (p5 != null) {
            String v0_0 = com.amazon.android.framework.exception.KiwiException.getRootCause(p5);
            StringBuilder v1_1 = new StringBuilder();
            v1_1.append(com.amazon.android.framework.exception.KiwiException.getName(p5)).append(":").append(p5.getMessage());
            if (p5 != v0_0) {
                v1_1.append("/").append(com.amazon.android.framework.exception.KiwiException.getName(v0_0)).append(":").append(v0_0.getMessage());
            }
            v0_2 = v1_1.toString();
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    private static String getName(Throwable p1)
    {
        return p1.getClass().getName();
    }

    private static Throwable getRootCause(Throwable p2)
    {
        Throwable v0 = p2;
        while (v0.getCause() != null) {
            v0 = v0.getCause();
        }
        return v0;
    }

    public final String getContext()
    {
        return this.context;
    }

    public final String getReason()
    {
        return this.reason;
    }

    public final String getType()
    {
        return this.type;
    }
}
