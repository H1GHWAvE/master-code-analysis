package com.amazon.android.framework.context;
public final class d implements com.amazon.android.framework.context.ContextManager, com.amazon.android.framework.resource.b {
    public static final com.amazon.android.framework.util.KiwiLogger a;
    private final java.util.concurrent.atomic.AtomicReference b;
    private final com.amazon.android.framework.util.c c;
    private final com.amazon.android.framework.util.c d;
    private final com.amazon.android.framework.util.c e;
    private final java.util.concurrent.atomic.AtomicBoolean f;
    private final java.util.concurrent.atomic.AtomicBoolean g;
    private final java.util.concurrent.atomic.AtomicBoolean h;
    private android.app.Application i;
    private com.amazon.android.framework.task.TaskManager j;
    private com.amazon.android.n.g k;
    private String l;
    private boolean m;

    static d()
    {
        com.amazon.android.framework.context.d.a = new com.amazon.android.framework.util.KiwiLogger("ContextManagerImpl");
        return;
    }

    public d()
    {
        this.b = new java.util.concurrent.atomic.AtomicReference();
        this.c = new com.amazon.android.framework.util.c();
        this.d = new com.amazon.android.framework.util.c();
        this.e = new com.amazon.android.framework.util.c();
        this.f = new java.util.concurrent.atomic.AtomicBoolean(0);
        this.g = new java.util.concurrent.atomic.AtomicBoolean(0);
        this.h = new java.util.concurrent.atomic.AtomicBoolean(0);
        this.m = 0;
        return;
    }

    static synthetic void a(com.amazon.android.framework.context.d p5)
    {
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.framework.context.d.a.trace("----------- EXECUTING FINISH ACTIVITIES -----------");
            com.amazon.android.framework.context.d.a.trace(p5.i.getPackageName());
            com.amazon.android.framework.context.d.a.trace("---------------------------------------------------");
        }
        com.amazon.android.framework.task.TaskManager v1_4 = p5.c.iterator();
        while (v1_4.hasNext()) {
            String v0_11 = ((android.app.Activity) v1_4.next());
            if ((!com.amazon.android.framework.context.d.a(v0_11)) && (v0_11 != null)) {
                if (!v0_11.isChild()) {
                    com.amazon.android.framework.context.d.a.trace(new StringBuilder().append("Finishing Activity: ").append(v0_11).toString());
                    v0_11.finish();
                } else {
                    com.amazon.android.framework.context.d.a.trace(new StringBuilder().append("Not finishing activity: ").append(v0_11).append(", it is a child of: ").append(v0_11.getParent()).toString());
                }
            }
        }
        String v0_6 = p5.getRoot();
        if (v0_6 != null) {
            com.amazon.android.framework.context.d.a.trace("Moving task to background");
            v0_6.moveTaskToBack(1);
            com.amazon.android.framework.context.d.a.trace(new StringBuilder().append("Popping activity stack, root: ").append(v0_6).toString());
            com.amazon.android.framework.task.TaskManager v1_9 = new android.content.Intent(v0_6, v0_6.getClass());
            v1_9.addFlags(67108864);
            v1_9.addFlags(536870912);
            v0_6.startActivity(v1_9);
        } else {
            com.amazon.android.framework.context.d.a.trace("Shutdown found no root, no activities to pop off stack!");
        }
        p5.j.enqueue(com.amazon.android.framework.task.pipeline.TaskPipelineId.FOREGROUND, new com.amazon.android.framework.context.a(p5));
        return;
    }

    static synthetic void a(com.amazon.android.framework.context.d p4, android.content.Intent p5)
    {
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.framework.context.d.a.trace(new StringBuilder().append("Received broadcast intent: ").append(p5).toString());
        }
        if (p5.getAction().equals(p4.l)) {
            if (p4.g.get()) {
                if (p4.h.compareAndSet(0, 1)) {
                    if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                        com.amazon.android.framework.context.d.a.trace("Stopping services in response to broadcast");
                        com.amazon.android.framework.context.d.a.trace(new StringBuilder().append("Service to stop: ").append(p4.d.a.size()).toString());
                    }
                    java.util.Iterator v0_12 = p4.d.iterator();
                    while (v0_12.hasNext()) {
                        android.app.Service v4_2 = ((android.app.Service) v0_12.next());
                        if (v4_2 != null) {
                            if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                                com.amazon.android.framework.context.d.a.trace(new StringBuilder().append("Stopping service: ").append(v4_2).toString());
                            }
                            v4_2.stopSelf();
                        }
                    }
                } else {
                    if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                        com.amazon.android.framework.context.d.a.trace("shutdown broadcast already received, ignoring");
                    }
                }
            } else {
                if (com.amazon.android.framework.util.KiwiLogger.ERROR_ON) {
                    com.amazon.android.framework.context.d.a.error(new StringBuilder().append("Received intent to shutdown app when we are not in shutdown state: ").append(p5).toString());
                }
            }
        } else {
            if (com.amazon.android.framework.util.KiwiLogger.ERROR_ON) {
                com.amazon.android.framework.context.d.a.error(new StringBuilder().append("Received broadcast for unrequested action: ").append(p5.getAction()).toString());
            }
        }
        return;
    }

    private void a(com.amazon.android.j.c p3, android.app.Activity p4)
    {
        this.k.a(new com.amazon.android.j.b(p3, p4));
        return;
    }

    private void a(com.amazon.android.j.d p3)
    {
        this.k.a(new com.amazon.android.j.a(p3, this.i));
        return;
    }

    private static boolean a(android.app.Activity p1)
    {
        boolean v0;
        if (p1 != null) {
            v0 = p1.isTaskRoot();
        } else {
            v0 = 0;
        }
        return v0;
    }

    private static android.app.Activity b(android.app.Activity p2)
    {
        android.app.Activity v0 = p2;
        while (v0.isChild()) {
            v0 = v0.getParent();
        }
        return v0;
    }

    public final void finishActivities()
    {
        if (this.f.compareAndSet(0, 1)) {
            if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                com.amazon.android.framework.context.d.a.trace("---------- SCHEDULING FINISH ACTIVITIES -----------");
                com.amazon.android.framework.context.d.a.trace(this.i.getPackageName());
                com.amazon.android.framework.context.d.a.trace(Thread.currentThread().toString());
                com.amazon.android.framework.context.d.a.trace("---------------------------------------------------");
            }
            this.j.enqueue(com.amazon.android.framework.task.pipeline.TaskPipelineId.FOREGROUND, new com.amazon.android.framework.context.c(this));
        }
        return;
    }

    public final android.app.Activity getRoot()
    {
        com.amazon.android.d.a.a();
        android.app.Activity v0_1 = this.c.iterator();
        while (v0_1.hasNext()) {
            android.app.Activity v2_2 = ((android.app.Activity) v0_1.next());
            if (com.amazon.android.framework.context.d.a(v2_2)) {
                android.app.Activity v0_2 = v2_2;
            }
            return v0_2;
        }
        v0_2 = 0;
        return v0_2;
    }

    public final android.app.Activity getVisible()
    {
        com.amazon.android.d.a.a();
        return ((android.app.Activity) this.b.get());
    }

    public final boolean hasAppShutdownBeenRequested()
    {
        if ((!this.g.get()) && (!this.f.get())) {
            int v0_4 = 0;
        } else {
            v0_4 = 1;
        }
        return v0_4;
    }

    public final boolean isVisible()
    {
        int v0_2;
        if (this.b.get() == null) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public final void onCreate(android.app.Activity p5)
    {
        com.amazon.android.d.a.a(p5, "activity");
        com.amazon.android.d.a.a();
        this.c.a(p5);
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.framework.context.d.a.trace(new StringBuilder().append("Activity->onCreate.  Activity: ").append(p5).append(", Total Activities: ").append(this.c.a.size()).toString());
        }
        com.amazon.android.j.d v0_9;
        this.a(com.amazon.android.j.c.a, p5);
        if ((this.m) || (this.c.a.size() != 1)) {
            v0_9 = 0;
        } else {
            v0_9 = 1;
        }
        if (v0_9 != null) {
            this.m = 1;
            this.a(com.amazon.android.j.d.a);
        }
        return;
    }

    public final void onCreate(android.app.Service p4)
    {
        com.amazon.android.d.a.a();
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.framework.context.d.a.trace(new StringBuilder().append("Service->onCreate: ").append(p4).toString());
        }
        this.d.a(p4);
        return;
    }

    public final void onDestroy(android.app.Activity p5)
    {
        com.amazon.android.d.a.a(p5, "activity");
        com.amazon.android.d.a.a();
        this.c.b(p5);
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.framework.context.d.a.trace(new StringBuilder().append("Activity->onDestroy.  Activity: ").append(p5).append(", Total Activities: ").append(this.c.a.size()).toString());
        }
        this.a(com.amazon.android.j.c.b, p5);
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.framework.context.d.a.trace("Checking if application is destroyed");
        }
        com.amazon.android.j.d v0_13;
        if (!this.c.a.isEmpty()) {
            v0_13 = 0;
        } else {
            com.amazon.android.framework.context.d.a.trace(new StringBuilder().append("App is destroyed: ").append(p5.isTaskRoot()).append(", ").append(p5.isFinishing()).toString());
            if ((!p5.isTaskRoot()) || (!p5.isFinishing())) {
            } else {
                v0_13 = 1;
            }
        }
        if (v0_13 != null) {
            this.m = 0;
            this.a(com.amazon.android.j.d.b);
        }
        return;
    }

    public final void onDestroy(android.app.Service p2)
    {
        com.amazon.android.d.a.a();
        this.d.b(p2);
        return;
    }

    public final void onPause(android.app.Activity p4)
    {
        com.amazon.android.d.a.a();
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.framework.context.d.a.trace(new StringBuilder().append("Activity paused: ").append(p4).append(", visible activity: ").append(this.getVisible()).toString());
        }
        if (this.getVisible() == p4) {
            if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                com.amazon.android.framework.context.d.a.trace("Setting visible activity to null");
            }
            this.b.set(0);
            this.a(com.amazon.android.j.c.d, p4);
        }
        return;
    }

    public final void onResourcesPopulated()
    {
        this.l = new StringBuilder().append("com.amazon.").append(this.i.getPackageName()).append(".shutdown").toString();
        this.i.registerReceiver(new com.amazon.android.framework.context.b(this), new android.content.IntentFilter(this.l));
        return;
    }

    public final void onResume(android.app.Activity p5)
    {
        com.amazon.android.d.a.a();
        com.amazon.android.framework.context.d.a.trace(new StringBuilder().append("Activity resumed: ").append(p5).append(", is child: ").append(p5.isChild()).toString());
        com.amazon.android.j.c v0_1 = com.amazon.android.framework.context.d.b(p5);
        com.amazon.android.framework.context.d.a.trace(new StringBuilder().append("Setting visible to: ").append(v0_1).toString());
        this.b.set(v0_1);
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.framework.context.d.a.trace(new StringBuilder().append("Activity now visible: ").append(p5).append(", ").append("publishing resume event").toString());
        }
        this.a(com.amazon.android.j.c.c, p5);
        return;
    }

    public final void onStart(android.app.Activity p4)
    {
        com.amazon.android.d.a.a(p4, "activity");
        com.amazon.android.d.a.a();
        com.amazon.android.framework.context.d.a.trace(new StringBuilder().append("Activity started: ").append(p4).toString());
        this.e.a(p4);
        this.a(com.amazon.android.j.c.e, p4);
        if (this.e.a.size() == 1) {
            this.a(com.amazon.android.j.d.c);
        }
        return;
    }

    public final void onStop(android.app.Activity p4)
    {
        com.amazon.android.d.a.a(p4, "activity");
        com.amazon.android.d.a.a();
        com.amazon.android.framework.context.d.a.trace(new StringBuilder().append("Activity stopped: ").append(p4).toString());
        this.e.b(p4);
        this.a(com.amazon.android.j.c.f, p4);
        if (this.e.a.isEmpty()) {
            this.a(com.amazon.android.j.d.d);
        }
        return;
    }

    public final void stopServices()
    {
        if ((!this.g.compareAndSet(0, 1)) && (com.amazon.android.framework.util.KiwiLogger.TRACE_ON)) {
            com.amazon.android.framework.context.d.a.trace("Ignoring duplicate stopServices request");
        }
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.framework.context.d.a.trace("------------- STOPPING SERVICES -------------------");
            com.amazon.android.framework.context.d.a.trace(this.i.getPackageName());
            com.amazon.android.framework.context.d.a.trace("---------------------------------------------------");
        }
        android.content.Intent v0_9 = new android.content.Intent(this.l);
        v0_9.setPackage(this.i.getPackageName());
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.framework.context.d.a.trace(new StringBuilder().append("Sending Broadcast!!!!: ").append(v0_9).append(", Thread: ").append(Thread.currentThread()).toString());
        }
        this.i.sendBroadcast(v0_9);
        return;
    }
}
