package com.amazon.android.framework.context;
final class a implements com.amazon.android.framework.task.Task {
    private synthetic com.amazon.android.framework.context.d a;

    a(com.amazon.android.framework.context.d p1)
    {
        this.a = p1;
        return;
    }

    public final void execute()
    {
        android.app.Activity v0_1 = this.a.getRoot();
        com.amazon.android.framework.context.d.a.trace(new StringBuilder().append("Finishing Root Task: ").append(v0_1).toString());
        if (v0_1 != null) {
            com.amazon.android.framework.context.d.a.trace("Finishing Root");
            v0_1.finish();
        }
        return;
    }

    public final String toString()
    {
        return "ContextManager: kill root task";
    }
}
