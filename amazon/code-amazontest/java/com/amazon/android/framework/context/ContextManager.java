package com.amazon.android.framework.context;
public interface ContextManager {

    public abstract void finishActivities();

    public abstract android.app.Activity getRoot();

    public abstract android.app.Activity getVisible();

    public abstract boolean hasAppShutdownBeenRequested();

    public abstract boolean isVisible();

    public abstract void onCreate();

    public abstract void onCreate();

    public abstract void onDestroy();

    public abstract void onDestroy();

    public abstract void onPause();

    public abstract void onResume();

    public abstract void onStart();

    public abstract void onStop();

    public abstract void stopServices();
}
