package com.amazon.android.framework.util;
public class KiwiLogger {
    public static boolean ERROR_ON = False;
    private static final String TAG = "Kiwi";
    private static boolean TEST_ON;
    public static boolean TRACE_ON;
    private String componentName;

    static KiwiLogger()
    {
        com.amazon.android.framework.util.KiwiLogger.TRACE_ON = 1;
        com.amazon.android.framework.util.KiwiLogger.ERROR_ON = 1;
        com.amazon.android.framework.util.KiwiLogger.TEST_ON = 0;
        return;
    }

    public KiwiLogger(String p1)
    {
        this.componentName = p1;
        return;
    }

    public static void enableTest()
    {
        com.amazon.android.framework.util.KiwiLogger.TEST_ON = 1;
        return;
    }

    private String getComponentMessage(String p3)
    {
        return new StringBuilder().append(this.componentName).append(": ").append(p3).toString();
    }

    public static boolean isTestEnabled()
    {
        return com.amazon.android.framework.util.KiwiLogger.TEST_ON;
    }

    public void error(String p3)
    {
        if (com.amazon.android.framework.util.KiwiLogger.ERROR_ON) {
            android.util.Log.e("Kiwi", this.getComponentMessage(p3));
        }
        return;
    }

    public void error(String p3, Throwable p4)
    {
        if (com.amazon.android.framework.util.KiwiLogger.ERROR_ON) {
            android.util.Log.e("Kiwi", this.getComponentMessage(p3), p4);
        }
        return;
    }

    public void test(String p4)
    {
        if (com.amazon.android.framework.util.KiwiLogger.TEST_ON) {
            android.util.Log.e("Kiwi", new StringBuilder().append("TEST-").append(this.getComponentMessage(p4)).toString());
        }
        return;
    }

    public void trace(String p3)
    {
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            android.util.Log.d("Kiwi", this.getComponentMessage(p3));
        }
        return;
    }

    public void trace(String p3, Throwable p4)
    {
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            android.util.Log.d("Kiwi", this.getComponentMessage(p3), p4);
        }
        return;
    }
}
