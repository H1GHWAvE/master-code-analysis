package com.amazon.android.framework.util;
public final class a {

    private a()
    {
        return;
    }

    public static final void a(java.io.InputStream p1)
    {
        if (p1 != null) {
            try {
                p1.close();
            } catch (java.io.IOException v0) {
            }
        }
        return;
    }

    public static final void a(java.io.OutputStream p1)
    {
        if (p1 != null) {
            try {
                p1.close();
            } catch (java.io.IOException v0) {
            }
        }
        return;
    }

    public static final void a(java.io.Reader p1)
    {
        if (p1 != null) {
            try {
                p1.close();
            } catch (java.io.IOException v0) {
            }
        }
        return;
    }
}
