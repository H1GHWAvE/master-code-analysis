package com.amazon.android.framework.util;
public final class c implements java.lang.Iterable {
    public java.util.WeakHashMap a;

    public c()
    {
        this.a = new java.util.WeakHashMap();
        return;
    }

    public final void a(Object p3)
    {
        this.a.put(p3, 0);
        return;
    }

    public final void b(Object p2)
    {
        this.a.remove(p2);
        return;
    }

    public final java.util.Iterator iterator()
    {
        return this.a.keySet().iterator();
    }

    public final String toString()
    {
        return this.a.keySet().toString();
    }
}
