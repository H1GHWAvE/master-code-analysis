package com.amazon.android.framework.util;
public final class d {
    private static final com.amazon.android.framework.util.KiwiLogger a;

    static d()
    {
        com.amazon.android.framework.util.d.a = new com.amazon.android.framework.util.KiwiLogger("Reflection");
        return;
    }

    public d()
    {
        return;
    }

    public static Class a(String p4)
    {
        com.amazon.android.d.a.a(p4, "String className");
        try {
            int v0_3 = Thread.currentThread().getContextClassLoader().loadClass(p4);
        } catch (int v0) {
            if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                com.amazon.android.framework.util.d.a.trace(new StringBuilder().append("ClassNoFound: ").append(p4).toString());
            }
            v0_3 = 0;
        }
        return v0_3;
    }

    public static boolean a(reflect.Method p1)
    {
        int v0_3;
        com.amazon.android.d.a.a(p1, "Method m");
        if (p1.getParameterTypes().length <= 0) {
            v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    public static boolean b(reflect.Method p2)
    {
        com.amazon.android.d.a.a(p2, "Method m");
        return p2.getReturnType().equals(Void.TYPE);
    }

    public static boolean c(reflect.Method p1)
    {
        int v0_3;
        com.amazon.android.d.a.a(p1, "Method m");
        if ((p1.getModifiers() & 8) == 0) {
            v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }
}
