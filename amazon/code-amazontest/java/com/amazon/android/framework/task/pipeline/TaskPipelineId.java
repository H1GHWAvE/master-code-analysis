package com.amazon.android.framework.task.pipeline;
public final enum class TaskPipelineId extends java.lang.Enum {
    private static final synthetic com.amazon.android.framework.task.pipeline.TaskPipelineId[] $VALUES;
    public static final enum com.amazon.android.framework.task.pipeline.TaskPipelineId BACKGROUND;
    public static final enum com.amazon.android.framework.task.pipeline.TaskPipelineId COMMAND;
    public static final enum com.amazon.android.framework.task.pipeline.TaskPipelineId FOREGROUND;

    static TaskPipelineId()
    {
        com.amazon.android.framework.task.pipeline.TaskPipelineId.FOREGROUND = new com.amazon.android.framework.task.pipeline.TaskPipelineId("FOREGROUND", 0);
        com.amazon.android.framework.task.pipeline.TaskPipelineId.COMMAND = new com.amazon.android.framework.task.pipeline.TaskPipelineId("COMMAND", 1);
        com.amazon.android.framework.task.pipeline.TaskPipelineId.BACKGROUND = new com.amazon.android.framework.task.pipeline.TaskPipelineId("BACKGROUND", 2);
        com.amazon.android.framework.task.pipeline.TaskPipelineId[] v0_7 = new com.amazon.android.framework.task.pipeline.TaskPipelineId[3];
        v0_7[0] = com.amazon.android.framework.task.pipeline.TaskPipelineId.FOREGROUND;
        v0_7[1] = com.amazon.android.framework.task.pipeline.TaskPipelineId.COMMAND;
        v0_7[2] = com.amazon.android.framework.task.pipeline.TaskPipelineId.BACKGROUND;
        com.amazon.android.framework.task.pipeline.TaskPipelineId.$VALUES = v0_7;
        return;
    }

    private TaskPipelineId(String p1, int p2)
    {
        this(p1, p2);
        return;
    }

    public static com.amazon.android.framework.task.pipeline.TaskPipelineId valueOf(String p1)
    {
        return ((com.amazon.android.framework.task.pipeline.TaskPipelineId) Enum.valueOf(com.amazon.android.framework.task.pipeline.TaskPipelineId, p1));
    }

    public static com.amazon.android.framework.task.pipeline.TaskPipelineId[] values()
    {
        return ((com.amazon.android.framework.task.pipeline.TaskPipelineId[]) com.amazon.android.framework.task.pipeline.TaskPipelineId.$VALUES.clone());
    }
}
