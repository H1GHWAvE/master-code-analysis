package com.amazon.android.framework.task.pipeline;
final class d implements java.lang.Runnable {
    private synthetic com.amazon.android.framework.task.Task a;
    private synthetic com.amazon.android.framework.task.pipeline.a b;

    d(com.amazon.android.framework.task.pipeline.a p1, com.amazon.android.framework.task.Task p2)
    {
        this.b = p1;
        this.a = p2;
        return;
    }

    public final void run()
    {
        com.amazon.android.framework.task.pipeline.a.a(this.b).remove(this);
        try {
            if (!com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                this.a.execute();
                if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                    com.amazon.android.framework.task.pipeline.a.b().trace(new StringBuilder().append(com.amazon.android.framework.task.pipeline.a.b(this.b)).append(": Task finished executing: ").append(this.a).toString());
                }
            } else {
                com.amazon.android.framework.task.pipeline.a.b().trace(new StringBuilder().append(com.amazon.android.framework.task.pipeline.a.b(this.b)).append(": Executing Task: ").append(this.a).append(", current time: ").append(new java.util.Date()).append(", uptime: ").append(android.os.SystemClock.uptimeMillis()).toString());
            }
        } catch (com.amazon.android.framework.util.KiwiLogger v0_4) {
            if (com.amazon.android.framework.util.KiwiLogger.ERROR_ON) {
                com.amazon.android.framework.task.pipeline.a.b().error(new StringBuilder().append("Task Failed with unhandled exception: ").append(v0_4).toString(), v0_4);
            }
            if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                com.amazon.android.framework.task.pipeline.a.b().trace(new StringBuilder().append(com.amazon.android.framework.task.pipeline.a.b(this.b)).append(": Task finished executing: ").append(this.a).toString());
            }
        } catch (com.amazon.android.framework.util.KiwiLogger v0_7) {
            if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                com.amazon.android.framework.task.pipeline.a.b().trace(new StringBuilder().append(com.amazon.android.framework.task.pipeline.a.b(this.b)).append(": Task finished executing: ").append(this.a).toString());
            }
            throw v0_7;
        }
        return;
    }

    public final String toString()
    {
        return this.a.toString();
    }
}
