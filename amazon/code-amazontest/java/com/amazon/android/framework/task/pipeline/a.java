package com.amazon.android.framework.task.pipeline;
public final class a implements com.amazon.android.framework.task.pipeline.f {
    private static final com.amazon.android.framework.util.KiwiLogger a;
    private final android.os.Handler b;
    private final java.util.Set c;
    private final String d;

    static a()
    {
        com.amazon.android.framework.task.pipeline.a.a = new com.amazon.android.framework.util.KiwiLogger("SimpleTaskPipeline");
        return;
    }

    private a(android.os.HandlerThread p3)
    {
        this.c = java.util.Collections.synchronizedSet(new java.util.HashSet());
        this.d = p3.getName();
        p3.start();
        this.b = new android.os.Handler(p3.getLooper());
        return;
    }

    private a(String p2)
    {
        this.c = java.util.Collections.synchronizedSet(new java.util.HashSet());
        this.d = p2;
        this.b = new android.os.Handler();
        return;
    }

    public static com.amazon.android.framework.task.pipeline.a a(String p3)
    {
        return new com.amazon.android.framework.task.pipeline.a(new android.os.HandlerThread(new StringBuilder().append("KIWI_").append(p3).toString()));
    }

    static synthetic java.util.Set a(com.amazon.android.framework.task.pipeline.a p1)
    {
        return p1.c;
    }

    public static com.amazon.android.framework.task.pipeline.a b(String p1)
    {
        return new com.amazon.android.framework.task.pipeline.a(p1);
    }

    static synthetic com.amazon.android.framework.util.KiwiLogger b()
    {
        return com.amazon.android.framework.task.pipeline.a.a;
    }

    static synthetic String b(com.amazon.android.framework.task.pipeline.a p1)
    {
        return p1.d;
    }

    private Runnable c(com.amazon.android.framework.task.Task p3)
    {
        com.amazon.android.framework.task.pipeline.d v0_1 = new com.amazon.android.framework.task.pipeline.d(this, p3);
        this.c.add(v0_1);
        return v0_1;
    }

    public final void a()
    {
        String v1_0 = this.c.iterator();
        while (v1_0.hasNext()) {
            android.os.Looper v0_17 = ((Runnable) v1_0.next());
            if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                com.amazon.android.framework.task.pipeline.a.a.trace(new StringBuilder().append(this.d).append(": Removing callback: ").append(v0_17).toString());
            }
            this.b.removeCallbacks(v0_17);
        }
        this.c.clear();
        if ((this.b.getLooper() != android.os.Looper.getMainLooper()) && (this.b.getLooper().getThread().isAlive())) {
            com.amazon.android.framework.task.pipeline.a.a.trace("Interrupting looper thread!");
            this.b.getLooper().getThread().interrupt();
            com.amazon.android.framework.task.pipeline.a.a.trace(new StringBuilder().append("Quitting looper: ").append(this.b.getLooper().getThread()).append(", ").append(this.b.getLooper().getThread().isAlive()).toString());
            this.b.getLooper().quit();
        }
        return;
    }

    public final void a(com.amazon.android.framework.task.Task p4)
    {
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.framework.task.pipeline.a.a.trace(new StringBuilder().append("Scheduling task: ").append(p4).toString());
        }
        this.b.post(this.c(p4));
        return;
    }

    public final void a(com.amazon.android.framework.task.Task p4, long p5)
    {
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.framework.task.pipeline.a.a.trace(new StringBuilder().append(this.d).append(": Scheduling task: ").append(p4).append(", with delay: ").append(p5).toString());
        }
        this.b.postDelayed(this.c(p4), p5);
        return;
    }

    public final void a(com.amazon.android.framework.task.Task p7, java.util.Date p8)
    {
        long v0_1 = (android.os.SystemClock.uptimeMillis() + (p8.getTime() - System.currentTimeMillis()));
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.framework.task.pipeline.a.a.trace(new StringBuilder().append(this.d).append(": Scheduling task: ").append(p7).append(", at time: ").append(p8).append(", System uptimeMillis: ").append(System.currentTimeMillis()).append(", uptimeMillis: ").append(v0_1).toString());
        }
        this.b.postAtTime(this.c(p7), v0_1);
        return;
    }

    public final void b(com.amazon.android.framework.task.Task p4)
    {
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.framework.task.pipeline.a.a.trace(new StringBuilder().append(this.d).append(": Scheduling task immediately: ").append(p4).toString());
        }
        this.b.postAtFrontOfQueue(this.c(p4));
        return;
    }
}
