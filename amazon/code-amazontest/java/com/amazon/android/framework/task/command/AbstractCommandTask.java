package com.amazon.android.framework.task.command;
public abstract class AbstractCommandTask extends com.amazon.android.l.c {
    private static final com.amazon.android.framework.util.KiwiLogger LOGGER;
    private android.app.Application application;
    private com.amazon.android.framework.task.command.e authTokenVerifier;
    private com.amazon.android.framework.task.command.b client;
    private com.amazon.android.q.d metricsManager;
    private com.amazon.android.framework.prompt.PromptManager promptManager;
    protected com.amazon.android.e.b resultManager;

    static AbstractCommandTask()
    {
        com.amazon.android.framework.task.command.AbstractCommandTask.LOGGER = new com.amazon.android.framework.util.KiwiLogger("AbstractCommandTask");
        return;
    }

    public AbstractCommandTask()
    {
        return;
    }

    static synthetic android.app.Application a(com.amazon.android.framework.task.command.AbstractCommandTask p1)
    {
        return p1.application;
    }

    private void expire(com.amazon.venezia.command.r p4, com.amazon.android.framework.task.command.a p5)
    {
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.framework.task.command.AbstractCommandTask.LOGGER.trace(new StringBuilder().append("Expiring Decision: ").append(p4).append(", reason: ").append(p5).toString());
        }
        this.handleCommandResult(this.client.a(p4, p5));
        return;
    }

    private com.amazon.android.q.b extractMetric(com.amazon.android.framework.exception.KiwiException p5)
    {
        com.amazon.android.q.b v0_1 = new com.amazon.android.q.b(this.getFailureMetricName());
        v0_1.a("subType", p5.getType()).a("reason", p5.getReason()).a("context", p5.getContext());
        return v0_1;
    }

    private com.amazon.venezia.command.w getCommand()
    {
        return new com.amazon.android.framework.task.command.g(this);
    }

    private String getFailureMetricName()
    {
        return new StringBuilder().append(this.getCommandName()).append("_failure").toString();
    }

    private void handleChoice(com.amazon.venezia.command.r p4, com.amazon.venezia.command.n p5)
    {
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.framework.task.command.AbstractCommandTask.LOGGER.trace(new StringBuilder().append("Handling customer choice: ").append(p5).toString());
        }
        com.amazon.android.framework.task.command.l v0_2 = p5.b();
        if (v0_2 == null) {
            if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                com.amazon.android.framework.task.command.AbstractCommandTask.LOGGER.trace("No intent given, choosing now");
            }
            this.handleCommandResult(this.client.a(p5));
        } else {
            if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                com.amazon.android.framework.task.command.AbstractCommandTask.LOGGER.trace("Choice has intent, scheduling it to be fired!!");
            }
            com.amazon.android.framework.task.command.l v0_7 = this.resultManager.a(v0_2);
            if (v0_7 != null) {
                if (v0_7.b != 0) {
                    if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                        com.amazon.android.framework.task.command.AbstractCommandTask.LOGGER.trace("Result received!!!, notifying service");
                    }
                    this.handleCommandResult(this.client.a(p5));
                } else {
                    if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                        com.amazon.android.framework.task.command.AbstractCommandTask.LOGGER.trace("Result canceled, expiring decision");
                    }
                    this.expire(p4, com.amazon.android.framework.task.command.a.c);
                }
            } else {
                if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                    com.amazon.android.framework.task.command.AbstractCommandTask.LOGGER.trace("No result recived, expiring decision");
                }
                this.expire(p4, com.amazon.android.framework.task.command.a.a);
            }
        }
        return;
    }

    private void handleCommandResult(com.amazon.android.framework.task.command.l p4)
    {
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.framework.task.command.AbstractCommandTask.LOGGER.trace(new StringBuilder().append("Received result from CommandService: ").append(p4).toString());
        }
        if (p4 != null) {
            if (p4.f == null) {
                if (p4.b == null) {
                    this.authTokenVerifier.a(p4.a, this.client.a());
                    if (p4.c == null) {
                        if (p4.d == null) {
                            this.handleDecision(p4.e);
                        } else {
                            this.handleFailure(p4.d);
                        }
                    } else {
                        this.handleSuccess(p4.c);
                    }
                } else {
                    throw p4.b;
                }
            } else {
                this.handleException(p4.f);
            }
        } else {
            if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                com.amazon.android.framework.task.command.AbstractCommandTask.LOGGER.trace("Received null result from command service, exiting task");
            }
        }
        return;
    }

    private void handleDecision(com.amazon.venezia.command.r p3)
    {
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.framework.task.command.AbstractCommandTask.LOGGER.trace("Handling Decision");
        }
        try {
            com.amazon.android.framework.task.command.a v0_3 = new com.amazon.android.framework.task.command.c(p3);
            this.promptManager.present(v0_3);
            com.amazon.android.framework.task.command.a v0_4 = v0_3.a();
        } catch (com.amazon.android.framework.task.command.a v0_8) {
            this.expire(p3, v0_8.a);
            return;
        }
        if (v0_4 != null) {
            this.handleChoice(p3, v0_4);
            return;
        } else {
            if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                com.amazon.android.framework.task.command.AbstractCommandTask.LOGGER.trace("DecisionChooser returned null!!, expiring");
            }
            this.expire(p3, com.amazon.android.framework.task.command.a.a);
            return;
        }
    }

    private void handleException(com.amazon.venezia.command.k p2)
    {
        throw new com.amazon.android.b.e(p2);
    }

    private void handleExecutionException(Throwable p4)
    {
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.framework.task.command.AbstractCommandTask.LOGGER.trace(new StringBuilder().append("Exception occurred while processing task: ").append(p4).toString(), p4);
        }
        com.amazon.android.q.b v0_2 = this.translate(p4);
        this.onException(v0_2);
        this.metricsManager.a(this.extractMetric(v0_2));
        return;
    }

    private void handleFailure(com.amazon.venezia.command.FailureResult p4)
    {
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.framework.task.command.AbstractCommandTask.LOGGER.trace(new StringBuilder().append("Command failed execution: ").append(p4.getDisplayableName()).toString());
        }
        this.onFailure(p4);
        return;
    }

    private void handleSuccess(com.amazon.venezia.command.SuccessResult p3)
    {
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.framework.task.command.AbstractCommandTask.LOGGER.trace("Command executed successfully");
        }
        this.onSuccess(p3);
        return;
    }

    private void postExecution()
    {
        if (!this.isWorkflowChild()) {
            this.client.b();
        }
        return;
    }

    private com.amazon.android.framework.exception.KiwiException translate(Throwable p2)
    {
        com.amazon.android.b.b v0_3;
        if (!(p2 instanceof com.amazon.android.framework.exception.KiwiException)) {
            if (!(p2 instanceof android.os.RemoteException)) {
                v0_3 = new com.amazon.android.b.b(p2);
            } else {
                v0_3 = new com.amazon.android.b.a(((android.os.RemoteException) p2));
            }
        } else {
            v0_3 = ((com.amazon.android.framework.exception.KiwiException) p2);
        }
        return v0_3;
    }

    public final void execute()
    {
        try {
            if (!com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                this.preExecution();
                if (this.isExecutionNeeded()) {
                    if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                        com.amazon.android.framework.task.command.AbstractCommandTask.LOGGER.trace(new StringBuilder().append("Executing Command: ").append(this.getCommandName()).toString());
                    }
                    this.handleCommandResult(this.client.a(this.getCommand()));
                    if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                        com.amazon.android.framework.task.command.AbstractCommandTask.LOGGER.trace("Task finished");
                    }
                    this.postExecution();
                } else {
                    if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                        com.amazon.android.framework.task.command.AbstractCommandTask.LOGGER.trace("Execution not needed, quitting");
                    }
                    if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                        com.amazon.android.framework.task.command.AbstractCommandTask.LOGGER.trace("Task finished");
                    }
                    this.postExecution();
                }
            } else {
                com.amazon.android.framework.task.command.AbstractCommandTask.LOGGER.trace("----------------------------------------------");
                com.amazon.android.framework.task.command.AbstractCommandTask.LOGGER.trace(new StringBuilder().append("Executing: ").append(this.getCommandName()).toString());
                com.amazon.android.framework.task.command.AbstractCommandTask.LOGGER.trace("----------------------------------------------");
            }
        } catch (com.amazon.android.framework.util.KiwiLogger v0_16) {
            if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                com.amazon.android.framework.task.command.AbstractCommandTask.LOGGER.trace("Task finished");
            }
            this.postExecution();
            throw v0_16;
        } catch (com.amazon.android.framework.util.KiwiLogger v0_13) {
            this.handleExecutionException(v0_13);
            if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                com.amazon.android.framework.task.command.AbstractCommandTask.LOGGER.trace("Task finished");
            }
            this.postExecution();
        }
        return;
    }

    protected abstract java.util.Map getCommandData();

    protected abstract String getCommandName();

    protected abstract String getCommandVersion();

    protected abstract boolean isExecutionNeeded();

    protected void onException(com.amazon.android.framework.exception.KiwiException p4)
    {
        com.amazon.android.framework.task.command.AbstractCommandTask.LOGGER.error(new StringBuilder().append("On Exception!!!!: ").append(p4).toString());
        return;
    }

    protected abstract void onFailure();

    protected abstract void onSuccess();

    protected void preExecution()
    {
        return;
    }
}
