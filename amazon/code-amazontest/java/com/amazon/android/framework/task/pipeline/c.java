package com.amazon.android.framework.task.pipeline;
final class c implements com.amazon.android.framework.task.Task {
    private synthetic com.amazon.android.framework.task.Task a;
    private synthetic com.amazon.android.framework.task.pipeline.e b;

    c(com.amazon.android.framework.task.pipeline.e p1, com.amazon.android.framework.task.Task p2)
    {
        this.b = p1;
        this.a = p2;
        return;
    }

    public final void execute()
    {
        com.amazon.android.framework.task.pipeline.e.a(this.b, this.a);
        return;
    }

    public final String toString()
    {
        return new StringBuilder().append("Future:PostToUITask: ").append(this.a.toString()).toString();
    }
}
