package com.amazon.android.framework.task.command;
public final class b {
    private static final com.amazon.android.framework.util.KiwiLogger a;
    private com.amazon.venezia.command.h b;
    private String c;
    private final java.util.concurrent.BlockingQueue d;
    private final java.util.concurrent.BlockingQueue e;
    private android.app.Application f;
    private com.amazon.android.framework.task.command.e g;
    private com.amazon.android.o.a h;
    private final android.content.ServiceConnection i;
    private final com.amazon.venezia.command.f j;

    static b()
    {
        com.amazon.android.framework.task.command.b.a = new com.amazon.android.framework.util.KiwiLogger("CommandServiceClient");
        return;
    }

    public b()
    {
        this.d = new java.util.concurrent.LinkedBlockingQueue();
        this.e = new java.util.concurrent.LinkedBlockingQueue();
        this.i = new com.amazon.android.framework.task.command.i(this);
        this.j = new com.amazon.android.framework.task.command.j(this);
        return;
    }

    private static android.content.Intent a(String p2)
    {
        android.content.Intent v0_1 = new android.content.Intent();
        v0_1.setAction("com.amazon.venezia.CommandService");
        v0_1.setClassName(p2, "com.amazon.venezia.service.command.CommandServiceImpl");
        return v0_1;
    }

    private android.content.Intent a(java.util.List p9)
    {
        StringBuilder v1_0 = -1;
        int v2_0 = 0;
        android.content.Intent v0_1 = 0;
        try {
            while (v2_0 < p9.size()) {
                android.content.Intent v0_3 = ((android.content.pm.ResolveInfo) p9.get(v2_0));
                String v3_3 = v0_3.serviceInfo.applicationInfo.packageName;
                com.amazon.android.framework.task.command.b.a.trace(new StringBuilder().append("Examining package ").append(v3_3).toString());
                com.amazon.android.framework.task.command.b.a.trace(new StringBuilder().append("Priority is ").append(v0_3.filter.getPriority()).toString());
                com.amazon.android.framework.task.command.b.a.trace(new StringBuilder().append("Checking signature of package ").append(v3_3).toString());
                com.amazon.android.framework.task.command.b.a.trace(new StringBuilder().append("isPackageSignatureTrusted ").append(v3_3).toString());
                if (!this.g.a(v3_3, com.amazon.android.framework.task.command.k.a)) {
                    com.amazon.android.framework.task.command.b.a.trace(new StringBuilder().append("Signature of package ").append(v3_3).append(" is bad").toString());
                    v2_0++;
                    v0_1 = v3_3;
                } else {
                    com.amazon.android.framework.task.command.b.a.trace(new StringBuilder().append("Signature of package ").append(v3_3).append(" is okay").toString());
                    v0_1 = v3_3;
                    v1_0 = v2_0;
                    break;
                }
                android.content.Intent v0_15 = 0;
                return v0_15;
            }
        } catch (android.content.Intent v0_16) {
            com.amazon.android.framework.task.command.b.a.trace(new StringBuilder().append("Caught exception ").append(v0_16).toString());
        }
        if (v1_0 >= null) {
            v0_15 = com.amazon.android.framework.task.command.b.a(v0_1);
            return v0_15;
        }
    }

    static synthetic java.util.concurrent.BlockingQueue a(com.amazon.android.framework.task.command.b p1)
    {
        return p1.e;
    }

    static synthetic java.util.concurrent.BlockingQueue b(com.amazon.android.framework.task.command.b p1)
    {
        return p1.d;
    }

    static synthetic com.amazon.android.framework.util.KiwiLogger c()
    {
        return com.amazon.android.framework.task.command.b.a;
    }

    private com.amazon.android.framework.task.command.l d()
    {
        try {
            com.amazon.android.framework.task.command.b.a.trace("Blocking for result from service");
            com.amazon.android.framework.task.command.l v2_2 = ((com.amazon.android.framework.task.command.l) this.d.take());
            com.amazon.android.framework.task.command.b.a.trace("Received result from service");
            int v0_3 = v2_2;
        } catch (int v0) {
            com.amazon.android.framework.task.command.b.a.trace("TaskThread interrupted, returning null result");
            v0_3 = 0;
        }
        return v0_3;
    }

    public final com.amazon.android.framework.task.command.l a(com.amazon.venezia.command.n p2)
    {
        p2.a(0);
        return this.d();
    }

    public final com.amazon.android.framework.task.command.l a(com.amazon.venezia.command.r p2, com.amazon.android.framework.task.command.a p3)
    {
        p2.a(new com.amazon.android.framework.task.command.h(this, p3));
        return this.d();
    }

    public final com.amazon.android.framework.task.command.l a(com.amazon.venezia.command.w p9)
    {
        com.amazon.android.o.a v0_1;
        if (this.b == null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        if (v0_1 == null) {
            String v3_6;
            String v1_0 = System.currentTimeMillis();
            com.amazon.android.framework.task.command.b.a.trace("Binding Service!!!");
            com.amazon.android.o.a v0_4 = new android.content.Intent();
            v0_4.setAction("com.amazon.venezia.CommandService");
            com.amazon.android.framework.task.command.b.a.trace("Created intent with  action  com.amazon.venezia.CommandService");
            if (this.f.getPackageManager().resolveService(v0_4, 64) == null) {
                v3_6 = 0;
            } else {
                v3_6 = 1;
            }
            if (v3_6 != null) {
                com.amazon.android.o.a v0_6;
                com.amazon.android.framework.task.command.b.a.trace("Found service on one or more packages");
                if (this.c == null) {
                    com.amazon.android.framework.task.command.b.a.trace("No previously determined package found, checking for suitable package.");
                    v0_6 = this.a(this.f.getPackageManager().queryIntentServices(v0_4, 64));
                    if (v0_6 == null) {
                        com.amazon.android.framework.task.command.b.a.trace("No app with valid signature was providing our service.");
                        throw new com.amazon.android.b.g();
                    }
                } else {
                    com.amazon.android.framework.task.command.b.a.trace(new StringBuilder().append("Using previously determined package ").append(this.c).toString());
                    v0_6 = com.amazon.android.framework.task.command.b.a(this.c);
                }
                String v3_18 = v0_6.getComponent().getPackageName();
                com.amazon.android.framework.task.command.b.a.trace(new StringBuilder().append("Attempting to bind to service on ").append(v3_18).toString());
                if (this.f.bindService(v0_6, this.i, 1)) {
                    try {
                        com.amazon.android.framework.task.command.b.a.trace("Blocking while service is being bound!!");
                        this.b = ((com.amazon.venezia.command.h) this.e.take());
                        com.amazon.android.framework.task.command.b.a.trace("service bound, returning!!");
                    } catch (com.amazon.android.o.a v0) {
                        Thread.currentThread().interrupt();
                        throw new com.amazon.android.b.f();
                    }
                    if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                        com.amazon.android.framework.task.command.b.a.trace(new StringBuilder().append("Kiwi.BindService Time: ").append((System.currentTimeMillis() - v1_0)).toString());
                    }
                    this.h.a("PACKAGE", v3_18);
                    this.c = v3_18;
                } else {
                    throw new com.amazon.android.b.f();
                }
            } else {
                throw new com.amazon.android.b.g();
            }
        }
        this.h.a("COMMAND", p9.a());
        this.b.a(p9, this.j);
        return this.d();
    }

    public final String a()
    {
        return this.c;
    }

    public final void b()
    {
        int v2_2;
        android.content.ServiceConnection v1_2 = new StringBuilder().append("Finishing CommandServiceClient, unbinding service: ");
        if (this.b == null) {
            v2_2 = 0;
        } else {
            v2_2 = 1;
        }
        com.amazon.android.framework.task.command.b.a.trace(v1_2.append(v2_2).toString());
        if (this.b != null) {
            this.f.unbindService(this.i);
            this.b = 0;
        }
        return;
    }
}
