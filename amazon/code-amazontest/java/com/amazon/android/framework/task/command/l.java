package com.amazon.android.framework.task.command;
public final class l {
    String a;
    android.os.RemoteException b;
    com.amazon.venezia.command.SuccessResult c;
    com.amazon.venezia.command.FailureResult d;
    com.amazon.venezia.command.r e;
    com.amazon.venezia.command.k f;
    private int g;

    public l(com.amazon.venezia.command.FailureResult p2)
    {
        this.d = p2;
        this.g = android.os.Binder.getCallingUid();
        try {
            this.a = p2.getAuthToken();
        } catch (android.os.RemoteException v0_2) {
            this.b = v0_2;
        }
        return;
    }

    public l(com.amazon.venezia.command.SuccessResult p2)
    {
        this.c = p2;
        this.g = android.os.Binder.getCallingUid();
        try {
            this.a = p2.getAuthToken();
        } catch (android.os.RemoteException v0_2) {
            this.b = v0_2;
        }
        return;
    }

    public l(com.amazon.venezia.command.k p2)
    {
        this.f = p2;
        this.g = android.os.Binder.getCallingUid();
        return;
    }

    public l(com.amazon.venezia.command.r p2)
    {
        this.e = p2;
        this.g = android.os.Binder.getCallingUid();
        try {
            this.a = p2.a();
        } catch (android.os.RemoteException v0_2) {
            this.b = v0_2;
        }
        return;
    }

    public final String toString()
    {
        String v0_1 = new StringBuilder("CommandResult: [");
        v0_1.append("CallingUid: ").append(this.g).append(", SuccessResult: ").append(this.c).append(", FailureResult: ").append(this.d).append(", DecisionResult: ").append(this.e).append(", ExceptionResult: ").append(this.f).append("]");
        return v0_1.toString();
    }
}
