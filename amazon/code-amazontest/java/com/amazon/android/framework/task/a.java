package com.amazon.android.framework.task;
public final class a implements com.amazon.android.framework.resource.b, com.amazon.android.framework.task.TaskManager {
    private static final com.amazon.android.framework.util.KiwiLogger a;
    private com.amazon.android.framework.resource.a b;
    private com.amazon.android.n.g c;
    private final java.util.concurrent.atomic.AtomicBoolean d;
    private final java.util.Map e;

    static a()
    {
        com.amazon.android.framework.task.a.a = new com.amazon.android.framework.util.KiwiLogger("TaskManagerImpl");
        return;
    }

    public a()
    {
        this.d = new java.util.concurrent.atomic.AtomicBoolean(0);
        this.e = new java.util.HashMap();
        java.util.Map v0_6 = com.amazon.android.framework.task.pipeline.a.a(com.amazon.android.framework.task.pipeline.TaskPipelineId.COMMAND.name());
        com.amazon.android.framework.task.pipeline.TaskPipelineId v1_3 = com.amazon.android.framework.task.pipeline.a.a(com.amazon.android.framework.task.pipeline.TaskPipelineId.BACKGROUND.name());
        com.amazon.android.framework.task.pipeline.e v2_1 = new com.amazon.android.framework.task.pipeline.e(v1_3);
        this.e.put(com.amazon.android.framework.task.pipeline.TaskPipelineId.COMMAND, v0_6);
        this.e.put(com.amazon.android.framework.task.pipeline.TaskPipelineId.BACKGROUND, v1_3);
        this.e.put(com.amazon.android.framework.task.pipeline.TaskPipelineId.FOREGROUND, v2_1);
        return;
    }

    private void a(com.amazon.android.framework.task.pipeline.TaskPipelineId p4, com.amazon.android.framework.task.Task p5, com.amazon.android.framework.task.b p6)
    {
        if (!this.d.get()) {
            if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                com.amazon.android.framework.task.a.a.trace(new StringBuilder().append("Populating Task: ").append(p5).toString());
            }
            this.b.b(p5);
            com.amazon.android.framework.task.pipeline.f v3_2 = ((com.amazon.android.framework.task.pipeline.f) this.e.get(p4));
            if (v3_2 != null) {
                p6.a(p5, v3_2);
            } else {
                throw new IllegalStateException(new StringBuilder().append("No pipeline registered with id: ").append(p4).toString());
            }
        } else {
            if (com.amazon.android.framework.util.KiwiLogger.ERROR_ON) {
                com.amazon.android.framework.task.a.a.error(new StringBuilder().append("Task enqueued after TaskManager has been finished! Task: ").append(p5).toString());
            }
        }
        return;
    }

    public final void a()
    {
        if (this.d.compareAndSet(0, 1)) {
            if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                com.amazon.android.framework.task.a.a.trace("TaskManager finishing....");
            }
            java.util.Iterator v0_6 = this.e.values().iterator();
            while (v0_6.hasNext()) {
                ((com.amazon.android.framework.task.pipeline.f) v0_6.next()).a();
            }
        }
        return;
    }

    public final void enqueue(com.amazon.android.framework.task.pipeline.TaskPipelineId p4, com.amazon.android.framework.task.Task p5)
    {
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.framework.task.a.a.trace(new StringBuilder().append("Enqueue task on pipeline id: ").append(p4).toString());
        }
        this.a(p4, p5, new com.amazon.android.framework.task.c(this));
        return;
    }

    public final void enqueueAfterDelay(com.amazon.android.framework.task.pipeline.TaskPipelineId p2, com.amazon.android.framework.task.Task p3, long p4)
    {
        this.a(p2, p3, new com.amazon.android.framework.task.f(this, p4));
        return;
    }

    public final void enqueueAtFront(com.amazon.android.framework.task.pipeline.TaskPipelineId p2, com.amazon.android.framework.task.Task p3)
    {
        this.a(p2, p3, new com.amazon.android.framework.task.d(this));
        return;
    }

    public final void enqueueAtTime(com.amazon.android.framework.task.pipeline.TaskPipelineId p2, com.amazon.android.framework.task.Task p3, java.util.Date p4)
    {
        this.a(p2, p3, new com.amazon.android.framework.task.e(this, p4));
        return;
    }

    public final void onResourcesPopulated()
    {
        com.amazon.android.n.g v1_0 = this.e.values().iterator();
        while (v1_0.hasNext()) {
            this.b.b(((com.amazon.android.framework.task.pipeline.f) v1_0.next()));
        }
        this.c.a(new com.amazon.android.framework.task.g(this));
        return;
    }
}
