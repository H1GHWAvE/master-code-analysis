package com.amazon.android.framework.task.command;
public final enum class a extends java.lang.Enum {
    public static final enum com.amazon.android.framework.task.command.a a;
    public static final enum com.amazon.android.framework.task.command.a b;
    public static final enum com.amazon.android.framework.task.command.a c;
    private static final synthetic com.amazon.android.framework.task.command.a[] d;

    static a()
    {
        com.amazon.android.framework.task.command.a.a = new com.amazon.android.framework.task.command.a("EXPIRATION_DURATION_ELAPSED", 0);
        com.amazon.android.framework.task.command.a.b = new com.amazon.android.framework.task.command.a("APP_NOT_COMPATIBLE", 1);
        com.amazon.android.framework.task.command.a.c = new com.amazon.android.framework.task.command.a("ACTION_CANCELED", 2);
        com.amazon.android.framework.task.command.a[] v0_7 = new com.amazon.android.framework.task.command.a[3];
        v0_7[0] = com.amazon.android.framework.task.command.a.a;
        v0_7[1] = com.amazon.android.framework.task.command.a.b;
        v0_7[2] = com.amazon.android.framework.task.command.a.c;
        com.amazon.android.framework.task.command.a.d = v0_7;
        return;
    }

    private a(String p1, int p2)
    {
        this(p1, p2);
        return;
    }

    public static com.amazon.android.framework.task.command.a valueOf(String p1)
    {
        return ((com.amazon.android.framework.task.command.a) Enum.valueOf(com.amazon.android.framework.task.command.a, p1));
    }

    public static com.amazon.android.framework.task.command.a[] values()
    {
        return ((com.amazon.android.framework.task.command.a[]) com.amazon.android.framework.task.command.a.d.clone());
    }
}
