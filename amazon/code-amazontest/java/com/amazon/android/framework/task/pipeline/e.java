package com.amazon.android.framework.task.pipeline;
public final class e implements com.amazon.android.framework.resource.b, com.amazon.android.framework.task.pipeline.f {
    private static final com.amazon.android.framework.util.KiwiLogger a;
    private com.amazon.android.framework.context.ContextManager b;
    private com.amazon.android.n.g c;
    private com.amazon.android.framework.task.pipeline.f d;
    private com.amazon.android.framework.task.pipeline.f e;
    private java.util.List f;

    static e()
    {
        com.amazon.android.framework.task.pipeline.e.a = new com.amazon.android.framework.util.KiwiLogger("ForegroundTaskPipeline");
        return;
    }

    public e(com.amazon.android.framework.task.pipeline.f p2)
    {
        this.f = new java.util.ArrayList();
        this.d = com.amazon.android.framework.task.pipeline.a.b("KIWI_UI");
        this.e = p2;
        return;
    }

    private void a(com.amazon.android.framework.task.Task p4, boolean p5)
    {
        if (!this.b.isVisible()) {
            if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                com.amazon.android.framework.task.pipeline.e.a.trace(new StringBuilder().append("No UI visible to execute task: ").append(p4).append(", placing into pending queue until task ").append("is visible").toString());
            }
            this.f.add(p4);
        } else {
            if (!p5) {
                this.d.a(p4);
            } else {
                this.d.b(p4);
            }
        }
        return;
    }

    static synthetic void a(com.amazon.android.framework.task.pipeline.e p3)
    {
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.framework.task.pipeline.e.a.trace("Activity resumed, scheduling tasks on UI thread");
        }
        java.util.Iterator v1_1 = p3.f.iterator();
        while (v1_1.hasNext()) {
            p3 = p3.a(((com.amazon.android.framework.task.Task) v1_1.next()), 1);
        }
        p3.f.clear();
        return;
    }

    static synthetic void a(com.amazon.android.framework.task.pipeline.e p1, com.amazon.android.framework.task.Task p2)
    {
        p1.a(p2, 1);
        return;
    }

    private com.amazon.android.framework.task.Task c(com.amazon.android.framework.task.Task p2)
    {
        return new com.amazon.android.framework.task.pipeline.c(this, p2);
    }

    public final void a()
    {
        this.d.a();
        this.e.a();
        this.f.clear();
        return;
    }

    public final void a(com.amazon.android.framework.task.Task p2)
    {
        this.a(p2, 0);
        return;
    }

    public final void a(com.amazon.android.framework.task.Task p3, long p4)
    {
        this.e.a(this.c(p3), p4);
        return;
    }

    public final void a(com.amazon.android.framework.task.Task p3, java.util.Date p4)
    {
        this.e.a(this.c(p3), p4);
        return;
    }

    public final void b(com.amazon.android.framework.task.Task p2)
    {
        this.a(p2, 1);
        return;
    }

    public final void onResourcesPopulated()
    {
        this.c.a(new com.amazon.android.framework.task.pipeline.b(this));
        return;
    }
}
