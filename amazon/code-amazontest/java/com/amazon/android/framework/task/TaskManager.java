package com.amazon.android.framework.task;
public interface TaskManager {

    public abstract void enqueue();

    public abstract void enqueueAfterDelay();

    public abstract void enqueueAtFront();

    public abstract void enqueueAtTime();
}
