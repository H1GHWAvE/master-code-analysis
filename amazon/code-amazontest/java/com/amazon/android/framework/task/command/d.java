package com.amazon.android.framework.task.command;
final class d implements android.content.DialogInterface$OnClickListener {
    private synthetic com.amazon.android.framework.task.command.m a;
    private synthetic com.amazon.android.framework.task.command.c b;

    d(com.amazon.android.framework.task.command.c p1, com.amazon.android.framework.task.command.m p2)
    {
        this.b = p1;
        this.a = p2;
        return;
    }

    public final void onClick(android.content.DialogInterface p3, int p4)
    {
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.framework.task.command.c.b().trace("Choice selected!");
        }
        if (com.amazon.android.framework.task.command.c.a(this.b)) {
            com.amazon.android.framework.task.command.c.b(this.b).add(this.a);
        }
        return;
    }
}
