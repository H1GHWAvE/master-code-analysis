package com.amazon.android.framework.task.command;
final class g extends com.amazon.venezia.command.z {
    private synthetic com.amazon.android.framework.task.command.AbstractCommandTask a;

    g(com.amazon.android.framework.task.command.AbstractCommandTask p1)
    {
        this.a = p1;
        return;
    }

    public final String a()
    {
        return this.a.getCommandName();
    }

    public final String b()
    {
        return this.a.getCommandVersion();
    }

    public final String c()
    {
        return com.amazon.android.framework.task.command.AbstractCommandTask.a(this.a).getPackageName();
    }

    public final java.util.Map d()
    {
        return this.a.getCommandData();
    }
}
