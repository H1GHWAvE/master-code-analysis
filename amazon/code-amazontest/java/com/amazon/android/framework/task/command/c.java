package com.amazon.android.framework.task.command;
public final class c extends com.amazon.android.framework.prompt.Prompt {
    private static final com.amazon.android.framework.util.KiwiLogger a;
    private final Thread b;
    private final com.amazon.android.framework.task.command.f c;
    private final java.util.concurrent.BlockingQueue d;

    static c()
    {
        com.amazon.android.framework.task.command.c.a = new com.amazon.android.framework.util.KiwiLogger("DecisionDialog");
        return;
    }

    public c(com.amazon.venezia.command.r p2)
    {
        this.d = new java.util.concurrent.LinkedBlockingQueue();
        this.b = Thread.currentThread();
        this.c = new com.amazon.android.framework.task.command.f(p2);
        return;
    }

    private static android.content.pm.ActivityInfo a(android.app.Activity p3)
    {
        try {
            int v0_1 = p3.getPackageManager().getActivityInfo(p3.getComponentName(), 128);
        } catch (int v0) {
            if (com.amazon.android.framework.util.KiwiLogger.ERROR_ON) {
                com.amazon.android.framework.task.command.c.a.error(new StringBuilder().append("Unable to get info for activity: ").append(p3).toString());
            }
            v0_1 = 0;
        }
        return v0_1;
    }

    private void a(android.app.AlertDialog p3, com.amazon.android.framework.task.command.m p4, int p5)
    {
        if (p4 != null) {
            p3.setButton(p5, p4.b, new com.amazon.android.framework.task.command.d(this, p4));
        }
        return;
    }

    static synthetic boolean a(com.amazon.android.framework.task.command.c p1)
    {
        return p1.dismiss();
    }

    private static boolean a(com.amazon.android.framework.task.command.m p1)
    {
        if ((p1 == null) || (p1.c == null)) {
            int v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    static synthetic com.amazon.android.framework.util.KiwiLogger b()
    {
        return com.amazon.android.framework.task.command.c.a;
    }

    static synthetic java.util.concurrent.BlockingQueue b(com.amazon.android.framework.task.command.c p1)
    {
        return p1.d;
    }

    private com.amazon.venezia.command.n c()
    {
        try {
            if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                com.amazon.android.framework.task.command.c.a.trace(new StringBuilder().append("Blocking while awaiting customer decision: ").append(Thread.currentThread()).toString());
            }
        } catch (com.amazon.android.b.c v0) {
            if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                com.amazon.android.framework.task.command.c.a.trace("Interrupted while awaiting decision, throwing decision expired!");
            }
            if (this.getExpirationReason() != com.amazon.android.framework.prompt.d.a) {
                com.amazon.android.framework.task.command.a v1_7 = com.amazon.android.framework.task.command.a.a;
            } else {
                v1_7 = com.amazon.android.framework.task.command.a.b;
            }
            throw new com.amazon.android.b.c(v1_7);
        }
        return ((com.amazon.android.framework.task.command.m) this.d.take()).a;
    }

    public final com.amazon.venezia.command.n a()
    {
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.framework.task.command.c.a.trace(new StringBuilder().append("GetCustomerDecision: ").append(this.b).toString());
        }
        return this.c();
    }

    public final boolean doCompatibilityCheck(android.app.Activity p9)
    {
        if ((!com.amazon.android.framework.task.command.c.a(this.c.d)) && ((!com.amazon.android.framework.task.command.c.a(this.c.e)) && (!com.amazon.android.framework.task.command.c.a(this.c.f)))) {
            int v0_9 = 0;
        } else {
            v0_9 = 1;
        }
        int v0_14;
        if (v0_9 != 0) {
            int v0_10 = com.amazon.android.framework.task.command.c.a(p9);
            if (v0_10 != 0) {
                int v1_1;
                if (v0_10.launchMode != 3) {
                    v1_1 = 0;
                } else {
                    v1_1 = 1;
                }
                int v2_4;
                com.amazon.android.framework.task.command.c.a.trace(new StringBuilder().append("Single instance: ").append(v1_1).toString());
                if ((v0_10.flags & 2) == 0) {
                    v2_4 = 0;
                } else {
                    v2_4 = 1;
                }
                int v0_13;
                com.amazon.android.framework.task.command.c.a.trace(new StringBuilder().append("Finish on task launch:").append(v2_4).toString());
                if ((v0_10.flags & 128) == 0) {
                    v0_13 = 0;
                } else {
                    v0_13 = 1;
                }
                com.amazon.android.framework.task.command.c.a.trace(new StringBuilder().append("No History: ").append(v0_13).toString());
                if ((v1_1 != 0) || ((v2_4 != 0) || (v0_13 != 0))) {
                    v0_14 = 0;
                } else {
                    v0_14 = 1;
                }
            } else {
                v0_14 = 0;
            }
        } else {
            v0_14 = 1;
        }
        return v0_14;
    }

    public final android.app.Dialog doCreate(android.app.Activity p4)
    {
        android.app.AlertDialog v0_1 = new android.app.AlertDialog$Builder(p4);
        v0_1.setTitle(this.c.a).setMessage(this.c.b).setCancelable(0);
        android.app.AlertDialog v0_2 = v0_1.create();
        this.a(v0_2, this.c.d, -1);
        this.a(v0_2, this.c.e, -3);
        this.a(v0_2, this.c.f, -2);
        return v0_2;
    }

    protected final void doExpiration(com.amazon.android.framework.prompt.d p4)
    {
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.framework.task.command.c.a.trace(new StringBuilder().append("Expiring Decision Dialog: Thread: ").append(Thread.currentThread()).toString());
        }
        this.b.interrupt();
        return;
    }

    protected final long getExpirationDurationInSeconds()
    {
        return this.c.c;
    }

    public final String toString()
    {
        return new StringBuilder().append("DecisionDialog: ").append(this.c.a).toString();
    }
}
