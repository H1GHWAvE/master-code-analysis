package com.amazon.android.framework.task.command;
public final class e {
    private static final com.amazon.android.framework.util.KiwiLogger a;
    private android.app.Application b;
    private com.amazon.android.m.c c;

    static e()
    {
        com.amazon.android.framework.task.command.e.a = new com.amazon.android.framework.util.KiwiLogger("CommandResultVerifier");
        return;
    }

    public e()
    {
        return;
    }

    private android.content.pm.PackageInfo a(String p5)
    {
        try {
            return this.b.getPackageManager().getPackageInfo(p5, 64);
        } catch (com.amazon.android.b.g v0_3) {
            com.amazon.android.framework.task.command.e.a.trace(new StringBuilder().append("getPackageInfo() caught exception").append(v0_3).toString());
            throw new com.amazon.android.b.g();
        }
    }

    private static String a(android.content.pm.Signature p2)
    {
        return com.amazon.mas.kiwi.util.Base64.encodeBytes(((java.security.cert.X509Certificate) java.security.cert.CertificateFactory.getInstance("X509").generateCertificate(new java.io.ByteArrayInputStream(p2.toByteArray()))).getSignature());
    }

    private boolean a(String p4, android.content.pm.Signature p5)
    {
        try {
            int v0_1 = com.amazon.android.m.a.a(com.amazon.android.framework.task.command.e.a(p5), p4, this.c.a());
        } catch (int v0) {
            if (com.amazon.android.framework.util.KiwiLogger.ERROR_ON) {
                com.amazon.android.framework.task.command.e.a.error(new StringBuilder().append("Failed to extract fingerprint from signature: ").append(p5).toString());
            }
            v0_1 = 0;
        }
        return v0_1;
    }

    private static boolean b(String p4, android.content.pm.Signature p5)
    {
        try {
            boolean v0 = p4.equals(com.amazon.android.framework.task.command.e.a(p5));
            com.amazon.android.framework.task.command.e.a.trace(new StringBuilder().append("Signature valid: ").append(v0).toString());
        } catch (com.amazon.android.framework.util.KiwiLogger v1) {
            com.amazon.android.framework.task.command.e.a.error("Failed to extract fingerprint from signature");
        }
        return v0;
    }

    public final void a(String p5, String p6)
    {
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.framework.task.command.e.a.trace(new StringBuilder().append("Verifying auth token: ").append(p5).toString());
        }
        com.amazon.android.b.d v0_3 = this.a(p6).signatures;
        int v2_1 = 0;
        while (v2_1 < v0_3.length) {
            if (!this.a(p5, v0_3[v2_1])) {
                v2_1++;
            } else {
                return;
            }
        }
        throw new com.amazon.android.b.d();
    }

    public final boolean a(String p8, java.util.List p9)
    {
        com.amazon.android.framework.task.command.e.a.trace(new StringBuilder().append("checkSignatures(").append(p8).append(", ").append(p9).toString());
        try {
            int v0_4;
            int v0_2 = this.a(p8).signatures;
            com.amazon.android.framework.util.KiwiLogger v1_7 = v0_2.length;
            int v2_2 = 0;
        } catch (int v0_3) {
            com.amazon.android.framework.task.command.e.a.error("isPackageSignatureValid: caught exception while checking", v0_3);
            v0_4 = 0;
            return v0_4;
        }
        while (v2_2 < v1_7) {
            int v3 = v0_2[v2_2];
            java.util.Iterator v4 = p9.iterator();
            while (v4.hasNext()) {
                if (com.amazon.android.framework.task.command.e.b(((String) v4.next()), v3)) {
                    v0_4 = 1;
                    return v0_4;
                }
            }
            v2_2++;
        }
    }
}
