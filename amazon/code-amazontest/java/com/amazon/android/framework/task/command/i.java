package com.amazon.android.framework.task.command;
final class i implements android.content.ServiceConnection {
    private synthetic com.amazon.android.framework.task.command.b a;

    i(com.amazon.android.framework.task.command.b p1)
    {
        this.a = p1;
        return;
    }

    public final void onServiceConnected(android.content.ComponentName p3, android.os.IBinder p4)
    {
        com.amazon.android.framework.task.command.b.c().trace("onServiceConnected");
        com.amazon.android.framework.task.command.b.a(this.a).add(com.amazon.venezia.command.q.a(p4));
        return;
    }

    public final void onServiceDisconnected(android.content.ComponentName p3)
    {
        com.amazon.android.framework.task.command.b.c().trace("onServiceDisconnected!!!");
        return;
    }
}
