package com.amazon.android.framework.resource;
public final class c implements com.amazon.android.framework.resource.a {
    private static com.amazon.android.framework.util.KiwiLogger a;
    private java.util.List b;
    private boolean c;

    static c()
    {
        com.amazon.android.framework.resource.c.a = new com.amazon.android.framework.util.KiwiLogger("ResourceManagerImpl");
        return;
    }

    public c()
    {
        this.b = new java.util.ArrayList();
        this.c = 0;
        this.a(this);
        return;
    }

    private Object a(Class p4)
    {
        Object v0_1 = this.b.iterator();
        while (v0_1.hasNext()) {
            Object v1_1 = v0_1.next();
            if (p4.isAssignableFrom(v1_1.getClass())) {
                Object v0_2 = v1_1;
            }
            return v0_2;
        }
        v0_2 = 0;
        return v0_2;
    }

    public final void a()
    {
        java.util.Iterator v0_1 = this.b.iterator();
        while (v0_1.hasNext()) {
            this.b(v0_1.next());
        }
        return;
    }

    public final void a(Object p4)
    {
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.framework.resource.c.a.trace(new StringBuilder().append("Registering resource: ").append(p4).toString());
        }
        com.amazon.android.d.a.a();
        if (!this.c) {
            java.util.List v0_6;
            com.amazon.android.d.a.a(p4, "resource");
            if (this.a(p4.getClass()) == null) {
                v0_6 = 0;
            } else {
                v0_6 = 1;
            }
            com.amazon.android.d.a.b(v0_6, new StringBuilder().append("Resource already registered: ").append(p4).toString());
            this.b.add(p4);
            return;
        } else {
            throw new IllegalStateException("Attempt made to register resource after population has begun!");
        }
    }

    public final void b(Object p12)
    {
        com.amazon.android.d.a.a(p12, "target");
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.framework.resource.c.a.trace(new StringBuilder().append("Populating: ").append(p12).toString());
        }
        if (!p12.getClass().getName().startsWith("com.amazon.android")) {
            com.amazon.android.framework.resource.c.a.trace(new StringBuilder().append("Ignoring: ").append(p12).append(", not a kiwi class").toString());
        }
        Exception v0_7 = p12.getClass();
        while (v0_7 != Object) {
            String v1_13 = v0_7.getDeclaredFields();
            com.amazon.android.d.b v2_3 = v1_13.length;
            int v3 = 0;
            while (v3 < v2_3) {
                String v5_2;
                reflect.Field v4 = v1_13[v3];
                if (v4.getAnnotation(com.amazon.android.framework.resource.Resource) == null) {
                    v5_2 = 0;
                } else {
                    v5_2 = 1;
                }
                if (v5_2 != null) {
                    int v7_3;
                    String v5_3 = v4.getType();
                    Object v6 = this.a(v5_3);
                    String v5_5 = new StringBuilder().append("no resource found for type: ").append(v5_3).toString();
                    if (v6 == null) {
                        v7_3 = 0;
                    } else {
                        v7_3 = 1;
                    }
                    com.amazon.android.d.a.a(v7_3, v5_5);
                    v4.setAccessible(1);
                    try {
                        v4.set(p12, v6);
                    } catch (Exception v0_9) {
                        throw new com.amazon.android.d.b(new StringBuilder().append("Failed to populate field: ").append(v4).toString(), v0_9);
                    }
                }
                v3++;
            }
            v0_7 = v0_7.getSuperclass();
        }
        if ((p12 instanceof com.amazon.android.framework.resource.b)) {
            ((com.amazon.android.framework.resource.b) p12).onResourcesPopulated();
        }
        return;
    }
}
