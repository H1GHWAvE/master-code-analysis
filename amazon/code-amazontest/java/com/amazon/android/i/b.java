package com.amazon.android.i;
public abstract class b implements com.amazon.android.framework.resource.b {
    private static final com.amazon.android.framework.util.KiwiLogger LOGGER;
    private java.util.concurrent.atomic.AtomicBoolean expired;
    private final java.util.List observers;
    protected com.amazon.android.framework.task.TaskManager taskManager;

    static b()
    {
        com.amazon.android.i.b.LOGGER = new com.amazon.android.framework.util.KiwiLogger("Expirable");
        return;
    }

    public b()
    {
        this.expired = new java.util.concurrent.atomic.AtomicBoolean(0);
        this.observers = new java.util.Vector();
        return;
    }

    private void notifyObservers()
    {
        java.util.Iterator v1 = this.observers.iterator();
        while (v1.hasNext()) {
            ((com.amazon.android.i.d) v1.next()).observe(this);
        }
        return;
    }

    private void scheduleExpiration()
    {
        this.taskManager.enqueueAtTime(com.amazon.android.framework.task.pipeline.TaskPipelineId.BACKGROUND, new com.amazon.android.i.a(this), this.getExpiration());
        return;
    }

    public final void discard()
    {
        if (this.expired.compareAndSet(0, 1)) {
            this.notifyObservers();
        }
        return;
    }

    protected abstract void doExpiration();

    public void expire()
    {
        if (this.expired.compareAndSet(0, 1)) {
            if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                com.amazon.android.i.b.LOGGER.trace(new StringBuilder().append("Expiring: ").append(this).toString());
            }
            this.doExpiration();
            if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                com.amazon.android.i.b.LOGGER.trace(new StringBuilder().append("Notifying Observers of expiration: ").append(this).toString());
            }
            this.notifyObservers();
        }
        return;
    }

    protected abstract java.util.Date getExpiration();

    protected boolean isExpired()
    {
        return this.expired.get();
    }

    public final void onResourcesPopulated()
    {
        this.scheduleExpiration();
        this.onResourcesPopulatedImpl();
        return;
    }

    protected void onResourcesPopulatedImpl()
    {
        return;
    }

    public final void register(com.amazon.android.i.d p2)
    {
        this.observers.add(p2);
        return;
    }
}
