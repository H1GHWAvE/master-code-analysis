package com.amazon.android.i;
public abstract class c extends com.amazon.android.i.b {
    private static final com.amazon.android.framework.util.KiwiLogger LOGGER = None;
    private static final long MILLISECONDS_PER_SECOND = 1000;
    private final java.util.Date instantiation;

    static c()
    {
        com.amazon.android.i.c.LOGGER = new com.amazon.android.framework.util.KiwiLogger("RelativeExpirable");
        return;
    }

    public c()
    {
        this.instantiation = new java.util.Date();
        return;
    }

    public final java.util.Date getExpiration()
    {
        com.amazon.android.i.c.LOGGER.trace(new StringBuilder().append("RelativeExpiration duration: ").append(this.getExpirationDurationInSeconds()).append(", expirable: ").append(this).toString());
        java.util.Date v2_6 = new java.util.Date((this.instantiation.getTime() + (1000 * this.getExpirationDurationInSeconds())));
        com.amazon.android.i.c.LOGGER.trace(new StringBuilder().append("Expiration should occur at time: ").append(v2_6).toString());
        return v2_6;
    }

    protected abstract long getExpirationDurationInSeconds();
}
