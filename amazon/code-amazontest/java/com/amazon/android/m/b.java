package com.amazon.android.m;
public final class b {
    private static final com.amazon.android.framework.util.KiwiLogger a;
    private static final java.util.Set e;
    private final java.security.cert.PKIXParameters b;
    private final java.security.cert.CertPathValidator c;
    private final java.util.Set d;

    static b()
    {
        com.amazon.android.m.b.a = new com.amazon.android.framework.util.KiwiLogger("CertVerifier");
        java.util.Set v0_3 = new java.util.HashSet();
        com.amazon.android.m.b.e = v0_3;
        v0_3.add("verisign");
        com.amazon.android.m.b.e.add("thawte");
        return;
    }

    public b()
    {
        com.amazon.android.framework.util.KiwiLogger v0_1 = javax.net.ssl.TrustManagerFactory.getInstance(javax.net.ssl.TrustManagerFactory.getDefaultAlgorithm());
        v0_1.init(0);
        this.d = new java.util.HashSet();
        java.util.Set v1_2 = v0_1.getTrustManagers();
        int v2 = v1_2.length;
        int v3 = 0;
        while (v3 < v2) {
            com.amazon.android.framework.util.KiwiLogger v0_10 = v1_2[v3];
            if ((v0_10 instanceof javax.net.ssl.X509TrustManager)) {
                com.amazon.android.framework.util.KiwiLogger v0_12 = ((javax.net.ssl.X509TrustManager) v0_10).getAcceptedIssuers();
                if (v0_12 != null) {
                    String v4_1 = v0_12.length;
                    Object[] v5_0 = 0;
                    int v6_0 = 0;
                    while (v5_0 < v4_1) {
                        java.util.Set v7_0 = v0_12[v5_0];
                        if (com.amazon.android.m.b.a(v7_0)) {
                            if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                                com.amazon.android.m.b.a.trace(new StringBuilder().append("Trusted Cert: ").append(v7_0.getSubjectX500Principal().getName()).toString());
                            }
                            this.d.add(new java.security.cert.TrustAnchor(v7_0, 0));
                            v6_0++;
                        }
                        v5_0++;
                    }
                    if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                        Object[] v5_2 = new Object[1];
                        v5_2[0] = Integer.valueOf(v6_0);
                        com.amazon.android.m.b.a.trace(String.format("loaded %d certs\n", v5_2));
                    }
                }
            }
            v3++;
        }
        if (this.d.isEmpty()) {
            com.amazon.android.m.b.a.error("TrustManager did not return valid accepted issuers, likely 3P custom TrustManager implementation issue.");
        }
        this.b = new java.security.cert.PKIXParameters(this.d);
        this.b.setRevocationEnabled(0);
        this.c = java.security.cert.CertPathValidator.getInstance("PKIX");
        return;
    }

    private static boolean a(java.security.cert.X509Certificate p3)
    {
        int v0_2 = p3.getSubjectDN().getName().toLowerCase();
        java.util.Iterator v1_1 = com.amazon.android.m.b.e.iterator();
        while (v1_1.hasNext()) {
            if (v0_2.contains(((String) v1_1.next()))) {
                int v0_3 = 1;
            }
            return v0_3;
        }
        v0_3 = 0;
        return v0_3;
    }

    public final boolean a(java.security.cert.CertPath p6)
    {
        try {
            this.c.validate(p6, this.b);
            String v0_0 = 1;
        } catch (String v1_1) {
            if (!com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            } else {
                com.amazon.android.m.b.a.error(new StringBuilder().append("Failed to verify cert path: ").append(v1_1).toString(), v1_1);
            }
        } catch (String v1_2) {
            if (!(v1_2.getCause() instanceof java.security.cert.CertificateExpiredException)) {
            } else {
                if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                    com.amazon.android.m.b.a.trace("CertVerifier doesn\'t care about an out of date certificate.");
                }
                v0_0 = 1;
            }
        }
        return v0_0;
    }
}
