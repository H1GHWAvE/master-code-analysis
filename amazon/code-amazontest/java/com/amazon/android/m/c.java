package com.amazon.android.m;
public final class c {
    private static final com.amazon.android.framework.util.KiwiLogger a;
    private android.app.Application b;
    private com.amazon.android.o.a c;

    static c()
    {
        com.amazon.android.m.c.a = new com.amazon.android.framework.util.KiwiLogger("DataAuthenticationKeyLoader");
        return;
    }

    public c()
    {
        return;
    }

    private static java.security.cert.CertPath a(java.util.jar.JarFile p3, java.util.jar.JarEntry p4)
    {
        try {
            if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                com.amazon.android.m.c.a.trace(new StringBuilder().append("Extracting cert from entry: ").append(p4.getName()).toString());
            }
        } catch (java.security.cert.CertPath v0_4) {
            throw com.amazon.android.h.a.a(v0_4);
        }
        java.security.cert.CertPath v0_3 = java.security.cert.CertificateFactory.getInstance("X.509");
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.m.c.a.trace("Generating certificates from entry input stream");
        }
        return v0_3.generateCertPath(new java.util.ArrayList(v0_3.generateCertificates(p3.getInputStream(p4))));
    }

    private static java.util.jar.JarEntry a(java.util.jar.JarFile p3)
    {
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.m.c.a.trace("Searching for cert in apk");
        }
        com.amazon.android.h.a v0_2 = p3.entries();
        while (v0_2.hasMoreElements()) {
            java.util.jar.JarEntry v3_2 = ((java.util.jar.JarEntry) v0_2.nextElement());
            if ((!v3_2.isDirectory()) && (v3_2.getName().equals("kiwi"))) {
                return v3_2;
            }
        }
        throw new com.amazon.android.h.a("CERT_NOT_FOUND", 0);
    }

    private java.util.jar.JarFile b()
    {
        com.amazon.android.h.a v0_1 = this.b.getPackageCodePath();
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.m.c.a.trace(new StringBuilder().append("Opening apk: ").append(v0_1).toString());
        }
        try {
            return new java.util.jar.JarFile(v0_1, 0);
        } catch (com.amazon.android.h.a v0_2) {
            throw com.amazon.android.h.a.a(v0_2);
        }
    }

    private static com.amazon.android.m.b c()
    {
        try {
            return new com.amazon.android.m.b();
        } catch (java.security.GeneralSecurityException v0_2) {
            throw new com.amazon.android.h.a("FAILED_TO_ESTABLISH_TRUST", v0_2);
        }
    }

    public final java.security.PublicKey a()
    {
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.m.c.a.trace("Loading data authentication key...");
        }
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.m.c.a.trace("Checking KiwiDataStore for key...");
        }
        int v0_6 = ((java.security.PublicKey) this.c.a("DATA_AUTHENTICATION_KEY"));
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            String v3_1;
            String v2_2 = new StringBuilder().append("Key was cached: ");
            if (v0_6 == 0) {
                v3_1 = 0;
            } else {
                v3_1 = 1;
            }
            com.amazon.android.m.c.a.trace(v2_2.append(v3_1).toString());
        }
        if (v0_6 == 0) {
            if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                com.amazon.android.m.c.a.trace("Loading authentication key from apk...");
            }
            int v0_15;
            int v0_9 = this.b();
            com.amazon.android.o.a v1_6 = com.amazon.android.m.c.a(v0_9, com.amazon.android.m.c.a(v0_9));
            if ((v1_6 == null) || (v1_6.getCertificates().size() <= 0)) {
                v0_15 = 0;
            } else {
                int v0_14 = ((java.security.cert.Certificate) v1_6.getCertificates().get(0));
                if (!(v0_14 instanceof java.security.cert.X509Certificate)) {
                } else {
                    int v0_18 = ((java.security.cert.X509Certificate) v0_14).getSubjectX500Principal().getName();
                    if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                        com.amazon.android.m.c.a.trace(new StringBuilder().append("Kiwi Cert Details: ").append(v0_18).toString());
                    }
                    if ((!v0_18.contains("O=Amazon.com\\, Inc.")) || ((!v0_18.contains("C=US")) || ((!v0_18.contains("ST=Washington")) || (!v0_18.contains("L=Seattle"))))) {
                        v0_15 = 0;
                    } else {
                        v0_15 = 1;
                    }
                }
            }
            if (v0_15 != 0) {
                if (com.amazon.android.m.c.c().a(v1_6)) {
                    v0_6 = ((java.security.cert.X509Certificate) v1_6.getCertificates().get(0)).getPublicKey();
                    if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                        com.amazon.android.m.c.a.trace("Placing auth key into storage");
                    }
                    this.c.a("DATA_AUTHENTICATION_KEY", v0_6);
                } else {
                    throw new com.amazon.android.h.a("VERIFICATION_FAILED", 0);
                }
            } else {
                throw new com.amazon.android.h.a("CERT_INVALID", 0);
            }
        }
        return v0_6;
    }
}
