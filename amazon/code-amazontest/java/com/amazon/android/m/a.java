package com.amazon.android.m;
public final class a {
    private static final com.amazon.android.framework.util.KiwiLogger a;

    static a()
    {
        com.amazon.android.m.a.a = new com.amazon.android.framework.util.KiwiLogger("SignatureVerifier");
        return;
    }

    public a()
    {
        return;
    }

    public static boolean a(String p4, String p5, java.security.PublicKey p6)
    {
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.m.a.a.trace(new StringBuilder().append("Verifying signature of data: \'").append(p4).append("\', signature: \'").append(p5).append("\', with key: \'").append(p6.toString()).toString());
        }
        try {
            int v0_3 = com.amazon.mas.kiwi.util.Base64.decode(p5.getBytes());
            java.security.Signature v1_10 = java.security.Signature.getInstance("SHA1withRSA");
            v1_10.initVerify(p6);
            v1_10.update(p4.getBytes());
            int v0_4 = v1_10.verify(v0_3);
        } catch (int v0) {
            v0_4 = 0;
        } catch (int v0) {
            v0_4 = 0;
        }
        return v0_4;
    }
}
