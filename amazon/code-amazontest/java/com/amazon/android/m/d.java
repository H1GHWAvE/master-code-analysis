package com.amazon.android.m;
public final class d {
    private static final com.amazon.android.framework.util.KiwiLogger a;
    private final java.util.Map b;

    static d()
    {
        com.amazon.android.m.d.a = new com.amazon.android.framework.util.KiwiLogger("SignedToken");
        return;
    }

    public d(String p6, java.security.PublicKey p7)
    {
        this.b = new java.util.HashMap();
        com.amazon.android.h.c v0_2 = com.amazon.android.m.d.b(p6);
        com.amazon.android.framework.util.KiwiLogger v1_1 = v0_2.lastIndexOf("|");
        if (v1_1 != -1) {
            String v2_2 = v0_2.substring(0, v1_1);
            com.amazon.android.h.c v0_3 = v0_2.substring((v1_1 + 1));
            if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                com.amazon.android.m.d.a.trace(new StringBuilder().append("Token data: ").append(v2_2).toString());
                com.amazon.android.m.d.a.trace(new StringBuilder().append("Signature: ").append(v0_3).toString());
            }
            if (com.amazon.android.m.a.a(v2_2, v0_3, p7)) {
                this.c(v2_2);
                return;
            } else {
                throw new com.amazon.android.h.c();
            }
        } else {
            throw com.amazon.android.h.b.a();
        }
    }

    private static String b(String p3)
    {
        try {
            return new String(com.amazon.mas.kiwi.util.Base64.decode(p3.getBytes()));
        } catch (String v0_2) {
            throw new com.amazon.android.h.b("DECODE", v0_2.getMessage());
        }
    }

    private void c(String p7)
    {
        com.amazon.android.h.b v0_1 = new java.util.StringTokenizer(p7, ",");
        while (v0_1.hasMoreElements()) {
            String v1_2 = v0_1.nextToken();
            com.amazon.android.m.d.a.trace(new StringBuilder().append("Field: ").append(v1_2).toString());
            java.util.Map v2_2 = v1_2.indexOf("=");
            if (v2_2 != -1) {
                String v3_7 = v1_2.substring(0, v2_2);
                String v1_3 = v1_2.substring((v2_2 + 1));
                com.amazon.android.m.d.a.trace(new StringBuilder().append("FieldName: ").append(v3_7).toString());
                com.amazon.android.m.d.a.trace(new StringBuilder().append("FieldValue: ").append(v1_3).toString());
                this.b.put(v3_7, v1_3);
            } else {
                throw com.amazon.android.h.b.a();
            }
        }
        return;
    }

    public final String a(String p2)
    {
        com.amazon.android.d.a.a(p2, "key");
        return ((String) this.b.get(p2));
    }

    public final String toString()
    {
        return new StringBuilder().append("Signed Token: ").append(this.b).toString();
    }
}
