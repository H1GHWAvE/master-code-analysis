package com.amazon.android;
final class e implements com.amazon.android.n.c {
    private synthetic com.amazon.android.Kiwi a;

    e(com.amazon.android.Kiwi p1)
    {
        this.a = p1;
        return;
    }

    public final com.amazon.android.n.f a()
    {
        return com.amazon.android.j.d.b;
    }

    public final bridge synthetic void a(com.amazon.android.n.d p4)
    {
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.Kiwi.a().trace(new StringBuilder().append("Beginning shutdown process for application: ").append(com.amazon.android.Kiwi.e(this.a).getPackageName()).toString());
        }
        com.amazon.android.Kiwi.b();
        return;
    }

    public final com.amazon.android.n.a b()
    {
        return com.amazon.android.n.a.c;
    }
}
