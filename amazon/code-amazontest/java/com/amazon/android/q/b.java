package com.amazon.android.q;
public class b implements java.io.Serializable {
    private static final long serialVersionUID = 1;
    public final java.util.Map a;

    public b(String p5)
    {
        this.a = new java.util.HashMap();
        this.a.put("name", p5);
        this.a.put("time", String.valueOf(System.currentTimeMillis()));
        return;
    }

    public final com.amazon.android.q.b a(String p2, String p3)
    {
        this.a.put(p2, p3);
        return this;
    }

    public String toString()
    {
        return new StringBuilder().append("Metric: [").append(this.a.toString()).append("]").toString();
    }
}
