package com.amazon.android.q;
public final class c implements com.amazon.android.q.d {
    private static final com.amazon.android.framework.util.KiwiLogger a;
    private com.amazon.android.q.a b;

    static c()
    {
        com.amazon.android.q.c.a = new com.amazon.android.framework.util.KiwiLogger("MetricsManagerImpl");
        return;
    }

    public c()
    {
        this.b = new com.amazon.android.q.a();
        return;
    }

    public final declared_synchronized com.amazon.android.q.a a()
    {
        try {
            com.amazon.android.q.a v0_2;
            if (!this.b.a()) {
                v0_2 = this.b;
                this.b = new com.amazon.android.q.a();
            } else {
                v0_2 = this.b;
            }
        } catch (com.amazon.android.q.a v0_3) {
            throw v0_3;
        }
        return v0_2;
    }

    public final declared_synchronized void a(com.amazon.android.q.b p4)
    {
        try {
            if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                com.amazon.android.q.c.a.trace(new StringBuilder().append("Recording Metric: ").append(p4).toString());
            }
        } catch (java.util.List v0_2) {
            throw v0_2;
        }
        this.b.a.add(p4);
        return;
    }
}
