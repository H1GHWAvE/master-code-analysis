package com.amazon.android.q;
public final class a implements java.io.Serializable, java.lang.Iterable {
    private static final long serialVersionUID = 1;
    final java.util.List a;

    public a()
    {
        this.a = new java.util.ArrayList();
        return;
    }

    public final boolean a()
    {
        return this.a.isEmpty();
    }

    public final int b()
    {
        return this.a.size();
    }

    public final java.util.Iterator iterator()
    {
        return this.a.iterator();
    }

    public final String toString()
    {
        return new StringBuilder().append("MetricBatch: [").append(this.a).append("]").toString();
    }
}
