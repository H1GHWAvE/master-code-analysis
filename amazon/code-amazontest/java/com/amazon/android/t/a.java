package com.amazon.android.t;
public final class a extends com.amazon.android.framework.task.command.AbstractCommandTask {
    private java.util.Map a;

    public a(java.util.Map p1)
    {
        this.a = p1;
        return;
    }

    protected final java.util.Map getCommandData()
    {
        return this.a;
    }

    protected final String getCommandName()
    {
        return "lifeCycle_Events";
    }

    protected final String getCommandVersion()
    {
        return "1.0";
    }

    protected final boolean isExecutionNeeded()
    {
        return 1;
    }

    protected final void onFailure(com.amazon.venezia.command.FailureResult p1)
    {
        return;
    }

    protected final void onSuccess(com.amazon.venezia.command.SuccessResult p1)
    {
        return;
    }
}
