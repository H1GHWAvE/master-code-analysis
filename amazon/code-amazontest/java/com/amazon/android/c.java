package com.amazon.android;
final class c extends android.content.BroadcastReceiver {
    private synthetic com.amazon.android.Kiwi a;

    c(com.amazon.android.Kiwi p1)
    {
        this.a = p1;
        return;
    }

    public final void onReceive(android.content.Context p5, android.content.Intent p6)
    {
        if (!com.amazon.android.Kiwi.a(this.a).b("TEST_MODE")) {
            if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                com.amazon.android.Kiwi.a().trace("Enable test mode broadcast received!");
            }
            com.amazon.android.framework.util.KiwiLogger.enableTest();
            com.amazon.android.Kiwi.a().test("Enabling test mode!");
            com.amazon.android.Kiwi.a().test(new StringBuilder().append("drm enabled: ").append(com.amazon.android.Kiwi.b(this.a)).toString());
            com.amazon.android.Kiwi.a(this.a).a("TEST_MODE", Boolean.valueOf(1));
            com.amazon.android.Kiwi.c(this.a).a(new com.amazon.android.a());
        }
        return;
    }
}
