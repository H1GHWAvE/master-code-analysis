package com.amazon.android;
public final class i extends com.amazon.android.framework.task.command.AbstractCommandTask {
    private static final com.amazon.android.framework.util.KiwiLogger a;
    private com.amazon.android.framework.prompt.PromptManager b;

    static i()
    {
        com.amazon.android.i.a = new com.amazon.android.framework.util.KiwiLogger("CheckIfAppisBlockedTask");
        return;
    }

    public i()
    {
        return;
    }

    protected final java.util.Map getCommandData()
    {
        return 0;
    }

    protected final String getCommandName()
    {
        return "check_blocked_status";
    }

    protected final String getCommandVersion()
    {
        return "1.0";
    }

    protected final boolean isExecutionNeeded()
    {
        return 1;
    }

    protected final void onFailure(com.amazon.venezia.command.FailureResult p6)
    {
        if (this.isWorkflowChild()) {
            this.quitParentWorkflow();
        }
        com.amazon.android.i.a.test("app is blocked, killing");
        this.b.present(new com.amazon.android.framework.prompt.c(new com.amazon.android.framework.prompt.PromptContent(p6.getDisplayableName(), p6.getDisplayableMessage(), p6.getButtonLabel(), p6.show())));
        return;
    }

    protected final void onSuccess(com.amazon.venezia.command.SuccessResult p4)
    {
        if ((p4.getData() != null) && (p4.getData().containsKey("verbose"))) {
            boolean v0_4 = ((Boolean) p4.getData().get("verbose")).booleanValue();
            com.amazon.android.framework.util.KiwiLogger.ERROR_ON = v0_4;
            com.amazon.android.framework.util.KiwiLogger.TRACE_ON = v0_4;
        }
        return;
    }
}
