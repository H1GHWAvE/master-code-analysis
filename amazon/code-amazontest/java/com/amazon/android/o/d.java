package com.amazon.android.o;
public final class d implements com.amazon.android.i.d {
    private static final com.amazon.android.framework.util.KiwiLogger a;
    private com.amazon.android.framework.resource.a b;
    private final java.util.Map c;

    static d()
    {
        com.amazon.android.o.d.a = new com.amazon.android.framework.util.KiwiLogger("ExpirableValueDataStore");
        return;
    }

    public d()
    {
        this.c = new java.util.HashMap();
        return;
    }

    static synthetic com.amazon.android.framework.util.KiwiLogger a()
    {
        return com.amazon.android.o.d.a;
    }

    private declared_synchronized void a(com.amazon.android.o.b p5)
    {
        try {
            if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                com.amazon.android.o.d.a.trace(new StringBuilder().append("Observed expiration: ").append(p5).append(" removing from store!").toString());
            }
        } catch (com.amazon.android.framework.util.KiwiLogger v0_10) {
            throw v0_10;
        }
        java.util.Iterator v1_6 = this.c.entrySet().iterator();
        while (v1_6.hasNext()) {
            if (((java.util.Map$Entry) v1_6.next()).getValue() == p5) {
                if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                    com.amazon.android.o.d.a.trace(new StringBuilder().append("Removing entry from store: ").append(p5).toString());
                }
                v1_6.remove();
            }
        }
        return;
    }

    public final declared_synchronized void a(String p4, com.amazon.android.o.b p5)
    {
        try {
            com.amazon.android.d.a.a(p4, "key");
            com.amazon.android.d.a.a(p5, "value");
        } catch (java.util.Map v0_4) {
            throw v0_4;
        }
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.o.d.a.trace(new StringBuilder().append("Placing value into store with key: ").append(p4).append(", expiration: ").append(p5.getExpiration()).toString());
        }
        this.b.b(p5);
        p5.register(this);
        this.c.put(p4, p5);
        return;
    }

    public final declared_synchronized void a(String p4, Object p5)
    {
        try {
            if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                com.amazon.android.o.d.a.trace(new StringBuilder().append("Placing non-expiring value into store with key: ").append(p4).toString());
            }
        } catch (com.amazon.android.o.c v0_2) {
            throw v0_2;
        }
        this.c.put(p4, new com.amazon.android.o.c(this, p5, new java.util.Date()));
        return;
    }

    public final declared_synchronized boolean a(String p2)
    {
        try {
            int v0_2;
            if (this.b(p2) != this.b(p2)) {
                v0_2 = 0;
            } else {
                v0_2 = 1;
            }
        } catch (int v0_1) {
            throw v0_1;
        }
        return v0_2;
    }

    public final declared_synchronized Object b(String p4)
    {
        try {
            if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                com.amazon.android.o.d.a.trace(new StringBuilder().append("Fetching value: ").append(p4).toString());
            }
        } catch (int v0_8) {
            throw v0_8;
        }
        int v0_5;
        int v0_4 = ((com.amazon.android.o.b) this.c.get(p4));
        if (v0_4 != 0) {
            v0_5 = v0_4.a;
        } else {
            if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                com.amazon.android.o.d.a.trace("Value not present in store, returning null");
            }
            v0_5 = 0;
        }
        return v0_5;
    }

    public final declared_synchronized void c(String p5)
    {
        try {
            Throwable v0_2 = ((com.amazon.android.o.b) this.c.get(p5));
        } catch (Throwable v0_3) {
            throw v0_3;
        }
        if (v0_2 != null) {
            if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                com.amazon.android.o.d.a.trace(new StringBuilder().append("Removing value associated with key: ").append(p5).append(", value: ").append(v0_2).toString());
            }
            this.c.remove(p5);
            v0_2.discard();
        }
        return;
    }

    public final bridge synthetic void observe(com.amazon.android.i.b p1)
    {
        this.a(((com.amazon.android.o.b) p1));
        return;
    }

    public final String toString()
    {
        return this.c.toString();
    }
}
