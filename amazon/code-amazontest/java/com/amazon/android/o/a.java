package com.amazon.android.o;
public final class a implements com.amazon.android.framework.resource.b {
    public com.amazon.android.o.d a;
    private com.amazon.android.framework.resource.a b;

    public a()
    {
        this.a = new com.amazon.android.o.d();
        return;
    }

    public final Object a(String p2)
    {
        return this.a.b(p2);
    }

    public final void a(String p2, Object p3)
    {
        this.a.a(p2, p3);
        return;
    }

    public final boolean b(String p2)
    {
        return this.a.a(p2);
    }

    public final void onResourcesPopulated()
    {
        this.b.b(this.a);
        return;
    }

    public final String toString()
    {
        return this.a.toString();
    }
}
