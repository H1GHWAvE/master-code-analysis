package com.amazon.android.l;
public abstract class c implements com.amazon.android.l.a {
    private com.amazon.android.l.b workflow;

    public c()
    {
        return;
    }

    protected final boolean isWorkflowChild()
    {
        int v0_1;
        if (this.workflow == null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    protected final void quitParentWorkflow()
    {
        com.amazon.android.d.a.a(this.isWorkflowChild(), "task is no a workflow child");
        this.workflow.c();
        return;
    }

    public final void setWorkflow(com.amazon.android.l.b p3)
    {
        int v0_2;
        com.amazon.android.d.a.a(p3, "workflow");
        if (this.workflow != null) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        com.amazon.android.d.a.a(v0_2, "workflow instance can only be set once");
        this.workflow = p3;
        return;
    }
}
