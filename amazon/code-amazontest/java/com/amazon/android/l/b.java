package com.amazon.android.l;
public abstract class b extends com.amazon.android.l.c implements com.amazon.android.framework.resource.b {
    private static final com.amazon.android.framework.util.KiwiLogger a;
    private com.amazon.android.framework.resource.a b;
    private java.util.List c;
    private java.util.concurrent.atomic.AtomicBoolean d;

    static b()
    {
        com.amazon.android.l.b.a = new com.amazon.android.framework.util.KiwiLogger("TaskWorkflow");
        return;
    }

    public b()
    {
        this.c = new java.util.ArrayList();
        this.d = new java.util.concurrent.atomic.AtomicBoolean(0);
        return;
    }

    protected void a()
    {
        return;
    }

    protected final void a(com.amazon.android.framework.task.Task p2)
    {
        com.amazon.android.d.a.a(p2, "task");
        this.c.add(p2);
        if ((p2 instanceof com.amazon.android.l.a)) {
            ((com.amazon.android.l.a) p2).setWorkflow(this);
        }
        return;
    }

    protected abstract String b();

    public final void c()
    {
        this.d.set(1);
        return;
    }

    public final void execute()
    {
        try {
            if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                com.amazon.android.l.b.a.trace(new StringBuilder().append("Exiting task workflow: ").append(this).toString());
            }
        } catch (com.amazon.android.framework.util.KiwiLogger v0_8) {
            this.a();
            throw v0_8;
        }
        String v1_5 = this.c.iterator();
        while (v1_5.hasNext()) {
            if (!this.d.get()) {
                ((com.amazon.android.framework.task.Task) v1_5.next()).execute();
            } else {
                if (!com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                    break;
                }
                com.amazon.android.l.b.a.trace("Finished set, exiting task workflow early");
                break;
            }
        }
        this.a();
        return;
    }

    public final void onResourcesPopulated()
    {
        java.util.Iterator v1 = this.c.iterator();
        while (v1.hasNext()) {
            this.b.b(((com.amazon.android.framework.task.Task) v1.next()));
        }
        return;
    }

    public final String toString()
    {
        return this.b();
    }
}
