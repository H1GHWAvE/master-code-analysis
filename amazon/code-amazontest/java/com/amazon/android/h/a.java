package com.amazon.android.h;
public class a extends com.amazon.android.framework.exception.KiwiException {
    private static final long serialVersionUID = 1;

    public a(String p2, Throwable p3)
    {
        this("DATA_AUTH_KEY_LOAD_FAILURE", p2, p3);
        return;
    }

    public static com.amazon.android.h.a a(Throwable p2)
    {
        return new com.amazon.android.h.a("CERT_FAILED_TO_LOAD", p2);
    }
}
