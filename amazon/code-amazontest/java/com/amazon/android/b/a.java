package com.amazon.android.b;
public class a extends com.amazon.android.framework.exception.KiwiException {
    private static final long serialVersionUID = 1;

    public a(android.os.RemoteException p2)
    {
        String v0_1;
        if (!(p2 instanceof android.os.DeadObjectException)) {
            v0_1 = "COMMAND_SERVICE_REMOTE_EXCEPTION";
        } else {
            v0_1 = "COMMAND_SERVICE_DEAD_OBJECT_EXCEPTION";
        }
        this(v0_1);
        return;
    }
}
