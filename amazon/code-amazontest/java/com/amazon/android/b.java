package com.amazon.android;
public final class b extends com.amazon.android.l.b {
    private com.amazon.android.framework.task.command.b a;

    public b()
    {
        this.a(new com.amazon.android.i());
        this.a(new com.amazon.android.r.a());
        this.a(new com.amazon.android.licensing.e());
        return;
    }

    protected final void a()
    {
        if (!this.isWorkflowChild()) {
            this.a.b();
        }
        return;
    }

    protected final String b()
    {
        return "DrmFullApplicationLaunchTaskWorkflow";
    }
}
