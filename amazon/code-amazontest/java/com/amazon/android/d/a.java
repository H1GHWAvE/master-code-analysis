package com.amazon.android.d;
public final class a {

    public a()
    {
        return;
    }

    public static void a()
    {
        String v0_2 = android.os.Looper.getMainLooper().getThread().getId();
        if (Thread.currentThread().getId() != v0_2) {
            com.amazon.android.d.a.a(new StringBuilder().append("Executing thread must be thread: ").append(v0_2).append(", was: ").append(Thread.currentThread().getId()).toString());
        }
        return;
    }

    public static void a(Object p2, String p3)
    {
        if (p2 == null) {
            com.amazon.android.d.a.a(new StringBuilder().append("Argument: ").append(p3).append(" cannot be null").toString());
        }
        return;
    }

    private static void a(String p1)
    {
        throw new com.amazon.android.d.b(p1);
    }

    public static void a(boolean p0, String p1)
    {
        if (!p0) {
            com.amazon.android.d.a.a(p1);
        }
        return;
    }

    public static void b(boolean p0, String p1)
    {
        if (p0) {
            com.amazon.android.d.a.a(p1);
        }
        return;
    }
}
