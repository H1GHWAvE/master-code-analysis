package com.amazon.android.j;
public final enum class d extends java.lang.Enum implements com.amazon.android.n.f {
    public static final enum com.amazon.android.j.d a;
    public static final enum com.amazon.android.j.d b;
    public static final enum com.amazon.android.j.d c;
    public static final enum com.amazon.android.j.d d;
    private static final synthetic com.amazon.android.j.d[] e;

    static d()
    {
        com.amazon.android.j.d.a = new com.amazon.android.j.d("CREATE", 0);
        com.amazon.android.j.d.b = new com.amazon.android.j.d("DESTROY", 1);
        com.amazon.android.j.d.c = new com.amazon.android.j.d("START", 2);
        com.amazon.android.j.d.d = new com.amazon.android.j.d("STOP", 3);
        com.amazon.android.j.d[] v0_9 = new com.amazon.android.j.d[4];
        v0_9[0] = com.amazon.android.j.d.a;
        v0_9[1] = com.amazon.android.j.d.b;
        v0_9[2] = com.amazon.android.j.d.c;
        v0_9[3] = com.amazon.android.j.d.d;
        com.amazon.android.j.d.e = v0_9;
        return;
    }

    private d(String p1, int p2)
    {
        this(p1, p2);
        return;
    }

    public static com.amazon.android.j.d valueOf(String p1)
    {
        return ((com.amazon.android.j.d) Enum.valueOf(com.amazon.android.j.d, p1));
    }

    public static com.amazon.android.j.d[] values()
    {
        return ((com.amazon.android.j.d[]) com.amazon.android.j.d.e.clone());
    }

    public final String toString()
    {
        return new StringBuilder().append("APPLICATION_").append(this.name()).toString();
    }
}
