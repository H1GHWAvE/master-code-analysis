package com.amazon.android.j;
public final enum class c extends java.lang.Enum implements com.amazon.android.n.f {
    public static final enum com.amazon.android.j.c a;
    public static final enum com.amazon.android.j.c b;
    public static final enum com.amazon.android.j.c c;
    public static final enum com.amazon.android.j.c d;
    public static final enum com.amazon.android.j.c e;
    public static final enum com.amazon.android.j.c f;
    private static final synthetic com.amazon.android.j.c[] g;

    static c()
    {
        com.amazon.android.j.c.a = new com.amazon.android.j.c("CREATE", 0);
        com.amazon.android.j.c.b = new com.amazon.android.j.c("DESTROY", 1);
        com.amazon.android.j.c.c = new com.amazon.android.j.c("RESUME", 2);
        com.amazon.android.j.c.d = new com.amazon.android.j.c("PAUSE", 3);
        com.amazon.android.j.c.e = new com.amazon.android.j.c("START", 4);
        com.amazon.android.j.c.f = new com.amazon.android.j.c("STOP", 5);
        com.amazon.android.j.c[] v0_13 = new com.amazon.android.j.c[6];
        v0_13[0] = com.amazon.android.j.c.a;
        v0_13[1] = com.amazon.android.j.c.b;
        v0_13[2] = com.amazon.android.j.c.c;
        v0_13[3] = com.amazon.android.j.c.d;
        v0_13[4] = com.amazon.android.j.c.e;
        v0_13[5] = com.amazon.android.j.c.f;
        com.amazon.android.j.c.g = v0_13;
        return;
    }

    private c(String p1, int p2)
    {
        this(p1, p2);
        return;
    }

    public static com.amazon.android.j.c valueOf(String p1)
    {
        return ((com.amazon.android.j.c) Enum.valueOf(com.amazon.android.j.c, p1));
    }

    public static com.amazon.android.j.c[] values()
    {
        return ((com.amazon.android.j.c[]) com.amazon.android.j.c.g.clone());
    }

    public final String toString()
    {
        return new StringBuilder().append("ACTIVITY_").append(this.name()).toString();
    }
}
