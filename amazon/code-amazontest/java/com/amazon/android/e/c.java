package com.amazon.android.e;
public final class c implements com.amazon.android.e.b, com.amazon.android.framework.resource.b {
    private static final com.amazon.android.framework.util.KiwiLogger a;
    private static final java.util.Random b;
    private com.amazon.android.framework.task.TaskManager c;
    private com.amazon.android.framework.context.ContextManager d;
    private com.amazon.android.n.g e;
    private java.util.concurrent.atomic.AtomicReference f;
    private java.util.concurrent.BlockingQueue g;

    static c()
    {
        com.amazon.android.e.c.a = new com.amazon.android.framework.util.KiwiLogger("ActivityResultManagerImpl");
        com.amazon.android.e.c.b = new java.util.Random();
        return;
    }

    public c()
    {
        this.f = new java.util.concurrent.atomic.AtomicReference();
        this.g = new java.util.concurrent.LinkedBlockingQueue();
        return;
    }

    static synthetic com.amazon.android.framework.util.KiwiLogger a()
    {
        return com.amazon.android.e.c.a;
    }

    static synthetic java.util.concurrent.atomic.AtomicReference a(com.amazon.android.e.c p1)
    {
        return p1.f;
    }

    static synthetic com.amazon.android.framework.context.ContextManager b(com.amazon.android.e.c p1)
    {
        return p1.d;
    }

    public final com.amazon.android.e.f a(android.content.Intent p8)
    {
        int v0_11;
        String v1_1 = new com.amazon.android.e.a(p8, (com.amazon.android.e.c.b.nextInt(65535) + 1));
        if (this.f.compareAndSet(0, v1_1)) {
            com.amazon.android.e.c.a.trace(new StringBuilder().append("Starting activity for result: ").append(p8).append(", ").append(p8.getFlags()).append(", requestId: ").append(v1_1.a).toString());
            this.c.enqueueAtFront(com.amazon.android.framework.task.pipeline.TaskPipelineId.FOREGROUND, new com.amazon.android.e.d(this, v1_1));
            try {
                com.amazon.android.e.c.a.trace(new StringBuilder().append("Blocking for request: ").append(v1_1.a).toString());
                v0_11 = ((com.amazon.android.e.f) this.g.take());
                com.amazon.android.e.c.a.trace(new StringBuilder().append("Received Response: ").append(v1_1.a).toString());
                String v1_5 = this.f;
                v1_5.set(0);
            } catch (int v0) {
                com.amazon.android.e.c.a.trace("Interrupted while awaiting for request, returning null");
                v1_5 = new StringBuilder().append("Received Response: ").append(v1_5.a).toString();
                com.amazon.android.e.c.a.trace(v1_5);
                this.f.set(0);
                v0_11 = 0;
            } catch (int v0_15) {
                com.amazon.android.e.c.a.trace(new StringBuilder().append("Received Response: ").append(v1_5.a).toString());
                this.f.set(0);
                throw v0_15;
            }
        } else {
            com.amazon.android.e.c.a.error("StartActivityForResult called while ActivityResultManager is already awaiting a result");
            v0_11 = 0;
        }
        return v0_11;
    }

    public final boolean a(com.amazon.android.e.f p5)
    {
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.e.c.a.trace(new StringBuilder().append("Recieved ActivityResult: ").append(p5).toString());
        }
        int v0_9;
        int v0_4 = ((com.amazon.android.e.a) this.f.get());
        if (v0_4 != 0) {
            if (v0_4.a == p5.a) {
                if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                    com.amazon.android.e.c.a.trace(new StringBuilder().append("Signaling thread waiting for request: ").append(p5.a).toString());
                }
                this.g.add(p5);
                v0_9 = 1;
            } else {
                if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                    com.amazon.android.e.c.a.trace(new StringBuilder().append("We don\'t have a request with code: ").append(p5.a).append(", returning").toString());
                }
                v0_9 = 0;
            }
        } else {
            if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                com.amazon.android.e.c.a.trace("We don\'t have a current open request, returning");
            }
            v0_9 = 0;
        }
        return v0_9;
    }

    public final void onResourcesPopulated()
    {
        this.e.a(new com.amazon.android.e.e(this));
        return;
    }
}
