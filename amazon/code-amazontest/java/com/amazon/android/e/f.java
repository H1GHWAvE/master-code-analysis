package com.amazon.android.e;
public final class f {
    final int a;
    public final int b;
    private final android.app.Activity c;
    private final android.content.Intent d;

    public f(android.app.Activity p1, int p2, int p3, android.content.Intent p4)
    {
        this.c = p1;
        this.a = p2;
        this.b = p3;
        this.d = p4;
        return;
    }

    public final String toString()
    {
        return new StringBuilder().append("ActivtyResult: [ requestCode: ").append(this.a).append(", resultCode: ").append(this.b).append(", activity: ").append(this.c).append(", intent: ").append(this.d).append("]").toString();
    }
}
