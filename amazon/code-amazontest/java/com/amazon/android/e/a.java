package com.amazon.android.e;
final class a {
    final int a;
    android.app.Activity b;
    private final android.content.Intent c;

    public a(android.content.Intent p1, int p2)
    {
        this.c = p1;
        this.a = p2;
        return;
    }

    public final void a(android.app.Activity p4)
    {
        com.amazon.android.e.c.a().trace(new StringBuilder().append("Calling startActivityForResult from: ").append(p4).toString());
        p4.startActivityForResult(this.c, this.a);
        this.b = p4;
        return;
    }
}
