package com.amazon.android.e;
final class e implements com.amazon.android.n.c {
    private synthetic com.amazon.android.e.c a;

    e(com.amazon.android.e.c p1)
    {
        this.a = p1;
        return;
    }

    public final com.amazon.android.n.f a()
    {
        return com.amazon.android.j.c.c;
    }

    public final bridge synthetic void a(com.amazon.android.n.d p5)
    {
        com.amazon.android.e.a v4_2 = ((com.amazon.android.e.a) com.amazon.android.e.c.a(this.a).get());
        if (v4_2 != null) {
            android.app.Activity v0_2 = ((com.amazon.android.j.b) p5).a;
            com.amazon.android.e.c.a().error("Context changed while awaiting result!");
            if (v4_2.b != null) {
                com.amazon.android.e.c.a().error(new StringBuilder().append("Finishing activity from old context: ").append(v4_2.b).toString());
                v4_2.b.finishActivity(v4_2.a);
            }
            v4_2.a(v0_2);
        }
        return;
    }

    public final com.amazon.android.n.a b()
    {
        return com.amazon.android.n.a.b;
    }
}
