package com.amazon.android.e;
final class d implements com.amazon.android.framework.task.Task {
    private synthetic com.amazon.android.e.a a;
    private synthetic com.amazon.android.e.c b;

    d(com.amazon.android.e.c p1, com.amazon.android.e.a p2)
    {
        this.b = p1;
        this.a = p2;
        return;
    }

    public final void execute()
    {
        com.amazon.android.framework.util.KiwiLogger v0_2 = com.amazon.android.e.c.b(this.b).getVisible();
        if (v0_2 != null) {
            this.a.a(v0_2);
        } else {
            com.amazon.android.e.c.a().trace("No activity to call startActivityForResult on. startActivityForResult when an activity becomes visible");
        }
        return;
    }
}
