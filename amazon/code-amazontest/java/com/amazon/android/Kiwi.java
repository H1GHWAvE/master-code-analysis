package com.amazon.android;
public final class Kiwi implements com.amazon.android.framework.resource.b {
    protected static final String ACTIVITY_NAME = "ActivityName";
    protected static final String EVENT_NAME = "EventName";
    private static com.amazon.android.Kiwi INSTANCE = None;
    private static final com.amazon.android.framework.util.KiwiLogger LOGGER = None;
    protected static final String TIMESTAMP = "Timestamp";
    private android.app.Application application;
    private com.amazon.android.m.c authKeyLoader;
    private com.amazon.android.framework.context.ContextManager contextManager;
    private com.amazon.android.o.a dataStore;
    private final boolean drmEnabled;
    private com.amazon.android.n.g eventManager;
    private com.amazon.android.framework.prompt.PromptManager promptManager;
    private com.amazon.android.e.b resultManager;
    private com.amazon.android.framework.task.TaskManager taskManager;

    static Kiwi()
    {
        com.amazon.android.Kiwi.LOGGER = new com.amazon.android.framework.util.KiwiLogger("Kiwi");
        return;
    }

    private Kiwi(android.app.Application p8, boolean p9)
    {
        String v0_0 = System.currentTimeMillis();
        this.drmEnabled = p9;
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.Kiwi.LOGGER.trace(new StringBuilder().append("Starting initialization process for application: ").append(p8.getPackageName()).toString());
            com.amazon.android.Kiwi.LOGGER.trace(new StringBuilder().append("DRM enabled: ").append(p9).toString());
        }
        this.instantiateTheWorld(p8);
        this.registerTestModeReceiver(p8);
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.Kiwi.LOGGER.trace(new StringBuilder().append("Kiwi.Constructor Time: ").append((System.currentTimeMillis() - v0_0)).toString());
        }
        return;
    }

    static synthetic com.amazon.android.framework.util.KiwiLogger a()
    {
        return com.amazon.android.Kiwi.LOGGER;
    }

    static synthetic com.amazon.android.o.a a(com.amazon.android.Kiwi p1)
    {
        return p1.dataStore;
    }

    static synthetic void a(com.amazon.android.framework.task.command.AbstractCommandTask p0)
    {
        com.amazon.android.Kiwi.unsafeAddCommandToCommandTaskPipeline(p0);
        return;
    }

    public static void addCommandToCommandTaskPipeline(com.amazon.android.framework.task.command.AbstractCommandTask p2)
    {
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.Kiwi.LOGGER.trace("WARNING: Use of deprecated method detected.");
        }
        if (!com.amazon.android.Kiwi.isInitialized()) {
            com.amazon.android.Kiwi.LOGGER.error("Kiwi subsystem is not fully initialized.  Cannot process task.");
        } else {
            com.amazon.android.Kiwi.unsafeAddCommandToCommandTaskPipeline(p2);
        }
        return;
    }

    public static void addCommandToCommandTaskPipeline(com.amazon.android.framework.task.command.AbstractCommandTask p3, android.content.Context p4)
    {
        if (com.amazon.android.Kiwi.isInitialized()) {
            com.amazon.android.Kiwi.unsafeAddCommandToCommandTaskPipeline(p3);
        } else {
            if ((p4 == null) || (p4.getApplicationContext() == null)) {
                com.amazon.android.Kiwi.LOGGER.error("Kiwi subsystem cannot be initialized because of null context. Unable to enqueue task.");
            } else {
                com.amazon.android.Kiwi.INSTANCE = new com.amazon.android.Kiwi(((android.app.Application) p4.getApplicationContext()), 0);
            }
        }
        return;
    }

    static synthetic com.amazon.android.Kiwi b()
    {
        com.amazon.android.Kiwi.INSTANCE = 0;
        return 0;
    }

    static synthetic boolean b(com.amazon.android.Kiwi p1)
    {
        return p1.drmEnabled;
    }

    static synthetic com.amazon.android.n.g c(com.amazon.android.Kiwi p1)
    {
        return p1.eventManager;
    }

    static synthetic void d(com.amazon.android.Kiwi p0)
    {
        p0.enqueueAppLaunchWorkflowTask();
        return;
    }

    static synthetic android.app.Application e(com.amazon.android.Kiwi p1)
    {
        return p1.application;
    }

    private void enqueueAppLaunchWorkflowTask()
    {
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.Kiwi.LOGGER.trace("Enqueuing launch workflow");
        }
        this.taskManager.enqueue(com.amazon.android.framework.task.pipeline.TaskPipelineId.COMMAND, this.getLaunchWorkflow());
        return;
    }

    private com.amazon.android.framework.task.Task getLaunchWorkflow()
    {
        com.amazon.android.h v0_2;
        if (!this.drmEnabled) {
            v0_2 = new com.amazon.android.h();
        } else {
            v0_2 = new com.amazon.android.b();
        }
        return v0_2;
    }

    public static com.amazon.android.framework.prompt.PromptManager getPromptManager()
    {
        return com.amazon.android.Kiwi.INSTANCE.promptManager;
    }

    private static void ignoreEvent(String p3, android.content.Context p4)
    {
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.Kiwi.LOGGER.trace(new StringBuilder().append(p3).append(" called on context: ").append(p4).append(" when ").append("Kiwi is dead, ignoring...").toString());
        }
        return;
    }

    private void instantiateTheWorld(android.app.Application p3)
    {
        com.amazon.android.framework.resource.c v0_1 = new com.amazon.android.framework.resource.c();
        v0_1.a(p3);
        v0_1.a(new com.amazon.android.framework.task.a());
        v0_1.a(new com.amazon.android.o.a());
        v0_1.a(new com.amazon.android.e.c());
        v0_1.a(new com.amazon.android.framework.context.d());
        v0_1.a(new com.amazon.android.framework.prompt.PromptManagerImpl());
        v0_1.a(new com.amazon.android.n.b());
        v0_1.a(new com.amazon.android.c.a());
        v0_1.a(new com.amazon.android.q.c());
        v0_1.a(new com.amazon.android.framework.task.command.b());
        v0_1.a(new com.amazon.android.m.c());
        v0_1.a(new com.amazon.android.framework.task.command.e());
        v0_1.a(new com.amazon.android.g.a());
        v0_1.a();
        v0_1.b(this);
        return;
    }

    private static boolean isInitialized()
    {
        int v0_1;
        if (com.amazon.android.Kiwi.INSTANCE == null) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public static boolean isSignedByKiwi(String p4, String p5)
    {
        int v0_5;
        if (!com.amazon.android.Kiwi.isInitialized()) {
            com.amazon.android.Kiwi.LOGGER.trace("Kiwi was not yet initialized - cannot do the IAP call");
            v0_5 = 0;
        } else {
            try {
                v0_5 = com.amazon.android.m.a.a(p4, p5, com.amazon.android.Kiwi.INSTANCE.authKeyLoader.a());
            } catch (int v0_6) {
                com.amazon.android.Kiwi.LOGGER.trace(new StringBuilder().append("Unable to validate signature: ").append(v0_6.getMessage()).toString());
            }
        }
        return v0_5;
    }

    public static boolean onActivityResult(android.app.Activity p2, int p3, int p4, android.content.Intent p5)
    {
        int v0_2;
        if (!com.amazon.android.Kiwi.preProcess("onActivityResult", p2)) {
            v0_2 = 0;
        } else {
            v0_2 = com.amazon.android.Kiwi.INSTANCE.resultManager.a(new com.amazon.android.e.f(p2, p3, p4, p5));
        }
        return v0_2;
    }

    public static void onCreate(android.app.Activity p7, boolean p8)
    {
        String v0_0 = System.currentTimeMillis();
        if (!com.amazon.android.Kiwi.isInitialized()) {
            com.amazon.android.Kiwi.INSTANCE = new com.amazon.android.Kiwi(p7.getApplication(), p8);
        }
        if (com.amazon.android.Kiwi.preProcess("onCreate", p7)) {
            com.amazon.android.Kiwi.INSTANCE.contextManager.onCreate(p7);
        }
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.Kiwi.LOGGER.trace(new StringBuilder().append("Kiwi.ActivityOnCreate Time: ").append((System.currentTimeMillis() - v0_0)).toString());
        }
        return;
    }

    public static void onCreate(android.app.Service p7, boolean p8)
    {
        String v0_0 = System.currentTimeMillis();
        if (com.amazon.android.Kiwi.preProcess("onCreate", p7)) {
            com.amazon.android.Kiwi.INSTANCE.contextManager.onCreate(p7);
        }
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.Kiwi.LOGGER.trace(new StringBuilder().append("Kiwi.ServiceOnCreate Time: ").append((System.currentTimeMillis() - v0_0)).toString());
        }
        return;
    }

    public static android.app.Dialog onCreateDialog(android.app.Activity p7, int p8)
    {
        int v0_4;
        int v0_0 = System.currentTimeMillis();
        if (!com.amazon.android.Kiwi.preProcess("onCreateDialog", p7)) {
            if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                com.amazon.android.Kiwi.LOGGER.trace(new StringBuilder().append("Kiwi.ActivityOnCreateDialog Time: ").append((System.currentTimeMillis() - v0_0)).toString());
            }
            v0_4 = 0;
        } else {
            v0_4 = com.amazon.android.Kiwi.INSTANCE.promptManager.onCreateDialog(p7, p8);
        }
        return v0_4;
    }

    public static void onDestroy(android.app.Activity p7)
    {
        String v0_0 = System.currentTimeMillis();
        if (com.amazon.android.Kiwi.preProcess("onDestroy", p7)) {
            try {
                com.amazon.android.Kiwi.INSTANCE.contextManager.onDestroy(p7);
            } catch (long v2_4) {
                com.amazon.android.Kiwi.LOGGER.error("Kiwi.ActivityOnDestroy Error: ", v2_4);
            }
        }
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.Kiwi.LOGGER.trace(new StringBuilder().append("Kiwi.ActivityOnDestroy Time: ").append((System.currentTimeMillis() - v0_0)).toString());
        }
        return;
    }

    public static void onDestroy(android.app.Service p7)
    {
        String v0_0 = System.currentTimeMillis();
        if (com.amazon.android.Kiwi.preProcess("onDestroy", p7)) {
            com.amazon.android.Kiwi.INSTANCE.contextManager.onDestroy(p7);
        }
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.Kiwi.LOGGER.trace(new StringBuilder().append("Kiwi.ServiceOnDestroy Time: ").append((System.currentTimeMillis() - v0_0)).toString());
        }
        return;
    }

    public static void onPause(android.app.Activity p7)
    {
        String v0_0 = System.currentTimeMillis();
        if (com.amazon.android.Kiwi.preProcess("onPause", p7)) {
            com.amazon.android.Kiwi.INSTANCE.contextManager.onPause(p7);
        }
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.Kiwi.LOGGER.trace(new StringBuilder().append("Kiwi.ActivityOnPause Time: ").append((System.currentTimeMillis() - v0_0)).toString());
        }
        return;
    }

    public static void onResume(android.app.Activity p7)
    {
        String v0_0 = System.currentTimeMillis();
        if (com.amazon.android.Kiwi.preProcess("onResume", p7)) {
            com.amazon.android.Kiwi.INSTANCE.contextManager.onResume(p7);
        }
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.Kiwi.LOGGER.trace(new StringBuilder().append("Kiwi.ActivityOnResume Time: ").append((System.currentTimeMillis() - v0_0)).toString());
        }
        return;
    }

    public static void onStart(android.app.Activity p7)
    {
        String v0_0 = System.currentTimeMillis();
        if (com.amazon.android.Kiwi.preProcess("onStart", p7)) {
            com.amazon.android.Kiwi.INSTANCE.contextManager.onStart(p7);
        }
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.Kiwi.LOGGER.trace(new StringBuilder().append("Kiwi.ActivityOnStart Time: ").append((System.currentTimeMillis() - v0_0)).toString());
        }
        return;
    }

    public static void onStop(android.app.Activity p7)
    {
        String v0_0 = System.currentTimeMillis();
        if (com.amazon.android.Kiwi.preProcess("onStop", p7)) {
            com.amazon.android.Kiwi.INSTANCE.contextManager.onStop(p7);
        }
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.Kiwi.LOGGER.trace(new StringBuilder().append("Kiwi.ActivityOnStop Time: ").append((System.currentTimeMillis() - v0_0)).toString());
        }
        return;
    }

    public static void onWindowFocusChanged(android.app.Activity p1, boolean p2)
    {
        if (com.amazon.android.Kiwi.preProcess("onWindowFocusChanged", p1)) {
            com.amazon.android.Kiwi.INSTANCE.promptManager.onWindowFocusChanged(p1, p2);
        }
        return;
    }

    private static boolean preProcess(String p3, android.content.Context p4)
    {
        com.amazon.android.d.a.a();
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.Kiwi.LOGGER.trace(new StringBuilder().append(p3).append(": ").append(p4).toString());
        }
        int v0_3;
        if (!com.amazon.android.Kiwi.isInitialized()) {
            com.amazon.android.Kiwi.ignoreEvent(p3, p4);
            v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    private void registerActivityLifeCyclePauseListener()
    {
        this.eventManager.a(new com.amazon.android.d(this));
        return;
    }

    private void registerActivityLifeCycleResumeListener()
    {
        this.eventManager.a(new com.amazon.android.f(this));
        return;
    }

    private void registerApplicationCreationListener()
    {
        this.eventManager.a(new com.amazon.android.g(this));
        return;
    }

    private void registerApplicationDestructionListener()
    {
        this.eventManager.a(new com.amazon.android.e(this));
        return;
    }

    private void registerTestModeReceiver(android.app.Application p4)
    {
        p4.registerReceiver(new com.amazon.android.c(this), new android.content.IntentFilter(new StringBuilder().append(p4.getPackageName()).append(".enable.test.mode").toString()));
        return;
    }

    private static void unsafeAddCommandToCommandTaskPipeline(com.amazon.android.framework.task.command.AbstractCommandTask p2)
    {
        com.amazon.android.Kiwi.INSTANCE.taskManager.enqueue(com.amazon.android.framework.task.pipeline.TaskPipelineId.COMMAND, p2);
        return;
    }

    public final void onResourcesPopulated()
    {
        this.registerApplicationCreationListener();
        this.registerApplicationDestructionListener();
        this.registerActivityLifeCyclePauseListener();
        this.registerActivityLifeCycleResumeListener();
        return;
    }
}
