package com.amazon.android.s;
public class a extends com.amazon.android.framework.exception.KiwiException {
    private static final long serialVersionUID = 1;
    public final com.amazon.android.k.a a;

    public a(com.amazon.android.k.a p4)
    {
        this("LICENSE_VERIFICATION_FAILURE", "VERIFICATION_ERRORS", com.amazon.android.s.a.a(p4));
        com.amazon.android.d.a.a(p4.a(), "Created a verification exception with a Verifier that has no errors");
        this.a = p4;
        return;
    }

    private static String a(com.amazon.android.k.a p3)
    {
        String v0_1 = new StringBuilder();
        java.util.Iterator v1 = p3.iterator();
        while (v1.hasNext()) {
            com.amazon.android.k.b v3_2 = ((com.amazon.android.k.b) v1.next());
            if (v0_1.length() != 0) {
                v0_1.append(",");
            }
            v0_1.append(v3_2.a.a());
        }
        return v0_1.toString();
    }

    public String toString()
    {
        return this.a.toString();
    }
}
