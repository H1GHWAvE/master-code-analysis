package com.amazon.android.r;
public final class a extends com.amazon.android.framework.task.command.AbstractCommandTask {
    private static final com.amazon.android.framework.util.KiwiLogger a;
    private com.amazon.android.c.d b;
    private java.util.List c;

    static a()
    {
        com.amazon.android.r.a.a = new com.amazon.android.framework.util.KiwiLogger("SubmitCrashReportsTask");
        return;
    }

    public a()
    {
        return;
    }

    protected final java.util.Map getCommandData()
    {
        java.util.HashMap v0_1 = new java.util.HashMap();
        java.util.ArrayList v1_1 = new java.util.ArrayList();
        String v2_1 = this.c.iterator();
        while (v2_1.hasNext()) {
            v1_1.add(((com.amazon.android.c.b) v2_1.next()).a());
        }
        v0_1.put("reports", v1_1);
        return v0_1;
    }

    protected final String getCommandName()
    {
        return "submit_crash_reports";
    }

    protected final String getCommandVersion()
    {
        return "1.0";
    }

    protected final boolean isExecutionNeeded()
    {
        int v0_2;
        if (this.c.isEmpty()) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    protected final void onFailure(com.amazon.venezia.command.FailureResult p1)
    {
        return;
    }

    protected final void onSuccess(com.amazon.venezia.command.SuccessResult p3)
    {
        this.b.a(this.c);
        return;
    }

    protected final void preExecution()
    {
        this.c = this.b.a();
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.r.a.a.trace("--------------- Crash Reports -------------------");
            com.amazon.android.r.a.a.trace(new StringBuilder().append("Size: ").append(this.c.size()).toString());
            com.amazon.android.r.a.a.trace("--------------------------------------------------");
        }
        return;
    }
}
