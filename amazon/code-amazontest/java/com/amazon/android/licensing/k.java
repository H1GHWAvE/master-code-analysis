package com.amazon.android.licensing;
public final enum class k extends java.lang.Enum implements com.amazon.android.k.c {
    public static final enum com.amazon.android.licensing.k a;
    public static final enum com.amazon.android.licensing.k b;
    public static final enum com.amazon.android.licensing.k c;
    public static final enum com.amazon.android.licensing.k d;
    public static final enum com.amazon.android.licensing.k e;
    private static final synthetic com.amazon.android.licensing.k[] f;

    static k()
    {
        com.amazon.android.licensing.k.a = new com.amazon.android.licensing.k("EXPIRATION", 0);
        com.amazon.android.licensing.k.b = new com.amazon.android.licensing.k("CUSTOMER_ID", 1);
        com.amazon.android.licensing.k.c = new com.amazon.android.licensing.k("DEVICE_ID", 2);
        com.amazon.android.licensing.k.d = new com.amazon.android.licensing.k("PACKAGE_NAME", 3);
        com.amazon.android.licensing.k.e = new com.amazon.android.licensing.k("CHECKSUM", 4);
        com.amazon.android.licensing.k[] v0_11 = new com.amazon.android.licensing.k[5];
        v0_11[0] = com.amazon.android.licensing.k.a;
        v0_11[1] = com.amazon.android.licensing.k.b;
        v0_11[2] = com.amazon.android.licensing.k.c;
        v0_11[3] = com.amazon.android.licensing.k.d;
        v0_11[4] = com.amazon.android.licensing.k.e;
        com.amazon.android.licensing.k.f = v0_11;
        return;
    }

    private k(String p1, int p2)
    {
        this(p1, p2);
        return;
    }

    public static com.amazon.android.licensing.k valueOf(String p1)
    {
        return ((com.amazon.android.licensing.k) Enum.valueOf(com.amazon.android.licensing.k, p1));
    }

    public static com.amazon.android.licensing.k[] values()
    {
        return ((com.amazon.android.licensing.k[]) com.amazon.android.licensing.k.f.clone());
    }

    public final String a()
    {
        return this.name();
    }
}
