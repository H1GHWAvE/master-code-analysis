package com.amazon.android.licensing;
public final class i {
    public static final com.amazon.android.framework.prompt.PromptContent a;
    public static final com.amazon.android.framework.prompt.PromptContent b;
    public static final com.amazon.android.framework.prompt.PromptContent c;
    public static final com.amazon.android.framework.prompt.PromptContent d;
    public static final com.amazon.android.framework.prompt.PromptContent e;
    public static final com.amazon.android.framework.prompt.PromptContent f;

    static i()
    {
        com.amazon.android.licensing.i.a = new com.amazon.android.framework.prompt.PromptContent("Amazon Appstore required", "It looks like you no longer have an Amazon Appstore on your device. Please install an Amazon Appstore and sign in with your username and password to use this app", "OK", 1, 1);
        com.amazon.android.licensing.i.b = new com.amazon.android.framework.prompt.PromptContent("Amazon Appstore: store connection failure", "An error occurred connecting to Amazon\'s Appstore. Please try again", "OK", 1, 0);
        com.amazon.android.licensing.i.c = new com.amazon.android.framework.prompt.PromptContent("Amazon Appstore required", "Your version of the Amazon Appstore appears to be out of date.  Please visit Amazon.com to install the latest version of the Appstore.", "OK", 1, 1);
        com.amazon.android.licensing.i.d = new com.amazon.android.framework.prompt.PromptContent("Amazon Appstore: internet connection required", "An internet connection is required to launch this app. Please connect to the internet to continue", "OK", 1, 0);
        com.amazon.android.licensing.i.e = new com.amazon.android.framework.prompt.PromptContent("Amazon Appstore: unknown error", "An error occurred. Please download this app again from the Amazon Appstore", "OK", 1, 0);
        com.amazon.android.licensing.i.f = new com.amazon.android.framework.prompt.PromptContent("Amazon Appstore: internal failure", "An internal error occured, please try launching the app again", "OK", 1, 0);
        return;
    }

    private i()
    {
        return;
    }
}
