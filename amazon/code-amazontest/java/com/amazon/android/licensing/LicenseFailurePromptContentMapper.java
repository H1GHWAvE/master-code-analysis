package com.amazon.android.licensing;
public class LicenseFailurePromptContentMapper {
    private final java.util.Map mappings;

    public LicenseFailurePromptContentMapper()
    {
        this.mappings = new java.util.HashMap();
        this.register(com.amazon.android.b.g, com.amazon.android.licensing.i.a);
        this.register(com.amazon.android.b.f, com.amazon.android.licensing.i.a);
        this.register(com.amazon.android.b.a, com.amazon.android.licensing.i.b);
        this.register(com.amazon.android.b.d, com.amazon.android.licensing.i.c);
        this.register(com.amazon.android.b.e, new com.amazon.android.licensing.m());
        this.register(com.amazon.android.b.h, com.amazon.android.licensing.i.e);
        this.register(com.amazon.android.b.b, com.amazon.android.licensing.i.e);
        this.register(com.amazon.android.s.a, new com.amazon.android.licensing.j());
        this.register(com.amazon.android.h.a, com.amazon.android.licensing.i.e);
        this.register(com.amazon.android.h.b, com.amazon.android.licensing.i.e);
        this.register(com.amazon.android.h.c, com.amazon.android.licensing.i.e);
        return;
    }

    private void register(Class p2, com.amazon.android.framework.prompt.PromptContent p3)
    {
        this.register(p2, new com.amazon.android.licensing.g(this, p3));
        return;
    }

    private void register(Class p4, com.amazon.android.licensing.h p5)
    {
        com.amazon.android.d.a.b(this.mappings.containsKey(p4), new StringBuilder().append("mapping exists for type: ").append(p4).toString());
        this.mappings.put(p4, p5);
        return;
    }

    public com.amazon.android.framework.prompt.PromptContent map(com.amazon.android.framework.exception.KiwiException p3)
    {
        com.amazon.android.framework.prompt.PromptContent v0_1;
        com.amazon.android.licensing.h v2_2 = ((com.amazon.android.licensing.h) this.mappings.get(p3.getClass()));
        if (v2_2 != null) {
            v0_1 = v2_2.a(p3);
        } else {
            v0_1 = 0;
        }
        return v0_1;
    }
}
