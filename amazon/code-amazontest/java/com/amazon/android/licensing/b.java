package com.amazon.android.licensing;
public final class b extends com.amazon.android.l.c {
    private static final com.amazon.android.framework.util.KiwiLogger a;
    private com.amazon.android.o.a b;
    private com.amazon.android.framework.prompt.PromptManager c;

    static b()
    {
        com.amazon.android.licensing.b.a = new com.amazon.android.framework.util.KiwiLogger("LicenseKillTask");
        return;
    }

    public b()
    {
        return;
    }

    public final void execute()
    {
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.licensing.b.a.trace("License Kill Task Executing!!!");
        }
        if (this.b.b("APPLICATION_LICENSE") != this.b.b("APPLICATION_LICENSE")) {
            if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                com.amazon.android.licensing.b.a.trace("License Kill Task determined app is not licensed, killing app");
            }
            if (this.isWorkflowChild()) {
                this.quitParentWorkflow();
            }
            com.amazon.android.framework.prompt.PromptManager v0_9 = ((com.amazon.android.framework.prompt.PromptContent) this.b.a("LICENSE_FAILURE_CONTENT"));
            if (v0_9 == null) {
                v0_9 = com.amazon.android.licensing.i.e;
            } else {
                if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                    com.amazon.android.licensing.b.a.trace(new StringBuilder().append("Fetched failure content from store: ").append(v0_9).toString());
                }
                this.b.a.c("LICENSE_FAILURE_CONTENT");
            }
            this.c.present(new com.amazon.android.framework.prompt.c(v0_9));
        } else {
            com.amazon.android.licensing.b.a.test("license verification succeeded");
        }
        return;
    }
}
