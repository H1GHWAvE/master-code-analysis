package com.amazon.android.licensing;
public final class c {
    final String a;
    final String b;
    final String c;

    public c(java.util.Map p4)
    {
        if (p4 != null) {
            this.a = com.amazon.android.licensing.c.a("license", p4);
            this.b = com.amazon.android.licensing.c.a("customerId", p4);
            this.c = com.amazon.android.licensing.c.a("deviceId", p4);
            return;
        } else {
            throw new com.amazon.android.b.h("EMPTY", 0);
        }
    }

    private static String a(String p2, java.util.Map p3)
    {
        String v1_1;
        com.amazon.android.b.h v0_1 = ((String) p3.get(p2));
        if ((v0_1 != null) && (v0_1.length() != 0)) {
            v1_1 = 0;
        } else {
            v1_1 = 1;
        }
        if (v1_1 == null) {
            return v0_1;
        } else {
            throw new com.amazon.android.b.h("MISSING_FIELD", p2);
        }
    }
}
