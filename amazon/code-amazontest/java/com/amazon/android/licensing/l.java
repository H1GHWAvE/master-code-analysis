package com.amazon.android.licensing;
public final class l extends com.amazon.android.framework.task.command.AbstractCommandTask {
    private static final com.amazon.android.framework.util.KiwiLogger a;
    private com.amazon.android.licensing.LicenseFailurePromptContentMapper b;
    private com.amazon.android.q.d c;
    private android.app.Application d;
    private com.amazon.android.framework.task.TaskManager e;
    private com.amazon.android.o.a f;
    private com.amazon.android.m.c g;

    static l()
    {
        com.amazon.android.licensing.l.a = new com.amazon.android.framework.util.KiwiLogger("VerifyApplicationEntitlmentTask");
        return;
    }

    public l()
    {
        this.b = new com.amazon.android.licensing.LicenseFailurePromptContentMapper();
        return;
    }

    protected final java.util.Map getCommandData()
    {
        return 0;
    }

    protected final String getCommandName()
    {
        return "get_license";
    }

    protected final String getCommandVersion()
    {
        return "1.0";
    }

    protected final boolean isExecutionNeeded()
    {
        int v0_2;
        if (this.f.a("APPLICATION_LICENSE") != null) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    protected final void onException(com.amazon.android.framework.exception.KiwiException p5)
    {
        com.amazon.android.framework.prompt.PromptContent v0_1 = this.b.map(p5);
        if ((v0_1 == null) && (com.amazon.android.framework.util.KiwiLogger.ERROR_ON)) {
            com.amazon.android.licensing.l.a.error(new StringBuilder().append("No mapping specified for exception: ").append(p5).toString(), p5);
        }
        this.f.a("LICENSE_FAILURE_CONTENT", v0_1);
        return;
    }

    protected final void onFailure(com.amazon.venezia.command.FailureResult p6)
    {
        com.amazon.android.framework.prompt.PromptContent v0_1 = new com.amazon.android.framework.prompt.PromptContent(p6.getDisplayableName(), p6.getDisplayableMessage(), p6.getButtonLabel(), p6.show());
        com.amazon.android.licensing.l.a.trace(new StringBuilder().append("onFailure: ").append(v0_1).toString());
        this.f.a("LICENSE_FAILURE_CONTENT", v0_1);
        return;
    }

    protected final void onSuccess(com.amazon.venezia.command.SuccessResult p10)
    {
        com.amazon.android.framework.task.TaskManager v0_1 = new com.amazon.android.licensing.c(p10.getData());
        com.amazon.android.framework.task.pipeline.TaskPipelineId v1_4 = new com.amazon.android.licensing.f(new com.amazon.android.m.d(v0_1.a, this.g.a()));
        com.amazon.android.licensing.n v2_3 = new com.amazon.android.k.a();
        v2_3.a(v0_1.b, v1_4.b, com.amazon.android.licensing.k.b);
        v2_3.a(v0_1.c, v1_4.c, com.amazon.android.licensing.k.c);
        v2_3.a(v1_4.e, this.d.getPackageName(), com.amazon.android.licensing.k.d);
        com.amazon.android.framework.task.TaskManager v0_4 = v1_4.d;
        com.amazon.android.licensing.k v3_6 = new java.util.Date();
        com.amazon.android.k.b v4_3 = com.amazon.android.licensing.k.a;
        if (v0_4.compareTo(v3_6) <= 0) {
            v2_3.a.put(v4_3, new com.amazon.android.k.b(v4_3, new StringBuilder().append("\'").append(v0_4).append("\' <= \'").append(v3_6).append("\'").toString()));
        }
        try {
            v2_3.a(v1_4.a, com.amazon.mas.kiwi.util.BC1.getBC1ChecksumBase64(this.d.getPackageCodePath()), com.amazon.android.licensing.k.e);
        } catch (com.amazon.android.framework.task.TaskManager v0_14) {
            com.amazon.android.licensing.k v3_8 = com.amazon.android.licensing.k.e;
            v2_3.a.put(v3_8, new com.amazon.android.k.b(v3_8, new StringBuilder().append("Exception: ").append(v0_14).toString()));
        }
        if (!v2_3.a()) {
            com.amazon.android.licensing.l.a.trace("License Verification succeeded!");
            this.f.a.a("APPLICATION_LICENSE", new com.amazon.android.licensing.d(this, v1_4, v1_4.d));
            this.c.a(new com.amazon.android.p.a());
            this.e.enqueue(com.amazon.android.framework.task.pipeline.TaskPipelineId.BACKGROUND, new com.amazon.android.licensing.n());
            return;
        } else {
            throw new com.amazon.android.s.a(v2_3);
        }
    }
}
