package com.amazon.android.licensing;
public final class n implements com.amazon.android.framework.task.Task {
    private static final com.amazon.android.framework.util.KiwiLogger a;

    static n()
    {
        com.amazon.android.licensing.n.a = new com.amazon.android.framework.util.KiwiLogger("DRMSuccessTask");
        return;
    }

    public n()
    {
        return;
    }

    private static reflect.Method a(Class p5, String p6)
    {
        com.amazon.android.d.a.a(p5, "Class<?> target");
        com.amazon.android.d.a.a(p6, "String methodName");
        try {
            int v0_3 = new Class[0];
            int v0_4 = p5.getDeclaredMethod(p6, v0_3);
        } catch (int v0) {
            if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                com.amazon.android.licensing.n.a.trace(new StringBuilder().append("Did not find method ").append(p6).toString());
            }
            v0_4 = 0;
            return v0_4;
        }
        if (v0_4 != 0) {
            v0_4.setAccessible(1);
            if (com.amazon.android.framework.util.d.c(v0_4)) {
                if (com.amazon.android.framework.util.d.b(v0_4)) {
                    if (!com.amazon.android.framework.util.d.a(v0_4)) {
                        return v0_4;
                    } else {
                        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                            com.amazon.android.licensing.n.a.trace(new StringBuilder().append("Callback ").append(p6).append(" takes parameters, ignoring...").toString());
                        }
                        v0_4 = 0;
                        return v0_4;
                    }
                } else {
                    if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                        com.amazon.android.licensing.n.a.trace(new StringBuilder().append("Callback ").append(p6).append(" returns a value, ignoring...").toString());
                    }
                    v0_4 = 0;
                    return v0_4;
                }
            } else {
                if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                    com.amazon.android.licensing.n.a.trace(new StringBuilder().append("Callback ").append(p6).append(" isn\'t static, ignoring...").toString());
                }
                v0_4 = 0;
                return v0_4;
            }
        } else {
            if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                com.amazon.android.licensing.n.a.trace(new StringBuilder().append("No exception thrown, but method \'").append(p6).append("\' was not found, this should not happen. ").toString());
            }
            v0_4 = 0;
            return v0_4;
        }
    }

    public final void execute()
    {
        com.amazon.android.framework.util.KiwiLogger v0_1 = com.amazon.android.framework.util.d.a("com.amazon.drm.AmazonLicenseVerificationCallback");
        if (v0_1 != null) {
            com.amazon.android.framework.util.KiwiLogger v0_2 = com.amazon.android.licensing.n.a(v0_1, "onDRMSuccess");
            if (v0_2 != null) {
                if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                    com.amazon.android.licensing.n.a.trace(new StringBuilder().append("Invoking callback: ").append(v0_2.getName()).toString());
                }
                try {
                    Object[] v2_6 = new Object[0];
                    v0_2.invoke(0, v2_6);
                } catch (com.amazon.android.framework.util.KiwiLogger v0) {
                }
                if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                    com.amazon.android.licensing.n.a.trace("Callback invoked.");
                }
            }
        }
        return;
    }
}
