package com.amazon.android.licensing;
public final class f {
    final String a;
    final String b;
    final String c;
    final java.util.Date d;
    final String e;

    public f(com.amazon.android.m.d p2)
    {
        this.a = com.amazon.android.licensing.f.a("checksum", p2);
        this.b = com.amazon.android.licensing.f.a("customerId", p2);
        this.c = com.amazon.android.licensing.f.a("deviceId", p2);
        this.e = com.amazon.android.licensing.f.a("packageName", p2);
        this.d = com.amazon.android.licensing.f.b("expiration", p2);
        return;
    }

    private static String a(String p2, com.amazon.android.m.d p3)
    {
        com.amazon.android.h.b v0_0 = p3.a(p2);
        if (!com.amazon.android.framework.util.b.a(v0_0)) {
            return v0_0;
        } else {
            throw new com.amazon.android.h.b("MISSING_FIELD", p2);
        }
    }

    private static java.util.Date b(String p5, com.amazon.android.m.d p6)
    {
        String v0_0 = com.amazon.android.licensing.f.a(p5, p6);
        try {
            return new java.util.Date(Long.parseLong(v0_0));
        } catch (com.amazon.android.h.b v1) {
            throw new com.amazon.android.h.b("INVALID_FIELD_VALUE", new StringBuilder().append(p5).append(":").append(v0_0).toString());
        }
    }
}
