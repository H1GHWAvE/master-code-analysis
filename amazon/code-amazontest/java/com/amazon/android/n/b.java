package com.amazon.android.n;
public final class b implements com.amazon.android.n.g {
    private static final com.amazon.android.framework.util.KiwiLogger a;
    private final java.util.Map b;

    static b()
    {
        com.amazon.android.n.b.a = new com.amazon.android.framework.util.KiwiLogger("EventManagerImpl");
        return;
    }

    public b()
    {
        this.b = new java.util.HashMap();
        return;
    }

    public final void a(com.amazon.android.n.c p5)
    {
        com.amazon.android.d.a.a(p5, "listener");
        com.amazon.android.d.a.a();
        com.amazon.android.n.f v1 = p5.a();
        com.amazon.android.n.b.a.trace(new StringBuilder().append("Registering listener for event: ").append(v1).append(", ").append(p5).toString());
        com.amazon.android.n.e v0_4 = ((com.amazon.android.n.e) this.b.get(v1));
        if (v0_4 == null) {
            v0_4 = new com.amazon.android.n.e();
            this.b.put(v1, v0_4);
        }
        v0_4.a(p5);
        return;
    }

    public final void a(com.amazon.android.n.d p5)
    {
        com.amazon.android.framework.util.KiwiLogger v0_0 = p5.a();
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.n.b.a.trace(new StringBuilder().append("Posting event: ").append(v0_0).toString());
        }
        if (this.b.containsKey(v0_0)) {
            ((com.amazon.android.n.e) this.b.get(v0_0)).a(p5);
        } else {
            if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                com.amazon.android.n.b.a.trace("No registered listeners, returning");
            }
        }
        return;
    }
}
