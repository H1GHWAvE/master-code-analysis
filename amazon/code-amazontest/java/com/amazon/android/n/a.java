package com.amazon.android.n;
public final enum class a extends java.lang.Enum {
    public static final enum com.amazon.android.n.a a;
    public static final enum com.amazon.android.n.a b;
    public static final enum com.amazon.android.n.a c;
    private static final synthetic com.amazon.android.n.a[] d;

    static a()
    {
        com.amazon.android.n.a.a = new com.amazon.android.n.a("FIRST", 0);
        com.amazon.android.n.a.b = new com.amazon.android.n.a("MIDDLE", 1);
        com.amazon.android.n.a.c = new com.amazon.android.n.a("LAST", 2);
        com.amazon.android.n.a[] v0_7 = new com.amazon.android.n.a[3];
        v0_7[0] = com.amazon.android.n.a.a;
        v0_7[1] = com.amazon.android.n.a.b;
        v0_7[2] = com.amazon.android.n.a.c;
        com.amazon.android.n.a.d = v0_7;
        return;
    }

    private a(String p1, int p2)
    {
        this(p1, p2);
        return;
    }

    public static com.amazon.android.n.a valueOf(String p1)
    {
        return ((com.amazon.android.n.a) Enum.valueOf(com.amazon.android.n.a, p1));
    }

    public static com.amazon.android.n.a[] values()
    {
        return ((com.amazon.android.n.a[]) com.amazon.android.n.a.d.clone());
    }
}
