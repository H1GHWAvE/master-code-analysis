package com.amazon.android.n;
public final class e implements java.util.Comparator {
    private static final com.amazon.android.framework.util.KiwiLogger a;
    private java.util.List b;

    static e()
    {
        com.amazon.android.n.e.a = new com.amazon.android.framework.util.KiwiLogger("EventListenerNotificationQueue");
        return;
    }

    public e()
    {
        this.b = new java.util.ArrayList();
        return;
    }

    public final void a(com.amazon.android.n.c p4)
    {
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.n.e.a.trace(new StringBuilder().append("Adding listener: ").append(p4).toString());
        }
        this.b.add(p4);
        java.util.Collections.sort(this.b, this);
        return;
    }

    public final void a(com.amazon.android.n.d p5)
    {
        java.util.Iterator v0_1 = this.b.iterator();
        while (v0_1.hasNext()) {
            com.amazon.android.n.c v4_2 = ((com.amazon.android.n.c) v0_1.next());
            if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
                com.amazon.android.n.e.a.trace(new StringBuilder().append("Notifying listener: ").append(v4_2).toString());
            }
            v4_2.a(p5);
        }
        return;
    }

    public final bridge synthetic int compare(Object p3, Object p4)
    {
        return ((com.amazon.android.n.c) p3).b().compareTo(((com.amazon.android.n.c) p4).b());
    }
}
