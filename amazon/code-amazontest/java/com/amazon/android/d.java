package com.amazon.android;
final class d implements com.amazon.android.n.c {
    private synthetic com.amazon.android.Kiwi a;

    d(com.amazon.android.Kiwi p1)
    {
        this.a = p1;
        return;
    }

    public final com.amazon.android.n.f a()
    {
        return com.amazon.android.j.c.d;
    }

    public final bridge synthetic void a(com.amazon.android.n.d p5)
    {
        com.amazon.android.framework.util.KiwiLogger v0_1 = new java.util.HashMap();
        v0_1.put("EventName", com.amazon.android.j.c.d.name());
        v0_1.put("ActivityName", ((com.amazon.android.j.b) p5).a.getClass().getName());
        v0_1.put("Timestamp", Long.valueOf(System.currentTimeMillis()));
        com.amazon.android.t.a v1_4 = new com.amazon.android.t.a(v0_1);
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.Kiwi.a().trace("Adding lifecycle PAUSE command to pipeline");
        }
        com.amazon.android.Kiwi.a(v1_4);
        return;
    }

    public final com.amazon.android.n.a b()
    {
        return com.amazon.android.n.a.b;
    }
}
