package com.amazon.android.k;
public final class a implements java.lang.Iterable {
    public java.util.Map a;

    public a()
    {
        this.a = new java.util.HashMap();
        return;
    }

    public final com.amazon.android.k.a a(Object p5, Object p6, com.amazon.android.k.c p7)
    {
        com.amazon.android.k.b v0_0;
        if (p5 != null) {
            v0_0 = p5.equals(p6);
        } else {
            if (p6 != null) {
                v0_0 = 0;
            } else {
                v0_0 = 1;
            }
        }
        if (v0_0 == null) {
            this.a.put(p7, new com.amazon.android.k.b(p7, new StringBuilder().append("\'").append(p5).append("\' != \'").append(p6).append("\'").toString()));
        }
        return this;
    }

    public final boolean a()
    {
        int v0_2;
        if (this.a.isEmpty()) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public final java.util.Iterator iterator()
    {
        return this.a.values().iterator();
    }

    public final String toString()
    {
        String v0_1 = new StringBuilder("Verifier:");
        java.util.Iterator v1_3 = this.a.values().iterator();
        while (v1_3.hasNext()) {
            v0_1.append(new StringBuilder().append("\n\t").append(((com.amazon.android.k.b) v1_3.next())).toString());
        }
        return v0_1.toString();
    }
}
