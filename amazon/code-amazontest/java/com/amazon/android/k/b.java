package com.amazon.android.k;
public final class b {
    public final com.amazon.android.k.c a;
    private final String b;

    public b(com.amazon.android.k.c p1, String p2)
    {
        this.a = p1;
        this.b = p2;
        return;
    }

    public final String toString()
    {
        return new StringBuilder().append(this.a.a()).append(": ").append(this.b).toString();
    }
}
