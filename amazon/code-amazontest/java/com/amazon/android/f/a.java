package com.amazon.android.f;
public final class a extends com.amazon.android.framework.task.command.AbstractCommandTask {
    private static final com.amazon.android.framework.util.KiwiLogger a;
    private com.amazon.android.q.d b;
    private com.amazon.android.q.a c;

    static a()
    {
        com.amazon.android.f.a.a = new com.amazon.android.framework.util.KiwiLogger("SubmitMetricsTask");
        return;
    }

    public a()
    {
        return;
    }

    protected final java.util.Map getCommandData()
    {
        java.util.HashMap v0_1 = new java.util.HashMap();
        java.util.ArrayList v1_1 = new java.util.ArrayList(this.c.b());
        String v2_3 = this.c.iterator();
        while (v2_3.hasNext()) {
            v1_1.add(((com.amazon.android.q.b) v2_3.next()).a);
        }
        v0_1.put("metrics", v1_1);
        return v0_1;
    }

    protected final String getCommandName()
    {
        return "submit_metrics";
    }

    protected final String getCommandVersion()
    {
        return "1.0";
    }

    protected final boolean isExecutionNeeded()
    {
        int v0_2;
        if (this.c.a()) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    protected final void onFailure(com.amazon.venezia.command.FailureResult p1)
    {
        return;
    }

    protected final void onSuccess(com.amazon.venezia.command.SuccessResult p1)
    {
        return;
    }

    protected final void preExecution()
    {
        this.c = this.b.a();
        if (com.amazon.android.framework.util.KiwiLogger.TRACE_ON) {
            com.amazon.android.f.a.a.trace("--------------- SUBMIT METRICS -------------------");
            com.amazon.android.f.a.a.trace(new StringBuilder().append("Size: ").append(this.c.b()).toString());
            com.amazon.android.f.a.a.trace("--------------------------------------------------");
        }
        return;
    }
}
