package com.amazon.android.u;
public final class a {
    private static final com.amazon.android.framework.util.KiwiLogger a;

    static a()
    {
        com.amazon.android.u.a.a = new com.amazon.android.framework.util.KiwiLogger("Serializer");
        return;
    }

    public a()
    {
        return;
    }

    public static Object a(String p6)
    {
        if ((p6 != null) && (p6.length() != 0)) {
            try {
            } catch (int v0_3) {
                if (com.amazon.android.framework.util.KiwiLogger.ERROR_ON) {
                    com.amazon.android.u.a.a.error("Could not decode string", v0_3);
                }
                int v0_4 = 0;
            }
            try {
                int v1_3 = new java.io.ObjectInputStream(new java.io.ByteArrayInputStream(com.amazon.mas.kiwi.util.Base64.decode(p6.getBytes())));
            } catch (int v0_5) {
                v1_3 = 0;
                if (com.amazon.android.framework.util.KiwiLogger.ERROR_ON) {
                    com.amazon.android.u.a.a.error(new StringBuilder().append("Could not read object from string: ").append(p6).toString(), v0_5);
                }
                com.amazon.android.framework.util.a.a(v1_3);
                v0_4 = 0;
            } catch (int v0_6) {
                v1_3 = 0;
                com.amazon.android.framework.util.a.a(v1_3);
                throw v0_6;
            } catch (int v0_6) {
            }
            try {
                v0_4 = v1_3.readObject();
                com.amazon.android.framework.util.a.a(v1_3);
            } catch (int v0_5) {
            }
        } else {
            v0_4 = 0;
        }
        return v0_4;
    }

    public static String a(java.io.Serializable p6)
    {
        int v0_5;
        if (p6 != null) {
            int v0_1 = new java.io.ByteArrayOutputStream();
            try {
                int v1_1 = new java.io.ObjectOutputStream(v0_1);
                try {
                    v1_1.writeObject(p6);
                    v0_5 = com.amazon.mas.kiwi.util.Base64.encodeBytes(v0_1.toByteArray());
                    com.amazon.android.framework.util.a.a(v1_1);
                } catch (int v0_2) {
                    if (com.amazon.android.framework.util.KiwiLogger.ERROR_ON) {
                        com.amazon.android.u.a.a.error(new StringBuilder().append("Could not serialize object: ").append(p6).toString(), v0_2);
                    }
                    com.amazon.android.framework.util.a.a(v1_1);
                    v0_5 = 0;
                }
            } catch (int v0_2) {
                v1_1 = 0;
            } catch (int v0_3) {
                v1_1 = 0;
                com.amazon.android.framework.util.a.a(v1_1);
                throw v0_3;
            } catch (int v0_3) {
            }
        } else {
            v0_5 = 0;
        }
        return v0_5;
    }
}
