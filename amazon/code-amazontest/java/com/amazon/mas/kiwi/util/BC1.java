package com.amazon.mas.kiwi.util;
public class BC1 {
    private static String CHECKSUM_ALGORITHM = "None";
    private static final int DIGEST_UPDATE_BUFFER_SIZE = 10240;
    private static final java.util.Set DIRECTORIES_TO_IGNORE_FOR_PARTIAL_CHECKSUM = None;
    private static final int MAX_SIZE_FOR_FULL_CHECKSUM = 20971520;

    static BC1()
    {
        com.amazon.mas.kiwi.util.BC1.CHECKSUM_ALGORITHM = "MD5";
        com.amazon.mas.kiwi.util.BC1.DIRECTORIES_TO_IGNORE_FOR_PARTIAL_CHECKSUM = new com.amazon.mas.kiwi.util.BC1$1();
        return;
    }

    public BC1()
    {
        return;
    }

    private static void calculateFullChecksum(java.io.File p1, java.security.MessageDigest p2)
    {
        com.amazon.mas.kiwi.util.BC1.updateMessageDigestWithInputStream(new java.io.FileInputStream(p1), p2);
        return;
    }

    private static void calculatePartialChecksum(java.io.File p5, java.security.MessageDigest p6, byte[] p7)
    {
        java.util.jar.JarFile v3_1 = new java.util.jar.JarFile(p5);
        java.util.Enumeration v0 = v3_1.entries();
        while (v0.hasMoreElements()) {
            java.util.jar.JarEntry v1_1 = ((java.util.jar.JarEntry) v0.nextElement());
            if (!com.amazon.mas.kiwi.util.BC1.isInDirectoryToIgnore(v1_1)) {
                try {
                    java.io.InputStream v2 = v3_1.getInputStream(v1_1);
                    com.amazon.mas.kiwi.util.BC1.updateMessageDigestWithInputStream(v2, p6, p7);
                    com.amazon.mas.kiwi.util.BC1.closeIgnoreException(v2);
                } catch (Throwable v4_2) {
                    com.amazon.mas.kiwi.util.BC1.closeIgnoreException(v2);
                    throw v4_2;
                }
            }
        }
        com.amazon.mas.kiwi.util.BC1.closeIgnoreException(v3_1);
        return;
    }

    private static void closeIgnoreException(java.io.InputStream p1)
    {
        if (p1 != null) {
            try {
                p1.close();
            } catch (java.io.IOException v0) {
            }
        }
        return;
    }

    private static void closeIgnoreException(java.util.zip.ZipFile p1)
    {
        if (p1 != null) {
            try {
                p1.close();
            } catch (java.io.IOException v0) {
            }
        }
        return;
    }

    public static byte[] getBC1Checksum(java.io.File p2)
    {
        java.security.MessageDigest v0 = com.amazon.mas.kiwi.util.BC1.getMessageDigest();
        if (!com.amazon.mas.kiwi.util.BC1.isTooLargeForFullChecksum(p2)) {
            com.amazon.mas.kiwi.util.BC1.calculateFullChecksum(p2, v0);
        } else {
            byte[] v1_2 = new byte[10240];
            com.amazon.mas.kiwi.util.BC1.calculatePartialChecksum(p2, v0, v1_2);
        }
        return v0.digest();
    }

    public static byte[] getBC1Checksum(String p4)
    {
        if (p4 != null) {
            java.io.File v0_1 = new java.io.File(p4);
            if (v0_1.exists()) {
                return com.amazon.mas.kiwi.util.BC1.getBC1Checksum(v0_1);
            } else {
                throw new java.io.IOException(new StringBuilder().append("Cannot calculate checksum, file does not exist: ").append(p4).toString());
            }
        } else {
            throw new IllegalArgumentException("Given path is null");
        }
    }

    public static String getBC1ChecksumBase64(String p2)
    {
        return com.amazon.mas.kiwi.util.BC1.toBase64(com.amazon.mas.kiwi.util.BC1.getBC1Checksum(p2));
    }

    private static java.security.MessageDigest getMessageDigest()
    {
        try {
            return java.security.MessageDigest.getInstance(com.amazon.mas.kiwi.util.BC1.CHECKSUM_ALGORITHM);
        } catch (IllegalStateException v1_2) {
            throw new IllegalStateException(v1_2);
        }
    }

    private static boolean isInDirectoryToIgnore(java.util.jar.JarEntry p3)
    {
        java.util.Iterator v0 = com.amazon.mas.kiwi.util.BC1.DIRECTORIES_TO_IGNORE_FOR_PARTIAL_CHECKSUM.iterator();
        while (v0.hasNext()) {
            if (p3.getName().startsWith(((String) v0.next()))) {
                int v2_2 = 1;
            }
            return v2_2;
        }
        v2_2 = 0;
        return v2_2;
    }

    private static boolean isTooLargeForFullChecksum(java.io.File p4)
    {
        int v0_2;
        if (p4.length() < 20971520) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    private static String toBase64(byte[] p2)
    {
        return new String(com.amazon.mas.kiwi.util.Base64.encodeBytes(p2));
    }

    private static void updateMessageDigestWithInputStream(java.io.InputStream p2, java.security.MessageDigest p3)
    {
        byte[] v0 = new byte[10240];
        com.amazon.mas.kiwi.util.BC1.updateMessageDigestWithInputStream(p2, p3, v0);
        return;
    }

    private static void updateMessageDigestWithInputStream(java.io.InputStream p2, java.security.MessageDigest p3, byte[] p4)
    {
        while(true) {
            int v0 = p2.read(p4);
            if (v0 == -1) {
                break;
            }
            p3.update(p4, 0, v0);
        }
        return;
    }
}
