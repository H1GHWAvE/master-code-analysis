package com.amazon.mas.kiwi.util;
public class Base64 {
    static final synthetic boolean $assertionsDisabled = False;
    public static final int DECODE = 0;
    public static final int DONT_GUNZIP = 4;
    public static final int DO_BREAK_LINES = 8;
    public static final int ENCODE = 1;
    private static final byte EQUALS_SIGN = 0x3d;
    private static final byte EQUALS_SIGN_ENC = 0x-1;
    public static final int GZIP = 2;
    private static final int MAX_LINE_LENGTH = 76;
    private static final byte NEW_LINE = 0xa;
    public static final int NO_OPTIONS = 0;
    public static final int ORDERED = 32;
    private static final String PREFERRED_ENCODING = "US-ASCII";
    public static final int URL_SAFE = 16;
    private static final byte WHITE_SPACE_ENC = 0x-5;
    private static final byte[] _ORDERED_ALPHABET;
    private static final byte[] _ORDERED_DECODABET;
    private static final byte[] _STANDARD_ALPHABET;
    private static final byte[] _STANDARD_DECODABET;
    private static final byte[] _URL_SAFE_ALPHABET;
    private static final byte[] _URL_SAFE_DECODABET;

    static Base64()
    {
        byte[] v0_2;
        if (com.amazon.mas.kiwi.util.Base64.desiredAssertionStatus()) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        com.amazon.mas.kiwi.util.Base64.$assertionsDisabled = v0_2;
        byte[] v0_3 = new byte[64];
        v0_3 = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 43, 47};
        com.amazon.mas.kiwi.util.Base64._STANDARD_ALPHABET = v0_3;
        byte[] v0_4 = new byte[256];
        v0_4 = {-9, -9, -9, -9, -9, -9, -9, -9, -9, -5, -5, -9, -9, -5, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -5, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, 62, -9, -9, -9, 63, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -9, -9, -9, -1, -9, -9, -9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -9, -9, -9, -9, -9, -9, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9};
        com.amazon.mas.kiwi.util.Base64._STANDARD_DECODABET = v0_4;
        byte[] v0_5 = new byte[64];
        v0_5 = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 45, 95};
        com.amazon.mas.kiwi.util.Base64._URL_SAFE_ALPHABET = v0_5;
        byte[] v0_6 = new byte[256];
        v0_6 = {-9, -9, -9, -9, -9, -9, -9, -9, -9, -5, -5, -9, -9, -5, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -5, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, 62, -9, -9, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -9, -9, -9, -1, -9, -9, -9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -9, -9, -9, -9, 63, -9, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9};
        com.amazon.mas.kiwi.util.Base64._URL_SAFE_DECODABET = v0_6;
        byte[] v0_7 = new byte[64];
        v0_7 = {45, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 95, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122};
        com.amazon.mas.kiwi.util.Base64._ORDERED_ALPHABET = v0_7;
        byte[] v0_9 = new byte[257];
        v0_9 = {-9, -9, -9, -9, -9, -9, -9, -9, -9, -5, -5, -9, -9, -5, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -5, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, 0, -9, -9, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, -9, -9, -9, -1, -9, -9, -9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, -9, -9, -9, -9, 37, -9, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9};
        com.amazon.mas.kiwi.util.Base64._ORDERED_DECODABET = v0_9;
        return;
    }

    private Base64()
    {
        return;
    }

    static synthetic byte[] access$000(int p1)
    {
        return com.amazon.mas.kiwi.util.Base64.getDecodabet(p1);
    }

    static synthetic byte[] access$100(byte[] p1, int p2, int p3, byte[] p4, int p5, int p6)
    {
        return com.amazon.mas.kiwi.util.Base64.encode3to4(p1, p2, p3, p4, p5, p6);
    }

    static synthetic int access$200(byte[] p1, int p2, byte[] p3, int p4, int p5)
    {
        return com.amazon.mas.kiwi.util.Base64.decode4to3(p1, p2, p3, p4, p5);
    }

    static synthetic byte[] access$300(byte[] p1, byte[] p2, int p3, int p4)
    {
        return com.amazon.mas.kiwi.util.Base64.encode3to4(p1, p2, p3, p4);
    }

    public static byte[] decode(String p1)
    {
        return com.amazon.mas.kiwi.util.Base64.decode(p1, 0);
    }

    public static byte[] decode(String p7, int p8)
    {
        if (p7 != null) {
            try {
                Throwable v7_1 = p7.getBytes("US-ASCII");
            } catch (Exception v0) {
                v7_1 = v7_1.getBytes();
            }
            Throwable v7_3;
            Throwable v2_0 = com.amazon.mas.kiwi.util.Base64.decode(v7_1, 0, v7_1.length, p8);
            if ((p8 & 4) == 0) {
                v7_3 = 0;
            } else {
                v7_3 = 1;
            }
            if ((v2_0 == null) || ((v2_0.length < 4) || ((v7_3 != null) || (35615 != ((v2_0[0] & 255) | ((v2_0[1] << 8) & 65280)))))) {
                Throwable v7_8 = v2_0;
            } else {
                Throwable v7_9 = 0;
                Exception v8_7 = 0;
                java.util.zip.GZIPInputStream v1_1 = new byte[2048];
                try {
                    Exception v0_6 = new java.io.ByteArrayOutputStream();
                    try {
                        Exception v8_9 = new java.io.ByteArrayInputStream(v2_0);
                    } catch (Exception v8_11) {
                        Throwable v2_1 = v8_11;
                        v8_7 = v0_6;
                        Exception v0_9 = 0;
                        try {
                            v8_7.close();
                        } catch (Exception v8) {
                        }
                        try {
                            v0_9.close();
                        } catch (Exception v8) {
                        }
                        try {
                            v7_9.close();
                        } catch (Throwable v7) {
                        }
                        throw v2_1;
                    } catch (Exception v8_10) {
                        java.util.zip.GZIPInputStream v1_2 = 0;
                        Exception v0_7 = v8_10;
                        v8_7 = v0_6;
                        try {
                            v0_7.printStackTrace();
                            try {
                                v8_7.close();
                            } catch (Exception v8) {
                            }
                            try {
                                v1_2.close();
                                try {
                                    v7_9.close();
                                    v7_8 = v2_0;
                                } catch (Throwable v7) {
                                    v7_8 = v2_0;
                                }
                            } catch (Exception v8) {
                            }
                        } catch (Exception v0_10) {
                            v2_1 = v0_10;
                            v0_9 = v1_2;
                        }
                    }
                    try {
                        java.util.zip.GZIPInputStream v4_1 = new java.util.zip.GZIPInputStream(v8_9);
                        try {
                            while(true) {
                                v7_8 = v4_1.read(v1_1);
                                v0_6.write(v1_1, 0, v7_8);
                            }
                            v7_8 = v0_6.toByteArray();
                            try {
                                v0_6.close();
                            } catch (Exception v0) {
                            }
                            try {
                                v4_1.close();
                                try {
                                    v8_9.close();
                                } catch (Exception v8) {
                                }
                            } catch (Exception v0) {
                            }
                        } catch (java.util.zip.GZIPInputStream v1_3) {
                            v7_9 = v8_9;
                            v8_7 = v0_6;
                            v0_7 = v1_3;
                            v1_2 = v4_1;
                        } catch (java.util.zip.GZIPInputStream v1_4) {
                            v2_1 = v1_4;
                            v7_9 = v8_9;
                            v8_7 = v0_6;
                            v0_9 = v4_1;
                        }
                        if (v7_8 < null) {
                        }
                    } catch (Throwable v7_11) {
                        v2_1 = v7_11;
                        v7_9 = v8_9;
                        v8_7 = v0_6;
                        v0_9 = 0;
                    } catch (Throwable v7_10) {
                        v1_2 = 0;
                        v0_7 = v7_10;
                        v7_9 = v8_9;
                        v8_7 = v0_6;
                    }
                } catch (Exception v0_7) {
                    v1_2 = 0;
                } catch (Exception v0_8) {
                    v2_1 = v0_8;
                    v0_9 = 0;
                }
            }
            return v7_8;
        } else {
            throw new NullPointerException("Input string was null.");
        }
    }

    public static byte[] decode(byte[] p3)
    {
        return com.amazon.mas.kiwi.util.Base64.decode(p3, 0, p3.length, 0);
    }

    public static byte[] decode(byte[] p10, int p11, int p12, int p13)
    {
        if (p10 != 0) {
            if ((p11 >= 0) && ((p11 + p12) <= p10.length)) {
                byte v10_6;
                if (p12 != 0) {
                    if (p12 >= 4) {
                        Integer v0_2 = com.amazon.mas.kiwi.util.Base64.getDecodabet(p13);
                        byte[] v5 = new byte[((p12 * 3) / 4)];
                        byte[] v1_4 = new byte[4];
                        int v6_1 = 0;
                        int v4_1 = p11;
                        int v3_1 = 0;
                        while (v4_1 < (p11 + p12)) {
                            byte v7_1 = v0_2[(p10[v4_1] & 255)];
                            if (v7_1 < -5) {
                                int v13_2 = new Object[2];
                                v13_2[0] = Integer.valueOf((p10[v4_1] & 255));
                                v13_2[1] = Integer.valueOf(v4_1);
                                throw new java.io.IOException(String.format("Bad Base64 input character decimal %d in array position %d", v13_2));
                            } else {
                                byte v2_6;
                                if (v7_1 < -1) {
                                    v2_6 = v3_1;
                                } else {
                                    v2_6 = (v3_1 + 1);
                                    v1_4[v3_1] = p10[v4_1];
                                    if (v2_6 > 3) {
                                        int v3_3 = (v6_1 + com.amazon.mas.kiwi.util.Base64.decode4to3(v1_4, 0, v5, v6_1, p13));
                                        v2_6 = 0;
                                        if (p10[v4_1] != 61) {
                                            v6_1 = v3_3;
                                        } else {
                                            int v11_1 = v3_3;
                                            v10_6 = new byte[v11_1];
                                            System.arraycopy(v5, 0, v10_6, 0, v11_1);
                                            return v10_6;
                                        }
                                    }
                                }
                                v4_1++;
                                v3_1 = v2_6;
                            }
                        }
                        v11_1 = v6_1;
                    } else {
                        throw new IllegalArgumentException(new StringBuilder().append("Base64-encoded string must have at least four characters, but length specified was ").append(p12).toString());
                    }
                } else {
                    v10_6 = new byte[0];
                }
                return v10_6;
            } else {
                byte[] v1_6 = new Object[3];
                v1_6[0] = Integer.valueOf(p10.length);
                v1_6[1] = Integer.valueOf(p11);
                v1_6[2] = Integer.valueOf(p12);
                throw new IllegalArgumentException(String.format("Source array with length %d cannot have offset of %d and process %d bytes.", v1_6));
            }
        } else {
            throw new NullPointerException("Cannot decode null source array.");
        }
    }

    private static int decode4to3(byte[] p8, int p9, byte[] p10, int p11, int p12)
    {
        if (p8 != null) {
            if (p10 != null) {
                if ((p9 >= 0) && ((p9 + 3) < p8.length)) {
                    if ((p11 >= 0) && ((p11 + 2) < p10.length)) {
                        int v2_16;
                        byte[] v0 = com.amazon.mas.kiwi.util.Base64.getDecodabet(p12);
                        if (p8[(p9 + 2)] != 61) {
                            if (p8[(p9 + 3)] != 61) {
                                int v1_0 = (((((v0[p8[p9]] & 255) << 18) | ((v0[p8[(p9 + 1)]] & 255) << 12)) | ((v0[p8[(p9 + 2)]] & 255) << 6)) | (v0[p8[(p9 + 3)]] & 255));
                                p10[p11] = ((byte) (v1_0 >> 16));
                                p10[(p11 + 1)] = ((byte) (v1_0 >> 8));
                                p10[(p11 + 2)] = ((byte) v1_0);
                                v2_16 = 3;
                            } else {
                                int v1_1 = ((((v0[p8[p9]] & 255) << 18) | ((v0[p8[(p9 + 1)]] & 255) << 12)) | ((v0[p8[(p9 + 2)]] & 255) << 6));
                                p10[p11] = ((byte) (v1_1 >> 16));
                                p10[(p11 + 1)] = ((byte) (v1_1 >> 8));
                                v2_16 = 2;
                            }
                        } else {
                            p10[p11] = ((byte) ((((v0[p8[p9]] & 255) << 18) | ((v0[p8[(p9 + 1)]] & 255) << 12)) >> 16));
                            v2_16 = 1;
                        }
                        return v2_16;
                    } else {
                        Object[] v4_1 = new Object[2];
                        v4_1[0] = Integer.valueOf(p10.length);
                        v4_1[1] = Integer.valueOf(p11);
                        throw new IllegalArgumentException(String.format("Destination array with length %d cannot have offset of %d and still store three bytes.", v4_1));
                    }
                } else {
                    Object[] v4_2 = new Object[2];
                    v4_2[0] = Integer.valueOf(p8.length);
                    v4_2[1] = Integer.valueOf(p9);
                    throw new IllegalArgumentException(String.format("Source array with length %d cannot have offset of %d and still process four bytes.", v4_2));
                }
            } else {
                throw new NullPointerException("Destination array was null.");
            }
        } else {
            throw new NullPointerException("Source array was null.");
        }
    }

    public static void decodeFileToFile(String p6, String p7)
    {
        byte[] v0 = com.amazon.mas.kiwi.util.Base64.decodeFromFile(p6);
        java.io.BufferedOutputStream v2 = 0;
        try {
            java.io.BufferedOutputStream v3_1 = new java.io.BufferedOutputStream(new java.io.FileOutputStream(p7));
        } catch (Exception v4_2) {
            java.io.IOException v1 = v4_2;
            throw v1;
        } catch (Exception v4_4) {
            try {
                v2.close();
            } catch (Exception v5) {
            }
            throw v4_4;
        }
        try {
            v3_1.write(v0);
            try {
                v3_1.close();
            } catch (Exception v4) {
            }
            return;
        } catch (Exception v4_4) {
            v2 = v3_1;
        } catch (Exception v4_3) {
            v1 = v4_3;
            v2 = v3_1;
        }
    }

    public static byte[] decodeFromFile(String p12)
    {
        com.amazon.mas.kiwi.util.Base64$InputStream v0 = 0;
        try {
            java.io.File v5_1 = new java.io.File(p12);
            int v6 = 0;
        } catch (Exception v8_9) {
            try {
                v0.close();
            } catch (int v9) {
            }
            throw v8_9;
        } catch (Exception v8_12) {
            java.io.IOException v4 = v8_12;
            throw v4;
        }
        if (v5_1.length() <= 2147483647) {
            byte[] v2 = new byte[((int) v5_1.length())];
            com.amazon.mas.kiwi.util.Base64$InputStream v1_1 = new com.amazon.mas.kiwi.util.Base64$InputStream(new java.io.BufferedInputStream(new java.io.FileInputStream(v5_1)), 0);
            try {
                while(true) {
                    int v7 = v1_1.read(v2, v6, 4096);
                    v6 += v7;
                }
                byte[] v3 = new byte[v6];
                System.arraycopy(v2, 0, v3, 0, v6);
                try {
                    v1_1.close();
                } catch (Exception v8) {
                }
                return v3;
            } catch (Exception v8_9) {
                v0 = v1_1;
            } catch (Exception v8_8) {
                v4 = v8_8;
                v0 = v1_1;
            }
            if (v7 >= 0) {
            }
        } else {
            throw new java.io.IOException(new StringBuilder().append("File is too big for this convenience method (").append(v5_1.length()).append(" bytes).").toString());
        }
    }

    public static void decodeToFile(String p5, String p6)
    {
        com.amazon.mas.kiwi.util.Base64$OutputStream v0 = 0;
        try {
            com.amazon.mas.kiwi.util.Base64$OutputStream v1_1 = new com.amazon.mas.kiwi.util.Base64$OutputStream(new java.io.FileOutputStream(p6), 0);
        } catch (Exception v3_6) {
            try {
                v0.close();
            } catch (Exception v4) {
            }
            throw v3_6;
        } catch (Exception v3_2) {
            java.io.IOException v2 = v3_2;
            throw v2;
        }
        try {
            v1_1.write(p5.getBytes("US-ASCII"));
            try {
                v1_1.close();
            } catch (Exception v3) {
            }
            return;
        } catch (Exception v3_5) {
            v2 = v3_5;
            v0 = v1_1;
        } catch (Exception v3_6) {
            v0 = v1_1;
        }
    }

    public static void encode(java.nio.ByteBuffer p6, java.nio.ByteBuffer p7)
    {
        byte[] v1 = new byte[3];
        byte[] v0 = new byte[4];
        while (p6.hasRemaining()) {
            int v2 = Math.min(3, p6.remaining());
            p6.get(v1, 0, v2);
            com.amazon.mas.kiwi.util.Base64.encode3to4(v0, v1, v2, 0);
            p7.put(v0);
        }
        return;
    }

    public static void encode(java.nio.ByteBuffer p8, java.nio.CharBuffer p9)
    {
        byte[] v2 = new byte[3];
        byte[] v0 = new byte[4];
        while (p8.hasRemaining()) {
            int v3 = Math.min(3, p8.remaining());
            p8.get(v2, 0, v3);
            com.amazon.mas.kiwi.util.Base64.encode3to4(v0, v2, v3, 0);
            int v1 = 0;
            while (v1 < 4) {
                p9.put(((char) (v0[v1] & 255)));
                v1++;
            }
        }
        return;
    }

    private static byte[] encode3to4(byte[] p6, int p7, int p8, byte[] p9, int p10, int p11)
    {
        int v2_0;
        byte[] v0 = com.amazon.mas.kiwi.util.Base64.getAlphabet(p11);
        if (p8 <= 0) {
            v2_0 = 0;
        } else {
            v2_0 = ((p6[p7] << 24) >> 8);
        }
        byte v3_1;
        if (p8 <= 1) {
            v3_1 = 0;
        } else {
            v3_1 = ((p6[(p7 + 1)] << 24) >> 16);
        }
        byte v3_6;
        if (p8 <= 2) {
            v3_6 = 0;
        } else {
            v3_6 = ((p6[(p7 + 2)] << 24) >> 24);
        }
        int v1 = ((v2_0 | v3_1) | v3_6);
        switch (p8) {
            case 1:
                p9[p10] = v0[(v1 >> 18)];
                p9[(p10 + 1)] = v0[((v1 >> 12) & 63)];
                p9[(p10 + 2)] = 61;
                p9[(p10 + 3)] = 61;
                break;
            case 2:
                p9[p10] = v0[(v1 >> 18)];
                p9[(p10 + 1)] = v0[((v1 >> 12) & 63)];
                p9[(p10 + 2)] = v0[((v1 >> 6) & 63)];
                p9[(p10 + 3)] = 61;
                break;
            case 3:
                p9[p10] = v0[(v1 >> 18)];
                p9[(p10 + 1)] = v0[((v1 >> 12) & 63)];
                p9[(p10 + 2)] = v0[((v1 >> 6) & 63)];
                p9[(p10 + 3)] = v0[(v1 & 63)];
                break;
        }
        return p9;
    }

    private static byte[] encode3to4(byte[] p6, byte[] p7, int p8, int p9)
    {
        com.amazon.mas.kiwi.util.Base64.encode3to4(p7, 0, p8, p6, 0, p9);
        return p6;
    }

    public static String encodeBytes(byte[] p5)
    {
        try {
            String v0 = com.amazon.mas.kiwi.util.Base64.encodeBytes(p5, 0, p5.length, 0);
        } catch (AssertionError v2_1) {
            if (com.amazon.mas.kiwi.util.Base64.$assertionsDisabled) {
            } else {
                throw new AssertionError(v2_1.getMessage());
            }
        }
        if ((com.amazon.mas.kiwi.util.Base64.$assertionsDisabled) || (v0 != null)) {
            return v0;
        } else {
            throw new AssertionError();
        }
    }

    public static String encodeBytes(byte[] p2, int p3)
    {
        return com.amazon.mas.kiwi.util.Base64.encodeBytes(p2, 0, p2.length, p3);
    }

    public static String encodeBytes(byte[] p4, int p5, int p6)
    {
        try {
            String v0 = com.amazon.mas.kiwi.util.Base64.encodeBytes(p4, p5, p6, 0);
        } catch (java.io.IOException v1) {
            if (com.amazon.mas.kiwi.util.Base64.$assertionsDisabled) {
            } else {
                throw new AssertionError(v1.getMessage());
            }
        }
        if ((com.amazon.mas.kiwi.util.Base64.$assertionsDisabled) || (v0 != null)) {
            return v0;
        } else {
            throw new AssertionError();
        }
    }

    public static String encodeBytes(byte[] p4, int p5, int p6, int p7)
    {
        byte[] v0 = com.amazon.mas.kiwi.util.Base64.encodeBytesToBytes(p4, p5, p6, p7);
        try {
            String v2_1 = new String(v0, "US-ASCII");
        } catch (String v2_2) {
            v2_1 = new String(v0);
        }
        return v2_1;
    }

    public static byte[] encodeBytesToBytes(byte[] p5)
    {
        try {
            byte[] v0 = com.amazon.mas.kiwi.util.Base64.encodeBytesToBytes(p5, 0, p5.length, 0);
        } catch (AssertionError v2_1) {
            if (com.amazon.mas.kiwi.util.Base64.$assertionsDisabled) {
            } else {
                throw new AssertionError(new StringBuilder().append("IOExceptions only come from GZipping, which is turned off: ").append(v2_1.getMessage()).toString());
            }
        }
        return v0;
    }

    public static byte[] encodeBytesToBytes(byte[] p10, int p11, int p12, int p13)
    {
        if (p10 != null) {
            if (p11 >= 0) {
                if (p12 >= 0) {
                    if ((p11 + p12) <= p10.length) {
                        byte[] v10_3;
                        if ((p13 & 2) == 0) {
                            int v6;
                            if ((p13 & 8) == 0) {
                                v6 = 0;
                            } else {
                                v6 = 1;
                            }
                            int v1_2;
                            if ((p12 % 3) <= 0) {
                                v1_2 = 0;
                            } else {
                                v1_2 = 4;
                            }
                            int v0_7 = (((p12 / 3) * 4) + v1_2);
                            if (v6 != 0) {
                                v0_7 += (v0_7 / 76);
                            }
                            byte[] v3_0 = new byte[v0_7];
                            int v4_0 = 0;
                            int v8 = (p12 - 2);
                            int v9 = 0;
                            int v7 = 0;
                            while (v7 < v8) {
                                int v1_9;
                                com.amazon.mas.kiwi.util.Base64.encode3to4(p10, (v7 + p11), 3, v3_0, v4_0, p13);
                                int v0_11 = (v9 + 4);
                                if ((v6 == 0) || (v0_11 < 76)) {
                                    v1_9 = v0_11;
                                } else {
                                    v3_0[(v4_0 + 4)] = 10;
                                    v4_0++;
                                    v1_9 = 0;
                                }
                                v4_0 += 4;
                                v9 = v1_9;
                                v7 += 3;
                            }
                            if (v7 < p12) {
                                com.amazon.mas.kiwi.util.Base64.encode3to4(p10, (v7 + p11), (p12 - v7), v3_0, v4_0, p13);
                                v4_0 += 4;
                            }
                            if (v4_0 > (v3_0.length - 1)) {
                                v10_3 = v3_0;
                            } else {
                                v10_3 = new byte[v4_0];
                                System.arraycopy(v3_0, 0, v10_3, 0, v4_0);
                            }
                        } else {
                            try {
                                int v2_3 = new java.io.ByteArrayOutputStream();
                                try {
                                    int v1_13 = new com.amazon.mas.kiwi.util.Base64$OutputStream(v2_3, (p13 | 1));
                                    try {
                                        Throwable v13_4 = new java.util.zip.GZIPOutputStream(v1_13);
                                    } catch (byte[] v10_9) {
                                        int v4_1 = v10_9;
                                        int v12_2 = 0;
                                        int v11_3 = v2_3;
                                        byte[] v10_5 = v1_13;
                                        try {
                                            throw v4_1;
                                        } catch (Throwable v13_1) {
                                            try {
                                                v12_2.close();
                                                try {
                                                    v10_5.close();
                                                } catch (byte[] v10) {
                                                }
                                                try {
                                                    v11_3.close();
                                                } catch (byte[] v10) {
                                                }
                                                throw v13_1;
                                            } catch (int v12) {
                                            }
                                        }
                                    } catch (byte[] v10_10) {
                                        v13_1 = v10_10;
                                        v12_2 = 0;
                                        v11_3 = v2_3;
                                        v10_5 = v1_13;
                                    }
                                    try {
                                        v13_4.write(p10, p11, p12);
                                        v13_4.close();
                                        try {
                                            v13_4.close();
                                        } catch (byte[] v10) {
                                        }
                                        try {
                                            v1_13.close();
                                            try {
                                                v2_3.close();
                                            } catch (byte[] v10) {
                                            }
                                            v10_3 = v2_3.toByteArray();
                                        } catch (byte[] v10) {
                                        }
                                    } catch (byte[] v10_12) {
                                        v12_2 = v13_4;
                                        v11_3 = v2_3;
                                        v13_1 = v10_12;
                                        v10_5 = v1_13;
                                    } catch (byte[] v10_11) {
                                        v4_1 = v10_11;
                                        v12_2 = v13_4;
                                        v11_3 = v2_3;
                                        v10_5 = v1_13;
                                    }
                                } catch (byte[] v10_8) {
                                    v13_1 = v10_8;
                                    v12_2 = 0;
                                    v11_3 = v2_3;
                                    v10_5 = 0;
                                } catch (byte[] v10_7) {
                                    v4_1 = v10_7;
                                    v12_2 = 0;
                                    v11_3 = v2_3;
                                    v10_5 = 0;
                                }
                            } catch (byte[] v10_6) {
                                v13_1 = v10_6;
                                v12_2 = 0;
                                v11_3 = 0;
                                v10_5 = 0;
                            } catch (byte[] v10_4) {
                                v4_1 = v10_4;
                                v12_2 = 0;
                                v11_3 = 0;
                                v10_5 = 0;
                            }
                        }
                        return v10_3;
                    } else {
                        int v1_15 = new Object[3];
                        v1_15[0] = Integer.valueOf(p11);
                        v1_15[1] = Integer.valueOf(p12);
                        v1_15[2] = Integer.valueOf(p10.length);
                        throw new IllegalArgumentException(String.format("Cannot have offset of %d and length of %d with array of length %d", v1_15));
                    }
                } else {
                    throw new IllegalArgumentException(new StringBuilder().append("Cannot have length offset: ").append(p12).toString());
                }
            } else {
                throw new IllegalArgumentException(new StringBuilder().append("Cannot have negative offset: ").append(p11).toString());
            }
        } else {
            throw new NullPointerException("Cannot serialize a null array.");
        }
    }

    public static void encodeFileToFile(String p6, String p7)
    {
        String v1 = com.amazon.mas.kiwi.util.Base64.encodeFromFile(p6);
        java.io.BufferedOutputStream v2 = 0;
        try {
            java.io.BufferedOutputStream v3_1 = new java.io.BufferedOutputStream(new java.io.FileOutputStream(p7));
        } catch (Exception v4_2) {
            java.io.IOException v0 = v4_2;
            throw v0;
        } catch (Exception v4_6) {
            try {
                v2.close();
            } catch (Exception v5) {
            }
            throw v4_6;
        }
        try {
            v3_1.write(v1.getBytes("US-ASCII"));
            try {
                v3_1.close();
            } catch (Exception v4) {
            }
            return;
        } catch (Exception v4_5) {
            v0 = v4_5;
            v2 = v3_1;
        } catch (Exception v4_6) {
            v2 = v3_1;
        }
    }

    public static String encodeFromFile(String p12)
    {
        com.amazon.mas.kiwi.util.Base64$InputStream v0 = 0;
        try {
            java.io.File v5_1 = new java.io.File(p12);
            byte[] v2 = new byte[Math.max(((int) ((((double) v5_1.length()) * 1.4) + 1.0)), 40)];
            int v6 = 0;
            com.amazon.mas.kiwi.util.Base64$InputStream v1_1 = new com.amazon.mas.kiwi.util.Base64$InputStream(new java.io.BufferedInputStream(new java.io.FileInputStream(v5_1)), 1);
            try {
                while(true) {
                    int v7 = v1_1.read(v2, v6, 4096);
                    v6 += v7;
                }
                String v4_1 = new String(v2, 0, v6, "US-ASCII");
                try {
                    v1_1.close();
                } catch (Exception v8) {
                }
                return v4_1;
            } catch (Exception v8_12) {
                v0 = v1_1;
                try {
                    v0.close();
                } catch (String v9) {
                }
                throw v8_12;
            } catch (Exception v8_11) {
                java.io.IOException v3 = v8_11;
                v0 = v1_1;
                throw v3;
            }
            if (v7 >= 0) {
            }
        } catch (Exception v8_8) {
            v3 = v8_8;
        } catch (Exception v8_12) {
        }
    }

    public static String encodeObject(java.io.Serializable p1)
    {
        return com.amazon.mas.kiwi.util.Base64.encodeObject(p1, 0);
    }

    public static String encodeObject(java.io.Serializable p13, int p14)
    {
        if (p13 != null) {
            java.io.ByteArrayOutputStream v2 = 0;
            com.amazon.mas.kiwi.util.Base64$OutputStream v0 = 0;
            java.util.zip.GZIPOutputStream v5 = 0;
            java.io.ObjectOutputStream v7 = 0;
            try {
                java.io.ByteArrayOutputStream v3_1 = new java.io.ByteArrayOutputStream();
            } catch (String v10_0) {
                java.io.IOException v4 = v10_0;
                throw v4;
            } catch (String v10_4) {
                try {
                    v7.close();
                } catch (byte[] v11) {
                }
                try {
                    v5.close();
                    try {
                        v0.close();
                        try {
                            v2.close();
                        } catch (byte[] v11) {
                        }
                        throw v10_4;
                    } catch (byte[] v11) {
                    }
                } catch (byte[] v11) {
                }
            }
            try {
                com.amazon.mas.kiwi.util.Base64$OutputStream v1_1 = new com.amazon.mas.kiwi.util.Base64$OutputStream(v3_1, (p14 | 1));
                try {
                    if ((p14 & 2) == 0) {
                        v7 = new java.io.ObjectOutputStream(v1_1);
                    } else {
                        java.util.zip.GZIPOutputStream v6_1 = new java.util.zip.GZIPOutputStream(v1_1);
                        try {
                            v7 = new java.io.ObjectOutputStream(v6_1);
                            v5 = v6_1;
                        } catch (String v10_4) {
                            v5 = v6_1;
                            v0 = v1_1;
                            v2 = v3_1;
                        } catch (String v10_5) {
                            v4 = v10_5;
                            v5 = v6_1;
                            v0 = v1_1;
                            v2 = v3_1;
                        }
                    }
                } catch (String v10_4) {
                    v0 = v1_1;
                    v2 = v3_1;
                } catch (String v10_6) {
                    v4 = v10_6;
                    v0 = v1_1;
                    v2 = v3_1;
                }
                v7.writeObject(p13);
                try {
                    v7.close();
                } catch (String v10) {
                }
                try {
                    v5.close();
                    try {
                        v1_1.close();
                    } catch (String v10) {
                    }
                    try {
                        v3_1.close();
                    } catch (String v10) {
                    }
                    try {
                        String v10_8 = new String(v3_1.toByteArray(), "US-ASCII");
                    } catch (String v10_9) {
                        v10_8 = new String(v3_1.toByteArray());
                    }
                    return v10_8;
                } catch (String v10) {
                }
            } catch (String v10_3) {
                v4 = v10_3;
                v2 = v3_1;
            } catch (String v10_4) {
                v2 = v3_1;
            }
        } else {
            throw new NullPointerException("Cannot serialize a null object.");
        }
    }

    public static void encodeToFile(byte[] p5, String p6)
    {
        if (p5 != null) {
            com.amazon.mas.kiwi.util.Base64$OutputStream v0 = 0;
            try {
                com.amazon.mas.kiwi.util.Base64$OutputStream v1_1 = new com.amazon.mas.kiwi.util.Base64$OutputStream(new java.io.FileOutputStream(p6), 1);
            } catch (Exception v3_2) {
                java.io.IOException v2 = v3_2;
                throw v2;
            } catch (Exception v3_4) {
                try {
                    v0.close();
                } catch (Exception v4) {
                }
                throw v3_4;
            }
            try {
                v1_1.write(p5);
            } catch (Exception v3_3) {
                v2 = v3_3;
                v0 = v1_1;
            } catch (Exception v3_4) {
                v0 = v1_1;
            }
            try {
                v1_1.close();
            } catch (Exception v3) {
            }
            return;
        } else {
            throw new NullPointerException("Data to encode was null.");
        }
    }

    private static final byte[] getAlphabet(int p2)
    {
        byte[] v0_2;
        if ((p2 & 16) != 16) {
            if ((p2 & 32) != 32) {
                v0_2 = com.amazon.mas.kiwi.util.Base64._STANDARD_ALPHABET;
            } else {
                v0_2 = com.amazon.mas.kiwi.util.Base64._ORDERED_ALPHABET;
            }
        } else {
            v0_2 = com.amazon.mas.kiwi.util.Base64._URL_SAFE_ALPHABET;
        }
        return v0_2;
    }

    private static final byte[] getDecodabet(int p2)
    {
        byte[] v0_2;
        if ((p2 & 16) != 16) {
            if ((p2 & 32) != 32) {
                v0_2 = com.amazon.mas.kiwi.util.Base64._STANDARD_DECODABET;
            } else {
                v0_2 = com.amazon.mas.kiwi.util.Base64._ORDERED_DECODABET;
            }
        } else {
            v0_2 = com.amazon.mas.kiwi.util.Base64._URL_SAFE_DECODABET;
        }
        return v0_2;
    }
}
