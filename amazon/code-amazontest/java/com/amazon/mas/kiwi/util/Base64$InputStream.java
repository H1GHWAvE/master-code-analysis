package com.amazon.mas.kiwi.util;
public class Base64$InputStream extends java.io.FilterInputStream {
    private boolean breakLines;
    private byte[] buffer;
    private int bufferLength;
    private byte[] decodabet;
    private boolean encode;
    private int lineLength;
    private int numSigBytes;
    private int options;
    private int position;

    public Base64$InputStream(java.io.InputStream p2)
    {
        this(p2, 0);
        return;
    }

    public Base64$InputStream(java.io.InputStream p4, int p5)
    {
        byte[] v0_1;
        this(p4);
        this.options = p5;
        if ((p5 & 8) <= 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        byte[] v0_3;
        this.breakLines = v0_1;
        if ((p5 & 1) <= 0) {
            v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        byte[] v0_5;
        this.encode = v0_3;
        if (!this.encode) {
            v0_5 = 3;
        } else {
            v0_5 = 4;
        }
        this.bufferLength = v0_5;
        byte[] v0_7 = new byte[this.bufferLength];
        this.buffer = v0_7;
        this.position = -1;
        this.lineLength = 0;
        this.decodabet = com.amazon.mas.kiwi.util.Base64.access$000(p5);
        return;
    }

    public int read()
    {
        int v1_3;
        if (this.position >= 0) {
            if (this.position < 0) {
                throw new java.io.IOException("Error in Base64 code reading stream.");
            } else {
                if (this.position < this.numSigBytes) {
                    if ((!this.encode) || ((!this.breakLines) || (this.lineLength < 76))) {
                        this.lineLength = (this.lineLength + 1);
                        int v1_8 = this.buffer;
                        byte v3_17 = this.position;
                        this.position = (v3_17 + 1);
                        int v6_2 = v1_8[v3_17];
                        if (this.position >= this.bufferLength) {
                            this.position = -1;
                        }
                        v1_3 = (v6_2 & 255);
                    } else {
                        this.lineLength = 0;
                        v1_3 = 10;
                    }
                } else {
                    v1_3 = -1;
                }
            }
        } else {
            if (!this.encode) {
                byte[] v7 = new byte[4];
                int v8_0 = 0;
                while (v8_0 < 4) {
                    do {
                        int v6_0 = this.in.read();
                    } while((v6_0 >= 0) && (this.decodabet[(v6_0 & 127)] <= -5));
                    if (v6_0 < 0) {
                        break;
                    }
                    v7[v8_0] = ((byte) v6_0);
                    v8_0++;
                }
                if (v8_0 != 4) {
                    if (v8_0 != 0) {
                        throw new java.io.IOException("Improperly padded Base64 input.");
                    } else {
                        v1_3 = -1;
                    }
                } else {
                    this.numSigBytes = com.amazon.mas.kiwi.util.Base64.access$200(v7, 0, this.buffer, 0, this.options);
                    this.position = 0;
                }
            } else {
                byte[] v0 = new byte[3];
                int v2 = 0;
                int v8_1 = 0;
                while (v8_1 < 3) {
                    int v6_1 = this.in.read();
                    if (v6_1 < 0) {
                        break;
                    }
                    v0[v8_1] = ((byte) v6_1);
                    v2++;
                    v8_1++;
                }
                if (v2 <= 0) {
                    v1_3 = -1;
                } else {
                    com.amazon.mas.kiwi.util.Base64.access$100(v0, 0, v2, this.buffer, 0, this.options);
                    this.position = 0;
                    this.numSigBytes = 4;
                }
            }
        }
        return v1_3;
    }

    public int read(byte[] p5, int p6, int p7)
    {
        int v1 = 0;
        while (v1 < p7) {
            int v0 = this.read();
            if (v0 < 0) {
                if (v1 != 0) {
                    break;
                }
                int v2_0 = -1;
            } else {
                p5[(p6 + v1)] = ((byte) v0);
                v1++;
            }
            return v2_0;
        }
        v2_0 = v1;
        return v2_0;
    }
}
