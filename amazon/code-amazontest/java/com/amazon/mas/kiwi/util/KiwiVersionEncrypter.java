package com.amazon.mas.kiwi.util;
public class KiwiVersionEncrypter {
    private static final String SECRET_KEY = "Kiwi__Version__Obfuscator";

    private KiwiVersionEncrypter()
    {
        return;
    }

    private static void checkInput(String p3, String p4)
    {
        if ((p3 != null) && (!p3.isEmpty())) {
            return;
        } else {
            throw new IllegalArgumentException(new StringBuilder().append("input \'").append(p4).append("\' cannot be null or empty").toString());
        }
    }

    public static String decrypt(String p3)
    {
        com.amazon.mas.kiwi.util.KiwiVersionEncrypter.checkInput(p3, "text");
        return new String(com.amazon.mas.kiwi.util.KiwiVersionEncrypter.performXOR(com.amazon.mas.kiwi.util.Base64.decode(p3.getBytes())), "UTF-8");
    }

    public static String decryptFromFile(String p6)
    {
        com.amazon.mas.kiwi.util.KiwiVersionEncrypter.checkInput(p6, "file");
        java.io.BufferedReader v0 = 0;
        try {
            java.io.BufferedReader v1_1 = new java.io.BufferedReader(new java.io.FileReader(p6));
        } catch (java.io.IOException v3_3) {
            if (v0 != null) {
                try {
                    v0.close();
                } catch (java.io.IOException v4) {
                }
            }
            throw v3_3;
        }
        try {
            String v2 = v1_1.readLine();
        } catch (java.io.IOException v3_3) {
            v0 = v1_1;
        }
        if (v1_1 != null) {
            try {
                v1_1.close();
            } catch (java.io.IOException v3) {
            }
        }
        return new String(com.amazon.mas.kiwi.util.KiwiVersionEncrypter.performXOR(com.amazon.mas.kiwi.util.Base64.decode(v2.getBytes())), "UTF-8");
    }

    public static String encrypt(String p1)
    {
        com.amazon.mas.kiwi.util.KiwiVersionEncrypter.checkInput(p1, "text");
        return com.amazon.mas.kiwi.util.Base64.encodeBytes(com.amazon.mas.kiwi.util.KiwiVersionEncrypter.performXOR(p1.getBytes()));
    }

    public static void encryptToFile(String p5, String p6)
    {
        com.amazon.mas.kiwi.util.KiwiVersionEncrypter.checkInput(p5, "text");
        com.amazon.mas.kiwi.util.KiwiVersionEncrypter.checkInput(p6, "file");
        String v2 = com.amazon.mas.kiwi.util.Base64.encodeBytes(com.amazon.mas.kiwi.util.KiwiVersionEncrypter.performXOR(p5.getBytes()));
        java.io.BufferedWriter v0 = 0;
        try {
            java.io.BufferedWriter v1_1 = new java.io.BufferedWriter(new java.io.FileWriter(p6));
            try {
                v1_1.write(v2);
                v1_1.flush();
            } catch (java.io.IOException v3_6) {
                v0 = v1_1;
                if (v0 != null) {
                    try {
                        v0.close();
                    } catch (java.io.IOException v4) {
                    }
                }
                throw v3_6;
            }
            if (v1_1 != null) {
                try {
                    v1_1.close();
                } catch (java.io.IOException v3) {
                }
            }
            return;
        } catch (java.io.IOException v3_6) {
        }
    }

    public static void main(String[] p3)
    {
        if ((p3 != null) && (p3.length != 0)) {
            if (p3.length <= 1) {
                System.out.println(com.amazon.mas.kiwi.util.KiwiVersionEncrypter.encrypt(p3[0]));
            } else {
                com.amazon.mas.kiwi.util.KiwiVersionEncrypter.encryptToFile(p3[0], p3[1]);
            }
        } else {
            System.out.println("Usage: com.amazon.mas.kiwi.util.KiwiVersionEncrypter <textToBeEncrypted> [<encryptToFileName>]");
        }
        return;
    }

    private static byte[] performXOR(byte[] p6)
    {
        byte[] v0 = new byte[p6.length];
        byte[] v2 = "Kiwi__Version__Obfuscator".getBytes();
        int v3 = 0;
        int v1 = 0;
        while (v1 < p6.length) {
            v0[v1] = ((byte) (p6[v1] ^ v2[v3]));
            v3++;
            if (v3 >= v2.length) {
                v3 = 0;
            }
            v1++;
        }
        return v0;
    }
}
