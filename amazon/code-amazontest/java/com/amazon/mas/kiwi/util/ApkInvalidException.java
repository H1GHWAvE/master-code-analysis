package com.amazon.mas.kiwi.util;
public class ApkInvalidException extends java.lang.RuntimeException {

    ApkInvalidException()
    {
        return;
    }

    ApkInvalidException(String p1)
    {
        this(p1);
        return;
    }

    ApkInvalidException(String p1, Throwable p2)
    {
        this(p1, p2);
        return;
    }

    ApkInvalidException(Throwable p1)
    {
        this(p1);
        return;
    }
}
