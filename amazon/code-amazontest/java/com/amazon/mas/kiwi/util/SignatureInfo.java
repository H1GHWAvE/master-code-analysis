package com.amazon.mas.kiwi.util;
public class SignatureInfo {
    private static final String VALIDITY_REQUIREMENTS_FOR_CREATING_KEYSTORE;
    private static final String VALIDITY_REQUIREMENTS_FOR_RETRIEVING_KEYSTORE;
    private String certificateId;
    private String commonName;
    private String developerId;
    private String developerName;

    static SignatureInfo()
    {
        Object[] v1_0 = new Object[1];
        v1_0[0] = com.amazon.mas.kiwi.util.SignatureInfo.getSimpleName();
        com.amazon.mas.kiwi.util.SignatureInfo.VALIDITY_REQUIREMENTS_FOR_CREATING_KEYSTORE = String.format("A valid instance of %s is required to have at least one non-blank ID, either developerId or certificateId, and at least one non-blank name, either developerName or commonName.", v1_0);
        Object[] v1_1 = new Object[1];
        v1_1[0] = com.amazon.mas.kiwi.util.SignatureInfo.getSimpleName();
        com.amazon.mas.kiwi.util.SignatureInfo.VALIDITY_REQUIREMENTS_FOR_RETRIEVING_KEYSTORE = String.format("A valid instance of %s is required to have a certificate ID.", v1_1);
        return;
    }

    public SignatureInfo()
    {
        return;
    }

    public static String getValidityRequirementsForCreatingKeystore()
    {
        return com.amazon.mas.kiwi.util.SignatureInfo.VALIDITY_REQUIREMENTS_FOR_CREATING_KEYSTORE;
    }

    public static String getValidityRequirementsForRetrievingKeystore()
    {
        return com.amazon.mas.kiwi.util.SignatureInfo.VALIDITY_REQUIREMENTS_FOR_RETRIEVING_KEYSTORE;
    }

    public boolean equals(Object p6)
    {
        boolean v2_7;
        if (p6 != null) {
            if (p6 != this) {
                if (p6.getClass() == this.getClass()) {
                    v2_7 = new org.apache.commons.lang3.builder.EqualsBuilder().append(this.certificateId, ((com.amazon.mas.kiwi.util.SignatureInfo) p6).certificateId).append(this.commonName, ((com.amazon.mas.kiwi.util.SignatureInfo) p6).commonName).append(this.developerId, ((com.amazon.mas.kiwi.util.SignatureInfo) p6).developerId).append(this.developerName, ((com.amazon.mas.kiwi.util.SignatureInfo) p6).developerName).isEquals();
                } else {
                    v2_7 = 0;
                }
            } else {
                v2_7 = 1;
            }
        } else {
            v2_7 = 0;
        }
        return v2_7;
    }

    public String getCertificateId()
    {
        return this.certificateId;
    }

    public String getCommonName()
    {
        return this.commonName;
    }

    public String getDeveloperId()
    {
        return this.developerId;
    }

    public String getDeveloperName()
    {
        return this.developerName;
    }

    public String getId()
    {
        String v0_2;
        if (!org.apache.commons.lang3.StringUtils.isBlank(this.certificateId)) {
            v0_2 = this.certificateId;
        } else {
            v0_2 = this.developerId;
        }
        return v0_2;
    }

    public String getName()
    {
        String v0_2;
        if (!org.apache.commons.lang3.StringUtils.isBlank(this.commonName)) {
            v0_2 = this.commonName;
        } else {
            v0_2 = this.developerName;
        }
        return v0_2;
    }

    public int hashCode()
    {
        return new org.apache.commons.lang3.builder.HashCodeBuilder().append(this.certificateId).append(this.commonName).append(this.developerId).append(this.developerName).toHashCode();
    }

    public boolean isValidForCreatingKeystore()
    {
        if ((org.apache.commons.lang3.StringUtils.isBlank(this.getId())) || (org.apache.commons.lang3.StringUtils.isBlank(this.getName()))) {
            int v0_4 = 0;
        } else {
            v0_4 = 1;
        }
        return v0_4;
    }

    public boolean isValidForRetrievingKeyStore()
    {
        int v0_2;
        if (org.apache.commons.lang3.StringUtils.isBlank(this.getId())) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public void setCertificateId(String p1)
    {
        this.certificateId = p1;
        return;
    }

    public void setCommonName(String p1)
    {
        this.commonName = p1;
        return;
    }

    public void setDeveloperId(String p1)
    {
        this.developerId = p1;
        return;
    }

    public void setDeveloperName(String p1)
    {
        this.developerName = p1;
        return;
    }

    public String toString()
    {
        return org.apache.commons.lang3.builder.ReflectionToStringBuilder.toString(this, org.apache.commons.lang3.builder.ToStringStyle.SHORT_PREFIX_STYLE);
    }

    public com.amazon.mas.kiwi.util.SignatureInfo withCertificateId(String p1)
    {
        this.certificateId = p1;
        return this;
    }

    public com.amazon.mas.kiwi.util.SignatureInfo withCommonName(String p1)
    {
        this.commonName = p1;
        return this;
    }

    public com.amazon.mas.kiwi.util.SignatureInfo withDeveloperId(String p1)
    {
        this.developerId = p1;
        return this;
    }

    public com.amazon.mas.kiwi.util.SignatureInfo withDeveloperName(String p1)
    {
        this.developerName = p1;
        return this;
    }
}
