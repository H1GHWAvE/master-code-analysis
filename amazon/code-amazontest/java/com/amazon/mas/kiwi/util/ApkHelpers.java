package com.amazon.mas.kiwi.util;
public class ApkHelpers {
    private static final String CONTENT_PREFIX = "com.amazon.content.id.";

    public ApkHelpers()
    {
        return;
    }

    public static byte[] getApkSignature(String p8)
    {
        if ((p8 != null) && (p8.length() != 0)) {
            java.util.jar.JarFile v4_1 = new java.util.jar.JarFile(p8);
            try {
                byte[] v6_1;
                com.amazon.mas.kiwi.util.ApkHelpers.scanJar(v4_1);
                java.security.cert.Certificate v1 = com.amazon.mas.kiwi.util.ApkHelpers.getFirstSigningCert(v4_1);
            } catch (SecurityException v3) {
                v6_1 = 0;
                return v6_1;
            }
            if (v1 != null) {
                byte[] v2 = 0;
                if ((v1 instanceof java.security.cert.X509Certificate)) {
                    v2 = ((java.security.cert.X509Certificate) v1).getSignature();
                }
                v6_1 = v2;
                return v6_1;
            } else {
                v6_1 = 0;
                return v6_1;
            }
        } else {
            throw new IllegalArgumentException("apkFileName cannot be null or empty!");
        }
    }

    private static java.security.CodeSigner[] getCodeSigners(java.util.jar.JarFile p4)
    {
        java.security.CodeSigner[] v2 = 0;
        java.util.Enumeration v0 = p4.entries();
        while (v0.hasMoreElements()) {
            v2 = ((java.util.jar.JarEntry) v0.nextElement()).getCodeSigners();
            if (v2 != null) {
                break;
            }
        }
        return v2;
    }

    public static String getContentID(java.util.jar.JarFile p5)
    {
        if (p5 != null) {
            java.util.Enumeration v2 = p5.entries();
            while (v2.hasMoreElements()) {
                java.util.jar.JarEntry v1_1 = ((java.util.jar.JarEntry) v2.nextElement());
                if (!v1_1.isDirectory()) {
                    String v0 = com.amazon.mas.kiwi.util.ApkHelpers.getContentIDFromName(v1_1.getName());
                    if ((v0 != null) && (v0.length() != 0)) {
                        String v3_1 = v0;
                    }
                }
                return v3_1;
            }
            v3_1 = 0;
            return v3_1;
        } else {
            throw new IllegalArgumentException("apkSrc must not be null!");
        }
    }

    public static String getContentIDFromName(String p5)
    {
        if ((p5 != null) && (p5.length() != 0)) {
            String v1_4;
            int v0 = p5.indexOf("com.amazon.content.id.");
            if (v0 >= 0) {
                if (p5.length() > "com.amazon.content.id.".length()) {
                    v1_4 = p5.substring(("com.amazon.content.id.".length() + v0));
                } else {
                    v1_4 = 0;
                }
            } else {
                v1_4 = 0;
            }
            return v1_4;
        } else {
            throw new IllegalArgumentException("name cannot be null or empty!");
        }
    }

    private static java.security.cert.Certificate getFirstSigningCert(java.util.jar.JarFile p5)
    {
        java.security.CodeSigner[] v2 = com.amazon.mas.kiwi.util.ApkHelpers.getCodeSigners(p5);
        java.security.cert.Certificate v0_0 = 0;
        if ((v2 != null) && (v2.length > 0)) {
            java.util.List v1 = v2[0].getSignerCertPath().getCertificates();
            if (!v1.isEmpty()) {
                v0_0 = ((java.security.cert.Certificate) v1.get(0));
            }
        }
        return v0_0;
    }

    public static boolean isSigned(java.io.File p3)
    {
        java.util.jar.JarFile v0 = 0;
        try {
            java.util.jar.JarFile v1_1 = new java.util.jar.JarFile(p3);
            try {
                Throwable v2_1 = com.amazon.mas.kiwi.util.ApkHelpers.isSigned(v1_1);
                v1_1.close();
                return v2_1;
            } catch (Throwable v2_0) {
                v0 = v1_1;
                v0.close();
                throw v2_0;
            }
        } catch (Throwable v2_0) {
        }
        v2_1 = com.amazon.mas.kiwi.util.ApkHelpers.isSigned(v1_1);
        v1_1.close();
        return v2_1;
    }

    public static boolean isSigned(java.util.jar.JarFile p4)
    {
        int v2_0 = 1;
        if (p4 != null) {
            try {
                com.amazon.mas.kiwi.util.ApkHelpers.scanJar(p4);
            } catch (SecurityException v1) {
                return v2_0;
            }
            if (com.amazon.mas.kiwi.util.ApkHelpers.getCodeSigners(p4) == null) {
                v2_0 = 0;
                return v2_0;
            } else {
                return v2_0;
            }
        } else {
            throw new IllegalArgumentException("apkSrc must not be null!");
        }
    }

    private static void scanJar(java.util.jar.JarFile p7)
    {
        java.util.Enumeration v2 = p7.entries();
        byte[] v0 = new byte[8192];
        while (v2.hasMoreElements()) {
            try {
                java.io.InputStream v3 = p7.getInputStream(((java.util.jar.JarEntry) v2.nextElement()));
            } catch (com.amazon.mas.kiwi.util.ApkInvalidException v5_5) {
                if (v3 != null) {
                    v3.close();
                }
                throw v5_5;
            } catch (com.amazon.mas.kiwi.util.ApkInvalidException v5_2) {
                throw new com.amazon.mas.kiwi.util.ApkInvalidException(v5_2);
            }
            while (v3.read(v0, 0, v0.length) != -1) {
            }
            if (v3 != null) {
                v3.close();
            }
        }
        return;
    }
}
