package com.amazon.mas.kiwi.util;
public class Base64$OutputStream extends java.io.FilterOutputStream {
    private byte[] b4;
    private boolean breakLines;
    private byte[] buffer;
    private int bufferLength;
    private byte[] decodabet;
    private boolean encode;
    private int lineLength;
    private int options;
    private int position;
    private boolean suspendEncoding;

    public Base64$OutputStream(java.io.OutputStream p2)
    {
        this(p2, 1);
        return;
    }

    public Base64$OutputStream(java.io.OutputStream p5, int p6)
    {
        byte[] v0_1;
        this(p5);
        if ((p6 & 8) == 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        byte[] v0_3;
        this.breakLines = v0_1;
        if ((p6 & 1) == 0) {
            v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        byte[] v0_5;
        this.encode = v0_3;
        if (!this.encode) {
            v0_5 = 4;
        } else {
            v0_5 = 3;
        }
        this.bufferLength = v0_5;
        byte[] v0_7 = new byte[this.bufferLength];
        this.buffer = v0_7;
        this.position = 0;
        this.lineLength = 0;
        this.suspendEncoding = 0;
        byte[] v0_8 = new byte[4];
        this.b4 = v0_8;
        this.options = p6;
        this.decodabet = com.amazon.mas.kiwi.util.Base64.access$000(p6);
        return;
    }

    public void close()
    {
        this.flushBase64();
        super.close();
        this.buffer = 0;
        this.out = 0;
        return;
    }

    public void flushBase64()
    {
        if (this.position > 0) {
            if (!this.encode) {
                throw new java.io.IOException("Base64 input not properly padded.");
            } else {
                this.out.write(com.amazon.mas.kiwi.util.Base64.access$300(this.b4, this.buffer, this.position, this.options));
                this.position = 0;
            }
        }
        return;
    }

    public void resumeEncoding()
    {
        this.suspendEncoding = 0;
        return;
    }

    public void suspendEncoding()
    {
        this.flushBase64();
        this.suspendEncoding = 1;
        return;
    }

    public void write(int p8)
    {
        if (!this.suspendEncoding) {
            if (!this.encode) {
                if (this.decodabet[(p8 & 127)] <= -5) {
                    if (this.decodabet[(p8 & 127)] != -5) {
                        throw new java.io.IOException("Invalid character in Base64 data.");
                    }
                } else {
                    java.io.OutputStream v1_8 = this.buffer;
                    int v2_3 = this.position;
                    this.position = (v2_3 + 1);
                    v1_8[v2_3] = ((byte) p8);
                    if (this.position >= this.bufferLength) {
                        this.out.write(this.b4, 0, com.amazon.mas.kiwi.util.Base64.access$200(this.buffer, 0, this.b4, 0, this.options));
                        this.position = 0;
                    }
                }
            } else {
                java.io.OutputStream v1_12 = this.buffer;
                int v2_7 = this.position;
                this.position = (v2_7 + 1);
                v1_12[v2_7] = ((byte) p8);
                if (this.position >= this.bufferLength) {
                    this.out.write(com.amazon.mas.kiwi.util.Base64.access$300(this.b4, this.buffer, this.bufferLength, this.options));
                    this.lineLength = (this.lineLength + 4);
                    if ((this.breakLines) && (this.lineLength >= 76)) {
                        this.out.write(10);
                        this.lineLength = 0;
                    }
                    this.position = 0;
                }
            }
        } else {
            this.out.write(p8);
        }
        return;
    }

    public void write(byte[] p3, int p4, int p5)
    {
        if (!this.suspendEncoding) {
            int v0 = 0;
            while (v0 < p5) {
                this.write(p3[(p4 + v0)]);
                v0++;
            }
        } else {
            this.out.write(p3, p4, p5);
        }
        return;
    }
}
