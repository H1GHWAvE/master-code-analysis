package me.neutze.license;
public class MainActivity extends android.app.Activity {
    private android.widget.Button mCheckLicenseButton;
    private android.os.Handler mHandler;
    private android.widget.TextView mStatusText;

    public MainActivity()
    {
        return;
    }

    static synthetic android.os.Handler access$002(me.neutze.license.MainActivity p0, android.os.Handler p1)
    {
        p0.mHandler = p1;
        return p1;
    }

    static synthetic void access$100(me.neutze.license.MainActivity p0)
    {
        p0.doCheck();
        return;
    }

    static synthetic android.widget.TextView access$200(me.neutze.license.MainActivity p1)
    {
        return p1.mStatusText;
    }

    static synthetic android.widget.Button access$300(me.neutze.license.MainActivity p1)
    {
        return p1.mCheckLicenseButton;
    }

    private void displayResult(String p5)
    {
        this.mHandler.postDelayed(new me.neutze.license.MainActivity$2(this, p5), 2000);
        return;
    }

    private void doCheck()
    {
        this.mCheckLicenseButton.setEnabled(0);
        this.setProgressBarIndeterminateVisibility(1);
        this.mStatusText.setText(2131099651);
        this.displayResult("amazon does not check here");
        return;
    }

    private void onCreateMainActivity(android.os.Bundle p3)
    {
        super.onCreate(p3);
        this.setContentView(2130903040);
        this.mStatusText = ((android.widget.TextView) this.findViewById(2131230720));
        this.mCheckLicenseButton = ((android.widget.Button) this.findViewById(2131230721));
        this.mCheckLicenseButton.setOnClickListener(new me.neutze.license.MainActivity$1(this));
        return;
    }

    private void onDestroyMainActivity()
    {
        super.onDestroy();
        return;
    }

    public void onActivityResult(int p2, int p3, android.content.Intent p4)
    {
        if (!com.amazon.android.Kiwi.onActivityResult(this, p2, p3, p4)) {
            super.onActivityResult(p2, p3, p4);
        }
        return;
    }

    public void onCreate(android.os.Bundle p2)
    {
        this.onCreateMainActivity(p2);
        com.amazon.android.Kiwi.onCreate(this, 1);
        return;
    }

    public android.app.Dialog onCreateDialog(int p3)
    {
        android.app.Dialog v1;
        android.app.Dialog v0 = com.amazon.android.Kiwi.onCreateDialog(this, p3);
        if (v0 == null) {
            v1 = super.onCreateDialog(p3);
        } else {
            v1 = v0;
        }
        return v1;
    }

    public android.app.Dialog onCreateDialog(int p2, android.os.Bundle p3)
    {
        android.app.Dialog v1_1;
        android.app.Dialog v0 = com.amazon.android.Kiwi.onCreateDialog(this, p2);
        if (v0 == null) {
            v1_1 = super.onCreateDialog(p2, p3);
        } else {
            v1_1 = v0;
        }
        return v1_1;
    }

    public void onDestroy()
    {
        this.onDestroyMainActivity();
        com.amazon.android.Kiwi.onDestroy(this);
        return;
    }

    public void onPause()
    {
        super.onPause();
        com.amazon.android.Kiwi.onPause(this);
        return;
    }

    public void onResume()
    {
        super.onResume();
        com.amazon.android.Kiwi.onResume(this);
        return;
    }

    public void onStart()
    {
        super.onStart();
        com.amazon.android.Kiwi.onStart(this);
        return;
    }

    public void onStop()
    {
        super.onStop();
        com.amazon.android.Kiwi.onStop(this);
        return;
    }
}
