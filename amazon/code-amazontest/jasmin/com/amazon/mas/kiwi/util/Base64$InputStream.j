.bytecode 50.0
.class public synchronized com/amazon/mas/kiwi/util/Base64$InputStream
.super java/io/FilterInputStream
.inner class public static InputStream inner com/amazon/mas/kiwi/util/Base64$InputStream outer com/amazon/mas/kiwi/util/Base64

.field private 'breakLines' Z

.field private 'buffer' [B

.field private 'bufferLength' I

.field private 'decodabet' [B

.field private 'encode' Z

.field private 'lineLength' I

.field private 'numSigBytes' I

.field private 'options' I

.field private 'position' I

.method public <init>(Ljava/io/InputStream;)V
aload 0
aload 1
iconst_0
invokespecial com/amazon/mas/kiwi/util/Base64$InputStream/<init>(Ljava/io/InputStream;I)V
return
.limit locals 2
.limit stack 3
.end method

.method public <init>(Ljava/io/InputStream;I)V
aload 0
aload 1
invokespecial java/io/FilterInputStream/<init>(Ljava/io/InputStream;)V
aload 0
iload 2
putfield com/amazon/mas/kiwi/util/Base64$InputStream/options I
iload 2
bipush 8
iand
ifle L0
iconst_1
istore 4
L1:
aload 0
iload 4
putfield com/amazon/mas/kiwi/util/Base64$InputStream/breakLines Z
iload 2
iconst_1
iand
ifle L2
iconst_1
istore 4
L3:
aload 0
iload 4
putfield com/amazon/mas/kiwi/util/Base64$InputStream/encode Z
aload 0
getfield com/amazon/mas/kiwi/util/Base64$InputStream/encode Z
ifeq L4
iconst_4
istore 3
L5:
aload 0
iload 3
putfield com/amazon/mas/kiwi/util/Base64$InputStream/bufferLength I
aload 0
aload 0
getfield com/amazon/mas/kiwi/util/Base64$InputStream/bufferLength I
newarray byte
putfield com/amazon/mas/kiwi/util/Base64$InputStream/buffer [B
aload 0
iconst_m1
putfield com/amazon/mas/kiwi/util/Base64$InputStream/position I
aload 0
iconst_0
putfield com/amazon/mas/kiwi/util/Base64$InputStream/lineLength I
aload 0
iload 2
invokestatic com/amazon/mas/kiwi/util/Base64/access$000(I)[B
putfield com/amazon/mas/kiwi/util/Base64$InputStream/decodabet [B
return
L0:
iconst_0
istore 4
goto L1
L2:
iconst_0
istore 4
goto L3
L4:
iconst_3
istore 3
goto L5
.limit locals 5
.limit stack 2
.end method

.method public read()I
.throws java/io/IOException
aload 0
getfield com/amazon/mas/kiwi/util/Base64$InputStream/position I
ifge L0
aload 0
getfield com/amazon/mas/kiwi/util/Base64$InputStream/encode Z
ifeq L1
iconst_3
newarray byte
astore 4
iconst_0
istore 2
iconst_0
istore 1
L2:
iload 1
iconst_3
if_icmpge L3
aload 0
getfield com/amazon/mas/kiwi/util/Base64$InputStream/in Ljava/io/InputStream;
invokevirtual java/io/InputStream/read()I
istore 3
iload 3
iflt L3
aload 4
iload 1
iload 3
i2b
bastore
iload 2
iconst_1
iadd
istore 2
iload 1
iconst_1
iadd
istore 1
goto L2
L3:
iload 2
ifle L4
aload 4
iconst_0
iload 2
aload 0
getfield com/amazon/mas/kiwi/util/Base64$InputStream/buffer [B
iconst_0
aload 0
getfield com/amazon/mas/kiwi/util/Base64$InputStream/options I
invokestatic com/amazon/mas/kiwi/util/Base64/access$100([BII[BII)[B
pop
aload 0
iconst_0
putfield com/amazon/mas/kiwi/util/Base64$InputStream/position I
aload 0
iconst_4
putfield com/amazon/mas/kiwi/util/Base64$InputStream/numSigBytes I
L0:
aload 0
getfield com/amazon/mas/kiwi/util/Base64$InputStream/position I
iflt L5
aload 0
getfield com/amazon/mas/kiwi/util/Base64$InputStream/position I
aload 0
getfield com/amazon/mas/kiwi/util/Base64$InputStream/numSigBytes I
if_icmplt L6
iconst_m1
ireturn
L4:
iconst_m1
ireturn
L1:
iconst_4
newarray byte
astore 4
iconst_0
istore 1
L7:
iload 1
iconst_4
if_icmpge L8
L9:
aload 0
getfield com/amazon/mas/kiwi/util/Base64$InputStream/in Ljava/io/InputStream;
invokevirtual java/io/InputStream/read()I
istore 2
iload 2
iflt L10
aload 0
getfield com/amazon/mas/kiwi/util/Base64$InputStream/decodabet [B
iload 2
bipush 127
iand
baload
bipush -5
if_icmple L9
L10:
iload 2
ifge L11
L8:
iload 1
iconst_4
if_icmpne L12
aload 0
aload 4
iconst_0
aload 0
getfield com/amazon/mas/kiwi/util/Base64$InputStream/buffer [B
iconst_0
aload 0
getfield com/amazon/mas/kiwi/util/Base64$InputStream/options I
invokestatic com/amazon/mas/kiwi/util/Base64/access$200([BI[BII)I
putfield com/amazon/mas/kiwi/util/Base64$InputStream/numSigBytes I
aload 0
iconst_0
putfield com/amazon/mas/kiwi/util/Base64$InputStream/position I
goto L0
L11:
aload 4
iload 1
iload 2
i2b
bastore
iload 1
iconst_1
iadd
istore 1
goto L7
L12:
iload 1
ifne L13
iconst_m1
ireturn
L13:
new java/io/IOException
dup
ldc "Improperly padded Base64 input."
invokespecial java/io/IOException/<init>(Ljava/lang/String;)V
athrow
L6:
aload 0
getfield com/amazon/mas/kiwi/util/Base64$InputStream/encode Z
ifeq L14
aload 0
getfield com/amazon/mas/kiwi/util/Base64$InputStream/breakLines Z
ifeq L14
aload 0
getfield com/amazon/mas/kiwi/util/Base64$InputStream/lineLength I
bipush 76
if_icmplt L14
aload 0
iconst_0
putfield com/amazon/mas/kiwi/util/Base64$InputStream/lineLength I
bipush 10
ireturn
L14:
aload 0
aload 0
getfield com/amazon/mas/kiwi/util/Base64$InputStream/lineLength I
iconst_1
iadd
putfield com/amazon/mas/kiwi/util/Base64$InputStream/lineLength I
aload 0
getfield com/amazon/mas/kiwi/util/Base64$InputStream/buffer [B
astore 4
aload 0
getfield com/amazon/mas/kiwi/util/Base64$InputStream/position I
istore 1
aload 0
iload 1
iconst_1
iadd
putfield com/amazon/mas/kiwi/util/Base64$InputStream/position I
aload 4
iload 1
baload
istore 1
aload 0
getfield com/amazon/mas/kiwi/util/Base64$InputStream/position I
aload 0
getfield com/amazon/mas/kiwi/util/Base64$InputStream/bufferLength I
if_icmplt L15
aload 0
iconst_m1
putfield com/amazon/mas/kiwi/util/Base64$InputStream/position I
L15:
iload 1
sipush 255
iand
ireturn
L5:
new java/io/IOException
dup
ldc "Error in Base64 code reading stream."
invokespecial java/io/IOException/<init>(Ljava/lang/String;)V
athrow
.limit locals 5
.limit stack 6
.end method

.method public read([BII)I
.throws java/io/IOException
iconst_0
istore 4
L0:
iload 4
iload 3
if_icmpge L1
aload 0
invokevirtual com/amazon/mas/kiwi/util/Base64$InputStream/read()I
istore 5
iload 5
iflt L2
aload 1
iload 2
iload 4
iadd
iload 5
i2b
bastore
iload 4
iconst_1
iadd
istore 4
goto L0
L2:
iload 4
ifne L1
iconst_m1
ireturn
L1:
iload 4
ireturn
.limit locals 6
.limit stack 3
.end method
