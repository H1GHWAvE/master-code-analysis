.bytecode 50.0
.class public synchronized com/amazon/mas/kiwi/util/SignatureInfo
.super java/lang/Object

.field private static final 'VALIDITY_REQUIREMENTS_FOR_CREATING_KEYSTORE' Ljava/lang/String;

.field private static final 'VALIDITY_REQUIREMENTS_FOR_RETRIEVING_KEYSTORE' Ljava/lang/String;

.field private 'certificateId' Ljava/lang/String;

.field private 'commonName' Ljava/lang/String;

.field private 'developerId' Ljava/lang/String;

.field private 'developerName' Ljava/lang/String;

.method static <clinit>()V
ldc "A valid instance of %s is required to have at least one non-blank ID, either developerId or certificateId, and at least one non-blank name, either developerName or commonName."
iconst_1
anewarray java/lang/Object
dup
iconst_0
ldc com/amazon/mas/kiwi/util/SignatureInfo
invokevirtual java/lang/Class/getSimpleName()Ljava/lang/String;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
putstatic com/amazon/mas/kiwi/util/SignatureInfo/VALIDITY_REQUIREMENTS_FOR_CREATING_KEYSTORE Ljava/lang/String;
ldc "A valid instance of %s is required to have a certificate ID."
iconst_1
anewarray java/lang/Object
dup
iconst_0
ldc com/amazon/mas/kiwi/util/SignatureInfo
invokevirtual java/lang/Class/getSimpleName()Ljava/lang/String;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
putstatic com/amazon/mas/kiwi/util/SignatureInfo/VALIDITY_REQUIREMENTS_FOR_RETRIEVING_KEYSTORE Ljava/lang/String;
return
.limit locals 0
.limit stack 5
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static getValidityRequirementsForCreatingKeystore()Ljava/lang/String;
getstatic com/amazon/mas/kiwi/util/SignatureInfo/VALIDITY_REQUIREMENTS_FOR_CREATING_KEYSTORE Ljava/lang/String;
areturn
.limit locals 0
.limit stack 1
.end method

.method public static getValidityRequirementsForRetrievingKeystore()Ljava/lang/String;
getstatic com/amazon/mas/kiwi/util/SignatureInfo/VALIDITY_REQUIREMENTS_FOR_RETRIEVING_KEYSTORE Ljava/lang/String;
areturn
.limit locals 0
.limit stack 1
.end method

.method public equals(Ljava/lang/Object;)Z
aload 1
ifnonnull L0
iconst_0
ireturn
L0:
aload 1
aload 0
if_acmpne L1
iconst_1
ireturn
L1:
aload 1
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
aload 0
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
if_acmpeq L2
iconst_0
ireturn
L2:
aload 1
checkcast com/amazon/mas/kiwi/util/SignatureInfo
astore 1
new org/apache/commons/lang3/builder/EqualsBuilder
dup
invokespecial org/apache/commons/lang3/builder/EqualsBuilder/<init>()V
aload 0
getfield com/amazon/mas/kiwi/util/SignatureInfo/certificateId Ljava/lang/String;
aload 1
getfield com/amazon/mas/kiwi/util/SignatureInfo/certificateId Ljava/lang/String;
invokevirtual org/apache/commons/lang3/builder/EqualsBuilder/append(Ljava/lang/Object;Ljava/lang/Object;)Lorg/apache/commons/lang3/builder/EqualsBuilder;
aload 0
getfield com/amazon/mas/kiwi/util/SignatureInfo/commonName Ljava/lang/String;
aload 1
getfield com/amazon/mas/kiwi/util/SignatureInfo/commonName Ljava/lang/String;
invokevirtual org/apache/commons/lang3/builder/EqualsBuilder/append(Ljava/lang/Object;Ljava/lang/Object;)Lorg/apache/commons/lang3/builder/EqualsBuilder;
aload 0
getfield com/amazon/mas/kiwi/util/SignatureInfo/developerId Ljava/lang/String;
aload 1
getfield com/amazon/mas/kiwi/util/SignatureInfo/developerId Ljava/lang/String;
invokevirtual org/apache/commons/lang3/builder/EqualsBuilder/append(Ljava/lang/Object;Ljava/lang/Object;)Lorg/apache/commons/lang3/builder/EqualsBuilder;
aload 0
getfield com/amazon/mas/kiwi/util/SignatureInfo/developerName Ljava/lang/String;
aload 1
getfield com/amazon/mas/kiwi/util/SignatureInfo/developerName Ljava/lang/String;
invokevirtual org/apache/commons/lang3/builder/EqualsBuilder/append(Ljava/lang/Object;Ljava/lang/Object;)Lorg/apache/commons/lang3/builder/EqualsBuilder;
invokevirtual org/apache/commons/lang3/builder/EqualsBuilder/isEquals()Z
ireturn
.limit locals 2
.limit stack 3
.end method

.method public getCertificateId()Ljava/lang/String;
aload 0
getfield com/amazon/mas/kiwi/util/SignatureInfo/certificateId Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getCommonName()Ljava/lang/String;
aload 0
getfield com/amazon/mas/kiwi/util/SignatureInfo/commonName Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getDeveloperId()Ljava/lang/String;
aload 0
getfield com/amazon/mas/kiwi/util/SignatureInfo/developerId Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getDeveloperName()Ljava/lang/String;
aload 0
getfield com/amazon/mas/kiwi/util/SignatureInfo/developerName Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getId()Ljava/lang/String;
aload 0
getfield com/amazon/mas/kiwi/util/SignatureInfo/certificateId Ljava/lang/String;
invokestatic org/apache/commons/lang3/StringUtils/isBlank(Ljava/lang/CharSequence;)Z
ifeq L0
aload 0
getfield com/amazon/mas/kiwi/util/SignatureInfo/developerId Ljava/lang/String;
areturn
L0:
aload 0
getfield com/amazon/mas/kiwi/util/SignatureInfo/certificateId Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getName()Ljava/lang/String;
aload 0
getfield com/amazon/mas/kiwi/util/SignatureInfo/commonName Ljava/lang/String;
invokestatic org/apache/commons/lang3/StringUtils/isBlank(Ljava/lang/CharSequence;)Z
ifeq L0
aload 0
getfield com/amazon/mas/kiwi/util/SignatureInfo/developerName Ljava/lang/String;
areturn
L0:
aload 0
getfield com/amazon/mas/kiwi/util/SignatureInfo/commonName Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public hashCode()I
new org/apache/commons/lang3/builder/HashCodeBuilder
dup
invokespecial org/apache/commons/lang3/builder/HashCodeBuilder/<init>()V
aload 0
getfield com/amazon/mas/kiwi/util/SignatureInfo/certificateId Ljava/lang/String;
invokevirtual org/apache/commons/lang3/builder/HashCodeBuilder/append(Ljava/lang/Object;)Lorg/apache/commons/lang3/builder/HashCodeBuilder;
aload 0
getfield com/amazon/mas/kiwi/util/SignatureInfo/commonName Ljava/lang/String;
invokevirtual org/apache/commons/lang3/builder/HashCodeBuilder/append(Ljava/lang/Object;)Lorg/apache/commons/lang3/builder/HashCodeBuilder;
aload 0
getfield com/amazon/mas/kiwi/util/SignatureInfo/developerId Ljava/lang/String;
invokevirtual org/apache/commons/lang3/builder/HashCodeBuilder/append(Ljava/lang/Object;)Lorg/apache/commons/lang3/builder/HashCodeBuilder;
aload 0
getfield com/amazon/mas/kiwi/util/SignatureInfo/developerName Ljava/lang/String;
invokevirtual org/apache/commons/lang3/builder/HashCodeBuilder/append(Ljava/lang/Object;)Lorg/apache/commons/lang3/builder/HashCodeBuilder;
invokevirtual org/apache/commons/lang3/builder/HashCodeBuilder/toHashCode()I
ireturn
.limit locals 1
.limit stack 2
.end method

.method public isValidForCreatingKeystore()Z
aload 0
invokevirtual com/amazon/mas/kiwi/util/SignatureInfo/getId()Ljava/lang/String;
invokestatic org/apache/commons/lang3/StringUtils/isBlank(Ljava/lang/CharSequence;)Z
ifne L0
aload 0
invokevirtual com/amazon/mas/kiwi/util/SignatureInfo/getName()Ljava/lang/String;
invokestatic org/apache/commons/lang3/StringUtils/isBlank(Ljava/lang/CharSequence;)Z
ifne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public isValidForRetrievingKeyStore()Z
aload 0
invokevirtual com/amazon/mas/kiwi/util/SignatureInfo/getId()Ljava/lang/String;
invokestatic org/apache/commons/lang3/StringUtils/isBlank(Ljava/lang/CharSequence;)Z
ifne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public setCertificateId(Ljava/lang/String;)V
aload 0
aload 1
putfield com/amazon/mas/kiwi/util/SignatureInfo/certificateId Ljava/lang/String;
return
.limit locals 2
.limit stack 2
.end method

.method public setCommonName(Ljava/lang/String;)V
aload 0
aload 1
putfield com/amazon/mas/kiwi/util/SignatureInfo/commonName Ljava/lang/String;
return
.limit locals 2
.limit stack 2
.end method

.method public setDeveloperId(Ljava/lang/String;)V
aload 0
aload 1
putfield com/amazon/mas/kiwi/util/SignatureInfo/developerId Ljava/lang/String;
return
.limit locals 2
.limit stack 2
.end method

.method public setDeveloperName(Ljava/lang/String;)V
aload 0
aload 1
putfield com/amazon/mas/kiwi/util/SignatureInfo/developerName Ljava/lang/String;
return
.limit locals 2
.limit stack 2
.end method

.method public toString()Ljava/lang/String;
aload 0
getstatic org/apache/commons/lang3/builder/ToStringStyle/SHORT_PREFIX_STYLE Lorg/apache/commons/lang3/builder/ToStringStyle;
invokestatic org/apache/commons/lang3/builder/ReflectionToStringBuilder/toString(Ljava/lang/Object;Lorg/apache/commons/lang3/builder/ToStringStyle;)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method

.method public withCertificateId(Ljava/lang/String;)Lcom/amazon/mas/kiwi/util/SignatureInfo;
aload 0
aload 1
putfield com/amazon/mas/kiwi/util/SignatureInfo/certificateId Ljava/lang/String;
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public withCommonName(Ljava/lang/String;)Lcom/amazon/mas/kiwi/util/SignatureInfo;
aload 0
aload 1
putfield com/amazon/mas/kiwi/util/SignatureInfo/commonName Ljava/lang/String;
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public withDeveloperId(Ljava/lang/String;)Lcom/amazon/mas/kiwi/util/SignatureInfo;
aload 0
aload 1
putfield com/amazon/mas/kiwi/util/SignatureInfo/developerId Ljava/lang/String;
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public withDeveloperName(Ljava/lang/String;)Lcom/amazon/mas/kiwi/util/SignatureInfo;
aload 0
aload 1
putfield com/amazon/mas/kiwi/util/SignatureInfo/developerName Ljava/lang/String;
aload 0
areturn
.limit locals 2
.limit stack 2
.end method
