.bytecode 50.0
.class public synchronized com/amazon/mas/kiwi/util/Base64
.super java/lang/Object
.inner class public static InputStream inner com/amazon/mas/kiwi/util/Base64$InputStream outer com/amazon/mas/kiwi/util/Base64
.inner class public static OutputStream inner com/amazon/mas/kiwi/util/Base64$OutputStream outer com/amazon/mas/kiwi/util/Base64

.field static final synthetic '$assertionsDisabled' Z

.field public static final 'DECODE' I = 0


.field public static final 'DONT_GUNZIP' I = 4


.field public static final 'DO_BREAK_LINES' I = 8


.field public static final 'ENCODE' I = 1


.field private static final 'EQUALS_SIGN' B = 61


.field private static final 'EQUALS_SIGN_ENC' B = -1


.field public static final 'GZIP' I = 2


.field private static final 'MAX_LINE_LENGTH' I = 76


.field private static final 'NEW_LINE' B = 10


.field public static final 'NO_OPTIONS' I = 0


.field public static final 'ORDERED' I = 32


.field private static final 'PREFERRED_ENCODING' Ljava/lang/String; = "US-ASCII"

.field public static final 'URL_SAFE' I = 16


.field private static final 'WHITE_SPACE_ENC' B = -5


.field private static final '_ORDERED_ALPHABET' [B

.field private static final '_ORDERED_DECODABET' [B

.field private static final '_STANDARD_ALPHABET' [B

.field private static final '_STANDARD_DECODABET' [B

.field private static final '_URL_SAFE_ALPHABET' [B

.field private static final '_URL_SAFE_DECODABET' [B

.method static <clinit>()V
ldc com/amazon/mas/kiwi/util/Base64
invokevirtual java/lang/Class/desiredAssertionStatus()Z
ifne L0
iconst_1
istore 0
L1:
iload 0
putstatic com/amazon/mas/kiwi/util/Base64/$assertionsDisabled Z
bipush 64
newarray byte
dup
iconst_0
ldc_w 65
bastore
dup
iconst_1
ldc_w 66
bastore
dup
iconst_2
ldc_w 67
bastore
dup
iconst_3
ldc_w 68
bastore
dup
iconst_4
ldc_w 69
bastore
dup
iconst_5
ldc_w 70
bastore
dup
bipush 6
ldc_w 71
bastore
dup
bipush 7
ldc_w 72
bastore
dup
bipush 8
ldc_w 73
bastore
dup
bipush 9
ldc_w 74
bastore
dup
bipush 10
ldc_w 75
bastore
dup
bipush 11
ldc_w 76
bastore
dup
bipush 12
ldc_w 77
bastore
dup
bipush 13
ldc_w 78
bastore
dup
bipush 14
ldc_w 79
bastore
dup
bipush 15
ldc_w 80
bastore
dup
bipush 16
ldc_w 81
bastore
dup
bipush 17
ldc_w 82
bastore
dup
bipush 18
ldc_w 83
bastore
dup
bipush 19
ldc_w 84
bastore
dup
bipush 20
ldc_w 85
bastore
dup
bipush 21
ldc_w 86
bastore
dup
bipush 22
ldc_w 87
bastore
dup
bipush 23
ldc_w 88
bastore
dup
bipush 24
ldc_w 89
bastore
dup
bipush 25
ldc_w 90
bastore
dup
bipush 26
ldc_w 97
bastore
dup
bipush 27
ldc_w 98
bastore
dup
bipush 28
ldc_w 99
bastore
dup
bipush 29
ldc_w 100
bastore
dup
bipush 30
ldc_w 101
bastore
dup
bipush 31
ldc_w 102
bastore
dup
bipush 32
ldc_w 103
bastore
dup
bipush 33
ldc_w 104
bastore
dup
bipush 34
ldc_w 105
bastore
dup
bipush 35
ldc_w 106
bastore
dup
bipush 36
ldc_w 107
bastore
dup
bipush 37
ldc_w 108
bastore
dup
bipush 38
ldc_w 109
bastore
dup
bipush 39
ldc_w 110
bastore
dup
bipush 40
ldc_w 111
bastore
dup
bipush 41
ldc_w 112
bastore
dup
bipush 42
ldc_w 113
bastore
dup
bipush 43
ldc_w 114
bastore
dup
bipush 44
ldc_w 115
bastore
dup
bipush 45
ldc_w 116
bastore
dup
bipush 46
ldc_w 117
bastore
dup
bipush 47
ldc_w 118
bastore
dup
bipush 48
ldc_w 119
bastore
dup
bipush 49
ldc_w 120
bastore
dup
bipush 50
ldc_w 121
bastore
dup
bipush 51
ldc_w 122
bastore
dup
bipush 52
ldc_w 48
bastore
dup
bipush 53
ldc_w 49
bastore
dup
bipush 54
ldc_w 50
bastore
dup
bipush 55
ldc_w 51
bastore
dup
bipush 56
ldc_w 52
bastore
dup
bipush 57
ldc_w 53
bastore
dup
bipush 58
ldc_w 54
bastore
dup
bipush 59
ldc_w 55
bastore
dup
bipush 60
ldc_w 56
bastore
dup
bipush 61
ldc_w 57
bastore
dup
bipush 62
ldc_w 43
bastore
dup
bipush 63
ldc_w 47
bastore
putstatic com/amazon/mas/kiwi/util/Base64/_STANDARD_ALPHABET [B
sipush 256
newarray byte
dup
iconst_0
ldc_w -9
bastore
dup
iconst_1
ldc_w -9
bastore
dup
iconst_2
ldc_w -9
bastore
dup
iconst_3
ldc_w -9
bastore
dup
iconst_4
ldc_w -9
bastore
dup
iconst_5
ldc_w -9
bastore
dup
bipush 6
ldc_w -9
bastore
dup
bipush 7
ldc_w -9
bastore
dup
bipush 8
ldc_w -9
bastore
dup
bipush 9
ldc_w -5
bastore
dup
bipush 10
ldc_w -5
bastore
dup
bipush 11
ldc_w -9
bastore
dup
bipush 12
ldc_w -9
bastore
dup
bipush 13
ldc_w -5
bastore
dup
bipush 14
ldc_w -9
bastore
dup
bipush 15
ldc_w -9
bastore
dup
bipush 16
ldc_w -9
bastore
dup
bipush 17
ldc_w -9
bastore
dup
bipush 18
ldc_w -9
bastore
dup
bipush 19
ldc_w -9
bastore
dup
bipush 20
ldc_w -9
bastore
dup
bipush 21
ldc_w -9
bastore
dup
bipush 22
ldc_w -9
bastore
dup
bipush 23
ldc_w -9
bastore
dup
bipush 24
ldc_w -9
bastore
dup
bipush 25
ldc_w -9
bastore
dup
bipush 26
ldc_w -9
bastore
dup
bipush 27
ldc_w -9
bastore
dup
bipush 28
ldc_w -9
bastore
dup
bipush 29
ldc_w -9
bastore
dup
bipush 30
ldc_w -9
bastore
dup
bipush 31
ldc_w -9
bastore
dup
bipush 32
ldc_w -5
bastore
dup
bipush 33
ldc_w -9
bastore
dup
bipush 34
ldc_w -9
bastore
dup
bipush 35
ldc_w -9
bastore
dup
bipush 36
ldc_w -9
bastore
dup
bipush 37
ldc_w -9
bastore
dup
bipush 38
ldc_w -9
bastore
dup
bipush 39
ldc_w -9
bastore
dup
bipush 40
ldc_w -9
bastore
dup
bipush 41
ldc_w -9
bastore
dup
bipush 42
ldc_w -9
bastore
dup
bipush 43
ldc_w 62
bastore
dup
bipush 44
ldc_w -9
bastore
dup
bipush 45
ldc_w -9
bastore
dup
bipush 46
ldc_w -9
bastore
dup
bipush 47
ldc_w 63
bastore
dup
bipush 48
ldc_w 52
bastore
dup
bipush 49
ldc_w 53
bastore
dup
bipush 50
ldc_w 54
bastore
dup
bipush 51
ldc_w 55
bastore
dup
bipush 52
ldc_w 56
bastore
dup
bipush 53
ldc_w 57
bastore
dup
bipush 54
ldc_w 58
bastore
dup
bipush 55
ldc_w 59
bastore
dup
bipush 56
ldc_w 60
bastore
dup
bipush 57
ldc_w 61
bastore
dup
bipush 58
ldc_w -9
bastore
dup
bipush 59
ldc_w -9
bastore
dup
bipush 60
ldc_w -9
bastore
dup
bipush 61
ldc_w -1
bastore
dup
bipush 62
ldc_w -9
bastore
dup
bipush 63
ldc_w -9
bastore
dup
bipush 64
ldc_w -9
bastore
dup
bipush 65
ldc_w 0
bastore
dup
bipush 66
ldc_w 1
bastore
dup
bipush 67
ldc_w 2
bastore
dup
bipush 68
ldc_w 3
bastore
dup
bipush 69
ldc_w 4
bastore
dup
bipush 70
ldc_w 5
bastore
dup
bipush 71
ldc_w 6
bastore
dup
bipush 72
ldc_w 7
bastore
dup
bipush 73
ldc_w 8
bastore
dup
bipush 74
ldc_w 9
bastore
dup
bipush 75
ldc_w 10
bastore
dup
bipush 76
ldc_w 11
bastore
dup
bipush 77
ldc_w 12
bastore
dup
bipush 78
ldc_w 13
bastore
dup
bipush 79
ldc_w 14
bastore
dup
bipush 80
ldc_w 15
bastore
dup
bipush 81
ldc_w 16
bastore
dup
bipush 82
ldc_w 17
bastore
dup
bipush 83
ldc_w 18
bastore
dup
bipush 84
ldc_w 19
bastore
dup
bipush 85
ldc_w 20
bastore
dup
bipush 86
ldc_w 21
bastore
dup
bipush 87
ldc_w 22
bastore
dup
bipush 88
ldc_w 23
bastore
dup
bipush 89
ldc_w 24
bastore
dup
bipush 90
ldc_w 25
bastore
dup
bipush 91
ldc_w -9
bastore
dup
bipush 92
ldc_w -9
bastore
dup
bipush 93
ldc_w -9
bastore
dup
bipush 94
ldc_w -9
bastore
dup
bipush 95
ldc_w -9
bastore
dup
bipush 96
ldc_w -9
bastore
dup
bipush 97
ldc_w 26
bastore
dup
bipush 98
ldc_w 27
bastore
dup
bipush 99
ldc_w 28
bastore
dup
bipush 100
ldc_w 29
bastore
dup
bipush 101
ldc_w 30
bastore
dup
bipush 102
ldc_w 31
bastore
dup
bipush 103
ldc_w 32
bastore
dup
bipush 104
ldc_w 33
bastore
dup
bipush 105
ldc_w 34
bastore
dup
bipush 106
ldc_w 35
bastore
dup
bipush 107
ldc_w 36
bastore
dup
bipush 108
ldc_w 37
bastore
dup
bipush 109
ldc_w 38
bastore
dup
bipush 110
ldc_w 39
bastore
dup
bipush 111
ldc_w 40
bastore
dup
bipush 112
ldc_w 41
bastore
dup
bipush 113
ldc_w 42
bastore
dup
bipush 114
ldc_w 43
bastore
dup
bipush 115
ldc_w 44
bastore
dup
bipush 116
ldc_w 45
bastore
dup
bipush 117
ldc_w 46
bastore
dup
bipush 118
ldc_w 47
bastore
dup
bipush 119
ldc_w 48
bastore
dup
bipush 120
ldc_w 49
bastore
dup
bipush 121
ldc_w 50
bastore
dup
bipush 122
ldc_w 51
bastore
dup
bipush 123
ldc_w -9
bastore
dup
bipush 124
ldc_w -9
bastore
dup
bipush 125
ldc_w -9
bastore
dup
bipush 126
ldc_w -9
bastore
dup
bipush 127
ldc_w -9
bastore
dup
sipush 128
ldc_w -9
bastore
dup
sipush 129
ldc_w -9
bastore
dup
sipush 130
ldc_w -9
bastore
dup
sipush 131
ldc_w -9
bastore
dup
sipush 132
ldc_w -9
bastore
dup
sipush 133
ldc_w -9
bastore
dup
sipush 134
ldc_w -9
bastore
dup
sipush 135
ldc_w -9
bastore
dup
sipush 136
ldc_w -9
bastore
dup
sipush 137
ldc_w -9
bastore
dup
sipush 138
ldc_w -9
bastore
dup
sipush 139
ldc_w -9
bastore
dup
sipush 140
ldc_w -9
bastore
dup
sipush 141
ldc_w -9
bastore
dup
sipush 142
ldc_w -9
bastore
dup
sipush 143
ldc_w -9
bastore
dup
sipush 144
ldc_w -9
bastore
dup
sipush 145
ldc_w -9
bastore
dup
sipush 146
ldc_w -9
bastore
dup
sipush 147
ldc_w -9
bastore
dup
sipush 148
ldc_w -9
bastore
dup
sipush 149
ldc_w -9
bastore
dup
sipush 150
ldc_w -9
bastore
dup
sipush 151
ldc_w -9
bastore
dup
sipush 152
ldc_w -9
bastore
dup
sipush 153
ldc_w -9
bastore
dup
sipush 154
ldc_w -9
bastore
dup
sipush 155
ldc_w -9
bastore
dup
sipush 156
ldc_w -9
bastore
dup
sipush 157
ldc_w -9
bastore
dup
sipush 158
ldc_w -9
bastore
dup
sipush 159
ldc_w -9
bastore
dup
sipush 160
ldc_w -9
bastore
dup
sipush 161
ldc_w -9
bastore
dup
sipush 162
ldc_w -9
bastore
dup
sipush 163
ldc_w -9
bastore
dup
sipush 164
ldc_w -9
bastore
dup
sipush 165
ldc_w -9
bastore
dup
sipush 166
ldc_w -9
bastore
dup
sipush 167
ldc_w -9
bastore
dup
sipush 168
ldc_w -9
bastore
dup
sipush 169
ldc_w -9
bastore
dup
sipush 170
ldc_w -9
bastore
dup
sipush 171
ldc_w -9
bastore
dup
sipush 172
ldc_w -9
bastore
dup
sipush 173
ldc_w -9
bastore
dup
sipush 174
ldc_w -9
bastore
dup
sipush 175
ldc_w -9
bastore
dup
sipush 176
ldc_w -9
bastore
dup
sipush 177
ldc_w -9
bastore
dup
sipush 178
ldc_w -9
bastore
dup
sipush 179
ldc_w -9
bastore
dup
sipush 180
ldc_w -9
bastore
dup
sipush 181
ldc_w -9
bastore
dup
sipush 182
ldc_w -9
bastore
dup
sipush 183
ldc_w -9
bastore
dup
sipush 184
ldc_w -9
bastore
dup
sipush 185
ldc_w -9
bastore
dup
sipush 186
ldc_w -9
bastore
dup
sipush 187
ldc_w -9
bastore
dup
sipush 188
ldc_w -9
bastore
dup
sipush 189
ldc_w -9
bastore
dup
sipush 190
ldc_w -9
bastore
dup
sipush 191
ldc_w -9
bastore
dup
sipush 192
ldc_w -9
bastore
dup
sipush 193
ldc_w -9
bastore
dup
sipush 194
ldc_w -9
bastore
dup
sipush 195
ldc_w -9
bastore
dup
sipush 196
ldc_w -9
bastore
dup
sipush 197
ldc_w -9
bastore
dup
sipush 198
ldc_w -9
bastore
dup
sipush 199
ldc_w -9
bastore
dup
sipush 200
ldc_w -9
bastore
dup
sipush 201
ldc_w -9
bastore
dup
sipush 202
ldc_w -9
bastore
dup
sipush 203
ldc_w -9
bastore
dup
sipush 204
ldc_w -9
bastore
dup
sipush 205
ldc_w -9
bastore
dup
sipush 206
ldc_w -9
bastore
dup
sipush 207
ldc_w -9
bastore
dup
sipush 208
ldc_w -9
bastore
dup
sipush 209
ldc_w -9
bastore
dup
sipush 210
ldc_w -9
bastore
dup
sipush 211
ldc_w -9
bastore
dup
sipush 212
ldc_w -9
bastore
dup
sipush 213
ldc_w -9
bastore
dup
sipush 214
ldc_w -9
bastore
dup
sipush 215
ldc_w -9
bastore
dup
sipush 216
ldc_w -9
bastore
dup
sipush 217
ldc_w -9
bastore
dup
sipush 218
ldc_w -9
bastore
dup
sipush 219
ldc_w -9
bastore
dup
sipush 220
ldc_w -9
bastore
dup
sipush 221
ldc_w -9
bastore
dup
sipush 222
ldc_w -9
bastore
dup
sipush 223
ldc_w -9
bastore
dup
sipush 224
ldc_w -9
bastore
dup
sipush 225
ldc_w -9
bastore
dup
sipush 226
ldc_w -9
bastore
dup
sipush 227
ldc_w -9
bastore
dup
sipush 228
ldc_w -9
bastore
dup
sipush 229
ldc_w -9
bastore
dup
sipush 230
ldc_w -9
bastore
dup
sipush 231
ldc_w -9
bastore
dup
sipush 232
ldc_w -9
bastore
dup
sipush 233
ldc_w -9
bastore
dup
sipush 234
ldc_w -9
bastore
dup
sipush 235
ldc_w -9
bastore
dup
sipush 236
ldc_w -9
bastore
dup
sipush 237
ldc_w -9
bastore
dup
sipush 238
ldc_w -9
bastore
dup
sipush 239
ldc_w -9
bastore
dup
sipush 240
ldc_w -9
bastore
dup
sipush 241
ldc_w -9
bastore
dup
sipush 242
ldc_w -9
bastore
dup
sipush 243
ldc_w -9
bastore
dup
sipush 244
ldc_w -9
bastore
dup
sipush 245
ldc_w -9
bastore
dup
sipush 246
ldc_w -9
bastore
dup
sipush 247
ldc_w -9
bastore
dup
sipush 248
ldc_w -9
bastore
dup
sipush 249
ldc_w -9
bastore
dup
sipush 250
ldc_w -9
bastore
dup
sipush 251
ldc_w -9
bastore
dup
sipush 252
ldc_w -9
bastore
dup
sipush 253
ldc_w -9
bastore
dup
sipush 254
ldc_w -9
bastore
dup
sipush 255
ldc_w -9
bastore
putstatic com/amazon/mas/kiwi/util/Base64/_STANDARD_DECODABET [B
bipush 64
newarray byte
dup
iconst_0
ldc_w 65
bastore
dup
iconst_1
ldc_w 66
bastore
dup
iconst_2
ldc_w 67
bastore
dup
iconst_3
ldc_w 68
bastore
dup
iconst_4
ldc_w 69
bastore
dup
iconst_5
ldc_w 70
bastore
dup
bipush 6
ldc_w 71
bastore
dup
bipush 7
ldc_w 72
bastore
dup
bipush 8
ldc_w 73
bastore
dup
bipush 9
ldc_w 74
bastore
dup
bipush 10
ldc_w 75
bastore
dup
bipush 11
ldc_w 76
bastore
dup
bipush 12
ldc_w 77
bastore
dup
bipush 13
ldc_w 78
bastore
dup
bipush 14
ldc_w 79
bastore
dup
bipush 15
ldc_w 80
bastore
dup
bipush 16
ldc_w 81
bastore
dup
bipush 17
ldc_w 82
bastore
dup
bipush 18
ldc_w 83
bastore
dup
bipush 19
ldc_w 84
bastore
dup
bipush 20
ldc_w 85
bastore
dup
bipush 21
ldc_w 86
bastore
dup
bipush 22
ldc_w 87
bastore
dup
bipush 23
ldc_w 88
bastore
dup
bipush 24
ldc_w 89
bastore
dup
bipush 25
ldc_w 90
bastore
dup
bipush 26
ldc_w 97
bastore
dup
bipush 27
ldc_w 98
bastore
dup
bipush 28
ldc_w 99
bastore
dup
bipush 29
ldc_w 100
bastore
dup
bipush 30
ldc_w 101
bastore
dup
bipush 31
ldc_w 102
bastore
dup
bipush 32
ldc_w 103
bastore
dup
bipush 33
ldc_w 104
bastore
dup
bipush 34
ldc_w 105
bastore
dup
bipush 35
ldc_w 106
bastore
dup
bipush 36
ldc_w 107
bastore
dup
bipush 37
ldc_w 108
bastore
dup
bipush 38
ldc_w 109
bastore
dup
bipush 39
ldc_w 110
bastore
dup
bipush 40
ldc_w 111
bastore
dup
bipush 41
ldc_w 112
bastore
dup
bipush 42
ldc_w 113
bastore
dup
bipush 43
ldc_w 114
bastore
dup
bipush 44
ldc_w 115
bastore
dup
bipush 45
ldc_w 116
bastore
dup
bipush 46
ldc_w 117
bastore
dup
bipush 47
ldc_w 118
bastore
dup
bipush 48
ldc_w 119
bastore
dup
bipush 49
ldc_w 120
bastore
dup
bipush 50
ldc_w 121
bastore
dup
bipush 51
ldc_w 122
bastore
dup
bipush 52
ldc_w 48
bastore
dup
bipush 53
ldc_w 49
bastore
dup
bipush 54
ldc_w 50
bastore
dup
bipush 55
ldc_w 51
bastore
dup
bipush 56
ldc_w 52
bastore
dup
bipush 57
ldc_w 53
bastore
dup
bipush 58
ldc_w 54
bastore
dup
bipush 59
ldc_w 55
bastore
dup
bipush 60
ldc_w 56
bastore
dup
bipush 61
ldc_w 57
bastore
dup
bipush 62
ldc_w 45
bastore
dup
bipush 63
ldc_w 95
bastore
putstatic com/amazon/mas/kiwi/util/Base64/_URL_SAFE_ALPHABET [B
sipush 256
newarray byte
dup
iconst_0
ldc_w -9
bastore
dup
iconst_1
ldc_w -9
bastore
dup
iconst_2
ldc_w -9
bastore
dup
iconst_3
ldc_w -9
bastore
dup
iconst_4
ldc_w -9
bastore
dup
iconst_5
ldc_w -9
bastore
dup
bipush 6
ldc_w -9
bastore
dup
bipush 7
ldc_w -9
bastore
dup
bipush 8
ldc_w -9
bastore
dup
bipush 9
ldc_w -5
bastore
dup
bipush 10
ldc_w -5
bastore
dup
bipush 11
ldc_w -9
bastore
dup
bipush 12
ldc_w -9
bastore
dup
bipush 13
ldc_w -5
bastore
dup
bipush 14
ldc_w -9
bastore
dup
bipush 15
ldc_w -9
bastore
dup
bipush 16
ldc_w -9
bastore
dup
bipush 17
ldc_w -9
bastore
dup
bipush 18
ldc_w -9
bastore
dup
bipush 19
ldc_w -9
bastore
dup
bipush 20
ldc_w -9
bastore
dup
bipush 21
ldc_w -9
bastore
dup
bipush 22
ldc_w -9
bastore
dup
bipush 23
ldc_w -9
bastore
dup
bipush 24
ldc_w -9
bastore
dup
bipush 25
ldc_w -9
bastore
dup
bipush 26
ldc_w -9
bastore
dup
bipush 27
ldc_w -9
bastore
dup
bipush 28
ldc_w -9
bastore
dup
bipush 29
ldc_w -9
bastore
dup
bipush 30
ldc_w -9
bastore
dup
bipush 31
ldc_w -9
bastore
dup
bipush 32
ldc_w -5
bastore
dup
bipush 33
ldc_w -9
bastore
dup
bipush 34
ldc_w -9
bastore
dup
bipush 35
ldc_w -9
bastore
dup
bipush 36
ldc_w -9
bastore
dup
bipush 37
ldc_w -9
bastore
dup
bipush 38
ldc_w -9
bastore
dup
bipush 39
ldc_w -9
bastore
dup
bipush 40
ldc_w -9
bastore
dup
bipush 41
ldc_w -9
bastore
dup
bipush 42
ldc_w -9
bastore
dup
bipush 43
ldc_w -9
bastore
dup
bipush 44
ldc_w -9
bastore
dup
bipush 45
ldc_w 62
bastore
dup
bipush 46
ldc_w -9
bastore
dup
bipush 47
ldc_w -9
bastore
dup
bipush 48
ldc_w 52
bastore
dup
bipush 49
ldc_w 53
bastore
dup
bipush 50
ldc_w 54
bastore
dup
bipush 51
ldc_w 55
bastore
dup
bipush 52
ldc_w 56
bastore
dup
bipush 53
ldc_w 57
bastore
dup
bipush 54
ldc_w 58
bastore
dup
bipush 55
ldc_w 59
bastore
dup
bipush 56
ldc_w 60
bastore
dup
bipush 57
ldc_w 61
bastore
dup
bipush 58
ldc_w -9
bastore
dup
bipush 59
ldc_w -9
bastore
dup
bipush 60
ldc_w -9
bastore
dup
bipush 61
ldc_w -1
bastore
dup
bipush 62
ldc_w -9
bastore
dup
bipush 63
ldc_w -9
bastore
dup
bipush 64
ldc_w -9
bastore
dup
bipush 65
ldc_w 0
bastore
dup
bipush 66
ldc_w 1
bastore
dup
bipush 67
ldc_w 2
bastore
dup
bipush 68
ldc_w 3
bastore
dup
bipush 69
ldc_w 4
bastore
dup
bipush 70
ldc_w 5
bastore
dup
bipush 71
ldc_w 6
bastore
dup
bipush 72
ldc_w 7
bastore
dup
bipush 73
ldc_w 8
bastore
dup
bipush 74
ldc_w 9
bastore
dup
bipush 75
ldc_w 10
bastore
dup
bipush 76
ldc_w 11
bastore
dup
bipush 77
ldc_w 12
bastore
dup
bipush 78
ldc_w 13
bastore
dup
bipush 79
ldc_w 14
bastore
dup
bipush 80
ldc_w 15
bastore
dup
bipush 81
ldc_w 16
bastore
dup
bipush 82
ldc_w 17
bastore
dup
bipush 83
ldc_w 18
bastore
dup
bipush 84
ldc_w 19
bastore
dup
bipush 85
ldc_w 20
bastore
dup
bipush 86
ldc_w 21
bastore
dup
bipush 87
ldc_w 22
bastore
dup
bipush 88
ldc_w 23
bastore
dup
bipush 89
ldc_w 24
bastore
dup
bipush 90
ldc_w 25
bastore
dup
bipush 91
ldc_w -9
bastore
dup
bipush 92
ldc_w -9
bastore
dup
bipush 93
ldc_w -9
bastore
dup
bipush 94
ldc_w -9
bastore
dup
bipush 95
ldc_w 63
bastore
dup
bipush 96
ldc_w -9
bastore
dup
bipush 97
ldc_w 26
bastore
dup
bipush 98
ldc_w 27
bastore
dup
bipush 99
ldc_w 28
bastore
dup
bipush 100
ldc_w 29
bastore
dup
bipush 101
ldc_w 30
bastore
dup
bipush 102
ldc_w 31
bastore
dup
bipush 103
ldc_w 32
bastore
dup
bipush 104
ldc_w 33
bastore
dup
bipush 105
ldc_w 34
bastore
dup
bipush 106
ldc_w 35
bastore
dup
bipush 107
ldc_w 36
bastore
dup
bipush 108
ldc_w 37
bastore
dup
bipush 109
ldc_w 38
bastore
dup
bipush 110
ldc_w 39
bastore
dup
bipush 111
ldc_w 40
bastore
dup
bipush 112
ldc_w 41
bastore
dup
bipush 113
ldc_w 42
bastore
dup
bipush 114
ldc_w 43
bastore
dup
bipush 115
ldc_w 44
bastore
dup
bipush 116
ldc_w 45
bastore
dup
bipush 117
ldc_w 46
bastore
dup
bipush 118
ldc_w 47
bastore
dup
bipush 119
ldc_w 48
bastore
dup
bipush 120
ldc_w 49
bastore
dup
bipush 121
ldc_w 50
bastore
dup
bipush 122
ldc_w 51
bastore
dup
bipush 123
ldc_w -9
bastore
dup
bipush 124
ldc_w -9
bastore
dup
bipush 125
ldc_w -9
bastore
dup
bipush 126
ldc_w -9
bastore
dup
bipush 127
ldc_w -9
bastore
dup
sipush 128
ldc_w -9
bastore
dup
sipush 129
ldc_w -9
bastore
dup
sipush 130
ldc_w -9
bastore
dup
sipush 131
ldc_w -9
bastore
dup
sipush 132
ldc_w -9
bastore
dup
sipush 133
ldc_w -9
bastore
dup
sipush 134
ldc_w -9
bastore
dup
sipush 135
ldc_w -9
bastore
dup
sipush 136
ldc_w -9
bastore
dup
sipush 137
ldc_w -9
bastore
dup
sipush 138
ldc_w -9
bastore
dup
sipush 139
ldc_w -9
bastore
dup
sipush 140
ldc_w -9
bastore
dup
sipush 141
ldc_w -9
bastore
dup
sipush 142
ldc_w -9
bastore
dup
sipush 143
ldc_w -9
bastore
dup
sipush 144
ldc_w -9
bastore
dup
sipush 145
ldc_w -9
bastore
dup
sipush 146
ldc_w -9
bastore
dup
sipush 147
ldc_w -9
bastore
dup
sipush 148
ldc_w -9
bastore
dup
sipush 149
ldc_w -9
bastore
dup
sipush 150
ldc_w -9
bastore
dup
sipush 151
ldc_w -9
bastore
dup
sipush 152
ldc_w -9
bastore
dup
sipush 153
ldc_w -9
bastore
dup
sipush 154
ldc_w -9
bastore
dup
sipush 155
ldc_w -9
bastore
dup
sipush 156
ldc_w -9
bastore
dup
sipush 157
ldc_w -9
bastore
dup
sipush 158
ldc_w -9
bastore
dup
sipush 159
ldc_w -9
bastore
dup
sipush 160
ldc_w -9
bastore
dup
sipush 161
ldc_w -9
bastore
dup
sipush 162
ldc_w -9
bastore
dup
sipush 163
ldc_w -9
bastore
dup
sipush 164
ldc_w -9
bastore
dup
sipush 165
ldc_w -9
bastore
dup
sipush 166
ldc_w -9
bastore
dup
sipush 167
ldc_w -9
bastore
dup
sipush 168
ldc_w -9
bastore
dup
sipush 169
ldc_w -9
bastore
dup
sipush 170
ldc_w -9
bastore
dup
sipush 171
ldc_w -9
bastore
dup
sipush 172
ldc_w -9
bastore
dup
sipush 173
ldc_w -9
bastore
dup
sipush 174
ldc_w -9
bastore
dup
sipush 175
ldc_w -9
bastore
dup
sipush 176
ldc_w -9
bastore
dup
sipush 177
ldc_w -9
bastore
dup
sipush 178
ldc_w -9
bastore
dup
sipush 179
ldc_w -9
bastore
dup
sipush 180
ldc_w -9
bastore
dup
sipush 181
ldc_w -9
bastore
dup
sipush 182
ldc_w -9
bastore
dup
sipush 183
ldc_w -9
bastore
dup
sipush 184
ldc_w -9
bastore
dup
sipush 185
ldc_w -9
bastore
dup
sipush 186
ldc_w -9
bastore
dup
sipush 187
ldc_w -9
bastore
dup
sipush 188
ldc_w -9
bastore
dup
sipush 189
ldc_w -9
bastore
dup
sipush 190
ldc_w -9
bastore
dup
sipush 191
ldc_w -9
bastore
dup
sipush 192
ldc_w -9
bastore
dup
sipush 193
ldc_w -9
bastore
dup
sipush 194
ldc_w -9
bastore
dup
sipush 195
ldc_w -9
bastore
dup
sipush 196
ldc_w -9
bastore
dup
sipush 197
ldc_w -9
bastore
dup
sipush 198
ldc_w -9
bastore
dup
sipush 199
ldc_w -9
bastore
dup
sipush 200
ldc_w -9
bastore
dup
sipush 201
ldc_w -9
bastore
dup
sipush 202
ldc_w -9
bastore
dup
sipush 203
ldc_w -9
bastore
dup
sipush 204
ldc_w -9
bastore
dup
sipush 205
ldc_w -9
bastore
dup
sipush 206
ldc_w -9
bastore
dup
sipush 207
ldc_w -9
bastore
dup
sipush 208
ldc_w -9
bastore
dup
sipush 209
ldc_w -9
bastore
dup
sipush 210
ldc_w -9
bastore
dup
sipush 211
ldc_w -9
bastore
dup
sipush 212
ldc_w -9
bastore
dup
sipush 213
ldc_w -9
bastore
dup
sipush 214
ldc_w -9
bastore
dup
sipush 215
ldc_w -9
bastore
dup
sipush 216
ldc_w -9
bastore
dup
sipush 217
ldc_w -9
bastore
dup
sipush 218
ldc_w -9
bastore
dup
sipush 219
ldc_w -9
bastore
dup
sipush 220
ldc_w -9
bastore
dup
sipush 221
ldc_w -9
bastore
dup
sipush 222
ldc_w -9
bastore
dup
sipush 223
ldc_w -9
bastore
dup
sipush 224
ldc_w -9
bastore
dup
sipush 225
ldc_w -9
bastore
dup
sipush 226
ldc_w -9
bastore
dup
sipush 227
ldc_w -9
bastore
dup
sipush 228
ldc_w -9
bastore
dup
sipush 229
ldc_w -9
bastore
dup
sipush 230
ldc_w -9
bastore
dup
sipush 231
ldc_w -9
bastore
dup
sipush 232
ldc_w -9
bastore
dup
sipush 233
ldc_w -9
bastore
dup
sipush 234
ldc_w -9
bastore
dup
sipush 235
ldc_w -9
bastore
dup
sipush 236
ldc_w -9
bastore
dup
sipush 237
ldc_w -9
bastore
dup
sipush 238
ldc_w -9
bastore
dup
sipush 239
ldc_w -9
bastore
dup
sipush 240
ldc_w -9
bastore
dup
sipush 241
ldc_w -9
bastore
dup
sipush 242
ldc_w -9
bastore
dup
sipush 243
ldc_w -9
bastore
dup
sipush 244
ldc_w -9
bastore
dup
sipush 245
ldc_w -9
bastore
dup
sipush 246
ldc_w -9
bastore
dup
sipush 247
ldc_w -9
bastore
dup
sipush 248
ldc_w -9
bastore
dup
sipush 249
ldc_w -9
bastore
dup
sipush 250
ldc_w -9
bastore
dup
sipush 251
ldc_w -9
bastore
dup
sipush 252
ldc_w -9
bastore
dup
sipush 253
ldc_w -9
bastore
dup
sipush 254
ldc_w -9
bastore
dup
sipush 255
ldc_w -9
bastore
putstatic com/amazon/mas/kiwi/util/Base64/_URL_SAFE_DECODABET [B
bipush 64
newarray byte
dup
iconst_0
ldc_w 45
bastore
dup
iconst_1
ldc_w 48
bastore
dup
iconst_2
ldc_w 49
bastore
dup
iconst_3
ldc_w 50
bastore
dup
iconst_4
ldc_w 51
bastore
dup
iconst_5
ldc_w 52
bastore
dup
bipush 6
ldc_w 53
bastore
dup
bipush 7
ldc_w 54
bastore
dup
bipush 8
ldc_w 55
bastore
dup
bipush 9
ldc_w 56
bastore
dup
bipush 10
ldc_w 57
bastore
dup
bipush 11
ldc_w 65
bastore
dup
bipush 12
ldc_w 66
bastore
dup
bipush 13
ldc_w 67
bastore
dup
bipush 14
ldc_w 68
bastore
dup
bipush 15
ldc_w 69
bastore
dup
bipush 16
ldc_w 70
bastore
dup
bipush 17
ldc_w 71
bastore
dup
bipush 18
ldc_w 72
bastore
dup
bipush 19
ldc_w 73
bastore
dup
bipush 20
ldc_w 74
bastore
dup
bipush 21
ldc_w 75
bastore
dup
bipush 22
ldc_w 76
bastore
dup
bipush 23
ldc_w 77
bastore
dup
bipush 24
ldc_w 78
bastore
dup
bipush 25
ldc_w 79
bastore
dup
bipush 26
ldc_w 80
bastore
dup
bipush 27
ldc_w 81
bastore
dup
bipush 28
ldc_w 82
bastore
dup
bipush 29
ldc_w 83
bastore
dup
bipush 30
ldc_w 84
bastore
dup
bipush 31
ldc_w 85
bastore
dup
bipush 32
ldc_w 86
bastore
dup
bipush 33
ldc_w 87
bastore
dup
bipush 34
ldc_w 88
bastore
dup
bipush 35
ldc_w 89
bastore
dup
bipush 36
ldc_w 90
bastore
dup
bipush 37
ldc_w 95
bastore
dup
bipush 38
ldc_w 97
bastore
dup
bipush 39
ldc_w 98
bastore
dup
bipush 40
ldc_w 99
bastore
dup
bipush 41
ldc_w 100
bastore
dup
bipush 42
ldc_w 101
bastore
dup
bipush 43
ldc_w 102
bastore
dup
bipush 44
ldc_w 103
bastore
dup
bipush 45
ldc_w 104
bastore
dup
bipush 46
ldc_w 105
bastore
dup
bipush 47
ldc_w 106
bastore
dup
bipush 48
ldc_w 107
bastore
dup
bipush 49
ldc_w 108
bastore
dup
bipush 50
ldc_w 109
bastore
dup
bipush 51
ldc_w 110
bastore
dup
bipush 52
ldc_w 111
bastore
dup
bipush 53
ldc_w 112
bastore
dup
bipush 54
ldc_w 113
bastore
dup
bipush 55
ldc_w 114
bastore
dup
bipush 56
ldc_w 115
bastore
dup
bipush 57
ldc_w 116
bastore
dup
bipush 58
ldc_w 117
bastore
dup
bipush 59
ldc_w 118
bastore
dup
bipush 60
ldc_w 119
bastore
dup
bipush 61
ldc_w 120
bastore
dup
bipush 62
ldc_w 121
bastore
dup
bipush 63
ldc_w 122
bastore
putstatic com/amazon/mas/kiwi/util/Base64/_ORDERED_ALPHABET [B
sipush 257
newarray byte
dup
iconst_0
ldc_w -9
bastore
dup
iconst_1
ldc_w -9
bastore
dup
iconst_2
ldc_w -9
bastore
dup
iconst_3
ldc_w -9
bastore
dup
iconst_4
ldc_w -9
bastore
dup
iconst_5
ldc_w -9
bastore
dup
bipush 6
ldc_w -9
bastore
dup
bipush 7
ldc_w -9
bastore
dup
bipush 8
ldc_w -9
bastore
dup
bipush 9
ldc_w -5
bastore
dup
bipush 10
ldc_w -5
bastore
dup
bipush 11
ldc_w -9
bastore
dup
bipush 12
ldc_w -9
bastore
dup
bipush 13
ldc_w -5
bastore
dup
bipush 14
ldc_w -9
bastore
dup
bipush 15
ldc_w -9
bastore
dup
bipush 16
ldc_w -9
bastore
dup
bipush 17
ldc_w -9
bastore
dup
bipush 18
ldc_w -9
bastore
dup
bipush 19
ldc_w -9
bastore
dup
bipush 20
ldc_w -9
bastore
dup
bipush 21
ldc_w -9
bastore
dup
bipush 22
ldc_w -9
bastore
dup
bipush 23
ldc_w -9
bastore
dup
bipush 24
ldc_w -9
bastore
dup
bipush 25
ldc_w -9
bastore
dup
bipush 26
ldc_w -9
bastore
dup
bipush 27
ldc_w -9
bastore
dup
bipush 28
ldc_w -9
bastore
dup
bipush 29
ldc_w -9
bastore
dup
bipush 30
ldc_w -9
bastore
dup
bipush 31
ldc_w -9
bastore
dup
bipush 32
ldc_w -5
bastore
dup
bipush 33
ldc_w -9
bastore
dup
bipush 34
ldc_w -9
bastore
dup
bipush 35
ldc_w -9
bastore
dup
bipush 36
ldc_w -9
bastore
dup
bipush 37
ldc_w -9
bastore
dup
bipush 38
ldc_w -9
bastore
dup
bipush 39
ldc_w -9
bastore
dup
bipush 40
ldc_w -9
bastore
dup
bipush 41
ldc_w -9
bastore
dup
bipush 42
ldc_w -9
bastore
dup
bipush 43
ldc_w -9
bastore
dup
bipush 44
ldc_w -9
bastore
dup
bipush 45
ldc_w 0
bastore
dup
bipush 46
ldc_w -9
bastore
dup
bipush 47
ldc_w -9
bastore
dup
bipush 48
ldc_w 1
bastore
dup
bipush 49
ldc_w 2
bastore
dup
bipush 50
ldc_w 3
bastore
dup
bipush 51
ldc_w 4
bastore
dup
bipush 52
ldc_w 5
bastore
dup
bipush 53
ldc_w 6
bastore
dup
bipush 54
ldc_w 7
bastore
dup
bipush 55
ldc_w 8
bastore
dup
bipush 56
ldc_w 9
bastore
dup
bipush 57
ldc_w 10
bastore
dup
bipush 58
ldc_w -9
bastore
dup
bipush 59
ldc_w -9
bastore
dup
bipush 60
ldc_w -9
bastore
dup
bipush 61
ldc_w -1
bastore
dup
bipush 62
ldc_w -9
bastore
dup
bipush 63
ldc_w -9
bastore
dup
bipush 64
ldc_w -9
bastore
dup
bipush 65
ldc_w 11
bastore
dup
bipush 66
ldc_w 12
bastore
dup
bipush 67
ldc_w 13
bastore
dup
bipush 68
ldc_w 14
bastore
dup
bipush 69
ldc_w 15
bastore
dup
bipush 70
ldc_w 16
bastore
dup
bipush 71
ldc_w 17
bastore
dup
bipush 72
ldc_w 18
bastore
dup
bipush 73
ldc_w 19
bastore
dup
bipush 74
ldc_w 20
bastore
dup
bipush 75
ldc_w 21
bastore
dup
bipush 76
ldc_w 22
bastore
dup
bipush 77
ldc_w 23
bastore
dup
bipush 78
ldc_w 24
bastore
dup
bipush 79
ldc_w 25
bastore
dup
bipush 80
ldc_w 26
bastore
dup
bipush 81
ldc_w 27
bastore
dup
bipush 82
ldc_w 28
bastore
dup
bipush 83
ldc_w 29
bastore
dup
bipush 84
ldc_w 30
bastore
dup
bipush 85
ldc_w 31
bastore
dup
bipush 86
ldc_w 32
bastore
dup
bipush 87
ldc_w 33
bastore
dup
bipush 88
ldc_w 34
bastore
dup
bipush 89
ldc_w 35
bastore
dup
bipush 90
ldc_w 36
bastore
dup
bipush 91
ldc_w -9
bastore
dup
bipush 92
ldc_w -9
bastore
dup
bipush 93
ldc_w -9
bastore
dup
bipush 94
ldc_w -9
bastore
dup
bipush 95
ldc_w 37
bastore
dup
bipush 96
ldc_w -9
bastore
dup
bipush 97
ldc_w 38
bastore
dup
bipush 98
ldc_w 39
bastore
dup
bipush 99
ldc_w 40
bastore
dup
bipush 100
ldc_w 41
bastore
dup
bipush 101
ldc_w 42
bastore
dup
bipush 102
ldc_w 43
bastore
dup
bipush 103
ldc_w 44
bastore
dup
bipush 104
ldc_w 45
bastore
dup
bipush 105
ldc_w 46
bastore
dup
bipush 106
ldc_w 47
bastore
dup
bipush 107
ldc_w 48
bastore
dup
bipush 108
ldc_w 49
bastore
dup
bipush 109
ldc_w 50
bastore
dup
bipush 110
ldc_w 51
bastore
dup
bipush 111
ldc_w 52
bastore
dup
bipush 112
ldc_w 53
bastore
dup
bipush 113
ldc_w 54
bastore
dup
bipush 114
ldc_w 55
bastore
dup
bipush 115
ldc_w 56
bastore
dup
bipush 116
ldc_w 57
bastore
dup
bipush 117
ldc_w 58
bastore
dup
bipush 118
ldc_w 59
bastore
dup
bipush 119
ldc_w 60
bastore
dup
bipush 120
ldc_w 61
bastore
dup
bipush 121
ldc_w 62
bastore
dup
bipush 122
ldc_w 63
bastore
dup
bipush 123
ldc_w -9
bastore
dup
bipush 124
ldc_w -9
bastore
dup
bipush 125
ldc_w -9
bastore
dup
bipush 126
ldc_w -9
bastore
dup
bipush 127
ldc_w -9
bastore
dup
sipush 128
ldc_w -9
bastore
dup
sipush 129
ldc_w -9
bastore
dup
sipush 130
ldc_w -9
bastore
dup
sipush 131
ldc_w -9
bastore
dup
sipush 132
ldc_w -9
bastore
dup
sipush 133
ldc_w -9
bastore
dup
sipush 134
ldc_w -9
bastore
dup
sipush 135
ldc_w -9
bastore
dup
sipush 136
ldc_w -9
bastore
dup
sipush 137
ldc_w -9
bastore
dup
sipush 138
ldc_w -9
bastore
dup
sipush 139
ldc_w -9
bastore
dup
sipush 140
ldc_w -9
bastore
dup
sipush 141
ldc_w -9
bastore
dup
sipush 142
ldc_w -9
bastore
dup
sipush 143
ldc_w -9
bastore
dup
sipush 144
ldc_w -9
bastore
dup
sipush 145
ldc_w -9
bastore
dup
sipush 146
ldc_w -9
bastore
dup
sipush 147
ldc_w -9
bastore
dup
sipush 148
ldc_w -9
bastore
dup
sipush 149
ldc_w -9
bastore
dup
sipush 150
ldc_w -9
bastore
dup
sipush 151
ldc_w -9
bastore
dup
sipush 152
ldc_w -9
bastore
dup
sipush 153
ldc_w -9
bastore
dup
sipush 154
ldc_w -9
bastore
dup
sipush 155
ldc_w -9
bastore
dup
sipush 156
ldc_w -9
bastore
dup
sipush 157
ldc_w -9
bastore
dup
sipush 158
ldc_w -9
bastore
dup
sipush 159
ldc_w -9
bastore
dup
sipush 160
ldc_w -9
bastore
dup
sipush 161
ldc_w -9
bastore
dup
sipush 162
ldc_w -9
bastore
dup
sipush 163
ldc_w -9
bastore
dup
sipush 164
ldc_w -9
bastore
dup
sipush 165
ldc_w -9
bastore
dup
sipush 166
ldc_w -9
bastore
dup
sipush 167
ldc_w -9
bastore
dup
sipush 168
ldc_w -9
bastore
dup
sipush 169
ldc_w -9
bastore
dup
sipush 170
ldc_w -9
bastore
dup
sipush 171
ldc_w -9
bastore
dup
sipush 172
ldc_w -9
bastore
dup
sipush 173
ldc_w -9
bastore
dup
sipush 174
ldc_w -9
bastore
dup
sipush 175
ldc_w -9
bastore
dup
sipush 176
ldc_w -9
bastore
dup
sipush 177
ldc_w -9
bastore
dup
sipush 178
ldc_w -9
bastore
dup
sipush 179
ldc_w -9
bastore
dup
sipush 180
ldc_w -9
bastore
dup
sipush 181
ldc_w -9
bastore
dup
sipush 182
ldc_w -9
bastore
dup
sipush 183
ldc_w -9
bastore
dup
sipush 184
ldc_w -9
bastore
dup
sipush 185
ldc_w -9
bastore
dup
sipush 186
ldc_w -9
bastore
dup
sipush 187
ldc_w -9
bastore
dup
sipush 188
ldc_w -9
bastore
dup
sipush 189
ldc_w -9
bastore
dup
sipush 190
ldc_w -9
bastore
dup
sipush 191
ldc_w -9
bastore
dup
sipush 192
ldc_w -9
bastore
dup
sipush 193
ldc_w -9
bastore
dup
sipush 194
ldc_w -9
bastore
dup
sipush 195
ldc_w -9
bastore
dup
sipush 196
ldc_w -9
bastore
dup
sipush 197
ldc_w -9
bastore
dup
sipush 198
ldc_w -9
bastore
dup
sipush 199
ldc_w -9
bastore
dup
sipush 200
ldc_w -9
bastore
dup
sipush 201
ldc_w -9
bastore
dup
sipush 202
ldc_w -9
bastore
dup
sipush 203
ldc_w -9
bastore
dup
sipush 204
ldc_w -9
bastore
dup
sipush 205
ldc_w -9
bastore
dup
sipush 206
ldc_w -9
bastore
dup
sipush 207
ldc_w -9
bastore
dup
sipush 208
ldc_w -9
bastore
dup
sipush 209
ldc_w -9
bastore
dup
sipush 210
ldc_w -9
bastore
dup
sipush 211
ldc_w -9
bastore
dup
sipush 212
ldc_w -9
bastore
dup
sipush 213
ldc_w -9
bastore
dup
sipush 214
ldc_w -9
bastore
dup
sipush 215
ldc_w -9
bastore
dup
sipush 216
ldc_w -9
bastore
dup
sipush 217
ldc_w -9
bastore
dup
sipush 218
ldc_w -9
bastore
dup
sipush 219
ldc_w -9
bastore
dup
sipush 220
ldc_w -9
bastore
dup
sipush 221
ldc_w -9
bastore
dup
sipush 222
ldc_w -9
bastore
dup
sipush 223
ldc_w -9
bastore
dup
sipush 224
ldc_w -9
bastore
dup
sipush 225
ldc_w -9
bastore
dup
sipush 226
ldc_w -9
bastore
dup
sipush 227
ldc_w -9
bastore
dup
sipush 228
ldc_w -9
bastore
dup
sipush 229
ldc_w -9
bastore
dup
sipush 230
ldc_w -9
bastore
dup
sipush 231
ldc_w -9
bastore
dup
sipush 232
ldc_w -9
bastore
dup
sipush 233
ldc_w -9
bastore
dup
sipush 234
ldc_w -9
bastore
dup
sipush 235
ldc_w -9
bastore
dup
sipush 236
ldc_w -9
bastore
dup
sipush 237
ldc_w -9
bastore
dup
sipush 238
ldc_w -9
bastore
dup
sipush 239
ldc_w -9
bastore
dup
sipush 240
ldc_w -9
bastore
dup
sipush 241
ldc_w -9
bastore
dup
sipush 242
ldc_w -9
bastore
dup
sipush 243
ldc_w -9
bastore
dup
sipush 244
ldc_w -9
bastore
dup
sipush 245
ldc_w -9
bastore
dup
sipush 246
ldc_w -9
bastore
dup
sipush 247
ldc_w -9
bastore
dup
sipush 248
ldc_w -9
bastore
dup
sipush 249
ldc_w -9
bastore
dup
sipush 250
ldc_w -9
bastore
dup
sipush 251
ldc_w -9
bastore
dup
sipush 252
ldc_w -9
bastore
dup
sipush 253
ldc_w -9
bastore
dup
sipush 254
ldc_w -9
bastore
dup
sipush 255
ldc_w -9
bastore
dup
sipush 256
ldc_w -9
bastore
putstatic com/amazon/mas/kiwi/util/Base64/_ORDERED_DECODABET [B
return
L0:
iconst_0
istore 0
goto L1
.limit locals 1
.limit stack 4
.end method

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method static synthetic access$000(I)[B
iload 0
invokestatic com/amazon/mas/kiwi/util/Base64/getDecodabet(I)[B
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic access$100([BII[BII)[B
aload 0
iload 1
iload 2
aload 3
iload 4
iload 5
invokestatic com/amazon/mas/kiwi/util/Base64/encode3to4([BII[BII)[B
areturn
.limit locals 6
.limit stack 6
.end method

.method static synthetic access$200([BI[BII)I
aload 0
iload 1
aload 2
iload 3
iload 4
invokestatic com/amazon/mas/kiwi/util/Base64/decode4to3([BI[BII)I
ireturn
.limit locals 5
.limit stack 5
.end method

.method static synthetic access$300([B[BII)[B
aload 0
aload 1
iload 2
iload 3
invokestatic com/amazon/mas/kiwi/util/Base64/encode3to4([B[BII)[B
areturn
.limit locals 4
.limit stack 4
.end method

.method public static decode(Ljava/lang/String;)[B
.throws java/io/IOException
aload 0
iconst_0
invokestatic com/amazon/mas/kiwi/util/Base64/decode(Ljava/lang/String;I)[B
areturn
.limit locals 1
.limit stack 2
.end method

.method public static decode(Ljava/lang/String;I)[B
.throws java/io/IOException
.catch java/io/UnsupportedEncodingException from L0 to L1 using L2
.catch java/io/IOException from L3 to L4 using L5
.catch all from L3 to L4 using L6
.catch java/io/IOException from L4 to L7 using L8
.catch all from L4 to L7 using L9
.catch java/io/IOException from L7 to L10 using L11
.catch all from L7 to L10 using L12
.catch java/io/IOException from L13 to L14 using L15
.catch all from L13 to L14 using L16
.catch java/io/IOException from L17 to L18 using L15
.catch all from L17 to L18 using L16
.catch all from L19 to L20 using L21
.catch java/lang/Exception from L20 to L22 using L23
.catch java/lang/Exception from L22 to L24 using L25
.catch java/lang/Exception from L24 to L26 using L27
.catch java/io/IOException from L28 to L29 using L15
.catch all from L28 to L29 using L16
.catch java/lang/Exception from L29 to L30 using L31
.catch java/lang/Exception from L30 to L32 using L33
.catch java/lang/Exception from L32 to L34 using L35
.catch java/lang/Exception from L36 to L37 using L38
.catch java/lang/Exception from L37 to L39 using L40
.catch java/lang/Exception from L39 to L41 using L42
aload 0
ifnonnull L0
new java/lang/NullPointerException
dup
ldc "Input string was null."
invokespecial java/lang/NullPointerException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
ldc "US-ASCII"
invokevirtual java/lang/String/getBytes(Ljava/lang/String;)[B
astore 4
L1:
aload 4
astore 0
L43:
aload 0
iconst_0
aload 0
arraylength
iload 1
invokestatic com/amazon/mas/kiwi/util/Base64/decode([BIII)[B
astore 9
iload 1
iconst_4
iand
ifeq L44
iconst_1
istore 1
L45:
aload 9
ifnull L46
aload 9
arraylength
iconst_4
if_icmplt L46
iload 1
ifne L46
ldc_w 35615
aload 9
iconst_0
baload
sipush 255
iand
aload 9
iconst_1
baload
bipush 8
ishl
ldc_w 65280
iand
ior
if_icmpne L46
aconst_null
astore 6
aconst_null
astore 7
aconst_null
astore 8
aconst_null
astore 5
sipush 2048
newarray byte
astore 10
L3:
new java/io/ByteArrayOutputStream
dup
invokespecial java/io/ByteArrayOutputStream/<init>()V
astore 0
L4:
new java/io/ByteArrayInputStream
dup
aload 9
invokespecial java/io/ByteArrayInputStream/<init>([B)V
astore 4
L7:
new java/util/zip/GZIPInputStream
dup
aload 4
invokespecial java/util/zip/GZIPInputStream/<init>(Ljava/io/InputStream;)V
astore 5
L10:
iconst_0
istore 1
L47:
iload 1
istore 2
iload 1
istore 3
L13:
aload 5
aload 10
invokevirtual java/util/zip/GZIPInputStream/read([B)I
istore 1
L14:
iload 1
iflt L48
iload 1
istore 2
iload 1
istore 3
L17:
aload 0
aload 10
iconst_0
iload 1
invokevirtual java/io/ByteArrayOutputStream/write([BII)V
L18:
goto L47
L15:
astore 7
iload 2
istore 1
aload 5
astore 8
aload 0
astore 5
aload 4
astore 6
aload 8
astore 0
aload 7
astore 4
L19:
aload 4
invokevirtual java/io/IOException/printStackTrace()V
L20:
aload 5
invokevirtual java/io/ByteArrayOutputStream/close()V
L22:
aload 0
invokevirtual java/util/zip/GZIPInputStream/close()V
L24:
aload 6
invokevirtual java/io/ByteArrayInputStream/close()V
L26:
aload 9
areturn
L2:
astore 4
aload 0
invokevirtual java/lang/String/getBytes()[B
astore 0
goto L43
L44:
iconst_0
istore 1
goto L45
L48:
iload 1
istore 2
iload 1
istore 3
L28:
aload 0
invokevirtual java/io/ByteArrayOutputStream/toByteArray()[B
astore 6
L29:
aload 0
invokevirtual java/io/ByteArrayOutputStream/close()V
L30:
aload 5
invokevirtual java/util/zip/GZIPInputStream/close()V
L32:
aload 4
invokevirtual java/io/ByteArrayInputStream/close()V
L34:
aload 6
areturn
L35:
astore 0
aload 6
areturn
L27:
astore 0
aload 9
areturn
L6:
astore 4
aconst_null
astore 0
aload 8
astore 5
L36:
aload 5
invokevirtual java/io/ByteArrayOutputStream/close()V
L37:
aload 0
invokevirtual java/util/zip/GZIPInputStream/close()V
L39:
aload 6
invokevirtual java/io/ByteArrayInputStream/close()V
L41:
aload 4
athrow
L31:
astore 0
goto L30
L33:
astore 0
goto L32
L23:
astore 4
goto L22
L25:
astore 0
goto L24
L38:
astore 5
goto L37
L40:
astore 0
goto L39
L42:
astore 0
goto L41
L9:
astore 4
aload 0
astore 5
aconst_null
astore 0
goto L36
L12:
astore 5
aload 4
astore 6
aload 0
astore 7
aconst_null
astore 0
aload 5
astore 4
aload 7
astore 5
goto L36
L16:
astore 6
aload 4
astore 7
aload 0
astore 8
aload 5
astore 0
aload 6
astore 4
aload 7
astore 6
aload 8
astore 5
goto L36
L21:
astore 4
goto L36
L5:
astore 4
aconst_null
astore 0
iconst_0
istore 1
aload 7
astore 6
goto L19
L8:
astore 4
aconst_null
astore 6
iconst_0
istore 1
aload 0
astore 5
aload 6
astore 0
aload 7
astore 6
goto L19
L11:
astore 7
aconst_null
astore 8
iconst_0
istore 1
aload 4
astore 6
aload 0
astore 5
aload 7
astore 4
aload 8
astore 0
goto L19
L46:
aload 9
areturn
.limit locals 11
.limit stack 4
.end method

.method public static decode([B)[B
.throws java/io/IOException
aload 0
iconst_0
aload 0
arraylength
iconst_0
invokestatic com/amazon/mas/kiwi/util/Base64/decode([BIII)[B
areturn
.limit locals 1
.limit stack 4
.end method

.method public static decode([BIII)[B
.throws java/io/IOException
aload 0
ifnonnull L0
new java/lang/NullPointerException
dup
ldc "Cannot decode null source array."
invokespecial java/lang/NullPointerException/<init>(Ljava/lang/String;)V
athrow
L0:
iload 1
iflt L1
iload 1
iload 2
iadd
aload 0
arraylength
if_icmple L2
L1:
new java/lang/IllegalArgumentException
dup
ldc "Source array with length %d cannot have offset of %d and process %d bytes."
iconst_3
anewarray java/lang/Object
dup
iconst_0
aload 0
arraylength
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_1
iload 1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_2
iload 2
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L2:
iload 2
ifne L3
iconst_0
newarray byte
areturn
L3:
iload 2
iconst_4
if_icmpge L4
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Base64-encoded string must have at least four characters, but length specified was "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 2
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L4:
iload 3
invokestatic com/amazon/mas/kiwi/util/Base64/getDecodabet(I)[B
astore 11
iload 2
iconst_3
imul
iconst_4
idiv
newarray byte
astore 10
iconst_4
newarray byte
astore 12
iconst_0
istore 5
iload 1
istore 6
iconst_0
istore 4
iconst_0
istore 8
L5:
iload 6
iload 1
iload 2
iadd
if_icmpge L6
aload 11
aload 0
iload 6
baload
sipush 255
iand
baload
istore 8
iload 8
bipush -5
if_icmplt L7
iload 8
iconst_m1
if_icmplt L8
iload 4
iconst_1
iadd
istore 9
aload 12
iload 4
aload 0
iload 6
baload
bastore
iload 9
istore 4
iload 5
istore 7
iload 9
iconst_3
if_icmple L9
iload 5
aload 12
iconst_0
aload 10
iload 5
iload 3
invokestatic com/amazon/mas/kiwi/util/Base64/decode4to3([BI[BII)I
iadd
istore 4
iconst_0
istore 5
aload 0
iload 6
baload
bipush 61
if_icmpne L10
L11:
iload 4
newarray byte
astore 0
aload 10
iconst_0
aload 0
iconst_0
iload 4
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
areturn
L7:
new java/io/IOException
dup
ldc "Bad Base64 input character decimal %d in array position %d"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 0
iload 6
baload
sipush 255
iand
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_1
iload 6
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokespecial java/io/IOException/<init>(Ljava/lang/String;)V
athrow
L10:
iload 4
istore 7
iload 5
istore 4
L9:
iload 6
iconst_1
iadd
istore 6
iload 7
istore 5
goto L5
L8:
iload 5
istore 7
goto L9
L6:
iload 5
istore 4
goto L11
.limit locals 13
.limit stack 8
.end method

.method private static decode4to3([BI[BII)I
aload 0
ifnonnull L0
new java/lang/NullPointerException
dup
ldc "Source array was null."
invokespecial java/lang/NullPointerException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 2
ifnonnull L1
new java/lang/NullPointerException
dup
ldc "Destination array was null."
invokespecial java/lang/NullPointerException/<init>(Ljava/lang/String;)V
athrow
L1:
iload 1
iflt L2
iload 1
iconst_3
iadd
aload 0
arraylength
if_icmplt L3
L2:
new java/lang/IllegalArgumentException
dup
ldc "Source array with length %d cannot have offset of %d and still process four bytes."
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 0
arraylength
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_1
iload 1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L3:
iload 3
iflt L4
iload 3
iconst_2
iadd
aload 2
arraylength
if_icmplt L5
L4:
new java/lang/IllegalArgumentException
dup
ldc "Destination array with length %d cannot have offset of %d and still store three bytes."
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 2
arraylength
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_1
iload 3
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L5:
iload 4
invokestatic com/amazon/mas/kiwi/util/Base64/getDecodabet(I)[B
astore 5
aload 0
iload 1
iconst_2
iadd
baload
bipush 61
if_icmpne L6
aload 2
iload 3
aload 5
aload 0
iload 1
baload
baload
sipush 255
iand
bipush 18
ishl
aload 5
aload 0
iload 1
iconst_1
iadd
baload
baload
sipush 255
iand
bipush 12
ishl
ior
bipush 16
iushr
i2b
bastore
iconst_1
ireturn
L6:
aload 0
iload 1
iconst_3
iadd
baload
bipush 61
if_icmpne L7
aload 5
aload 0
iload 1
baload
baload
sipush 255
iand
bipush 18
ishl
aload 5
aload 0
iload 1
iconst_1
iadd
baload
baload
sipush 255
iand
bipush 12
ishl
ior
aload 5
aload 0
iload 1
iconst_2
iadd
baload
baload
sipush 255
iand
bipush 6
ishl
ior
istore 1
aload 2
iload 3
iload 1
bipush 16
iushr
i2b
bastore
aload 2
iload 3
iconst_1
iadd
iload 1
bipush 8
iushr
i2b
bastore
iconst_2
ireturn
L7:
aload 5
aload 0
iload 1
baload
baload
sipush 255
iand
bipush 18
ishl
aload 5
aload 0
iload 1
iconst_1
iadd
baload
baload
sipush 255
iand
bipush 12
ishl
ior
aload 5
aload 0
iload 1
iconst_2
iadd
baload
baload
sipush 255
iand
bipush 6
ishl
ior
aload 5
aload 0
iload 1
iconst_3
iadd
baload
baload
sipush 255
iand
ior
istore 1
aload 2
iload 3
iload 1
bipush 16
ishr
i2b
bastore
aload 2
iload 3
iconst_1
iadd
iload 1
bipush 8
ishr
i2b
bastore
aload 2
iload 3
iconst_2
iadd
iload 1
i2b
bastore
iconst_3
ireturn
.limit locals 6
.limit stack 7
.end method

.method public static decodeFileToFile(Ljava/lang/String;Ljava/lang/String;)V
.throws java/io/IOException
.catch java/io/IOException from L0 to L1 using L2
.catch all from L0 to L1 using L3
.catch java/io/IOException from L1 to L4 using L5
.catch all from L1 to L4 using L6
.catch java/lang/Exception from L4 to L7 using L8
.catch all from L9 to L3 using L3
.catch java/lang/Exception from L10 to L11 using L12
aload 0
invokestatic com/amazon/mas/kiwi/util/Base64/decodeFromFile(Ljava/lang/String;)[B
astore 3
aconst_null
astore 0
aconst_null
astore 2
L0:
new java/io/BufferedOutputStream
dup
new java/io/FileOutputStream
dup
aload 1
invokespecial java/io/FileOutputStream/<init>(Ljava/lang/String;)V
invokespecial java/io/BufferedOutputStream/<init>(Ljava/io/OutputStream;)V
astore 1
L1:
aload 1
aload 3
invokevirtual java/io/OutputStream/write([B)V
L4:
aload 1
invokevirtual java/io/OutputStream/close()V
L7:
return
L2:
astore 1
aload 2
astore 0
L9:
aload 1
athrow
L3:
astore 1
L10:
aload 0
invokevirtual java/io/OutputStream/close()V
L11:
aload 1
athrow
L8:
astore 0
return
L12:
astore 0
goto L11
L6:
astore 2
aload 1
astore 0
aload 2
astore 1
goto L10
L5:
astore 0
aload 1
astore 2
aload 0
astore 1
aload 2
astore 0
goto L9
.limit locals 4
.limit stack 5
.end method

.method public static decodeFromFile(Ljava/lang/String;)[B
.throws java/io/IOException
.catch java/io/IOException from L0 to L1 using L2
.catch all from L0 to L1 using L3
.catch java/io/IOException from L4 to L5 using L2
.catch all from L4 to L5 using L3
.catch java/io/IOException from L6 to L2 using L2
.catch all from L6 to L2 using L3
.catch all from L7 to L3 using L3
.catch java/lang/Exception from L8 to L9 using L10
.catch java/io/IOException from L11 to L12 using L2
.catch all from L11 to L12 using L3
.catch java/io/IOException from L13 to L14 using L2
.catch all from L13 to L14 using L3
.catch java/io/IOException from L14 to L15 using L16
.catch all from L14 to L15 using L17
.catch java/io/IOException from L18 to L19 using L16
.catch all from L18 to L19 using L17
.catch java/lang/Exception from L19 to L20 using L21
aconst_null
astore 5
aconst_null
astore 4
aload 5
astore 3
L0:
new java/io/File
dup
aload 0
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 0
L1:
iconst_0
istore 1
aload 5
astore 3
L4:
aload 0
invokevirtual java/io/File/length()J
ldc2_w 2147483647L
lcmp
ifle L22
L5:
aload 5
astore 3
L6:
new java/io/IOException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "File is too big for this convenience method ("
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/io/File/length()J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc " bytes)."
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/IOException/<init>(Ljava/lang/String;)V
athrow
L2:
astore 0
aload 4
astore 3
L7:
aload 0
athrow
L3:
astore 0
L8:
aload 3
invokevirtual com/amazon/mas/kiwi/util/Base64$InputStream/close()V
L9:
aload 0
athrow
L22:
aload 5
astore 3
L11:
aload 0
invokevirtual java/io/File/length()J
l2i
newarray byte
astore 6
L12:
aload 5
astore 3
L13:
new com/amazon/mas/kiwi/util/Base64$InputStream
dup
new java/io/BufferedInputStream
dup
new java/io/FileInputStream
dup
aload 0
invokespecial java/io/FileInputStream/<init>(Ljava/io/File;)V
invokespecial java/io/BufferedInputStream/<init>(Ljava/io/InputStream;)V
iconst_0
invokespecial com/amazon/mas/kiwi/util/Base64$InputStream/<init>(Ljava/io/InputStream;I)V
astore 0
L14:
aload 0
aload 6
iload 1
sipush 4096
invokevirtual com/amazon/mas/kiwi/util/Base64$InputStream/read([BII)I
istore 2
L15:
iload 2
iflt L18
iload 1
iload 2
iadd
istore 1
goto L14
L18:
iload 1
newarray byte
astore 3
aload 6
iconst_0
aload 3
iconst_0
iload 1
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
L19:
aload 0
invokevirtual com/amazon/mas/kiwi/util/Base64$InputStream/close()V
L20:
aload 3
areturn
L21:
astore 0
aload 3
areturn
L10:
astore 3
goto L9
L17:
astore 4
aload 0
astore 3
aload 4
astore 0
goto L8
L16:
astore 4
aload 0
astore 3
aload 4
astore 0
goto L7
.limit locals 7
.limit stack 7
.end method

.method public static decodeToFile(Ljava/lang/String;Ljava/lang/String;)V
.throws java/io/IOException
.catch java/io/IOException from L0 to L1 using L2
.catch all from L0 to L1 using L3
.catch java/io/IOException from L1 to L4 using L5
.catch all from L1 to L4 using L6
.catch java/lang/Exception from L4 to L7 using L8
.catch all from L9 to L3 using L3
.catch java/lang/Exception from L10 to L11 using L12
aconst_null
astore 2
aconst_null
astore 3
L0:
new com/amazon/mas/kiwi/util/Base64$OutputStream
dup
new java/io/FileOutputStream
dup
aload 1
invokespecial java/io/FileOutputStream/<init>(Ljava/lang/String;)V
iconst_0
invokespecial com/amazon/mas/kiwi/util/Base64$OutputStream/<init>(Ljava/io/OutputStream;I)V
astore 1
L1:
aload 1
aload 0
ldc "US-ASCII"
invokevirtual java/lang/String/getBytes(Ljava/lang/String;)[B
invokevirtual com/amazon/mas/kiwi/util/Base64$OutputStream/write([B)V
L4:
aload 1
invokevirtual com/amazon/mas/kiwi/util/Base64$OutputStream/close()V
L7:
return
L2:
astore 0
aload 3
astore 2
L9:
aload 0
athrow
L3:
astore 0
L10:
aload 2
invokevirtual com/amazon/mas/kiwi/util/Base64$OutputStream/close()V
L11:
aload 0
athrow
L8:
astore 0
return
L12:
astore 1
goto L11
L6:
astore 0
aload 1
astore 2
goto L10
L5:
astore 0
aload 1
astore 2
goto L9
.limit locals 4
.limit stack 5
.end method

.method public static encode(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)V
iconst_3
newarray byte
astore 3
iconst_4
newarray byte
astore 4
L0:
aload 0
invokevirtual java/nio/ByteBuffer/hasRemaining()Z
ifeq L1
iconst_3
aload 0
invokevirtual java/nio/ByteBuffer/remaining()I
invokestatic java/lang/Math/min(II)I
istore 2
aload 0
aload 3
iconst_0
iload 2
invokevirtual java/nio/ByteBuffer/get([BII)Ljava/nio/ByteBuffer;
pop
aload 4
aload 3
iload 2
iconst_0
invokestatic com/amazon/mas/kiwi/util/Base64/encode3to4([B[BII)[B
pop
aload 1
aload 4
invokevirtual java/nio/ByteBuffer/put([B)Ljava/nio/ByteBuffer;
pop
goto L0
L1:
return
.limit locals 5
.limit stack 4
.end method

.method public static encode(Ljava/nio/ByteBuffer;Ljava/nio/CharBuffer;)V
iconst_3
newarray byte
astore 3
iconst_4
newarray byte
astore 4
L0:
aload 0
invokevirtual java/nio/ByteBuffer/hasRemaining()Z
ifeq L1
iconst_3
aload 0
invokevirtual java/nio/ByteBuffer/remaining()I
invokestatic java/lang/Math/min(II)I
istore 2
aload 0
aload 3
iconst_0
iload 2
invokevirtual java/nio/ByteBuffer/get([BII)Ljava/nio/ByteBuffer;
pop
aload 4
aload 3
iload 2
iconst_0
invokestatic com/amazon/mas/kiwi/util/Base64/encode3to4([B[BII)[B
pop
iconst_0
istore 2
L2:
iload 2
iconst_4
if_icmpge L0
aload 1
aload 4
iload 2
baload
sipush 255
iand
i2c
invokevirtual java/nio/CharBuffer/put(C)Ljava/nio/CharBuffer;
pop
iload 2
iconst_1
iadd
istore 2
goto L2
L1:
return
.limit locals 5
.limit stack 4
.end method

.method private static encode3to4([BII[BII)[B
iload 5
invokestatic com/amazon/mas/kiwi/util/Base64/getAlphabet(I)[B
astore 7
iload 2
ifle L0
aload 0
iload 1
baload
bipush 24
ishl
bipush 8
iushr
istore 5
L1:
iload 2
iconst_1
if_icmple L2
aload 0
iload 1
iconst_1
iadd
baload
bipush 24
ishl
bipush 16
iushr
istore 6
L3:
iload 2
iconst_2
if_icmple L4
aload 0
iload 1
iconst_2
iadd
baload
bipush 24
ishl
bipush 24
iushr
istore 1
L5:
iload 5
iload 6
ior
iload 1
ior
istore 1
iload 2
tableswitch 1
L6
L7
L8
default : L9
L9:
aload 3
areturn
L0:
iconst_0
istore 5
goto L1
L2:
iconst_0
istore 6
goto L3
L4:
iconst_0
istore 1
goto L5
L8:
aload 3
iload 4
aload 7
iload 1
bipush 18
iushr
baload
bastore
aload 3
iload 4
iconst_1
iadd
aload 7
iload 1
bipush 12
iushr
bipush 63
iand
baload
bastore
aload 3
iload 4
iconst_2
iadd
aload 7
iload 1
bipush 6
iushr
bipush 63
iand
baload
bastore
aload 3
iload 4
iconst_3
iadd
aload 7
iload 1
bipush 63
iand
baload
bastore
aload 3
areturn
L7:
aload 3
iload 4
aload 7
iload 1
bipush 18
iushr
baload
bastore
aload 3
iload 4
iconst_1
iadd
aload 7
iload 1
bipush 12
iushr
bipush 63
iand
baload
bastore
aload 3
iload 4
iconst_2
iadd
aload 7
iload 1
bipush 6
iushr
bipush 63
iand
baload
bastore
aload 3
iload 4
iconst_3
iadd
bipush 61
bastore
aload 3
areturn
L6:
aload 3
iload 4
aload 7
iload 1
bipush 18
iushr
baload
bastore
aload 3
iload 4
iconst_1
iadd
aload 7
iload 1
bipush 12
iushr
bipush 63
iand
baload
bastore
aload 3
iload 4
iconst_2
iadd
bipush 61
bastore
aload 3
iload 4
iconst_3
iadd
bipush 61
bastore
aload 3
areturn
.limit locals 8
.limit stack 5
.end method

.method private static encode3to4([B[BII)[B
aload 1
iconst_0
iload 2
aload 0
iconst_0
iload 3
invokestatic com/amazon/mas/kiwi/util/Base64/encode3to4([BII[BII)[B
pop
aload 0
areturn
.limit locals 4
.limit stack 6
.end method

.method public static encodeBytes([B)Ljava/lang/String;
.catch java/io/IOException from L0 to L1 using L2
aconst_null
astore 1
L0:
aload 0
iconst_0
aload 0
arraylength
iconst_0
invokestatic com/amazon/mas/kiwi/util/Base64/encodeBytes([BIII)Ljava/lang/String;
astore 0
L1:
getstatic com/amazon/mas/kiwi/util/Base64/$assertionsDisabled Z
ifne L3
aload 0
ifnonnull L3
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L2:
astore 2
aload 1
astore 0
getstatic com/amazon/mas/kiwi/util/Base64/$assertionsDisabled Z
ifne L1
new java/lang/AssertionError
dup
aload 2
invokevirtual java/io/IOException/getMessage()Ljava/lang/String;
invokespecial java/lang/AssertionError/<init>(Ljava/lang/Object;)V
athrow
L3:
aload 0
areturn
.limit locals 3
.limit stack 4
.end method

.method public static encodeBytes([BI)Ljava/lang/String;
.throws java/io/IOException
aload 0
iconst_0
aload 0
arraylength
iload 1
invokestatic com/amazon/mas/kiwi/util/Base64/encodeBytes([BIII)Ljava/lang/String;
areturn
.limit locals 2
.limit stack 4
.end method

.method public static encodeBytes([BII)Ljava/lang/String;
.catch java/io/IOException from L0 to L1 using L2
aconst_null
astore 3
L0:
aload 0
iload 1
iload 2
iconst_0
invokestatic com/amazon/mas/kiwi/util/Base64/encodeBytes([BIII)Ljava/lang/String;
astore 0
L1:
getstatic com/amazon/mas/kiwi/util/Base64/$assertionsDisabled Z
ifne L3
aload 0
ifnonnull L3
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L2:
astore 4
aload 3
astore 0
getstatic com/amazon/mas/kiwi/util/Base64/$assertionsDisabled Z
ifne L1
new java/lang/AssertionError
dup
aload 4
invokevirtual java/io/IOException/getMessage()Ljava/lang/String;
invokespecial java/lang/AssertionError/<init>(Ljava/lang/Object;)V
athrow
L3:
aload 0
areturn
.limit locals 5
.limit stack 4
.end method

.method public static encodeBytes([BIII)Ljava/lang/String;
.throws java/io/IOException
.catch java/io/UnsupportedEncodingException from L0 to L1 using L2
aload 0
iload 1
iload 2
iload 3
invokestatic com/amazon/mas/kiwi/util/Base64/encodeBytesToBytes([BIII)[B
astore 0
L0:
new java/lang/String
dup
aload 0
ldc "US-ASCII"
invokespecial java/lang/String/<init>([BLjava/lang/String;)V
astore 4
L1:
aload 4
areturn
L2:
astore 4
new java/lang/String
dup
aload 0
invokespecial java/lang/String/<init>([B)V
areturn
.limit locals 5
.limit stack 4
.end method

.method public static encodeBytesToBytes([B)[B
.catch java/io/IOException from L0 to L1 using L2
aconst_null
astore 1
L0:
aload 0
iconst_0
aload 0
arraylength
iconst_0
invokestatic com/amazon/mas/kiwi/util/Base64/encodeBytesToBytes([BIII)[B
astore 0
L1:
aload 0
areturn
L2:
astore 2
aload 1
astore 0
getstatic com/amazon/mas/kiwi/util/Base64/$assertionsDisabled Z
ifne L1
new java/lang/AssertionError
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "IOExceptions only come from GZipping, which is turned off: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/io/IOException/getMessage()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/AssertionError/<init>(Ljava/lang/Object;)V
athrow
.limit locals 3
.limit stack 4
.end method

.method public static encodeBytesToBytes([BIII)[B
.throws java/io/IOException
.catch java/io/IOException from L0 to L1 using L2
.catch all from L0 to L1 using L3
.catch java/io/IOException from L1 to L4 using L5
.catch all from L1 to L4 using L6
.catch java/io/IOException from L4 to L7 using L8
.catch all from L4 to L7 using L9
.catch java/io/IOException from L7 to L10 using L11
.catch all from L7 to L10 using L12
.catch java/lang/Exception from L10 to L13 using L14
.catch java/lang/Exception from L13 to L15 using L16
.catch java/lang/Exception from L15 to L17 using L18
.catch all from L19 to L20 using L20
.catch java/lang/Exception from L21 to L22 using L23
.catch java/lang/Exception from L22 to L24 using L25
.catch java/lang/Exception from L24 to L26 using L27
aload 0
ifnonnull L28
new java/lang/NullPointerException
dup
ldc "Cannot serialize a null array."
invokespecial java/lang/NullPointerException/<init>(Ljava/lang/String;)V
athrow
L28:
iload 1
ifge L29
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Cannot have negative offset: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L29:
iload 2
ifge L30
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Cannot have length offset: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 2
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L30:
iload 1
iload 2
iadd
aload 0
arraylength
if_icmple L31
new java/lang/IllegalArgumentException
dup
ldc "Cannot have offset of %d and length of %d with array of length %d"
iconst_3
anewarray java/lang/Object
dup
iconst_0
iload 1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_1
iload 2
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_2
aload 0
arraylength
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L31:
iload 3
iconst_2
iand
ifeq L32
L0:
new java/io/ByteArrayOutputStream
dup
invokespecial java/io/ByteArrayOutputStream/<init>()V
astore 8
L1:
new com/amazon/mas/kiwi/util/Base64$OutputStream
dup
aload 8
iload 3
iconst_1
ior
invokespecial com/amazon/mas/kiwi/util/Base64$OutputStream/<init>(Ljava/io/OutputStream;I)V
astore 9
L4:
new java/util/zip/GZIPOutputStream
dup
aload 9
invokespecial java/util/zip/GZIPOutputStream/<init>(Ljava/io/OutputStream;)V
astore 10
L7:
aload 10
aload 0
iload 1
iload 2
invokevirtual java/util/zip/GZIPOutputStream/write([BII)V
aload 10
invokevirtual java/util/zip/GZIPOutputStream/close()V
L10:
aload 10
invokevirtual java/util/zip/GZIPOutputStream/close()V
L13:
aload 9
invokevirtual com/amazon/mas/kiwi/util/Base64$OutputStream/close()V
L15:
aload 8
invokevirtual java/io/ByteArrayOutputStream/close()V
L17:
aload 8
invokevirtual java/io/ByteArrayOutputStream/toByteArray()[B
areturn
L2:
astore 10
aconst_null
astore 9
aconst_null
astore 8
aconst_null
astore 0
L19:
aload 10
athrow
L20:
astore 10
L21:
aload 9
invokevirtual java/util/zip/GZIPOutputStream/close()V
L22:
aload 0
invokevirtual com/amazon/mas/kiwi/util/Base64$OutputStream/close()V
L24:
aload 8
invokevirtual java/io/ByteArrayOutputStream/close()V
L26:
aload 10
athrow
L32:
iload 3
bipush 8
iand
ifeq L33
iconst_1
istore 6
L34:
iload 2
iconst_3
idiv
istore 5
iload 2
iconst_3
irem
ifle L35
iconst_4
istore 4
L36:
iload 5
iconst_4
imul
iload 4
iadd
istore 5
iload 5
istore 4
iload 6
ifeq L37
iload 5
iload 5
bipush 76
idiv
iadd
istore 4
L37:
iload 4
newarray byte
astore 8
iconst_0
istore 4
iconst_0
istore 5
iconst_0
istore 7
L38:
iload 7
iload 2
iconst_2
isub
if_icmpge L39
aload 0
iload 7
iload 1
iadd
iconst_3
aload 8
iload 4
iload 3
invokestatic com/amazon/mas/kiwi/util/Base64/encode3to4([BII[BII)[B
pop
iload 5
iconst_4
iadd
istore 5
iload 6
ifeq L40
iload 5
bipush 76
if_icmplt L40
aload 8
iload 4
iconst_4
iadd
bipush 10
bastore
iload 4
iconst_1
iadd
istore 4
iconst_0
istore 5
L41:
iload 4
iconst_4
iadd
istore 4
iload 7
iconst_3
iadd
istore 7
goto L38
L33:
iconst_0
istore 6
goto L34
L35:
iconst_0
istore 4
goto L36
L39:
iload 4
istore 5
iload 7
iload 2
if_icmpge L42
aload 0
iload 7
iload 1
iadd
iload 2
iload 7
isub
aload 8
iload 4
iload 3
invokestatic com/amazon/mas/kiwi/util/Base64/encode3to4([BII[BII)[B
pop
iload 4
iconst_4
iadd
istore 5
L42:
iload 5
aload 8
arraylength
iconst_1
isub
if_icmpgt L43
iload 5
newarray byte
astore 0
aload 8
iconst_0
aload 0
iconst_0
iload 5
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
areturn
L43:
aload 8
areturn
L14:
astore 0
goto L13
L16:
astore 0
goto L15
L18:
astore 0
goto L17
L23:
astore 9
goto L22
L25:
astore 0
goto L24
L27:
astore 0
goto L26
L3:
astore 10
aconst_null
astore 9
aconst_null
astore 8
aconst_null
astore 0
goto L21
L6:
astore 10
aconst_null
astore 9
aconst_null
astore 0
goto L21
L9:
astore 10
aconst_null
astore 11
aload 9
astore 0
aload 11
astore 9
goto L21
L12:
astore 11
aload 9
astore 0
aload 10
astore 9
aload 11
astore 10
goto L21
L5:
astore 10
aconst_null
astore 9
aconst_null
astore 0
goto L19
L8:
astore 10
aconst_null
astore 11
aload 9
astore 0
aload 11
astore 9
goto L19
L11:
astore 0
aload 10
astore 11
aload 0
astore 10
aload 9
astore 0
aload 11
astore 9
goto L19
L40:
goto L41
.limit locals 12
.limit stack 7
.end method

.method public static encodeFileToFile(Ljava/lang/String;Ljava/lang/String;)V
.throws java/io/IOException
.catch java/io/IOException from L0 to L1 using L2
.catch all from L0 to L1 using L3
.catch java/io/IOException from L1 to L4 using L5
.catch all from L1 to L4 using L6
.catch java/lang/Exception from L4 to L7 using L8
.catch all from L9 to L3 using L3
.catch java/lang/Exception from L10 to L11 using L12
aload 0
invokestatic com/amazon/mas/kiwi/util/Base64/encodeFromFile(Ljava/lang/String;)Ljava/lang/String;
astore 3
aconst_null
astore 0
aconst_null
astore 2
L0:
new java/io/BufferedOutputStream
dup
new java/io/FileOutputStream
dup
aload 1
invokespecial java/io/FileOutputStream/<init>(Ljava/lang/String;)V
invokespecial java/io/BufferedOutputStream/<init>(Ljava/io/OutputStream;)V
astore 1
L1:
aload 1
aload 3
ldc "US-ASCII"
invokevirtual java/lang/String/getBytes(Ljava/lang/String;)[B
invokevirtual java/io/OutputStream/write([B)V
L4:
aload 1
invokevirtual java/io/OutputStream/close()V
L7:
return
L2:
astore 1
aload 2
astore 0
L9:
aload 1
athrow
L3:
astore 1
L10:
aload 0
invokevirtual java/io/OutputStream/close()V
L11:
aload 1
athrow
L8:
astore 0
return
L12:
astore 0
goto L11
L6:
astore 2
aload 1
astore 0
aload 2
astore 1
goto L10
L5:
astore 0
aload 1
astore 2
aload 0
astore 1
aload 2
astore 0
goto L9
.limit locals 4
.limit stack 5
.end method

.method public static encodeFromFile(Ljava/lang/String;)Ljava/lang/String;
.throws java/io/IOException
.catch java/io/IOException from L0 to L1 using L2
.catch all from L0 to L1 using L3
.catch java/io/IOException from L4 to L5 using L2
.catch all from L4 to L5 using L3
.catch java/io/IOException from L6 to L7 using L2
.catch all from L6 to L7 using L3
.catch java/io/IOException from L7 to L8 using L9
.catch all from L7 to L8 using L10
.catch java/io/IOException from L11 to L12 using L9
.catch all from L11 to L12 using L10
.catch java/lang/Exception from L12 to L13 using L14
.catch all from L15 to L3 using L3
.catch java/lang/Exception from L16 to L17 using L18
aconst_null
astore 5
aconst_null
astore 4
aload 5
astore 3
L0:
new java/io/File
dup
aload 0
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 0
L1:
aload 5
astore 3
L4:
aload 0
invokevirtual java/io/File/length()J
l2d
ldc2_w 1.4D
dmul
dconst_1
dadd
d2i
bipush 40
invokestatic java/lang/Math/max(II)I
newarray byte
astore 6
L5:
iconst_0
istore 1
aload 5
astore 3
L6:
new com/amazon/mas/kiwi/util/Base64$InputStream
dup
new java/io/BufferedInputStream
dup
new java/io/FileInputStream
dup
aload 0
invokespecial java/io/FileInputStream/<init>(Ljava/io/File;)V
invokespecial java/io/BufferedInputStream/<init>(Ljava/io/InputStream;)V
iconst_1
invokespecial com/amazon/mas/kiwi/util/Base64$InputStream/<init>(Ljava/io/InputStream;I)V
astore 0
L7:
aload 0
aload 6
iload 1
sipush 4096
invokevirtual com/amazon/mas/kiwi/util/Base64$InputStream/read([BII)I
istore 2
L8:
iload 2
iflt L11
iload 1
iload 2
iadd
istore 1
goto L7
L11:
new java/lang/String
dup
aload 6
iconst_0
iload 1
ldc "US-ASCII"
invokespecial java/lang/String/<init>([BIILjava/lang/String;)V
astore 3
L12:
aload 0
invokevirtual com/amazon/mas/kiwi/util/Base64$InputStream/close()V
L13:
aload 3
areturn
L2:
astore 0
aload 4
astore 3
L15:
aload 0
athrow
L3:
astore 0
L16:
aload 3
invokevirtual com/amazon/mas/kiwi/util/Base64$InputStream/close()V
L17:
aload 0
athrow
L14:
astore 0
aload 3
areturn
L18:
astore 3
goto L17
L10:
astore 4
aload 0
astore 3
aload 4
astore 0
goto L16
L9:
astore 4
aload 0
astore 3
aload 4
astore 0
goto L15
.limit locals 7
.limit stack 7
.end method

.method public static encodeObject(Ljava/io/Serializable;)Ljava/lang/String;
.throws java/io/IOException
aload 0
iconst_0
invokestatic com/amazon/mas/kiwi/util/Base64/encodeObject(Ljava/io/Serializable;I)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method

.method public static encodeObject(Ljava/io/Serializable;I)Ljava/lang/String;
.throws java/io/IOException
.catch java/io/IOException from L0 to L1 using L2
.catch all from L0 to L1 using L3
.catch java/io/IOException from L1 to L4 using L5
.catch all from L1 to L4 using L6
.catch java/io/IOException from L7 to L8 using L9
.catch all from L7 to L8 using L10
.catch java/io/IOException from L8 to L11 using L12
.catch all from L8 to L11 using L13
.catch java/io/IOException from L14 to L15 using L9
.catch all from L14 to L15 using L10
.catch java/lang/Exception from L15 to L16 using L17
.catch java/lang/Exception from L16 to L18 using L19
.catch java/lang/Exception from L18 to L20 using L21
.catch java/lang/Exception from L20 to L22 using L23
.catch java/io/UnsupportedEncodingException from L22 to L24 using L25
.catch java/io/IOException from L26 to L27 using L9
.catch all from L26 to L27 using L10
.catch all from L28 to L3 using L3
.catch java/lang/Exception from L29 to L30 using L31
.catch java/lang/Exception from L30 to L32 using L33
.catch java/lang/Exception from L32 to L34 using L35
.catch java/lang/Exception from L34 to L36 using L37
aload 0
ifnonnull L38
new java/lang/NullPointerException
dup
ldc "Cannot serialize a null object."
invokespecial java/lang/NullPointerException/<init>(Ljava/lang/String;)V
athrow
L38:
aconst_null
astore 6
aconst_null
astore 13
aconst_null
astore 5
aconst_null
astore 16
aconst_null
astore 9
aconst_null
astore 4
aconst_null
astore 7
aconst_null
astore 14
aconst_null
astore 19
aconst_null
astore 17
aconst_null
astore 12
aconst_null
astore 8
aconst_null
astore 11
aconst_null
astore 18
aconst_null
astore 15
aconst_null
astore 10
L0:
new java/io/ByteArrayOutputStream
dup
invokespecial java/io/ByteArrayOutputStream/<init>()V
astore 2
L1:
new com/amazon/mas/kiwi/util/Base64$OutputStream
dup
aload 2
iload 1
iconst_1
ior
invokespecial com/amazon/mas/kiwi/util/Base64$OutputStream/<init>(Ljava/io/OutputStream;I)V
astore 3
L4:
iload 1
iconst_2
iand
ifeq L39
aload 19
astore 6
aload 18
astore 7
aload 17
astore 8
aload 15
astore 9
L7:
new java/util/zip/GZIPOutputStream
dup
aload 3
invokespecial java/util/zip/GZIPOutputStream/<init>(Ljava/io/OutputStream;)V
astore 4
L8:
new java/io/ObjectOutputStream
dup
aload 4
invokespecial java/io/ObjectOutputStream/<init>(Ljava/io/OutputStream;)V
astore 5
L11:
aload 4
astore 6
aload 5
astore 7
aload 4
astore 8
aload 5
astore 9
L14:
aload 5
aload 0
invokevirtual java/io/ObjectOutputStream/writeObject(Ljava/lang/Object;)V
L15:
aload 5
invokevirtual java/io/ObjectOutputStream/close()V
L16:
aload 4
invokevirtual java/util/zip/GZIPOutputStream/close()V
L18:
aload 3
invokevirtual java/io/OutputStream/close()V
L20:
aload 2
invokevirtual java/io/ByteArrayOutputStream/close()V
L22:
new java/lang/String
dup
aload 2
invokevirtual java/io/ByteArrayOutputStream/toByteArray()[B
ldc "US-ASCII"
invokespecial java/lang/String/<init>([BLjava/lang/String;)V
astore 0
L24:
aload 0
areturn
L39:
aload 19
astore 6
aload 18
astore 7
aload 17
astore 8
aload 15
astore 9
L26:
new java/io/ObjectOutputStream
dup
aload 3
invokespecial java/io/ObjectOutputStream/<init>(Ljava/io/OutputStream;)V
astore 5
L27:
aload 12
astore 4
goto L11
L2:
astore 3
aload 10
astore 8
aload 13
astore 0
aload 9
astore 2
L40:
aload 2
astore 5
aload 0
astore 6
aload 4
astore 7
L28:
aload 3
athrow
L3:
astore 4
aload 7
astore 3
aload 6
astore 2
aload 5
astore 0
L29:
aload 8
invokevirtual java/io/ObjectOutputStream/close()V
L30:
aload 3
invokevirtual java/util/zip/GZIPOutputStream/close()V
L32:
aload 0
invokevirtual java/io/OutputStream/close()V
L34:
aload 2
invokevirtual java/io/ByteArrayOutputStream/close()V
L36:
aload 4
athrow
L25:
astore 0
new java/lang/String
dup
aload 2
invokevirtual java/io/ByteArrayOutputStream/toByteArray()[B
invokespecial java/lang/String/<init>([B)V
areturn
L17:
astore 0
goto L16
L19:
astore 0
goto L18
L21:
astore 0
goto L20
L23:
astore 0
goto L22
L31:
astore 5
goto L30
L33:
astore 3
goto L32
L35:
astore 0
goto L34
L37:
astore 0
goto L36
L6:
astore 4
aload 16
astore 0
aload 14
astore 3
aload 11
astore 8
goto L29
L10:
astore 4
aload 3
astore 0
aload 6
astore 3
aload 7
astore 8
goto L29
L13:
astore 5
aload 3
astore 0
aload 4
astore 3
aload 11
astore 8
aload 5
astore 4
goto L29
L5:
astore 3
aload 2
astore 0
aload 9
astore 2
aload 10
astore 8
goto L40
L9:
astore 4
aload 2
astore 0
aload 3
astore 2
aload 4
astore 3
aload 8
astore 4
aload 9
astore 8
goto L40
L12:
astore 5
aload 2
astore 0
aload 3
astore 2
aload 5
astore 3
aload 10
astore 8
goto L40
.limit locals 20
.limit stack 5
.end method

.method public static encodeToFile([BLjava/lang/String;)V
.throws java/io/IOException
.catch java/io/IOException from L0 to L1 using L2
.catch all from L0 to L1 using L3
.catch java/io/IOException from L1 to L4 using L5
.catch all from L1 to L4 using L6
.catch java/lang/Exception from L4 to L7 using L8
.catch all from L9 to L3 using L3
.catch java/lang/Exception from L10 to L11 using L12
aload 0
ifnonnull L13
new java/lang/NullPointerException
dup
ldc "Data to encode was null."
invokespecial java/lang/NullPointerException/<init>(Ljava/lang/String;)V
athrow
L13:
aconst_null
astore 2
aconst_null
astore 3
L0:
new com/amazon/mas/kiwi/util/Base64$OutputStream
dup
new java/io/FileOutputStream
dup
aload 1
invokespecial java/io/FileOutputStream/<init>(Ljava/lang/String;)V
iconst_1
invokespecial com/amazon/mas/kiwi/util/Base64$OutputStream/<init>(Ljava/io/OutputStream;I)V
astore 1
L1:
aload 1
aload 0
invokevirtual com/amazon/mas/kiwi/util/Base64$OutputStream/write([B)V
L4:
aload 1
invokevirtual com/amazon/mas/kiwi/util/Base64$OutputStream/close()V
L7:
return
L2:
astore 0
aload 3
astore 2
L9:
aload 0
athrow
L3:
astore 0
L10:
aload 2
invokevirtual com/amazon/mas/kiwi/util/Base64$OutputStream/close()V
L11:
aload 0
athrow
L8:
astore 0
return
L12:
astore 1
goto L11
L6:
astore 0
aload 1
astore 2
goto L10
L5:
astore 0
aload 1
astore 2
goto L9
.limit locals 4
.limit stack 5
.end method

.method private static final getAlphabet(I)[B
iload 0
bipush 16
iand
bipush 16
if_icmpne L0
getstatic com/amazon/mas/kiwi/util/Base64/_URL_SAFE_ALPHABET [B
areturn
L0:
iload 0
bipush 32
iand
bipush 32
if_icmpne L1
getstatic com/amazon/mas/kiwi/util/Base64/_ORDERED_ALPHABET [B
areturn
L1:
getstatic com/amazon/mas/kiwi/util/Base64/_STANDARD_ALPHABET [B
areturn
.limit locals 1
.limit stack 2
.end method

.method private static final getDecodabet(I)[B
iload 0
bipush 16
iand
bipush 16
if_icmpne L0
getstatic com/amazon/mas/kiwi/util/Base64/_URL_SAFE_DECODABET [B
areturn
L0:
iload 0
bipush 32
iand
bipush 32
if_icmpne L1
getstatic com/amazon/mas/kiwi/util/Base64/_ORDERED_DECODABET [B
areturn
L1:
getstatic com/amazon/mas/kiwi/util/Base64/_STANDARD_DECODABET [B
areturn
.limit locals 1
.limit stack 2
.end method
