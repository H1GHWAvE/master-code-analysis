.bytecode 50.0
.class public synchronized com/amazon/mas/kiwi/util/Base64$OutputStream
.super java/io/FilterOutputStream
.inner class public static OutputStream inner com/amazon/mas/kiwi/util/Base64$OutputStream outer com/amazon/mas/kiwi/util/Base64

.field private 'b4' [B

.field private 'breakLines' Z

.field private 'buffer' [B

.field private 'bufferLength' I

.field private 'decodabet' [B

.field private 'encode' Z

.field private 'lineLength' I

.field private 'options' I

.field private 'position' I

.field private 'suspendEncoding' Z

.method public <init>(Ljava/io/OutputStream;)V
aload 0
aload 1
iconst_1
invokespecial com/amazon/mas/kiwi/util/Base64$OutputStream/<init>(Ljava/io/OutputStream;I)V
return
.limit locals 2
.limit stack 3
.end method

.method public <init>(Ljava/io/OutputStream;I)V
aload 0
aload 1
invokespecial java/io/FilterOutputStream/<init>(Ljava/io/OutputStream;)V
iload 2
bipush 8
iand
ifeq L0
iconst_1
istore 4
L1:
aload 0
iload 4
putfield com/amazon/mas/kiwi/util/Base64$OutputStream/breakLines Z
iload 2
iconst_1
iand
ifeq L2
iconst_1
istore 4
L3:
aload 0
iload 4
putfield com/amazon/mas/kiwi/util/Base64$OutputStream/encode Z
aload 0
getfield com/amazon/mas/kiwi/util/Base64$OutputStream/encode Z
ifeq L4
iconst_3
istore 3
L5:
aload 0
iload 3
putfield com/amazon/mas/kiwi/util/Base64$OutputStream/bufferLength I
aload 0
aload 0
getfield com/amazon/mas/kiwi/util/Base64$OutputStream/bufferLength I
newarray byte
putfield com/amazon/mas/kiwi/util/Base64$OutputStream/buffer [B
aload 0
iconst_0
putfield com/amazon/mas/kiwi/util/Base64$OutputStream/position I
aload 0
iconst_0
putfield com/amazon/mas/kiwi/util/Base64$OutputStream/lineLength I
aload 0
iconst_0
putfield com/amazon/mas/kiwi/util/Base64$OutputStream/suspendEncoding Z
aload 0
iconst_4
newarray byte
putfield com/amazon/mas/kiwi/util/Base64$OutputStream/b4 [B
aload 0
iload 2
putfield com/amazon/mas/kiwi/util/Base64$OutputStream/options I
aload 0
iload 2
invokestatic com/amazon/mas/kiwi/util/Base64/access$000(I)[B
putfield com/amazon/mas/kiwi/util/Base64$OutputStream/decodabet [B
return
L0:
iconst_0
istore 4
goto L1
L2:
iconst_0
istore 4
goto L3
L4:
iconst_4
istore 3
goto L5
.limit locals 5
.limit stack 2
.end method

.method public close()V
.throws java/io/IOException
aload 0
invokevirtual com/amazon/mas/kiwi/util/Base64$OutputStream/flushBase64()V
aload 0
invokespecial java/io/FilterOutputStream/close()V
aload 0
aconst_null
putfield com/amazon/mas/kiwi/util/Base64$OutputStream/buffer [B
aload 0
aconst_null
putfield com/amazon/mas/kiwi/util/Base64$OutputStream/out Ljava/io/OutputStream;
return
.limit locals 1
.limit stack 2
.end method

.method public flushBase64()V
.throws java/io/IOException
aload 0
getfield com/amazon/mas/kiwi/util/Base64$OutputStream/position I
ifle L0
aload 0
getfield com/amazon/mas/kiwi/util/Base64$OutputStream/encode Z
ifeq L1
aload 0
getfield com/amazon/mas/kiwi/util/Base64$OutputStream/out Ljava/io/OutputStream;
aload 0
getfield com/amazon/mas/kiwi/util/Base64$OutputStream/b4 [B
aload 0
getfield com/amazon/mas/kiwi/util/Base64$OutputStream/buffer [B
aload 0
getfield com/amazon/mas/kiwi/util/Base64$OutputStream/position I
aload 0
getfield com/amazon/mas/kiwi/util/Base64$OutputStream/options I
invokestatic com/amazon/mas/kiwi/util/Base64/access$300([B[BII)[B
invokevirtual java/io/OutputStream/write([B)V
aload 0
iconst_0
putfield com/amazon/mas/kiwi/util/Base64$OutputStream/position I
L0:
return
L1:
new java/io/IOException
dup
ldc "Base64 input not properly padded."
invokespecial java/io/IOException/<init>(Ljava/lang/String;)V
athrow
.limit locals 1
.limit stack 5
.end method

.method public resumeEncoding()V
aload 0
iconst_0
putfield com/amazon/mas/kiwi/util/Base64$OutputStream/suspendEncoding Z
return
.limit locals 1
.limit stack 2
.end method

.method public suspendEncoding()V
.throws java/io/IOException
aload 0
invokevirtual com/amazon/mas/kiwi/util/Base64$OutputStream/flushBase64()V
aload 0
iconst_1
putfield com/amazon/mas/kiwi/util/Base64$OutputStream/suspendEncoding Z
return
.limit locals 1
.limit stack 2
.end method

.method public write(I)V
.throws java/io/IOException
aload 0
getfield com/amazon/mas/kiwi/util/Base64$OutputStream/suspendEncoding Z
ifeq L0
aload 0
getfield com/amazon/mas/kiwi/util/Base64$OutputStream/out Ljava/io/OutputStream;
iload 1
invokevirtual java/io/OutputStream/write(I)V
L1:
return
L0:
aload 0
getfield com/amazon/mas/kiwi/util/Base64$OutputStream/encode Z
ifeq L2
aload 0
getfield com/amazon/mas/kiwi/util/Base64$OutputStream/buffer [B
astore 3
aload 0
getfield com/amazon/mas/kiwi/util/Base64$OutputStream/position I
istore 2
aload 0
iload 2
iconst_1
iadd
putfield com/amazon/mas/kiwi/util/Base64$OutputStream/position I
aload 3
iload 2
iload 1
i2b
bastore
aload 0
getfield com/amazon/mas/kiwi/util/Base64$OutputStream/position I
aload 0
getfield com/amazon/mas/kiwi/util/Base64$OutputStream/bufferLength I
if_icmplt L1
aload 0
getfield com/amazon/mas/kiwi/util/Base64$OutputStream/out Ljava/io/OutputStream;
aload 0
getfield com/amazon/mas/kiwi/util/Base64$OutputStream/b4 [B
aload 0
getfield com/amazon/mas/kiwi/util/Base64$OutputStream/buffer [B
aload 0
getfield com/amazon/mas/kiwi/util/Base64$OutputStream/bufferLength I
aload 0
getfield com/amazon/mas/kiwi/util/Base64$OutputStream/options I
invokestatic com/amazon/mas/kiwi/util/Base64/access$300([B[BII)[B
invokevirtual java/io/OutputStream/write([B)V
aload 0
aload 0
getfield com/amazon/mas/kiwi/util/Base64$OutputStream/lineLength I
iconst_4
iadd
putfield com/amazon/mas/kiwi/util/Base64$OutputStream/lineLength I
aload 0
getfield com/amazon/mas/kiwi/util/Base64$OutputStream/breakLines Z
ifeq L3
aload 0
getfield com/amazon/mas/kiwi/util/Base64$OutputStream/lineLength I
bipush 76
if_icmplt L3
aload 0
getfield com/amazon/mas/kiwi/util/Base64$OutputStream/out Ljava/io/OutputStream;
bipush 10
invokevirtual java/io/OutputStream/write(I)V
aload 0
iconst_0
putfield com/amazon/mas/kiwi/util/Base64$OutputStream/lineLength I
L3:
aload 0
iconst_0
putfield com/amazon/mas/kiwi/util/Base64$OutputStream/position I
return
L2:
aload 0
getfield com/amazon/mas/kiwi/util/Base64$OutputStream/decodabet [B
iload 1
bipush 127
iand
baload
bipush -5
if_icmple L4
aload 0
getfield com/amazon/mas/kiwi/util/Base64$OutputStream/buffer [B
astore 3
aload 0
getfield com/amazon/mas/kiwi/util/Base64$OutputStream/position I
istore 2
aload 0
iload 2
iconst_1
iadd
putfield com/amazon/mas/kiwi/util/Base64$OutputStream/position I
aload 3
iload 2
iload 1
i2b
bastore
aload 0
getfield com/amazon/mas/kiwi/util/Base64$OutputStream/position I
aload 0
getfield com/amazon/mas/kiwi/util/Base64$OutputStream/bufferLength I
if_icmplt L1
aload 0
getfield com/amazon/mas/kiwi/util/Base64$OutputStream/buffer [B
iconst_0
aload 0
getfield com/amazon/mas/kiwi/util/Base64$OutputStream/b4 [B
iconst_0
aload 0
getfield com/amazon/mas/kiwi/util/Base64$OutputStream/options I
invokestatic com/amazon/mas/kiwi/util/Base64/access$200([BI[BII)I
istore 1
aload 0
getfield com/amazon/mas/kiwi/util/Base64$OutputStream/out Ljava/io/OutputStream;
aload 0
getfield com/amazon/mas/kiwi/util/Base64$OutputStream/b4 [B
iconst_0
iload 1
invokevirtual java/io/OutputStream/write([BII)V
aload 0
iconst_0
putfield com/amazon/mas/kiwi/util/Base64$OutputStream/position I
return
L4:
aload 0
getfield com/amazon/mas/kiwi/util/Base64$OutputStream/decodabet [B
iload 1
bipush 127
iand
baload
bipush -5
if_icmpeq L1
new java/io/IOException
dup
ldc "Invalid character in Base64 data."
invokespecial java/io/IOException/<init>(Ljava/lang/String;)V
athrow
.limit locals 4
.limit stack 5
.end method

.method public write([BII)V
.throws java/io/IOException
aload 0
getfield com/amazon/mas/kiwi/util/Base64$OutputStream/suspendEncoding Z
ifeq L0
aload 0
getfield com/amazon/mas/kiwi/util/Base64$OutputStream/out Ljava/io/OutputStream;
aload 1
iload 2
iload 3
invokevirtual java/io/OutputStream/write([BII)V
L1:
return
L0:
iconst_0
istore 4
L2:
iload 4
iload 3
if_icmpge L1
aload 0
aload 1
iload 2
iload 4
iadd
baload
invokevirtual com/amazon/mas/kiwi/util/Base64$OutputStream/write(I)V
iload 4
iconst_1
iadd
istore 4
goto L2
.limit locals 5
.limit stack 4
.end method
