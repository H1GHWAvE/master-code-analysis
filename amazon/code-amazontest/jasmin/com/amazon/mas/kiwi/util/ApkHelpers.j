.bytecode 50.0
.class public synchronized com/amazon/mas/kiwi/util/ApkHelpers
.super java/lang/Object

.field private static final 'CONTENT_PREFIX' Ljava/lang/String; = "com.amazon.content.id."

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static getApkSignature(Ljava/lang/String;)[B
.throws java/io/IOException
.throws com/amazon/mas/kiwi/util/ApkInvalidException
.catch java/lang/SecurityException from L0 to L1 using L2
aload 0
ifnull L3
aload 0
invokevirtual java/lang/String/length()I
ifne L4
L3:
new java/lang/IllegalArgumentException
dup
ldc "apkFileName cannot be null or empty!"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L4:
new java/util/jar/JarFile
dup
aload 0
invokespecial java/util/jar/JarFile/<init>(Ljava/lang/String;)V
astore 0
L0:
aload 0
invokestatic com/amazon/mas/kiwi/util/ApkHelpers/scanJar(Ljava/util/jar/JarFile;)V
L1:
aload 0
invokestatic com/amazon/mas/kiwi/util/ApkHelpers/getFirstSigningCert(Ljava/util/jar/JarFile;)Ljava/security/cert/Certificate;
astore 1
aload 1
ifnonnull L5
aconst_null
areturn
L2:
astore 0
aconst_null
areturn
L5:
aconst_null
astore 0
aload 1
instanceof java/security/cert/X509Certificate
ifeq L6
aload 1
checkcast java/security/cert/X509Certificate
invokevirtual java/security/cert/X509Certificate/getSignature()[B
astore 0
L6:
aload 0
areturn
.limit locals 2
.limit stack 3
.end method

.method private static getCodeSigners(Ljava/util/jar/JarFile;)[Ljava/security/CodeSigner;
aconst_null
astore 1
aload 0
invokevirtual java/util/jar/JarFile/entries()Ljava/util/Enumeration;
astore 2
aload 1
astore 0
L0:
aload 2
invokeinterface java/util/Enumeration/hasMoreElements()Z 0
ifeq L1
aload 2
invokeinterface java/util/Enumeration/nextElement()Ljava/lang/Object; 0
checkcast java/util/jar/JarEntry
invokevirtual java/util/jar/JarEntry/getCodeSigners()[Ljava/security/CodeSigner;
astore 1
aload 1
astore 0
aload 1
ifnull L0
aload 1
astore 0
L1:
aload 0
areturn
.limit locals 3
.limit stack 1
.end method

.method public static getContentID(Ljava/util/jar/JarFile;)Ljava/lang/String;
aload 0
ifnonnull L0
new java/lang/IllegalArgumentException
dup
ldc "apkSrc must not be null!"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
invokevirtual java/util/jar/JarFile/entries()Ljava/util/Enumeration;
astore 0
L1:
aload 0
invokeinterface java/util/Enumeration/hasMoreElements()Z 0
ifeq L2
aload 0
invokeinterface java/util/Enumeration/nextElement()Ljava/lang/Object; 0
checkcast java/util/jar/JarEntry
astore 1
aload 1
invokevirtual java/util/jar/JarEntry/isDirectory()Z
ifne L1
aload 1
invokevirtual java/util/jar/JarEntry/getName()Ljava/lang/String;
invokestatic com/amazon/mas/kiwi/util/ApkHelpers/getContentIDFromName(Ljava/lang/String;)Ljava/lang/String;
astore 1
aload 1
ifnull L1
aload 1
invokevirtual java/lang/String/length()I
ifeq L1
aload 1
areturn
L2:
aconst_null
areturn
.limit locals 2
.limit stack 3
.end method

.method public static getContentIDFromName(Ljava/lang/String;)Ljava/lang/String;
aload 0
ifnull L0
aload 0
invokevirtual java/lang/String/length()I
ifne L1
L0:
new java/lang/IllegalArgumentException
dup
ldc "name cannot be null or empty!"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
ldc "com.amazon.content.id."
invokevirtual java/lang/String/indexOf(Ljava/lang/String;)I
istore 1
iload 1
ifge L2
aconst_null
areturn
L2:
aload 0
invokevirtual java/lang/String/length()I
ldc "com.amazon.content.id."
invokevirtual java/lang/String/length()I
if_icmpgt L3
aconst_null
areturn
L3:
aload 0
ldc "com.amazon.content.id."
invokevirtual java/lang/String/length()I
iload 1
iadd
invokevirtual java/lang/String/substring(I)Ljava/lang/String;
areturn
.limit locals 2
.limit stack 3
.end method

.method private static getFirstSigningCert(Ljava/util/jar/JarFile;)Ljava/security/cert/Certificate;
aload 0
invokestatic com/amazon/mas/kiwi/util/ApkHelpers/getCodeSigners(Ljava/util/jar/JarFile;)[Ljava/security/CodeSigner;
astore 2
aconst_null
astore 1
aload 1
astore 0
aload 2
ifnull L0
aload 1
astore 0
aload 2
arraylength
ifle L0
aload 2
iconst_0
aaload
invokevirtual java/security/CodeSigner/getSignerCertPath()Ljava/security/cert/CertPath;
invokevirtual java/security/cert/CertPath/getCertificates()Ljava/util/List;
astore 2
aload 1
astore 0
aload 2
invokeinterface java/util/List/isEmpty()Z 0
ifne L0
aload 2
iconst_0
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast java/security/cert/Certificate
astore 0
L0:
aload 0
areturn
.limit locals 3
.limit stack 2
.end method

.method public static isSigned(Ljava/io/File;)Z
.throws java/io/IOException
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L4
aconst_null
astore 3
L0:
new java/util/jar/JarFile
dup
aload 0
invokespecial java/util/jar/JarFile/<init>(Ljava/io/File;)V
astore 0
L1:
aload 0
invokestatic com/amazon/mas/kiwi/util/ApkHelpers/isSigned(Ljava/util/jar/JarFile;)Z
istore 1
L3:
aload 0
invokevirtual java/util/jar/JarFile/close()V
iload 1
ireturn
L2:
astore 2
aload 3
astore 0
L5:
aload 0
invokevirtual java/util/jar/JarFile/close()V
aload 2
athrow
L4:
astore 2
goto L5
.limit locals 4
.limit stack 3
.end method

.method public static isSigned(Ljava/util/jar/JarFile;)Z
.throws java/io/IOException
.catch java/lang/SecurityException from L0 to L1 using L2
aload 0
ifnonnull L0
new java/lang/IllegalArgumentException
dup
ldc "apkSrc must not be null!"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
invokestatic com/amazon/mas/kiwi/util/ApkHelpers/scanJar(Ljava/util/jar/JarFile;)V
L1:
aload 0
invokestatic com/amazon/mas/kiwi/util/ApkHelpers/getCodeSigners(Ljava/util/jar/JarFile;)[Ljava/security/CodeSigner;
ifnull L3
iconst_1
ireturn
L2:
astore 0
iconst_1
ireturn
L3:
iconst_0
ireturn
.limit locals 1
.limit stack 3
.end method

.method private static scanJar(Ljava/util/jar/JarFile;)V
.throws java/io/IOException
.catch java/lang/Exception from L0 to L1 using L2
.catch all from L0 to L1 using L3
.catch java/lang/Exception from L4 to L5 using L2
.catch all from L4 to L5 using L3
.catch all from L6 to L3 using L3
aload 0
invokevirtual java/util/jar/JarFile/entries()Ljava/util/Enumeration;
astore 5
sipush 8192
newarray byte
astore 6
L7:
aload 5
invokeinterface java/util/Enumeration/hasMoreElements()Z 0
ifeq L8
aload 5
invokeinterface java/util/Enumeration/nextElement()Ljava/lang/Object; 0
checkcast java/util/jar/JarEntry
astore 4
aconst_null
astore 3
aconst_null
astore 2
L0:
aload 0
aload 4
invokevirtual java/util/jar/JarFile/getInputStream(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;
astore 4
L1:
aload 4
astore 2
aload 4
astore 3
L4:
aload 4
aload 6
iconst_0
aload 6
arraylength
invokevirtual java/io/InputStream/read([BII)I
istore 1
L5:
iload 1
iconst_m1
if_icmpne L1
aload 4
ifnull L7
aload 4
invokevirtual java/io/InputStream/close()V
goto L7
L2:
astore 0
aload 2
astore 3
L6:
new com/amazon/mas/kiwi/util/ApkInvalidException
dup
aload 0
invokespecial com/amazon/mas/kiwi/util/ApkInvalidException/<init>(Ljava/lang/Throwable;)V
athrow
L3:
astore 0
aload 3
ifnull L9
aload 3
invokevirtual java/io/InputStream/close()V
L9:
aload 0
athrow
L8:
return
.limit locals 7
.limit stack 4
.end method
