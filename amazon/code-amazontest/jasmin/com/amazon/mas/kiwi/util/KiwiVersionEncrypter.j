.bytecode 50.0
.class public synchronized com/amazon/mas/kiwi/util/KiwiVersionEncrypter
.super java/lang/Object

.field private static final 'SECRET_KEY' Ljava/lang/String; = "Kiwi__Version__Obfuscator"

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static checkInput(Ljava/lang/String;Ljava/lang/String;)V
aload 0
ifnull L0
aload 0
invokevirtual java/lang/String/isEmpty()Z
ifeq L1
L0:
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "input '"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "' cannot be null or empty"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L1:
return
.limit locals 2
.limit stack 4
.end method

.method public static decrypt(Ljava/lang/String;)Ljava/lang/String;
.throws java/io/IOException
aload 0
ldc "text"
invokestatic com/amazon/mas/kiwi/util/KiwiVersionEncrypter/checkInput(Ljava/lang/String;Ljava/lang/String;)V
new java/lang/String
dup
aload 0
invokevirtual java/lang/String/getBytes()[B
invokestatic com/amazon/mas/kiwi/util/Base64/decode([B)[B
invokestatic com/amazon/mas/kiwi/util/KiwiVersionEncrypter/performXOR([B)[B
ldc "UTF-8"
invokespecial java/lang/String/<init>([BLjava/lang/String;)V
areturn
.limit locals 1
.limit stack 4
.end method

.method public static decryptFromFile(Ljava/lang/String;)Ljava/lang/String;
.throws java/io/IOException
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L4
.catch java/io/IOException from L5 to L6 using L7
.catch java/io/IOException from L8 to L9 using L10
aload 0
ldc "file"
invokestatic com/amazon/mas/kiwi/util/KiwiVersionEncrypter/checkInput(Ljava/lang/String;Ljava/lang/String;)V
aconst_null
astore 1
L0:
new java/io/BufferedReader
dup
new java/io/FileReader
dup
aload 0
invokespecial java/io/FileReader/<init>(Ljava/lang/String;)V
invokespecial java/io/BufferedReader/<init>(Ljava/io/Reader;)V
astore 0
L1:
aload 0
invokevirtual java/io/BufferedReader/readLine()Ljava/lang/String;
astore 1
L3:
aload 0
ifnull L6
L5:
aload 0
invokevirtual java/io/BufferedReader/close()V
L6:
new java/lang/String
dup
aload 1
invokevirtual java/lang/String/getBytes()[B
invokestatic com/amazon/mas/kiwi/util/Base64/decode([B)[B
invokestatic com/amazon/mas/kiwi/util/KiwiVersionEncrypter/performXOR([B)[B
ldc "UTF-8"
invokespecial java/lang/String/<init>([BLjava/lang/String;)V
areturn
L2:
astore 0
L11:
aload 1
ifnull L9
L8:
aload 1
invokevirtual java/io/BufferedReader/close()V
L9:
aload 0
athrow
L7:
astore 0
goto L6
L10:
astore 1
goto L9
L4:
astore 2
aload 0
astore 1
aload 2
astore 0
goto L11
.limit locals 3
.limit stack 5
.end method

.method public static encrypt(Ljava/lang/String;)Ljava/lang/String;
aload 0
ldc "text"
invokestatic com/amazon/mas/kiwi/util/KiwiVersionEncrypter/checkInput(Ljava/lang/String;Ljava/lang/String;)V
aload 0
invokevirtual java/lang/String/getBytes()[B
invokestatic com/amazon/mas/kiwi/util/KiwiVersionEncrypter/performXOR([B)[B
invokestatic com/amazon/mas/kiwi/util/Base64/encodeBytes([B)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method

.method public static encryptToFile(Ljava/lang/String;Ljava/lang/String;)V
.throws java/io/IOException
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L4
.catch java/io/IOException from L5 to L6 using L7
.catch java/io/IOException from L8 to L9 using L10
aload 0
ldc "text"
invokestatic com/amazon/mas/kiwi/util/KiwiVersionEncrypter/checkInput(Ljava/lang/String;Ljava/lang/String;)V
aload 1
ldc "file"
invokestatic com/amazon/mas/kiwi/util/KiwiVersionEncrypter/checkInput(Ljava/lang/String;Ljava/lang/String;)V
aload 0
invokevirtual java/lang/String/getBytes()[B
invokestatic com/amazon/mas/kiwi/util/KiwiVersionEncrypter/performXOR([B)[B
invokestatic com/amazon/mas/kiwi/util/Base64/encodeBytes([B)Ljava/lang/String;
astore 0
aconst_null
astore 2
L0:
new java/io/BufferedWriter
dup
new java/io/FileWriter
dup
aload 1
invokespecial java/io/FileWriter/<init>(Ljava/lang/String;)V
invokespecial java/io/BufferedWriter/<init>(Ljava/io/Writer;)V
astore 1
L1:
aload 1
aload 0
invokevirtual java/io/BufferedWriter/write(Ljava/lang/String;)V
aload 1
invokevirtual java/io/BufferedWriter/flush()V
L3:
aload 1
ifnull L6
L5:
aload 1
invokevirtual java/io/BufferedWriter/close()V
L6:
return
L2:
astore 0
aload 2
astore 1
L11:
aload 1
ifnull L9
L8:
aload 1
invokevirtual java/io/BufferedWriter/close()V
L9:
aload 0
athrow
L7:
astore 0
return
L10:
astore 1
goto L9
L4:
astore 0
goto L11
.limit locals 3
.limit stack 5
.end method

.method public static main([Ljava/lang/String;)V
.throws java/lang/Exception
aload 0
ifnull L0
aload 0
arraylength
ifne L1
L0:
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "Usage: com.amazon.mas.kiwi.util.KiwiVersionEncrypter <textToBeEncrypted> [<encryptToFileName>]"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
return
L1:
aload 0
arraylength
iconst_1
if_icmple L2
aload 0
iconst_0
aaload
aload 0
iconst_1
aaload
invokestatic com/amazon/mas/kiwi/util/KiwiVersionEncrypter/encryptToFile(Ljava/lang/String;Ljava/lang/String;)V
return
L2:
getstatic java/lang/System/out Ljava/io/PrintStream;
aload 0
iconst_0
aaload
invokestatic com/amazon/mas/kiwi/util/KiwiVersionEncrypter/encrypt(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
return
.limit locals 1
.limit stack 3
.end method

.method private static performXOR([B)[B
aload 0
arraylength
newarray byte
astore 4
ldc "Kiwi__Version__Obfuscator"
invokevirtual java/lang/String/getBytes()[B
astore 5
iconst_0
istore 1
iconst_0
istore 2
L0:
iload 2
aload 0
arraylength
if_icmpge L1
aload 4
iload 2
aload 0
iload 2
baload
aload 5
iload 1
baload
ixor
i2b
bastore
iload 1
iconst_1
iadd
istore 3
iload 3
istore 1
iload 3
aload 5
arraylength
if_icmplt L2
iconst_0
istore 1
L2:
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
aload 4
areturn
.limit locals 6
.limit stack 5
.end method
