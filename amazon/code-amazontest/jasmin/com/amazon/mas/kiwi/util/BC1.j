.bytecode 50.0
.class public synchronized com/amazon/mas/kiwi/util/BC1
.super java/lang/Object
.inner class static final inner com/amazon/mas/kiwi/util/BC1$1

.field private static 'CHECKSUM_ALGORITHM' Ljava/lang/String;

.field private static final 'DIGEST_UPDATE_BUFFER_SIZE' I = 10240


.field private static final 'DIRECTORIES_TO_IGNORE_FOR_PARTIAL_CHECKSUM' Ljava/util/Set; signature "Ljava/util/Set<Ljava/lang/String;>;"

.field private static final 'MAX_SIZE_FOR_FULL_CHECKSUM' I = 20971520


.method static <clinit>()V
ldc "MD5"
putstatic com/amazon/mas/kiwi/util/BC1/CHECKSUM_ALGORITHM Ljava/lang/String;
new com/amazon/mas/kiwi/util/BC1$1
dup
invokespecial com/amazon/mas/kiwi/util/BC1$1/<init>()V
putstatic com/amazon/mas/kiwi/util/BC1/DIRECTORIES_TO_IGNORE_FOR_PARTIAL_CHECKSUM Ljava/util/Set;
return
.limit locals 0
.limit stack 2
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static calculateFullChecksum(Ljava/io/File;Ljava/security/MessageDigest;)V
.throws java/io/IOException
new java/io/FileInputStream
dup
aload 0
invokespecial java/io/FileInputStream/<init>(Ljava/io/File;)V
aload 1
invokestatic com/amazon/mas/kiwi/util/BC1/updateMessageDigestWithInputStream(Ljava/io/InputStream;Ljava/security/MessageDigest;)V
return
.limit locals 2
.limit stack 3
.end method

.method private static calculatePartialChecksum(Ljava/io/File;Ljava/security/MessageDigest;[B)V
.throws java/io/IOException
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
new java/util/jar/JarFile
dup
aload 0
invokespecial java/util/jar/JarFile/<init>(Ljava/io/File;)V
astore 4
aload 4
invokevirtual java/util/jar/JarFile/entries()Ljava/util/Enumeration;
astore 5
L5:
aload 5
invokeinterface java/util/Enumeration/hasMoreElements()Z 0
ifeq L6
aload 5
invokeinterface java/util/Enumeration/nextElement()Ljava/lang/Object; 0
checkcast java/util/jar/JarEntry
astore 3
aload 3
invokestatic com/amazon/mas/kiwi/util/BC1/isInDirectoryToIgnore(Ljava/util/jar/JarEntry;)Z
ifne L5
aconst_null
astore 0
L0:
aload 4
aload 3
invokevirtual java/util/jar/JarFile/getInputStream(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;
astore 3
L1:
aload 3
astore 0
L3:
aload 3
aload 1
aload 2
invokestatic com/amazon/mas/kiwi/util/BC1/updateMessageDigestWithInputStream(Ljava/io/InputStream;Ljava/security/MessageDigest;[B)V
L4:
aload 3
invokestatic com/amazon/mas/kiwi/util/BC1/closeIgnoreException(Ljava/io/InputStream;)V
goto L5
L2:
astore 1
aload 0
invokestatic com/amazon/mas/kiwi/util/BC1/closeIgnoreException(Ljava/io/InputStream;)V
aload 1
athrow
L6:
aload 4
invokestatic com/amazon/mas/kiwi/util/BC1/closeIgnoreException(Ljava/util/zip/ZipFile;)V
return
.limit locals 6
.limit stack 3
.end method

.method private static closeIgnoreException(Ljava/io/InputStream;)V
.catch java/io/IOException from L0 to L1 using L2
aload 0
ifnull L1
L0:
aload 0
invokevirtual java/io/InputStream/close()V
L1:
return
L2:
astore 0
return
.limit locals 1
.limit stack 1
.end method

.method private static closeIgnoreException(Ljava/util/zip/ZipFile;)V
.catch java/io/IOException from L0 to L1 using L2
aload 0
ifnull L1
L0:
aload 0
invokevirtual java/util/zip/ZipFile/close()V
L1:
return
L2:
astore 0
return
.limit locals 1
.limit stack 1
.end method

.method public static getBC1Checksum(Ljava/io/File;)[B
.throws java/io/IOException
invokestatic com/amazon/mas/kiwi/util/BC1/getMessageDigest()Ljava/security/MessageDigest;
astore 1
aload 0
invokestatic com/amazon/mas/kiwi/util/BC1/isTooLargeForFullChecksum(Ljava/io/File;)Z
ifeq L0
aload 0
aload 1
sipush 10240
newarray byte
invokestatic com/amazon/mas/kiwi/util/BC1/calculatePartialChecksum(Ljava/io/File;Ljava/security/MessageDigest;[B)V
L1:
aload 1
invokevirtual java/security/MessageDigest/digest()[B
areturn
L0:
aload 0
aload 1
invokestatic com/amazon/mas/kiwi/util/BC1/calculateFullChecksum(Ljava/io/File;Ljava/security/MessageDigest;)V
goto L1
.limit locals 2
.limit stack 3
.end method

.method public static getBC1Checksum(Ljava/lang/String;)[B
.throws java/io/IOException
aload 0
ifnonnull L0
new java/lang/IllegalArgumentException
dup
ldc "Given path is null"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
new java/io/File
dup
aload 0
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 1
aload 1
invokevirtual java/io/File/exists()Z
ifne L1
new java/io/IOException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Cannot calculate checksum, file does not exist: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/IOException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 1
invokestatic com/amazon/mas/kiwi/util/BC1/getBC1Checksum(Ljava/io/File;)[B
areturn
.limit locals 2
.limit stack 4
.end method

.method public static getBC1ChecksumBase64(Ljava/lang/String;)Ljava/lang/String;
.throws java/io/IOException
aload 0
invokestatic com/amazon/mas/kiwi/util/BC1/getBC1Checksum(Ljava/lang/String;)[B
invokestatic com/amazon/mas/kiwi/util/BC1/toBase64([B)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static getMessageDigest()Ljava/security/MessageDigest;
.catch java/security/NoSuchAlgorithmException from L0 to L1 using L2
L0:
getstatic com/amazon/mas/kiwi/util/BC1/CHECKSUM_ALGORITHM Ljava/lang/String;
invokestatic java/security/MessageDigest/getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
astore 0
L1:
aload 0
areturn
L2:
astore 0
new java/lang/IllegalStateException
dup
aload 0
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 1
.limit stack 3
.end method

.method private static isInDirectoryToIgnore(Ljava/util/jar/JarEntry;)Z
getstatic com/amazon/mas/kiwi/util/BC1/DIRECTORIES_TO_IGNORE_FOR_PARTIAL_CHECKSUM Ljava/util/Set;
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 1
L0:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/String
astore 2
aload 0
invokevirtual java/util/jar/JarEntry/getName()Ljava/lang/String;
aload 2
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L0
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 3
.limit stack 2
.end method

.method private static isTooLargeForFullChecksum(Ljava/io/File;)Z
aload 0
invokevirtual java/io/File/length()J
ldc2_w 20971520L
lcmp
iflt L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 4
.end method

.method private static toBase64([B)Ljava/lang/String;
new java/lang/String
dup
aload 0
invokestatic com/amazon/mas/kiwi/util/Base64/encodeBytes([B)Ljava/lang/String;
invokespecial java/lang/String/<init>(Ljava/lang/String;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static updateMessageDigestWithInputStream(Ljava/io/InputStream;Ljava/security/MessageDigest;)V
.throws java/io/FileNotFoundException
.throws java/io/IOException
aload 0
aload 1
sipush 10240
newarray byte
invokestatic com/amazon/mas/kiwi/util/BC1/updateMessageDigestWithInputStream(Ljava/io/InputStream;Ljava/security/MessageDigest;[B)V
return
.limit locals 2
.limit stack 3
.end method

.method private static updateMessageDigestWithInputStream(Ljava/io/InputStream;Ljava/security/MessageDigest;[B)V
.throws java/io/FileNotFoundException
.throws java/io/IOException
L0:
aload 0
aload 2
invokevirtual java/io/InputStream/read([B)I
istore 3
iload 3
iconst_m1
if_icmpeq L1
aload 1
aload 2
iconst_0
iload 3
invokevirtual java/security/MessageDigest/update([BII)V
goto L0
L1:
return
.limit locals 4
.limit stack 4
.end method
