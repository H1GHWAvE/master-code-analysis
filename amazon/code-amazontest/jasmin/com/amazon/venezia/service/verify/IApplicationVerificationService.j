.bytecode 50.0
.class public abstract interface com/amazon/venezia/service/verify/IApplicationVerificationService
.super java/lang/Object
.implements android/os/IInterface
.inner class public static abstract Stub inner com/amazon/venezia/service/verify/IApplicationVerificationService$Stub outer com/amazon/venezia/service/verify/IApplicationVerificationService
.inner class private static Proxy inner com/amazon/venezia/service/verify/IApplicationVerificationService$Stub$Proxy outer com/amazon/venezia/service/verify/IApplicationVerificationService

.method public abstract getAmazonId()Ljava/lang/String;
.throws android/os/RemoteException
.end method

.method public abstract getDeviceId()Ljava/lang/String;
.throws android/os/RemoteException
.end method

.method public abstract getToken(Ljava/lang/String;)Ljava/lang/String;
.throws android/os/RemoteException
.end method

.method public abstract reportVerificationResults(Ljava/lang/String;ZLjava/lang/String;)V
.throws android/os/RemoteException
.end method
