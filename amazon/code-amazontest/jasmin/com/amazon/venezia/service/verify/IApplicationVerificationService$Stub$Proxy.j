.bytecode 50.0
.class synchronized com/amazon/venezia/service/verify/IApplicationVerificationService$Stub$Proxy
.super java/lang/Object
.implements com/amazon/venezia/service/verify/IApplicationVerificationService
.inner class public static abstract Stub inner com/amazon/venezia/service/verify/IApplicationVerificationService$Stub outer com/amazon/venezia/service/verify/IApplicationVerificationService
.inner class private static Proxy inner com/amazon/venezia/service/verify/IApplicationVerificationService$Stub$Proxy outer com/amazon/venezia/service/verify/IApplicationVerificationService$Stub

.field private 'mRemote' Landroid/os/IBinder;

.method <init>(Landroid/os/IBinder;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield com/amazon/venezia/service/verify/IApplicationVerificationService$Stub$Proxy/mRemote Landroid/os/IBinder;
return
.limit locals 2
.limit stack 2
.end method

.method public asBinder()Landroid/os/IBinder;
aload 0
getfield com/amazon/venezia/service/verify/IApplicationVerificationService$Stub$Proxy/mRemote Landroid/os/IBinder;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getAmazonId()Ljava/lang/String;
.throws android/os/RemoteException
.catch all from L0 to L1 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 1
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 2
L0:
aload 1
ldc "com.amazon.venezia.service.verify.IApplicationVerificationService"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
aload 0
getfield com/amazon/venezia/service/verify/IApplicationVerificationService$Stub$Proxy/mRemote Landroid/os/IBinder;
iconst_3
aload 1
aload 2
iconst_0
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
aload 2
invokevirtual android/os/Parcel/readException()V
aload 2
invokevirtual android/os/Parcel/readString()Ljava/lang/String;
astore 3
L1:
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
invokevirtual android/os/Parcel/recycle()V
aload 3
areturn
L2:
astore 3
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
invokevirtual android/os/Parcel/recycle()V
aload 3
athrow
.limit locals 4
.limit stack 5
.end method

.method public getDeviceId()Ljava/lang/String;
.throws android/os/RemoteException
.catch all from L0 to L1 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 1
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 2
L0:
aload 1
ldc "com.amazon.venezia.service.verify.IApplicationVerificationService"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
aload 0
getfield com/amazon/venezia/service/verify/IApplicationVerificationService$Stub$Proxy/mRemote Landroid/os/IBinder;
iconst_4
aload 1
aload 2
iconst_0
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
aload 2
invokevirtual android/os/Parcel/readException()V
aload 2
invokevirtual android/os/Parcel/readString()Ljava/lang/String;
astore 3
L1:
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
invokevirtual android/os/Parcel/recycle()V
aload 3
areturn
L2:
astore 3
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
invokevirtual android/os/Parcel/recycle()V
aload 3
athrow
.limit locals 4
.limit stack 5
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
ldc "com.amazon.venezia.service.verify.IApplicationVerificationService"
areturn
.limit locals 1
.limit stack 1
.end method

.method public getToken(Ljava/lang/String;)Ljava/lang/String;
.throws android/os/RemoteException
.catch all from L0 to L1 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 3
L0:
aload 2
ldc "com.amazon.venezia.service.verify.IApplicationVerificationService"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
aload 2
aload 1
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
aload 0
getfield com/amazon/venezia/service/verify/IApplicationVerificationService$Stub$Proxy/mRemote Landroid/os/IBinder;
iconst_2
aload 2
aload 3
iconst_0
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
aload 3
invokevirtual android/os/Parcel/readException()V
aload 3
invokevirtual android/os/Parcel/readString()Ljava/lang/String;
astore 1
L1:
aload 3
invokevirtual android/os/Parcel/recycle()V
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
areturn
L2:
astore 1
aload 3
invokevirtual android/os/Parcel/recycle()V
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
athrow
.limit locals 4
.limit stack 5
.end method

.method public reportVerificationResults(Ljava/lang/String;ZLjava/lang/String;)V
.throws android/os/RemoteException
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 5
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 6
L0:
aload 5
ldc "com.amazon.venezia.service.verify.IApplicationVerificationService"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
aload 5
aload 1
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
L1:
iload 2
ifeq L5
iconst_1
istore 4
L3:
aload 5
iload 4
invokevirtual android/os/Parcel/writeInt(I)V
aload 5
aload 3
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
aload 0
getfield com/amazon/venezia/service/verify/IApplicationVerificationService$Stub$Proxy/mRemote Landroid/os/IBinder;
iconst_1
aload 5
aload 6
iconst_0
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
aload 6
invokevirtual android/os/Parcel/readException()V
L4:
aload 6
invokevirtual android/os/Parcel/recycle()V
aload 5
invokevirtual android/os/Parcel/recycle()V
return
L5:
iconst_0
istore 4
goto L3
L2:
astore 1
aload 6
invokevirtual android/os/Parcel/recycle()V
aload 5
invokevirtual android/os/Parcel/recycle()V
aload 1
athrow
.limit locals 7
.limit stack 5
.end method
