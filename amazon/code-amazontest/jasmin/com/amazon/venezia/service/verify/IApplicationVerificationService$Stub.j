.bytecode 50.0
.class public synchronized abstract com/amazon/venezia/service/verify/IApplicationVerificationService$Stub
.super android/os/Binder
.implements com/amazon/venezia/service/verify/IApplicationVerificationService
.inner class public static abstract Stub inner com/amazon/venezia/service/verify/IApplicationVerificationService$Stub outer com/amazon/venezia/service/verify/IApplicationVerificationService
.inner class private static Proxy inner com/amazon/venezia/service/verify/IApplicationVerificationService$Stub$Proxy outer com/amazon/venezia/service/verify/IApplicationVerificationService$Stub

.field private static final 'DESCRIPTOR' Ljava/lang/String; = "com.amazon.venezia.service.verify.IApplicationVerificationService"

.field static final 'TRANSACTION_getAmazonId' I = 3


.field static final 'TRANSACTION_getDeviceId' I = 4


.field static final 'TRANSACTION_getToken' I = 2


.field static final 'TRANSACTION_reportVerificationResults' I = 1


.method public <init>()V
aload 0
invokespecial android/os/Binder/<init>()V
aload 0
aload 0
ldc "com.amazon.venezia.service.verify.IApplicationVerificationService"
invokevirtual com/amazon/venezia/service/verify/IApplicationVerificationService$Stub/attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V
return
.limit locals 1
.limit stack 3
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/amazon/venezia/service/verify/IApplicationVerificationService;
aload 0
ifnonnull L0
aconst_null
areturn
L0:
aload 0
ldc "com.amazon.venezia.service.verify.IApplicationVerificationService"
invokeinterface android/os/IBinder/queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface; 1
astore 1
aload 1
ifnull L1
aload 1
instanceof com/amazon/venezia/service/verify/IApplicationVerificationService
ifeq L1
aload 1
checkcast com/amazon/venezia/service/verify/IApplicationVerificationService
areturn
L1:
new com/amazon/venezia/service/verify/IApplicationVerificationService$Stub$Proxy
dup
aload 0
invokespecial com/amazon/venezia/service/verify/IApplicationVerificationService$Stub$Proxy/<init>(Landroid/os/IBinder;)V
areturn
.limit locals 2
.limit stack 3
.end method

.method public asBinder()Landroid/os/IBinder;
aload 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
.throws android/os/RemoteException
iload 1
lookupswitch
1 : L0
2 : L1
3 : L2
4 : L3
1598968902 : L4
default : L5
L5:
aload 0
iload 1
aload 2
aload 3
iload 4
invokespecial android/os/Binder/onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
ireturn
L4:
aload 3
ldc "com.amazon.venezia.service.verify.IApplicationVerificationService"
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
iconst_1
ireturn
L0:
aload 2
ldc "com.amazon.venezia.service.verify.IApplicationVerificationService"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 2
invokevirtual android/os/Parcel/readString()Ljava/lang/String;
astore 6
aload 2
invokevirtual android/os/Parcel/readInt()I
ifeq L6
iconst_1
istore 5
L7:
aload 0
aload 6
iload 5
aload 2
invokevirtual android/os/Parcel/readString()Ljava/lang/String;
invokevirtual com/amazon/venezia/service/verify/IApplicationVerificationService$Stub/reportVerificationResults(Ljava/lang/String;ZLjava/lang/String;)V
aload 3
invokevirtual android/os/Parcel/writeNoException()V
iconst_1
ireturn
L6:
iconst_0
istore 5
goto L7
L1:
aload 2
ldc "com.amazon.venezia.service.verify.IApplicationVerificationService"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
aload 2
invokevirtual android/os/Parcel/readString()Ljava/lang/String;
invokevirtual com/amazon/venezia/service/verify/IApplicationVerificationService$Stub/getToken(Ljava/lang/String;)Ljava/lang/String;
astore 2
aload 3
invokevirtual android/os/Parcel/writeNoException()V
aload 3
aload 2
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
iconst_1
ireturn
L2:
aload 2
ldc "com.amazon.venezia.service.verify.IApplicationVerificationService"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
invokevirtual com/amazon/venezia/service/verify/IApplicationVerificationService$Stub/getAmazonId()Ljava/lang/String;
astore 2
aload 3
invokevirtual android/os/Parcel/writeNoException()V
aload 3
aload 2
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
iconst_1
ireturn
L3:
aload 2
ldc "com.amazon.venezia.service.verify.IApplicationVerificationService"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
invokevirtual com/amazon/venezia/service/verify/IApplicationVerificationService$Stub/getDeviceId()Ljava/lang/String;
astore 2
aload 3
invokevirtual android/os/Parcel/writeNoException()V
aload 3
aload 2
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
iconst_1
ireturn
.limit locals 7
.limit stack 5
.end method
