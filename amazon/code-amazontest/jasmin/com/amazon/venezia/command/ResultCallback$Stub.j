.bytecode 50.0
.class public synchronized abstract com/amazon/venezia/command/ResultCallback$Stub
.super android/os/Binder
.implements com/amazon/venezia/command/ResultCallback
.inner class public static abstract Stub inner com/amazon/venezia/command/ResultCallback$Stub outer com/amazon/venezia/command/ResultCallback
.inner class private static Proxy inner com/amazon/venezia/command/ResultCallback$Stub$Proxy outer com/amazon/venezia/command/ResultCallback$Stub

.field private static final 'DESCRIPTOR' Ljava/lang/String; = "com.amazon.venezia.command.ResultCallback"

.field static final 'TRANSACTION_onDecide' I = 3


.field static final 'TRANSACTION_onException' I = 4


.field static final 'TRANSACTION_onFailure' I = 2


.field static final 'TRANSACTION_onSuccess' I = 1


.method public <init>()V
aload 0
invokespecial android/os/Binder/<init>()V
aload 0
aload 0
ldc "com.amazon.venezia.command.ResultCallback"
invokevirtual com/amazon/venezia/command/ResultCallback$Stub/attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V
return
.limit locals 1
.limit stack 3
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/amazon/venezia/command/ResultCallback;
aload 0
ifnonnull L0
aconst_null
areturn
L0:
aload 0
ldc "com.amazon.venezia.command.ResultCallback"
invokeinterface android/os/IBinder/queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface; 1
astore 1
aload 1
ifnull L1
aload 1
instanceof com/amazon/venezia/command/ResultCallback
ifeq L1
aload 1
checkcast com/amazon/venezia/command/ResultCallback
areturn
L1:
new com/amazon/venezia/command/ResultCallback$Stub$Proxy
dup
aload 0
invokespecial com/amazon/venezia/command/ResultCallback$Stub$Proxy/<init>(Landroid/os/IBinder;)V
areturn
.limit locals 2
.limit stack 3
.end method

.method public asBinder()Landroid/os/IBinder;
aload 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
.throws android/os/RemoteException
iload 1
lookupswitch
1 : L0
2 : L1
3 : L2
4 : L3
1598968902 : L4
default : L5
L5:
aload 0
iload 1
aload 2
aload 3
iload 4
invokespecial android/os/Binder/onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
ireturn
L4:
aload 3
ldc "com.amazon.venezia.command.ResultCallback"
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
iconst_1
ireturn
L0:
aload 2
ldc "com.amazon.venezia.command.ResultCallback"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
aload 2
invokevirtual android/os/Parcel/readStrongBinder()Landroid/os/IBinder;
invokestatic com/amazon/venezia/command/SuccessResult$Stub/asInterface(Landroid/os/IBinder;)Lcom/amazon/venezia/command/SuccessResult;
invokevirtual com/amazon/venezia/command/ResultCallback$Stub/onSuccess(Lcom/amazon/venezia/command/SuccessResult;)V
aload 3
invokevirtual android/os/Parcel/writeNoException()V
iconst_1
ireturn
L1:
aload 2
ldc "com.amazon.venezia.command.ResultCallback"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
aload 2
invokevirtual android/os/Parcel/readStrongBinder()Landroid/os/IBinder;
invokestatic com/amazon/venezia/command/FailureResult$Stub/asInterface(Landroid/os/IBinder;)Lcom/amazon/venezia/command/FailureResult;
invokevirtual com/amazon/venezia/command/ResultCallback$Stub/onFailure(Lcom/amazon/venezia/command/FailureResult;)V
aload 3
invokevirtual android/os/Parcel/writeNoException()V
iconst_1
ireturn
L2:
aload 2
ldc "com.amazon.venezia.command.ResultCallback"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
aload 2
invokevirtual android/os/Parcel/readStrongBinder()Landroid/os/IBinder;
invokestatic com/amazon/venezia/command/DecisionResult$Stub/asInterface(Landroid/os/IBinder;)Lcom/amazon/venezia/command/DecisionResult;
invokevirtual com/amazon/venezia/command/ResultCallback$Stub/onDecide(Lcom/amazon/venezia/command/DecisionResult;)V
aload 3
invokevirtual android/os/Parcel/writeNoException()V
iconst_1
ireturn
L3:
aload 2
ldc "com.amazon.venezia.command.ResultCallback"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
aload 2
invokevirtual android/os/Parcel/readStrongBinder()Landroid/os/IBinder;
invokestatic com/amazon/venezia/command/ExceptionResult$Stub/asInterface(Landroid/os/IBinder;)Lcom/amazon/venezia/command/ExceptionResult;
invokevirtual com/amazon/venezia/command/ResultCallback$Stub/onException(Lcom/amazon/venezia/command/ExceptionResult;)V
aload 3
invokevirtual android/os/Parcel/writeNoException()V
iconst_1
ireturn
.limit locals 5
.limit stack 5
.end method
