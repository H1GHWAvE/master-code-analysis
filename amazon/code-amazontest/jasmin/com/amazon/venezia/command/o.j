.bytecode 50.0
.class public synchronized abstract com/amazon/venezia/command/o
.super android/os/Binder
.implements com/amazon/venezia/command/f

.method public <init>()V
aload 0
invokespecial android/os/Binder/<init>()V
aload 0
aload 0
ldc "com.amazon.venezia.command.ResultCallback"
invokevirtual com/amazon/venezia/command/o/attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V
return
.limit locals 1
.limit stack 3
.end method

.method public asBinder()Landroid/os/IBinder;
aload 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
.throws android/os/RemoteException
iload 1
lookupswitch
1 : L0
2 : L1
3 : L2
4 : L3
1598968902 : L4
default : L5
L5:
aload 0
iload 1
aload 2
aload 3
iload 4
invokespecial android/os/Binder/onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
ireturn
L4:
aload 3
ldc "com.amazon.venezia.command.ResultCallback"
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
iconst_1
ireturn
L0:
aload 2
ldc "com.amazon.venezia.command.ResultCallback"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 2
invokevirtual android/os/Parcel/readStrongBinder()Landroid/os/IBinder;
astore 2
aload 2
ifnonnull L6
aconst_null
astore 2
L7:
aload 0
aload 2
invokevirtual com/amazon/venezia/command/o/a(Lcom/amazon/venezia/command/SuccessResult;)V
aload 3
invokevirtual android/os/Parcel/writeNoException()V
iconst_1
ireturn
L6:
aload 2
ldc "com.amazon.venezia.command.SuccessResult"
invokeinterface android/os/IBinder/queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface; 1
astore 5
aload 5
ifnull L8
aload 5
instanceof com/amazon/venezia/command/SuccessResult
ifeq L8
aload 5
checkcast com/amazon/venezia/command/SuccessResult
astore 2
goto L7
L8:
new com/amazon/venezia/command/x
dup
aload 2
invokespecial com/amazon/venezia/command/x/<init>(Landroid/os/IBinder;)V
astore 2
goto L7
L1:
aload 2
ldc "com.amazon.venezia.command.ResultCallback"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 2
invokevirtual android/os/Parcel/readStrongBinder()Landroid/os/IBinder;
astore 2
aload 2
ifnonnull L9
aconst_null
astore 2
L10:
aload 0
aload 2
invokevirtual com/amazon/venezia/command/o/a(Lcom/amazon/venezia/command/FailureResult;)V
aload 3
invokevirtual android/os/Parcel/writeNoException()V
iconst_1
ireturn
L9:
aload 2
ldc "com.amazon.venezia.command.FailureResult"
invokeinterface android/os/IBinder/queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface; 1
astore 5
aload 5
ifnull L11
aload 5
instanceof com/amazon/venezia/command/FailureResult
ifeq L11
aload 5
checkcast com/amazon/venezia/command/FailureResult
astore 2
goto L10
L11:
new com/amazon/venezia/command/ab
dup
aload 2
invokespecial com/amazon/venezia/command/ab/<init>(Landroid/os/IBinder;)V
astore 2
goto L10
L2:
aload 2
ldc "com.amazon.venezia.command.ResultCallback"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 2
invokevirtual android/os/Parcel/readStrongBinder()Landroid/os/IBinder;
astore 2
aload 2
ifnonnull L12
aconst_null
astore 2
L13:
aload 0
aload 2
invokevirtual com/amazon/venezia/command/o/a(Lcom/amazon/venezia/command/r;)V
aload 3
invokevirtual android/os/Parcel/writeNoException()V
iconst_1
ireturn
L12:
aload 2
ldc "com.amazon.venezia.command.DecisionResult"
invokeinterface android/os/IBinder/queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface; 1
astore 5
aload 5
ifnull L14
aload 5
instanceof com/amazon/venezia/command/r
ifeq L14
aload 5
checkcast com/amazon/venezia/command/r
astore 2
goto L13
L14:
new com/amazon/venezia/command/aa
dup
aload 2
invokespecial com/amazon/venezia/command/aa/<init>(Landroid/os/IBinder;)V
astore 2
goto L13
L3:
aload 2
ldc "com.amazon.venezia.command.ResultCallback"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 2
invokevirtual android/os/Parcel/readStrongBinder()Landroid/os/IBinder;
astore 2
aload 2
ifnonnull L15
aconst_null
astore 2
L16:
aload 0
aload 2
invokevirtual com/amazon/venezia/command/o/a(Lcom/amazon/venezia/command/k;)V
aload 3
invokevirtual android/os/Parcel/writeNoException()V
iconst_1
ireturn
L15:
aload 2
ldc "com.amazon.venezia.command.ExceptionResult"
invokeinterface android/os/IBinder/queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface; 1
astore 5
aload 5
ifnull L17
aload 5
instanceof com/amazon/venezia/command/k
ifeq L17
aload 5
checkcast com/amazon/venezia/command/k
astore 2
goto L16
L17:
new com/amazon/venezia/command/v
dup
aload 2
invokespecial com/amazon/venezia/command/v/<init>(Landroid/os/IBinder;)V
astore 2
goto L16
.limit locals 6
.limit stack 5
.end method
