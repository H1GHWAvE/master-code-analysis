.bytecode 50.0
.class public synchronized abstract com/amazon/venezia/command/FailureResult$Stub
.super android/os/Binder
.implements com/amazon/venezia/command/FailureResult
.inner class public static abstract Stub inner com/amazon/venezia/command/FailureResult$Stub outer com/amazon/venezia/command/FailureResult
.inner class private static Proxy inner com/amazon/venezia/command/FailureResult$Stub$Proxy outer com/amazon/venezia/command/FailureResult$Stub

.field private static final 'DESCRIPTOR' Ljava/lang/String; = "com.amazon.venezia.command.FailureResult"

.field static final 'TRANSACTION_getAuthToken' I = 1


.field static final 'TRANSACTION_getButtonLabel' I = 4


.field static final 'TRANSACTION_getDisplayableMessage' I = 3


.field static final 'TRANSACTION_getDisplayableName' I = 2


.field static final 'TRANSACTION_getExtensionData' I = 6


.field static final 'TRANSACTION_show' I = 5


.method public <init>()V
aload 0
invokespecial android/os/Binder/<init>()V
aload 0
aload 0
ldc "com.amazon.venezia.command.FailureResult"
invokevirtual com/amazon/venezia/command/FailureResult$Stub/attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V
return
.limit locals 1
.limit stack 3
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/amazon/venezia/command/FailureResult;
aload 0
ifnonnull L0
aconst_null
areturn
L0:
aload 0
ldc "com.amazon.venezia.command.FailureResult"
invokeinterface android/os/IBinder/queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface; 1
astore 1
aload 1
ifnull L1
aload 1
instanceof com/amazon/venezia/command/FailureResult
ifeq L1
aload 1
checkcast com/amazon/venezia/command/FailureResult
areturn
L1:
new com/amazon/venezia/command/FailureResult$Stub$Proxy
dup
aload 0
invokespecial com/amazon/venezia/command/FailureResult$Stub$Proxy/<init>(Landroid/os/IBinder;)V
areturn
.limit locals 2
.limit stack 3
.end method

.method public asBinder()Landroid/os/IBinder;
aload 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
.throws android/os/RemoteException
iload 1
lookupswitch
1 : L0
2 : L1
3 : L2
4 : L3
5 : L4
6 : L5
1598968902 : L6
default : L7
L7:
aload 0
iload 1
aload 2
aload 3
iload 4
invokespecial android/os/Binder/onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
ireturn
L6:
aload 3
ldc "com.amazon.venezia.command.FailureResult"
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
iconst_1
ireturn
L0:
aload 2
ldc "com.amazon.venezia.command.FailureResult"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
invokevirtual com/amazon/venezia/command/FailureResult$Stub/getAuthToken()Ljava/lang/String;
astore 2
aload 3
invokevirtual android/os/Parcel/writeNoException()V
aload 3
aload 2
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
iconst_1
ireturn
L1:
aload 2
ldc "com.amazon.venezia.command.FailureResult"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
invokevirtual com/amazon/venezia/command/FailureResult$Stub/getDisplayableName()Ljava/lang/String;
astore 2
aload 3
invokevirtual android/os/Parcel/writeNoException()V
aload 3
aload 2
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
iconst_1
ireturn
L2:
aload 2
ldc "com.amazon.venezia.command.FailureResult"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
invokevirtual com/amazon/venezia/command/FailureResult$Stub/getDisplayableMessage()Ljava/lang/String;
astore 2
aload 3
invokevirtual android/os/Parcel/writeNoException()V
aload 3
aload 2
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
iconst_1
ireturn
L3:
aload 2
ldc "com.amazon.venezia.command.FailureResult"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
invokevirtual com/amazon/venezia/command/FailureResult$Stub/getButtonLabel()Ljava/lang/String;
astore 2
aload 3
invokevirtual android/os/Parcel/writeNoException()V
aload 3
aload 2
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
iconst_1
ireturn
L4:
aload 2
ldc "com.amazon.venezia.command.FailureResult"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
invokevirtual com/amazon/venezia/command/FailureResult$Stub/show()Z
istore 5
aload 3
invokevirtual android/os/Parcel/writeNoException()V
iload 5
ifeq L8
iconst_1
istore 1
L9:
aload 3
iload 1
invokevirtual android/os/Parcel/writeInt(I)V
iconst_1
ireturn
L8:
iconst_0
istore 1
goto L9
L5:
aload 2
ldc "com.amazon.venezia.command.FailureResult"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
invokevirtual com/amazon/venezia/command/FailureResult$Stub/getExtensionData()Ljava/util/Map;
astore 2
aload 3
invokevirtual android/os/Parcel/writeNoException()V
aload 3
aload 2
invokevirtual android/os/Parcel/writeMap(Ljava/util/Map;)V
iconst_1
ireturn
.limit locals 6
.limit stack 5
.end method
