.bytecode 50.0
.class public synchronized abstract com/amazon/venezia/command/q
.super android/os/Binder
.implements com/amazon/venezia/command/h

.method public <init>()V
aload 0
invokespecial android/os/Binder/<init>()V
aload 0
aload 0
ldc "com.amazon.venezia.command.CommandService"
invokevirtual com/amazon/venezia/command/q/attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V
return
.limit locals 1
.limit stack 3
.end method

.method public static a(Landroid/os/IBinder;)Lcom/amazon/venezia/command/h;
aload 0
ifnonnull L0
aconst_null
areturn
L0:
aload 0
ldc "com.amazon.venezia.command.CommandService"
invokeinterface android/os/IBinder/queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface; 1
astore 1
aload 1
ifnull L1
aload 1
instanceof com/amazon/venezia/command/h
ifeq L1
aload 1
checkcast com/amazon/venezia/command/h
areturn
L1:
new com/amazon/venezia/command/l
dup
aload 0
invokespecial com/amazon/venezia/command/l/<init>(Landroid/os/IBinder;)V
areturn
.limit locals 2
.limit stack 3
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
.throws android/os/RemoteException
iload 1
lookupswitch
1 : L0
1598968902 : L1
default : L2
L2:
aload 0
iload 1
aload 2
aload 3
iload 4
invokespecial android/os/Binder/onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
ireturn
L1:
aload 3
ldc "com.amazon.venezia.command.CommandService"
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
iconst_1
ireturn
L0:
aload 2
ldc "com.amazon.venezia.command.CommandService"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 2
invokevirtual android/os/Parcel/readStrongBinder()Landroid/os/IBinder;
astore 5
aload 5
ifnonnull L3
aconst_null
astore 5
L4:
aload 2
invokevirtual android/os/Parcel/readStrongBinder()Landroid/os/IBinder;
astore 2
aload 2
ifnonnull L5
aconst_null
astore 2
L6:
aload 0
aload 5
aload 2
invokevirtual com/amazon/venezia/command/q/a(Lcom/amazon/venezia/command/w;Lcom/amazon/venezia/command/f;)V
aload 3
invokevirtual android/os/Parcel/writeNoException()V
iconst_1
ireturn
L3:
aload 5
ldc "com.amazon.venezia.command.Command"
invokeinterface android/os/IBinder/queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface; 1
astore 6
aload 6
ifnull L7
aload 6
instanceof com/amazon/venezia/command/w
ifeq L7
aload 6
checkcast com/amazon/venezia/command/w
astore 5
goto L4
L7:
new com/amazon/venezia/command/d
dup
aload 5
invokespecial com/amazon/venezia/command/d/<init>(Landroid/os/IBinder;)V
astore 5
goto L4
L5:
aload 2
ldc "com.amazon.venezia.command.ResultCallback"
invokeinterface android/os/IBinder/queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface; 1
astore 6
aload 6
ifnull L8
aload 6
instanceof com/amazon/venezia/command/f
ifeq L8
aload 6
checkcast com/amazon/venezia/command/f
astore 2
goto L6
L8:
new com/amazon/venezia/command/t
dup
aload 2
invokespecial com/amazon/venezia/command/t/<init>(Landroid/os/IBinder;)V
astore 2
goto L6
.limit locals 7
.limit stack 5
.end method
