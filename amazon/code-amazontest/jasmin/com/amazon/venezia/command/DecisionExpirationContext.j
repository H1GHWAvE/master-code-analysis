.bytecode 50.0
.class public abstract interface com/amazon/venezia/command/DecisionExpirationContext
.super java/lang/Object
.implements android/os/IInterface
.inner class public static abstract Stub inner com/amazon/venezia/command/DecisionExpirationContext$Stub outer com/amazon/venezia/command/DecisionExpirationContext
.inner class private static Proxy inner com/amazon/venezia/command/DecisionExpirationContext$Stub$Proxy outer com/amazon/venezia/command/DecisionExpirationContext

.method public abstract getExtensionData()Ljava/util/Map;
.throws android/os/RemoteException
.end method

.method public abstract getReason()Ljava/lang/String;
.throws android/os/RemoteException
.end method
