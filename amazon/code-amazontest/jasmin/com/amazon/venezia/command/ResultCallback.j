.bytecode 50.0
.class public abstract interface com/amazon/venezia/command/ResultCallback
.super java/lang/Object
.implements android/os/IInterface
.inner class public static abstract Stub inner com/amazon/venezia/command/ResultCallback$Stub outer com/amazon/venezia/command/ResultCallback
.inner class private static Proxy inner com/amazon/venezia/command/ResultCallback$Stub$Proxy outer com/amazon/venezia/command/ResultCallback

.method public abstract onDecide(Lcom/amazon/venezia/command/DecisionResult;)V
.throws android/os/RemoteException
.end method

.method public abstract onException(Lcom/amazon/venezia/command/ExceptionResult;)V
.throws android/os/RemoteException
.end method

.method public abstract onFailure(Lcom/amazon/venezia/command/FailureResult;)V
.throws android/os/RemoteException
.end method

.method public abstract onSuccess(Lcom/amazon/venezia/command/SuccessResult;)V
.throws android/os/RemoteException
.end method
