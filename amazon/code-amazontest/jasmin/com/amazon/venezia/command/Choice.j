.bytecode 50.0
.class public abstract interface com/amazon/venezia/command/Choice
.super java/lang/Object
.implements android/os/IInterface
.inner class public static abstract Stub inner com/amazon/venezia/command/Choice$Stub outer com/amazon/venezia/command/Choice
.inner class private static Proxy inner com/amazon/venezia/command/Choice$Stub$Proxy outer com/amazon/venezia/command/Choice

.method public abstract choose(Lcom/amazon/venezia/command/ChoiceContext;)V
.throws android/os/RemoteException
.end method

.method public abstract getDisplayableName()Ljava/lang/String;
.throws android/os/RemoteException
.end method

.method public abstract getExtensionData()Ljava/util/Map;
.throws android/os/RemoteException
.end method

.method public abstract getIntent()Landroid/content/Intent;
.throws android/os/RemoteException
.end method
