.bytecode 50.0
.class public abstract interface com/amazon/venezia/command/DecisionResult
.super java/lang/Object
.implements android/os/IInterface
.inner class public static abstract Stub inner com/amazon/venezia/command/DecisionResult$Stub outer com/amazon/venezia/command/DecisionResult
.inner class private static Proxy inner com/amazon/venezia/command/DecisionResult$Stub$Proxy outer com/amazon/venezia/command/DecisionResult

.method public abstract expire(Lcom/amazon/venezia/command/DecisionExpirationContext;)V
.throws android/os/RemoteException
.end method

.method public abstract getAuthToken()Ljava/lang/String;
.throws android/os/RemoteException
.end method

.method public abstract getDecisionDurationInSeconds()J
.throws android/os/RemoteException
.end method

.method public abstract getDescription()Ljava/lang/String;
.throws android/os/RemoteException
.end method

.method public abstract getDisplayableName()Ljava/lang/String;
.throws android/os/RemoteException
.end method

.method public abstract getExtensionData()Ljava/util/Map;
.throws android/os/RemoteException
.end method

.method public abstract getNegativeChoice()Lcom/amazon/venezia/command/Choice;
.throws android/os/RemoteException
.end method

.method public abstract getNeutralChoice()Lcom/amazon/venezia/command/Choice;
.throws android/os/RemoteException
.end method

.method public abstract getPositiveChoice()Lcom/amazon/venezia/command/Choice;
.throws android/os/RemoteException
.end method
