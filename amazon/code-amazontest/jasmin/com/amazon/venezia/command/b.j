.bytecode 50.0
.class public synchronized abstract com/amazon/venezia/command/b
.super android/os/Binder
.implements com/amazon/venezia/command/FailureResult

.method public <init>()V
aload 0
invokespecial android/os/Binder/<init>()V
aload 0
aload 0
ldc "com.amazon.venezia.command.FailureResult"
invokevirtual com/amazon/venezia/command/b/attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V
return
.limit locals 1
.limit stack 3
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
.throws android/os/RemoteException
iload 1
lookupswitch
1 : L0
2 : L1
3 : L2
4 : L3
5 : L4
6 : L5
1598968902 : L6
default : L7
L7:
aload 0
iload 1
aload 2
aload 3
iload 4
invokespecial android/os/Binder/onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
ireturn
L6:
aload 3
ldc "com.amazon.venezia.command.FailureResult"
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
iconst_1
ireturn
L0:
aload 2
ldc "com.amazon.venezia.command.FailureResult"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
invokevirtual com/amazon/venezia/command/b/getAuthToken()Ljava/lang/String;
astore 2
aload 3
invokevirtual android/os/Parcel/writeNoException()V
aload 3
aload 2
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
iconst_1
ireturn
L1:
aload 2
ldc "com.amazon.venezia.command.FailureResult"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
invokevirtual com/amazon/venezia/command/b/getDisplayableName()Ljava/lang/String;
astore 2
aload 3
invokevirtual android/os/Parcel/writeNoException()V
aload 3
aload 2
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
iconst_1
ireturn
L2:
aload 2
ldc "com.amazon.venezia.command.FailureResult"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
invokevirtual com/amazon/venezia/command/b/getDisplayableMessage()Ljava/lang/String;
astore 2
aload 3
invokevirtual android/os/Parcel/writeNoException()V
aload 3
aload 2
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
iconst_1
ireturn
L3:
aload 2
ldc "com.amazon.venezia.command.FailureResult"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
invokevirtual com/amazon/venezia/command/b/getButtonLabel()Ljava/lang/String;
astore 2
aload 3
invokevirtual android/os/Parcel/writeNoException()V
aload 3
aload 2
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
iconst_1
ireturn
L4:
aload 2
ldc "com.amazon.venezia.command.FailureResult"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
invokevirtual com/amazon/venezia/command/b/show()Z
istore 5
aload 3
invokevirtual android/os/Parcel/writeNoException()V
iload 5
ifeq L8
iconst_1
istore 1
L9:
aload 3
iload 1
invokevirtual android/os/Parcel/writeInt(I)V
iconst_1
ireturn
L8:
iconst_0
istore 1
goto L9
L5:
aload 2
ldc "com.amazon.venezia.command.FailureResult"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
invokevirtual com/amazon/venezia/command/b/getExtensionData()Ljava/util/Map;
astore 2
aload 3
invokevirtual android/os/Parcel/writeNoException()V
aload 3
aload 2
invokevirtual android/os/Parcel/writeMap(Ljava/util/Map;)V
iconst_1
ireturn
.limit locals 6
.limit stack 5
.end method
