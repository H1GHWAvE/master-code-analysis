.bytecode 50.0
.class final synchronized com/amazon/venezia/command/t
.super java/lang/Object
.implements com/amazon/venezia/command/f

.field private 'a' Landroid/os/IBinder;

.method <init>(Landroid/os/IBinder;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield com/amazon/venezia/command/t/a Landroid/os/IBinder;
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Lcom/amazon/venezia/command/FailureResult;)V
.throws android/os/RemoteException
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L4 to L5 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 3
L0:
aload 2
ldc "com.amazon.venezia.command.ResultCallback"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
L1:
aload 1
ifnull L6
L3:
aload 1
invokeinterface com/amazon/venezia/command/FailureResult/asBinder()Landroid/os/IBinder; 0
astore 1
L4:
aload 2
aload 1
invokevirtual android/os/Parcel/writeStrongBinder(Landroid/os/IBinder;)V
aload 0
getfield com/amazon/venezia/command/t/a Landroid/os/IBinder;
iconst_2
aload 2
aload 3
iconst_0
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
aload 3
invokevirtual android/os/Parcel/readException()V
L5:
aload 3
invokevirtual android/os/Parcel/recycle()V
aload 2
invokevirtual android/os/Parcel/recycle()V
return
L6:
aconst_null
astore 1
goto L4
L2:
astore 1
aload 3
invokevirtual android/os/Parcel/recycle()V
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
athrow
.limit locals 4
.limit stack 5
.end method

.method public final a(Lcom/amazon/venezia/command/SuccessResult;)V
.throws android/os/RemoteException
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L4 to L5 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 3
L0:
aload 2
ldc "com.amazon.venezia.command.ResultCallback"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
L1:
aload 1
ifnull L6
L3:
aload 1
invokeinterface com/amazon/venezia/command/SuccessResult/asBinder()Landroid/os/IBinder; 0
astore 1
L4:
aload 2
aload 1
invokevirtual android/os/Parcel/writeStrongBinder(Landroid/os/IBinder;)V
aload 0
getfield com/amazon/venezia/command/t/a Landroid/os/IBinder;
iconst_1
aload 2
aload 3
iconst_0
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
aload 3
invokevirtual android/os/Parcel/readException()V
L5:
aload 3
invokevirtual android/os/Parcel/recycle()V
aload 2
invokevirtual android/os/Parcel/recycle()V
return
L6:
aconst_null
astore 1
goto L4
L2:
astore 1
aload 3
invokevirtual android/os/Parcel/recycle()V
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
athrow
.limit locals 4
.limit stack 5
.end method

.method public final a(Lcom/amazon/venezia/command/k;)V
.throws android/os/RemoteException
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L4 to L5 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 3
L0:
aload 2
ldc "com.amazon.venezia.command.ResultCallback"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
L1:
aload 1
ifnull L6
L3:
aload 1
invokeinterface com/amazon/venezia/command/k/asBinder()Landroid/os/IBinder; 0
astore 1
L4:
aload 2
aload 1
invokevirtual android/os/Parcel/writeStrongBinder(Landroid/os/IBinder;)V
aload 0
getfield com/amazon/venezia/command/t/a Landroid/os/IBinder;
iconst_4
aload 2
aload 3
iconst_0
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
aload 3
invokevirtual android/os/Parcel/readException()V
L5:
aload 3
invokevirtual android/os/Parcel/recycle()V
aload 2
invokevirtual android/os/Parcel/recycle()V
return
L6:
aconst_null
astore 1
goto L4
L2:
astore 1
aload 3
invokevirtual android/os/Parcel/recycle()V
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
athrow
.limit locals 4
.limit stack 5
.end method

.method public final a(Lcom/amazon/venezia/command/r;)V
.throws android/os/RemoteException
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L4 to L5 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 3
L0:
aload 2
ldc "com.amazon.venezia.command.ResultCallback"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
L1:
aload 1
ifnull L6
L3:
aload 1
invokeinterface com/amazon/venezia/command/r/asBinder()Landroid/os/IBinder; 0
astore 1
L4:
aload 2
aload 1
invokevirtual android/os/Parcel/writeStrongBinder(Landroid/os/IBinder;)V
aload 0
getfield com/amazon/venezia/command/t/a Landroid/os/IBinder;
iconst_3
aload 2
aload 3
iconst_0
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
aload 3
invokevirtual android/os/Parcel/readException()V
L5:
aload 3
invokevirtual android/os/Parcel/recycle()V
aload 2
invokevirtual android/os/Parcel/recycle()V
return
L6:
aconst_null
astore 1
goto L4
L2:
astore 1
aload 3
invokevirtual android/os/Parcel/recycle()V
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
athrow
.limit locals 4
.limit stack 5
.end method

.method public final asBinder()Landroid/os/IBinder;
aload 0
getfield com/amazon/venezia/command/t/a Landroid/os/IBinder;
areturn
.limit locals 1
.limit stack 1
.end method
