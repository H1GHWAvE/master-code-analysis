.bytecode 50.0
.class synchronized com/amazon/venezia/command/DecisionResult$Stub$Proxy
.super java/lang/Object
.implements com/amazon/venezia/command/DecisionResult
.inner class public static abstract Stub inner com/amazon/venezia/command/DecisionResult$Stub outer com/amazon/venezia/command/DecisionResult
.inner class private static Proxy inner com/amazon/venezia/command/DecisionResult$Stub$Proxy outer com/amazon/venezia/command/DecisionResult$Stub

.field private 'mRemote' Landroid/os/IBinder;

.method <init>(Landroid/os/IBinder;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield com/amazon/venezia/command/DecisionResult$Stub$Proxy/mRemote Landroid/os/IBinder;
return
.limit locals 2
.limit stack 2
.end method

.method public asBinder()Landroid/os/IBinder;
aload 0
getfield com/amazon/venezia/command/DecisionResult$Stub$Proxy/mRemote Landroid/os/IBinder;
areturn
.limit locals 1
.limit stack 1
.end method

.method public expire(Lcom/amazon/venezia/command/DecisionExpirationContext;)V
.throws android/os/RemoteException
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L4 to L5 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 3
L0:
aload 2
ldc "com.amazon.venezia.command.DecisionResult"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
L1:
aload 1
ifnull L6
L3:
aload 1
invokeinterface com/amazon/venezia/command/DecisionExpirationContext/asBinder()Landroid/os/IBinder; 0
astore 1
L4:
aload 2
aload 1
invokevirtual android/os/Parcel/writeStrongBinder(Landroid/os/IBinder;)V
aload 0
getfield com/amazon/venezia/command/DecisionResult$Stub$Proxy/mRemote Landroid/os/IBinder;
iconst_5
aload 2
aload 3
iconst_0
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
aload 3
invokevirtual android/os/Parcel/readException()V
L5:
aload 3
invokevirtual android/os/Parcel/recycle()V
aload 2
invokevirtual android/os/Parcel/recycle()V
return
L6:
aconst_null
astore 1
goto L4
L2:
astore 1
aload 3
invokevirtual android/os/Parcel/recycle()V
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
athrow
.limit locals 4
.limit stack 5
.end method

.method public getAuthToken()Ljava/lang/String;
.throws android/os/RemoteException
.catch all from L0 to L1 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 1
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 2
L0:
aload 1
ldc "com.amazon.venezia.command.DecisionResult"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
aload 0
getfield com/amazon/venezia/command/DecisionResult$Stub$Proxy/mRemote Landroid/os/IBinder;
iconst_1
aload 1
aload 2
iconst_0
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
aload 2
invokevirtual android/os/Parcel/readException()V
aload 2
invokevirtual android/os/Parcel/readString()Ljava/lang/String;
astore 3
L1:
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
invokevirtual android/os/Parcel/recycle()V
aload 3
areturn
L2:
astore 3
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
invokevirtual android/os/Parcel/recycle()V
aload 3
athrow
.limit locals 4
.limit stack 5
.end method

.method public getDecisionDurationInSeconds()J
.throws android/os/RemoteException
.catch all from L0 to L1 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 3
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 4
L0:
aload 3
ldc "com.amazon.venezia.command.DecisionResult"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
aload 0
getfield com/amazon/venezia/command/DecisionResult$Stub$Proxy/mRemote Landroid/os/IBinder;
iconst_4
aload 3
aload 4
iconst_0
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
aload 4
invokevirtual android/os/Parcel/readException()V
aload 4
invokevirtual android/os/Parcel/readLong()J
lstore 1
L1:
aload 4
invokevirtual android/os/Parcel/recycle()V
aload 3
invokevirtual android/os/Parcel/recycle()V
lload 1
lreturn
L2:
astore 5
aload 4
invokevirtual android/os/Parcel/recycle()V
aload 3
invokevirtual android/os/Parcel/recycle()V
aload 5
athrow
.limit locals 6
.limit stack 5
.end method

.method public getDescription()Ljava/lang/String;
.throws android/os/RemoteException
.catch all from L0 to L1 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 1
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 2
L0:
aload 1
ldc "com.amazon.venezia.command.DecisionResult"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
aload 0
getfield com/amazon/venezia/command/DecisionResult$Stub$Proxy/mRemote Landroid/os/IBinder;
iconst_3
aload 1
aload 2
iconst_0
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
aload 2
invokevirtual android/os/Parcel/readException()V
aload 2
invokevirtual android/os/Parcel/readString()Ljava/lang/String;
astore 3
L1:
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
invokevirtual android/os/Parcel/recycle()V
aload 3
areturn
L2:
astore 3
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
invokevirtual android/os/Parcel/recycle()V
aload 3
athrow
.limit locals 4
.limit stack 5
.end method

.method public getDisplayableName()Ljava/lang/String;
.throws android/os/RemoteException
.catch all from L0 to L1 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 1
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 2
L0:
aload 1
ldc "com.amazon.venezia.command.DecisionResult"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
aload 0
getfield com/amazon/venezia/command/DecisionResult$Stub$Proxy/mRemote Landroid/os/IBinder;
iconst_2
aload 1
aload 2
iconst_0
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
aload 2
invokevirtual android/os/Parcel/readException()V
aload 2
invokevirtual android/os/Parcel/readString()Ljava/lang/String;
astore 3
L1:
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
invokevirtual android/os/Parcel/recycle()V
aload 3
areturn
L2:
astore 3
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
invokevirtual android/os/Parcel/recycle()V
aload 3
athrow
.limit locals 4
.limit stack 5
.end method

.method public getExtensionData()Ljava/util/Map;
.throws android/os/RemoteException
.catch all from L0 to L1 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 1
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 2
L0:
aload 1
ldc "com.amazon.venezia.command.DecisionResult"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
aload 0
getfield com/amazon/venezia/command/DecisionResult$Stub$Proxy/mRemote Landroid/os/IBinder;
bipush 9
aload 1
aload 2
iconst_0
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
aload 2
invokevirtual android/os/Parcel/readException()V
aload 2
aload 0
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getClassLoader()Ljava/lang/ClassLoader;
invokevirtual android/os/Parcel/readHashMap(Ljava/lang/ClassLoader;)Ljava/util/HashMap;
astore 3
L1:
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
invokevirtual android/os/Parcel/recycle()V
aload 3
areturn
L2:
astore 3
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
invokevirtual android/os/Parcel/recycle()V
aload 3
athrow
.limit locals 4
.limit stack 5
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
ldc "com.amazon.venezia.command.DecisionResult"
areturn
.limit locals 1
.limit stack 1
.end method

.method public getNegativeChoice()Lcom/amazon/venezia/command/Choice;
.throws android/os/RemoteException
.catch all from L0 to L1 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 1
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 2
L0:
aload 1
ldc "com.amazon.venezia.command.DecisionResult"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
aload 0
getfield com/amazon/venezia/command/DecisionResult$Stub$Proxy/mRemote Landroid/os/IBinder;
bipush 8
aload 1
aload 2
iconst_0
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
aload 2
invokevirtual android/os/Parcel/readException()V
aload 2
invokevirtual android/os/Parcel/readStrongBinder()Landroid/os/IBinder;
invokestatic com/amazon/venezia/command/Choice$Stub/asInterface(Landroid/os/IBinder;)Lcom/amazon/venezia/command/Choice;
astore 3
L1:
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
invokevirtual android/os/Parcel/recycle()V
aload 3
areturn
L2:
astore 3
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
invokevirtual android/os/Parcel/recycle()V
aload 3
athrow
.limit locals 4
.limit stack 5
.end method

.method public getNeutralChoice()Lcom/amazon/venezia/command/Choice;
.throws android/os/RemoteException
.catch all from L0 to L1 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 1
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 2
L0:
aload 1
ldc "com.amazon.venezia.command.DecisionResult"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
aload 0
getfield com/amazon/venezia/command/DecisionResult$Stub$Proxy/mRemote Landroid/os/IBinder;
bipush 7
aload 1
aload 2
iconst_0
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
aload 2
invokevirtual android/os/Parcel/readException()V
aload 2
invokevirtual android/os/Parcel/readStrongBinder()Landroid/os/IBinder;
invokestatic com/amazon/venezia/command/Choice$Stub/asInterface(Landroid/os/IBinder;)Lcom/amazon/venezia/command/Choice;
astore 3
L1:
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
invokevirtual android/os/Parcel/recycle()V
aload 3
areturn
L2:
astore 3
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
invokevirtual android/os/Parcel/recycle()V
aload 3
athrow
.limit locals 4
.limit stack 5
.end method

.method public getPositiveChoice()Lcom/amazon/venezia/command/Choice;
.throws android/os/RemoteException
.catch all from L0 to L1 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 1
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 2
L0:
aload 1
ldc "com.amazon.venezia.command.DecisionResult"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
aload 0
getfield com/amazon/venezia/command/DecisionResult$Stub$Proxy/mRemote Landroid/os/IBinder;
bipush 6
aload 1
aload 2
iconst_0
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
aload 2
invokevirtual android/os/Parcel/readException()V
aload 2
invokevirtual android/os/Parcel/readStrongBinder()Landroid/os/IBinder;
invokestatic com/amazon/venezia/command/Choice$Stub/asInterface(Landroid/os/IBinder;)Lcom/amazon/venezia/command/Choice;
astore 3
L1:
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
invokevirtual android/os/Parcel/recycle()V
aload 3
areturn
L2:
astore 3
aload 2
invokevirtual android/os/Parcel/recycle()V
aload 1
invokevirtual android/os/Parcel/recycle()V
aload 3
athrow
.limit locals 4
.limit stack 5
.end method
