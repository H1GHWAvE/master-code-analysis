.bytecode 50.0
.class public abstract interface com/amazon/venezia/command/FailureResult
.super java/lang/Object
.implements android/os/IInterface
.inner class public static abstract Stub inner com/amazon/venezia/command/FailureResult$Stub outer com/amazon/venezia/command/FailureResult
.inner class private static Proxy inner com/amazon/venezia/command/FailureResult$Stub$Proxy outer com/amazon/venezia/command/FailureResult

.method public abstract getAuthToken()Ljava/lang/String;
.throws android/os/RemoteException
.end method

.method public abstract getButtonLabel()Ljava/lang/String;
.throws android/os/RemoteException
.end method

.method public abstract getDisplayableMessage()Ljava/lang/String;
.throws android/os/RemoteException
.end method

.method public abstract getDisplayableName()Ljava/lang/String;
.throws android/os/RemoteException
.end method

.method public abstract getExtensionData()Ljava/util/Map;
.throws android/os/RemoteException
.end method

.method public abstract show()Z
.throws android/os/RemoteException
.end method
