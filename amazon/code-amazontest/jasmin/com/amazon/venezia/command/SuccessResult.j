.bytecode 50.0
.class public abstract interface com/amazon/venezia/command/SuccessResult
.super java/lang/Object
.implements android/os/IInterface
.inner class public static abstract Stub inner com/amazon/venezia/command/SuccessResult$Stub outer com/amazon/venezia/command/SuccessResult
.inner class private static Proxy inner com/amazon/venezia/command/SuccessResult$Stub$Proxy outer com/amazon/venezia/command/SuccessResult

.method public abstract getAuthToken()Ljava/lang/String;
.throws android/os/RemoteException
.end method

.method public abstract getData()Ljava/util/Map;
.throws android/os/RemoteException
.end method
