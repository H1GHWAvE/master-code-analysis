.bytecode 50.0
.class public abstract interface com/amazon/venezia/command/Command
.super java/lang/Object
.implements android/os/IInterface
.inner class public static abstract Stub inner com/amazon/venezia/command/Command$Stub outer com/amazon/venezia/command/Command
.inner class private static Proxy inner com/amazon/venezia/command/Command$Stub$Proxy outer com/amazon/venezia/command/Command

.method public abstract getData()Ljava/util/Map;
.throws android/os/RemoteException
.end method

.method public abstract getName()Ljava/lang/String;
.throws android/os/RemoteException
.end method

.method public abstract getPackageName()Ljava/lang/String;
.throws android/os/RemoteException
.end method

.method public abstract getVersion()Ljava/lang/String;
.throws android/os/RemoteException
.end method
