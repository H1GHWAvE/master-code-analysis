.bytecode 50.0
.class public abstract interface com/amazon/venezia/command/ExceptionResult
.super java/lang/Object
.implements android/os/IInterface
.inner class public static abstract Stub inner com/amazon/venezia/command/ExceptionResult$Stub outer com/amazon/venezia/command/ExceptionResult
.inner class private static Proxy inner com/amazon/venezia/command/ExceptionResult$Stub$Proxy outer com/amazon/venezia/command/ExceptionResult

.method public abstract getErrorCode()Ljava/lang/String;
.throws android/os/RemoteException
.end method

.method public abstract getExtensionData()Ljava/util/Map;
.throws android/os/RemoteException
.end method
