.bytecode 50.0
.class synchronized com/amazon/venezia/command/CommandService$Stub$Proxy
.super java/lang/Object
.implements com/amazon/venezia/command/CommandService
.inner class public static abstract Stub inner com/amazon/venezia/command/CommandService$Stub outer com/amazon/venezia/command/CommandService
.inner class private static Proxy inner com/amazon/venezia/command/CommandService$Stub$Proxy outer com/amazon/venezia/command/CommandService$Stub

.field private 'mRemote' Landroid/os/IBinder;

.method <init>(Landroid/os/IBinder;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield com/amazon/venezia/command/CommandService$Stub$Proxy/mRemote Landroid/os/IBinder;
return
.limit locals 2
.limit stack 2
.end method

.method public asBinder()Landroid/os/IBinder;
aload 0
getfield com/amazon/venezia/command/CommandService$Stub$Proxy/mRemote Landroid/os/IBinder;
areturn
.limit locals 1
.limit stack 1
.end method

.method public execute(Lcom/amazon/venezia/command/Command;Lcom/amazon/venezia/command/ResultCallback;)V
.throws android/os/RemoteException
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L4 to L5 using L2
.catch all from L6 to L7 using L2
.catch all from L7 to L8 using L2
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 3
invokestatic android/os/Parcel/obtain()Landroid/os/Parcel;
astore 4
L0:
aload 3
ldc "com.amazon.venezia.command.CommandService"
invokevirtual android/os/Parcel/writeInterfaceToken(Ljava/lang/String;)V
L1:
aload 1
ifnull L9
L3:
aload 1
invokeinterface com/amazon/venezia/command/Command/asBinder()Landroid/os/IBinder; 0
astore 1
L4:
aload 3
aload 1
invokevirtual android/os/Parcel/writeStrongBinder(Landroid/os/IBinder;)V
L5:
aload 2
ifnull L10
L6:
aload 2
invokeinterface com/amazon/venezia/command/ResultCallback/asBinder()Landroid/os/IBinder; 0
astore 1
L7:
aload 3
aload 1
invokevirtual android/os/Parcel/writeStrongBinder(Landroid/os/IBinder;)V
aload 0
getfield com/amazon/venezia/command/CommandService$Stub$Proxy/mRemote Landroid/os/IBinder;
iconst_1
aload 3
aload 4
iconst_0
invokeinterface android/os/IBinder/transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z 4
pop
aload 4
invokevirtual android/os/Parcel/readException()V
L8:
aload 4
invokevirtual android/os/Parcel/recycle()V
aload 3
invokevirtual android/os/Parcel/recycle()V
return
L9:
aconst_null
astore 1
goto L4
L10:
aconst_null
astore 1
goto L7
L2:
astore 1
aload 4
invokevirtual android/os/Parcel/recycle()V
aload 3
invokevirtual android/os/Parcel/recycle()V
aload 1
athrow
.limit locals 5
.limit stack 5
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
ldc "com.amazon.venezia.command.CommandService"
areturn
.limit locals 1
.limit stack 1
.end method
