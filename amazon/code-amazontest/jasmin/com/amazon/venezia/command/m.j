.bytecode 50.0
.class public synchronized abstract com/amazon/venezia/command/m
.super android/os/Binder
.implements com/amazon/venezia/command/n

.method public <init>()V
aload 0
invokespecial android/os/Binder/<init>()V
aload 0
aload 0
ldc "com.amazon.venezia.command.Choice"
invokevirtual com/amazon/venezia/command/m/attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V
return
.limit locals 1
.limit stack 3
.end method

.method public static a(Landroid/os/IBinder;)Lcom/amazon/venezia/command/n;
aload 0
ifnonnull L0
aconst_null
areturn
L0:
aload 0
ldc "com.amazon.venezia.command.Choice"
invokeinterface android/os/IBinder/queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface; 1
astore 1
aload 1
ifnull L1
aload 1
instanceof com/amazon/venezia/command/n
ifeq L1
aload 1
checkcast com/amazon/venezia/command/n
areturn
L1:
new com/amazon/venezia/command/a
dup
aload 0
invokespecial com/amazon/venezia/command/a/<init>(Landroid/os/IBinder;)V
areturn
.limit locals 2
.limit stack 3
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
.throws android/os/RemoteException
iload 1
lookupswitch
1 : L0
2 : L1
3 : L2
4 : L3
1598968902 : L4
default : L5
L5:
aload 0
iload 1
aload 2
aload 3
iload 4
invokespecial android/os/Binder/onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
ireturn
L4:
aload 3
ldc "com.amazon.venezia.command.Choice"
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
iconst_1
ireturn
L0:
aload 2
ldc "com.amazon.venezia.command.Choice"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
invokevirtual com/amazon/venezia/command/m/a()Ljava/lang/String;
astore 2
aload 3
invokevirtual android/os/Parcel/writeNoException()V
aload 3
aload 2
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
iconst_1
ireturn
L1:
aload 2
ldc "com.amazon.venezia.command.Choice"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
invokevirtual com/amazon/venezia/command/m/b()Landroid/content/Intent;
astore 2
aload 3
invokevirtual android/os/Parcel/writeNoException()V
aload 2
ifnull L6
aload 3
iconst_1
invokevirtual android/os/Parcel/writeInt(I)V
aload 2
aload 3
iconst_1
invokevirtual android/content/Intent/writeToParcel(Landroid/os/Parcel;I)V
L7:
iconst_1
ireturn
L6:
aload 3
iconst_0
invokevirtual android/os/Parcel/writeInt(I)V
goto L7
L2:
aload 2
ldc "com.amazon.venezia.command.Choice"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 2
invokevirtual android/os/Parcel/readStrongBinder()Landroid/os/IBinder;
astore 2
aload 2
ifnonnull L8
aconst_null
astore 2
L9:
aload 0
aload 2
invokevirtual com/amazon/venezia/command/m/a(Lcom/amazon/venezia/command/y;)V
aload 3
invokevirtual android/os/Parcel/writeNoException()V
iconst_1
ireturn
L8:
aload 2
ldc "com.amazon.venezia.command.ChoiceContext"
invokeinterface android/os/IBinder/queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface; 1
astore 5
aload 5
ifnull L10
aload 5
instanceof com/amazon/venezia/command/y
ifeq L10
aload 5
checkcast com/amazon/venezia/command/y
astore 2
goto L9
L10:
new com/amazon/venezia/command/j
dup
aload 2
invokespecial com/amazon/venezia/command/j/<init>(Landroid/os/IBinder;)V
astore 2
goto L9
L3:
aload 2
ldc "com.amazon.venezia.command.Choice"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
invokevirtual com/amazon/venezia/command/m/c()Ljava/util/Map;
astore 2
aload 3
invokevirtual android/os/Parcel/writeNoException()V
aload 3
aload 2
invokevirtual android/os/Parcel/writeMap(Ljava/util/Map;)V
iconst_1
ireturn
.limit locals 6
.limit stack 5
.end method
