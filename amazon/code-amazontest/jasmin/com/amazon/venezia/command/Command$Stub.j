.bytecode 50.0
.class public synchronized abstract com/amazon/venezia/command/Command$Stub
.super android/os/Binder
.implements com/amazon/venezia/command/Command
.inner class public static abstract Stub inner com/amazon/venezia/command/Command$Stub outer com/amazon/venezia/command/Command
.inner class private static Proxy inner com/amazon/venezia/command/Command$Stub$Proxy outer com/amazon/venezia/command/Command$Stub

.field private static final 'DESCRIPTOR' Ljava/lang/String; = "com.amazon.venezia.command.Command"

.field static final 'TRANSACTION_getData' I = 4


.field static final 'TRANSACTION_getName' I = 1


.field static final 'TRANSACTION_getPackageName' I = 3


.field static final 'TRANSACTION_getVersion' I = 2


.method public <init>()V
aload 0
invokespecial android/os/Binder/<init>()V
aload 0
aload 0
ldc "com.amazon.venezia.command.Command"
invokevirtual com/amazon/venezia/command/Command$Stub/attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V
return
.limit locals 1
.limit stack 3
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/amazon/venezia/command/Command;
aload 0
ifnonnull L0
aconst_null
areturn
L0:
aload 0
ldc "com.amazon.venezia.command.Command"
invokeinterface android/os/IBinder/queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface; 1
astore 1
aload 1
ifnull L1
aload 1
instanceof com/amazon/venezia/command/Command
ifeq L1
aload 1
checkcast com/amazon/venezia/command/Command
areturn
L1:
new com/amazon/venezia/command/Command$Stub$Proxy
dup
aload 0
invokespecial com/amazon/venezia/command/Command$Stub$Proxy/<init>(Landroid/os/IBinder;)V
areturn
.limit locals 2
.limit stack 3
.end method

.method public asBinder()Landroid/os/IBinder;
aload 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
.throws android/os/RemoteException
iload 1
lookupswitch
1 : L0
2 : L1
3 : L2
4 : L3
1598968902 : L4
default : L5
L5:
aload 0
iload 1
aload 2
aload 3
iload 4
invokespecial android/os/Binder/onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
ireturn
L4:
aload 3
ldc "com.amazon.venezia.command.Command"
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
iconst_1
ireturn
L0:
aload 2
ldc "com.amazon.venezia.command.Command"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
invokevirtual com/amazon/venezia/command/Command$Stub/getName()Ljava/lang/String;
astore 2
aload 3
invokevirtual android/os/Parcel/writeNoException()V
aload 3
aload 2
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
iconst_1
ireturn
L1:
aload 2
ldc "com.amazon.venezia.command.Command"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
invokevirtual com/amazon/venezia/command/Command$Stub/getVersion()Ljava/lang/String;
astore 2
aload 3
invokevirtual android/os/Parcel/writeNoException()V
aload 3
aload 2
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
iconst_1
ireturn
L2:
aload 2
ldc "com.amazon.venezia.command.Command"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
invokevirtual com/amazon/venezia/command/Command$Stub/getPackageName()Ljava/lang/String;
astore 2
aload 3
invokevirtual android/os/Parcel/writeNoException()V
aload 3
aload 2
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
iconst_1
ireturn
L3:
aload 2
ldc "com.amazon.venezia.command.Command"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
invokevirtual com/amazon/venezia/command/Command$Stub/getData()Ljava/util/Map;
astore 2
aload 3
invokevirtual android/os/Parcel/writeNoException()V
aload 3
aload 2
invokevirtual android/os/Parcel/writeMap(Ljava/util/Map;)V
iconst_1
ireturn
.limit locals 5
.limit stack 5
.end method
