.bytecode 50.0
.class public synchronized abstract com/amazon/venezia/command/DecisionResult$Stub
.super android/os/Binder
.implements com/amazon/venezia/command/DecisionResult
.inner class public static abstract Stub inner com/amazon/venezia/command/DecisionResult$Stub outer com/amazon/venezia/command/DecisionResult
.inner class private static Proxy inner com/amazon/venezia/command/DecisionResult$Stub$Proxy outer com/amazon/venezia/command/DecisionResult$Stub

.field private static final 'DESCRIPTOR' Ljava/lang/String; = "com.amazon.venezia.command.DecisionResult"

.field static final 'TRANSACTION_expire' I = 5


.field static final 'TRANSACTION_getAuthToken' I = 1


.field static final 'TRANSACTION_getDecisionDurationInSeconds' I = 4


.field static final 'TRANSACTION_getDescription' I = 3


.field static final 'TRANSACTION_getDisplayableName' I = 2


.field static final 'TRANSACTION_getExtensionData' I = 9


.field static final 'TRANSACTION_getNegativeChoice' I = 8


.field static final 'TRANSACTION_getNeutralChoice' I = 7


.field static final 'TRANSACTION_getPositiveChoice' I = 6


.method public <init>()V
aload 0
invokespecial android/os/Binder/<init>()V
aload 0
aload 0
ldc "com.amazon.venezia.command.DecisionResult"
invokevirtual com/amazon/venezia/command/DecisionResult$Stub/attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V
return
.limit locals 1
.limit stack 3
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/amazon/venezia/command/DecisionResult;
aload 0
ifnonnull L0
aconst_null
areturn
L0:
aload 0
ldc "com.amazon.venezia.command.DecisionResult"
invokeinterface android/os/IBinder/queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface; 1
astore 1
aload 1
ifnull L1
aload 1
instanceof com/amazon/venezia/command/DecisionResult
ifeq L1
aload 1
checkcast com/amazon/venezia/command/DecisionResult
areturn
L1:
new com/amazon/venezia/command/DecisionResult$Stub$Proxy
dup
aload 0
invokespecial com/amazon/venezia/command/DecisionResult$Stub$Proxy/<init>(Landroid/os/IBinder;)V
areturn
.limit locals 2
.limit stack 3
.end method

.method public asBinder()Landroid/os/IBinder;
aload 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
.throws android/os/RemoteException
iload 1
lookupswitch
1 : L0
2 : L1
3 : L2
4 : L3
5 : L4
6 : L5
7 : L6
8 : L7
9 : L8
1598968902 : L9
default : L10
L10:
aload 0
iload 1
aload 2
aload 3
iload 4
invokespecial android/os/Binder/onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
ireturn
L9:
aload 3
ldc "com.amazon.venezia.command.DecisionResult"
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
iconst_1
ireturn
L0:
aload 2
ldc "com.amazon.venezia.command.DecisionResult"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
invokevirtual com/amazon/venezia/command/DecisionResult$Stub/getAuthToken()Ljava/lang/String;
astore 2
aload 3
invokevirtual android/os/Parcel/writeNoException()V
aload 3
aload 2
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
iconst_1
ireturn
L1:
aload 2
ldc "com.amazon.venezia.command.DecisionResult"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
invokevirtual com/amazon/venezia/command/DecisionResult$Stub/getDisplayableName()Ljava/lang/String;
astore 2
aload 3
invokevirtual android/os/Parcel/writeNoException()V
aload 3
aload 2
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
iconst_1
ireturn
L2:
aload 2
ldc "com.amazon.venezia.command.DecisionResult"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
invokevirtual com/amazon/venezia/command/DecisionResult$Stub/getDescription()Ljava/lang/String;
astore 2
aload 3
invokevirtual android/os/Parcel/writeNoException()V
aload 3
aload 2
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
iconst_1
ireturn
L3:
aload 2
ldc "com.amazon.venezia.command.DecisionResult"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
invokevirtual com/amazon/venezia/command/DecisionResult$Stub/getDecisionDurationInSeconds()J
lstore 5
aload 3
invokevirtual android/os/Parcel/writeNoException()V
aload 3
lload 5
invokevirtual android/os/Parcel/writeLong(J)V
iconst_1
ireturn
L4:
aload 2
ldc "com.amazon.venezia.command.DecisionResult"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
aload 2
invokevirtual android/os/Parcel/readStrongBinder()Landroid/os/IBinder;
invokestatic com/amazon/venezia/command/DecisionExpirationContext$Stub/asInterface(Landroid/os/IBinder;)Lcom/amazon/venezia/command/DecisionExpirationContext;
invokevirtual com/amazon/venezia/command/DecisionResult$Stub/expire(Lcom/amazon/venezia/command/DecisionExpirationContext;)V
aload 3
invokevirtual android/os/Parcel/writeNoException()V
iconst_1
ireturn
L5:
aload 2
ldc "com.amazon.venezia.command.DecisionResult"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
invokevirtual com/amazon/venezia/command/DecisionResult$Stub/getPositiveChoice()Lcom/amazon/venezia/command/Choice;
astore 2
aload 3
invokevirtual android/os/Parcel/writeNoException()V
aload 2
ifnull L11
aload 2
invokeinterface com/amazon/venezia/command/Choice/asBinder()Landroid/os/IBinder; 0
astore 2
L12:
aload 3
aload 2
invokevirtual android/os/Parcel/writeStrongBinder(Landroid/os/IBinder;)V
iconst_1
ireturn
L11:
aconst_null
astore 2
goto L12
L6:
aload 2
ldc "com.amazon.venezia.command.DecisionResult"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
invokevirtual com/amazon/venezia/command/DecisionResult$Stub/getNeutralChoice()Lcom/amazon/venezia/command/Choice;
astore 2
aload 3
invokevirtual android/os/Parcel/writeNoException()V
aload 2
ifnull L13
aload 2
invokeinterface com/amazon/venezia/command/Choice/asBinder()Landroid/os/IBinder; 0
astore 2
L14:
aload 3
aload 2
invokevirtual android/os/Parcel/writeStrongBinder(Landroid/os/IBinder;)V
iconst_1
ireturn
L13:
aconst_null
astore 2
goto L14
L7:
aload 2
ldc "com.amazon.venezia.command.DecisionResult"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
invokevirtual com/amazon/venezia/command/DecisionResult$Stub/getNegativeChoice()Lcom/amazon/venezia/command/Choice;
astore 2
aload 3
invokevirtual android/os/Parcel/writeNoException()V
aload 2
ifnull L15
aload 2
invokeinterface com/amazon/venezia/command/Choice/asBinder()Landroid/os/IBinder; 0
astore 2
L16:
aload 3
aload 2
invokevirtual android/os/Parcel/writeStrongBinder(Landroid/os/IBinder;)V
iconst_1
ireturn
L15:
aconst_null
astore 2
goto L16
L8:
aload 2
ldc "com.amazon.venezia.command.DecisionResult"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
invokevirtual com/amazon/venezia/command/DecisionResult$Stub/getExtensionData()Ljava/util/Map;
astore 2
aload 3
invokevirtual android/os/Parcel/writeNoException()V
aload 3
aload 2
invokevirtual android/os/Parcel/writeMap(Ljava/util/Map;)V
iconst_1
ireturn
.limit locals 7
.limit stack 5
.end method
