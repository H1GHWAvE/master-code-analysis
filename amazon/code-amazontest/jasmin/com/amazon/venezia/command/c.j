.bytecode 50.0
.class public synchronized abstract com/amazon/venezia/command/c
.super android/os/Binder
.implements com/amazon/venezia/command/r

.method public <init>()V
aload 0
invokespecial android/os/Binder/<init>()V
aload 0
aload 0
ldc "com.amazon.venezia.command.DecisionResult"
invokevirtual com/amazon/venezia/command/c/attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V
return
.limit locals 1
.limit stack 3
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
.throws android/os/RemoteException
iload 1
lookupswitch
1 : L0
2 : L1
3 : L2
4 : L3
5 : L4
6 : L5
7 : L6
8 : L7
9 : L8
1598968902 : L9
default : L10
L10:
aload 0
iload 1
aload 2
aload 3
iload 4
invokespecial android/os/Binder/onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
ireturn
L9:
aload 3
ldc "com.amazon.venezia.command.DecisionResult"
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
iconst_1
ireturn
L0:
aload 2
ldc "com.amazon.venezia.command.DecisionResult"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
invokevirtual com/amazon/venezia/command/c/a()Ljava/lang/String;
astore 2
aload 3
invokevirtual android/os/Parcel/writeNoException()V
aload 3
aload 2
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
iconst_1
ireturn
L1:
aload 2
ldc "com.amazon.venezia.command.DecisionResult"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
invokevirtual com/amazon/venezia/command/c/b()Ljava/lang/String;
astore 2
aload 3
invokevirtual android/os/Parcel/writeNoException()V
aload 3
aload 2
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
iconst_1
ireturn
L2:
aload 2
ldc "com.amazon.venezia.command.DecisionResult"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
invokevirtual com/amazon/venezia/command/c/c()Ljava/lang/String;
astore 2
aload 3
invokevirtual android/os/Parcel/writeNoException()V
aload 3
aload 2
invokevirtual android/os/Parcel/writeString(Ljava/lang/String;)V
iconst_1
ireturn
L3:
aload 2
ldc "com.amazon.venezia.command.DecisionResult"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
invokevirtual com/amazon/venezia/command/c/d()J
lstore 5
aload 3
invokevirtual android/os/Parcel/writeNoException()V
aload 3
lload 5
invokevirtual android/os/Parcel/writeLong(J)V
iconst_1
ireturn
L4:
aload 2
ldc "com.amazon.venezia.command.DecisionResult"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 2
invokevirtual android/os/Parcel/readStrongBinder()Landroid/os/IBinder;
astore 2
aload 2
ifnonnull L11
aconst_null
astore 2
L12:
aload 0
aload 2
invokevirtual com/amazon/venezia/command/c/a(Lcom/amazon/venezia/command/s;)V
aload 3
invokevirtual android/os/Parcel/writeNoException()V
iconst_1
ireturn
L11:
aload 2
ldc "com.amazon.venezia.command.DecisionExpirationContext"
invokeinterface android/os/IBinder/queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface; 1
astore 7
aload 7
ifnull L13
aload 7
instanceof com/amazon/venezia/command/s
ifeq L13
aload 7
checkcast com/amazon/venezia/command/s
astore 2
goto L12
L13:
new com/amazon/venezia/command/u
dup
aload 2
invokespecial com/amazon/venezia/command/u/<init>(Landroid/os/IBinder;)V
astore 2
goto L12
L5:
aload 2
ldc "com.amazon.venezia.command.DecisionResult"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
invokevirtual com/amazon/venezia/command/c/e()Lcom/amazon/venezia/command/n;
astore 2
aload 3
invokevirtual android/os/Parcel/writeNoException()V
aload 2
ifnull L14
aload 2
invokeinterface com/amazon/venezia/command/n/asBinder()Landroid/os/IBinder; 0
astore 2
L15:
aload 3
aload 2
invokevirtual android/os/Parcel/writeStrongBinder(Landroid/os/IBinder;)V
iconst_1
ireturn
L14:
aconst_null
astore 2
goto L15
L6:
aload 2
ldc "com.amazon.venezia.command.DecisionResult"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
invokevirtual com/amazon/venezia/command/c/f()Lcom/amazon/venezia/command/n;
astore 2
aload 3
invokevirtual android/os/Parcel/writeNoException()V
aload 2
ifnull L16
aload 2
invokeinterface com/amazon/venezia/command/n/asBinder()Landroid/os/IBinder; 0
astore 2
L17:
aload 3
aload 2
invokevirtual android/os/Parcel/writeStrongBinder(Landroid/os/IBinder;)V
iconst_1
ireturn
L16:
aconst_null
astore 2
goto L17
L7:
aload 2
ldc "com.amazon.venezia.command.DecisionResult"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
invokevirtual com/amazon/venezia/command/c/g()Lcom/amazon/venezia/command/n;
astore 2
aload 3
invokevirtual android/os/Parcel/writeNoException()V
aload 2
ifnull L18
aload 2
invokeinterface com/amazon/venezia/command/n/asBinder()Landroid/os/IBinder; 0
astore 2
L19:
aload 3
aload 2
invokevirtual android/os/Parcel/writeStrongBinder(Landroid/os/IBinder;)V
iconst_1
ireturn
L18:
aconst_null
astore 2
goto L19
L8:
aload 2
ldc "com.amazon.venezia.command.DecisionResult"
invokevirtual android/os/Parcel/enforceInterface(Ljava/lang/String;)V
aload 0
invokevirtual com/amazon/venezia/command/c/h()Ljava/util/Map;
astore 2
aload 3
invokevirtual android/os/Parcel/writeNoException()V
aload 3
aload 2
invokevirtual android/os/Parcel/writeMap(Ljava/util/Map;)V
iconst_1
ireturn
.limit locals 8
.limit stack 5
.end method
