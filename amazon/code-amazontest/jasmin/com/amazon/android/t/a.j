.bytecode 50.0
.class public final synchronized com/amazon/android/t/a
.super com/amazon/android/framework/task/command/AbstractCommandTask

.field private 'a' Ljava/util/Map;

.method public <init>(Ljava/util/Map;)V
aload 0
invokespecial com/amazon/android/framework/task/command/AbstractCommandTask/<init>()V
aload 0
aload 1
putfield com/amazon/android/t/a/a Ljava/util/Map;
return
.limit locals 2
.limit stack 2
.end method

.method protected final getCommandData()Ljava/util/Map;
aload 0
getfield com/amazon/android/t/a/a Ljava/util/Map;
areturn
.limit locals 1
.limit stack 1
.end method

.method protected final getCommandName()Ljava/lang/String;
ldc "lifeCycle_Events"
areturn
.limit locals 1
.limit stack 1
.end method

.method protected final getCommandVersion()Ljava/lang/String;
ldc "1.0"
areturn
.limit locals 1
.limit stack 1
.end method

.method protected final isExecutionNeeded()Z
iconst_1
ireturn
.limit locals 1
.limit stack 1
.end method

.method protected final onFailure(Lcom/amazon/venezia/command/FailureResult;)V
.throws android/os/RemoteException
.throws com/amazon/android/framework/exception/KiwiException
return
.limit locals 2
.limit stack 0
.end method

.method protected final onSuccess(Lcom/amazon/venezia/command/SuccessResult;)V
.throws android/os/RemoteException
.throws com/amazon/android/framework/exception/KiwiException
return
.limit locals 2
.limit stack 0
.end method
