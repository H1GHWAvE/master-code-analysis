.bytecode 50.0
.class public final synchronized com/amazon/android/u/a
.super java/lang/Object

.field private static final 'a' Lcom/amazon/android/framework/util/KiwiLogger;

.method static <clinit>()V
new com/amazon/android/framework/util/KiwiLogger
dup
ldc "Serializer"
invokespecial com/amazon/android/framework/util/KiwiLogger/<init>(Ljava/lang/String;)V
putstatic com/amazon/android/u/a/a Lcom/amazon/android/framework/util/KiwiLogger;
return
.limit locals 0
.limit stack 3
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/Object;
.catch java/io/IOException from L0 to L1 using L2
.catch java/lang/Exception from L1 to L3 using L4
.catch all from L1 to L3 using L5
.catch java/lang/Exception from L6 to L7 using L8
.catch all from L6 to L7 using L9
.catch all from L10 to L11 using L9
.catch all from L12 to L13 using L9
aload 0
ifnull L14
aload 0
invokevirtual java/lang/String/length()I
ifne L0
L14:
aconst_null
areturn
L0:
aload 0
invokevirtual java/lang/String/getBytes()[B
invokestatic com/amazon/mas/kiwi/util/Base64/decode([B)[B
astore 1
L1:
new java/io/ObjectInputStream
dup
new java/io/ByteArrayInputStream
dup
aload 1
invokespecial java/io/ByteArrayInputStream/<init>([B)V
invokespecial java/io/ObjectInputStream/<init>(Ljava/io/InputStream;)V
astore 2
L3:
aload 2
astore 1
L6:
aload 2
invokevirtual java/io/ObjectInputStream/readObject()Ljava/lang/Object;
astore 3
L7:
aload 2
invokestatic com/amazon/android/framework/util/a/a(Ljava/io/InputStream;)V
aload 3
areturn
L2:
astore 0
getstatic com/amazon/android/framework/util/KiwiLogger/ERROR_ON Z
ifeq L15
getstatic com/amazon/android/u/a/a Lcom/amazon/android/framework/util/KiwiLogger;
ldc "Could not decode string"
aload 0
invokevirtual com/amazon/android/framework/util/KiwiLogger/error(Ljava/lang/String;Ljava/lang/Throwable;)V
L15:
aconst_null
areturn
L4:
astore 3
aconst_null
astore 2
L16:
aload 2
astore 1
L10:
getstatic com/amazon/android/framework/util/KiwiLogger/ERROR_ON Z
ifeq L13
L11:
aload 2
astore 1
L12:
getstatic com/amazon/android/u/a/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Could not read object from string: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 3
invokevirtual com/amazon/android/framework/util/KiwiLogger/error(Ljava/lang/String;Ljava/lang/Throwable;)V
L13:
aload 2
invokestatic com/amazon/android/framework/util/a/a(Ljava/io/InputStream;)V
aconst_null
areturn
L5:
astore 0
aconst_null
astore 1
L17:
aload 1
invokestatic com/amazon/android/framework/util/a/a(Ljava/io/InputStream;)V
aload 0
athrow
L9:
astore 0
goto L17
L8:
astore 3
goto L16
.limit locals 4
.limit stack 5
.end method

.method public static a(Ljava/io/Serializable;)Ljava/lang/String;
.catch java/io/IOException from L0 to L1 using L2
.catch all from L0 to L1 using L3
.catch java/io/IOException from L4 to L5 using L6
.catch all from L4 to L5 using L7
.catch java/io/IOException from L8 to L9 using L6
.catch all from L8 to L9 using L7
.catch all from L10 to L11 using L7
.catch all from L12 to L13 using L7
aload 0
ifnonnull L14
aconst_null
areturn
L14:
new java/io/ByteArrayOutputStream
dup
invokespecial java/io/ByteArrayOutputStream/<init>()V
astore 3
L0:
new java/io/ObjectOutputStream
dup
aload 3
invokespecial java/io/ObjectOutputStream/<init>(Ljava/io/OutputStream;)V
astore 2
L1:
aload 2
astore 1
L4:
aload 2
aload 0
invokevirtual java/io/ObjectOutputStream/writeObject(Ljava/lang/Object;)V
L5:
aload 2
astore 1
L8:
aload 3
invokevirtual java/io/ByteArrayOutputStream/toByteArray()[B
invokestatic com/amazon/mas/kiwi/util/Base64/encodeBytes([B)Ljava/lang/String;
astore 3
L9:
aload 2
invokestatic com/amazon/android/framework/util/a/a(Ljava/io/OutputStream;)V
aload 3
areturn
L2:
astore 3
aconst_null
astore 2
L15:
aload 2
astore 1
L10:
getstatic com/amazon/android/framework/util/KiwiLogger/ERROR_ON Z
ifeq L13
L11:
aload 2
astore 1
L12:
getstatic com/amazon/android/u/a/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Could not serialize object: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 3
invokevirtual com/amazon/android/framework/util/KiwiLogger/error(Ljava/lang/String;Ljava/lang/Throwable;)V
L13:
aload 2
invokestatic com/amazon/android/framework/util/a/a(Ljava/io/OutputStream;)V
aconst_null
areturn
L3:
astore 0
aconst_null
astore 1
L16:
aload 1
invokestatic com/amazon/android/framework/util/a/a(Ljava/io/OutputStream;)V
aload 0
athrow
L7:
astore 0
goto L16
L6:
astore 3
goto L15
.limit locals 4
.limit stack 3
.end method
