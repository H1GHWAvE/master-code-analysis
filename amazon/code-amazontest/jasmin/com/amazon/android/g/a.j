.bytecode 50.0
.class public final synchronized com/amazon/android/g/a
.super java/lang/Object
.implements com/amazon/android/framework/resource/b
.implements com/amazon/android/g/c

.field private static final 'a' Lcom/amazon/android/framework/util/KiwiLogger;

.field private static final 'b' Ljava/util/Comparator;

.field private 'c' Landroid/app/Application;
.annotation visible Lcom/amazon/android/framework/resource/Resource;
.end annotation
.end field

.field private 'd' Ljavax/crypto/spec/SecretKeySpec;

.method static <clinit>()V
new com/amazon/android/framework/util/KiwiLogger
dup
ldc "ObfuscationManagerImpl"
invokespecial com/amazon/android/framework/util/KiwiLogger/<init>(Ljava/lang/String;)V
putstatic com/amazon/android/g/a/a Lcom/amazon/android/framework/util/KiwiLogger;
new com/amazon/android/g/b
dup
invokespecial com/amazon/android/g/b/<init>()V
putstatic com/amazon/android/g/a/b Ljava/util/Comparator;
return
.limit locals 0
.limit stack 3
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private a()Ljavax/crypto/spec/SecretKeySpec;
.catch java/security/NoSuchAlgorithmException from L0 to L1 using L2
.catch java/security/NoSuchProviderException from L0 to L1 using L3
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
astore 1
aload 1
aload 0
getfield com/amazon/android/g/a/c Landroid/app/Application;
invokevirtual android/app/Application/getPackageName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 1
aload 0
getfield com/amazon/android/g/a/c Landroid/app/Application;
invokevirtual android/app/Application/getContentResolver()Landroid/content/ContentResolver;
ldc "android_id"
invokestatic android/provider/Settings$Secure/getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 1
getstatic android/os/Build/FINGERPRINT Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 1
getstatic android/os/Build/BRAND Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 1
getstatic android/os/Build/BOARD Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 1
getstatic android/os/Build/MODEL Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 1
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 1
L0:
invokestatic com/amazon/android/g/a/b()Ljava/security/SecureRandom;
astore 2
ldc "AES"
invokestatic javax/crypto/KeyGenerator/getInstance(Ljava/lang/String;)Ljavax/crypto/KeyGenerator;
astore 3
aload 2
ldc "SHA-256"
ldc "MD5"
aload 1
invokevirtual java/lang/String/getBytes()[B
invokestatic com/amazon/android/g/a/a(Ljava/lang/String;[B)[B
invokestatic com/amazon/android/g/a/a(Ljava/lang/String;[B)[B
invokevirtual java/security/SecureRandom/setSeed([B)V
aload 3
sipush 128
aload 2
invokevirtual javax/crypto/KeyGenerator/init(ILjava/security/SecureRandom;)V
new javax/crypto/spec/SecretKeySpec
dup
aload 3
invokevirtual javax/crypto/KeyGenerator/generateKey()Ljavax/crypto/SecretKey;
invokeinterface javax/crypto/SecretKey/getEncoded()[B 0
ldc "AES"
invokespecial javax/crypto/spec/SecretKeySpec/<init>([BLjava/lang/String;)V
astore 1
L1:
aload 1
areturn
L2:
astore 1
getstatic com/amazon/android/framework/util/KiwiLogger/ERROR_ON Z
ifeq L4
getstatic com/amazon/android/g/a/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Unable to create KeySpec: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 1
invokevirtual com/amazon/android/framework/util/KiwiLogger/error(Ljava/lang/String;Ljava/lang/Throwable;)V
L4:
aconst_null
areturn
L3:
astore 1
getstatic com/amazon/android/framework/util/KiwiLogger/ERROR_ON Z
ifeq L4
getstatic com/amazon/android/g/a/a Lcom/amazon/android/framework/util/KiwiLogger;
ldc "Unable to find appropriate provider: "
aload 1
invokevirtual com/amazon/android/framework/util/KiwiLogger/error(Ljava/lang/String;Ljava/lang/Throwable;)V
goto L4
.limit locals 4
.limit stack 4
.end method

.method private static a(Ljava/lang/String;[B)[B
.catch java/security/NoSuchAlgorithmException from L0 to L1 using L2
L0:
aload 0
invokestatic java/security/MessageDigest/getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
astore 0
L1:
aload 0
aload 1
invokevirtual java/security/MessageDigest/update([B)V
aload 0
invokevirtual java/security/MessageDigest/digest()[B
areturn
L2:
astore 0
getstatic com/amazon/android/framework/util/KiwiLogger/ERROR_ON Z
ifeq L3
getstatic com/amazon/android/g/a/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Failed to create MessageDigest: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 0
invokevirtual com/amazon/android/framework/util/KiwiLogger/error(Ljava/lang/String;Ljava/lang/Throwable;)V
L3:
aconst_null
areturn
.limit locals 2
.limit stack 3
.end method

.method private a([B)[B
.catch java/lang/Exception from L0 to L1 using L2
L0:
ldc "AES"
invokestatic javax/crypto/Cipher/getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;
astore 2
aload 2
iconst_1
aload 0
getfield com/amazon/android/g/a/d Ljavax/crypto/spec/SecretKeySpec;
invokevirtual javax/crypto/Cipher/init(ILjava/security/Key;)V
aload 2
aload 1
invokevirtual javax/crypto/Cipher/doFinal([B)[B
astore 1
L1:
aload 1
areturn
L2:
astore 1
getstatic com/amazon/android/framework/util/KiwiLogger/ERROR_ON Z
ifeq L3
getstatic com/amazon/android/g/a/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Error obfuscating data: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 1
invokevirtual com/amazon/android/framework/util/KiwiLogger/error(Ljava/lang/String;Ljava/lang/Throwable;)V
L3:
aconst_null
areturn
.limit locals 3
.limit stack 3
.end method

.method private static b()Ljava/security/SecureRandom;
.throws java/security/NoSuchAlgorithmException
.throws java/security/NoSuchProviderException
.catch java/security/NoSuchProviderException from L0 to L1 using L2
getstatic android/os/Build$VERSION/SDK_INT I
bipush 17
if_icmplt L3
L0:
ldc "SHA1PRNG"
ldc "Crypto"
invokestatic java/security/SecureRandom/getInstance(Ljava/lang/String;Ljava/lang/String;)Ljava/security/SecureRandom;
astore 2
L1:
aload 2
areturn
L2:
astore 2
ldc "SecureRandom.SHA1PRNG"
invokestatic java/security/Security/getProviders(Ljava/lang/String;)[Ljava/security/Provider;
astore 2
aload 2
getstatic com/amazon/android/g/a/b Ljava/util/Comparator;
invokestatic java/util/Arrays/sort([Ljava/lang/Object;Ljava/util/Comparator;)V
bipush 8
newarray byte
astore 3
aload 3
dup
iconst_0
ldc_w 0
bastore
dup
iconst_1
ldc_w 1
bastore
dup
iconst_2
ldc_w 2
bastore
dup
iconst_3
ldc_w 3
bastore
dup
iconst_4
ldc_w 4
bastore
dup
iconst_5
ldc_w 5
bastore
dup
bipush 6
ldc_w 6
bastore
dup
bipush 7
ldc_w 7
bastore
pop
sipush 128
newarray byte
astore 4
sipush 128
newarray byte
astore 5
aload 2
arraylength
istore 1
iconst_0
istore 0
L4:
iload 0
iload 1
if_icmpge L5
aload 2
iload 0
aaload
astore 6
ldc "SHA1PRNG"
aload 6
invokestatic java/security/SecureRandom/getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljava/security/SecureRandom;
astore 7
aload 7
aload 3
invokevirtual java/security/SecureRandom/setSeed([B)V
aload 7
aload 4
invokevirtual java/security/SecureRandom/nextBytes([B)V
ldc "SHA1PRNG"
aload 6
invokestatic java/security/SecureRandom/getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljava/security/SecureRandom;
astore 7
aload 7
aload 3
invokevirtual java/security/SecureRandom/setSeed([B)V
aload 7
aload 5
invokevirtual java/security/SecureRandom/nextBytes([B)V
aload 4
aload 5
invokestatic java/util/Arrays/equals([B[B)Z
ifeq L6
ldc "SHA1PRNG"
aload 6
invokestatic java/security/SecureRandom/getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljava/security/SecureRandom;
areturn
L6:
iload 0
iconst_1
iadd
istore 0
goto L4
L5:
new java/security/NoSuchProviderException
dup
ldc "No provider with predictable SecureRandom found."
invokespecial java/security/NoSuchProviderException/<init>(Ljava/lang/String;)V
athrow
L3:
ldc "SHA1PRNG"
invokestatic java/security/SecureRandom/getInstance(Ljava/lang/String;)Ljava/security/SecureRandom;
areturn
.limit locals 8
.limit stack 4
.end method

.method private b([B)[B
.catch java/lang/Exception from L0 to L1 using L2
L0:
ldc "AES"
invokestatic javax/crypto/Cipher/getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;
astore 2
aload 2
iconst_2
aload 0
getfield com/amazon/android/g/a/d Ljavax/crypto/spec/SecretKeySpec;
invokevirtual javax/crypto/Cipher/init(ILjava/security/Key;)V
aload 2
aload 1
invokevirtual javax/crypto/Cipher/doFinal([B)[B
astore 1
L1:
aload 1
areturn
L2:
astore 1
getstatic com/amazon/android/framework/util/KiwiLogger/ERROR_ON Z
ifeq L3
getstatic com/amazon/android/g/a/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Error unobfuscating data: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 1
invokevirtual com/amazon/android/framework/util/KiwiLogger/error(Ljava/lang/String;Ljava/lang/Throwable;)V
L3:
aconst_null
areturn
.limit locals 3
.limit stack 3
.end method

.method private static c(Ljava/lang/String;)[B
aload 0
invokevirtual java/lang/String/length()I
istore 2
iload 2
iconst_2
idiv
newarray byte
astore 3
iconst_0
istore 1
L0:
iload 1
iload 2
if_icmpge L1
aload 3
iload 1
iconst_2
idiv
aload 0
iload 1
invokevirtual java/lang/String/charAt(I)C
bipush 16
invokestatic java/lang/Character/digit(CI)I
iconst_4
ishl
aload 0
iload 1
iconst_1
iadd
invokevirtual java/lang/String/charAt(I)C
bipush 16
invokestatic java/lang/Character/digit(CI)I
ior
i2b
bastore
iload 1
iconst_2
iadd
istore 1
goto L0
L1:
aload 3
areturn
.limit locals 4
.limit stack 6
.end method

.method public final a(Ljava/lang/String;)Ljava/lang/String;
.catch java/io/UnsupportedEncodingException from L0 to L1 using L2
.catch java/io/UnsupportedEncodingException from L3 to L4 using L2
.catch java/io/UnsupportedEncodingException from L5 to L6 using L2
.catch java/io/UnsupportedEncodingException from L7 to L8 using L2
aload 1
ifnonnull L9
aconst_null
areturn
L9:
aload 0
getfield com/amazon/android/g/a/d Ljavax/crypto/spec/SecretKeySpec;
ifnonnull L0
aconst_null
areturn
L0:
aload 0
aload 1
ldc "UTF-8"
invokevirtual java/lang/String/getBytes(Ljava/lang/String;)[B
invokespecial com/amazon/android/g/a/a([B)[B
astore 1
new java/lang/StringBuffer
dup
aload 1
arraylength
iconst_2
imul
invokespecial java/lang/StringBuffer/<init>(I)V
astore 4
L1:
iconst_0
istore 2
L3:
iload 2
aload 1
arraylength
if_icmpge L7
L4:
aload 1
iload 2
baload
istore 3
L5:
aload 4
ldc "0123456789ABCDEF"
iload 3
iconst_4
ishr
bipush 15
iand
invokevirtual java/lang/String/charAt(I)C
invokevirtual java/lang/StringBuffer/append(C)Ljava/lang/StringBuffer;
ldc "0123456789ABCDEF"
iload 3
bipush 15
iand
invokevirtual java/lang/String/charAt(I)C
invokevirtual java/lang/StringBuffer/append(C)Ljava/lang/StringBuffer;
pop
L6:
iload 2
iconst_1
iadd
istore 2
goto L3
L7:
aload 4
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
astore 1
L8:
aload 1
areturn
L2:
astore 1
aconst_null
areturn
.limit locals 5
.limit stack 4
.end method

.method public final b(Ljava/lang/String;)Ljava/lang/String;
.catch java/io/UnsupportedEncodingException from L0 to L1 using L2
aload 1
ifnonnull L3
aconst_null
areturn
L3:
aload 0
getfield com/amazon/android/g/a/d Ljavax/crypto/spec/SecretKeySpec;
ifnonnull L0
aconst_null
areturn
L0:
new java/lang/String
dup
aload 0
aload 1
invokestatic com/amazon/android/g/a/c(Ljava/lang/String;)[B
invokespecial com/amazon/android/g/a/b([B)[B
ldc "UTF-8"
invokespecial java/lang/String/<init>([BLjava/lang/String;)V
astore 1
L1:
aload 1
areturn
L2:
astore 1
aconst_null
areturn
.limit locals 2
.limit stack 4
.end method

.method public final onResourcesPopulated()V
aload 0
aload 0
invokespecial com/amazon/android/g/a/a()Ljavax/crypto/spec/SecretKeySpec;
putfield com/amazon/android/g/a/d Ljavax/crypto/spec/SecretKeySpec;
return
.limit locals 1
.limit stack 2
.end method
