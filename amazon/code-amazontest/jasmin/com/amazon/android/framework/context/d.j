.bytecode 50.0
.class public final synchronized com/amazon/android/framework/context/d
.super java/lang/Object
.implements com/amazon/android/framework/context/ContextManager
.implements com/amazon/android/framework/resource/b

.field public static final 'a' Lcom/amazon/android/framework/util/KiwiLogger;

.field private final 'b' Ljava/util/concurrent/atomic/AtomicReference;

.field private final 'c' Lcom/amazon/android/framework/util/c;

.field private final 'd' Lcom/amazon/android/framework/util/c;

.field private final 'e' Lcom/amazon/android/framework/util/c;

.field private final 'f' Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final 'g' Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final 'h' Ljava/util/concurrent/atomic/AtomicBoolean;

.field private 'i' Landroid/app/Application;
.annotation visible Lcom/amazon/android/framework/resource/Resource;
.end annotation
.end field

.field private 'j' Lcom/amazon/android/framework/task/TaskManager;
.annotation visible Lcom/amazon/android/framework/resource/Resource;
.end annotation
.end field

.field private 'k' Lcom/amazon/android/n/g;
.annotation visible Lcom/amazon/android/framework/resource/Resource;
.end annotation
.end field

.field private 'l' Ljava/lang/String;

.field private 'm' Z

.method static <clinit>()V
new com/amazon/android/framework/util/KiwiLogger
dup
ldc "ContextManagerImpl"
invokespecial com/amazon/android/framework/util/KiwiLogger/<init>(Ljava/lang/String;)V
putstatic com/amazon/android/framework/context/d/a Lcom/amazon/android/framework/util/KiwiLogger;
return
.limit locals 0
.limit stack 3
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/util/concurrent/atomic/AtomicReference
dup
invokespecial java/util/concurrent/atomic/AtomicReference/<init>()V
putfield com/amazon/android/framework/context/d/b Ljava/util/concurrent/atomic/AtomicReference;
aload 0
new com/amazon/android/framework/util/c
dup
invokespecial com/amazon/android/framework/util/c/<init>()V
putfield com/amazon/android/framework/context/d/c Lcom/amazon/android/framework/util/c;
aload 0
new com/amazon/android/framework/util/c
dup
invokespecial com/amazon/android/framework/util/c/<init>()V
putfield com/amazon/android/framework/context/d/d Lcom/amazon/android/framework/util/c;
aload 0
new com/amazon/android/framework/util/c
dup
invokespecial com/amazon/android/framework/util/c/<init>()V
putfield com/amazon/android/framework/context/d/e Lcom/amazon/android/framework/util/c;
aload 0
new java/util/concurrent/atomic/AtomicBoolean
dup
iconst_0
invokespecial java/util/concurrent/atomic/AtomicBoolean/<init>(Z)V
putfield com/amazon/android/framework/context/d/f Ljava/util/concurrent/atomic/AtomicBoolean;
aload 0
new java/util/concurrent/atomic/AtomicBoolean
dup
iconst_0
invokespecial java/util/concurrent/atomic/AtomicBoolean/<init>(Z)V
putfield com/amazon/android/framework/context/d/g Ljava/util/concurrent/atomic/AtomicBoolean;
aload 0
new java/util/concurrent/atomic/AtomicBoolean
dup
iconst_0
invokespecial java/util/concurrent/atomic/AtomicBoolean/<init>(Z)V
putfield com/amazon/android/framework/context/d/h Ljava/util/concurrent/atomic/AtomicBoolean;
aload 0
iconst_0
putfield com/amazon/android/framework/context/d/m Z
return
.limit locals 1
.limit stack 4
.end method

.method static synthetic a(Lcom/amazon/android/framework/context/d;)V
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L0
getstatic com/amazon/android/framework/context/d/a Lcom/amazon/android/framework/util/KiwiLogger;
ldc "----------- EXECUTING FINISH ACTIVITIES -----------"
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
getstatic com/amazon/android/framework/context/d/a Lcom/amazon/android/framework/util/KiwiLogger;
aload 0
getfield com/amazon/android/framework/context/d/i Landroid/app/Application;
invokevirtual android/app/Application/getPackageName()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
getstatic com/amazon/android/framework/context/d/a Lcom/amazon/android/framework/util/KiwiLogger;
ldc "---------------------------------------------------"
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L0:
aload 0
getfield com/amazon/android/framework/context/d/c Lcom/amazon/android/framework/util/c;
invokevirtual com/amazon/android/framework/util/c/iterator()Ljava/util/Iterator;
astore 1
L1:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L2
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast android/app/Activity
astore 2
aload 2
invokestatic com/amazon/android/framework/context/d/a(Landroid/app/Activity;)Z
ifne L1
aload 2
ifnull L1
aload 2
invokevirtual android/app/Activity/isChild()Z
ifeq L3
getstatic com/amazon/android/framework/context/d/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Not finishing activity: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc ", it is a child of: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual android/app/Activity/getParent()Landroid/app/Activity;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
goto L1
L3:
getstatic com/amazon/android/framework/context/d/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Finishing Activity: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
aload 2
invokevirtual android/app/Activity/finish()V
goto L1
L2:
aload 0
invokevirtual com/amazon/android/framework/context/d/getRoot()Landroid/app/Activity;
astore 1
aload 1
ifnonnull L4
getstatic com/amazon/android/framework/context/d/a Lcom/amazon/android/framework/util/KiwiLogger;
ldc "Shutdown found no root, no activities to pop off stack!"
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L5:
new com/amazon/android/framework/context/a
dup
aload 0
invokespecial com/amazon/android/framework/context/a/<init>(Lcom/amazon/android/framework/context/d;)V
astore 1
aload 0
getfield com/amazon/android/framework/context/d/j Lcom/amazon/android/framework/task/TaskManager;
getstatic com/amazon/android/framework/task/pipeline/TaskPipelineId/FOREGROUND Lcom/amazon/android/framework/task/pipeline/TaskPipelineId;
aload 1
invokeinterface com/amazon/android/framework/task/TaskManager/enqueue(Lcom/amazon/android/framework/task/pipeline/TaskPipelineId;Lcom/amazon/android/framework/task/Task;)V 2
return
L4:
getstatic com/amazon/android/framework/context/d/a Lcom/amazon/android/framework/util/KiwiLogger;
ldc "Moving task to background"
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
aload 1
iconst_1
invokevirtual android/app/Activity/moveTaskToBack(Z)Z
pop
getstatic com/amazon/android/framework/context/d/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Popping activity stack, root: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
new android/content/Intent
dup
aload 1
aload 1
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokespecial android/content/Intent/<init>(Landroid/content/Context;Ljava/lang/Class;)V
astore 2
aload 2
ldc_w 67108864
invokevirtual android/content/Intent/addFlags(I)Landroid/content/Intent;
pop
aload 2
ldc_w 536870912
invokevirtual android/content/Intent/addFlags(I)Landroid/content/Intent;
pop
aload 1
aload 2
invokevirtual android/app/Activity/startActivity(Landroid/content/Intent;)V
goto L5
.limit locals 3
.limit stack 4
.end method

.method static synthetic a(Lcom/amazon/android/framework/context/d;Landroid/content/Intent;)V
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L0
getstatic com/amazon/android/framework/context/d/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Received broadcast intent: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L0:
aload 1
invokevirtual android/content/Intent/getAction()Ljava/lang/String;
aload 0
getfield com/amazon/android/framework/context/d/l Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L1
getstatic com/amazon/android/framework/util/KiwiLogger/ERROR_ON Z
ifeq L2
getstatic com/amazon/android/framework/context/d/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Received broadcast for unrequested action: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual android/content/Intent/getAction()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/error(Ljava/lang/String;)V
L2:
return
L1:
aload 0
getfield com/amazon/android/framework/context/d/g Ljava/util/concurrent/atomic/AtomicBoolean;
invokevirtual java/util/concurrent/atomic/AtomicBoolean/get()Z
ifne L3
getstatic com/amazon/android/framework/util/KiwiLogger/ERROR_ON Z
ifeq L2
getstatic com/amazon/android/framework/context/d/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Received intent to shutdown app when we are not in shutdown state: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/error(Ljava/lang/String;)V
return
L3:
aload 0
getfield com/amazon/android/framework/context/d/h Ljava/util/concurrent/atomic/AtomicBoolean;
iconst_0
iconst_1
invokevirtual java/util/concurrent/atomic/AtomicBoolean/compareAndSet(ZZ)Z
ifne L4
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L2
getstatic com/amazon/android/framework/context/d/a Lcom/amazon/android/framework/util/KiwiLogger;
ldc "shutdown broadcast already received, ignoring"
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
return
L4:
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L5
getstatic com/amazon/android/framework/context/d/a Lcom/amazon/android/framework/util/KiwiLogger;
ldc "Stopping services in response to broadcast"
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
getstatic com/amazon/android/framework/context/d/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Service to stop: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/amazon/android/framework/context/d/d Lcom/amazon/android/framework/util/c;
getfield com/amazon/android/framework/util/c/a Ljava/util/WeakHashMap;
invokevirtual java/util/WeakHashMap/size()I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L5:
aload 0
getfield com/amazon/android/framework/context/d/d Lcom/amazon/android/framework/util/c;
invokevirtual com/amazon/android/framework/util/c/iterator()Ljava/util/Iterator;
astore 0
L6:
aload 0
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L2
aload 0
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast android/app/Service
astore 1
aload 1
ifnull L6
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L7
getstatic com/amazon/android/framework/context/d/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Stopping service: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L7:
aload 1
invokevirtual android/app/Service/stopSelf()V
goto L6
.limit locals 2
.limit stack 3
.end method

.method private a(Lcom/amazon/android/j/c;Landroid/app/Activity;)V
new com/amazon/android/j/b
dup
aload 1
aload 2
invokespecial com/amazon/android/j/b/<init>(Lcom/amazon/android/j/c;Landroid/app/Activity;)V
astore 1
aload 0
getfield com/amazon/android/framework/context/d/k Lcom/amazon/android/n/g;
aload 1
invokeinterface com/amazon/android/n/g/a(Lcom/amazon/android/n/d;)V 1
return
.limit locals 3
.limit stack 4
.end method

.method private a(Lcom/amazon/android/j/d;)V
new com/amazon/android/j/a
dup
aload 1
aload 0
getfield com/amazon/android/framework/context/d/i Landroid/app/Application;
invokespecial com/amazon/android/j/a/<init>(Lcom/amazon/android/j/d;Landroid/app/Application;)V
astore 1
aload 0
getfield com/amazon/android/framework/context/d/k Lcom/amazon/android/n/g;
aload 1
invokeinterface com/amazon/android/n/g/a(Lcom/amazon/android/n/d;)V 1
return
.limit locals 2
.limit stack 4
.end method

.method private static a(Landroid/app/Activity;)Z
aload 0
ifnonnull L0
iconst_0
ireturn
L0:
aload 0
invokevirtual android/app/Activity/isTaskRoot()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static b(Landroid/app/Activity;)Landroid/app/Activity;
L0:
aload 0
invokevirtual android/app/Activity/isChild()Z
ifeq L1
aload 0
invokevirtual android/app/Activity/getParent()Landroid/app/Activity;
astore 0
goto L0
L1:
aload 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public final finishActivities()V
aload 0
getfield com/amazon/android/framework/context/d/f Ljava/util/concurrent/atomic/AtomicBoolean;
iconst_0
iconst_1
invokevirtual java/util/concurrent/atomic/AtomicBoolean/compareAndSet(ZZ)Z
ifne L0
return
L0:
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L1
getstatic com/amazon/android/framework/context/d/a Lcom/amazon/android/framework/util/KiwiLogger;
ldc "---------- SCHEDULING FINISH ACTIVITIES -----------"
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
getstatic com/amazon/android/framework/context/d/a Lcom/amazon/android/framework/util/KiwiLogger;
aload 0
getfield com/amazon/android/framework/context/d/i Landroid/app/Application;
invokevirtual android/app/Application/getPackageName()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
getstatic com/amazon/android/framework/context/d/a Lcom/amazon/android/framework/util/KiwiLogger;
invokestatic java/lang/Thread/currentThread()Ljava/lang/Thread;
invokevirtual java/lang/Thread/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
getstatic com/amazon/android/framework/context/d/a Lcom/amazon/android/framework/util/KiwiLogger;
ldc "---------------------------------------------------"
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L1:
new com/amazon/android/framework/context/c
dup
aload 0
invokespecial com/amazon/android/framework/context/c/<init>(Lcom/amazon/android/framework/context/d;)V
astore 1
aload 0
getfield com/amazon/android/framework/context/d/j Lcom/amazon/android/framework/task/TaskManager;
getstatic com/amazon/android/framework/task/pipeline/TaskPipelineId/FOREGROUND Lcom/amazon/android/framework/task/pipeline/TaskPipelineId;
aload 1
invokeinterface com/amazon/android/framework/task/TaskManager/enqueue(Lcom/amazon/android/framework/task/pipeline/TaskPipelineId;Lcom/amazon/android/framework/task/Task;)V 2
return
.limit locals 2
.limit stack 3
.end method

.method public final getRoot()Landroid/app/Activity;
invokestatic com/amazon/android/d/a/a()V
aload 0
getfield com/amazon/android/framework/context/d/c Lcom/amazon/android/framework/util/c;
invokevirtual com/amazon/android/framework/util/c/iterator()Ljava/util/Iterator;
astore 1
L0:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast android/app/Activity
astore 2
aload 2
invokestatic com/amazon/android/framework/context/d/a(Landroid/app/Activity;)Z
ifeq L0
aload 2
areturn
L1:
aconst_null
areturn
.limit locals 3
.limit stack 1
.end method

.method public final getVisible()Landroid/app/Activity;
invokestatic com/amazon/android/d/a/a()V
aload 0
getfield com/amazon/android/framework/context/d/b Ljava/util/concurrent/atomic/AtomicReference;
invokevirtual java/util/concurrent/atomic/AtomicReference/get()Ljava/lang/Object;
checkcast android/app/Activity
areturn
.limit locals 1
.limit stack 1
.end method

.method public final hasAppShutdownBeenRequested()Z
aload 0
getfield com/amazon/android/framework/context/d/g Ljava/util/concurrent/atomic/AtomicBoolean;
invokevirtual java/util/concurrent/atomic/AtomicBoolean/get()Z
ifne L0
aload 0
getfield com/amazon/android/framework/context/d/f Ljava/util/concurrent/atomic/AtomicBoolean;
invokevirtual java/util/concurrent/atomic/AtomicBoolean/get()Z
ifeq L1
L0:
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final isVisible()Z
aload 0
getfield com/amazon/android/framework/context/d/b Ljava/util/concurrent/atomic/AtomicReference;
invokevirtual java/util/concurrent/atomic/AtomicReference/get()Ljava/lang/Object;
ifnull L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final onCreate(Landroid/app/Activity;)V
aload 1
ldc "activity"
invokestatic com/amazon/android/d/a/a(Ljava/lang/Object;Ljava/lang/String;)V
invokestatic com/amazon/android/d/a/a()V
aload 0
getfield com/amazon/android/framework/context/d/c Lcom/amazon/android/framework/util/c;
aload 1
invokevirtual com/amazon/android/framework/util/c/a(Ljava/lang/Object;)V
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L0
getstatic com/amazon/android/framework/context/d/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Activity->onCreate.  Activity: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc ", Total Activities: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/amazon/android/framework/context/d/c Lcom/amazon/android/framework/util/c;
getfield com/amazon/android/framework/util/c/a Ljava/util/WeakHashMap;
invokevirtual java/util/WeakHashMap/size()I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L0:
aload 0
getstatic com/amazon/android/j/c/a Lcom/amazon/android/j/c;
aload 1
invokespecial com/amazon/android/framework/context/d/a(Lcom/amazon/android/j/c;Landroid/app/Activity;)V
aload 0
getfield com/amazon/android/framework/context/d/m Z
ifne L1
aload 0
getfield com/amazon/android/framework/context/d/c Lcom/amazon/android/framework/util/c;
getfield com/amazon/android/framework/util/c/a Ljava/util/WeakHashMap;
invokevirtual java/util/WeakHashMap/size()I
iconst_1
if_icmpne L1
iconst_1
istore 2
L2:
iload 2
ifeq L3
aload 0
iconst_1
putfield com/amazon/android/framework/context/d/m Z
aload 0
getstatic com/amazon/android/j/d/a Lcom/amazon/android/j/d;
invokespecial com/amazon/android/framework/context/d/a(Lcom/amazon/android/j/d;)V
L3:
return
L1:
iconst_0
istore 2
goto L2
.limit locals 3
.limit stack 3
.end method

.method public final onCreate(Landroid/app/Service;)V
invokestatic com/amazon/android/d/a/a()V
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L0
getstatic com/amazon/android/framework/context/d/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Service->onCreate: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L0:
aload 0
getfield com/amazon/android/framework/context/d/d Lcom/amazon/android/framework/util/c;
aload 1
invokevirtual com/amazon/android/framework/util/c/a(Ljava/lang/Object;)V
return
.limit locals 2
.limit stack 3
.end method

.method public final onDestroy(Landroid/app/Activity;)V
aload 1
ldc "activity"
invokestatic com/amazon/android/d/a/a(Ljava/lang/Object;Ljava/lang/String;)V
invokestatic com/amazon/android/d/a/a()V
aload 0
getfield com/amazon/android/framework/context/d/c Lcom/amazon/android/framework/util/c;
aload 1
invokevirtual com/amazon/android/framework/util/c/b(Ljava/lang/Object;)V
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L0
getstatic com/amazon/android/framework/context/d/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Activity->onDestroy.  Activity: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc ", Total Activities: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/amazon/android/framework/context/d/c Lcom/amazon/android/framework/util/c;
getfield com/amazon/android/framework/util/c/a Ljava/util/WeakHashMap;
invokevirtual java/util/WeakHashMap/size()I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L0:
aload 0
getstatic com/amazon/android/j/c/b Lcom/amazon/android/j/c;
aload 1
invokespecial com/amazon/android/framework/context/d/a(Lcom/amazon/android/j/c;Landroid/app/Activity;)V
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L1
getstatic com/amazon/android/framework/context/d/a Lcom/amazon/android/framework/util/KiwiLogger;
ldc "Checking if application is destroyed"
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L1:
aload 0
getfield com/amazon/android/framework/context/d/c Lcom/amazon/android/framework/util/c;
getfield com/amazon/android/framework/util/c/a Ljava/util/WeakHashMap;
invokevirtual java/util/WeakHashMap/isEmpty()Z
ifeq L2
getstatic com/amazon/android/framework/context/d/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "App is destroyed: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual android/app/Activity/isTaskRoot()Z
invokevirtual java/lang/StringBuilder/append(Z)Ljava/lang/StringBuilder;
ldc ", "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual android/app/Activity/isFinishing()Z
invokevirtual java/lang/StringBuilder/append(Z)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
aload 1
invokevirtual android/app/Activity/isTaskRoot()Z
ifeq L2
aload 1
invokevirtual android/app/Activity/isFinishing()Z
ifeq L2
iconst_1
istore 2
L3:
iload 2
ifeq L4
aload 0
iconst_0
putfield com/amazon/android/framework/context/d/m Z
aload 0
getstatic com/amazon/android/j/d/b Lcom/amazon/android/j/d;
invokespecial com/amazon/android/framework/context/d/a(Lcom/amazon/android/j/d;)V
L4:
return
L2:
iconst_0
istore 2
goto L3
.limit locals 3
.limit stack 3
.end method

.method public final onDestroy(Landroid/app/Service;)V
invokestatic com/amazon/android/d/a/a()V
aload 0
getfield com/amazon/android/framework/context/d/d Lcom/amazon/android/framework/util/c;
aload 1
invokevirtual com/amazon/android/framework/util/c/b(Ljava/lang/Object;)V
return
.limit locals 2
.limit stack 2
.end method

.method public final onPause(Landroid/app/Activity;)V
invokestatic com/amazon/android/d/a/a()V
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L0
getstatic com/amazon/android/framework/context/d/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Activity paused: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc ", visible activity: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual com/amazon/android/framework/context/d/getVisible()Landroid/app/Activity;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L0:
aload 0
invokevirtual com/amazon/android/framework/context/d/getVisible()Landroid/app/Activity;
aload 1
if_acmpne L1
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L2
getstatic com/amazon/android/framework/context/d/a Lcom/amazon/android/framework/util/KiwiLogger;
ldc "Setting visible activity to null"
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L2:
aload 0
getfield com/amazon/android/framework/context/d/b Ljava/util/concurrent/atomic/AtomicReference;
aconst_null
invokevirtual java/util/concurrent/atomic/AtomicReference/set(Ljava/lang/Object;)V
aload 0
getstatic com/amazon/android/j/c/d Lcom/amazon/android/j/c;
aload 1
invokespecial com/amazon/android/framework/context/d/a(Lcom/amazon/android/j/c;Landroid/app/Activity;)V
L1:
return
.limit locals 2
.limit stack 3
.end method

.method public final onResourcesPopulated()V
aload 0
getfield com/amazon/android/framework/context/d/i Landroid/app/Application;
invokevirtual android/app/Application/getPackageName()Ljava/lang/String;
astore 1
aload 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "com.amazon."
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".shutdown"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
putfield com/amazon/android/framework/context/d/l Ljava/lang/String;
new com/amazon/android/framework/context/b
dup
aload 0
invokespecial com/amazon/android/framework/context/b/<init>(Lcom/amazon/android/framework/context/d;)V
astore 1
new android/content/IntentFilter
dup
aload 0
getfield com/amazon/android/framework/context/d/l Ljava/lang/String;
invokespecial android/content/IntentFilter/<init>(Ljava/lang/String;)V
astore 2
aload 0
getfield com/amazon/android/framework/context/d/i Landroid/app/Application;
aload 1
aload 2
invokevirtual android/app/Application/registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
pop
return
.limit locals 3
.limit stack 3
.end method

.method public final onResume(Landroid/app/Activity;)V
invokestatic com/amazon/android/d/a/a()V
getstatic com/amazon/android/framework/context/d/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Activity resumed: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc ", is child: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual android/app/Activity/isChild()Z
invokevirtual java/lang/StringBuilder/append(Z)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
aload 1
invokestatic com/amazon/android/framework/context/d/b(Landroid/app/Activity;)Landroid/app/Activity;
astore 2
getstatic com/amazon/android/framework/context/d/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Setting visible to: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
aload 0
getfield com/amazon/android/framework/context/d/b Ljava/util/concurrent/atomic/AtomicReference;
aload 2
invokevirtual java/util/concurrent/atomic/AtomicReference/set(Ljava/lang/Object;)V
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L0
getstatic com/amazon/android/framework/context/d/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Activity now visible: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc ", "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "publishing resume event"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L0:
aload 0
getstatic com/amazon/android/j/c/c Lcom/amazon/android/j/c;
aload 1
invokespecial com/amazon/android/framework/context/d/a(Lcom/amazon/android/j/c;Landroid/app/Activity;)V
return
.limit locals 3
.limit stack 3
.end method

.method public final onStart(Landroid/app/Activity;)V
aload 1
ldc "activity"
invokestatic com/amazon/android/d/a/a(Ljava/lang/Object;Ljava/lang/String;)V
invokestatic com/amazon/android/d/a/a()V
getstatic com/amazon/android/framework/context/d/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Activity started: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
aload 0
getfield com/amazon/android/framework/context/d/e Lcom/amazon/android/framework/util/c;
aload 1
invokevirtual com/amazon/android/framework/util/c/a(Ljava/lang/Object;)V
aload 0
getstatic com/amazon/android/j/c/e Lcom/amazon/android/j/c;
aload 1
invokespecial com/amazon/android/framework/context/d/a(Lcom/amazon/android/j/c;Landroid/app/Activity;)V
aload 0
getfield com/amazon/android/framework/context/d/e Lcom/amazon/android/framework/util/c;
getfield com/amazon/android/framework/util/c/a Ljava/util/WeakHashMap;
invokevirtual java/util/WeakHashMap/size()I
iconst_1
if_icmpne L0
aload 0
getstatic com/amazon/android/j/d/c Lcom/amazon/android/j/d;
invokespecial com/amazon/android/framework/context/d/a(Lcom/amazon/android/j/d;)V
L0:
return
.limit locals 2
.limit stack 3
.end method

.method public final onStop(Landroid/app/Activity;)V
aload 1
ldc "activity"
invokestatic com/amazon/android/d/a/a(Ljava/lang/Object;Ljava/lang/String;)V
invokestatic com/amazon/android/d/a/a()V
getstatic com/amazon/android/framework/context/d/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Activity stopped: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
aload 0
getfield com/amazon/android/framework/context/d/e Lcom/amazon/android/framework/util/c;
aload 1
invokevirtual com/amazon/android/framework/util/c/b(Ljava/lang/Object;)V
aload 0
getstatic com/amazon/android/j/c/f Lcom/amazon/android/j/c;
aload 1
invokespecial com/amazon/android/framework/context/d/a(Lcom/amazon/android/j/c;Landroid/app/Activity;)V
aload 0
getfield com/amazon/android/framework/context/d/e Lcom/amazon/android/framework/util/c;
getfield com/amazon/android/framework/util/c/a Ljava/util/WeakHashMap;
invokevirtual java/util/WeakHashMap/isEmpty()Z
ifeq L0
aload 0
getstatic com/amazon/android/j/d/d Lcom/amazon/android/j/d;
invokespecial com/amazon/android/framework/context/d/a(Lcom/amazon/android/j/d;)V
L0:
return
.limit locals 2
.limit stack 3
.end method

.method public final stopServices()V
aload 0
getfield com/amazon/android/framework/context/d/g Ljava/util/concurrent/atomic/AtomicBoolean;
iconst_0
iconst_1
invokevirtual java/util/concurrent/atomic/AtomicBoolean/compareAndSet(ZZ)Z
ifne L0
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L0
getstatic com/amazon/android/framework/context/d/a Lcom/amazon/android/framework/util/KiwiLogger;
ldc "Ignoring duplicate stopServices request"
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L0:
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L1
getstatic com/amazon/android/framework/context/d/a Lcom/amazon/android/framework/util/KiwiLogger;
ldc "------------- STOPPING SERVICES -------------------"
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
getstatic com/amazon/android/framework/context/d/a Lcom/amazon/android/framework/util/KiwiLogger;
aload 0
getfield com/amazon/android/framework/context/d/i Landroid/app/Application;
invokevirtual android/app/Application/getPackageName()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
getstatic com/amazon/android/framework/context/d/a Lcom/amazon/android/framework/util/KiwiLogger;
ldc "---------------------------------------------------"
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L1:
new android/content/Intent
dup
aload 0
getfield com/amazon/android/framework/context/d/l Ljava/lang/String;
invokespecial android/content/Intent/<init>(Ljava/lang/String;)V
astore 1
aload 1
aload 0
getfield com/amazon/android/framework/context/d/i Landroid/app/Application;
invokevirtual android/app/Application/getPackageName()Ljava/lang/String;
invokevirtual android/content/Intent/setPackage(Ljava/lang/String;)Landroid/content/Intent;
pop
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L2
getstatic com/amazon/android/framework/context/d/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Sending Broadcast!!!!: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc ", Thread: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokestatic java/lang/Thread/currentThread()Ljava/lang/Thread;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L2:
aload 0
getfield com/amazon/android/framework/context/d/i Landroid/app/Application;
aload 1
invokevirtual android/app/Application/sendBroadcast(Landroid/content/Intent;)V
return
.limit locals 2
.limit stack 3
.end method
