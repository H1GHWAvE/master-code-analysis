.bytecode 50.0
.class final synchronized com/amazon/android/framework/context/a
.super java/lang/Object
.implements com/amazon/android/framework/task/Task

.field private synthetic 'a' Lcom/amazon/android/framework/context/d;

.method <init>(Lcom/amazon/android/framework/context/d;)V
aload 0
aload 1
putfield com/amazon/android/framework/context/a/a Lcom/amazon/android/framework/context/d;
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 2
.limit stack 2
.end method

.method public final execute()V
aload 0
getfield com/amazon/android/framework/context/a/a Lcom/amazon/android/framework/context/d;
invokevirtual com/amazon/android/framework/context/d/getRoot()Landroid/app/Activity;
astore 1
getstatic com/amazon/android/framework/context/d/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Finishing Root Task: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
aload 1
ifnull L0
getstatic com/amazon/android/framework/context/d/a Lcom/amazon/android/framework/util/KiwiLogger;
ldc "Finishing Root"
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
aload 1
invokevirtual android/app/Activity/finish()V
L0:
return
.limit locals 2
.limit stack 3
.end method

.method public final toString()Ljava/lang/String;
ldc "ContextManager: kill root task"
areturn
.limit locals 1
.limit stack 1
.end method
