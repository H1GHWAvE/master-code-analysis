.bytecode 50.0
.class public synchronized com/amazon/android/framework/util/KiwiLogger
.super java/lang/Object

.field public static 'ERROR_ON' Z = 0


.field private static final 'TAG' Ljava/lang/String; = "Kiwi"

.field private static 'TEST_ON' Z

.field public static 'TRACE_ON' Z

.field private 'componentName' Ljava/lang/String;

.method static <clinit>()V
iconst_1
putstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
iconst_1
putstatic com/amazon/android/framework/util/KiwiLogger/ERROR_ON Z
iconst_0
putstatic com/amazon/android/framework/util/KiwiLogger/TEST_ON Z
return
.limit locals 0
.limit stack 1
.end method

.method public <init>(Ljava/lang/String;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield com/amazon/android/framework/util/KiwiLogger/componentName Ljava/lang/String;
return
.limit locals 2
.limit stack 2
.end method

.method public static enableTest()V
iconst_1
putstatic com/amazon/android/framework/util/KiwiLogger/TEST_ON Z
return
.limit locals 0
.limit stack 1
.end method

.method private getComponentMessage(Ljava/lang/String;)Ljava/lang/String;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
getfield com/amazon/android/framework/util/KiwiLogger/componentName Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ": "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 2
.end method

.method public static isTestEnabled()Z
getstatic com/amazon/android/framework/util/KiwiLogger/TEST_ON Z
ireturn
.limit locals 0
.limit stack 1
.end method

.method public error(Ljava/lang/String;)V
getstatic com/amazon/android/framework/util/KiwiLogger/ERROR_ON Z
ifeq L0
ldc "Kiwi"
aload 0
aload 1
invokespecial com/amazon/android/framework/util/KiwiLogger/getComponentMessage(Ljava/lang/String;)Ljava/lang/String;
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
L0:
return
.limit locals 2
.limit stack 3
.end method

.method public error(Ljava/lang/String;Ljava/lang/Throwable;)V
getstatic com/amazon/android/framework/util/KiwiLogger/ERROR_ON Z
ifeq L0
ldc "Kiwi"
aload 0
aload 1
invokespecial com/amazon/android/framework/util/KiwiLogger/getComponentMessage(Ljava/lang/String;)Ljava/lang/String;
aload 2
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
L0:
return
.limit locals 3
.limit stack 3
.end method

.method public test(Ljava/lang/String;)V
getstatic com/amazon/android/framework/util/KiwiLogger/TEST_ON Z
ifeq L0
ldc "Kiwi"
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "TEST-"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
aload 1
invokespecial com/amazon/android/framework/util/KiwiLogger/getComponentMessage(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
L0:
return
.limit locals 2
.limit stack 4
.end method

.method public trace(Ljava/lang/String;)V
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L0
ldc "Kiwi"
aload 0
aload 1
invokespecial com/amazon/android/framework/util/KiwiLogger/getComponentMessage(Ljava/lang/String;)Ljava/lang/String;
invokestatic android/util/Log/d(Ljava/lang/String;Ljava/lang/String;)I
pop
L0:
return
.limit locals 2
.limit stack 3
.end method

.method public trace(Ljava/lang/String;Ljava/lang/Throwable;)V
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L0
ldc "Kiwi"
aload 0
aload 1
invokespecial com/amazon/android/framework/util/KiwiLogger/getComponentMessage(Ljava/lang/String;)Ljava/lang/String;
aload 2
invokestatic android/util/Log/d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
L0:
return
.limit locals 3
.limit stack 3
.end method
