.bytecode 50.0
.class public final synchronized com/amazon/android/framework/util/d
.super java/lang/Object

.field private static final 'a' Lcom/amazon/android/framework/util/KiwiLogger;

.method static <clinit>()V
new com/amazon/android/framework/util/KiwiLogger
dup
ldc "Reflection"
invokespecial com/amazon/android/framework/util/KiwiLogger/<init>(Ljava/lang/String;)V
putstatic com/amazon/android/framework/util/d/a Lcom/amazon/android/framework/util/KiwiLogger;
return
.limit locals 0
.limit stack 3
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/Class;
.catch java/lang/ClassNotFoundException from L0 to L1 using L2
aload 0
ldc "String className"
invokestatic com/amazon/android/d/a/a(Ljava/lang/Object;Ljava/lang/String;)V
invokestatic java/lang/Thread/currentThread()Ljava/lang/Thread;
invokevirtual java/lang/Thread/getContextClassLoader()Ljava/lang/ClassLoader;
astore 1
L0:
aload 1
aload 0
invokevirtual java/lang/ClassLoader/loadClass(Ljava/lang/String;)Ljava/lang/Class;
astore 1
L1:
aload 1
areturn
L2:
astore 1
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L3
getstatic com/amazon/android/framework/util/d/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "ClassNoFound: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L3:
aconst_null
areturn
.limit locals 2
.limit stack 3
.end method

.method public static a(Ljava/lang/reflect/Method;)Z
aload 0
ldc "Method m"
invokestatic com/amazon/android/d/a/a(Ljava/lang/Object;Ljava/lang/String;)V
aload 0
invokevirtual java/lang/reflect/Method/getParameterTypes()[Ljava/lang/Class;
arraylength
ifle L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method public static b(Ljava/lang/reflect/Method;)Z
aload 0
ldc "Method m"
invokestatic com/amazon/android/d/a/a(Ljava/lang/Object;Ljava/lang/String;)V
aload 0
invokevirtual java/lang/reflect/Method/getReturnType()Ljava/lang/Class;
getstatic java/lang/Void/TYPE Ljava/lang/Class;
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ireturn
.limit locals 1
.limit stack 2
.end method

.method public static c(Ljava/lang/reflect/Method;)Z
aload 0
ldc "Method m"
invokestatic com/amazon/android/d/a/a(Ljava/lang/Object;Ljava/lang/String;)V
aload 0
invokevirtual java/lang/reflect/Method/getModifiers()I
bipush 8
iand
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method
