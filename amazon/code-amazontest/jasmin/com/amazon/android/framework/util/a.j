.bytecode 50.0
.class public final synchronized com/amazon/android/framework/util/a
.super java/lang/Object

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static final a(Ljava/io/InputStream;)V
.catch java/io/IOException from L0 to L1 using L2
aload 0
ifnull L1
L0:
aload 0
invokevirtual java/io/InputStream/close()V
L1:
return
L2:
astore 0
return
.limit locals 1
.limit stack 1
.end method

.method public static final a(Ljava/io/OutputStream;)V
.catch java/io/IOException from L0 to L1 using L2
aload 0
ifnull L1
L0:
aload 0
invokevirtual java/io/OutputStream/close()V
L1:
return
L2:
astore 0
return
.limit locals 1
.limit stack 1
.end method

.method public static final a(Ljava/io/Reader;)V
.catch java/io/IOException from L0 to L1 using L2
aload 0
ifnull L1
L0:
aload 0
invokevirtual java/io/Reader/close()V
L1:
return
L2:
astore 0
return
.limit locals 1
.limit stack 1
.end method
