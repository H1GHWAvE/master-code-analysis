.bytecode 50.0
.class public final synchronized com/amazon/android/framework/util/c
.super java/lang/Object
.implements java/lang/Iterable

.field public 'a' Ljava/util/WeakHashMap;

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/util/WeakHashMap
dup
invokespecial java/util/WeakHashMap/<init>()V
putfield com/amazon/android/framework/util/c/a Ljava/util/WeakHashMap;
return
.limit locals 1
.limit stack 3
.end method

.method public final a(Ljava/lang/Object;)V
aload 0
getfield com/amazon/android/framework/util/c/a Ljava/util/WeakHashMap;
aload 1
aconst_null
invokevirtual java/util/WeakHashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
return
.limit locals 2
.limit stack 3
.end method

.method public final b(Ljava/lang/Object;)V
aload 0
getfield com/amazon/android/framework/util/c/a Ljava/util/WeakHashMap;
aload 1
invokevirtual java/util/WeakHashMap/remove(Ljava/lang/Object;)Ljava/lang/Object;
pop
return
.limit locals 2
.limit stack 2
.end method

.method public final iterator()Ljava/util/Iterator;
aload 0
getfield com/amazon/android/framework/util/c/a Ljava/util/WeakHashMap;
invokevirtual java/util/WeakHashMap/keySet()Ljava/util/Set;
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public final toString()Ljava/lang/String;
aload 0
getfield com/amazon/android/framework/util/c/a Ljava/util/WeakHashMap;
invokevirtual java/util/WeakHashMap/keySet()Ljava/util/Set;
invokevirtual java/lang/Object/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method
