.bytecode 50.0
.class public final synchronized com/amazon/android/framework/prompt/c
.super com/amazon/android/framework/prompt/SimplePrompt
.implements com/amazon/android/framework/resource/b

.field private static final 'a' Lcom/amazon/android/framework/util/KiwiLogger;

.field private 'b' Lcom/amazon/android/framework/context/ContextManager;
.annotation visible Lcom/amazon/android/framework/resource/Resource;
.end annotation
.end field

.method static <clinit>()V
new com/amazon/android/framework/util/KiwiLogger
dup
ldc "ShutdownPrompt"
invokespecial com/amazon/android/framework/util/KiwiLogger/<init>(Ljava/lang/String;)V
putstatic com/amazon/android/framework/prompt/c/a Lcom/amazon/android/framework/util/KiwiLogger;
return
.limit locals 0
.limit stack 3
.end method

.method public <init>(Lcom/amazon/android/framework/prompt/PromptContent;)V
aload 0
aload 1
invokespecial com/amazon/android/framework/prompt/SimplePrompt/<init>(Lcom/amazon/android/framework/prompt/PromptContent;)V
return
.limit locals 2
.limit stack 2
.end method

.method protected final doAction()V
aload 0
getfield com/amazon/android/framework/prompt/c/b Lcom/amazon/android/framework/context/ContextManager;
invokeinterface com/amazon/android/framework/context/ContextManager/finishActivities()V 0
getstatic com/amazon/android/framework/prompt/c/a Lcom/amazon/android/framework/util/KiwiLogger;
ldc "license verification failed"
invokevirtual com/amazon/android/framework/util/KiwiLogger/test(Ljava/lang/String;)V
getstatic com/amazon/android/framework/prompt/c/a Lcom/amazon/android/framework/util/KiwiLogger;
ldc "Killing application"
invokevirtual com/amazon/android/framework/util/KiwiLogger/test(Ljava/lang/String;)V
return
.limit locals 1
.limit stack 2
.end method

.method protected final getExpirationDurationInSeconds()J
ldc2_w 31536000L
lreturn
.limit locals 1
.limit stack 2
.end method

.method public final onResourcesPopulatedImpl()V
aload 0
getfield com/amazon/android/framework/prompt/c/b Lcom/amazon/android/framework/context/ContextManager;
invokeinterface com/amazon/android/framework/context/ContextManager/stopServices()V 0
return
.limit locals 1
.limit stack 1
.end method

.method public final toString()Ljava/lang/String;
ldc "ShutdownPrompt"
areturn
.limit locals 1
.limit stack 1
.end method
