.bytecode 50.0
.class public synchronized abstract com/amazon/android/framework/prompt/Prompt
.super com/amazon/android/i/c

.field private static final 'LOGGER' Lcom/amazon/android/framework/util/KiwiLogger;

.field private 'context' Landroid/app/Activity;

.field private 'dataStore' Lcom/amazon/android/o/a;
.annotation visible Lcom/amazon/android/framework/resource/Resource;
.end annotation
.end field

.field private 'dialog' Landroid/app/Dialog;

.field private final 'dismissed' Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final 'identifier' I

.field private 'manualExpirationReason' Lcom/amazon/android/framework/prompt/d;

.method static <clinit>()V
new com/amazon/android/framework/util/KiwiLogger
dup
ldc "Prompt"
invokespecial com/amazon/android/framework/util/KiwiLogger/<init>(Ljava/lang/String;)V
putstatic com/amazon/android/framework/prompt/Prompt/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
return
.limit locals 0
.limit stack 3
.end method

.method public <init>()V
aload 0
invokespecial com/amazon/android/i/c/<init>()V
aload 0
new java/util/concurrent/atomic/AtomicBoolean
dup
iconst_0
invokespecial java/util/concurrent/atomic/AtomicBoolean/<init>(Z)V
putfield com/amazon/android/framework/prompt/Prompt/dismissed Ljava/util/concurrent/atomic/AtomicBoolean;
aload 0
aload 0
invokespecial com/amazon/android/framework/prompt/Prompt/createIdentifier()I
putfield com/amazon/android/framework/prompt/Prompt/identifier I
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L0
getstatic com/amazon/android/framework/prompt/Prompt/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Creating Prompt: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/amazon/android/framework/prompt/Prompt/identifier I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L0:
return
.limit locals 1
.limit stack 4
.end method

.method private createIdentifier()I
new java/util/Random
dup
invokespecial java/util/Random/<init>()V
ldc_w 2146249079
invokevirtual java/util/Random/nextInt(I)I
ldc_w 1234567
iadd
istore 2
iload 2
istore 1
iload 2
ldc_w 1234567
if_icmpgt L0
ldc_w 1234567
istore 1
L0:
iload 1
ireturn
.limit locals 3
.limit stack 2
.end method

.method private dismissDialog()V
.catch java/lang/Exception from L0 to L1 using L2
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L0
getstatic com/amazon/android/framework/prompt/Prompt/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Dismissing dialog: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/amazon/android/framework/prompt/Prompt/identifier I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/error(Ljava/lang/String;)V
L0:
aload 0
getfield com/amazon/android/framework/prompt/Prompt/context Landroid/app/Activity;
aload 0
getfield com/amazon/android/framework/prompt/Prompt/identifier I
invokevirtual android/app/Activity/dismissDialog(I)V
aload 0
getfield com/amazon/android/framework/prompt/Prompt/context Landroid/app/Activity;
aload 0
getfield com/amazon/android/framework/prompt/Prompt/identifier I
invokevirtual android/app/Activity/removeDialog(I)V
L1:
aload 0
aconst_null
putfield com/amazon/android/framework/prompt/Prompt/context Landroid/app/Activity;
aload 0
aconst_null
putfield com/amazon/android/framework/prompt/Prompt/dialog Landroid/app/Dialog;
return
L2:
astore 1
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L1
getstatic com/amazon/android/framework/prompt/Prompt/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Unable to remove dialog: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/amazon/android/framework/prompt/Prompt/identifier I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/error(Ljava/lang/String;)V
goto L1
.limit locals 2
.limit stack 3
.end method

.method private expire(Lcom/amazon/android/framework/prompt/d;)V
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L0
getstatic com/amazon/android/framework/prompt/Prompt/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Expiring prompt pre-maturely: id: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual com/amazon/android/framework/prompt/Prompt/getIdentifier()I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ", prompt: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc ","
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ", reason: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L0:
aload 0
aload 1
putfield com/amazon/android/framework/prompt/Prompt/manualExpirationReason Lcom/amazon/android/framework/prompt/d;
aload 0
invokevirtual com/amazon/android/framework/prompt/Prompt/expire()V
return
.limit locals 2
.limit stack 3
.end method

.method private isCompatible(Landroid/app/Activity;)Z
aload 0
getfield com/amazon/android/framework/prompt/Prompt/dataStore Lcom/amazon/android/o/a;
ldc "TEST_MODE"
invokevirtual com/amazon/android/o/a/b(Ljava/lang/String;)Z
ifeq L0
iconst_0
ireturn
L0:
aload 0
aload 1
invokevirtual com/amazon/android/framework/prompt/Prompt/doCompatibilityCheck(Landroid/app/Activity;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method private showDialog(Landroid/app/Activity;)V
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L0
getstatic com/amazon/android/framework/prompt/Prompt/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Showing prompt, id: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual com/amazon/android/framework/prompt/Prompt/getIdentifier()I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ", prompt: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc ", activity: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L0:
aload 0
getfield com/amazon/android/framework/prompt/Prompt/context Landroid/app/Activity;
ifnull L1
aload 0
invokespecial com/amazon/android/framework/prompt/Prompt/dismissDialog()V
L1:
aload 1
aload 0
invokevirtual com/amazon/android/framework/prompt/Prompt/getIdentifier()I
invokevirtual android/app/Activity/showDialog(I)V
return
.limit locals 2
.limit stack 3
.end method

.method public final create(Landroid/app/Activity;)Landroid/app/Dialog;
aload 0
aload 1
putfield com/amazon/android/framework/prompt/Prompt/context Landroid/app/Activity;
aload 0
aload 0
aload 1
invokevirtual com/amazon/android/framework/prompt/Prompt/doCreate(Landroid/app/Activity;)Landroid/app/Dialog;
putfield com/amazon/android/framework/prompt/Prompt/dialog Landroid/app/Dialog;
aload 0
getfield com/amazon/android/framework/prompt/Prompt/dialog Landroid/app/Dialog;
iconst_0
invokevirtual android/app/Dialog/setCancelable(Z)V
aload 0
getfield com/amazon/android/framework/prompt/Prompt/dialog Landroid/app/Dialog;
new com/amazon/android/framework/prompt/b
dup
aload 0
invokespecial com/amazon/android/framework/prompt/b/<init>(Lcom/amazon/android/framework/prompt/Prompt;)V
invokevirtual android/app/Dialog/setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V
aload 0
getfield com/amazon/android/framework/prompt/Prompt/dialog Landroid/app/Dialog;
areturn
.limit locals 2
.limit stack 4
.end method

.method protected final dismiss()Z
invokestatic com/amazon/android/d/a/a()V
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L0
getstatic com/amazon/android/framework/prompt/Prompt/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Dismissing Prompt: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/amazon/android/framework/prompt/Prompt/identifier I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L0:
aload 0
getfield com/amazon/android/framework/prompt/Prompt/dismissed Ljava/util/concurrent/atomic/AtomicBoolean;
iconst_0
iconst_1
invokevirtual java/util/concurrent/atomic/AtomicBoolean/compareAndSet(ZZ)Z
ifne L1
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L2
getstatic com/amazon/android/framework/prompt/Prompt/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
ldc "Prompt has already been dismissed"
invokevirtual com/amazon/android/framework/util/KiwiLogger/error(Ljava/lang/String;)V
L2:
iconst_0
ireturn
L1:
aload 0
getfield com/amazon/android/framework/prompt/Prompt/context Landroid/app/Activity;
ifnull L3
aload 0
invokespecial com/amazon/android/framework/prompt/Prompt/dismissDialog()V
L3:
aload 0
invokevirtual com/amazon/android/framework/prompt/Prompt/discard()V
iconst_1
ireturn
.limit locals 1
.limit stack 3
.end method

.method protected doCompatibilityCheck(Landroid/app/Activity;)Z
iconst_1
ireturn
.limit locals 2
.limit stack 1
.end method

.method protected abstract doCreate(Landroid/app/Activity;)Landroid/app/Dialog;
.end method

.method protected final doExpiration()V
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L0
getstatic com/amazon/android/framework/prompt/Prompt/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Expiring prompt: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L0:
new com/amazon/android/framework/prompt/a
dup
aload 0
invokespecial com/amazon/android/framework/prompt/a/<init>(Lcom/amazon/android/framework/prompt/Prompt;)V
astore 1
aload 0
getfield com/amazon/android/framework/prompt/Prompt/taskManager Lcom/amazon/android/framework/task/TaskManager;
getstatic com/amazon/android/framework/task/pipeline/TaskPipelineId/FOREGROUND Lcom/amazon/android/framework/task/pipeline/TaskPipelineId;
aload 1
invokeinterface com/amazon/android/framework/task/TaskManager/enqueue(Lcom/amazon/android/framework/task/pipeline/TaskPipelineId;Lcom/amazon/android/framework/task/Task;)V 2
aload 0
aload 0
invokevirtual com/amazon/android/framework/prompt/Prompt/getExpirationReason()Lcom/amazon/android/framework/prompt/d;
invokevirtual com/amazon/android/framework/prompt/Prompt/doExpiration(Lcom/amazon/android/framework/prompt/d;)V
return
.limit locals 2
.limit stack 3
.end method

.method protected abstract doExpiration(Lcom/amazon/android/framework/prompt/d;)V
.end method

.method protected getExpirationReason()Lcom/amazon/android/framework/prompt/d;
aload 0
invokevirtual com/amazon/android/framework/prompt/Prompt/isExpired()Z
ifne L0
aconst_null
areturn
L0:
aload 0
getfield com/amazon/android/framework/prompt/Prompt/manualExpirationReason Lcom/amazon/android/framework/prompt/d;
ifnonnull L1
getstatic com/amazon/android/framework/prompt/d/b Lcom/amazon/android/framework/prompt/d;
areturn
L1:
aload 0
getfield com/amazon/android/framework/prompt/Prompt/manualExpirationReason Lcom/amazon/android/framework/prompt/d;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getIdentifier()I
aload 0
getfield com/amazon/android/framework/prompt/Prompt/identifier I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public onFocusChanged(Landroid/app/Activity;Z)V
aload 1
aload 0
getfield com/amazon/android/framework/prompt/Prompt/context Landroid/app/Activity;
if_acmpeq L0
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L1
getstatic com/amazon/android/framework/prompt/Prompt/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
ldc "Unrecognized context"
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L1:
return
L0:
iload 2
ifeq L1
aload 0
getfield com/amazon/android/framework/prompt/Prompt/dialog Landroid/app/Dialog;
invokevirtual android/app/Dialog/isShowing()Z
ifne L1
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L2
getstatic com/amazon/android/framework/prompt/Prompt/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
ldc "showing dialog because it was not showing"
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L2:
aload 0
getfield com/amazon/android/framework/prompt/Prompt/dialog Landroid/app/Dialog;
invokevirtual android/app/Dialog/show()V
return
.limit locals 3
.limit stack 2
.end method

.method public final show(Landroid/app/Activity;)V
aload 1
ldc "activity"
invokestatic com/amazon/android/d/a/a(Ljava/lang/Object;Ljava/lang/String;)V
invokestatic com/amazon/android/d/a/a()V
aload 0
aload 1
invokespecial com/amazon/android/framework/prompt/Prompt/isCompatible(Landroid/app/Activity;)Z
ifeq L0
aload 0
aload 1
invokespecial com/amazon/android/framework/prompt/Prompt/showDialog(Landroid/app/Activity;)V
return
L0:
aload 0
getstatic com/amazon/android/framework/prompt/d/a Lcom/amazon/android/framework/prompt/d;
invokespecial com/amazon/android/framework/prompt/Prompt/expire(Lcom/amazon/android/framework/prompt/d;)V
return
.limit locals 2
.limit stack 2
.end method
