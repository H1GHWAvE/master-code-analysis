.bytecode 50.0
.class public synchronized abstract com/amazon/android/framework/prompt/SimplePrompt
.super com/amazon/android/framework/prompt/Prompt

.field protected final 'content' Lcom/amazon/android/framework/prompt/PromptContent;

.method public <init>(Lcom/amazon/android/framework/prompt/PromptContent;)V
aload 0
invokespecial com/amazon/android/framework/prompt/Prompt/<init>()V
aload 1
ldc "content"
invokestatic com/amazon/android/d/a/a(Ljava/lang/Object;Ljava/lang/String;)V
aload 0
aload 1
putfield com/amazon/android/framework/prompt/SimplePrompt/content Lcom/amazon/android/framework/prompt/PromptContent;
return
.limit locals 2
.limit stack 2
.end method

.method protected abstract doAction()V
.end method

.method protected doCompatibilityCheck(Landroid/app/Activity;)Z
aload 0
getfield com/amazon/android/framework/prompt/SimplePrompt/content Lcom/amazon/android/framework/prompt/PromptContent;
invokevirtual com/amazon/android/framework/prompt/PromptContent/isVisible()Z
ireturn
.limit locals 2
.limit stack 1
.end method

.method public final doCreate(Landroid/app/Activity;)Landroid/app/Dialog;
new android/app/AlertDialog$Builder
dup
aload 1
invokespecial android/app/AlertDialog$Builder/<init>(Landroid/content/Context;)V
astore 1
aload 1
aload 0
getfield com/amazon/android/framework/prompt/SimplePrompt/content Lcom/amazon/android/framework/prompt/PromptContent;
invokevirtual com/amazon/android/framework/prompt/PromptContent/getTitle()Ljava/lang/String;
invokevirtual android/app/AlertDialog$Builder/setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;
aload 0
getfield com/amazon/android/framework/prompt/SimplePrompt/content Lcom/amazon/android/framework/prompt/PromptContent;
invokevirtual com/amazon/android/framework/prompt/PromptContent/getMessage()Ljava/lang/String;
invokevirtual android/app/AlertDialog$Builder/setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;
iconst_0
invokevirtual android/app/AlertDialog$Builder/setCancelable(Z)Landroid/app/AlertDialog$Builder;
aload 0
getfield com/amazon/android/framework/prompt/SimplePrompt/content Lcom/amazon/android/framework/prompt/PromptContent;
invokevirtual com/amazon/android/framework/prompt/PromptContent/getButtonLabel()Ljava/lang/String;
new com/amazon/android/framework/prompt/g
dup
aload 0
invokespecial com/amazon/android/framework/prompt/g/<init>(Lcom/amazon/android/framework/prompt/SimplePrompt;)V
invokevirtual android/app/AlertDialog$Builder/setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;
pop
aload 1
invokevirtual android/app/AlertDialog$Builder/create()Landroid/app/AlertDialog;
areturn
.limit locals 2
.limit stack 5
.end method

.method protected doExpiration(Lcom/amazon/android/framework/prompt/d;)V
aload 0
invokevirtual com/amazon/android/framework/prompt/SimplePrompt/doAction()V
return
.limit locals 2
.limit stack 1
.end method
