.bytecode 50.0
.class public synchronized com/amazon/android/framework/prompt/PromptContent
.super java/lang/Object

.field private final 'buttonLabel' Ljava/lang/String;

.field private final 'message' Ljava/lang/String;

.field private final 'shouldDownload' Z

.field private final 'title' Ljava/lang/String;

.field private final 'visible' Z

.method public <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
aload 0
aload 1
aload 2
aload 3
iload 4
iconst_0
invokespecial com/amazon/android/framework/prompt/PromptContent/<init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V
return
.limit locals 5
.limit stack 6
.end method

.method public <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield com/amazon/android/framework/prompt/PromptContent/title Ljava/lang/String;
aload 0
aload 2
putfield com/amazon/android/framework/prompt/PromptContent/message Ljava/lang/String;
aload 0
aload 3
putfield com/amazon/android/framework/prompt/PromptContent/buttonLabel Ljava/lang/String;
aload 0
iload 4
putfield com/amazon/android/framework/prompt/PromptContent/visible Z
aload 0
iload 5
putfield com/amazon/android/framework/prompt/PromptContent/shouldDownload Z
return
.limit locals 6
.limit stack 2
.end method

.method public getButtonLabel()Ljava/lang/String;
aload 0
getfield com/amazon/android/framework/prompt/PromptContent/buttonLabel Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getMessage()Ljava/lang/String;
aload 0
getfield com/amazon/android/framework/prompt/PromptContent/message Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getTitle()Ljava/lang/String;
aload 0
getfield com/amazon/android/framework/prompt/PromptContent/title Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public isVisible()Z
aload 0
getfield com/amazon/android/framework/prompt/PromptContent/visible Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public shouldDownload()Z
aload 0
getfield com/amazon/android/framework/prompt/PromptContent/shouldDownload Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
ldc "PromptContent: [ title:"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
astore 1
aload 1
aload 0
getfield com/amazon/android/framework/prompt/PromptContent/title Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ", message: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/amazon/android/framework/prompt/PromptContent/message Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ", label: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/amazon/android/framework/prompt/PromptContent/buttonLabel Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ", visible: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/amazon/android/framework/prompt/PromptContent/visible Z
invokevirtual java/lang/StringBuilder/append(Z)Ljava/lang/StringBuilder;
ldc ", shouldDownload: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/amazon/android/framework/prompt/PromptContent/shouldDownload Z
invokevirtual java/lang/StringBuilder/append(Z)Ljava/lang/StringBuilder;
ldc "]"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 1
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 3
.end method
