.bytecode 50.0
.class public abstract interface com/amazon/android/framework/prompt/PromptManager
.super java/lang/Object
.implements com/amazon/android/i/d

.method public abstract onCreateDialog(Landroid/app/Activity;I)Landroid/app/Dialog;
.end method

.method public abstract onWindowFocusChanged(Landroid/app/Activity;Z)V
.end method

.method public abstract present(Lcom/amazon/android/framework/prompt/Prompt;)V
.end method
