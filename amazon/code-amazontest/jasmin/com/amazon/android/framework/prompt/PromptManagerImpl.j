.bytecode 50.0
.class public synchronized com/amazon/android/framework/prompt/PromptManagerImpl
.super java/lang/Object
.implements com/amazon/android/framework/prompt/PromptManager
.implements com/amazon/android/framework/resource/b

.field public static final 'LOGGER' Lcom/amazon/android/framework/util/KiwiLogger;

.field private 'contextManager' Lcom/amazon/android/framework/context/ContextManager;
.annotation visible Lcom/amazon/android/framework/resource/Resource;
.end annotation
.end field

.field private 'eventManager' Lcom/amazon/android/n/g;
.annotation visible Lcom/amazon/android/framework/resource/Resource;
.end annotation
.end field

.field private final 'finished' Ljava/util/concurrent/atomic/AtomicBoolean;

.field private 'pending' Ljava/util/Set;

.field private 'resourceManager' Lcom/amazon/android/framework/resource/a;
.annotation visible Lcom/amazon/android/framework/resource/Resource;
.end annotation
.end field

.field private 'showing' Lcom/amazon/android/framework/prompt/Prompt;

.field private 'taskManager' Lcom/amazon/android/framework/task/TaskManager;
.annotation visible Lcom/amazon/android/framework/resource/Resource;
.end annotation
.end field

.method static <clinit>()V
new com/amazon/android/framework/util/KiwiLogger
dup
ldc "PromptManagerImpl"
invokespecial com/amazon/android/framework/util/KiwiLogger/<init>(Ljava/lang/String;)V
putstatic com/amazon/android/framework/prompt/PromptManagerImpl/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
return
.limit locals 0
.limit stack 3
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/util/LinkedHashSet
dup
invokespecial java/util/LinkedHashSet/<init>()V
putfield com/amazon/android/framework/prompt/PromptManagerImpl/pending Ljava/util/Set;
aload 0
new java/util/concurrent/atomic/AtomicBoolean
dup
iconst_0
invokespecial java/util/concurrent/atomic/AtomicBoolean/<init>(Z)V
putfield com/amazon/android/framework/prompt/PromptManagerImpl/finished Ljava/util/concurrent/atomic/AtomicBoolean;
return
.limit locals 1
.limit stack 4
.end method

.method static synthetic a(Lcom/amazon/android/framework/prompt/PromptManagerImpl;)V
aload 0
invokespecial com/amazon/android/framework/prompt/PromptManagerImpl/finish()V
return
.limit locals 1
.limit stack 1
.end method

.method static synthetic a(Lcom/amazon/android/framework/prompt/PromptManagerImpl;Landroid/app/Activity;)V
aload 0
aload 1
invokespecial com/amazon/android/framework/prompt/PromptManagerImpl/onResume(Landroid/app/Activity;)V
return
.limit locals 2
.limit stack 2
.end method

.method static synthetic a(Lcom/amazon/android/framework/prompt/PromptManagerImpl;Lcom/amazon/android/framework/prompt/Prompt;)V
aload 0
aload 1
invokespecial com/amazon/android/framework/prompt/PromptManagerImpl/presentImpl(Lcom/amazon/android/framework/prompt/Prompt;)V
return
.limit locals 2
.limit stack 2
.end method

.method static synthetic b(Lcom/amazon/android/framework/prompt/PromptManagerImpl;)Lcom/amazon/android/framework/prompt/Prompt;
aload 0
getfield com/amazon/android/framework/prompt/PromptManagerImpl/showing Lcom/amazon/android/framework/prompt/Prompt;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic b(Lcom/amazon/android/framework/prompt/PromptManagerImpl;Lcom/amazon/android/framework/prompt/Prompt;)V
aload 0
aload 1
invokespecial com/amazon/android/framework/prompt/PromptManagerImpl/removeExpiredPrompt(Lcom/amazon/android/framework/prompt/Prompt;)V
return
.limit locals 2
.limit stack 2
.end method

.method private finish()V
aload 0
getfield com/amazon/android/framework/prompt/PromptManagerImpl/finished Ljava/util/concurrent/atomic/AtomicBoolean;
iconst_0
iconst_1
invokevirtual java/util/concurrent/atomic/AtomicBoolean/compareAndSet(ZZ)Z
ifne L0
L1:
return
L0:
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L2
getstatic com/amazon/android/framework/prompt/PromptManagerImpl/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
ldc "PromptManager finishing...."
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L2:
aload 0
getfield com/amazon/android/framework/prompt/PromptManagerImpl/pending Ljava/util/Set;
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 1
L3:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L4
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/amazon/android/framework/prompt/Prompt
astore 2
aload 1
invokeinterface java/util/Iterator/remove()V 0
aload 2
invokevirtual com/amazon/android/framework/prompt/Prompt/expire()V
goto L3
L4:
aload 0
getfield com/amazon/android/framework/prompt/PromptManagerImpl/showing Lcom/amazon/android/framework/prompt/Prompt;
ifnull L1
aload 0
getfield com/amazon/android/framework/prompt/PromptManagerImpl/showing Lcom/amazon/android/framework/prompt/Prompt;
invokevirtual com/amazon/android/framework/prompt/Prompt/dismiss()Z
pop
return
.limit locals 3
.limit stack 3
.end method

.method private getNextPending()Lcom/amazon/android/framework/prompt/Prompt;
aload 0
getfield com/amazon/android/framework/prompt/PromptManagerImpl/pending Ljava/util/Set;
invokeinterface java/util/Set/isEmpty()Z 0
ifeq L0
aconst_null
areturn
L0:
aload 0
getfield com/amazon/android/framework/prompt/PromptManagerImpl/pending Ljava/util/Set;
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/amazon/android/framework/prompt/Prompt
areturn
.limit locals 1
.limit stack 1
.end method

.method private onResume(Landroid/app/Activity;)V
aload 0
getfield com/amazon/android/framework/prompt/PromptManagerImpl/showing Lcom/amazon/android/framework/prompt/Prompt;
ifnull L0
aload 0
aload 0
getfield com/amazon/android/framework/prompt/PromptManagerImpl/showing Lcom/amazon/android/framework/prompt/Prompt;
aload 1
invokespecial com/amazon/android/framework/prompt/PromptManagerImpl/show(Lcom/amazon/android/framework/prompt/Prompt;Landroid/app/Activity;)V
return
L0:
aload 0
aload 1
invokespecial com/amazon/android/framework/prompt/PromptManagerImpl/presentNextPending(Landroid/app/Activity;)V
return
.limit locals 2
.limit stack 3
.end method

.method private presentImpl(Lcom/amazon/android/framework/prompt/Prompt;)V
aload 0
getfield com/amazon/android/framework/prompt/PromptManagerImpl/finished Ljava/util/concurrent/atomic/AtomicBoolean;
invokevirtual java/util/concurrent/atomic/AtomicBoolean/get()Z
ifeq L0
getstatic com/amazon/android/framework/util/KiwiLogger/ERROR_ON Z
ifeq L1
getstatic com/amazon/android/framework/prompt/PromptManagerImpl/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Prompt: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " presented after app"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " destruction expiring it now!"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/error(Ljava/lang/String;)V
L1:
aload 1
invokevirtual com/amazon/android/framework/prompt/Prompt/expire()V
L2:
return
L0:
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L3
getstatic com/amazon/android/framework/prompt/PromptManagerImpl/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Presening Prompt: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L3:
aload 1
aload 0
invokevirtual com/amazon/android/framework/prompt/Prompt/register(Lcom/amazon/android/i/d;)V
aload 0
getfield com/amazon/android/framework/prompt/PromptManagerImpl/pending Ljava/util/Set;
aload 1
invokeinterface java/util/Set/add(Ljava/lang/Object;)Z 1
pop
aload 0
getfield com/amazon/android/framework/prompt/PromptManagerImpl/showing Lcom/amazon/android/framework/prompt/Prompt;
ifnull L4
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L2
getstatic com/amazon/android/framework/prompt/PromptManagerImpl/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
ldc "Dialog currently showing, not presenting given dialog"
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
return
L4:
aload 0
getfield com/amazon/android/framework/prompt/PromptManagerImpl/contextManager Lcom/amazon/android/framework/context/ContextManager;
invokeinterface com/amazon/android/framework/context/ContextManager/getVisible()Landroid/app/Activity; 0
astore 1
aload 1
ifnull L2
aload 0
aload 1
invokespecial com/amazon/android/framework/prompt/PromptManagerImpl/presentNextPending(Landroid/app/Activity;)V
return
.limit locals 2
.limit stack 3
.end method

.method private presentNextPending(Landroid/app/Activity;)V
aload 0
invokespecial com/amazon/android/framework/prompt/PromptManagerImpl/getNextPending()Lcom/amazon/android/framework/prompt/Prompt;
astore 2
aload 2
ifnonnull L0
return
L0:
aload 0
aload 2
aload 1
invokespecial com/amazon/android/framework/prompt/PromptManagerImpl/show(Lcom/amazon/android/framework/prompt/Prompt;Landroid/app/Activity;)V
return
.limit locals 3
.limit stack 3
.end method

.method private registerActivityResumedListener()V
aload 0
getfield com/amazon/android/framework/prompt/PromptManagerImpl/eventManager Lcom/amazon/android/n/g;
new com/amazon/android/framework/prompt/f
dup
aload 0
invokespecial com/amazon/android/framework/prompt/f/<init>(Lcom/amazon/android/framework/prompt/PromptManagerImpl;)V
invokeinterface com/amazon/android/n/g/a(Lcom/amazon/android/n/c;)V 1
return
.limit locals 1
.limit stack 4
.end method

.method private registerAppDestructionListener()V
new com/amazon/android/framework/prompt/i
dup
aload 0
invokespecial com/amazon/android/framework/prompt/i/<init>(Lcom/amazon/android/framework/prompt/PromptManagerImpl;)V
astore 1
aload 0
getfield com/amazon/android/framework/prompt/PromptManagerImpl/eventManager Lcom/amazon/android/n/g;
aload 1
invokeinterface com/amazon/android/n/g/a(Lcom/amazon/android/n/c;)V 1
return
.limit locals 2
.limit stack 3
.end method

.method private registerTestModeListener()V
new com/amazon/android/framework/prompt/j
dup
aload 0
invokespecial com/amazon/android/framework/prompt/j/<init>(Lcom/amazon/android/framework/prompt/PromptManagerImpl;)V
astore 1
aload 0
getfield com/amazon/android/framework/prompt/PromptManagerImpl/eventManager Lcom/amazon/android/n/g;
aload 1
invokeinterface com/amazon/android/n/g/a(Lcom/amazon/android/n/c;)V 1
return
.limit locals 2
.limit stack 3
.end method

.method private removeExpiredPrompt(Lcom/amazon/android/framework/prompt/Prompt;)V
aload 0
getfield com/amazon/android/framework/prompt/PromptManagerImpl/pending Ljava/util/Set;
aload 1
invokeinterface java/util/Set/remove(Ljava/lang/Object;)Z 1
pop
aload 0
getfield com/amazon/android/framework/prompt/PromptManagerImpl/showing Lcom/amazon/android/framework/prompt/Prompt;
aload 1
if_acmpne L0
aload 0
aconst_null
putfield com/amazon/android/framework/prompt/PromptManagerImpl/showing Lcom/amazon/android/framework/prompt/Prompt;
aload 0
getfield com/amazon/android/framework/prompt/PromptManagerImpl/contextManager Lcom/amazon/android/framework/context/ContextManager;
invokeinterface com/amazon/android/framework/context/ContextManager/getVisible()Landroid/app/Activity; 0
astore 1
aload 1
ifnull L0
aload 0
aload 1
invokespecial com/amazon/android/framework/prompt/PromptManagerImpl/presentNextPending(Landroid/app/Activity;)V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method private show(Lcom/amazon/android/framework/prompt/Prompt;Landroid/app/Activity;)V
aload 0
aload 1
putfield com/amazon/android/framework/prompt/PromptManagerImpl/showing Lcom/amazon/android/framework/prompt/Prompt;
aload 1
aload 2
invokevirtual com/amazon/android/framework/prompt/Prompt/show(Landroid/app/Activity;)V
return
.limit locals 3
.limit stack 2
.end method

.method public observe(Lcom/amazon/android/framework/prompt/Prompt;)V
new com/amazon/android/framework/prompt/e
dup
aload 0
aload 1
invokespecial com/amazon/android/framework/prompt/e/<init>(Lcom/amazon/android/framework/prompt/PromptManagerImpl;Lcom/amazon/android/framework/prompt/Prompt;)V
astore 1
aload 0
getfield com/amazon/android/framework/prompt/PromptManagerImpl/taskManager Lcom/amazon/android/framework/task/TaskManager;
getstatic com/amazon/android/framework/task/pipeline/TaskPipelineId/FOREGROUND Lcom/amazon/android/framework/task/pipeline/TaskPipelineId;
aload 1
invokeinterface com/amazon/android/framework/task/TaskManager/enqueue(Lcom/amazon/android/framework/task/pipeline/TaskPipelineId;Lcom/amazon/android/framework/task/Task;)V 2
return
.limit locals 2
.limit stack 4
.end method

.method public volatile synthetic observe(Lcom/amazon/android/i/b;)V
aload 0
aload 1
checkcast com/amazon/android/framework/prompt/Prompt
invokevirtual com/amazon/android/framework/prompt/PromptManagerImpl/observe(Lcom/amazon/android/framework/prompt/Prompt;)V
return
.limit locals 2
.limit stack 2
.end method

.method public onCreateDialog(Landroid/app/Activity;I)Landroid/app/Dialog;
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L0
getstatic com/amazon/android/framework/prompt/PromptManagerImpl/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "onCreateDialog, id: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 2
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ", activity: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L0:
aload 0
getfield com/amazon/android/framework/prompt/PromptManagerImpl/showing Lcom/amazon/android/framework/prompt/Prompt;
ifnonnull L1
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L2
getstatic com/amazon/android/framework/prompt/PromptManagerImpl/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
ldc "Showing dialog is null, returning"
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L2:
aconst_null
areturn
L1:
aload 0
getfield com/amazon/android/framework/prompt/PromptManagerImpl/showing Lcom/amazon/android/framework/prompt/Prompt;
invokevirtual com/amazon/android/framework/prompt/Prompt/getIdentifier()I
iload 2
if_icmpeq L3
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L4
getstatic com/amazon/android/framework/prompt/PromptManagerImpl/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Showing dialog id does not match given id: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 2
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ", returning"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L4:
aconst_null
areturn
L3:
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L5
getstatic com/amazon/android/framework/prompt/PromptManagerImpl/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Creating dialog prompt: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/amazon/android/framework/prompt/PromptManagerImpl/showing Lcom/amazon/android/framework/prompt/Prompt;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L5:
aload 0
getfield com/amazon/android/framework/prompt/PromptManagerImpl/showing Lcom/amazon/android/framework/prompt/Prompt;
aload 1
invokevirtual com/amazon/android/framework/prompt/Prompt/create(Landroid/app/Activity;)Landroid/app/Dialog;
areturn
.limit locals 3
.limit stack 3
.end method

.method public onResourcesPopulated()V
aload 0
invokespecial com/amazon/android/framework/prompt/PromptManagerImpl/registerActivityResumedListener()V
aload 0
invokespecial com/amazon/android/framework/prompt/PromptManagerImpl/registerAppDestructionListener()V
aload 0
invokespecial com/amazon/android/framework/prompt/PromptManagerImpl/registerTestModeListener()V
return
.limit locals 1
.limit stack 1
.end method

.method public onWindowFocusChanged(Landroid/app/Activity;Z)V
aload 0
getfield com/amazon/android/framework/prompt/PromptManagerImpl/showing Lcom/amazon/android/framework/prompt/Prompt;
ifnull L0
aload 0
getfield com/amazon/android/framework/prompt/PromptManagerImpl/showing Lcom/amazon/android/framework/prompt/Prompt;
aload 1
iload 2
invokevirtual com/amazon/android/framework/prompt/Prompt/onFocusChanged(Landroid/app/Activity;Z)V
L0:
return
.limit locals 3
.limit stack 3
.end method

.method public present(Lcom/amazon/android/framework/prompt/Prompt;)V
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L0
getstatic com/amazon/android/framework/prompt/PromptManagerImpl/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Scheduling presentation: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L0:
aload 0
getfield com/amazon/android/framework/prompt/PromptManagerImpl/resourceManager Lcom/amazon/android/framework/resource/a;
aload 1
invokeinterface com/amazon/android/framework/resource/a/b(Ljava/lang/Object;)V 1
aload 0
getfield com/amazon/android/framework/prompt/PromptManagerImpl/finished Ljava/util/concurrent/atomic/AtomicBoolean;
invokevirtual java/util/concurrent/atomic/AtomicBoolean/get()Z
ifeq L1
getstatic com/amazon/android/framework/util/KiwiLogger/ERROR_ON Z
ifeq L2
getstatic com/amazon/android/framework/prompt/PromptManagerImpl/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Prompt: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " presented after app"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " destruction expiring it now!"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/error(Ljava/lang/String;)V
L2:
aload 1
invokevirtual com/amazon/android/framework/prompt/Prompt/expire()V
return
L1:
new com/amazon/android/framework/prompt/h
dup
aload 0
aload 1
invokespecial com/amazon/android/framework/prompt/h/<init>(Lcom/amazon/android/framework/prompt/PromptManagerImpl;Lcom/amazon/android/framework/prompt/Prompt;)V
astore 1
aload 0
getfield com/amazon/android/framework/prompt/PromptManagerImpl/taskManager Lcom/amazon/android/framework/task/TaskManager;
getstatic com/amazon/android/framework/task/pipeline/TaskPipelineId/FOREGROUND Lcom/amazon/android/framework/task/pipeline/TaskPipelineId;
aload 1
invokeinterface com/amazon/android/framework/task/TaskManager/enqueue(Lcom/amazon/android/framework/task/pipeline/TaskPipelineId;Lcom/amazon/android/framework/task/Task;)V 2
return
.limit locals 2
.limit stack 4
.end method
