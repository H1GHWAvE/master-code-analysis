.bytecode 50.0
.class public synchronized abstract com/amazon/android/framework/exception/KiwiException
.super java/lang/Exception

.field private static final 'serialVersionUID' J = 1L


.field private final 'context' Ljava/lang/String;

.field private final 'reason' Ljava/lang/String;

.field private final 'type' Ljava/lang/String;

.method public <init>(Ljava/lang/String;)V
aload 0
aload 1
aconst_null
invokespecial com/amazon/android/framework/exception/KiwiException/<init>(Ljava/lang/String;Ljava/lang/String;)V
return
.limit locals 2
.limit stack 3
.end method

.method public <init>(Ljava/lang/String;Ljava/lang/String;)V
aload 0
aload 1
aload 2
aconst_null
invokespecial com/amazon/android/framework/exception/KiwiException/<init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
return
.limit locals 3
.limit stack 4
.end method

.method public <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
aload 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ": "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ": "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/Exception/<init>(Ljava/lang/String;)V
aload 0
aload 1
putfield com/amazon/android/framework/exception/KiwiException/type Ljava/lang/String;
aload 0
aload 2
putfield com/amazon/android/framework/exception/KiwiException/reason Ljava/lang/String;
aload 0
aload 3
putfield com/amazon/android/framework/exception/KiwiException/context Ljava/lang/String;
return
.limit locals 4
.limit stack 3
.end method

.method public <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
aload 0
aload 1
aload 2
aload 3
invokestatic com/amazon/android/framework/exception/KiwiException/getContext(Ljava/lang/Throwable;)Ljava/lang/String;
invokespecial com/amazon/android/framework/exception/KiwiException/<init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
return
.limit locals 4
.limit stack 4
.end method

.method public <init>(Ljava/lang/String;Ljava/lang/Throwable;)V
aload 0
aload 1
aload 2
invokestatic com/amazon/android/framework/exception/KiwiException/getName(Ljava/lang/Throwable;)Ljava/lang/String;
aload 2
invokespecial com/amazon/android/framework/exception/KiwiException/<init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
return
.limit locals 3
.limit stack 4
.end method

.method private static getContext(Ljava/lang/Throwable;)Ljava/lang/String;
aload 0
ifnonnull L0
aconst_null
areturn
L0:
aload 0
invokestatic com/amazon/android/framework/exception/KiwiException/getRootCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;
astore 1
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
astore 2
aload 2
aload 0
invokestatic com/amazon/android/framework/exception/KiwiException/getName(Ljava/lang/Throwable;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/Throwable/getMessage()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 0
aload 1
if_acmpeq L1
aload 2
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokestatic com/amazon/android/framework/exception/KiwiException/getName(Ljava/lang/Throwable;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/Throwable/getMessage()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
L1:
aload 2
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 3
.limit stack 2
.end method

.method private static getName(Ljava/lang/Throwable;)Ljava/lang/String;
aload 0
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getName()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static getRootCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;
L0:
aload 0
invokevirtual java/lang/Throwable/getCause()Ljava/lang/Throwable;
ifnull L1
aload 0
invokevirtual java/lang/Throwable/getCause()Ljava/lang/Throwable;
astore 0
goto L0
L1:
aload 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public final getContext()Ljava/lang/String;
aload 0
getfield com/amazon/android/framework/exception/KiwiException/context Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final getReason()Ljava/lang/String;
aload 0
getfield com/amazon/android/framework/exception/KiwiException/reason Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final getType()Ljava/lang/String;
aload 0
getfield com/amazon/android/framework/exception/KiwiException/type Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method
