.bytecode 50.0
.class public final synchronized com/amazon/android/framework/resource/c
.super java/lang/Object
.implements com/amazon/android/framework/resource/a

.field private static 'a' Lcom/amazon/android/framework/util/KiwiLogger;

.field private 'b' Ljava/util/List;

.field private 'c' Z

.method static <clinit>()V
new com/amazon/android/framework/util/KiwiLogger
dup
ldc "ResourceManagerImpl"
invokespecial com/amazon/android/framework/util/KiwiLogger/<init>(Ljava/lang/String;)V
putstatic com/amazon/android/framework/resource/c/a Lcom/amazon/android/framework/util/KiwiLogger;
return
.limit locals 0
.limit stack 3
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield com/amazon/android/framework/resource/c/b Ljava/util/List;
aload 0
iconst_0
putfield com/amazon/android/framework/resource/c/c Z
aload 0
aload 0
invokevirtual com/amazon/android/framework/resource/c/a(Ljava/lang/Object;)V
return
.limit locals 1
.limit stack 3
.end method

.method private a(Ljava/lang/Class;)Ljava/lang/Object;
aload 0
getfield com/amazon/android/framework/resource/c/b Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 2
L0:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
astore 3
aload 1
aload 3
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/isAssignableFrom(Ljava/lang/Class;)Z
ifeq L0
aload 3
areturn
L1:
aconst_null
areturn
.limit locals 4
.limit stack 2
.end method

.method public final a()V
aload 0
getfield com/amazon/android/framework/resource/c/b Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 1
L0:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 0
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
invokevirtual com/amazon/android/framework/resource/c/b(Ljava/lang/Object;)V
goto L0
L1:
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Ljava/lang/Object;)V
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L0
getstatic com/amazon/android/framework/resource/c/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Registering resource: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L0:
invokestatic com/amazon/android/d/a/a()V
aload 0
getfield com/amazon/android/framework/resource/c/c Z
ifeq L1
new java/lang/IllegalStateException
dup
ldc "Attempt made to register resource after population has begun!"
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L1:
aload 1
ldc "resource"
invokestatic com/amazon/android/d/a/a(Ljava/lang/Object;Ljava/lang/String;)V
aload 0
aload 1
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokespecial com/amazon/android/framework/resource/c/a(Ljava/lang/Class;)Ljava/lang/Object;
ifnull L2
iconst_1
istore 2
L3:
iload 2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Resource already registered: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/amazon/android/d/a/b(ZLjava/lang/String;)V
aload 0
getfield com/amazon/android/framework/resource/c/b Ljava/util/List;
aload 1
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
return
L2:
iconst_0
istore 2
goto L3
.limit locals 3
.limit stack 3
.end method

.method public final b(Ljava/lang/Object;)V
.catch java/lang/Exception from L0 to L1 using L2
aload 1
ldc "target"
invokestatic com/amazon/android/d/a/a(Ljava/lang/Object;Ljava/lang/String;)V
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L3
getstatic com/amazon/android/framework/resource/c/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Populating: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L3:
aload 1
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getName()Ljava/lang/String;
ldc "com.amazon.android"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifne L4
getstatic com/amazon/android/framework/resource/c/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Ignoring: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc ", not a kiwi class"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L4:
aload 1
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
astore 6
L5:
aload 6
ldc java/lang/Object
if_acmpeq L6
aload 6
invokevirtual java/lang/Class/getDeclaredFields()[Ljava/lang/reflect/Field;
astore 8
aload 8
arraylength
istore 4
iconst_0
istore 2
L7:
iload 2
iload 4
if_icmpge L8
aload 8
iload 2
aaload
astore 7
aload 7
ldc com/amazon/android/framework/resource/Resource
invokevirtual java/lang/reflect/Field/getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;
ifnull L9
iconst_1
istore 3
L10:
iload 3
ifeq L1
aload 7
invokevirtual java/lang/reflect/Field/getType()Ljava/lang/Class;
astore 10
aload 0
aload 10
invokespecial com/amazon/android/framework/resource/c/a(Ljava/lang/Class;)Ljava/lang/Object;
astore 9
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "no resource found for type: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 10
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 10
aload 9
ifnull L11
iconst_1
istore 5
L12:
iload 5
aload 10
invokestatic com/amazon/android/d/a/a(ZLjava/lang/String;)V
aload 7
iconst_1
invokevirtual java/lang/reflect/Field/setAccessible(Z)V
L0:
aload 7
aload 1
aload 9
invokevirtual java/lang/reflect/Field/set(Ljava/lang/Object;Ljava/lang/Object;)V
L1:
iload 2
iconst_1
iadd
istore 2
goto L7
L9:
iconst_0
istore 3
goto L10
L11:
iconst_0
istore 5
goto L12
L2:
astore 1
new com/amazon/android/d/b
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Failed to populate field: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 7
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 1
invokespecial com/amazon/android/d/b/<init>(Ljava/lang/String;Ljava/lang/Throwable;)V
athrow
L8:
aload 6
invokevirtual java/lang/Class/getSuperclass()Ljava/lang/Class;
astore 6
goto L5
L6:
aload 1
instanceof com/amazon/android/framework/resource/b
ifeq L13
aload 1
checkcast com/amazon/android/framework/resource/b
invokeinterface com/amazon/android/framework/resource/b/onResourcesPopulated()V 0
L13:
return
.limit locals 11
.limit stack 4
.end method
