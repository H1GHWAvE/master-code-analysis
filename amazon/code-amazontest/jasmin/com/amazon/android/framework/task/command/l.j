.bytecode 50.0
.class public final synchronized com/amazon/android/framework/task/command/l
.super java/lang/Object

.field 'a' Ljava/lang/String;

.field 'b' Landroid/os/RemoteException;

.field 'c' Lcom/amazon/venezia/command/SuccessResult;

.field 'd' Lcom/amazon/venezia/command/FailureResult;

.field 'e' Lcom/amazon/venezia/command/r;

.field 'f' Lcom/amazon/venezia/command/k;

.field private 'g' I

.method public <init>(Lcom/amazon/venezia/command/FailureResult;)V
.catch android/os/RemoteException from L0 to L1 using L2
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield com/amazon/android/framework/task/command/l/d Lcom/amazon/venezia/command/FailureResult;
aload 0
invokestatic android/os/Binder/getCallingUid()I
putfield com/amazon/android/framework/task/command/l/g I
L0:
aload 0
aload 1
invokeinterface com/amazon/venezia/command/FailureResult/getAuthToken()Ljava/lang/String; 0
putfield com/amazon/android/framework/task/command/l/a Ljava/lang/String;
L1:
return
L2:
astore 1
aload 0
aload 1
putfield com/amazon/android/framework/task/command/l/b Landroid/os/RemoteException;
return
.limit locals 2
.limit stack 2
.end method

.method public <init>(Lcom/amazon/venezia/command/SuccessResult;)V
.catch android/os/RemoteException from L0 to L1 using L2
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield com/amazon/android/framework/task/command/l/c Lcom/amazon/venezia/command/SuccessResult;
aload 0
invokestatic android/os/Binder/getCallingUid()I
putfield com/amazon/android/framework/task/command/l/g I
L0:
aload 0
aload 1
invokeinterface com/amazon/venezia/command/SuccessResult/getAuthToken()Ljava/lang/String; 0
putfield com/amazon/android/framework/task/command/l/a Ljava/lang/String;
L1:
return
L2:
astore 1
aload 0
aload 1
putfield com/amazon/android/framework/task/command/l/b Landroid/os/RemoteException;
return
.limit locals 2
.limit stack 2
.end method

.method public <init>(Lcom/amazon/venezia/command/k;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield com/amazon/android/framework/task/command/l/f Lcom/amazon/venezia/command/k;
aload 0
invokestatic android/os/Binder/getCallingUid()I
putfield com/amazon/android/framework/task/command/l/g I
return
.limit locals 2
.limit stack 2
.end method

.method public <init>(Lcom/amazon/venezia/command/r;)V
.catch android/os/RemoteException from L0 to L1 using L2
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield com/amazon/android/framework/task/command/l/e Lcom/amazon/venezia/command/r;
aload 0
invokestatic android/os/Binder/getCallingUid()I
putfield com/amazon/android/framework/task/command/l/g I
L0:
aload 0
aload 1
invokeinterface com/amazon/venezia/command/r/a()Ljava/lang/String; 0
putfield com/amazon/android/framework/task/command/l/a Ljava/lang/String;
L1:
return
L2:
astore 1
aload 0
aload 1
putfield com/amazon/android/framework/task/command/l/b Landroid/os/RemoteException;
return
.limit locals 2
.limit stack 2
.end method

.method public final toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
ldc "CommandResult: ["
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
astore 1
aload 1
ldc "CallingUid: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/amazon/android/framework/task/command/l/g I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ", SuccessResult: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/amazon/android/framework/task/command/l/c Lcom/amazon/venezia/command/SuccessResult;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc ", FailureResult: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/amazon/android/framework/task/command/l/d Lcom/amazon/venezia/command/FailureResult;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc ", DecisionResult: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/amazon/android/framework/task/command/l/e Lcom/amazon/venezia/command/r;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc ", ExceptionResult: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/amazon/android/framework/task/command/l/f Lcom/amazon/venezia/command/k;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "]"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 1
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 3
.end method
