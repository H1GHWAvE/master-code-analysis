.bytecode 50.0
.class public final synchronized enum com/amazon/android/framework/task/pipeline/TaskPipelineId
.super java/lang/Enum

.field private static final synthetic '$VALUES' [Lcom/amazon/android/framework/task/pipeline/TaskPipelineId;

.field public static final enum 'BACKGROUND' Lcom/amazon/android/framework/task/pipeline/TaskPipelineId;

.field public static final enum 'COMMAND' Lcom/amazon/android/framework/task/pipeline/TaskPipelineId;

.field public static final enum 'FOREGROUND' Lcom/amazon/android/framework/task/pipeline/TaskPipelineId;

.method static <clinit>()V
new com/amazon/android/framework/task/pipeline/TaskPipelineId
dup
ldc "FOREGROUND"
iconst_0
invokespecial com/amazon/android/framework/task/pipeline/TaskPipelineId/<init>(Ljava/lang/String;I)V
putstatic com/amazon/android/framework/task/pipeline/TaskPipelineId/FOREGROUND Lcom/amazon/android/framework/task/pipeline/TaskPipelineId;
new com/amazon/android/framework/task/pipeline/TaskPipelineId
dup
ldc "COMMAND"
iconst_1
invokespecial com/amazon/android/framework/task/pipeline/TaskPipelineId/<init>(Ljava/lang/String;I)V
putstatic com/amazon/android/framework/task/pipeline/TaskPipelineId/COMMAND Lcom/amazon/android/framework/task/pipeline/TaskPipelineId;
new com/amazon/android/framework/task/pipeline/TaskPipelineId
dup
ldc "BACKGROUND"
iconst_2
invokespecial com/amazon/android/framework/task/pipeline/TaskPipelineId/<init>(Ljava/lang/String;I)V
putstatic com/amazon/android/framework/task/pipeline/TaskPipelineId/BACKGROUND Lcom/amazon/android/framework/task/pipeline/TaskPipelineId;
iconst_3
anewarray com/amazon/android/framework/task/pipeline/TaskPipelineId
dup
iconst_0
getstatic com/amazon/android/framework/task/pipeline/TaskPipelineId/FOREGROUND Lcom/amazon/android/framework/task/pipeline/TaskPipelineId;
aastore
dup
iconst_1
getstatic com/amazon/android/framework/task/pipeline/TaskPipelineId/COMMAND Lcom/amazon/android/framework/task/pipeline/TaskPipelineId;
aastore
dup
iconst_2
getstatic com/amazon/android/framework/task/pipeline/TaskPipelineId/BACKGROUND Lcom/amazon/android/framework/task/pipeline/TaskPipelineId;
aastore
putstatic com/amazon/android/framework/task/pipeline/TaskPipelineId/$VALUES [Lcom/amazon/android/framework/task/pipeline/TaskPipelineId;
return
.limit locals 0
.limit stack 4
.end method

.method private <init>(Ljava/lang/String;I)V
aload 0
aload 1
iload 2
invokespecial java/lang/Enum/<init>(Ljava/lang/String;I)V
return
.limit locals 3
.limit stack 3
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/amazon/android/framework/task/pipeline/TaskPipelineId;
ldc com/amazon/android/framework/task/pipeline/TaskPipelineId
aload 0
invokestatic java/lang/Enum/valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;
checkcast com/amazon/android/framework/task/pipeline/TaskPipelineId
areturn
.limit locals 1
.limit stack 2
.end method

.method public static values()[Lcom/amazon/android/framework/task/pipeline/TaskPipelineId;
getstatic com/amazon/android/framework/task/pipeline/TaskPipelineId/$VALUES [Lcom/amazon/android/framework/task/pipeline/TaskPipelineId;
invokevirtual [Lcom/amazon/android/framework/task/pipeline/TaskPipelineId;/clone()Ljava/lang/Object;
checkcast [Lcom/amazon/android/framework/task/pipeline/TaskPipelineId;
areturn
.limit locals 0
.limit stack 1
.end method
