.bytecode 50.0
.class final synchronized com/amazon/android/framework/task/pipeline/d
.super java/lang/Object
.implements java/lang/Runnable

.field private synthetic 'a' Lcom/amazon/android/framework/task/Task;

.field private synthetic 'b' Lcom/amazon/android/framework/task/pipeline/a;

.method <init>(Lcom/amazon/android/framework/task/pipeline/a;Lcom/amazon/android/framework/task/Task;)V
aload 0
aload 1
putfield com/amazon/android/framework/task/pipeline/d/b Lcom/amazon/android/framework/task/pipeline/a;
aload 0
aload 2
putfield com/amazon/android/framework/task/pipeline/d/a Lcom/amazon/android/framework/task/Task;
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 3
.limit stack 2
.end method

.method public final run()V
.catch java/lang/Throwable from L0 to L1 using L2
.catch all from L0 to L1 using L3
.catch java/lang/Throwable from L1 to L4 using L2
.catch all from L1 to L4 using L3
.catch all from L5 to L6 using L3
aload 0
getfield com/amazon/android/framework/task/pipeline/d/b Lcom/amazon/android/framework/task/pipeline/a;
invokestatic com/amazon/android/framework/task/pipeline/a/a(Lcom/amazon/android/framework/task/pipeline/a;)Ljava/util/Set;
aload 0
invokeinterface java/util/Set/remove(Ljava/lang/Object;)Z 1
pop
L0:
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L1
invokestatic com/amazon/android/framework/task/pipeline/a/b()Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
getfield com/amazon/android/framework/task/pipeline/d/b Lcom/amazon/android/framework/task/pipeline/a;
invokestatic com/amazon/android/framework/task/pipeline/a/b(Lcom/amazon/android/framework/task/pipeline/a;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ": Executing Task: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/amazon/android/framework/task/pipeline/d/a Lcom/amazon/android/framework/task/Task;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc ", current time: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
new java/util/Date
dup
invokespecial java/util/Date/<init>()V
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc ", uptime: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokestatic android/os/SystemClock/uptimeMillis()J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L1:
aload 0
getfield com/amazon/android/framework/task/pipeline/d/a Lcom/amazon/android/framework/task/Task;
invokeinterface com/amazon/android/framework/task/Task/execute()V 0
L4:
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L7
invokestatic com/amazon/android/framework/task/pipeline/a/b()Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
getfield com/amazon/android/framework/task/pipeline/d/b Lcom/amazon/android/framework/task/pipeline/a;
invokestatic com/amazon/android/framework/task/pipeline/a/b(Lcom/amazon/android/framework/task/pipeline/a;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ": Task finished executing: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/amazon/android/framework/task/pipeline/d/a Lcom/amazon/android/framework/task/Task;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L7:
return
L2:
astore 1
L5:
getstatic com/amazon/android/framework/util/KiwiLogger/ERROR_ON Z
ifeq L6
invokestatic com/amazon/android/framework/task/pipeline/a/b()Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Task Failed with unhandled exception: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 1
invokevirtual com/amazon/android/framework/util/KiwiLogger/error(Ljava/lang/String;Ljava/lang/Throwable;)V
L6:
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L7
invokestatic com/amazon/android/framework/task/pipeline/a/b()Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
getfield com/amazon/android/framework/task/pipeline/d/b Lcom/amazon/android/framework/task/pipeline/a;
invokestatic com/amazon/android/framework/task/pipeline/a/b(Lcom/amazon/android/framework/task/pipeline/a;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ": Task finished executing: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/amazon/android/framework/task/pipeline/d/a Lcom/amazon/android/framework/task/Task;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
return
L3:
astore 1
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L8
invokestatic com/amazon/android/framework/task/pipeline/a/b()Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
getfield com/amazon/android/framework/task/pipeline/d/b Lcom/amazon/android/framework/task/pipeline/a;
invokestatic com/amazon/android/framework/task/pipeline/a/b(Lcom/amazon/android/framework/task/pipeline/a;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ": Task finished executing: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/amazon/android/framework/task/pipeline/d/a Lcom/amazon/android/framework/task/Task;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L8:
aload 1
athrow
.limit locals 2
.limit stack 4
.end method

.method public final toString()Ljava/lang/String;
aload 0
getfield com/amazon/android/framework/task/pipeline/d/a Lcom/amazon/android/framework/task/Task;
invokevirtual java/lang/Object/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method
