.bytecode 50.0
.class public final synchronized com/amazon/android/framework/task/command/b
.super java/lang/Object

.field private static final 'a' Lcom/amazon/android/framework/util/KiwiLogger;

.field private 'b' Lcom/amazon/venezia/command/h;

.field private 'c' Ljava/lang/String;

.field private final 'd' Ljava/util/concurrent/BlockingQueue;

.field private final 'e' Ljava/util/concurrent/BlockingQueue;

.field private 'f' Landroid/app/Application;
.annotation visible Lcom/amazon/android/framework/resource/Resource;
.end annotation
.end field

.field private 'g' Lcom/amazon/android/framework/task/command/e;
.annotation visible Lcom/amazon/android/framework/resource/Resource;
.end annotation
.end field

.field private 'h' Lcom/amazon/android/o/a;
.annotation visible Lcom/amazon/android/framework/resource/Resource;
.end annotation
.end field

.field private final 'i' Landroid/content/ServiceConnection;

.field private final 'j' Lcom/amazon/venezia/command/f;

.method static <clinit>()V
new com/amazon/android/framework/util/KiwiLogger
dup
ldc "CommandServiceClient"
invokespecial com/amazon/android/framework/util/KiwiLogger/<init>(Ljava/lang/String;)V
putstatic com/amazon/android/framework/task/command/b/a Lcom/amazon/android/framework/util/KiwiLogger;
return
.limit locals 0
.limit stack 3
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/util/concurrent/LinkedBlockingQueue
dup
invokespecial java/util/concurrent/LinkedBlockingQueue/<init>()V
putfield com/amazon/android/framework/task/command/b/d Ljava/util/concurrent/BlockingQueue;
aload 0
new java/util/concurrent/LinkedBlockingQueue
dup
invokespecial java/util/concurrent/LinkedBlockingQueue/<init>()V
putfield com/amazon/android/framework/task/command/b/e Ljava/util/concurrent/BlockingQueue;
aload 0
new com/amazon/android/framework/task/command/i
dup
aload 0
invokespecial com/amazon/android/framework/task/command/i/<init>(Lcom/amazon/android/framework/task/command/b;)V
putfield com/amazon/android/framework/task/command/b/i Landroid/content/ServiceConnection;
aload 0
new com/amazon/android/framework/task/command/j
dup
aload 0
invokespecial com/amazon/android/framework/task/command/j/<init>(Lcom/amazon/android/framework/task/command/b;)V
putfield com/amazon/android/framework/task/command/b/j Lcom/amazon/venezia/command/f;
return
.limit locals 1
.limit stack 4
.end method

.method private static a(Ljava/lang/String;)Landroid/content/Intent;
new android/content/Intent
dup
invokespecial android/content/Intent/<init>()V
astore 1
aload 1
ldc "com.amazon.venezia.CommandService"
invokevirtual android/content/Intent/setAction(Ljava/lang/String;)Landroid/content/Intent;
pop
aload 1
aload 0
ldc "com.amazon.venezia.service.command.CommandServiceImpl"
invokevirtual android/content/Intent/setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
pop
aload 1
areturn
.limit locals 2
.limit stack 3
.end method

.method private a(Ljava/util/List;)Landroid/content/Intent;
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch java/lang/Exception from L4 to L5 using L2
iconst_m1
istore 4
iconst_0
istore 2
aconst_null
astore 5
L6:
iload 4
istore 3
L0:
iload 2
aload 1
invokeinterface java/util/List/size()I 0
if_icmpge L7
aload 1
iload 2
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast android/content/pm/ResolveInfo
astore 6
aload 6
getfield android/content/pm/ResolveInfo/serviceInfo Landroid/content/pm/ServiceInfo;
getfield android/content/pm/ServiceInfo/applicationInfo Landroid/content/pm/ApplicationInfo;
getfield android/content/pm/ApplicationInfo/packageName Ljava/lang/String;
astore 5
getstatic com/amazon/android/framework/task/command/b/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Examining package "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 5
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
getstatic com/amazon/android/framework/task/command/b/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Priority is "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
getfield android/content/pm/ResolveInfo/filter Landroid/content/IntentFilter;
invokevirtual android/content/IntentFilter/getPriority()I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
getstatic com/amazon/android/framework/task/command/b/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Checking signature of package "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 5
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
getstatic com/amazon/android/framework/task/command/b/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "isPackageSignatureTrusted "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 5
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
aload 0
getfield com/amazon/android/framework/task/command/b/g Lcom/amazon/android/framework/task/command/e;
aload 5
getstatic com/amazon/android/framework/task/command/k/a Ljava/util/List;
invokevirtual com/amazon/android/framework/task/command/e/a(Ljava/lang/String;Ljava/util/List;)Z
ifeq L4
getstatic com/amazon/android/framework/task/command/b/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Signature of package "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 5
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " is okay"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L1:
iload 2
istore 3
L7:
iload 3
iflt L8
L3:
aload 5
invokestatic com/amazon/android/framework/task/command/b/a(Ljava/lang/String;)Landroid/content/Intent;
areturn
L4:
getstatic com/amazon/android/framework/task/command/b/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Signature of package "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 5
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " is bad"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L5:
iload 2
iconst_1
iadd
istore 2
goto L6
L2:
astore 1
getstatic com/amazon/android/framework/task/command/b/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Caught exception "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L8:
aconst_null
areturn
.limit locals 7
.limit stack 3
.end method

.method static synthetic a(Lcom/amazon/android/framework/task/command/b;)Ljava/util/concurrent/BlockingQueue;
aload 0
getfield com/amazon/android/framework/task/command/b/e Ljava/util/concurrent/BlockingQueue;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic b(Lcom/amazon/android/framework/task/command/b;)Ljava/util/concurrent/BlockingQueue;
aload 0
getfield com/amazon/android/framework/task/command/b/d Ljava/util/concurrent/BlockingQueue;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic c()Lcom/amazon/android/framework/util/KiwiLogger;
getstatic com/amazon/android/framework/task/command/b/a Lcom/amazon/android/framework/util/KiwiLogger;
areturn
.limit locals 0
.limit stack 1
.end method

.method private d()Lcom/amazon/android/framework/task/command/l;
.throws android/os/RemoteException
.catch java/lang/InterruptedException from L0 to L1 using L2
L0:
getstatic com/amazon/android/framework/task/command/b/a Lcom/amazon/android/framework/util/KiwiLogger;
ldc "Blocking for result from service"
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
aload 0
getfield com/amazon/android/framework/task/command/b/d Ljava/util/concurrent/BlockingQueue;
invokeinterface java/util/concurrent/BlockingQueue/take()Ljava/lang/Object; 0
checkcast com/amazon/android/framework/task/command/l
astore 1
getstatic com/amazon/android/framework/task/command/b/a Lcom/amazon/android/framework/util/KiwiLogger;
ldc "Received result from service"
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L1:
aload 1
areturn
L2:
astore 1
getstatic com/amazon/android/framework/task/command/b/a Lcom/amazon/android/framework/util/KiwiLogger;
ldc "TaskThread interrupted, returning null result"
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
aconst_null
areturn
.limit locals 2
.limit stack 2
.end method

.method public final a(Lcom/amazon/venezia/command/n;)Lcom/amazon/android/framework/task/command/l;
.throws android/os/RemoteException
aload 1
aconst_null
invokeinterface com/amazon/venezia/command/n/a(Lcom/amazon/venezia/command/y;)V 1
aload 0
invokespecial com/amazon/android/framework/task/command/b/d()Lcom/amazon/android/framework/task/command/l;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final a(Lcom/amazon/venezia/command/r;Lcom/amazon/android/framework/task/command/a;)Lcom/amazon/android/framework/task/command/l;
.throws android/os/RemoteException
aload 1
new com/amazon/android/framework/task/command/h
dup
aload 0
aload 2
invokespecial com/amazon/android/framework/task/command/h/<init>(Lcom/amazon/android/framework/task/command/b;Lcom/amazon/android/framework/task/command/a;)V
invokeinterface com/amazon/venezia/command/r/a(Lcom/amazon/venezia/command/s;)V 1
aload 0
invokespecial com/amazon/android/framework/task/command/b/d()Lcom/amazon/android/framework/task/command/l;
areturn
.limit locals 3
.limit stack 5
.end method

.method public final a(Lcom/amazon/venezia/command/w;)Lcom/amazon/android/framework/task/command/l;
.throws com/amazon/android/b/g
.throws com/amazon/android/b/f
.throws android/os/RemoteException
.catch java/lang/InterruptedException from L0 to L1 using L2
aload 0
getfield com/amazon/android/framework/task/command/b/b Lcom/amazon/venezia/command/h;
ifnull L3
iconst_1
istore 2
L4:
iload 2
ifne L5
invokestatic java/lang/System/currentTimeMillis()J
lstore 3
getstatic com/amazon/android/framework/task/command/b/a Lcom/amazon/android/framework/util/KiwiLogger;
ldc "Binding Service!!!"
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
new android/content/Intent
dup
invokespecial android/content/Intent/<init>()V
astore 7
aload 7
ldc "com.amazon.venezia.CommandService"
invokevirtual android/content/Intent/setAction(Ljava/lang/String;)Landroid/content/Intent;
pop
getstatic com/amazon/android/framework/task/command/b/a Lcom/amazon/android/framework/util/KiwiLogger;
ldc "Created intent with  action  com.amazon.venezia.CommandService"
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
aload 0
getfield com/amazon/android/framework/task/command/b/f Landroid/app/Application;
invokevirtual android/content/Context/getPackageManager()Landroid/content/pm/PackageManager;
aload 7
bipush 64
invokevirtual android/content/pm/PackageManager/resolveService(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;
ifnull L6
iconst_1
istore 2
L7:
iload 2
ifne L8
new com/amazon/android/b/g
dup
invokespecial com/amazon/android/b/g/<init>()V
athrow
L3:
iconst_0
istore 2
goto L4
L6:
iconst_0
istore 2
goto L7
L8:
getstatic com/amazon/android/framework/task/command/b/a Lcom/amazon/android/framework/util/KiwiLogger;
ldc "Found service on one or more packages"
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
aload 0
getfield com/amazon/android/framework/task/command/b/c Ljava/lang/String;
ifnull L9
getstatic com/amazon/android/framework/task/command/b/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Using previously determined package "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/amazon/android/framework/task/command/b/c Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
aload 0
getfield com/amazon/android/framework/task/command/b/c Ljava/lang/String;
invokestatic com/amazon/android/framework/task/command/b/a(Ljava/lang/String;)Landroid/content/Intent;
astore 7
L10:
aload 7
invokevirtual android/content/Intent/getComponent()Landroid/content/ComponentName;
invokevirtual android/content/ComponentName/getPackageName()Ljava/lang/String;
astore 8
getstatic com/amazon/android/framework/task/command/b/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Attempting to bind to service on "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 8
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
aload 0
getfield com/amazon/android/framework/task/command/b/f Landroid/app/Application;
aload 7
aload 0
getfield com/amazon/android/framework/task/command/b/i Landroid/content/ServiceConnection;
iconst_1
invokevirtual android/app/Application/bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
ifne L0
new com/amazon/android/b/f
dup
invokespecial com/amazon/android/b/f/<init>()V
athrow
L9:
getstatic com/amazon/android/framework/task/command/b/a Lcom/amazon/android/framework/util/KiwiLogger;
ldc "No previously determined package found, checking for suitable package."
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
aload 0
aload 0
getfield com/amazon/android/framework/task/command/b/f Landroid/app/Application;
invokevirtual android/app/Application/getPackageManager()Landroid/content/pm/PackageManager;
aload 7
bipush 64
invokevirtual android/content/pm/PackageManager/queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;
invokespecial com/amazon/android/framework/task/command/b/a(Ljava/util/List;)Landroid/content/Intent;
astore 8
aload 8
astore 7
aload 8
ifnonnull L10
getstatic com/amazon/android/framework/task/command/b/a Lcom/amazon/android/framework/util/KiwiLogger;
ldc "No app with valid signature was providing our service."
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
new com/amazon/android/b/g
dup
invokespecial com/amazon/android/b/g/<init>()V
athrow
L0:
getstatic com/amazon/android/framework/task/command/b/a Lcom/amazon/android/framework/util/KiwiLogger;
ldc "Blocking while service is being bound!!"
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
aload 0
aload 0
getfield com/amazon/android/framework/task/command/b/e Ljava/util/concurrent/BlockingQueue;
invokeinterface java/util/concurrent/BlockingQueue/take()Ljava/lang/Object; 0
checkcast com/amazon/venezia/command/h
putfield com/amazon/android/framework/task/command/b/b Lcom/amazon/venezia/command/h;
getstatic com/amazon/android/framework/task/command/b/a Lcom/amazon/android/framework/util/KiwiLogger;
ldc "service bound, returning!!"
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L1
invokestatic java/lang/System/currentTimeMillis()J
lstore 5
getstatic com/amazon/android/framework/task/command/b/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Kiwi.BindService Time: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
lload 5
lload 3
lsub
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L1:
aload 0
getfield com/amazon/android/framework/task/command/b/h Lcom/amazon/android/o/a;
ldc "PACKAGE"
aload 8
invokevirtual com/amazon/android/o/a/a(Ljava/lang/String;Ljava/lang/Object;)V
aload 0
aload 8
putfield com/amazon/android/framework/task/command/b/c Ljava/lang/String;
L5:
aload 0
getfield com/amazon/android/framework/task/command/b/h Lcom/amazon/android/o/a;
ldc "COMMAND"
aload 1
invokeinterface com/amazon/venezia/command/w/a()Ljava/lang/String; 0
invokevirtual com/amazon/android/o/a/a(Ljava/lang/String;Ljava/lang/Object;)V
aload 0
getfield com/amazon/android/framework/task/command/b/b Lcom/amazon/venezia/command/h;
aload 1
aload 0
getfield com/amazon/android/framework/task/command/b/j Lcom/amazon/venezia/command/f;
invokeinterface com/amazon/venezia/command/h/a(Lcom/amazon/venezia/command/w;Lcom/amazon/venezia/command/f;)V 2
aload 0
invokespecial com/amazon/android/framework/task/command/b/d()Lcom/amazon/android/framework/task/command/l;
areturn
L2:
astore 1
invokestatic java/lang/Thread/currentThread()Ljava/lang/Thread;
invokevirtual java/lang/Thread/interrupt()V
new com/amazon/android/b/f
dup
invokespecial com/amazon/android/b/f/<init>()V
athrow
.limit locals 9
.limit stack 6
.end method

.method public final a()Ljava/lang/String;
aload 0
getfield com/amazon/android/framework/task/command/b/c Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final b()V
getstatic com/amazon/android/framework/task/command/b/a Lcom/amazon/android/framework/util/KiwiLogger;
astore 2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Finishing CommandServiceClient, unbinding service: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
astore 3
aload 0
getfield com/amazon/android/framework/task/command/b/b Lcom/amazon/venezia/command/h;
ifnull L0
iconst_1
istore 1
L1:
aload 2
aload 3
iload 1
invokevirtual java/lang/StringBuilder/append(Z)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
aload 0
getfield com/amazon/android/framework/task/command/b/b Lcom/amazon/venezia/command/h;
ifnull L2
aload 0
getfield com/amazon/android/framework/task/command/b/f Landroid/app/Application;
aload 0
getfield com/amazon/android/framework/task/command/b/i Landroid/content/ServiceConnection;
invokevirtual android/app/Application/unbindService(Landroid/content/ServiceConnection;)V
aload 0
aconst_null
putfield com/amazon/android/framework/task/command/b/b Lcom/amazon/venezia/command/h;
L2:
return
L0:
iconst_0
istore 1
goto L1
.limit locals 4
.limit stack 3
.end method
