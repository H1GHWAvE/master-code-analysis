.bytecode 50.0
.class public final synchronized com/amazon/android/framework/task/pipeline/e
.super java/lang/Object
.implements com/amazon/android/framework/resource/b
.implements com/amazon/android/framework/task/pipeline/f

.field private static final 'a' Lcom/amazon/android/framework/util/KiwiLogger;

.field private 'b' Lcom/amazon/android/framework/context/ContextManager;
.annotation visible Lcom/amazon/android/framework/resource/Resource;
.end annotation
.end field

.field private 'c' Lcom/amazon/android/n/g;
.annotation visible Lcom/amazon/android/framework/resource/Resource;
.end annotation
.end field

.field private 'd' Lcom/amazon/android/framework/task/pipeline/f;

.field private 'e' Lcom/amazon/android/framework/task/pipeline/f;

.field private 'f' Ljava/util/List;

.method static <clinit>()V
new com/amazon/android/framework/util/KiwiLogger
dup
ldc "ForegroundTaskPipeline"
invokespecial com/amazon/android/framework/util/KiwiLogger/<init>(Ljava/lang/String;)V
putstatic com/amazon/android/framework/task/pipeline/e/a Lcom/amazon/android/framework/util/KiwiLogger;
return
.limit locals 0
.limit stack 3
.end method

.method public <init>(Lcom/amazon/android/framework/task/pipeline/f;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield com/amazon/android/framework/task/pipeline/e/f Ljava/util/List;
aload 0
ldc "KIWI_UI"
invokestatic com/amazon/android/framework/task/pipeline/a/b(Ljava/lang/String;)Lcom/amazon/android/framework/task/pipeline/a;
putfield com/amazon/android/framework/task/pipeline/e/d Lcom/amazon/android/framework/task/pipeline/f;
aload 0
aload 1
putfield com/amazon/android/framework/task/pipeline/e/e Lcom/amazon/android/framework/task/pipeline/f;
return
.limit locals 2
.limit stack 3
.end method

.method private a(Lcom/amazon/android/framework/task/Task;Z)V
aload 0
getfield com/amazon/android/framework/task/pipeline/e/b Lcom/amazon/android/framework/context/ContextManager;
invokeinterface com/amazon/android/framework/context/ContextManager/isVisible()Z 0
ifeq L0
iload 2
ifeq L1
aload 0
getfield com/amazon/android/framework/task/pipeline/e/d Lcom/amazon/android/framework/task/pipeline/f;
aload 1
invokeinterface com/amazon/android/framework/task/pipeline/f/b(Lcom/amazon/android/framework/task/Task;)V 1
return
L1:
aload 0
getfield com/amazon/android/framework/task/pipeline/e/d Lcom/amazon/android/framework/task/pipeline/f;
aload 1
invokeinterface com/amazon/android/framework/task/pipeline/f/a(Lcom/amazon/android/framework/task/Task;)V 1
return
L0:
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L2
getstatic com/amazon/android/framework/task/pipeline/e/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "No UI visible to execute task: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc ", placing into pending queue until task "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "is visible"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L2:
aload 0
getfield com/amazon/android/framework/task/pipeline/e/f Ljava/util/List;
aload 1
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
return
.limit locals 3
.limit stack 3
.end method

.method static synthetic a(Lcom/amazon/android/framework/task/pipeline/e;)V
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L0
getstatic com/amazon/android/framework/task/pipeline/e/a Lcom/amazon/android/framework/util/KiwiLogger;
ldc "Activity resumed, scheduling tasks on UI thread"
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L0:
aload 0
getfield com/amazon/android/framework/task/pipeline/e/f Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 1
L1:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L2
aload 0
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/amazon/android/framework/task/Task
iconst_1
invokespecial com/amazon/android/framework/task/pipeline/e/a(Lcom/amazon/android/framework/task/Task;Z)V
goto L1
L2:
aload 0
getfield com/amazon/android/framework/task/pipeline/e/f Ljava/util/List;
invokeinterface java/util/List/clear()V 0
return
.limit locals 2
.limit stack 3
.end method

.method static synthetic a(Lcom/amazon/android/framework/task/pipeline/e;Lcom/amazon/android/framework/task/Task;)V
aload 0
aload 1
iconst_1
invokespecial com/amazon/android/framework/task/pipeline/e/a(Lcom/amazon/android/framework/task/Task;Z)V
return
.limit locals 2
.limit stack 3
.end method

.method private c(Lcom/amazon/android/framework/task/Task;)Lcom/amazon/android/framework/task/Task;
new com/amazon/android/framework/task/pipeline/c
dup
aload 0
aload 1
invokespecial com/amazon/android/framework/task/pipeline/c/<init>(Lcom/amazon/android/framework/task/pipeline/e;Lcom/amazon/android/framework/task/Task;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method public final a()V
aload 0
getfield com/amazon/android/framework/task/pipeline/e/d Lcom/amazon/android/framework/task/pipeline/f;
invokeinterface com/amazon/android/framework/task/pipeline/f/a()V 0
aload 0
getfield com/amazon/android/framework/task/pipeline/e/e Lcom/amazon/android/framework/task/pipeline/f;
invokeinterface com/amazon/android/framework/task/pipeline/f/a()V 0
aload 0
getfield com/amazon/android/framework/task/pipeline/e/f Ljava/util/List;
invokeinterface java/util/List/clear()V 0
return
.limit locals 1
.limit stack 1
.end method

.method public final a(Lcom/amazon/android/framework/task/Task;)V
aload 0
aload 1
iconst_0
invokespecial com/amazon/android/framework/task/pipeline/e/a(Lcom/amazon/android/framework/task/Task;Z)V
return
.limit locals 2
.limit stack 3
.end method

.method public final a(Lcom/amazon/android/framework/task/Task;J)V
aload 0
getfield com/amazon/android/framework/task/pipeline/e/e Lcom/amazon/android/framework/task/pipeline/f;
aload 0
aload 1
invokespecial com/amazon/android/framework/task/pipeline/e/c(Lcom/amazon/android/framework/task/Task;)Lcom/amazon/android/framework/task/Task;
lload 2
invokeinterface com/amazon/android/framework/task/pipeline/f/a(Lcom/amazon/android/framework/task/Task;J)V 3
return
.limit locals 4
.limit stack 4
.end method

.method public final a(Lcom/amazon/android/framework/task/Task;Ljava/util/Date;)V
aload 0
getfield com/amazon/android/framework/task/pipeline/e/e Lcom/amazon/android/framework/task/pipeline/f;
aload 0
aload 1
invokespecial com/amazon/android/framework/task/pipeline/e/c(Lcom/amazon/android/framework/task/Task;)Lcom/amazon/android/framework/task/Task;
aload 2
invokeinterface com/amazon/android/framework/task/pipeline/f/a(Lcom/amazon/android/framework/task/Task;Ljava/util/Date;)V 2
return
.limit locals 3
.limit stack 3
.end method

.method public final b(Lcom/amazon/android/framework/task/Task;)V
aload 0
aload 1
iconst_1
invokespecial com/amazon/android/framework/task/pipeline/e/a(Lcom/amazon/android/framework/task/Task;Z)V
return
.limit locals 2
.limit stack 3
.end method

.method public final onResourcesPopulated()V
aload 0
getfield com/amazon/android/framework/task/pipeline/e/c Lcom/amazon/android/n/g;
new com/amazon/android/framework/task/pipeline/b
dup
aload 0
invokespecial com/amazon/android/framework/task/pipeline/b/<init>(Lcom/amazon/android/framework/task/pipeline/e;)V
invokeinterface com/amazon/android/n/g/a(Lcom/amazon/android/n/c;)V 1
return
.limit locals 1
.limit stack 4
.end method
