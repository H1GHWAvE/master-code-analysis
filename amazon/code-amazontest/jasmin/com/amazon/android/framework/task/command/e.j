.bytecode 50.0
.class public final synchronized com/amazon/android/framework/task/command/e
.super java/lang/Object

.field private static final 'a' Lcom/amazon/android/framework/util/KiwiLogger;

.field private 'b' Landroid/app/Application;
.annotation visible Lcom/amazon/android/framework/resource/Resource;
.end annotation
.end field

.field private 'c' Lcom/amazon/android/m/c;
.annotation visible Lcom/amazon/android/framework/resource/Resource;
.end annotation
.end field

.method static <clinit>()V
new com/amazon/android/framework/util/KiwiLogger
dup
ldc "CommandResultVerifier"
invokespecial com/amazon/android/framework/util/KiwiLogger/<init>(Ljava/lang/String;)V
putstatic com/amazon/android/framework/task/command/e/a Lcom/amazon/android/framework/util/KiwiLogger;
return
.limit locals 0
.limit stack 3
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private a(Ljava/lang/String;)Landroid/content/pm/PackageInfo;
.throws com/amazon/android/b/g
.catch android/content/pm/PackageManager$NameNotFoundException from L0 to L1 using L2
aload 0
getfield com/amazon/android/framework/task/command/e/b Landroid/app/Application;
invokevirtual android/app/Application/getPackageManager()Landroid/content/pm/PackageManager;
astore 2
L0:
aload 2
aload 1
bipush 64
invokevirtual android/content/pm/PackageManager/getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
astore 1
L1:
aload 1
areturn
L2:
astore 1
getstatic com/amazon/android/framework/task/command/e/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "getPackageInfo() caught exception"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
new com/amazon/android/b/g
dup
invokespecial com/amazon/android/b/g/<init>()V
athrow
.limit locals 3
.limit stack 3
.end method

.method private static a(Landroid/content/pm/Signature;)Ljava/lang/String;
.throws java/security/cert/CertificateException
new java/io/ByteArrayInputStream
dup
aload 0
invokevirtual android/content/pm/Signature/toByteArray()[B
invokespecial java/io/ByteArrayInputStream/<init>([B)V
astore 0
ldc "X509"
invokestatic java/security/cert/CertificateFactory/getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;
aload 0
invokevirtual java/security/cert/CertificateFactory/generateCertificate(Ljava/io/InputStream;)Ljava/security/cert/Certificate;
checkcast java/security/cert/X509Certificate
invokevirtual java/security/cert/X509Certificate/getSignature()[B
invokestatic com/amazon/mas/kiwi/util/Base64/encodeBytes([B)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 3
.end method

.method private a(Ljava/lang/String;Landroid/content/pm/Signature;)Z
.throws com/amazon/android/h/a
.catch java/security/cert/CertificateException from L0 to L1 using L2
L0:
aload 2
invokestatic com/amazon/android/framework/task/command/e/a(Landroid/content/pm/Signature;)Ljava/lang/String;
astore 3
L1:
aload 3
aload 1
aload 0
getfield com/amazon/android/framework/task/command/e/c Lcom/amazon/android/m/c;
invokevirtual com/amazon/android/m/c/a()Ljava/security/PublicKey;
invokestatic com/amazon/android/m/a/a(Ljava/lang/String;Ljava/lang/String;Ljava/security/PublicKey;)Z
ireturn
L2:
astore 1
getstatic com/amazon/android/framework/util/KiwiLogger/ERROR_ON Z
ifeq L3
getstatic com/amazon/android/framework/task/command/e/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Failed to extract fingerprint from signature: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/error(Ljava/lang/String;)V
L3:
iconst_0
ireturn
.limit locals 4
.limit stack 3
.end method

.method private static b(Ljava/lang/String;Landroid/content/pm/Signature;)Z
.catch java/security/cert/CertificateException from L0 to L1 using L2
.catch java/security/cert/CertificateException from L3 to L4 using L2
iconst_0
istore 2
L0:
aload 0
aload 1
invokestatic com/amazon/android/framework/task/command/e/a(Landroid/content/pm/Signature;)Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
istore 3
L1:
iload 3
istore 2
L3:
getstatic com/amazon/android/framework/task/command/e/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Signature valid: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 3
invokevirtual java/lang/StringBuilder/append(Z)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L4:
iload 3
ireturn
L2:
astore 0
getstatic com/amazon/android/framework/task/command/e/a Lcom/amazon/android/framework/util/KiwiLogger;
ldc "Failed to extract fingerprint from signature"
invokevirtual com/amazon/android/framework/util/KiwiLogger/error(Ljava/lang/String;)V
iload 2
ireturn
.limit locals 4
.limit stack 3
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
.throws com/amazon/android/framework/exception/KiwiException
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L0
getstatic com/amazon/android/framework/task/command/e/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Verifying auth token: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L0:
aload 0
aload 2
invokespecial com/amazon/android/framework/task/command/e/a(Ljava/lang/String;)Landroid/content/pm/PackageInfo;
getfield android/content/pm/PackageInfo/signatures [Landroid/content/pm/Signature;
astore 2
aload 2
arraylength
istore 4
iconst_0
istore 3
L1:
iload 3
iload 4
if_icmpge L2
aload 0
aload 1
aload 2
iload 3
aaload
invokespecial com/amazon/android/framework/task/command/e/a(Ljava/lang/String;Landroid/content/pm/Signature;)Z
ifeq L3
return
L3:
iload 3
iconst_1
iadd
istore 3
goto L1
L2:
new com/amazon/android/b/d
dup
invokespecial com/amazon/android/b/d/<init>()V
athrow
.limit locals 5
.limit stack 4
.end method

.method public final a(Ljava/lang/String;Ljava/util/List;)Z
.catch com/amazon/android/b/g from L0 to L1 using L2
.catch com/amazon/android/b/g from L3 to L4 using L2
.catch com/amazon/android/b/g from L4 to L5 using L2
getstatic com/amazon/android/framework/task/command/e/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "checkSignatures("
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ", "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L0:
aload 0
aload 1
invokespecial com/amazon/android/framework/task/command/e/a(Ljava/lang/String;)Landroid/content/pm/PackageInfo;
getfield android/content/pm/PackageInfo/signatures [Landroid/content/pm/Signature;
astore 1
aload 1
arraylength
istore 4
L1:
iconst_0
istore 3
L6:
iload 3
iload 4
if_icmpge L7
aload 1
iload 3
aaload
astore 6
L3:
aload 2
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 7
L4:
aload 7
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L8
aload 7
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/String
aload 6
invokestatic com/amazon/android/framework/task/command/e/b(Ljava/lang/String;Landroid/content/pm/Signature;)Z
istore 5
L5:
iload 5
ifeq L4
iconst_1
ireturn
L8:
iload 3
iconst_1
iadd
istore 3
goto L6
L2:
astore 1
getstatic com/amazon/android/framework/task/command/e/a Lcom/amazon/android/framework/util/KiwiLogger;
ldc "isPackageSignatureValid: caught exception while checking"
aload 1
invokevirtual com/amazon/android/framework/util/KiwiLogger/error(Ljava/lang/String;Ljava/lang/Throwable;)V
L7:
iconst_0
ireturn
.limit locals 8
.limit stack 3
.end method
