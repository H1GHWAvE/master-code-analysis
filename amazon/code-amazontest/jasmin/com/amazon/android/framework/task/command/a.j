.bytecode 50.0
.class public final synchronized enum com/amazon/android/framework/task/command/a
.super java/lang/Enum

.field public static final enum 'a' Lcom/amazon/android/framework/task/command/a;

.field public static final enum 'b' Lcom/amazon/android/framework/task/command/a;

.field public static final enum 'c' Lcom/amazon/android/framework/task/command/a;

.field private static final synthetic 'd' [Lcom/amazon/android/framework/task/command/a;

.method static <clinit>()V
new com/amazon/android/framework/task/command/a
dup
ldc "EXPIRATION_DURATION_ELAPSED"
iconst_0
invokespecial com/amazon/android/framework/task/command/a/<init>(Ljava/lang/String;I)V
putstatic com/amazon/android/framework/task/command/a/a Lcom/amazon/android/framework/task/command/a;
new com/amazon/android/framework/task/command/a
dup
ldc "APP_NOT_COMPATIBLE"
iconst_1
invokespecial com/amazon/android/framework/task/command/a/<init>(Ljava/lang/String;I)V
putstatic com/amazon/android/framework/task/command/a/b Lcom/amazon/android/framework/task/command/a;
new com/amazon/android/framework/task/command/a
dup
ldc "ACTION_CANCELED"
iconst_2
invokespecial com/amazon/android/framework/task/command/a/<init>(Ljava/lang/String;I)V
putstatic com/amazon/android/framework/task/command/a/c Lcom/amazon/android/framework/task/command/a;
iconst_3
anewarray com/amazon/android/framework/task/command/a
dup
iconst_0
getstatic com/amazon/android/framework/task/command/a/a Lcom/amazon/android/framework/task/command/a;
aastore
dup
iconst_1
getstatic com/amazon/android/framework/task/command/a/b Lcom/amazon/android/framework/task/command/a;
aastore
dup
iconst_2
getstatic com/amazon/android/framework/task/command/a/c Lcom/amazon/android/framework/task/command/a;
aastore
putstatic com/amazon/android/framework/task/command/a/d [Lcom/amazon/android/framework/task/command/a;
return
.limit locals 0
.limit stack 4
.end method

.method private <init>(Ljava/lang/String;I)V
aload 0
aload 1
iload 2
invokespecial java/lang/Enum/<init>(Ljava/lang/String;I)V
return
.limit locals 3
.limit stack 3
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/amazon/android/framework/task/command/a;
ldc com/amazon/android/framework/task/command/a
aload 0
invokestatic java/lang/Enum/valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;
checkcast com/amazon/android/framework/task/command/a
areturn
.limit locals 1
.limit stack 2
.end method

.method public static values()[Lcom/amazon/android/framework/task/command/a;
getstatic com/amazon/android/framework/task/command/a/d [Lcom/amazon/android/framework/task/command/a;
invokevirtual [Lcom/amazon/android/framework/task/command/a;/clone()Ljava/lang/Object;
checkcast [Lcom/amazon/android/framework/task/command/a;
areturn
.limit locals 0
.limit stack 1
.end method
