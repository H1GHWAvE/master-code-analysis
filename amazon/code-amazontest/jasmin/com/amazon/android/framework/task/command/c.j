.bytecode 50.0
.class public final synchronized com/amazon/android/framework/task/command/c
.super com/amazon/android/framework/prompt/Prompt

.field private static final 'a' Lcom/amazon/android/framework/util/KiwiLogger;

.field private final 'b' Ljava/lang/Thread;

.field private final 'c' Lcom/amazon/android/framework/task/command/f;

.field private final 'd' Ljava/util/concurrent/BlockingQueue;

.method static <clinit>()V
new com/amazon/android/framework/util/KiwiLogger
dup
ldc "DecisionDialog"
invokespecial com/amazon/android/framework/util/KiwiLogger/<init>(Ljava/lang/String;)V
putstatic com/amazon/android/framework/task/command/c/a Lcom/amazon/android/framework/util/KiwiLogger;
return
.limit locals 0
.limit stack 3
.end method

.method public <init>(Lcom/amazon/venezia/command/r;)V
.throws android/os/RemoteException
aload 0
invokespecial com/amazon/android/framework/prompt/Prompt/<init>()V
aload 0
new java/util/concurrent/LinkedBlockingQueue
dup
invokespecial java/util/concurrent/LinkedBlockingQueue/<init>()V
putfield com/amazon/android/framework/task/command/c/d Ljava/util/concurrent/BlockingQueue;
aload 0
invokestatic java/lang/Thread/currentThread()Ljava/lang/Thread;
putfield com/amazon/android/framework/task/command/c/b Ljava/lang/Thread;
aload 0
new com/amazon/android/framework/task/command/f
dup
aload 1
invokespecial com/amazon/android/framework/task/command/f/<init>(Lcom/amazon/venezia/command/r;)V
putfield com/amazon/android/framework/task/command/c/c Lcom/amazon/android/framework/task/command/f;
return
.limit locals 2
.limit stack 4
.end method

.method private static a(Landroid/app/Activity;)Landroid/content/pm/ActivityInfo;
.catch android/content/pm/PackageManager$NameNotFoundException from L0 to L1 using L2
L0:
aload 0
invokevirtual android/app/Activity/getPackageManager()Landroid/content/pm/PackageManager;
aload 0
invokevirtual android/app/Activity/getComponentName()Landroid/content/ComponentName;
sipush 128
invokevirtual android/content/pm/PackageManager/getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;
astore 1
L1:
aload 1
areturn
L2:
astore 1
getstatic com/amazon/android/framework/util/KiwiLogger/ERROR_ON Z
ifeq L3
getstatic com/amazon/android/framework/task/command/c/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Unable to get info for activity: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/error(Ljava/lang/String;)V
L3:
aconst_null
areturn
.limit locals 2
.limit stack 3
.end method

.method private a(Landroid/app/AlertDialog;Lcom/amazon/android/framework/task/command/m;I)V
aload 2
ifnonnull L0
return
L0:
aload 1
iload 3
aload 2
getfield com/amazon/android/framework/task/command/m/b Ljava/lang/String;
new com/amazon/android/framework/task/command/d
dup
aload 0
aload 2
invokespecial com/amazon/android/framework/task/command/d/<init>(Lcom/amazon/android/framework/task/command/c;Lcom/amazon/android/framework/task/command/m;)V
invokevirtual android/app/AlertDialog/setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V
return
.limit locals 4
.limit stack 7
.end method

.method static synthetic a(Lcom/amazon/android/framework/task/command/c;)Z
aload 0
invokevirtual com/amazon/android/framework/task/command/c/dismiss()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static a(Lcom/amazon/android/framework/task/command/m;)Z
aload 0
ifnull L0
aload 0
getfield com/amazon/android/framework/task/command/m/c Landroid/content/Intent;
ifnull L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic b()Lcom/amazon/android/framework/util/KiwiLogger;
getstatic com/amazon/android/framework/task/command/c/a Lcom/amazon/android/framework/util/KiwiLogger;
areturn
.limit locals 0
.limit stack 1
.end method

.method static synthetic b(Lcom/amazon/android/framework/task/command/c;)Ljava/util/concurrent/BlockingQueue;
aload 0
getfield com/amazon/android/framework/task/command/c/d Ljava/util/concurrent/BlockingQueue;
areturn
.limit locals 1
.limit stack 1
.end method

.method private c()Lcom/amazon/venezia/command/n;
.throws com/amazon/android/b/c
.catch java/lang/InterruptedException from L0 to L1 using L2
.catch java/lang/InterruptedException from L1 to L3 using L2
L0:
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L1
getstatic com/amazon/android/framework/task/command/c/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Blocking while awaiting customer decision: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokestatic java/lang/Thread/currentThread()Ljava/lang/Thread;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L1:
aload 0
getfield com/amazon/android/framework/task/command/c/d Ljava/util/concurrent/BlockingQueue;
invokeinterface java/util/concurrent/BlockingQueue/take()Ljava/lang/Object; 0
checkcast com/amazon/android/framework/task/command/m
getfield com/amazon/android/framework/task/command/m/a Lcom/amazon/venezia/command/n;
astore 1
L3:
aload 1
areturn
L2:
astore 1
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L4
getstatic com/amazon/android/framework/task/command/c/a Lcom/amazon/android/framework/util/KiwiLogger;
ldc "Interrupted while awaiting decision, throwing decision expired!"
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L4:
aload 0
invokevirtual com/amazon/android/framework/task/command/c/getExpirationReason()Lcom/amazon/android/framework/prompt/d;
getstatic com/amazon/android/framework/prompt/d/a Lcom/amazon/android/framework/prompt/d;
if_acmpne L5
getstatic com/amazon/android/framework/task/command/a/b Lcom/amazon/android/framework/task/command/a;
astore 1
L6:
new com/amazon/android/b/c
dup
aload 1
invokespecial com/amazon/android/b/c/<init>(Lcom/amazon/android/framework/task/command/a;)V
athrow
L5:
getstatic com/amazon/android/framework/task/command/a/a Lcom/amazon/android/framework/task/command/a;
astore 1
goto L6
.limit locals 2
.limit stack 3
.end method

.method public final a()Lcom/amazon/venezia/command/n;
.throws com/amazon/android/b/c
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L0
getstatic com/amazon/android/framework/task/command/c/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "GetCustomerDecision: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/amazon/android/framework/task/command/c/b Ljava/lang/Thread;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L0:
aload 0
invokespecial com/amazon/android/framework/task/command/c/c()Lcom/amazon/venezia/command/n;
areturn
.limit locals 1
.limit stack 3
.end method

.method public final doCompatibilityCheck(Landroid/app/Activity;)Z
aload 0
getfield com/amazon/android/framework/task/command/c/c Lcom/amazon/android/framework/task/command/f;
getfield com/amazon/android/framework/task/command/f/d Lcom/amazon/android/framework/task/command/m;
invokestatic com/amazon/android/framework/task/command/c/a(Lcom/amazon/android/framework/task/command/m;)Z
ifne L0
aload 0
getfield com/amazon/android/framework/task/command/c/c Lcom/amazon/android/framework/task/command/f;
getfield com/amazon/android/framework/task/command/f/e Lcom/amazon/android/framework/task/command/m;
invokestatic com/amazon/android/framework/task/command/c/a(Lcom/amazon/android/framework/task/command/m;)Z
ifne L0
aload 0
getfield com/amazon/android/framework/task/command/c/c Lcom/amazon/android/framework/task/command/f;
getfield com/amazon/android/framework/task/command/f/f Lcom/amazon/android/framework/task/command/m;
invokestatic com/amazon/android/framework/task/command/c/a(Lcom/amazon/android/framework/task/command/m;)Z
ifeq L1
L0:
iconst_1
istore 2
L2:
iload 2
ifne L3
iconst_1
ireturn
L1:
iconst_0
istore 2
goto L2
L3:
aload 1
invokestatic com/amazon/android/framework/task/command/c/a(Landroid/app/Activity;)Landroid/content/pm/ActivityInfo;
astore 1
aload 1
ifnonnull L4
iconst_0
ireturn
L4:
aload 1
getfield android/content/pm/ActivityInfo/launchMode I
iconst_3
if_icmpne L5
iconst_1
istore 3
L6:
getstatic com/amazon/android/framework/task/command/c/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Single instance: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 3
invokevirtual java/lang/StringBuilder/append(Z)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
aload 1
getfield android/content/pm/ActivityInfo/flags I
iconst_2
iand
ifeq L7
iconst_1
istore 4
L8:
getstatic com/amazon/android/framework/task/command/c/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Finish on task launch:"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 4
invokevirtual java/lang/StringBuilder/append(Z)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
aload 1
getfield android/content/pm/ActivityInfo/flags I
sipush 128
iand
ifeq L9
iconst_1
istore 5
L10:
getstatic com/amazon/android/framework/task/command/c/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "No History: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 5
invokevirtual java/lang/StringBuilder/append(Z)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
iload 3
ifne L11
iload 4
ifne L11
iload 5
ifne L11
iconst_1
ireturn
L5:
iconst_0
istore 3
goto L6
L7:
iconst_0
istore 4
goto L8
L9:
iconst_0
istore 5
goto L10
L11:
iconst_0
ireturn
.limit locals 6
.limit stack 3
.end method

.method public final doCreate(Landroid/app/Activity;)Landroid/app/Dialog;
new android/app/AlertDialog$Builder
dup
aload 1
invokespecial android/app/AlertDialog$Builder/<init>(Landroid/content/Context;)V
astore 1
aload 1
aload 0
getfield com/amazon/android/framework/task/command/c/c Lcom/amazon/android/framework/task/command/f;
getfield com/amazon/android/framework/task/command/f/a Ljava/lang/String;
invokevirtual android/app/AlertDialog$Builder/setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;
aload 0
getfield com/amazon/android/framework/task/command/c/c Lcom/amazon/android/framework/task/command/f;
getfield com/amazon/android/framework/task/command/f/b Ljava/lang/String;
invokevirtual android/app/AlertDialog$Builder/setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;
iconst_0
invokevirtual android/app/AlertDialog$Builder/setCancelable(Z)Landroid/app/AlertDialog$Builder;
pop
aload 1
invokevirtual android/app/AlertDialog$Builder/create()Landroid/app/AlertDialog;
astore 1
aload 0
aload 1
aload 0
getfield com/amazon/android/framework/task/command/c/c Lcom/amazon/android/framework/task/command/f;
getfield com/amazon/android/framework/task/command/f/d Lcom/amazon/android/framework/task/command/m;
iconst_m1
invokespecial com/amazon/android/framework/task/command/c/a(Landroid/app/AlertDialog;Lcom/amazon/android/framework/task/command/m;I)V
aload 0
aload 1
aload 0
getfield com/amazon/android/framework/task/command/c/c Lcom/amazon/android/framework/task/command/f;
getfield com/amazon/android/framework/task/command/f/e Lcom/amazon/android/framework/task/command/m;
bipush -3
invokespecial com/amazon/android/framework/task/command/c/a(Landroid/app/AlertDialog;Lcom/amazon/android/framework/task/command/m;I)V
aload 0
aload 1
aload 0
getfield com/amazon/android/framework/task/command/c/c Lcom/amazon/android/framework/task/command/f;
getfield com/amazon/android/framework/task/command/f/f Lcom/amazon/android/framework/task/command/m;
bipush -2
invokespecial com/amazon/android/framework/task/command/c/a(Landroid/app/AlertDialog;Lcom/amazon/android/framework/task/command/m;I)V
aload 1
areturn
.limit locals 2
.limit stack 4
.end method

.method protected final doExpiration(Lcom/amazon/android/framework/prompt/d;)V
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L0
getstatic com/amazon/android/framework/task/command/c/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Expiring Decision Dialog: Thread: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokestatic java/lang/Thread/currentThread()Ljava/lang/Thread;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L0:
aload 0
getfield com/amazon/android/framework/task/command/c/b Ljava/lang/Thread;
invokevirtual java/lang/Thread/interrupt()V
return
.limit locals 2
.limit stack 3
.end method

.method protected final getExpirationDurationInSeconds()J
aload 0
getfield com/amazon/android/framework/task/command/c/c Lcom/amazon/android/framework/task/command/f;
getfield com/amazon/android/framework/task/command/f/c J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public final toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "DecisionDialog: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/amazon/android/framework/task/command/c/c Lcom/amazon/android/framework/task/command/f;
getfield com/amazon/android/framework/task/command/f/a Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method
