.bytecode 50.0
.class public final synchronized com/amazon/android/framework/task/pipeline/a
.super java/lang/Object
.implements com/amazon/android/framework/task/pipeline/f

.field private static final 'a' Lcom/amazon/android/framework/util/KiwiLogger;

.field private final 'b' Landroid/os/Handler;

.field private final 'c' Ljava/util/Set;

.field private final 'd' Ljava/lang/String;

.method static <clinit>()V
new com/amazon/android/framework/util/KiwiLogger
dup
ldc "SimpleTaskPipeline"
invokespecial com/amazon/android/framework/util/KiwiLogger/<init>(Ljava/lang/String;)V
putstatic com/amazon/android/framework/task/pipeline/a/a Lcom/amazon/android/framework/util/KiwiLogger;
return
.limit locals 0
.limit stack 3
.end method

.method private <init>(Landroid/os/HandlerThread;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/util/HashSet
dup
invokespecial java/util/HashSet/<init>()V
invokestatic java/util/Collections/synchronizedSet(Ljava/util/Set;)Ljava/util/Set;
putfield com/amazon/android/framework/task/pipeline/a/c Ljava/util/Set;
aload 0
aload 1
invokevirtual android/os/HandlerThread/getName()Ljava/lang/String;
putfield com/amazon/android/framework/task/pipeline/a/d Ljava/lang/String;
aload 1
invokevirtual android/os/HandlerThread/start()V
aload 0
new android/os/Handler
dup
aload 1
invokevirtual android/os/HandlerThread/getLooper()Landroid/os/Looper;
invokespecial android/os/Handler/<init>(Landroid/os/Looper;)V
putfield com/amazon/android/framework/task/pipeline/a/b Landroid/os/Handler;
return
.limit locals 2
.limit stack 4
.end method

.method private <init>(Ljava/lang/String;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/util/HashSet
dup
invokespecial java/util/HashSet/<init>()V
invokestatic java/util/Collections/synchronizedSet(Ljava/util/Set;)Ljava/util/Set;
putfield com/amazon/android/framework/task/pipeline/a/c Ljava/util/Set;
aload 0
aload 1
putfield com/amazon/android/framework/task/pipeline/a/d Ljava/lang/String;
aload 0
new android/os/Handler
dup
invokespecial android/os/Handler/<init>()V
putfield com/amazon/android/framework/task/pipeline/a/b Landroid/os/Handler;
return
.limit locals 2
.limit stack 3
.end method

.method public static a(Ljava/lang/String;)Lcom/amazon/android/framework/task/pipeline/a;
new com/amazon/android/framework/task/pipeline/a
dup
new android/os/HandlerThread
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "KIWI_"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial android/os/HandlerThread/<init>(Ljava/lang/String;)V
invokespecial com/amazon/android/framework/task/pipeline/a/<init>(Landroid/os/HandlerThread;)V
areturn
.limit locals 1
.limit stack 6
.end method

.method static synthetic a(Lcom/amazon/android/framework/task/pipeline/a;)Ljava/util/Set;
aload 0
getfield com/amazon/android/framework/task/pipeline/a/c Ljava/util/Set;
areturn
.limit locals 1
.limit stack 1
.end method

.method public static b(Ljava/lang/String;)Lcom/amazon/android/framework/task/pipeline/a;
new com/amazon/android/framework/task/pipeline/a
dup
aload 0
invokespecial com/amazon/android/framework/task/pipeline/a/<init>(Ljava/lang/String;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method static synthetic b()Lcom/amazon/android/framework/util/KiwiLogger;
getstatic com/amazon/android/framework/task/pipeline/a/a Lcom/amazon/android/framework/util/KiwiLogger;
areturn
.limit locals 0
.limit stack 1
.end method

.method static synthetic b(Lcom/amazon/android/framework/task/pipeline/a;)Ljava/lang/String;
aload 0
getfield com/amazon/android/framework/task/pipeline/a/d Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private c(Lcom/amazon/android/framework/task/Task;)Ljava/lang/Runnable;
new com/amazon/android/framework/task/pipeline/d
dup
aload 0
aload 1
invokespecial com/amazon/android/framework/task/pipeline/d/<init>(Lcom/amazon/android/framework/task/pipeline/a;Lcom/amazon/android/framework/task/Task;)V
astore 1
aload 0
getfield com/amazon/android/framework/task/pipeline/a/c Ljava/util/Set;
aload 1
invokeinterface java/util/Set/add(Ljava/lang/Object;)Z 1
pop
aload 1
areturn
.limit locals 2
.limit stack 4
.end method

.method public final a()V
aload 0
getfield com/amazon/android/framework/task/pipeline/a/c Ljava/util/Set;
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 1
L0:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/Runnable
astore 2
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L2
getstatic com/amazon/android/framework/task/pipeline/a/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
getfield com/amazon/android/framework/task/pipeline/a/d Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ": Removing callback: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L2:
aload 0
getfield com/amazon/android/framework/task/pipeline/a/b Landroid/os/Handler;
aload 2
invokevirtual android/os/Handler/removeCallbacks(Ljava/lang/Runnable;)V
goto L0
L1:
aload 0
getfield com/amazon/android/framework/task/pipeline/a/c Ljava/util/Set;
invokeinterface java/util/Set/clear()V 0
aload 0
getfield com/amazon/android/framework/task/pipeline/a/b Landroid/os/Handler;
invokevirtual android/os/Handler/getLooper()Landroid/os/Looper;
invokestatic android/os/Looper/getMainLooper()Landroid/os/Looper;
if_acmpeq L3
aload 0
getfield com/amazon/android/framework/task/pipeline/a/b Landroid/os/Handler;
invokevirtual android/os/Handler/getLooper()Landroid/os/Looper;
invokevirtual android/os/Looper/getThread()Ljava/lang/Thread;
invokevirtual java/lang/Thread/isAlive()Z
ifeq L3
getstatic com/amazon/android/framework/task/pipeline/a/a Lcom/amazon/android/framework/util/KiwiLogger;
ldc "Interrupting looper thread!"
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
aload 0
getfield com/amazon/android/framework/task/pipeline/a/b Landroid/os/Handler;
invokevirtual android/os/Handler/getLooper()Landroid/os/Looper;
invokevirtual android/os/Looper/getThread()Ljava/lang/Thread;
invokevirtual java/lang/Thread/interrupt()V
getstatic com/amazon/android/framework/task/pipeline/a/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Quitting looper: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/amazon/android/framework/task/pipeline/a/b Landroid/os/Handler;
invokevirtual android/os/Handler/getLooper()Landroid/os/Looper;
invokevirtual android/os/Looper/getThread()Ljava/lang/Thread;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc ", "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/amazon/android/framework/task/pipeline/a/b Landroid/os/Handler;
invokevirtual android/os/Handler/getLooper()Landroid/os/Looper;
invokevirtual android/os/Looper/getThread()Ljava/lang/Thread;
invokevirtual java/lang/Thread/isAlive()Z
invokevirtual java/lang/StringBuilder/append(Z)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
aload 0
getfield com/amazon/android/framework/task/pipeline/a/b Landroid/os/Handler;
invokevirtual android/os/Handler/getLooper()Landroid/os/Looper;
invokevirtual android/os/Looper/quit()V
L3:
return
.limit locals 3
.limit stack 3
.end method

.method public final a(Lcom/amazon/android/framework/task/Task;)V
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L0
getstatic com/amazon/android/framework/task/pipeline/a/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Scheduling task: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L0:
aload 0
getfield com/amazon/android/framework/task/pipeline/a/b Landroid/os/Handler;
aload 0
aload 1
invokespecial com/amazon/android/framework/task/pipeline/a/c(Lcom/amazon/android/framework/task/Task;)Ljava/lang/Runnable;
invokevirtual android/os/Handler/post(Ljava/lang/Runnable;)Z
pop
return
.limit locals 2
.limit stack 3
.end method

.method public final a(Lcom/amazon/android/framework/task/Task;J)V
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L0
getstatic com/amazon/android/framework/task/pipeline/a/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
getfield com/amazon/android/framework/task/pipeline/a/d Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ": Scheduling task: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc ", with delay: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
lload 2
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L0:
aload 0
getfield com/amazon/android/framework/task/pipeline/a/b Landroid/os/Handler;
aload 0
aload 1
invokespecial com/amazon/android/framework/task/pipeline/a/c(Lcom/amazon/android/framework/task/Task;)Ljava/lang/Runnable;
lload 2
invokevirtual android/os/Handler/postDelayed(Ljava/lang/Runnable;J)Z
pop
return
.limit locals 4
.limit stack 4
.end method

.method public final a(Lcom/amazon/android/framework/task/Task;Ljava/util/Date;)V
invokestatic android/os/SystemClock/uptimeMillis()J
aload 2
invokevirtual java/util/Date/getTime()J
invokestatic java/lang/System/currentTimeMillis()J
lsub
ladd
lstore 3
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L0
getstatic com/amazon/android/framework/task/pipeline/a/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
getfield com/amazon/android/framework/task/pipeline/a/d Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ": Scheduling task: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc ", at time: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc ", System uptimeMillis: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokestatic java/lang/System/currentTimeMillis()J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc ", uptimeMillis: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
lload 3
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L0:
aload 0
getfield com/amazon/android/framework/task/pipeline/a/b Landroid/os/Handler;
aload 0
aload 1
invokespecial com/amazon/android/framework/task/pipeline/a/c(Lcom/amazon/android/framework/task/Task;)Ljava/lang/Runnable;
lload 3
invokevirtual android/os/Handler/postAtTime(Ljava/lang/Runnable;J)Z
pop
return
.limit locals 5
.limit stack 6
.end method

.method public final b(Lcom/amazon/android/framework/task/Task;)V
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L0
getstatic com/amazon/android/framework/task/pipeline/a/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
getfield com/amazon/android/framework/task/pipeline/a/d Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ": Scheduling task immediately: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L0:
aload 0
getfield com/amazon/android/framework/task/pipeline/a/b Landroid/os/Handler;
aload 0
aload 1
invokespecial com/amazon/android/framework/task/pipeline/a/c(Lcom/amazon/android/framework/task/Task;)Ljava/lang/Runnable;
invokevirtual android/os/Handler/postAtFrontOfQueue(Ljava/lang/Runnable;)Z
pop
return
.limit locals 2
.limit stack 3
.end method
