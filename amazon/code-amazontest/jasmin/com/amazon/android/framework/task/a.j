.bytecode 50.0
.class public final synchronized com/amazon/android/framework/task/a
.super java/lang/Object
.implements com/amazon/android/framework/resource/b
.implements com/amazon/android/framework/task/TaskManager

.field private static final 'a' Lcom/amazon/android/framework/util/KiwiLogger;

.field private 'b' Lcom/amazon/android/framework/resource/a;
.annotation visible Lcom/amazon/android/framework/resource/Resource;
.end annotation
.end field

.field private 'c' Lcom/amazon/android/n/g;
.annotation visible Lcom/amazon/android/framework/resource/Resource;
.end annotation
.end field

.field private final 'd' Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final 'e' Ljava/util/Map;

.method static <clinit>()V
new com/amazon/android/framework/util/KiwiLogger
dup
ldc "TaskManagerImpl"
invokespecial com/amazon/android/framework/util/KiwiLogger/<init>(Ljava/lang/String;)V
putstatic com/amazon/android/framework/task/a/a Lcom/amazon/android/framework/util/KiwiLogger;
return
.limit locals 0
.limit stack 3
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/util/concurrent/atomic/AtomicBoolean
dup
iconst_0
invokespecial java/util/concurrent/atomic/AtomicBoolean/<init>(Z)V
putfield com/amazon/android/framework/task/a/d Ljava/util/concurrent/atomic/AtomicBoolean;
aload 0
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
putfield com/amazon/android/framework/task/a/e Ljava/util/Map;
getstatic com/amazon/android/framework/task/pipeline/TaskPipelineId/COMMAND Lcom/amazon/android/framework/task/pipeline/TaskPipelineId;
invokevirtual com/amazon/android/framework/task/pipeline/TaskPipelineId/name()Ljava/lang/String;
invokestatic com/amazon/android/framework/task/pipeline/a/a(Ljava/lang/String;)Lcom/amazon/android/framework/task/pipeline/a;
astore 1
getstatic com/amazon/android/framework/task/pipeline/TaskPipelineId/BACKGROUND Lcom/amazon/android/framework/task/pipeline/TaskPipelineId;
invokevirtual com/amazon/android/framework/task/pipeline/TaskPipelineId/name()Ljava/lang/String;
invokestatic com/amazon/android/framework/task/pipeline/a/a(Ljava/lang/String;)Lcom/amazon/android/framework/task/pipeline/a;
astore 2
new com/amazon/android/framework/task/pipeline/e
dup
aload 2
invokespecial com/amazon/android/framework/task/pipeline/e/<init>(Lcom/amazon/android/framework/task/pipeline/f;)V
astore 3
aload 0
getfield com/amazon/android/framework/task/a/e Ljava/util/Map;
getstatic com/amazon/android/framework/task/pipeline/TaskPipelineId/COMMAND Lcom/amazon/android/framework/task/pipeline/TaskPipelineId;
aload 1
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
aload 0
getfield com/amazon/android/framework/task/a/e Ljava/util/Map;
getstatic com/amazon/android/framework/task/pipeline/TaskPipelineId/BACKGROUND Lcom/amazon/android/framework/task/pipeline/TaskPipelineId;
aload 2
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
aload 0
getfield com/amazon/android/framework/task/a/e Ljava/util/Map;
getstatic com/amazon/android/framework/task/pipeline/TaskPipelineId/FOREGROUND Lcom/amazon/android/framework/task/pipeline/TaskPipelineId;
aload 3
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
return
.limit locals 4
.limit stack 4
.end method

.method private a(Lcom/amazon/android/framework/task/pipeline/TaskPipelineId;Lcom/amazon/android/framework/task/Task;Lcom/amazon/android/framework/task/b;)V
aload 0
getfield com/amazon/android/framework/task/a/d Ljava/util/concurrent/atomic/AtomicBoolean;
invokevirtual java/util/concurrent/atomic/AtomicBoolean/get()Z
ifeq L0
getstatic com/amazon/android/framework/util/KiwiLogger/ERROR_ON Z
ifeq L1
getstatic com/amazon/android/framework/task/a/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Task enqueued after TaskManager has been finished! Task: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/error(Ljava/lang/String;)V
L1:
return
L0:
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L2
getstatic com/amazon/android/framework/task/a/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Populating Task: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L2:
aload 0
getfield com/amazon/android/framework/task/a/b Lcom/amazon/android/framework/resource/a;
aload 2
invokeinterface com/amazon/android/framework/resource/a/b(Ljava/lang/Object;)V 1
aload 0
getfield com/amazon/android/framework/task/a/e Ljava/util/Map;
aload 1
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast com/amazon/android/framework/task/pipeline/f
astore 4
aload 4
ifnonnull L3
new java/lang/IllegalStateException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "No pipeline registered with id: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L3:
aload 3
aload 2
aload 4
invokeinterface com/amazon/android/framework/task/b/a(Lcom/amazon/android/framework/task/Task;Lcom/amazon/android/framework/task/pipeline/f;)V 2
return
.limit locals 5
.limit stack 4
.end method

.method public final a()V
aload 0
getfield com/amazon/android/framework/task/a/d Ljava/util/concurrent/atomic/AtomicBoolean;
iconst_0
iconst_1
invokevirtual java/util/concurrent/atomic/AtomicBoolean/compareAndSet(ZZ)Z
ifne L0
L1:
return
L0:
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L2
getstatic com/amazon/android/framework/task/a/a Lcom/amazon/android/framework/util/KiwiLogger;
ldc "TaskManager finishing...."
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L2:
aload 0
getfield com/amazon/android/framework/task/a/e Ljava/util/Map;
invokeinterface java/util/Map/values()Ljava/util/Collection; 0
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
astore 1
L3:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/amazon/android/framework/task/pipeline/f
invokeinterface com/amazon/android/framework/task/pipeline/f/a()V 0
goto L3
.limit locals 2
.limit stack 3
.end method

.method public final enqueue(Lcom/amazon/android/framework/task/pipeline/TaskPipelineId;Lcom/amazon/android/framework/task/Task;)V
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L0
getstatic com/amazon/android/framework/task/a/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Enqueue task on pipeline id: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L0:
aload 0
aload 1
aload 2
new com/amazon/android/framework/task/c
dup
aload 0
invokespecial com/amazon/android/framework/task/c/<init>(Lcom/amazon/android/framework/task/a;)V
invokespecial com/amazon/android/framework/task/a/a(Lcom/amazon/android/framework/task/pipeline/TaskPipelineId;Lcom/amazon/android/framework/task/Task;Lcom/amazon/android/framework/task/b;)V
return
.limit locals 3
.limit stack 6
.end method

.method public final enqueueAfterDelay(Lcom/amazon/android/framework/task/pipeline/TaskPipelineId;Lcom/amazon/android/framework/task/Task;J)V
aload 0
aload 1
aload 2
new com/amazon/android/framework/task/f
dup
aload 0
lload 3
invokespecial com/amazon/android/framework/task/f/<init>(Lcom/amazon/android/framework/task/a;J)V
invokespecial com/amazon/android/framework/task/a/a(Lcom/amazon/android/framework/task/pipeline/TaskPipelineId;Lcom/amazon/android/framework/task/Task;Lcom/amazon/android/framework/task/b;)V
return
.limit locals 5
.limit stack 8
.end method

.method public final enqueueAtFront(Lcom/amazon/android/framework/task/pipeline/TaskPipelineId;Lcom/amazon/android/framework/task/Task;)V
aload 0
aload 1
aload 2
new com/amazon/android/framework/task/d
dup
aload 0
invokespecial com/amazon/android/framework/task/d/<init>(Lcom/amazon/android/framework/task/a;)V
invokespecial com/amazon/android/framework/task/a/a(Lcom/amazon/android/framework/task/pipeline/TaskPipelineId;Lcom/amazon/android/framework/task/Task;Lcom/amazon/android/framework/task/b;)V
return
.limit locals 3
.limit stack 6
.end method

.method public final enqueueAtTime(Lcom/amazon/android/framework/task/pipeline/TaskPipelineId;Lcom/amazon/android/framework/task/Task;Ljava/util/Date;)V
aload 0
aload 1
aload 2
new com/amazon/android/framework/task/e
dup
aload 0
aload 3
invokespecial com/amazon/android/framework/task/e/<init>(Lcom/amazon/android/framework/task/a;Ljava/util/Date;)V
invokespecial com/amazon/android/framework/task/a/a(Lcom/amazon/android/framework/task/pipeline/TaskPipelineId;Lcom/amazon/android/framework/task/Task;Lcom/amazon/android/framework/task/b;)V
return
.limit locals 4
.limit stack 7
.end method

.method public final onResourcesPopulated()V
aload 0
getfield com/amazon/android/framework/task/a/e Ljava/util/Map;
invokeinterface java/util/Map/values()Ljava/util/Collection; 0
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
astore 1
L0:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/amazon/android/framework/task/pipeline/f
astore 2
aload 0
getfield com/amazon/android/framework/task/a/b Lcom/amazon/android/framework/resource/a;
aload 2
invokeinterface com/amazon/android/framework/resource/a/b(Ljava/lang/Object;)V 1
goto L0
L1:
new com/amazon/android/framework/task/g
dup
aload 0
invokespecial com/amazon/android/framework/task/g/<init>(Lcom/amazon/android/framework/task/a;)V
astore 1
aload 0
getfield com/amazon/android/framework/task/a/c Lcom/amazon/android/n/g;
aload 1
invokeinterface com/amazon/android/n/g/a(Lcom/amazon/android/n/c;)V 1
return
.limit locals 3
.limit stack 3
.end method
