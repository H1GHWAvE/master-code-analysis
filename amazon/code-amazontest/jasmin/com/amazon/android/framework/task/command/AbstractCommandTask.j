.bytecode 50.0
.class public synchronized abstract com/amazon/android/framework/task/command/AbstractCommandTask
.super com/amazon/android/l/c

.field private static final 'LOGGER' Lcom/amazon/android/framework/util/KiwiLogger;

.field private 'application' Landroid/app/Application;
.annotation visible Lcom/amazon/android/framework/resource/Resource;
.end annotation
.end field

.field private 'authTokenVerifier' Lcom/amazon/android/framework/task/command/e;
.annotation visible Lcom/amazon/android/framework/resource/Resource;
.end annotation
.end field

.field private 'client' Lcom/amazon/android/framework/task/command/b;
.annotation visible Lcom/amazon/android/framework/resource/Resource;
.end annotation
.end field

.field private 'metricsManager' Lcom/amazon/android/q/d;
.annotation visible Lcom/amazon/android/framework/resource/Resource;
.end annotation
.end field

.field private 'promptManager' Lcom/amazon/android/framework/prompt/PromptManager;
.annotation visible Lcom/amazon/android/framework/resource/Resource;
.end annotation
.end field

.field protected 'resultManager' Lcom/amazon/android/e/b;
.annotation visible Lcom/amazon/android/framework/resource/Resource;
.end annotation
.end field

.method static <clinit>()V
new com/amazon/android/framework/util/KiwiLogger
dup
ldc "AbstractCommandTask"
invokespecial com/amazon/android/framework/util/KiwiLogger/<init>(Ljava/lang/String;)V
putstatic com/amazon/android/framework/task/command/AbstractCommandTask/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
return
.limit locals 0
.limit stack 3
.end method

.method public <init>()V
aload 0
invokespecial com/amazon/android/l/c/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method static synthetic a(Lcom/amazon/android/framework/task/command/AbstractCommandTask;)Landroid/app/Application;
aload 0
getfield com/amazon/android/framework/task/command/AbstractCommandTask/application Landroid/app/Application;
areturn
.limit locals 1
.limit stack 1
.end method

.method private expire(Lcom/amazon/venezia/command/r;Lcom/amazon/android/framework/task/command/a;)V
.throws android/os/RemoteException
.throws com/amazon/android/framework/exception/KiwiException
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L0
getstatic com/amazon/android/framework/task/command/AbstractCommandTask/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Expiring Decision: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc ", reason: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L0:
aload 0
aload 0
getfield com/amazon/android/framework/task/command/AbstractCommandTask/client Lcom/amazon/android/framework/task/command/b;
aload 1
aload 2
invokevirtual com/amazon/android/framework/task/command/b/a(Lcom/amazon/venezia/command/r;Lcom/amazon/android/framework/task/command/a;)Lcom/amazon/android/framework/task/command/l;
invokespecial com/amazon/android/framework/task/command/AbstractCommandTask/handleCommandResult(Lcom/amazon/android/framework/task/command/l;)V
return
.limit locals 3
.limit stack 4
.end method

.method private extractMetric(Lcom/amazon/android/framework/exception/KiwiException;)Lcom/amazon/android/q/b;
new com/amazon/android/q/b
dup
aload 0
invokespecial com/amazon/android/framework/task/command/AbstractCommandTask/getFailureMetricName()Ljava/lang/String;
invokespecial com/amazon/android/q/b/<init>(Ljava/lang/String;)V
astore 2
aload 2
ldc "subType"
aload 1
invokevirtual com/amazon/android/framework/exception/KiwiException/getType()Ljava/lang/String;
invokevirtual com/amazon/android/q/b/a(Ljava/lang/String;Ljava/lang/String;)Lcom/amazon/android/q/b;
ldc "reason"
aload 1
invokevirtual com/amazon/android/framework/exception/KiwiException/getReason()Ljava/lang/String;
invokevirtual com/amazon/android/q/b/a(Ljava/lang/String;Ljava/lang/String;)Lcom/amazon/android/q/b;
ldc "context"
aload 1
invokevirtual com/amazon/android/framework/exception/KiwiException/getContext()Ljava/lang/String;
invokevirtual com/amazon/android/q/b/a(Ljava/lang/String;Ljava/lang/String;)Lcom/amazon/android/q/b;
pop
aload 2
areturn
.limit locals 3
.limit stack 3
.end method

.method private getCommand()Lcom/amazon/venezia/command/w;
new com/amazon/android/framework/task/command/g
dup
aload 0
invokespecial com/amazon/android/framework/task/command/g/<init>(Lcom/amazon/android/framework/task/command/AbstractCommandTask;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private getFailureMetricName()Ljava/lang/String;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual com/amazon/android/framework/task/command/AbstractCommandTask/getCommandName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "_failure"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method

.method private handleChoice(Lcom/amazon/venezia/command/r;Lcom/amazon/venezia/command/n;)V
.throws android/os/RemoteException
.throws com/amazon/android/framework/exception/KiwiException
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L0
getstatic com/amazon/android/framework/task/command/AbstractCommandTask/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Handling customer choice: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L0:
aload 2
invokeinterface com/amazon/venezia/command/n/b()Landroid/content/Intent; 0
astore 3
aload 3
ifnull L1
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L2
getstatic com/amazon/android/framework/task/command/AbstractCommandTask/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
ldc "Choice has intent, scheduling it to be fired!!"
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L2:
aload 0
getfield com/amazon/android/framework/task/command/AbstractCommandTask/resultManager Lcom/amazon/android/e/b;
aload 3
invokeinterface com/amazon/android/e/b/a(Landroid/content/Intent;)Lcom/amazon/android/e/f; 1
astore 3
aload 3
ifnonnull L3
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L4
getstatic com/amazon/android/framework/task/command/AbstractCommandTask/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
ldc "No result recived, expiring decision"
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L4:
aload 0
aload 1
getstatic com/amazon/android/framework/task/command/a/a Lcom/amazon/android/framework/task/command/a;
invokespecial com/amazon/android/framework/task/command/AbstractCommandTask/expire(Lcom/amazon/venezia/command/r;Lcom/amazon/android/framework/task/command/a;)V
return
L3:
aload 3
getfield com/amazon/android/e/f/b I
ifne L5
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L6
getstatic com/amazon/android/framework/task/command/AbstractCommandTask/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
ldc "Result canceled, expiring decision"
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L6:
aload 0
aload 1
getstatic com/amazon/android/framework/task/command/a/c Lcom/amazon/android/framework/task/command/a;
invokespecial com/amazon/android/framework/task/command/AbstractCommandTask/expire(Lcom/amazon/venezia/command/r;Lcom/amazon/android/framework/task/command/a;)V
return
L5:
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L7
getstatic com/amazon/android/framework/task/command/AbstractCommandTask/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
ldc "Result received!!!, notifying service"
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L7:
aload 0
aload 0
getfield com/amazon/android/framework/task/command/AbstractCommandTask/client Lcom/amazon/android/framework/task/command/b;
aload 2
invokevirtual com/amazon/android/framework/task/command/b/a(Lcom/amazon/venezia/command/n;)Lcom/amazon/android/framework/task/command/l;
invokespecial com/amazon/android/framework/task/command/AbstractCommandTask/handleCommandResult(Lcom/amazon/android/framework/task/command/l;)V
return
L1:
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L8
getstatic com/amazon/android/framework/task/command/AbstractCommandTask/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
ldc "No intent given, choosing now"
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L8:
aload 0
aload 0
getfield com/amazon/android/framework/task/command/AbstractCommandTask/client Lcom/amazon/android/framework/task/command/b;
aload 2
invokevirtual com/amazon/android/framework/task/command/b/a(Lcom/amazon/venezia/command/n;)Lcom/amazon/android/framework/task/command/l;
invokespecial com/amazon/android/framework/task/command/AbstractCommandTask/handleCommandResult(Lcom/amazon/android/framework/task/command/l;)V
return
.limit locals 4
.limit stack 3
.end method

.method private handleCommandResult(Lcom/amazon/android/framework/task/command/l;)V
.throws com/amazon/android/framework/exception/KiwiException
.throws android/os/RemoteException
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L0
getstatic com/amazon/android/framework/task/command/AbstractCommandTask/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Received result from CommandService: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L0:
aload 1
ifnonnull L1
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L2
getstatic com/amazon/android/framework/task/command/AbstractCommandTask/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
ldc "Received null result from command service, exiting task"
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L2:
return
L1:
aload 1
getfield com/amazon/android/framework/task/command/l/f Lcom/amazon/venezia/command/k;
ifnull L3
aload 0
aload 1
getfield com/amazon/android/framework/task/command/l/f Lcom/amazon/venezia/command/k;
invokespecial com/amazon/android/framework/task/command/AbstractCommandTask/handleException(Lcom/amazon/venezia/command/k;)V
return
L3:
aload 1
getfield com/amazon/android/framework/task/command/l/b Landroid/os/RemoteException;
ifnull L4
aload 1
getfield com/amazon/android/framework/task/command/l/b Landroid/os/RemoteException;
athrow
L4:
aload 1
getfield com/amazon/android/framework/task/command/l/a Ljava/lang/String;
astore 2
aload 0
getfield com/amazon/android/framework/task/command/AbstractCommandTask/authTokenVerifier Lcom/amazon/android/framework/task/command/e;
aload 2
aload 0
getfield com/amazon/android/framework/task/command/AbstractCommandTask/client Lcom/amazon/android/framework/task/command/b;
invokevirtual com/amazon/android/framework/task/command/b/a()Ljava/lang/String;
invokevirtual com/amazon/android/framework/task/command/e/a(Ljava/lang/String;Ljava/lang/String;)V
aload 1
getfield com/amazon/android/framework/task/command/l/c Lcom/amazon/venezia/command/SuccessResult;
ifnull L5
aload 0
aload 1
getfield com/amazon/android/framework/task/command/l/c Lcom/amazon/venezia/command/SuccessResult;
invokespecial com/amazon/android/framework/task/command/AbstractCommandTask/handleSuccess(Lcom/amazon/venezia/command/SuccessResult;)V
return
L5:
aload 1
getfield com/amazon/android/framework/task/command/l/d Lcom/amazon/venezia/command/FailureResult;
ifnull L6
aload 0
aload 1
getfield com/amazon/android/framework/task/command/l/d Lcom/amazon/venezia/command/FailureResult;
invokespecial com/amazon/android/framework/task/command/AbstractCommandTask/handleFailure(Lcom/amazon/venezia/command/FailureResult;)V
return
L6:
aload 0
aload 1
getfield com/amazon/android/framework/task/command/l/e Lcom/amazon/venezia/command/r;
invokespecial com/amazon/android/framework/task/command/AbstractCommandTask/handleDecision(Lcom/amazon/venezia/command/r;)V
return
.limit locals 3
.limit stack 3
.end method

.method private handleDecision(Lcom/amazon/venezia/command/r;)V
.throws android/os/RemoteException
.throws com/amazon/android/framework/exception/KiwiException
.catch com/amazon/android/b/c from L0 to L1 using L2
.catch com/amazon/android/b/c from L3 to L4 using L2
.catch com/amazon/android/b/c from L4 to L5 using L2
.catch com/amazon/android/b/c from L6 to L7 using L2
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L0
getstatic com/amazon/android/framework/task/command/AbstractCommandTask/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
ldc "Handling Decision"
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L0:
new com/amazon/android/framework/task/command/c
dup
aload 1
invokespecial com/amazon/android/framework/task/command/c/<init>(Lcom/amazon/venezia/command/r;)V
astore 2
aload 0
getfield com/amazon/android/framework/task/command/AbstractCommandTask/promptManager Lcom/amazon/android/framework/prompt/PromptManager;
aload 2
invokeinterface com/amazon/android/framework/prompt/PromptManager/present(Lcom/amazon/android/framework/prompt/Prompt;)V 1
aload 2
invokevirtual com/amazon/android/framework/task/command/c/a()Lcom/amazon/venezia/command/n;
astore 2
L1:
aload 2
ifnonnull L6
L3:
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L4
getstatic com/amazon/android/framework/task/command/AbstractCommandTask/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
ldc "DecisionChooser returned null!!, expiring"
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L4:
aload 0
aload 1
getstatic com/amazon/android/framework/task/command/a/a Lcom/amazon/android/framework/task/command/a;
invokespecial com/amazon/android/framework/task/command/AbstractCommandTask/expire(Lcom/amazon/venezia/command/r;Lcom/amazon/android/framework/task/command/a;)V
L5:
return
L6:
aload 0
aload 1
aload 2
invokespecial com/amazon/android/framework/task/command/AbstractCommandTask/handleChoice(Lcom/amazon/venezia/command/r;Lcom/amazon/venezia/command/n;)V
L7:
return
L2:
astore 2
aload 0
aload 1
aload 2
getfield com/amazon/android/b/c/a Lcom/amazon/android/framework/task/command/a;
invokespecial com/amazon/android/framework/task/command/AbstractCommandTask/expire(Lcom/amazon/venezia/command/r;Lcom/amazon/android/framework/task/command/a;)V
return
.limit locals 3
.limit stack 3
.end method

.method private handleException(Lcom/amazon/venezia/command/k;)V
.throws android/os/RemoteException
.throws com/amazon/android/framework/exception/KiwiException
new com/amazon/android/b/e
dup
aload 1
invokespecial com/amazon/android/b/e/<init>(Lcom/amazon/venezia/command/k;)V
athrow
.limit locals 2
.limit stack 3
.end method

.method private handleExecutionException(Ljava/lang/Throwable;)V
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L0
getstatic com/amazon/android/framework/task/command/AbstractCommandTask/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Exception occurred while processing task: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 1
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;Ljava/lang/Throwable;)V
L0:
aload 0
aload 1
invokespecial com/amazon/android/framework/task/command/AbstractCommandTask/translate(Ljava/lang/Throwable;)Lcom/amazon/android/framework/exception/KiwiException;
astore 1
aload 0
aload 1
invokevirtual com/amazon/android/framework/task/command/AbstractCommandTask/onException(Lcom/amazon/android/framework/exception/KiwiException;)V
aload 0
aload 1
invokespecial com/amazon/android/framework/task/command/AbstractCommandTask/extractMetric(Lcom/amazon/android/framework/exception/KiwiException;)Lcom/amazon/android/q/b;
astore 1
aload 0
getfield com/amazon/android/framework/task/command/AbstractCommandTask/metricsManager Lcom/amazon/android/q/d;
aload 1
invokeinterface com/amazon/android/q/d/a(Lcom/amazon/android/q/b;)V 1
return
.limit locals 2
.limit stack 3
.end method

.method private handleFailure(Lcom/amazon/venezia/command/FailureResult;)V
.throws android/os/RemoteException
.throws com/amazon/android/framework/exception/KiwiException
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L0
getstatic com/amazon/android/framework/task/command/AbstractCommandTask/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Command failed execution: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokeinterface com/amazon/venezia/command/FailureResult/getDisplayableName()Ljava/lang/String; 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L0:
aload 0
aload 1
invokevirtual com/amazon/android/framework/task/command/AbstractCommandTask/onFailure(Lcom/amazon/venezia/command/FailureResult;)V
return
.limit locals 2
.limit stack 3
.end method

.method private handleSuccess(Lcom/amazon/venezia/command/SuccessResult;)V
.throws android/os/RemoteException
.throws com/amazon/android/framework/exception/KiwiException
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L0
getstatic com/amazon/android/framework/task/command/AbstractCommandTask/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
ldc "Command executed successfully"
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L0:
aload 0
aload 1
invokevirtual com/amazon/android/framework/task/command/AbstractCommandTask/onSuccess(Lcom/amazon/venezia/command/SuccessResult;)V
return
.limit locals 2
.limit stack 2
.end method

.method private postExecution()V
aload 0
invokevirtual com/amazon/android/framework/task/command/AbstractCommandTask/isWorkflowChild()Z
ifne L0
aload 0
getfield com/amazon/android/framework/task/command/AbstractCommandTask/client Lcom/amazon/android/framework/task/command/b;
invokevirtual com/amazon/android/framework/task/command/b/b()V
L0:
return
.limit locals 1
.limit stack 1
.end method

.method private translate(Ljava/lang/Throwable;)Lcom/amazon/android/framework/exception/KiwiException;
aload 1
instanceof com/amazon/android/framework/exception/KiwiException
ifeq L0
aload 1
checkcast com/amazon/android/framework/exception/KiwiException
areturn
L0:
aload 1
instanceof android/os/RemoteException
ifeq L1
new com/amazon/android/b/a
dup
aload 1
checkcast android/os/RemoteException
invokespecial com/amazon/android/b/a/<init>(Landroid/os/RemoteException;)V
areturn
L1:
new com/amazon/android/b/b
dup
aload 1
invokespecial com/amazon/android/b/b/<init>(Ljava/lang/Throwable;)V
areturn
.limit locals 2
.limit stack 3
.end method

.method public final execute()V
.catch java/lang/Throwable from L0 to L1 using L2
.catch all from L0 to L1 using L3
.catch java/lang/Throwable from L1 to L4 using L2
.catch all from L1 to L4 using L3
.catch java/lang/Throwable from L5 to L6 using L2
.catch all from L5 to L6 using L3
.catch java/lang/Throwable from L6 to L7 using L2
.catch all from L6 to L7 using L3
.catch all from L8 to L9 using L3
L0:
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L1
getstatic com/amazon/android/framework/task/command/AbstractCommandTask/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
ldc "----------------------------------------------"
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
getstatic com/amazon/android/framework/task/command/AbstractCommandTask/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Executing: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual com/amazon/android/framework/task/command/AbstractCommandTask/getCommandName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
getstatic com/amazon/android/framework/task/command/AbstractCommandTask/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
ldc "----------------------------------------------"
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L1:
aload 0
invokevirtual com/amazon/android/framework/task/command/AbstractCommandTask/preExecution()V
aload 0
invokevirtual com/amazon/android/framework/task/command/AbstractCommandTask/isExecutionNeeded()Z
ifne L5
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L4
getstatic com/amazon/android/framework/task/command/AbstractCommandTask/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
ldc "Execution not needed, quitting"
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L4:
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L10
getstatic com/amazon/android/framework/task/command/AbstractCommandTask/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
ldc "Task finished"
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L10:
aload 0
invokespecial com/amazon/android/framework/task/command/AbstractCommandTask/postExecution()V
return
L5:
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L6
getstatic com/amazon/android/framework/task/command/AbstractCommandTask/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Executing Command: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual com/amazon/android/framework/task/command/AbstractCommandTask/getCommandName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L6:
aload 0
aload 0
getfield com/amazon/android/framework/task/command/AbstractCommandTask/client Lcom/amazon/android/framework/task/command/b;
aload 0
invokespecial com/amazon/android/framework/task/command/AbstractCommandTask/getCommand()Lcom/amazon/venezia/command/w;
invokevirtual com/amazon/android/framework/task/command/b/a(Lcom/amazon/venezia/command/w;)Lcom/amazon/android/framework/task/command/l;
invokespecial com/amazon/android/framework/task/command/AbstractCommandTask/handleCommandResult(Lcom/amazon/android/framework/task/command/l;)V
L7:
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L11
getstatic com/amazon/android/framework/task/command/AbstractCommandTask/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
ldc "Task finished"
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L11:
aload 0
invokespecial com/amazon/android/framework/task/command/AbstractCommandTask/postExecution()V
return
L2:
astore 1
L8:
aload 0
aload 1
invokespecial com/amazon/android/framework/task/command/AbstractCommandTask/handleExecutionException(Ljava/lang/Throwable;)V
L9:
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L12
getstatic com/amazon/android/framework/task/command/AbstractCommandTask/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
ldc "Task finished"
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L12:
aload 0
invokespecial com/amazon/android/framework/task/command/AbstractCommandTask/postExecution()V
return
L3:
astore 1
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L13
getstatic com/amazon/android/framework/task/command/AbstractCommandTask/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
ldc "Task finished"
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L13:
aload 0
invokespecial com/amazon/android/framework/task/command/AbstractCommandTask/postExecution()V
aload 1
athrow
.limit locals 2
.limit stack 3
.end method

.method protected abstract getCommandData()Ljava/util/Map;
.end method

.method protected abstract getCommandName()Ljava/lang/String;
.end method

.method protected abstract getCommandVersion()Ljava/lang/String;
.end method

.method protected abstract isExecutionNeeded()Z
.end method

.method protected onException(Lcom/amazon/android/framework/exception/KiwiException;)V
getstatic com/amazon/android/framework/task/command/AbstractCommandTask/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "On Exception!!!!: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/error(Ljava/lang/String;)V
return
.limit locals 2
.limit stack 3
.end method

.method protected abstract onFailure(Lcom/amazon/venezia/command/FailureResult;)V
.throws android/os/RemoteException
.throws com/amazon/android/framework/exception/KiwiException
.end method

.method protected abstract onSuccess(Lcom/amazon/venezia/command/SuccessResult;)V
.throws android/os/RemoteException
.throws com/amazon/android/framework/exception/KiwiException
.end method

.method protected preExecution()V
.throws com/amazon/android/framework/exception/KiwiException
return
.limit locals 1
.limit stack 0
.end method
