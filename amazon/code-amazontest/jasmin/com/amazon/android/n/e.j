.bytecode 50.0
.class public final synchronized com/amazon/android/n/e
.super java/lang/Object
.implements java/util/Comparator

.field private static final 'a' Lcom/amazon/android/framework/util/KiwiLogger;

.field private 'b' Ljava/util/List;

.method static <clinit>()V
new com/amazon/android/framework/util/KiwiLogger
dup
ldc "EventListenerNotificationQueue"
invokespecial com/amazon/android/framework/util/KiwiLogger/<init>(Ljava/lang/String;)V
putstatic com/amazon/android/n/e/a Lcom/amazon/android/framework/util/KiwiLogger;
return
.limit locals 0
.limit stack 3
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield com/amazon/android/n/e/b Ljava/util/List;
return
.limit locals 1
.limit stack 3
.end method

.method public final a(Lcom/amazon/android/n/c;)V
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L0
getstatic com/amazon/android/n/e/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Adding listener: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L0:
aload 0
getfield com/amazon/android/n/e/b Ljava/util/List;
aload 1
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 0
getfield com/amazon/android/n/e/b Ljava/util/List;
aload 0
invokestatic java/util/Collections/sort(Ljava/util/List;Ljava/util/Comparator;)V
return
.limit locals 2
.limit stack 3
.end method

.method public final a(Lcom/amazon/android/n/d;)V
aload 0
getfield com/amazon/android/n/e/b Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 2
L0:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/amazon/android/n/c
astore 3
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L2
getstatic com/amazon/android/n/e/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Notifying listener: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L2:
aload 3
aload 1
invokeinterface com/amazon/android/n/c/a(Lcom/amazon/android/n/d;)V 1
goto L0
L1:
return
.limit locals 4
.limit stack 3
.end method

.method public final volatile synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
aload 1
checkcast com/amazon/android/n/c
astore 1
aload 2
checkcast com/amazon/android/n/c
astore 2
aload 1
invokeinterface com/amazon/android/n/c/b()Lcom/amazon/android/n/a; 0
aload 2
invokeinterface com/amazon/android/n/c/b()Lcom/amazon/android/n/a; 0
invokevirtual com/amazon/android/n/a/compareTo(Ljava/lang/Enum;)I
ireturn
.limit locals 3
.limit stack 2
.end method
