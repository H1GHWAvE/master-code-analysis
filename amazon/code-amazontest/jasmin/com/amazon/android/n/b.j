.bytecode 50.0
.class public final synchronized com/amazon/android/n/b
.super java/lang/Object
.implements com/amazon/android/n/g

.field private static final 'a' Lcom/amazon/android/framework/util/KiwiLogger;

.field private final 'b' Ljava/util/Map;

.method static <clinit>()V
new com/amazon/android/framework/util/KiwiLogger
dup
ldc "EventManagerImpl"
invokespecial com/amazon/android/framework/util/KiwiLogger/<init>(Ljava/lang/String;)V
putstatic com/amazon/android/n/b/a Lcom/amazon/android/framework/util/KiwiLogger;
return
.limit locals 0
.limit stack 3
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
putfield com/amazon/android/n/b/b Ljava/util/Map;
return
.limit locals 1
.limit stack 3
.end method

.method public final a(Lcom/amazon/android/n/c;)V
aload 1
ldc "listener"
invokestatic com/amazon/android/d/a/a(Ljava/lang/Object;Ljava/lang/String;)V
invokestatic com/amazon/android/d/a/a()V
aload 1
invokeinterface com/amazon/android/n/c/a()Lcom/amazon/android/n/f; 0
astore 4
getstatic com/amazon/android/n/b/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Registering listener for event: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc ", "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
aload 0
getfield com/amazon/android/n/b/b Ljava/util/Map;
aload 4
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast com/amazon/android/n/e
astore 3
aload 3
astore 2
aload 3
ifnonnull L0
new com/amazon/android/n/e
dup
invokespecial com/amazon/android/n/e/<init>()V
astore 2
aload 0
getfield com/amazon/android/n/b/b Ljava/util/Map;
aload 4
aload 2
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L0:
aload 2
aload 1
invokevirtual com/amazon/android/n/e/a(Lcom/amazon/android/n/c;)V
return
.limit locals 5
.limit stack 3
.end method

.method public final a(Lcom/amazon/android/n/d;)V
aload 1
invokeinterface com/amazon/android/n/d/a()Lcom/amazon/android/n/f; 0
astore 2
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L0
getstatic com/amazon/android/n/b/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Posting event: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L0:
aload 0
getfield com/amazon/android/n/b/b Ljava/util/Map;
aload 2
invokeinterface java/util/Map/containsKey(Ljava/lang/Object;)Z 1
ifne L1
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L2
getstatic com/amazon/android/n/b/a Lcom/amazon/android/framework/util/KiwiLogger;
ldc "No registered listeners, returning"
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L2:
return
L1:
aload 0
getfield com/amazon/android/n/b/b Ljava/util/Map;
aload 2
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast com/amazon/android/n/e
aload 1
invokevirtual com/amazon/android/n/e/a(Lcom/amazon/android/n/d;)V
return
.limit locals 3
.limit stack 3
.end method
