.bytecode 50.0
.class public final synchronized com/amazon/android/k/a
.super java/lang/Object
.implements java/lang/Iterable

.field public 'a' Ljava/util/Map;

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
putfield com/amazon/android/k/a/a Ljava/util/Map;
return
.limit locals 1
.limit stack 3
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;Lcom/amazon/android/k/c;)Lcom/amazon/android/k/a;
aload 1
ifnonnull L0
aload 2
ifnonnull L1
iconst_1
istore 4
L2:
iload 4
ifne L3
new com/amazon/android/k/b
dup
aload 3
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "'"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "' != '"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "'"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial com/amazon/android/k/b/<init>(Lcom/amazon/android/k/c;Ljava/lang/String;)V
astore 1
aload 0
getfield com/amazon/android/k/a/a Ljava/util/Map;
aload 3
aload 1
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L3:
aload 0
areturn
L1:
iconst_0
istore 4
goto L2
L0:
aload 1
aload 2
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
istore 4
goto L2
.limit locals 5
.limit stack 5
.end method

.method public final a()Z
aload 0
getfield com/amazon/android/k/a/a Ljava/util/Map;
invokeinterface java/util/Map/isEmpty()Z 0
ifne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final iterator()Ljava/util/Iterator;
aload 0
getfield com/amazon/android/k/a/a Ljava/util/Map;
invokeinterface java/util/Map/values()Ljava/util/Collection; 0
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public final toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
ldc "Verifier:"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
astore 1
aload 0
getfield com/amazon/android/k/a/a Ljava/util/Map;
invokeinterface java/util/Map/values()Ljava/util/Collection; 0
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
astore 2
L0:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/amazon/android/k/b
astore 3
aload 1
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "\n\u0009"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
goto L0
L1:
aload 1
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 4
.limit stack 3
.end method
