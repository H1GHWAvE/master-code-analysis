.bytecode 50.0
.class public final synchronized enum com/amazon/android/j/c
.super java/lang/Enum
.implements com/amazon/android/n/f

.field public static final enum 'a' Lcom/amazon/android/j/c;

.field public static final enum 'b' Lcom/amazon/android/j/c;

.field public static final enum 'c' Lcom/amazon/android/j/c;

.field public static final enum 'd' Lcom/amazon/android/j/c;

.field public static final enum 'e' Lcom/amazon/android/j/c;

.field public static final enum 'f' Lcom/amazon/android/j/c;

.field private static final synthetic 'g' [Lcom/amazon/android/j/c;

.method static <clinit>()V
new com/amazon/android/j/c
dup
ldc "CREATE"
iconst_0
invokespecial com/amazon/android/j/c/<init>(Ljava/lang/String;I)V
putstatic com/amazon/android/j/c/a Lcom/amazon/android/j/c;
new com/amazon/android/j/c
dup
ldc "DESTROY"
iconst_1
invokespecial com/amazon/android/j/c/<init>(Ljava/lang/String;I)V
putstatic com/amazon/android/j/c/b Lcom/amazon/android/j/c;
new com/amazon/android/j/c
dup
ldc "RESUME"
iconst_2
invokespecial com/amazon/android/j/c/<init>(Ljava/lang/String;I)V
putstatic com/amazon/android/j/c/c Lcom/amazon/android/j/c;
new com/amazon/android/j/c
dup
ldc "PAUSE"
iconst_3
invokespecial com/amazon/android/j/c/<init>(Ljava/lang/String;I)V
putstatic com/amazon/android/j/c/d Lcom/amazon/android/j/c;
new com/amazon/android/j/c
dup
ldc "START"
iconst_4
invokespecial com/amazon/android/j/c/<init>(Ljava/lang/String;I)V
putstatic com/amazon/android/j/c/e Lcom/amazon/android/j/c;
new com/amazon/android/j/c
dup
ldc "STOP"
iconst_5
invokespecial com/amazon/android/j/c/<init>(Ljava/lang/String;I)V
putstatic com/amazon/android/j/c/f Lcom/amazon/android/j/c;
bipush 6
anewarray com/amazon/android/j/c
dup
iconst_0
getstatic com/amazon/android/j/c/a Lcom/amazon/android/j/c;
aastore
dup
iconst_1
getstatic com/amazon/android/j/c/b Lcom/amazon/android/j/c;
aastore
dup
iconst_2
getstatic com/amazon/android/j/c/c Lcom/amazon/android/j/c;
aastore
dup
iconst_3
getstatic com/amazon/android/j/c/d Lcom/amazon/android/j/c;
aastore
dup
iconst_4
getstatic com/amazon/android/j/c/e Lcom/amazon/android/j/c;
aastore
dup
iconst_5
getstatic com/amazon/android/j/c/f Lcom/amazon/android/j/c;
aastore
putstatic com/amazon/android/j/c/g [Lcom/amazon/android/j/c;
return
.limit locals 0
.limit stack 4
.end method

.method private <init>(Ljava/lang/String;I)V
aload 0
aload 1
iload 2
invokespecial java/lang/Enum/<init>(Ljava/lang/String;I)V
return
.limit locals 3
.limit stack 3
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/amazon/android/j/c;
ldc com/amazon/android/j/c
aload 0
invokestatic java/lang/Enum/valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;
checkcast com/amazon/android/j/c
areturn
.limit locals 1
.limit stack 2
.end method

.method public static values()[Lcom/amazon/android/j/c;
getstatic com/amazon/android/j/c/g [Lcom/amazon/android/j/c;
invokevirtual [Lcom/amazon/android/j/c;/clone()Ljava/lang/Object;
checkcast [Lcom/amazon/android/j/c;
areturn
.limit locals 0
.limit stack 1
.end method

.method public final toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "ACTIVITY_"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual com/amazon/android/j/c/name()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method
