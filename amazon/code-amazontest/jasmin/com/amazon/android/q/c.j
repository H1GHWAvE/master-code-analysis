.bytecode 50.0
.class public final synchronized com/amazon/android/q/c
.super java/lang/Object
.implements com/amazon/android/q/d

.field private static final 'a' Lcom/amazon/android/framework/util/KiwiLogger;

.field private 'b' Lcom/amazon/android/q/a;

.method static <clinit>()V
new com/amazon/android/framework/util/KiwiLogger
dup
ldc "MetricsManagerImpl"
invokespecial com/amazon/android/framework/util/KiwiLogger/<init>(Ljava/lang/String;)V
putstatic com/amazon/android/q/c/a Lcom/amazon/android/framework/util/KiwiLogger;
return
.limit locals 0
.limit stack 3
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new com/amazon/android/q/a
dup
invokespecial com/amazon/android/q/a/<init>()V
putfield com/amazon/android/q/c/b Lcom/amazon/android/q/a;
return
.limit locals 1
.limit stack 3
.end method

.method public final a()Lcom/amazon/android/q/a;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
monitorenter
L0:
aload 0
getfield com/amazon/android/q/c/b Lcom/amazon/android/q/a;
invokevirtual com/amazon/android/q/a/a()Z
ifeq L3
aload 0
getfield com/amazon/android/q/c/b Lcom/amazon/android/q/a;
astore 1
L1:
aload 0
monitorexit
aload 1
areturn
L3:
aload 0
getfield com/amazon/android/q/c/b Lcom/amazon/android/q/a;
astore 1
aload 0
new com/amazon/android/q/a
dup
invokespecial com/amazon/android/q/a/<init>()V
putfield com/amazon/android/q/c/b Lcom/amazon/android/q/a;
L4:
goto L1
L2:
astore 1
aload 0
monitorexit
aload 1
athrow
.limit locals 2
.limit stack 3
.end method

.method public final a(Lcom/amazon/android/q/b;)V
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L2
aload 0
monitorenter
L0:
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L1
getstatic com/amazon/android/q/c/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Recording Metric: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L1:
aload 0
getfield com/amazon/android/q/c/b Lcom/amazon/android/q/a;
getfield com/amazon/android/q/a/a Ljava/util/List;
aload 1
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
L3:
aload 0
monitorexit
return
L2:
astore 1
aload 0
monitorexit
aload 1
athrow
.limit locals 2
.limit stack 3
.end method
