.bytecode 50.0
.class public final synchronized com/amazon/android/q/a
.super java/lang/Object
.implements java/io/Serializable
.implements java/lang/Iterable

.field private static final 'serialVersionUID' J = 1L


.field final 'a' Ljava/util/List;

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield com/amazon/android/q/a/a Ljava/util/List;
return
.limit locals 1
.limit stack 3
.end method

.method public final a()Z
aload 0
getfield com/amazon/android/q/a/a Ljava/util/List;
invokeinterface java/util/List/isEmpty()Z 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final b()I
aload 0
getfield com/amazon/android/q/a/a Ljava/util/List;
invokeinterface java/util/List/size()I 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final iterator()Ljava/util/Iterator;
aload 0
getfield com/amazon/android/q/a/a Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public final toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "MetricBatch: ["
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/amazon/android/q/a/a Ljava/util/List;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "]"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method
