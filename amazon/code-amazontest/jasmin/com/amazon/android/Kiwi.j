.bytecode 50.0
.class public final synchronized com/amazon/android/Kiwi
.super java/lang/Object
.implements com/amazon/android/framework/resource/b

.field protected static final 'ACTIVITY_NAME' Ljava/lang/String; = "ActivityName"

.field protected static final 'EVENT_NAME' Ljava/lang/String; = "EventName"

.field private static 'INSTANCE' Lcom/amazon/android/Kiwi;

.field private static final 'LOGGER' Lcom/amazon/android/framework/util/KiwiLogger;

.field protected static final 'TIMESTAMP' Ljava/lang/String; = "Timestamp"

.field private 'application' Landroid/app/Application;
.annotation visible Lcom/amazon/android/framework/resource/Resource;
.end annotation
.end field

.field private 'authKeyLoader' Lcom/amazon/android/m/c;
.annotation visible Lcom/amazon/android/framework/resource/Resource;
.end annotation
.end field

.field private 'contextManager' Lcom/amazon/android/framework/context/ContextManager;
.annotation visible Lcom/amazon/android/framework/resource/Resource;
.end annotation
.end field

.field private 'dataStore' Lcom/amazon/android/o/a;
.annotation visible Lcom/amazon/android/framework/resource/Resource;
.end annotation
.end field

.field private final 'drmEnabled' Z

.field private 'eventManager' Lcom/amazon/android/n/g;
.annotation visible Lcom/amazon/android/framework/resource/Resource;
.end annotation
.end field

.field private 'promptManager' Lcom/amazon/android/framework/prompt/PromptManager;
.annotation visible Lcom/amazon/android/framework/resource/Resource;
.end annotation
.end field

.field private 'resultManager' Lcom/amazon/android/e/b;
.annotation visible Lcom/amazon/android/framework/resource/Resource;
.end annotation
.end field

.field private 'taskManager' Lcom/amazon/android/framework/task/TaskManager;
.annotation visible Lcom/amazon/android/framework/resource/Resource;
.end annotation
.end field

.method static <clinit>()V
new com/amazon/android/framework/util/KiwiLogger
dup
ldc "Kiwi"
invokespecial com/amazon/android/framework/util/KiwiLogger/<init>(Ljava/lang/String;)V
putstatic com/amazon/android/Kiwi/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
return
.limit locals 0
.limit stack 3
.end method

.method private <init>(Landroid/app/Application;Z)V
aload 0
invokespecial java/lang/Object/<init>()V
invokestatic java/lang/System/currentTimeMillis()J
lstore 3
aload 0
iload 2
putfield com/amazon/android/Kiwi/drmEnabled Z
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L0
getstatic com/amazon/android/Kiwi/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Starting initialization process for application: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual android/app/Application/getPackageName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
getstatic com/amazon/android/Kiwi/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "DRM enabled: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 2
invokevirtual java/lang/StringBuilder/append(Z)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L0:
aload 0
aload 1
invokespecial com/amazon/android/Kiwi/instantiateTheWorld(Landroid/app/Application;)V
aload 0
aload 1
invokespecial com/amazon/android/Kiwi/registerTestModeReceiver(Landroid/app/Application;)V
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L1
invokestatic java/lang/System/currentTimeMillis()J
lstore 5
getstatic com/amazon/android/Kiwi/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Kiwi.Constructor Time: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
lload 5
lload 3
lsub
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L1:
return
.limit locals 7
.limit stack 6
.end method

.method static synthetic a()Lcom/amazon/android/framework/util/KiwiLogger;
getstatic com/amazon/android/Kiwi/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
areturn
.limit locals 0
.limit stack 1
.end method

.method static synthetic a(Lcom/amazon/android/Kiwi;)Lcom/amazon/android/o/a;
aload 0
getfield com/amazon/android/Kiwi/dataStore Lcom/amazon/android/o/a;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic a(Lcom/amazon/android/framework/task/command/AbstractCommandTask;)V
aload 0
invokestatic com/amazon/android/Kiwi/unsafeAddCommandToCommandTaskPipeline(Lcom/amazon/android/framework/task/command/AbstractCommandTask;)V
return
.limit locals 1
.limit stack 1
.end method

.method public static addCommandToCommandTaskPipeline(Lcom/amazon/android/framework/task/command/AbstractCommandTask;)V
.annotation visible Ljava/lang/Deprecated;
.end annotation
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L0
getstatic com/amazon/android/Kiwi/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
ldc "WARNING: Use of deprecated method detected."
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L0:
invokestatic com/amazon/android/Kiwi/isInitialized()Z
ifeq L1
aload 0
invokestatic com/amazon/android/Kiwi/unsafeAddCommandToCommandTaskPipeline(Lcom/amazon/android/framework/task/command/AbstractCommandTask;)V
return
L1:
getstatic com/amazon/android/Kiwi/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
ldc "Kiwi subsystem is not fully initialized.  Cannot process task."
invokevirtual com/amazon/android/framework/util/KiwiLogger/error(Ljava/lang/String;)V
return
.limit locals 1
.limit stack 2
.end method

.method public static addCommandToCommandTaskPipeline(Lcom/amazon/android/framework/task/command/AbstractCommandTask;Landroid/content/Context;)V
invokestatic com/amazon/android/Kiwi/isInitialized()Z
ifne L0
aload 1
ifnull L1
aload 1
invokevirtual android/content/Context/getApplicationContext()Landroid/content/Context;
ifnull L1
new com/amazon/android/Kiwi
dup
aload 1
invokevirtual android/content/Context/getApplicationContext()Landroid/content/Context;
checkcast android/app/Application
iconst_0
invokespecial com/amazon/android/Kiwi/<init>(Landroid/app/Application;Z)V
putstatic com/amazon/android/Kiwi/INSTANCE Lcom/amazon/android/Kiwi;
L0:
aload 0
invokestatic com/amazon/android/Kiwi/unsafeAddCommandToCommandTaskPipeline(Lcom/amazon/android/framework/task/command/AbstractCommandTask;)V
return
L1:
getstatic com/amazon/android/Kiwi/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
ldc "Kiwi subsystem cannot be initialized because of null context. Unable to enqueue task."
invokevirtual com/amazon/android/framework/util/KiwiLogger/error(Ljava/lang/String;)V
return
.limit locals 2
.limit stack 4
.end method

.method static synthetic b()Lcom/amazon/android/Kiwi;
aconst_null
putstatic com/amazon/android/Kiwi/INSTANCE Lcom/amazon/android/Kiwi;
aconst_null
areturn
.limit locals 0
.limit stack 1
.end method

.method static synthetic b(Lcom/amazon/android/Kiwi;)Z
aload 0
getfield com/amazon/android/Kiwi/drmEnabled Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic c(Lcom/amazon/android/Kiwi;)Lcom/amazon/android/n/g;
aload 0
getfield com/amazon/android/Kiwi/eventManager Lcom/amazon/android/n/g;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic d(Lcom/amazon/android/Kiwi;)V
aload 0
invokespecial com/amazon/android/Kiwi/enqueueAppLaunchWorkflowTask()V
return
.limit locals 1
.limit stack 1
.end method

.method static synthetic e(Lcom/amazon/android/Kiwi;)Landroid/app/Application;
aload 0
getfield com/amazon/android/Kiwi/application Landroid/app/Application;
areturn
.limit locals 1
.limit stack 1
.end method

.method private enqueueAppLaunchWorkflowTask()V
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L0
getstatic com/amazon/android/Kiwi/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
ldc "Enqueuing launch workflow"
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L0:
aload 0
invokespecial com/amazon/android/Kiwi/getLaunchWorkflow()Lcom/amazon/android/framework/task/Task;
astore 1
aload 0
getfield com/amazon/android/Kiwi/taskManager Lcom/amazon/android/framework/task/TaskManager;
getstatic com/amazon/android/framework/task/pipeline/TaskPipelineId/COMMAND Lcom/amazon/android/framework/task/pipeline/TaskPipelineId;
aload 1
invokeinterface com/amazon/android/framework/task/TaskManager/enqueue(Lcom/amazon/android/framework/task/pipeline/TaskPipelineId;Lcom/amazon/android/framework/task/Task;)V 2
return
.limit locals 2
.limit stack 3
.end method

.method private getLaunchWorkflow()Lcom/amazon/android/framework/task/Task;
aload 0
getfield com/amazon/android/Kiwi/drmEnabled Z
ifeq L0
new com/amazon/android/b
dup
invokespecial com/amazon/android/b/<init>()V
areturn
L0:
new com/amazon/android/h
dup
invokespecial com/amazon/android/h/<init>()V
areturn
.limit locals 1
.limit stack 2
.end method

.method public static getPromptManager()Lcom/amazon/android/framework/prompt/PromptManager;
getstatic com/amazon/android/Kiwi/INSTANCE Lcom/amazon/android/Kiwi;
getfield com/amazon/android/Kiwi/promptManager Lcom/amazon/android/framework/prompt/PromptManager;
areturn
.limit locals 0
.limit stack 1
.end method

.method private static ignoreEvent(Ljava/lang/String;Landroid/content/Context;)V
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L0
getstatic com/amazon/android/Kiwi/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " called on context: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc " when "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "Kiwi is dead, ignoring..."
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L0:
return
.limit locals 2
.limit stack 3
.end method

.method private instantiateTheWorld(Landroid/app/Application;)V
new com/amazon/android/framework/resource/c
dup
invokespecial com/amazon/android/framework/resource/c/<init>()V
astore 2
aload 2
aload 1
invokeinterface com/amazon/android/framework/resource/a/a(Ljava/lang/Object;)V 1
aload 2
new com/amazon/android/framework/task/a
dup
invokespecial com/amazon/android/framework/task/a/<init>()V
invokeinterface com/amazon/android/framework/resource/a/a(Ljava/lang/Object;)V 1
aload 2
new com/amazon/android/o/a
dup
invokespecial com/amazon/android/o/a/<init>()V
invokeinterface com/amazon/android/framework/resource/a/a(Ljava/lang/Object;)V 1
aload 2
new com/amazon/android/e/c
dup
invokespecial com/amazon/android/e/c/<init>()V
invokeinterface com/amazon/android/framework/resource/a/a(Ljava/lang/Object;)V 1
aload 2
new com/amazon/android/framework/context/d
dup
invokespecial com/amazon/android/framework/context/d/<init>()V
invokeinterface com/amazon/android/framework/resource/a/a(Ljava/lang/Object;)V 1
aload 2
new com/amazon/android/framework/prompt/PromptManagerImpl
dup
invokespecial com/amazon/android/framework/prompt/PromptManagerImpl/<init>()V
invokeinterface com/amazon/android/framework/resource/a/a(Ljava/lang/Object;)V 1
aload 2
new com/amazon/android/n/b
dup
invokespecial com/amazon/android/n/b/<init>()V
invokeinterface com/amazon/android/framework/resource/a/a(Ljava/lang/Object;)V 1
aload 2
new com/amazon/android/c/a
dup
invokespecial com/amazon/android/c/a/<init>()V
invokeinterface com/amazon/android/framework/resource/a/a(Ljava/lang/Object;)V 1
aload 2
new com/amazon/android/q/c
dup
invokespecial com/amazon/android/q/c/<init>()V
invokeinterface com/amazon/android/framework/resource/a/a(Ljava/lang/Object;)V 1
aload 2
new com/amazon/android/framework/task/command/b
dup
invokespecial com/amazon/android/framework/task/command/b/<init>()V
invokeinterface com/amazon/android/framework/resource/a/a(Ljava/lang/Object;)V 1
aload 2
new com/amazon/android/m/c
dup
invokespecial com/amazon/android/m/c/<init>()V
invokeinterface com/amazon/android/framework/resource/a/a(Ljava/lang/Object;)V 1
aload 2
new com/amazon/android/framework/task/command/e
dup
invokespecial com/amazon/android/framework/task/command/e/<init>()V
invokeinterface com/amazon/android/framework/resource/a/a(Ljava/lang/Object;)V 1
aload 2
new com/amazon/android/g/a
dup
invokespecial com/amazon/android/g/a/<init>()V
invokeinterface com/amazon/android/framework/resource/a/a(Ljava/lang/Object;)V 1
aload 2
invokeinterface com/amazon/android/framework/resource/a/a()V 0
aload 2
aload 0
invokeinterface com/amazon/android/framework/resource/a/b(Ljava/lang/Object;)V 1
return
.limit locals 3
.limit stack 3
.end method

.method private static isInitialized()Z
getstatic com/amazon/android/Kiwi/INSTANCE Lcom/amazon/android/Kiwi;
ifnull L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 0
.limit stack 1
.end method

.method public static isSignedByKiwi(Ljava/lang/String;Ljava/lang/String;)Z
.catch com/amazon/android/h/a from L0 to L1 using L2
invokestatic com/amazon/android/Kiwi/isInitialized()Z
ifeq L3
L0:
aload 0
aload 1
getstatic com/amazon/android/Kiwi/INSTANCE Lcom/amazon/android/Kiwi;
getfield com/amazon/android/Kiwi/authKeyLoader Lcom/amazon/android/m/c;
invokevirtual com/amazon/android/m/c/a()Ljava/security/PublicKey;
invokestatic com/amazon/android/m/a/a(Ljava/lang/String;Ljava/lang/String;Ljava/security/PublicKey;)Z
istore 2
L1:
iload 2
ireturn
L2:
astore 0
getstatic com/amazon/android/Kiwi/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Unable to validate signature: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual com/amazon/android/h/a/getMessage()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L4:
iconst_0
ireturn
L3:
getstatic com/amazon/android/Kiwi/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
ldc "Kiwi was not yet initialized - cannot do the IAP call"
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
goto L4
.limit locals 3
.limit stack 3
.end method

.method public static onActivityResult(Landroid/app/Activity;IILandroid/content/Intent;)Z
ldc "onActivityResult"
aload 0
invokestatic com/amazon/android/Kiwi/preProcess(Ljava/lang/String;Landroid/content/Context;)Z
ifeq L0
new com/amazon/android/e/f
dup
aload 0
iload 1
iload 2
aload 3
invokespecial com/amazon/android/e/f/<init>(Landroid/app/Activity;IILandroid/content/Intent;)V
astore 0
getstatic com/amazon/android/Kiwi/INSTANCE Lcom/amazon/android/Kiwi;
getfield com/amazon/android/Kiwi/resultManager Lcom/amazon/android/e/b;
aload 0
invokeinterface com/amazon/android/e/b/a(Lcom/amazon/android/e/f;)Z 1
ireturn
L0:
iconst_0
ireturn
.limit locals 4
.limit stack 6
.end method

.method public static onCreate(Landroid/app/Activity;Z)V
invokestatic java/lang/System/currentTimeMillis()J
lstore 2
invokestatic com/amazon/android/Kiwi/isInitialized()Z
ifne L0
new com/amazon/android/Kiwi
dup
aload 0
invokevirtual android/app/Activity/getApplication()Landroid/app/Application;
iload 1
invokespecial com/amazon/android/Kiwi/<init>(Landroid/app/Application;Z)V
putstatic com/amazon/android/Kiwi/INSTANCE Lcom/amazon/android/Kiwi;
L0:
ldc "onCreate"
aload 0
invokestatic com/amazon/android/Kiwi/preProcess(Ljava/lang/String;Landroid/content/Context;)Z
ifeq L1
getstatic com/amazon/android/Kiwi/INSTANCE Lcom/amazon/android/Kiwi;
getfield com/amazon/android/Kiwi/contextManager Lcom/amazon/android/framework/context/ContextManager;
aload 0
invokeinterface com/amazon/android/framework/context/ContextManager/onCreate(Landroid/app/Activity;)V 1
L1:
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L2
invokestatic java/lang/System/currentTimeMillis()J
lstore 4
getstatic com/amazon/android/Kiwi/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Kiwi.ActivityOnCreate Time: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
lload 4
lload 2
lsub
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L2:
return
.limit locals 6
.limit stack 6
.end method

.method public static onCreate(Landroid/app/Service;Z)V
invokestatic java/lang/System/currentTimeMillis()J
lstore 2
ldc "onCreate"
aload 0
invokestatic com/amazon/android/Kiwi/preProcess(Ljava/lang/String;Landroid/content/Context;)Z
ifeq L0
getstatic com/amazon/android/Kiwi/INSTANCE Lcom/amazon/android/Kiwi;
getfield com/amazon/android/Kiwi/contextManager Lcom/amazon/android/framework/context/ContextManager;
aload 0
invokeinterface com/amazon/android/framework/context/ContextManager/onCreate(Landroid/app/Service;)V 1
L0:
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L1
invokestatic java/lang/System/currentTimeMillis()J
lstore 4
getstatic com/amazon/android/Kiwi/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Kiwi.ServiceOnCreate Time: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
lload 4
lload 2
lsub
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L1:
return
.limit locals 6
.limit stack 6
.end method

.method public static onCreateDialog(Landroid/app/Activity;I)Landroid/app/Dialog;
invokestatic java/lang/System/currentTimeMillis()J
lstore 2
ldc "onCreateDialog"
aload 0
invokestatic com/amazon/android/Kiwi/preProcess(Ljava/lang/String;Landroid/content/Context;)Z
ifeq L0
getstatic com/amazon/android/Kiwi/INSTANCE Lcom/amazon/android/Kiwi;
getfield com/amazon/android/Kiwi/promptManager Lcom/amazon/android/framework/prompt/PromptManager;
aload 0
iload 1
invokeinterface com/amazon/android/framework/prompt/PromptManager/onCreateDialog(Landroid/app/Activity;I)Landroid/app/Dialog; 2
areturn
L0:
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L1
invokestatic java/lang/System/currentTimeMillis()J
lstore 4
getstatic com/amazon/android/Kiwi/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Kiwi.ActivityOnCreateDialog Time: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
lload 4
lload 2
lsub
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L1:
aconst_null
areturn
.limit locals 6
.limit stack 6
.end method

.method public static onDestroy(Landroid/app/Activity;)V
.catch java/lang/Exception from L0 to L1 using L2
invokestatic java/lang/System/currentTimeMillis()J
lstore 1
ldc "onDestroy"
aload 0
invokestatic com/amazon/android/Kiwi/preProcess(Ljava/lang/String;Landroid/content/Context;)Z
ifeq L1
L0:
getstatic com/amazon/android/Kiwi/INSTANCE Lcom/amazon/android/Kiwi;
getfield com/amazon/android/Kiwi/contextManager Lcom/amazon/android/framework/context/ContextManager;
aload 0
invokeinterface com/amazon/android/framework/context/ContextManager/onDestroy(Landroid/app/Activity;)V 1
L1:
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L3
invokestatic java/lang/System/currentTimeMillis()J
lstore 3
getstatic com/amazon/android/Kiwi/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Kiwi.ActivityOnDestroy Time: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
lload 3
lload 1
lsub
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L3:
return
L2:
astore 0
getstatic com/amazon/android/Kiwi/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
ldc "Kiwi.ActivityOnDestroy Error: "
aload 0
invokevirtual com/amazon/android/framework/util/KiwiLogger/error(Ljava/lang/String;Ljava/lang/Throwable;)V
goto L1
.limit locals 5
.limit stack 6
.end method

.method public static onDestroy(Landroid/app/Service;)V
invokestatic java/lang/System/currentTimeMillis()J
lstore 1
ldc "onDestroy"
aload 0
invokestatic com/amazon/android/Kiwi/preProcess(Ljava/lang/String;Landroid/content/Context;)Z
ifeq L0
getstatic com/amazon/android/Kiwi/INSTANCE Lcom/amazon/android/Kiwi;
getfield com/amazon/android/Kiwi/contextManager Lcom/amazon/android/framework/context/ContextManager;
aload 0
invokeinterface com/amazon/android/framework/context/ContextManager/onDestroy(Landroid/app/Service;)V 1
L0:
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L1
invokestatic java/lang/System/currentTimeMillis()J
lstore 3
getstatic com/amazon/android/Kiwi/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Kiwi.ServiceOnDestroy Time: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
lload 3
lload 1
lsub
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L1:
return
.limit locals 5
.limit stack 6
.end method

.method public static onPause(Landroid/app/Activity;)V
invokestatic java/lang/System/currentTimeMillis()J
lstore 1
ldc "onPause"
aload 0
invokestatic com/amazon/android/Kiwi/preProcess(Ljava/lang/String;Landroid/content/Context;)Z
ifeq L0
getstatic com/amazon/android/Kiwi/INSTANCE Lcom/amazon/android/Kiwi;
getfield com/amazon/android/Kiwi/contextManager Lcom/amazon/android/framework/context/ContextManager;
aload 0
invokeinterface com/amazon/android/framework/context/ContextManager/onPause(Landroid/app/Activity;)V 1
L0:
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L1
invokestatic java/lang/System/currentTimeMillis()J
lstore 3
getstatic com/amazon/android/Kiwi/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Kiwi.ActivityOnPause Time: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
lload 3
lload 1
lsub
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L1:
return
.limit locals 5
.limit stack 6
.end method

.method public static onResume(Landroid/app/Activity;)V
invokestatic java/lang/System/currentTimeMillis()J
lstore 1
ldc "onResume"
aload 0
invokestatic com/amazon/android/Kiwi/preProcess(Ljava/lang/String;Landroid/content/Context;)Z
ifeq L0
getstatic com/amazon/android/Kiwi/INSTANCE Lcom/amazon/android/Kiwi;
getfield com/amazon/android/Kiwi/contextManager Lcom/amazon/android/framework/context/ContextManager;
aload 0
invokeinterface com/amazon/android/framework/context/ContextManager/onResume(Landroid/app/Activity;)V 1
L0:
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L1
invokestatic java/lang/System/currentTimeMillis()J
lstore 3
getstatic com/amazon/android/Kiwi/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Kiwi.ActivityOnResume Time: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
lload 3
lload 1
lsub
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L1:
return
.limit locals 5
.limit stack 6
.end method

.method public static onStart(Landroid/app/Activity;)V
invokestatic java/lang/System/currentTimeMillis()J
lstore 1
ldc "onStart"
aload 0
invokestatic com/amazon/android/Kiwi/preProcess(Ljava/lang/String;Landroid/content/Context;)Z
ifeq L0
getstatic com/amazon/android/Kiwi/INSTANCE Lcom/amazon/android/Kiwi;
getfield com/amazon/android/Kiwi/contextManager Lcom/amazon/android/framework/context/ContextManager;
aload 0
invokeinterface com/amazon/android/framework/context/ContextManager/onStart(Landroid/app/Activity;)V 1
L0:
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L1
invokestatic java/lang/System/currentTimeMillis()J
lstore 3
getstatic com/amazon/android/Kiwi/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Kiwi.ActivityOnStart Time: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
lload 3
lload 1
lsub
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L1:
return
.limit locals 5
.limit stack 6
.end method

.method public static onStop(Landroid/app/Activity;)V
invokestatic java/lang/System/currentTimeMillis()J
lstore 1
ldc "onStop"
aload 0
invokestatic com/amazon/android/Kiwi/preProcess(Ljava/lang/String;Landroid/content/Context;)Z
ifeq L0
getstatic com/amazon/android/Kiwi/INSTANCE Lcom/amazon/android/Kiwi;
getfield com/amazon/android/Kiwi/contextManager Lcom/amazon/android/framework/context/ContextManager;
aload 0
invokeinterface com/amazon/android/framework/context/ContextManager/onStop(Landroid/app/Activity;)V 1
L0:
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L1
invokestatic java/lang/System/currentTimeMillis()J
lstore 3
getstatic com/amazon/android/Kiwi/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Kiwi.ActivityOnStop Time: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
lload 3
lload 1
lsub
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L1:
return
.limit locals 5
.limit stack 6
.end method

.method public static onWindowFocusChanged(Landroid/app/Activity;Z)V
ldc "onWindowFocusChanged"
aload 0
invokestatic com/amazon/android/Kiwi/preProcess(Ljava/lang/String;Landroid/content/Context;)Z
ifeq L0
getstatic com/amazon/android/Kiwi/INSTANCE Lcom/amazon/android/Kiwi;
getfield com/amazon/android/Kiwi/promptManager Lcom/amazon/android/framework/prompt/PromptManager;
aload 0
iload 1
invokeinterface com/amazon/android/framework/prompt/PromptManager/onWindowFocusChanged(Landroid/app/Activity;Z)V 2
L0:
return
.limit locals 2
.limit stack 3
.end method

.method private static preProcess(Ljava/lang/String;Landroid/content/Context;)Z
invokestatic com/amazon/android/d/a/a()V
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L0
getstatic com/amazon/android/Kiwi/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ": "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L0:
invokestatic com/amazon/android/Kiwi/isInitialized()Z
ifeq L1
iconst_1
ireturn
L1:
aload 0
aload 1
invokestatic com/amazon/android/Kiwi/ignoreEvent(Ljava/lang/String;Landroid/content/Context;)V
iconst_0
ireturn
.limit locals 2
.limit stack 3
.end method

.method private registerActivityLifeCyclePauseListener()V
new com/amazon/android/d
dup
aload 0
invokespecial com/amazon/android/d/<init>(Lcom/amazon/android/Kiwi;)V
astore 1
aload 0
getfield com/amazon/android/Kiwi/eventManager Lcom/amazon/android/n/g;
aload 1
invokeinterface com/amazon/android/n/g/a(Lcom/amazon/android/n/c;)V 1
return
.limit locals 2
.limit stack 3
.end method

.method private registerActivityLifeCycleResumeListener()V
new com/amazon/android/f
dup
aload 0
invokespecial com/amazon/android/f/<init>(Lcom/amazon/android/Kiwi;)V
astore 1
aload 0
getfield com/amazon/android/Kiwi/eventManager Lcom/amazon/android/n/g;
aload 1
invokeinterface com/amazon/android/n/g/a(Lcom/amazon/android/n/c;)V 1
return
.limit locals 2
.limit stack 3
.end method

.method private registerApplicationCreationListener()V
new com/amazon/android/g
dup
aload 0
invokespecial com/amazon/android/g/<init>(Lcom/amazon/android/Kiwi;)V
astore 1
aload 0
getfield com/amazon/android/Kiwi/eventManager Lcom/amazon/android/n/g;
aload 1
invokeinterface com/amazon/android/n/g/a(Lcom/amazon/android/n/c;)V 1
return
.limit locals 2
.limit stack 3
.end method

.method private registerApplicationDestructionListener()V
new com/amazon/android/e
dup
aload 0
invokespecial com/amazon/android/e/<init>(Lcom/amazon/android/Kiwi;)V
astore 1
aload 0
getfield com/amazon/android/Kiwi/eventManager Lcom/amazon/android/n/g;
aload 1
invokeinterface com/amazon/android/n/g/a(Lcom/amazon/android/n/c;)V 1
return
.limit locals 2
.limit stack 3
.end method

.method private registerTestModeReceiver(Landroid/app/Application;)V
aload 1
new com/amazon/android/c
dup
aload 0
invokespecial com/amazon/android/c/<init>(Lcom/amazon/android/Kiwi;)V
new android/content/IntentFilter
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 1
invokevirtual android/app/Application/getPackageName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".enable.test.mode"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial android/content/IntentFilter/<init>(Ljava/lang/String;)V
invokevirtual android/app/Application/registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
pop
return
.limit locals 2
.limit stack 6
.end method

.method private static unsafeAddCommandToCommandTaskPipeline(Lcom/amazon/android/framework/task/command/AbstractCommandTask;)V
getstatic com/amazon/android/Kiwi/INSTANCE Lcom/amazon/android/Kiwi;
getfield com/amazon/android/Kiwi/taskManager Lcom/amazon/android/framework/task/TaskManager;
getstatic com/amazon/android/framework/task/pipeline/TaskPipelineId/COMMAND Lcom/amazon/android/framework/task/pipeline/TaskPipelineId;
aload 0
invokeinterface com/amazon/android/framework/task/TaskManager/enqueue(Lcom/amazon/android/framework/task/pipeline/TaskPipelineId;Lcom/amazon/android/framework/task/Task;)V 2
return
.limit locals 1
.limit stack 3
.end method

.method public final onResourcesPopulated()V
aload 0
invokespecial com/amazon/android/Kiwi/registerApplicationCreationListener()V
aload 0
invokespecial com/amazon/android/Kiwi/registerApplicationDestructionListener()V
aload 0
invokespecial com/amazon/android/Kiwi/registerActivityLifeCyclePauseListener()V
aload 0
invokespecial com/amazon/android/Kiwi/registerActivityLifeCycleResumeListener()V
return
.limit locals 1
.limit stack 1
.end method
