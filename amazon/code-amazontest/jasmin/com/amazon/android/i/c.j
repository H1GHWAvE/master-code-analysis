.bytecode 50.0
.class public synchronized abstract com/amazon/android/i/c
.super com/amazon/android/i/b

.field private static final 'LOGGER' Lcom/amazon/android/framework/util/KiwiLogger;

.field private static final 'MILLISECONDS_PER_SECOND' J = 1000L


.field private final 'instantiation' Ljava/util/Date;

.method static <clinit>()V
new com/amazon/android/framework/util/KiwiLogger
dup
ldc "RelativeExpirable"
invokespecial com/amazon/android/framework/util/KiwiLogger/<init>(Ljava/lang/String;)V
putstatic com/amazon/android/i/c/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
return
.limit locals 0
.limit stack 3
.end method

.method public <init>()V
aload 0
invokespecial com/amazon/android/i/b/<init>()V
aload 0
new java/util/Date
dup
invokespecial java/util/Date/<init>()V
putfield com/amazon/android/i/c/instantiation Ljava/util/Date;
return
.limit locals 1
.limit stack 3
.end method

.method public final getExpiration()Ljava/util/Date;
getstatic com/amazon/android/i/c/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "RelativeExpiration duration: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual com/amazon/android/i/c/getExpirationDurationInSeconds()J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc ", expirable: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
new java/util/Date
dup
aload 0
getfield com/amazon/android/i/c/instantiation Ljava/util/Date;
invokevirtual java/util/Date/getTime()J
ldc2_w 1000L
aload 0
invokevirtual com/amazon/android/i/c/getExpirationDurationInSeconds()J
lmul
ladd
invokespecial java/util/Date/<init>(J)V
astore 1
getstatic com/amazon/android/i/c/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Expiration should occur at time: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
aload 1
areturn
.limit locals 2
.limit stack 8
.end method

.method protected abstract getExpirationDurationInSeconds()J
.end method
