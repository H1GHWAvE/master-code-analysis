.bytecode 50.0
.class public synchronized abstract com/amazon/android/i/b
.super java/lang/Object
.implements com/amazon/android/framework/resource/b

.field private static final 'LOGGER' Lcom/amazon/android/framework/util/KiwiLogger;

.field private 'expired' Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final 'observers' Ljava/util/List;

.field protected 'taskManager' Lcom/amazon/android/framework/task/TaskManager;
.annotation visible Lcom/amazon/android/framework/resource/Resource;
.end annotation
.end field

.method static <clinit>()V
new com/amazon/android/framework/util/KiwiLogger
dup
ldc "Expirable"
invokespecial com/amazon/android/framework/util/KiwiLogger/<init>(Ljava/lang/String;)V
putstatic com/amazon/android/i/b/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
return
.limit locals 0
.limit stack 3
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/util/concurrent/atomic/AtomicBoolean
dup
iconst_0
invokespecial java/util/concurrent/atomic/AtomicBoolean/<init>(Z)V
putfield com/amazon/android/i/b/expired Ljava/util/concurrent/atomic/AtomicBoolean;
aload 0
new java/util/Vector
dup
invokespecial java/util/Vector/<init>()V
putfield com/amazon/android/i/b/observers Ljava/util/List;
return
.limit locals 1
.limit stack 4
.end method

.method private notifyObservers()V
aload 0
getfield com/amazon/android/i/b/observers Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 1
L0:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/amazon/android/i/d
aload 0
invokeinterface com/amazon/android/i/d/observe(Lcom/amazon/android/i/b;)V 1
goto L0
L1:
return
.limit locals 2
.limit stack 2
.end method

.method private scheduleExpiration()V
new com/amazon/android/i/a
dup
aload 0
invokespecial com/amazon/android/i/a/<init>(Lcom/amazon/android/i/b;)V
astore 1
aload 0
getfield com/amazon/android/i/b/taskManager Lcom/amazon/android/framework/task/TaskManager;
getstatic com/amazon/android/framework/task/pipeline/TaskPipelineId/BACKGROUND Lcom/amazon/android/framework/task/pipeline/TaskPipelineId;
aload 1
aload 0
invokevirtual com/amazon/android/i/b/getExpiration()Ljava/util/Date;
invokeinterface com/amazon/android/framework/task/TaskManager/enqueueAtTime(Lcom/amazon/android/framework/task/pipeline/TaskPipelineId;Lcom/amazon/android/framework/task/Task;Ljava/util/Date;)V 3
return
.limit locals 2
.limit stack 4
.end method

.method public final discard()V
aload 0
getfield com/amazon/android/i/b/expired Ljava/util/concurrent/atomic/AtomicBoolean;
iconst_0
iconst_1
invokevirtual java/util/concurrent/atomic/AtomicBoolean/compareAndSet(ZZ)Z
ifne L0
return
L0:
aload 0
invokespecial com/amazon/android/i/b/notifyObservers()V
return
.limit locals 1
.limit stack 3
.end method

.method protected abstract doExpiration()V
.end method

.method public expire()V
aload 0
getfield com/amazon/android/i/b/expired Ljava/util/concurrent/atomic/AtomicBoolean;
iconst_0
iconst_1
invokevirtual java/util/concurrent/atomic/AtomicBoolean/compareAndSet(ZZ)Z
ifne L0
return
L0:
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L1
getstatic com/amazon/android/i/b/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Expiring: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L1:
aload 0
invokevirtual com/amazon/android/i/b/doExpiration()V
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L2
getstatic com/amazon/android/i/b/LOGGER Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Notifying Observers of expiration: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L2:
aload 0
invokespecial com/amazon/android/i/b/notifyObservers()V
return
.limit locals 1
.limit stack 3
.end method

.method protected abstract getExpiration()Ljava/util/Date;
.end method

.method protected isExpired()Z
aload 0
getfield com/amazon/android/i/b/expired Ljava/util/concurrent/atomic/AtomicBoolean;
invokevirtual java/util/concurrent/atomic/AtomicBoolean/get()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final onResourcesPopulated()V
aload 0
invokespecial com/amazon/android/i/b/scheduleExpiration()V
aload 0
invokevirtual com/amazon/android/i/b/onResourcesPopulatedImpl()V
return
.limit locals 1
.limit stack 1
.end method

.method protected onResourcesPopulatedImpl()V
return
.limit locals 1
.limit stack 0
.end method

.method public final register(Lcom/amazon/android/i/d;)V
aload 0
getfield com/amazon/android/i/b/observers Ljava/util/List;
aload 1
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
return
.limit locals 2
.limit stack 2
.end method
