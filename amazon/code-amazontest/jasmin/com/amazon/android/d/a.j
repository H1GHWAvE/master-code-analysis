.bytecode 50.0
.class public final synchronized com/amazon/android/d/a
.super java/lang/Object

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static a()V
invokestatic android/os/Looper/getMainLooper()Landroid/os/Looper;
invokevirtual android/os/Looper/getThread()Ljava/lang/Thread;
invokevirtual java/lang/Thread/getId()J
lstore 0
invokestatic java/lang/Thread/currentThread()Ljava/lang/Thread;
invokevirtual java/lang/Thread/getId()J
lload 0
lcmp
ifeq L0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Executing thread must be thread: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
lload 0
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc ", was: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokestatic java/lang/Thread/currentThread()Ljava/lang/Thread;
invokevirtual java/lang/Thread/getId()J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/amazon/android/d/a/a(Ljava/lang/String;)V
L0:
return
.limit locals 2
.limit stack 4
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/String;)V
aload 0
ifnonnull L0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Argument: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " cannot be null"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/amazon/android/d/a/a(Ljava/lang/String;)V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method private static a(Ljava/lang/String;)V
new com/amazon/android/d/b
dup
aload 0
invokespecial com/amazon/android/d/b/<init>(Ljava/lang/String;)V
athrow
.limit locals 1
.limit stack 3
.end method

.method public static a(ZLjava/lang/String;)V
iload 0
ifne L0
aload 1
invokestatic com/amazon/android/d/a/a(Ljava/lang/String;)V
L0:
return
.limit locals 2
.limit stack 1
.end method

.method public static b(ZLjava/lang/String;)V
iload 0
ifeq L0
aload 1
invokestatic com/amazon/android/d/a/a(Ljava/lang/String;)V
L0:
return
.limit locals 2
.limit stack 1
.end method
