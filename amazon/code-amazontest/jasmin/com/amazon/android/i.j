.bytecode 50.0
.class public final synchronized com/amazon/android/i
.super com/amazon/android/framework/task/command/AbstractCommandTask

.field private static final 'a' Lcom/amazon/android/framework/util/KiwiLogger;

.field private 'b' Lcom/amazon/android/framework/prompt/PromptManager;
.annotation visible Lcom/amazon/android/framework/resource/Resource;
.end annotation
.end field

.method static <clinit>()V
new com/amazon/android/framework/util/KiwiLogger
dup
ldc "CheckIfAppisBlockedTask"
invokespecial com/amazon/android/framework/util/KiwiLogger/<init>(Ljava/lang/String;)V
putstatic com/amazon/android/i/a Lcom/amazon/android/framework/util/KiwiLogger;
return
.limit locals 0
.limit stack 3
.end method

.method public <init>()V
aload 0
invokespecial com/amazon/android/framework/task/command/AbstractCommandTask/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method protected final getCommandData()Ljava/util/Map;
aconst_null
areturn
.limit locals 1
.limit stack 1
.end method

.method protected final getCommandName()Ljava/lang/String;
ldc "check_blocked_status"
areturn
.limit locals 1
.limit stack 1
.end method

.method protected final getCommandVersion()Ljava/lang/String;
ldc "1.0"
areturn
.limit locals 1
.limit stack 1
.end method

.method protected final isExecutionNeeded()Z
iconst_1
ireturn
.limit locals 1
.limit stack 1
.end method

.method protected final onFailure(Lcom/amazon/venezia/command/FailureResult;)V
.throws android/os/RemoteException
.throws com/amazon/android/framework/exception/KiwiException
aload 0
invokevirtual com/amazon/android/i/isWorkflowChild()Z
ifeq L0
aload 0
invokevirtual com/amazon/android/i/quitParentWorkflow()V
L0:
getstatic com/amazon/android/i/a Lcom/amazon/android/framework/util/KiwiLogger;
ldc "app is blocked, killing"
invokevirtual com/amazon/android/framework/util/KiwiLogger/test(Ljava/lang/String;)V
new com/amazon/android/framework/prompt/c
dup
new com/amazon/android/framework/prompt/PromptContent
dup
aload 1
invokeinterface com/amazon/venezia/command/FailureResult/getDisplayableName()Ljava/lang/String; 0
aload 1
invokeinterface com/amazon/venezia/command/FailureResult/getDisplayableMessage()Ljava/lang/String; 0
aload 1
invokeinterface com/amazon/venezia/command/FailureResult/getButtonLabel()Ljava/lang/String; 0
aload 1
invokeinterface com/amazon/venezia/command/FailureResult/show()Z 0
invokespecial com/amazon/android/framework/prompt/PromptContent/<init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
invokespecial com/amazon/android/framework/prompt/c/<init>(Lcom/amazon/android/framework/prompt/PromptContent;)V
astore 1
aload 0
getfield com/amazon/android/i/b Lcom/amazon/android/framework/prompt/PromptManager;
aload 1
invokeinterface com/amazon/android/framework/prompt/PromptManager/present(Lcom/amazon/android/framework/prompt/Prompt;)V 1
return
.limit locals 2
.limit stack 8
.end method

.method protected final onSuccess(Lcom/amazon/venezia/command/SuccessResult;)V
.throws android/os/RemoteException
.throws com/amazon/android/framework/exception/KiwiException
aload 1
invokeinterface com/amazon/venezia/command/SuccessResult/getData()Ljava/util/Map; 0
ifnull L0
aload 1
invokeinterface com/amazon/venezia/command/SuccessResult/getData()Ljava/util/Map; 0
ldc "verbose"
invokeinterface java/util/Map/containsKey(Ljava/lang/Object;)Z 1
ifeq L0
aload 1
invokeinterface com/amazon/venezia/command/SuccessResult/getData()Ljava/util/Map; 0
ldc "verbose"
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/lang/Boolean
invokevirtual java/lang/Boolean/booleanValue()Z
istore 2
iload 2
putstatic com/amazon/android/framework/util/KiwiLogger/ERROR_ON Z
iload 2
putstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
L0:
return
.limit locals 3
.limit stack 2
.end method
