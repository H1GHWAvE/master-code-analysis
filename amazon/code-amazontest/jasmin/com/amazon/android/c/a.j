.bytecode 50.0
.class public final synchronized com/amazon/android/c/a
.super java/lang/Object
.implements com/amazon/android/c/d
.implements com/amazon/android/framework/resource/b
.implements java/lang/Thread$UncaughtExceptionHandler

.field private static final 'a' Lcom/amazon/android/framework/util/KiwiLogger;

.field private 'b' Lcom/amazon/android/n/g;
.annotation visible Lcom/amazon/android/framework/resource/Resource;
.end annotation
.end field

.field private 'c' Lcom/amazon/android/g/c;
.annotation visible Lcom/amazon/android/framework/resource/Resource;
.end annotation
.end field

.field private 'd' Landroid/app/Application;
.annotation visible Lcom/amazon/android/framework/resource/Resource;
.end annotation
.end field

.field private 'e' Ljava/lang/Thread$UncaughtExceptionHandler;

.field private 'f' Ljava/util/Map;

.method static <clinit>()V
new com/amazon/android/framework/util/KiwiLogger
dup
ldc "CrashManagerImpl"
invokespecial com/amazon/android/framework/util/KiwiLogger/<init>(Ljava/lang/String;)V
putstatic com/amazon/android/c/a/a Lcom/amazon/android/framework/util/KiwiLogger;
return
.limit locals 0
.limit stack 3
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
putfield com/amazon/android/c/a/f Ljava/util/Map;
return
.limit locals 1
.limit stack 3
.end method

.method static synthetic a(Lcom/amazon/android/c/a;)Ljava/util/Map;
aload 0
getfield com/amazon/android/c/a/f Ljava/util/Map;
areturn
.limit locals 1
.limit stack 1
.end method

.method private a(Ljava/lang/String;)V
.catch all from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L5
.catch all from L3 to L4 using L6
.catch java/lang/Exception from L4 to L7 using L8
.catch all from L4 to L7 using L9
.catch all from L7 to L10 using L2
.catch all from L11 to L12 using L6
.catch all from L13 to L14 using L6
.catch all from L14 to L15 using L2
.catch all from L16 to L9 using L2
aload 0
monitorenter
L0:
new java/util/Random
dup
invokespecial java/util/Random/<init>()V
ldc_w 99999
invokevirtual java/util/Random/nextInt(I)I
istore 2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "s-"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 2
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ".amzst"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 4
L1:
aconst_null
astore 3
aconst_null
astore 5
L3:
aload 0
getfield com/amazon/android/c/a/d Landroid/app/Application;
aload 4
iconst_0
invokevirtual android/app/Application/openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;
astore 4
L4:
aload 4
aload 1
invokevirtual java/lang/String/getBytes()[B
invokevirtual java/io/FileOutputStream/write([B)V
L7:
aload 4
invokestatic com/amazon/android/framework/util/a/a(Ljava/io/OutputStream;)V
L10:
aload 0
monitorexit
return
L5:
astore 4
aload 5
astore 1
L17:
aload 1
astore 3
L11:
getstatic com/amazon/android/framework/util/KiwiLogger/ERROR_ON Z
ifeq L14
L12:
aload 1
astore 3
L13:
getstatic com/amazon/android/c/a/a Lcom/amazon/android/framework/util/KiwiLogger;
ldc "Coud not save crash report to file"
aload 4
invokevirtual com/amazon/android/framework/util/KiwiLogger/error(Ljava/lang/String;Ljava/lang/Throwable;)V
L14:
aload 1
invokestatic com/amazon/android/framework/util/a/a(Ljava/io/OutputStream;)V
L15:
goto L10
L2:
astore 1
aload 0
monitorexit
aload 1
athrow
L6:
astore 1
L16:
aload 3
invokestatic com/amazon/android/framework/util/a/a(Ljava/io/OutputStream;)V
aload 1
athrow
L9:
astore 1
aload 4
astore 3
goto L16
L8:
astore 3
aload 4
astore 1
aload 3
astore 4
goto L17
.limit locals 6
.limit stack 3
.end method

.method private b(Ljava/lang/String;)Lcom/amazon/android/c/b;
.catch java/lang/Exception from L0 to L1 using L2
L0:
aload 1
invokestatic com/amazon/android/c/a/c(Ljava/lang/String;)Ljava/lang/String;
astore 2
aload 0
getfield com/amazon/android/c/a/c Lcom/amazon/android/g/c;
aload 2
invokeinterface com/amazon/android/g/c/b(Ljava/lang/String;)Ljava/lang/String; 1
invokestatic com/amazon/android/u/a/a(Ljava/lang/String;)Ljava/lang/Object;
checkcast com/amazon/android/c/b
astore 2
L1:
aload 2
areturn
L2:
astore 2
getstatic com/amazon/android/framework/util/KiwiLogger/ERROR_ON Z
ifeq L3
getstatic com/amazon/android/c/a/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Failed to load crash report: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/error(Ljava/lang/String;)V
L3:
aconst_null
areturn
.limit locals 3
.limit stack 3
.end method

.method private b()Z
aload 0
getfield com/amazon/android/c/a/f Ljava/util/Map;
invokeinterface java/util/Map/size()I 0
iconst_5
if_icmplt L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method private static c(Ljava/lang/String;)Ljava/lang/String;
.throws java/io/IOException
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L4
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
astore 2
aconst_null
astore 1
L0:
new java/io/BufferedReader
dup
new java/io/FileReader
dup
aload 0
invokespecial java/io/FileReader/<init>(Ljava/lang/String;)V
invokespecial java/io/BufferedReader/<init>(Ljava/io/Reader;)V
astore 0
L1:
aload 0
invokevirtual java/io/BufferedReader/ready()Z
ifeq L5
aload 2
aload 0
invokevirtual java/io/BufferedReader/readLine()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
L3:
goto L1
L4:
astore 2
aload 0
astore 1
aload 2
astore 0
L6:
aload 1
invokestatic com/amazon/android/framework/util/a/a(Ljava/io/Reader;)V
aload 0
athrow
L5:
aload 0
invokestatic com/amazon/android/framework/util/a/a(Ljava/io/Reader;)V
aload 2
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
L2:
astore 0
goto L6
.limit locals 3
.limit stack 5
.end method

.method private static d(Ljava/lang/String;)V
.catch java/lang/Exception from L0 to L1 using L2
L0:
new java/io/File
dup
aload 0
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/delete()Z
pop
L1:
return
L2:
astore 1
getstatic com/amazon/android/framework/util/KiwiLogger/ERROR_ON Z
ifeq L1
getstatic com/amazon/android/c/a/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Cannot delete file: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 1
invokevirtual com/amazon/android/framework/util/KiwiLogger/error(Ljava/lang/String;Ljava/lang/Throwable;)V
return
.limit locals 2
.limit stack 3
.end method

.method public final a()Ljava/util/List;
aload 0
invokespecial com/amazon/android/c/a/b()Z
ifeq L0
invokestatic java/util/Collections/emptyList()Ljava/util/List;
astore 2
L1:
aload 2
areturn
L0:
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 3
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
getfield com/amazon/android/c/a/d Landroid/app/Application;
invokevirtual android/app/Application/getFilesDir()Ljava/io/File;
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
new com/amazon/android/c/c
dup
aload 0
invokespecial com/amazon/android/c/c/<init>(Lcom/amazon/android/c/a;)V
invokevirtual java/io/File/list(Ljava/io/FilenameFilter;)[Ljava/lang/String;
astore 4
iconst_0
istore 1
L2:
aload 3
astore 2
iload 1
aload 4
arraylength
if_icmpge L1
aload 3
astore 2
aload 0
invokespecial com/amazon/android/c/a/b()Z
ifne L1
aload 4
iload 1
aaload
astore 2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
getfield com/amazon/android/c/a/d Landroid/app/Application;
invokevirtual android/app/Application/getFilesDir()Ljava/io/File;
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 2
aload 0
aload 2
invokespecial com/amazon/android/c/a/b(Ljava/lang/String;)Lcom/amazon/android/c/b;
astore 5
aload 5
ifnull L3
aload 0
getfield com/amazon/android/c/a/f Ljava/util/Map;
aload 5
aload 2
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
aload 3
aload 5
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
L4:
iload 1
iconst_1
iadd
istore 1
goto L2
L3:
aload 2
invokestatic com/amazon/android/c/a/d(Ljava/lang/String;)V
goto L4
.limit locals 6
.limit stack 4
.end method

.method public final a(Ljava/util/List;)V
aload 1
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 1
L0:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/amazon/android/c/b
astore 2
aload 0
getfield com/amazon/android/c/a/f Ljava/util/Map;
aload 2
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/lang/String
invokestatic com/amazon/android/c/a/d(Ljava/lang/String;)V
aload 0
getfield com/amazon/android/c/a/f Ljava/util/Map;
aload 2
invokeinterface java/util/Map/remove(Ljava/lang/Object;)Ljava/lang/Object; 1
pop
goto L0
L1:
return
.limit locals 3
.limit stack 2
.end method

.method public final onResourcesPopulated()V
invokestatic com/amazon/android/d/a/a()V
invokestatic java/lang/Thread/getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;
instanceof com/amazon/android/c/d
ifne L0
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L1
getstatic com/amazon/android/c/a/a Lcom/amazon/android/framework/util/KiwiLogger;
ldc "Registering Crash Handler"
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L1:
aload 0
invokestatic java/lang/Thread/getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;
putfield com/amazon/android/c/a/e Ljava/lang/Thread$UncaughtExceptionHandler;
aload 0
invokestatic java/lang/Thread/setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V
L0:
return
.limit locals 1
.limit stack 2
.end method

.method public final uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V
.catch java/lang/Throwable from L0 to L1 using L2
.catch java/lang/Throwable from L1 to L3 using L4
.catch java/lang/Throwable from L5 to L6 using L4
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L0
getstatic com/amazon/android/c/a/a Lcom/amazon/android/framework/util/KiwiLogger;
ldc "Crash detected"
aload 2
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;Ljava/lang/Throwable;)V
L0:
new com/amazon/android/c/b
dup
aload 0
getfield com/amazon/android/c/a/d Landroid/app/Application;
aload 2
invokespecial com/amazon/android/c/b/<init>(Landroid/app/Application;Ljava/lang/Throwable;)V
invokestatic com/amazon/android/u/a/a(Ljava/io/Serializable;)Ljava/lang/String;
astore 3
aload 0
aload 0
getfield com/amazon/android/c/a/c Lcom/amazon/android/g/c;
aload 3
invokeinterface com/amazon/android/g/c/a(Ljava/lang/String;)Ljava/lang/String; 1
invokespecial com/amazon/android/c/a/a(Ljava/lang/String;)V
L1:
aload 0
getfield com/amazon/android/c/a/b Lcom/amazon/android/n/g;
new com/amazon/android/a/b
dup
invokespecial com/amazon/android/a/b/<init>()V
invokeinterface com/amazon/android/n/g/a(Lcom/amazon/android/n/d;)V 1
L3:
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L7
getstatic com/amazon/android/c/a/a Lcom/amazon/android/framework/util/KiwiLogger;
ldc "Calling previous handler"
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L7:
aload 0
getfield com/amazon/android/c/a/e Ljava/lang/Thread$UncaughtExceptionHandler;
ifnull L8
aload 0
getfield com/amazon/android/c/a/e Ljava/lang/Thread$UncaughtExceptionHandler;
aload 1
aload 2
invokeinterface java/lang/Thread$UncaughtExceptionHandler/uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V 2
L8:
return
L2:
astore 3
L5:
getstatic com/amazon/android/framework/util/KiwiLogger/ERROR_ON Z
ifeq L1
getstatic com/amazon/android/c/a/a Lcom/amazon/android/framework/util/KiwiLogger;
ldc "Could not handle uncaught exception"
aload 3
invokevirtual com/amazon/android/framework/util/KiwiLogger/error(Ljava/lang/String;Ljava/lang/Throwable;)V
L6:
goto L1
L4:
astore 3
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L3
getstatic com/amazon/android/c/a/a Lcom/amazon/android/framework/util/KiwiLogger;
ldc "Error occured while handling exception"
aload 3
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;Ljava/lang/Throwable;)V
goto L3
.limit locals 4
.limit stack 4
.end method
