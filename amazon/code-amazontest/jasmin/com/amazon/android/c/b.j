.bytecode 50.0
.class public final synchronized com/amazon/android/c/b
.super java/lang/Object
.implements java/io/Serializable

.field private static final 'a' Lcom/amazon/android/framework/util/KiwiLogger;

.field private static final 'serialVersionUID' J = 1L


.field private final 'b' Ljava/util/HashMap;

.method static <clinit>()V
new com/amazon/android/framework/util/KiwiLogger
dup
ldc "CrashReport"
invokespecial com/amazon/android/framework/util/KiwiLogger/<init>(Ljava/lang/String;)V
putstatic com/amazon/android/c/b/a Lcom/amazon/android/framework/util/KiwiLogger;
return
.limit locals 0
.limit stack 3
.end method

.method public <init>(Landroid/app/Application;Ljava/lang/Throwable;)V
.catch java/lang/Throwable from L0 to L1 using L2
.catch java/lang/Throwable from L3 to L4 using L2
.catch java/lang/Throwable from L4 to L5 using L2
.catch java/lang/Throwable from L6 to L7 using L2
.catch java/lang/Throwable from L7 to L8 using L2
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/util/LinkedHashMap
dup
invokespecial java/util/LinkedHashMap/<init>()V
putfield com/amazon/android/c/b/b Ljava/util/HashMap;
L0:
aload 0
getfield com/amazon/android/c/b/b Ljava/util/HashMap;
ldc "crashTime"
new java/util/Date
dup
invokespecial java/util/Date/<init>()V
invokevirtual java/util/Date/toString()Ljava/lang/String;
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
invokestatic com/amazon/android/c/b/a(Landroid/app/Application;)Landroid/content/pm/PackageInfo;
astore 5
L1:
aload 5
ifnull L4
L3:
aload 0
getfield com/amazon/android/c/b/b Ljava/util/HashMap;
ldc "packageVersionName"
aload 5
getfield android/content/pm/PackageInfo/versionName Ljava/lang/String;
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
getfield com/amazon/android/c/b/b Ljava/util/HashMap;
ldc "packageName"
aload 5
getfield android/content/pm/PackageInfo/packageName Ljava/lang/String;
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
getfield com/amazon/android/c/b/b Ljava/util/HashMap;
ldc "packageFilePath"
aload 1
invokevirtual android/app/Application/getFilesDir()Ljava/io/File;
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
L4:
aload 0
getfield com/amazon/android/c/b/b Ljava/util/HashMap;
ldc "deviceModel"
getstatic android/os/Build/MODEL Ljava/lang/String;
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
getfield com/amazon/android/c/b/b Ljava/util/HashMap;
ldc "androidVersion"
getstatic android/os/Build$VERSION/RELEASE Ljava/lang/String;
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
getfield com/amazon/android/c/b/b Ljava/util/HashMap;
ldc "deviceBoard"
getstatic android/os/Build/BOARD Ljava/lang/String;
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
getfield com/amazon/android/c/b/b Ljava/util/HashMap;
ldc "deviceBrand"
getstatic android/os/Build/BRAND Ljava/lang/String;
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
getfield com/amazon/android/c/b/b Ljava/util/HashMap;
ldc "deviceDisplay"
getstatic android/os/Build/DISPLAY Ljava/lang/String;
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
getfield com/amazon/android/c/b/b Ljava/util/HashMap;
ldc "deviceFingerPrint"
getstatic android/os/Build/FINGERPRINT Ljava/lang/String;
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
getfield com/amazon/android/c/b/b Ljava/util/HashMap;
ldc "deviceHost"
getstatic android/os/Build/HOST Ljava/lang/String;
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
getfield com/amazon/android/c/b/b Ljava/util/HashMap;
ldc "deviceId"
getstatic android/os/Build/ID Ljava/lang/String;
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
getfield com/amazon/android/c/b/b Ljava/util/HashMap;
ldc "deviceManufacturer"
getstatic android/os/Build/MANUFACTURER Ljava/lang/String;
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
getfield com/amazon/android/c/b/b Ljava/util/HashMap;
ldc "deviceProduct"
getstatic android/os/Build/PRODUCT Ljava/lang/String;
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
getfield com/amazon/android/c/b/b Ljava/util/HashMap;
ldc "deviceTags"
getstatic android/os/Build/TAGS Ljava/lang/String;
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
getfield com/amazon/android/c/b/b Ljava/util/HashMap;
ldc "deviceTime"
getstatic android/os/Build/TIME J
invokestatic java/lang/Long/toString(J)Ljava/lang/String;
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
getfield com/amazon/android/c/b/b Ljava/util/HashMap;
ldc "deviceType"
getstatic android/os/Build/TYPE Ljava/lang/String;
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
getfield com/amazon/android/c/b/b Ljava/util/HashMap;
ldc "deviceUser"
getstatic android/os/Build/USER Ljava/lang/String;
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
getfield com/amazon/android/c/b/b Ljava/util/HashMap;
astore 5
new android/os/StatFs
dup
invokestatic android/os/Environment/getDataDirectory()Ljava/io/File;
invokevirtual java/io/File/getPath()Ljava/lang/String;
invokespecial android/os/StatFs/<init>(Ljava/lang/String;)V
astore 6
aload 6
invokevirtual android/os/StatFs/getBlockSize()I
i2l
lstore 3
aload 5
ldc "totalInternalMemorySize"
aload 6
invokevirtual android/os/StatFs/getBlockCount()I
i2l
lload 3
lmul
invokestatic java/lang/Long/toString(J)Ljava/lang/String;
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
getfield com/amazon/android/c/b/b Ljava/util/HashMap;
astore 5
new android/os/StatFs
dup
invokestatic android/os/Environment/getDataDirectory()Ljava/io/File;
invokevirtual java/io/File/getPath()Ljava/lang/String;
invokespecial android/os/StatFs/<init>(Ljava/lang/String;)V
astore 6
aload 6
invokevirtual android/os/StatFs/getBlockSize()I
i2l
lstore 3
aload 5
ldc "availableInternalMemorySize"
aload 6
invokevirtual android/os/StatFs/getAvailableBlocks()I
i2l
lload 3
lmul
invokestatic java/lang/Long/toString(J)Ljava/lang/String;
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
ldc "activity"
invokevirtual android/app/Application/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/app/ActivityManager
astore 1
L5:
aload 1
ifnull L7
L6:
new android/app/ActivityManager$MemoryInfo
dup
invokespecial android/app/ActivityManager$MemoryInfo/<init>()V
astore 5
aload 1
aload 5
invokevirtual android/app/ActivityManager/getMemoryInfo(Landroid/app/ActivityManager$MemoryInfo;)V
aload 0
getfield com/amazon/android/c/b/b Ljava/util/HashMap;
ldc "memLowFlag"
aload 5
getfield android/app/ActivityManager$MemoryInfo/lowMemory Z
invokestatic java/lang/Boolean/toString(Z)Ljava/lang/String;
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
getfield com/amazon/android/c/b/b Ljava/util/HashMap;
ldc "memLowThreshold"
aload 5
getfield android/app/ActivityManager$MemoryInfo/threshold J
invokestatic java/lang/Long/toString(J)Ljava/lang/String;
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
L7:
aload 0
getfield com/amazon/android/c/b/b Ljava/util/HashMap;
ldc "nativeHeapSize"
invokestatic android/os/Debug/getNativeHeapSize()J
invokestatic java/lang/Long/toString(J)Ljava/lang/String;
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
getfield com/amazon/android/c/b/b Ljava/util/HashMap;
ldc "nativeHeapFreeSize"
invokestatic android/os/Debug/getNativeHeapAllocatedSize()J
invokestatic java/lang/Long/toString(J)Ljava/lang/String;
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
getfield com/amazon/android/c/b/b Ljava/util/HashMap;
ldc "threadAllocCount"
invokestatic android/os/Debug/getThreadAllocCount()I
i2l
invokestatic java/lang/Long/toString(J)Ljava/lang/String;
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
getfield com/amazon/android/c/b/b Ljava/util/HashMap;
ldc "threadAllocSize"
invokestatic android/os/Debug/getThreadAllocSize()I
i2l
invokestatic java/lang/Long/toString(J)Ljava/lang/String;
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
aload 2
invokespecial com/amazon/android/c/b/a(Ljava/lang/Throwable;)V
aload 0
invokespecial com/amazon/android/c/b/b()V
aload 0
invokespecial com/amazon/android/c/b/c()V
L8:
return
L2:
astore 1
getstatic com/amazon/android/framework/util/KiwiLogger/ERROR_ON Z
ifeq L8
getstatic com/amazon/android/c/b/a Lcom/amazon/android/framework/util/KiwiLogger;
ldc "Error collection crash report details"
aload 1
invokevirtual com/amazon/android/framework/util/KiwiLogger/error(Ljava/lang/String;Ljava/lang/Throwable;)V
return
.limit locals 7
.limit stack 6
.end method

.method private static a(Landroid/app/Application;)Landroid/content/pm/PackageInfo;
.catch android/content/pm/PackageManager$NameNotFoundException from L0 to L1 using L2
aload 0
invokevirtual android/app/Application/getPackageManager()Landroid/content/pm/PackageManager;
astore 1
L0:
aload 1
aload 0
invokevirtual android/app/Application/getPackageName()Ljava/lang/String;
iconst_0
invokevirtual android/content/pm/PackageManager/getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
astore 0
L1:
aload 0
areturn
L2:
astore 0
getstatic com/amazon/android/framework/util/KiwiLogger/ERROR_ON Z
ifeq L3
getstatic com/amazon/android/c/b/a Lcom/amazon/android/framework/util/KiwiLogger;
ldc "Unable to fetch package info"
aload 0
invokevirtual com/amazon/android/framework/util/KiwiLogger/error(Ljava/lang/String;Ljava/lang/Throwable;)V
L3:
aconst_null
areturn
.limit locals 2
.limit stack 3
.end method

.method private a(Ljava/lang/Throwable;)V
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
astore 2
new java/io/StringWriter
dup
invokespecial java/io/StringWriter/<init>()V
astore 3
new java/io/PrintWriter
dup
aload 3
invokespecial java/io/PrintWriter/<init>(Ljava/io/Writer;)V
astore 4
aload 1
aload 4
invokevirtual java/lang/Throwable/printStackTrace(Ljava/io/PrintWriter;)V
aload 2
aload 3
invokevirtual java/lang/Object/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 2
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 1
invokevirtual java/lang/Throwable/getCause()Ljava/lang/Throwable;
astore 1
L0:
aload 1
ifnull L1
aload 1
aload 4
invokevirtual java/lang/Throwable/printStackTrace(Ljava/io/PrintWriter;)V
aload 2
aload 3
invokevirtual java/lang/Object/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 1
invokevirtual java/lang/Throwable/getCause()Ljava/lang/Throwable;
astore 1
aload 2
ldc "\n\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
goto L0
L1:
aload 4
invokevirtual java/io/PrintWriter/close()V
aload 0
getfield com/amazon/android/c/b/b Ljava/util/HashMap;
ldc "stackTrace"
aload 2
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
return
.limit locals 5
.limit stack 3
.end method

.method private b()V
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
astore 2
invokestatic java/lang/Thread/getAllStackTraces()Ljava/util/Map;
invokeinterface java/util/Map/entrySet()Ljava/util/Set; 0
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 3
L0:
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Map$Entry
astore 5
aload 5
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
checkcast java/lang/Thread
astore 4
aload 5
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
checkcast [Ljava/lang/StackTraceElement;
astore 5
aload 2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Thread : "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/Thread/getId()J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 4
invokevirtual java/lang/Thread/getName()Ljava/lang/String;
invokestatic com/amazon/android/framework/util/b/a(Ljava/lang/String;)Z
ifne L2
aload 2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/Thread/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
L2:
aload 2
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "isAlive : "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/Thread/isAlive()Z
invokevirtual java/lang/StringBuilder/append(Z)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "isInterrupted : "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/Thread/isInterrupted()Z
invokevirtual java/lang/StringBuilder/append(Z)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "isDaemon : "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/Thread/isDaemon()Z
invokevirtual java/lang/StringBuilder/append(Z)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
iconst_0
istore 1
L3:
iload 1
aload 5
arraylength
if_icmpge L4
aload 2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "\u0009at "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 5
iload 1
aaload
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
iload 1
iconst_1
iadd
istore 1
goto L3
L4:
aload 2
ldc "\n\n"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
goto L0
L1:
aload 0
getfield com/amazon/android/c/b/b Ljava/util/HashMap;
ldc "threadDump"
aload 2
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
return
.limit locals 6
.limit stack 4
.end method

.method private c()V
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch java/lang/Exception from L4 to L5 using L2
.catch java/lang/Exception from L6 to L7 using L2
L0:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
astore 1
aload 1
aload 0
getfield com/amazon/android/c/b/b Ljava/util/HashMap;
ldc "packageName"
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/amazon/android/c/b/b Ljava/util/HashMap;
ldc "packageVersionName"
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/amazon/android/c/b/b Ljava/util/HashMap;
ldc "androidVersion"
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
aload 0
getfield com/amazon/android/c/b/b Ljava/util/HashMap;
ldc "stackTrace"
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
astore 2
L1:
aload 2
ifnull L6
L3:
ldc "([a-zA-Z0-9_.]+(Exception|Error))|(at\\s.*\\(.*\\))"
invokestatic java/util/regex/Pattern/compile(Ljava/lang/String;)Ljava/util/regex/Pattern;
aload 2
invokevirtual java/util/regex/Pattern/matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
astore 2
L4:
aload 2
invokevirtual java/util/regex/Matcher/find()Z
ifeq L6
aload 1
aload 2
invokevirtual java/util/regex/Matcher/group()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
L5:
goto L4
L2:
astore 1
getstatic com/amazon/android/framework/util/KiwiLogger/ERROR_ON Z
ifeq L8
getstatic com/amazon/android/c/b/a Lcom/amazon/android/framework/util/KiwiLogger;
ldc "Error capturing crash id"
aload 1
invokevirtual com/amazon/android/framework/util/KiwiLogger/error(Ljava/lang/String;Ljava/lang/Throwable;)V
L8:
return
L6:
new java/math/BigInteger
dup
ldc "SHA1"
invokestatic java/security/MessageDigest/getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
aload 1
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
ldc "UTF-8"
invokevirtual java/lang/String/getBytes(Ljava/lang/String;)[B
invokevirtual java/security/MessageDigest/digest([B)[B
invokespecial java/math/BigInteger/<init>([B)V
invokevirtual java/math/BigInteger/abs()Ljava/math/BigInteger;
bipush 16
invokevirtual java/math/BigInteger/toString(I)Ljava/lang/String;
astore 1
aload 0
getfield com/amazon/android/c/b/b Ljava/util/HashMap;
ldc "crashId"
aload 1
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
L7:
return
.limit locals 3
.limit stack 5
.end method

.method public final a()Ljava/util/Map;
aload 0
getfield com/amazon/android/c/b/b Ljava/util/HashMap;
areturn
.limit locals 1
.limit stack 1
.end method
