.bytecode 50.0
.class public synchronized abstract com/amazon/android/l/c
.super java/lang/Object
.implements com/amazon/android/l/a

.field private 'workflow' Lcom/amazon/android/l/b;

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method protected final isWorkflowChild()Z
aload 0
getfield com/amazon/android/l/c/workflow Lcom/amazon/android/l/b;
ifnull L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method protected final quitParentWorkflow()V
aload 0
invokevirtual com/amazon/android/l/c/isWorkflowChild()Z
ldc "task is no a workflow child"
invokestatic com/amazon/android/d/a/a(ZLjava/lang/String;)V
aload 0
getfield com/amazon/android/l/c/workflow Lcom/amazon/android/l/b;
invokevirtual com/amazon/android/l/b/c()V
return
.limit locals 1
.limit stack 2
.end method

.method public final setWorkflow(Lcom/amazon/android/l/b;)V
aload 1
ldc "workflow"
invokestatic com/amazon/android/d/a/a(Ljava/lang/Object;Ljava/lang/String;)V
aload 0
getfield com/amazon/android/l/c/workflow Lcom/amazon/android/l/b;
ifnonnull L0
iconst_1
istore 2
L1:
iload 2
ldc "workflow instance can only be set once"
invokestatic com/amazon/android/d/a/a(ZLjava/lang/String;)V
aload 0
aload 1
putfield com/amazon/android/l/c/workflow Lcom/amazon/android/l/b;
return
L0:
iconst_0
istore 2
goto L1
.limit locals 3
.limit stack 2
.end method
