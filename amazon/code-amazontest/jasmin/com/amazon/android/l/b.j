.bytecode 50.0
.class public synchronized abstract com/amazon/android/l/b
.super com/amazon/android/l/c
.implements com/amazon/android/framework/resource/b

.field private static final 'a' Lcom/amazon/android/framework/util/KiwiLogger;

.field private 'b' Lcom/amazon/android/framework/resource/a;
.annotation visible Lcom/amazon/android/framework/resource/Resource;
.end annotation
.end field

.field private 'c' Ljava/util/List;

.field private 'd' Ljava/util/concurrent/atomic/AtomicBoolean;

.method static <clinit>()V
new com/amazon/android/framework/util/KiwiLogger
dup
ldc "TaskWorkflow"
invokespecial com/amazon/android/framework/util/KiwiLogger/<init>(Ljava/lang/String;)V
putstatic com/amazon/android/l/b/a Lcom/amazon/android/framework/util/KiwiLogger;
return
.limit locals 0
.limit stack 3
.end method

.method public <init>()V
aload 0
invokespecial com/amazon/android/l/c/<init>()V
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield com/amazon/android/l/b/c Ljava/util/List;
aload 0
new java/util/concurrent/atomic/AtomicBoolean
dup
iconst_0
invokespecial java/util/concurrent/atomic/AtomicBoolean/<init>(Z)V
putfield com/amazon/android/l/b/d Ljava/util/concurrent/atomic/AtomicBoolean;
return
.limit locals 1
.limit stack 4
.end method

.method protected a()V
return
.limit locals 1
.limit stack 0
.end method

.method protected final a(Lcom/amazon/android/framework/task/Task;)V
aload 1
ldc "task"
invokestatic com/amazon/android/d/a/a(Ljava/lang/Object;Ljava/lang/String;)V
aload 0
getfield com/amazon/android/l/b/c Ljava/util/List;
aload 1
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 1
instanceof com/amazon/android/l/a
ifeq L0
aload 1
checkcast com/amazon/android/l/a
aload 0
invokeinterface com/amazon/android/l/a/setWorkflow(Lcom/amazon/android/l/b;)V 1
L0:
return
.limit locals 2
.limit stack 2
.end method

.method protected abstract b()Ljava/lang/String;
.end method

.method public final c()V
aload 0
getfield com/amazon/android/l/b/d Ljava/util/concurrent/atomic/AtomicBoolean;
iconst_1
invokevirtual java/util/concurrent/atomic/AtomicBoolean/set(Z)V
return
.limit locals 1
.limit stack 2
.end method

.method public final execute()V
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L2
.catch all from L3 to L4 using L2
.catch all from L5 to L6 using L2
L0:
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L1
getstatic com/amazon/android/l/b/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Exiting task workflow: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L1:
aload 0
getfield com/amazon/android/l/b/c Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 1
L3:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L4
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/amazon/android/framework/task/Task
astore 2
aload 0
getfield com/amazon/android/l/b/d Ljava/util/concurrent/atomic/AtomicBoolean;
invokevirtual java/util/concurrent/atomic/AtomicBoolean/get()Z
ifeq L5
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L4
getstatic com/amazon/android/l/b/a Lcom/amazon/android/framework/util/KiwiLogger;
ldc "Finished set, exiting task workflow early"
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L4:
aload 0
invokevirtual com/amazon/android/l/b/a()V
return
L5:
aload 2
invokeinterface com/amazon/android/framework/task/Task/execute()V 0
L6:
goto L3
L2:
astore 1
aload 0
invokevirtual com/amazon/android/l/b/a()V
aload 1
athrow
.limit locals 3
.limit stack 3
.end method

.method public final onResourcesPopulated()V
aload 0
getfield com/amazon/android/l/b/c Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 1
L0:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/amazon/android/framework/task/Task
astore 2
aload 0
getfield com/amazon/android/l/b/b Lcom/amazon/android/framework/resource/a;
aload 2
invokeinterface com/amazon/android/framework/resource/a/b(Ljava/lang/Object;)V 1
goto L0
L1:
return
.limit locals 3
.limit stack 2
.end method

.method public final toString()Ljava/lang/String;
aload 0
invokevirtual com/amazon/android/l/b/b()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method
