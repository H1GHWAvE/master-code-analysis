.bytecode 50.0
.class public final synchronized com/amazon/android/licensing/f
.super java/lang/Object

.field final 'a' Ljava/lang/String;

.field final 'b' Ljava/lang/String;

.field final 'c' Ljava/lang/String;

.field final 'd' Ljava/util/Date;

.field final 'e' Ljava/lang/String;

.method public <init>(Lcom/amazon/android/m/d;)V
.throws com/amazon/android/h/b
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
ldc "checksum"
aload 1
invokestatic com/amazon/android/licensing/f/a(Ljava/lang/String;Lcom/amazon/android/m/d;)Ljava/lang/String;
putfield com/amazon/android/licensing/f/a Ljava/lang/String;
aload 0
ldc "customerId"
aload 1
invokestatic com/amazon/android/licensing/f/a(Ljava/lang/String;Lcom/amazon/android/m/d;)Ljava/lang/String;
putfield com/amazon/android/licensing/f/b Ljava/lang/String;
aload 0
ldc "deviceId"
aload 1
invokestatic com/amazon/android/licensing/f/a(Ljava/lang/String;Lcom/amazon/android/m/d;)Ljava/lang/String;
putfield com/amazon/android/licensing/f/c Ljava/lang/String;
aload 0
ldc "packageName"
aload 1
invokestatic com/amazon/android/licensing/f/a(Ljava/lang/String;Lcom/amazon/android/m/d;)Ljava/lang/String;
putfield com/amazon/android/licensing/f/e Ljava/lang/String;
aload 0
ldc "expiration"
aload 1
invokestatic com/amazon/android/licensing/f/b(Ljava/lang/String;Lcom/amazon/android/m/d;)Ljava/util/Date;
putfield com/amazon/android/licensing/f/d Ljava/util/Date;
return
.limit locals 2
.limit stack 3
.end method

.method private static a(Ljava/lang/String;Lcom/amazon/android/m/d;)Ljava/lang/String;
.throws com/amazon/android/h/b
aload 1
aload 0
invokevirtual com/amazon/android/m/d/a(Ljava/lang/String;)Ljava/lang/String;
astore 1
aload 1
invokestatic com/amazon/android/framework/util/b/a(Ljava/lang/String;)Z
ifeq L0
new com/amazon/android/h/b
dup
ldc "MISSING_FIELD"
aload 0
invokespecial com/amazon/android/h/b/<init>(Ljava/lang/String;Ljava/lang/String;)V
athrow
L0:
aload 1
areturn
.limit locals 2
.limit stack 4
.end method

.method private static b(Ljava/lang/String;Lcom/amazon/android/m/d;)Ljava/util/Date;
.throws com/amazon/android/h/b
.catch java/lang/NumberFormatException from L0 to L1 using L2
aload 0
aload 1
invokestatic com/amazon/android/licensing/f/a(Ljava/lang/String;Lcom/amazon/android/m/d;)Ljava/lang/String;
astore 1
L0:
new java/util/Date
dup
aload 1
invokestatic java/lang/Long/parseLong(Ljava/lang/String;)J
invokespecial java/util/Date/<init>(J)V
astore 2
L1:
aload 2
areturn
L2:
astore 2
new com/amazon/android/h/b
dup
ldc "INVALID_FIELD_VALUE"
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial com/amazon/android/h/b/<init>(Ljava/lang/String;Ljava/lang/String;)V
athrow
.limit locals 3
.limit stack 5
.end method
