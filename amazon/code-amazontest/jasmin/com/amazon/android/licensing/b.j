.bytecode 50.0
.class public final synchronized com/amazon/android/licensing/b
.super com/amazon/android/l/c

.field private static final 'a' Lcom/amazon/android/framework/util/KiwiLogger;

.field private 'b' Lcom/amazon/android/o/a;
.annotation visible Lcom/amazon/android/framework/resource/Resource;
.end annotation
.end field

.field private 'c' Lcom/amazon/android/framework/prompt/PromptManager;
.annotation visible Lcom/amazon/android/framework/resource/Resource;
.end annotation
.end field

.method static <clinit>()V
new com/amazon/android/framework/util/KiwiLogger
dup
ldc "LicenseKillTask"
invokespecial com/amazon/android/framework/util/KiwiLogger/<init>(Ljava/lang/String;)V
putstatic com/amazon/android/licensing/b/a Lcom/amazon/android/framework/util/KiwiLogger;
return
.limit locals 0
.limit stack 3
.end method

.method public <init>()V
aload 0
invokespecial com/amazon/android/l/c/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public final execute()V
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L0
getstatic com/amazon/android/licensing/b/a Lcom/amazon/android/framework/util/KiwiLogger;
ldc "License Kill Task Executing!!!"
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L0:
aload 0
getfield com/amazon/android/licensing/b/b Lcom/amazon/android/o/a;
ldc "APPLICATION_LICENSE"
invokevirtual com/amazon/android/o/a/b(Ljava/lang/String;)Z
pop
getstatic com/amazon/android/licensing/b/a Lcom/amazon/android/framework/util/KiwiLogger;
ldc "license verification succeeded"
invokevirtual com/amazon/android/framework/util/KiwiLogger/test(Ljava/lang/String;)V
return
.limit locals 1
.limit stack 2
.end method
