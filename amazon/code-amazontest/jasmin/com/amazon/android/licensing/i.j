.bytecode 50.0
.class public final synchronized com/amazon/android/licensing/i
.super java/lang/Object

.field public static final 'a' Lcom/amazon/android/framework/prompt/PromptContent;

.field public static final 'b' Lcom/amazon/android/framework/prompt/PromptContent;

.field public static final 'c' Lcom/amazon/android/framework/prompt/PromptContent;

.field public static final 'd' Lcom/amazon/android/framework/prompt/PromptContent;

.field public static final 'e' Lcom/amazon/android/framework/prompt/PromptContent;

.field public static final 'f' Lcom/amazon/android/framework/prompt/PromptContent;

.method static <clinit>()V
new com/amazon/android/framework/prompt/PromptContent
dup
ldc "Amazon Appstore required"
ldc "It looks like you no longer have an Amazon Appstore on your device. Please install an Amazon Appstore and sign in with your username and password to use this app"
ldc "OK"
iconst_1
iconst_1
invokespecial com/amazon/android/framework/prompt/PromptContent/<init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V
putstatic com/amazon/android/licensing/i/a Lcom/amazon/android/framework/prompt/PromptContent;
new com/amazon/android/framework/prompt/PromptContent
dup
ldc "Amazon Appstore: store connection failure"
ldc "An error occurred connecting to Amazon's Appstore. Please try again"
ldc "OK"
iconst_1
iconst_0
invokespecial com/amazon/android/framework/prompt/PromptContent/<init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V
putstatic com/amazon/android/licensing/i/b Lcom/amazon/android/framework/prompt/PromptContent;
new com/amazon/android/framework/prompt/PromptContent
dup
ldc "Amazon Appstore required"
ldc "Your version of the Amazon Appstore appears to be out of date.  Please visit Amazon.com to install the latest version of the Appstore."
ldc "OK"
iconst_1
iconst_1
invokespecial com/amazon/android/framework/prompt/PromptContent/<init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V
putstatic com/amazon/android/licensing/i/c Lcom/amazon/android/framework/prompt/PromptContent;
new com/amazon/android/framework/prompt/PromptContent
dup
ldc "Amazon Appstore: internet connection required"
ldc "An internet connection is required to launch this app. Please connect to the internet to continue"
ldc "OK"
iconst_1
iconst_0
invokespecial com/amazon/android/framework/prompt/PromptContent/<init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V
putstatic com/amazon/android/licensing/i/d Lcom/amazon/android/framework/prompt/PromptContent;
new com/amazon/android/framework/prompt/PromptContent
dup
ldc "Amazon Appstore: unknown error"
ldc "An error occurred. Please download this app again from the Amazon Appstore"
ldc "OK"
iconst_1
iconst_0
invokespecial com/amazon/android/framework/prompt/PromptContent/<init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V
putstatic com/amazon/android/licensing/i/e Lcom/amazon/android/framework/prompt/PromptContent;
new com/amazon/android/framework/prompt/PromptContent
dup
ldc "Amazon Appstore: internal failure"
ldc "An internal error occured, please try launching the app again"
ldc "OK"
iconst_1
iconst_0
invokespecial com/amazon/android/framework/prompt/PromptContent/<init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V
putstatic com/amazon/android/licensing/i/f Lcom/amazon/android/framework/prompt/PromptContent;
return
.limit locals 0
.limit stack 7
.end method

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method
