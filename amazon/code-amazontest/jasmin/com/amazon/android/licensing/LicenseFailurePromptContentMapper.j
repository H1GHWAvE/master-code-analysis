.bytecode 50.0
.class public synchronized com/amazon/android/licensing/LicenseFailurePromptContentMapper
.super java/lang/Object

.field private final 'mappings' Ljava/util/Map;

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
putfield com/amazon/android/licensing/LicenseFailurePromptContentMapper/mappings Ljava/util/Map;
aload 0
ldc com/amazon/android/b/g
getstatic com/amazon/android/licensing/i/a Lcom/amazon/android/framework/prompt/PromptContent;
invokespecial com/amazon/android/licensing/LicenseFailurePromptContentMapper/register(Ljava/lang/Class;Lcom/amazon/android/framework/prompt/PromptContent;)V
aload 0
ldc com/amazon/android/b/f
getstatic com/amazon/android/licensing/i/a Lcom/amazon/android/framework/prompt/PromptContent;
invokespecial com/amazon/android/licensing/LicenseFailurePromptContentMapper/register(Ljava/lang/Class;Lcom/amazon/android/framework/prompt/PromptContent;)V
aload 0
ldc com/amazon/android/b/a
getstatic com/amazon/android/licensing/i/b Lcom/amazon/android/framework/prompt/PromptContent;
invokespecial com/amazon/android/licensing/LicenseFailurePromptContentMapper/register(Ljava/lang/Class;Lcom/amazon/android/framework/prompt/PromptContent;)V
aload 0
ldc com/amazon/android/b/d
getstatic com/amazon/android/licensing/i/c Lcom/amazon/android/framework/prompt/PromptContent;
invokespecial com/amazon/android/licensing/LicenseFailurePromptContentMapper/register(Ljava/lang/Class;Lcom/amazon/android/framework/prompt/PromptContent;)V
aload 0
ldc com/amazon/android/b/e
new com/amazon/android/licensing/m
dup
invokespecial com/amazon/android/licensing/m/<init>()V
invokespecial com/amazon/android/licensing/LicenseFailurePromptContentMapper/register(Ljava/lang/Class;Lcom/amazon/android/licensing/h;)V
aload 0
ldc com/amazon/android/b/h
getstatic com/amazon/android/licensing/i/e Lcom/amazon/android/framework/prompt/PromptContent;
invokespecial com/amazon/android/licensing/LicenseFailurePromptContentMapper/register(Ljava/lang/Class;Lcom/amazon/android/framework/prompt/PromptContent;)V
aload 0
ldc com/amazon/android/b/b
getstatic com/amazon/android/licensing/i/e Lcom/amazon/android/framework/prompt/PromptContent;
invokespecial com/amazon/android/licensing/LicenseFailurePromptContentMapper/register(Ljava/lang/Class;Lcom/amazon/android/framework/prompt/PromptContent;)V
aload 0
ldc com/amazon/android/s/a
new com/amazon/android/licensing/j
dup
invokespecial com/amazon/android/licensing/j/<init>()V
invokespecial com/amazon/android/licensing/LicenseFailurePromptContentMapper/register(Ljava/lang/Class;Lcom/amazon/android/licensing/h;)V
aload 0
ldc com/amazon/android/h/a
getstatic com/amazon/android/licensing/i/e Lcom/amazon/android/framework/prompt/PromptContent;
invokespecial com/amazon/android/licensing/LicenseFailurePromptContentMapper/register(Ljava/lang/Class;Lcom/amazon/android/framework/prompt/PromptContent;)V
aload 0
ldc com/amazon/android/h/b
getstatic com/amazon/android/licensing/i/e Lcom/amazon/android/framework/prompt/PromptContent;
invokespecial com/amazon/android/licensing/LicenseFailurePromptContentMapper/register(Ljava/lang/Class;Lcom/amazon/android/framework/prompt/PromptContent;)V
aload 0
ldc com/amazon/android/h/c
getstatic com/amazon/android/licensing/i/e Lcom/amazon/android/framework/prompt/PromptContent;
invokespecial com/amazon/android/licensing/LicenseFailurePromptContentMapper/register(Ljava/lang/Class;Lcom/amazon/android/framework/prompt/PromptContent;)V
return
.limit locals 1
.limit stack 4
.end method

.method private register(Ljava/lang/Class;Lcom/amazon/android/framework/prompt/PromptContent;)V
aload 0
aload 1
new com/amazon/android/licensing/g
dup
aload 0
aload 2
invokespecial com/amazon/android/licensing/g/<init>(Lcom/amazon/android/licensing/LicenseFailurePromptContentMapper;Lcom/amazon/android/framework/prompt/PromptContent;)V
invokespecial com/amazon/android/licensing/LicenseFailurePromptContentMapper/register(Ljava/lang/Class;Lcom/amazon/android/licensing/h;)V
return
.limit locals 3
.limit stack 6
.end method

.method private register(Ljava/lang/Class;Lcom/amazon/android/licensing/h;)V
aload 0
getfield com/amazon/android/licensing/LicenseFailurePromptContentMapper/mappings Ljava/util/Map;
aload 1
invokeinterface java/util/Map/containsKey(Ljava/lang/Object;)Z 1
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "mapping exists for type: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/amazon/android/d/a/b(ZLjava/lang/String;)V
aload 0
getfield com/amazon/android/licensing/LicenseFailurePromptContentMapper/mappings Ljava/util/Map;
aload 1
aload 2
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
return
.limit locals 3
.limit stack 3
.end method

.method public map(Lcom/amazon/android/framework/exception/KiwiException;)Lcom/amazon/android/framework/prompt/PromptContent;
aload 0
getfield com/amazon/android/licensing/LicenseFailurePromptContentMapper/mappings Ljava/util/Map;
aload 1
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast com/amazon/android/licensing/h
astore 2
aload 2
ifnonnull L0
aconst_null
areturn
L0:
aload 2
aload 1
invokeinterface com/amazon/android/licensing/h/a(Lcom/amazon/android/framework/exception/KiwiException;)Lcom/amazon/android/framework/prompt/PromptContent; 1
areturn
.limit locals 3
.limit stack 2
.end method
