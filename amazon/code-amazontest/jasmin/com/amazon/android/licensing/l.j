.bytecode 50.0
.class public final synchronized com/amazon/android/licensing/l
.super com/amazon/android/framework/task/command/AbstractCommandTask

.field private static final 'a' Lcom/amazon/android/framework/util/KiwiLogger;

.field private 'b' Lcom/amazon/android/licensing/LicenseFailurePromptContentMapper;

.field private 'c' Lcom/amazon/android/q/d;
.annotation visible Lcom/amazon/android/framework/resource/Resource;
.end annotation
.end field

.field private 'd' Landroid/app/Application;
.annotation visible Lcom/amazon/android/framework/resource/Resource;
.end annotation
.end field

.field private 'e' Lcom/amazon/android/framework/task/TaskManager;
.annotation visible Lcom/amazon/android/framework/resource/Resource;
.end annotation
.end field

.field private 'f' Lcom/amazon/android/o/a;
.annotation visible Lcom/amazon/android/framework/resource/Resource;
.end annotation
.end field

.field private 'g' Lcom/amazon/android/m/c;
.annotation visible Lcom/amazon/android/framework/resource/Resource;
.end annotation
.end field

.method static <clinit>()V
new com/amazon/android/framework/util/KiwiLogger
dup
ldc "VerifyApplicationEntitlmentTask"
invokespecial com/amazon/android/framework/util/KiwiLogger/<init>(Ljava/lang/String;)V
putstatic com/amazon/android/licensing/l/a Lcom/amazon/android/framework/util/KiwiLogger;
return
.limit locals 0
.limit stack 3
.end method

.method public <init>()V
aload 0
invokespecial com/amazon/android/framework/task/command/AbstractCommandTask/<init>()V
aload 0
new com/amazon/android/licensing/LicenseFailurePromptContentMapper
dup
invokespecial com/amazon/android/licensing/LicenseFailurePromptContentMapper/<init>()V
putfield com/amazon/android/licensing/l/b Lcom/amazon/android/licensing/LicenseFailurePromptContentMapper;
return
.limit locals 1
.limit stack 3
.end method

.method protected final getCommandData()Ljava/util/Map;
aconst_null
areturn
.limit locals 1
.limit stack 1
.end method

.method protected final getCommandName()Ljava/lang/String;
ldc "get_license"
areturn
.limit locals 1
.limit stack 1
.end method

.method protected final getCommandVersion()Ljava/lang/String;
ldc "1.0"
areturn
.limit locals 1
.limit stack 1
.end method

.method protected final isExecutionNeeded()Z
aload 0
getfield com/amazon/android/licensing/l/f Lcom/amazon/android/o/a;
ldc "APPLICATION_LICENSE"
invokevirtual com/amazon/android/o/a/a(Ljava/lang/String;)Ljava/lang/Object;
ifnonnull L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method protected final onException(Lcom/amazon/android/framework/exception/KiwiException;)V
aload 0
getfield com/amazon/android/licensing/l/b Lcom/amazon/android/licensing/LicenseFailurePromptContentMapper;
aload 1
invokevirtual com/amazon/android/licensing/LicenseFailurePromptContentMapper/map(Lcom/amazon/android/framework/exception/KiwiException;)Lcom/amazon/android/framework/prompt/PromptContent;
astore 2
aload 2
ifnonnull L0
getstatic com/amazon/android/framework/util/KiwiLogger/ERROR_ON Z
ifeq L0
getstatic com/amazon/android/licensing/l/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "No mapping specified for exception: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 1
invokevirtual com/amazon/android/framework/util/KiwiLogger/error(Ljava/lang/String;Ljava/lang/Throwable;)V
L0:
aload 0
getfield com/amazon/android/licensing/l/f Lcom/amazon/android/o/a;
ldc "LICENSE_FAILURE_CONTENT"
aload 2
invokevirtual com/amazon/android/o/a/a(Ljava/lang/String;Ljava/lang/Object;)V
return
.limit locals 3
.limit stack 3
.end method

.method protected final onFailure(Lcom/amazon/venezia/command/FailureResult;)V
.throws android/os/RemoteException
.throws com/amazon/android/framework/exception/KiwiException
new com/amazon/android/framework/prompt/PromptContent
dup
aload 1
invokeinterface com/amazon/venezia/command/FailureResult/getDisplayableName()Ljava/lang/String; 0
aload 1
invokeinterface com/amazon/venezia/command/FailureResult/getDisplayableMessage()Ljava/lang/String; 0
aload 1
invokeinterface com/amazon/venezia/command/FailureResult/getButtonLabel()Ljava/lang/String; 0
aload 1
invokeinterface com/amazon/venezia/command/FailureResult/show()Z 0
invokespecial com/amazon/android/framework/prompt/PromptContent/<init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
astore 1
getstatic com/amazon/android/licensing/l/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "onFailure: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
aload 0
getfield com/amazon/android/licensing/l/f Lcom/amazon/android/o/a;
ldc "LICENSE_FAILURE_CONTENT"
aload 1
invokevirtual com/amazon/android/o/a/a(Ljava/lang/String;Ljava/lang/Object;)V
return
.limit locals 2
.limit stack 6
.end method

.method protected final onSuccess(Lcom/amazon/venezia/command/SuccessResult;)V
.throws android/os/RemoteException
.throws com/amazon/android/framework/exception/KiwiException
.catch java/io/IOException from L0 to L1 using L2
new com/amazon/android/licensing/c
dup
aload 1
invokeinterface com/amazon/venezia/command/SuccessResult/getData()Ljava/util/Map; 0
invokespecial com/amazon/android/licensing/c/<init>(Ljava/util/Map;)V
astore 3
aload 0
getfield com/amazon/android/licensing/l/g Lcom/amazon/android/m/c;
invokevirtual com/amazon/android/m/c/a()Ljava/security/PublicKey;
astore 1
new com/amazon/android/licensing/f
dup
new com/amazon/android/m/d
dup
aload 3
getfield com/amazon/android/licensing/c/a Ljava/lang/String;
aload 1
invokespecial com/amazon/android/m/d/<init>(Ljava/lang/String;Ljava/security/PublicKey;)V
invokespecial com/amazon/android/licensing/f/<init>(Lcom/amazon/android/m/d;)V
astore 1
new com/amazon/android/k/a
dup
invokespecial com/amazon/android/k/a/<init>()V
astore 2
aload 2
aload 3
getfield com/amazon/android/licensing/c/b Ljava/lang/String;
aload 1
getfield com/amazon/android/licensing/f/b Ljava/lang/String;
getstatic com/amazon/android/licensing/k/b Lcom/amazon/android/licensing/k;
invokevirtual com/amazon/android/k/a/a(Ljava/lang/Object;Ljava/lang/Object;Lcom/amazon/android/k/c;)Lcom/amazon/android/k/a;
pop
aload 2
aload 3
getfield com/amazon/android/licensing/c/c Ljava/lang/String;
aload 1
getfield com/amazon/android/licensing/f/c Ljava/lang/String;
getstatic com/amazon/android/licensing/k/c Lcom/amazon/android/licensing/k;
invokevirtual com/amazon/android/k/a/a(Ljava/lang/Object;Ljava/lang/Object;Lcom/amazon/android/k/c;)Lcom/amazon/android/k/a;
pop
aload 2
aload 1
getfield com/amazon/android/licensing/f/e Ljava/lang/String;
aload 0
getfield com/amazon/android/licensing/l/d Landroid/app/Application;
invokevirtual android/app/Application/getPackageName()Ljava/lang/String;
getstatic com/amazon/android/licensing/k/d Lcom/amazon/android/licensing/k;
invokevirtual com/amazon/android/k/a/a(Ljava/lang/Object;Ljava/lang/Object;Lcom/amazon/android/k/c;)Lcom/amazon/android/k/a;
pop
aload 1
getfield com/amazon/android/licensing/f/d Ljava/util/Date;
astore 4
new java/util/Date
dup
invokespecial java/util/Date/<init>()V
astore 5
getstatic com/amazon/android/licensing/k/a Lcom/amazon/android/licensing/k;
astore 3
aload 4
aload 5
invokeinterface java/lang/Comparable/compareTo(Ljava/lang/Object;)I 1
ifgt L0
new com/amazon/android/k/b
dup
aload 3
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "'"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "' <= '"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 5
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "'"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial com/amazon/android/k/b/<init>(Lcom/amazon/android/k/c;Ljava/lang/String;)V
astore 4
aload 2
getfield com/amazon/android/k/a/a Ljava/util/Map;
aload 3
aload 4
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L0:
aload 0
getfield com/amazon/android/licensing/l/d Landroid/app/Application;
invokevirtual android/app/Application/getPackageCodePath()Ljava/lang/String;
invokestatic com/amazon/mas/kiwi/util/BC1/getBC1ChecksumBase64(Ljava/lang/String;)Ljava/lang/String;
astore 3
aload 2
aload 1
getfield com/amazon/android/licensing/f/a Ljava/lang/String;
aload 3
getstatic com/amazon/android/licensing/k/e Lcom/amazon/android/licensing/k;
invokevirtual com/amazon/android/k/a/a(Ljava/lang/Object;Ljava/lang/Object;Lcom/amazon/android/k/c;)Lcom/amazon/android/k/a;
pop
L1:
aload 2
invokevirtual com/amazon/android/k/a/a()Z
ifeq L3
new com/amazon/android/s/a
dup
aload 2
invokespecial com/amazon/android/s/a/<init>(Lcom/amazon/android/k/a;)V
athrow
L2:
astore 4
getstatic com/amazon/android/licensing/k/e Lcom/amazon/android/licensing/k;
astore 3
new com/amazon/android/k/b
dup
aload 3
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Exception: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial com/amazon/android/k/b/<init>(Lcom/amazon/android/k/c;Ljava/lang/String;)V
astore 4
aload 2
getfield com/amazon/android/k/a/a Ljava/util/Map;
aload 3
aload 4
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
goto L1
L3:
getstatic com/amazon/android/licensing/l/a Lcom/amazon/android/framework/util/KiwiLogger;
ldc "License Verification succeeded!"
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
new com/amazon/android/licensing/d
dup
aload 0
aload 1
aload 1
getfield com/amazon/android/licensing/f/d Ljava/util/Date;
invokespecial com/amazon/android/licensing/d/<init>(Lcom/amazon/android/licensing/l;Ljava/lang/Object;Ljava/util/Date;)V
astore 1
aload 0
getfield com/amazon/android/licensing/l/f Lcom/amazon/android/o/a;
getfield com/amazon/android/o/a/a Lcom/amazon/android/o/d;
ldc "APPLICATION_LICENSE"
aload 1
invokevirtual com/amazon/android/o/d/a(Ljava/lang/String;Lcom/amazon/android/o/b;)V
aload 0
getfield com/amazon/android/licensing/l/c Lcom/amazon/android/q/d;
new com/amazon/android/p/a
dup
invokespecial com/amazon/android/p/a/<init>()V
invokeinterface com/amazon/android/q/d/a(Lcom/amazon/android/q/b;)V 1
aload 0
getfield com/amazon/android/licensing/l/e Lcom/amazon/android/framework/task/TaskManager;
getstatic com/amazon/android/framework/task/pipeline/TaskPipelineId/BACKGROUND Lcom/amazon/android/framework/task/pipeline/TaskPipelineId;
new com/amazon/android/licensing/n
dup
invokespecial com/amazon/android/licensing/n/<init>()V
invokeinterface com/amazon/android/framework/task/TaskManager/enqueue(Lcom/amazon/android/framework/task/pipeline/TaskPipelineId;Lcom/amazon/android/framework/task/Task;)V 2
return
.limit locals 6
.limit stack 6
.end method
