.bytecode 50.0
.class public final synchronized enum com/amazon/android/licensing/k
.super java/lang/Enum
.implements com/amazon/android/k/c

.field public static final enum 'a' Lcom/amazon/android/licensing/k;

.field public static final enum 'b' Lcom/amazon/android/licensing/k;

.field public static final enum 'c' Lcom/amazon/android/licensing/k;

.field public static final enum 'd' Lcom/amazon/android/licensing/k;

.field public static final enum 'e' Lcom/amazon/android/licensing/k;

.field private static final synthetic 'f' [Lcom/amazon/android/licensing/k;

.method static <clinit>()V
new com/amazon/android/licensing/k
dup
ldc "EXPIRATION"
iconst_0
invokespecial com/amazon/android/licensing/k/<init>(Ljava/lang/String;I)V
putstatic com/amazon/android/licensing/k/a Lcom/amazon/android/licensing/k;
new com/amazon/android/licensing/k
dup
ldc "CUSTOMER_ID"
iconst_1
invokespecial com/amazon/android/licensing/k/<init>(Ljava/lang/String;I)V
putstatic com/amazon/android/licensing/k/b Lcom/amazon/android/licensing/k;
new com/amazon/android/licensing/k
dup
ldc "DEVICE_ID"
iconst_2
invokespecial com/amazon/android/licensing/k/<init>(Ljava/lang/String;I)V
putstatic com/amazon/android/licensing/k/c Lcom/amazon/android/licensing/k;
new com/amazon/android/licensing/k
dup
ldc "PACKAGE_NAME"
iconst_3
invokespecial com/amazon/android/licensing/k/<init>(Ljava/lang/String;I)V
putstatic com/amazon/android/licensing/k/d Lcom/amazon/android/licensing/k;
new com/amazon/android/licensing/k
dup
ldc "CHECKSUM"
iconst_4
invokespecial com/amazon/android/licensing/k/<init>(Ljava/lang/String;I)V
putstatic com/amazon/android/licensing/k/e Lcom/amazon/android/licensing/k;
iconst_5
anewarray com/amazon/android/licensing/k
dup
iconst_0
getstatic com/amazon/android/licensing/k/a Lcom/amazon/android/licensing/k;
aastore
dup
iconst_1
getstatic com/amazon/android/licensing/k/b Lcom/amazon/android/licensing/k;
aastore
dup
iconst_2
getstatic com/amazon/android/licensing/k/c Lcom/amazon/android/licensing/k;
aastore
dup
iconst_3
getstatic com/amazon/android/licensing/k/d Lcom/amazon/android/licensing/k;
aastore
dup
iconst_4
getstatic com/amazon/android/licensing/k/e Lcom/amazon/android/licensing/k;
aastore
putstatic com/amazon/android/licensing/k/f [Lcom/amazon/android/licensing/k;
return
.limit locals 0
.limit stack 4
.end method

.method private <init>(Ljava/lang/String;I)V
aload 0
aload 1
iload 2
invokespecial java/lang/Enum/<init>(Ljava/lang/String;I)V
return
.limit locals 3
.limit stack 3
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/amazon/android/licensing/k;
ldc com/amazon/android/licensing/k
aload 0
invokestatic java/lang/Enum/valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;
checkcast com/amazon/android/licensing/k
areturn
.limit locals 1
.limit stack 2
.end method

.method public static values()[Lcom/amazon/android/licensing/k;
getstatic com/amazon/android/licensing/k/f [Lcom/amazon/android/licensing/k;
invokevirtual [Lcom/amazon/android/licensing/k;/clone()Ljava/lang/Object;
checkcast [Lcom/amazon/android/licensing/k;
areturn
.limit locals 0
.limit stack 1
.end method

.method public final a()Ljava/lang/String;
aload 0
invokevirtual com/amazon/android/licensing/k/name()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method
