.bytecode 50.0
.class public final synchronized com/amazon/android/licensing/n
.super java/lang/Object
.implements com/amazon/android/framework/task/Task

.field private static final 'a' Lcom/amazon/android/framework/util/KiwiLogger;

.method static <clinit>()V
new com/amazon/android/framework/util/KiwiLogger
dup
ldc "DRMSuccessTask"
invokespecial com/amazon/android/framework/util/KiwiLogger/<init>(Ljava/lang/String;)V
putstatic com/amazon/android/licensing/n/a Lcom/amazon/android/framework/util/KiwiLogger;
return
.limit locals 0
.limit stack 3
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Method;
.catch java/lang/NoSuchMethodException from L0 to L1 using L2
aload 0
ldc "Class<?> target"
invokestatic com/amazon/android/d/a/a(Ljava/lang/Object;Ljava/lang/String;)V
aload 1
ldc "String methodName"
invokestatic com/amazon/android/d/a/a(Ljava/lang/Object;Ljava/lang/String;)V
L0:
aload 0
aload 1
iconst_0
anewarray java/lang/Class
invokevirtual java/lang/Class/getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
astore 0
L1:
aload 0
ifnonnull L3
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L4
getstatic com/amazon/android/licensing/n/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "No exception thrown, but method '"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "' was not found, this should not happen. "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L4:
aconst_null
astore 2
L5:
aload 2
areturn
L2:
astore 0
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L6
getstatic com/amazon/android/licensing/n/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Did not find method "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L6:
aconst_null
areturn
L3:
aload 0
iconst_1
invokevirtual java/lang/reflect/Method/setAccessible(Z)V
aload 0
invokestatic com/amazon/android/framework/util/d/c(Ljava/lang/reflect/Method;)Z
ifne L7
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L8
getstatic com/amazon/android/licensing/n/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Callback "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " isn't static, ignoring..."
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L8:
aconst_null
areturn
L7:
aload 0
invokestatic com/amazon/android/framework/util/d/b(Ljava/lang/reflect/Method;)Z
ifne L9
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L10
getstatic com/amazon/android/licensing/n/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Callback "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " returns a value, ignoring..."
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L10:
aconst_null
areturn
L9:
aload 0
astore 2
aload 0
invokestatic com/amazon/android/framework/util/d/a(Ljava/lang/reflect/Method;)Z
ifeq L5
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L11
getstatic com/amazon/android/licensing/n/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Callback "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " takes parameters, ignoring..."
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L11:
aconst_null
areturn
.limit locals 3
.limit stack 3
.end method

.method public final execute()V
.catch java/lang/Exception from L0 to L1 using L2
ldc "com.amazon.drm.AmazonLicenseVerificationCallback"
invokestatic com/amazon/android/framework/util/d/a(Ljava/lang/String;)Ljava/lang/Class;
astore 1
aload 1
ifnonnull L3
L4:
return
L3:
aload 1
ldc "onDRMSuccess"
invokestatic com/amazon/android/licensing/n/a(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/reflect/Method;
astore 1
aload 1
ifnull L4
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L0
getstatic com/amazon/android/licensing/n/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Invoking callback: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/reflect/Method/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L0:
aload 1
aconst_null
iconst_0
anewarray java/lang/Object
invokevirtual java/lang/reflect/Method/invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
pop
L1:
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L4
getstatic com/amazon/android/licensing/n/a Lcom/amazon/android/framework/util/KiwiLogger;
ldc "Callback invoked."
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
return
L2:
astore 1
return
.limit locals 2
.limit stack 3
.end method
