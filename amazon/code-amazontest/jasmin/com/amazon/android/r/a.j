.bytecode 50.0
.class public final synchronized com/amazon/android/r/a
.super com/amazon/android/framework/task/command/AbstractCommandTask

.field private static final 'a' Lcom/amazon/android/framework/util/KiwiLogger;

.field private 'b' Lcom/amazon/android/c/d;
.annotation visible Lcom/amazon/android/framework/resource/Resource;
.end annotation
.end field

.field private 'c' Ljava/util/List;

.method static <clinit>()V
new com/amazon/android/framework/util/KiwiLogger
dup
ldc "SubmitCrashReportsTask"
invokespecial com/amazon/android/framework/util/KiwiLogger/<init>(Ljava/lang/String;)V
putstatic com/amazon/android/r/a/a Lcom/amazon/android/framework/util/KiwiLogger;
return
.limit locals 0
.limit stack 3
.end method

.method public <init>()V
aload 0
invokespecial com/amazon/android/framework/task/command/AbstractCommandTask/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method protected final getCommandData()Ljava/util/Map;
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
astore 1
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 2
aload 0
getfield com/amazon/android/r/a/c Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 3
L0:
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 2
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/amazon/android/c/b
invokevirtual com/amazon/android/c/b/a()Ljava/util/Map;
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
goto L0
L1:
aload 1
ldc "reports"
aload 2
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
aload 1
areturn
.limit locals 4
.limit stack 3
.end method

.method protected final getCommandName()Ljava/lang/String;
ldc "submit_crash_reports"
areturn
.limit locals 1
.limit stack 1
.end method

.method protected final getCommandVersion()Ljava/lang/String;
ldc "1.0"
areturn
.limit locals 1
.limit stack 1
.end method

.method protected final isExecutionNeeded()Z
aload 0
getfield com/amazon/android/r/a/c Ljava/util/List;
invokeinterface java/util/List/isEmpty()Z 0
ifne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method protected final onFailure(Lcom/amazon/venezia/command/FailureResult;)V
.throws android/os/RemoteException
.throws com/amazon/android/framework/exception/KiwiException
return
.limit locals 2
.limit stack 0
.end method

.method protected final onSuccess(Lcom/amazon/venezia/command/SuccessResult;)V
.throws android/os/RemoteException
.throws com/amazon/android/framework/exception/KiwiException
aload 0
getfield com/amazon/android/r/a/b Lcom/amazon/android/c/d;
aload 0
getfield com/amazon/android/r/a/c Ljava/util/List;
invokeinterface com/amazon/android/c/d/a(Ljava/util/List;)V 1
return
.limit locals 2
.limit stack 2
.end method

.method protected final preExecution()V
.throws com/amazon/android/framework/exception/KiwiException
aload 0
aload 0
getfield com/amazon/android/r/a/b Lcom/amazon/android/c/d;
invokeinterface com/amazon/android/c/d/a()Ljava/util/List; 0
putfield com/amazon/android/r/a/c Ljava/util/List;
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L0
getstatic com/amazon/android/r/a/a Lcom/amazon/android/framework/util/KiwiLogger;
ldc "--------------- Crash Reports -------------------"
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
getstatic com/amazon/android/r/a/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Size: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/amazon/android/r/a/c Ljava/util/List;
invokeinterface java/util/List/size()I 0
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
getstatic com/amazon/android/r/a/a Lcom/amazon/android/framework/util/KiwiLogger;
ldc "--------------------------------------------------"
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L0:
return
.limit locals 1
.limit stack 3
.end method
