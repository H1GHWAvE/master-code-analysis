.bytecode 50.0
.class public final synchronized com/amazon/android/e/c
.super java/lang/Object
.implements com/amazon/android/e/b
.implements com/amazon/android/framework/resource/b

.field private static final 'a' Lcom/amazon/android/framework/util/KiwiLogger;

.field private static final 'b' Ljava/util/Random;

.field private 'c' Lcom/amazon/android/framework/task/TaskManager;
.annotation visible Lcom/amazon/android/framework/resource/Resource;
.end annotation
.end field

.field private 'd' Lcom/amazon/android/framework/context/ContextManager;
.annotation visible Lcom/amazon/android/framework/resource/Resource;
.end annotation
.end field

.field private 'e' Lcom/amazon/android/n/g;
.annotation visible Lcom/amazon/android/framework/resource/Resource;
.end annotation
.end field

.field private 'f' Ljava/util/concurrent/atomic/AtomicReference;

.field private 'g' Ljava/util/concurrent/BlockingQueue;

.method static <clinit>()V
new com/amazon/android/framework/util/KiwiLogger
dup
ldc "ActivityResultManagerImpl"
invokespecial com/amazon/android/framework/util/KiwiLogger/<init>(Ljava/lang/String;)V
putstatic com/amazon/android/e/c/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/util/Random
dup
invokespecial java/util/Random/<init>()V
putstatic com/amazon/android/e/c/b Ljava/util/Random;
return
.limit locals 0
.limit stack 3
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/util/concurrent/atomic/AtomicReference
dup
invokespecial java/util/concurrent/atomic/AtomicReference/<init>()V
putfield com/amazon/android/e/c/f Ljava/util/concurrent/atomic/AtomicReference;
aload 0
new java/util/concurrent/LinkedBlockingQueue
dup
invokespecial java/util/concurrent/LinkedBlockingQueue/<init>()V
putfield com/amazon/android/e/c/g Ljava/util/concurrent/BlockingQueue;
return
.limit locals 1
.limit stack 3
.end method

.method static synthetic a()Lcom/amazon/android/framework/util/KiwiLogger;
getstatic com/amazon/android/e/c/a Lcom/amazon/android/framework/util/KiwiLogger;
areturn
.limit locals 0
.limit stack 1
.end method

.method static synthetic a(Lcom/amazon/android/e/c;)Ljava/util/concurrent/atomic/AtomicReference;
aload 0
getfield com/amazon/android/e/c/f Ljava/util/concurrent/atomic/AtomicReference;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic b(Lcom/amazon/android/e/c;)Lcom/amazon/android/framework/context/ContextManager;
aload 0
getfield com/amazon/android/e/c/d Lcom/amazon/android/framework/context/ContextManager;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a(Landroid/content/Intent;)Lcom/amazon/android/e/f;
.catch java/lang/InterruptedException from L0 to L1 using L2
.catch all from L0 to L1 using L3
.catch all from L4 to L5 using L3
new com/amazon/android/e/a
dup
aload 1
getstatic com/amazon/android/e/c/b Ljava/util/Random;
ldc_w 65535
invokevirtual java/util/Random/nextInt(I)I
iconst_1
iadd
invokespecial com/amazon/android/e/a/<init>(Landroid/content/Intent;I)V
astore 2
aload 0
getfield com/amazon/android/e/c/f Ljava/util/concurrent/atomic/AtomicReference;
aconst_null
aload 2
invokevirtual java/util/concurrent/atomic/AtomicReference/compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z
ifne L6
getstatic com/amazon/android/e/c/a Lcom/amazon/android/framework/util/KiwiLogger;
ldc "StartActivityForResult called while ActivityResultManager is already awaiting a result"
invokevirtual com/amazon/android/framework/util/KiwiLogger/error(Ljava/lang/String;)V
aconst_null
areturn
L6:
getstatic com/amazon/android/e/c/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Starting activity for result: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc ", "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual android/content/Intent/getFlags()I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ", requestId: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
getfield com/amazon/android/e/a/a I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
new com/amazon/android/e/d
dup
aload 0
aload 2
invokespecial com/amazon/android/e/d/<init>(Lcom/amazon/android/e/c;Lcom/amazon/android/e/a;)V
astore 1
aload 0
getfield com/amazon/android/e/c/c Lcom/amazon/android/framework/task/TaskManager;
getstatic com/amazon/android/framework/task/pipeline/TaskPipelineId/FOREGROUND Lcom/amazon/android/framework/task/pipeline/TaskPipelineId;
aload 1
invokeinterface com/amazon/android/framework/task/TaskManager/enqueueAtFront(Lcom/amazon/android/framework/task/pipeline/TaskPipelineId;Lcom/amazon/android/framework/task/Task;)V 2
L0:
getstatic com/amazon/android/e/c/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Blocking for request: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
getfield com/amazon/android/e/a/a I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
aload 0
getfield com/amazon/android/e/c/g Ljava/util/concurrent/BlockingQueue;
invokeinterface java/util/concurrent/BlockingQueue/take()Ljava/lang/Object; 0
checkcast com/amazon/android/e/f
astore 1
L1:
getstatic com/amazon/android/e/c/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Received Response: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
getfield com/amazon/android/e/a/a I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
aload 0
getfield com/amazon/android/e/c/f Ljava/util/concurrent/atomic/AtomicReference;
aconst_null
invokevirtual java/util/concurrent/atomic/AtomicReference/set(Ljava/lang/Object;)V
aload 1
areturn
L2:
astore 1
L4:
getstatic com/amazon/android/e/c/a Lcom/amazon/android/framework/util/KiwiLogger;
ldc "Interrupted while awaiting for request, returning null"
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L5:
getstatic com/amazon/android/e/c/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Received Response: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
getfield com/amazon/android/e/a/a I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
aload 0
getfield com/amazon/android/e/c/f Ljava/util/concurrent/atomic/AtomicReference;
aconst_null
invokevirtual java/util/concurrent/atomic/AtomicReference/set(Ljava/lang/Object;)V
aconst_null
areturn
L3:
astore 1
getstatic com/amazon/android/e/c/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Received Response: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
getfield com/amazon/android/e/a/a I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
aload 0
getfield com/amazon/android/e/c/f Ljava/util/concurrent/atomic/AtomicReference;
aconst_null
invokevirtual java/util/concurrent/atomic/AtomicReference/set(Ljava/lang/Object;)V
aload 1
athrow
.limit locals 3
.limit stack 5
.end method

.method public final a(Lcom/amazon/android/e/f;)Z
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L0
getstatic com/amazon/android/e/c/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Recieved ActivityResult: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L0:
aload 0
getfield com/amazon/android/e/c/f Ljava/util/concurrent/atomic/AtomicReference;
invokevirtual java/util/concurrent/atomic/AtomicReference/get()Ljava/lang/Object;
checkcast com/amazon/android/e/a
astore 2
aload 2
ifnonnull L1
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L2
getstatic com/amazon/android/e/c/a Lcom/amazon/android/framework/util/KiwiLogger;
ldc "We don't have a current open request, returning"
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L2:
iconst_0
ireturn
L1:
aload 2
getfield com/amazon/android/e/a/a I
aload 1
getfield com/amazon/android/e/f/a I
if_icmpeq L3
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L4
getstatic com/amazon/android/e/c/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "We don't have a request with code: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
getfield com/amazon/android/e/f/a I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ", returning"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L4:
iconst_0
ireturn
L3:
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L5
getstatic com/amazon/android/e/c/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Signaling thread waiting for request: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
getfield com/amazon/android/e/f/a I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L5:
aload 0
getfield com/amazon/android/e/c/g Ljava/util/concurrent/BlockingQueue;
aload 1
invokeinterface java/util/concurrent/BlockingQueue/add(Ljava/lang/Object;)Z 1
pop
iconst_1
ireturn
.limit locals 3
.limit stack 3
.end method

.method public final onResourcesPopulated()V
aload 0
getfield com/amazon/android/e/c/e Lcom/amazon/android/n/g;
new com/amazon/android/e/e
dup
aload 0
invokespecial com/amazon/android/e/e/<init>(Lcom/amazon/android/e/c;)V
invokeinterface com/amazon/android/n/g/a(Lcom/amazon/android/n/c;)V 1
return
.limit locals 1
.limit stack 4
.end method
