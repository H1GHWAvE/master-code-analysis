.bytecode 50.0
.class public synchronized com/amazon/android/s/a
.super com/amazon/android/framework/exception/KiwiException

.field private static final 'serialVersionUID' J = 1L


.field public final 'a' Lcom/amazon/android/k/a;

.method public <init>(Lcom/amazon/android/k/a;)V
aload 0
ldc "LICENSE_VERIFICATION_FAILURE"
ldc "VERIFICATION_ERRORS"
aload 1
invokestatic com/amazon/android/s/a/a(Lcom/amazon/android/k/a;)Ljava/lang/String;
invokespecial com/amazon/android/framework/exception/KiwiException/<init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
aload 1
invokevirtual com/amazon/android/k/a/a()Z
ldc "Created a verification exception with a Verifier that has no errors"
invokestatic com/amazon/android/d/a/a(ZLjava/lang/String;)V
aload 0
aload 1
putfield com/amazon/android/s/a/a Lcom/amazon/android/k/a;
return
.limit locals 2
.limit stack 4
.end method

.method private static a(Lcom/amazon/android/k/a;)Ljava/lang/String;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
astore 1
aload 0
invokevirtual com/amazon/android/k/a/iterator()Ljava/util/Iterator;
astore 0
L0:
aload 0
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 0
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/amazon/android/k/b
astore 2
aload 1
invokevirtual java/lang/StringBuilder/length()I
ifeq L2
aload 1
ldc ","
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
L2:
aload 1
aload 2
getfield com/amazon/android/k/b/a Lcom/amazon/android/k/c;
invokeinterface com/amazon/android/k/c/a()Ljava/lang/String; 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
goto L0
L1:
aload 1
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 3
.limit stack 2
.end method

.method public toString()Ljava/lang/String;
aload 0
getfield com/amazon/android/s/a/a Lcom/amazon/android/k/a;
invokevirtual com/amazon/android/k/a/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method
