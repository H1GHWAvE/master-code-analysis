.bytecode 50.0
.class public final synchronized com/amazon/android/m/c
.super java/lang/Object

.field private static final 'a' Lcom/amazon/android/framework/util/KiwiLogger;

.field private 'b' Landroid/app/Application;
.annotation visible Lcom/amazon/android/framework/resource/Resource;
.end annotation
.end field

.field private 'c' Lcom/amazon/android/o/a;
.annotation visible Lcom/amazon/android/framework/resource/Resource;
.end annotation
.end field

.method static <clinit>()V
new com/amazon/android/framework/util/KiwiLogger
dup
ldc "DataAuthenticationKeyLoader"
invokespecial com/amazon/android/framework/util/KiwiLogger/<init>(Ljava/lang/String;)V
putstatic com/amazon/android/m/c/a Lcom/amazon/android/framework/util/KiwiLogger;
return
.limit locals 0
.limit stack 3
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Ljava/util/jar/JarFile;Ljava/util/jar/JarEntry;)Ljava/security/cert/CertPath;
.throws com/amazon/android/h/a
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L1 to L3 using L2
.catch java/lang/Exception from L3 to L4 using L2
L0:
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L1
getstatic com/amazon/android/m/c/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Extracting cert from entry: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/util/jar/JarEntry/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L1:
ldc "X.509"
invokestatic java/security/cert/CertificateFactory/getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;
astore 2
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L3
getstatic com/amazon/android/m/c/a Lcom/amazon/android/framework/util/KiwiLogger;
ldc "Generating certificates from entry input stream"
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L3:
aload 2
new java/util/ArrayList
dup
aload 2
aload 0
aload 1
invokevirtual java/util/jar/JarFile/getInputStream(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;
invokevirtual java/security/cert/CertificateFactory/generateCertificates(Ljava/io/InputStream;)Ljava/util/Collection;
invokespecial java/util/ArrayList/<init>(Ljava/util/Collection;)V
invokevirtual java/security/cert/CertificateFactory/generateCertPath(Ljava/util/List;)Ljava/security/cert/CertPath;
astore 0
L4:
aload 0
areturn
L2:
astore 0
aload 0
invokestatic com/amazon/android/h/a/a(Ljava/lang/Throwable;)Lcom/amazon/android/h/a;
athrow
.limit locals 3
.limit stack 6
.end method

.method private static a(Ljava/util/jar/JarFile;)Ljava/util/jar/JarEntry;
.throws com/amazon/android/h/a
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L0
getstatic com/amazon/android/m/c/a Lcom/amazon/android/framework/util/KiwiLogger;
ldc "Searching for cert in apk"
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L0:
aload 0
invokevirtual java/util/jar/JarFile/entries()Ljava/util/Enumeration;
astore 0
L1:
aload 0
invokeinterface java/util/Enumeration/hasMoreElements()Z 0
ifeq L2
aload 0
invokeinterface java/util/Enumeration/nextElement()Ljava/lang/Object; 0
checkcast java/util/jar/JarEntry
astore 1
aload 1
invokevirtual java/util/jar/JarEntry/isDirectory()Z
ifne L1
aload 1
invokevirtual java/util/jar/JarEntry/getName()Ljava/lang/String;
ldc "kiwi"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L1
aload 1
areturn
L2:
new com/amazon/android/h/a
dup
ldc "CERT_NOT_FOUND"
aconst_null
invokespecial com/amazon/android/h/a/<init>(Ljava/lang/String;Ljava/lang/Throwable;)V
athrow
.limit locals 2
.limit stack 4
.end method

.method private b()Ljava/util/jar/JarFile;
.throws com/amazon/android/h/a
.catch java/io/IOException from L0 to L1 using L2
aload 0
getfield com/amazon/android/m/c/b Landroid/app/Application;
invokevirtual android/app/Application/getPackageCodePath()Ljava/lang/String;
astore 1
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L0
getstatic com/amazon/android/m/c/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Opening apk: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L0:
new java/util/jar/JarFile
dup
aload 1
iconst_0
invokespecial java/util/jar/JarFile/<init>(Ljava/lang/String;Z)V
astore 1
L1:
aload 1
areturn
L2:
astore 1
aload 1
invokestatic com/amazon/android/h/a/a(Ljava/lang/Throwable;)Lcom/amazon/android/h/a;
athrow
.limit locals 2
.limit stack 4
.end method

.method private static c()Lcom/amazon/android/m/b;
.throws com/amazon/android/h/a
.catch java/security/GeneralSecurityException from L0 to L1 using L2
L0:
new com/amazon/android/m/b
dup
invokespecial com/amazon/android/m/b/<init>()V
astore 0
L1:
aload 0
areturn
L2:
astore 0
new com/amazon/android/h/a
dup
ldc "FAILED_TO_ESTABLISH_TRUST"
aload 0
invokespecial com/amazon/android/h/a/<init>(Ljava/lang/String;Ljava/lang/Throwable;)V
athrow
.limit locals 1
.limit stack 4
.end method

.method public final a()Ljava/security/PublicKey;
.throws com/amazon/android/h/a
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L0
getstatic com/amazon/android/m/c/a Lcom/amazon/android/framework/util/KiwiLogger;
ldc "Loading data authentication key..."
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L0:
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L1
getstatic com/amazon/android/m/c/a Lcom/amazon/android/framework/util/KiwiLogger;
ldc "Checking KiwiDataStore for key..."
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L1:
aload 0
getfield com/amazon/android/m/c/c Lcom/amazon/android/o/a;
ldc "DATA_AUTHENTICATION_KEY"
invokevirtual com/amazon/android/o/a/a(Ljava/lang/String;)Ljava/lang/Object;
checkcast java/security/PublicKey
astore 3
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L2
getstatic com/amazon/android/m/c/a Lcom/amazon/android/framework/util/KiwiLogger;
astore 4
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Key was cached: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
astore 5
aload 3
ifnull L3
iconst_1
istore 2
L4:
aload 4
aload 5
iload 2
invokevirtual java/lang/StringBuilder/append(Z)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L2:
aload 3
ifnull L5
aload 3
areturn
L3:
iconst_0
istore 2
goto L4
L5:
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L6
getstatic com/amazon/android/m/c/a Lcom/amazon/android/framework/util/KiwiLogger;
ldc "Loading authentication key from apk..."
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L6:
aload 0
invokespecial com/amazon/android/m/c/b()Ljava/util/jar/JarFile;
astore 3
aload 3
aload 3
invokestatic com/amazon/android/m/c/a(Ljava/util/jar/JarFile;)Ljava/util/jar/JarEntry;
invokestatic com/amazon/android/m/c/a(Ljava/util/jar/JarFile;Ljava/util/jar/JarEntry;)Ljava/security/cert/CertPath;
astore 3
aload 3
ifnull L7
aload 3
invokevirtual java/security/cert/CertPath/getCertificates()Ljava/util/List;
invokeinterface java/util/List/size()I 0
ifle L7
aload 3
invokevirtual java/security/cert/CertPath/getCertificates()Ljava/util/List;
iconst_0
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast java/security/cert/Certificate
astore 4
aload 4
instanceof java/security/cert/X509Certificate
ifeq L7
aload 4
checkcast java/security/cert/X509Certificate
invokevirtual java/security/cert/X509Certificate/getSubjectX500Principal()Ljavax/security/auth/x500/X500Principal;
invokevirtual javax/security/auth/x500/X500Principal/getName()Ljava/lang/String;
astore 4
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L8
getstatic com/amazon/android/m/c/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Kiwi Cert Details: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L8:
aload 4
ldc "O=Amazon.com\\, Inc."
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L9
aload 4
ldc "C=US"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L9
aload 4
ldc "ST=Washington"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L9
aload 4
ldc "L=Seattle"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L9
iconst_1
istore 1
L10:
iload 1
ifne L11
new com/amazon/android/h/a
dup
ldc "CERT_INVALID"
aconst_null
invokespecial com/amazon/android/h/a/<init>(Ljava/lang/String;Ljava/lang/Throwable;)V
athrow
L9:
iconst_0
istore 1
goto L10
L7:
iconst_0
istore 1
goto L10
L11:
invokestatic com/amazon/android/m/c/c()Lcom/amazon/android/m/b;
aload 3
invokevirtual com/amazon/android/m/b/a(Ljava/security/cert/CertPath;)Z
ifne L12
new com/amazon/android/h/a
dup
ldc "VERIFICATION_FAILED"
aconst_null
invokespecial com/amazon/android/h/a/<init>(Ljava/lang/String;Ljava/lang/Throwable;)V
athrow
L12:
aload 3
invokevirtual java/security/cert/CertPath/getCertificates()Ljava/util/List;
iconst_0
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast java/security/cert/X509Certificate
invokevirtual java/security/cert/X509Certificate/getPublicKey()Ljava/security/PublicKey;
astore 3
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L13
getstatic com/amazon/android/m/c/a Lcom/amazon/android/framework/util/KiwiLogger;
ldc "Placing auth key into storage"
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L13:
aload 0
getfield com/amazon/android/m/c/c Lcom/amazon/android/o/a;
ldc "DATA_AUTHENTICATION_KEY"
aload 3
invokevirtual com/amazon/android/o/a/a(Ljava/lang/String;Ljava/lang/Object;)V
aload 3
areturn
.limit locals 6
.limit stack 4
.end method
