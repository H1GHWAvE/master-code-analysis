.bytecode 50.0
.class public final synchronized com/amazon/android/m/b
.super java/lang/Object

.field private static final 'a' Lcom/amazon/android/framework/util/KiwiLogger;

.field private static final 'e' Ljava/util/Set;

.field private final 'b' Ljava/security/cert/PKIXParameters;

.field private final 'c' Ljava/security/cert/CertPathValidator;

.field private final 'd' Ljava/util/Set;

.method static <clinit>()V
new com/amazon/android/framework/util/KiwiLogger
dup
ldc "CertVerifier"
invokespecial com/amazon/android/framework/util/KiwiLogger/<init>(Ljava/lang/String;)V
putstatic com/amazon/android/m/b/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/util/HashSet
dup
invokespecial java/util/HashSet/<init>()V
astore 0
aload 0
putstatic com/amazon/android/m/b/e Ljava/util/Set;
aload 0
ldc "verisign"
invokeinterface java/util/Set/add(Ljava/lang/Object;)Z 1
pop
getstatic com/amazon/android/m/b/e Ljava/util/Set;
ldc "thawte"
invokeinterface java/util/Set/add(Ljava/lang/Object;)Z 1
pop
return
.limit locals 1
.limit stack 3
.end method

.method public <init>()V
.throws java/security/GeneralSecurityException
aload 0
invokespecial java/lang/Object/<init>()V
invokestatic javax/net/ssl/TrustManagerFactory/getDefaultAlgorithm()Ljava/lang/String;
invokestatic javax/net/ssl/TrustManagerFactory/getInstance(Ljava/lang/String;)Ljavax/net/ssl/TrustManagerFactory;
astore 7
aload 7
aconst_null
invokevirtual javax/net/ssl/TrustManagerFactory/init(Ljava/security/KeyStore;)V
aload 0
new java/util/HashSet
dup
invokespecial java/util/HashSet/<init>()V
putfield com/amazon/android/m/b/d Ljava/util/Set;
aload 7
invokevirtual javax/net/ssl/TrustManagerFactory/getTrustManagers()[Ljavax/net/ssl/TrustManager;
astore 7
aload 7
arraylength
istore 5
iconst_0
istore 1
L0:
iload 1
iload 5
if_icmpge L1
aload 7
iload 1
aaload
astore 8
aload 8
instanceof javax/net/ssl/X509TrustManager
ifeq L2
aload 8
checkcast javax/net/ssl/X509TrustManager
invokeinterface javax/net/ssl/X509TrustManager/getAcceptedIssuers()[Ljava/security/cert/X509Certificate; 0
astore 8
aload 8
ifnull L2
aload 8
arraylength
istore 6
iconst_0
istore 2
iconst_0
istore 3
L3:
iload 2
iload 6
if_icmpge L4
aload 8
iload 2
aaload
astore 9
iload 3
istore 4
aload 9
invokestatic com/amazon/android/m/b/a(Ljava/security/cert/X509Certificate;)Z
ifeq L5
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L6
getstatic com/amazon/android/m/b/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Trusted Cert: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 9
invokevirtual java/security/cert/X509Certificate/getSubjectX500Principal()Ljavax/security/auth/x500/X500Principal;
invokevirtual javax/security/auth/x500/X500Principal/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L6:
new java/security/cert/TrustAnchor
dup
aload 9
aconst_null
invokespecial java/security/cert/TrustAnchor/<init>(Ljava/security/cert/X509Certificate;[B)V
astore 9
aload 0
getfield com/amazon/android/m/b/d Ljava/util/Set;
aload 9
invokeinterface java/util/Set/add(Ljava/lang/Object;)Z 1
pop
iload 3
iconst_1
iadd
istore 4
L5:
iload 2
iconst_1
iadd
istore 2
iload 4
istore 3
goto L3
L4:
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L2
getstatic com/amazon/android/m/b/a Lcom/amazon/android/framework/util/KiwiLogger;
ldc "loaded %d certs\n"
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 3
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L2:
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
aload 0
getfield com/amazon/android/m/b/d Ljava/util/Set;
invokeinterface java/util/Set/isEmpty()Z 0
ifeq L7
getstatic com/amazon/android/m/b/a Lcom/amazon/android/framework/util/KiwiLogger;
ldc "TrustManager did not return valid accepted issuers, likely 3P custom TrustManager implementation issue."
invokevirtual com/amazon/android/framework/util/KiwiLogger/error(Ljava/lang/String;)V
L7:
aload 0
new java/security/cert/PKIXParameters
dup
aload 0
getfield com/amazon/android/m/b/d Ljava/util/Set;
invokespecial java/security/cert/PKIXParameters/<init>(Ljava/util/Set;)V
putfield com/amazon/android/m/b/b Ljava/security/cert/PKIXParameters;
aload 0
getfield com/amazon/android/m/b/b Ljava/security/cert/PKIXParameters;
iconst_0
invokevirtual java/security/cert/PKIXParameters/setRevocationEnabled(Z)V
aload 0
ldc "PKIX"
invokestatic java/security/cert/CertPathValidator/getInstance(Ljava/lang/String;)Ljava/security/cert/CertPathValidator;
putfield com/amazon/android/m/b/c Ljava/security/cert/CertPathValidator;
return
.limit locals 10
.limit stack 6
.end method

.method private static a(Ljava/security/cert/X509Certificate;)Z
aload 0
invokevirtual java/security/cert/X509Certificate/getSubjectDN()Ljava/security/Principal;
invokeinterface java/security/Principal/getName()Ljava/lang/String; 0
invokevirtual java/lang/String/toLowerCase()Ljava/lang/String;
astore 0
getstatic com/amazon/android/m/b/e Ljava/util/Set;
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 1
L0:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 0
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/String
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L0
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final a(Ljava/security/cert/CertPath;)Z
.catch java/security/cert/CertPathValidatorException from L0 to L1 using L2
.catch java/lang/Exception from L0 to L1 using L3
iconst_0
istore 2
L0:
aload 0
getfield com/amazon/android/m/b/c Ljava/security/cert/CertPathValidator;
aload 1
aload 0
getfield com/amazon/android/m/b/b Ljava/security/cert/PKIXParameters;
invokevirtual java/security/cert/CertPathValidator/validate(Ljava/security/cert/CertPath;Ljava/security/cert/CertPathParameters;)Ljava/security/cert/CertPathValidatorResult;
pop
L1:
iconst_1
istore 2
L4:
iload 2
ireturn
L2:
astore 1
aload 1
invokevirtual java/security/cert/CertPathValidatorException/getCause()Ljava/lang/Throwable;
instanceof java/security/cert/CertificateExpiredException
ifeq L4
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L5
getstatic com/amazon/android/m/b/a Lcom/amazon/android/framework/util/KiwiLogger;
ldc "CertVerifier doesn't care about an out of date certificate."
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L5:
iconst_1
ireturn
L3:
astore 1
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L4
getstatic com/amazon/android/m/b/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Failed to verify cert path: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 1
invokevirtual com/amazon/android/framework/util/KiwiLogger/error(Ljava/lang/String;Ljava/lang/Throwable;)V
iconst_0
ireturn
.limit locals 3
.limit stack 3
.end method
