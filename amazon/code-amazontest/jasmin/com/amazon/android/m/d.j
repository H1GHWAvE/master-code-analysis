.bytecode 50.0
.class public final synchronized com/amazon/android/m/d
.super java/lang/Object

.field private static final 'a' Lcom/amazon/android/framework/util/KiwiLogger;

.field private final 'b' Ljava/util/Map;

.method static <clinit>()V
new com/amazon/android/framework/util/KiwiLogger
dup
ldc "SignedToken"
invokespecial com/amazon/android/framework/util/KiwiLogger/<init>(Ljava/lang/String;)V
putstatic com/amazon/android/m/d/a Lcom/amazon/android/framework/util/KiwiLogger;
return
.limit locals 0
.limit stack 3
.end method

.method public <init>(Ljava/lang/String;Ljava/security/PublicKey;)V
.throws com/amazon/android/h/b
.throws com/amazon/android/h/c
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
putfield com/amazon/android/m/d/b Ljava/util/Map;
aload 1
invokestatic com/amazon/android/m/d/b(Ljava/lang/String;)Ljava/lang/String;
astore 4
aload 4
ldc "|"
invokevirtual java/lang/String/lastIndexOf(Ljava/lang/String;)I
istore 3
iload 3
iconst_m1
if_icmpne L0
invokestatic com/amazon/android/h/b/a()Lcom/amazon/android/h/b;
athrow
L0:
aload 4
iconst_0
iload 3
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
astore 1
aload 4
iload 3
iconst_1
iadd
invokevirtual java/lang/String/substring(I)Ljava/lang/String;
astore 4
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L1
getstatic com/amazon/android/m/d/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Token data: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
getstatic com/amazon/android/m/d/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Signature: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L1:
aload 1
aload 4
aload 2
invokestatic com/amazon/android/m/a/a(Ljava/lang/String;Ljava/lang/String;Ljava/security/PublicKey;)Z
ifne L2
new com/amazon/android/h/c
dup
invokespecial com/amazon/android/h/c/<init>()V
athrow
L2:
aload 0
aload 1
invokespecial com/amazon/android/m/d/c(Ljava/lang/String;)V
return
.limit locals 5
.limit stack 3
.end method

.method private static b(Ljava/lang/String;)Ljava/lang/String;
.throws com/amazon/android/h/b
.catch java/io/IOException from L0 to L1 using L2
L0:
new java/lang/String
dup
aload 0
invokevirtual java/lang/String/getBytes()[B
invokestatic com/amazon/mas/kiwi/util/Base64/decode([B)[B
invokespecial java/lang/String/<init>([B)V
astore 0
L1:
aload 0
areturn
L2:
astore 0
new com/amazon/android/h/b
dup
ldc "DECODE"
aload 0
invokevirtual java/lang/Throwable/getMessage()Ljava/lang/String;
invokespecial com/amazon/android/h/b/<init>(Ljava/lang/String;Ljava/lang/String;)V
athrow
.limit locals 1
.limit stack 4
.end method

.method private c(Ljava/lang/String;)V
.throws com/amazon/android/h/b
new java/util/StringTokenizer
dup
aload 1
ldc ","
invokespecial java/util/StringTokenizer/<init>(Ljava/lang/String;Ljava/lang/String;)V
astore 1
L0:
aload 1
invokevirtual java/util/StringTokenizer/hasMoreElements()Z
ifeq L1
aload 1
invokevirtual java/util/StringTokenizer/nextToken()Ljava/lang/String;
astore 4
getstatic com/amazon/android/m/d/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Field: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
aload 4
ldc "="
invokevirtual java/lang/String/indexOf(Ljava/lang/String;)I
istore 2
iload 2
iconst_m1
if_icmpne L2
invokestatic com/amazon/android/h/b/a()Lcom/amazon/android/h/b;
athrow
L2:
aload 4
iconst_0
iload 2
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
astore 3
aload 4
iload 2
iconst_1
iadd
invokevirtual java/lang/String/substring(I)Ljava/lang/String;
astore 4
getstatic com/amazon/android/m/d/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "FieldName: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
getstatic com/amazon/android/m/d/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "FieldValue: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
aload 0
getfield com/amazon/android/m/d/b Ljava/util/Map;
aload 3
aload 4
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
goto L0
L1:
return
.limit locals 5
.limit stack 4
.end method

.method public final a(Ljava/lang/String;)Ljava/lang/String;
aload 1
ldc "key"
invokestatic com/amazon/android/d/a/a(Ljava/lang/Object;Ljava/lang/String;)V
aload 0
getfield com/amazon/android/m/d/b Ljava/util/Map;
aload 1
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/lang/String
areturn
.limit locals 2
.limit stack 2
.end method

.method public final toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Signed Token: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/amazon/android/m/d/b Ljava/util/Map;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method
