.bytecode 50.0
.class public final synchronized com/amazon/android/m/a
.super java/lang/Object

.field private static final 'a' Lcom/amazon/android/framework/util/KiwiLogger;

.method static <clinit>()V
new com/amazon/android/framework/util/KiwiLogger
dup
ldc "SignatureVerifier"
invokespecial com/amazon/android/framework/util/KiwiLogger/<init>(Ljava/lang/String;)V
putstatic com/amazon/android/m/a/a Lcom/amazon/android/framework/util/KiwiLogger;
return
.limit locals 0
.limit stack 3
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/security/PublicKey;)Z
.catch java/security/GeneralSecurityException from L0 to L1 using L2
.catch java/io/IOException from L0 to L1 using L3
getstatic com/amazon/android/framework/util/KiwiLogger/TRACE_ON Z
ifeq L0
getstatic com/amazon/android/m/a/a Lcom/amazon/android/framework/util/KiwiLogger;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
ldc "Verifying signature of data: '"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "', signature: '"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "', with key: '"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/Object/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/amazon/android/framework/util/KiwiLogger/trace(Ljava/lang/String;)V
L0:
aload 1
invokevirtual java/lang/String/getBytes()[B
invokestatic com/amazon/mas/kiwi/util/Base64/decode([B)[B
astore 1
ldc "SHA1withRSA"
invokestatic java/security/Signature/getInstance(Ljava/lang/String;)Ljava/security/Signature;
astore 4
aload 4
aload 2
invokevirtual java/security/Signature/initVerify(Ljava/security/PublicKey;)V
aload 4
aload 0
invokevirtual java/lang/String/getBytes()[B
invokevirtual java/security/Signature/update([B)V
aload 4
aload 1
invokevirtual java/security/Signature/verify([B)Z
istore 3
L1:
iload 3
ireturn
L2:
astore 0
iconst_0
ireturn
L3:
astore 0
iconst_0
ireturn
.limit locals 5
.limit stack 3
.end method
