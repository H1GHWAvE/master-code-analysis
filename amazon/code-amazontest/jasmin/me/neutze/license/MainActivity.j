.bytecode 50.0
.class public synchronized me/neutze/license/MainActivity
.super android/app/Activity
.inner class inner me/neutze/license/MainActivity$1
.inner class inner me/neutze/license/MainActivity$2

.field private 'mCheckLicenseButton' Landroid/widget/Button;

.field private 'mHandler' Landroid/os/Handler;

.field private 'mStatusText' Landroid/widget/TextView;

.method public <init>()V
aload 0
invokespecial android/app/Activity/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method static synthetic access$002(Lme/neutze/license/MainActivity;Landroid/os/Handler;)Landroid/os/Handler;
aload 0
aload 1
putfield me/neutze/license/MainActivity/mHandler Landroid/os/Handler;
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method static synthetic access$100(Lme/neutze/license/MainActivity;)V
aload 0
invokespecial me/neutze/license/MainActivity/doCheck()V
return
.limit locals 1
.limit stack 1
.end method

.method static synthetic access$200(Lme/neutze/license/MainActivity;)Landroid/widget/TextView;
aload 0
getfield me/neutze/license/MainActivity/mStatusText Landroid/widget/TextView;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic access$300(Lme/neutze/license/MainActivity;)Landroid/widget/Button;
aload 0
getfield me/neutze/license/MainActivity/mCheckLicenseButton Landroid/widget/Button;
areturn
.limit locals 1
.limit stack 1
.end method

.method private displayResult(Ljava/lang/String;)V
aload 0
getfield me/neutze/license/MainActivity/mHandler Landroid/os/Handler;
new me/neutze/license/MainActivity$2
dup
aload 0
aload 1
invokespecial me/neutze/license/MainActivity$2/<init>(Lme/neutze/license/MainActivity;Ljava/lang/String;)V
ldc2_w 2000L
invokevirtual android/os/Handler/postDelayed(Ljava/lang/Runnable;J)Z
pop
return
.limit locals 2
.limit stack 5
.end method

.method private doCheck()V
aload 0
getfield me/neutze/license/MainActivity/mCheckLicenseButton Landroid/widget/Button;
iconst_0
invokevirtual android/widget/Button/setEnabled(Z)V
aload 0
iconst_1
invokevirtual me/neutze/license/MainActivity/setProgressBarIndeterminateVisibility(Z)V
aload 0
getfield me/neutze/license/MainActivity/mStatusText Landroid/widget/TextView;
ldc_w 2131099651
invokevirtual android/widget/TextView/setText(I)V
aload 0
ldc "amazon does not check here"
invokespecial me/neutze/license/MainActivity/displayResult(Ljava/lang/String;)V
return
.limit locals 1
.limit stack 2
.end method

.method private onCreateMainActivity(Landroid/os/Bundle;)V
aload 0
aload 1
invokespecial android/app/Activity/onCreate(Landroid/os/Bundle;)V
aload 0
ldc_w 2130903040
invokevirtual me/neutze/license/MainActivity/setContentView(I)V
aload 0
aload 0
ldc_w 2131230720
invokevirtual me/neutze/license/MainActivity/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
putfield me/neutze/license/MainActivity/mStatusText Landroid/widget/TextView;
aload 0
aload 0
ldc_w 2131230721
invokevirtual me/neutze/license/MainActivity/findViewById(I)Landroid/view/View;
checkcast android/widget/Button
putfield me/neutze/license/MainActivity/mCheckLicenseButton Landroid/widget/Button;
aload 0
getfield me/neutze/license/MainActivity/mCheckLicenseButton Landroid/widget/Button;
new me/neutze/license/MainActivity$1
dup
aload 0
invokespecial me/neutze/license/MainActivity$1/<init>(Lme/neutze/license/MainActivity;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
return
.limit locals 2
.limit stack 4
.end method

.method private onDestroyMainActivity()V
aload 0
invokespecial android/app/Activity/onDestroy()V
return
.limit locals 1
.limit stack 1
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
aload 0
iload 1
iload 2
aload 3
invokestatic com/amazon/android/Kiwi/onActivityResult(Landroid/app/Activity;IILandroid/content/Intent;)Z
ifeq L0
return
L0:
aload 0
iload 1
iload 2
aload 3
invokespecial android/app/Activity/onActivityResult(IILandroid/content/Intent;)V
return
.limit locals 4
.limit stack 4
.end method

.method public onCreate(Landroid/os/Bundle;)V
aload 0
aload 1
invokespecial me/neutze/license/MainActivity/onCreateMainActivity(Landroid/os/Bundle;)V
aload 0
iconst_1
invokestatic com/amazon/android/Kiwi/onCreate(Landroid/app/Activity;Z)V
return
.limit locals 2
.limit stack 2
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
aload 0
iload 1
invokestatic com/amazon/android/Kiwi/onCreateDialog(Landroid/app/Activity;I)Landroid/app/Dialog;
astore 2
aload 2
ifnull L0
aload 2
areturn
L0:
aload 0
iload 1
invokespecial android/app/Activity/onCreateDialog(I)Landroid/app/Dialog;
areturn
.limit locals 3
.limit stack 2
.end method

.method public onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
aload 0
iload 1
invokestatic com/amazon/android/Kiwi/onCreateDialog(Landroid/app/Activity;I)Landroid/app/Dialog;
astore 3
aload 3
ifnull L0
aload 3
areturn
L0:
aload 0
iload 1
aload 2
invokespecial android/app/Activity/onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
areturn
.limit locals 4
.limit stack 3
.end method

.method public onDestroy()V
aload 0
invokespecial me/neutze/license/MainActivity/onDestroyMainActivity()V
aload 0
invokestatic com/amazon/android/Kiwi/onDestroy(Landroid/app/Activity;)V
return
.limit locals 1
.limit stack 1
.end method

.method public onPause()V
aload 0
invokespecial android/app/Activity/onPause()V
aload 0
invokestatic com/amazon/android/Kiwi/onPause(Landroid/app/Activity;)V
return
.limit locals 1
.limit stack 1
.end method

.method public onResume()V
aload 0
invokespecial android/app/Activity/onResume()V
aload 0
invokestatic com/amazon/android/Kiwi/onResume(Landroid/app/Activity;)V
return
.limit locals 1
.limit stack 1
.end method

.method public onStart()V
aload 0
invokespecial android/app/Activity/onStart()V
aload 0
invokestatic com/amazon/android/Kiwi/onStart(Landroid/app/Activity;)V
return
.limit locals 1
.limit stack 1
.end method

.method public onStop()V
aload 0
invokespecial android/app/Activity/onStop()V
aload 0
invokestatic com/amazon/android/Kiwi/onStop(Landroid/app/Activity;)V
return
.limit locals 1
.limit stack 1
.end method
