#!/bin/bash

rm -rf ~/Desktop/master_thesis/sourcefiles/status.log

echo "--- STARTING LVL ---" >> ~/Desktop/master_thesis/sourcefiles/status.log

echo "___androguard.sh"
bash ~/Desktop/master_thesis/sourcefiles/_scripts/lvl/androguard-lvl.sh
echo "apkdec.sh"
bash ~/Desktop/master_thesis/sourcefiles/_scripts/lvl/apkdec-lvl.sh
echo "___apktool.sh"
bash ~/Desktop/master_thesis/sourcefiles/_scripts/lvl/apktool-lvl.sh
echo "___baksmali.sh"
bash ~/Desktop/master_thesis/sourcefiles/_scripts/lvl/baksmali-lvl.sh
echo "___bytecodeviewer.sh"
bash ~/Desktop/master_thesis/sourcefiles/_scripts/lvl/bytecodeviewer-lvl.sh
echo "___d2j.sh"
bash ~/Desktop/master_thesis/sourcefiles/_scripts/lvl/d2j-lvl.sh
echo "___jadx.sh"
bash ~/Desktop/master_thesis/sourcefiles/_scripts/lvl/jadx-lvl.sh
echo "___jd.sh"
bash ~/Desktop/master_thesis/sourcefiles/_scripts/lvl/jd-lvl.sh
echo "___winmerge.sh"
bash ~/Desktop/master_thesis/sourcefiles/_scripts/lvl/winmerge-lvl.sh

echo "--- FINISHED LVL ---" >> ~/Desktop/master_thesis/sourcefiles/status.log
