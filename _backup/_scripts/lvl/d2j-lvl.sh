#!/bin/bash

#src https://github.com/pxb1988/dex2jar
#-> converts apk/dex into jar/jasmin

#comments
#https://github.com/pxb1988/dex2jar/wiki/ModifyApkWithDexTool

#reset
echo "Removing old files"
rm -rf ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-original/jasmin/d2j
rm -rf ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-original/jar/lvltest_original_d2j.jar
rm -rf ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-apk/jasmin/d2j
rm -rf ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-apk/jar/lvltest_modified_apk_d2j.jar
rm -rf ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-dex/jasmin/d2j
rm -rf ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-dex/jar/lvltest_modified_dex_d2j.jar
rm -rf ~/Desktop/master_thesis/sourcefiles/2_lucky/jasmin/d2j
rm -rf ~/Desktop/master_thesis/sourcefiles/2_lucky/jar/lucky_d2j.jar
echo "Done removing old files"


#lvltest_original
echo "Starting lvltest_original"
bash ~/Desktop/master_thesis/sourcefiles/_tools/_dex2jar-2.0/d2j-dex2jar.sh -f -o ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-original/jar/lvltest_original_d2j.jar ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-original/lvltest-original.apk
bash ~/Desktop/master_thesis/sourcefiles/_tools/_dex2jar-2.0/d2j-jar2jasmin.sh -f -o ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-original/jasmin/d2j ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-original/jar/lvltest_original_d2j.jar
echo "Finished lvltest_original"

#lvltest_modified_apk
echo "Starting lvltest_modified_apk"
bash ~/Desktop/master_thesis/sourcefiles/_tools/_dex2jar-2.0/d2j-dex2jar.sh -f -o ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-apk/jar/lvltest_modified_apk_d2j.jar ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-apk/lvltest-apk.apk
bash ~/Desktop/master_thesis/sourcefiles/_tools/_dex2jar-2.0/d2j-jar2jasmin.sh -f -o ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-apk/jasmin/d2j ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-apk/jar/lvltest_modified_apk_d2j.jar
echo "Finished lvltest_modified_apk"

#lvltest_modified_dex
echo "Starting lvltest_modified_dex"
# only dex available
echo "d2j.sh not usable on lvltest_modified_dex" >> ~/Desktop/master_thesis/sourcefiles/status.log
#bash ~/Desktop/master_thesis/sourcefiles/_tools/_dex2jar-2.0/d2j-dex2jar.sh -f -o ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-dex/jar/lvltest_modified_dex_d2j.jar ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-dex/lvltest_modified_dex.apk
#bash ~/Desktop/master_thesis/sourcefiles/_tools/_dex2jar-2.0/d2j-jar2jasmin.sh -f -o ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-dex/jasmin/d2j ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-dex/jar/lvltest_modified_dex_d2j.jar
echo "Finished lvltest_modified_dex"


echo "DONE - d2j.sh" >> ~/Desktop/master_thesis/sourcefiles/status.log
