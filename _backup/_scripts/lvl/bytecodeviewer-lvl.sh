#!/bin/bash

#src https://github.com/Konloch/bytecode-viewer
#-> converts apk/dex into java

#comments
#

#reset
echo "Removing old files"
rm -rf ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-original/XXX
rm -rf ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-apk/XXX
rm -rf ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-dex/XXX
rm -rf ~/Desktop/master_thesis/sourcefiles/2_lucky/XXX
echo "Done removing old files"


#lvltest_original
echo "Starting lvltest_original"
java -jar ~/Desktop/master_thesis/sourcefiles/_tools/bytecodeviewer.jar -decompiler Procyon -i ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-original/lvltest-original.apk -o ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-original/java/procyon.zip -t all -nowait true
java -jar ~/Desktop/master_thesis/sourcefiles/_tools/bytecodeviewer.jar -decompiler CFR -i ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-original/lvltest-original.apk -o ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-original/java/cfr.zip -t all -nowait true
java -jar ~/Desktop/master_thesis/sourcefiles/_tools/bytecodeviewer.jar -decompiler FernFlower -i ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-original/lvltest-original.apk -o ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-original/java/fernflower.zip -t all -nowait true
java -jar ~/Desktop/master_thesis/sourcefiles/_tools/bytecodeviewer.jar -decompiler Krakatau -i ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-original/lvltest-original.apk -o ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-original/java/krakatau.zip -t all -nowait true
java -jar ~/Desktop/master_thesis/sourcefiles/_tools/bytecodeviewer.jar -decompiler Krakatau-Bytecode -i ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-original/lvltest-original.apk -o ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-original/java/krakataubyte.zip -t all -nowait true
java -jar ~/Desktop/master_thesis/sourcefiles/_tools/bytecodeviewer.jar -decompiler JD-GUI -i ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-original/lvltest-original.apk -o ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-original/java/jdgui.zip -t all -nowait true
java -jar ~/Desktop/master_thesis/sourcefiles/_tools/bytecodeviewer.jar -decompiler Smali -i ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-original/lvltest-original.apk -o ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-original/smali/bytecodeviewer.zip -t all -nowait true
echo "Finished lvltest_original"

#lvltest_modified_apk
echo "Starting lvltest_modified_apk"
java -jar ~/Desktop/master_thesis/sourcefiles/_tools/bytecodeviewer.jar -decompiler Procyon -i ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-apk/lvltest-apk.apk -o ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-apk/java/procyon.zip -t all -nowait true
java -jar ~/Desktop/master_thesis/sourcefiles/_tools/bytecodeviewer.jar -decompiler CFR -i ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-apk/lvltest-apk.apk -o ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-apk/java/cfr.zip -t all -nowait true
java -jar ~/Desktop/master_thesis/sourcefiles/_tools/bytecodeviewer.jar -decompiler FernFlower -i ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-apk/lvltest-apk.apk -o ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-apk/java/fernflower.zip -t all -nowait true
java -jar ~/Desktop/master_thesis/sourcefiles/_tools/bytecodeviewer.jar -decompiler Krakatau -i ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-apk/lvltest-apk.apk -o ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-apk/java/krakatau.zip -t all -nowait true
java -jar ~/Desktop/master_thesis/sourcefiles/_tools/bytecodeviewer.jar -decompiler Krakatau-Bytecode -i ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-apk/lvltest-apk.apk -o ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-apk/java/krakataubyte.zip -t all -nowait true
java -jar ~/Desktop/master_thesis/sourcefiles/_tools/bytecodeviewer.jar -decompiler JD-GUI -i ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-original/1_lvltest_modified_apk/lvltest-apk.apk -o ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-apk/java/jdgui.zip -t all -nowait true
java -jar ~/Desktop/master_thesis/sourcefiles/_tools/bytecodeviewer.jar -decompiler Smali -i ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-original/1_lvltest_modified_apk/lvltest-apk.apk -o ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-apk/smali/bytecodeviewer.zip -t all -nowait true
echo "Finished lvltest_modified_apk"

#lvltest_modified_dex
echo "Starting lvltest_modified_dex"
java -jar ~/Desktop/master_thesis/sourcefiles/_tools/bytecodeviewer.jar -decompiler Procyon -i ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-dex/lvltest-dex.dex -o ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-dex/java/procyon.zip -t all -nowait true
java -jar ~/Desktop/master_thesis/sourcefiles/_tools/bytecodeviewer.jar -decompiler CFR -i ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-dex/lvltest-dex.dex -o ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-dex/java/cfr.zip -t all -nowait true
java -jar ~/Desktop/master_thesis/sourcefiles/_tools/bytecodeviewer.jar -decompiler FernFlower -i ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-dex/lvltest-dex.dex -o ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-dex/java/fernflower.zip -t all -nowait true
java -jar ~/Desktop/master_thesis/sourcefiles/_tools/bytecodeviewer.jar -decompiler Krakatau -i ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-dex/lvltest-dex.dex -o ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-dex/java/krakatau.zip -t all -nowait true
java -jar ~/Desktop/master_thesis/sourcefiles/_tools/bytecodeviewer.jar -decompiler Krakatau-Bytecode -i ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-dex/lvltest-dex.dex -o ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-dex/java/krakataubyte.zip -t all -nowait true
java -jar ~/Desktop/master_thesis/sourcefiles/_tools/bytecodeviewer.jar -decompiler JD-GUI -i ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-dex/lvltest-dex.dex -o ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-dex/java/jdgui.zip -t all -nowait true
java -jar ~/Desktop/master_thesis/sourcefiles/_tools/bytecodeviewer.jar -decompiler Smali -i ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-dex/lvltest-dex.dex -o ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-dex/smali/bytecodeviewer.zip -t all -nowait true
echo "Finished lvltest_modified_dex"


echo "DONE - bytecodeviewer.sh" >> ~/Desktop/master_thesis/sourcefiles/status.log
