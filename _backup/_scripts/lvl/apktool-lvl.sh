#!/bin/bash

#src https://ibotpeaches.github.io/Apktool/
#-> converts apk/dex into smali (and other resources)

#reset
echo "Removing old files"
rm -rf ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-original/smali/apktool/
rm -rf ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-original/manifest/apktool/
rm -rf ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-apk/smali/apktool/
rm -rf ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-apk/manifest/apktool/
rm -rf ~/Desktop/master_thesis/sourcefiles/2_lucky/smali/apktool/
rm -rf ~/Desktop/master_thesis/sourcefiles/2_lucky/assets/apktool/
rm -rf ~/Desktop/master_thesis/sourcefiles/2_lucky/manifest/apktool/
echo "Done removing old files"


#lvltest_original
echo "Starting lvltest_original"
mkdir ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-original/smali/
mkdir ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-original/smali/apktool/
mkdir ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-original/manifest/
mkdir ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-original/manifest/apktool/

java -jar ~/Desktop/master_thesis/sourcefiles/_tools/apktool.jar d ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-original/lvltest-original.apk -o ~/Desktop/master_thesis/sourcefiles/_tools/tmp/

mv -f ~/Desktop/master_thesis/sourcefiles/_tools/tmp/smali/* ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-original/smali/apktool/
mv -f ~/Desktop/master_thesis/sourcefiles/_tools/tmp/original/AndroidManifest.xml ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-original/manifest/apktool/AndroidManifest.xml

rm -rf ~/Desktop/master_thesis/sourcefiles/_tools/tmp/
echo "Finished lvltest_original"

#lvltest_modified_apk
echo "Starting lvltest_modified_apk"
mkdir ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-apk/smali/
mkdir ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-apk/smali/apktool/
mkdir ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-apk/manifest/
mkdir ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-apk/manifest/apktool/

java -jar ~/Desktop/master_thesis/sourcefiles/_tools/apktool.jar d ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-apk/lvltest-apk.apk -o ~/Desktop/master_thesis/sourcefiles/_tools/tmp

mv -f ~/Desktop/master_thesis/sourcefiles/_tools/tmp/smali/* ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-apk/smali/apktool/
mv -f ~/Desktop/master_thesis/sourcefiles/_tools/tmp/original/AndroidManifest.xml ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-apk/manifest/apktool/AndroidManifest.xml

rm -rf ~/Desktop/master_thesis/sourcefiles/_tools/tmp/
echo "Finished lvltest_modified_apk"

#lvltest_modified_dex
echo "Starting lvltest_modified_dex"
# only dex available
echo "apktool.sh not usable on lvltest_modified_dex" >> ~/Desktop/master_thesis/sourcefiles/status.log
echo "Finished lvltest_modified_dex"

echo "DONE - apktool.sh" >> ~/Desktop/master_thesis/sourcefiles/status.log
