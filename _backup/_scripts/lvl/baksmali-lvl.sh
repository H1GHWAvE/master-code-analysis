#!/bin/bash

#src https://github.com/JesusFreke/smali
#-> converts apk into smali

#reset
echo "Removing old files"
rm -rf ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-original/smali/baksmali/
rm -rf ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-apk/smali/baksmali/
rm -rf ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-dex/smali/baksmali/
rm -rf ~/Desktop/master_thesis/sourcefiles/2_lucky/smali/baksmali/
echo "Done removing old files"


#lvltest_original
echo "Starting lvltest_original"
java -jar ~/Desktop/master_thesis/sourcefiles/_tools/baksmali.jar -x ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-original/lvltest-original.apk -o ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-original/smali/baksmali/
echo "Finished lvltest_original"

#lvltest_modified_apk
echo "Starting lvltest_modified_apk"
java -jar ~/Desktop/master_thesis/sourcefiles/_tools/baksmali.jar -x ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-apk/lvltest-apk.apk -o ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-apk/smali/baksmali/
echo "Finished lvltest_modified_apk"

#lvltest_modified_dex
echo "Starting lvltest_modified_dex"
java -jar ~/Desktop/master_thesis/sourcefiles/_tools/baksmali.jar -x ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-dex/lvltest-dex.dex -o ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-dex/smali/baksmali/
echo "Finished lvltest_modified_dex"


echo "DONE - baksmali.sh" >> ~/Desktop/master_thesis/sourcefiles/status.log
