#!/bin/bash

#src https://github.com/skylot/jadx
#-> converts apk/dex into java

#comments
#

#reset
echo "Removing old files"
rm -rf ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-original/java/jadx
rm -rf ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-original/manifest/jadx/AndroidManifest.xml
rm -rf ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-apk/java/jadx
rm -rf ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-apk/manifest/jadx/AndroidManifest.xml
rm -rf ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-dex/java/jadx
rm -rf ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-dex/manifest/jadx/AndroidManifest.xml
rm -rf ~/Desktop/master_thesis/sourcefiles/2_lucky/java/jadx
rm -rf ~/Desktop/master_thesis/sourcefiles/2_lucky/java/manifest/jadx/AndroidManifest.xml
echo "Done removing old files"


#lvltest_original
echo "Starting lvltest_original"
mkdir ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-original/manifest/
mkdir ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-original/manifest/jadx/

~/Desktop/master_thesis/sourcefiles/_tools/_jadx-0.6.1-dev-build221/bin/jadx -d ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-original/java/jadx --deobf --show-bad-code ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-original/lvltest-original.apk

mv ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-original/java/jadx/AndroidManifest.xml ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-original/manifest/jadx/AndroidManifest.xml
echo "Finished lvltest_original"

#lvltest_modified_apk
echo "Starting lvltest_modified_apk"
mkdir ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-apk/manifest/
mkdir ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-apk/manifest/jadx/

~/Desktop/master_thesis/sourcefiles/_tools/_jadx-0.6.1-dev-build221/bin/jadx -d ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-apk/java/jadx --deobf --show-bad-code ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-apk/lvltest-apk.apk

mv ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-apk/java/jadx/AndroidManifest.xml ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-apk/manifest/jadx/AndroidManifest.xml
echo "Finished lvltest_modified_apk"

#lvltest_modified_dex
echo "Starting lvltest_modified_dex"
mkdir ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-dex/manifest/
mkdir ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-dex/manifest/jadx/

~/Desktop/master_thesis/sourcefiles/_tools/_jadx-0.6.1-dev-build221/bin/jadx -d ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-dex/java/jadx --deobf --show-bad-code ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-dex/lvltest-dex.dex

mv ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-dex/java/jadx/AndroidManifest.xml ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-dex/manifest/jadx/AndroidManifest.xml
echo "Finished lvltest_modified_dex"


echo "DONE - jadx.sh" >> ~/Desktop/master_thesis/sourcefiles/status.log
