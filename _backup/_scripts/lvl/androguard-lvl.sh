#!/bin/bash

#src https://code.google.com/p/androguard/

#comments
#http://www.technotalkative.com/part-2-deep-analysis-using-androguard-tools-2/

#androdd
#-> converts apk/dex into java

#reset
echo "Removing old files"
rm -rf ~/Desktop/master_thesis/sourcefiles/0_lvltest_original/java/androguard/
rm -rf ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-apk/java/androguard/
rm -rf ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-dex/java/androguard/
echo "Done removing old files"


#lvltest_original
echo "Starting lvltest_original"
python ~/Desktop/master_thesis/sourcefiles/_tools/androdd.py -i ~/Desktop/master_thesis/sourcefiles/0_lvltest_original/lvltest-original.apk -o ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-original/java/androguard/
echo "Finished lvltest_original"

#lvltest_modified_apk
echo "Starting lvltest_modified_apk"
python ~/Desktop/master_thesis/sourcefiles/_tools/androdd.py -i ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-apk/lvltest-apk.apk -o ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-apk/java/androguard/
echo "Finished lvltest_modified_apk"

#lvltest_modified_dex
echo "Starting lvltest_modified_dex"
python ~/Desktop/master_thesis/sourcefiles/_tools/androdd.py -i ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-dex/lvltest-dex.dex -o ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-dex/java/androguard/
echo "Finished lvltest_modified_dex"

#androsim
#-> generates log with diff of two apks
#python ~/Desktop/master_thesis/sourcefiles/_tools/androguard-2.0/androsim.py -i ~/Desktop/master_thesis/sourcefiles/0_lvltest_original/lvltest-original.apk ~/Desktop/master_thesis/sourcefiles/lvl/lvltest-apk/lvltest-apk.apk -c ZLIB -n >> ~/Desktop/master_thesis/sourcefiles/differences_original_apk.log

echo "DONE - androguard.sh" >> ~/Desktop/master_thesis/sourcefiles/status.log
