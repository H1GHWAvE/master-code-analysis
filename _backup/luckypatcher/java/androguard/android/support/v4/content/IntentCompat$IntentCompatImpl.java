package android.support.v4.content;
interface IntentCompat$IntentCompatImpl {

    public abstract android.content.Intent makeMainActivity();

    public abstract android.content.Intent makeMainSelectorActivity();

    public abstract android.content.Intent makeRestartActivityTask();
}
