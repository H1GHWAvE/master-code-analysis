package android.support.v4.content;
 class ModernAsyncTask$AsyncTaskResult {
    final Object[] mData;
    final android.support.v4.content.ModernAsyncTask mTask;

    varargs ModernAsyncTask$AsyncTaskResult(android.support.v4.content.ModernAsyncTask p1, Object[] p2)
    {
        this.mTask = p1;
        this.mData = p2;
        return;
    }
}
