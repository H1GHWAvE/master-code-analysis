package android.support.v4.content;
public class ContextCompat {

    public ContextCompat()
    {
        return;
    }

    public static boolean startActivities(android.content.Context p1, android.content.Intent[] p2)
    {
        return android.support.v4.content.ContextCompat.startActivities(p1, p2, 0);
    }

    public static boolean startActivities(android.content.Context p3, android.content.Intent[] p4, android.os.Bundle p5)
    {
        int v1 = 1;
        int v0 = android.os.Build$VERSION.SDK_INT;
        if (v0 < 16) {
            if (v0 < 11) {
                v1 = 0;
            } else {
                android.support.v4.content.ContextCompatHoneycomb.startActivities(p3, p4);
            }
        } else {
            android.support.v4.content.ContextCompatJellybean.startActivities(p3, p4, p5);
        }
        return v1;
    }
}
