package android.support.v4.graphics.drawable;
public class DrawableCompat {
    static final android.support.v4.graphics.drawable.DrawableCompat$DrawableImpl IMPL;

    static DrawableCompat()
    {
        if (android.os.Build$VERSION.SDK_INT < 11) {
            android.support.v4.graphics.drawable.DrawableCompat.IMPL = new android.support.v4.graphics.drawable.DrawableCompat$BaseDrawableImpl();
        } else {
            android.support.v4.graphics.drawable.DrawableCompat.IMPL = new android.support.v4.graphics.drawable.DrawableCompat$HoneycombDrawableImpl();
        }
        return;
    }

    public DrawableCompat()
    {
        return;
    }

    public static void jumpToCurrentState(android.graphics.drawable.Drawable p1)
    {
        android.support.v4.graphics.drawable.DrawableCompat.IMPL.jumpToCurrentState(p1);
        return;
    }
}
