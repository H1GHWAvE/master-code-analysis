package android.support.v4.text;
 class ICUCompat$ICUCompatImplIcs implements android.support.v4.text.ICUCompat$ICUCompatImpl {

    ICUCompat$ICUCompatImplIcs()
    {
        return;
    }

    public String addLikelySubtags(String p2)
    {
        return android.support.v4.text.ICUCompatIcs.addLikelySubtags(p2);
    }

    public String getScript(String p2)
    {
        return android.support.v4.text.ICUCompatIcs.getScript(p2);
    }
}
