package android.support.v4.text;
interface ICUCompat$ICUCompatImpl {

    public abstract String addLikelySubtags();

    public abstract String getScript();
}
