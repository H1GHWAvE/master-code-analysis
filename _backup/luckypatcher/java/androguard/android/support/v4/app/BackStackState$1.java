package android.support.v4.app;
final class BackStackState$1 implements android.os.Parcelable$Creator {

    BackStackState$1()
    {
        return;
    }

    public android.support.v4.app.BackStackState createFromParcel(android.os.Parcel p2)
    {
        return new android.support.v4.app.BackStackState(p2);
    }

    public bridge synthetic Object createFromParcel(android.os.Parcel p2)
    {
        return this.createFromParcel(p2);
    }

    public android.support.v4.app.BackStackState[] newArray(int p2)
    {
        android.support.v4.app.BackStackState[] v0 = new android.support.v4.app.BackStackState[p2];
        return v0;
    }

    public bridge synthetic Object[] newArray(int p2)
    {
        return this.newArray(p2);
    }
}
