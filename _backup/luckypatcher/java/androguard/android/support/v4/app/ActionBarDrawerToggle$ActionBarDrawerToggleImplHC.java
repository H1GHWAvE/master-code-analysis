package android.support.v4.app;
 class ActionBarDrawerToggle$ActionBarDrawerToggleImplHC implements android.support.v4.app.ActionBarDrawerToggle$ActionBarDrawerToggleImpl {

    private ActionBarDrawerToggle$ActionBarDrawerToggleImplHC()
    {
        return;
    }

    synthetic ActionBarDrawerToggle$ActionBarDrawerToggleImplHC(android.support.v4.app.ActionBarDrawerToggle$1 p1)
    {
        return;
    }

    public android.graphics.drawable.Drawable getThemeUpIndicator(android.app.Activity p2)
    {
        return android.support.v4.app.ActionBarDrawerToggleHoneycomb.getThemeUpIndicator(p2);
    }

    public Object setActionBarDescription(Object p2, android.app.Activity p3, int p4)
    {
        return android.support.v4.app.ActionBarDrawerToggleHoneycomb.setActionBarDescription(p2, p3, p4);
    }

    public Object setActionBarUpIndicator(Object p2, android.app.Activity p3, android.graphics.drawable.Drawable p4, int p5)
    {
        return android.support.v4.app.ActionBarDrawerToggleHoneycomb.setActionBarUpIndicator(p2, p3, p4, p5);
    }
}
