package android.support.v4.app;
 class NotificationCompatIceCreamSandwich {

    NotificationCompatIceCreamSandwich()
    {
        return;
    }

    static android.app.Notification add(android.content.Context p8, android.app.Notification p9, CharSequence p10, CharSequence p11, CharSequence p12, android.widget.RemoteViews p13, int p14, android.app.PendingIntent p15, android.app.PendingIntent p16, android.graphics.Bitmap p17, int p18, int p19, boolean p20)
    {
        android.app.Notification v4_10;
        android.app.Notification$Builder v5_7 = new android.app.Notification$Builder(p8).setWhen(p9.when).setSmallIcon(p9.icon, p9.iconLevel).setContent(p9.contentView).setTicker(p9.tickerText, p13).setSound(p9.sound, p9.audioStreamType).setVibrate(p9.vibrate).setLights(p9.ledARGB, p9.ledOnMS, p9.ledOffMS);
        if ((p9.flags & 2) == 0) {
            v4_10 = 0;
        } else {
            v4_10 = 1;
        }
        android.app.Notification v4_13;
        android.app.Notification$Builder v5_8 = v5_7.setOngoing(v4_10);
        if ((p9.flags & 8) == 0) {
            v4_13 = 0;
        } else {
            v4_13 = 1;
        }
        android.app.Notification v4_16;
        android.app.Notification$Builder v5_9 = v5_8.setOnlyAlertOnce(v4_13);
        if ((p9.flags & 16) == 0) {
            v4_16 = 0;
        } else {
            v4_16 = 1;
        }
        android.app.Notification v4_25;
        android.app.Notification$Builder v5_12 = v5_9.setAutoCancel(v4_16).setDefaults(p9.defaults).setContentTitle(p10).setContentText(p11).setContentInfo(p12).setContentIntent(p15).setDeleteIntent(p9.deleteIntent);
        if ((p9.flags & 128) == 0) {
            v4_25 = 0;
        } else {
            v4_25 = 1;
        }
        return v5_12.setFullScreenIntent(p16, v4_25).setLargeIcon(p17).setNumber(p14).setProgress(p18, p19, p20).getNotification();
    }
}
