package android.support.v4.app;
public interface FragmentManager$OnBackStackChangedListener {

    public abstract void onBackStackChanged();
}
