package android.support.v4.app;
public interface ActionBarDrawerToggle$DelegateProvider {

    public abstract android.support.v4.app.ActionBarDrawerToggle$Delegate getDrawerToggleDelegate();
}
