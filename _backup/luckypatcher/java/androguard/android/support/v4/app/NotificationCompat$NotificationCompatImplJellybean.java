package android.support.v4.app;
 class NotificationCompat$NotificationCompatImplJellybean implements android.support.v4.app.NotificationCompat$NotificationCompatImpl {

    NotificationCompat$NotificationCompatImplJellybean()
    {
        return;
    }

    public android.app.Notification build(android.support.v4.app.NotificationCompat$Builder p22)
    {
        android.support.v4.app.NotificationCompatJellybean v1_1 = new android.support.v4.app.NotificationCompatJellybean(p22.mContext, p22.mNotification, p22.mContentTitle, p22.mContentText, p22.mContentInfo, p22.mTickerView, p22.mNumber, p22.mContentIntent, p22.mFullScreenIntent, p22.mLargeIcon, p22.mProgressMax, p22.mProgress, p22.mProgressIndeterminate, p22.mUseChronometer, p22.mPriority, p22.mSubText);
        java.util.Iterator v19 = p22.mActions.iterator();
        while (v19.hasNext()) {
            android.support.v4.app.NotificationCompat$Action v18_1 = ((android.support.v4.app.NotificationCompat$Action) v19.next());
            v1_1.addAction(v18_1.icon, v18_1.title, v18_1.actionIntent);
        }
        if (p22.mStyle != null) {
            if (!(p22.mStyle instanceof android.support.v4.app.NotificationCompat$BigTextStyle)) {
                if (!(p22.mStyle instanceof android.support.v4.app.NotificationCompat$InboxStyle)) {
                    if ((p22.mStyle instanceof android.support.v4.app.NotificationCompat$BigPictureStyle)) {
                        android.support.v4.app.NotificationCompat$BigPictureStyle v20_1 = ((android.support.v4.app.NotificationCompat$BigPictureStyle) p22.mStyle);
                        v1_1.addBigPictureStyle(v20_1.mBigContentTitle, v20_1.mSummaryTextSet, v20_1.mSummaryText, v20_1.mPicture, v20_1.mBigLargeIcon, v20_1.mBigLargeIconSet);
                    }
                } else {
                    android.support.v4.app.NotificationCompat$BigPictureStyle v20_3 = ((android.support.v4.app.NotificationCompat$InboxStyle) p22.mStyle);
                    v1_1.addInboxStyle(v20_3.mBigContentTitle, v20_3.mSummaryTextSet, v20_3.mSummaryText, v20_3.mTexts);
                }
            } else {
                android.support.v4.app.NotificationCompat$BigPictureStyle v20_5 = ((android.support.v4.app.NotificationCompat$BigTextStyle) p22.mStyle);
                v1_1.addBigTextStyle(v20_5.mBigContentTitle, v20_5.mSummaryTextSet, v20_5.mSummaryText, v20_5.mBigText);
            }
        }
        return v1_1.build();
    }
}
