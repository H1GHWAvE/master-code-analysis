package android.support.v4.app;
 class NotificationCompatHoneycomb {

    NotificationCompatHoneycomb()
    {
        return;
    }

    static android.app.Notification add(android.content.Context p5, android.app.Notification p6, CharSequence p7, CharSequence p8, CharSequence p9, android.widget.RemoteViews p10, int p11, android.app.PendingIntent p12, android.app.PendingIntent p13, android.graphics.Bitmap p14)
    {
        android.app.Notification v1_10;
        android.app.Notification$Builder v2_7 = new android.app.Notification$Builder(p5).setWhen(p6.when).setSmallIcon(p6.icon, p6.iconLevel).setContent(p6.contentView).setTicker(p6.tickerText, p10).setSound(p6.sound, p6.audioStreamType).setVibrate(p6.vibrate).setLights(p6.ledARGB, p6.ledOnMS, p6.ledOffMS);
        if ((p6.flags & 2) == 0) {
            v1_10 = 0;
        } else {
            v1_10 = 1;
        }
        android.app.Notification v1_13;
        android.app.Notification$Builder v2_8 = v2_7.setOngoing(v1_10);
        if ((p6.flags & 8) == 0) {
            v1_13 = 0;
        } else {
            v1_13 = 1;
        }
        android.app.Notification v1_16;
        android.app.Notification$Builder v2_9 = v2_8.setOnlyAlertOnce(v1_13);
        if ((p6.flags & 16) == 0) {
            v1_16 = 0;
        } else {
            v1_16 = 1;
        }
        android.app.Notification v1_25;
        android.app.Notification$Builder v2_12 = v2_9.setAutoCancel(v1_16).setDefaults(p6.defaults).setContentTitle(p7).setContentText(p8).setContentInfo(p9).setContentIntent(p12).setDeleteIntent(p6.deleteIntent);
        if ((p6.flags & 128) == 0) {
            v1_25 = 0;
        } else {
            v1_25 = 1;
        }
        return v2_12.setFullScreenIntent(p13, v1_25).setLargeIcon(p14).setNumber(p11).getNotification();
    }
}
