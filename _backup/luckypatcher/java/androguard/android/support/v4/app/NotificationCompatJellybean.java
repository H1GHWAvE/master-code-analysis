package android.support.v4.app;
 class NotificationCompatJellybean {
    private android.app.Notification$Builder b;

    public NotificationCompatJellybean(android.content.Context p8, android.app.Notification p9, CharSequence p10, CharSequence p11, CharSequence p12, android.widget.RemoteViews p13, int p14, android.app.PendingIntent p15, android.app.PendingIntent p16, android.graphics.Bitmap p17, int p18, int p19, boolean p20, boolean p21, int p22, CharSequence p23)
    {
        android.app.Notification$Builder v3_10;
        android.app.Notification$Builder v4_7 = new android.app.Notification$Builder(p8).setWhen(p9.when).setSmallIcon(p9.icon, p9.iconLevel).setContent(p9.contentView).setTicker(p9.tickerText, p13).setSound(p9.sound, p9.audioStreamType).setVibrate(p9.vibrate).setLights(p9.ledARGB, p9.ledOnMS, p9.ledOffMS);
        if ((p9.flags & 2) == 0) {
            v3_10 = 0;
        } else {
            v3_10 = 1;
        }
        android.app.Notification$Builder v3_13;
        android.app.Notification$Builder v4_8 = v4_7.setOngoing(v3_10);
        if ((p9.flags & 8) == 0) {
            v3_13 = 0;
        } else {
            v3_13 = 1;
        }
        android.app.Notification$Builder v3_16;
        android.app.Notification$Builder v4_9 = v4_8.setOnlyAlertOnce(v3_13);
        if ((p9.flags & 16) == 0) {
            v3_16 = 0;
        } else {
            v3_16 = 1;
        }
        android.app.Notification$Builder v3_26;
        android.app.Notification$Builder v4_12 = v4_9.setAutoCancel(v3_16).setDefaults(p9.defaults).setContentTitle(p10).setContentText(p11).setSubText(p23).setContentInfo(p12).setContentIntent(p15).setDeleteIntent(p9.deleteIntent);
        if ((p9.flags & 128) == 0) {
            v3_26 = 0;
        } else {
            v3_26 = 1;
        }
        this.b = v4_12.setFullScreenIntent(p16, v3_26).setLargeIcon(p17).setNumber(p14).setUsesChronometer(p21).setPriority(p22).setProgress(p18, p19, p20);
        return;
    }

    public void addAction(int p2, CharSequence p3, android.app.PendingIntent p4)
    {
        this.b.addAction(p2, p3, p4);
        return;
    }

    public void addBigPictureStyle(CharSequence p4, boolean p5, CharSequence p6, android.graphics.Bitmap p7, android.graphics.Bitmap p8, boolean p9)
    {
        android.app.Notification$BigPictureStyle v0 = new android.app.Notification$BigPictureStyle(this.b).setBigContentTitle(p4).bigPicture(p7);
        if (p9) {
            v0.bigLargeIcon(p8);
        }
        if (p5) {
            v0.setSummaryText(p6);
        }
        return;
    }

    public void addBigTextStyle(CharSequence p4, boolean p5, CharSequence p6, CharSequence p7)
    {
        android.app.Notification$BigTextStyle v0 = new android.app.Notification$BigTextStyle(this.b).setBigContentTitle(p4).bigText(p7);
        if (p5) {
            v0.setSummaryText(p6);
        }
        return;
    }

    public void addInboxStyle(CharSequence p6, boolean p7, CharSequence p8, java.util.ArrayList p9)
    {
        android.app.Notification$InboxStyle v1 = new android.app.Notification$InboxStyle(this.b).setBigContentTitle(p6);
        if (p7) {
            v1.setSummaryText(p8);
        }
        java.util.Iterator v0 = p9.iterator();
        while (v0.hasNext()) {
            v1.addLine(((CharSequence) v0.next()));
        }
        return;
    }

    public android.app.Notification build()
    {
        return this.b.build();
    }
}
