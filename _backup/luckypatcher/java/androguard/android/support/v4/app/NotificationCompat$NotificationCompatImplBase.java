package android.support.v4.app;
 class NotificationCompat$NotificationCompatImplBase implements android.support.v4.app.NotificationCompat$NotificationCompatImpl {

    NotificationCompat$NotificationCompatImplBase()
    {
        return;
    }

    public android.app.Notification build(android.support.v4.app.NotificationCompat$Builder p6)
    {
        android.app.Notification v0 = p6.mNotification;
        v0.setLatestEventInfo(p6.mContext, p6.mContentTitle, p6.mContentText, p6.mContentIntent);
        if (p6.mPriority > 0) {
            v0.flags = (v0.flags | 128);
        }
        return v0;
    }
}
