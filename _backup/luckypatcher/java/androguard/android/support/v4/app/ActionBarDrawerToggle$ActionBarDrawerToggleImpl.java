package android.support.v4.app;
interface ActionBarDrawerToggle$ActionBarDrawerToggleImpl {

    public abstract android.graphics.drawable.Drawable getThemeUpIndicator();

    public abstract Object setActionBarDescription();

    public abstract Object setActionBarUpIndicator();
}
