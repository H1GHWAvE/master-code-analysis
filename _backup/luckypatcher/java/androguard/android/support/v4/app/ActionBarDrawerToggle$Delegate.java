package android.support.v4.app;
public interface ActionBarDrawerToggle$Delegate {

    public abstract android.graphics.drawable.Drawable getThemeUpIndicator();

    public abstract void setActionBarDescription();

    public abstract void setActionBarUpIndicator();
}
