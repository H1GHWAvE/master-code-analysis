package android.support.v4.app;
public class NotificationCompat {
    public static final int FLAG_HIGH_PRIORITY = 128;
    private static final android.support.v4.app.NotificationCompat$NotificationCompatImpl IMPL = None;
    public static final int PRIORITY_DEFAULT = 0;
    public static final int PRIORITY_HIGH = 1;
    public static final int PRIORITY_LOW = 255;
    public static final int PRIORITY_MAX = 2;
    public static final int PRIORITY_MIN = 254;

    static NotificationCompat()
    {
        if (android.os.Build$VERSION.SDK_INT < 16) {
            if (android.os.Build$VERSION.SDK_INT < 14) {
                if (android.os.Build$VERSION.SDK_INT < 11) {
                    android.support.v4.app.NotificationCompat.IMPL = new android.support.v4.app.NotificationCompat$NotificationCompatImplBase();
                } else {
                    android.support.v4.app.NotificationCompat.IMPL = new android.support.v4.app.NotificationCompat$NotificationCompatImplHoneycomb();
                }
            } else {
                android.support.v4.app.NotificationCompat.IMPL = new android.support.v4.app.NotificationCompat$NotificationCompatImplIceCreamSandwich();
            }
        } else {
            android.support.v4.app.NotificationCompat.IMPL = new android.support.v4.app.NotificationCompat$NotificationCompatImplJellybean();
        }
        return;
    }

    public NotificationCompat()
    {
        return;
    }

    static synthetic android.support.v4.app.NotificationCompat$NotificationCompatImpl access$000()
    {
        return android.support.v4.app.NotificationCompat.IMPL;
    }
}
