package android.support.v4.view;
 class ViewCompatGingerbread {

    ViewCompatGingerbread()
    {
        return;
    }

    public static int getOverScrollMode(android.view.View p1)
    {
        return p1.getOverScrollMode();
    }

    public static void setOverScrollMode(android.view.View p0, int p1)
    {
        p0.setOverScrollMode(p1);
        return;
    }
}
