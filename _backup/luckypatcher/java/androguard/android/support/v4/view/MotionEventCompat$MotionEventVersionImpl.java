package android.support.v4.view;
interface MotionEventCompat$MotionEventVersionImpl {

    public abstract int findPointerIndex();

    public abstract int getPointerCount();

    public abstract int getPointerId();

    public abstract float getX();

    public abstract float getY();
}
