package android.support.v4.view;
interface ViewPager$OnAdapterChangeListener {

    public abstract void onAdapterChanged();
}
