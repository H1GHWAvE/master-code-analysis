package android.support.v4.view;
 class MarginLayoutParamsCompatJellybeanMr1 {

    MarginLayoutParamsCompatJellybeanMr1()
    {
        return;
    }

    public static int getLayoutDirection(android.view.ViewGroup$MarginLayoutParams p1)
    {
        return p1.getLayoutDirection();
    }

    public static int getMarginEnd(android.view.ViewGroup$MarginLayoutParams p1)
    {
        return p1.getMarginEnd();
    }

    public static int getMarginStart(android.view.ViewGroup$MarginLayoutParams p1)
    {
        return p1.getMarginStart();
    }

    public static boolean isMarginRelative(android.view.ViewGroup$MarginLayoutParams p1)
    {
        return p1.isMarginRelative();
    }

    public static void resolveLayoutDirection(android.view.ViewGroup$MarginLayoutParams p0, int p1)
    {
        p0.resolveLayoutDirection(p1);
        return;
    }

    public static void setLayoutDirection(android.view.ViewGroup$MarginLayoutParams p0, int p1)
    {
        p0.setLayoutDirection(p1);
        return;
    }

    public static void setMarginEnd(android.view.ViewGroup$MarginLayoutParams p0, int p1)
    {
        p0.setMarginEnd(p1);
        return;
    }

    public static void setMarginStart(android.view.ViewGroup$MarginLayoutParams p0, int p1)
    {
        p0.setMarginStart(p1);
        return;
    }
}
