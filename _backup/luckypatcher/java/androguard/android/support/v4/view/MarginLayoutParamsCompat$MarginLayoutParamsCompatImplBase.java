package android.support.v4.view;
 class MarginLayoutParamsCompat$MarginLayoutParamsCompatImplBase implements android.support.v4.view.MarginLayoutParamsCompat$MarginLayoutParamsCompatImpl {

    MarginLayoutParamsCompat$MarginLayoutParamsCompatImplBase()
    {
        return;
    }

    public int getLayoutDirection(android.view.ViewGroup$MarginLayoutParams p2)
    {
        return 0;
    }

    public int getMarginEnd(android.view.ViewGroup$MarginLayoutParams p2)
    {
        return p2.rightMargin;
    }

    public int getMarginStart(android.view.ViewGroup$MarginLayoutParams p2)
    {
        return p2.leftMargin;
    }

    public boolean isMarginRelative(android.view.ViewGroup$MarginLayoutParams p2)
    {
        return 0;
    }

    public void resolveLayoutDirection(android.view.ViewGroup$MarginLayoutParams p1, int p2)
    {
        return;
    }

    public void setLayoutDirection(android.view.ViewGroup$MarginLayoutParams p1, int p2)
    {
        return;
    }

    public void setMarginEnd(android.view.ViewGroup$MarginLayoutParams p1, int p2)
    {
        p1.rightMargin = p2;
        return;
    }

    public void setMarginStart(android.view.ViewGroup$MarginLayoutParams p1, int p2)
    {
        p1.leftMargin = p2;
        return;
    }
}
