package android.support.v4.view;
final class ViewPager$SavedState$1 implements android.support.v4.os.ParcelableCompatCreatorCallbacks {

    ViewPager$SavedState$1()
    {
        return;
    }

    public android.support.v4.view.ViewPager$SavedState createFromParcel(android.os.Parcel p2, ClassLoader p3)
    {
        return new android.support.v4.view.ViewPager$SavedState(p2, p3);
    }

    public bridge synthetic Object createFromParcel(android.os.Parcel p2, ClassLoader p3)
    {
        return this.createFromParcel(p2, p3);
    }

    public android.support.v4.view.ViewPager$SavedState[] newArray(int p2)
    {
        android.support.v4.view.ViewPager$SavedState[] v0 = new android.support.v4.view.ViewPager$SavedState[p2];
        return v0;
    }

    public bridge synthetic Object[] newArray(int p2)
    {
        return this.newArray(p2);
    }
}
