package android.support.v4.view;
interface MenuItemCompatIcs$SupportActionExpandProxy {

    public abstract boolean onMenuItemActionCollapse();

    public abstract boolean onMenuItemActionExpand();
}
