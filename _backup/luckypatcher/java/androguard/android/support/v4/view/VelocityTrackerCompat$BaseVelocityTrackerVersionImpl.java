package android.support.v4.view;
 class VelocityTrackerCompat$BaseVelocityTrackerVersionImpl implements android.support.v4.view.VelocityTrackerCompat$VelocityTrackerVersionImpl {

    VelocityTrackerCompat$BaseVelocityTrackerVersionImpl()
    {
        return;
    }

    public float getXVelocity(android.view.VelocityTracker p2, int p3)
    {
        return p2.getXVelocity();
    }

    public float getYVelocity(android.view.VelocityTracker p2, int p3)
    {
        return p2.getYVelocity();
    }
}
