package android.support.v4.view.accessibility;
interface AccessibilityManagerCompat$AccessibilityManagerVersionImpl {

    public abstract boolean addAccessibilityStateChangeListener();

    public abstract java.util.List getEnabledAccessibilityServiceList();

    public abstract java.util.List getInstalledAccessibilityServiceList();

    public abstract boolean isTouchExplorationEnabled();

    public abstract Object newAccessiblityStateChangeListener();

    public abstract boolean removeAccessibilityStateChangeListener();
}
