package android.support.v4.view;
 class ViewParentCompat$ViewParentCompatICSImpl extends android.support.v4.view.ViewParentCompat$ViewParentCompatStubImpl {

    ViewParentCompat$ViewParentCompatICSImpl()
    {
        return;
    }

    public boolean requestSendAccessibilityEvent(android.view.ViewParent p2, android.view.View p3, android.view.accessibility.AccessibilityEvent p4)
    {
        return android.support.v4.view.ViewParentCompatICS.requestSendAccessibilityEvent(p2, p3, p4);
    }
}
