package android.support.v4.view.accessibility;
 class AccessibilityRecordCompatIcsMr1 {

    AccessibilityRecordCompatIcsMr1()
    {
        return;
    }

    public static int getMaxScrollX(Object p1)
    {
        return ((android.view.accessibility.AccessibilityRecord) p1).getMaxScrollX();
    }

    public static int getMaxScrollY(Object p1)
    {
        return ((android.view.accessibility.AccessibilityRecord) p1).getMaxScrollY();
    }

    public static void setMaxScrollX(Object p0, int p1)
    {
        ((android.view.accessibility.AccessibilityRecord) p0).setMaxScrollX(p1);
        return;
    }

    public static void setMaxScrollY(Object p0, int p1)
    {
        ((android.view.accessibility.AccessibilityRecord) p0).setMaxScrollY(p1);
        return;
    }
}
