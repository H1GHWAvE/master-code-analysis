package android.support.v4.view;
public interface MenuItemCompat$OnActionExpandListener {

    public abstract boolean onMenuItemActionCollapse();

    public abstract boolean onMenuItemActionExpand();
}
