package android.support.v4.view;
 class GravityCompat$GravityCompatImplJellybeanMr1 implements android.support.v4.view.GravityCompat$GravityCompatImpl {

    GravityCompat$GravityCompatImplJellybeanMr1()
    {
        return;
    }

    public void apply(int p1, int p2, int p3, android.graphics.Rect p4, int p5, int p6, android.graphics.Rect p7, int p8)
    {
        android.support.v4.view.GravityCompatJellybeanMr1.apply(p1, p2, p3, p4, p5, p6, p7, p8);
        return;
    }

    public void apply(int p1, int p2, int p3, android.graphics.Rect p4, android.graphics.Rect p5, int p6)
    {
        android.support.v4.view.GravityCompatJellybeanMr1.apply(p1, p2, p3, p4, p5, p6);
        return;
    }

    public void applyDisplay(int p1, android.graphics.Rect p2, android.graphics.Rect p3, int p4)
    {
        android.support.v4.view.GravityCompatJellybeanMr1.applyDisplay(p1, p2, p3, p4);
        return;
    }

    public int getAbsoluteGravity(int p2, int p3)
    {
        return android.support.v4.view.GravityCompatJellybeanMr1.getAbsoluteGravity(p2, p3);
    }
}
