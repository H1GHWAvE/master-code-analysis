package android.support.v4.view;
 class ViewCompatHC {

    ViewCompatHC()
    {
        return;
    }

    public static float getAlpha(android.view.View p1)
    {
        return p1.getAlpha();
    }

    static long getFrameTime()
    {
        return android.animation.ValueAnimator.getFrameDelay();
    }

    public static int getLayerType(android.view.View p1)
    {
        return p1.getLayerType();
    }

    public static int resolveSizeAndState(int p1, int p2, int p3)
    {
        return android.view.View.resolveSizeAndState(p1, p2, p3);
    }

    public static void setLayerType(android.view.View p0, int p1, android.graphics.Paint p2)
    {
        p0.setLayerType(p1, p2);
        return;
    }
}
