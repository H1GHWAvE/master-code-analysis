package android.support.v4.view;
public interface ViewPager$OnPageChangeListener {

    public abstract void onPageScrollStateChanged();

    public abstract void onPageScrolled();

    public abstract void onPageSelected();
}
