package android.support.v4.view.accessibility;
interface AccessibilityNodeProviderCompatJellyBean$AccessibilityNodeInfoBridge {

    public abstract Object createAccessibilityNodeInfo();

    public abstract java.util.List findAccessibilityNodeInfosByText();

    public abstract boolean performAction();
}
