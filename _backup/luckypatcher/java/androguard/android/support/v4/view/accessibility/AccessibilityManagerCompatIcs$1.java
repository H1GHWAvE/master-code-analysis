package android.support.v4.view.accessibility;
final class AccessibilityManagerCompatIcs$1 implements android.view.accessibility.AccessibilityManager$AccessibilityStateChangeListener {
    final synthetic android.support.v4.view.accessibility.AccessibilityManagerCompatIcs$AccessibilityStateChangeListenerBridge val$bridge;

    AccessibilityManagerCompatIcs$1(android.support.v4.view.accessibility.AccessibilityManagerCompatIcs$AccessibilityStateChangeListenerBridge p1)
    {
        this.val$bridge = p1;
        return;
    }

    public void onAccessibilityStateChanged(boolean p2)
    {
        this.val$bridge.onAccessibilityStateChanged(p2);
        return;
    }
}
