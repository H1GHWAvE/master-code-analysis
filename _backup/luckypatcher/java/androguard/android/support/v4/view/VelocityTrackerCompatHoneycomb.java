package android.support.v4.view;
 class VelocityTrackerCompatHoneycomb {

    VelocityTrackerCompatHoneycomb()
    {
        return;
    }

    public static float getXVelocity(android.view.VelocityTracker p1, int p2)
    {
        return p1.getXVelocity(p2);
    }

    public static float getYVelocity(android.view.VelocityTracker p1, int p2)
    {
        return p1.getYVelocity(p2);
    }
}
