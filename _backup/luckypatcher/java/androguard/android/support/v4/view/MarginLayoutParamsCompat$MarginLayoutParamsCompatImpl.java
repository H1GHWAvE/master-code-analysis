package android.support.v4.view;
interface MarginLayoutParamsCompat$MarginLayoutParamsCompatImpl {

    public abstract int getLayoutDirection();

    public abstract int getMarginEnd();

    public abstract int getMarginStart();

    public abstract boolean isMarginRelative();

    public abstract void resolveLayoutDirection();

    public abstract void setLayoutDirection();

    public abstract void setMarginEnd();

    public abstract void setMarginStart();
}
