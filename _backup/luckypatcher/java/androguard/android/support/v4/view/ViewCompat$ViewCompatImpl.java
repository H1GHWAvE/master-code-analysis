package android.support.v4.view;
interface ViewCompat$ViewCompatImpl {

    public abstract boolean canScrollHorizontally();

    public abstract boolean canScrollVertically();

    public abstract android.support.v4.view.accessibility.AccessibilityNodeProviderCompat getAccessibilityNodeProvider();

    public abstract float getAlpha();

    public abstract int getImportantForAccessibility();

    public abstract int getLabelFor();

    public abstract int getLayerType();

    public abstract int getLayoutDirection();

    public abstract int getOverScrollMode();

    public abstract android.view.ViewParent getParentForAccessibility();

    public abstract boolean hasTransientState();

    public abstract boolean isOpaque();

    public abstract int[] mergeDrawableStates();

    public abstract void onInitializeAccessibilityEvent();

    public abstract void onInitializeAccessibilityNodeInfo();

    public abstract void onPopulateAccessibilityEvent();

    public abstract boolean performAccessibilityAction();

    public abstract void postInvalidateOnAnimation();

    public abstract void postInvalidateOnAnimation();

    public abstract void postOnAnimation();

    public abstract void postOnAnimationDelayed();

    public abstract int resolveSizeAndState();

    public abstract void setAccessibilityDelegate();

    public abstract void setHasTransientState();

    public abstract void setImportantForAccessibility();

    public abstract void setLabelFor();

    public abstract void setLayerPaint();

    public abstract void setLayerType();

    public abstract void setLayoutDirection();

    public abstract void setOverScrollMode();
}
