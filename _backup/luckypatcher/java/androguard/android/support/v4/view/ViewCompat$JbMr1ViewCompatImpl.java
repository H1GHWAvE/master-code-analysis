package android.support.v4.view;
 class ViewCompat$JbMr1ViewCompatImpl extends android.support.v4.view.ViewCompat$JBViewCompatImpl {

    ViewCompat$JbMr1ViewCompatImpl()
    {
        return;
    }

    public int getLabelFor(android.view.View p2)
    {
        return android.support.v4.view.ViewCompatJellybeanMr1.getLabelFor(p2);
    }

    public int getLayoutDirection(android.view.View p2)
    {
        return android.support.v4.view.ViewCompatJellybeanMr1.getLayoutDirection(p2);
    }

    public void setLabelFor(android.view.View p1, int p2)
    {
        android.support.v4.view.ViewCompatJellybeanMr1.setLabelFor(p1, p2);
        return;
    }

    public void setLayerPaint(android.view.View p1, android.graphics.Paint p2)
    {
        android.support.v4.view.ViewCompatJellybeanMr1.setLayerPaint(p1, p2);
        return;
    }

    public void setLayoutDirection(android.view.View p1, int p2)
    {
        android.support.v4.view.ViewCompatJellybeanMr1.setLayoutDirection(p1, p2);
        return;
    }
}
