package android.support.v4.view;
 class AccessibilityDelegateCompat$AccessibilityDelegateStubImpl implements android.support.v4.view.AccessibilityDelegateCompat$AccessibilityDelegateImpl {

    AccessibilityDelegateCompat$AccessibilityDelegateStubImpl()
    {
        return;
    }

    public boolean dispatchPopulateAccessibilityEvent(Object p2, android.view.View p3, android.view.accessibility.AccessibilityEvent p4)
    {
        return 0;
    }

    public android.support.v4.view.accessibility.AccessibilityNodeProviderCompat getAccessibilityNodeProvider(Object p2, android.view.View p3)
    {
        return 0;
    }

    public Object newAccessiblityDelegateBridge(android.support.v4.view.AccessibilityDelegateCompat p2)
    {
        return 0;
    }

    public Object newAccessiblityDelegateDefaultImpl()
    {
        return 0;
    }

    public void onInitializeAccessibilityEvent(Object p1, android.view.View p2, android.view.accessibility.AccessibilityEvent p3)
    {
        return;
    }

    public void onInitializeAccessibilityNodeInfo(Object p1, android.view.View p2, android.support.v4.view.accessibility.AccessibilityNodeInfoCompat p3)
    {
        return;
    }

    public void onPopulateAccessibilityEvent(Object p1, android.view.View p2, android.view.accessibility.AccessibilityEvent p3)
    {
        return;
    }

    public boolean onRequestSendAccessibilityEvent(Object p2, android.view.ViewGroup p3, android.view.View p4, android.view.accessibility.AccessibilityEvent p5)
    {
        return 1;
    }

    public boolean performAccessibilityAction(Object p2, android.view.View p3, int p4, android.os.Bundle p5)
    {
        return 0;
    }

    public void sendAccessibilityEvent(Object p1, android.view.View p2, int p3)
    {
        return;
    }

    public void sendAccessibilityEventUnchecked(Object p1, android.view.View p2, android.view.accessibility.AccessibilityEvent p3)
    {
        return;
    }
}
