package android.support.v4.view;
 class ViewConfigurationCompat$BaseViewConfigurationVersionImpl implements android.support.v4.view.ViewConfigurationCompat$ViewConfigurationVersionImpl {

    ViewConfigurationCompat$BaseViewConfigurationVersionImpl()
    {
        return;
    }

    public int getScaledPagingTouchSlop(android.view.ViewConfiguration p2)
    {
        return p2.getScaledTouchSlop();
    }
}
