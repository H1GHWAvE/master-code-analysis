package android.support.v4.view;
 class GestureDetectorCompat$GestureDetectorCompatImplBase implements android.support.v4.view.GestureDetectorCompat$GestureDetectorCompatImpl {
    private static final int DOUBLE_TAP_TIMEOUT = 0;
    private static final int LONGPRESS_TIMEOUT = 0;
    private static final int LONG_PRESS = 2;
    private static final int SHOW_PRESS = 1;
    private static final int TAP = 3;
    private static final int TAP_TIMEOUT;
    private boolean mAlwaysInBiggerTapRegion;
    private boolean mAlwaysInTapRegion;
    private android.view.MotionEvent mCurrentDownEvent;
    private boolean mDeferConfirmSingleTap;
    private android.view.GestureDetector$OnDoubleTapListener mDoubleTapListener;
    private int mDoubleTapSlopSquare;
    private float mDownFocusX;
    private float mDownFocusY;
    private final android.os.Handler mHandler;
    private boolean mInLongPress;
    private boolean mIsDoubleTapping;
    private boolean mIsLongpressEnabled;
    private float mLastFocusX;
    private float mLastFocusY;
    private final android.view.GestureDetector$OnGestureListener mListener;
    private int mMaximumFlingVelocity;
    private int mMinimumFlingVelocity;
    private android.view.MotionEvent mPreviousUpEvent;
    private boolean mStillDown;
    private int mTouchSlopSquare;
    private android.view.VelocityTracker mVelocityTracker;

    static GestureDetectorCompat$GestureDetectorCompatImplBase()
    {
        android.support.v4.view.GestureDetectorCompat$GestureDetectorCompatImplBase.LONGPRESS_TIMEOUT = android.view.ViewConfiguration.getLongPressTimeout();
        android.support.v4.view.GestureDetectorCompat$GestureDetectorCompatImplBase.TAP_TIMEOUT = android.view.ViewConfiguration.getTapTimeout();
        android.support.v4.view.GestureDetectorCompat$GestureDetectorCompatImplBase.DOUBLE_TAP_TIMEOUT = android.view.ViewConfiguration.getDoubleTapTimeout();
        return;
    }

    public GestureDetectorCompat$GestureDetectorCompatImplBase(android.content.Context p2, android.view.GestureDetector$OnGestureListener p3, android.os.Handler p4)
    {
        if (p4 == null) {
            this.mHandler = new android.support.v4.view.GestureDetectorCompat$GestureDetectorCompatImplBase$GestureHandler(this);
        } else {
            this.mHandler = new android.support.v4.view.GestureDetectorCompat$GestureDetectorCompatImplBase$GestureHandler(this, p4);
        }
        this.mListener = p3;
        if ((p3 instanceof android.view.GestureDetector$OnDoubleTapListener)) {
            this.setOnDoubleTapListener(((android.view.GestureDetector$OnDoubleTapListener) p3));
        }
        this.init(p2);
        return;
    }

    static synthetic android.view.MotionEvent access$000(android.support.v4.view.GestureDetectorCompat$GestureDetectorCompatImplBase p1)
    {
        return p1.mCurrentDownEvent;
    }

    static synthetic android.view.GestureDetector$OnGestureListener access$100(android.support.v4.view.GestureDetectorCompat$GestureDetectorCompatImplBase p1)
    {
        return p1.mListener;
    }

    static synthetic void access$200(android.support.v4.view.GestureDetectorCompat$GestureDetectorCompatImplBase p0)
    {
        p0.dispatchLongPress();
        return;
    }

    static synthetic android.view.GestureDetector$OnDoubleTapListener access$300(android.support.v4.view.GestureDetectorCompat$GestureDetectorCompatImplBase p1)
    {
        return p1.mDoubleTapListener;
    }

    static synthetic boolean access$400(android.support.v4.view.GestureDetectorCompat$GestureDetectorCompatImplBase p1)
    {
        return p1.mStillDown;
    }

    static synthetic boolean access$502(android.support.v4.view.GestureDetectorCompat$GestureDetectorCompatImplBase p0, boolean p1)
    {
        p0.mDeferConfirmSingleTap = p1;
        return p1;
    }

    private void cancel()
    {
        this.mHandler.removeMessages(1);
        this.mHandler.removeMessages(2);
        this.mHandler.removeMessages(3);
        this.mVelocityTracker.recycle();
        this.mVelocityTracker = 0;
        this.mIsDoubleTapping = 0;
        this.mStillDown = 0;
        this.mAlwaysInTapRegion = 0;
        this.mAlwaysInBiggerTapRegion = 0;
        this.mDeferConfirmSingleTap = 0;
        if (this.mInLongPress) {
            this.mInLongPress = 0;
        }
        return;
    }

    private void cancelTaps()
    {
        this.mHandler.removeMessages(1);
        this.mHandler.removeMessages(2);
        this.mHandler.removeMessages(3);
        this.mIsDoubleTapping = 0;
        this.mAlwaysInTapRegion = 0;
        this.mAlwaysInBiggerTapRegion = 0;
        this.mDeferConfirmSingleTap = 0;
        if (this.mInLongPress) {
            this.mInLongPress = 0;
        }
        return;
    }

    private void dispatchLongPress()
    {
        this.mHandler.removeMessages(3);
        this.mDeferConfirmSingleTap = 0;
        this.mInLongPress = 1;
        this.mListener.onLongPress(this.mCurrentDownEvent);
        return;
    }

    private void init(android.content.Context p6)
    {
        if (p6 != null) {
            if (this.mListener != null) {
                this.mIsLongpressEnabled = 1;
                android.view.ViewConfiguration v0 = android.view.ViewConfiguration.get(p6);
                int v2 = v0.getScaledTouchSlop();
                int v1 = v0.getScaledDoubleTapSlop();
                this.mMinimumFlingVelocity = v0.getScaledMinimumFlingVelocity();
                this.mMaximumFlingVelocity = v0.getScaledMaximumFlingVelocity();
                this.mTouchSlopSquare = (v2 * v2);
                this.mDoubleTapSlopSquare = (v1 * v1);
                return;
            } else {
                throw new IllegalArgumentException("OnGestureListener must not be null");
            }
        } else {
            throw new IllegalArgumentException("Context must not be null");
        }
    }

    private boolean isConsideredDoubleTap(android.view.MotionEvent p8, android.view.MotionEvent p9, android.view.MotionEvent p10)
    {
        int v2 = 0;
        if ((this.mAlwaysInBiggerTapRegion) && (((p10.getEventTime() - p9.getEventTime()) <= ((long) android.support.v4.view.GestureDetectorCompat$GestureDetectorCompatImplBase.DOUBLE_TAP_TIMEOUT)) && ((((((int) p8.getX()) - ((int) p10.getX())) * (((int) p8.getX()) - ((int) p10.getX()))) + ((((int) p8.getY()) - ((int) p10.getY())) * (((int) p8.getY()) - ((int) p10.getY())))) < this.mDoubleTapSlopSquare))) {
            v2 = 1;
        }
        return v2;
    }

    public boolean isLongpressEnabled()
    {
        return this.mIsLongpressEnabled;
    }

    public boolean onTouchEvent(android.view.MotionEvent p42)
    {
        int v5 = p42.getAction();
        if (this.mVelocityTracker == null) {
            this.mVelocityTracker = android.view.VelocityTracker.obtain();
        }
        int v21;
        this.mVelocityTracker.addMovement(p42);
        if ((v5 & 255) != 6) {
            v21 = 0;
        } else {
            v21 = 1;
        }
        int v24;
        if (v21 == 0) {
            v24 = -1;
        } else {
            v24 = android.support.v4.view.MotionEventCompat.getActionIndex(p42);
        }
        float v25 = 0;
        float v26 = 0;
        int v6 = android.support.v4.view.MotionEventCompat.getPointerCount(p42);
        int v17_0 = 0;
        while (v17_0 < v6) {
            if (v24 != v17_0) {
                v25 += android.support.v4.view.MotionEventCompat.getX(p42, v17_0);
                v26 += android.support.v4.view.MotionEventCompat.getY(p42, v17_0);
            }
            v17_0++;
        }
        int v11;
        if (v21 == 0) {
            v11 = v6;
        } else {
            v11 = (v6 - 1);
        }
        float v13 = (v25 / ((float) v11));
        float v14 = (v26 / ((float) v11));
        int v16_0 = 0;
        switch ((v5 & 255)) {
            case 0:
                if (this.mDoubleTapListener != null) {
                    boolean v15 = this.mHandler.hasMessages(3);
                    if (v15) {
                        this.mHandler.removeMessages(3);
                    }
                    if ((this.mCurrentDownEvent == null) || ((this.mPreviousUpEvent == null) || ((!v15) || (!this.isConsideredDoubleTap(this.mCurrentDownEvent, this.mPreviousUpEvent, p42))))) {
                        this.mHandler.sendEmptyMessageDelayed(3, ((long) android.support.v4.view.GestureDetectorCompat$GestureDetectorCompatImplBase.DOUBLE_TAP_TIMEOUT));
                    } else {
                        this.mIsDoubleTapping = 1;
                        v16_0 = ((0 | this.mDoubleTapListener.onDoubleTap(this.mCurrentDownEvent)) | this.mDoubleTapListener.onDoubleTapEvent(p42));
                    }
                }
                this.mLastFocusX = v13;
                this.mDownFocusX = v13;
                this.mLastFocusY = v14;
                this.mDownFocusY = v14;
                if (this.mCurrentDownEvent != null) {
                    this.mCurrentDownEvent.recycle();
                }
                this.mCurrentDownEvent = android.view.MotionEvent.obtain(p42);
                this.mAlwaysInTapRegion = 1;
                this.mAlwaysInBiggerTapRegion = 1;
                this.mStillDown = 1;
                this.mInLongPress = 0;
                this.mDeferConfirmSingleTap = 0;
                if (this.mIsLongpressEnabled) {
                    this.mHandler.removeMessages(2);
                    this.mHandler.sendEmptyMessageAtTime(2, ((this.mCurrentDownEvent.getDownTime() + ((long) android.support.v4.view.GestureDetectorCompat$GestureDetectorCompatImplBase.TAP_TIMEOUT)) + ((long) android.support.v4.view.GestureDetectorCompat$GestureDetectorCompatImplBase.LONGPRESS_TIMEOUT)));
                }
                this.mHandler.sendEmptyMessageAtTime(1, (this.mCurrentDownEvent.getDownTime() + ((long) android.support.v4.view.GestureDetectorCompat$GestureDetectorCompatImplBase.TAP_TIMEOUT)));
                v16_0 |= this.mListener.onDown(p42);
                break;
            case 1:
                this.mStillDown = 0;
                android.view.MotionEvent v7 = android.view.MotionEvent.obtain(p42);
                if (!this.mIsDoubleTapping) {
                    if (!this.mInLongPress) {
                        if (!this.mAlwaysInTapRegion) {
                            android.view.VelocityTracker v28 = this.mVelocityTracker;
                            int v20 = android.support.v4.view.MotionEventCompat.getPointerId(p42, 0);
                            v28.computeCurrentVelocity(1000, ((float) this.mMaximumFlingVelocity));
                            float v30 = android.support.v4.view.VelocityTrackerCompat.getYVelocity(v28, v20);
                            float v29 = android.support.v4.view.VelocityTrackerCompat.getXVelocity(v28, v20);
                            if ((Math.abs(v30) > ((float) this.mMinimumFlingVelocity)) || (Math.abs(v29) > ((float) this.mMinimumFlingVelocity))) {
                                v16_0 = this.mListener.onFling(this.mCurrentDownEvent, p42, v29, v30);
                            }
                        } else {
                            v16_0 = this.mListener.onSingleTapUp(p42);
                            if ((this.mDeferConfirmSingleTap) && (this.mDoubleTapListener != null)) {
                                this.mDoubleTapListener.onSingleTapConfirmed(p42);
                            }
                        }
                    } else {
                        this.mHandler.removeMessages(3);
                        this.mInLongPress = 0;
                    }
                } else {
                    v16_0 = (0 | this.mDoubleTapListener.onDoubleTapEvent(p42));
                }
                if (this.mPreviousUpEvent != null) {
                    this.mPreviousUpEvent.recycle();
                }
                this.mPreviousUpEvent = v7;
                if (this.mVelocityTracker != null) {
                    this.mVelocityTracker.recycle();
                    this.mVelocityTracker = 0;
                }
                this.mIsDoubleTapping = 0;
                this.mDeferConfirmSingleTap = 0;
                this.mHandler.removeMessages(1);
                this.mHandler.removeMessages(2);
                break;
            case 2:
                if (this.mInLongPress) {
                } else {
                    float v22 = (this.mLastFocusX - v13);
                    float v23 = (this.mLastFocusY - v14);
                    if (!this.mIsDoubleTapping) {
                        if (!this.mAlwaysInTapRegion) {
                            if ((Math.abs(v22) < 1065353216) && (Math.abs(v23) < 1065353216)) {
                            } else {
                                v16_0 = this.mListener.onScroll(this.mCurrentDownEvent, p42, v22, v23);
                                this.mLastFocusX = v13;
                                this.mLastFocusY = v14;
                            }
                        } else {
                            int v10 = ((((int) (v13 - this.mDownFocusX)) * ((int) (v13 - this.mDownFocusX))) + (((int) (v14 - this.mDownFocusY)) * ((int) (v14 - this.mDownFocusY))));
                            if (v10 > this.mTouchSlopSquare) {
                                v16_0 = this.mListener.onScroll(this.mCurrentDownEvent, p42, v22, v23);
                                this.mLastFocusX = v13;
                                this.mLastFocusY = v14;
                                this.mAlwaysInTapRegion = 0;
                                this.mHandler.removeMessages(3);
                                this.mHandler.removeMessages(1);
                                this.mHandler.removeMessages(2);
                            }
                            if (v10 <= this.mTouchSlopSquare) {
                            } else {
                                this.mAlwaysInBiggerTapRegion = 0;
                            }
                        }
                    } else {
                        v16_0 = (0 | this.mDoubleTapListener.onDoubleTapEvent(p42));
                    }
                }
                break;
            case 3:
                this.cancel();
            case 4:
            default:
                break;
            case 5:
                this.mLastFocusX = v13;
                this.mDownFocusX = v13;
                this.mLastFocusY = v14;
                this.mDownFocusY = v14;
                this.cancelTaps();
                break;
            case 6:
                this.mLastFocusX = v13;
                this.mDownFocusX = v13;
                this.mLastFocusY = v14;
                this.mDownFocusY = v14;
                this.mVelocityTracker.computeCurrentVelocity(1000, ((float) this.mMaximumFlingVelocity));
                int v27 = android.support.v4.view.MotionEventCompat.getActionIndex(p42);
                int v18 = android.support.v4.view.MotionEventCompat.getPointerId(p42, v27);
                float v32 = android.support.v4.view.VelocityTrackerCompat.getXVelocity(this.mVelocityTracker, v18);
                float v34 = android.support.v4.view.VelocityTrackerCompat.getYVelocity(this.mVelocityTracker, v18);
                int v17_1 = 0;
                while (v17_1 < v6) {
                    if (v17_1 != v27) {
                        int v19 = android.support.v4.view.MotionEventCompat.getPointerId(p42, v17_1);
                        if (((v32 * android.support.v4.view.VelocityTrackerCompat.getXVelocity(this.mVelocityTracker, v19)) + (v34 * android.support.v4.view.VelocityTrackerCompat.getYVelocity(this.mVelocityTracker, v19))) < 0) {
                            this.mVelocityTracker.clear();
                            break;
                        }
                    }
                    v17_1++;
                }
                break;
        }
        return v16_0;
    }

    public void setIsLongpressEnabled(boolean p1)
    {
        this.mIsLongpressEnabled = p1;
        return;
    }

    public void setOnDoubleTapListener(android.view.GestureDetector$OnDoubleTapListener p1)
    {
        this.mDoubleTapListener = p1;
        return;
    }
}
