package android.support.v4.accessibilityservice;
interface AccessibilityServiceInfoCompat$AccessibilityServiceInfoVersionImpl {

    public abstract boolean getCanRetrieveWindowContent();

    public abstract int getCapabilities();

    public abstract String getDescription();

    public abstract String getId();

    public abstract android.content.pm.ResolveInfo getResolveInfo();

    public abstract String getSettingsActivityName();
}
