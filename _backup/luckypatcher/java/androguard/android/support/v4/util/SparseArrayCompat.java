package android.support.v4.util;
public class SparseArrayCompat {
    private static final Object DELETED;
    private boolean mGarbage;
    private int[] mKeys;
    private int mSize;
    private Object[] mValues;

    static SparseArrayCompat()
    {
        android.support.v4.util.SparseArrayCompat.DELETED = new Object();
        return;
    }

    public SparseArrayCompat()
    {
        this(10);
        return;
    }

    public SparseArrayCompat(int p3)
    {
        this.mGarbage = 0;
        int v3_1 = android.support.v4.util.SparseArrayCompat.idealIntArraySize(p3);
        Object[] v0_0 = new int[v3_1];
        this.mKeys = v0_0;
        Object[] v0_1 = new Object[v3_1];
        this.mValues = v0_1;
        this.mSize = 0;
        return;
    }

    private static int binarySearch(int[] p5, int p6, int p7, int p8)
    {
        int v1 = (p6 + p7);
        int v2 = (p6 - 1);
        while ((v1 - v2) > 1) {
            int v0 = ((v1 + v2) / 2);
            if (p5[v0] >= p8) {
                v1 = v0;
            } else {
                v2 = v0;
            }
        }
        if (v1 != (p6 + p7)) {
            if (p5[v1] != p8) {
                v1 ^= -1;
            }
        } else {
            v1 = ((p6 + p7) ^ -1);
        }
        return v1;
    }

    private void gc()
    {
        int v2 = this.mSize;
        int v3 = 0;
        int[] v1 = this.mKeys;
        Object[] v5 = this.mValues;
        int v0 = 0;
        while (v0 < v2) {
            Object v4 = v5[v0];
            if (v4 != android.support.v4.util.SparseArrayCompat.DELETED) {
                if (v0 != v3) {
                    v1[v3] = v1[v0];
                    v5[v3] = v4;
                }
                v3++;
            }
            v0++;
        }
        this.mGarbage = 0;
        this.mSize = v3;
        return;
    }

    static int idealByteArraySize(int p3)
    {
        int v0 = 4;
        while (v0 < 32) {
            if (p3 > ((1 << v0) - 12)) {
                v0++;
            } else {
                p3 = ((1 << v0) - 12);
                break;
            }
        }
        return p3;
    }

    static int idealIntArraySize(int p1)
    {
        return (android.support.v4.util.SparseArrayCompat.idealByteArraySize((p1 * 4)) / 4);
    }

    public void append(int p8, Object p9)
    {
        if ((this.mSize == 0) || (p8 > this.mKeys[(this.mSize - 1)])) {
            if ((this.mGarbage) && (this.mSize >= this.mKeys.length)) {
                this.gc();
            }
            int v3 = this.mSize;
            if (v3 >= this.mKeys.length) {
                int v0 = android.support.v4.util.SparseArrayCompat.idealIntArraySize((v3 + 1));
                int[] v1 = new int[v0];
                Object[] v2 = new Object[v0];
                System.arraycopy(this.mKeys, 0, v1, 0, this.mKeys.length);
                System.arraycopy(this.mValues, 0, v2, 0, this.mValues.length);
                this.mKeys = v1;
                this.mValues = v2;
            }
            this.mKeys[v3] = p8;
            this.mValues[v3] = p9;
            this.mSize = (v3 + 1);
        } else {
            this.put(p8, p9);
        }
        return;
    }

    public void clear()
    {
        int v1 = this.mSize;
        int v0 = 0;
        while (v0 < v1) {
            this.mValues[v0] = 0;
            v0++;
        }
        this.mSize = 0;
        this.mGarbage = 0;
        return;
    }

    public void delete(int p5)
    {
        int v0 = android.support.v4.util.SparseArrayCompat.binarySearch(this.mKeys, 0, this.mSize, p5);
        if ((v0 >= 0) && (this.mValues[v0] != android.support.v4.util.SparseArrayCompat.DELETED)) {
            this.mValues[v0] = android.support.v4.util.SparseArrayCompat.DELETED;
            this.mGarbage = 1;
        }
        return;
    }

    public Object get(int p2)
    {
        return this.get(p2, 0);
    }

    public Object get(int p5, Object p6)
    {
        int v0 = android.support.v4.util.SparseArrayCompat.binarySearch(this.mKeys, 0, this.mSize, p5);
        if ((v0 >= 0) && (this.mValues[v0] != android.support.v4.util.SparseArrayCompat.DELETED)) {
            p6 = this.mValues[v0];
        }
        return p6;
    }

    public int indexOfKey(int p4)
    {
        if (this.mGarbage) {
            this.gc();
        }
        return android.support.v4.util.SparseArrayCompat.binarySearch(this.mKeys, 0, this.mSize, p4);
    }

    public int indexOfValue(Object p3)
    {
        if (this.mGarbage) {
            this.gc();
        }
        int v0 = 0;
        while (v0 < this.mSize) {
            if (this.mValues[v0] != p3) {
                v0++;
            }
            return v0;
        }
        v0 = -1;
        return v0;
    }

    public int keyAt(int p2)
    {
        if (this.mGarbage) {
            this.gc();
        }
        return this.mKeys[p2];
    }

    public void put(int p9, Object p10)
    {
        int v0_0 = android.support.v4.util.SparseArrayCompat.binarySearch(this.mKeys, 0, this.mSize, p9);
        if (v0_0 < 0) {
            int v0_1 = (v0_0 ^ -1);
            if ((v0_1 >= this.mSize) || (this.mValues[v0_1] != android.support.v4.util.SparseArrayCompat.DELETED)) {
                if ((this.mGarbage) && (this.mSize >= this.mKeys.length)) {
                    this.gc();
                    v0_1 = (android.support.v4.util.SparseArrayCompat.binarySearch(this.mKeys, 0, this.mSize, p9) ^ -1);
                }
                if (this.mSize >= this.mKeys.length) {
                    int v1 = android.support.v4.util.SparseArrayCompat.idealIntArraySize((this.mSize + 1));
                    int[] v2 = new int[v1];
                    Object[] v3 = new Object[v1];
                    System.arraycopy(this.mKeys, 0, v2, 0, this.mKeys.length);
                    System.arraycopy(this.mValues, 0, v3, 0, this.mValues.length);
                    this.mKeys = v2;
                    this.mValues = v3;
                }
                if ((this.mSize - v0_1) != 0) {
                    System.arraycopy(this.mKeys, v0_1, this.mKeys, (v0_1 + 1), (this.mSize - v0_1));
                    System.arraycopy(this.mValues, v0_1, this.mValues, (v0_1 + 1), (this.mSize - v0_1));
                }
                this.mKeys[v0_1] = p9;
                this.mValues[v0_1] = p10;
                this.mSize = (this.mSize + 1);
            } else {
                this.mKeys[v0_1] = p9;
                this.mValues[v0_1] = p10;
            }
        } else {
            this.mValues[v0_0] = p10;
        }
        return;
    }

    public void remove(int p1)
    {
        this.delete(p1);
        return;
    }

    public void removeAt(int p3)
    {
        if (this.mValues[p3] != android.support.v4.util.SparseArrayCompat.DELETED) {
            this.mValues[p3] = android.support.v4.util.SparseArrayCompat.DELETED;
            this.mGarbage = 1;
        }
        return;
    }

    public void removeAtRange(int p5, int p6)
    {
        int v0 = Math.min(this.mSize, (p5 + p6));
        int v1 = p5;
        while (v1 < v0) {
            this.removeAt(v1);
            v1++;
        }
        return;
    }

    public void setValueAt(int p2, Object p3)
    {
        if (this.mGarbage) {
            this.gc();
        }
        this.mValues[p2] = p3;
        return;
    }

    public int size()
    {
        if (this.mGarbage) {
            this.gc();
        }
        return this.mSize;
    }

    public Object valueAt(int p2)
    {
        if (this.mGarbage) {
            this.gc();
        }
        return this.mValues[p2];
    }
}
