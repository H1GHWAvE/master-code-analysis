package android.support.v4.widget;
 class SearchViewCompat$SearchViewCompatStubImpl implements android.support.v4.widget.SearchViewCompat$SearchViewCompatImpl {

    SearchViewCompat$SearchViewCompatStubImpl()
    {
        return;
    }

    public CharSequence getQuery(android.view.View p2)
    {
        return 0;
    }

    public boolean isIconified(android.view.View p2)
    {
        return 1;
    }

    public boolean isQueryRefinementEnabled(android.view.View p2)
    {
        return 0;
    }

    public boolean isSubmitButtonEnabled(android.view.View p2)
    {
        return 0;
    }

    public Object newOnCloseListener(android.support.v4.widget.SearchViewCompat$OnCloseListenerCompat p2)
    {
        return 0;
    }

    public Object newOnQueryTextListener(android.support.v4.widget.SearchViewCompat$OnQueryTextListenerCompat p2)
    {
        return 0;
    }

    public android.view.View newSearchView(android.content.Context p2)
    {
        return 0;
    }

    public void setIconified(android.view.View p1, boolean p2)
    {
        return;
    }

    public void setImeOptions(android.view.View p1, int p2)
    {
        return;
    }

    public void setInputType(android.view.View p1, int p2)
    {
        return;
    }

    public void setMaxWidth(android.view.View p1, int p2)
    {
        return;
    }

    public void setOnCloseListener(Object p1, Object p2)
    {
        return;
    }

    public void setOnQueryTextListener(Object p1, Object p2)
    {
        return;
    }

    public void setQuery(android.view.View p1, CharSequence p2, boolean p3)
    {
        return;
    }

    public void setQueryHint(android.view.View p1, CharSequence p2)
    {
        return;
    }

    public void setQueryRefinementEnabled(android.view.View p1, boolean p2)
    {
        return;
    }

    public void setSearchableInfo(android.view.View p1, android.content.ComponentName p2)
    {
        return;
    }

    public void setSubmitButtonEnabled(android.view.View p1, boolean p2)
    {
        return;
    }
}
