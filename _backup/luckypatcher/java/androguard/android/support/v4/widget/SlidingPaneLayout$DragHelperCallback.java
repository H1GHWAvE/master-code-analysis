package android.support.v4.widget;
 class SlidingPaneLayout$DragHelperCallback extends android.support.v4.widget.ViewDragHelper$Callback {
    final synthetic android.support.v4.widget.SlidingPaneLayout this$0;

    private SlidingPaneLayout$DragHelperCallback(android.support.v4.widget.SlidingPaneLayout p1)
    {
        this.this$0 = p1;
        return;
    }

    synthetic SlidingPaneLayout$DragHelperCallback(android.support.v4.widget.SlidingPaneLayout p1, android.support.v4.widget.SlidingPaneLayout$1 p2)
    {
        this(p1);
        return;
    }

    public int clampViewPositionHorizontal(android.view.View p7, int p8, int p9)
    {
        int v0 = (this.this$0.getPaddingLeft() + ((android.support.v4.widget.SlidingPaneLayout$LayoutParams) android.support.v4.widget.SlidingPaneLayout.access$400(this.this$0).getLayoutParams()).leftMargin);
        return Math.min(Math.max(p8, v0), (v0 + android.support.v4.widget.SlidingPaneLayout.access$700(this.this$0)));
    }

    public int getViewHorizontalDragRange(android.view.View p2)
    {
        return android.support.v4.widget.SlidingPaneLayout.access$700(this.this$0);
    }

    public void onEdgeDragStarted(int p3, int p4)
    {
        android.support.v4.widget.SlidingPaneLayout.access$200(this.this$0).captureChildView(android.support.v4.widget.SlidingPaneLayout.access$400(this.this$0), p4);
        return;
    }

    public void onViewCaptured(android.view.View p2, int p3)
    {
        this.this$0.setAllChildrenVisible();
        return;
    }

    public void onViewDragStateChanged(int p3)
    {
        if (android.support.v4.widget.SlidingPaneLayout.access$200(this.this$0).getViewDragState() == 0) {
            if (android.support.v4.widget.SlidingPaneLayout.access$300(this.this$0) != 0) {
                this.this$0.dispatchOnPanelOpened(android.support.v4.widget.SlidingPaneLayout.access$400(this.this$0));
                android.support.v4.widget.SlidingPaneLayout.access$502(this.this$0, 1);
            } else {
                this.this$0.updateObscuredViewsVisibility(android.support.v4.widget.SlidingPaneLayout.access$400(this.this$0));
                this.this$0.dispatchOnPanelClosed(android.support.v4.widget.SlidingPaneLayout.access$400(this.this$0));
                android.support.v4.widget.SlidingPaneLayout.access$502(this.this$0, 0);
            }
        }
        return;
    }

    public void onViewPositionChanged(android.view.View p2, int p3, int p4, int p5, int p6)
    {
        android.support.v4.widget.SlidingPaneLayout.access$600(this.this$0, p3);
        this.this$0.invalidate();
        return;
    }

    public void onViewReleased(android.view.View p6, float p7, float p8)
    {
        int v0 = (this.this$0.getPaddingLeft() + ((android.support.v4.widget.SlidingPaneLayout$LayoutParams) p6.getLayoutParams()).leftMargin);
        if ((p7 > 0) || ((p7 == 0) && (android.support.v4.widget.SlidingPaneLayout.access$300(this.this$0) > 1056964608))) {
            v0 += android.support.v4.widget.SlidingPaneLayout.access$700(this.this$0);
        }
        android.support.v4.widget.SlidingPaneLayout.access$200(this.this$0).settleCapturedViewAt(v0, p6.getTop());
        this.this$0.invalidate();
        return;
    }

    public boolean tryCaptureView(android.view.View p2, int p3)
    {
        boolean v0_4;
        if (!android.support.v4.widget.SlidingPaneLayout.access$100(this.this$0)) {
            v0_4 = ((android.support.v4.widget.SlidingPaneLayout$LayoutParams) p2.getLayoutParams()).slideable;
        } else {
            v0_4 = 0;
        }
        return v0_4;
    }
}
