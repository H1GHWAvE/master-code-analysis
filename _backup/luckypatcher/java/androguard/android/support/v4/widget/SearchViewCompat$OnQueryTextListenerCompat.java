package android.support.v4.widget;
public abstract class SearchViewCompat$OnQueryTextListenerCompat {
    final Object mListener;

    public SearchViewCompat$OnQueryTextListenerCompat()
    {
        this.mListener = android.support.v4.widget.SearchViewCompat.access$000().newOnQueryTextListener(this);
        return;
    }

    public boolean onQueryTextChange(String p2)
    {
        return 0;
    }

    public boolean onQueryTextSubmit(String p2)
    {
        return 0;
    }
}
