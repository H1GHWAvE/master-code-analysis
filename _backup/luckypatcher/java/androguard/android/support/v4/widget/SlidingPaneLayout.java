package android.support.v4.widget;
public class SlidingPaneLayout extends android.view.ViewGroup {
    private static final int DEFAULT_FADE_COLOR = 3435973836;
    private static final int DEFAULT_OVERHANG_SIZE = 32;
    static final android.support.v4.widget.SlidingPaneLayout$SlidingPanelLayoutImpl IMPL = None;
    private static final int MIN_FLING_VELOCITY = 400;
    private static final String TAG = "SlidingPaneLayout";
    private boolean mCanSlide;
    private int mCoveredFadeColor;
    private final android.support.v4.widget.ViewDragHelper mDragHelper;
    private boolean mFirstLayout;
    private float mInitialMotionX;
    private float mInitialMotionY;
    private boolean mIsUnableToDrag;
    private final int mOverhangSize;
    private android.support.v4.widget.SlidingPaneLayout$PanelSlideListener mPanelSlideListener;
    private int mParallaxBy;
    private float mParallaxOffset;
    private final java.util.ArrayList mPostedRunnables;
    private boolean mPreservedOpenState;
    private android.graphics.drawable.Drawable mShadowDrawable;
    private float mSlideOffset;
    private int mSlideRange;
    private android.view.View mSlideableView;
    private int mSliderFadeColor;
    private final android.graphics.Rect mTmpRect;

    static SlidingPaneLayout()
    {
        int v0 = android.os.Build$VERSION.SDK_INT;
        if (v0 < 17) {
            if (v0 < 16) {
                android.support.v4.widget.SlidingPaneLayout.IMPL = new android.support.v4.widget.SlidingPaneLayout$SlidingPanelLayoutImplBase();
            } else {
                android.support.v4.widget.SlidingPaneLayout.IMPL = new android.support.v4.widget.SlidingPaneLayout$SlidingPanelLayoutImplJB();
            }
        } else {
            android.support.v4.widget.SlidingPaneLayout.IMPL = new android.support.v4.widget.SlidingPaneLayout$SlidingPanelLayoutImplJBMR1();
        }
        return;
    }

    public SlidingPaneLayout(android.content.Context p2)
    {
        this(p2, 0);
        return;
    }

    public SlidingPaneLayout(android.content.Context p2, android.util.AttributeSet p3)
    {
        this(p2, p3, 0);
        return;
    }

    public SlidingPaneLayout(android.content.Context p7, android.util.AttributeSet p8, int p9)
    {
        this(p7, p8, p9);
        this.mSliderFadeColor = -858993460;
        this.mFirstLayout = 1;
        this.mTmpRect = new android.graphics.Rect();
        this.mPostedRunnables = new java.util.ArrayList();
        float v0 = p7.getResources().getDisplayMetrics().density;
        this.mOverhangSize = ((int) ((1107296256 * v0) + 1056964608));
        android.view.ViewConfiguration.get(p7);
        this.setWillNotDraw(0);
        android.support.v4.view.ViewCompat.setAccessibilityDelegate(this, new android.support.v4.widget.SlidingPaneLayout$AccessibilityDelegate(this));
        android.support.v4.view.ViewCompat.setImportantForAccessibility(this, 1);
        this.mDragHelper = android.support.v4.widget.ViewDragHelper.create(this, 1056964608, new android.support.v4.widget.SlidingPaneLayout$DragHelperCallback(this, 0));
        this.mDragHelper.setEdgeTrackingEnabled(1);
        this.mDragHelper.setMinVelocity((1137180672 * v0));
        return;
    }

    static synthetic boolean access$100(android.support.v4.widget.SlidingPaneLayout p1)
    {
        return p1.mIsUnableToDrag;
    }

    static synthetic java.util.ArrayList access$1000(android.support.v4.widget.SlidingPaneLayout p1)
    {
        return p1.mPostedRunnables;
    }

    static synthetic android.support.v4.widget.ViewDragHelper access$200(android.support.v4.widget.SlidingPaneLayout p1)
    {
        return p1.mDragHelper;
    }

    static synthetic float access$300(android.support.v4.widget.SlidingPaneLayout p1)
    {
        return p1.mSlideOffset;
    }

    static synthetic android.view.View access$400(android.support.v4.widget.SlidingPaneLayout p1)
    {
        return p1.mSlideableView;
    }

    static synthetic boolean access$502(android.support.v4.widget.SlidingPaneLayout p0, boolean p1)
    {
        p0.mPreservedOpenState = p1;
        return p1;
    }

    static synthetic void access$600(android.support.v4.widget.SlidingPaneLayout p0, int p1)
    {
        p0.onPanelDragged(p1);
        return;
    }

    static synthetic int access$700(android.support.v4.widget.SlidingPaneLayout p1)
    {
        return p1.mSlideRange;
    }

    static synthetic void access$900(android.support.v4.widget.SlidingPaneLayout p0, android.view.View p1)
    {
        p0.invalidateChildRegion(p1);
        return;
    }

    private boolean closePane(android.view.View p3, int p4)
    {
        int v0 = 0;
        if ((this.mFirstLayout) || (this.smoothSlideTo(0, p4))) {
            this.mPreservedOpenState = 0;
            v0 = 1;
        }
        return v0;
    }

    private void dimChildView(android.view.View p10, float p11, int p12)
    {
        android.support.v4.widget.SlidingPaneLayout$LayoutParams v4_1 = ((android.support.v4.widget.SlidingPaneLayout$LayoutParams) p10.getLayoutParams());
        if ((p11 <= 0) || (p12 == 0)) {
            if (android.support.v4.view.ViewCompat.getLayerType(p10) != 0) {
                if (v4_1.dimPaint != null) {
                    v4_1.dimPaint.setColorFilter(0);
                }
                android.support.v4.widget.SlidingPaneLayout$DisableLayerRunnable v2_1 = new android.support.v4.widget.SlidingPaneLayout$DisableLayerRunnable(this, p10);
                this.mPostedRunnables.add(v2_1);
                android.support.v4.view.ViewCompat.postOnAnimation(this, v2_1);
            }
        } else {
            int v1 = ((((int) (((float) ((-16777216 & p12) >> 24)) * p11)) << 24) | (16777215 & p12));
            if (v4_1.dimPaint == null) {
                v4_1.dimPaint = new android.graphics.Paint();
            }
            v4_1.dimPaint.setColorFilter(new android.graphics.PorterDuffColorFilter(v1, android.graphics.PorterDuff$Mode.SRC_OVER));
            if (android.support.v4.view.ViewCompat.getLayerType(p10) != 2) {
                android.support.v4.view.ViewCompat.setLayerType(p10, 2, v4_1.dimPaint);
            }
            this.invalidateChildRegion(p10);
        }
        return;
    }

    private void invalidateChildRegion(android.view.View p2)
    {
        android.support.v4.widget.SlidingPaneLayout.IMPL.invalidateChildRegion(this, p2);
        return;
    }

    private void onPanelDragged(int p6)
    {
        android.support.v4.widget.SlidingPaneLayout$LayoutParams v1_1 = ((android.support.v4.widget.SlidingPaneLayout$LayoutParams) this.mSlideableView.getLayoutParams());
        this.mSlideOffset = (((float) (p6 - (this.getPaddingLeft() + v1_1.leftMargin))) / ((float) this.mSlideRange));
        if (this.mParallaxBy != 0) {
            this.parallaxOtherViews(this.mSlideOffset);
        }
        if (v1_1.dimWhenOffset) {
            this.dimChildView(this.mSlideableView, this.mSlideOffset, this.mSliderFadeColor);
        }
        this.dispatchOnPanelSlide(this.mSlideableView);
        return;
    }

    private boolean openPane(android.view.View p3, int p4)
    {
        int v0 = 1;
        if ((!this.mFirstLayout) && (!this.smoothSlideTo(1065353216, p4))) {
            v0 = 0;
        } else {
            this.mPreservedOpenState = 1;
        }
        return v0;
    }

    private void parallaxOtherViews(float p12)
    {
        int v1;
        android.support.v4.widget.SlidingPaneLayout$LayoutParams v6_1 = ((android.support.v4.widget.SlidingPaneLayout$LayoutParams) this.mSlideableView.getLayoutParams());
        if ((!v6_1.dimWhenOffset) || (v6_1.leftMargin > 0)) {
            v1 = 0;
        } else {
            v1 = 1;
        }
        int v0 = this.getChildCount();
        int v3 = 0;
        while (v3 < v0) {
            android.view.View v7 = this.getChildAt(v3);
            if (v7 != this.mSlideableView) {
                int v5 = ((int) ((1065353216 - this.mParallaxOffset) * ((float) this.mParallaxBy)));
                this.mParallaxOffset = p12;
                v7.offsetLeftAndRight((v5 - ((int) ((1065353216 - p12) * ((float) this.mParallaxBy)))));
                if (v1 != 0) {
                    this.dimChildView(v7, (1065353216 - this.mParallaxOffset), this.mCoveredFadeColor);
                }
            }
            v3++;
        }
        return;
    }

    private static boolean viewIsOpaque(android.view.View p5)
    {
        int v1 = 1;
        if (!android.support.v4.view.ViewCompat.isOpaque(p5)) {
            if (android.os.Build$VERSION.SDK_INT < 18) {
                android.graphics.drawable.Drawable v0 = p5.getBackground();
                if (v0 == null) {
                    v1 = 0;
                } else {
                    if (v0.getOpacity() != -1) {
                        v1 = 0;
                    }
                }
            } else {
                v1 = 0;
            }
        }
        return v1;
    }

    protected boolean canScroll(android.view.View p12, boolean p13, int p14, int p15, int p16)
    {
        int v0_3;
        if (!(p12 instanceof android.view.ViewGroup)) {
            if ((!p13) || (!android.support.v4.view.ViewCompat.canScrollHorizontally(p12, (- p14)))) {
                v0_3 = 0;
            } else {
                v0_3 = 1;
            }
        } else {
            int v9 = p12.getScrollX();
            int v10 = p12.getScrollY();
            int v8 = (((android.view.ViewGroup) p12).getChildCount() - 1);
            while (v8 >= 0) {
                android.view.View v1 = ((android.view.ViewGroup) p12).getChildAt(v8);
                if (((p15 + v9) < v1.getLeft()) || (((p15 + v9) >= v1.getRight()) || (((p16 + v10) < v1.getTop()) || (((p16 + v10) >= v1.getBottom()) || (!this.canScroll(v1, 1, p14, ((p15 + v9) - v1.getLeft()), ((p16 + v10) - v1.getTop()))))))) {
                    v8--;
                } else {
                    v0_3 = 1;
                }
            }
        }
        return v0_3;
    }

    public boolean canSlide()
    {
        return this.mCanSlide;
    }

    protected boolean checkLayoutParams(android.view.ViewGroup$LayoutParams p2)
    {
        if ((!(p2 instanceof android.support.v4.widget.SlidingPaneLayout$LayoutParams)) || (!super.checkLayoutParams(p2))) {
            int v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public boolean closePane()
    {
        return this.closePane(this.mSlideableView, 0);
    }

    public void computeScroll()
    {
        if (this.mDragHelper.continueSettling(1)) {
            if (this.mCanSlide) {
                android.support.v4.view.ViewCompat.postInvalidateOnAnimation(this);
            } else {
                this.mDragHelper.abort();
            }
        }
        return;
    }

    void dispatchOnPanelClosed(android.view.View p2)
    {
        if (this.mPanelSlideListener != null) {
            this.mPanelSlideListener.onPanelClosed(p2);
        }
        this.sendAccessibilityEvent(32);
        return;
    }

    void dispatchOnPanelOpened(android.view.View p2)
    {
        if (this.mPanelSlideListener != null) {
            this.mPanelSlideListener.onPanelOpened(p2);
        }
        this.sendAccessibilityEvent(32);
        return;
    }

    void dispatchOnPanelSlide(android.view.View p3)
    {
        if (this.mPanelSlideListener != null) {
            this.mPanelSlideListener.onPanelSlide(p3, this.mSlideOffset);
        }
        return;
    }

    public void draw(android.graphics.Canvas p9)
    {
        int v3;
        super.draw(p9);
        if (this.getChildCount() <= 1) {
            v3 = 0;
        } else {
            v3 = this.getChildAt(1);
        }
        if ((v3 != 0) && (this.mShadowDrawable != null)) {
            int v4 = this.mShadowDrawable.getIntrinsicWidth();
            int v2 = v3.getLeft();
            this.mShadowDrawable.setBounds((v2 - v4), v3.getTop(), v2, v3.getBottom());
            this.mShadowDrawable.draw(p9);
        }
        return;
    }

    protected boolean drawChild(android.graphics.Canvas p8, android.view.View p9, long p10)
    {
        android.support.v4.widget.SlidingPaneLayout$LayoutParams v1_1 = ((android.support.v4.widget.SlidingPaneLayout$LayoutParams) p9.getLayoutParams());
        int v3 = p8.save(2);
        if ((this.mCanSlide) && ((!v1_1.slideable) && (this.mSlideableView != null))) {
            p8.getClipBounds(this.mTmpRect);
            this.mTmpRect.right = Math.min(this.mTmpRect.right, this.mSlideableView.getLeft());
            p8.clipRect(this.mTmpRect);
        }
        boolean v2;
        if (android.os.Build$VERSION.SDK_INT < 11) {
            if ((!v1_1.dimWhenOffset) || (this.mSlideOffset <= 0)) {
                if (p9.isDrawingCacheEnabled()) {
                    p9.setDrawingCacheEnabled(0);
                }
                v2 = super.drawChild(p8, p9, p10);
            } else {
                if (!p9.isDrawingCacheEnabled()) {
                    p9.setDrawingCacheEnabled(1);
                }
                android.graphics.Bitmap v0 = p9.getDrawingCache();
                if (v0 == null) {
                    android.util.Log.e("SlidingPaneLayout", new StringBuilder().append("drawChild: child view ").append(p9).append(" returned null drawing cache").toString());
                    v2 = super.drawChild(p8, p9, p10);
                } else {
                    p8.drawBitmap(v0, ((float) p9.getLeft()), ((float) p9.getTop()), v1_1.dimPaint);
                    v2 = 0;
                }
            }
        } else {
            v2 = super.drawChild(p8, p9, p10);
        }
        p8.restoreToCount(v3);
        return v2;
    }

    protected android.view.ViewGroup$LayoutParams generateDefaultLayoutParams()
    {
        return new android.support.v4.widget.SlidingPaneLayout$LayoutParams();
    }

    public android.view.ViewGroup$LayoutParams generateLayoutParams(android.util.AttributeSet p3)
    {
        return new android.support.v4.widget.SlidingPaneLayout$LayoutParams(this.getContext(), p3);
    }

    protected android.view.ViewGroup$LayoutParams generateLayoutParams(android.view.ViewGroup$LayoutParams p2)
    {
        android.support.v4.widget.SlidingPaneLayout$LayoutParams v0_2;
        if (!(p2 instanceof android.view.ViewGroup$MarginLayoutParams)) {
            v0_2 = new android.support.v4.widget.SlidingPaneLayout$LayoutParams(p2);
        } else {
            v0_2 = new android.support.v4.widget.SlidingPaneLayout$LayoutParams(((android.view.ViewGroup$MarginLayoutParams) p2));
        }
        return v0_2;
    }

    public int getCoveredFadeColor()
    {
        return this.mCoveredFadeColor;
    }

    public int getParallaxDistance()
    {
        return this.mParallaxBy;
    }

    public int getSliderFadeColor()
    {
        return this.mSliderFadeColor;
    }

    boolean isDimmed(android.view.View p5)
    {
        int v1 = 0;
        if ((p5 != null) && ((this.mCanSlide) && ((((android.support.v4.widget.SlidingPaneLayout$LayoutParams) p5.getLayoutParams()).dimWhenOffset) && (this.mSlideOffset > 0)))) {
            v1 = 1;
        }
        return v1;
    }

    public boolean isOpen()
    {
        if ((this.mCanSlide) && (this.mSlideOffset != 1065353216)) {
            int v0_3 = 0;
        } else {
            v0_3 = 1;
        }
        return v0_3;
    }

    public boolean isSlideable()
    {
        return this.mCanSlide;
    }

    protected void onAttachedToWindow()
    {
        super.onAttachedToWindow();
        this.mFirstLayout = 1;
        return;
    }

    protected void onDetachedFromWindow()
    {
        super.onDetachedFromWindow();
        this.mFirstLayout = 1;
        int v2 = 0;
        int v0 = this.mPostedRunnables.size();
        while (v2 < v0) {
            ((android.support.v4.widget.SlidingPaneLayout$DisableLayerRunnable) this.mPostedRunnables.get(v2)).run();
            v2++;
        }
        this.mPostedRunnables.clear();
        return;
    }

    public boolean onInterceptTouchEvent(android.view.MotionEvent p14)
    {
        int v0 = android.support.v4.view.MotionEventCompat.getActionMasked(p14);
        if ((!this.mCanSlide) && ((v0 == 0) && (this.getChildCount() > 1))) {
            android.view.View v5 = this.getChildAt(1);
            if (v5 != null) {
                int v9_5;
                if (this.mDragHelper.isViewUnder(v5, ((int) p14.getX()), ((int) p14.getY()))) {
                    v9_5 = 0;
                } else {
                    v9_5 = 1;
                }
                this.mPreservedOpenState = v9_5;
            }
        }
        if ((this.mCanSlide) && ((!this.mIsUnableToDrag) || (v0 == 0))) {
            if ((v0 != 3) && (v0 != 1)) {
                int v4 = 0;
                switch (v0) {
                    case 0:
                        this.mIsUnableToDrag = 0;
                        float v7_1 = p14.getX();
                        float v8_1 = p14.getY();
                        this.mInitialMotionX = v7_1;
                        this.mInitialMotionY = v8_1;
                        if ((this.mDragHelper.isViewUnder(this.mSlideableView, ((int) v7_1), ((int) v8_1))) && (this.isDimmed(this.mSlideableView))) {
                            v4 = 1;
                        } else {
                        }
                    case 1:
                        break;
                    case 2:
                        float v7_0 = p14.getX();
                        float v8_0 = p14.getY();
                        float v1 = Math.abs((v7_0 - this.mInitialMotionX));
                        float v2 = Math.abs((v8_0 - this.mInitialMotionY));
                        if ((v1 <= ((float) this.mDragHelper.getTouchSlop())) || (v2 <= v1)) {
                        } else {
                            this.mDragHelper.cancel();
                            this.mIsUnableToDrag = 1;
                            int v9_20 = 0;
                        }
                        break;
                    default:
                        if (this.mDragHelper.shouldInterceptTouchEvent(p14)) {
                            v9_20 = 1;
                        } else {
                            if (v4 == 0) {
                                v9_20 = 0;
                            } else {
                            }
                        }
                }
                if ((!this.mDragHelper.shouldInterceptTouchEvent(p14)) && (v4 == 0)) {
                }
            } else {
                this.mDragHelper.cancel();
                v9_20 = 0;
            }
        } else {
            this.mDragHelper.cancel();
            v9_20 = super.onInterceptTouchEvent(p14);
        }
        return v9_20;
    }

    protected void onLayout(boolean p26, int p27, int p28, int p29, int p30)
    {
        int v20 = (p29 - p27);
        int v16 = this.getPaddingLeft();
        int v17 = this.getPaddingRight();
        int v18 = this.getPaddingTop();
        int v6 = this.getChildCount();
        int v21 = v16;
        int v14 = v21;
        if (this.mFirstLayout) {
            if ((!this.mCanSlide) || (!this.mPreservedOpenState)) {
                android.view.View v22_3 = 0;
            } else {
                v22_3 = 1065353216;
            }
            this.mSlideOffset = v22_3;
        }
        int v11_0 = 0;
        while (v11_0 < v6) {
            android.view.View v4 = this.getChildAt(v11_0);
            if (v4.getVisibility() != 8) {
                android.support.v4.widget.SlidingPaneLayout$LayoutParams v12_1 = ((android.support.v4.widget.SlidingPaneLayout$LayoutParams) v4.getLayoutParams());
                int v10 = v4.getMeasuredWidth();
                int v15 = 0;
                if (!v12_1.slideable) {
                    if ((!this.mCanSlide) || (this.mParallaxBy == 0)) {
                        v21 = v14;
                    } else {
                        v15 = ((int) ((1065353216 - this.mSlideOffset) * ((float) this.mParallaxBy)));
                        v21 = v14;
                    }
                } else {
                    android.view.View v22_32;
                    int v19 = ((Math.min(v14, ((v20 - v17) - this.mOverhangSize)) - v21) - (v12_1.leftMargin + v12_1.rightMargin));
                    this.mSlideRange = v19;
                    if ((((v12_1.leftMargin + v21) + v19) + (v10 / 2)) <= (v20 - v17)) {
                        v22_32 = 0;
                    } else {
                        v22_32 = 1;
                    }
                    v12_1.dimWhenOffset = v22_32;
                    v21 += (((int) (((float) v19) * this.mSlideOffset)) + v12_1.leftMargin);
                }
                int v7 = (v21 - v15);
                v4.layout(v7, v18, (v7 + v10), (v18 + v4.getMeasuredHeight()));
                v14 += v4.getWidth();
            }
            v11_0++;
        }
        if (this.mFirstLayout) {
            if (!this.mCanSlide) {
                int v11_1 = 0;
                while (v11_1 < v6) {
                    this.dimChildView(this.getChildAt(v11_1), 0, this.mSliderFadeColor);
                    v11_1++;
                }
            } else {
                if (this.mParallaxBy != 0) {
                    this.parallaxOtherViews(this.mSlideOffset);
                }
                if (((android.support.v4.widget.SlidingPaneLayout$LayoutParams) this.mSlideableView.getLayoutParams()).dimWhenOffset) {
                    this.dimChildView(this.mSlideableView, this.mSlideOffset, this.mSliderFadeColor);
                }
            }
            this.updateObscuredViewsVisibility(this.mSlideableView);
        }
        this.mFirstLayout = 0;
        return;
    }

    protected void onMeasure(int p29, int p30)
    {
        int v22 = android.view.View$MeasureSpec.getMode(p29);
        int v24 = android.view.View$MeasureSpec.getSize(p29);
        int v11 = android.view.View$MeasureSpec.getMode(p30);
        int v12 = android.view.View$MeasureSpec.getSize(p30);
        if (v22 == 1073741824) {
            if (v11 == 0) {
                if (!this.isInEditMode()) {
                    throw new IllegalStateException("Height must not be UNSPECIFIED");
                } else {
                    if (v11 == 0) {
                        v11 = -2147483648;
                        v12 = 300;
                    }
                }
            }
        } else {
            if (!this.isInEditMode()) {
                throw new IllegalStateException("Width must have an exact value or MATCH_PARENT");
            } else {
                if ((v22 != -2147483648) && (v22 == 0)) {
                    v24 = 300;
                }
            }
        }
        int v15 = 0;
        int v17 = -1;
        switch (v11) {
            case -2147483648:
                v17 = ((v12 - this.getPaddingTop()) - this.getPaddingBottom());
                break;
            case 1073741824:
                v17 = ((v12 - this.getPaddingTop()) - this.getPaddingBottom());
                v15 = v17;
                break;
        }
        float v21 = 0;
        int v3 = 0;
        int v23 = ((v24 - this.getPaddingLeft()) - this.getPaddingRight());
        int v5 = this.getChildCount();
        if (v5 > 2) {
            android.util.Log.e("SlidingPaneLayout", "onMeasure: More than two child views are not supported.");
        }
        this.mSlideableView = 0;
        int v14_0 = 0;
        while (v14_0 < v5) {
            android.view.View v4_1 = this.getChildAt(v14_0);
            android.support.v4.widget.SlidingPaneLayout$LayoutParams v16_3 = ((android.support.v4.widget.SlidingPaneLayout$LayoutParams) v4_1.getLayoutParams());
            if (v4_1.getVisibility() != 8) {
                if (v16_3.weight > 0) {
                    v21 += v16_3.weight;
                    if (v16_3.width == 0) {
                        v14_0++;
                    }
                }
                int v9_3;
                int v13_1 = (v16_3.leftMargin + v16_3.rightMargin);
                if (v16_3.width != -2) {
                    if (v16_3.width != -1) {
                        v9_3 = android.view.View$MeasureSpec.makeMeasureSpec(v16_3.width, 1073741824);
                    } else {
                        v9_3 = android.view.View$MeasureSpec.makeMeasureSpec((v24 - v13_1), 1073741824);
                    }
                } else {
                    v9_3 = android.view.View$MeasureSpec.makeMeasureSpec((v24 - v13_1), -2147483648);
                }
                int v7_2;
                if (v16_3.height != -2) {
                    if (v16_3.height != -1) {
                        v7_2 = android.view.View$MeasureSpec.makeMeasureSpec(v16_3.height, 1073741824);
                    } else {
                        v7_2 = android.view.View$MeasureSpec.makeMeasureSpec(v17, 1073741824);
                    }
                } else {
                    v7_2 = android.view.View$MeasureSpec.makeMeasureSpec(v17, -2147483648);
                }
                v4_1.measure(v9_3, v7_2);
                int v8 = v4_1.getMeasuredWidth();
                int v6 = v4_1.getMeasuredHeight();
                if ((v11 == -2147483648) && (v6 > v15)) {
                    v15 = Math.min(v6, v17);
                }
                int v26_72;
                v23 -= v8;
                if (v23 >= 0) {
                    v26_72 = 0;
                } else {
                    v26_72 = 1;
                }
                v16_3.slideable = v26_72;
                v3 |= v26_72;
                if (v16_3.slideable) {
                    this.mSlideableView = v4_1;
                }
            } else {
                v16_3.dimWhenOffset = 0;
            }
        }
        if ((v3 != 0) || (v21 > 0)) {
            int v10 = (v24 - this.mOverhangSize);
            int v14_1 = 0;
            while (v14_1 < v5) {
                android.view.View v4_0 = this.getChildAt(v14_1);
                if (v4_0.getVisibility() != 8) {
                    android.support.v4.widget.SlidingPaneLayout$LayoutParams v16_1 = ((android.support.v4.widget.SlidingPaneLayout$LayoutParams) v4_0.getLayoutParams());
                    if (v4_0.getVisibility() != 8) {
                        if ((v16_1.width != 0) || (v16_1.weight <= 0)) {
                            int v20 = 0;
                        } else {
                            v20 = 1;
                        }
                        int v18;
                        if (v20 == 0) {
                            v18 = v4_0.getMeasuredWidth();
                        } else {
                            v18 = 0;
                        }
                        if ((v3 == 0) || (v4_0 == this.mSlideableView)) {
                            if (v16_1.weight > 0) {
                                int v7_0;
                                if (v16_1.width != 0) {
                                    v7_0 = android.view.View$MeasureSpec.makeMeasureSpec(v4_0.getMeasuredHeight(), 1073741824);
                                } else {
                                    if (v16_1.height != -2) {
                                        if (v16_1.height != -1) {
                                            v7_0 = android.view.View$MeasureSpec.makeMeasureSpec(v16_1.height, 1073741824);
                                        } else {
                                            v7_0 = android.view.View$MeasureSpec.makeMeasureSpec(v17, 1073741824);
                                        }
                                    } else {
                                        v7_0 = android.view.View$MeasureSpec.makeMeasureSpec(v17, -2147483648);
                                    }
                                }
                                if (v3 == 0) {
                                    v4_0.measure(android.view.View$MeasureSpec.makeMeasureSpec((v18 + ((int) ((v16_1.weight * ((float) Math.max(0, v23))) / v21))), 1073741824), v7_0);
                                } else {
                                    int v19 = (v24 - (v16_1.leftMargin + v16_1.rightMargin));
                                    int v9_1 = android.view.View$MeasureSpec.makeMeasureSpec(v19, 1073741824);
                                    if (v18 != v19) {
                                        v4_0.measure(v9_1, v7_0);
                                    }
                                }
                            }
                        } else {
                            if ((v16_1.width < 0) && ((v18 > v10) || (v16_1.weight > 0))) {
                                int v7_1;
                                if (v20 == 0) {
                                    v7_1 = android.view.View$MeasureSpec.makeMeasureSpec(v4_0.getMeasuredHeight(), 1073741824);
                                } else {
                                    if (v16_1.height != -2) {
                                        if (v16_1.height != -1) {
                                            v7_1 = android.view.View$MeasureSpec.makeMeasureSpec(v16_1.height, 1073741824);
                                        } else {
                                            v7_1 = android.view.View$MeasureSpec.makeMeasureSpec(v17, 1073741824);
                                        }
                                    } else {
                                        v7_1 = android.view.View$MeasureSpec.makeMeasureSpec(v17, -2147483648);
                                    }
                                }
                                v4_0.measure(android.view.View$MeasureSpec.makeMeasureSpec(v10, 1073741824), v7_1);
                            }
                        }
                    }
                }
                v14_1++;
            }
        }
        this.setMeasuredDimension(v24, v15);
        this.mCanSlide = v3;
        if ((this.mDragHelper.getViewDragState() != 0) && (v3 == 0)) {
            this.mDragHelper.abort();
        }
        return;
    }

    protected void onRestoreInstanceState(android.os.Parcelable p3)
    {
        super.onRestoreInstanceState(((android.support.v4.widget.SlidingPaneLayout$SavedState) p3).getSuperState());
        if (!((android.support.v4.widget.SlidingPaneLayout$SavedState) p3).isOpen) {
            this.closePane();
        } else {
            this.openPane();
        }
        this.mPreservedOpenState = ((android.support.v4.widget.SlidingPaneLayout$SavedState) p3).isOpen;
        return;
    }

    protected android.os.Parcelable onSaveInstanceState()
    {
        boolean v2_1;
        android.support.v4.widget.SlidingPaneLayout$SavedState v0_1 = new android.support.v4.widget.SlidingPaneLayout$SavedState(super.onSaveInstanceState());
        if (!this.isSlideable()) {
            v2_1 = this.mPreservedOpenState;
        } else {
            v2_1 = this.isOpen();
        }
        v0_1.isOpen = v2_1;
        return v0_1;
    }

    protected void onSizeChanged(int p2, int p3, int p4, int p5)
    {
        super.onSizeChanged(p2, p3, p4, p5);
        if (p2 != p4) {
            this.mFirstLayout = 1;
        }
        return;
    }

    public boolean onTouchEvent(android.view.MotionEvent p12)
    {
        int v4;
        if (this.mCanSlide) {
            this.mDragHelper.processTouchEvent(p12);
            v4 = 1;
            switch ((p12.getAction() & 255)) {
                case 0:
                    float v5_1 = p12.getX();
                    float v6_1 = p12.getY();
                    this.mInitialMotionX = v5_1;
                    this.mInitialMotionY = v6_1;
                    break;
                case 1:
                    if (this.isDimmed(this.mSlideableView)) {
                        float v5_0 = p12.getX();
                        float v6_0 = p12.getY();
                        if (((((v5_0 - this.mInitialMotionX) * (v5_0 - this.mInitialMotionX)) + ((v6_0 - this.mInitialMotionY) * (v6_0 - this.mInitialMotionY))) < ((float) (this.mDragHelper.getTouchSlop() * this.mDragHelper.getTouchSlop()))) && (this.mDragHelper.isViewUnder(this.mSlideableView, ((int) v5_0), ((int) v6_0)))) {
                            this.closePane(this.mSlideableView, 0);
                        }
                    }
                    break;
                default:
            }
        } else {
            v4 = super.onTouchEvent(p12);
        }
        return v4;
    }

    public boolean openPane()
    {
        return this.openPane(this.mSlideableView, 0);
    }

    public void requestChildFocus(android.view.View p2, android.view.View p3)
    {
        super.requestChildFocus(p2, p3);
        if ((!this.isInTouchMode()) && (!this.mCanSlide)) {
            int v0_3;
            if (p2 != this.mSlideableView) {
                v0_3 = 0;
            } else {
                v0_3 = 1;
            }
            this.mPreservedOpenState = v0_3;
        }
        return;
    }

    void setAllChildrenVisible()
    {
        int v2 = 0;
        int v1 = this.getChildCount();
        while (v2 < v1) {
            android.view.View v0 = this.getChildAt(v2);
            if (v0.getVisibility() == 4) {
                v0.setVisibility(0);
            }
            v2++;
        }
        return;
    }

    public void setCoveredFadeColor(int p1)
    {
        this.mCoveredFadeColor = p1;
        return;
    }

    public void setPanelSlideListener(android.support.v4.widget.SlidingPaneLayout$PanelSlideListener p1)
    {
        this.mPanelSlideListener = p1;
        return;
    }

    public void setParallaxDistance(int p1)
    {
        this.mParallaxBy = p1;
        this.requestLayout();
        return;
    }

    public void setShadowDrawable(android.graphics.drawable.Drawable p1)
    {
        this.mShadowDrawable = p1;
        return;
    }

    public void setShadowResource(int p2)
    {
        this.setShadowDrawable(this.getResources().getDrawable(p2));
        return;
    }

    public void setSliderFadeColor(int p1)
    {
        this.mSliderFadeColor = p1;
        return;
    }

    public void smoothSlideClosed()
    {
        this.closePane();
        return;
    }

    public void smoothSlideOpen()
    {
        this.openPane();
        return;
    }

    boolean smoothSlideTo(float p8, int p9)
    {
        int v3 = 0;
        if ((this.mCanSlide) && (this.mDragHelper.smoothSlideViewTo(this.mSlideableView, ((int) (((float) (this.getPaddingLeft() + ((android.support.v4.widget.SlidingPaneLayout$LayoutParams) this.mSlideableView.getLayoutParams()).leftMargin)) + (((float) this.mSlideRange) * p8))), this.mSlideableView.getTop()))) {
            this.setAllChildrenVisible();
            android.support.v4.view.ViewCompat.postInvalidateOnAnimation(this);
            v3 = 1;
        }
        return v3;
    }

    void updateObscuredViewsVisibility(android.view.View p20)
    {
        int v10;
        int v14;
        int v12;
        int v1;
        int v11 = this.getPaddingLeft();
        int v13 = (this.getWidth() - this.getPaddingRight());
        int v15 = this.getPaddingTop();
        int v2 = (this.getHeight() - this.getPaddingBottom());
        if ((p20 == null) || (!android.support.v4.widget.SlidingPaneLayout.viewIsOpaque(p20))) {
            v1 = 0;
            v14 = 0;
            v12 = 0;
            v10 = 0;
        } else {
            v10 = p20.getLeft();
            v12 = p20.getRight();
            v14 = p20.getTop();
            v1 = p20.getBottom();
        }
        int v9 = 0;
        int v4 = this.getChildCount();
        while (v9 < v4) {
            android.view.View v3 = this.getChildAt(v9);
            if (v3 == p20) {
                break;
            }
            int v16;
            int v6 = Math.max(v11, v3.getLeft());
            int v8 = Math.max(v15, v3.getTop());
            int v7 = Math.min(v13, v3.getRight());
            int v5 = Math.min(v2, v3.getBottom());
            if ((v6 < v10) || ((v8 < v14) || ((v7 > v12) || (v5 > v1)))) {
                v16 = 0;
            } else {
                v16 = 4;
            }
            v3.setVisibility(v16);
            v9++;
        }
        return;
    }
}
