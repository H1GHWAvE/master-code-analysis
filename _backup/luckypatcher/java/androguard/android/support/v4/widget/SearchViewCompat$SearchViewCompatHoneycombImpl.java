package android.support.v4.widget;
 class SearchViewCompat$SearchViewCompatHoneycombImpl extends android.support.v4.widget.SearchViewCompat$SearchViewCompatStubImpl {

    SearchViewCompat$SearchViewCompatHoneycombImpl()
    {
        return;
    }

    public CharSequence getQuery(android.view.View p2)
    {
        return android.support.v4.widget.SearchViewCompatHoneycomb.getQuery(p2);
    }

    public boolean isIconified(android.view.View p2)
    {
        return android.support.v4.widget.SearchViewCompatHoneycomb.isIconified(p2);
    }

    public boolean isQueryRefinementEnabled(android.view.View p2)
    {
        return android.support.v4.widget.SearchViewCompatHoneycomb.isQueryRefinementEnabled(p2);
    }

    public boolean isSubmitButtonEnabled(android.view.View p2)
    {
        return android.support.v4.widget.SearchViewCompatHoneycomb.isSubmitButtonEnabled(p2);
    }

    public Object newOnCloseListener(android.support.v4.widget.SearchViewCompat$OnCloseListenerCompat p2)
    {
        return android.support.v4.widget.SearchViewCompatHoneycomb.newOnCloseListener(new android.support.v4.widget.SearchViewCompat$SearchViewCompatHoneycombImpl$2(this, p2));
    }

    public Object newOnQueryTextListener(android.support.v4.widget.SearchViewCompat$OnQueryTextListenerCompat p2)
    {
        return android.support.v4.widget.SearchViewCompatHoneycomb.newOnQueryTextListener(new android.support.v4.widget.SearchViewCompat$SearchViewCompatHoneycombImpl$1(this, p2));
    }

    public android.view.View newSearchView(android.content.Context p2)
    {
        return android.support.v4.widget.SearchViewCompatHoneycomb.newSearchView(p2);
    }

    public void setIconified(android.view.View p1, boolean p2)
    {
        android.support.v4.widget.SearchViewCompatHoneycomb.setIconified(p1, p2);
        return;
    }

    public void setMaxWidth(android.view.View p1, int p2)
    {
        android.support.v4.widget.SearchViewCompatHoneycomb.setMaxWidth(p1, p2);
        return;
    }

    public void setOnCloseListener(Object p1, Object p2)
    {
        android.support.v4.widget.SearchViewCompatHoneycomb.setOnCloseListener(p1, p2);
        return;
    }

    public void setOnQueryTextListener(Object p1, Object p2)
    {
        android.support.v4.widget.SearchViewCompatHoneycomb.setOnQueryTextListener(p1, p2);
        return;
    }

    public void setQuery(android.view.View p1, CharSequence p2, boolean p3)
    {
        android.support.v4.widget.SearchViewCompatHoneycomb.setQuery(p1, p2, p3);
        return;
    }

    public void setQueryHint(android.view.View p1, CharSequence p2)
    {
        android.support.v4.widget.SearchViewCompatHoneycomb.setQueryHint(p1, p2);
        return;
    }

    public void setQueryRefinementEnabled(android.view.View p1, boolean p2)
    {
        android.support.v4.widget.SearchViewCompatHoneycomb.setQueryRefinementEnabled(p1, p2);
        return;
    }

    public void setSearchableInfo(android.view.View p1, android.content.ComponentName p2)
    {
        android.support.v4.widget.SearchViewCompatHoneycomb.setSearchableInfo(p1, p2);
        return;
    }

    public void setSubmitButtonEnabled(android.view.View p1, boolean p2)
    {
        android.support.v4.widget.SearchViewCompatHoneycomb.setSubmitButtonEnabled(p1, p2);
        return;
    }
}
