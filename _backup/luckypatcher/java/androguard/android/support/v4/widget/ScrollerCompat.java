package android.support.v4.widget;
public class ScrollerCompat {
    static final android.support.v4.widget.ScrollerCompat$ScrollerCompatImpl IMPL;
    Object mScroller;

    static ScrollerCompat()
    {
        int v0 = android.os.Build$VERSION.SDK_INT;
        if (v0 < 14) {
            if (v0 < 9) {
                android.support.v4.widget.ScrollerCompat.IMPL = new android.support.v4.widget.ScrollerCompat$ScrollerCompatImplBase();
            } else {
                android.support.v4.widget.ScrollerCompat.IMPL = new android.support.v4.widget.ScrollerCompat$ScrollerCompatImplGingerbread();
            }
        } else {
            android.support.v4.widget.ScrollerCompat.IMPL = new android.support.v4.widget.ScrollerCompat$ScrollerCompatImplIcs();
        }
        return;
    }

    ScrollerCompat(android.content.Context p2, android.view.animation.Interpolator p3)
    {
        this.mScroller = android.support.v4.widget.ScrollerCompat.IMPL.createScroller(p2, p3);
        return;
    }

    public static android.support.v4.widget.ScrollerCompat create(android.content.Context p1)
    {
        return android.support.v4.widget.ScrollerCompat.create(p1, 0);
    }

    public static android.support.v4.widget.ScrollerCompat create(android.content.Context p1, android.view.animation.Interpolator p2)
    {
        return new android.support.v4.widget.ScrollerCompat(p1, p2);
    }

    public void abortAnimation()
    {
        android.support.v4.widget.ScrollerCompat.IMPL.abortAnimation(this.mScroller);
        return;
    }

    public boolean computeScrollOffset()
    {
        return android.support.v4.widget.ScrollerCompat.IMPL.computeScrollOffset(this.mScroller);
    }

    public void fling(int p11, int p12, int p13, int p14, int p15, int p16, int p17, int p18)
    {
        android.support.v4.widget.ScrollerCompat.IMPL.fling(this.mScroller, p11, p12, p13, p14, p15, p16, p17, p18);
        return;
    }

    public void fling(int p13, int p14, int p15, int p16, int p17, int p18, int p19, int p20, int p21, int p22)
    {
        android.support.v4.widget.ScrollerCompat.IMPL.fling(this.mScroller, p13, p14, p15, p16, p17, p18, p19, p20, p21, p22);
        return;
    }

    public float getCurrVelocity()
    {
        return android.support.v4.widget.ScrollerCompat.IMPL.getCurrVelocity(this.mScroller);
    }

    public int getCurrX()
    {
        return android.support.v4.widget.ScrollerCompat.IMPL.getCurrX(this.mScroller);
    }

    public int getCurrY()
    {
        return android.support.v4.widget.ScrollerCompat.IMPL.getCurrY(this.mScroller);
    }

    public int getFinalX()
    {
        return android.support.v4.widget.ScrollerCompat.IMPL.getFinalX(this.mScroller);
    }

    public int getFinalY()
    {
        return android.support.v4.widget.ScrollerCompat.IMPL.getFinalY(this.mScroller);
    }

    public boolean isFinished()
    {
        return android.support.v4.widget.ScrollerCompat.IMPL.isFinished(this.mScroller);
    }

    public boolean isOverScrolled()
    {
        return android.support.v4.widget.ScrollerCompat.IMPL.isOverScrolled(this.mScroller);
    }

    public void notifyHorizontalEdgeReached(int p3, int p4, int p5)
    {
        android.support.v4.widget.ScrollerCompat.IMPL.notifyHorizontalEdgeReached(this.mScroller, p3, p4, p5);
        return;
    }

    public void notifyVerticalEdgeReached(int p3, int p4, int p5)
    {
        android.support.v4.widget.ScrollerCompat.IMPL.notifyVerticalEdgeReached(this.mScroller, p3, p4, p5);
        return;
    }

    public void startScroll(int p7, int p8, int p9, int p10)
    {
        android.support.v4.widget.ScrollerCompat.IMPL.startScroll(this.mScroller, p7, p8, p9, p10);
        return;
    }

    public void startScroll(int p8, int p9, int p10, int p11, int p12)
    {
        android.support.v4.widget.ScrollerCompat.IMPL.startScroll(this.mScroller, p8, p9, p10, p11, p12);
        return;
    }
}
