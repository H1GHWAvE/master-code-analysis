package android.support.v4.widget;
 class SearchViewCompat$SearchViewCompatIcsImpl extends android.support.v4.widget.SearchViewCompat$SearchViewCompatHoneycombImpl {

    SearchViewCompat$SearchViewCompatIcsImpl()
    {
        return;
    }

    public android.view.View newSearchView(android.content.Context p2)
    {
        return android.support.v4.widget.SearchViewCompatIcs.newSearchView(p2);
    }

    public void setImeOptions(android.view.View p1, int p2)
    {
        android.support.v4.widget.SearchViewCompatIcs.setImeOptions(p1, p2);
        return;
    }

    public void setInputType(android.view.View p1, int p2)
    {
        android.support.v4.widget.SearchViewCompatIcs.setInputType(p1, p2);
        return;
    }
}
