package android.support.v4.widget;
interface SearchViewCompat$SearchViewCompatImpl {

    public abstract CharSequence getQuery();

    public abstract boolean isIconified();

    public abstract boolean isQueryRefinementEnabled();

    public abstract boolean isSubmitButtonEnabled();

    public abstract Object newOnCloseListener();

    public abstract Object newOnQueryTextListener();

    public abstract android.view.View newSearchView();

    public abstract void setIconified();

    public abstract void setImeOptions();

    public abstract void setInputType();

    public abstract void setMaxWidth();

    public abstract void setOnCloseListener();

    public abstract void setOnQueryTextListener();

    public abstract void setQuery();

    public abstract void setQueryHint();

    public abstract void setQueryRefinementEnabled();

    public abstract void setSearchableInfo();

    public abstract void setSubmitButtonEnabled();
}
