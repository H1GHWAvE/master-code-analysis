package android.support.v4.widget;
final class SlidingPaneLayout$SavedState$1 implements android.os.Parcelable$Creator {

    SlidingPaneLayout$SavedState$1()
    {
        return;
    }

    public android.support.v4.widget.SlidingPaneLayout$SavedState createFromParcel(android.os.Parcel p3)
    {
        return new android.support.v4.widget.SlidingPaneLayout$SavedState(p3, 0);
    }

    public bridge synthetic Object createFromParcel(android.os.Parcel p2)
    {
        return this.createFromParcel(p2);
    }

    public android.support.v4.widget.SlidingPaneLayout$SavedState[] newArray(int p2)
    {
        android.support.v4.widget.SlidingPaneLayout$SavedState[] v0 = new android.support.v4.widget.SlidingPaneLayout$SavedState[p2];
        return v0;
    }

    public bridge synthetic Object[] newArray(int p2)
    {
        return this.newArray(p2);
    }
}
