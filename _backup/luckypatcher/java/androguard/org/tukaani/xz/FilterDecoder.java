package org.tukaani.xz;
interface FilterDecoder implements org.tukaani.xz.FilterCoder {

    public abstract java.io.InputStream getInputStream();

    public abstract int getMemoryUsage();
}
