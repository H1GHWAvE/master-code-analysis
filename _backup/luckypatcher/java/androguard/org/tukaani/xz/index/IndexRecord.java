package org.tukaani.xz.index;
 class IndexRecord {
    final long uncompressed;
    final long unpadded;

    IndexRecord(long p1, long p3)
    {
        this.unpadded = p1;
        this.uncompressed = p3;
        return;
    }
}
