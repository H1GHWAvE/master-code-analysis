package org.tukaani.xz.index;
public class BlockInfo {
    public int blockNumber;
    public long compressedOffset;
    org.tukaani.xz.index.IndexDecoder index;
    public long uncompressedOffset;
    public long uncompressedSize;
    public long unpaddedSize;

    public BlockInfo(org.tukaani.xz.index.IndexDecoder p4)
    {
        this.blockNumber = -1;
        this.compressedOffset = -1;
        this.uncompressedOffset = -1;
        this.unpaddedSize = -1;
        this.uncompressedSize = -1;
        this.index = p4;
        return;
    }

    public int getCheckType()
    {
        return this.index.getStreamFlags().checkType;
    }

    public boolean hasNext()
    {
        return this.index.hasRecord((this.blockNumber + 1));
    }

    public void setNext()
    {
        this.index.setBlockInfo(this, (this.blockNumber + 1));
        return;
    }
}
