package org.tukaani.xz.index;
public class IndexEncoder extends org.tukaani.xz.index.IndexBase {
    private final java.util.ArrayList records;

    public IndexEncoder()
    {
        this(new org.tukaani.xz.XZIOException("XZ Stream or its Index has grown too big"));
        this.records = new java.util.ArrayList();
        return;
    }

    public void add(long p3, long p5)
    {
        super.add(p3, p5);
        this.records.add(new org.tukaani.xz.index.IndexRecord(p3, p5));
        return;
    }

    public void encode(java.io.OutputStream p10)
    {
        java.util.zip.CRC32 v0_1 = new java.util.zip.CRC32();
        java.util.zip.CheckedOutputStream v2_1 = new java.util.zip.CheckedOutputStream(p10, v0_1);
        v2_1.write(0);
        org.tukaani.xz.common.EncoderUtil.encodeVLI(v2_1, this.recordCount);
        int v1_0 = this.records.iterator();
        while (v1_0.hasNext()) {
            org.tukaani.xz.index.IndexRecord v3_1 = ((org.tukaani.xz.index.IndexRecord) v1_0.next());
            org.tukaani.xz.common.EncoderUtil.encodeVLI(v2_1, v3_1.unpadded);
            org.tukaani.xz.common.EncoderUtil.encodeVLI(v2_1, v3_1.uncompressed);
        }
        int v1_1 = this.getIndexPaddingSize();
        while (v1_1 > 0) {
            v2_1.write(0);
            v1_1--;
        }
        long v4 = v0_1.getValue();
        int v1_2 = 0;
        while (v1_2 < 4) {
            p10.write(((byte) ((int) (v4 >> (v1_2 * 8)))));
            v1_2++;
        }
        return;
    }

    public bridge synthetic long getIndexSize()
    {
        return super.getIndexSize();
    }

    public bridge synthetic long getStreamSize()
    {
        return super.getStreamSize();
    }
}
