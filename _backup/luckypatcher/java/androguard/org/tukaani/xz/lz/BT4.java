package org.tukaani.xz.lz;
final class BT4 extends org.tukaani.xz.lz.LZEncoder {
    private int cyclicPos;
    private final int cyclicSize;
    private final int depthLimit;
    private final org.tukaani.xz.lz.Hash234 hash;
    private int lzPos;
    private final org.tukaani.xz.lz.Matches matches;
    private final int[] tree;

    BT4(int p3, int p4, int p5, int p6, int p7, int p8)
    {
        org.tukaani.xz.lz.BT4 v2_1 = this(p3, p4, p5, p6, p7);
        v2_1.cyclicPos = -1;
        v2_1.cyclicSize = (p3 + 1);
        v2_1.lzPos = v2_1.cyclicSize;
        v2_1.hash = new org.tukaani.xz.lz.Hash234(p3);
        int v0_7 = new int[(v2_1.cyclicSize * 2)];
        v2_1.tree = v0_7;
        v2_1.matches = new org.tukaani.xz.lz.Matches((p6 - 1));
        if (p8 <= 0) {
            p8 = ((p6 / 2) + 16);
        }
        v2_1.depthLimit = p8;
        return;
    }

    static int getMemoryUsage(int p2)
    {
        return ((org.tukaani.xz.lz.Hash234.getMemoryUsage(p2) + (p2 / 128)) + 10);
    }

    private int movePos()
    {
        int v0 = this.movePos(this.niceLen, 4);
        if (v0 != 0) {
            int v2_2 = (this.lzPos + 1);
            this.lzPos = v2_2;
            if (v2_2 == 2147483647) {
                int v1 = (2147483647 - this.cyclicSize);
                this.hash.normalize(v1);
                org.tukaani.xz.lz.BT4.normalize(this.tree, v1);
                this.lzPos = (this.lzPos - v1);
            }
            int v2_9 = (this.cyclicPos + 1);
            this.cyclicPos = v2_9;
            if (v2_9 == this.cyclicSize) {
                this.cyclicPos = 0;
            }
        }
        return v0;
    }

    private void skip(int p14, int p15)
    {
        int v1 = this.depthLimit;
        int v7 = ((this.cyclicPos << 1) + 1);
        int v8 = (this.cyclicPos << 1);
        int v4 = 0;
        int v5 = 0;
        while(true) {
            int v0 = (this.lzPos - p15);
            int v2 = (v1 - 1);
            if ((v1 != 0) && (v0 < this.cyclicSize)) {
                int[] v9_7;
                if (v0 <= this.cyclicPos) {
                    v9_7 = 0;
                } else {
                    v9_7 = this.cyclicSize;
                }
                int v6 = ((v9_7 + (this.cyclicPos - v0)) << 1);
                int v3 = Math.min(v4, v5);
                if (this.buf[((this.readPos + v3) - v0)] == this.buf[(this.readPos + v3)]) {
                    do {
                        v3++;
                        if (v3 != p14) {
                        } else {
                            this.tree[v8] = this.tree[v6];
                            this.tree[v7] = this.tree[(v6 + 1)];
                            return;
                        }
                    } while(this.buf[((this.readPos + v3) - v0)] == this.buf[(this.readPos + v3)]);
                }
                if ((this.buf[((this.readPos + v3) - v0)] & 255) >= (this.buf[(this.readPos + v3)] & 255)) {
                    this.tree[v7] = p15;
                    v7 = v6;
                    p15 = this.tree[v7];
                    v4 = v3;
                } else {
                    this.tree[v8] = p15;
                    v8 = (v6 + 1);
                    p15 = this.tree[v8];
                    v5 = v3;
                }
                v1 = v2;
            } else {
                this.tree[v7] = 0;
                this.tree[v8] = 0;
            }
            return;
        }
    }

    public org.tukaani.xz.lz.Matches getMatches()
    {
        org.tukaani.xz.lz.Matches v18_53;
        this.matches.count = 0;
        int v13 = this.matchLenMax;
        int v14 = this.niceLen;
        int v2 = this.movePos();
        if (v2 >= v13) {
            this.hash.calcHashes(this.buf, this.readPos);
            int v5 = (this.lzPos - this.hash.getHash2Pos());
            int v6 = (this.lzPos - this.hash.getHash3Pos());
            int v3 = this.hash.getHash4Pos();
            this.hash.updateTables(this.lzPos);
            int v12 = 0;
            if ((v5 < this.cyclicSize) && (this.buf[(this.readPos - v5)] == this.buf[this.readPos])) {
                v12 = 2;
                this.matches.len[0] = 2;
                this.matches.dist[0] = (v5 - 1);
                this.matches.count = 1;
            }
            if ((v5 != v6) && ((v6 < this.cyclicSize) && (this.buf[(this.readPos - v6)] == this.buf[this.readPos]))) {
                v12 = 3;
                org.tukaani.xz.lz.Matches v18_18 = this.matches.dist;
                int v19_18 = this.matches;
                int v20_5 = v19_18.count;
                v19_18.count = (v20_5 + 1);
                v18_18[v20_5] = (v6 - 1);
                v5 = v6;
            }
            if (this.matches.count > 0) {
                while ((v12 < v13) && (this.buf[((this.readPos + v12) - v5)] == this.buf[(this.readPos + v12)])) {
                    v12++;
                }
                this.matches.len[(this.matches.count - 1)] = v12;
                if (v12 >= v14) {
                    this.skip(v14, v3);
                    v18_53 = this.matches;
                    return v18_53;
                }
            }
            if (v12 < 3) {
                v12 = 3;
            }
            int v7 = this.depthLimit;
            int v16 = ((this.cyclicPos << 1) + 1);
            int v17 = (this.cyclicPos << 1);
            int v10 = 0;
            int v11 = 0;
            while(true) {
                int v4 = (this.lzPos - v3);
                int v8 = (v7 - 1);
                if ((v7 == 0) || (v4 >= this.cyclicSize)) {
                    break;
                }
                org.tukaani.xz.lz.Matches v18_33;
                if (v4 <= this.cyclicPos) {
                    v18_33 = 0;
                } else {
                    v18_33 = this.cyclicSize;
                }
                int v15 = ((v18_33 + (this.cyclicPos - v4)) << 1);
                int v9 = Math.min(v10, v11);
                if (this.buf[((this.readPos + v9) - v4)] == this.buf[(this.readPos + v9)]) {
                    do {
                        v9++;
                    } while((v9 < v13) && (this.buf[((this.readPos + v9) - v4)] == this.buf[(this.readPos + v9)]));
                    if (v9 > v12) {
                        v12 = v9;
                        this.matches.len[this.matches.count] = v9;
                        this.matches.dist[this.matches.count] = (v4 - 1);
                        org.tukaani.xz.lz.Matches v18_43 = this.matches;
                        v18_43.count = (v18_43.count + 1);
                        if (v9 >= v14) {
                            this.tree[v17] = this.tree[v15];
                            this.tree[v16] = this.tree[(v15 + 1)];
                            v18_53 = this.matches;
                            return v18_53;
                        }
                    }
                }
                if ((this.buf[((this.readPos + v9) - v4)] & 255) >= (this.buf[(this.readPos + v9)] & 255)) {
                    this.tree[v16] = v3;
                    v16 = v15;
                    v3 = this.tree[v16];
                    v10 = v9;
                } else {
                    this.tree[v17] = v3;
                    v17 = (v15 + 1);
                    v3 = this.tree[v17];
                    v11 = v9;
                }
                v7 = v8;
            }
            this.tree[v16] = 0;
            this.tree[v17] = 0;
            v18_53 = this.matches;
        } else {
            if (v2 != 0) {
                v13 = v2;
                if (v14 <= v2) {
                } else {
                    v14 = v2;
                }
            } else {
                v18_53 = this.matches;
            }
        }
        return v18_53;
    }

    public void skip(int p8)
    {
        int v2 = p8;
        while(true) {
            int v8_1 = (v2 - 1);
            if (v2 <= 0) {
                break;
            }
            int v3 = this.niceLen;
            int v0 = this.movePos();
            if (v0 < v3) {
                if (v0 != 0) {
                    v3 = v0;
                } else {
                    v2 = v8_1;
                }
            }
            this.hash.calcHashes(this.buf, this.readPos);
            int v1 = this.hash.getHash4Pos();
            this.hash.updateTables(this.lzPos);
            this.skip(v3, v1);
            v2 = v8_1;
        }
        return;
    }
}
