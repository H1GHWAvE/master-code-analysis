package org.tukaani.xz.lz;
public abstract class LZEncoder {
    static final synthetic boolean $assertionsDisabled = False;
    public static final int MF_BT4 = 20;
    public static final int MF_HC4 = 4;
    final byte[] buf;
    private boolean finishing;
    private final int keepSizeAfter;
    private final int keepSizeBefore;
    final int matchLenMax;
    final int niceLen;
    private int pendingSize;
    private int readLimit;
    int readPos;
    private int writePos;

    static LZEncoder()
    {
        int v0_2;
        if (org.tukaani.xz.lz.LZEncoder.desiredAssertionStatus()) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        org.tukaani.xz.lz.LZEncoder.$assertionsDisabled = v0_2;
        return;
    }

    LZEncoder(int p3, int p4, int p5, int p6, int p7)
    {
        this.readPos = -1;
        this.readLimit = -1;
        this.finishing = 0;
        this.writePos = 0;
        this.pendingSize = 0;
        int v0_2 = new byte[org.tukaani.xz.lz.LZEncoder.getBufSize(p3, p4, p5, p7)];
        this.buf = v0_2;
        this.keepSizeBefore = (p4 + p3);
        this.keepSizeAfter = (p5 + p7);
        this.matchLenMax = p7;
        this.niceLen = p6;
        return;
    }

    private static int getBufSize(int p5, int p6, int p7, int p8)
    {
        return (((p6 + p5) + (p7 + p8)) + Math.min(((p5 / 2) + 262144), 536870912));
    }

    public static org.tukaani.xz.lz.LZEncoder getInstance(int p7, int p8, int p9, int p10, int p11, int p12, int p13)
    {
        org.tukaani.xz.lz.BT4 v0_1;
        switch (p12) {
            case 4:
                v0_1 = new org.tukaani.xz.lz.HC4(p7, p8, p9, p10, p11, p13);
                break;
            case 20:
                v0_1 = new org.tukaani.xz.lz.BT4(p7, p8, p9, p10, p11, p13);
                break;
            default:
                throw new IllegalArgumentException();
        }
        return v0_1;
    }

    public static int getMemoryUsage(int p2, int p3, int p4, int p5, int p6)
    {
        int v0_1;
        int v0_0 = ((org.tukaani.xz.lz.LZEncoder.getBufSize(p2, p3, p4, p5) / 1024) + 10);
        switch (p6) {
            case 4:
                v0_1 = (v0_0 + org.tukaani.xz.lz.HC4.getMemoryUsage(p2));
                break;
            case 20:
                v0_1 = (v0_0 + org.tukaani.xz.lz.BT4.getMemoryUsage(p2));
                break;
            default:
                throw new IllegalArgumentException();
        }
        return v0_1;
    }

    private void moveWindow()
    {
        int v0 = (((this.readPos + 1) - this.keepSizeBefore) & -16);
        System.arraycopy(this.buf, v0, this.buf, 0, (this.writePos - v0));
        this.readPos = (this.readPos - v0);
        this.readLimit = (this.readLimit - v0);
        this.writePos = (this.writePos - v0);
        return;
    }

    static void normalize(int[] p2, int p3)
    {
        int v0 = 0;
        while (v0 < p2.length) {
            if (p2[v0] > p3) {
                p2[v0] = (p2[v0] - p3);
            } else {
                p2[v0] = 0;
            }
            v0++;
        }
        return;
    }

    private void processPendingBytes()
    {
        if ((this.pendingSize > 0) && (this.readPos < this.readLimit)) {
            this.readPos = (this.readPos - this.pendingSize);
            int v0 = this.pendingSize;
            this.pendingSize = 0;
            this.skip(v0);
            if ((!org.tukaani.xz.lz.LZEncoder.$assertionsDisabled) && (this.pendingSize >= v0)) {
                throw new AssertionError();
            }
        }
        return;
    }

    public void copyUncompressed(java.io.OutputStream p3, int p4, int p5)
    {
        p3.write(this.buf, ((this.readPos + 1) - p4), p5);
        return;
    }

    public int fillWindow(byte[] p4, int p5, int p6)
    {
        if ((org.tukaani.xz.lz.LZEncoder.$assertionsDisabled) || (!this.finishing)) {
            if (this.readPos >= (this.buf.length - this.keepSizeAfter)) {
                this.moveWindow();
            }
            if (p6 > (this.buf.length - this.writePos)) {
                p6 = (this.buf.length - this.writePos);
            }
            System.arraycopy(p4, p5, this.buf, this.writePos, p6);
            this.writePos = (this.writePos + p6);
            if (this.writePos >= this.keepSizeAfter) {
                this.readLimit = (this.writePos - this.keepSizeAfter);
            }
            this.processPendingBytes();
            return p6;
        } else {
            throw new AssertionError();
        }
    }

    public int getAvail()
    {
        if ((org.tukaani.xz.lz.LZEncoder.$assertionsDisabled) || (this.isStarted())) {
            return (this.writePos - this.readPos);
        } else {
            throw new AssertionError();
        }
    }

    public int getByte(int p3)
    {
        return (this.buf[(this.readPos - p3)] & 255);
    }

    public int getByte(int p3, int p4)
    {
        return (this.buf[((this.readPos + p3) - p4)] & 255);
    }

    public int getMatchLen(int p6, int p7)
    {
        int v1 = 0;
        while ((v1 < p7) && (this.buf[(this.readPos + v1)] == this.buf[(((this.readPos - p6) - 1) + v1)])) {
            v1++;
        }
        return v1;
    }

    public int getMatchLen(int p7, int p8, int p9)
    {
        int v1 = (this.readPos + p7);
        int v2 = 0;
        while ((v2 < p9) && (this.buf[(v1 + v2)] == this.buf[(((v1 - p8) - 1) + v2)])) {
            v2++;
        }
        return v2;
    }

    public abstract org.tukaani.xz.lz.Matches getMatches();

    public int getPos()
    {
        return this.readPos;
    }

    public boolean hasEnoughData(int p3)
    {
        int v0_2;
        if ((this.readPos - p3) >= this.readLimit) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public boolean isStarted()
    {
        int v0_1;
        if (this.readPos == -1) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    int movePos(int p4, int p5)
    {
        if ((org.tukaani.xz.lz.LZEncoder.$assertionsDisabled) || (p4 >= p5)) {
            this.readPos = (this.readPos + 1);
            int v0 = (this.writePos - this.readPos);
            if ((v0 < p4) && ((v0 < p5) || (!this.finishing))) {
                this.pendingSize = (this.pendingSize + 1);
                v0 = 0;
            }
            return v0;
        } else {
            throw new AssertionError();
        }
    }

    public void setFinishing()
    {
        this.readLimit = (this.writePos - 1);
        this.finishing = 1;
        this.processPendingBytes();
        return;
    }

    public void setFlushing()
    {
        this.readLimit = (this.writePos - 1);
        this.processPendingBytes();
        return;
    }

    public void setPresetDict(int p5, byte[] p6)
    {
        if ((org.tukaani.xz.lz.LZEncoder.$assertionsDisabled) || (!this.isStarted())) {
            if ((org.tukaani.xz.lz.LZEncoder.$assertionsDisabled) || (this.writePos == 0)) {
                if (p6 != null) {
                    int v0 = Math.min(p6.length, p5);
                    System.arraycopy(p6, (p6.length - v0), this.buf, 0, v0);
                    this.writePos = (this.writePos + v0);
                    this.skip(v0);
                }
                return;
            } else {
                throw new AssertionError();
            }
        } else {
            throw new AssertionError();
        }
    }

    public abstract void skip();

    public boolean verifyMatches(org.tukaani.xz.lz.Matches p5)
    {
        int v1 = Math.min(this.getAvail(), this.matchLenMax);
        int v0 = 0;
        while (v0 < p5.count) {
            if (this.getMatchLen(p5.dist[v0], v1) == p5.len[v0]) {
                v0++;
            } else {
                int v2_2 = 0;
            }
            return v2_2;
        }
        v2_2 = 1;
        return v2_2;
    }
}
