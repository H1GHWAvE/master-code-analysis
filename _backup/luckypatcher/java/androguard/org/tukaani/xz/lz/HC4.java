package org.tukaani.xz.lz;
final class HC4 extends org.tukaani.xz.lz.LZEncoder {
    static final synthetic boolean $assertionsDisabled;
    private final int[] chain;
    private int cyclicPos;
    private final int cyclicSize;
    private final int depthLimit;
    private final org.tukaani.xz.lz.Hash234 hash;
    private int lzPos;
    private final org.tukaani.xz.lz.Matches matches;

    static HC4()
    {
        int v0_2;
        if (org.tukaani.xz.lz.HC4.desiredAssertionStatus()) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        org.tukaani.xz.lz.HC4.$assertionsDisabled = v0_2;
        return;
    }

    HC4(int p3, int p4, int p5, int p6, int p7, int p8)
    {
        org.tukaani.xz.lz.HC4 v2_1 = this(p3, p4, p5, p6, p7);
        v2_1.cyclicPos = -1;
        v2_1.hash = new org.tukaani.xz.lz.Hash234(p3);
        v2_1.cyclicSize = (p3 + 1);
        int v0_5 = new int[v2_1.cyclicSize];
        v2_1.chain = v0_5;
        v2_1.lzPos = v2_1.cyclicSize;
        v2_1.matches = new org.tukaani.xz.lz.Matches((p6 - 1));
        if (p8 <= 0) {
            p8 = ((p6 / 4) + 4);
        }
        v2_1.depthLimit = p8;
        return;
    }

    static int getMemoryUsage(int p2)
    {
        return ((org.tukaani.xz.lz.Hash234.getMemoryUsage(p2) + (p2 / 256)) + 10);
    }

    private int movePos()
    {
        int v0 = this.movePos(4, 4);
        if (v0 != 0) {
            int v2_2 = (this.lzPos + 1);
            this.lzPos = v2_2;
            if (v2_2 == 2147483647) {
                int v1 = (2147483647 - this.cyclicSize);
                this.hash.normalize(v1);
                org.tukaani.xz.lz.HC4.normalize(this.chain, v1);
                this.lzPos = (this.lzPos - v1);
            }
            int v2_9 = (this.cyclicPos + 1);
            this.cyclicPos = v2_9;
            if (v2_9 == this.cyclicSize) {
                this.cyclicPos = 0;
            }
        }
        return v0;
    }

    public org.tukaani.xz.lz.Matches getMatches()
    {
        org.tukaani.xz.lz.Matches v11_44;
        this.matches.count = 0;
        int v9 = this.matchLenMax;
        int v10 = this.niceLen;
        int v0 = this.movePos();
        if (v0 >= v9) {
            this.hash.calcHashes(this.buf, this.readPos);
            int v3 = (this.lzPos - this.hash.getHash2Pos());
            int v4 = (this.lzPos - this.hash.getHash3Pos());
            int v1 = this.hash.getHash4Pos();
            this.hash.updateTables(this.lzPos);
            this.chain[this.cyclicPos] = v1;
            int v8 = 0;
            if ((v3 < this.cyclicSize) && (this.buf[(this.readPos - v3)] == this.buf[this.readPos])) {
                v8 = 2;
                this.matches.len[0] = 2;
                this.matches.dist[0] = (v3 - 1);
                this.matches.count = 1;
            }
            if ((v3 != v4) && ((v4 < this.cyclicSize) && (this.buf[(this.readPos - v4)] == this.buf[this.readPos]))) {
                v8 = 3;
                org.tukaani.xz.lz.Matches v11_19 = this.matches.dist;
                int v12_19 = this.matches;
                int v13_5 = v12_19.count;
                v12_19.count = (v13_5 + 1);
                v11_19[v13_5] = (v4 - 1);
                v3 = v4;
            }
            if (this.matches.count > 0) {
                while ((v8 < v9) && (this.buf[((this.readPos + v8) - v3)] == this.buf[(this.readPos + v8)])) {
                    v8++;
                }
                this.matches.len[(this.matches.count - 1)] = v8;
                if (v8 >= v10) {
                    v11_44 = this.matches;
                    return v11_44;
                }
            }
            if (v8 < 3) {
                v8 = 3;
            }
            int v5 = this.depthLimit;
            while(true) {
                int v2 = (this.lzPos - v1);
                int v6 = (v5 - 1);
                if ((v5 != 0) && (v2 < this.cyclicSize)) {
                    org.tukaani.xz.lz.Matches v11_31;
                    if (v2 <= this.cyclicPos) {
                        v11_31 = 0;
                    } else {
                        v11_31 = this.cyclicSize;
                    }
                    v1 = this.chain[(v11_31 + (this.cyclicPos - v2))];
                    if ((this.buf[((this.readPos + v8) - v2)] == this.buf[(this.readPos + v8)]) && (this.buf[(this.readPos - v2)] == this.buf[this.readPos])) {
                        int v7 = 0;
                        do {
                            v7++;
                        } while((v7 < v9) && (this.buf[((this.readPos + v7) - v2)] == this.buf[(this.readPos + v7)]));
                        if (v7 > v8) {
                            v8 = v7;
                            this.matches.len[this.matches.count] = v7;
                            this.matches.dist[this.matches.count] = (v2 - 1);
                            org.tukaani.xz.lz.Matches v11_43 = this.matches;
                            v11_43.count = (v11_43.count + 1);
                            if (v7 >= v10) {
                                break;
                            }
                        }
                    }
                    v5 = v6;
                } else {
                    v11_44 = this.matches;
                }
            }
            v11_44 = this.matches;
        } else {
            if (v0 != 0) {
                v9 = v0;
                if (v10 <= v0) {
                } else {
                    v10 = v0;
                }
            } else {
                v11_44 = this.matches;
            }
        }
        return v11_44;
    }

    public void skip(int p5)
    {
        if ((!org.tukaani.xz.lz.HC4.$assertionsDisabled) && (p5 < 0)) {
            throw new AssertionError();
        }
        do {
            int v0 = p5;
            p5 = (v0 - 1);
            if (v0 <= 0) {
                return;
            } else {
            }
        } while(this.movePos() == 0);
        this.hash.calcHashes(this.buf, this.readPos);
        this.chain[this.cyclicPos] = this.hash.getHash4Pos();
        this.hash.updateTables(this.lzPos);
        v0 = p5;
        while(true) {
        }
    }
}
