package org.tukaani.xz;
 class LZMA2Decoder extends org.tukaani.xz.LZMA2Coder implements org.tukaani.xz.FilterDecoder {
    private int dictSize;

    LZMA2Decoder(byte[] p4)
    {
        if ((p4.length == 1) && ((p4[0] & 255) <= 37)) {
            this.dictSize = ((p4[0] & 1) | 2);
            this.dictSize = (this.dictSize << ((p4[0] >> 1) + 11));
            return;
        } else {
            throw new org.tukaani.xz.UnsupportedOptionsException("Unsupported LZMA2 properties");
        }
    }

    public java.io.InputStream getInputStream(java.io.InputStream p3)
    {
        return new org.tukaani.xz.LZMA2InputStream(p3, this.dictSize);
    }

    public int getMemoryUsage()
    {
        return org.tukaani.xz.LZMA2InputStream.getMemoryUsage(this.dictSize);
    }
}
