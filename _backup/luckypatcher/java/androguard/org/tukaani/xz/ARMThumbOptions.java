package org.tukaani.xz;
public class ARMThumbOptions extends org.tukaani.xz.BCJOptions {
    private static final int ALIGNMENT = 2;

    public ARMThumbOptions()
    {
        this(2);
        return;
    }

    public bridge synthetic Object clone()
    {
        return super.clone();
    }

    public bridge synthetic int getDecoderMemoryUsage()
    {
        return super.getDecoderMemoryUsage();
    }

    public bridge synthetic int getEncoderMemoryUsage()
    {
        return super.getEncoderMemoryUsage();
    }

    org.tukaani.xz.FilterEncoder getFilterEncoder()
    {
        return new org.tukaani.xz.BCJEncoder(this, 8);
    }

    public java.io.InputStream getInputStream(java.io.InputStream p5)
    {
        return new org.tukaani.xz.SimpleInputStream(p5, new org.tukaani.xz.simple.ARMThumb(0, this.startOffset));
    }

    public org.tukaani.xz.FinishableOutputStream getOutputStream(org.tukaani.xz.FinishableOutputStream p5)
    {
        return new org.tukaani.xz.SimpleOutputStream(p5, new org.tukaani.xz.simple.ARMThumb(1, this.startOffset));
    }

    public bridge synthetic int getStartOffset()
    {
        return super.getStartOffset();
    }

    public bridge synthetic void setStartOffset(int p1)
    {
        super.setStartOffset(p1);
        return;
    }
}
