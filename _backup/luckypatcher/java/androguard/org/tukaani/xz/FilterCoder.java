package org.tukaani.xz;
interface FilterCoder {

    public abstract boolean changesSize();

    public abstract boolean lastOK();

    public abstract boolean nonLastOK();
}
