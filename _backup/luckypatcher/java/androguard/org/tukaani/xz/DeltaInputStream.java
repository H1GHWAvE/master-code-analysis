package org.tukaani.xz;
public class DeltaInputStream extends java.io.InputStream {
    public static final int DISTANCE_MAX = 256;
    public static final int DISTANCE_MIN = 1;
    private final org.tukaani.xz.delta.DeltaDecoder delta;
    private java.io.IOException exception;
    private java.io.InputStream in;
    private final byte[] tempBuf;

    public DeltaInputStream(java.io.InputStream p2, int p3)
    {
        this.exception = 0;
        org.tukaani.xz.delta.DeltaDecoder v0_2 = new byte[1];
        this.tempBuf = v0_2;
        if (p2 != null) {
            this.in = p2;
            this.delta = new org.tukaani.xz.delta.DeltaDecoder(p3);
            return;
        } else {
            throw new NullPointerException();
        }
    }

    public int available()
    {
        if (this.in != null) {
            if (this.exception == null) {
                return this.in.available();
            } else {
                throw this.exception;
            }
        } else {
            throw new org.tukaani.xz.XZIOException("Stream closed");
        }
    }

    public void close()
    {
        if (this.in != null) {
            try {
                this.in.close();
                this.in = 0;
            } catch (Throwable v0_2) {
                this.in = 0;
                throw v0_2;
            }
        }
        return;
    }

    public int read()
    {
        int v0_0 = -1;
        if (this.read(this.tempBuf, 0, 1) != -1) {
            v0_0 = (this.tempBuf[0] & 255);
        }
        return v0_0;
    }

    public int read(byte[] p5, int p6, int p7)
    {
        java.io.IOException v1;
        if (p7 != 0) {
            if (this.in != null) {
                if (this.exception == null) {
                    try {
                        v1 = this.in.read(p5, p6, p7);
                    } catch (java.io.IOException v0) {
                        this.exception = v0;
                        throw v0;
                    }
                    if (v1 != -1) {
                        this.delta.decode(p5, p6, v1);
                    } else {
                        v1 = -1;
                    }
                } else {
                    throw this.exception;
                }
            } else {
                throw new org.tukaani.xz.XZIOException("Stream closed");
            }
        } else {
            v1 = 0;
        }
        return v1;
    }
}
