package org.tukaani.xz;
public class XZOutputStream extends org.tukaani.xz.FinishableOutputStream {
    private org.tukaani.xz.BlockOutputStream blockEncoder;
    private final org.tukaani.xz.check.Check check;
    private java.io.IOException exception;
    private org.tukaani.xz.FilterEncoder[] filters;
    private boolean filtersSupportFlushing;
    private boolean finished;
    private final org.tukaani.xz.index.IndexEncoder index;
    private java.io.OutputStream out;
    private final org.tukaani.xz.common.StreamFlags streamFlags;
    private final byte[] tempBuf;

    public XZOutputStream(java.io.OutputStream p2, org.tukaani.xz.FilterOptions p3)
    {
        this(p2, p3, 4);
        return;
    }

    public XZOutputStream(java.io.OutputStream p3, org.tukaani.xz.FilterOptions p4, int p5)
    {
        org.tukaani.xz.FilterOptions[] v0_1 = new org.tukaani.xz.FilterOptions[1];
        v0_1[0] = p4;
        this(p3, v0_1, p5);
        return;
    }

    public XZOutputStream(java.io.OutputStream p2, org.tukaani.xz.FilterOptions[] p3)
    {
        this(p2, p3, 4);
        return;
    }

    public XZOutputStream(java.io.OutputStream p3, org.tukaani.xz.FilterOptions[] p4, int p5)
    {
        this.streamFlags = new org.tukaani.xz.common.StreamFlags();
        this.index = new org.tukaani.xz.index.IndexEncoder();
        this.blockEncoder = 0;
        this.exception = 0;
        this.finished = 0;
        org.tukaani.xz.check.Check v0_6 = new byte[1];
        this.tempBuf = v0_6;
        this.out = p3;
        this.updateFilters(p4);
        this.streamFlags.checkType = p5;
        this.check = org.tukaani.xz.check.Check.getInstance(p5);
        this.encodeStreamHeader();
        return;
    }

    private void encodeStreamFlags(byte[] p3, int p4)
    {
        p3[p4] = 0;
        p3[(p4 + 1)] = ((byte) this.streamFlags.checkType);
        return;
    }

    private void encodeStreamFooter()
    {
        byte[] v2 = new byte[6];
        long v0 = ((this.index.getIndexSize() / 4) - 1);
        int v3 = 0;
        while (v3 < 4) {
            v2[v3] = ((byte) ((int) (v0 >> (v3 * 8))));
            v3++;
        }
        this.encodeStreamFlags(v2, 4);
        org.tukaani.xz.common.EncoderUtil.writeCRC32(this.out, v2);
        this.out.write(v2);
        this.out.write(org.tukaani.xz.XZ.FOOTER_MAGIC);
        return;
    }

    private void encodeStreamHeader()
    {
        this.out.write(org.tukaani.xz.XZ.HEADER_MAGIC);
        byte[] v0 = new byte[2];
        this.encodeStreamFlags(v0, 0);
        this.out.write(v0);
        org.tukaani.xz.common.EncoderUtil.writeCRC32(this.out, v0);
        return;
    }

    public void close()
    {
        if (this.out != null) {
            try {
                this.finish();
                try {
                    this.out.close();
                } catch (java.io.IOException v0) {
                    if (this.exception != null) {
                    } else {
                        this.exception = v0;
                    }
                }
                this.out = 0;
            } catch (int v1) {
            }
        }
        if (this.exception == null) {
            return;
        } else {
            throw this.exception;
        }
    }

    public void endBlock()
    {
        if (this.exception == null) {
            if (!this.finished) {
                if (this.blockEncoder != null) {
                    try {
                        this.blockEncoder.finish();
                        this.index.add(this.blockEncoder.getUnpaddedSize(), this.blockEncoder.getUncompressedSize());
                        this.blockEncoder = 0;
                    } catch (java.io.IOException v0) {
                        this.exception = v0;
                        throw v0;
                    }
                }
                return;
            } else {
                throw new org.tukaani.xz.XZIOException("Stream finished or closed");
            }
        } else {
            throw this.exception;
        }
    }

    public void finish()
    {
        if (!this.finished) {
            this.endBlock();
            try {
                this.index.encode(this.out);
                this.encodeStreamFooter();
                this.finished = 1;
            } catch (java.io.IOException v0) {
                this.exception = v0;
                throw v0;
            }
        }
        return;
    }

    public void flush()
    {
        if (this.exception == null) {
            if (!this.finished) {
                try {
                    if (this.blockEncoder == null) {
                        this.out.flush();
                    } else {
                        if (!this.filtersSupportFlushing) {
                            this.endBlock();
                            this.out.flush();
                        } else {
                            this.blockEncoder.flush();
                        }
                    }
                } catch (java.io.IOException v0) {
                    this.exception = v0;
                    throw v0;
                }
                return;
            } else {
                throw new org.tukaani.xz.XZIOException("Stream finished or closed");
            }
        } else {
            throw this.exception;
        }
    }

    public void updateFilters(org.tukaani.xz.FilterOptions p3)
    {
        org.tukaani.xz.FilterOptions[] v0 = new org.tukaani.xz.FilterOptions[1];
        v0[0] = p3;
        this.updateFilters(v0);
        return;
    }

    public void updateFilters(org.tukaani.xz.FilterOptions[] p6)
    {
        if (this.blockEncoder == null) {
            if ((p6.length >= 1) && (p6.length <= 4)) {
                this.filtersSupportFlushing = 1;
                org.tukaani.xz.FilterEncoder[] v1 = new org.tukaani.xz.FilterEncoder[p6.length];
                int v0 = 0;
                while (v0 < p6.length) {
                    v1[v0] = p6[v0].getFilterEncoder();
                    this.filtersSupportFlushing = (this.filtersSupportFlushing & v1[v0].supportsFlushing());
                    v0++;
                }
                org.tukaani.xz.RawCoder.validate(v1);
                this.filters = v1;
                return;
            } else {
                throw new org.tukaani.xz.UnsupportedOptionsException("XZ filter chain must be 1-4 filters");
            }
        } else {
            throw new org.tukaani.xz.UnsupportedOptionsException("Changing filter options in the middle of a XZ Block not implemented");
        }
    }

    public void write(int p4)
    {
        this.tempBuf[0] = ((byte) p4);
        this.write(this.tempBuf, 0, 1);
        return;
    }

    public void write(byte[] p6, int p7, int p8)
    {
        if ((p7 >= 0) && ((p8 >= 0) && (((p7 + p8) >= 0) && ((p7 + p8) <= p6.length)))) {
            if (this.exception == null) {
                if (!this.finished) {
                    try {
                        if (this.blockEncoder == null) {
                            this.blockEncoder = new org.tukaani.xz.BlockOutputStream(this.out, this.filters, this.check);
                        }
                    } catch (java.io.IOException v0) {
                        this.exception = v0;
                        throw v0;
                    }
                    this.blockEncoder.write(p6, p7, p8);
                    return;
                } else {
                    throw new org.tukaani.xz.XZIOException("Stream finished or closed");
                }
            } else {
                throw this.exception;
            }
        } else {
            throw new IndexOutOfBoundsException();
        }
    }
}
