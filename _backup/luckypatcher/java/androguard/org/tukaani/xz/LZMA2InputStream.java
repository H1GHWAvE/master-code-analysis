package org.tukaani.xz;
public class LZMA2InputStream extends java.io.InputStream {
    private static final int COMPRESSED_SIZE_MAX = 65536;
    public static final int DICT_SIZE_MAX = 2147483632;
    public static final int DICT_SIZE_MIN = 4096;
    private boolean endReached;
    private java.io.IOException exception;
    private java.io.DataInputStream in;
    private boolean isLZMAChunk;
    private final org.tukaani.xz.lz.LZDecoder lz;
    private org.tukaani.xz.lzma.LZMADecoder lzma;
    private boolean needDictReset;
    private boolean needProps;
    private final org.tukaani.xz.rangecoder.RangeDecoderFromBuffer rc;
    private final byte[] tempBuf;
    private int uncompressedSize;

    public LZMA2InputStream(java.io.InputStream p2, int p3)
    {
        this(p2, p3, 0);
        return;
    }

    public LZMA2InputStream(java.io.InputStream p5, int p6, byte[] p7)
    {
        this.rc = new org.tukaani.xz.rangecoder.RangeDecoderFromBuffer(65536);
        this.uncompressedSize = 0;
        this.needDictReset = 1;
        this.needProps = 1;
        this.endReached = 0;
        this.exception = 0;
        int v0_3 = new byte[1];
        this.tempBuf = v0_3;
        if (p5 != null) {
            this.in = new java.io.DataInputStream(p5);
            this.lz = new org.tukaani.xz.lz.LZDecoder(org.tukaani.xz.LZMA2InputStream.getDictSize(p6), p7);
            if ((p7 != null) && (p7.length > 0)) {
                this.needDictReset = 0;
            }
            return;
        } else {
            throw new NullPointerException();
        }
    }

    private void decodeChunkHeader()
    {
        int v1 = this.in.readUnsignedByte();
        if (v1 != 0) {
            if ((v1 < 224) && (v1 != 1)) {
                if (this.needDictReset) {
                    throw new org.tukaani.xz.CorruptedInputException();
                }
            } else {
                this.needProps = 1;
                this.needDictReset = 0;
                this.lz.reset();
            }
            if (v1 < 128) {
                if (v1 <= 2) {
                    this.isLZMAChunk = 0;
                    this.uncompressedSize = (this.in.readUnsignedShort() + 1);
                } else {
                    throw new org.tukaani.xz.CorruptedInputException();
                }
            } else {
                this.isLZMAChunk = 1;
                this.uncompressedSize = ((v1 & 31) << 16);
                this.uncompressedSize = (this.uncompressedSize + (this.in.readUnsignedShort() + 1));
                int v0 = (this.in.readUnsignedShort() + 1);
                if (v1 < 192) {
                    if (!this.needProps) {
                        if (v1 >= 160) {
                            this.lzma.reset();
                        }
                    } else {
                        throw new org.tukaani.xz.CorruptedInputException();
                    }
                } else {
                    this.needProps = 0;
                    this.decodeProps();
                }
                this.rc.prepareInputBuffer(this.in, v0);
            }
        } else {
            this.endReached = 1;
        }
        return;
    }

    private void decodeProps()
    {
        int v6_0 = this.in.readUnsignedByte();
        if (v6_0 <= 224) {
            int v5 = (v6_0 / 45);
            int v6_1 = (v6_0 - ((v5 * 9) * 5));
            int v4 = (v6_1 / 9);
            int v3 = (v6_1 - (v4 * 9));
            if ((v3 + v4) <= 4) {
                this.lzma = new org.tukaani.xz.lzma.LZMADecoder(this.lz, this.rc, v3, v4, v5);
                return;
            } else {
                throw new org.tukaani.xz.CorruptedInputException();
            }
        } else {
            throw new org.tukaani.xz.CorruptedInputException();
        }
    }

    private static int getDictSize(int p3)
    {
        if ((p3 >= 4096) && (p3 <= 2147483632)) {
            return ((p3 + 15) & -16);
        } else {
            throw new IllegalArgumentException(new StringBuilder().append("Unsupported dictionary size ").append(p3).toString());
        }
    }

    public static int getMemoryUsage(int p1)
    {
        return ((org.tukaani.xz.LZMA2InputStream.getDictSize(p1) / 1024) + 104);
    }

    public int available()
    {
        if (this.in != null) {
            if (this.exception == null) {
                return this.uncompressedSize;
            } else {
                throw this.exception;
            }
        } else {
            throw new org.tukaani.xz.XZIOException("Stream closed");
        }
    }

    public void close()
    {
        if (this.in != null) {
            try {
                this.in.close();
                this.in = 0;
            } catch (Throwable v0_2) {
                this.in = 0;
                throw v0_2;
            }
        }
        return;
    }

    public int read()
    {
        int v0_0 = -1;
        if (this.read(this.tempBuf, 0, 1) != -1) {
            v0_0 = (this.tempBuf[0] & 255);
        }
        return v0_0;
    }

    public int read(byte[] p8, int p9, int p10)
    {
        if ((p9 >= 0) && ((p10 >= 0) && (((p9 + p10) >= 0) && ((p9 + p10) <= p8.length)))) {
            int v3;
            if (p10 != 0) {
                if (this.in != null) {
                    if (this.exception == null) {
                        if (!this.endReached) {
                            v3 = 0;
                            while (p10 > 0) {
                                try {
                                    if (this.uncompressedSize == 0) {
                                        this.decodeChunkHeader();
                                        if (this.endReached) {
                                            if (v3 != 0) {
                                                break;
                                            }
                                            v3 = -1;
                                            break;
                                        }
                                    }
                                    int v1 = Math.min(this.uncompressedSize, p10);
                                    if (this.isLZMAChunk) {
                                        this.lz.setLimit(v1);
                                        this.lzma.decode();
                                        if (!this.rc.isInBufferOK()) {
                                            throw new org.tukaani.xz.CorruptedInputException();
                                        }
                                    } else {
                                        this.lz.copyUncompressed(this.in, v1);
                                    }
                                    int v0 = this.lz.flush(p8, p9);
                                    p9 += v0;
                                    p10 -= v0;
                                    v3 += v0;
                                    this.uncompressedSize = (this.uncompressedSize - v0);
                                    if ((this.uncompressedSize == 0) && ((!this.rc.isFinished()) || (this.lz.hasPending()))) {
                                        throw new org.tukaani.xz.CorruptedInputException();
                                    }
                                } catch (java.io.IOException v2) {
                                    this.exception = v2;
                                    throw v2;
                                }
                            }
                        } else {
                            v3 = -1;
                        }
                    } else {
                        throw this.exception;
                    }
                } else {
                    throw new org.tukaani.xz.XZIOException("Stream closed");
                }
            } else {
                v3 = 0;
            }
            return v3;
        } else {
            throw new IndexOutOfBoundsException();
        }
    }
}
