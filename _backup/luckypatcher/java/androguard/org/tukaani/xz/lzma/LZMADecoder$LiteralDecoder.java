package org.tukaani.xz.lzma;
 class LZMADecoder$LiteralDecoder extends org.tukaani.xz.lzma.LZMACoder$LiteralCoder {
    org.tukaani.xz.lzma.LZMADecoder$LiteralDecoder$LiteralSubdecoder[] subdecoders;
    final synthetic org.tukaani.xz.lzma.LZMADecoder this$0;

    LZMADecoder$LiteralDecoder(org.tukaani.xz.lzma.LZMADecoder p5, int p6, int p7)
    {
        this.this$0 = p5;
        this(p5, p6, p7);
        org.tukaani.xz.lzma.LZMADecoder$LiteralDecoder$LiteralSubdecoder[] v1_2 = new org.tukaani.xz.lzma.LZMADecoder$LiteralDecoder$LiteralSubdecoder[(1 << (p6 + p7))];
        this.subdecoders = v1_2;
        int v0 = 0;
        while (v0 < this.subdecoders.length) {
            this.subdecoders[v0] = new org.tukaani.xz.lzma.LZMADecoder$LiteralDecoder$LiteralSubdecoder(this, 0);
            v0++;
        }
        return;
    }

    void decode()
    {
        this.subdecoders[this.getSubcoderIndex(org.tukaani.xz.lzma.LZMADecoder.access$200(this.this$0).getByte(0), org.tukaani.xz.lzma.LZMADecoder.access$200(this.this$0).getPos())].decode();
        return;
    }

    void reset()
    {
        int v0 = 0;
        while (v0 < this.subdecoders.length) {
            this.subdecoders[v0].reset();
            v0++;
        }
        return;
    }
}
