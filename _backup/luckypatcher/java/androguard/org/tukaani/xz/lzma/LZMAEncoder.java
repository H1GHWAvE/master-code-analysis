package org.tukaani.xz.lzma;
public abstract class LZMAEncoder extends org.tukaani.xz.lzma.LZMACoder {
    static final synthetic boolean $assertionsDisabled = False;
    private static final int ALIGN_PRICE_UPDATE_INTERVAL = 16;
    private static final int DIST_PRICE_UPDATE_INTERVAL = 128;
    private static final int LZMA2_COMPRESSED_LIMIT = 65510;
    private static final int LZMA2_UNCOMPRESSED_LIMIT = 2096879;
    public static final int MODE_FAST = 1;
    public static final int MODE_NORMAL = 2;
    private int alignPriceCount;
    private final int[] alignPrices;
    int back;
    private int distPriceCount;
    private final int[][] distSlotPrices;
    private final int distSlotPricesSize;
    private final int[][] fullDistPrices;
    final org.tukaani.xz.lzma.LZMAEncoder$LiteralEncoder literalEncoder;
    final org.tukaani.xz.lz.LZEncoder lz;
    final org.tukaani.xz.lzma.LZMAEncoder$LengthEncoder matchLenEncoder;
    final int niceLen;
    private final org.tukaani.xz.rangecoder.RangeEncoder rc;
    int readAhead;
    final org.tukaani.xz.lzma.LZMAEncoder$LengthEncoder repLenEncoder;
    private int uncompressedSize;

    static LZMAEncoder()
    {
        int v0_2;
        if (org.tukaani.xz.lzma.LZMAEncoder.desiredAssertionStatus()) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        org.tukaani.xz.lzma.LZMAEncoder.$assertionsDisabled = v0_2;
        return;
    }

    LZMAEncoder(org.tukaani.xz.rangecoder.RangeEncoder p5, org.tukaani.xz.lz.LZEncoder p6, int p7, int p8, int p9, int p10, int p11)
    {
        this(p9);
        this.distPriceCount = 0;
        this.alignPriceCount = 0;
        this.fullDistPrices = ((int[][]) reflect.Array.newInstance(Integer.TYPE, new int[] {4, 128})));
        int[][] v0_5 = new int[16];
        this.alignPrices = v0_5;
        this.back = 0;
        this.readAhead = -1;
        this.uncompressedSize = 0;
        this.rc = p5;
        this.lz = p6;
        this.niceLen = p11;
        this.literalEncoder = new org.tukaani.xz.lzma.LZMAEncoder$LiteralEncoder(this, p7, p8);
        this.matchLenEncoder = new org.tukaani.xz.lzma.LZMAEncoder$LengthEncoder(this, p9, p11);
        this.repLenEncoder = new org.tukaani.xz.lzma.LZMAEncoder$LengthEncoder(this, p9, p11);
        this.distSlotPricesSize = (org.tukaani.xz.lzma.LZMAEncoder.getDistSlot((p10 - 1)) + 1);
        this.distSlotPrices = ((int[][]) reflect.Array.newInstance(Integer.TYPE, new int[] {4, this.distSlotPricesSize})));
        this.reset();
        return;
    }

    static synthetic org.tukaani.xz.rangecoder.RangeEncoder access$100(org.tukaani.xz.lzma.LZMAEncoder p1)
    {
        return p1.rc;
    }

    private boolean encodeInit()
    {
        AssertionError v0_0 = 0;
        if ((org.tukaani.xz.lzma.LZMAEncoder.$assertionsDisabled) || (this.readAhead == -1)) {
            if (this.lz.hasEnoughData(0)) {
                this.skip(1);
                this.rc.encodeBit(this.isMatch[this.state.get()], 0, 0);
                this.literalEncoder.encodeInit();
                this.readAhead = (this.readAhead - 1);
                if ((org.tukaani.xz.lzma.LZMAEncoder.$assertionsDisabled) || (this.readAhead == -1)) {
                    this.uncompressedSize = (this.uncompressedSize + 1);
                    if ((org.tukaani.xz.lzma.LZMAEncoder.$assertionsDisabled) || (this.uncompressedSize == 1)) {
                        v0_0 = 1;
                    } else {
                        throw new AssertionError();
                    }
                } else {
                    throw new AssertionError();
                }
            }
            return v0_0;
        } else {
            throw new AssertionError();
        }
    }

    private void encodeMatch(int p11, int p12, int p13)
    {
        this.state.updateMatch();
        this.matchLenEncoder.encode(p12, p13);
        int v2 = org.tukaani.xz.lzma.LZMAEncoder.getDistSlot(p11);
        this.rc.encodeBitTree(this.distSlots[org.tukaani.xz.lzma.LZMAEncoder.getDistState(p12)], v2);
        if (v2 >= 4) {
            int v3 = ((v2 >> 1) - 1);
            int v1 = (p11 - (((v2 & 1) | 2) << v3));
            if (v2 >= 14) {
                this.rc.encodeDirectBits((v1 >> 4), (v3 - 4));
                this.rc.encodeReverseBitTree(this.distAlign, (v1 & 15));
                this.alignPriceCount = (this.alignPriceCount - 1);
            } else {
                this.rc.encodeReverseBitTree(this.distSpecial[(v2 - 4)], v1);
            }
        }
        this.reps[3] = this.reps[2];
        this.reps[2] = this.reps[1];
        this.reps[1] = this.reps[0];
        this.reps[0] = p11;
        this.distPriceCount = (this.distPriceCount - 1);
        return;
    }

    private void encodeRepMatch(int p10, int p11, int p12)
    {
        org.tukaani.xz.lzma.State v1_0 = 0;
        if (p10 != 0) {
            int v0 = this.reps[p10];
            this.rc.encodeBit(this.isRep0, this.state.get(), 1);
            if (p10 != 1) {
                this.rc.encodeBit(this.isRep1, this.state.get(), 1);
                this.rc.encodeBit(this.isRep2, this.state.get(), (p10 - 2));
                if (p10 == 3) {
                    this.reps[3] = this.reps[2];
                }
                this.reps[2] = this.reps[1];
            } else {
                this.rc.encodeBit(this.isRep1, this.state.get(), 0);
            }
            this.reps[1] = this.reps[0];
            this.reps[0] = v0;
        } else {
            this.rc.encodeBit(this.isRep0, this.state.get(), 0);
            if (p11 != 1) {
                v1_0 = 1;
            }
            this.rc.encodeBit(this.isRep0Long[this.state.get()], p12, v1_0);
        }
        if (p11 != 1) {
            this.repLenEncoder.encode(p11, p12);
            this.state.updateLongRep();
        } else {
            this.state.updateShortRep();
        }
        return;
    }

    private boolean encodeSymbol()
    {
        AssertionError v2_0 = 0;
        if (this.lz.hasEnoughData((this.readAhead + 1))) {
            int v0 = this.getNextSymbol();
            if ((org.tukaani.xz.lzma.LZMAEncoder.$assertionsDisabled) || (this.readAhead >= 0)) {
                int v1 = ((this.lz.getPos() - this.readAhead) & this.posMask);
                if (this.back != -1) {
                    this.rc.encodeBit(this.isMatch[this.state.get()], v1, 1);
                    if (this.back >= 4) {
                        if ((org.tukaani.xz.lzma.LZMAEncoder.$assertionsDisabled) || (this.lz.getMatchLen((- this.readAhead), (this.back - 4), v0) == v0)) {
                            this.rc.encodeBit(this.isRep, this.state.get(), 0);
                            this.encodeMatch((this.back - 4), v0, v1);
                        } else {
                            throw new AssertionError();
                        }
                    } else {
                        if ((org.tukaani.xz.lzma.LZMAEncoder.$assertionsDisabled) || (this.lz.getMatchLen((- this.readAhead), this.reps[this.back], v0) == v0)) {
                            this.rc.encodeBit(this.isRep, this.state.get(), 1);
                            this.encodeRepMatch(this.back, v0, v1);
                        } else {
                            throw new AssertionError();
                        }
                    }
                } else {
                    if ((org.tukaani.xz.lzma.LZMAEncoder.$assertionsDisabled) || (v0 == 1)) {
                        this.rc.encodeBit(this.isMatch[this.state.get()], v1, 0);
                        this.literalEncoder.encode();
                    } else {
                        throw new AssertionError();
                    }
                }
                this.readAhead = (this.readAhead - v0);
                this.uncompressedSize = (this.uncompressedSize + v0);
                v2_0 = 1;
            } else {
                throw new AssertionError();
            }
        }
        return v2_0;
    }

    public static int getDistSlot(int p4)
    {
        if (p4 > 4) {
            int v1 = p4;
            int v0 = 31;
            if ((-65536 & p4) == 0) {
                v1 = (p4 << 16);
                v0 = 15;
            }
            if ((-16777216 & v1) == 0) {
                v1 <<= 8;
                v0 -= 8;
            }
            if ((-268435456 & v1) == 0) {
                v1 <<= 4;
                v0 -= 4;
            }
            if ((-1073741824 & v1) == 0) {
                v1 <<= 2;
                v0 -= 2;
            }
            if ((-2147483648 & v1) == 0) {
                v0--;
            }
            p4 = ((v0 << 1) + ((p4 >> (v0 - 1)) & 1));
        }
        return p4;
    }

    public static org.tukaani.xz.lzma.LZMAEncoder getInstance(org.tukaani.xz.rangecoder.RangeEncoder p10, int p11, int p12, int p13, int p14, int p15, int p16, int p17, int p18, int p19)
    {
        org.tukaani.xz.lzma.LZMAEncoderNormal v0_1;
        switch (p14) {
            case 1:
                v0_1 = new org.tukaani.xz.lzma.LZMAEncoderFast(p10, p11, p12, p13, p15, p16, p17, p18, p19);
                break;
            case 2:
                v0_1 = new org.tukaani.xz.lzma.LZMAEncoderNormal(p10, p11, p12, p13, p15, p16, p17, p18, p19);
                break;
            default:
                throw new IllegalArgumentException();
        }
        return v0_1;
    }

    public static int getMemoryUsage(int p2, int p3, int p4, int p5)
    {
        int v0_1;
        switch (p2) {
            case 1:
                v0_1 = (80 + org.tukaani.xz.lzma.LZMAEncoderFast.getMemoryUsage(p3, p4, p5));
                break;
            case 2:
                v0_1 = (80 + org.tukaani.xz.lzma.LZMAEncoderNormal.getMemoryUsage(p3, p4, p5));
                break;
            default:
                throw new IllegalArgumentException();
        }
        return v0_1;
    }

    private void updateAlignPrices()
    {
        this.alignPriceCount = 16;
        int v0 = 0;
        while (v0 < 16) {
            this.alignPrices[v0] = org.tukaani.xz.rangecoder.RangeEncoder.getReverseBitTreePrice(this.distAlign, v0);
            v0++;
        }
        return;
    }

    private void updateDistPrices()
    {
        this.distPriceCount = 128;
        int v5_0 = 0;
        while (v5_0 < 4) {
            int v4_1 = 0;
            while (v4_1 < this.distSlotPricesSize) {
                this.distSlotPrices[v5_0][v4_1] = org.tukaani.xz.rangecoder.RangeEncoder.getBitTreePrice(this.distSlots[v5_0], v4_1);
                v4_1++;
            }
            int v4_2 = 14;
            while (v4_2 < this.distSlotPricesSize) {
                int[] v10_20 = this.distSlotPrices[v5_0];
                v10_20[v4_2] = (v10_20[v4_2] + org.tukaani.xz.rangecoder.RangeEncoder.getDirectBitsPrice((((v4_2 >> 1) - 1) - 4)));
                v4_2++;
            }
            int v2_1 = 0;
            while (v2_1 < 4) {
                this.fullDistPrices[v5_0][v2_1] = this.distSlotPrices[v5_0][v2_1];
                v2_1++;
            }
            v5_0++;
        }
        int v2_0 = 4;
        int v4_0 = 4;
        while (v4_0 < 14) {
            int v0 = (((v4_0 & 1) | 2) << ((v4_0 >> 1) - 1));
            int v8 = this.distSpecial[(v4_0 - 4)].length;
            int v7 = 0;
            while (v7 < v8) {
                int v9 = org.tukaani.xz.rangecoder.RangeEncoder.getReverseBitTreePrice(this.distSpecial[(v4_0 - 4)], (v2_0 - v0));
                int v5_1 = 0;
                while (v5_1 < 4) {
                    this.fullDistPrices[v5_1][v2_0] = (this.distSlotPrices[v5_1][v4_0] + v9);
                    v5_1++;
                }
                v2_0++;
                v7++;
            }
            v4_0++;
        }
        if ((org.tukaani.xz.lzma.LZMAEncoder.$assertionsDisabled) || (v2_0 == 128)) {
            return;
        } else {
            throw new AssertionError();
        }
    }

    public boolean encodeForLZMA2()
    {
        int v0 = 0;
        if ((this.lz.isStarted()) || (this.encodeInit())) {
            while ((this.uncompressedSize <= 2096879) && (this.rc.getPendingSize() <= 65510)) {
                if (!this.encodeSymbol()) {
                }
            }
            v0 = 1;
        }
        return v0;
    }

    int getAnyMatchPrice(org.tukaani.xz.lzma.State p3, int p4)
    {
        return org.tukaani.xz.rangecoder.RangeEncoder.getBitPrice(this.isMatch[p3.get()][p4], 1);
    }

    int getAnyRepPrice(int p3, org.tukaani.xz.lzma.State p4)
    {
        return (org.tukaani.xz.rangecoder.RangeEncoder.getBitPrice(this.isRep[p4.get()], 1) + p3);
    }

    public org.tukaani.xz.lz.LZEncoder getLZEncoder()
    {
        return this.lz;
    }

    int getLongRepAndLenPrice(int p5, int p6, org.tukaani.xz.lzma.State p7, int p8)
    {
        return (this.repLenEncoder.getPrice(p6, p8) + this.getLongRepPrice(this.getAnyRepPrice(this.getAnyMatchPrice(p7, p8), p7), p5, p7, p8));
    }

    int getLongRepPrice(int p6, int p7, org.tukaani.xz.lzma.State p8, int p9)
    {
        int v0_2;
        if (p7 != 0) {
            int v0_1 = (p6 + org.tukaani.xz.rangecoder.RangeEncoder.getBitPrice(this.isRep0[p8.get()], 1));
            if (p7 != 1) {
                v0_2 = (v0_1 + (org.tukaani.xz.rangecoder.RangeEncoder.getBitPrice(this.isRep1[p8.get()], 1) + org.tukaani.xz.rangecoder.RangeEncoder.getBitPrice(this.isRep2[p8.get()], (p7 - 2))));
            } else {
                v0_2 = (v0_1 + org.tukaani.xz.rangecoder.RangeEncoder.getBitPrice(this.isRep1[p8.get()], 0));
            }
        } else {
            v0_2 = (p6 + (org.tukaani.xz.rangecoder.RangeEncoder.getBitPrice(this.isRep0[p8.get()], 0) + org.tukaani.xz.rangecoder.RangeEncoder.getBitPrice(this.isRep0Long[p8.get()][p9], 1)));
        }
        return v0_2;
    }

    int getMatchAndLenPrice(int p7, int p8, int p9, int p10)
    {
        int v2_1;
        int v2_0 = (p7 + this.matchLenEncoder.getPrice(p9, p10));
        int v1 = org.tukaani.xz.lzma.LZMAEncoder.getDistState(p9);
        if (p8 >= 128) {
            v2_1 = (v2_0 + (this.distSlotPrices[v1][org.tukaani.xz.lzma.LZMAEncoder.getDistSlot(p8)] + this.alignPrices[(p8 & 15)]));
        } else {
            v2_1 = (v2_0 + this.fullDistPrices[v1][p8]);
        }
        return v2_1;
    }

    org.tukaani.xz.lz.Matches getMatches()
    {
        this.readAhead = (this.readAhead + 1);
        org.tukaani.xz.lz.Matches v0 = this.lz.getMatches();
        if ((org.tukaani.xz.lzma.LZMAEncoder.$assertionsDisabled) || (this.lz.verifyMatches(v0))) {
            return v0;
        } else {
            throw new AssertionError();
        }
    }

    abstract int getNextSymbol();

    int getNormalMatchPrice(int p3, org.tukaani.xz.lzma.State p4)
    {
        return (org.tukaani.xz.rangecoder.RangeEncoder.getBitPrice(this.isRep[p4.get()], 0) + p3);
    }

    int getShortRepPrice(int p5, org.tukaani.xz.lzma.State p6, int p7)
    {
        return ((org.tukaani.xz.rangecoder.RangeEncoder.getBitPrice(this.isRep0[p6.get()], 0) + p5) + org.tukaani.xz.rangecoder.RangeEncoder.getBitPrice(this.isRep0Long[p6.get()][p7], 0));
    }

    public int getUncompressedSize()
    {
        return this.uncompressedSize;
    }

    public void reset()
    {
        super.reset();
        this.literalEncoder.reset();
        this.matchLenEncoder.reset();
        this.repLenEncoder.reset();
        this.distPriceCount = 0;
        this.alignPriceCount = 0;
        this.uncompressedSize = (this.uncompressedSize + (this.readAhead + 1));
        this.readAhead = -1;
        return;
    }

    public void resetUncompressedSize()
    {
        this.uncompressedSize = 0;
        return;
    }

    void skip(int p2)
    {
        this.readAhead = (this.readAhead + p2);
        this.lz.skip(p2);
        return;
    }

    void updatePrices()
    {
        if (this.distPriceCount <= 0) {
            this.updateDistPrices();
        }
        if (this.alignPriceCount <= 0) {
            this.updateAlignPrices();
        }
        this.matchLenEncoder.updatePrices();
        this.repLenEncoder.updatePrices();
        return;
    }
}
