package org.tukaani.xz.lzma;
 class LZMADecoder$LiteralDecoder$LiteralSubdecoder extends org.tukaani.xz.lzma.LZMACoder$LiteralCoder$LiteralSubcoder {
    final synthetic org.tukaani.xz.lzma.LZMADecoder$LiteralDecoder this$1;

    private LZMADecoder$LiteralDecoder$LiteralSubdecoder(org.tukaani.xz.lzma.LZMADecoder$LiteralDecoder p1)
    {
        this.this$1 = p1;
        this(p1);
        return;
    }

    synthetic LZMADecoder$LiteralDecoder$LiteralSubdecoder(org.tukaani.xz.lzma.LZMADecoder$LiteralDecoder p1, org.tukaani.xz.lzma.LZMADecoder$1 p2)
    {
        this(p1);
        return;
    }

    void decode()
    {
        int v4;
        v4 = 1;
        if (!this.this$1.this$0.state.isLiteral()) {
            int v2 = org.tukaani.xz.lzma.LZMADecoder.access$200(this.this$1.this$0).getByte(this.this$1.this$0.reps[0]);
            int v3 = 256;
            do {
                v2 <<= 1;
                int v1 = (v2 & v3);
                int v0 = org.tukaani.xz.lzma.LZMADecoder.access$300(this.this$1.this$0).decodeBit(this.probs, ((v3 + v1) + v4));
                v4 = ((v4 << 1) | v0);
                v3 &= ((0 - v0) ^ (v1 ^ -1));
            } while(v4 < 256);
        } else {
            do {
                v4 = ((v4 << 1) | org.tukaani.xz.lzma.LZMADecoder.access$300(this.this$1.this$0).decodeBit(this.probs, v4));
            } while(v4 < 256);
        }
        org.tukaani.xz.lzma.LZMADecoder.access$200(this.this$1.this$0).putByte(((byte) v4));
        this.this$1.this$0.state.updateLiteral();
        return;
    }
}
