package org.tukaani.xz.lzma;
abstract class LZMACoder$LiteralCoder$LiteralSubcoder {
    final short[] probs;
    final synthetic org.tukaani.xz.lzma.LZMACoder$LiteralCoder this$1;

    LZMACoder$LiteralCoder$LiteralSubcoder(org.tukaani.xz.lzma.LZMACoder$LiteralCoder p2)
    {
        this.this$1 = p2;
        short[] v0_1 = new short[768];
        this.probs = v0_1;
        return;
    }

    void reset()
    {
        org.tukaani.xz.rangecoder.RangeCoder.initProbs(this.probs);
        return;
    }
}
