package org.tukaani.xz.lzma;
 class LZMAEncoder$LiteralEncoder extends org.tukaani.xz.lzma.LZMACoder$LiteralCoder {
    static final synthetic boolean $assertionsDisabled;
    org.tukaani.xz.lzma.LZMAEncoder$LiteralEncoder$LiteralSubencoder[] subencoders;
    final synthetic org.tukaani.xz.lzma.LZMAEncoder this$0;

    static LZMAEncoder$LiteralEncoder()
    {
        int v0_2;
        if (org.tukaani.xz.lzma.LZMAEncoder.desiredAssertionStatus()) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        org.tukaani.xz.lzma.LZMAEncoder$LiteralEncoder.$assertionsDisabled = v0_2;
        return;
    }

    LZMAEncoder$LiteralEncoder(org.tukaani.xz.lzma.LZMAEncoder p5, int p6, int p7)
    {
        this.this$0 = p5;
        this(p5, p6, p7);
        org.tukaani.xz.lzma.LZMAEncoder$LiteralEncoder$LiteralSubencoder[] v1_2 = new org.tukaani.xz.lzma.LZMAEncoder$LiteralEncoder$LiteralSubencoder[(1 << (p6 + p7))];
        this.subencoders = v1_2;
        int v0 = 0;
        while (v0 < this.subencoders.length) {
            this.subencoders[v0] = new org.tukaani.xz.lzma.LZMAEncoder$LiteralEncoder$LiteralSubencoder(this, 0);
            v0++;
        }
        return;
    }

    void encode()
    {
        if ((org.tukaani.xz.lzma.LZMAEncoder$LiteralEncoder.$assertionsDisabled) || (this.this$0.readAhead >= 0)) {
            this.subencoders[this.getSubcoderIndex(this.this$0.lz.getByte((this.this$0.readAhead + 1)), (this.this$0.lz.getPos() - this.this$0.readAhead))].encode();
            return;
        } else {
            throw new AssertionError();
        }
    }

    void encodeInit()
    {
        if ((org.tukaani.xz.lzma.LZMAEncoder$LiteralEncoder.$assertionsDisabled) || (this.this$0.readAhead >= 0)) {
            this.subencoders[0].encode();
            return;
        } else {
            throw new AssertionError();
        }
    }

    int getPrice(int p5, int p6, int p7, int p8, org.tukaani.xz.lzma.State p9)
    {
        int v2_7;
        int v1_0 = org.tukaani.xz.rangecoder.RangeEncoder.getBitPrice(this.this$0.isMatch[p9.get()][(this.this$0.posMask & p8)], 0);
        int v0 = this.getSubcoderIndex(p7, p8);
        if (!p9.isLiteral()) {
            v2_7 = this.subencoders[v0].getMatchedPrice(p5, p6);
        } else {
            v2_7 = this.subencoders[v0].getNormalPrice(p5);
        }
        return (v1_0 + v2_7);
    }

    void reset()
    {
        int v0 = 0;
        while (v0 < this.subencoders.length) {
            this.subencoders[v0].reset();
            v0++;
        }
        return;
    }
}
