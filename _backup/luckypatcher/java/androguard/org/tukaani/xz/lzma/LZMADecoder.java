package org.tukaani.xz.lzma;
public final class LZMADecoder extends org.tukaani.xz.lzma.LZMACoder {
    private final org.tukaani.xz.lzma.LZMADecoder$LiteralDecoder literalDecoder;
    private final org.tukaani.xz.lz.LZDecoder lz;
    private final org.tukaani.xz.lzma.LZMADecoder$LengthDecoder matchLenDecoder;
    private final org.tukaani.xz.rangecoder.RangeDecoder rc;
    private final org.tukaani.xz.lzma.LZMADecoder$LengthDecoder repLenDecoder;

    public LZMADecoder(org.tukaani.xz.lz.LZDecoder p3, org.tukaani.xz.rangecoder.RangeDecoder p4, int p5, int p6, int p7)
    {
        this(p7);
        this.matchLenDecoder = new org.tukaani.xz.lzma.LZMADecoder$LengthDecoder(this, 0);
        this.repLenDecoder = new org.tukaani.xz.lzma.LZMADecoder$LengthDecoder(this, 0);
        this.lz = p3;
        this.rc = p4;
        this.literalDecoder = new org.tukaani.xz.lzma.LZMADecoder$LiteralDecoder(this, p5, p6);
        this.reset();
        return;
    }

    static synthetic org.tukaani.xz.lz.LZDecoder access$200(org.tukaani.xz.lzma.LZMADecoder p1)
    {
        return p1.lz;
    }

    static synthetic org.tukaani.xz.rangecoder.RangeDecoder access$300(org.tukaani.xz.lzma.LZMADecoder p1)
    {
        return p1.rc;
    }

    private int decodeMatch(int p10)
    {
        this.state.updateMatch();
        this.reps[3] = this.reps[2];
        this.reps[2] = this.reps[1];
        this.reps[1] = this.reps[0];
        int v1 = this.matchLenDecoder.decode(p10);
        int v0 = this.rc.decodeBitTree(this.distSlots[org.tukaani.xz.lzma.LZMADecoder.getDistState(v1)]);
        if (v0 >= 4) {
            int v2 = ((v0 >> 1) - 1);
            this.reps[0] = (((v0 & 1) | 2) << v2);
            if (v0 >= 14) {
                int[] v3_10 = this.reps;
                v3_10[0] = (v3_10[0] | (this.rc.decodeDirectBits((v2 - 4)) << 4));
                int[] v3_11 = this.reps;
                v3_11[0] = (v3_11[0] | this.rc.decodeReverseBitTree(this.distAlign));
            } else {
                int[] v3_12 = this.reps;
                v3_12[0] = (v3_12[0] | this.rc.decodeReverseBitTree(this.distSpecial[(v0 - 4)]));
            }
        } else {
            this.reps[0] = v0;
        }
        return v1;
    }

    private int decodeRepMatch(int p9)
    {
        int[] v1_0 = 1;
        if (this.rc.decodeBit(this.isRep0, this.state.get()) != 0) {
            int v0;
            if (this.rc.decodeBit(this.isRep1, this.state.get()) != 0) {
                if (this.rc.decodeBit(this.isRep2, this.state.get()) != 0) {
                    v0 = this.reps[3];
                    this.reps[3] = this.reps[2];
                } else {
                    v0 = this.reps[2];
                }
                this.reps[2] = this.reps[1];
            } else {
                v0 = this.reps[1];
            }
            this.reps[1] = this.reps[0];
            this.reps[0] = v0;
            this.state.updateLongRep();
            v1_0 = this.repLenDecoder.decode(p9);
        } else {
            if (this.rc.decodeBit(this.isRep0Long[this.state.get()], p9) != 0) {
            } else {
                this.state.updateShortRep();
            }
        }
        return v1_0;
    }

    public void decode()
    {
        this.lz.repeatPending();
        while (this.lz.hasSpace()) {
            int v1 = (this.lz.getPos() & this.posMask);
            if (this.rc.decodeBit(this.isMatch[this.state.get()], v1) != 0) {
                int v0;
                if (this.rc.decodeBit(this.isRep, this.state.get()) != 0) {
                    v0 = this.decodeRepMatch(v1);
                } else {
                    v0 = this.decodeMatch(v1);
                }
                this.lz.repeat(this.reps[0], v0);
            } else {
                this.literalDecoder.decode();
            }
        }
        this.rc.normalize();
        return;
    }

    public boolean endMarkerDetected()
    {
        int v0 = 0;
        if (this.reps[0] == -1) {
            v0 = 1;
        }
        return v0;
    }

    public void reset()
    {
        super.reset();
        this.literalDecoder.reset();
        this.matchLenDecoder.reset();
        this.repLenDecoder.reset();
        return;
    }
}
