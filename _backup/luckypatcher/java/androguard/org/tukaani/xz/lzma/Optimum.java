package org.tukaani.xz.lzma;
final class Optimum {
    private static final int INFINITY_PRICE = 1073741824;
    int backPrev;
    int backPrev2;
    boolean hasPrev2;
    int optPrev;
    int optPrev2;
    boolean prev1IsLiteral;
    int price;
    final int[] reps;
    final org.tukaani.xz.lzma.State state;

    Optimum()
    {
        this.state = new org.tukaani.xz.lzma.State();
        int[] v0_3 = new int[4];
        this.reps = v0_3;
        return;
    }

    void reset()
    {
        this.price = 1073741824;
        return;
    }

    void set1(int p2, int p3, int p4)
    {
        this.price = p2;
        this.optPrev = p3;
        this.backPrev = p4;
        this.prev1IsLiteral = 0;
        return;
    }

    void set2(int p2, int p3, int p4)
    {
        this.price = p2;
        this.optPrev = (p3 + 1);
        this.backPrev = p4;
        this.prev1IsLiteral = 1;
        this.hasPrev2 = 0;
        return;
    }

    void set3(int p3, int p4, int p5, int p6, int p7)
    {
        this.price = p3;
        this.optPrev = ((p4 + p6) + 1);
        this.backPrev = p7;
        this.prev1IsLiteral = 1;
        this.hasPrev2 = 1;
        this.optPrev2 = p4;
        this.backPrev2 = p5;
        return;
    }
}
