package org.tukaani.xz.lzma;
final class State {
    private static final int LIT_LIT = 0;
    private static final int LIT_LONGREP = 8;
    private static final int LIT_MATCH = 7;
    private static final int LIT_SHORTREP = 9;
    private static final int LIT_STATES = 7;
    private static final int MATCH_LIT = 4;
    private static final int MATCH_LIT_LIT = 1;
    private static final int NONLIT_MATCH = 10;
    private static final int NONLIT_REP = 11;
    private static final int REP_LIT = 5;
    private static final int REP_LIT_LIT = 2;
    private static final int SHORTREP_LIT = 6;
    private static final int SHORTREP_LIT_LIT = 3;
    static final int STATES = 12;
    private int state;

    State()
    {
        return;
    }

    State(org.tukaani.xz.lzma.State p2)
    {
        this.state = p2.state;
        return;
    }

    int get()
    {
        return this.state;
    }

    boolean isLiteral()
    {
        int v0_1;
        if (this.state >= 7) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    void reset()
    {
        this.state = 0;
        return;
    }

    void set(org.tukaani.xz.lzma.State p2)
    {
        this.state = p2.state;
        return;
    }

    void updateLiteral()
    {
        if (this.state > 3) {
            if (this.state > 9) {
                this.state = (this.state - 6);
            } else {
                this.state = (this.state - 3);
            }
        } else {
            this.state = 0;
        }
        return;
    }

    void updateLongRep()
    {
        int v0_1;
        if (this.state >= 7) {
            v0_1 = 11;
        } else {
            v0_1 = 8;
        }
        this.state = v0_1;
        return;
    }

    void updateMatch()
    {
        int v0 = 7;
        if (this.state >= 7) {
            v0 = 10;
        }
        this.state = v0;
        return;
    }

    void updateShortRep()
    {
        int v0_1;
        if (this.state >= 7) {
            v0_1 = 11;
        } else {
            v0_1 = 9;
        }
        this.state = v0_1;
        return;
    }
}
