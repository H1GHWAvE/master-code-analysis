package org.tukaani.xz.lzma;
abstract class LZMACoder$LiteralCoder {
    private final int lc;
    private final int literalPosMask;
    final synthetic org.tukaani.xz.lzma.LZMACoder this$0;

    LZMACoder$LiteralCoder(org.tukaani.xz.lzma.LZMACoder p2, int p3, int p4)
    {
        this.this$0 = p2;
        this.lc = p3;
        this.literalPosMask = ((1 << p4) - 1);
        return;
    }

    final int getSubcoderIndex(int p5, int p6)
    {
        return ((p5 >> (8 - this.lc)) + ((this.literalPosMask & p6) << this.lc));
    }
}
