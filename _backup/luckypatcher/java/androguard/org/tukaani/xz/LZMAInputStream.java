package org.tukaani.xz;
public class LZMAInputStream extends java.io.InputStream {
    static final synthetic boolean $assertionsDisabled = False;
    public static final int DICT_SIZE_MAX = 2147483632;
    private boolean endReached;
    private java.io.IOException exception;
    private java.io.InputStream in;
    private org.tukaani.xz.lz.LZDecoder lz;
    private org.tukaani.xz.lzma.LZMADecoder lzma;
    private org.tukaani.xz.rangecoder.RangeDecoderFromStream rc;
    private long remainingSize;
    private final byte[] tempBuf;

    static LZMAInputStream()
    {
        int v0_2;
        if (org.tukaani.xz.LZMAInputStream.desiredAssertionStatus()) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        org.tukaani.xz.LZMAInputStream.$assertionsDisabled = v0_2;
        return;
    }

    public LZMAInputStream(java.io.InputStream p2)
    {
        this(p2, -1);
        return;
    }

    public LZMAInputStream(java.io.InputStream p12, int p13)
    {
        this.endReached = 0;
        org.tukaani.xz.MemoryLimitException v0_2 = new byte[1];
        this.tempBuf = v0_2;
        this.exception = 0;
        java.io.DataInputStream v8_1 = new java.io.DataInputStream(p12);
        byte v4 = v8_1.readByte();
        int v5 = 0;
        int v7_0 = 0;
        while (v7_0 < 4) {
            v5 |= (v8_1.readUnsignedByte() << (v7_0 * 8));
            v7_0++;
        }
        long v2 = 0;
        int v7_1 = 0;
        while (v7_1 < 8) {
            v2 |= (((long) v8_1.readUnsignedByte()) << (v7_1 * 8));
            v7_1++;
        }
        int v9 = org.tukaani.xz.LZMAInputStream.getMemoryUsage(v5, v4);
        if ((p13 == -1) || (v9 <= p13)) {
            this.initialize(p12, v2, v4, v5, 0);
            return;
        } else {
            throw new org.tukaani.xz.MemoryLimitException(v9, p13);
        }
    }

    public LZMAInputStream(java.io.InputStream p8, long p9, byte p11, int p12)
    {
        this.endReached = 0;
        void v0_2 = new byte[1];
        this.tempBuf = v0_2;
        this.exception = 0;
        this.initialize(p8, p9, p11, p12, 0);
        return;
    }

    public LZMAInputStream(java.io.InputStream p2, long p3, byte p5, int p6, byte[] p7)
    {
        this.endReached = 0;
        int v0_2 = new byte[1];
        this.tempBuf = v0_2;
        this.exception = 0;
        this.initialize(p2, p3, p5, p6, p7);
        return;
    }

    public LZMAInputStream(java.io.InputStream p2, long p3, int p5, int p6, int p7, int p8, byte[] p9)
    {
        this.endReached = 0;
        int v0_2 = new byte[1];
        this.tempBuf = v0_2;
        this.exception = 0;
        this.initialize(p2, p3, p5, p6, p7, p8, p9);
        return;
    }

    private static int getDictSize(int p2)
    {
        if ((p2 >= 0) && (p2 <= 2147483632)) {
            if (p2 < 4096) {
                p2 = 4096;
            }
            return ((p2 + 15) & -16);
        } else {
            throw new IllegalArgumentException("LZMA dictionary is too big for this implementation");
        }
    }

    public static int getMemoryUsage(int p5, byte p6)
    {
        if ((p5 >= 0) && (p5 <= 2147483632)) {
            int v2_0 = (p6 & 255);
            if (v2_0 <= 224) {
                int v2_1 = (v2_0 % 45);
                int v1 = (v2_1 / 9);
                return org.tukaani.xz.LZMAInputStream.getMemoryUsage(p5, (v2_1 - (v1 * 9)), v1);
            } else {
                throw new org.tukaani.xz.CorruptedInputException("Invalid LZMA properties byte");
            }
        } else {
            throw new org.tukaani.xz.UnsupportedOptionsException("LZMA dictionary is too big for this implementation");
        }
    }

    public static int getMemoryUsage(int p3, int p4, int p5)
    {
        if ((p4 >= 0) && ((p4 <= 8) && ((p5 >= 0) && (p5 <= 4)))) {
            return (((org.tukaani.xz.LZMAInputStream.getDictSize(p3) / 1024) + 10) + ((1536 << (p4 + p5)) / 1024));
        } else {
            throw new IllegalArgumentException("Invalid lc or lp");
        }
    }

    private void initialize(java.io.InputStream p11, long p12, byte p14, int p15, byte[] p16)
    {
        if (p12 >= -1) {
            int v9_0 = (p14 & 255);
            if (v9_0 <= 224) {
                int v6 = (v9_0 / 45);
                int v9_1 = (v9_0 - ((v6 * 9) * 5));
                int v5 = (v9_1 / 9);
                if ((p15 >= 0) && (p15 <= 2147483632)) {
                    this.initialize(p11, p12, (v9_1 - (v5 * 9)), v5, v6, p15, p16);
                    return;
                } else {
                    throw new org.tukaani.xz.UnsupportedOptionsException("LZMA dictionary is too big for this implementation");
                }
            } else {
                throw new org.tukaani.xz.CorruptedInputException("Invalid LZMA properties byte");
            }
        } else {
            throw new org.tukaani.xz.UnsupportedOptionsException("Uncompressed size is too big");
        }
    }

    private void initialize(java.io.InputStream p7, long p8, int p10, int p11, int p12, int p13, byte[] p14)
    {
        if ((p8 >= -1) && ((p10 >= 0) && ((p10 <= 8) && ((p11 >= 0) && ((p11 <= 4) && ((p12 >= 0) && (p12 <= 4))))))) {
            this.in = p7;
            int v13_1 = org.tukaani.xz.LZMAInputStream.getDictSize(p13);
            if ((p8 >= 0) && (((long) v13_1) > p8)) {
                v13_1 = org.tukaani.xz.LZMAInputStream.getDictSize(((int) p8));
            }
            this.lz = new org.tukaani.xz.lz.LZDecoder(org.tukaani.xz.LZMAInputStream.getDictSize(v13_1), p14);
            this.rc = new org.tukaani.xz.rangecoder.RangeDecoderFromStream(p7);
            this.lzma = new org.tukaani.xz.lzma.LZMADecoder(this.lz, this.rc, p10, p11, p12);
            this.remainingSize = p8;
            return;
        } else {
            throw new IllegalArgumentException();
        }
    }

    public void close()
    {
        if (this.in != null) {
            try {
                this.in.close();
                this.in = 0;
            } catch (Throwable v0_2) {
                this.in = 0;
                throw v0_2;
            }
        }
        return;
    }

    public int read()
    {
        int v0_0 = -1;
        if (this.read(this.tempBuf, 0, 1) != -1) {
            v0_0 = (this.tempBuf[0] & 255);
        }
        return v0_0;
    }

    public int read(byte[] p12, int p13, int p14)
    {
        if ((p13 >= 0) && ((p14 >= 0) && (((p13 + p14) >= 0) && ((p13 + p14) <= p12.length)))) {
            org.tukaani.xz.CorruptedInputException v3;
            if (p14 != 0) {
                if (this.in != null) {
                    if (this.exception == null) {
                        if (!this.endReached) {
                            v3 = 0;
                            while (p14 > 0) {
                                int v1 = p14;
                                try {
                                    if ((this.remainingSize >= 0) && (this.remainingSize < ((long) p14))) {
                                        v1 = ((int) this.remainingSize);
                                    }
                                    this.lz.setLimit(v1);
                                    try {
                                        this.lzma.decode();
                                    } catch (org.tukaani.xz.CorruptedInputException v2_0) {
                                        if (this.remainingSize == -1) {
                                            if (this.lzma.endMarkerDetected()) {
                                                this.endReached = 1;
                                                this.rc.normalize();
                                            }
                                        }
                                        throw v2_0;
                                    }
                                    int v0 = this.lz.flush(p12, p13);
                                    p13 += v0;
                                    p14 -= v0;
                                    v3 += v0;
                                    if (this.remainingSize >= 0) {
                                        this.remainingSize = (this.remainingSize - ((long) v0));
                                        if ((org.tukaani.xz.LZMAInputStream.$assertionsDisabled) || (this.remainingSize >= 0)) {
                                            if (this.remainingSize == 0) {
                                                this.endReached = 1;
                                            }
                                        } else {
                                            throw new AssertionError();
                                        }
                                    }
                                    if (this.endReached) {
                                        if ((this.rc.isFinished()) && (!this.lz.hasPending())) {
                                            if (v3 != null) {
                                                break;
                                            }
                                            v3 = -1;
                                            break;
                                        } else {
                                            throw new org.tukaani.xz.CorruptedInputException();
                                        }
                                    }
                                } catch (org.tukaani.xz.CorruptedInputException v2_1) {
                                    this.exception = v2_1;
                                    throw v2_1;
                                }
                            }
                        } else {
                            v3 = -1;
                        }
                    } else {
                        throw this.exception;
                    }
                } else {
                    throw new org.tukaani.xz.XZIOException("Stream closed");
                }
            } else {
                v3 = 0;
            }
            return v3;
        } else {
            throw new IndexOutOfBoundsException();
        }
    }
}
