package org.tukaani.xz.rangecoder;
public final class RangeDecoderFromStream extends org.tukaani.xz.rangecoder.RangeDecoder {
    private final java.io.DataInputStream inData;

    public RangeDecoderFromStream(java.io.InputStream p2)
    {
        this.inData = new java.io.DataInputStream(p2);
        if (this.inData.readUnsignedByte() == 0) {
            this.code = this.inData.readInt();
            this.range = -1;
            return;
        } else {
            throw new org.tukaani.xz.CorruptedInputException();
        }
    }

    public boolean isFinished()
    {
        int v0_1;
        if (this.code != 0) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    public void normalize()
    {
        if ((this.range & -16777216) == 0) {
            this.code = ((this.code << 8) | this.inData.readUnsignedByte());
            this.range = (this.range << 8);
        }
        return;
    }
}
