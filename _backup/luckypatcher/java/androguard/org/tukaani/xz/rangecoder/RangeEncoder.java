package org.tukaani.xz.rangecoder;
public final class RangeEncoder extends org.tukaani.xz.rangecoder.RangeCoder {
    static final synthetic boolean $assertionsDisabled = False;
    private static final int BIT_PRICE_SHIFT_BITS = 4;
    private static final int MOVE_REDUCING_BITS = 4;
    private static final int[] prices;
    private final byte[] buf;
    private int bufPos;
    private byte cache;
    private int cacheSize;
    private long low;
    private int range;

    static RangeEncoder()
    {
        int v4_2;
        if (org.tukaani.xz.rangecoder.RangeEncoder.desiredAssertionStatus()) {
            v4_2 = 0;
        } else {
            v4_2 = 1;
        }
        org.tukaani.xz.rangecoder.RangeEncoder.$assertionsDisabled = v4_2;
        int v4_4 = new int[128];
        org.tukaani.xz.rangecoder.RangeEncoder.prices = v4_4;
        int v1 = 8;
        while (v1 < 2048) {
            int v3 = v1;
            int v0 = 0;
            int v2 = 0;
            while (v2 < 4) {
                v3 *= v3;
                v0 <<= 1;
                while ((-65536 & v3) != 0) {
                    v3 >>= 1;
                    v0++;
                }
                v2++;
            }
            org.tukaani.xz.rangecoder.RangeEncoder.prices[(v1 >> 4)] = (161 - v0);
            v1 += 16;
        }
        return;
    }

    public RangeEncoder(int p2)
    {
        byte[] v0 = new byte[p2];
        this.buf = v0;
        this.reset();
        return;
    }

    public static int getBitPrice(int p2, int p3)
    {
        if ((org.tukaani.xz.rangecoder.RangeEncoder.$assertionsDisabled) || ((p3 == 0) || (p3 == 1))) {
            return org.tukaani.xz.rangecoder.RangeEncoder.prices[((((- p3) & 2047) ^ p2) >> 4)];
        } else {
            throw new AssertionError();
        }
    }

    public static int getBitTreePrice(short[] p3, int p4)
    {
        int v1 = 0;
        int v4_1 = (p4 | p3.length);
        do {
            int v0 = (v4_1 & 1);
            v4_1 >>= 1;
            v1 += org.tukaani.xz.rangecoder.RangeEncoder.getBitPrice(p3[v4_1], v0);
        } while(v4_1 != 1);
        return v1;
    }

    public static int getDirectBitsPrice(int p1)
    {
        return (p1 << 4);
    }

    public static int getReverseBitTreePrice(short[] p4, int p5)
    {
        int v2 = 0;
        int v1 = 1;
        int v5_1 = (p5 | p4.length);
        do {
            int v0 = (v5_1 & 1);
            v5_1 >>= 1;
            v2 += org.tukaani.xz.rangecoder.RangeEncoder.getBitPrice(p4[v1], v0);
            v1 = ((v1 << 1) | v0);
        } while(v5_1 != 1);
        return v2;
    }

    private void shiftLow()
    {
        int v0 = ((int) (this.low >> 32));
        if ((v0 != 0) || (this.low < 2.113706745e-314)) {
            int v1 = this.cache;
            do {
                byte v2_4 = this.buf;
                int v3 = this.bufPos;
                this.bufPos = (v3 + 1);
                v2_4[v3] = ((byte) (v1 + v0));
                v1 = 255;
                byte v2_6 = (this.cacheSize - 1);
                this.cacheSize = v2_6;
            } while(v2_6 != 0);
            this.cache = ((byte) ((int) (this.low >> 24)));
        }
        this.cacheSize = (this.cacheSize + 1);
        this.low = ((this.low & 16777215) << 8);
        return;
    }

    public void encodeBit(short[] p9, int p10, int p11)
    {
        short v1 = p9[p10];
        int v0 = ((this.range >> 11) * v1);
        if (p11 != 0) {
            this.low = (this.low + (((long) v0) & 2.1219957905e-314));
            this.range = (this.range - v0);
            p9[p10] = ((short) (v1 - (v1 >> 5)));
        } else {
            this.range = v0;
            p9[p10] = ((short) (((2048 - v1) >> 5) + v1));
        }
        if ((this.range & -16777216) == 0) {
            this.range = (this.range << 8);
            this.shiftLow();
        }
        return;
    }

    public void encodeBitTree(short[] p5, int p6)
    {
        int v1 = 1;
        int v2 = p5.length;
        do {
            v2 >>= 1;
            int v0 = (p6 & v2);
            this.encodeBit(p5, v1, v0);
            v1 <<= 1;
            if (v0 != 0) {
                v1 |= 1;
            }
        } while(v2 != 1);
        return;
    }

    public void encodeDirectBits(int p5, int p6)
    {
        do {
            this.range = (this.range >> 1);
            p6--;
            this.low = (this.low + ((long) (this.range & (0 - ((p5 >> p6) & 1)))));
            if ((this.range & -16777216) == 0) {
                this.range = (this.range << 8);
                this.shiftLow();
            }
        } while(p6 != 0);
        return;
    }

    public void encodeReverseBitTree(short[] p4, int p5)
    {
        int v1 = 1;
        int v5_1 = (p5 | p4.length);
        do {
            int v0 = (v5_1 & 1);
            v5_1 >>= 1;
            this.encodeBit(p4, v1, v0);
            v1 = ((v1 << 1) | v0);
        } while(v5_1 != 1);
        return;
    }

    public int finish()
    {
        int v0 = 0;
        while (v0 < 5) {
            this.shiftLow();
            v0++;
        }
        return this.bufPos;
    }

    public int getPendingSize()
    {
        return (((this.bufPos + this.cacheSize) + 5) - 1);
    }

    public void reset()
    {
        this.low = 0;
        this.range = -1;
        this.cache = 0;
        this.cacheSize = 1;
        this.bufPos = 0;
        return;
    }

    public void write(java.io.OutputStream p4)
    {
        p4.write(this.buf, 0, this.bufPos);
        return;
    }
}
