package org.tukaani.xz.rangecoder;
public abstract class RangeCoder {
    static final int BIT_MODEL_TOTAL = 2048;
    static final int BIT_MODEL_TOTAL_BITS = 11;
    static final int MOVE_BITS = 5;
    static final short PROB_INIT = 1024;
    static final int SHIFT_BITS = 8;
    static final int TOP_MASK = 4278190080;

    public RangeCoder()
    {
        return;
    }

    public static final void initProbs(short[] p1)
    {
        java.util.Arrays.fill(p1, 1024);
        return;
    }
}
