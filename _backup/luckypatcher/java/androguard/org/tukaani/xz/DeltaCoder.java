package org.tukaani.xz;
abstract class DeltaCoder implements org.tukaani.xz.FilterCoder {
    public static final long FILTER_ID = 3;

    DeltaCoder()
    {
        return;
    }

    public boolean changesSize()
    {
        return 0;
    }

    public boolean lastOK()
    {
        return 0;
    }

    public boolean nonLastOK()
    {
        return 1;
    }
}
