package org.tukaani.xz;
 class BlockInputStream extends java.io.InputStream {
    private final org.tukaani.xz.check.Check check;
    private long compressedSizeInHeader;
    private long compressedSizeLimit;
    private boolean endReached;
    private java.io.InputStream filterChain;
    private final int headerSize;
    private final org.tukaani.xz.CountingInputStream inCounted;
    private final java.io.DataInputStream inData;
    private final byte[] tempBuf;
    private long uncompressedSize;
    private long uncompressedSizeInHeader;

    public BlockInputStream(java.io.InputStream p22, org.tukaani.xz.check.Check p23, int p24, long p25, long p27)
    {
        org.tukaani.xz.BlockInputStream v21_1 = ;
v21_1.uncompressedSizeInHeader = -1;
        v21_1.compressedSizeInHeader = -1;
        v21_1.uncompressedSize = 0;
        v21_1.endReached = 0;
        org.tukaani.xz.MemoryLimitException v0_5 = new byte[1];
        v21_1.tempBuf = v0_5;
        v21_1.check = p23;
        org.tukaani.xz.MemoryLimitException v17_6 = new java.io.DataInputStream;
        v17_6(p22);
        v21_1.inData = v17_6;
        byte[] v3 = new byte[1024];
        v21_1.inData.readFully(v3, 0, 1);
        if (v3[0] != 0) {
            v21_1.headerSize = (((v3[0] & 255) + 1) * 4);
            v21_1.inData.readFully(v3, 1, (v21_1.headerSize - 1));
            if (org.tukaani.xz.common.DecoderUtil.isCRC32Valid(v3, 0, (v21_1.headerSize - 4), (v21_1.headerSize - 4))) {
                if ((v3[1] & 60) == 0) {
                    int v8 = ((v3[1] & 3) + 1);
                    long[] v9 = new long[v8];
                    byte[][] v10 = new byte[][v8];
                    java.io.ByteArrayInputStream v4_1 = new java.io.ByteArrayInputStream(v3, 2, (v21_1.headerSize - 6));
                    try {
                        v21_1.compressedSizeLimit = ((nan - ((long) v21_1.headerSize)) - ((long) p23.getSize()));
                    } catch (java.io.IOException v7) {
                        throw new org.tukaani.xz.CorruptedInputException("XZ Block Header is corrupt");
                    }
                    if ((v3[1] & 64) != 0) {
                        v21_1.compressedSizeInHeader = org.tukaani.xz.common.DecoderUtil.decodeVLI(v4_1);
                        if ((v21_1.compressedSizeInHeader != 0) && (v21_1.compressedSizeInHeader <= v21_1.compressedSizeLimit)) {
                            v21_1.compressedSizeLimit = v21_1.compressedSizeInHeader;
                        } else {
                            throw new org.tukaani.xz.CorruptedInputException();
                        }
                    }
                    if ((v3[1] & 128) != 0) {
                        v21_1.uncompressedSizeInHeader = org.tukaani.xz.common.DecoderUtil.decodeVLI(v4_1);
                    }
                    int v15_0 = 0;
                    while (v15_0 < v8) {
                        v9[v15_0] = org.tukaani.xz.common.DecoderUtil.decodeVLI(v4_1);
                        long v11 = org.tukaani.xz.common.DecoderUtil.decodeVLI(v4_1);
                        if (v11 <= ((long) v4_1.available())) {
                            org.tukaani.xz.MemoryLimitException v0_88 = new byte[((int) v11)];
                            v10[v15_0] = v0_88;
                            v4_1.read(v10[v15_0]);
                            v15_0++;
                        } else {
                            throw new org.tukaani.xz.CorruptedInputException();
                        }
                    }
                    int v15_1 = v4_1.available();
                    while (v15_1 > 0) {
                        if (v4_1.read() == 0) {
                            v15_1--;
                        } else {
                            throw new org.tukaani.xz.UnsupportedOptionsException("Unsupported options in XZ Block Header");
                        }
                    }
                    if (p25 != -1) {
                        int v14 = (v21_1.headerSize + p23.getSize());
                        if (((long) v14) < p25) {
                            long v5 = (p25 - ((long) v14));
                            if ((v5 <= v21_1.compressedSizeLimit) && ((v21_1.compressedSizeInHeader == -1) || (v21_1.compressedSizeInHeader == v5))) {
                                if ((v21_1.uncompressedSizeInHeader == -1) || (v21_1.uncompressedSizeInHeader == p27)) {
                                    v21_1.compressedSizeLimit = v5;
                                    v21_1.compressedSizeInHeader = v5;
                                    v21_1.uncompressedSizeInHeader = p27;
                                } else {
                                    throw new org.tukaani.xz.CorruptedInputException("XZ Index does not match a Block Header");
                                }
                            } else {
                                throw new org.tukaani.xz.CorruptedInputException("XZ Index does not match a Block Header");
                            }
                        } else {
                            throw new org.tukaani.xz.CorruptedInputException("XZ Index does not match a Block Header");
                        }
                    }
                    org.tukaani.xz.FilterDecoder[] v13 = new org.tukaani.xz.FilterDecoder[v9.length];
                    int v15_2 = 0;
                    while (v15_2 < v13.length) {
                        if (v9[v15_2] != 33) {
                            if (v9[v15_2] != 3) {
                                if (!org.tukaani.xz.BCJDecoder.isBCJFilterID(v9[v15_2])) {
                                    throw new org.tukaani.xz.UnsupportedOptionsException(new StringBuilder().append("Unknown Filter ID ").append(v9[v15_2]).toString());
                                } else {
                                    v13[v15_2] = new org.tukaani.xz.BCJDecoder(v9[v15_2], v10[v15_2]);
                                }
                            } else {
                                v13[v15_2] = new org.tukaani.xz.DeltaDecoder(v10[v15_2]);
                            }
                        } else {
                            v13[v15_2] = new org.tukaani.xz.LZMA2Decoder(v10[v15_2]);
                        }
                        v15_2++;
                    }
                    org.tukaani.xz.RawCoder.validate(v13);
                    if (p24 >= 0) {
                        int v16 = 0;
                        int v15_3 = 0;
                        while (v15_3 < v13.length) {
                            v16 += v13[v15_3].getMemoryUsage();
                            v15_3++;
                        }
                        if (v16 > p24) {
                            org.tukaani.xz.MemoryLimitException v17_66 = new org.tukaani.xz.MemoryLimitException;
                            v17_66(v16, p24);
                            throw v17_66;
                        }
                    }
                    org.tukaani.xz.MemoryLimitException v17_61 = new org.tukaani.xz.CountingInputStream;
                    v17_61(p22);
                    v21_1.inCounted = v17_61;
                    v21_1.filterChain = v21_1.inCounted;
                    int v15_4 = (v13.length - 1);
                    while (v15_4 >= 0) {
                        v21_1.filterChain = v13[v15_4].getInputStream(v21_1.filterChain);
                        v15_4--;
                    }
                    return;
                } else {
                    throw new org.tukaani.xz.UnsupportedOptionsException("Unsupported options in XZ Block Header");
                }
            } else {
                throw new org.tukaani.xz.CorruptedInputException("XZ Block Header is corrupt");
            }
        } else {
            throw new org.tukaani.xz.IndexIndicatorException();
        }
    }

    private void validate()
    {
        long v0 = this.inCounted.getSize();
        if (((this.compressedSizeInHeader != -1) && (this.compressedSizeInHeader != v0)) || ((this.uncompressedSizeInHeader != -1) && (this.uncompressedSizeInHeader != this.uncompressedSize))) {
            throw new org.tukaani.xz.CorruptedInputException();
        }
        do {
            long v2 = v0;
            v0 = (v2 + 1);
            if ((3 & v2) == 0) {
                byte[] v4 = new byte[this.check.getSize()];
                this.inData.readFully(v4);
                if (java.util.Arrays.equals(this.check.finish(), v4)) {
                    return;
                } else {
                    throw new org.tukaani.xz.CorruptedInputException(new StringBuilder().append("Integrity check (").append(this.check.getName()).append(") does not match").toString());
                }
            } else {
            }
        } while(this.inData.readUnsignedByte() == 0);
        throw new org.tukaani.xz.CorruptedInputException();
    }

    public int available()
    {
        return this.filterChain.available();
    }

    public long getUncompressedSize()
    {
        return this.uncompressedSize;
    }

    public long getUnpaddedSize()
    {
        return ((((long) this.headerSize) + this.inCounted.getSize()) + ((long) this.check.getSize()));
    }

    public int read()
    {
        int v0_0 = -1;
        if (this.read(this.tempBuf, 0, 1) != -1) {
            v0_0 = (this.tempBuf[0] & 255);
        }
        return v0_0;
    }

    public int read(byte[] p12, int p13, int p14)
    {
        int v2;
        if (!this.endReached) {
            v2 = this.filterChain.read(p12, p13, p14);
            if (v2 <= 0) {
                if (v2 == -1) {
                    this.validate();
                    this.endReached = 1;
                }
            } else {
                this.check.update(p12, p13, v2);
                this.uncompressedSize = (this.uncompressedSize + ((long) v2));
                long v0 = this.inCounted.getSize();
                if ((v0 >= 0) && ((v0 <= this.compressedSizeLimit) && ((this.uncompressedSize >= 0) && ((this.uncompressedSizeInHeader == -1) || (this.uncompressedSize <= this.uncompressedSizeInHeader))))) {
                    if ((v2 < p14) || (this.uncompressedSize == this.uncompressedSizeInHeader)) {
                        if (this.filterChain.read() == -1) {
                            this.validate();
                            this.endReached = 1;
                        } else {
                            throw new org.tukaani.xz.CorruptedInputException();
                        }
                    }
                } else {
                    throw new org.tukaani.xz.CorruptedInputException();
                }
            }
        } else {
            v2 = -1;
        }
        return v2;
    }
}
