package org.tukaani.xz;
 class SimpleOutputStream extends org.tukaani.xz.FinishableOutputStream {
    static final synthetic boolean $assertionsDisabled = False;
    private static final int FILTER_BUF_SIZE = 4096;
    private java.io.IOException exception;
    private final byte[] filterBuf;
    private boolean finished;
    private org.tukaani.xz.FinishableOutputStream out;
    private int pos;
    private final org.tukaani.xz.simple.SimpleFilter simpleFilter;
    private final byte[] tempBuf;
    private int unfiltered;

    static SimpleOutputStream()
    {
        int v0_2;
        if (org.tukaani.xz.SimpleOutputStream.desiredAssertionStatus()) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        org.tukaani.xz.SimpleOutputStream.$assertionsDisabled = v0_2;
        return;
    }

    SimpleOutputStream(org.tukaani.xz.FinishableOutputStream p3, org.tukaani.xz.simple.SimpleFilter p4)
    {
        NullPointerException v0_1 = new byte[4096];
        this.filterBuf = v0_1;
        this.pos = 0;
        this.unfiltered = 0;
        this.exception = 0;
        this.finished = 0;
        NullPointerException v0_4 = new byte[1];
        this.tempBuf = v0_4;
        if (p3 != null) {
            this.out = p3;
            this.simpleFilter = p4;
            return;
        } else {
            throw new NullPointerException();
        }
    }

    static int getMemoryUsage()
    {
        return 5;
    }

    private void writePending()
    {
        if ((org.tukaani.xz.SimpleOutputStream.$assertionsDisabled) || (!this.finished)) {
            if (this.exception == null) {
                try {
                    this.out.write(this.filterBuf, this.pos, this.unfiltered);
                    this.finished = 1;
                    return;
                } catch (java.io.IOException v0) {
                    this.exception = v0;
                    throw v0;
                }
            } else {
                throw this.exception;
            }
        } else {
            throw new AssertionError();
        }
    }

    public void close()
    {
        if (this.out != null) {
            if (!this.finished) {
                try {
                    this.writePending();
                } catch (int v1) {
                }
                this.out.close();
                this.out = 0;
                if (this.exception == null) {
                    return;
                } else {
                    throw this.exception;
                }
            }
            try {
            } catch (java.io.IOException v0) {
                if (this.exception != null) {
                } else {
                    this.exception = v0;
                }
            }
        }
    }

    public void finish()
    {
        if (!this.finished) {
            this.writePending();
            try {
                this.out.finish();
            } catch (java.io.IOException v0) {
                this.exception = v0;
                throw v0;
            }
        }
        return;
    }

    public void flush()
    {
        throw new org.tukaani.xz.UnsupportedOptionsException("Flushing is not supported");
    }

    public void write(int p4)
    {
        this.tempBuf[0] = ((byte) p4);
        this.write(this.tempBuf, 0, 1);
        return;
    }

    public void write(byte[] p9, int p10, int p11)
    {
        if ((p10 >= 0) && ((p11 >= 0) && (((p10 + p11) >= 0) && ((p10 + p11) <= p9.length)))) {
            if (this.exception == null) {
                if (this.finished) {
                    throw new org.tukaani.xz.XZIOException("Stream finished or closed");
                }
                while (p11 > 0) {
                    int v0 = Math.min(p11, (4096 - (this.pos + this.unfiltered)));
                    System.arraycopy(p9, p10, this.filterBuf, (this.pos + this.unfiltered), v0);
                    p10 += v0;
                    p11 -= v0;
                    this.unfiltered = (this.unfiltered + v0);
                    int v2 = this.simpleFilter.code(this.filterBuf, this.pos, this.unfiltered);
                    if ((org.tukaani.xz.SimpleOutputStream.$assertionsDisabled) || (v2 <= this.unfiltered)) {
                        this.unfiltered = (this.unfiltered - v2);
                        try {
                            this.out.write(this.filterBuf, this.pos, v2);
                            this.pos = (this.pos + v2);
                        } catch (java.io.IOException v1) {
                            this.exception = v1;
                            throw v1;
                        }
                        if ((this.pos + this.unfiltered) == 4096) {
                            System.arraycopy(this.filterBuf, this.pos, this.filterBuf, 0, this.unfiltered);
                            this.pos = 0;
                        }
                    } else {
                        throw new AssertionError();
                    }
                }
                return;
            } else {
                throw this.exception;
            }
        } else {
            throw new IndexOutOfBoundsException();
        }
    }
}
