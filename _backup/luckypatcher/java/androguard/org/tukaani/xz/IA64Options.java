package org.tukaani.xz;
public class IA64Options extends org.tukaani.xz.BCJOptions {
    private static final int ALIGNMENT = 16;

    public IA64Options()
    {
        this(16);
        return;
    }

    public bridge synthetic Object clone()
    {
        return super.clone();
    }

    public bridge synthetic int getDecoderMemoryUsage()
    {
        return super.getDecoderMemoryUsage();
    }

    public bridge synthetic int getEncoderMemoryUsage()
    {
        return super.getEncoderMemoryUsage();
    }

    org.tukaani.xz.FilterEncoder getFilterEncoder()
    {
        return new org.tukaani.xz.BCJEncoder(this, 6);
    }

    public java.io.InputStream getInputStream(java.io.InputStream p5)
    {
        return new org.tukaani.xz.SimpleInputStream(p5, new org.tukaani.xz.simple.IA64(0, this.startOffset));
    }

    public org.tukaani.xz.FinishableOutputStream getOutputStream(org.tukaani.xz.FinishableOutputStream p5)
    {
        return new org.tukaani.xz.SimpleOutputStream(p5, new org.tukaani.xz.simple.IA64(1, this.startOffset));
    }

    public bridge synthetic int getStartOffset()
    {
        return super.getStartOffset();
    }

    public bridge synthetic void setStartOffset(int p1)
    {
        super.setStartOffset(p1);
        return;
    }
}
