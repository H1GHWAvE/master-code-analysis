package org.tukaani.xz;
 class UncompressedLZMA2OutputStream extends org.tukaani.xz.FinishableOutputStream {
    private boolean dictResetNeeded;
    private java.io.IOException exception;
    private boolean finished;
    private org.tukaani.xz.FinishableOutputStream out;
    private final java.io.DataOutputStream outData;
    private final byte[] tempBuf;
    private final byte[] uncompBuf;
    private int uncompPos;

    UncompressedLZMA2OutputStream(org.tukaani.xz.FinishableOutputStream p4)
    {
        java.io.DataOutputStream v0_1 = new byte[65536];
        this.uncompBuf = v0_1;
        this.uncompPos = 0;
        this.dictResetNeeded = 1;
        this.finished = 0;
        this.exception = 0;
        java.io.DataOutputStream v0_3 = new byte[1];
        this.tempBuf = v0_3;
        if (p4 != null) {
            this.out = p4;
            this.outData = new java.io.DataOutputStream(p4);
            return;
        } else {
            throw new NullPointerException();
        }
    }

    static int getMemoryUsage()
    {
        return 70;
    }

    private void writeChunk()
    {
        java.io.DataOutputStream v0_1;
        if (!this.dictResetNeeded) {
            v0_1 = 2;
        } else {
            v0_1 = 1;
        }
        this.outData.writeByte(v0_1);
        this.outData.writeShort((this.uncompPos - 1));
        this.outData.write(this.uncompBuf, 0, this.uncompPos);
        this.uncompPos = 0;
        this.dictResetNeeded = 0;
        return;
    }

    private void writeEndMarker()
    {
        if (this.exception == null) {
            if (!this.finished) {
                try {
                    if (this.uncompPos > 0) {
                        this.writeChunk();
                    }
                } catch (java.io.IOException v0) {
                    this.exception = v0;
                    throw v0;
                }
                this.out.write(0);
                return;
            } else {
                throw new org.tukaani.xz.XZIOException("Stream finished or closed");
            }
        } else {
            throw this.exception;
        }
    }

    public void close()
    {
        if (this.out != null) {
            if (!this.finished) {
                try {
                    this.writeEndMarker();
                } catch (int v1) {
                }
                this.out.close();
                this.out = 0;
                if (this.exception == null) {
                    return;
                } else {
                    throw this.exception;
                }
            }
            try {
            } catch (java.io.IOException v0) {
                if (this.exception != null) {
                } else {
                    this.exception = v0;
                }
            }
        }
    }

    public void finish()
    {
        if (!this.finished) {
            this.writeEndMarker();
            try {
                this.out.finish();
                this.finished = 1;
            } catch (java.io.IOException v0) {
                this.exception = v0;
                throw v0;
            }
        }
        return;
    }

    public void flush()
    {
        if (this.exception == null) {
            if (!this.finished) {
                try {
                    if (this.uncompPos > 0) {
                        this.writeChunk();
                    }
                } catch (java.io.IOException v0) {
                    this.exception = v0;
                    throw v0;
                }
                this.out.flush();
                return;
            } else {
                throw new org.tukaani.xz.XZIOException("Stream finished or closed");
            }
        } else {
            throw this.exception;
        }
    }

    public void write(int p4)
    {
        this.tempBuf[0] = ((byte) p4);
        this.write(this.tempBuf, 0, 1);
        return;
    }

    public void write(byte[] p5, int p6, int p7)
    {
        if ((p6 >= 0) && ((p7 >= 0) && (((p6 + p7) >= 0) && ((p6 + p7) <= p5.length)))) {
            if (this.exception == null) {
                if (this.finished) {
                    throw new org.tukaani.xz.XZIOException("Stream finished or closed");
                }
                while (p7 > 0) {
                    try {
                        int v0 = Math.min((this.uncompBuf.length - this.uncompPos), p7);
                        System.arraycopy(p5, p6, this.uncompBuf, this.uncompPos, v0);
                        p7 -= v0;
                        this.uncompPos = (this.uncompPos + v0);
                    } catch (java.io.IOException v1) {
                        this.exception = v1;
                        throw v1;
                    }
                    if (this.uncompPos == this.uncompBuf.length) {
                        this.writeChunk();
                    }
                }
                return;
            } else {
                throw this.exception;
            }
        } else {
            throw new IndexOutOfBoundsException();
        }
    }
}
