package org.tukaani.xz;
public class UnsupportedOptionsException extends org.tukaani.xz.XZIOException {
    private static final long serialVersionUID = 3;

    public UnsupportedOptionsException()
    {
        return;
    }

    public UnsupportedOptionsException(String p1)
    {
        this(p1);
        return;
    }
}
