package org.tukaani.xz.common;
public class EncoderUtil extends org.tukaani.xz.common.Util {

    public EncoderUtil()
    {
        return;
    }

    public static void encodeVLI(java.io.OutputStream p4, long p5)
    {
        while (p5 >= 128) {
            p4.write(((byte) ((int) (p5 | 128))));
            p5 >>= 7;
        }
        p4.write(((byte) ((int) p5)));
        return;
    }

    public static void writeCRC32(java.io.OutputStream p6, byte[] p7)
    {
        java.util.zip.CRC32 v0_1 = new java.util.zip.CRC32();
        v0_1.update(p7);
        long v2 = v0_1.getValue();
        int v1 = 0;
        while (v1 < 4) {
            p6.write(((byte) ((int) (v2 >> (v1 * 8)))));
            v1++;
        }
        return;
    }
}
