package org.tukaani.xz.common;
public class Util {
    public static final long BACKWARD_SIZE_MAX = 17179869184;
    public static final int BLOCK_HEADER_SIZE_MAX = 1024;
    public static final int STREAM_HEADER_SIZE = 12;
    public static final long VLI_MAX = 9223372036854775807;
    public static final int VLI_SIZE_MAX = 9;

    public Util()
    {
        return;
    }

    public static int getVLISize(long p3)
    {
        int v0 = 0;
        do {
            v0++;
            p3 >>= 7;
        } while(p3 != 0);
        return v0;
    }
}
