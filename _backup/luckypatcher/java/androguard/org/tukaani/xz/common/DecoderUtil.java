package org.tukaani.xz.common;
public class DecoderUtil extends org.tukaani.xz.common.Util {

    public DecoderUtil()
    {
        return;
    }

    public static boolean areStreamFlagsEqual(org.tukaani.xz.common.StreamFlags p2, org.tukaani.xz.common.StreamFlags p3)
    {
        int v0_1;
        if (p2.checkType != p3.checkType) {
            v0_1 = 0;
        } else {
            v0_1 = 1;
        }
        return v0_1;
    }

    private static org.tukaani.xz.common.StreamFlags decodeStreamFlags(byte[] p3, int p4)
    {
        if ((p3[p4] == 0) && ((p3[(p4 + 1)] & 255) < 16)) {
            org.tukaani.xz.common.StreamFlags v0_1 = new org.tukaani.xz.common.StreamFlags();
            v0_1.checkType = p3[(p4 + 1)];
            return v0_1;
        } else {
            throw new org.tukaani.xz.UnsupportedOptionsException();
        }
    }

    public static org.tukaani.xz.common.StreamFlags decodeStreamFooter(byte[] p8)
    {
        if ((p8[10] == org.tukaani.xz.XZ.FOOTER_MAGIC[0]) && (p8[11] == org.tukaani.xz.XZ.FOOTER_MAGIC[1])) {
            if (org.tukaani.xz.common.DecoderUtil.isCRC32Valid(p8, 4, 6, 0)) {
                try {
                    org.tukaani.xz.common.StreamFlags v2 = org.tukaani.xz.common.DecoderUtil.decodeStreamFlags(p8, 8);
                    v2.backwardSize = 0;
                    int v1 = 0;
                } catch (org.tukaani.xz.UnsupportedOptionsException v0) {
                    throw new org.tukaani.xz.UnsupportedOptionsException("Unsupported options in XZ Stream Footer");
                }
                while (v1 < 4) {
                    v2.backwardSize = (v2.backwardSize | ((long) ((p8[(v1 + 4)] & 255) << (v1 * 8))));
                    v1++;
                }
                v2.backwardSize = ((v2.backwardSize + 1) * 4);
                return v2;
            } else {
                throw new org.tukaani.xz.CorruptedInputException("XZ Stream Footer is corrupt");
            }
        } else {
            throw new org.tukaani.xz.CorruptedInputException("XZ Stream Footer is corrupt");
        }
    }

    public static org.tukaani.xz.common.StreamFlags decodeStreamHeader(byte[] p5)
    {
        int v1 = 0;
        while (v1 < org.tukaani.xz.XZ.HEADER_MAGIC.length) {
            if (p5[v1] == org.tukaani.xz.XZ.HEADER_MAGIC[v1]) {
                v1++;
            } else {
                throw new org.tukaani.xz.XZFormatException();
            }
        }
        if (org.tukaani.xz.common.DecoderUtil.isCRC32Valid(p5, org.tukaani.xz.XZ.HEADER_MAGIC.length, 2, (org.tukaani.xz.XZ.HEADER_MAGIC.length + 2))) {
            try {
                return org.tukaani.xz.common.DecoderUtil.decodeStreamFlags(p5, org.tukaani.xz.XZ.HEADER_MAGIC.length);
            } catch (org.tukaani.xz.UnsupportedOptionsException v0) {
                throw new org.tukaani.xz.UnsupportedOptionsException("Unsupported options in XZ Stream Header");
            }
        } else {
            throw new org.tukaani.xz.CorruptedInputException("XZ Stream Header is corrupt");
        }
    }

    public static long decodeVLI(java.io.InputStream p8)
    {
        int v0 = p8.read();
        if (v0 != -1) {
            long v2 = ((long) (v0 & 127));
            int v1 = 0;
            while ((v0 & 128) != 0) {
                v1++;
                if (v1 < 9) {
                    v0 = p8.read();
                    if (v0 != -1) {
                        if (v0 != 0) {
                            v2 |= (((long) (v0 & 127)) << (v1 * 7));
                        } else {
                            throw new org.tukaani.xz.CorruptedInputException();
                        }
                    } else {
                        throw new java.io.EOFException();
                    }
                } else {
                    throw new org.tukaani.xz.CorruptedInputException();
                }
            }
            return v2;
        } else {
            throw new java.io.EOFException();
        }
    }

    public static boolean isCRC32Valid(byte[] p6, int p7, int p8, int p9)
    {
        java.util.zip.CRC32 v0_1 = new java.util.zip.CRC32();
        v0_1.update(p6, p7, p8);
        long v2 = v0_1.getValue();
        int v1 = 0;
        while (v1 < 4) {
            if (((byte) ((int) (v2 >> (v1 * 8)))) == p6[(p9 + v1)]) {
                v1++;
            } else {
                int v4_1 = 0;
            }
            return v4_1;
        }
        v4_1 = 1;
        return v4_1;
    }
}
