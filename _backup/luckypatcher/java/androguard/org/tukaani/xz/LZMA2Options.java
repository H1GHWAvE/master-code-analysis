package org.tukaani.xz;
public class LZMA2Options extends org.tukaani.xz.FilterOptions {
    static final synthetic boolean $assertionsDisabled = False;
    public static final int DICT_SIZE_DEFAULT = 8388608;
    public static final int DICT_SIZE_MAX = 805306368;
    public static final int DICT_SIZE_MIN = 4096;
    public static final int LC_DEFAULT = 3;
    public static final int LC_LP_MAX = 4;
    public static final int LP_DEFAULT = 0;
    public static final int MF_BT4 = 20;
    public static final int MF_HC4 = 4;
    public static final int MODE_FAST = 1;
    public static final int MODE_NORMAL = 2;
    public static final int MODE_UNCOMPRESSED = 0;
    public static final int NICE_LEN_MAX = 273;
    public static final int NICE_LEN_MIN = 8;
    public static final int PB_DEFAULT = 2;
    public static final int PB_MAX = 4;
    public static final int PRESET_DEFAULT = 6;
    public static final int PRESET_MAX = 9;
    public static final int PRESET_MIN;
    private static final int[] presetToDepthLimit;
    private static final int[] presetToDictSize;
    private int depthLimit;
    private int dictSize;
    private int lc;
    private int lp;
    private int mf;
    private int mode;
    private int niceLen;
    private int pb;
    private byte[] presetDict;

    static LZMA2Options()
    {
        int[] v0_2;
        if (org.tukaani.xz.LZMA2Options.desiredAssertionStatus()) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        org.tukaani.xz.LZMA2Options.$assertionsDisabled = v0_2;
        int[] v0_4 = new int[10];
        v0_4 = {262144, 1048576, 2097152, 4194304, 4194304, 8388608, 8388608, 16777216, 33554432, 67108864};
        org.tukaani.xz.LZMA2Options.presetToDictSize = v0_4;
        int[] v0_6 = new int[4];
        v0_6 = {4, 8, 24, 48};
        org.tukaani.xz.LZMA2Options.presetToDepthLimit = v0_6;
        return;
    }

    public LZMA2Options()
    {
        this.presetDict = 0;
        try {
            this.setPreset(6);
            return;
        } catch (org.tukaani.xz.UnsupportedOptionsException v0) {
            if (org.tukaani.xz.LZMA2Options.$assertionsDisabled) {
                throw new RuntimeException();
            } else {
                throw new AssertionError();
            }
        }
    }

    public LZMA2Options(int p2)
    {
        this.presetDict = 0;
        this.setPreset(p2);
        return;
    }

    public LZMA2Options(int p2, int p3, int p4, int p5, int p6, int p7, int p8, int p9)
    {
        this.presetDict = 0;
        this.setDictSize(p2);
        this.setLcLp(p3, p4);
        this.setPb(p5);
        this.setMode(p6);
        this.setNiceLen(p7);
        this.setMatchFinder(p8);
        this.setDepthLimit(p9);
        return;
    }

    public Object clone()
    {
        try {
            return super.clone();
        } catch (CloneNotSupportedException v0) {
            if (org.tukaani.xz.LZMA2Options.$assertionsDisabled) {
                throw new RuntimeException();
            } else {
                throw new AssertionError();
            }
        }
    }

    public int getDecoderMemoryUsage()
    {
        int v0_0 = (this.dictSize - 1);
        int v0_1 = (v0_0 | (v0_0 >> 2));
        int v0_2 = (v0_1 | (v0_1 >> 3));
        int v0_3 = (v0_2 | (v0_2 >> 4));
        int v0_4 = (v0_3 | (v0_3 >> 8));
        return org.tukaani.xz.LZMA2InputStream.getMemoryUsage(((v0_4 | (v0_4 >> 16)) + 1));
    }

    public int getDepthLimit()
    {
        return this.depthLimit;
    }

    public int getDictSize()
    {
        return this.dictSize;
    }

    public int getEncoderMemoryUsage()
    {
        int v0_1;
        if (this.mode != 0) {
            v0_1 = org.tukaani.xz.LZMA2OutputStream.getMemoryUsage(this);
        } else {
            v0_1 = org.tukaani.xz.UncompressedLZMA2OutputStream.getMemoryUsage();
        }
        return v0_1;
    }

    org.tukaani.xz.FilterEncoder getFilterEncoder()
    {
        return new org.tukaani.xz.LZMA2Encoder(this);
    }

    public java.io.InputStream getInputStream(java.io.InputStream p3)
    {
        return new org.tukaani.xz.LZMA2InputStream(p3, this.dictSize);
    }

    public int getLc()
    {
        return this.lc;
    }

    public int getLp()
    {
        return this.lp;
    }

    public int getMatchFinder()
    {
        return this.mf;
    }

    public int getMode()
    {
        return this.mode;
    }

    public int getNiceLen()
    {
        return this.niceLen;
    }

    public org.tukaani.xz.FinishableOutputStream getOutputStream(org.tukaani.xz.FinishableOutputStream p2)
    {
        org.tukaani.xz.LZMA2OutputStream v0_2;
        if (this.mode != 0) {
            v0_2 = new org.tukaani.xz.LZMA2OutputStream(p2, this);
        } else {
            v0_2 = new org.tukaani.xz.UncompressedLZMA2OutputStream(p2);
        }
        return v0_2;
    }

    public int getPb()
    {
        return this.pb;
    }

    public byte[] getPresetDict()
    {
        return this.presetDict;
    }

    public void setDepthLimit(int p4)
    {
        if (p4 >= 0) {
            this.depthLimit = p4;
            return;
        } else {
            throw new org.tukaani.xz.UnsupportedOptionsException(new StringBuilder().append("Depth limit cannot be negative: ").append(p4).toString());
        }
    }

    public void setDictSize(int p4)
    {
        if (p4 >= 4096) {
            if (p4 <= 805306368) {
                this.dictSize = p4;
                return;
            } else {
                throw new org.tukaani.xz.UnsupportedOptionsException(new StringBuilder().append("LZMA2 dictionary size must not exceed 768 MiB: ").append(p4).append(" B").toString());
            }
        } else {
            throw new org.tukaani.xz.UnsupportedOptionsException(new StringBuilder().append("LZMA2 dictionary size must be at least 4 KiB: ").append(p4).append(" B").toString());
        }
    }

    public void setLc(int p2)
    {
        this.setLcLp(p2, this.lp);
        return;
    }

    public void setLcLp(int p4, int p5)
    {
        if ((p4 >= 0) && ((p5 >= 0) && ((p4 <= 4) && ((p5 <= 4) && ((p4 + p5) <= 4))))) {
            this.lc = p4;
            this.lp = p5;
            return;
        } else {
            throw new org.tukaani.xz.UnsupportedOptionsException(new StringBuilder().append("lc + lp must not exceed 4: ").append(p4).append(" + ").append(p5).toString());
        }
    }

    public void setLp(int p2)
    {
        this.setLcLp(this.lc, p2);
        return;
    }

    public void setMatchFinder(int p4)
    {
        if ((p4 == 4) || (p4 == 20)) {
            this.mf = p4;
            return;
        } else {
            throw new org.tukaani.xz.UnsupportedOptionsException(new StringBuilder().append("Unsupported match finder: ").append(p4).toString());
        }
    }

    public void setMode(int p4)
    {
        if ((p4 >= 0) && (p4 <= 2)) {
            this.mode = p4;
            return;
        } else {
            throw new org.tukaani.xz.UnsupportedOptionsException(new StringBuilder().append("Unsupported compression mode: ").append(p4).toString());
        }
    }

    public void setNiceLen(int p4)
    {
        if (p4 >= 8) {
            if (p4 <= 273) {
                this.niceLen = p4;
                return;
            } else {
                throw new org.tukaani.xz.UnsupportedOptionsException(new StringBuilder().append("Maximum nice length of matches is 273: ").append(p4).toString());
            }
        } else {
            throw new org.tukaani.xz.UnsupportedOptionsException(new StringBuilder().append("Minimum nice length of matches is 8 bytes: ").append(p4).toString());
        }
    }

    public void setPb(int p4)
    {
        if ((p4 >= 0) && (p4 <= 4)) {
            this.pb = p4;
            return;
        } else {
            throw new org.tukaani.xz.UnsupportedOptionsException(new StringBuilder().append("pb must not exceed 4: ").append(p4).toString());
        }
    }

    public void setPreset(int p7)
    {
        if ((p7 >= 0) && (p7 <= 9)) {
            this.lc = 3;
            this.lp = 0;
            this.pb = 2;
            this.dictSize = org.tukaani.xz.LZMA2Options.presetToDictSize[p7];
            if (p7 > 3) {
                int v0_5;
                this.mode = 2;
                this.mf = 20;
                if (p7 != 4) {
                    if (p7 != 5) {
                        v0_5 = 64;
                    } else {
                        v0_5 = 32;
                    }
                } else {
                    v0_5 = 16;
                }
                this.niceLen = v0_5;
                this.depthLimit = 0;
            } else {
                int v0_6;
                this.mode = 1;
                this.mf = 4;
                if (p7 > 1) {
                    v0_6 = 273;
                } else {
                    v0_6 = 128;
                }
                this.niceLen = v0_6;
                this.depthLimit = org.tukaani.xz.LZMA2Options.presetToDepthLimit[p7];
            }
            return;
        } else {
            throw new org.tukaani.xz.UnsupportedOptionsException(new StringBuilder().append("Unsupported preset: ").append(p7).toString());
        }
    }

    public void setPresetDict(byte[] p1)
    {
        this.presetDict = p1;
        return;
    }
}
