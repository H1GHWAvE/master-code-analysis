package org.tukaani.xz;
public class MemoryLimitException extends org.tukaani.xz.XZIOException {
    private static final long serialVersionUID = 3;
    private final int memoryLimit;
    private final int memoryNeeded;

    public MemoryLimitException(int p3, int p4)
    {
        this(new StringBuilder().append("").append(p3).append(" KiB of memory would be needed; limit was ").append(p4).append(" KiB").toString());
        this.memoryNeeded = p3;
        this.memoryLimit = p4;
        return;
    }

    public int getMemoryLimit()
    {
        return this.memoryLimit;
    }

    public int getMemoryNeeded()
    {
        return this.memoryNeeded;
    }
}
