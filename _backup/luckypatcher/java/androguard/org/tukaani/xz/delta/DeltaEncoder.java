package org.tukaani.xz.delta;
public class DeltaEncoder extends org.tukaani.xz.delta.DeltaCoder {

    public DeltaEncoder(int p1)
    {
        this(p1);
        return;
    }

    public void encode(byte[] p6, int p7, int p8, byte[] p9)
    {
        int v0 = 0;
        while (v0 < p8) {
            byte v1 = this.history[((this.distance + this.pos) & 255)];
            byte v2_1 = this.history;
            int v3_3 = this.pos;
            this.pos = (v3_3 - 1);
            v2_1[(v3_3 & 255)] = p6[(p7 + v0)];
            p9[v0] = ((byte) (p6[(p7 + v0)] - v1));
            v0++;
        }
        return;
    }
}
