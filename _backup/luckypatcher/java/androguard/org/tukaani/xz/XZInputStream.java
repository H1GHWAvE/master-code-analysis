package org.tukaani.xz;
public class XZInputStream extends java.io.InputStream {
    private boolean endReached;
    private java.io.IOException exception;
    private java.io.InputStream in;
    private final int memoryLimit;
    private final byte[] tempBuf;
    private org.tukaani.xz.SingleXZInputStream xzIn;

    public XZInputStream(java.io.InputStream p2)
    {
        this(p2, -1);
        return;
    }

    public XZInputStream(java.io.InputStream p2, int p3)
    {
        this.endReached = 0;
        this.exception = 0;
        org.tukaani.xz.SingleXZInputStream v0_3 = new byte[1];
        this.tempBuf = v0_3;
        this.in = p2;
        this.memoryLimit = p3;
        this.xzIn = new org.tukaani.xz.SingleXZInputStream(p2, p3);
        return;
    }

    private void prepareNextStream()
    {
        java.io.DataInputStream v2_1 = new java.io.DataInputStream(this.in);
        byte[] v0 = new byte[12];
        while (v2_1.read(v0, 0, 1) != -1) {
            v2_1.readFully(v0, 1, 3);
            if ((v0[0] != 0) || ((v0[1] != 0) || ((v0[2] != 0) || (v0[3] != 0)))) {
                v2_1.readFully(v0, 4, 8);
                try {
                    this.xzIn = new org.tukaani.xz.SingleXZInputStream(this.in, this.memoryLimit, v0);
                } catch (org.tukaani.xz.XZFormatException v1) {
                    throw new org.tukaani.xz.CorruptedInputException("Garbage after a valid XZ Stream");
                }
            }
            return;
        }
        this.endReached = 1;
        return;
    }

    public int available()
    {
        if (this.in != null) {
            if (this.exception == null) {
                int v0_4;
                if (this.xzIn != null) {
                    v0_4 = this.xzIn.available();
                } else {
                    v0_4 = 0;
                }
                return v0_4;
            } else {
                throw this.exception;
            }
        } else {
            throw new org.tukaani.xz.XZIOException("Stream closed");
        }
    }

    public void close()
    {
        if (this.in != null) {
            try {
                this.in.close();
                this.in = 0;
            } catch (Throwable v0_2) {
                this.in = 0;
                throw v0_2;
            }
        }
        return;
    }

    public int read()
    {
        int v0_0 = -1;
        if (this.read(this.tempBuf, 0, 1) != -1) {
            v0_0 = (this.tempBuf[0] & 255);
        }
        return v0_0;
    }

    public int read(byte[] p7, int p8, int p9)
    {
        if ((p8 >= 0) && ((p9 >= 0) && (((p8 + p9) >= 0) && ((p8 + p9) <= p7.length)))) {
            java.io.IOException v2;
            if (p9 != 0) {
                if (this.in != null) {
                    if (this.exception == null) {
                        if (!this.endReached) {
                            v2 = 0;
                            while (p9 > 0) {
                                try {
                                    if (this.xzIn == null) {
                                        this.prepareNextStream();
                                        if (this.endReached) {
                                            if (v2 != null) {
                                                break;
                                            }
                                            v2 = -1;
                                            break;
                                        }
                                    }
                                    int v1 = this.xzIn.read(p7, p8, p9);
                                    if (v1 <= 0) {
                                        if (v1 == -1) {
                                            this.xzIn = 0;
                                        }
                                    } else {
                                        v2 += v1;
                                        p8 += v1;
                                        p9 -= v1;
                                    }
                                } catch (java.io.IOException v0) {
                                    this.exception = v0;
                                    if (v2 != null) {
                                        break;
                                    }
                                    throw v0;
                                }
                            }
                        } else {
                            v2 = -1;
                        }
                    } else {
                        throw this.exception;
                    }
                } else {
                    throw new org.tukaani.xz.XZIOException("Stream closed");
                }
            } else {
                v2 = 0;
            }
            return v2;
        } else {
            throw new IndexOutOfBoundsException();
        }
    }
}
