package org.tukaani.xz.simple;
public final class ARMThumb implements org.tukaani.xz.simple.SimpleFilter {
    private final boolean isEncoder;
    private int pos;

    public ARMThumb(boolean p2, int p3)
    {
        this.isEncoder = p2;
        this.pos = (p3 + 4);
        return;
    }

    public int code(byte[] p7, int p8, int p9)
    {
        int v1 = ((p8 + p9) - 4);
        int v2_0 = p8;
        while (v2_0 <= v1) {
            if (((p7[(v2_0 + 1)] & 248) == 240) && ((p7[(v2_0 + 3)] & 248) == 248)) {
                int v0_0;
                int v3_1 = ((((((p7[(v2_0 + 1)] & 7) << 19) | ((p7[v2_0] & 255) << 11)) | ((p7[(v2_0 + 3)] & 7) << 8)) | (p7[(v2_0 + 2)] & 255)) << 1);
                if (!this.isEncoder) {
                    v0_0 = (v3_1 - ((this.pos + v2_0) - p8));
                } else {
                    v0_0 = (v3_1 + ((this.pos + v2_0) - p8));
                }
                int v0_1 = (v0_0 >> 1);
                p7[(v2_0 + 1)] = ((byte) (((v0_1 >> 19) & 7) | 240));
                p7[v2_0] = ((byte) (v0_1 >> 11));
                p7[(v2_0 + 3)] = ((byte) (((v0_1 >> 8) & 7) | 248));
                p7[(v2_0 + 2)] = ((byte) v0_1);
                v2_0 += 2;
            }
            v2_0 += 2;
        }
        int v2_1 = (v2_0 - p8);
        this.pos = (this.pos + v2_1);
        return v2_1;
    }
}
