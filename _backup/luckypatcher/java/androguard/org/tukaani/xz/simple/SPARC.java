package org.tukaani.xz.simple;
public final class SPARC implements org.tukaani.xz.simple.SimpleFilter {
    private final boolean isEncoder;
    private int pos;

    public SPARC(boolean p1, int p2)
    {
        this.isEncoder = p1;
        this.pos = p2;
        return;
    }

    public int code(byte[] p7, int p8, int p9)
    {
        int v1 = ((p8 + p9) - 4);
        int v2_0 = p8;
        while (v2_0 <= v1) {
            if (((p7[v2_0] == 64) && ((p7[(v2_0 + 1)] & 192) == 0)) || ((p7[v2_0] == 127) && ((p7[(v2_0 + 1)] & 192) == 192))) {
                int v0_0;
                int v3_1 = ((((((p7[v2_0] & 255) << 24) | ((p7[(v2_0 + 1)] & 255) << 16)) | ((p7[(v2_0 + 2)] & 255) << 8)) | (p7[(v2_0 + 3)] & 255)) << 2);
                if (!this.isEncoder) {
                    v0_0 = (v3_1 - ((this.pos + v2_0) - p8));
                } else {
                    v0_0 = (v3_1 + ((this.pos + v2_0) - p8));
                }
                int v0_1 = (v0_0 >> 2);
                int v0_2 = (((((0 - ((v0_1 >> 22) & 1)) << 22) & 1073741823) | (4194303 & v0_1)) | 1073741824);
                p7[v2_0] = ((byte) (v0_2 >> 24));
                p7[(v2_0 + 1)] = ((byte) (v0_2 >> 16));
                p7[(v2_0 + 2)] = ((byte) (v0_2 >> 8));
                p7[(v2_0 + 3)] = ((byte) v0_2);
            }
            v2_0 += 4;
        }
        int v2_1 = (v2_0 - p8);
        this.pos = (this.pos + v2_1);
        return v2_1;
    }
}
