package org.tukaani.xz.simple;
public final class IA64 implements org.tukaani.xz.simple.SimpleFilter {
    private static final int[] BRANCH_TABLE;
    private final boolean isEncoder;
    private int pos;

    static IA64()
    {
        int[] v0_1 = new int[32];
        v0_1 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 4, 6, 6, 0, 0, 7, 7, 4, 4, 0, 0, 4, 4, 0, 0};
        org.tukaani.xz.simple.IA64.BRANCH_TABLE = v0_1;
        return;
    }

    public IA64(boolean p1, int p2)
    {
        this.isEncoder = p1;
        this.pos = p2;
        return;
    }

    public int code(byte[] p22, int p23, int p24)
    {
        int v6 = ((p23 + p24) - 16);
        int v7_0 = p23;
        while (v7_0 <= v6) {
            int v14 = org.tukaani.xz.simple.IA64.BRANCH_TABLE[(p22[v7_0] & 31)];
            int v15 = 0;
            int v2 = 5;
            while (v15 < 3) {
                if (((v14 >> v15) & 1) != 0) {
                    int v4 = (v2 >> 3);
                    int v3 = (v2 & 7);
                    long v8_0 = 0;
                    int v13_0 = 0;
                    while (v13_0 < 6) {
                        v8_0 |= ((((long) p22[((v7_0 + v4) + v13_0)]) & 255) << (v13_0 * 8));
                        v13_0++;
                    }
                    long v10_0 = (v8_0 >> v3);
                    if ((((v10_0 >> 37) & 15) == 5) && (((v10_0 >> 9) & 7) == 0)) {
                        int v5_0;
                        int v16_2 = ((((int) ((v10_0 >> 13) & 1048575)) | ((((int) (v10_0 >> 36)) & 1) << 20)) << 4);
                        if (!this.isEncoder) {
                            v5_0 = (v16_2 - ((this.pos + v7_0) - p23));
                        } else {
                            v5_0 = (v16_2 + ((this.pos + v7_0) - p23));
                        }
                        int v5_1 = (v5_0 >> 4);
                        int v13_1 = 0;
                        while (v13_1 < 6) {
                            p22[((v7_0 + v4) + v13_1)] = ((byte) ((int) (((v8_0 & ((long) ((1 << v3) - 1))) | ((((v10_0 & nan) | ((((long) v5_1) & 1048575) << 13)) | ((((long) v5_1) & 1048576) << 16)) << v3)) >> (v13_1 * 8))));
                            v13_1++;
                        }
                    }
                }
                v15++;
                v2 += 41;
            }
            v7_0 += 16;
        }
        int v7_1 = (v7_0 - p23);
        this.pos = (this.pos + v7_1);
        return v7_1;
    }
}
