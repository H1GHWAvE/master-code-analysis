package org.tukaani.xz;
public class XZ {
    public static final int CHECK_CRC32 = 1;
    public static final int CHECK_CRC64 = 4;
    public static final int CHECK_NONE = 0;
    public static final int CHECK_SHA256 = 10;
    public static final byte[] FOOTER_MAGIC;
    public static final byte[] HEADER_MAGIC;

    static XZ()
    {
        byte[] v0_1 = new byte[6];
        v0_1 = {-3, 55, 122, 88, 90, 0};
        org.tukaani.xz.XZ.HEADER_MAGIC = v0_1;
        byte[] v0_3 = new byte[2];
        v0_3 = {89, 90};
        org.tukaani.xz.XZ.FOOTER_MAGIC = v0_3;
        return;
    }

    private XZ()
    {
        return;
    }
}
