package org.tukaani.xz;
 class DeltaDecoder extends org.tukaani.xz.DeltaCoder implements org.tukaani.xz.FilterDecoder {
    private final int distance;

    DeltaDecoder(byte[] p3)
    {
        if (p3.length == 1) {
            this.distance = ((p3[0] & 255) + 1);
            return;
        } else {
            throw new org.tukaani.xz.UnsupportedOptionsException("Unsupported Delta filter properties");
        }
    }

    public java.io.InputStream getInputStream(java.io.InputStream p3)
    {
        return new org.tukaani.xz.DeltaInputStream(p3, this.distance);
    }

    public int getMemoryUsage()
    {
        return 1;
    }
}
