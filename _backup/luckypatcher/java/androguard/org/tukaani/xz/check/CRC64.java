package org.tukaani.xz.check;
public class CRC64 extends org.tukaani.xz.check.Check {
    private static final long[] crcTable = None;
    private static final long poly = 14514072000185962306;
    private long crc;

    static CRC64()
    {
        long v4_1 = new long[256];
        org.tukaani.xz.check.CRC64.crcTable = v4_1;
        int v0 = 0;
        while (v0 < org.tukaani.xz.check.CRC64.crcTable.length) {
            long v2 = ((long) v0);
            int v1 = 0;
            while (v1 < 8) {
                if ((v2 & 1) != 1) {
                    v2 >>= 1;
                } else {
                    v2 = ((v2 >> 1) ^ -5.0564049839430436e+45);
                }
                v1++;
            }
            org.tukaani.xz.check.CRC64.crcTable[v0] = v2;
            v0++;
        }
        return;
    }

    public CRC64()
    {
        this.crc = -1;
        this.size = 8;
        this.name = "CRC64";
        return;
    }

    public byte[] finish()
    {
        long v2 = (this.crc ^ -1);
        this.crc = -1;
        byte[] v0 = new byte[8];
        int v1 = 0;
        while (v1 < v0.length) {
            v0[v1] = ((byte) ((int) (v2 >> (v1 * 8))));
            v1++;
        }
        return v0;
    }

    public void update(byte[] p8, int p9, int p10)
    {
        int v0 = (p9 + p10);
        int v1 = p9;
        while (v1 < v0) {
            int v9_1 = (v1 + 1);
            this.crc = (org.tukaani.xz.check.CRC64.crcTable[((p8[v1] ^ ((int) this.crc)) & 255)] ^ (this.crc >> 8));
            v1 = v9_1;
        }
        return;
    }
}
