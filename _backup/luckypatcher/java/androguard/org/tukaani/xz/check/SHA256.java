package org.tukaani.xz.check;
public class SHA256 extends org.tukaani.xz.check.Check {
    private final java.security.MessageDigest sha256;

    public SHA256()
    {
        this.size = 32;
        this.name = "SHA-256";
        this.sha256 = java.security.MessageDigest.getInstance("SHA-256");
        return;
    }

    public byte[] finish()
    {
        byte[] v0 = this.sha256.digest();
        this.sha256.reset();
        return v0;
    }

    public void update(byte[] p2, int p3, int p4)
    {
        this.sha256.update(p2, p3, p4);
        return;
    }
}
