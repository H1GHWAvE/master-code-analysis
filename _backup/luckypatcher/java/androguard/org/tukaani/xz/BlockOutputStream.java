package org.tukaani.xz;
 class BlockOutputStream extends org.tukaani.xz.FinishableOutputStream {
    private final org.tukaani.xz.check.Check check;
    private final long compressedSizeLimit;
    private org.tukaani.xz.FinishableOutputStream filterChain;
    private final int headerSize;
    private final java.io.OutputStream out;
    private final org.tukaani.xz.CountingOutputStream outCounted;
    private final byte[] tempBuf;
    private long uncompressedSize;

    public BlockOutputStream(java.io.OutputStream p9, org.tukaani.xz.FilterEncoder[] p10, org.tukaani.xz.check.Check p11)
    {
        this.uncompressedSize = 0;
        long v4_2 = new byte[1];
        this.tempBuf = v4_2;
        this.out = p9;
        this.check = p11;
        this.outCounted = new org.tukaani.xz.CountingOutputStream(p9);
        this.filterChain = this.outCounted;
        int v3_0 = (p10.length - 1);
        while (v3_0 >= 0) {
            this.filterChain = p10[v3_0].getOutputStream(this.filterChain);
            v3_0--;
        }
        java.io.ByteArrayOutputStream v1_1 = new java.io.ByteArrayOutputStream();
        v1_1.write(0);
        v1_1.write((p10.length - 1));
        int v3_1 = 0;
        while (v3_1 < p10.length) {
            org.tukaani.xz.common.EncoderUtil.encodeVLI(v1_1, p10[v3_1].getFilterID());
            byte[] v2 = p10[v3_1].getFilterProps();
            org.tukaani.xz.common.EncoderUtil.encodeVLI(v1_1, ((long) v2.length));
            v1_1.write(v2);
            v3_1++;
        }
        while ((v1_1.size() & 3) != 0) {
            v1_1.write(0);
        }
        byte[] v0 = v1_1.toByteArray();
        this.headerSize = (v0.length + 4);
        if (this.headerSize <= 1024) {
            v0[0] = ((byte) (v0.length / 4));
            p9.write(v0);
            org.tukaani.xz.common.EncoderUtil.writeCRC32(p9, v0);
            this.compressedSizeLimit = ((nan - ((long) this.headerSize)) - ((long) p11.getSize()));
            return;
        } else {
            throw new org.tukaani.xz.UnsupportedOptionsException();
        }
    }

    private void validate()
    {
        long v0 = this.outCounted.getSize();
        if ((v0 >= 0) && ((v0 <= this.compressedSizeLimit) && (this.uncompressedSize >= 0))) {
            return;
        } else {
            throw new org.tukaani.xz.XZIOException("XZ Stream has grown too big");
        }
    }

    public void finish()
    {
        this.filterChain.finish();
        this.validate();
        long v0 = this.outCounted.getSize();
        while ((3 & v0) != 0) {
            this.out.write(0);
            v0++;
        }
        this.out.write(this.check.finish());
        return;
    }

    public void flush()
    {
        this.filterChain.flush();
        this.validate();
        return;
    }

    public long getUncompressedSize()
    {
        return this.uncompressedSize;
    }

    public long getUnpaddedSize()
    {
        return ((((long) this.headerSize) + this.outCounted.getSize()) + ((long) this.check.getSize()));
    }

    public void write(int p4)
    {
        this.tempBuf[0] = ((byte) p4);
        this.write(this.tempBuf, 0, 1);
        return;
    }

    public void write(byte[] p5, int p6, int p7)
    {
        this.filterChain.write(p5, p6, p7);
        this.check.update(p5, p6, p7);
        this.uncompressedSize = (this.uncompressedSize + ((long) p7));
        this.validate();
        return;
    }
}
