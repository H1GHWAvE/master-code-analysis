package org.tukaani.xz;
public class SingleXZInputStream extends java.io.InputStream {
    private org.tukaani.xz.BlockInputStream blockDecoder;
    private org.tukaani.xz.check.Check check;
    private boolean endReached;
    private java.io.IOException exception;
    private java.io.InputStream in;
    private final org.tukaani.xz.index.IndexHash indexHash;
    private int memoryLimit;
    private org.tukaani.xz.common.StreamFlags streamHeaderFlags;
    private final byte[] tempBuf;

    public SingleXZInputStream(java.io.InputStream p3)
    {
        this.blockDecoder = 0;
        this.indexHash = new org.tukaani.xz.index.IndexHash();
        this.endReached = 0;
        this.exception = 0;
        int v0_4 = new byte[1];
        this.tempBuf = v0_4;
        this.initialize(p3, -1);
        return;
    }

    public SingleXZInputStream(java.io.InputStream p3, int p4)
    {
        this.blockDecoder = 0;
        this.indexHash = new org.tukaani.xz.index.IndexHash();
        this.endReached = 0;
        this.exception = 0;
        byte[] v0_4 = new byte[1];
        this.tempBuf = v0_4;
        this.initialize(p3, p4);
        return;
    }

    SingleXZInputStream(java.io.InputStream p3, int p4, byte[] p5)
    {
        this.blockDecoder = 0;
        this.indexHash = new org.tukaani.xz.index.IndexHash();
        this.endReached = 0;
        this.exception = 0;
        byte[] v0_4 = new byte[1];
        this.tempBuf = v0_4;
        this.initialize(p3, p4, p5);
        return;
    }

    private void initialize(java.io.InputStream p3, int p4)
    {
        byte[] v0 = new byte[12];
        new java.io.DataInputStream(p3).readFully(v0);
        this.initialize(p3, p4, v0);
        return;
    }

    private void initialize(java.io.InputStream p2, int p3, byte[] p4)
    {
        this.in = p2;
        this.memoryLimit = p3;
        this.streamHeaderFlags = org.tukaani.xz.common.DecoderUtil.decodeStreamHeader(p4);
        this.check = org.tukaani.xz.check.Check.getInstance(this.streamHeaderFlags.checkType);
        return;
    }

    private void validateStreamFooter()
    {
        byte[] v0 = new byte[12];
        new java.io.DataInputStream(this.in).readFully(v0);
        org.tukaani.xz.common.StreamFlags v1 = org.tukaani.xz.common.DecoderUtil.decodeStreamFooter(v0);
        if ((org.tukaani.xz.common.DecoderUtil.areStreamFlagsEqual(this.streamHeaderFlags, v1)) && (this.indexHash.getIndexSize() == v1.backwardSize)) {
            return;
        } else {
            throw new org.tukaani.xz.CorruptedInputException("XZ Stream Footer does not match Stream Header");
        }
    }

    public int available()
    {
        if (this.in != null) {
            if (this.exception == null) {
                int v0_4;
                if (this.blockDecoder != null) {
                    v0_4 = this.blockDecoder.available();
                } else {
                    v0_4 = 0;
                }
                return v0_4;
            } else {
                throw this.exception;
            }
        } else {
            throw new org.tukaani.xz.XZIOException("Stream closed");
        }
    }

    public void close()
    {
        if (this.in != null) {
            try {
                this.in.close();
                this.in = 0;
            } catch (Throwable v0_2) {
                this.in = 0;
                throw v0_2;
            }
        }
        return;
    }

    public String getCheckName()
    {
        return this.check.getName();
    }

    public int getCheckType()
    {
        return this.streamHeaderFlags.checkType;
    }

    public int read()
    {
        int v0_0 = -1;
        if (this.read(this.tempBuf, 0, 1) != -1) {
            v0_0 = (this.tempBuf[0] & 255);
        }
        return v0_0;
    }

    public int read(byte[] p13, int p14, int p15)
    {
        if ((p14 >= 0) && ((p15 >= 0) && (((p14 + p15) >= 0) && ((p14 + p15) <= p13.length)))) {
            int v10;
            if (p15 != 0) {
                if (this.in != null) {
                    if (this.exception == null) {
                        if (!this.endReached) {
                            v10 = 0;
                            while (p15 > 0) {
                                try {
                                    if (this.blockDecoder == null) {
                                        this.blockDecoder = new org.tukaani.xz.BlockInputStream(this.in, this.check, this.memoryLimit, -1, -1);
                                    }
                                    int v9 = this.blockDecoder.read(p13, p14, p15);
                                    if (v9 <= 0) {
                                        if (v9 == -1) {
                                            this.indexHash.add(this.blockDecoder.getUnpaddedSize(), this.blockDecoder.getUncompressedSize());
                                            this.blockDecoder = 0;
                                        }
                                    } else {
                                        v10 += v9;
                                        p14 += v9;
                                        p15 -= v9;
                                    }
                                } catch (org.tukaani.xz.IndexIndicatorException v8) {
                                    this.exception = v8;
                                    if (v10 != 0) {
                                        break;
                                    }
                                    throw v8;
                                }
                            }
                        } else {
                            v10 = -1;
                        }
                    } else {
                        throw this.exception;
                    }
                } else {
                    throw new org.tukaani.xz.XZIOException("Stream closed");
                }
            } else {
                v10 = 0;
            }
            return v10;
        } else {
            throw new IndexOutOfBoundsException();
        }
    }
}
