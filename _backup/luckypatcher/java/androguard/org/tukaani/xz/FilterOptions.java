package org.tukaani.xz;
public abstract class FilterOptions implements java.lang.Cloneable {

    FilterOptions()
    {
        return;
    }

    public static int getDecoderMemoryUsage(org.tukaani.xz.FilterOptions[] p3)
    {
        int v1 = 0;
        int v0 = 0;
        while (v0 < p3.length) {
            v1 += p3[v0].getDecoderMemoryUsage();
            v0++;
        }
        return v1;
    }

    public static int getEncoderMemoryUsage(org.tukaani.xz.FilterOptions[] p3)
    {
        int v1 = 0;
        int v0 = 0;
        while (v0 < p3.length) {
            v1 += p3[v0].getEncoderMemoryUsage();
            v0++;
        }
        return v1;
    }

    public abstract int getDecoderMemoryUsage();

    public abstract int getEncoderMemoryUsage();

    abstract org.tukaani.xz.FilterEncoder getFilterEncoder();

    public abstract java.io.InputStream getInputStream();

    public abstract org.tukaani.xz.FinishableOutputStream getOutputStream();
}
