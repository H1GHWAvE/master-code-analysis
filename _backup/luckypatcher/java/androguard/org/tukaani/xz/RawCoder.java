package org.tukaani.xz;
 class RawCoder {

    RawCoder()
    {
        return;
    }

    static void validate(org.tukaani.xz.FilterCoder[] p4)
    {
        int v1_0 = 0;
        while (v1_0 < (p4.length - 1)) {
            if (p4[v1_0].nonLastOK()) {
                v1_0++;
            } else {
                throw new org.tukaani.xz.UnsupportedOptionsException("Unsupported XZ filter chain");
            }
        }
        if (p4[(p4.length - 1)].lastOK()) {
            int v0 = 0;
            int v1_1 = 0;
            while (v1_1 < p4.length) {
                if (p4[v1_1].changesSize()) {
                    v0++;
                }
                v1_1++;
            }
            if (v0 <= 3) {
                return;
            } else {
                throw new org.tukaani.xz.UnsupportedOptionsException("Unsupported XZ filter chain");
            }
        } else {
            throw new org.tukaani.xz.UnsupportedOptionsException("Unsupported XZ filter chain");
        }
    }
}
