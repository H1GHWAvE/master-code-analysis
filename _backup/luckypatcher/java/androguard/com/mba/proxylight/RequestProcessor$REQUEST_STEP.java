package com.mba.proxylight;
final enum class RequestProcessor$REQUEST_STEP extends java.lang.Enum {
    private static final synthetic com.mba.proxylight.RequestProcessor$REQUEST_STEP[] $VALUES;
    public static final enum com.mba.proxylight.RequestProcessor$REQUEST_STEP REQUEST_CONTENT;
    public static final enum com.mba.proxylight.RequestProcessor$REQUEST_STEP REQUEST_HEADERS;
    public static final enum com.mba.proxylight.RequestProcessor$REQUEST_STEP STATUS_LINE;
    public static final enum com.mba.proxylight.RequestProcessor$REQUEST_STEP TRANSFER;

    static RequestProcessor$REQUEST_STEP()
    {
        com.mba.proxylight.RequestProcessor$REQUEST_STEP.STATUS_LINE = new com.mba.proxylight.RequestProcessor$REQUEST_STEP("STATUS_LINE", 0);
        com.mba.proxylight.RequestProcessor$REQUEST_STEP.REQUEST_HEADERS = new com.mba.proxylight.RequestProcessor$REQUEST_STEP("REQUEST_HEADERS", 1);
        com.mba.proxylight.RequestProcessor$REQUEST_STEP.REQUEST_CONTENT = new com.mba.proxylight.RequestProcessor$REQUEST_STEP("REQUEST_CONTENT", 2);
        com.mba.proxylight.RequestProcessor$REQUEST_STEP.TRANSFER = new com.mba.proxylight.RequestProcessor$REQUEST_STEP("TRANSFER", 3);
        com.mba.proxylight.RequestProcessor$REQUEST_STEP[] v0_9 = new com.mba.proxylight.RequestProcessor$REQUEST_STEP[4];
        v0_9[0] = com.mba.proxylight.RequestProcessor$REQUEST_STEP.STATUS_LINE;
        v0_9[1] = com.mba.proxylight.RequestProcessor$REQUEST_STEP.REQUEST_HEADERS;
        v0_9[2] = com.mba.proxylight.RequestProcessor$REQUEST_STEP.REQUEST_CONTENT;
        v0_9[3] = com.mba.proxylight.RequestProcessor$REQUEST_STEP.TRANSFER;
        com.mba.proxylight.RequestProcessor$REQUEST_STEP.$VALUES = v0_9;
        return;
    }

    private RequestProcessor$REQUEST_STEP(String p1, int p2)
    {
        this(p1, p2);
        return;
    }

    public static com.mba.proxylight.RequestProcessor$REQUEST_STEP valueOf(String p1)
    {
        return ((com.mba.proxylight.RequestProcessor$REQUEST_STEP) Enum.valueOf(com.mba.proxylight.RequestProcessor$REQUEST_STEP, p1));
    }

    public static com.mba.proxylight.RequestProcessor$REQUEST_STEP[] values()
    {
        return ((com.mba.proxylight.RequestProcessor$REQUEST_STEP[]) com.mba.proxylight.RequestProcessor$REQUEST_STEP.$VALUES.clone());
    }
}
