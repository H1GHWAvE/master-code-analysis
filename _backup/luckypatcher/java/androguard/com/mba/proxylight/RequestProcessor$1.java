package com.mba.proxylight;
 class RequestProcessor$1 implements java.lang.Runnable {
    final synthetic com.mba.proxylight.RequestProcessor this$0;

    RequestProcessor$1(com.mba.proxylight.RequestProcessor p1)
    {
        this.this$0 = p1;
        return;
    }

    public void run()
    {
        com.mba.proxylight.RequestProcessor.access$008();
        try {
            while(true) {
                System.out.println(new StringBuilder().append("inData:").append(com.mba.proxylight.RequestProcessor.access$1200(this.this$0)).toString());
                com.mba.proxylight.RequestProcessor.access$1202(this.this$0, "");
                System.out.println(new StringBuilder().append("outData:").append(com.mba.proxylight.RequestProcessor.access$1900(this.this$0)).toString());
                com.mba.proxylight.RequestProcessor.access$1902(this.this$0, "");
                com.mba.proxylight.RequestProcessor.access$800(this.this$0);
                this.this$0.recycle();
            }
            System.out.println(new StringBuilder().append("inData:").append(com.mba.proxylight.RequestProcessor.access$1200(this.this$0)).toString());
            com.mba.proxylight.RequestProcessor.access$1202(this.this$0, "");
            System.out.println(new StringBuilder().append("outData:").append(com.mba.proxylight.RequestProcessor.access$1900(this.this$0)).toString());
            com.mba.proxylight.RequestProcessor.access$1902(this.this$0, "");
            com.mba.proxylight.RequestProcessor.access$800(this.this$0);
            this.this$0.recycle();
        } catch (String v35_164) {
            com.mba.proxylight.RequestProcessor.access$800(this.this$0);
            com.mba.proxylight.RequestProcessor.access$010();
            this.this$0.error(new StringBuilder().append("Fin du processor ").append(com.mba.proxylight.RequestProcessor.access$700(this.this$0)).toString(), 0);
            throw v35_164;
        }
        com.mba.proxylight.RequestProcessor.access$102(this.this$0, 1);
        if ((com.mba.proxylight.RequestProcessor.access$200(this.this$0) != null) || (com.mba.proxylight.RequestProcessor.access$300(this.this$0))) {
            if (!com.mba.proxylight.RequestProcessor.access$300(this.this$0)) {
                int v25_0 = 0;
                int v6 = 0;
                try {
                    com.mba.proxylight.RequestProcessor$REQUEST_STEP v33 = com.mba.proxylight.RequestProcessor$REQUEST_STEP.STATUS_LINE;
                } catch (String v35_156) {
                    System.out.println(new StringBuilder().append("inData:").append(com.mba.proxylight.RequestProcessor.access$1200(this.this$0)).toString());
                    com.mba.proxylight.RequestProcessor.access$1202(this.this$0, "");
                    System.out.println(new StringBuilder().append("outData:").append(com.mba.proxylight.RequestProcessor.access$1900(this.this$0)).toString());
                    com.mba.proxylight.RequestProcessor.access$1902(this.this$0, "");
                    com.mba.proxylight.RequestProcessor.access$800(this.this$0);
                    this.this$0.recycle();
                    throw v35_156;
                } catch (Exception v7_1) {
                    this.this$0.error(0, v7_1);
                }
                while (com.mba.proxylight.RequestProcessor.access$200(this.this$0) != null) {
                    com.mba.proxylight.RequestProcessor.access$200(this.this$0).select(5000);
                    if (com.mba.proxylight.RequestProcessor.access$400(this.this$0) == null) {
                        break;
                    }
                    long v20 = System.currentTimeMillis();
                    if (com.mba.proxylight.RequestProcessor.access$200(this.this$0).selectedKeys().size() == 0) {
                        long v18 = (v20 - com.mba.proxylight.RequestProcessor.access$500());
                        java.util.Iterator v11 = com.mba.proxylight.RequestProcessor.access$600(this.this$0).entrySet().iterator();
                        while (v11.hasNext()) {
                            java.util.Map$Entry v8_1 = ((java.util.Map$Entry) v11.next());
                            com.mba.proxylight.Socket v31_1 = ((com.mba.proxylight.Socket) v8_1.getValue());
                            if (Math.max(v31_1.lastRead, v31_1.lastWrite) < v18) {
                                this.this$0.debug(new StringBuilder().append("processeur ").append(com.mba.proxylight.RequestProcessor.access$700(this.this$0)).append(" : Fermeture pour inactivite de la socket vers ").append(((String) v8_1.getKey())).toString());
                                if ((v25_0 == 0) || (!"CONNECT".equals(v25_0.getMethod()))) {
                                    v11.remove();
                                    try {
                                        v31_1.socket.close();
                                    } catch (Exception v9) {
                                        this.this$0.error("", v9);
                                    }
                                    if (v31_1 == com.mba.proxylight.RequestProcessor.access$900(this.this$0)) {
                                        com.mba.proxylight.RequestProcessor.access$902(this.this$0, 0);
                                    }
                                } else {
                                    com.mba.proxylight.RequestProcessor.access$800(this.this$0);
                                    break;
                                }
                            }
                        }
                        if (com.mba.proxylight.RequestProcessor.access$600(this.this$0).size() == 0) {
                            com.mba.proxylight.RequestProcessor.access$800(this.this$0);
                        }
                        if (com.mba.proxylight.RequestProcessor.access$400(this.this$0) == null) {
                            break;
                        }
                    }
                    java.util.Iterator v28 = com.mba.proxylight.RequestProcessor.access$200(this.this$0).selectedKeys().iterator();
                    while (v28.hasNext()) {
                        java.nio.channels.SelectionKey v15_1 = ((java.nio.channels.SelectionKey) v28.next());
                        v28.remove();
                        if ((v15_1.isValid()) && (v15_1.isReadable())) {
                            com.mba.proxylight.Socket v32_1 = ((com.mba.proxylight.Socket) v15_1.attachment());
                            if (v32_1 != com.mba.proxylight.RequestProcessor.access$400(this.this$0)) {
                                if (v32_1 == com.mba.proxylight.RequestProcessor.access$900(this.this$0)) {
                                    if (!com.mba.proxylight.RequestProcessor.access$1800(this.this$0, v32_1, com.mba.proxylight.RequestProcessor.access$400(this.this$0), v20)) {
                                        if (!"CONNECT".equals(v25_0.getMethod())) {
                                            com.mba.proxylight.RequestProcessor.access$1700(this.this$0, v32_1);
                                            com.mba.proxylight.RequestProcessor.access$902(this.this$0, 0);
                                            break;
                                        } else {
                                            com.mba.proxylight.RequestProcessor.access$800(this.this$0);
                                            break;
                                        }
                                    }
                                } else {
                                    com.mba.proxylight.RequestProcessor.access$1700(this.this$0, v32_1);
                                }
                            } else {
                                com.mba.proxylight.RequestProcessor.access$1000(this.this$0).clear();
                                int v22 = com.mba.proxylight.RequestProcessor.access$1100(this.this$0, com.mba.proxylight.RequestProcessor.access$400(this.this$0), com.mba.proxylight.RequestProcessor.access$1000(this.this$0), v20);
                                com.mba.proxylight.RequestProcessor.access$1202(this.this$0, new StringBuilder().append(com.mba.proxylight.RequestProcessor.access$1200(this.this$0)).append(new String(com.mba.proxylight.RequestProcessor.access$1000(this.this$0).array())).toString());
                                if (v22 != -1) {
                                    if (v22 > 0) {
                                        com.mba.proxylight.RequestProcessor.access$1000(this.this$0).flip();
                                        int v26 = v25_0;
                                        while (com.mba.proxylight.RequestProcessor.access$1000(this.this$0).remaining() > 0) {
                                            if (v33 != com.mba.proxylight.RequestProcessor$REQUEST_STEP.STATUS_LINE) {
                                                if (v33 != com.mba.proxylight.RequestProcessor$REQUEST_STEP.REQUEST_HEADERS) {
                                                    if (v33 != com.mba.proxylight.RequestProcessor$REQUEST_STEP.REQUEST_CONTENT) {
                                                        if (v33 == com.mba.proxylight.RequestProcessor$REQUEST_STEP.TRANSFER) {
                                                            com.mba.proxylight.RequestProcessor.access$1600(this.this$0, com.mba.proxylight.RequestProcessor.access$900(this.this$0), com.mba.proxylight.RequestProcessor.access$1000(this.this$0), v20);
                                                        }
                                                    } else {
                                                        v6 -= com.mba.proxylight.RequestProcessor.access$1000(this.this$0).remaining();
                                                        if (v6 <= 0) {
                                                            v33 = com.mba.proxylight.RequestProcessor$REQUEST_STEP.STATUS_LINE;
                                                        }
                                                        com.mba.proxylight.RequestProcessor.access$900(this.this$0).socket.write(com.mba.proxylight.RequestProcessor.access$1000(this.this$0));
                                                    }
                                                } else {
                                                    String v27_0 = com.mba.proxylight.RequestProcessor.access$1300(this.this$0, com.mba.proxylight.RequestProcessor.access$1000(this.this$0));
                                                    if (v27_0 != null) {
                                                        if (v27_0.length() != 0) {
                                                            v26.addHeader(v27_0);
                                                        } else {
                                                            if (!com.mba.proxylight.RequestProcessor.access$1400(this.this$0, v26)) {
                                                                int v14;
                                                                boolean v13 = "GET".equals(v26.getMethod());
                                                                if ((v13) || (!"POST".equals(v26.getMethod()))) {
                                                                    v14 = 0;
                                                                } else {
                                                                    v14 = 1;
                                                                }
                                                                if ((v13) || ((v14 != 0) || (!"CONNECT".equals(v26.getMethod())))) {
                                                                    int v12 = 0;
                                                                } else {
                                                                    v12 = 1;
                                                                }
                                                                if ((v13) || ((v14 != 0) || (v12 != 0))) {
                                                                    String v23 = new StringBuilder().append(v26.getHost()).append(":").append(v26.getPort()).toString();
                                                                    com.mba.proxylight.Socket v24_1 = ((com.mba.proxylight.Socket) com.mba.proxylight.RequestProcessor.access$600(this.this$0).get(v23));
                                                                    if (v24_1 == null) {
                                                                        v24_1 = new com.mba.proxylight.Socket();
                                                                        v24_1.socket = java.nio.channels.SocketChannel.open();
                                                                        v24_1.socket.configureBlocking(0);
                                                                        if (!v24_1.socket.connect(new java.net.InetSocketAddress(this.this$0.resolve(v26.getHost()), v26.getPort()))) {
                                                                            do {
                                                                                Thread.sleep(50);
                                                                            } while(!v24_1.socket.finishConnect());
                                                                        }
                                                                        v24_1.socket.register(com.mba.proxylight.RequestProcessor.access$200(this.this$0), 1, v24_1);
                                                                        com.mba.proxylight.RequestProcessor.access$600(this.this$0).put(v23, v24_1);
                                                                        this.this$0.debug(new StringBuilder().append("Ajout d\'une socket vers ").append(v23).append(" sur le processeur ").append(com.mba.proxylight.RequestProcessor.access$700(this.this$0)).append(". Socket count=").append(com.mba.proxylight.RequestProcessor.access$600(this.this$0).size()).toString());
                                                                    }
                                                                    com.mba.proxylight.RequestProcessor.access$902(this.this$0, v24_1);
                                                                    if (v12 == 0) {
                                                                        StringBuffer v29 = new StringBuffer(v26.getMethod()).append(" ");
                                                                        String v34 = v26.getUrl();
                                                                        if (!v34.startsWith("/")) {
                                                                            v34 = v34.substring(v34.indexOf(47, 8));
                                                                        }
                                                                        v29.append(v34).append(" ").append(v26.getProtocol()).append("\r\n");
                                                                        String v36_53 = v26.getHeaders().entrySet().iterator();
                                                                        while (v36_53.hasNext()) {
                                                                            java.util.Map$Entry v10_1 = ((java.util.Map$Entry) v36_53.next());
                                                                            v29.append(((String) v10_1.getKey())).append(": ").append(((String) v10_1.getValue())).append("\r\n");
                                                                        }
                                                                        v29.append("\r\n");
                                                                        com.mba.proxylight.RequestProcessor.access$1600(this.this$0, v24_1, java.nio.ByteBuffer.wrap(v29.toString().getBytes()), v20);
                                                                        v6 = 0;
                                                                        if (v14 != 0) {
                                                                            v6 = Integer.parseInt(((String) v26.getHeaders().get("Content-Length")));
                                                                        }
                                                                        if (v6 != 0) {
                                                                            v33 = com.mba.proxylight.RequestProcessor$REQUEST_STEP.REQUEST_CONTENT;
                                                                        } else {
                                                                            v33 = com.mba.proxylight.RequestProcessor$REQUEST_STEP.STATUS_LINE;
                                                                        }
                                                                    } else {
                                                                        com.mba.proxylight.RequestProcessor.access$1600(this.this$0, com.mba.proxylight.RequestProcessor.access$400(this.this$0), java.nio.ByteBuffer.wrap(com.mba.proxylight.RequestProcessor.access$1500()), v20);
                                                                        v33 = com.mba.proxylight.RequestProcessor$REQUEST_STEP.TRANSFER;
                                                                    }
                                                                } else {
                                                                    throw new RuntimeException(new StringBuilder().append("Unknown method : ").append(v26.getMethod()).toString());
                                                                }
                                                            } else {
                                                                throw new Exception("Requete interdite.");
                                                            }
                                                        }
                                                    }
                                                }
                                            } else {
                                                int v25_1;
                                                String v27_1 = com.mba.proxylight.RequestProcessor.access$1300(this.this$0, com.mba.proxylight.RequestProcessor.access$1000(this.this$0));
                                                if (v27_1 == null) {
                                                    v25_1 = v26;
                                                } else {
                                                    v25_1 = new com.mba.proxylight.Request();
                                                    v25_1.setStatusline(v27_1);
                                                    v33 = com.mba.proxylight.RequestProcessor$REQUEST_STEP.REQUEST_HEADERS;
                                                }
                                                v26 = v25_1;
                                            }
                                        }
                                        v25_0 = v26;
                                    }
                                } else {
                                    com.mba.proxylight.RequestProcessor.access$800(this.this$0);
                                    break;
                                }
                            }
                        }
                    }
                }
            } else {
                com.mba.proxylight.RequestProcessor.access$800(this.this$0);
                com.mba.proxylight.RequestProcessor.access$010();
                this.this$0.error(new StringBuilder().append("Fin du processor ").append(com.mba.proxylight.RequestProcessor.access$700(this.this$0)).toString(), 0);
            }
        } else {
            try {
                this.this$0.wait(20000);
            } catch (Exception v7_0) {
                this.this$0.error(0, v7_0);
                com.mba.proxylight.RequestProcessor.access$800(this.this$0);
                com.mba.proxylight.RequestProcessor.access$010();
                this.this$0.error(new StringBuilder().append("Fin du processor ").append(com.mba.proxylight.RequestProcessor.access$700(this.this$0)).toString(), 0);
            }
        }
        return;
    }
}
