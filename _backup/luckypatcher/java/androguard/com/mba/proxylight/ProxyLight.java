package com.mba.proxylight;
public class ProxyLight {
    private java.util.List filters;
    private java.util.Map ipCache;
    private int port;
    private java.util.Stack processors;
    private String remoteProxyHost;
    private int remoteProxyPort;
    boolean running;
    private java.nio.channels.Selector selector;
    java.nio.channels.ServerSocketChannel server;

    public ProxyLight()
    {
        this.port = 8080;
        this.running = 0;
        this.processors = new java.util.Stack();
        this.selector = 0;
        this.remoteProxyHost = 0;
        this.remoteProxyPort = 8080;
        this.server = 0;
        this.ipCache = new java.util.HashMap();
        this.filters = new java.util.ArrayList();
        return;
    }

    static synthetic java.nio.channels.Selector access$000(com.mba.proxylight.ProxyLight p1)
    {
        return p1.selector;
    }

    static synthetic java.nio.channels.Selector access$002(com.mba.proxylight.ProxyLight p0, java.nio.channels.Selector p1)
    {
        p0.selector = p1;
        return p1;
    }

    static synthetic java.util.Stack access$100(com.mba.proxylight.ProxyLight p1)
    {
        return p1.processors;
    }

    public void debug(String p2)
    {
        if (p2 != null) {
            System.err.println(p2);
        }
        return;
    }

    public void error(String p2, Throwable p3)
    {
        if (p2 != null) {
            System.err.println(p2);
        }
        // Both branches of the condition point to the same code.
        // if (p3 == null) {
            return;
        // }
    }

    public int getPort()
    {
        return this.port;
    }

    public String getRemoteProxyHost()
    {
        return this.remoteProxyHost;
    }

    public int getRemoteProxyPort()
    {
        return this.remoteProxyPort;
    }

    public java.util.List getRequestFilters()
    {
        return this.filters;
    }

    public boolean isRunning()
    {
        return this.running;
    }

    public void recycle(com.mba.proxylight.RequestProcessor p3)
    {
        try {
            this.processors.add(p3);
            return;
        } catch (Throwable v0_1) {
            throw v0_1;
        }
    }

    protected java.net.InetAddress resolve(String p5)
    {
        java.net.InetAddress v0_1 = ((java.net.InetAddress) this.ipCache.get(p5));
        if (v0_1 != null) {
            String v3_3 = v0_1;
        } else {
            try {
                v0_1 = java.net.InetAddress.getByName(p5);
                this.ipCache.put(p5, v0_1);
            } catch (java.net.UnknownHostException v2) {
                v3_3 = 0;
            } catch (Throwable v1) {
                this.error("", v1);
            }
        }
        return v3_3;
    }

    public void setPort(int p1)
    {
        this.port = p1;
        return;
    }

    public void setRemoteProxy(String p1, int p2)
    {
        this.remoteProxyHost = p1;
        this.remoteProxyPort = p2;
        return;
    }

    public declared_synchronized void start()
    {
        try {
            if (!this.running) {
                this.running = 1;
                Thread v0_1 = new Thread(new com.mba.proxylight.ProxyLight$1(this));
                v0_1.setDaemon(0);
                v0_1.setName("ProxyLight server");
                v0_1.start();
            }
        } catch (String v1_6) {
            throw v1_6;
        }
        return;
    }

    public void stop()
    {
        if (this.running) {
            try {
                this.server.close();
                this.server = 0;
                this.selector.wakeup();
                this.selector = 0;
            } catch (Exception v0) {
                this.error(0, v0);
            }
            while (this.processors.size() > 0) {
                ((com.mba.proxylight.RequestProcessor) this.processors.pop()).shutdown();
            }
        }
        return;
    }
}
