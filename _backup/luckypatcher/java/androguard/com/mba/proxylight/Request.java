package com.mba.proxylight;
public class Request {
    private static java.util.regex.Pattern CONNECT_PATTERN;
    private static java.util.regex.Pattern GETPOST_PATTERN;
    private java.util.Map headers;
    private String host;
    private String method;
    private int port;
    private String protocol;
    private String statusline;
    private String url;

    static Request()
    {
        com.mba.proxylight.Request.CONNECT_PATTERN = java.util.regex.Pattern.compile("(.*):([\\d]+)");
        com.mba.proxylight.Request.GETPOST_PATTERN = java.util.regex.Pattern.compile("(https?)://([^:/]+)(:[\\d]+])?/.*");
        return;
    }

    public Request()
    {
        this.statusline = 0;
        this.method = 0;
        this.url = 0;
        this.protocol = 0;
        this.headers = new java.util.LinkedHashMap();
        this.host = 0;
        this.port = -1;
        return;
    }

    public void addHeader(String p7)
    {
        int v0 = 1;
        while ((p7.charAt(v0) != 58) && (p7.charAt(v0) != 32)) {
            v0++;
        }
        String v1 = p7.substring(0, v0);
        do {
            v0++;
        } while((p7.charAt(v0) == 58) || (p7.charAt(v0) == 32));
        this.headers.put(v1, p7.substring(v0));
        return;
    }

    public void dump()
    {
        return;
    }

    public java.util.Map getHeaders()
    {
        return this.headers;
    }

    public String getHost()
    {
        int v2_29;
        if (this.host == null) {
            if (this.getUrl() != null) {
                if (!"CONNECT".equals(this.method)) {
                    java.util.regex.Matcher v1_0 = com.mba.proxylight.Request.GETPOST_PATTERN.matcher(this.getUrl());
                    if (v1_0.matches()) {
                        this.host = v1_0.group(2);
                        if (v1_0.group(3) == null) {
                            if (!"http".equals(v1_0.group(1))) {
                                this.port = 443;
                            } else {
                                this.port = 80;
                            }
                        } else {
                            Integer.parseInt(v1_0.group(3).substring(1));
                        }
                    }
                } else {
                    java.util.regex.Matcher v1_1 = com.mba.proxylight.Request.CONNECT_PATTERN.matcher(this.getUrl());
                    if (v1_1.matches()) {
                        this.host = v1_1.group(1);
                        this.port = Integer.parseInt(v1_1.group(2));
                    }
                }
                if (this.host == null) {
                    this.host = ((String) this.getHeaders().get("Host"));
                    int v0 = this.host.indexOf(58);
                    if (v0 <= -1) {
                        this.port = 80;
                    } else {
                        this.port = Integer.parseInt(this.host.substring((v0 + 1)));
                        this.host = this.host.substring(0, v0);
                    }
                }
            }
            v2_29 = this.host;
        } else {
            v2_29 = this.host;
        }
        return v2_29;
    }

    public String getMethod()
    {
        return this.method;
    }

    public int getPort()
    {
        this.getHost();
        return this.port;
    }

    public String getProtocol()
    {
        return this.protocol;
    }

    public String getStatusline()
    {
        return this.statusline;
    }

    public String getUrl()
    {
        return this.url;
    }

    public void setMethod(String p1)
    {
        this.method = p1;
        return;
    }

    public void setProtocol(String p1)
    {
        this.protocol = p1;
        return;
    }

    public void setStatusline(String p6)
    {
        this.statusline = p6;
        int v0 = p6.indexOf(32);
        if ((v0 != -1) && (v0 >= 3)) {
            this.method = p6.substring(0, v0);
            do {
                v0++;
            } while(p6.charAt(v0) == 32);
            int v1 = p6.indexOf(32, v0);
            if (v1 != -1) {
                this.url = p6.substring(v0, v1);
                do {
                    v1++;
                } while(p6.charAt(v1) == 32);
                this.protocol = p6.substring(v1);
                return;
            } else {
                throw new IllegalArgumentException(new StringBuilder().append("statusline: ").append(p6).toString());
            }
        } else {
            throw new IllegalArgumentException(new StringBuilder().append("statusline: ").append(p6).toString());
        }
    }

    public void setUrl(String p1)
    {
        this.url = p1;
        return;
    }
}
