package com.mba.proxylight;
 class ProxyLight$1 implements java.lang.Runnable {
    final synthetic com.mba.proxylight.ProxyLight this$0;

    ProxyLight$1(com.mba.proxylight.ProxyLight p1)
    {
        this.this$0 = p1;
        return;
    }

    public void run()
    {
        try {
            this.this$0.server = java.nio.channels.ServerSocketChannel.open();
            this.this$0.server.configureBlocking(0);
            this.this$0.server.socket().setReuseAddress(1);
            this.this$0.server.socket().bind(new java.net.InetSocketAddress(8080));
            com.mba.proxylight.ProxyLight.access$002(this.this$0, java.nio.channels.Selector.open());
            this.this$0.server.register(com.mba.proxylight.ProxyLight.access$000(this.this$0), 16);
        } catch (boolean v6) {
            this.this$0.running = 0;
            return;
        } catch (boolean v6_39) {
            this.this$0.running = 0;
            throw v6_39;
        } catch (Throwable v5_1) {
            this.this$0.error(0, v5_1);
            this.this$0.running = 0;
            return;
        }
        while(true) {
            com.mba.proxylight.ProxyLight.access$000(this.this$0).select();
            if (this.this$0.server != null) {
                java.util.Iterator v1 = com.mba.proxylight.ProxyLight.access$000(this.this$0).selectedKeys().iterator();
                while (v1.hasNext()) {
                    java.nio.channels.SelectionKey v2_1 = ((java.nio.channels.SelectionKey) v1.next());
                    v1.remove();
                    if ((v2_1.isValid()) && (v2_1.isAcceptable())) {
                        com.mba.proxylight.RequestProcessor v3_0 = 0;
                        try {
                            com.mba.proxylight.ProxyLight.access$100(this.this$0);
                            try {
                                while (com.mba.proxylight.ProxyLight.access$100(this.this$0).size() > 0) {
                                    v3_0 = ((com.mba.proxylight.RequestProcessor) com.mba.proxylight.ProxyLight.access$100(this.this$0).pop());
                                    if (v3_0.isAlive()) {
                                        break;
                                    }
                                }
                            } catch (boolean v6_30) {
                                throw v6_30;
                            }
                            com.mba.proxylight.RequestProcessor v4 = v3_0;
                            try {
                                if ((v4 != null) && (v4.isAlive())) {
                                    com.mba.proxylight.RequestProcessor v3_1 = v4;
                                } else {
                                    v3_1 = new com.mba.proxylight.ProxyLight$1$1(this);
                                }
                                v3_1.process(v2_1);
                            } catch (boolean v6_30) {
                            } catch (Throwable v5_0) {
                                if (this.this$0.server != null) {
                                    this.this$0.error(0, v5_0);
                                } else {
                                    this.this$0.running = 0;
                                    return;
                                }
                            }
                        } catch (Throwable v5_0) {
                        }
                    }
                }
            } else {
                this.this$0.running = 0;
                return;
            }
        }
    }
}
