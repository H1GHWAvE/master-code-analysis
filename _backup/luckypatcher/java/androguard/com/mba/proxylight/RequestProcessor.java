package com.mba.proxylight;
public abstract class RequestProcessor {
    private static final byte[] CONNECT_OK = None;
    private static final String CRLF = "
";
    private static long SOCKET_TIMEOUT;
    private static int processorsCount;
    private static int processorsCpt;
    private boolean alive;
    private com.mba.proxylight.Socket currentOutSocket;
    private String inData;
    private com.mba.proxylight.Socket inSocket;
    private String outData;
    private java.util.Map outSockets;
    private int processorIdx;
    private java.nio.ByteBuffer readBuffer;
    private char[] read_buf;
    int read_offset;
    private java.nio.channels.Selector selector;
    private boolean shutdown;
    private Thread t;

    static RequestProcessor()
    {
        com.mba.proxylight.RequestProcessor.processorsCpt = 1;
        com.mba.proxylight.RequestProcessor.processorsCount = 0;
        com.mba.proxylight.RequestProcessor.SOCKET_TIMEOUT = 15000;
        com.mba.proxylight.RequestProcessor.CONNECT_OK = "HTTP/1.0 200 Connection established\r\nProxy-agent: ProxyLight\r\n\r\n".getBytes();
        return;
    }

    public RequestProcessor()
    {
        this.t = 0;
        this.alive = 0;
        this.shutdown = 0;
        this.processorIdx = 1;
        this.selector = 0;
        this.readBuffer = java.nio.ByteBuffer.allocate(128);
        this.inSocket = 0;
        this.inData = "";
        this.outData = "";
        this.outSockets = new java.util.HashMap();
        this.currentOutSocket = 0;
        String v0_5 = new char[128];
        this.read_buf = v0_5;
        this.read_offset = 0;
        this.t = new Thread(new com.mba.proxylight.RequestProcessor$1(this));
        String v0_8 = this.t;
        String v1_5 = new StringBuilder().append("ProxyLight processor - ");
        int v2_2 = com.mba.proxylight.RequestProcessor.processorsCpt;
        com.mba.proxylight.RequestProcessor.processorsCpt = (v2_2 + 1);
        this.processorIdx = v2_2;
        v0_8.setName(v1_5.append(v2_2).toString());
        this.t.setDaemon(1);
        this.t.start();
        while (!this.isAlive()) {
            new com.chelpus.Utils("w").waitLP(50);
        }
        this.debug(new StringBuilder().append("Processeur ").append(this.processorIdx).append(" demarre.").toString());
        return;
    }

    static synthetic int access$008()
    {
        int v0 = com.mba.proxylight.RequestProcessor.processorsCount;
        com.mba.proxylight.RequestProcessor.processorsCount = (v0 + 1);
        return v0;
    }

    static synthetic int access$010()
    {
        int v0 = com.mba.proxylight.RequestProcessor.processorsCount;
        com.mba.proxylight.RequestProcessor.processorsCount = (v0 - 1);
        return v0;
    }

    static synthetic java.nio.ByteBuffer access$1000(com.mba.proxylight.RequestProcessor p1)
    {
        return p1.readBuffer;
    }

    static synthetic boolean access$102(com.mba.proxylight.RequestProcessor p0, boolean p1)
    {
        p0.alive = p1;
        return p1;
    }

    static synthetic int access$1100(com.mba.proxylight.RequestProcessor p1, com.mba.proxylight.Socket p2, java.nio.ByteBuffer p3, long p4)
    {
        return p1.read(p2, p3, p4);
    }

    static synthetic String access$1200(com.mba.proxylight.RequestProcessor p1)
    {
        return p1.inData;
    }

    static synthetic String access$1202(com.mba.proxylight.RequestProcessor p0, String p1)
    {
        p0.inData = p1;
        return p1;
    }

    static synthetic String access$1300(com.mba.proxylight.RequestProcessor p1, java.nio.ByteBuffer p2)
    {
        return p1.readNext(p2);
    }

    static synthetic boolean access$1400(com.mba.proxylight.RequestProcessor p1, com.mba.proxylight.Request p2)
    {
        return p1.filterRequest(p2);
    }

    static synthetic byte[] access$1500()
    {
        return com.mba.proxylight.RequestProcessor.CONNECT_OK;
    }

    static synthetic void access$1600(com.mba.proxylight.RequestProcessor p0, com.mba.proxylight.Socket p1, java.nio.ByteBuffer p2, long p3)
    {
        p0.write(p1, p2, p3);
        return;
    }

    static synthetic void access$1700(com.mba.proxylight.RequestProcessor p0, com.mba.proxylight.Socket p1)
    {
        p0.closeOutSocket(p1);
        return;
    }

    static synthetic boolean access$1800(com.mba.proxylight.RequestProcessor p1, com.mba.proxylight.Socket p2, com.mba.proxylight.Socket p3, long p4)
    {
        return p1.transfer(p2, p3, p4);
    }

    static synthetic String access$1900(com.mba.proxylight.RequestProcessor p1)
    {
        return p1.outData;
    }

    static synthetic String access$1902(com.mba.proxylight.RequestProcessor p0, String p1)
    {
        p0.outData = p1;
        return p1;
    }

    static synthetic java.nio.channels.Selector access$200(com.mba.proxylight.RequestProcessor p1)
    {
        return p1.selector;
    }

    static synthetic boolean access$300(com.mba.proxylight.RequestProcessor p1)
    {
        return p1.shutdown;
    }

    static synthetic com.mba.proxylight.Socket access$400(com.mba.proxylight.RequestProcessor p1)
    {
        return p1.inSocket;
    }

    static synthetic long access$500()
    {
        return com.mba.proxylight.RequestProcessor.SOCKET_TIMEOUT;
    }

    static synthetic java.util.Map access$600(com.mba.proxylight.RequestProcessor p1)
    {
        return p1.outSockets;
    }

    static synthetic int access$700(com.mba.proxylight.RequestProcessor p1)
    {
        return p1.processorIdx;
    }

    static synthetic void access$800(com.mba.proxylight.RequestProcessor p0)
    {
        p0.closeAll();
        return;
    }

    static synthetic com.mba.proxylight.Socket access$900(com.mba.proxylight.RequestProcessor p1)
    {
        return p1.currentOutSocket;
    }

    static synthetic com.mba.proxylight.Socket access$902(com.mba.proxylight.RequestProcessor p0, com.mba.proxylight.Socket p1)
    {
        p0.currentOutSocket = p1;
        return p1;
    }

    private void closeAll()
    {
        if (this.inSocket != null) {
            try {
                this.inSocket.socket.close();
            } catch (Exception v0_0) {
                this.error(0, v0_0);
            }
            this.inSocket = 0;
        }
        java.nio.channels.Selector v2_5 = this.outSockets.values().iterator();
        while (v2_5.hasNext()) {
            try {
                ((com.mba.proxylight.Socket) v2_5.next()).socket.close();
            } catch (Exception v0_2) {
                this.error(0, v0_2);
            }
        }
        this.outSockets.clear();
        this.currentOutSocket = 0;
        if (this.selector != null) {
            try {
                this.selector.wakeup();
            } catch (Exception v0_1) {
                this.error(0, v0_1);
            }
            this.selector = 0;
        }
        return;
    }

    private void closeOutSocket(com.mba.proxylight.Socket p5)
    {
        try {
            java.nio.channels.SocketChannel v2_2 = this.outSockets.entrySet().iterator();
        } catch (Exception v0) {
            this.error("", v0);
            return;
        }
        while (v2_2.hasNext()) {
            java.util.Map$Entry v1_1 = ((java.util.Map$Entry) v2_2.next());
            if (v1_1.getValue() == p5) {
                this.outSockets.remove(v1_1.getKey());
                this.debug(new StringBuilder().append("Fermeture de la socket vers ").append(((String) v1_1.getKey())).toString());
                break;
            }
        }
        if (!p5.socket.isOpen()) {
            return;
        } else {
            p5.socket.close();
            return;
        }
    }

    private boolean filterRequest(com.mba.proxylight.Request p5)
    {
        int v3_2;
        java.util.List v1 = this.getRequestFilters();
        if (v1.size() <= 0) {
            v3_2 = 0;
        } else {
            int v2 = 0;
            while (v2 < v1.size()) {
                if (!((com.mba.proxylight.RequestFilter) v1.get(v2)).filter(p5)) {
                    v2++;
                } else {
                    v3_2 = 1;
                }
            }
        }
        return v3_2;
    }

    private int read(com.mba.proxylight.Socket p3, java.nio.ByteBuffer p4, long p5)
    {
        int v0 = p3.socket.read(p4);
        if (v0 > 0) {
            p3.lastWrite = p5;
        }
        return v0;
    }

    private String readNext(java.nio.ByteBuffer p9)
    {
        int v0 = 0;
        while (p9.remaining() > 0) {
            byte v1 = p9.get();
            if ((v1 != -1) && (v1 != 10)) {
                if (v1 != 13) {
                    if (this.read_offset == this.read_buf.length) {
                        char[] v3 = this.read_buf;
                        char[] v4_7 = new char[(v3.length * 2)];
                        this.read_buf = v4_7;
                        System.arraycopy(v3, 0, this.read_buf, 0, this.read_offset);
                    }
                    char[] v4_9 = this.read_buf;
                    int v5_3 = this.read_offset;
                    this.read_offset = (v5_3 + 1);
                    v4_9[v5_3] = ((char) v1);
                }
            } else {
                v0 = 1;
                break;
            }
        }
        String v2;
        if (v0 != 0) {
            v2 = String.copyValueOf(this.read_buf, 0, this.read_offset);
            this.read_offset = 0;
        } else {
            v2 = 0;
        }
        return v2;
    }

    private boolean transfer(com.mba.proxylight.Socket p3, com.mba.proxylight.Socket p4, long p5)
    {
        int v1_5;
        this.readBuffer.clear();
        int v0 = this.read(p3, this.readBuffer, p5);
        if (v0 != -1) {
            if (v0 > 0) {
                this.readBuffer.flip();
                this.write(p4, this.readBuffer, p5);
            }
            v1_5 = 1;
        } else {
            v1_5 = 0;
        }
        return v1_5;
    }

    private void write(com.mba.proxylight.Socket p2, java.nio.ByteBuffer p3, long p4)
    {
        p2.socket.write(p3);
        p2.lastWrite = p4;
        return;
    }

    public abstract void debug();

    public abstract void error();

    public abstract String getRemoteProxyHost();

    public abstract int getRemoteProxyPort();

    public abstract java.util.List getRequestFilters();

    public boolean isAlive()
    {
        return this.alive;
    }

    public void process(java.nio.channels.SelectionKey p6)
    {
        try {
            java.nio.channels.ServerSocketChannel v0_1 = ((java.nio.channels.ServerSocketChannel) p6.channel());
            this.inSocket = new com.mba.proxylight.Socket();
            this.inSocket.socket = v0_1.accept();
            this.inSocket.socket.configureBlocking(0);
            this.selector = java.nio.channels.spi.SelectorProvider.provider().openSelector();
            this.inSocket.socket.register(this.selector, 1, this.inSocket);
            this.notify();
            return;
        } catch (Throwable v1_9) {
            throw v1_9;
        }
    }

    public abstract void recycle();

    public abstract java.net.InetAddress resolve();

    public void shutdown()
    {
        this.closeAll();
        this.shutdown = 1;
        try {
            this.notify();
            return;
        } catch (Throwable v0_1) {
            throw v0_1;
        }
    }
}
