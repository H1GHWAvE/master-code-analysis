package com.proxoid;
 class IProxoidControl$Stub$Proxy implements com.proxoid.IProxoidControl {
    private android.os.IBinder mRemote;

    IProxoidControl$Stub$Proxy(android.os.IBinder p1)
    {
        this.mRemote = p1;
        return;
    }

    public android.os.IBinder asBinder()
    {
        return this.mRemote;
    }

    public String getInterfaceDescriptor()
    {
        return "com.proxoid.IProxoidControl";
    }

    public boolean update()
    {
        Throwable v2 = 1;
        android.os.Parcel v0 = android.os.Parcel.obtain();
        android.os.Parcel v1 = android.os.Parcel.obtain();
        try {
            v0.writeInterfaceToken("com.proxoid.IProxoidControl");
            this.mRemote.transact(1, v0, v1, 0);
            v1.readException();
        } catch (Throwable v3_1) {
            v1.recycle();
            v0.recycle();
            throw v3_1;
        }
        if (v1.readInt() == 0) {
            v2 = 0;
        }
        v1.recycle();
        v0.recycle();
        return v2;
    }
}
