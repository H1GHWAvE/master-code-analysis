package com.proxoid;
public class Proxoid implements android.content.ServiceConnection {
    protected static final String KEY_ONOFF = "onoff";
    protected static final String KEY_PORT = "port";
    protected static final String KEY_PREFS = "proxoidv6";
    protected static final String KEY_USERAGENT = "useragent";
    private static final String TAG = "proxoid";
    protected static final String USERAGENT_ASIS = "asis";
    protected static final String USERAGENT_RANDOM = "random";
    protected static final String USERAGENT_REMOVE = "remove";
    protected static final String USERAGENT_REPLACE = "replace";
    public android.content.Context mContext;
    private com.proxoid.IProxoidControl proxoidControl;

    public Proxoid(android.content.Context p6)
    {
        this.proxoidControl = 0;
        this.mContext = 0;
        this.mContext = p6;
        p6.bindService(new android.content.Intent(p6, com.proxoid.ProxoidService), this, 1);
        this.getSharedPreferences().edit().putBoolean("onoff", 1).commit();
        return;
    }

    private android.content.SharedPreferences getSharedPreferences()
    {
        return this.mContext.getSharedPreferences("proxoidv6", 4);
    }

    public void onServiceConnected(android.content.ComponentName p3, android.os.IBinder p4)
    {
        this.proxoidControl = ((com.proxoid.IProxoidControl) p4);
        if (this.proxoidControl != null) {
            try {
                this.proxoidControl.update();
            } catch (android.os.RemoteException v0) {
                v0.printStackTrace();
            }
        }
        return;
    }

    public void onServiceDisconnected(android.content.ComponentName p2)
    {
        this.proxoidControl = 0;
        return;
    }
}
