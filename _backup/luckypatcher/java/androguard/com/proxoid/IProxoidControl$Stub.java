package com.proxoid;
public abstract class IProxoidControl$Stub extends android.os.Binder implements com.proxoid.IProxoidControl {
    private static final String DESCRIPTOR = "com.proxoid.IProxoidControl";
    static final int TRANSACTION_update = 1;

    public IProxoidControl$Stub()
    {
        this.attachInterface(this, "com.proxoid.IProxoidControl");
        return;
    }

    public static com.proxoid.IProxoidControl asInterface(android.os.IBinder p2)
    {
        com.proxoid.IProxoidControl v0_2;
        if (p2 != null) {
            com.proxoid.IProxoidControl v0_0 = p2.queryLocalInterface("com.proxoid.IProxoidControl");
            if ((v0_0 == null) || (!(v0_0 instanceof com.proxoid.IProxoidControl))) {
                v0_2 = new com.proxoid.IProxoidControl$Stub$Proxy(p2);
            } else {
                v0_2 = ((com.proxoid.IProxoidControl) v0_0);
            }
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    public android.os.IBinder asBinder()
    {
        return this;
    }

    public boolean onTransact(int p4, android.os.Parcel p5, android.os.Parcel p6, int p7)
    {
        boolean v2 = 1;
        switch (p4) {
            case 1:
                int v1_2;
                p5.enforceInterface("com.proxoid.IProxoidControl");
                boolean v0 = this.update();
                p6.writeNoException();
                if (!v0) {
                    v1_2 = 0;
                } else {
                    v1_2 = 1;
                }
                p6.writeInt(v1_2);
                break;
            case 1598968902:
                p6.writeString("com.proxoid.IProxoidControl");
                break;
            default:
                v2 = super.onTransact(p4, p5, p6, p7);
        }
        return v2;
    }
}
