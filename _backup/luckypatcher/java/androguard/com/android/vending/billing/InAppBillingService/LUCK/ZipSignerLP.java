package com.android.vending.billing.InAppBillingService.LUCK;
public class ZipSignerLP {
    private static final String CERT_RSA_NAME = "META-INF/CERT.RSA";
    private static final String CERT_SF_NAME = "META-INF/CERT.SF";
    public static final String KEY_NONE = "none";
    public static final String KEY_TESTKEY = "testkey";
    public static final String MODE_AUTO = "auto";
    public static final String MODE_AUTO_NONE = "auto-none";
    public static final String MODE_AUTO_TESTKEY = "auto-testkey";
    public static final String[] SUPPORTED_KEY_MODES;
    static kellinwood.logging.LoggerInterface log;
    private static java.util.regex.Pattern stripPattern;
    java.util.Map autoKeyDetect;
    com.android.vending.billing.InAppBillingService.LUCK.ZipSignerLP$AutoKeyObservable autoKeyObservable;
    private boolean canceled;
    kellinwood.security.zipsigner.KeySet keySet;
    String keymode;
    java.util.Map loadedKeys;
    private kellinwood.security.zipsigner.ProgressHelper progressHelper;

    static ZipSignerLP()
    {
        com.android.vending.billing.InAppBillingService.LUCK.ZipSignerLP.log = 0;
        com.android.vending.billing.InAppBillingService.LUCK.ZipSignerLP.stripPattern = java.util.regex.Pattern.compile("^META-INF/(.*)[.](SF|RSA|DSA)$");
        String[] v0_4 = new String[8];
        v0_4[0] = "auto-testkey";
        v0_4[1] = "auto";
        v0_4[2] = "auto-none";
        v0_4[3] = "media";
        v0_4[4] = "platform";
        v0_4[5] = "shared";
        v0_4[6] = "testkey";
        v0_4[7] = "none";
        com.android.vending.billing.InAppBillingService.LUCK.ZipSignerLP.SUPPORTED_KEY_MODES = v0_4;
        return;
    }

    public ZipSignerLP()
    {
        this.canceled = 0;
        this.progressHelper = new kellinwood.security.zipsigner.ProgressHelper();
        this.loadedKeys = new java.util.HashMap();
        this.keySet = 0;
        this.keymode = "testkey";
        this.autoKeyDetect = new java.util.HashMap();
        this.autoKeyObservable = new com.android.vending.billing.InAppBillingService.LUCK.ZipSignerLP$AutoKeyObservable();
        this.autoKeyDetect.put("aa9852bc5a53272ac8031d49b65e4b0e", "media");
        this.autoKeyDetect.put("e60418c4b638f20d0721e115674ca11f", "platform");
        this.autoKeyDetect.put("3e24e49741b60c215c010dc6048fca7d", "shared");
        this.autoKeyDetect.put("dab2cead827ef5313f28e22b6fa8479f", "testkey");
        return;
    }

    private java.util.jar.Manifest addDigestsToManifest(java.util.Map p25, java.util.ArrayList p26)
    {
        java.util.jar.Manifest v12_0 = 0;
        kellinwood.zipio.ZioEntry v14_1 = ((kellinwood.zipio.ZioEntry) p25.get("META-INF/MANIFEST.MF"));
        if (v14_1 != null) {
            v12_0 = new java.util.jar.Manifest();
            v12_0.read(v14_1.getInputStream());
        }
        java.util.jar.Manifest v18_1 = new java.util.jar.Manifest();
        java.util.jar.Attributes v13 = v18_1.getMainAttributes();
        if (v12_0 == null) {
            v13.putValue("Manifest-Version", "1.0");
            v13.putValue("Created-By", "1.0 (Android SignApk)");
        } else {
            v13.putAll(v12_0.getMainAttributes());
        }
        java.security.MessageDigest v15 = java.security.MessageDigest.getInstance("SHA1");
        byte[] v3 = new byte[512];
        java.util.TreeMap v4_1 = new java.util.TreeMap();
        v4_1.putAll(p25);
        boolean v7 = com.android.vending.billing.InAppBillingService.LUCK.ZipSignerLP.getLogger().isDebugEnabled();
        if (v7) {
            com.android.vending.billing.InAppBillingService.LUCK.ZipSignerLP.getLogger().debug("Manifest entries:");
        }
        java.util.Iterator v19_10 = v4_1.values().iterator();
        while (v19_10.hasNext()) {
            kellinwood.zipio.ZioEntry v9_1 = ((kellinwood.zipio.ZioEntry) v19_10.next());
            if (this.canceled) {
                break;
            }
            String v16 = v9_1.getName();
            if (v7) {
                com.android.vending.billing.InAppBillingService.LUCK.ZipSignerLP.getLogger().debug(v16);
            }
            if ((!v9_1.isDirectory()) && ((!v16.equals("META-INF/MANIFEST.MF")) && ((!v16.equals("META-INF/CERT.SF")) && ((!v16.equals("META-INF/CERT.RSA")) && ((com.android.vending.billing.InAppBillingService.LUCK.ZipSignerLP.stripPattern == null) || (!com.android.vending.billing.InAppBillingService.LUCK.ZipSignerLP.stripPattern.matcher(v16).matches())))))) {
                this.progressHelper.progress(0, "Generating manifest");
                java.io.FileInputStream v5 = v9_1.getInputStream();
                java.util.Map v20_18 = p26.iterator();
                while (v20_18.hasNext()) {
                    com.android.vending.billing.InAppBillingService.LUCK.AddFilesItem v10_1 = ((com.android.vending.billing.InAppBillingService.LUCK.AddFilesItem) v20_18.next());
                    if (v16.equals(v10_1.fileName.replace(v10_1.basePath, ""))) {
                        try {
                            java.io.FileInputStream v6_1 = new java.io.FileInputStream(v10_1.fileName);
                            try {
                                System.out.println(new StringBuilder().append("LuckyPatcher (signer): Additional files to manifest added! ").append(v10_1).toString());
                                v5 = v6_1;
                            } catch (Exception v8) {
                                v5 = v6_1;
                                System.out.println(v8);
                            }
                        } catch (Exception v8) {
                        }
                    }
                }
                while(true) {
                    int v17 = v5.read(v3);
                    if (v17 <= 0) {
                        break;
                    }
                    v15.update(v3, 0, v17);
                }
                v5.close();
                java.util.jar.Attributes v2_0 = 0;
                if (v12_0 != null) {
                    java.util.jar.Attributes v11 = v12_0.getAttributes(v16);
                    if (v11 != null) {
                        v2_0 = new java.util.jar.Attributes(v11);
                    }
                }
                if (v2_0 == null) {
                    v2_0 = new java.util.jar.Attributes();
                }
                v2_0.putValue("SHA1-Digest", kellinwood.security.zipsigner.Base64.encode(v15.digest()));
                v18_1.getEntries().put(v16, v2_0);
            }
        }
        return v18_1;
    }

    private void copyFiles(java.util.Map p20, kellinwood.zipio.ZipOutput p21, java.util.ArrayList p22)
    {
        java.util.Iterator v14_1 = p20.values().iterator();
        while (v14_1.hasNext()) {
            kellinwood.zipio.ZioEntry v9_1 = ((kellinwood.zipio.ZioEntry) v14_1.next());
            String v11 = v9_1.getName();
            int v10 = 0;
            java.util.Iterator v15_1 = p22.iterator();
            while (v15_1.hasNext()) {
                com.android.vending.billing.InAppBillingService.LUCK.AddFilesItem v7_1 = ((com.android.vending.billing.InAppBillingService.LUCK.AddFilesItem) v15_1.next());
                if (v11.equals(v7_1.fileName.replace(v7_1.basePath, ""))) {
                    try {
                        java.io.File v8_1 = new java.io.File(v7_1.fileName);
                        byte[] v3 = new byte[8192];
                        java.io.FileInputStream v5_1 = new java.io.FileInputStream(v7_1.fileName);
                        kellinwood.zipio.ZioEntry v2_1 = new kellinwood.zipio.ZioEntry(v11);
                        v2_1.setCompression(v9_1.getCompression());
                        v2_1.setTime(v9_1.getTime());
                        java.io.OutputStream v13 = v2_1.getOutputStream();
                        java.util.zip.CRC32 v4_1 = new java.util.zip.CRC32();
                        v4_1.reset();
                    } catch (Exception v6) {
                        System.out.println(v6);
                    }
                    while(true) {
                        int v12 = v5_1.read(v3);
                        if (v12 <= 0) {
                            break;
                        }
                        v13.write(v3, 0, v12);
                        v4_1.update(v3, 0, v12);
                    }
                    v13.flush();
                    v5_1.close();
                    p21.write(v2_1);
                    v10 = 1;
                    v8_1.delete();
                    System.out.println(new StringBuilder().append("LuckyPatcher (signer): Additional files added! ").append(v7_1).toString());
                }
            }
            if (v10 == 0) {
                p21.write(v9_1);
            }
        }
        return;
    }

    private void copyFiles(java.util.jar.Manifest p24, java.util.Map p25, java.util.jar.JarOutputStream p26, long p27, java.util.ArrayList p29)
    {
        java.util.ArrayList v14_1 = new java.util.ArrayList(p24.getEntries().keySet());
        java.util.Collections.sort(v14_1);
        java.util.Iterator v19 = v14_1.iterator();
        while (v19.hasNext()) {
            String v13_1 = ((String) v19.next());
            kellinwood.zipio.ZioEntry v10_1 = ((kellinwood.zipio.ZioEntry) p25.get(v13_1));
            try {
                java.util.jar.JarEntry v16;
                if (((kellinwood.zipio.ZioEntry) p25.get(v13_1)).getCompression() != 0) {
                    java.util.jar.JarEntry v17_0 = new java.util.jar.JarEntry;
                    v17_0(v13_1);
                    v17_0.setTime(((kellinwood.zipio.ZioEntry) p25.get(v13_1)).getTime());
                    v17_0.setMethod(v10_1.getCompression());
                    v16 = v17_0;
                    int v11 = 0;
                    int v18_22 = p29.iterator();
                    while (v18_22.hasNext()) {
                        com.android.vending.billing.InAppBillingService.LUCK.AddFilesItem v8_1 = ((com.android.vending.billing.InAppBillingService.LUCK.AddFilesItem) v18_22.next());
                        if (v13_1.equals(v8_1.fileName.replace(v8_1.basePath, ""))) {
                            try {
                                java.io.File v9_1 = new java.io.File(v8_1.fileName);
                                byte[] v3_1 = new byte[8192];
                                java.io.InputStream v5_2 = new java.io.FileInputStream(v8_1.fileName);
                                p26.putNextEntry(v16);
                            } catch (Exception v6_0) {
                                System.out.println(v6_0);
                            }
                            while(true) {
                                int v15_1 = v5_2.read(v3_1);
                                if (v15_1 <= 0) {
                                    break;
                                }
                                p26.write(v3_1, 0, v15_1);
                            }
                            p26.flush();
                            v5_2.close();
                            v11 = 1;
                            v9_1.delete();
                            System.out.println(new StringBuilder().append("LuckyPatcher (signer): Additional files added! ").append(v8_1).toString());
                        }
                    }
                    if (v11 == 0) {
                        p26.putNextEntry(v16);
                        java.io.InputStream v5_0 = v10_1.getInputStream();
                        byte[] v3_0 = new byte[8192];
                        while(true) {
                            int v15_0 = v5_0.read(v3_0);
                            if (v15_0 <= 0) {
                                break;
                            }
                            p26.write(v3_0, 0, v15_0);
                        }
                        p26.flush();
                    }
                } else {
                    v17_0 = new java.util.jar.JarEntry(((kellinwood.zipio.ZioEntry) p25.get(v13_1)).getName());
                    v17_0.setMethod(0);
                    int v12 = 0;
                    java.io.PrintStream v20_1 = p29.iterator();
                    while (v20_1.hasNext()) {
                        com.android.vending.billing.InAppBillingService.LUCK.AddFilesItem v8_3 = ((com.android.vending.billing.InAppBillingService.LUCK.AddFilesItem) v20_1.next());
                        if (v13_1.equals(v8_3.fileName.replace(v8_3.basePath, ""))) {
                            try {
                                java.io.File v9_3 = new java.io.File(v8_3.fileName);
                                byte[] v3_2 = new byte[((int) v9_3.length())];
                                java.io.InputStream v5_4 = new java.io.FileInputStream(v8_3.fileName);
                                v5_4.read(v3_2);
                                v5_4.close();
                                v17_0.setCompressedSize(v9_3.length());
                                v17_0.setSize(v9_3.length());
                                java.util.zip.CRC32 v4_3 = new java.util.zip.CRC32();
                                v4_3.update(v3_2);
                                v17_0.setCrc(v4_3.getValue());
                                v17_0.setTime(((kellinwood.zipio.ZioEntry) p25.get(v13_1)).getTime());
                                v12 = 1;
                            } catch (Exception v6_2) {
                                System.out.println(v6_2);
                            }
                        }
                    }
                    if (v12 == 0) {
                        v17_0.setCompressedSize(((long) ((kellinwood.zipio.ZioEntry) p25.get(v13_1)).getSize()));
                        v17_0.setSize(((long) ((kellinwood.zipio.ZioEntry) p25.get(v13_1)).getSize()));
                        java.util.zip.CRC32 v4_1 = new java.util.zip.CRC32();
                        v4_1.update(v10_1.getData());
                        v17_0.setCrc(v4_1.getValue());
                        v17_0.setTime(((kellinwood.zipio.ZioEntry) p25.get(v13_1)).getTime());
                    }
                    v16 = v17_0;
                }
                System.out.println(Exception v6_1);
                v6_1.printStackTrace();
            } catch (Exception v6_1) {
            }
        }
        return;
    }

    private java.security.spec.KeySpec decryptPrivateKey(byte[] p9, String p10)
    {
        try {
            javax.crypto.EncryptedPrivateKeyInfo v1_1 = new javax.crypto.EncryptedPrivateKeyInfo(p9);
            javax.crypto.SecretKey v3 = javax.crypto.SecretKeyFactory.getInstance(v1_1.getAlgName()).generateSecret(new javax.crypto.spec.PBEKeySpec(p10.toCharArray()));
            javax.crypto.Cipher v0 = javax.crypto.Cipher.getInstance(v1_1.getAlgName());
            v0.init(2, v3, v1_1.getAlgParameters());
            try {
                kellinwood.logging.LoggerInterface v6_5 = v1_1.getKeySpec(v0);
            } catch (java.security.spec.InvalidKeySpecException v2) {
                com.android.vending.billing.InAppBillingService.LUCK.ZipSignerLP.getLogger().error("signapk: Password for private key may be bad.");
                throw v2;
            }
            return v6_5;
        } catch (java.security.spec.InvalidKeySpecException v2) {
            v6_5 = 0;
            return v6_5;
        }
    }

    private void generateSignatureFile(java.util.jar.Manifest p11, java.io.OutputStream p12)
    {
        p12.write("Signature-Version: 1.0\r\n".getBytes());
        p12.write("Created-By: 1.0 (Android SignApk)\r\n".getBytes());
        java.security.MessageDigest v3 = java.security.MessageDigest.getInstance("SHA1");
        java.io.PrintStream v5_1 = new java.io.PrintStream(new java.security.DigestOutputStream(new java.io.ByteArrayOutputStream(), v3), 1, "UTF-8");
        p11.write(v5_1);
        v5_1.flush();
        p12.write(new StringBuilder().append("SHA1-Digest-Manifest: ").append(kellinwood.security.zipsigner.Base64.encode(v3.digest())).append("\r\n\r\n").toString().getBytes());
        java.util.Iterator v7_7 = p11.getEntries().entrySet().iterator();
        while (v7_7.hasNext()) {
            java.util.Map$Entry v2_1 = ((java.util.Map$Entry) v7_7.next());
            if (this.canceled) {
                break;
            }
            this.progressHelper.progress(0, "Generating signature file");
            String v4 = new StringBuilder().append("Name: ").append(((String) v2_1.getKey())).append("\r\n").toString();
            v5_1.print(v4);
            byte[] v6_27 = ((java.util.jar.Attributes) v2_1.getValue()).entrySet().iterator();
            while (v6_27.hasNext()) {
                java.util.Map$Entry v0_1 = ((java.util.Map$Entry) v6_27.next());
                v5_1.print(new StringBuilder().append(v0_1.getKey()).append(": ").append(v0_1.getValue()).append("\r\n").toString());
            }
            v5_1.print("\r\n");
            v5_1.flush();
            p12.write(v4.getBytes());
            p12.write(new StringBuilder().append("SHA1-Digest: ").append(kellinwood.security.zipsigner.Base64.encode(v3.digest())).append("\r\n\r\n").toString().getBytes());
        }
        return;
    }

    public static kellinwood.logging.LoggerInterface getLogger()
    {
        if (com.android.vending.billing.InAppBillingService.LUCK.ZipSignerLP.log == null) {
            com.android.vending.billing.InAppBillingService.LUCK.ZipSignerLP.log = kellinwood.logging.LoggerManager.getLogger(com.android.vending.billing.InAppBillingService.LUCK.ZipSignerLP.getName());
        }
        return com.android.vending.billing.InAppBillingService.LUCK.ZipSignerLP.log;
    }

    public static String[] getSupportedKeyModes()
    {
        return com.android.vending.billing.InAppBillingService.LUCK.ZipSignerLP.SUPPORTED_KEY_MODES;
    }

    private void writeSignatureBlock(byte[] p8, byte[] p9, java.security.cert.X509Certificate p10, java.io.OutputStream p11)
    {
        if (p8 == null) {
            try {
                Class v0 = Class.forName("kellinwood.sigblock.SignatureBlockWriter");
                Object[] v4_1 = new Class[3];
                Class v6_1 = new byte[1];
                v4_1[0] = v6_1.getClass();
                v4_1[1] = java.security.cert.X509Certificate;
                v4_1[2] = java.io.OutputStream;
                reflect.Method v1 = v0.getMethod("writeSignatureBlock", v4_1);
            } catch (Exception v2) {
                com.android.vending.billing.InAppBillingService.LUCK.ZipSignerLP.getLogger().error(v2.getMessage(), v2);
                throw new IllegalStateException(new StringBuilder().append("Failed to invoke writeSignatureBlock(): ").append(v2.getClass().getName()).append(": ").append(v2.getMessage()).toString());
            }
            if (v1 != null) {
                Object[] v4_3 = new Object[3];
                v4_3[0] = p9;
                v4_3[1] = p10;
                v4_3[2] = p11;
                v1.invoke(0, v4_3);
            } else {
                throw new IllegalStateException("writeSignatureBlock() method not found.");
            }
        } else {
            p11.write(p8);
            p11.write(p9);
        }
        return;
    }

    public void addAutoKeyObserver(java.util.Observer p2)
    {
        this.autoKeyObservable.addObserver(p2);
        return;
    }

    public void addProgressListener(kellinwood.security.zipsigner.ProgressListener p2)
    {
        this.progressHelper.addProgressListener(p2);
        return;
    }

    protected String autoDetectKey(String p19, java.util.Map p20)
    {
        boolean v3 = com.android.vending.billing.InAppBillingService.LUCK.ZipSignerLP.getLogger().isDebugEnabled();
        if (p19.startsWith("auto")) {
            String v7_0 = 0;
            String v12_0 = p20.entrySet().iterator();
            while (v12_0.hasNext()) {
                java.util.Map$Entry v4_1 = ((java.util.Map$Entry) v12_0.next());
                String v6_1 = ((String) v4_1.getKey());
                if ((v6_1.startsWith("META-INF/")) && (v6_1.endsWith(".RSA"))) {
                    java.security.MessageDigest v8 = java.security.MessageDigest.getInstance("MD5");
                    byte[] v5 = ((kellinwood.zipio.ZioEntry) v4_1.getValue()).getData();
                    if (v5.length < 1458) {
                        break;
                    }
                    v8.update(v5, 0, 1458);
                    byte[] v10 = v8.digest();
                    StringBuilder v2_1 = new StringBuilder();
                    String v13_2 = v10.length;
                    kellinwood.logging.LoggerInterface v11_14 = 0;
                    while (v11_14 < v13_2) {
                        int v15_4 = new Object[1];
                        v15_4[0] = Byte.valueOf(v10[v11_14]);
                        v2_1.append(String.format("%02x", v15_4));
                        v11_14++;
                    }
                    String v9 = v2_1.toString();
                    v7_0 = ((String) this.autoKeyDetect.get(v9));
                    if (v3) {
                        if (v7_0 == null) {
                            kellinwood.logging.LoggerInterface v11_16 = com.android.vending.billing.InAppBillingService.LUCK.ZipSignerLP.getLogger();
                            Object[] v14_1 = new Object[1];
                            v14_1[0] = v9;
                            v11_16.debug(String.format("Auto key determination failed for md5=%s", v14_1));
                        } else {
                            kellinwood.logging.LoggerInterface v11_17 = com.android.vending.billing.InAppBillingService.LUCK.ZipSignerLP.getLogger();
                            Object[] v14_3 = new Object[2];
                            v14_3[0] = v7_0;
                            v14_3[1] = v9;
                            v11_17.debug(String.format("Auto-determined key=%s using md5=%s", v14_3));
                        }
                    }
                    if (v7_0 != null) {
                        p19 = v7_0;
                        return p19;
                    }
                }
            }
            if (!p19.equals("auto-testkey")) {
                if (!p19.equals("auto-none")) {
                    p19 = 0;
                } else {
                    if (v3) {
                        com.android.vending.billing.InAppBillingService.LUCK.ZipSignerLP.getLogger().debug("Unable to determine key, returning: none");
                    }
                    p19 = "none";
                }
            } else {
                if (v3) {
                    com.android.vending.billing.InAppBillingService.LUCK.ZipSignerLP.getLogger().debug(new StringBuilder().append("Falling back to key=").append(v7_0).toString());
                }
                p19 = "testkey";
            }
        }
        return p19;
    }

    public void cancel()
    {
        this.canceled = 1;
        return;
    }

    public kellinwood.security.zipsigner.KeySet getKeySet()
    {
        return this.keySet;
    }

    public String getKeymode()
    {
        return this.keymode;
    }

    public boolean isCanceled()
    {
        return this.canceled;
    }

    public void loadKeys(String p7)
    {
        this.keySet = ((kellinwood.security.zipsigner.KeySet) this.loadedKeys.get(p7));
        if (this.keySet == null) {
            this.keySet = new kellinwood.security.zipsigner.KeySet();
            this.keySet.setName(p7);
            this.loadedKeys.put(p7, this.keySet);
            if (!"none".equals(p7)) {
                this.progressHelper.progress(1, "Loading certificate and private key");
                this.keySet.setPrivateKey(this.readPrivateKey(this.getClass().getResource(new StringBuilder().append("/keys/").append(p7).append(".pk8").toString()), 0));
                this.keySet.setPublicKey(this.readPublicKey(this.getClass().getResource(new StringBuilder().append("/keys/").append(p7).append(".x509.pem").toString())));
                java.net.URL v2 = this.getClass().getResource(new StringBuilder().append("/keys/").append(p7).append(".sbt").toString());
                if (v2 != null) {
                    this.keySet.setSigBlockTemplate(this.readContentAsBytes(v2));
                }
            }
        }
        return;
    }

    public void loadProvider(String p4)
    {
        java.security.Security.insertProviderAt(((java.security.Provider) Class.forName(p4).newInstance()), 1);
        return;
    }

    public byte[] readContentAsBytes(java.io.InputStream p6)
    {
        java.io.ByteArrayOutputStream v0_1 = new java.io.ByteArrayOutputStream();
        byte[] v1 = new byte[2048];
        int v3 = p6.read(v1);
        while (v3 != -1) {
            v0_1.write(v1, 0, v3);
            v3 = p6.read(v1);
        }
        return v0_1.toByteArray();
    }

    public byte[] readContentAsBytes(java.net.URL p2)
    {
        return this.readContentAsBytes(p2.openStream());
    }

    public java.security.PrivateKey readPrivateKey(java.net.URL p6, String p7)
    {
        java.io.DataInputStream v2_1 = new java.io.DataInputStream(p6.openStream());
        try {
            byte[] v0 = this.readContentAsBytes(v2_1);
            java.security.spec.PKCS8EncodedKeySpec v3_0 = this.decryptPrivateKey(v0, p7);
        } catch (java.security.PrivateKey v4_6) {
            v2_1.close();
            throw v4_6;
        }
        if (v3_0 == null) {
            v3_0 = new java.security.spec.PKCS8EncodedKeySpec(v0);
        }
        try {
            java.security.PrivateKey v4_3 = java.security.KeyFactory.getInstance("RSA").generatePrivate(v3_0);
            v2_1.close();
        } catch (java.security.spec.InvalidKeySpecException v1) {
            v4_3 = java.security.KeyFactory.getInstance("DSA").generatePrivate(v3_0);
            v2_1.close();
        }
        return v4_3;
    }

    public java.security.cert.X509Certificate readPublicKey(java.net.URL p4)
    {
        java.io.InputStream v1 = p4.openStream();
        try {
            Throwable v2_2 = ((java.security.cert.X509Certificate) java.security.cert.CertificateFactory.getInstance("X.509").generateCertificate(v1));
            v1.close();
            return v2_2;
        } catch (Throwable v2_3) {
            v1.close();
            throw v2_3;
        }
    }

    public declared_synchronized void removeProgressListener(kellinwood.security.zipsigner.ProgressListener p2)
    {
        try {
            this.progressHelper.removeProgressListener(p2);
            return;
        } catch (Throwable v0_1) {
            throw v0_1;
        }
    }

    public void resetCanceled()
    {
        this.canceled = 0;
        return;
    }

    public void setKeymode(String p4)
    {
        if (com.android.vending.billing.InAppBillingService.LUCK.ZipSignerLP.getLogger().isDebugEnabled()) {
            com.android.vending.billing.InAppBillingService.LUCK.ZipSignerLP.getLogger().debug(new StringBuilder().append("setKeymode: ").append(p4).toString());
        }
        this.keymode = p4;
        if (!this.keymode.startsWith("auto")) {
            this.progressHelper.initProgress();
            this.loadKeys(this.keymode);
        } else {
            this.keySet = 0;
        }
        return;
    }

    public void setKeys(String p2, java.security.cert.X509Certificate p3, java.security.PrivateKey p4, byte[] p5)
    {
        this.keySet = new kellinwood.security.zipsigner.KeySet(p2, p3, p4, p5);
        return;
    }

    public void signZip(String p7, String p8, java.util.ArrayList p9)
    {
        if (!new java.io.File(p7).getCanonicalFile().equals(new java.io.File(p8).getCanonicalFile())) {
            this.progressHelper.initProgress();
            this.progressHelper.progress(1, "Parsing the input\'s central directory");
            this.signZip(kellinwood.zipio.ZipInput.read(p7).getEntries(), new java.io.FileOutputStream(p8), p8, p9);
            return;
        } else {
            throw new IllegalArgumentException("Input and output files are the same.  Specify a different name for the output.");
        }
    }

    public void signZip(java.net.URL p12, String p13, String p14, String p15, String p16, String p17, String p18, java.util.ArrayList p19)
    {
        if (p13 == null) {
            try {
                p13 = java.security.KeyStore.getDefaultType();
            } catch (Throwable v9_0) {
                if (0 != 0) {
                    0.close();
                }
                throw v9_0;
            }
        }
        java.security.KeyStore v5 = java.security.KeyStore.getInstance(p13);
        java.io.InputStream v6_1 = p12.openStream();
        v5.load(v6_1, p14.toCharArray());
        this.setKeys("custom", ((java.security.cert.X509Certificate) v5.getCertificate(p15)), ((java.security.PrivateKey) v5.getKey(p15, p16.toCharArray())), 0);
        this.signZip(p17, p18, p19);
        if (v6_1 != null) {
            v6_1.close();
        }
        return;
    }

    public void signZip(java.util.Map p34, java.io.OutputStream p35, String p36, java.util.ArrayList p37)
    {
        boolean v12 = com.android.vending.billing.InAppBillingService.LUCK.ZipSignerLP.getLogger().isDebugEnabled();
        this.progressHelper.initProgress();
        if (this.keySet == null) {
            if (this.keymode.startsWith("auto")) {
                String v15 = this.autoDetectKey(this.keymode, p34);
                if (v15 != null) {
                    this.autoKeyObservable.notifyObservers(v15);
                    this.loadKeys(v15);
                } else {
                    throw new kellinwood.security.zipsigner.AutoKeyException(new StringBuilder().append("Unable to auto-select key for signing ").append(new java.io.File(p36).getName()).toString());
                }
            } else {
                throw new IllegalStateException("No keys configured for signing the file!");
            }
        }
        try {
            kellinwood.zipio.ZipOutput v28 = new kellinwood.zipio.ZipOutput;
            v28(p35);
            try {
                int v7_0 = new java.util.jar.JarOutputStream(new java.io.FileOutputStream(p36));
            } catch (kellinwood.logging.LoggerInterface v4_7) {
                v7_0 = 0;
                if (v7_0 != 0) {
                    if (!this.keymode.equals("none")) {
                        v7_0.close();
                    }
                }
                if (this.canceled) {
                    if (p36 != null) {
                        try {
                            new java.io.File(p36).delete();
                        } catch (Throwable v25_5) {
                            com.android.vending.billing.InAppBillingService.LUCK.ZipSignerLP.getLogger().warning(new StringBuilder().append(v25_5.getClass().getName()).append(":").append(v25_5.getMessage()).toString());
                        }
                    }
                }
                throw v4_7;
            }
            try {
                if (!"none".equals(this.keySet.getName())) {
                    int v20_0 = 0;
                    kellinwood.logging.LoggerInterface v4_13 = p34.values().iterator();
                    while (v4_13.hasNext()) {
                        kellinwood.zipio.ZioEntry v13_1 = ((kellinwood.zipio.ZioEntry) v4_13.next());
                        String v17 = v13_1.getName();
                        if ((!v13_1.isDirectory()) && ((!v17.equals("META-INF/MANIFEST.MF")) && ((!v17.equals("META-INF/CERT.SF")) && ((!v17.equals("META-INF/CERT.RSA")) && ((com.android.vending.billing.InAppBillingService.LUCK.ZipSignerLP.stripPattern == null) || (!com.android.vending.billing.InAppBillingService.LUCK.ZipSignerLP.stripPattern.matcher(v17).matches())))))) {
                            v20_0 += 3;
                        }
                    }
                    this.progressHelper.setProgressTotalItems((v20_0 + 1));
                    this.progressHelper.setProgressCurrentItem(0);
                    long v8 = (this.keySet.getPublicKey().getNotBefore().getTime() + 3600000);
                    java.util.jar.Manifest v5 = this.addDigestsToManifest(p34, p37);
                    if (!this.canceled) {
                        java.util.jar.JarEntry v14_1 = new java.util.jar.JarEntry("META-INF/MANIFEST.MF");
                        v14_1.setTime(v8);
                        v7_0.putNextEntry(v14_1);
                        v5.write(v7_0);
                        kellinwood.security.zipsigner.ZipSignature v23_1 = new kellinwood.security.zipsigner.ZipSignature();
                        v23_1.initSign(this.keySet.getPrivateKey());
                        java.io.ByteArrayOutputStream v18_1 = new java.io.ByteArrayOutputStream();
                        this.generateSignatureFile(v5, v18_1);
                        if (!this.canceled) {
                            byte[] v21 = v18_1.toByteArray();
                            if (v12) {
                                com.android.vending.billing.InAppBillingService.LUCK.ZipSignerLP.getLogger().debug(new StringBuilder().append("Signature File: \n").append(new String(v21)).append("\n").append(kellinwood.security.zipsigner.HexDumpEncoder.encode(v21)).toString());
                            }
                            java.util.jar.JarEntry v14_3 = new java.util.jar.JarEntry("META-INF/CERT.SF");
                            v14_3.setTime(v8);
                            v7_0.putNextEntry(v14_3);
                            v7_0.write(v21);
                            v23_1.update(v21);
                            byte[] v24 = v23_1.sign();
                            if (v12) {
                                java.security.MessageDigest v16 = java.security.MessageDigest.getInstance("SHA1");
                                v16.update(v21);
                                com.android.vending.billing.InAppBillingService.LUCK.ZipSignerLP.getLogger().debug(new StringBuilder().append("Sig File SHA1: \n").append(kellinwood.security.zipsigner.HexDumpEncoder.encode(v16.digest())).toString());
                                com.android.vending.billing.InAppBillingService.LUCK.ZipSignerLP.getLogger().debug(new StringBuilder().append("Signature: \n").append(kellinwood.security.zipsigner.HexDumpEncoder.encode(v24)).toString());
                                javax.crypto.Cipher v11 = javax.crypto.Cipher.getInstance("RSA/ECB/PKCS1Padding");
                                v11.init(2, this.keySet.getPublicKey());
                                com.android.vending.billing.InAppBillingService.LUCK.ZipSignerLP.getLogger().debug(new StringBuilder().append("Signature Decrypted: \n").append(kellinwood.security.zipsigner.HexDumpEncoder.encode(v11.doFinal(v24))).toString());
                            }
                            this.progressHelper.progress(0, "Generating signature block file");
                            java.util.jar.JarEntry v14_5 = new java.util.jar.JarEntry("META-INF/CERT.RSA");
                            v14_5.setTime(v8);
                            v7_0.putNextEntry(v14_5);
                            this.writeSignatureBlock(this.keySet.getSigBlockTemplate(), v24, this.keySet.getPublicKey(), v7_0);
                            if (!this.canceled) {
                                this.copyFiles(v5, p34, v7_0, v8, p37);
                                if (!this.canceled) {
                                    if ((v7_0 != 0) && (!this.keymode.equals("none"))) {
                                        v7_0.close();
                                    }
                                    if ((this.canceled) && (p36 != null)) {
                                        try {
                                            new java.io.File(p36).delete();
                                        } catch (Throwable v25_0) {
                                            com.android.vending.billing.InAppBillingService.LUCK.ZipSignerLP.getLogger().warning(new StringBuilder().append(v25_0.getClass().getName()).append(":").append(v25_0.getMessage()).toString());
                                        }
                                    }
                                } else {
                                    if ((v7_0 != 0) && (!this.keymode.equals("none"))) {
                                        v7_0.close();
                                    }
                                    if ((this.canceled) && (p36 != null)) {
                                        try {
                                            new java.io.File(p36).delete();
                                        } catch (Throwable v25_1) {
                                            com.android.vending.billing.InAppBillingService.LUCK.ZipSignerLP.getLogger().warning(new StringBuilder().append(v25_1.getClass().getName()).append(":").append(v25_1.getMessage()).toString());
                                        }
                                    }
                                }
                            } else {
                                if ((v7_0 != 0) && (!this.keymode.equals("none"))) {
                                    v7_0.close();
                                }
                                if ((this.canceled) && (p36 != null)) {
                                    try {
                                        new java.io.File(p36).delete();
                                    } catch (Throwable v25_2) {
                                        com.android.vending.billing.InAppBillingService.LUCK.ZipSignerLP.getLogger().warning(new StringBuilder().append(v25_2.getClass().getName()).append(":").append(v25_2.getMessage()).toString());
                                    }
                                }
                            }
                        } else {
                            if ((v7_0 != 0) && (!this.keymode.equals("none"))) {
                                v7_0.close();
                            }
                            if ((this.canceled) && (p36 != null)) {
                                try {
                                    new java.io.File(p36).delete();
                                } catch (Throwable v25_3) {
                                    com.android.vending.billing.InAppBillingService.LUCK.ZipSignerLP.getLogger().warning(new StringBuilder().append(v25_3.getClass().getName()).append(":").append(v25_3.getMessage()).toString());
                                }
                            }
                        }
                    } else {
                        if ((v7_0 != 0) && (!this.keymode.equals("none"))) {
                            v7_0.close();
                        }
                        if ((this.canceled) && (p36 != null)) {
                            try {
                                new java.io.File(p36).delete();
                            } catch (Throwable v25_4) {
                                com.android.vending.billing.InAppBillingService.LUCK.ZipSignerLP.getLogger().warning(new StringBuilder().append(v25_4.getClass().getName()).append(":").append(v25_4.getMessage()).toString());
                            }
                        }
                    }
                } else {
                    this.progressHelper.setProgressTotalItems(p34.size());
                    this.progressHelper.setProgressCurrentItem(0);
                    this.copyFiles(p34, v28, p37);
                    v28.close();
                    if ((v7_0 != 0) && (!this.keymode.equals("none"))) {
                        v7_0.close();
                    }
                    if ((this.canceled) && (p36 != null)) {
                        try {
                            new java.io.File(p36).delete();
                        } catch (Throwable v25_6) {
                            com.android.vending.billing.InAppBillingService.LUCK.ZipSignerLP.getLogger().warning(new StringBuilder().append(v25_6.getClass().getName()).append(":").append(v25_6.getMessage()).toString());
                        }
                    }
                }
            } catch (kellinwood.logging.LoggerInterface v4_7) {
            }
            return;
        } catch (kellinwood.logging.LoggerInterface v4_7) {
            v7_0 = 0;
        }
    }

    public void signZip(java.util.Map p2, String p3, java.util.ArrayList p4)
    {
        this.progressHelper.initProgress();
        this.signZip(p2, new java.io.FileOutputStream(p3), p3, p4);
        return;
    }
}
