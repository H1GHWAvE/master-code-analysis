package com.android.vending.billing.InAppBillingService.LUCK;
public class PatchService extends android.app.Service {
    public static int notifyIndex;
    public android.content.Context context;
    final android.os.Handler handler;
    private Thread t;

    static PatchService()
    {
        com.android.vending.billing.InAppBillingService.LUCK.PatchService.notifyIndex = 50;
        return;
    }

    public PatchService()
    {
        this.context = 0;
        this.handler = new android.os.Handler();
        return;
    }

    static synthetic void access$000(com.android.vending.billing.InAppBillingService.LUCK.PatchService p0, int p1, String p2, String p3, String p4)
    {
        p0.showNotify(p1, p2, p3, p4);
        return;
    }

    private void showNotify(int p15, String p16, String p17, String p18)
    {
        if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getBoolean("hide_notify", 0)) {
            long v9 = System.currentTimeMillis();
            android.content.Context v4 = this.getApplicationContext();
            android.app.PendingIntent v1 = android.app.PendingIntent.getActivity(this, 0, new android.content.Intent(this, com.android.vending.billing.InAppBillingService.LUCK.patchActivity), 0);
            android.app.NotificationManager v8_1 = ((android.app.NotificationManager) this.getSystemService("notification"));
            android.app.Notification v6_1 = new android.app.Notification(2130837552, p17, v9);
            v6_1.setLatestEventInfo(v4, p16, p18, v1);
            v8_1.notify(p15, v6_1);
        }
        return;
    }

    public void dexoptcopy()
    {
        java.io.File v1_1 = new java.io.File(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir).append("/").append("dexopt-wrapper").toString());
        if ((v1_1.exists()) && (v1_1.length() == com.chelpus.Utils.getRawLength(2131099652))) {
            com.chelpus.Utils.run_all(new StringBuilder().append("chmod 777 ").append(new java.io.File(new StringBuilder().append(this.getApplicationContext().getFilesDir()).append("/").append("dexopt-wrapper").toString())).toString());
            com.chelpus.Utils.run_all(new StringBuilder().append("chown 0.0 ").append(new java.io.File(new StringBuilder().append(this.getApplicationContext().getFilesDir()).append("/").append("dexopt-wrapper").toString())).toString());
            com.chelpus.Utils.run_all(new StringBuilder().append("chmod 0:0 ").append(new java.io.File(new StringBuilder().append(this.getApplicationContext().getFilesDir()).append("/").append("dexopt-wrapper").toString())).toString());
        } else {
            try {
                com.chelpus.Utils.getRawToFile(2131099652, new java.io.File(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir).append("/dexopt-wrapper").toString()));
                try {
                    com.chelpus.Utils.chmod(new java.io.File(new StringBuilder().append(this.getApplicationContext().getFilesDir()).append("/").append("dexopt-wrapper").toString()), 777);
                } catch (Exception v0) {
                    System.out.println(v0);
                    v0.printStackTrace();
                }
                com.chelpus.Utils.run_all(new StringBuilder().append("chmod 777 ").append(new java.io.File(new StringBuilder().append(this.getApplicationContext().getFilesDir()).append("/").append("dexopt-wrapper").toString())).toString());
                com.chelpus.Utils.run_all(new StringBuilder().append("chown 0.0 ").append(new java.io.File(new StringBuilder().append(this.getApplicationContext().getFilesDir()).append("/").append("dexopt-wrapper").toString())).toString());
                com.chelpus.Utils.run_all(new StringBuilder().append("chmod 0:0 ").append(new java.io.File(new StringBuilder().append(this.getApplicationContext().getFilesDir()).append("/").append("dexopt-wrapper").toString())).toString());
            } catch (String v2) {
            }
        }
        return;
    }

    public android.os.IBinder onBind(android.content.Intent p2)
    {
        return 0;
    }

    public void onCreate()
    {
        System.out.println("LuckyPatcher: Create service");
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchOnBoot = 1;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.init();
        this.context = this;
        return;
    }

    public void onDestroy()
    {
        super.onDestroy();
        System.out.println("Killing Service!!!!!!!!!!!!!!!!!!!!!!!");
        com.chelpus.Utils.exit();
        return;
    }

    public int onStartCommand(android.content.Intent p3, int p4, int p5)
    {
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchOnBoot = 1;
        this.t = new Thread(new com.android.vending.billing.InAppBillingService.LUCK.PatchService$1(this));
        System.out.println("LuckyPatcher: Start thread patch!");
        this.t.setPriority(10);
        this.t.start();
        return 2;
    }
}
