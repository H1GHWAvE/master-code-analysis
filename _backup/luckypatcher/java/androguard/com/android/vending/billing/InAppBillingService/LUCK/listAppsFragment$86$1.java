package com.android.vending.billing.InAppBillingService.LUCK;
 class listAppsFragment$86$1 extends android.widget.ArrayAdapter {
    final synthetic com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$86 this$1;

    listAppsFragment$86$1(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$86 p1, android.content.Context p2, int p3, java.util.List p4)
    {
        this.this$1 = p1;
        this(p2, p3, p4);
        return;
    }

    public android.view.View getView(int p12, android.view.View p13, android.view.ViewGroup p14)
    {
        android.view.View v5 = super.getView(p12, p13, p14);
        android.widget.TextView v4_1 = ((android.widget.TextView) v5.findViewById(2131558457));
        v4_1.setTextAppearance(this.getContext(), com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getSizeText());
        v4_1.setCompoundDrawablePadding(((int) ((1084227584 * com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDisplayMetrics().density) + 1056964608)));
        String v3_0 = new StringBuilder().append(((com.android.vending.billing.InAppBillingService.LUCK.Perm) this.getItem(p12)).Name.replace("chelpa.disabled.", "").replace("android.permission.", "").replace("com.android.vending.", "")).append("\n").toString();
        if (!((com.android.vending.billing.InAppBillingService.LUCK.Perm) this.getItem(p12)).Status) {
            v4_1.setText(com.chelpus.Utils.getColoredText(v3_0, "#ffff0000", "bold"));
        } else {
            v4_1.setText(com.chelpus.Utils.getColoredText(v3_0, "#ff00ff00", "bold"));
        }
        android.content.pm.PackageManager v2 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng();
        try {
            String v3_1 = v2.getPermissionInfo(((com.android.vending.billing.InAppBillingService.LUCK.Perm) this.getItem(p12)).Name.replace("chelpa.disabled.", ""), 0).loadDescription(v2).toString();
        } catch (NullPointerException v1) {
            v4_1.append(com.chelpus.Utils.getColoredText(com.chelpus.Utils.getText(2131165625), "#ff888888", "italic"));
            return v5;
        } catch (NullPointerException v1) {
            v4_1.append(com.chelpus.Utils.getColoredText(com.chelpus.Utils.getText(2131165625), "#ff888888", "italic"));
            return v5;
        }
        if (v3_1 != null) {
            v4_1.append(com.chelpus.Utils.getColoredText(v3_1, "#ff888888", "italic"));
            return v5;
        } else {
            v4_1.append(com.chelpus.Utils.getColoredText(com.chelpus.Utils.getText(2131165625), "#ff888888", "italic"));
            return v5;
        }
    }
}
