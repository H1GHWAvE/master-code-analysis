package com.android.vending.billing.InAppBillingService.LUCK.dialogs;
public class All_Dialogs {
    public static final int ADD_BOOT = 2;
    public static final int CREATE_APK = 0;
    public static final int CUSTOM_PATCH = 1;
    android.app.Dialog dialog;
    public int dialog_int;

    public All_Dialogs()
    {
        this.dialog_int = 255;
        this.dialog = 0;
        return;
    }

    public void dismiss()
    {
        if (this.dialog != null) {
            this.dialog.dismiss();
            this.dialog = 0;
        }
        return;
    }

    public boolean isVisible()
    {
        int v0_1;
        if (this.dialog == null) {
            v0_1 = 0;
        } else {
            v0_1 = this.dialog.isShowing();
        }
        return v0_1;
    }

    public android.app.Dialog onCreateDialog()
    {
        System.out.println("Create dialog");
        this.dialog_int = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.dialog_int;
        if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag == null) || (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext() == null)) {
            this.dismiss();
        }
        try {
            android.app.Dialog v32;
            System.out.println("All Dialog create.");
            switch (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.dialog_int) {
                case 3:
                    com.android.vending.billing.InAppBillingService.LUCK.AlertDlg v9_1 = new com.android.vending.billing.InAppBillingService.LUCK.AlertDlg(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext());
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.refresh_boot();
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adapter_boot = new com.android.vending.billing.InAppBillingService.LUCK.BootListItemAdapter(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext(), 2130968593, com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getInt("viewsize", 0), com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.boot_pat);
                    String v46_115 = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.All_Dialogs$byPkgName;
                    v46_115(this);
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adapter_boot.sorter = v46_115;
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.notifyDataSetChanged();
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adapter_boot.notifyDataSetChanged();
                    v9_1.setTitle(com.chelpus.Utils.getText(2131165407));
                    String v46_121 = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.All_Dialogs$38;
                    v46_121(this);
                    v9_1.setAdapter(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adapter_boot, v46_121);
                    System.out.println("asd2");
                    v32 = v9_1.create();
                    return v32;
                case 10:
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.changedPermissions = new java.util.ArrayList();
                    com.android.vending.billing.InAppBillingService.LUCK.AlertDlg v10_1 = new com.android.vending.billing.InAppBillingService.LUCK.AlertDlg(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext());
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.perm_adapt != null) {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.perm_adapt.setNotifyOnChange(1);
                        v10_1.setAdapterNotClose(1);
                        String v46_111 = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.All_Dialogs$11;
                        v46_111(this);
                        v10_1.setAdapter(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.perm_adapt, v46_111);
                        android.text.SpannableString v45_340 = com.chelpus.Utils.getText(2131165623);
                        String v46_112 = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.All_Dialogs$12;
                        v46_112(this);
                        v10_1.setPositiveButton(v45_340, v46_112);
                    }
                    v32 = v10_1.create();
                    return v32;
                case 13:
                    com.android.vending.billing.InAppBillingService.LUCK.AlertDlg v11_1 = new com.android.vending.billing.InAppBillingService.LUCK.AlertDlg(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext());
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patch_adapt != null) {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patch_adapt.setNotifyOnChange(1);
                        v11_1.setAdapterNotClose(1);
                        String v46_108 = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.All_Dialogs$13;
                        v46_108(this);
                        v11_1.setAdapter(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patch_adapt, v46_108);
                        android.text.SpannableString v45_330 = com.chelpus.Utils.getText(2131165592);
                        String v46_109 = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.All_Dialogs$14;
                        v46_109(this);
                        v11_1.setPositiveButton(v45_330, v46_109);
                    }
                    v32 = v11_1.create();
                    return v32;
                case 16:
                    com.android.vending.billing.InAppBillingService.LUCK.AlertDlg v12_1 = new com.android.vending.billing.InAppBillingService.LUCK.AlertDlg(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext());
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adapt != null) {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adapt.setNotifyOnChange(1);
                        String v46_106 = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.All_Dialogs$15;
                        v46_106(this);
                        v12_1.setAdapter(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adapt, v46_106);
                    }
                    v32 = v12_1.create();
                    return v32;
                case 17:
                    com.android.vending.billing.InAppBillingService.LUCK.AlertDlg v5_1 = new com.android.vending.billing.InAppBillingService.LUCK.AlertDlg(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext());
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patch_adapt != null) {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patch_adapt.setNotifyOnChange(1);
                        v5_1.setAdapterNotClose(1);
                        String v46_103 = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.All_Dialogs$19;
                        v46_103(this);
                        v5_1.setAdapter(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patch_adapt, v46_103);
                        android.text.SpannableString v45_317 = com.chelpus.Utils.getText(2131165592);
                        String v46_104 = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.All_Dialogs$20;
                        v46_104(this);
                        v5_1.setPositiveButton(v45_317, v46_104);
                    }
                    v32 = v5_1.create();
                    return v32;
                case 18:
                    com.android.vending.billing.InAppBillingService.LUCK.AlertDlg v4_1 = new com.android.vending.billing.InAppBillingService.LUCK.AlertDlg(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext());
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patch_adapt != null) {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patch_adapt.setNotifyOnChange(1);
                        v4_1.setAdapterNotClose(1);
                        String v46_100 = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.All_Dialogs$21;
                        v46_100(this);
                        v4_1.setAdapter(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patch_adapt, v46_100);
                        android.text.SpannableString v45_309 = com.chelpus.Utils.getText(2131165382);
                        String v46_101 = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.All_Dialogs$22;
                        v46_101(this);
                        v4_1.setPositiveButton(v45_309, v46_101);
                    }
                    v32 = v4_1.create();
                    return v32;
                case 19:
                    com.android.vending.billing.InAppBillingService.LUCK.AlertDlg v33 = new com.android.vending.billing.InAppBillingService.LUCK.AlertDlg;
                    v33(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext());
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patch_adapt != null) {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patch_adapt.setNotifyOnChange(1);
                        v33.setAdapterNotClose(1);
                        String v46_97 = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.All_Dialogs$27;
                        v46_97(this);
                        v33.setAdapter(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patch_adapt, v46_97);
                        android.text.SpannableString v45_301 = com.chelpus.Utils.getText(2131165382);
                        String v46_98 = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.All_Dialogs$28;
                        v46_98(this);
                        v33.setPositiveButton(v45_301, v46_98);
                    }
                    v32 = v33.create();
                    return v32;
                case 20:
                    com.android.vending.billing.InAppBillingService.LUCK.AlertDlg v34 = new com.android.vending.billing.InAppBillingService.LUCK.AlertDlg;
                    v34(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext());
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patch_adapt != null) {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patch_adapt.setNotifyOnChange(1);
                        v34.setAdapterNotClose(1);
                        String v46_94 = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.All_Dialogs$29;
                        v46_94(this);
                        v34.setAdapter(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patch_adapt, v46_94);
                        android.text.SpannableString v45_293 = com.chelpus.Utils.getText(2131165592);
                        String v46_95 = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.All_Dialogs$30;
                        v46_95(this);
                        v34.setPositiveButton(v45_293, v46_95);
                    }
                    v32 = v34.create();
                    return v32;
                case 21:
                    com.android.vending.billing.InAppBillingService.LUCK.AlertDlg v39 = new com.android.vending.billing.InAppBillingService.LUCK.AlertDlg;
                    v39(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext());
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.perm_adapt != null) {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.perm_adapt.setNotifyOnChange(1);
                        v39.setAdapterNotClose(1);
                        String v46_91 = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.All_Dialogs$1;
                        v46_91(this);
                        v39.setAdapter(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.perm_adapt, v46_91);
                        android.text.SpannableString v45_285 = com.chelpus.Utils.getText(2131165382);
                        String v46_92 = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.All_Dialogs$2;
                        v46_92(this);
                        v39.setPositiveButton(v45_285, v46_92);
                    }
                    v32 = v39.create();
                    return v32;
                case 22:
                    com.android.vending.billing.InAppBillingService.LUCK.AlertDlg v37 = new com.android.vending.billing.InAppBillingService.LUCK.AlertDlg;
                    v37(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext());
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.perm_adapt != null) {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.perm_adapt.setNotifyOnChange(1);
                        v37.setAdapterNotClose(1);
                        String v46_88 = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.All_Dialogs$3;
                        v46_88(this);
                        v37.setAdapter(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.perm_adapt, v46_88);
                        android.text.SpannableString v45_277 = com.chelpus.Utils.getText(2131165648);
                        String v46_89 = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.All_Dialogs$4;
                        v46_89(this);
                        v37.setPositiveButton(v45_277, v46_89);
                    }
                    v32 = v37.create();
                    return v32;
                case 24:
                    android.widget.LinearLayout v22_1 = ((android.widget.LinearLayout) android.view.View.inflate(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext(), 2130968608, 0));
                    android.widget.ListView v29_1 = ((android.widget.ListView) v22_1.findViewById(2131558530));
                    android.widget.CheckBox v14_1 = ((android.widget.CheckBox) v22_1.findViewById(2131558531));
                    v14_1.setChecked(1);
                    v14_1.setText(com.chelpus.Utils.getText(2131165371));
                    v14_1.setMaxLines(1);
                    if ((com.chelpus.Utils.getFileDalvikCache("/system/framework/core.jar") == null) || (com.chelpus.Utils.getCurrentRuntimeValue().equals("ART"))) {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchOnlyDalvikCore = 0;
                        v14_1.setChecked(0);
                        v14_1.setEnabled(0);
                    } else {
                        v14_1.setChecked(1);
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchOnlyDalvikCore = 1;
                    }
                    android.text.SpannableString v45_258 = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.All_Dialogs$31;
                    v45_258(this, v14_1);
                    v14_1.setOnCheckedChangeListener(v45_258);
                    com.android.vending.billing.InAppBillingService.LUCK.AlertDlg v21 = new com.android.vending.billing.InAppBillingService.LUCK.AlertDlg;
                    v21(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext());
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patch_adapt != null) {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patch_adapt.setNotifyOnChange(1);
                        v29_1.setAdapter(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patch_adapt);
                        String v0_112 = new int[3];
                        int[] v18_1 = v0_112;
                        v18_1 = {-1715492012, -4215980, -1715492012};
                        android.text.SpannableString v45_265 = new android.graphics.drawable.GradientDrawable;
                        v45_265(android.graphics.drawable.GradientDrawable$Orientation.RIGHT_LEFT, v18_1);
                        v29_1.setDivider(v45_265);
                        v29_1.setDividerHeight(1);
                        android.text.SpannableString v45_267 = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.All_Dialogs$32;
                        v45_267(this);
                        v29_1.setOnItemClickListener(v45_267);
                        v21.setView(v22_1);
                        android.text.SpannableString v45_269 = com.chelpus.Utils.getText(2131165592);
                        String v46_86 = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.All_Dialogs$33;
                        v46_86(this);
                        v21.setPositiveButton(v45_269, v46_86);
                    }
                    v32 = v21.create();
                    return v32;
                case 25:
                    com.android.vending.billing.InAppBillingService.LUCK.AlertDlg v38 = new com.android.vending.billing.InAppBillingService.LUCK.AlertDlg;
                    v38(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext());
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.perm_adapt != null) {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.perm_adapt.setNotifyOnChange(1);
                        v38.setAdapterNotClose(1);
                        String v46_80 = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.All_Dialogs$9;
                        v46_80(this);
                        v38.setAdapter(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.perm_adapt, v46_80);
                        android.text.SpannableString v45_240 = com.chelpus.Utils.getText(2131165648);
                        String v46_81 = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.All_Dialogs$10;
                        v46_81(this);
                        v38.setPositiveButton(v45_240, v46_81);
                    }
                    v32 = v38.create();
                    return v32;
                case 26:
                    com.android.vending.billing.InAppBillingService.LUCK.AlertDlg v7_1 = new com.android.vending.billing.InAppBillingService.LUCK.AlertDlg(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext());
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adapt != null) {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adapt.setNotifyOnChange(1);
                        String v46_78 = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.All_Dialogs$16;
                        v46_78(this);
                        v7_1.setAdapter(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adapt, v46_78);
                    }
                    v32 = v7_1.create();
                    return v32;
                case 28:
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adapt != null) {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adapt.setNotifyOnChange(1);
                    }
                    com.android.vending.billing.InAppBillingService.LUCK.AlertDlg v43 = new com.android.vending.billing.InAppBillingService.LUCK.AlertDlg;
                    v43(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext(), 1);
                    String v46_76 = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.All_Dialogs$17;
                    v46_76(this);
                    v43.setAdapter(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adapt, v46_76);
                    v43.setCancelable(1);
                    android.text.SpannableString v45_227 = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.All_Dialogs$18;
                    v45_227(this);
                    v43.setOnCancelListener(v45_227);
                    v32 = v43.create();
                    return v32;
                case 29:
                    com.android.vending.billing.InAppBillingService.LUCK.AlertDlg v3_1 = new com.android.vending.billing.InAppBillingService.LUCK.AlertDlg(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext());
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.perm_adapt != null) {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.perm_adapt.setNotifyOnChange(1);
                        v3_1.setAdapterNotClose(1);
                        String v46_72 = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.All_Dialogs$5;
                        v46_72(this);
                        v3_1.setAdapter(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.perm_adapt, v46_72);
                        android.text.SpannableString v45_220 = com.chelpus.Utils.getText(2131165510);
                        String v46_73 = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.All_Dialogs$6;
                        v46_73(this);
                        v3_1.setPositiveButton(v45_220, v46_73);
                    }
                    v32 = v3_1.create();
                    return v32;
                case 31:
                    com.android.vending.billing.InAppBillingService.LUCK.AlertDlg v19 = new com.android.vending.billing.InAppBillingService.LUCK.AlertDlg;
                    v19(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext());
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.component_adapt != null) {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.component_adapt.setNotifyOnChange(1);
                        v19.setAdapterNotClose(1);
                        String v46_69 = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.All_Dialogs$7;
                        v46_69(this);
                        v19.setAdapter(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.component_adapt, v46_69);
                        android.text.SpannableString v45_212 = com.chelpus.Utils.getText(2131165510);
                        String v46_70 = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.All_Dialogs$8;
                        v46_70(this);
                        v19.setPositiveButton(v45_212, v46_70);
                    }
                    v32 = v19.create();
                    return v32;
                case 34:
                    com.android.vending.billing.InAppBillingService.LUCK.AlertDlg v42 = new com.android.vending.billing.InAppBillingService.LUCK.AlertDlg;
                    v42(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext());
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patch_adapt != null) {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patch_adapt.setNotifyOnChange(1);
                        v42.setAdapterNotClose(1);
                        String v46_66 = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.All_Dialogs$23;
                        v46_66(this);
                        v42.setAdapter(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patch_adapt, v46_66);
                        android.text.SpannableString v45_204 = com.chelpus.Utils.getText(2131165592);
                        String v46_67 = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.All_Dialogs$24;
                        v46_67(this);
                        v42.setPositiveButton(v45_204, v46_67);
                    }
                    v32 = v42.create();
                    return v32;
                case 35:
                    com.android.vending.billing.InAppBillingService.LUCK.AlertDlg v41 = new com.android.vending.billing.InAppBillingService.LUCK.AlertDlg;
                    v41(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext());
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patch_adapt != null) {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patch_adapt.setNotifyOnChange(1);
                        v41.setAdapterNotClose(1);
                        String v46_63 = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.All_Dialogs$25;
                        v46_63(this);
                        v41.setAdapter(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patch_adapt, v46_63);
                        android.text.SpannableString v45_196 = com.chelpus.Utils.getText(2131165382);
                        String v46_64 = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.All_Dialogs$26;
                        v46_64(this);
                        v41.setPositiveButton(v45_196, v46_64);
                    }
                    v32 = v41.create();
                    return v32;
                case 37:
                    android.widget.LinearLayout v8_1 = ((android.widget.LinearLayout) android.view.View.inflate(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext(), 2130968606, 0));
                    android.widget.TextView v44_1 = ((android.widget.TextView) v8_1.findViewById(2131558527));
                    android.widget.CheckBox v15_1 = ((android.widget.CheckBox) v8_1.findViewById(2131558528));
                    v44_1.setText(com.chelpus.Utils.getText(2131165768));
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.result_core_patch.contains("_patch1")) {
                        int v17_0;
                        String v6_0;
                        int v28 = 0;
                        if ((!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("Core patched!")) && (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("Core 2 patched!"))) {
                            v6_0 = com.chelpus.Utils.getText(2131165373);
                            v17_0 = -65536;
                        } else {
                            v6_0 = com.chelpus.Utils.getText(2131165372);
                            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("Core patched!")) {
                                v28 = (0 + 1);
                            }
                            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("Core 2 patched!")) {
                                v28++;
                            }
                            v17_0 = -16711936;
                        }
                        v44_1.append(new StringBuilder().append(com.chelpus.Utils.getText(2131165300)).append(" ").append(v28).append("/2 ").toString());
                        v44_1.append(com.chelpus.Utils.getColoredText(new StringBuilder().append(v6_0).append("\n").toString(), v17_0, ""));
                    }
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.result_core_patch.contains("_patch2")) {
                        String v6_1;
                        int v17_1;
                        if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("Core unsigned install patched!")) {
                            v6_1 = com.chelpus.Utils.getText(2131165373);
                            v17_1 = -65536;
                        } else {
                            v6_1 = com.chelpus.Utils.getText(2131165372);
                            v17_1 = -16711936;
                        }
                        v44_1.append(new StringBuilder().append(com.chelpus.Utils.getText(2131165302)).append(" ").toString());
                        v44_1.append(com.chelpus.Utils.getColoredText(new StringBuilder().append(v6_1).append("\n").toString(), v17_1, ""));
                    }
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.result_core_patch.contains("_patch3")) {
                        int v17_2;
                        String v6_2;
                        if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("Core4 patched!")) {
                            v6_2 = com.chelpus.Utils.getText(2131165372);
                            v17_2 = -65536;
                        } else {
                            v6_2 = com.chelpus.Utils.getText(2131165372);
                            v17_2 = -16711936;
                        }
                        v44_1.append(new StringBuilder().append(com.chelpus.Utils.getText(2131165304)).append(" ").toString());
                        v44_1.append(com.chelpus.Utils.getColoredText(new StringBuilder().append(v6_2).append("\n").toString(), v17_2, ""));
                    }
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.result_core_patch.contains("restore")) {
                        v15_1.setChecked(0);
                        v15_1.setEnabled(0);
                        com.chelpus.Utils.turn_off_patch_on_boot_all();
                        if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("Core restored!")) || (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("Core 2 restored!"))) {
                            String v6_3 = com.chelpus.Utils.getText(2131165781);
                            v44_1.append(new StringBuilder().append(com.chelpus.Utils.getText(2131165300)).append(" ").toString());
                            v44_1.append(com.chelpus.Utils.getColoredText(new StringBuilder().append(v6_3).append("\n").toString(), -16711936, ""));
                        }
                        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("Core unsigned install restored!")) {
                            String v6_4 = com.chelpus.Utils.getText(2131165781);
                            v44_1.append(new StringBuilder().append(com.chelpus.Utils.getText(2131165302)).append(" ").toString());
                            v44_1.append(com.chelpus.Utils.getColoredText(new StringBuilder().append(v6_4).append("\n").toString(), -16711936, ""));
                        }
                        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("Core4 restored!")) {
                            String v6_5 = com.chelpus.Utils.getText(2131165781);
                            v44_1.append(new StringBuilder().append(com.chelpus.Utils.getText(2131165304)).append(" ").toString());
                            v44_1.append(com.chelpus.Utils.getColoredText(new StringBuilder().append(v6_5).append("\n").toString(), -16711936, ""));
                        }
                    }
                    v44_1.append(new StringBuilder().append("\n").append(com.chelpus.Utils.getText(2131165767)).toString());
                    v15_1.setChecked(0);
                    v15_1.setText(com.chelpus.Utils.getText(2131165769));
                    if (!v15_1.isChecked()) {
                        com.chelpus.Utils.turn_off_patch_on_boot(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.result_core_patch);
                    } else {
                        com.chelpus.Utils.turn_on_patch_on_boot(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.result_core_patch);
                    }
                    android.text.SpannableString v45_183 = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.All_Dialogs$36;
                    v45_183(this);
                    v15_1.setOnCheckedChangeListener(v45_183);
                    com.android.vending.billing.InAppBillingService.LUCK.AlertDlg v20 = new com.android.vending.billing.InAppBillingService.LUCK.AlertDlg;
                    v20(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext());
                    v20.setView(v8_1);
                    android.text.SpannableString v45_187 = com.chelpus.Utils.getText(2131165587);
                    String v46_60 = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.All_Dialogs$37;
                    v46_60(this, v15_1);
                    v20.setPositiveButton(v45_187, v46_60).setTitle(2131165495);
                    v32 = v20.create();
                    return v32;
                case 39:
                    android.widget.LinearLayout v23_1 = ((android.widget.LinearLayout) android.view.View.inflate(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext(), 2130968608, 0));
                    android.widget.CheckBox v16_1 = ((android.widget.CheckBox) v23_1.findViewById(2131558531));
                    v16_1.setChecked(com.chelpus.Utils.readXposedParamBoolean().getBoolean("module_on"));
                    if (!com.chelpus.Utils.isXposedEnabled()) {
                        v16_1.setEnabled(0);
                        v16_1.setChecked(0);
                        v16_1.setText(com.chelpus.Utils.getText(2131165752));
                    } else {
                        v16_1.setEnabled(1);
                        v16_1.setText(com.chelpus.Utils.getText(2131165750));
                    }
                    v16_1.setMaxLines(2);
                    com.android.vending.billing.InAppBillingService.LUCK.AlertDlg v13_1 = new com.android.vending.billing.InAppBillingService.LUCK.AlertDlg(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext());
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patch_adapt != null) {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patch_adapt.setNotifyOnChange(1);
                        android.widget.ListView v30_1 = ((android.widget.ListView) v23_1.findViewById(2131558530));
                        v30_1.setAdapter(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patch_adapt);
                        String v0_25 = new int[3];
                        int[] v18_0 = v0_25;
                        v18_0 = {-1715492012, -4215980, -1715492012};
                        android.text.SpannableString v45_50 = new android.graphics.drawable.GradientDrawable;
                        v45_50(android.graphics.drawable.GradientDrawable$Orientation.RIGHT_LEFT, v18_0);
                        v30_1.setDivider(v45_50);
                        v30_1.setDividerHeight(1);
                        if (com.chelpus.Utils.isXposedEnabled()) {
                            android.text.SpannableString v45_53 = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.All_Dialogs$34;
                            v45_53(this);
                            v30_1.setOnItemClickListener(v45_53);
                        }
                        v13_1.setView(v23_1);
                        if (com.chelpus.Utils.isXposedEnabled()) {
                            android.text.SpannableString v45_56 = com.chelpus.Utils.getText(2131165736);
                            String v46_15 = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.All_Dialogs$35;
                            v46_15(this, v16_1);
                            v13_1.setPositiveButton(v45_56, v46_15);
                        } else {
                            v13_1.setPositiveButton(com.chelpus.Utils.getText(2131165587), 0);
                        }
                    }
                    v32 = v13_1.create();
                    return v32;
                case 3255:
                    android.app.ProgressDialog v36 = new android.app.ProgressDialog;
                    v36(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext());
                    v36.setTitle("Progress");
                    v36.setMessage(com.chelpus.Utils.getText(2131165255));
                    v36.setIndeterminate(1);
                    v32 = v36;
                    return v32;
                case 345350:
                case 3535788:
                    String v35;
                    com.android.vending.billing.InAppBillingService.LUCK.AlertDlg v31_1 = new com.android.vending.billing.InAppBillingService.LUCK.AlertDlg;
                    v31_1(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext());
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.dialog_int != 345350) {
                        v35 = com.chelpus.Utils.getText(2131165435);
                    } else {
                        v35 = com.chelpus.Utils.getText(2131165656);
                    }
                    android.text.SpannableString v45_21 = v31_1.setTitle(com.chelpus.Utils.getText(2131165748)).setIcon(2130837550).setMessage(v35);
                    String v46_8 = com.chelpus.Utils.getText(2131165187);
                    com.android.vending.billing.InAppBillingService.LUCK.dialogs.All_Dialogs$39 v47_1 = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.All_Dialogs$40;
                    v47_1(this);
                    android.text.SpannableString v45_22 = v45_21.setPositiveButton(v46_8, v47_1);
                    String v46_10 = com.chelpus.Utils.getText(2131165563);
                    com.android.vending.billing.InAppBillingService.LUCK.dialogs.All_Dialogs$39 v47_2 = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.All_Dialogs$39;
                    v47_2(this);
                    v45_22.setNegativeButton(v46_10, v47_2);
                    v32 = v31_1.create();
                    return v32;
                case 3535122:
                    com.android.vending.billing.InAppBillingService.LUCK.AlertDlg v31_0 = new com.android.vending.billing.InAppBillingService.LUCK.AlertDlg;
                    v31_0(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext());
                    v31_0.setTitle("Error").setMessage(com.chelpus.Utils.getText(2131165433)).setNegativeButton("OK", 0);
                    v32 = v31_0.create();
                    return v32;
                default:
                    v32 = 0;
                    return v32;
            }
        } catch (Exception v25_1) {
        }
        System.out.println(new StringBuilder().append("LuckyPatcher (Create Dialog): ").append(v25_1).toString());
        v25_1.printStackTrace();
        com.android.vending.billing.InAppBillingService.LUCK.AlertDlg v26 = new com.android.vending.billing.InAppBillingService.LUCK.AlertDlg;
        v26(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext());
        v26.setTitle("Error").setMessage("Sorry...\nShow Dialog - Error...").setNegativeButton("OK", 0);
        v32 = v26.create();
        return v32;
    }

    public void showDialog()
    {
        if (this.dialog == null) {
            this.dialog = this.onCreateDialog();
        }
        if (this.dialog != null) {
            this.dialog.show();
        }
        return;
    }
}
