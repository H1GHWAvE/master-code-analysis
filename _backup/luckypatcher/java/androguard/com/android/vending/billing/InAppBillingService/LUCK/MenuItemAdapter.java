package com.android.vending.billing.InAppBillingService.LUCK;
public class MenuItemAdapter extends android.widget.BaseExpandableListAdapter {
    public static final int TEXT_DEFAULT = 0;
    public static final int TEXT_LARGE = 2;
    public static final int TEXT_MEDIUM = 1;
    public static final int TEXT_SMALL;
    public android.content.Context context;
    public com.android.vending.billing.InAppBillingService.LUCK.MenuItem[] groups;
    public java.util.ArrayList groupsViews;
    private android.widget.ImageView imgIcon;
    private int size;
    public java.util.Comparator sorter;

    public MenuItemAdapter(android.content.Context p3, int p4, java.util.List p5)
    {
        com.android.vending.billing.InAppBillingService.LUCK.MenuItem[] v0_1 = new com.android.vending.billing.InAppBillingService.LUCK.MenuItem[0];
        this.groups = v0_1;
        this.context = 0;
        this.groupsViews = 0;
        this.context = p3;
        this.size = p4;
        com.android.vending.billing.InAppBillingService.LUCK.MenuItem[] v0_3 = new com.android.vending.billing.InAppBillingService.LUCK.MenuItem[p5.size()];
        this.groups = ((com.android.vending.billing.InAppBillingService.LUCK.MenuItem[]) ((com.android.vending.billing.InAppBillingService.LUCK.MenuItem[]) p5.toArray(v0_3)));
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.menu_adapter = this;
        return;
    }

    public MenuItemAdapter(android.content.Context p4, java.util.List p5)
    {
        com.android.vending.billing.InAppBillingService.LUCK.MenuItem[] v0_0 = new com.android.vending.billing.InAppBillingService.LUCK.MenuItem[0];
        this.groups = v0_0;
        this.context = 0;
        this.groupsViews = 0;
        this.context = p4;
        this.size = 0;
        com.android.vending.billing.InAppBillingService.LUCK.MenuItem[] v0_2 = new com.android.vending.billing.InAppBillingService.LUCK.MenuItem[p5.size()];
        this.groups = ((com.android.vending.billing.InAppBillingService.LUCK.MenuItem[]) ((com.android.vending.billing.InAppBillingService.LUCK.MenuItem[]) p5.toArray(v0_2)));
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.menu_adapter = this;
        return;
    }

    private int getColor(int p3)
    {
        int v0 = android.graphics.Color.parseColor("#DDDDDD");
        switch (p3) {
            case 2131165243:
                v0 = android.graphics.Color.parseColor("#99cccc");
                break;
            case 2131165252:
                v0 = android.graphics.Color.parseColor("#c2f055");
                break;
            case 2131165259:
                v0 = android.graphics.Color.parseColor("#99cccc");
                break;
            case 2131165262:
                v0 = android.graphics.Color.parseColor("#c2f055");
                break;
            case 2131165375:
                v0 = android.graphics.Color.parseColor("#cc99cc");
                break;
            case 2131165413:
                v0 = android.graphics.Color.parseColor("#c2f055");
                break;
            case 2131165520:
                v0 = android.graphics.Color.parseColor("#cc99cc");
                break;
            case 2131165546:
                v0 = android.graphics.Color.parseColor("#cc99cc");
                break;
            case 2131165584:
                v0 = android.graphics.Color.parseColor("#ffffbb");
                break;
            case 2131165630:
                v0 = android.graphics.Color.parseColor("#fe6c00");
                break;
            case 2131165636:
                v0 = android.graphics.Color.parseColor("#c2f055");
                break;
            case 2131165638:
                v0 = android.graphics.Color.parseColor("#c2f055");
                break;
            case 2131165647:
                v0 = android.graphics.Color.parseColor("#fe6c00");
                break;
            case 2131165658:
                v0 = android.graphics.Color.parseColor("#ffffbb");
                break;
            case 2131165724:
                v0 = android.graphics.Color.parseColor("#c2f055");
                break;
            case 2131165753:
                v0 = android.graphics.Color.parseColor("#cc99cc");
                break;
        }
        return v0;
    }

    public void add(java.util.ArrayList p2)
    {
        com.android.vending.billing.InAppBillingService.LUCK.MenuItem[] v0_1 = new com.android.vending.billing.InAppBillingService.LUCK.MenuItem[p2.size()];
        this.groups = ((com.android.vending.billing.InAppBillingService.LUCK.MenuItem[]) ((com.android.vending.billing.InAppBillingService.LUCK.MenuItem[]) p2.toArray(v0_1)));
        this.notifyDataSetChanged();
        return;
    }

    public void clear()
    {
        int v0_1 = new com.android.vending.billing.InAppBillingService.LUCK.MenuItem[0];
        this.groups = v0_1;
        this.groupsViews = 0;
        this.notifyDataSetChanged();
        return;
    }

    public Integer getChild(int p2, int p3)
    {
        if ((this.groups != null) && (this.groups.length != 0)) {
            Integer v0_7 = ((Integer) this.groups[p2].childs.get(p3));
        } else {
            v0_7 = 0;
        }
        return v0_7;
    }

    public bridge synthetic Object getChild(int p2, int p3)
    {
        return this.getChild(p2, p3);
    }

    public long getChildId(int p3, int p4)
    {
        return 0;
    }

    public android.view.View getChildView(int p45, int p46, boolean p47, android.view.View p48, android.view.ViewGroup p49)
    {
        com.android.vending.billing.InAppBillingService.LUCK.MenuItem v27 = this.getGroup(p45);
        int v3 = this.getChild(p45, p46).intValue();
        switch (v3) {
            case 1:
                p48 = ((android.view.LayoutInflater) this.context.getSystemService("layout_inflater")).inflate(2130968603, 0);
                android.widget.RadioGroup v12_1 = ((android.widget.RadioGroup) p48.findViewById(2131558521));
                switch (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getInt("viewsize", 0)) {
                    case 0:
                        ((android.widget.RadioButton) v12_1.findViewById(2131558522)).setChecked(1);
                        break;
                    case 1:
                        ((android.widget.RadioButton) v12_1.findViewById(2131558523)).setChecked(1);
                        break;
                    case 2:
                        ((android.widget.RadioButton) v12_1.findViewById(2131558524)).setChecked(1);
                        break;
                }
                int v41_148 = new com.android.vending.billing.InAppBillingService.LUCK.MenuItemAdapter$1;
                v41_148(this);
                v12_1.setOnCheckedChangeListener(v41_148);
                break;
            case 2:
                p48 = ((android.view.LayoutInflater) this.context.getSystemService("layout_inflater")).inflate(2130968600, 0);
                android.widget.RadioGroup v13_1 = ((android.widget.RadioGroup) p48.findViewById(2131558509));
                switch (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getInt("orientstion", 3)) {
                    case 1:
                        ((android.widget.RadioButton) v13_1.findViewById(2131558512)).setChecked(1);
                        break;
                    case 2:
                        ((android.widget.RadioButton) v13_1.findViewById(2131558511)).setChecked(1);
                        break;
                    case 3:
                        ((android.widget.RadioButton) v13_1.findViewById(2131558510)).setChecked(1);
                        break;
                }
                int v41_136 = new com.android.vending.billing.InAppBillingService.LUCK.MenuItemAdapter$2;
                v41_136(this);
                v13_1.setOnCheckedChangeListener(v41_136);
                break;
            case 3:
                p48 = ((android.view.LayoutInflater) this.context.getSystemService("layout_inflater")).inflate(2130968602, 0);
                android.widget.RadioGroup v14_1 = ((android.widget.RadioGroup) p48.findViewById(2131558517));
                switch (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getInt("sortby", 2)) {
                    case 1:
                        ((android.widget.RadioButton) v14_1.findViewById(2131558518)).setChecked(1);
                        break;
                    case 2:
                        ((android.widget.RadioButton) v14_1.findViewById(2131558519)).setChecked(1);
                        break;
                    case 3:
                        ((android.widget.RadioButton) v14_1.findViewById(2131558520)).setChecked(1);
                        break;
                }
                int v41_124 = new com.android.vending.billing.InAppBillingService.LUCK.MenuItemAdapter$3;
                v41_124(this);
                v14_1.setOnCheckedChangeListener(v41_124);
                break;
            case 4:
                p48 = ((android.view.LayoutInflater) this.context.getSystemService("layout_inflater")).inflate(2130968595, 0);
                android.widget.CheckBox v7_1 = ((android.widget.CheckBox) p48.findViewById(2131558490));
                v7_1.setChecked(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getBoolean("lvlapp", 1));
                v7_1.append(com.chelpus.Utils.getColoredText(new StringBuilder().append("\n").append(com.chelpus.Utils.getText(2131165517)).toString(), -7829368, ""));
                int v41_52 = new com.android.vending.billing.InAppBillingService.LUCK.MenuItemAdapter$4;
                v41_52(this);
                v7_1.setOnCheckedChangeListener(v41_52);
                android.widget.CheckBox v4_1 = ((android.widget.CheckBox) p48.findViewById(2131558491));
                v4_1.setChecked(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getBoolean("adsapp", 1));
                v4_1.append(com.chelpus.Utils.getColoredText(new StringBuilder().append("\n").append(com.chelpus.Utils.getText(2131165195)).toString(), -7829368, ""));
                int v41_62 = new com.android.vending.billing.InAppBillingService.LUCK.MenuItemAdapter$5;
                v41_62(this);
                v4_1.setOnCheckedChangeListener(v41_62);
                android.widget.CheckBox v5_1 = ((android.widget.CheckBox) p48.findViewById(2131558492));
                v5_1.setChecked(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getBoolean("customapp", 1));
                v5_1.append(com.chelpus.Utils.getColoredText(new StringBuilder().append("\n").append(com.chelpus.Utils.getText(2131165400)).toString(), -7829368, ""));
                int v41_72 = new com.android.vending.billing.InAppBillingService.LUCK.MenuItemAdapter$6;
                v41_72(this);
                v5_1.setOnCheckedChangeListener(v41_72);
                android.widget.CheckBox v8_1 = ((android.widget.CheckBox) p48.findViewById(2131558493));
                v8_1.setChecked(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getBoolean("modifapp", 1));
                v8_1.append(com.chelpus.Utils.getColoredText(new StringBuilder().append("\n").append(com.chelpus.Utils.getText(2131165551)).toString(), -7829368, ""));
                int v41_82 = new com.android.vending.billing.InAppBillingService.LUCK.MenuItemAdapter$7;
                v41_82(this);
                v8_1.setOnCheckedChangeListener(v41_82);
                android.widget.CheckBox v6_1 = ((android.widget.CheckBox) p48.findViewById(2131558494));
                v6_1.setChecked(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getBoolean("fixedapp", 1));
                v6_1.append(com.chelpus.Utils.getColoredText(new StringBuilder().append("\n").append(com.chelpus.Utils.getText(2131165474)).toString(), -7829368, ""));
                int v41_92 = new com.android.vending.billing.InAppBillingService.LUCK.MenuItemAdapter$8;
                v41_92(this);
                v6_1.setOnCheckedChangeListener(v41_92);
                android.widget.CheckBox v9_1 = ((android.widget.CheckBox) p48.findViewById(2131558495));
                v9_1.setChecked(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getBoolean("noneapp", 1));
                v9_1.append(com.chelpus.Utils.getColoredText(new StringBuilder().append("\n").append(com.chelpus.Utils.getText(2131165574)).toString(), -7829368, ""));
                int v41_102 = new com.android.vending.billing.InAppBillingService.LUCK.MenuItemAdapter$9;
                v41_102(this);
                v9_1.setOnCheckedChangeListener(v41_102);
                android.widget.CheckBox v10_1 = ((android.widget.CheckBox) p48.findViewById(2131558496));
                v10_1.setChecked(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getBoolean("systemapp", 0));
                v10_1.append(com.chelpus.Utils.getColoredText(new StringBuilder().append("\n").append(com.chelpus.Utils.getText(2131165715)).toString(), -7829368, ""));
                int v41_112 = new com.android.vending.billing.InAppBillingService.LUCK.MenuItemAdapter$10;
                v41_112(this);
                v10_1.setOnCheckedChangeListener(v41_112);
                break;
            case 5:
                p48 = ((android.view.LayoutInflater) this.context.getSystemService("layout_inflater")).inflate(2130968601, 0);
                android.widget.RadioGroup v15_1 = ((android.widget.RadioGroup) p48.findViewById(2131558513));
                switch (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getInt("root_force", 0)) {
                    case 0:
                        ((android.widget.RadioButton) v15_1.findViewById(2131558514)).setChecked(1);
                        break;
                    case 1:
                        ((android.widget.RadioButton) v15_1.findViewById(2131558515)).setChecked(1);
                        break;
                    case 2:
                        ((android.widget.RadioButton) v15_1.findViewById(2131558516)).setChecked(1);
                        break;
                }
                int v41_40 = new com.android.vending.billing.InAppBillingService.LUCK.MenuItemAdapter$11;
                v41_40(this);
                v15_1.setOnCheckedChangeListener(v41_40);
                break;
            case 6:
                p48 = ((android.view.LayoutInflater) this.context.getSystemService("layout_inflater")).inflate(2130968599, 0);
                android.widget.RadioGroup v16_1 = ((android.widget.RadioGroup) p48.findViewById(2131558506));
                switch (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getInt("apkname", 1)) {
                    case 0:
                        ((android.widget.RadioButton) v16_1.findViewById(2131558507)).setChecked(1);
                        break;
                    case 1:
                        ((android.widget.RadioButton) v16_1.findViewById(2131558508)).setChecked(1);
                        break;
                }
                int v41_28 = new com.android.vending.billing.InAppBillingService.LUCK.MenuItemAdapter$12;
                v41_28(this);
                v16_1.setOnCheckedChangeListener(v41_28);
                break;
            case 7:
                p48 = ((android.view.LayoutInflater) this.context.getSystemService("layout_inflater")).inflate(2130968598, 0);
                android.widget.RadioGroup v17_1 = ((android.widget.RadioGroup) p48.findViewById(2131558499));
                switch (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getInt("default_icon_for_lp", 0)) {
                    case 0:
                        ((android.widget.RadioButton) v17_1.findViewById(2131558500)).setChecked(1);
                        break;
                    case 1:
                        ((android.widget.RadioButton) v17_1.findViewById(2131558501)).setChecked(1);
                        break;
                    case 2:
                        ((android.widget.RadioButton) v17_1.findViewById(2131558502)).setChecked(1);
                        break;
                    case 3:
                        ((android.widget.RadioButton) v17_1.findViewById(2131558503)).setChecked(1);
                        break;
                    case 4:
                        ((android.widget.RadioButton) v17_1.findViewById(2131558504)).setChecked(1);
                        break;
                    case 5:
                        ((android.widget.RadioButton) v17_1.findViewById(2131558505)).setChecked(1);
                        break;
                }
                int v41_18 = new com.android.vending.billing.InAppBillingService.LUCK.MenuItemAdapter$13;
                v41_18(this);
                v17_1.setOnCheckedChangeListener(v41_18);
                break;
            default:
                p48 = ((android.view.LayoutInflater) this.context.getSystemService("layout_inflater")).inflate(2130968604, 0);
                android.widget.TextView v40_1 = ((android.widget.TextView) p48.findViewById(2131558526));
                v40_1.setTextAppearance(this.context, com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getSizeText());
                v40_1.setTextAppearance(this.context, com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getSizeText());
                v40_1.setText(com.chelpus.Utils.getText(v3));
                android.widget.ImageView v18_1 = ((android.widget.ImageView) p48.findViewById(2131558525));
                v18_1.setImageDrawable(this.getImage(v27.punkt_menu));
                int v11 = this.getColor(this.getGroup(p45).punkt_menu);
                v18_1.setColorFilter(v11, android.graphics.PorterDuff$Mode.MULTIPLY);
                v40_1.setTextColor(v11);
        }
        return p48;
    }

    public int getChildrenCount(int p2)
    {
        if ((this.groups != null) && (this.groups.length != 0)) {
            int v0_6 = this.groups[p2].childs.size();
        } else {
            v0_6 = 0;
        }
        return v0_6;
    }

    public com.android.vending.billing.InAppBillingService.LUCK.MenuItem getGroup(int p2)
    {
        return this.groups[p2];
    }

    public bridge synthetic Object getGroup(int p2)
    {
        return this.getGroup(p2);
    }

    public int getGroupCount()
    {
        if ((this.groups != null) && (this.groups.length != 0)) {
            int v0_4 = this.groups.length;
        } else {
            v0_4 = 0;
        }
        return v0_4;
    }

    public long getGroupId(int p3)
    {
        return 0;
    }

    public android.view.View getGroupView(int p27, boolean p28, android.view.View p29, android.view.ViewGroup p30)
    {
        com.android.vending.billing.InAppBillingService.LUCK.MenuItem v15 = this.getGroup(p27);
        int v7 = v15.punkt_menu;
        try {
            android.view.View v29_1 = ((android.view.View) this.groupsViews.get(p27));
        } catch (Exception v6) {
            v29_1 = 0;
        }
        if (v29_1 == null) {
            android.view.LayoutInflater v12_1 = ((android.view.LayoutInflater) this.context.getSystemService("layout_inflater"));
            switch (v15.type) {
                case 3:
                    v29_1 = v12_1.inflate(2130968619, 0);
                    break;
                default:
                    v29_1 = v12_1.inflate(2130968620, 0);
            }
            if (this.groupsViews == null) {
                this.groupsViews = new java.util.ArrayList(this.size);
                int v23_0 = this.groups.length;
                int v21_8 = 0;
                while (v21_8 < v23_0) {
                    this.groupsViews.add(0);
                    v21_8++;
                }
            }
        }
        v29_1.setClickable(0);
        switch (v15.type) {
            case 0:
                android.widget.TextView v16_1 = ((android.widget.TextView) v29_1.findViewById(2131558585));
                v16_1.setTextAppearance(this.context, com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getSizeText());
                v16_1.setTextAppearance(this.context, com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getSizeText());
                v16_1.setText(com.chelpus.Utils.getText(v7));
                v16_1.setTypeface(v16_1.getTypeface(), 1);
                android.widget.ImageView v8_1 = ((android.widget.ImageView) v29_1.findViewById(2131558584));
                v8_1.setImageDrawable(this.getImage(v7));
                v8_1.setVisibility(0);
                int v5 = this.getColor(v7);
                v16_1.setTextColor(v5);
                v8_1.setColorFilter(v5, android.graphics.PorterDuff$Mode.MULTIPLY);
                break;
            case 1:
                ((android.widget.LinearLayout) v29_1.findViewById(2131558588)).setPadding(10, 5, 50, 5);
                android.widget.TextView v17_1 = ((android.widget.TextView) v29_1.findViewById(2131558585));
                v17_1.setTextAppearance(this.context, com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getSizeText());
                v17_1.setTextAppearance(this.context, com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getSizeText());
                v17_1.setTextColor(android.graphics.Color.parseColor("#000000"));
                v17_1.setTypeface(v17_1.getTypeface(), 1);
                ((android.widget.ImageView) v29_1.findViewById(2131558584)).setVisibility(8);
                v29_1.setBackgroundColor(android.graphics.Color.parseColor("#9F9F9F"));
                v17_1.setText(com.chelpus.Utils.getText(v7));
                break;
            case 2:
                android.widget.TextView v18_1 = ((android.widget.TextView) v29_1.findViewById(2131558585));
                v18_1.setTextAppearance(this.context, com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getSizeText());
                v18_1.setTextAppearance(this.context, com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getSizeText());
                v18_1.setText(com.chelpus.Utils.getText(v7));
                v18_1.setTypeface(v18_1.getTypeface(), 1);
                v18_1.setTextColor(android.graphics.Color.parseColor("#feeb9c"));
                android.widget.ImageView v10_1 = ((android.widget.ImageView) v29_1.findViewById(2131558584));
                v10_1.setImageDrawable(this.getImage(v7));
                v10_1.setVisibility(0);
                break;
            case 3:
                android.widget.TextView v19_1 = ((android.widget.TextView) v29_1.findViewById(2131558585));
                v19_1.setTextAppearance(this.context, com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getSizeText());
                v19_1.setTextAppearance(this.context, com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getSizeText());
                v19_1.setText(com.chelpus.Utils.getText(v7));
                v19_1.setTypeface(v19_1.getTypeface(), 1);
                v19_1.setTextColor(android.graphics.Color.parseColor("#feeb9c"));
                android.widget.ImageView v11_1 = ((android.widget.ImageView) v29_1.findViewById(2131558584));
                android.widget.CheckBox v4_1 = ((android.widget.CheckBox) v29_1.findViewById(2131558587));
                v4_1.setChecked(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getBoolean(v15.booleanPref, v15.booleanPrefDef));
                v4_1.setClickable(0);
                v11_1.setImageDrawable(this.getImage(v7));
                v11_1.setVisibility(0);
                break;
        }
        if (v15.punkt_menu_descr == 0) {
            ((android.widget.TextView) v29_1.findViewById(2131558586)).setVisibility(8);
        } else {
            android.widget.TextView v20_3 = ((android.widget.TextView) v29_1.findViewById(2131558586));
            v20_3.setText(com.chelpus.Utils.getText(v15.punkt_menu_descr));
            v20_3.setTextColor(-3355444);
        }
        this.groupsViews.set(p27, v29_1);
        return v29_1;
    }

    android.graphics.drawable.Drawable getImage(int p8)
    {
        android.graphics.drawable.Drawable v0 = 0;
        switch (p8) {
            case 2131165190:
                v0 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDrawable(2130903044);
                break;
            case 2131165200:
                v0 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDrawable(2130903045);
                break;
            case 2131165211:
                v0 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDrawable(2130837568);
                break;
            case 2131165226:
                v0 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDrawable(2130837504);
                v0.setColorFilter(android.graphics.Color.parseColor("#FFFFFF"), android.graphics.PorterDuff$Mode.SRC_IN);
                break;
            case 2131165227:
                v0 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDrawable(2130837504);
                v0.setColorFilter(android.graphics.Color.parseColor("#FFFFFF"), android.graphics.PorterDuff$Mode.SRC_IN);
                break;
            case 2131165243:
                v0 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDrawable(2130837505);
                v0.setColorFilter(android.graphics.Color.parseColor("#FFFFFF"), android.graphics.PorterDuff$Mode.SRC_IN);
                break;
            case 2131165249:
                v0 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDrawable(2130903045);
                break;
            case 2131165252:
                v0 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDrawable(2130837530);
                break;
            case 2131165259:
                v0 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDrawable(2130837505);
                break;
            case 2131165279:
                v0 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDrawable(2130837505);
                break;
            case 2131165291:
                v0 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDrawable(2130837530);
                break;
            case 2131165375:
                v0 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDrawable(2130837504);
                v0.setColorFilter(android.graphics.Color.parseColor("#FFFFFF"), android.graphics.PorterDuff$Mode.SRC_IN);
                break;
            case 2131165404:
                v0 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDrawable(2130903043);
                break;
            case 2131165411:
                v0 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDrawable(2130903043);
                break;
            case 2131165413:
                v0 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDrawable(2130837540);
                break;
            case 2131165415:
                v0 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDrawable(2130903042);
                break;
            case 2131165453:
                v0 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDrawable(2130903042);
                break;
            case 2131165456:
                v0 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDrawable(2130903041);
                break;
            case 2131165483:
                v0 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDrawable(2130903044);
                break;
            case 2131165486:
                v0 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDrawable(2130903042);
                break;
            case 2131165488:
                v0 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDrawable(2130903042);
                break;
            case 2131165509:
                v0 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDrawable(2130903041);
                break;
            case 2131165513:
                v0 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDrawable(2130837504);
                v0.setColorFilter(android.graphics.Color.parseColor("#FFFFFF"), android.graphics.PorterDuff$Mode.SRC_IN);
                break;
            case 2131165514:
                v0 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDrawable(2130837504);
                v0.setColorFilter(android.graphics.Color.parseColor("#FFFFFF"), android.graphics.PorterDuff$Mode.SRC_IN);
                break;
            case 2131165520:
                v0 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDrawable(2130837541);
                break;
            case 2131165546:
                v0 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDrawable(2130837541);
                break;
            case 2131165565:
                v0 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDrawable(2130903042);
                break;
            case 2131165584:
                v0 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDrawable(2130837535);
                break;
            case 2131165590:
                v0 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDrawable(2130903045);
                break;
            case 2131165630:
                v0 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDrawable(2130837538);
                break;
            case 2131165636:
                v0 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDrawable(2130837504);
                v0.setColorFilter(android.graphics.Color.parseColor("#FFFFFF"), android.graphics.PorterDuff$Mode.SRC_IN);
                break;
            case 2131165638:
                v0 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDrawable(2130837530);
                break;
            case 2131165657:
                v0 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDrawable(2130903044);
                break;
            case 2131165658:
                v0 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDrawable(2130837505);
                v0.setColorFilter(android.graphics.Color.parseColor("#FFFFFF"), android.graphics.PorterDuff$Mode.SRC_IN);
                break;
            case 2131165663:
                v0 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDrawable(2130903042);
                break;
            case 2131165666:
                v0 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDrawable(2130903045);
                break;
            case 2131165677:
                v0 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDrawable(2130903045);
                break;
            case 2131165718:
                v0 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDrawable(2130903045);
                break;
            case 2131165724:
                v0 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDrawable(2130837530);
                break;
            case 2131165734:
                v0 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDrawable(2130903044);
                break;
            case 2131165738:
                v0 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDrawable(2130903042);
                break;
            case 2131165753:
                v0 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDrawable(2130837504);
                v0.setColorFilter(android.graphics.Color.parseColor("#FFFFFF"), android.graphics.PorterDuff$Mode.SRC_IN);
                break;
        }
        return v0;
    }

    public boolean hasStableIds()
    {
        return 0;
    }

    public boolean isChildSelectable(int p2, int p3)
    {
        return 1;
    }

    public void onGroupCollapsed(int p1)
    {
        super.onGroupCollapsed(p1);
        return;
    }

    public void onGroupExpanded(int p1)
    {
        super.onGroupExpanded(p1);
        return;
    }

    public void setTextSize(int p1)
    {
        this.size = p1;
        this.notifyDataSetChanged();
        return;
    }
}
