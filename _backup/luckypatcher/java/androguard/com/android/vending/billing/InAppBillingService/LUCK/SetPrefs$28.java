package com.android.vending.billing.InAppBillingService.LUCK;
 class SetPrefs$28 implements android.preference.Preference$OnPreferenceChangeListener {
    final synthetic com.android.vending.billing.InAppBillingService.LUCK.SetPrefs this$0;

    SetPrefs$28(com.android.vending.billing.InAppBillingService.LUCK.SetPrefs p1)
    {
        this.this$0 = p1;
        return;
    }

    public boolean onPreferenceChange(android.preference.Preference p6, Object p7)
    {
        int v1 = 1;
        android.content.SharedPreferences v0 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getSharedPreferences("config", 4);
        if ((p6 != this.this$0.findPreference("days_on_up")) || (!com.android.vending.billing.InAppBillingService.LUCK.SetPrefs.access$100(this.this$0, p7))) {
            v1 = 0;
        } else {
            v0.edit().putInt("days_on_up", Integer.valueOf(p7.toString()).intValue()).commit();
            p6.getEditor().putString("days_on_up", p7.toString());
            v0.edit().putBoolean("settings_change", 1).commit();
            v0.edit().putBoolean("lang_change", 1).commit();
        }
        return v1;
    }
}
