package com.android.vending.billing.InAppBillingService.LUCK.dialogs;
public class Market_Install_Dialog {
    public static final int LOADING_PROGRESS_DIALOG = 23;
    android.app.Dialog dialog;

    public Market_Install_Dialog()
    {
        this.dialog = 0;
        return;
    }

    public void dismiss()
    {
        if (this.dialog != null) {
            this.dialog.dismiss();
            this.dialog = 0;
        }
        return;
    }

    public android.app.Dialog onCreateDialog()
    {
        System.out.println("Market install Dialog create.");
        if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag == null) || (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext() == null)) {
            this.dismiss();
        }
        android.widget.LinearLayout v6_1 = ((android.widget.LinearLayout) android.view.View.inflate(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext(), 2130968618, 0));
        android.widget.LinearLayout v17_1 = ((android.widget.LinearLayout) v6_1.findViewById(2131558567).findViewById(2131558412));
        android.widget.TextView v20_1 = ((android.widget.TextView) v17_1.findViewById(2131558580));
        android.widget.RadioGroup v8_1 = ((android.widget.RadioGroup) v17_1.findViewById(2131558414));
        v8_1.getCheckedRadioButtonId();
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.verMarket = "mod.market53.apk";
        ((android.widget.RadioButton) v8_1.findViewById(2131558571)).setText(new StringBuilder().append(com.chelpus.Utils.getText(2131165525)).append(" 4.1.6 (Android 2.2 and UP)").toString());
        ((android.widget.RadioButton) v8_1.findViewById(2131558577)).setText(new StringBuilder().append(com.chelpus.Utils.getText(2131165526)).append(" 4.1.6 (Android 2.2 and UP)").toString());
        ((android.widget.RadioButton) v8_1.findViewById(2131558570)).setText(new StringBuilder().append(com.chelpus.Utils.getText(2131165525)).append(" 4.9.13 (Android 2.2 and UP)").toString());
        ((android.widget.RadioButton) v8_1.findViewById(2131558576)).setText(new StringBuilder().append(com.chelpus.Utils.getText(2131165526)).append(" 4.9.13 (Android 2.2 and UP)").toString());
        ((android.widget.RadioButton) v8_1.findViewById(2131558569)).setText(new StringBuilder().append(com.chelpus.Utils.getText(2131165525)).append(" 5.1.11 (Android 2.2 and UP)").toString());
        ((android.widget.RadioButton) v8_1.findViewById(2131558575)).setText(new StringBuilder().append(com.chelpus.Utils.getText(2131165526)).append(" 5.1.11 (Android 2.2 and UP)").toString());
        ((android.widget.RadioButton) v8_1.findViewById(2131558568)).setText(new StringBuilder().append(com.chelpus.Utils.getText(2131165525)).append(" 5.3.6 (Android 2.2 and UP)").toString());
        ((android.widget.RadioButton) v8_1.findViewById(2131558574)).setText(new StringBuilder().append(com.chelpus.Utils.getText(2131165526)).append(" 5.3.6 (Android 2.2 and UP)").toString());
        int v21_59 = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.Market_Install_Dialog$1;
        v21_59(this);
        v8_1.setOnCheckedChangeListener(v21_59);
        android.widget.Button v4_1 = ((android.widget.Button) v6_1.findViewById(2131558582));
        android.widget.CheckBox v5_1 = ((android.widget.CheckBox) v6_1.findViewById(2131558581));
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.asUser = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.asNeedUser = 0;
        try {
            android.content.pm.ApplicationInfo v3 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getApplicationInfo("com.android.vending", 0);
        } catch (Exception v7) {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.asUser = 0;
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.asNeedUser = 0;
            v5_1.setEnabled(1);
            v5_1.setChecked(0);
            int v21_86 = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.Market_Install_Dialog$2;
            v21_86(this);
            v5_1.setOnClickListener(v21_86);
            v20_1.setText(com.chelpus.Utils.getColoredText(new StringBuilder().append("  ").append(com.chelpus.Utils.getText(2131165505)).toString(), 65280, "bold"));
            v20_1.append(new StringBuilder().append("\n\n").append(com.chelpus.Utils.getText(2131165519)).toString());
            int v21_98 = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.Market_Install_Dialog$3;
            v21_98(this);
            v4_1.setOnClickListener(v21_98);
            android.app.Dialog v19 = new com.android.vending.billing.InAppBillingService.LUCK.AlertDlg(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext(), 1).setView(v6_1).create();
            v19.setCancelable(1);
            int v21_103 = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.Market_Install_Dialog$4;
            v21_103(this);
            v19.setOnCancelListener(v21_103);
            return v19;
        } catch (Exception v7) {
            v7.printStackTrace();
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.asUser = 0;
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.asNeedUser = 0;
            v5_1.setEnabled(1);
            v5_1.setChecked(0);
            v21_86 = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.Market_Install_Dialog$2;
            v21_86(this);
            v5_1.setOnClickListener(v21_86);
            v20_1.setText(com.chelpus.Utils.getColoredText(new StringBuilder().append("  ").append(com.chelpus.Utils.getText(2131165505)).toString(), 65280, "bold"));
            v20_1.append(new StringBuilder().append("\n\n").append(com.chelpus.Utils.getText(2131165519)).toString());
            v21_98 = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.Market_Install_Dialog$3;
            v21_98(this);
            v4_1.setOnClickListener(v21_98);
            v19 = new com.android.vending.billing.InAppBillingService.LUCK.AlertDlg(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext(), 1).setView(v6_1).create();
            v19.setCancelable(1);
            v21_103 = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.Market_Install_Dialog$4;
            v21_103(this);
            v19.setOnCancelListener(v21_103);
            return v19;
        }
        if ((!com.chelpus.Utils.checkCoreJarPatch11()) || ((!com.chelpus.Utils.checkCoreJarPatch12()) || ((!com.chelpus.Utils.checkCoreJarPatch20()) || ((v3.flags & 1) == 0)))) {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.asUser = 0;
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.asNeedUser = 0;
            v5_1.setEnabled(1);
            v5_1.setChecked(0);
            v21_86 = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.Market_Install_Dialog$2;
            v21_86(this);
            v5_1.setOnClickListener(v21_86);
            v20_1.setText(com.chelpus.Utils.getColoredText(new StringBuilder().append("  ").append(com.chelpus.Utils.getText(2131165505)).toString(), 65280, "bold"));
            v20_1.append(new StringBuilder().append("\n\n").append(com.chelpus.Utils.getText(2131165519)).toString());
            v21_98 = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.Market_Install_Dialog$3;
            v21_98(this);
            v4_1.setOnClickListener(v21_98);
            v19 = new com.android.vending.billing.InAppBillingService.LUCK.AlertDlg(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext(), 1).setView(v6_1).create();
            v19.setCancelable(1);
            v21_103 = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.Market_Install_Dialog$4;
            v21_103(this);
            v19.setOnCancelListener(v21_103);
            return v19;
        } else {
            v5_1.setEnabled(1);
            v5_1.setChecked(0);
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.asUser = 0;
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.asNeedUser = 1;
            v21_86 = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.Market_Install_Dialog$2;
            v21_86(this);
            v5_1.setOnClickListener(v21_86);
            v20_1.setText(com.chelpus.Utils.getColoredText(new StringBuilder().append("  ").append(com.chelpus.Utils.getText(2131165505)).toString(), 65280, "bold"));
            v20_1.append(new StringBuilder().append("\n\n").append(com.chelpus.Utils.getText(2131165519)).toString());
            v21_98 = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.Market_Install_Dialog$3;
            v21_98(this);
            v4_1.setOnClickListener(v21_98);
            v19 = new com.android.vending.billing.InAppBillingService.LUCK.AlertDlg(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext(), 1).setView(v6_1).create();
            v19.setCancelable(1);
            v21_103 = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.Market_Install_Dialog$4;
            v21_103(this);
            v19.setOnCancelListener(v21_103);
            return v19;
        }
    }

    public void showDialog()
    {
        if (this.dialog == null) {
            this.dialog = this.onCreateDialog();
        }
        if (this.dialog != null) {
            this.dialog.show();
        }
        return;
    }
}
