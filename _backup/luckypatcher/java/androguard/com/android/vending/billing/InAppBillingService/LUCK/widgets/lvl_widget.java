package com.android.vending.billing.InAppBillingService.LUCK.widgets;
public class lvl_widget extends android.appwidget.AppWidgetProvider {
    public static String ACTION_WIDGET_RECEIVER;
    public static String ACTION_WIDGET_RECEIVER_Updater;

    static lvl_widget()
    {
        com.android.vending.billing.InAppBillingService.LUCK.widgets.lvl_widget.ACTION_WIDGET_RECEIVER = "ActionReceiverLVLWidget";
        com.android.vending.billing.InAppBillingService.LUCK.widgets.lvl_widget.ACTION_WIDGET_RECEIVER_Updater = "ActionReceiverWidgetLVLUpdate";
        return;
    }

    public lvl_widget()
    {
        return;
    }

    public static void pushWidgetUpdate(android.content.Context p3, android.widget.RemoteViews p4)
    {
        android.appwidget.AppWidgetManager.getInstance(p3).updateAppWidget(new android.content.ComponentName(p3, com.android.vending.billing.InAppBillingService.LUCK.widgets.lvl_widget), p4);
        return;
    }

    public void onReceive(android.content.Context p13, android.content.Intent p14)
    {
        String v0 = p14.getAction();
        if (com.android.vending.billing.InAppBillingService.LUCK.widgets.lvl_widget.ACTION_WIDGET_RECEIVER.equals(v0)) {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.init();
            android.widget.RemoteViews v4_1 = new android.widget.RemoteViews(p13.getPackageName(), 2130968638);
            v4_1.setTextViewText(2131558638, "");
            v4_1.setViewVisibility(2131558639, 0);
            android.appwidget.AppWidgetManager v2_0 = android.appwidget.AppWidgetManager.getInstance(p13);
            v2_0.updateAppWidget(v2_0.getAppWidgetIds(new android.content.ComponentName(p13, com.android.vending.billing.InAppBillingService.LUCK.widgets.lvl_widget)), v4_1);
            if (p13.getPackageManager().getComponentEnabledSetting(new android.content.ComponentName(p13, com.google.android.finsky.services.LicensingService)) != 2) {
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), com.google.android.finsky.services.LicensingService), 2, 1);
                if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su) {
                    com.chelpus.Utils.market_licensing_services(1);
                }
                android.widget.Toast.makeText(p13, "LVL-OFF", 0).show();
            } else {
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), com.google.android.finsky.services.LicensingService), 1, 1);
                if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getBoolean("LVL_enable", 0)) {
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().edit().putBoolean("LVL_enable", 1).commit();
                }
                android.widget.Toast.makeText(p13, "LVL-ON", 0).show();
            }
        }
        if (com.android.vending.billing.InAppBillingService.LUCK.widgets.lvl_widget.ACTION_WIDGET_RECEIVER_Updater.equals(v0)) {
            try {
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.appDisabler = 1;
                android.appwidget.AppWidgetManager v2_1 = android.appwidget.AppWidgetManager.getInstance(p13);
                this.onUpdate(p13, v2_1, v2_1.getAppWidgetIds(new android.content.ComponentName(p13, com.android.vending.billing.InAppBillingService.LUCK.widgets.lvl_widget)));
            } catch (Exception v1) {
                v1.printStackTrace();
            }
        }
        super.onReceive(p13, p14);
        return;
    }

    public void onUpdate(android.content.Context p8, android.appwidget.AppWidgetManager p9, int[] p10)
    {
        android.widget.RemoteViews v2_1 = new android.widget.RemoteViews(p8.getPackageName(), 2130968638);
        android.content.Intent v1_1 = new android.content.Intent(p8, com.android.vending.billing.InAppBillingService.LUCK.widgets.lvl_widget);
        v1_1.setAction(com.android.vending.billing.InAppBillingService.LUCK.widgets.lvl_widget.ACTION_WIDGET_RECEIVER);
        v1_1.putExtra("msg", "Hello Habrahabr");
        v2_1.setOnClickPendingIntent(2131558638, android.app.PendingIntent.getBroadcast(p8, 0, v1_1, 0));
        v2_1.setTextViewText(2131558638, "LVL");
        v2_1.setViewVisibility(2131558639, 8);
        if (p8.getPackageManager().getComponentEnabledSetting(new android.content.ComponentName(p8, com.google.android.finsky.services.LicensingService)) != 2) {
            v2_1.setTextColor(2131558638, android.graphics.Color.parseColor("#00FF00"));
        } else {
            v2_1.setTextColor(2131558638, android.graphics.Color.parseColor("#FF0000"));
        }
        p9.updateAppWidget(p10, v2_1);
        return;
    }
}
