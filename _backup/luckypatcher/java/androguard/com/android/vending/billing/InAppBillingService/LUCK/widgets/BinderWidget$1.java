package com.android.vending.billing.InAppBillingService.LUCK.widgets;
final class BinderWidget$1 implements java.lang.Runnable {
    final synthetic int val$appWidgetId;
    final synthetic android.content.Context val$context;

    BinderWidget$1(android.content.Context p1, int p2)
    {
        this.val$context = p1;
        this.val$appWidgetId = p2;
        return;
    }

    public void run()
    {
        android.widget.RemoteViews v10_1 = new android.widget.RemoteViews(this.val$context.getPackageName(), 2130968589);
        android.content.Intent v2_1 = new android.content.Intent(this.val$context, com.android.vending.billing.InAppBillingService.LUCK.widgets.BinderWidget);
        v2_1.setAction(com.android.vending.billing.InAppBillingService.LUCK.widgets.BinderWidget.ACTION_WIDGET_RECEIVER);
        v2_1.putExtra("appWidgetId", this.val$appWidgetId);
        v10_1.setOnClickPendingIntent(2131558444, android.app.PendingIntent.getBroadcast(this.val$context, this.val$appWidgetId, v2_1, 0));
        v10_1.setInt(2131558443, "setBackgroundResource", 2130837584);
        v10_1.setTextColor(2131558442, android.graphics.Color.parseColor("#AAAAAA"));
        v10_1.setTextViewText(2131558442, "wait");
        try {
            android.appwidget.AppWidgetManager.getInstance(this.val$context).updateAppWidget(this.val$appWidgetId, v10_1);
        } catch (Exception v5_0) {
            v5_0.printStackTrace();
        }
        if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su) {
            v10_1.setInt(2131558443, "setBackgroundResource", 2130837584);
            v10_1.setTextColor(2131558442, android.graphics.Color.parseColor("#AAAAAA"));
            v10_1.setTextViewText(2131558442, "you need root access");
            try {
                android.appwidget.AppWidgetManager.getInstance(this.val$context).updateAppWidget(this.val$appWidgetId, v10_1);
            } catch (Exception v5_1) {
                v5_1.printStackTrace();
            }
        } else {
            String v4 = com.android.vending.billing.InAppBillingService.LUCK.widgets.BinderWidgetConfigureActivity.loadTitlePref(this.val$context, this.val$appWidgetId);
            System.out.println(v4);
            com.android.vending.billing.InAppBillingService.LUCK.BindItem v8_1 = new com.android.vending.billing.InAppBillingService.LUCK.BindItem(v4.replaceAll("~chelpus_disabled~", ""));
            if ((v8_1.TargetDir != null) && ((!v8_1.TargetDir.equals("")) && ((v8_1.SourceDir != null) && (!v8_1.SourceDir.equals(""))))) {
                String[] v9 = v8_1.TargetDir.split("/");
                v10_1.setTextViewText(2131558442, v9[(v9.length - 1)]);
                if (!com.chelpus.Utils.checkBind(v8_1)) {
                    v10_1.setTextColor(2131558442, android.graphics.Color.parseColor("#FF0000"));
                    v10_1.setInt(2131558443, "setBackgroundResource", 2130837584);
                } else {
                    v10_1.setTextColor(2131558442, android.graphics.Color.parseColor("#00FF00"));
                    v10_1.setInt(2131558443, "setBackgroundResource", 2130837585);
                }
                int v6 = 0;
                android.appwidget.AppWidgetManager v11_36 = com.android.vending.billing.InAppBillingService.LUCK.BinderActivity.getBindes(this.val$context).iterator();
                while (v11_36.hasNext()) {
                    com.android.vending.billing.InAppBillingService.LUCK.BindItem v7_1 = ((com.android.vending.billing.InAppBillingService.LUCK.BindItem) v11_36.next());
                    if ((v7_1.SourceDir.replaceAll("~chelpus_disabled~", "").equals(v8_1.SourceDir.replaceAll("~chelpus_disabled~", ""))) && (v7_1.TargetDir.replaceAll("~chelpus_disabled~", "").equals(v8_1.TargetDir.replaceAll("~chelpus_disabled~", "")))) {
                        v7_1.TargetDir = v7_1.TargetDir.replaceAll("~chelpus_disabled~", "");
                        v7_1.SourceDir = v7_1.SourceDir.replaceAll("~chelpus_disabled~", "");
                        v6 = 1;
                    }
                }
                if (v6 == 0) {
                    if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su) {
                        android.appwidget.AppWidgetManager v11_39 = new String[1];
                        v11_39[0] = new StringBuilder().append("umount \'").append(v8_1.TargetDir.replaceAll("~chelpus_disabled~", "")).append("\'").toString();
                        com.chelpus.Utils.cmd(v11_39);
                    } else {
                        com.chelpus.Utils.run_all(new StringBuilder().append("umount -f \'").append(v8_1.TargetDir.replaceAll("~chelpus_disabled~", "")).append("\'").toString());
                        com.chelpus.Utils.run_all(new StringBuilder().append("umount -l \'").append(v8_1.TargetDir.replaceAll("~chelpus_disabled~", "")).append("\'").toString());
                    }
                    v10_1.setInt(2131558443, "setBackgroundResource", 2130837584);
                    v10_1.setTextColor(2131558442, android.graphics.Color.parseColor("#AAAAAA"));
                    v10_1.setTextViewText(2131558442, "unknown bind");
                    try {
                        android.appwidget.AppWidgetManager.getInstance(this.val$context).updateAppWidget(this.val$appWidgetId, v10_1);
                    } catch (Exception v5_2) {
                        v5_2.printStackTrace();
                    }
                    android.appwidget.AppWidgetManager.getInstance(this.val$context).updateAppWidget(this.val$appWidgetId, v10_1);
                    return;
                }
                try {
                } catch (Exception v5_3) {
                    v5_3.printStackTrace();
                }
            }
        }
        return;
    }
}
