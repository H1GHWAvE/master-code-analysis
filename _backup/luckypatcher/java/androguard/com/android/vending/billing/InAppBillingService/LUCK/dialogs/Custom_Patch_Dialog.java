package com.android.vending.billing.InAppBillingService.LUCK.dialogs;
public class Custom_Patch_Dialog {
    android.app.Dialog dialog;

    public Custom_Patch_Dialog()
    {
        this.dialog = 0;
        return;
    }

    public void dismiss()
    {
        if (this.dialog != null) {
            this.dialog.dismiss();
            this.dialog = 0;
        }
        return;
    }

    public android.app.Dialog onCreateDialog()
    {
        System.out.println("Custom Dialog create.");
        if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag == null) || (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext() == null)) {
            this.dismiss();
        }
        android.widget.LinearLayout v2_1 = ((android.widget.LinearLayout) android.view.View.inflate(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext(), 2130968612, 0));
        try {
            android.widget.LinearLayout v0_1 = ((android.widget.LinearLayout) v2_1.findViewById(2131558539).findViewById(2131558540));
        } catch (Exception v3) {
            v3.printStackTrace();
            this.dismiss();
            com.android.vending.billing.InAppBillingService.LUCK.AlertDlg v1_1 = new com.android.vending.billing.InAppBillingService.LUCK.AlertDlg(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext());
            v1_1.setIcon(2130837548);
            v1_1.setTitle(com.chelpus.Utils.getText(2131165186));
            v1_1.setCancelable(1);
            v1_1.setPositiveButton(com.chelpus.Utils.getText(17039370), 0);
            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.func != 0) {
                v1_1.setNeutralButton(2131165510, new com.android.vending.billing.InAppBillingService.LUCK.dialogs.Custom_Patch_Dialog$1(this));
            }
            v1_1.setView(v2_1);
            return v1_1.create();
        }
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str == null) {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str = " ";
        }
        if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("SU Java-Code Running!")) {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str = "Root not stable. Try again or update your root.";
        }
        if ((!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("Error LP:")) && (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("Object not found!"))) {
            ((android.widget.TextView) v0_1.findViewById(2131558541)).append(com.chelpus.Utils.getColoredText(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str, "#ff00ff73", "bold"));
        }
        if ((!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("Error LP:")) && (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("Object not found!"))) {
        } else {
            ((android.widget.TextView) v0_1.findViewById(2131558541)).append(com.chelpus.Utils.getColoredText(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str, "#ffff0055", "bold"));
        }
    }

    public void showDialog()
    {
        if (this.dialog == null) {
            this.dialog = this.onCreateDialog();
        }
        if (this.dialog != null) {
            this.dialog.show();
        }
        return;
    }
}
