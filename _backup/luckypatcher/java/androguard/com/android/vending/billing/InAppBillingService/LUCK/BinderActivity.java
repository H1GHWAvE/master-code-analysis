package com.android.vending.billing.InAppBillingService.LUCK;
public class BinderActivity extends android.app.Activity {
    public java.util.ArrayList bindes;
    public android.content.Context context;
    private com.android.vending.billing.InAppBillingService.LUCK.BinderActivity$ItemFile current;
    private android.widget.ListView filebrowser;
    public android.widget.ListView lv;
    private android.widget.TextView myPath;
    public android.app.Dialog pp4;
    private String root;
    public int sizeText;

    public BinderActivity()
    {
        this.pp4 = 0;
        this.bindes = 0;
        this.lv = 0;
        this.sizeText = 0;
        return;
    }

    static synthetic android.widget.ListView access$000(com.android.vending.billing.InAppBillingService.LUCK.BinderActivity p1)
    {
        return p1.filebrowser;
    }

    static synthetic android.widget.ListView access$002(com.android.vending.billing.InAppBillingService.LUCK.BinderActivity p0, android.widget.ListView p1)
    {
        p0.filebrowser = p1;
        return p1;
    }

    static synthetic void access$100(com.android.vending.billing.InAppBillingService.LUCK.BinderActivity p0, String p1, android.widget.ListView p2)
    {
        p0.getDir(p1, p2);
        return;
    }

    static synthetic android.widget.TextView access$200(com.android.vending.billing.InAppBillingService.LUCK.BinderActivity p1)
    {
        return p1.myPath;
    }

    static synthetic android.widget.TextView access$202(com.android.vending.billing.InAppBillingService.LUCK.BinderActivity p0, android.widget.TextView p1)
    {
        p0.myPath = p1;
        return p1;
    }

    static synthetic com.android.vending.billing.InAppBillingService.LUCK.BinderActivity$ItemFile access$300(com.android.vending.billing.InAppBillingService.LUCK.BinderActivity p1)
    {
        return p1.current;
    }

    static synthetic com.android.vending.billing.InAppBillingService.LUCK.BinderActivity$ItemFile access$302(com.android.vending.billing.InAppBillingService.LUCK.BinderActivity p0, com.android.vending.billing.InAppBillingService.LUCK.BinderActivity$ItemFile p1)
    {
        p0.current = p1;
        return p1;
    }

    static synthetic String access$400(com.android.vending.billing.InAppBillingService.LUCK.BinderActivity p1)
    {
        return p1.root;
    }

    static synthetic String access$402(com.android.vending.billing.InAppBillingService.LUCK.BinderActivity p0, String p1)
    {
        p0.root = p1;
        return p1;
    }

    public static java.util.ArrayList getBindes(android.content.Context p12)
    {
        java.util.ArrayList v8_1 = new java.util.ArrayList();
        java.io.File v0_1 = new java.io.File(new StringBuilder().append(p12.getDir("binder", 0)).append("/bind.txt").toString());
        try {
            if (v0_1.exists()) {
                java.io.FileInputStream v4_1 = new java.io.FileInputStream(v0_1);
                java.io.BufferedReader v1_1 = new java.io.BufferedReader(new java.io.InputStreamReader(v4_1, "UTF-8"));
                do {
                    String v2 = v1_1.readLine();
                    if (v2 == null) {
                        v4_1.close();
                    } else {
                        String[] v6 = v2.split(";");
                    }
                } while(v6.length != 2);
                System.out.println(new StringBuilder().append(v6[0]).append(" ; ").append(v6[1]).toString());
                v8_1.add(new com.android.vending.billing.InAppBillingService.LUCK.BindItem(v6[0], v6[1]));
            } else {
                v0_1.createNewFile();
            }
        } catch (java.io.IOException v3) {
            System.out.println("Not found bind.txt");
        } catch (java.io.IOException v3) {
            System.out.println(new StringBuilder().append("").append(v3).toString());
        }
        return v8_1;
    }

    private void getDir(String p10, android.widget.ListView p11)
    {
        this.myPath.setText(new StringBuilder().append(com.chelpus.Utils.getText(2131165240)).append(" ").append(p10).toString());
        this.current = new com.android.vending.billing.InAppBillingService.LUCK.BinderActivity$ItemFile(this, "/", p10);
        java.util.ArrayList v5_1 = new java.util.ArrayList();
        java.io.File v0_1 = new java.io.File(p10);
        java.io.File[] v3 = v0_1.listFiles();
        // Both branches of the condition point to the same code.
        // if (v3 != null) {
            if (!p10.equals(this.root)) {
                v5_1.add(new com.android.vending.billing.InAppBillingService.LUCK.BinderActivity$ItemFile(this, this.root));
                v5_1.add(new com.android.vending.billing.InAppBillingService.LUCK.BinderActivity$ItemFile(this, "../", v0_1.getParent()));
            }
            int v4 = 0;
            while (v4 < v3.length) {
                java.io.File v1 = v3[v4];
                if ((v1.canRead()) && ((v1.isDirectory()) || (v1.toString().toLowerCase().endsWith(".apk")))) {
                    v5_1.add(new com.android.vending.billing.InAppBillingService.LUCK.BinderActivity$ItemFile(this, v1.toString()));
                }
                v4++;
            }
            com.android.vending.billing.InAppBillingService.LUCK.BinderActivity$3 v2_1 = new com.android.vending.billing.InAppBillingService.LUCK.BinderActivity$3(this, this, 2130968615, v5_1);
            v2_1.sort(new com.android.vending.billing.InAppBillingService.LUCK.BinderActivity$byNameFile(this, 0));
            p11.setAdapter(v2_1);
            v2_1.notifyDataSetChanged();
            return;
        // }
    }

    public static void savetoFile(java.util.ArrayList p8, android.content.Context p9)
    {
        java.io.File v0_1 = new java.io.File(new StringBuilder().append(p9.getDir("binder", 0)).append("/bind.txt").toString());
        try {
            if (v0_1.exists()) {
                StringBuilder v4_1 = new StringBuilder();
                byte[] v5_6 = p8.iterator();
                while (v5_6.hasNext()) {
                    com.android.vending.billing.InAppBillingService.LUCK.BindItem v3_1 = ((com.android.vending.billing.InAppBillingService.LUCK.BindItem) v5_6.next());
                    v4_1.append(new StringBuilder().append(v3_1.SourceDir).append(";").append(v3_1.TargetDir).append("\n").toString());
                }
                java.io.FileOutputStream v2_1 = new java.io.FileOutputStream(v0_1);
                v2_1.write(v4_1.toString().getBytes());
                v2_1.close();
            } else {
                v0_1.createNewFile();
            }
        } catch (java.io.IOException v1) {
            v1.printStackTrace();
        }
        return;
    }

    public void onCreate(android.os.Bundle p8)
    {
        super.onCreate(p8);
        this.context = this;
        java.io.File v0_1 = new java.io.File(new StringBuilder().append(this.getDir("binder", 0)).append("/bind.txt").toString());
        if (!v0_1.exists()) {
            try {
                v0_1.createNewFile();
            } catch (java.io.IOException v3) {
                v3.printStackTrace();
            }
        }
        this.bindes = com.android.vending.billing.InAppBillingService.LUCK.BinderActivity.getBindes(this.context);
        this.setContentView(2130968586);
        android.widget.Button v1_1 = ((android.widget.Button) this.findViewById(2131558459));
        this.lv = ((android.widget.ListView) this.findViewById(2131558460));
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adaptBind = new com.android.vending.billing.InAppBillingService.LUCK.BinderActivity$1(this, this, 2130968592, this.bindes);
        this.lv.setAdapter(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adaptBind);
        this.lv.invalidateViews();
        this.lv.setBackgroundColor(-16777216);
        v1_1.setOnClickListener(new com.android.vending.billing.InAppBillingService.LUCK.BinderActivity$2(this));
        return;
    }

    protected void onPause()
    {
        super.onPause();
        return;
    }

    protected void onResume()
    {
        super.onResume();
        return;
    }
}
