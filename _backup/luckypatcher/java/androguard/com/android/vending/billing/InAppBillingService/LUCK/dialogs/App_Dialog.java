package com.android.vending.billing.InAppBillingService.LUCK.dialogs;
public class App_Dialog {
    android.app.Dialog dialog;

    public App_Dialog()
    {
        this.dialog = 0;
        return;
    }

    public void dismiss()
    {
        if (this.dialog != null) {
            this.dialog.dismiss();
            this.dialog = 0;
        }
        return;
    }

    public android.app.Dialog onCreateDialog()
    {
        System.out.println("App Dialog create.");
        if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag == null) || (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext() == null)) {
            this.dismiss();
        }
        android.text.SpannableStringBuilder v5_1 = new android.text.SpannableStringBuilder();
        android.text.SpannableStringBuilder v8_1 = new android.text.SpannableStringBuilder();
        android.text.SpannableStringBuilder v6_1 = new android.text.SpannableStringBuilder();
        android.widget.LinearLayout v12_1 = ((android.widget.LinearLayout) android.view.View.inflate(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext(), 2130968584, 0));
        android.widget.LinearLayout v16_1 = ((android.widget.LinearLayout) v12_1.findViewById(2131558447).findViewById(2131558412));
        android.widget.TextView v3_1 = ((android.widget.TextView) v16_1.findViewById(2131558449));
        android.widget.TextView v4_1 = ((android.widget.TextView) v16_1.findViewById(2131558413));
        android.widget.TextView v9_1 = ((android.widget.TextView) v16_1.findViewById(2131558452));
        android.widget.TextView v7_1 = ((android.widget.TextView) v16_1.findViewById(2131558455));
        android.widget.ProgressBar v10_1 = ((android.widget.ProgressBar) v16_1.findViewById(2131558451));
        try {
            ((android.widget.ImageView) v16_1.findViewById(2131558448)).setImageDrawable(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getApplicationIcon(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName));
        } catch (Exception v14_1) {
            v14_1.printStackTrace();
        } catch (Exception v14_0) {
            v14_0.printStackTrace();
        } catch (OutOfMemoryError v11) {
            v11.printStackTrace();
        }
        new Thread(new com.android.vending.billing.InAppBillingService.LUCK.dialogs.App_Dialog$1(this, v3_1, v4_1, v5_1, v6_1, v7_1, v8_1, v9_1, v10_1)).start();
        android.app.Dialog v17 = new com.android.vending.billing.InAppBillingService.LUCK.AlertDlg(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext(), 1).setView(v12_1).create();
        v17.setCancelable(1);
        v17.setOnCancelListener(new com.android.vending.billing.InAppBillingService.LUCK.dialogs.App_Dialog$2(this));
        return v17;
    }

    public void showDialog()
    {
        if (this.dialog == null) {
            this.dialog = this.onCreateDialog();
        }
        if (this.dialog != null) {
            this.dialog.show();
        }
        return;
    }
}
