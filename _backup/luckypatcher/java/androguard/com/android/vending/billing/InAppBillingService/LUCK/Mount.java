package com.android.vending.billing.InAppBillingService.LUCK;
public class Mount {
    protected final java.io.File mDevice;
    protected final java.util.Set mFlags;
    protected final java.io.File mMountPoint;
    protected final String mType;

    public Mount(java.io.File p3, java.io.File p4, String p5, String p6)
    {
        this.mDevice = p3;
        this.mMountPoint = p4;
        this.mType = p5;
        this.mFlags = new java.util.HashSet(java.util.Arrays.asList(p6.split(",")));
        return;
    }

    public java.io.File getDevice()
    {
        return this.mDevice;
    }

    public java.util.Set getFlags()
    {
        return this.mFlags;
    }

    public java.io.File getMountPoint()
    {
        return this.mMountPoint;
    }

    public String getType()
    {
        return this.mType;
    }

    public String toString()
    {
        Object[] v1_1 = new Object[4];
        v1_1[0] = this.mDevice;
        v1_1[1] = this.mMountPoint;
        v1_1[2] = this.mType;
        v1_1[3] = this.mFlags;
        return String.format("%s on %s type %s %s", v1_1);
    }
}
