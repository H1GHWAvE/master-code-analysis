package com.android.vending.billing.InAppBillingService.LUCK;
 class listAppsFragment$45 extends android.os.AsyncTask {
    android.app.ProgressDialog pd;
    final synthetic com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment this$0;

    listAppsFragment$45(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment p1)
    {
        this.this$0 = p1;
        return;
    }

    protected varargs Boolean doInBackground(Void[] p5)
    {
        try {
            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.mLogCollector != null) {
                Boolean v1_4 = Boolean.valueOf(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.mLogCollector.collect(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), 1));
            } else {
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.mLogCollector = new com.android.vending.billing.InAppBillingService.LUCK.LogCollector();
            }
        } catch (Exception v0) {
            v0.printStackTrace();
            v1_4 = Boolean.valueOf(0);
        }
        return v1_4;
    }

    protected bridge synthetic Object doInBackground(Object[] p2)
    {
        return this.doInBackground(((Void[]) p2));
    }

    protected void onPostExecute(Boolean p11)
    {
        try {
            if ((this.pd != null) && (this.pd.isShowing())) {
                this.pd.dismiss();
            }
        } catch (android.content.pm.PackageManager$NameNotFoundException v0_0) {
            v0_0.printStackTrace();
        }
        if (!p11.booleanValue()) {
            android.app.AlertDialog$Builder v1_1 = new android.app.AlertDialog$Builder(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchAct);
            v1_1.setTitle("Error").setMessage(com.chelpus.Utils.getText(2131165433)).setNegativeButton("OK", 0);
            v1_1.create().show();
        } else {
            try {
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.mLogCollector.sendLog(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), "lp.chelpus@gmail.com", "Error Log", new StringBuilder().append("Lucky Patcher ").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext().getPackageName(), 0).versionName).toString());
            } catch (android.content.pm.PackageManager$NameNotFoundException v0_1) {
                v0_1.printStackTrace();
            }
        }
        return;
    }

    protected bridge synthetic void onPostExecute(Object p1)
    {
        this.onPostExecute(((Boolean) p1));
        return;
    }

    protected void onPreExecute()
    {
        this.pd = new android.app.ProgressDialog(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchAct);
        this.pd.setTitle("Progress");
        this.pd.setMessage(com.chelpus.Utils.getText(2131165255));
        this.pd.setIndeterminate(1);
        this.pd.show();
        return;
    }
}
