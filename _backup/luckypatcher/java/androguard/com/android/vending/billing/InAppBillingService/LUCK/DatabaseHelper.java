package com.android.vending.billing.InAppBillingService.LUCK;
public class DatabaseHelper extends android.database.sqlite.SQLiteOpenHelper {
    static final String ads = "ads";
    static final String billing = "billing";
    static final String boot_ads = "boot_ads";
    static final String boot_custom = "boot_custom";
    static final String boot_lvl = "boot_lvl";
    static final String boot_manual = "boot_manual";
    public static android.content.Context contextdb = None;
    static final String custom = "custom";
    public static android.database.sqlite.SQLiteDatabase db = None;
    static final String dbName = "PackagesDB";
    public static boolean getPackage = False;
    static final String hidden = "hidden";
    static final String icon = "icon";
    static final String lvl = "lvl";
    static final String modified = "modified";
    static final String odex = "odex";
    static final String packagesTable = "Packages";
    static final String pkgLabel = "pkgLabel";
    static final String pkgName = "pkgName";
    public static boolean savePackage = False;
    static final String statusi = "statusi";
    static final String stored = "stored";
    static final String storepref = "storepref";
    static final String system = "system";
    static final String updatetime = "updatetime";

    static DatabaseHelper()
    {
        com.android.vending.billing.InAppBillingService.LUCK.DatabaseHelper.contextdb = 0;
        com.android.vending.billing.InAppBillingService.LUCK.DatabaseHelper.db = 0;
        com.android.vending.billing.InAppBillingService.LUCK.DatabaseHelper.getPackage = 0;
        com.android.vending.billing.InAppBillingService.LUCK.DatabaseHelper.savePackage = 0;
        return;
    }

    public DatabaseHelper(android.content.Context p6)
    {
        this(p6, "PackagesDB", 0, 42);
        com.android.vending.billing.InAppBillingService.LUCK.DatabaseHelper.contextdb = p6;
        try {
            com.android.vending.billing.InAppBillingService.LUCK.DatabaseHelper.db = this.getWritableDatabase();
            System.out.println(new StringBuilder().append("SQLite base version is ").append(com.android.vending.billing.InAppBillingService.LUCK.DatabaseHelper.db.getVersion()).toString());
        } catch (android.database.sqlite.SQLiteException v0) {
            v0.printStackTrace();
            return;
        }
        if (com.android.vending.billing.InAppBillingService.LUCK.DatabaseHelper.db.getVersion() == 42) {
            return;
        } else {
            System.out.println("SQL delete and recreate.");
            com.android.vending.billing.InAppBillingService.LUCK.DatabaseHelper.db.execSQL("DROP TABLE IF EXISTS Packages");
            this.onCreate(com.android.vending.billing.InAppBillingService.LUCK.DatabaseHelper.db);
            return;
        }
    }

    public void deletePackage(com.android.vending.billing.InAppBillingService.LUCK.PkgListItem p6)
    {
        try {
            com.android.vending.billing.InAppBillingService.LUCK.DatabaseHelper.db.delete("Packages", new StringBuilder().append("pkgName = \'").append(p6.pkgName).append("\'").toString(), 0);
        } catch (Exception v0) {
            System.out.println(new StringBuilder().append("LuckyPatcher-Error: deletePackage ").append(v0).toString());
        }
        return;
    }

    public void deletePackage(String p6)
    {
        try {
            com.android.vending.billing.InAppBillingService.LUCK.DatabaseHelper.db.delete("Packages", new StringBuilder().append("pkgName = \'").append(p6).append("\'").toString(), 0);
        } catch (Exception v0) {
            System.out.println(new StringBuilder().append("LuckyPatcher-Error: deletePackage ").append(v0).toString());
        }
        return;
    }

    public java.util.ArrayList getPackage(boolean p42, boolean p43)
    {
        java.util.ArrayList v33_1 = new java.util.ArrayList();
        v33_1.clear();
        android.content.pm.PackageManager v35 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng();
        com.android.vending.billing.InAppBillingService.LUCK.DatabaseHelper.getPackage = 1;
        try {
            String v4_0 = new String[19];
            v4_0[0] = "pkgName";
            v4_0[1] = "pkgLabel";
            v4_0[2] = "stored";
            v4_0[3] = "storepref";
            v4_0[4] = "hidden";
            v4_0[5] = "statusi";
            v4_0[6] = "boot_ads";
            v4_0[7] = "boot_lvl";
            v4_0[8] = "boot_custom";
            v4_0[9] = "boot_manual";
            v4_0[10] = "custom";
            v4_0[11] = "lvl";
            v4_0[12] = "ads";
            v4_0[13] = "modified";
            v4_0[14] = "system";
            v4_0[15] = "odex";
            v4_0[16] = "icon";
            v4_0[17] = "updatetime";
            v4_0[18] = "billing";
            android.database.Cursor v27 = com.android.vending.billing.InAppBillingService.LUCK.DatabaseHelper.db.query("Packages", v4_0, 0, 0, 0, 0, 0);
            v27.moveToFirst();
            try {
                do {
                    String v4_1 = v27.getString(v27.getColumnIndexOrThrow("pkgName"));
                } while(
    public boolean isOpen()
    {
        int v0_2;
        if (!com.android.vending.billing.InAppBillingService.LUCK.DatabaseHelper.db.isOpen()) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public void onCreate(android.database.sqlite.SQLiteDatabase p2)
    {
        p2.execSQL("CREATE TABLE Packages (pkgName TEXT PRIMARY KEY, pkgLabel TEXT, stored Integer, storepref Integer, hidden Integer, statusi TEXT, boot_ads Integer, boot_lvl Integer, boot_custom Integer, boot_manual Integer, custom Integer, lvl Integer, ads Integer, modified Integer, system Integer, odex Integer, icon BLOB, updatetime Integer, billing Integer );");
        return;
    }

    public void onUpgrade(android.database.sqlite.SQLiteDatabase p2, int p3, int p4)
    {
        p2.execSQL("DROP TABLE IF EXISTS Packages");
        this.onCreate(p2);
        return;
    }

    public void savePackage(com.android.vending.billing.InAppBillingService.LUCK.PkgListItem p12)
    {
        try {
            com.android.vending.billing.InAppBillingService.LUCK.DatabaseHelper.savePackage = 1;
            android.content.ContentValues v1_1 = new android.content.ContentValues();
            v1_1.put("pkgName", p12.pkgName);
            v1_1.put("pkgLabel", p12.name);
            v1_1.put("stored", Integer.valueOf(p12.stored));
            v1_1.put("storepref", Integer.valueOf(p12.storepref));
            v1_1.put("hidden", Boolean.valueOf(p12.hidden));
            v1_1.put("statusi", p12.statusi);
            v1_1.put("boot_ads", Boolean.valueOf(p12.boot_ads));
            v1_1.put("boot_lvl", Boolean.valueOf(p12.boot_lvl));
            v1_1.put("boot_custom", Boolean.valueOf(p12.boot_custom));
            v1_1.put("boot_manual", Boolean.valueOf(p12.boot_manual));
            v1_1.put("custom", Boolean.valueOf(p12.custom));
            v1_1.put("lvl", Boolean.valueOf(p12.lvl));
            v1_1.put("ads", Boolean.valueOf(p12.ads));
            v1_1.put("modified", Boolean.valueOf(p12.modified));
            v1_1.put("system", Boolean.valueOf(p12.system));
            v1_1.put("odex", Boolean.valueOf(p12.odex));
            v1_1.put("updatetime", Integer.valueOf(p12.updatetime));
            v1_1.put("billing", Boolean.valueOf(p12.billing));
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getApplicationInfo(p12.pkgName, 0);
            android.graphics.Bitmap v3 = 0;
        } catch (Exception v2) {
            com.android.vending.billing.InAppBillingService.LUCK.DatabaseHelper.savePackage = 0;
            System.out.println(new StringBuilder().append("LuckyPatcher-Error: savePackage ").append(v2).toString());
            return;
        }
        try {
            if (p12.icon == null) {
                if (v3 != null) {
                    java.io.ByteArrayOutputStream v4_1 = new java.io.ByteArrayOutputStream();
                    v3.compress(android.graphics.Bitmap$CompressFormat.PNG, 100, v4_1);
                    v1_1.put("icon", v4_1.toByteArray());
                }
            } else {
                v3 = ((android.graphics.drawable.BitmapDrawable) p12.icon).getBitmap();
            }
            try {
                com.android.vending.billing.InAppBillingService.LUCK.DatabaseHelper.db.insertOrThrow("Packages", "pkgName", v1_1);
            } catch (Exception v2) {
                com.android.vending.billing.InAppBillingService.LUCK.DatabaseHelper.db.replace("Packages", 0, v1_1);
            }
            com.android.vending.billing.InAppBillingService.LUCK.DatabaseHelper.savePackage = 0;
            com.android.vending.billing.InAppBillingService.LUCK.DatabaseHelper.savePackage = 0;
            return;
        } catch (int v7) {
        }
    }

    public void updatePackage(java.util.List p1)
    {
        return;
    }
}
