package com.android.vending.billing.InAppBillingService.LUCK;
public class LuckyFileObserver extends android.os.FileObserver {
    public String absolutePath;
    public int i;

    public LuckyFileObserver(String p2)
    {
        this(p2, 4095);
        this.i = 0;
        this.absolutePath = p2;
        return;
    }

    public void onEvent(int p5, String p6)
    {
        System.out.println(new StringBuilder().append(this.absolutePath).append("/").append(p6).append("\n").toString());
        if (p6 != null) {
            if ((p5 & 256) != 0) {
                com.chelpus.Utils.copyFile(new StringBuilder().append("/").append(p6).toString(), new StringBuilder().append("/mnt/sdcard/!Capture").append(this.i).append(".jar").toString(), 0, 1);
                this.i = (this.i + 1);
                System.out.println(new StringBuilder().append(this.absolutePath).append("/").append(p6).append(" is created\n").toString());
            }
            if ((p5 & 32) != 0) {
                System.out.println(new StringBuilder().append(p6).append(" is opened\n").toString());
            }
            if ((p5 & 1) != 0) {
                System.out.println(new StringBuilder().append(this.absolutePath).append("/").append(p6).append(" is accessed/read\n").toString());
            }
            if ((p5 & 2) != 0) {
                System.out.println(new StringBuilder().append(this.absolutePath).append("/").append(p6).append(" is modified\n").toString());
            }
            if ((p5 & 16) != 0) {
                System.out.println(new StringBuilder().append(p6).append(" is closed\n").toString());
            }
            if ((p5 & 8) != 0) {
                System.out.println(new StringBuilder().append(this.absolutePath).append("/").append(p6).append(" is written and closed\n").toString());
            }
            if ((p5 & 512) != 0) {
                System.out.println(new StringBuilder().append(this.absolutePath).append("/").append(p6).append(" is deleted\n").toString());
            }
            if ((p5 & 1024) != 0) {
                System.out.println(new StringBuilder().append(this.absolutePath).append("/").append(" is deleted\n").toString());
            }
            if ((p5 & 64) != 0) {
                System.out.println(new StringBuilder().append(this.absolutePath).append("/").append(p6).append(" is moved to somewhere ").append("\n").toString());
            }
            if ((p5 & 128) != 0) {
                System.out.println(new StringBuilder().append("File is moved to ").append(this.absolutePath).append("/").append(p6).append("\n").toString());
            }
            if ((p5 & 2048) != 0) {
                System.out.println(new StringBuilder().append(p6).append(" is moved\n").toString());
            }
            if ((p5 & 4) != 0) {
                System.out.println(new StringBuilder().append(this.absolutePath).append("/").append(p6).append(" is changed (permissions, owner, timestamp)\n").toString());
            }
        }
        return;
    }
}
