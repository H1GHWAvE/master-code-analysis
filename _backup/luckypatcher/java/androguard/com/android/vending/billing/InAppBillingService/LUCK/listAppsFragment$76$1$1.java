package com.android.vending.billing.InAppBillingService.LUCK;
 class listAppsFragment$76$1$1 extends android.widget.ArrayAdapter {
    final synthetic com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$76$1 this$2;
    android.widget.TextView txtStatus;
    android.widget.TextView txtStatusPatch;
    android.widget.TextView txtTitle;

    listAppsFragment$76$1$1(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$76$1 p1, android.content.Context p2, int p3, java.util.List p4)
    {
        this.this$2 = p1;
        this(p2, p3, p4);
        return;
    }

    public android.view.View getView(int p11, android.view.View p12, android.view.ViewGroup p13)
    {
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.xposedNotify) {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchAct.runOnUiThread(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$76$1$1$1(this));
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.xposedNotify = 0;
        }
        com.android.vending.billing.InAppBillingService.LUCK.CoreItem v3_1 = ((com.android.vending.billing.InAppBillingService.LUCK.CoreItem) this.getItem(p11));
        android.view.View v4 = ((android.view.LayoutInflater) com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getSystemService("layout_inflater")).inflate(2130968607, p13, 0);
        this.txtTitle = ((android.widget.TextView) v4.findViewById(2131558478));
        this.txtStatus = ((android.widget.TextView) v4.findViewById(2131558479));
        this.txtStatusPatch = ((android.widget.TextView) v4.findViewById(2131558529));
        this.txtTitle.setTextAppearance(this.getContext(), com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getSizeText());
        this.txtStatus.setTextAppearance(this.getContext(), com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getSizeText());
        this.txtStatusPatch.setTextAppearance(this.getContext(), com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getSizeText());
        android.widget.CheckBox v0_1 = ((android.widget.CheckBox) v4.findViewById(2131558528));
        v0_1.setChecked(v3_1.Status);
        if (p11 == 0) {
            if (!com.chelpus.Utils.checkCoreJarPatch11()) {
                if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.fileNotSpace) {
                    this.txtStatusPatch.setText(com.chelpus.Utils.getText(2131165373));
                } else {
                    this.txtStatusPatch.setText(com.chelpus.Utils.getText(2131165374));
                }
            } else {
                int v1 = 0;
                if (com.chelpus.Utils.checkCoreJarPatch11()) {
                    v1 = (0 + 1);
                }
                if (com.chelpus.Utils.checkCoreJarPatch12()) {
                    v1++;
                }
                if (v1 == 2) {
                    v0_1.setEnabled(0);
                    v3_1.disabled = 1;
                    this.txtStatusPatch.setText(new StringBuilder().append("2/2 ").append(com.chelpus.Utils.getText(2131165372)).toString());
                }
                if (v1 == 1) {
                    this.txtStatusPatch.setText(new StringBuilder().append("1/2 ").append(com.chelpus.Utils.getText(2131165372)).toString());
                }
            }
        }
        if (p11 == 1) {
            if (!com.chelpus.Utils.checkCoreJarPatch20()) {
                if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.fileNotSpace) {
                    this.txtStatusPatch.setText(com.chelpus.Utils.getText(2131165373));
                } else {
                    this.txtStatusPatch.setText(com.chelpus.Utils.getText(2131165374));
                }
            } else {
                v0_1.setEnabled(0);
                v3_1.disabled = 1;
                this.txtStatusPatch.setText(com.chelpus.Utils.getText(2131165372));
            }
        }
        if (p11 == 2) {
            if ((!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.filePatch3) && (!com.chelpus.Utils.checkCoreJarPatch30(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng()))) {
                if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.fileNotSpace) {
                    this.txtStatusPatch.setText(com.chelpus.Utils.getText(2131165373));
                } else {
                    this.txtStatusPatch.setText(com.chelpus.Utils.getText(2131165374));
                }
            } else {
                v0_1.setEnabled(0);
                v3_1.disabled = 1;
                this.txtStatusPatch.setText(com.chelpus.Utils.getText(2131165372));
            }
        }
        if (p11 == 3) {
            v0_1.setEnabled(0);
            if ((!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.filePatch3) && (!com.chelpus.Utils.checkCoreJarPatch40())) {
                if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.fileNotSpace) {
                    this.txtStatusPatch.setText(com.chelpus.Utils.getText(2131165373));
                } else {
                    this.txtStatusPatch.setText(com.chelpus.Utils.getText(2131165374));
                }
            } else {
                v0_1.setEnabled(0);
                v3_1.disabled = 1;
                this.txtStatusPatch.setText(com.chelpus.Utils.getText(2131165372));
            }
            this.txtStatusPatch.append(com.chelpus.Utils.getColoredText(new StringBuilder().append("\n").append(com.chelpus.Utils.getText(2131165310)).toString(), "#FF0000", "normal"));
        }
        if (p11 == 4) {
            this.txtStatusPatch.setText(com.chelpus.Utils.getColoredText(com.chelpus.Utils.getText(2131165314), "#ff888888", "italic"));
        }
        if (p11 == 5) {
            this.txtStatusPatch.setText(com.chelpus.Utils.getColoredText(com.chelpus.Utils.getText(2131165313), "#ff888888", "italic"));
        }
        v0_1.setClickable(0);
        this.txtStatus.setTextAppearance(this.getContext(), 16973894);
        this.txtStatus.setTextColor(-7829368);
        this.txtTitle.setTextColor(-1);
        this.txtTitle.setText(((com.android.vending.billing.InAppBillingService.LUCK.CoreItem) this.getItem(p11)).Name);
        this.txtTitle.setTypeface(0, 1);
        String v5_2 = ((com.android.vending.billing.InAppBillingService.LUCK.CoreItem) this.getItem(p11)).Name;
        if ((p11 != 0) && (p11 != 1)) {
            if ((p11 != 4) && (p11 != 5)) {
                this.txtTitle.setText(com.chelpus.Utils.getColoredText(v5_2, "#ff00ff00", "bold"));
            } else {
                this.txtTitle.setText(com.chelpus.Utils.getColoredText(v5_2, "#ffff0000", "bold"));
            }
        } else {
            this.txtTitle.setText(com.chelpus.Utils.getColoredText(v5_2, "#ff00ff00", "bold"));
        }
        if (p11 == 0) {
            v5_2 = com.chelpus.Utils.getText(2131165301);
        }
        if (p11 == 1) {
            v5_2 = com.chelpus.Utils.getText(2131165303);
        }
        if (p11 == 2) {
            v5_2 = new StringBuilder().append(com.chelpus.Utils.getText(2131165305)).append("\n").append(com.chelpus.Utils.getText(2131165306)).toString();
        }
        if (p11 == 3) {
            v5_2 = com.chelpus.Utils.getText(2131165308);
        }
        if (p11 == 4) {
            v5_2 = "";
        }
        if (p11 == 5) {
            v5_2 = "";
        }
        this.txtStatus.append(com.chelpus.Utils.getColoredText(v5_2, "#ff888888", "italic"));
        return v4;
    }
}
