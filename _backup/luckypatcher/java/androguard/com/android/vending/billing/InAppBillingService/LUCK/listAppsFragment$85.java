package com.android.vending.billing.InAppBillingService.LUCK;
 class listAppsFragment$85 extends android.widget.ArrayAdapter {
    final synthetic com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment this$0;
    android.widget.TextView txtStatus;
    android.widget.TextView txtTitle;
    final synthetic boolean val$installpatch;

    listAppsFragment$85(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment p1, android.content.Context p2, int p3, java.util.List p4, boolean p5)
    {
        this.this$0 = p1;
        this.val$installpatch = p5;
        this(p2, p3, p4);
        return;
    }

    public android.view.View getView(int p9, android.view.View p10, android.view.ViewGroup p11)
    {
        com.android.vending.billing.InAppBillingService.LUCK.Patterns v2_1 = ((com.android.vending.billing.InAppBillingService.LUCK.Patterns) this.getItem(p9));
        android.view.View v3 = ((android.view.LayoutInflater) com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getSystemService("layout_inflater")).inflate(2130968632, p11, 0);
        this.txtTitle = ((android.widget.TextView) v3.findViewById(2131558478));
        this.txtStatus = ((android.widget.TextView) v3.findViewById(2131558479));
        this.txtTitle.setTextAppearance(this.getContext(), com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getSizeText());
        this.txtStatus.setTextAppearance(this.getContext(), com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getSizeText());
        android.widget.CheckBox v0_1 = ((android.widget.CheckBox) v3.findViewById(2131558528));
        v0_1.setChecked(v2_1.Status);
        v0_1.setClickable(0);
        this.txtStatus.setTextAppearance(this.getContext(), 16973894);
        this.txtStatus.setTextColor(-7829368);
        this.txtTitle.setTextColor(-1);
        this.txtTitle.setText(((com.android.vending.billing.InAppBillingService.LUCK.Patterns) this.getItem(p9)).Name);
        this.txtTitle.setTypeface(0, 1);
        String v4 = ((com.android.vending.billing.InAppBillingService.LUCK.Patterns) this.getItem(p9)).Name;
        if ((p9 != 9) && (p9 != 10)) {
            if (p9 != 11) {
                this.txtTitle.setText(com.chelpus.Utils.getColoredText(v4, "#ff00ff00", "bold"));
            } else {
                this.txtTitle.setText(com.chelpus.Utils.getColoredText(v4, "#ffff0000", "bold"));
            }
        } else {
            this.txtTitle.setText(com.chelpus.Utils.getColoredText(v4, "#ffffff00", "bold"));
        }
        if (p9 == 0) {
            v4 = com.chelpus.Utils.getText(2131165295);
        }
        if (p9 == 1) {
            v4 = com.chelpus.Utils.getText(2131165338);
        }
        if (p9 == 2) {
            v4 = com.chelpus.Utils.getText(2131165340);
        }
        if (p9 == 3) {
            v4 = com.chelpus.Utils.getText(2131165342);
        }
        if (p9 == 4) {
            v4 = com.chelpus.Utils.getText(2131165346);
        }
        if (p9 == 5) {
            v4 = com.chelpus.Utils.getText(2131165348);
        }
        if (p9 == 6) {
            v4 = com.chelpus.Utils.getText(2131165344);
        }
        if (p9 == 7) {
            v4 = com.chelpus.Utils.getText(2131165351);
        }
        if (p9 == 8) {
            v4 = com.chelpus.Utils.getText(2131165320);
        }
        if (this.val$installpatch) {
            if (p9 == 9) {
                v4 = com.chelpus.Utils.getText(2131165359);
            }
            if (p9 == 10) {
                v4 = com.chelpus.Utils.getText(2131165361);
            }
            if (p9 == 11) {
                v4 = com.chelpus.Utils.getText(2131165363);
            }
        }
        this.txtStatus.append(com.chelpus.Utils.getColoredText(v4, "#ff888888", "italic"));
        return v3;
    }
}
