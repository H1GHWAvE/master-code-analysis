package com.android.vending.billing.InAppBillingService.LUCK;
public class PkgListItem {
    public static final int PKG_STORED_EXTERNAL = 1;
    public static final int PKG_STORED_INTERNAL = 0;
    public static final int PKG_STOREPREF_AUTO = 0;
    public static final int PKG_STOREPREF_EXT = 2;
    public static final int PKG_STOREPREF_INT = 1;
    public static final int lastIntPosition = 10;
    public static final int noneAppIntPosition = 6;
    public boolean ads;
    public boolean billing;
    public boolean boot_ads;
    public boolean boot_custom;
    public boolean boot_lvl;
    public boolean boot_manual;
    public boolean custom;
    public boolean enable;
    public boolean hidden;
    public android.graphics.drawable.Drawable icon;
    public boolean lvl;
    public boolean modified;
    public String name;
    public boolean odex;
    public boolean on_sd;
    public String pkgName;
    public boolean selected;
    public String statusi;
    public int stored;
    public int storepref;
    public boolean system;
    public int updatetime;

    public PkgListItem(android.content.Context p3, com.android.vending.billing.InAppBillingService.LUCK.PkgListItem p4)
    {
        this.hidden = 0;
        this.statusi = "";
        this.boot_ads = 0;
        this.boot_lvl = 0;
        this.boot_custom = 0;
        this.boot_manual = 0;
        this.custom = 0;
        this.lvl = 0;
        this.ads = 0;
        this.billing = 0;
        this.modified = 0;
        this.system = 0;
        this.odex = 0;
        this.updatetime = 0;
        this.enable = 0;
        this.selected = 0;
        this.on_sd = 0;
        this.pkgName = p4.pkgName;
        this.name = p4.name;
        this.storepref = p4.storepref;
        this.stored = p4.stored;
        this.hidden = p4.hidden;
        this.statusi = p4.statusi;
        this.boot_ads = p4.boot_ads;
        this.boot_lvl = p4.boot_lvl;
        this.boot_custom = p4.boot_custom;
        this.boot_manual = p4.boot_manual;
        this.custom = p4.custom;
        this.lvl = p4.lvl;
        this.ads = p4.ads;
        this.modified = p4.modified;
        this.on_sd = com.chelpus.Utils.isInstalledOnSdCard(this.pkgName);
        this.system = p4.system;
        this.odex = p4.odex;
        this.icon = p4.icon;
        this.enable = p4.enable;
        if (!this.enable) {
            this.stored = 10;
        }
        this.updatetime = p4.updatetime;
        return;
    }

    public PkgListItem(android.content.Context p37, String p38, int p39, boolean p40)
    {
        com.android.vending.billing.InAppBillingService.LUCK.PkgListItem v36_1 = ;
v36_1.hidden = 0;
        v36_1.statusi = "";
        v36_1.boot_ads = 0;
        v36_1.boot_lvl = 0;
        v36_1.boot_custom = 0;
        v36_1.boot_manual = 0;
        v36_1.custom = 0;
        v36_1.lvl = 0;
        v36_1.ads = 0;
        v36_1.billing = 0;
        v36_1.modified = 0;
        v36_1.system = 0;
        v36_1.odex = 0;
        v36_1.updatetime = 0;
        v36_1.enable = 0;
        v36_1.selected = 0;
        v36_1.on_sd = 0;
        int v25_0 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng();
        android.content.pm.ApplicationInfo v10 = 0;
        if (p40) {
            int v17 = ((int) ((1108082688 * com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDisplayMetrics().density) + 1056964608));
            try {
                int v2 = ((android.graphics.drawable.BitmapDrawable) v25_0.getApplicationIcon(p38)).getBitmap();
                int v5 = v2.getWidth();
                int v6 = v2.getHeight();
                float v30 = (((float) v17) / ((float) v5));
                float v28 = (((float) v17) / ((float) v6));
                int v7_1 = new android.graphics.Matrix();
                v7_1.postScale(v30, v28);
                v36_1.icon = new android.graphics.drawable.BitmapDrawable(android.graphics.Bitmap.createBitmap(v2, 0, 0, v5, v6, v7_1, 1));
            } catch (Exception v14_0) {
                v14_0.printStackTrace();
            } catch (Exception v14) {
            } catch (Exception v14_1) {
                v14_1.printStackTrace();
            }
        }
        v36_1.pkgName = p38;
        v36_1.on_sd = com.chelpus.Utils.isInstalledOnSdCard(v36_1.pkgName);
        v36_1.boot_ads = 0;
        v36_1.boot_lvl = 0;
        v36_1.boot_custom = 0;
        v36_1.boot_manual = 0;
        v36_1.custom = 0;
        v36_1.lvl = 0;
        v36_1.ads = 0;
        v36_1.modified = 0;
        v36_1.system = 0;
        if ((p38 != null) && (p38 != "")) {
            int v25_1 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng();
            try {
                v36_1.pkgName = p38;
                try {
                    if (0 == 0) {
                        v10 = v25_1.getPackageInfo(p38, 0).applicationInfo;
                    }
                } catch (Exception v14) {
                    System.out.println(v36_1.pkgName);
                    throw new IllegalArgumentException("package scan error");
                }
                v36_1.enable = v10.enabled;
                v36_1.name = v25_1.getPackageInfo(p38, 0).applicationInfo.loadLabel(v25_1).toString();
                String v23 = v25_1.getApplicationInfo(v36_1.pkgName, 0).sourceDir;
                v36_1.updatetime = ((int) (com.chelpus.Utils.getfirstInstallTime(v36_1.pkgName, 0) / 1000));
                if (!com.chelpus.Utils.isOdex(v23)) {
                    v36_1.odex = 0;
                } else {
                    v36_1.odex = 1;
                }
                v36_1.stored = 2;
                v36_1.storepref = 0;
                v36_1.modified = com.chelpus.Utils.isModified(v36_1.pkgName, com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance());
                if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.bootlist != null) {
                    int v16 = 0;
                    while (v16 < com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.bootlist.length) {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.bootlist[v16].split("%");
                        String[] v31 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.bootlist[v16].split("%");
                        if (v31[0].endsWith(v36_1.pkgName)) {
                            int v18 = 1;
                            while (v18 < v31.length) {
                                if (v31[v18].contains("ads")) {
                                    v36_1.boot_ads = 1;
                                }
                                if (v31[v18].contains("lvl")) {
                                    v36_1.boot_lvl = 1;
                                }
                                if (v31[v18].contains("custom")) {
                                    v36_1.boot_custom = 1;
                                }
                                v18++;
                            }
                        }
                        v16++;
                    }
                }
                java.io.File[] v19 = p37.getDir("bootlist", 0).listFiles();
                if (v19 != null) {
                    String v4_10 = v19.length;
                    boolean v3_67 = 0;
                    while (v3_67 < v4_10) {
                        java.io.File v9 = v19[v3_67];
                        if ((!v9.getName().endsWith("bootlist")) && (v36_1.pkgName.equals(v9.getName()))) {
                            v36_1.boot_custom = 1;
                        }
                        v3_67++;
                    }
                }
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes();
                try {
                    if (v10 != null) {
                        if ((v10.flags & 1) != 0) {
                            v36_1.system = 1;
                        }
                    } else {
                        v10 = v25_1.getPackageInfo(p38, 0).applicationInfo;
                    }
                    try {
                        android.content.pm.PackageInfo v24_0 = v25_1.getPackageInfo(p38, 4097);
                        int v13_0 = 0;
                    } catch (boolean v3) {
                        if (v24_0 == null) {
                            try {
                                v24_0 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(p38, 4096);
                            } catch (Exception v15_0) {
                                v15_0.printStackTrace();
                            }
                        }
                        if ((v24_0.requestedPermissions != null) && (v24_0.requestedPermissions.length != 0)) {
                            int v13_1 = 0;
                            while (v13_1 < v24_0.requestedPermissions.length) {
                                if ((v24_0.requestedPermissions[v13_1].toUpperCase().contains("BILLING")) && (!v36_1.pkgName.equals(p37.getApplicationContext().getPackageName()))) {
                                    v36_1.billing = 1;
                                    if (!v36_1.lvl) {
                                        v36_1.stored = 3;
                                    }
                                }
                                if ((v24_0.requestedPermissions[v13_1].contains("CHECK_LICENSE")) && (!v36_1.pkgName.equals(p37.getApplicationContext().getPackageName()))) {
                                    if (v36_1.ads) {
                                        v36_1.stored = 2;
                                        v36_1.lvl = 1;
                                        if ((v36_1.lvl) && (v36_1.billing)) {
                                            break;
                                        }
                                    } else {
                                        v36_1.lvl = 1;
                                        v36_1.stored = 2;
                                        if ((v36_1.lvl) && (v36_1.billing)) {
                                            break;
                                        }
                                    }
                                }
                                v13_1++;
                            }
                        }
                        if ((!v36_1.lvl) && ((!v36_1.ads) && (!v36_1.billing))) {
                            v36_1.stored = 6;
                        }
                        try {
                            if (com.chelpus.Utils.isCustomPatchesForPkg(v36_1.pkgName)) {
                                v36_1.stored = 1;
                                v36_1.custom = 1;
                            }
                        } catch (Exception v15_1) {
                            System.out.println(new StringBuilder().append("LuckyPatcher (PkgListItem): Custom patches not found! ").append(v15_1).toString());
                            v15_1.printStackTrace();
                            if (!v36_1.enable) {
                                v36_1.stored = 10;
                            }
                            return;
                        }
                        if (Math.abs((((int) (System.currentTimeMillis() / 1000)) - v36_1.updatetime)) >= (86400 * p39)) {
                        } else {
                            v36_1.stored = 0;
                        }
                        v13_0++;
                    }
                    while (v13_0 < v24_0.activities.length) {
                        if (com.chelpus.Utils.isAds(v24_0.activities[v13_0].name)) {
                            v36_1.stored = 4;
                            v36_1.ads = 1;
                        }
                    }
                } catch (Exception v14_2) {
                    System.out.println(new StringBuilder().append("LuckyPatcher (PkgListItem): ").append(v14_2).toString());
                }
            } catch (Exception v14_3) {
                v14_3.printStackTrace();
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes();
                try {
                    android.content.pm.PackageInfo v24_1 = v25_1.getPackageInfo(p38, 4097);
                    int v13_2 = 0;
                } catch (boolean v3) {
                    if (v24_1 == null) {
                        try {
                            v24_1 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(p38, 4096);
                        } catch (Exception v15_2) {
                            v15_2.printStackTrace();
                        }
                    }
                    if (v24_1.requestedPermissions != null) {
                        if (v24_1.requestedPermissions.length != 0) {
                            int v13_3 = 0;
                        }
                    }
                    if (!v36_1.lvl) {
                        if (!v36_1.ads) {
                            if (!v36_1.billing) {
                                v36_1.stored = 6;
                            }
                        }
                    }
                    try {
                        if (com.chelpus.Utils.isCustomPatchesForPkg(v36_1.pkgName)) {
                            v36_1.stored = 1;
                            v36_1.custom = 1;
                        }
                    } catch (Exception v15_3) {
                        System.out.println(new StringBuilder().append("LuckyPatcher (PkgListItem): Custom patches not found! ").append(v15_3).toString());
                        v15_3.printStackTrace();
                    }
                    if (Math.abs((((int) (System.currentTimeMillis() / 1000)) - v36_1.updatetime)) >= (86400 * p39)) {
                    } else {
                        v36_1.stored = 0;
                    }
                    v13_2++;
                }
            }
        } else {
            throw new IllegalArgumentException("package scan error");
        }
    }

    public PkgListItem(android.content.Context p21, String p22, String p23, int p24, int p25, int p26, String p27, int p28, int p29, int p30, int p31, int p32, int p33, int p34, int p35, int p36, int p37, android.graphics.Bitmap p38, int p39, int p40, boolean p41, boolean p42, boolean p43)
    {
        com.android.vending.billing.InAppBillingService.LUCK.PkgListItem v20_1 = ;
v20_1.hidden = 0;
        v20_1.statusi = "";
        v20_1.boot_ads = 0;
        v20_1.boot_lvl = 0;
        v20_1.boot_custom = 0;
        v20_1.boot_manual = 0;
        v20_1.custom = 0;
        v20_1.lvl = 0;
        v20_1.ads = 0;
        v20_1.billing = 0;
        v20_1.modified = 0;
        v20_1.system = 0;
        v20_1.odex = 0;
        v20_1.updatetime = 0;
        v20_1.enable = 0;
        v20_1.selected = 0;
        v20_1.on_sd = 0;
        v20_1.pkgName = p22;
        v20_1.name = p23;
        v20_1.storepref = p25;
        v20_1.stored = p24;
        v20_1.enable = p41;
        if (!v20_1.enable) {
            v20_1.stored = 10;
        }
        if (p26 != 0) {
            v20_1.hidden = 1;
        } else {
            v20_1.hidden = 0;
        }
        v20_1.statusi = p27;
        if (p28 != 0) {
            v20_1.boot_ads = 1;
        } else {
            v20_1.boot_ads = 0;
        }
        if (p29 != 0) {
            v20_1.boot_lvl = 1;
        } else {
            v20_1.boot_lvl = 0;
        }
        if (p30 != 0) {
            v20_1.boot_custom = 1;
        } else {
            v20_1.boot_custom = 0;
        }
        if (p31 != 0) {
            v20_1.boot_manual = 1;
        } else {
            v20_1.boot_manual = 0;
        }
        if (p32 != 0) {
            v20_1.custom = 1;
        } else {
            v20_1.custom = 0;
        }
        if (p33 != 0) {
            v20_1.lvl = 1;
        } else {
            v20_1.lvl = 0;
        }
        if (p34 != 0) {
            v20_1.ads = 1;
        } else {
            v20_1.ads = 0;
        }
        if (p35 != 0) {
            v20_1.modified = 1;
        } else {
            v20_1.modified = 0;
        }
        v20_1.on_sd = p42;
        if (p36 != 0) {
            v20_1.system = 1;
        } else {
            v20_1.system = 0;
        }
        if (p37 != 0) {
            v20_1.odex = 1;
        } else {
            v20_1.odex = 0;
        }
        if (p40 != 0) {
            v20_1.billing = 1;
        } else {
            v20_1.billing = 0;
        }
        if (!p43) {
            if (p38 == null) {
                int v11 = ((int) ((1108082688 * com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDisplayMetrics().density) + 1056964608));
                try {
                    int v2 = ((android.graphics.drawable.BitmapDrawable) p21.getPackageManager().getApplicationIcon(v20_1.pkgName)).getBitmap();
                    int v5 = v2.getWidth();
                    int v6 = v2.getHeight();
                    float v19 = (((float) v11) / ((float) v5));
                    float v17 = (((float) v11) / ((float) v6));
                    int v7_1 = new android.graphics.Matrix();
                    v7_1.postScale(v19, v17);
                    v20_1.icon = new android.graphics.drawable.BitmapDrawable(android.graphics.Bitmap.createBitmap(v2, 0, 0, v5, v6, v7_1, 1));
                } catch (OutOfMemoryError v10_0) {
                    v10_0.printStackTrace();
                } catch (OutOfMemoryError v10_1) {
                    v10_1.printStackTrace();
                } catch (OutOfMemoryError v10) {
                }
            } else {
                v20_1.updatetime = p39;
                v20_1.icon = new android.graphics.drawable.BitmapDrawable(p38);
            }
        }
        return;
    }

    public boolean equalsPli(com.android.vending.billing.InAppBillingService.LUCK.PkgListItem p6)
    {
        int v1 = 0;
        try {
            if ((this.pkgName.equals(p6.pkgName)) && ((this.name.equals(p6.name)) && ((this.stored == p6.stored) && ((this.storepref == p6.storepref) && ((this.hidden == p6.hidden) && ((this.boot_ads == p6.boot_ads) && ((this.boot_lvl == p6.boot_lvl) && ((this.boot_custom == p6.boot_custom) && ((this.boot_manual == p6.boot_manual) && ((this.custom == p6.custom) && ((this.lvl == p6.lvl) && ((this.ads == p6.ads) && ((this.modified == p6.modified) && ((this.on_sd == p6.on_sd) && ((this.system == p6.system) && ((this.odex == p6.odex) && ((this.enable == p6.enable) && ((this.billing == p6.billing) && (this.updatetime == p6.updatetime))))))))))))))))))) {
                v1 = 1;
            }
        } catch (NullPointerException v0) {
            System.out.println(new StringBuilder().append("LuckyPacther: ").append(p6.pkgName).toString());
            v0.printStackTrace();
        }
        return v1;
    }

    public int getColor()
    {
        int v0 = 2131427329;
        switch (this.stored) {
            case 0:
                v0 = 2131427333;
                break;
            case 1:
                v0 = 2131427328;
                break;
            case 2:
                v0 = 2131427331;
                break;
            case 3:
                v0 = 2131427330;
                break;
        }
        return v0;
    }

    public void saveItem()
    {
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.database == null) {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.database = new com.android.vending.billing.InAppBillingService.LUCK.DatabaseHelper(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance());
        }
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.database.savePackage(this);
        return;
    }

    public String toString()
    {
        return this.name;
    }
}
