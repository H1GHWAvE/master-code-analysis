package com.android.vending.billing.InAppBillingService.LUCK;
 class SetPrefs$21 implements android.preference.Preference$OnPreferenceChangeListener {
    android.app.ProgressDialog progress;
    final synthetic com.android.vending.billing.InAppBillingService.LUCK.SetPrefs this$0;

    SetPrefs$21(com.android.vending.billing.InAppBillingService.LUCK.SetPrefs p2)
    {
        this.this$0 = p2;
        this.progress = 0;
        return;
    }

    public boolean onPreferenceChange(android.preference.Preference p14, Object p15)
    {
        int v9_22;
        android.content.SharedPreferences v5 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getSharedPreferences("config", 0);
        if ((p14 != this.this$0.findPreference("path")) || ((!p15.toString().contains("/")) || (p15.toString().equals("/")))) {
            com.android.vending.billing.InAppBillingService.LUCK.SetPrefs.access$000(this.this$0, this.this$0, com.chelpus.Utils.getText(2131165748), com.chelpus.Utils.getText(2131165444));
            v9_22 = 0;
        } else {
            String[] v8 = p15.toString().trim().replaceAll("\\s+", ".").split("\\/+");
            String v2 = "";
            String v10_7 = v8.length;
            int v9_11 = 0;
            while (v9_11 < v10_7) {
                String v0 = v8[v9_11];
                if (!v0.equals("")) {
                    v2 = new StringBuilder().append(v2).append("/").append(v0).toString();
                }
                v9_11++;
            }
            String v1 = v2;
            if (new java.io.File(v1).exists()) {
                com.android.vending.billing.InAppBillingService.LUCK.SetPrefs.access$000(this.this$0, this.this$0, com.chelpus.Utils.getText(2131165748), com.chelpus.Utils.getText(2131165445));
            } else {
                if ((!this.this$0.testPath(1, v1)) || (v1.startsWith(this.this$0.getDir("sdcard", 0).getAbsolutePath()))) {
                } else {
                    v5.edit().putString("path", v1).commit();
                    v5.edit().putBoolean("manual_path", 1).commit();
                    p14.getEditor().putString("path", v1).commit();
                    v5.edit().putBoolean("path_changed", 1).commit();
                    java.io.File v6_1 = new java.io.File(v5.getString("basepath", "Noting"));
                    java.io.File v3_1 = new java.io.File(v1);
                    if (v6_1.exists()) {
                        this.progress = new android.app.ProgressDialog(this.this$0);
                        this.progress.setCancelable(0);
                        this.progress.setMessage(this.this$0.getString(2131165747));
                        this.progress.show();
                        new Thread(new com.android.vending.billing.InAppBillingService.LUCK.SetPrefs$21$2(this, v6_1, v3_1, new com.android.vending.billing.InAppBillingService.LUCK.SetPrefs$21$1(this, v5, v1))).start();
                    } else {
                        System.out.println("Directory does not exist.");
                    }
                    v9_22 = 1;
                }
            }
        }
        return v9_22;
    }
}
