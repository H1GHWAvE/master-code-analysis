package com.android.vending.billing.InAppBillingService.LUCK;
 class LuckyApp$1 implements java.lang.Thread$UncaughtExceptionHandler {
    final synthetic com.android.vending.billing.InAppBillingService.LUCK.LuckyApp this$0;

    LuckyApp$1(com.android.vending.billing.InAppBillingService.LUCK.LuckyApp p1)
    {
        this.this$0 = p1;
        return;
    }

    public void uncaughtException(Thread p11, Throwable p12)
    {
        System.out.println(new StringBuilder().append("FATAL Exception LP ").append(p12.toString()).toString());
        try {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.init();
        } catch (Exception v0_0) {
            v0_0.printStackTrace();
        }
        p12.printStackTrace();
        int v3 = 0;
        if (this.this$0.getSharedPreferences("config", 4).getBoolean("force_close", 0)) {
            v3 = 1;
        }
        this.this$0.getSharedPreferences("config", 4).edit().putBoolean("force_close", 1).commit();
        try {
            new com.android.vending.billing.InAppBillingService.LUCK.LogCollector().collect(com.android.vending.billing.InAppBillingService.LUCK.LuckyApp.instance, 1);
        } catch (Exception v0_2) {
            v0_2.printStackTrace();
            System.exit(0);
            return;
        } catch (Exception v0_1) {
            System.exit(0);
            v0_1.printStackTrace();
            System.exit(0);
            return;
        }
        if (v3 != 0) {
            System.exit(0);
            return;
        } else {
            this.this$0.startActivity(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getLaunchIntentForPackage(this.this$0.getPackageName()));
            System.exit(0);
            return;
        }
    }
}
