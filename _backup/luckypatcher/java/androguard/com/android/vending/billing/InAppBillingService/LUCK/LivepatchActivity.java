package com.android.vending.billing.InAppBillingService.LUCK;
public class LivepatchActivity extends android.app.Activity {
    private static final int DIALOG_LOAD_FILE = 3;
    private static final String SETTINGS_VIEWSIZE = "viewsize";
    private static final int SETTINGS_VIEWSIZE_SMALL = 0;
    private static final String TAG = "F_PATH";
    public static android.content.Context context;
    static android.text.InputFilter filter;
    static android.text.InputFilter filter2;
    private static android.widget.EditText orhex;
    public static String patch;
    private static android.widget.EditText rephex;
    public static String selabpath;
    public static String selpath;
    public static String str;
    private static android.widget.TextView tv;
    android.widget.ListAdapter adapter;
    private String chosenFile;
    private com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity$Item[] fileList;
    private Boolean firstLvl;
    private java.io.File path;
    public com.android.vending.billing.InAppBillingService.LUCK.PkgListItem pli;
    int start;
    java.util.ArrayList stri;

    static LivepatchActivity()
    {
        com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity.selpath = "";
        com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity.selabpath = "";
        com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity.patch = "";
        com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity.filter = new com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity$1();
        com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity.filter2 = new com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity$2();
        return;
    }

    public LivepatchActivity()
    {
        this.stri = new java.util.ArrayList();
        this.firstLvl = Boolean.valueOf(1);
        this.path = new java.io.File(new StringBuilder().append(android.os.Environment.getExternalStorageDirectory()).append("").toString());
        this.start = 0;
        return;
    }

    static synthetic android.widget.EditText access$000()
    {
        return com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity.rephex;
    }

    static synthetic java.io.File access$100(com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity p1)
    {
        return p1.path;
    }

    static synthetic java.io.File access$102(com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity p0, java.io.File p1)
    {
        p0.path = p1;
        return p1;
    }

    static synthetic void access$200(com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity p0)
    {
        p0.loadFileList();
        return;
    }

    static synthetic String access$300(com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity p1)
    {
        return p1.chosenFile;
    }

    static synthetic String access$302(com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity p0, String p1)
    {
        p0.chosenFile = p1;
        return p1;
    }

    static synthetic com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity$Item[] access$400(com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity p1)
    {
        return p1.fileList;
    }

    static synthetic com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity$Item[] access$402(com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity p0, com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity$Item[] p1)
    {
        p0.fileList = p1;
        return p1;
    }

    static synthetic Boolean access$502(com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity p0, Boolean p1)
    {
        p0.firstLvl = p1;
        return p1;
    }

    static synthetic android.widget.TextView access$600()
    {
        return com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity.tv;
    }

    private void loadFileList()
    {
        String[] v7_0 = new String[1];
        v7_0[0] = "empty";
        try {
            com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity$Item v0_2 = new com.chelpus.Utils("");
            String v1_2 = new String[2];
            v1_2[0] = new StringBuilder().append("cd ").append(this.path).toString();
            v1_2[1] = "ls";
            String[] v7_1 = v0_2.cmdRoot(v1_2).split("\n");
        } catch (Exception v6) {
            v6.printStackTrace();
        }
        com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity$Item v0_5 = new com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity$Item[v7_1.length];
        this.fileList = v0_5;
        int v8 = 0;
        while (v8 < v7_1.length) {
            if ((!v7_1[v8].equals("")) || (v7_1.length != 1)) {
                this.fileList[v8] = new com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity$Item(this, v7_1[v8], Integer.valueOf(2130837547));
                if (!new java.io.File(this.path, v7_1[v8]).isDirectory()) {
                    android.util.Log.d("FILE", this.fileList[v8].file);
                } else {
                    this.fileList[v8].icon = 2130837546;
                    android.util.Log.d("DIRECTORY", this.fileList[v8].file);
                }
            } else {
                this.fileList[v8] = new com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity$Item(this, "Up", Integer.valueOf(2130837545));
            }
            v8++;
        }
        if ((!this.firstLvl.booleanValue()) && ((this.fileList.length != 1) || (!this.fileList[0].file.equals("Up")))) {
            com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity$Item[] v11 = new com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity$Item[(this.fileList.length + 1)];
            System.arraycopy(this.fileList, 0, v11, 1, this.fileList.length);
            v11[0] = new com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity$Item(this, "Up", Integer.valueOf(2130837545));
            this.fileList = v11;
        }
        this.adapter = new com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity$11(this, this, 17367057, 16908308, this.fileList);
        return;
    }

    public void backup_click()
    {
        if (com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity.patch == "dalvik") {
            try {
                android.widget.Toast.makeText(this.getApplication().getApplicationContext(), "Backup processing... Please wait...", 0).show();
                android.widget.TextView v3_5 = new com.chelpus.Utils("");
                android.text.SpannableString v4_4 = new String[1];
                v4_4[0] = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.dalvikruncommand).append(".live_backup ").append(this.pli.pkgName).toString();
                com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity.tv.setText(com.chelpus.Utils.getColoredText(v3_5.cmdRoot(v4_4), "#ff00ff73", "bold"));
            } catch (Exception v0_0) {
                v0_0.printStackTrace();
            }
        }
        if (com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity.patch == "lib") {
            try {
                if (this.getPackageManager().getPackageInfo(this.pli.pkgName, 0).applicationInfo.sourceDir.contains("/mnt/asec/")) {
                    com.chelpus.Utils.remount(this.getPackageManager().getPackageInfo(this.pli.pkgName, 0).applicationInfo.sourceDir.replace("/pkg.apk", ""), "rw");
                }
            } catch (Exception v0_1) {
                v0_1.printStackTrace();
            }
            android.widget.TextView v3_19 = new com.chelpus.Utils("");
            android.text.SpannableString v4_17 = new String[1];
            v4_17[0] = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.dalvikruncommand).append(".live_backuplib ").append(com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity.selpath).toString();
            com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity.str = v3_19.cmdRoot(v4_17);
            if (this.getPackageManager().getPackageInfo(this.pli.pkgName, 0).applicationInfo.sourceDir.contains("/mnt/asec/")) {
                com.chelpus.Utils.remount(this.getPackageManager().getPackageInfo(this.pli.pkgName, 0).applicationInfo.sourceDir.replace("/pkg.apk", ""), "ro");
            }
            String v1_1 = com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity.str;
            if (com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity.str.contains("Error")) {
                com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity.tv.setText(com.chelpus.Utils.getColoredText(v1_1, "#ffff0055", "bold"));
            }
            if (!com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity.str.contains("Error")) {
                com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity.tv.setText(com.chelpus.Utils.getColoredText(v1_1, "#ff00ff73", "bold"));
            }
        }
        return;
    }

    public void launch_click()
    {
        try {
            this.startActivity(this.getPackageManager().getLaunchIntentForPackage(this.pli.pkgName));
        } catch (Exception v0) {
            android.widget.Toast.makeText(this.getApplicationContext(), com.chelpus.Utils.getText(2131165437), 1).show();
        } catch (Exception v0) {
            v0.printStackTrace();
        }
        return;
    }

    public void livepatch_click()
    {
        System.out.println("run patch");
        int v9 = 1;
        String v3 = com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity.orhex.getText().toString();
        String v6 = com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity.rephex.getText().toString();
        if ((v3 != null) && (v6 != null)) {
            String[] v0 = v3.split("\\s+");
            String v12_1 = v0.length;
            android.widget.TextView v10_6 = 0;
            while (v10_6 < v12_1) {
                if (!java.util.regex.Pattern.matches("([0-9A-Fa-f*?]){2}||([0-9A-Fa-f*?]){1}", v0[v10_6])) {
                    v9 = 0;
                    android.widget.Toast.makeText(this.getApplication().getBaseContext(), com.chelpus.Utils.getText(2131165442), 0).show();
                }
                v10_6++;
            }
            String[] v1 = v6.split("\\s+");
            String v12_2 = v1.length;
            android.widget.TextView v10_8 = 0;
            while (v10_8 < v12_2) {
                if (!java.util.regex.Pattern.matches("([0-9A-Fa-f*?]){2}||([0-9A-Fa-f*?]){1}", v1[v10_8])) {
                    v9 = 0;
                    android.widget.Toast.makeText(this.getApplication().getBaseContext(), com.chelpus.Utils.getText(2131165446), 0).show();
                }
                v10_8++;
            }
            if (v0.length != v1.length) {
                v9 = 0;
                android.widget.Toast.makeText(this.getApplication().getBaseContext(), com.chelpus.Utils.getText(2131165438), 0).show();
            }
        }
        if (v9 != 0) {
            if (com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity.patch == "dalvik") {
                try {
                    android.widget.Toast.makeText(this.getApplication().getBaseContext(), "Search bytes... Please wait...", 0).show();
                    android.widget.TextView v10_18 = new com.chelpus.Utils("");
                    android.text.SpannableString v11_5 = new String[1];
                    v11_5[0] = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.dalvikruncommand).append(".liverunpatch ").append(this.pli.pkgName).append(" ").append("\"").append(v3).append("\"").append(" ").append("\"").append(v6).append("\"").toString();
                    com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity.str = v10_18.cmdRoot(v11_5);
                    String v7_0 = com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity.str;
                } catch (Exception v2_0) {
                    v2_0.printStackTrace();
                }
                if (com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity.str.contains("Error")) {
                    com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity.tv.setText(com.chelpus.Utils.getColoredText(v7_0, "#ffff0055", "bold"));
                }
                if (!com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity.str.contains("Error")) {
                    com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity.tv.setText(com.chelpus.Utils.getColoredText(v7_0, "#ff00ff73", "bold"));
                }
            }
            if (com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity.patch == "lib") {
                try {
                    android.widget.Toast.makeText(this.getApplication().getApplicationContext(), "Search bytes... Please wait...", 0).show();
                } catch (Exception v2_1) {
                    v2_1.printStackTrace();
                }
                if (this.getPackageManager().getPackageInfo(this.pli.pkgName, 0).applicationInfo.sourceDir.contains("/mnt/asec/")) {
                    com.chelpus.Utils.remount(this.getPackageManager().getPackageInfo(this.pli.pkgName, 0).applicationInfo.sourceDir.replace("/pkg.apk", ""), "rw");
                }
                android.widget.TextView v10_41 = new com.chelpus.Utils("");
                android.text.SpannableString v11_23 = new String[1];
                v11_23[0] = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.dalvikruncommand).append(".liverunpatchlib ").append(com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity.selpath).append(" ").append("\"").append(v3).append("\"").append(" ").append("\"").append(v6).append("\"").toString();
                com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity.str = v10_41.cmdRoot(v11_23);
                if (this.getPackageManager().getPackageInfo(this.pli.pkgName, 0).applicationInfo.sourceDir.contains("/mnt/asec/")) {
                    com.chelpus.Utils.remount(this.getPackageManager().getPackageInfo(this.pli.pkgName, 0).applicationInfo.sourceDir.replace("/pkg.apk", ""), "ro");
                }
                String v7_1 = com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity.str;
                this.getApplication().getApplicationContext().getResources();
                if (com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity.str.contains("Error")) {
                    com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity.tv.setText(com.chelpus.Utils.getColoredText(v7_1, "#ffff0055", "bold"));
                }
                if (!com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity.str.contains("Error")) {
                    com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity.tv.setText(com.chelpus.Utils.getColoredText(v7_1, "#ff00ff73", "bold"));
                }
            }
        }
        return;
    }

    public void onCreate(android.os.Bundle p13)
    {
        super.onCreate(p13);
        com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity.context = this.getApplicationContext();
        this.setContentView(2130968623);
        this.pli = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli;
        android.widget.LinearLayout v1_1 = ((android.widget.LinearLayout) this.findViewById(2131558596).findViewById(2131558597));
        com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity.patch = "dalvik";
        com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity.orhex = ((android.widget.EditText) this.findViewById(2131558590));
        com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity.rephex = ((android.widget.EditText) this.findViewById(2131558591));
        android.text.SpannableString v9_2 = new android.text.InputFilter[1];
        v9_2[0] = com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity.filter;
        com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity.orhex.setFilters(v9_2);
        com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity.orhex.addTextChangedListener(new com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity$3(this));
        com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity.tv = ((android.widget.TextView) v1_1.findViewById(2131558598));
        android.content.res.Resources v4 = this.getApplication().getApplicationContext().getResources();
        com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity.str = v4.getString(2131165560);
        com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity.tv.setText(com.chelpus.Utils.getColoredText(new StringBuilder().append(v4.getString(2131165406)).append("\n\n").append(com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity.str).toString(), "#ff00ff73", "bold"));
        ((android.widget.Button) this.findViewById(2131558592)).setOnClickListener(new com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity$4(this));
        ((android.widget.Button) this.findViewById(2131558595)).setOnClickListener(new com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity$5(this));
        ((android.widget.Button) this.findViewById(2131558593)).setOnClickListener(new com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity$6(this));
        ((android.widget.Button) this.findViewById(2131558594)).setOnClickListener(new com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity$7(this));
        ((android.widget.Button) this.findViewById(2131558599)).setOnClickListener(new com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity$8(this));
        return;
    }

    protected android.app.Dialog onCreateDialog(int p6)
    {
        android.app.AlertDialog v1_1;
        switch (p6) {
            case 3:
                new android.app.Dialog(this);
                android.widget.ListView v2_1 = new android.widget.ListView(this);
                android.app.AlertDialog$Builder v0_1 = new android.app.AlertDialog$Builder(this);
                if (this.fileList != null) {
                    v0_1.setTitle("Choose your file");
                    if (this.adapter != null) {
                        v2_1.setAdapter(this.adapter);
                        v2_1.invalidateViews();
                        v2_1.setCacheColorHint(-16777216);
                        v2_1.setBackgroundColor(-16777216);
                        v2_1.setOnItemClickListener(new com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity$9(this));
                        v0_1.setView(v2_1);
                    }
                    v1_1 = v0_1.setOnCancelListener(new com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity$10(this)).setCancelable(1).create();
                } else {
                    android.util.Log.e("F_PATH", "No files loaded");
                    v1_1 = v0_1.create();
                }
                break;
            default:
                v1_1 = 0;
        }
        return v1_1;
    }

    public void onRestart()
    {
        super.onRestart();
        return;
    }

    public void onStop()
    {
        super.onStop();
        return;
    }

    public void restore_click()
    {
        if (com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity.patch == "dalvik") {
            try {
                android.widget.Toast.makeText(this.getApplication().getApplicationContext(), "Restore processing... Please wait...", 0).show();
                android.widget.TextView v3_5 = new com.chelpus.Utils("");
                android.text.SpannableString v4_4 = new String[1];
                v4_4[0] = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.dalvikruncommand).append(".live_restore ").append(this.pli.pkgName).toString();
                com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity.tv.setText(com.chelpus.Utils.getColoredText(v3_5.cmdRoot(v4_4), "#ff00ff73", "bold"));
            } catch (Exception v0_0) {
                v0_0.printStackTrace();
            }
        }
        if (com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity.patch == "lib") {
            try {
                if (this.getPackageManager().getPackageInfo(this.pli.pkgName, 0).applicationInfo.sourceDir.contains("/mnt/asec/")) {
                    com.chelpus.Utils.remount(this.getPackageManager().getPackageInfo(this.pli.pkgName, 0).applicationInfo.sourceDir.replace("/pkg.apk", ""), "rw");
                }
            } catch (Exception v0_1) {
                v0_1.printStackTrace();
            }
            android.widget.TextView v3_19 = new com.chelpus.Utils("");
            android.text.SpannableString v4_17 = new String[1];
            v4_17[0] = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.dalvikruncommand).append(".live_restorelib ").append(com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity.selpath).toString();
            com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity.str = v3_19.cmdRoot(v4_17);
            if (this.getPackageManager().getPackageInfo(this.pli.pkgName, 0).applicationInfo.sourceDir.contains("/mnt/asec/")) {
                com.chelpus.Utils.remount(this.getPackageManager().getPackageInfo(this.pli.pkgName, 0).applicationInfo.sourceDir.replace("/pkg.apk", ""), "ro");
            }
            String v1_1 = com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity.str;
            if (com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity.str.contains("Error")) {
                com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity.tv.setText(com.chelpus.Utils.getColoredText(v1_1, "#ffff0055", "bold"));
            }
            if (!com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity.str.contains("Error")) {
                com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity.tv.setText(com.chelpus.Utils.getColoredText(v1_1, "#ff00ff73", "bold"));
            }
        }
        return;
    }
}
