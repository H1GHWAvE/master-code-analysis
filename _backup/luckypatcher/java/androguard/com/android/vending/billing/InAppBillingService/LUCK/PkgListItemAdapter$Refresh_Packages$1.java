package com.android.vending.billing.InAppBillingService.LUCK;
 class PkgListItemAdapter$Refresh_Packages$1 implements java.lang.Runnable {
    final synthetic com.android.vending.billing.InAppBillingService.LUCK.PkgListItemAdapter$Refresh_Packages this$1;

    PkgListItemAdapter$Refresh_Packages$1(com.android.vending.billing.InAppBillingService.LUCK.PkgListItemAdapter$Refresh_Packages p1)
    {
        this.this$1 = p1;
        return;
    }

    public void run()
    {
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.refresh = 0;
        try {
            java.util.Iterator v2_4 = this.this$1.this$0.add.iterator();
        } catch (Exception v0) {
            System.out.println("LuckyPatcher (AddApps): on progress adapter not found!");
            v0.printStackTrace();
            return;
        }
        while (v2_4.hasNext()) {
            com.android.vending.billing.InAppBillingService.LUCK.PkgListItem v1_3 = ((com.android.vending.billing.InAppBillingService.LUCK.PkgListItem) v2_4.next());
            if (this.this$1.this$0.getItem(v1_3.pkgName) == null) {
                this.this$1.this$0.add(v1_3);
                this.this$1.this$0.notifyDataSetChanged();
                this.this$1.this$0.sort();
                this.this$1.this$0.notifyDataSetChanged();
            }
        }
        java.util.Iterator v2_8 = this.this$1.this$0.del.iterator();
        while (v2_8.hasNext()) {
            com.android.vending.billing.InAppBillingService.LUCK.PkgListItem v1_1 = ((com.android.vending.billing.InAppBillingService.LUCK.PkgListItem) v2_8.next());
            if (this.this$1.this$0.getItem(v1_1.pkgName) != null) {
                this.this$1.this$0.remove(v1_1.pkgName);
                this.this$1.this$0.notifyDataSetChanged();
                this.this$1.this$0.sort();
                this.this$1.this$0.notifyDataSetChanged();
            }
        }
        return;
    }
}
