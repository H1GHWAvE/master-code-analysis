package com.android.vending.billing.InAppBillingService.LUCK;
public class PackageChangeReceiver extends android.content.BroadcastReceiver {
    static android.content.ServiceConnection mServiceConn;
    static android.content.ServiceConnection mServiceConnL;
    boolean hackedBilling;
    android.os.Handler handler;
    com.google.android.finsky.billing.iab.google.util.IInAppBillingService mService;
    com.android.vending.licensing.ILicensingService mServiceL;
    boolean mSetupDone;
    int responseCode;

    public PackageChangeReceiver()
    {
        this.handler = 0;
        this.mSetupDone = 0;
        this.hackedBilling = 0;
        this.responseCode = 255;
        return;
    }

    private void cleanupService()
    {
        if (this.mServiceL != null) {
            try {
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().unbindService(com.android.vending.billing.InAppBillingService.LUCK.PackageChangeReceiver.mServiceConnL);
            } catch (int v0) {
            }
            this.mServiceL = 0;
        }
        return;
    }

    public String backup(String p10)
    {
        String v2 = "";
        try {
            if (this.testSD()) {
                new java.io.File(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).append("/Backup/").toString()).mkdirs();
                String v0;
                if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getInt("apkname", 0) == 0) {
                    v0 = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).append("/Backup/").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(p10, 0).applicationInfo.loadLabel(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng()).toString()).append(".ver.").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(p10, 0).versionName.replaceAll(" ", ".")).append(".build.").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(p10, 0).versionCode).append(".apk").toString();
                } else {
                    v0 = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).append("/Backup/").append(p10).append(".ver.").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(p10, 0).versionName.replaceAll(" ", ".")).append(".build.").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(p10, 0).versionCode).append(".apk").toString();
                }
                if (new java.io.File(v0).exists()) {
                    new java.io.File(v0).delete();
                }
                try {
                    com.chelpus.Utils.copyFile(new java.io.File(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(p10, 0).applicationInfo.sourceDir), new java.io.File(v0));
                } catch (Exception v1_0) {
                    com.chelpus.Utils.copyFile(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(p10, 0).applicationInfo.sourceDir, v0, 0, 0);
                    v1_0.printStackTrace();
                }
                if (!new java.io.File(v0).exists()) {
                    android.os.Handler v3_40 = new com.chelpus.Utils("");
                    com.android.vending.billing.InAppBillingService.LUCK.PackageChangeReceiver$5 v4_45 = new String[1];
                    v4_45[0] = new StringBuilder().append("dd if=").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(p10, 0).applicationInfo.sourceDir).append(" of=").append(v0).toString();
                    v3_40.cmdRoot(v4_45);
                    com.chelpus.Utils.run_all(new StringBuilder().append("chmod 777 ").append(v0).toString());
                }
                if (!new java.io.File(v0).exists()) {
                    this.handler.post(new com.android.vending.billing.InAppBillingService.LUCK.PackageChangeReceiver$5(this));
                } else {
                    this.handler.post(new com.android.vending.billing.InAppBillingService.LUCK.PackageChangeReceiver$4(this));
                    v2 = v0;
                }
            }
        } catch (Exception v1_2) {
            v1_2.printStackTrace();
        } catch (Exception v1_1) {
            v1_1.printStackTrace();
        }
        return v2;
    }

    public void connectToBilling()
    {
        if (this.mSetupDone) {
            android.content.Intent v2_1 = new android.content.Intent("com.android.vending.billing.InAppBillingService.BIND");
            try {
                if (com.android.vending.billing.InAppBillingService.LUCK.PackageChangeReceiver.mServiceConn != null) {
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().unbindService(com.android.vending.billing.InAppBillingService.LUCK.PackageChangeReceiver.mServiceConn);
                }
            } catch (Exception v0) {
                v0.printStackTrace();
            }
            this.mSetupDone = 0;
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().bindService(v2_1, com.android.vending.billing.InAppBillingService.LUCK.PackageChangeReceiver.mServiceConn, 1);
        }
        com.android.vending.billing.InAppBillingService.LUCK.PackageChangeReceiver.mServiceConn = new com.android.vending.billing.InAppBillingService.LUCK.PackageChangeReceiver$2(this);
        android.content.Intent v2_3 = new android.content.Intent("com.android.vending.billing.InAppBillingService.BIND");
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().queryIntentServices(v2_3, 0).isEmpty()) {
            System.out.println("Billing service unavailable on device.");
            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su) {
                com.chelpus.Utils.market_billing_services(1);
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().bindService(v2_3, com.android.vending.billing.InAppBillingService.LUCK.PackageChangeReceiver.mServiceConn, 1);
                android.content.Intent v1_1 = new android.content.Intent(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), com.android.vending.billing.InAppBillingService.LUCK.widgets.inapp_widget);
                v1_1.setAction(com.android.vending.billing.InAppBillingService.LUCK.widgets.inapp_widget.ACTION_WIDGET_RECEIVER_Updater);
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().sendBroadcast(v1_1);
            }
        } else {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().bindService(v2_3, com.android.vending.billing.InAppBillingService.LUCK.PackageChangeReceiver.mServiceConn, 1);
        }
        return;
    }

    public void connectToLicensing()
    {
        com.android.vending.billing.InAppBillingService.LUCK.PackageChangeReceiver.mServiceConnL = new com.android.vending.billing.InAppBillingService.LUCK.PackageChangeReceiver$3(this);
        try {
            if ((this.mServiceL == null) && (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().bindService(new android.content.Intent(new String(com.google.android.vending.licensing.util.Base64.decode("Y29tLmFuZHJvaWQudmVuZGluZy5saWNlbnNpbmcuSUxpY2Vuc2luZ1NlcnZpY2U="))), com.android.vending.billing.InAppBillingService.LUCK.PackageChangeReceiver.mServiceConnL, 1))) {
                this.cleanupService();
            }
        } catch (com.google.android.vending.licensing.util.Base64DecoderException v1) {
            v1.printStackTrace();
            this.cleanupService();
        } catch (com.google.android.vending.licensing.util.Base64DecoderException v1) {
            this.cleanupService();
        }
        return;
    }

    String getPackageName(android.content.Intent p3)
    {
        int v0;
        android.net.Uri v1 = p3.getData();
        if (v1 == null) {
            v0 = 0;
        } else {
            v0 = v1.getSchemeSpecificPart();
        }
        return v0;
    }

    public void onReceive(android.content.Context p3, android.content.Intent p4)
    {
        this.handler = new android.os.Handler();
        new Thread(new com.android.vending.billing.InAppBillingService.LUCK.PackageChangeReceiver$1(this, p4, p3)).start();
        return;
    }

    public boolean testSD()
    {
        int v1 = 0;
        if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath.startsWith(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getDir("sdcard", 0).getAbsolutePath())) {
            if (!new java.io.File(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).exists()) {
                new java.io.File(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).mkdirs();
            }
            if (new java.io.File(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).exists()) {
                new java.io.File(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).append("/tmp.txt").toString()).delete();
                System.out.println(new StringBuilder().append("LuckyAppManager (sdcard test create file): ").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).toString());
                if (new java.io.File(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).append("/tmp.txt").toString()).createNewFile()) {
                    System.out.println(new StringBuilder().append("LuckyAppManager (sdcard test create file true): ").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).toString());
                    new java.io.File(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).append("/tmp.txt").toString()).delete();
                    v1 = 1;
                }
            } else {
                System.out.println(new StringBuilder().append("LuckyAppManager (sdcard directory not found and not created): ").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).toString());
            }
        } else {
            System.out.println(new StringBuilder().append("LuckyAppManager (sdcard to internal memory): ").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).toString());
        }
        return v1;
    }
}
