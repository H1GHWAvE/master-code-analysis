package com.android.vending.billing.InAppBillingService.LUCK;
public abstract class ITestServiceInterface$Stub extends android.os.Binder implements com.android.vending.billing.InAppBillingService.LUCK.ITestServiceInterface {
    private static final String DESCRIPTOR = "com.android.vending.billing.InAppBillingService.LUCK.ITestServiceInterface";
    static final int TRANSACTION_checkService = 1;

    public ITestServiceInterface$Stub()
    {
        this.attachInterface(this, "com.android.vending.billing.InAppBillingService.LUCK.ITestServiceInterface");
        return;
    }

    public static com.android.vending.billing.InAppBillingService.LUCK.ITestServiceInterface asInterface(android.os.IBinder p2)
    {
        com.android.vending.billing.InAppBillingService.LUCK.ITestServiceInterface v0_2;
        if (p2 != null) {
            com.android.vending.billing.InAppBillingService.LUCK.ITestServiceInterface v0_0 = p2.queryLocalInterface("com.android.vending.billing.InAppBillingService.LUCK.ITestServiceInterface");
            if ((v0_0 == null) || (!(v0_0 instanceof com.android.vending.billing.InAppBillingService.LUCK.ITestServiceInterface))) {
                v0_2 = new com.android.vending.billing.InAppBillingService.LUCK.ITestServiceInterface$Stub$Proxy(p2);
            } else {
                v0_2 = ((com.android.vending.billing.InAppBillingService.LUCK.ITestServiceInterface) v0_0);
            }
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    public android.os.IBinder asBinder()
    {
        return this;
    }

    public boolean onTransact(int p4, android.os.Parcel p5, android.os.Parcel p6, int p7)
    {
        boolean v2 = 1;
        switch (p4) {
            case 1:
                int v1_2;
                p5.enforceInterface("com.android.vending.billing.InAppBillingService.LUCK.ITestServiceInterface");
                boolean v0 = this.checkService();
                p6.writeNoException();
                if (!v0) {
                    v1_2 = 0;
                } else {
                    v1_2 = 1;
                }
                p6.writeInt(v1_2);
                break;
            case 1598968902:
                p6.writeString("com.android.vending.billing.InAppBillingService.LUCK.ITestServiceInterface");
                break;
            default:
                v2 = super.onTransact(p4, p5, p6, p7);
        }
        return v2;
    }
}
