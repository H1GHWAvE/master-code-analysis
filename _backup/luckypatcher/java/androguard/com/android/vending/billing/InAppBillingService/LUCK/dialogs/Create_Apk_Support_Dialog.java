package com.android.vending.billing.InAppBillingService.LUCK.dialogs;
public class Create_Apk_Support_Dialog {
    android.app.Dialog dialog;
    String resultFile;

    public Create_Apk_Support_Dialog()
    {
        this.dialog = 0;
        this.resultFile = "";
        return;
    }

    public void dismiss()
    {
        if (this.dialog != null) {
            this.dialog.dismiss();
            this.dialog = 0;
        }
        return;
    }

    public android.app.Dialog onCreateDialog()
    {
        System.out.println("Create apk support Dialog create.");
        if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag == null) || (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext() == null)) {
            this.dismiss();
        }
        android.widget.LinearLayout v3_1 = ((android.widget.LinearLayout) android.view.View.inflate(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext(), 2130968612, 0));
        android.widget.LinearLayout v2_1 = ((android.widget.LinearLayout) v3_1.findViewById(2131558539).findViewById(2131558540));
        try {
            String v6 = new StringBuilder().append(".v.").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName, 0).versionName).append(".b.").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName, 0).versionCode).toString();
        } catch (NullPointerException v4) {
            v4.printStackTrace();
        } catch (NullPointerException v4) {
            this.dismiss();
        }
        android.widget.TextView v5_1 = ((android.widget.TextView) v2_1.findViewById(2131558541));
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str == null) {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str = " ";
        }
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patch_dialog_text_builder(v5_1, 1);
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli != null) {
            this.resultFile = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).append("/Modified/").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.name.replaceAll(" ", ".").replaceAll("/", ".")).append("/").toString();
            java.io.File v0_1 = new java.io.File(new StringBuilder().append(this.resultFile).append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.name.replaceAll(" ", ".").replaceAll("/", ".")).append(v6).append(".crk.Support.apk").toString());
            java.io.File v1_1 = new java.io.File(new StringBuilder().append(this.resultFile).append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName).append(v6).append(".crk.Support.apk").toString());
            if ((!v0_1.exists()) && (!v1_1.exists())) {
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str = new StringBuilder().append(com.chelpus.Utils.getText(2131165390)).append(" ").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.name).append(com.chelpus.Utils.getText(2131165395)).toString();
                ((android.widget.TextView) v2_1.findViewById(2131558541)).append(com.chelpus.Utils.getColoredText(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str, -65451, "bold"));
            } else {
                if ((v0_1.exists()) && (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getInt("apkname", 0) == 0)) {
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str = new StringBuilder().append(com.chelpus.Utils.getText(2131165390)).append(" ").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.name).append(" ").append(com.chelpus.Utils.getText(2131165392)).append("\n").append(this.resultFile).append("\n\n").append(com.chelpus.Utils.getText(2131165396)).append("\n").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.name.replaceAll(" ", ".").replaceAll("/", ".")).append(v6).append(".crk.Support.apk").append(com.chelpus.Utils.getText(2131165393)).toString();
                }
                if ((v1_1.exists()) && (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getInt("apkname", 0) == 1)) {
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str = new StringBuilder().append(com.chelpus.Utils.getText(2131165390)).append(" ").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.name).append(" ").append(com.chelpus.Utils.getText(2131165392)).append("\n").append(this.resultFile).append("\n\n").append(com.chelpus.Utils.getText(2131165396)).append("\n").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName).append(v6).append(".crk.Support.apk").append(com.chelpus.Utils.getText(2131165393)).toString();
                }
                ((android.widget.TextView) v2_1.findViewById(2131558541)).append(com.chelpus.Utils.getColoredText(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str, -990142, "bold"));
            }
        }
        return new com.android.vending.billing.InAppBillingService.LUCK.AlertDlg(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext()).setTitle(com.chelpus.Utils.getText(2131165186)).setCancelable(1).setIcon(2130837548).setPositiveButton(com.chelpus.Utils.getText(17039370), 0).setNeutralButton(com.chelpus.Utils.getText(2131165476), new com.android.vending.billing.InAppBillingService.LUCK.dialogs.Create_Apk_Support_Dialog$1(this)).setView(v3_1).create();
    }

    public void showDialog()
    {
        if (this.dialog == null) {
            this.dialog = this.onCreateDialog();
        }
        if (this.dialog != null) {
            this.dialog.show();
        }
        return;
    }
}
