package com.android.vending.billing.InAppBillingService.LUCK;
public class BootListItemAdapter extends android.widget.ArrayAdapter {
    public static final int TEXT_DEFAULT = 0;
    public static final int TEXT_LARGE = 2;
    public static final int TEXT_MEDIUM = 1;
    public static final int TEXT_SMALL;
    android.content.Context context;
    java.util.List data;
    android.widget.ImageView imgIcon;
    int layoutResourceId;
    int size;
    public java.util.Comparator sorter;
    android.widget.TextView txtStatus;
    android.widget.TextView txtTitle;

    public BootListItemAdapter(android.content.Context p1, int p2, int p3, java.util.List p4)
    {
        this(p1, p2, p4);
        this.context = p1;
        this.layoutResourceId = p2;
        this.size = p3;
        return;
    }

    public BootListItemAdapter(android.content.Context p2, int p3, java.util.List p4)
    {
        this(p2, p3, p4);
        this.context = p2;
        this.layoutResourceId = p3;
        this.size = 0;
        this.data = p4;
        return;
    }

    public com.android.vending.billing.InAppBillingService.LUCK.PkgListItem getItem(String p3)
    {
        int v0 = 0;
        while (v0 < this.getCount()) {
            if (!((com.android.vending.billing.InAppBillingService.LUCK.PkgListItem) this.getItem(v0)).pkgName.contentEquals(p3)) {
                v0++;
            } else {
                com.android.vending.billing.InAppBillingService.LUCK.PkgListItem v1_1 = ((com.android.vending.billing.InAppBillingService.LUCK.PkgListItem) this.getItem(v0));
            }
            return v1_1;
        }
        v1_1 = 0;
        return v1_1;
    }

    public android.view.View getView(int p13, android.view.View p14, android.view.ViewGroup p15)
    {
        android.view.View v3_0;
        com.android.vending.billing.InAppBillingService.LUCK.PkgListItem v4_1 = ((com.android.vending.billing.InAppBillingService.LUCK.PkgListItem) this.getItem(p13));
        p15.setBackgroundColor(-16777216);
        if ((v4_1.name == null) || ((v4_1.boot_ads) || ((v4_1.boot_custom) || ((v4_1.boot_lvl) || (v4_1.boot_manual))))) {
            android.view.View v6 = ((android.app.Activity) this.context).getLayoutInflater().inflate(this.layoutResourceId, p15, 0);
            v6.setBackgroundColor(-16777216);
            this.txtTitle = ((android.widget.TextView) v6.findViewById(2131558478));
            this.txtStatus = ((android.widget.TextView) v6.findViewById(2131558479));
            this.imgIcon = ((android.widget.ImageView) v6.findViewById(2131558456));
            this.txtTitle.setText(v4_1.name);
            this.imgIcon.setMaxHeight(1);
            this.imgIcon.setMaxWidth(1);
            try {
                this.imgIcon.setImageDrawable(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getApplicationIcon(v4_1.pkgName));
            } catch (android.content.pm.PackageManager$NameNotFoundException v1) {
                v1.printStackTrace();
            }
            this.txtTitle.setTextAppearance(this.context, com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getSizeText());
            this.txtStatus.setTextAppearance(this.context, com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getSizeText());
            this.txtTitle.setTextColor(-7829368);
            String v0 = "";
            android.content.res.Resources v5 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes();
            if (v4_1.boot_ads) {
                v0 = new StringBuilder().append("").append(v5.getString(2131165678)).append("; ").toString();
                this.txtTitle.setTextColor(-16711681);
            }
            if (v4_1.boot_lvl) {
                v0 = new StringBuilder().append(v0).append(v5.getString(2131165680)).append("; ").toString();
                this.txtTitle.setTextColor(-16711936);
            }
            if (v4_1.boot_custom) {
                v0 = new StringBuilder().append(v0).append(v5.getString(2131165679)).append("; ").toString();
                this.txtTitle.setTextColor(-256);
            }
            this.txtStatus.setText(v0);
            if (v4_1.pkgName.equals(com.chelpus.Utils.getText(2131165754))) {
                this.imgIcon.setImageResource(2130837551);
                this.txtStatus.setText(com.chelpus.Utils.getText(2131165755));
            }
            this.txtStatus.setTextAppearance(this.context, 16973894);
            this.txtStatus.setTextColor(-7829368);
            this.txtTitle.setBackgroundColor(-16777216);
            this.txtStatus.setBackgroundColor(-16777216);
            v3_0 = v6;
        } else {
            v3_0 = new android.view.View(this.context);
        }
        return v3_0;
    }

    public int getViewTypeCount()
    {
        return 2;
    }

    public void setTextSize(int p1)
    {
        this.size = p1;
        this.notifyDataSetChanged();
        return;
    }

    public void sort()
    {
        super.sort(this.sorter);
        this.notifyDataSetChanged();
        return;
    }
}
