package com.android.vending.billing.InAppBillingService.LUCK;
public class ProgressDlg {
    public static int max;
    public android.content.Context context;
    public android.app.Dialog dialog;
    public String format;
    public boolean incrementStyle;
    public android.view.View root;

    static ProgressDlg()
    {
        com.android.vending.billing.InAppBillingService.LUCK.ProgressDlg.max = 0;
        return;
    }

    public ProgressDlg(android.content.Context p5)
    {
        this.dialog = 0;
        this.root = 0;
        this.context = 0;
        this.format = "%1d/%2d";
        this.incrementStyle = 0;
        this.context = p5;
        this.dialog = new android.app.Dialog(p5);
        this.dialog.requestWindowFeature(1);
        this.dialog.getWindow().setBackgroundDrawable(new android.graphics.drawable.ColorDrawable(0));
        this.dialog.getWindow().getDecorView().getViewTreeObserver().addOnGlobalLayoutListener(new com.android.vending.billing.InAppBillingService.LUCK.ProgressDlg$1(this));
        this.dialog.setContentView(2130968630);
        this.dialog.setCancelable(0);
        android.widget.ProgressBar v0_1 = ((android.widget.ProgressBar) this.dialog.findViewById(2131558621));
        v0_1.setMax(0);
        v0_1.setProgress(0);
        v0_1.setSecondaryProgress(0);
        return;
    }

    public android.app.Dialog create()
    {
        return this.dialog;
    }

    public boolean isShowing()
    {
        return this.dialog.isShowing();
    }

    public com.android.vending.billing.InAppBillingService.LUCK.ProgressDlg setCancelable(boolean p2)
    {
        this.dialog.setCancelable(p2);
        return this;
    }

    public com.android.vending.billing.InAppBillingService.LUCK.ProgressDlg setDefaultStyle()
    {
        this.incrementStyle = 0;
        try {
            ((android.app.Activity) this.context).runOnUiThread(new com.android.vending.billing.InAppBillingService.LUCK.ProgressDlg$7(this));
        } catch (Exception v0) {
            v0.printStackTrace();
        }
        return this;
    }

    public com.android.vending.billing.InAppBillingService.LUCK.ProgressDlg setIcon(int p4)
    {
        try {
            ((android.app.Activity) this.context).runOnUiThread(new com.android.vending.billing.InAppBillingService.LUCK.ProgressDlg$2(this, p4));
        } catch (Exception v0) {
            v0.printStackTrace();
        }
        return this;
    }

    public com.android.vending.billing.InAppBillingService.LUCK.ProgressDlg setIncrementStyle()
    {
        this.incrementStyle = 1;
        try {
            ((android.app.Activity) this.context).runOnUiThread(new com.android.vending.billing.InAppBillingService.LUCK.ProgressDlg$6(this));
        } catch (Exception v0) {
            v0.printStackTrace();
        }
        return this;
    }

    public com.android.vending.billing.InAppBillingService.LUCK.ProgressDlg setMax(int p5)
    {
        try {
            com.android.vending.billing.InAppBillingService.LUCK.ProgressDlg.max = p5;
            android.widget.ProgressBar v1_1 = ((android.widget.ProgressBar) this.dialog.findViewById(2131558621));
            v1_1.setVisibility(0);
            v1_1.setMax(com.android.vending.billing.InAppBillingService.LUCK.ProgressDlg.max);
            ((android.app.Activity) this.context).runOnUiThread(new com.android.vending.billing.InAppBillingService.LUCK.ProgressDlg$8(this, p5));
        } catch (Exception v0) {
            v0.printStackTrace();
        }
        return this;
    }

    public com.android.vending.billing.InAppBillingService.LUCK.ProgressDlg setMessage(String p4)
    {
        try {
            ((android.app.Activity) this.context).runOnUiThread(new com.android.vending.billing.InAppBillingService.LUCK.ProgressDlg$5(this, p4));
        } catch (Exception v0) {
            v0.printStackTrace();
        }
        return this;
    }

    public com.android.vending.billing.InAppBillingService.LUCK.ProgressDlg setOnCancelListener(android.content.DialogInterface$OnCancelListener p2)
    {
        this.dialog.setOnCancelListener(p2);
        return this;
    }

    public com.android.vending.billing.InAppBillingService.LUCK.ProgressDlg setProgress(int p4)
    {
        try {
            ((android.app.Activity) this.context).runOnUiThread(new com.android.vending.billing.InAppBillingService.LUCK.ProgressDlg$9(this, p4));
        } catch (Exception v0) {
            v0.printStackTrace();
        }
        return this;
    }

    public void setProgressNumberFormat(String p4)
    {
        try {
            this.format = p4;
            ((android.app.Activity) this.context).runOnUiThread(new com.android.vending.billing.InAppBillingService.LUCK.ProgressDlg$10(this));
        } catch (Exception v0) {
            v0.printStackTrace();
        }
        return;
    }

    public com.android.vending.billing.InAppBillingService.LUCK.ProgressDlg setTitle(int p4)
    {
        try {
            ((android.app.Activity) this.context).runOnUiThread(new com.android.vending.billing.InAppBillingService.LUCK.ProgressDlg$4(this, p4));
        } catch (Exception v0) {
            v0.printStackTrace();
        }
        return this;
    }

    public com.android.vending.billing.InAppBillingService.LUCK.ProgressDlg setTitle(String p4)
    {
        try {
            ((android.app.Activity) this.context).runOnUiThread(new com.android.vending.billing.InAppBillingService.LUCK.ProgressDlg$3(this, p4));
        } catch (Exception v0) {
            v0.printStackTrace();
        }
        return this;
    }
}
