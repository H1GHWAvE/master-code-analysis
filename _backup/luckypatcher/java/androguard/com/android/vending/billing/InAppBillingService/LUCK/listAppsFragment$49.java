package com.android.vending.billing.InAppBillingService.LUCK;
 class listAppsFragment$49 implements android.widget.AdapterView$OnItemClickListener {
    final synthetic com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment this$0;

    listAppsFragment$49(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment p1)
    {
        this.this$0 = p1;
        return;
    }

    public void onItemClick(android.widget.AdapterView p11, android.view.View p12, int p13, long p14)
    {
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getSharedPreferences("config", 0);
        String[] v3 = ((String) p11.getItemAtPosition(p13)).split("_");
        java.util.Locale v1_0 = 0;
        if (v3.length == 1) {
            v1_0 = new java.util.Locale(v3[0]);
        }
        if (v3.length == 2) {
            v1_0 = new java.util.Locale(v3[0], v3[1], "");
        }
        if (v3.length == 3) {
            v1_0 = new java.util.Locale(v3[0], v3[1], v3[2]);
        }
        java.util.Locale.setDefault(v1_0);
        android.content.res.Configuration v0_1 = new android.content.res.Configuration();
        v0_1.locale = v1_0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().updateConfiguration(v0_1, com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDisplayMetrics());
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().edit().putString("force_language", ((String) p11.getItemAtPosition(p13))).commit();
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().edit().putBoolean("settings_change", 1).commit();
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().edit().putBoolean("lang_change", 1).commit();
        this.this$0.onResume();
        return;
    }
}
