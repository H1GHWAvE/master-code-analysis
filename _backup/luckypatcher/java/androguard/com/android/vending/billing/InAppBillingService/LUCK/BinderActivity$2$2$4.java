package com.android.vending.billing.InAppBillingService.LUCK;
 class BinderActivity$2$2$4 implements android.widget.AdapterView$OnItemClickListener {
    final synthetic com.android.vending.billing.InAppBillingService.LUCK.BinderActivity$2$2 this$2;

    BinderActivity$2$2$4(com.android.vending.billing.InAppBillingService.LUCK.BinderActivity$2$2 p1)
    {
        this.this$2 = p1;
        return;
    }

    public void onItemClick(android.widget.AdapterView p6, android.view.View p7, int p8, long p9)
    {
        com.android.vending.billing.InAppBillingService.LUCK.BinderActivity$ItemFile v1_1 = ((com.android.vending.billing.InAppBillingService.LUCK.BinderActivity$ItemFile) p6.getAdapter().getItem(p8));
        java.io.File v0_1 = new java.io.File(v1_1.full);
        if (v0_1.isDirectory()) {
            com.android.vending.billing.InAppBillingService.LUCK.BinderActivity.access$302(this.this$2.this$1.this$0, v1_1);
            if ((!v0_1.canRead()) || (v0_1.listFiles() == null)) {
                com.chelpus.Utils.showDialog(new android.app.AlertDialog$Builder(this.this$2.this$1.this$0.context).setIcon(17301659).setTitle(new StringBuilder().append("[").append(v0_1.getName()).append("] folder can\'t be read!").toString()).setPositiveButton("OK", 0).create());
            } else {
                com.android.vending.billing.InAppBillingService.LUCK.BinderActivity.access$002(this.this$2.this$1.this$0, ((android.widget.ListView) p6));
                com.android.vending.billing.InAppBillingService.LUCK.BinderActivity.access$100(this.this$2.this$1.this$0, new java.io.File(v1_1.full).getPath(), ((android.widget.ListView) p6));
            }
        }
        return;
    }
}
