package com.android.vending.billing.InAppBillingService.LUCK;
 class OnBootLuckyPatcher$2 implements java.lang.Runnable {
    final synthetic com.android.vending.billing.InAppBillingService.LUCK.OnBootLuckyPatcher this$0;
    final synthetic android.content.Context val$context;

    OnBootLuckyPatcher$2(com.android.vending.billing.InAppBillingService.LUCK.OnBootLuckyPatcher p1, android.content.Context p2)
    {
        this.this$0 = p1;
        this.val$context = p2;
        return;
    }

    public void run()
    {
        java.io.File v0_1 = new java.io.File(new StringBuilder().append(this.val$context.getDir("binder", 0)).append("/bind.txt").toString());
        if ((v0_1.exists()) && (v0_1.length() > 0)) {
            System.out.println("LuckyPatcher binder start!");
            try {
                if (v0_1.exists()) {
                    java.io.FileInputStream v4_1 = new java.io.FileInputStream(v0_1);
                    java.io.BufferedReader v1_1 = new java.io.BufferedReader(new java.io.InputStreamReader(v4_1, "UTF-8"));
                    do {
                        String v2 = v1_1.readLine();
                        if (v2 == null) {
                            android.content.Intent v5_1 = new android.content.Intent(this.val$context, com.android.vending.billing.InAppBillingService.LUCK.widgets.BinderWidget);
                            v5_1.setAction(com.android.vending.billing.InAppBillingService.LUCK.widgets.BinderWidget.ACTION_WIDGET_RECEIVER_Updater);
                            this.val$context.sendBroadcast(v5_1);
                            v4_1.close();
                        } else {
                            String[] v7 = v2.split(";");
                        }
                    } while(v7.length != 2);
                    com.chelpus.Utils.verify_bind_and_run("mount", new StringBuilder().append("-o bind \'").append(v7[0]).append("\' \'").append(v7[1]).append("\'").toString(), v7[0], v7[1]);
                } else {
                    v0_1.createNewFile();
                }
            } catch (java.io.IOException v3) {
                System.out.println(new StringBuilder().append("").append(v3).toString());
            } catch (java.io.IOException v3) {
                System.out.println("Not found bind.txt");
            }
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchOnBoot = 0;
            com.chelpus.Utils.exit();
        }
        return;
    }
}
