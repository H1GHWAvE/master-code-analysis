package com.android.vending.billing.InAppBillingService.LUCK;
public class PatchesItem {
    public String group;
    public boolean insert;
    public byte[] origByte;
    public int[] origMask;
    public byte[] repByte;
    public int[] repMask;
    public boolean result;

    public PatchesItem(byte[] p3, int[] p4, byte[] p5, int[] p6, String p7, boolean p8)
    {
        this.result = 0;
        this.group = "";
        this.insert = 0;
        int[] v0_2 = new byte[p3.length];
        this.origByte = v0_2;
        this.origByte = p3;
        int[] v0_4 = new int[p4.length];
        this.origMask = v0_4;
        this.origMask = p4;
        int[] v0_6 = new byte[p5.length];
        this.repByte = v0_6;
        this.repByte = p5;
        int[] v0_8 = new int[p6.length];
        this.repMask = v0_8;
        this.repMask = p6;
        this.group = p7;
        this.insert = p8;
        return;
    }
}
