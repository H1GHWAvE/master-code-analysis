package com.android.vending.billing.InAppBillingService.LUCK;
 class listAppsFragment$118 extends android.widget.ArrayAdapter {
    final synthetic com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment this$0;

    listAppsFragment$118(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment p1, android.content.Context p2, int p3, java.util.List p4)
    {
        this.this$0 = p1;
        this(p2, p3, p4);
        return;
    }

    public android.view.View getView(int p9, android.view.View p10, android.view.ViewGroup p11)
    {
        android.view.View v3 = super.getView(p9, p10, p11);
        android.widget.TextView v2_1 = ((android.widget.TextView) v3.findViewById(2131558457));
        v2_1.setTextAppearance(this.getContext(), com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getSizeText());
        v2_1.setCompoundDrawablePadding(((int) ((1084227584 * com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDisplayMetrics().density) + 1056964608)));
        String v1_0 = new StringBuilder().append(((com.android.vending.billing.InAppBillingService.LUCK.Perm) this.getItem(p9)).Name.replace("disabled_", "").replace("chelpa_per_", "").replace("chelpus_", "").replace("android.permission.", "").replace("com.android.vending.", "")).append("\n").toString();
        if (!((com.android.vending.billing.InAppBillingService.LUCK.Perm) this.getItem(p9)).Status) {
            v2_1.setText(com.chelpus.Utils.getColoredText(v1_0, "#ffff0000", "bold"));
        } else {
            v2_1.setText(com.chelpus.Utils.getColoredText(v1_0, "#ff00ffff", "bold"));
            if (com.chelpus.Utils.isAds(((com.android.vending.billing.InAppBillingService.LUCK.Perm) this.getItem(p9)).Name)) {
                v2_1.setText(com.chelpus.Utils.getColoredText(v1_0, "#ffffff00", "bold"));
            }
        }
        String v1_1;
        if (!com.chelpus.Utils.isAds(((com.android.vending.billing.InAppBillingService.LUCK.Perm) this.getItem(p9)).Name)) {
            v1_1 = com.chelpus.Utils.getText(2131165193);
        } else {
            v1_1 = com.chelpus.Utils.getText(2131165192);
        }
        v2_1.append(com.chelpus.Utils.getColoredText(v1_1, "#ff888888", "italic"));
        return v3;
    }
}
