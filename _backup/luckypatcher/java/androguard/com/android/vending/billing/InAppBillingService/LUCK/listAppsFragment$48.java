package com.android.vending.billing.InAppBillingService.LUCK;
 class listAppsFragment$48 extends android.widget.ArrayAdapter {
    final synthetic com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment this$0;

    listAppsFragment$48(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment p1, android.content.Context p2, int p3, java.util.List p4)
    {
        this.this$0 = p1;
        this(p2, p3, p4);
        return;
    }

    public android.view.View getView(int p7, android.view.View p8, android.view.ViewGroup p9)
    {
        android.view.View v3 = super.getView(p7, p8, p9);
        android.widget.TextView v1_1 = ((android.widget.TextView) v3.findViewById(2131558457));
        v1_1.setTextColor(-1);
        String v0_1 = ((String) this.getItem(p7));
        v1_1.setText(new java.util.Locale(v0_1, v0_1).getDisplayName(new java.util.Locale(v0_1, v0_1)));
        if (v0_1.equals("my")) {
            v1_1.setText("Malay translation");
        }
        if (v0_1.equals("be")) {
            v1_1.setText("\u0411\u0435\u043b\u0430\u0440\u0443\u0441\u043a\u0430\u044f (Be\u0142arusian)");
        }
        if (v0_1.equals("zh_TW")) {
            v1_1.setText("Chinese (Taiwan)");
        }
        if (v0_1.equals("zh_CN")) {
            v1_1.setText("Chinese (China)");
        }
        if (v0_1.equals("zh_HK")) {
            v1_1.setText("Chinese (Hong Kong)");
        }
        if (v0_1.equals("pt_rBR")) {
            v1_1.setText("Portuguese (Brazil)");
        }
        v1_1.setTypeface(0, 1);
        return v3;
    }
}
