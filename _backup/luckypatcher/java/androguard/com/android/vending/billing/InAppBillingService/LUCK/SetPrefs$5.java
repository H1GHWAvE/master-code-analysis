package com.android.vending.billing.InAppBillingService.LUCK;
 class SetPrefs$5 implements android.preference.Preference$OnPreferenceChangeListener {
    final synthetic com.android.vending.billing.InAppBillingService.LUCK.SetPrefs this$0;

    SetPrefs$5(com.android.vending.billing.InAppBillingService.LUCK.SetPrefs p1)
    {
        this.this$0 = p1;
        return;
    }

    public boolean onPreferenceChange(android.preference.Preference p12, Object p13)
    {
        android.content.SharedPreferences$Editor v6 = 1;
        android.content.SharedPreferences v5 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getSharedPreferences("config", 0);
        if ((p12 != this.this$0.findPreference("language")) || (!p13.equals("1"))) {
            if ((p12 != this.this$0.findPreference("language")) || (!p13.equals("2"))) {
                v6 = 0;
            } else {
                java.util.Locale v3_1 = new java.util.Locale("en");
                java.util.Locale.setDefault(v3_1);
                android.content.res.Configuration v1_1 = new android.content.res.Configuration();
                v1_1.locale = v3_1;
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().updateConfiguration(v1_1, com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDisplayMetrics());
                v5.edit().putInt("language", 2).commit();
                android.content.Intent v4_0 = this.this$0.getIntent();
                v4_0.addFlags(131072);
                this.this$0.finish();
                this.this$0.startActivity(v4_0);
                v5.edit().putBoolean("settings_change", 1).commit();
                v5.edit().putBoolean("lang_change", 1).commit();
            }
        } else {
            java.util.Locale.setDefault(java.util.Locale.getDefault());
            android.content.res.Configuration v0_1 = new android.content.res.Configuration();
            v0_1.locale = android.content.res.Resources.getSystem().getConfiguration().locale;
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().updateConfiguration(v0_1, com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDisplayMetrics());
            v5.edit().putInt("language", 1).commit();
            android.content.Intent v4_1 = this.this$0.getIntent();
            v4_1.addFlags(131072);
            this.this$0.finish();
            this.this$0.startActivity(v4_1);
            v5.edit().putBoolean("settings_change", 1).commit();
            v5.edit().putBoolean("lang_change", 1).commit();
        }
        return v6;
    }
}
