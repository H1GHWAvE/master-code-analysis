package com.android.vending.billing.InAppBillingService.LUCK;
public class patchActivity extends android.support.v4.app.FragmentActivity {
    public static final int APP_DIALOG = 6;
    public static final int CONTEXT_DIALOG = 7;
    public static final int CREATE_APK = 0;
    public static final int CUSTOM2_DIALOG = 15;
    public static final int CUSTOM_PATCH = 1;
    public static final int DIALOG_REPORT_FORCE_CLOSE = 3535788;
    public static final int MARKET_INSTALL_DIALOG = 30;
    public static final int PROGRESS_DIALOG2 = 11;
    public static final int RESTORE_FROM_BACKUP = 28;
    private static final int SETTINGS_ORIENT_LANDSCAPE = 1;
    private static final int SETTINGS_ORIENT_PORTRET = 2;
    public static final int SETTINGS_VIEWSIZE_DEFAULT;
    private static final int SETTINGS_VIEWSIZE_SMALL;
    public static com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment frag;
    boolean mIsRestoredToTop;

    static patchActivity()
    {
        com.android.vending.billing.InAppBillingService.LUCK.patchActivity.frag = 0;
        return;
    }

    public patchActivity()
    {
        this.mIsRestoredToTop = 0;
        return;
    }

    public void apply_click(android.view.View p3)
    {
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(15);
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.func == 1) {
            com.android.vending.billing.InAppBillingService.LUCK.patchActivity.frag.custompatch(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli);
        }
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.func == 0) {
            com.android.vending.billing.InAppBillingService.LUCK.patchActivity.frag.createapkcustom();
        }
        return;
    }

    public void backup_click(android.view.View p9)
    {
        try {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str = "";
            android.widget.TextView v3_2 = new com.chelpus.Utils("");
            android.text.SpannableString v4_2 = new String[1];
            v4_2[0] = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.dalvikruncommand).append(".backup ").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName).toString();
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.tvt.setText(com.chelpus.Utils.getColoredText(v3_2.cmdRoot(v4_2), "#ff00ff73", "bold"));
        } catch (Exception v0) {
            v0.printStackTrace();
        }
        return;
    }

    public void cancel_click(android.view.View p2)
    {
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(15);
        return;
    }

    public void finish()
    {
        super.finish();
        if ((android.os.Build$VERSION.SDK_INT >= 19) && ((!this.isTaskRoot()) && (this.mIsRestoredToTop))) {
            ((android.app.ActivityManager) this.getSystemService("activity")).moveTaskToFront(this.getTaskId(), 2);
        }
        return;
    }

    public void fixobject_click(android.view.View p6)
    {
        try {
            com.android.vending.billing.InAppBillingService.LUCK.patchActivity.frag.odex(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli);
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.tvt.setText(com.chelpus.Utils.getColoredText(new StringBuilder().append("").append(com.chelpus.Utils.getText(2131165199)).toString(), "#ff00ff73", "bold"));
        } catch (Exception v0) {
            v0.printStackTrace();
            android.widget.Toast.makeText(this, "Error while saving file", 1).show();
        }
        return;
    }

    public void launch_click(android.view.View p6)
    {
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getBoolean("vibration", 0)) {
            com.android.vending.billing.InAppBillingService.LUCK.patchActivity.frag.vib = ((android.os.Vibrator) this.getSystemService("vibrator"));
            com.android.vending.billing.InAppBillingService.LUCK.patchActivity.frag.vib.vibrate(50);
        }
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(6);
        try {
            com.chelpus.Utils.run_all(new StringBuilder().append("killall ").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName).toString());
            this.startActivity(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getLaunchIntentForPackage(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName));
        } catch (Exception v0) {
            android.widget.Toast.makeText(this, com.chelpus.Utils.getText(2131165437), 1).show();
        } catch (Exception v0) {
            v0.printStackTrace();
        }
        return;
    }

    public void mod_market_check(android.view.View p2)
    {
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.connectToLicensing();
        return;
    }

    public void onBackPressed()
    {
        try {
            if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.menu_open) {
                if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adapterSelect) {
                    if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getBoolean("confirm_exit", 1)) {
                        super.onBackPressed();
                    } else {
                        com.chelpus.Utils.showDialogYesNo(com.chelpus.Utils.getText(2131165209), com.chelpus.Utils.getText(2131165540), new com.android.vending.billing.InAppBillingService.LUCK.patchActivity$4(this), new com.android.vending.billing.InAppBillingService.LUCK.patchActivity$4(this), 0);
                    }
                } else {
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.resetBatchOperation();
                }
            } else {
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.hideMenu();
            }
        } catch (Exception v1) {
            v1.printStackTrace();
            super.onBackPressed();
        }
        return;
    }

    public void onCreate(android.os.Bundle p11)
    {
        super.onCreate(p11);
        System.out.println("LuckyPatcher: create activity");
        if (!this.getSharedPreferences("config", 4).getBoolean("force_close", 0)) {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.desktop_launch = 1;
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchAct = this;
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.appcontext = this;
            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getBoolean("hide_title", 0)) {
                int v0 = android.os.Build$VERSION.SDK_INT;
                if ((v0 >= 14) && (android.view.ViewConfiguration.get(this).hasPermanentMenuKey())) {
                    this.requestWindowFeature(1);
                }
                if (v0 <= 10) {
                    this.requestWindowFeature(1);
                }
            }
            this.setContentView(2130968578);
            try {
                if (com.chelpus.Utils.getText(2131165244) == null) {
                    this.finish();
                }
            } catch (Exception v1_0) {
                v1_0.printStackTrace();
                this.finish();
            }
            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getInt("orientstion", 3) == 1) {
                this.setRequestedOrientation(0);
            }
            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getInt("orientstion", 3) == 2) {
                this.setRequestedOrientation(1);
            }
            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getInt("orientstion", 3) == 3) {
                this.setRequestedOrientation(4);
            }
            this.getWindow().addFlags(128);
        } else {
            System.out.println("LP FC detected!");
            try {
                this.getSharedPreferences("config", 4).edit().putBoolean("force_close", 0).commit();
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.mLogCollector = new com.android.vending.billing.InAppBillingService.LUCK.LogCollector();
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.init();
                android.app.AlertDialog$Builder v2_1 = new android.app.AlertDialog$Builder(this);
                v2_1.setTitle(com.chelpus.Utils.getText(2131165748)).setIcon(17301543).setMessage(com.chelpus.Utils.getText(2131165435)).setPositiveButton(com.chelpus.Utils.getText(2131165187), new com.android.vending.billing.InAppBillingService.LUCK.patchActivity$3(this)).setNegativeButton(com.chelpus.Utils.getText(2131165563), new com.android.vending.billing.InAppBillingService.LUCK.patchActivity$2(this)).setOnCancelListener(new com.android.vending.billing.InAppBillingService.LUCK.patchActivity$1(this));
                v2_1.create().show();
            } catch (Exception v1_1) {
                v1_1.printStackTrace();
                this.getSharedPreferences("config", 4).edit().putBoolean("force_close", 0).commit();
                this.finish();
                System.exit(0);
            }
        }
        return;
    }

    public void onMemoryLow()
    {
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.goodMemory = 0;
        System.out.println("LuckyPatcher (onMemoryLow): started!");
        return;
    }

    protected void onNewIntent(android.content.Intent p3)
    {
        super.onNewIntent(p3);
        System.out.println("Lucky Patcher: on new intent activity.");
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchAct = this;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.handler = new android.os.Handler();
        return;
    }

    public void onPause()
    {
        try {
            super.onPause();
        } catch (Exception v0) {
            v0.printStackTrace();
        }
        System.out.println("Lucky Patcher: activity pause.");
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchAct = this;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.handler = new android.os.Handler();
        return;
    }

    public void onResume()
    {
        try {
            super.onResume();
        } catch (Exception v0) {
            v0.printStackTrace();
        }
        System.out.println("Lucky Patcher: activity resume.");
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchAct = this;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.handler = new android.os.Handler();
        return;
    }

    public void onStart()
    {
        super.onStart();
        System.out.println("Lucky Patcher: start activity.");
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchAct = this;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.handler = new android.os.Handler();
        return;
    }

    public void onWindowFocusChanged(boolean p13)
    {
        super.onWindowFocusChanged(p13);
        System.out.println("onWindowFocusChanged");
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchAct = this;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.handler = new android.os.Handler();
        if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.return_from_control_panel) && (p13)) {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.createExpandMenu();
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.return_from_control_panel = 0;
            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli != null) {
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().edit().remove(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName).commit();
            }
            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia != null) {
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.notifyDataSetChanged();
            }
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.refresh = 1;
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(6);
            new Thread(new com.android.vending.billing.InAppBillingService.LUCK.patchActivity$5(this)).start();
        }
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia != null) {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.notifyDataSetChanged();
        }
        android.app.ActivityManager$MemoryInfo v3_1 = new android.app.ActivityManager$MemoryInfo();
        ((android.app.ActivityManager) com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getSystemService("activity")).getMemoryInfo(v3_1);
        System.out.println(new StringBuilder().append("LuckyPatcher ").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.version).append(" (FreeMemory): ").append((v3_1.availMem / 1048576)).append(" lowMemory:").append(v3_1.lowMemory).append(" TrashOld:").append((v3_1.threshold / 1048576)).toString());
        if (((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.firstrun == null) || (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.firstrun.booleanValue())) && ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia != null) && (p13))) {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.refresh = 1;
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.refreshPkgs(1);
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.firstrun = Boolean.valueOf(0);
        }
        if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia != null) && ((p13) && (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.refresh))) {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.refreshPkgs(0);
        }
        return;
    }

    public void patch_click(android.view.View p7)
    {
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.CurentSelect != 0) {
            com.chelpus.Utils.kill(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName);
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str = "";
            com.chelpus.Utils.kill(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plipack);
            android.widget.TextView v1_6 = new com.chelpus.Utils("");
            android.text.SpannableString v2_2 = new String[1];
            v2_2[0] = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.dalvikruncommand).append(".nerorunpatch ").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plipack).append(" ").append("object").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.CurentSelect).toString();
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str = v1_6.cmdRoot(v2_2);
            if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("Done")) {
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.tvt.setText(com.chelpus.Utils.getColoredText(new StringBuilder().append("Object N").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.CurentSelect).append("\n").append(com.chelpus.Utils.getText(2131165452)).toString(), "#ffff0055", "bold"));
            } else {
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.tvt.setText(com.chelpus.Utils.getColoredText(new StringBuilder().append("Object N").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.CurentSelect).append("\n").append(com.chelpus.Utils.getText(2131165451)).toString(), "#ff00ff73", "bold"));
            }
        }
        return;
    }

    public void restore_click(android.view.View p9)
    {
        try {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str = "";
            android.widget.TextView v3_2 = new com.chelpus.Utils("");
            android.text.SpannableString v4_2 = new String[1];
            v4_2[0] = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.dalvikruncommand).append(".restore ").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName).toString();
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.tvt.setText(com.chelpus.Utils.getColoredText(v3_2.cmdRoot(v4_2), "#ff00ff73", "bold"));
        } catch (Exception v0) {
            v0.printStackTrace();
        }
        return;
    }

    public void saveobject_click(android.view.View p9)
    {
        try {
            java.io.File v1_1 = new java.io.File(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).append("/").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName).append(".txt").toString());
        } catch (Exception v0_1) {
            v0_1.printStackTrace();
            android.widget.Toast.makeText(this, "Error while saving file", 1).show();
            return;
        } catch (Exception v0_0) {
            v0_0.printStackTrace();
            android.widget.Toast.makeText(this, "Error while saving file", 1).show();
            return;
        }
        if (v1_1.exists()) {
            v1_1.delete();
        }
        java.io.FileWriter v2_1 = new java.io.FileWriter(v1_1);
        v2_1.write(new StringBuilder().append("[BEGIN]\ngetActivity() Custom Patch generated by Luckypatcher the manual mode! For Object N").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.CurentSelect).append("...\n[CLASSES]\n{\"object\":\"").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.CurentSelect).append("\"}\n[ODEX]\n[END]\nApplication patched on object N").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.CurentSelect).append(". Please test...\nIf all works well. Make a \"Dalvik-cache Fix Apply\".").toString());
        v2_1.close();
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.tvt.setText(com.chelpus.Utils.getColoredText(new StringBuilder().append("Object N").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.CurentSelect).append(" ").append(com.chelpus.Utils.getText(2131165651)).toString(), "#ff00ff73", "bold"));
        return;
    }

    public void show_file_explorer(String p12)
    {
        try {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.onGroupCollapsedAll();
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli = 0;
        } catch (Exception v1) {
            v1.printStackTrace();
        }
        android.widget.LinearLayout v0_1 = ((android.widget.LinearLayout) android.view.View.inflate(this, 2130968614, 0));
        android.app.Dialog v4 = new com.android.vending.billing.InAppBillingService.LUCK.AlertDlg(this, 1).setView(v0_1).create();
        v4.setCancelable(0);
        v4.setOnKeyListener(new com.android.vending.billing.InAppBillingService.LUCK.patchActivity$28(this));
        v4.show();
        com.android.vending.billing.InAppBillingService.LUCK.patchActivity.frag.myPath = ((android.widget.TextView) v4.findViewById(2131558464));
        com.android.vending.billing.InAppBillingService.LUCK.patchActivity.frag.root = p12;
        ((android.widget.ListView) v0_1.findViewById(2131558467)).setOnItemClickListener(new com.android.vending.billing.InAppBillingService.LUCK.patchActivity$29(this));
        com.android.vending.billing.InAppBillingService.LUCK.patchActivity.frag.filebrowser = ((android.widget.ListView) v0_1.findViewById(2131558467));
        try {
            com.android.vending.billing.InAppBillingService.LUCK.patchActivity.frag.getDir(com.android.vending.billing.InAppBillingService.LUCK.patchActivity.frag.root, ((android.widget.ListView) v0_1.findViewById(2131558467)), 0);
        } catch (Exception v1) {
            try {
                com.android.vending.billing.InAppBillingService.LUCK.patchActivity.frag.root = new java.io.File(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).getParent();
                com.android.vending.billing.InAppBillingService.LUCK.patchActivity.frag.getDir(com.android.vending.billing.InAppBillingService.LUCK.patchActivity.frag.root, ((android.widget.ListView) v0_1.findViewById(2131558467)), 0);
            } catch (Exception v2) {
                com.android.vending.billing.InAppBillingService.LUCK.patchActivity.frag.root = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath;
                com.android.vending.billing.InAppBillingService.LUCK.patchActivity.frag.getDir(com.android.vending.billing.InAppBillingService.LUCK.patchActivity.frag.root, ((android.widget.ListView) v0_1.findViewById(2131558467)), 0);
            }
        }
        return;
    }

    public void toolbar_backups_click(android.view.View p5)
    {
        new java.io.File(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).append("/Backup/").toString()).mkdirs();
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.showDialogLP(11);
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.progress2.setCancelable(0);
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.progress2.setMessage(com.chelpus.Utils.getText(2131165747));
        Thread v0_1 = new Thread(new com.android.vending.billing.InAppBillingService.LUCK.patchActivity$25(this));
        v0_1.setPriority(10);
        v0_1.start();
        return;
    }

    public void toolbar_market_install_click(android.view.View p3)
    {
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(30);
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.showDialogLP(30);
        return;
    }

    public void toolbar_menu_click(android.view.View p2)
    {
        com.android.vending.billing.InAppBillingService.LUCK.patchActivity.frag.getActivity().openOptionsMenu();
        return;
    }

    public void toolbar_rebuild_click(android.view.View p13)
    {
        try {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.onGroupCollapsedAll();
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli = 0;
        } catch (Exception v1_0) {
            v1_0.printStackTrace();
        }
        android.widget.LinearLayout v0_1 = ((android.widget.LinearLayout) android.view.View.inflate(this, 2130968614, 0));
        android.app.Dialog v5 = new com.android.vending.billing.InAppBillingService.LUCK.AlertDlg(this, 1).setView(v0_1).create();
        v5.setCancelable(0);
        v5.setOnKeyListener(new com.android.vending.billing.InAppBillingService.LUCK.patchActivity$26(this));
        v5.show();
        com.android.vending.billing.InAppBillingService.LUCK.patchActivity.frag.myPath = ((android.widget.TextView) v5.findViewById(2131558464));
        String v4 = new java.io.File(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.extStorageDirectory).getParent();
        try {
            while (new java.io.File(new java.io.File(v4).getParent()).getParent() != null) {
                v4 = new java.io.File(v4).getParent();
                System.out.println(new StringBuilder().append("Parent directory:").append(v4).toString());
            }
        } catch (Exception v1_1) {
            v1_1.printStackTrace();
        }
        com.android.vending.billing.InAppBillingService.LUCK.patchActivity.frag.root = v4;
        ((android.widget.ListView) v0_1.findViewById(2131558467)).setOnItemClickListener(new com.android.vending.billing.InAppBillingService.LUCK.patchActivity$27(this));
        com.android.vending.billing.InAppBillingService.LUCK.patchActivity.frag.filebrowser = ((android.widget.ListView) v0_1.findViewById(2131558467));
        try {
            com.android.vending.billing.InAppBillingService.LUCK.patchActivity.frag.getDir(com.android.vending.billing.InAppBillingService.LUCK.patchActivity.frag.root, ((android.widget.ListView) v0_1.findViewById(2131558467)), 1);
        } catch (Exception v1) {
            try {
                com.android.vending.billing.InAppBillingService.LUCK.patchActivity.frag.root = new java.io.File(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).getParent();
                com.android.vending.billing.InAppBillingService.LUCK.patchActivity.frag.getDir(com.android.vending.billing.InAppBillingService.LUCK.patchActivity.frag.root, ((android.widget.ListView) v0_1.findViewById(2131558467)), 1);
            } catch (Exception v2) {
                com.android.vending.billing.InAppBillingService.LUCK.patchActivity.frag.root = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath;
                com.android.vending.billing.InAppBillingService.LUCK.patchActivity.frag.getDir(com.android.vending.billing.InAppBillingService.LUCK.patchActivity.frag.root, ((android.widget.ListView) v0_1.findViewById(2131558467)), 1);
            }
        }
        return;
    }

    public void toolbar_refresh_click(android.view.View p5)
    {
        try {
            android.support.v4.app.FragmentTransaction v1 = this.getSupportFragmentManager().beginTransaction();
        } catch (Exception v0) {
            v0.printStackTrace();
            return;
        }
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.filter != null) {
            v1.remove(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.filter);
            v1.commit();
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.filter.onDestroy();
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.filter = 0;
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.getFilter().filter("");
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.notifyDataSetChanged();
            return;
        } else {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.filter = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.FilterFragment();
            v1.add(2131558608, com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.filter);
            v1.commit();
            return;
        }
    }

    public void toolbar_settings_click()
    {
        if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.menu_open) {
            java.util.ArrayList v10_1 = new java.util.ArrayList();
            v10_1.add(new com.android.vending.billing.InAppBillingService.LUCK.MenuItem(2131165211, new java.util.ArrayList(), 1));
            v10_1.add(new com.android.vending.billing.InAppBillingService.LUCK.MenuItem(2131165743, new java.util.ArrayList(), 1, 1));
            v10_1.add(new com.android.vending.billing.InAppBillingService.LUCK.MenuItem(2131165718, new com.android.vending.billing.InAppBillingService.LUCK.patchActivity$11(this), 2, 1));
            v10_1.add(new com.android.vending.billing.InAppBillingService.LUCK.MenuItem(2131165590, new com.android.vending.billing.InAppBillingService.LUCK.patchActivity$12(this), 2, 1));
            v10_1.add(new com.android.vending.billing.InAppBillingService.LUCK.MenuItem(2131165677, new com.android.vending.billing.InAppBillingService.LUCK.patchActivity$13(this), 2, 1));
            v10_1.add(new com.android.vending.billing.InAppBillingService.LUCK.MenuItem(2131165456, 2131165471, new com.android.vending.billing.InAppBillingService.LUCK.patchActivity$14(this), 2, 1));
            v10_1.add(new com.android.vending.billing.InAppBillingService.LUCK.MenuItem(2131165591, new java.util.ArrayList(), 1, 1));
            v10_1.add(new com.android.vending.billing.InAppBillingService.LUCK.MenuItem(2131165509, new java.util.ArrayList(), 2, 1));
            v10_1.add(new com.android.vending.billing.InAppBillingService.LUCK.MenuItem(2131165666, 2131165667, new com.android.vending.billing.InAppBillingService.LUCK.patchActivity$15(this), 2, 1));
            v10_1.add(new com.android.vending.billing.InAppBillingService.LUCK.MenuItem(2131165411, 2131165412, new java.util.ArrayList(), 2, 1));
            v10_1.add(new com.android.vending.billing.InAppBillingService.LUCK.MenuItem(2131165404, 2131165405, new java.util.ArrayList(), 2, 1));
            v10_1.add(new com.android.vending.billing.InAppBillingService.LUCK.MenuItem(2131165663, 2131165664, new java.util.ArrayList(), 3, 1, "confirm_exit", 1, new com.android.vending.billing.InAppBillingService.LUCK.patchActivity$16(this)));
            v10_1.add(new com.android.vending.billing.InAppBillingService.LUCK.MenuItem(2131165453, 2131165454, new java.util.ArrayList(), 3, 1, "fast_start", 0, new com.android.vending.billing.InAppBillingService.LUCK.patchActivity$17(this)));
            v10_1.add(new com.android.vending.billing.InAppBillingService.LUCK.MenuItem(2131165565, 2131165566, new java.util.ArrayList(), 3, 1, "no_icon", 0, new com.android.vending.billing.InAppBillingService.LUCK.patchActivity$18(this)));
            v10_1.add(new com.android.vending.billing.InAppBillingService.LUCK.MenuItem(2131165200, 2131165203, new com.android.vending.billing.InAppBillingService.LUCK.patchActivity$19(this), 2, 1));
            v10_1.add(new com.android.vending.billing.InAppBillingService.LUCK.MenuItem(2131165249, 2131165250, new com.android.vending.billing.InAppBillingService.LUCK.patchActivity$20(this), 2, 1));
            v10_1.add(new com.android.vending.billing.InAppBillingService.LUCK.MenuItem(2131165486, 2131165487, new java.util.ArrayList(), 3, 1, "hide_notify", 0, new com.android.vending.billing.InAppBillingService.LUCK.patchActivity$21(this)));
            v10_1.add(new com.android.vending.billing.InAppBillingService.LUCK.MenuItem(2131165488, 2131165489, new java.util.ArrayList(), 3, 1, "hide_title", 0, new com.android.vending.billing.InAppBillingService.LUCK.patchActivity$22(this)));
            v10_1.add(new com.android.vending.billing.InAppBillingService.LUCK.MenuItem(2131165415, 2131165416, new java.util.ArrayList(), 3, 1, "disable_autoupdate", 0, new com.android.vending.billing.InAppBillingService.LUCK.patchActivity$23(this)));
            v10_1.add(new com.android.vending.billing.InAppBillingService.LUCK.MenuItem(2131165738, 2131165739, new java.util.ArrayList(), 3, 1, "vibration", 0, new com.android.vending.billing.InAppBillingService.LUCK.patchActivity$24(this)));
            v10_1.add(new com.android.vending.billing.InAppBillingService.LUCK.MenuItem(2131165483, new java.util.ArrayList(), 2, 1));
            v10_1.add(new com.android.vending.billing.InAppBillingService.LUCK.MenuItem(2131165734, new java.util.ArrayList(), 2, 1));
            v10_1.add(new com.android.vending.billing.InAppBillingService.LUCK.MenuItem(2131165657, new java.util.ArrayList(), 2, 1));
            v10_1.add(new com.android.vending.billing.InAppBillingService.LUCK.MenuItem(2131165190, new java.util.ArrayList(), 2, 1));
            try {
                if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.menu_adapter != null) {
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.menu_adapter.add(v10_1);
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.showMenu();
                }
            } catch (Exception v9) {
                v9.printStackTrace();
            }
        } else {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.hideMenu();
        }
        return;
    }

    public void toolbar_switchers_click(android.view.View p3)
    {
        Thread v0_1 = new Thread(new com.android.vending.billing.InAppBillingService.LUCK.patchActivity$6(this));
        v0_1.setPriority(10);
        v0_1.start();
        return;
    }

    public void toolbar_system_utils_click(android.view.View p9)
    {
        if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.menu_open) {
            java.util.ArrayList v1_1 = new java.util.ArrayList();
            v1_1.add(new com.android.vending.billing.InAppBillingService.LUCK.MenuItem(2131165211, new java.util.ArrayList(), 1));
            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su) {
                v1_1.add(new com.android.vending.billing.InAppBillingService.LUCK.MenuItem(2131165243, new java.util.ArrayList(), 1));
                v1_1.add(new com.android.vending.billing.InAppBillingService.LUCK.MenuItem(2131165259, new com.android.vending.billing.InAppBillingService.LUCK.patchActivity$7(this), 1));
                java.util.List v2 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().queryIntentServices(new android.content.Intent("com.google.android.gms.ads.identifier.service.START"), 0);
                if ((v2 == null) || (v2.size() <= 0)) {
                    v1_1.add(new com.android.vending.billing.InAppBillingService.LUCK.MenuItem(2131165724, new com.android.vending.billing.InAppBillingService.LUCK.patchActivity$9(this), 1));
                } else {
                    v1_1.add(new com.android.vending.billing.InAppBillingService.LUCK.MenuItem(2131165724, new com.android.vending.billing.InAppBillingService.LUCK.patchActivity$8(this), 1));
                }
            }
            v1_1.add(new com.android.vending.billing.InAppBillingService.LUCK.MenuItem(2131165636, new java.util.ArrayList(), 1));
            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su) {
                v1_1.add(new com.android.vending.billing.InAppBillingService.LUCK.MenuItem(2131165413, new java.util.ArrayList(), 1));
                v1_1.add(new com.android.vending.billing.InAppBillingService.LUCK.MenuItem(2131165375, new java.util.ArrayList(), 1));
                if (com.chelpus.Utils.isXposedEnabled()) {
                    v1_1.add(new com.android.vending.billing.InAppBillingService.LUCK.MenuItem(2131165753, new java.util.ArrayList(), 1));
                }
                v1_1.add(new com.android.vending.billing.InAppBillingService.LUCK.MenuItem(2131165546, new java.util.ArrayList(), 1));
                v1_1.add(new com.android.vending.billing.InAppBillingService.LUCK.MenuItem(2131165520, new java.util.ArrayList(), 1));
                v1_1.add(new com.android.vending.billing.InAppBillingService.LUCK.MenuItem(2131165584, new java.util.ArrayList(), 1));
                v1_1.add(new com.android.vending.billing.InAppBillingService.LUCK.MenuItem(2131165638, new java.util.ArrayList(), 1));
                v1_1.add(new com.android.vending.billing.InAppBillingService.LUCK.MenuItem(2131165252, new java.util.ArrayList(), 1));
            }
            if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.api < 19) || (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su)) {
                v1_1.add(new com.android.vending.billing.InAppBillingService.LUCK.MenuItem(2131165658, new com.android.vending.billing.InAppBillingService.LUCK.patchActivity$10(this), 1));
            }
            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su) {
                v1_1.add(new com.android.vending.billing.InAppBillingService.LUCK.MenuItem(2131165630, new java.util.ArrayList(), 1));
            }
            try {
                if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.menu_adapter != null) {
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.menu_adapter.add(v1_1);
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.showMenu();
                }
            } catch (Exception v0) {
                v0.printStackTrace();
            }
        } else {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.hideMenu();
        }
        return;
    }
}
