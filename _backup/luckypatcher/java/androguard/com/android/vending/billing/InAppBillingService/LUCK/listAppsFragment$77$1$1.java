package com.android.vending.billing.InAppBillingService.LUCK;
 class listAppsFragment$77$1$1 extends android.widget.ArrayAdapter {
    final synthetic com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$77$1 this$2;
    android.widget.TextView txtStatus;
    android.widget.TextView txtTitle;

    listAppsFragment$77$1$1(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$77$1 p1, android.content.Context p2, int p3, java.util.List p4)
    {
        this.this$2 = p1;
        this(p2, p3, p4);
        return;
    }

    public android.view.View getView(int p11, android.view.View p12, android.view.ViewGroup p13)
    {
        com.android.vending.billing.InAppBillingService.LUCK.CoreItem v2_1 = ((com.android.vending.billing.InAppBillingService.LUCK.CoreItem) this.getItem(p11));
        android.view.View v3 = ((android.view.LayoutInflater) com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getSystemService("layout_inflater")).inflate(2130968639, p13, 0);
        this.txtTitle = ((android.widget.TextView) v3.findViewById(2131558478));
        this.txtStatus = ((android.widget.TextView) v3.findViewById(2131558479));
        this.txtTitle.setTextAppearance(this.getContext(), com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getSizeText());
        this.txtStatus.setTextAppearance(this.getContext(), com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getSizeText());
        android.widget.CheckBox v0_1 = ((android.widget.CheckBox) v3.findViewById(2131558528));
        v0_1.setChecked(v2_1.Status);
        if (com.chelpus.Utils.isXposedEnabled()) {
            v0_1.setEnabled(1);
        } else {
            v0_1.setEnabled(0);
        }
        v0_1.setClickable(0);
        this.txtStatus.setTextAppearance(this.getContext(), 16973894);
        this.txtStatus.setTextColor(-7829368);
        this.txtTitle.setTextColor(-1);
        this.txtTitle.setText(((com.android.vending.billing.InAppBillingService.LUCK.CoreItem) this.getItem(p11)).Name);
        this.txtTitle.setTypeface(0, 1);
        String v4 = ((com.android.vending.billing.InAppBillingService.LUCK.CoreItem) this.getItem(p11)).Name;
        this.txtTitle.setText(com.chelpus.Utils.getColoredText(v4, "#ff00ff00", "bold"));
        if (p11 == 0) {
            v4 = com.chelpus.Utils.getText(2131165301);
        }
        if (p11 == 1) {
            v4 = com.chelpus.Utils.getText(2131165303);
        }
        if (p11 == 2) {
            v4 = new StringBuilder().append(com.chelpus.Utils.getText(2131165305)).append("\n").append(com.chelpus.Utils.getText(2131165306)).toString();
        }
        if (p11 == 3) {
            v4 = com.chelpus.Utils.getText(2131165309);
        }
        if (p11 == 4) {
            v4 = com.chelpus.Utils.getText(2131165274);
        }
        this.txtStatus.append(com.chelpus.Utils.getColoredText(v4, "#ff888888", "italic"));
        return v3;
    }
}
