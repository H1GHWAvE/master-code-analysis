package com.android.vending.billing.InAppBillingService.LUCK;
public class HelpActivity extends android.app.Activity {
    public android.content.Context context;
    android.app.LocalActivityManager mLocalActivityManager;
    android.widget.TabHost tabHost;

    public HelpActivity()
    {
        return;
    }

    private static android.view.View createTabView(android.content.Context p5, String p6)
    {
        android.view.View v1 = android.view.LayoutInflater.from(p5).inflate(2130968636, 0);
        ((android.widget.TextView) v1.findViewById(2131558635)).setText(p6);
        return v1;
    }

    private void initTabs(android.os.Bundle p11)
    {
        android.content.res.Resources v1 = this.getResources();
        this.tabHost = ((android.widget.TabHost) this.findViewById(16908306));
        this.mLocalActivityManager = new android.app.LocalActivityManager(this, 0);
        this.mLocalActivityManager.dispatchCreate(p11);
        this.tabHost.setup(this.mLocalActivityManager);
        android.widget.TabHost v3_1 = ((android.widget.TabHost) this.findViewById(16908306));
        v3_1.getTabWidget().setDividerDrawable(2130837589);
        android.view.View v4 = com.android.vending.billing.InAppBillingService.LUCK.HelpActivity.createTabView(v3_1.getContext(), v1.getString(2131165484));
        android.view.View v5 = com.android.vending.billing.InAppBillingService.LUCK.HelpActivity.createTabView(v3_1.getContext(), v1.getString(2131165485));
        v3_1.addTab(v3_1.newTabSpec("Common").setIndicator(v4).setContent(new android.content.Intent().setClass(this, com.android.vending.billing.InAppBillingService.LUCK.HelpCommon)));
        v3_1.addTab(v3_1.newTabSpec("Create").setIndicator(v5).setContent(new android.content.Intent().setClass(this, com.android.vending.billing.InAppBillingService.LUCK.HelpCustom)));
        v3_1.setCurrentTab(0);
        v3_1.getTabWidget().setCurrentTab(0);
        return;
    }

    public void onCreate(android.os.Bundle p2)
    {
        super.onCreate(p2);
        this.setContentView(2130968621);
        this.initTabs(p2);
        return;
    }

    protected void onPause()
    {
        this.mLocalActivityManager.dispatchPause(this.isFinishing());
        super.onPause();
        return;
    }

    protected void onResume()
    {
        this.mLocalActivityManager.dispatchResume();
        super.onResume();
        return;
    }
}
