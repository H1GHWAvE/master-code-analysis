package com.android.vending.billing.InAppBillingService.LUCK.dialogs;
public class Patch_Dialog {
    android.app.Dialog dialog;

    public Patch_Dialog()
    {
        this.dialog = 0;
        return;
    }

    public void dismiss()
    {
        if (this.dialog != null) {
            this.dialog.dismiss();
            this.dialog = 0;
        }
        return;
    }

    public android.app.Dialog onCreateDialog()
    {
        System.out.println("Patch Dialog create.");
        if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag == null) || (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext() == null)) {
            this.dismiss();
        }
        android.widget.LinearLayout v2_1 = ((android.widget.LinearLayout) android.view.View.inflate(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext(), 2130968625, 0));
        try {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patch_dialog_text_builder(((android.widget.TextView) ((android.widget.LinearLayout) v2_1.findViewById(2131558596).findViewById(2131558597)).findViewById(2131558600)), 0);
        } catch (Exception v3) {
            v3.printStackTrace();
        }
        com.android.vending.billing.InAppBillingService.LUCK.AlertDlg v1_1 = new com.android.vending.billing.InAppBillingService.LUCK.AlertDlg(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext());
        v1_1.setIcon(2130837548);
        v1_1.setTitle(com.chelpus.Utils.getText(2131165186));
        return v1_1.setCancelable(1).setPositiveButton(com.chelpus.Utils.getText(17039370), 0).setNeutralButton(2131165510, new com.android.vending.billing.InAppBillingService.LUCK.dialogs.Patch_Dialog$1(this)).setView(v2_1).create();
    }

    public void showDialog()
    {
        if (this.dialog == null) {
            this.dialog = this.onCreateDialog();
        }
        if (this.dialog != null) {
            this.dialog.show();
        }
        return;
    }
}
