package com.android.vending.billing.InAppBillingService.LUCK;
 class listAppsFragment$117 extends android.widget.ArrayAdapter {
    final synthetic com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment this$0;

    listAppsFragment$117(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment p1, android.content.Context p2, int p3, java.util.List p4)
    {
        this.this$0 = p1;
        this(p2, p3, p4);
        return;
    }

    public android.view.View getView(int p11, android.view.View p12, android.view.ViewGroup p13)
    {
        android.view.View v5 = super.getView(p11, p12, p13);
        android.widget.TextView v4_1 = ((android.widget.TextView) v5.findViewById(2131558457));
        v4_1.setTextAppearance(this.getContext(), com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getSizeText());
        v4_1.setCompoundDrawablePadding(((int) ((1084227584 * com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDisplayMetrics().density) + 1056964608)));
        String v3_0 = new StringBuilder().append(((com.android.vending.billing.InAppBillingService.LUCK.Perm) this.getItem(p11)).Name.replace("disabled_", "").replace("chelpa_per_", "").replace("chelpus_", "").replace("android.permission.", "").replace("com.android.vending.", "")).append("\n").toString();
        if (!((com.android.vending.billing.InAppBillingService.LUCK.Perm) this.getItem(p11)).Status) {
            v4_1.setText(com.chelpus.Utils.getColoredText(v3_0, "#ffff0000", "bold"));
        } else {
            if ((!((com.android.vending.billing.InAppBillingService.LUCK.Perm) this.getItem(p11)).Status) || (!((com.android.vending.billing.InAppBillingService.LUCK.Perm) this.getItem(p11)).Name.contains("chelpus_"))) {
                v4_1.setText(com.chelpus.Utils.getColoredText(v3_0, "#ff00ff00", "bold"));
            } else {
                v4_1.setText(com.chelpus.Utils.getColoredText(v3_0, "#ff00ffff", "bold"));
                if (com.chelpus.Utils.isAds(((com.android.vending.billing.InAppBillingService.LUCK.Perm) this.getItem(p11)).Name)) {
                    v4_1.setText(com.chelpus.Utils.getColoredText(v3_0, "#ffffff00", "bold"));
                }
            }
        }
        android.content.pm.PackageManager v2 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng();
        try {
            String v3_1 = v2.getPermissionInfo(((com.android.vending.billing.InAppBillingService.LUCK.Perm) this.getItem(p11)).Name.replace("chelpa_per_", "").replace("chelpus_", ""), 0).loadDescription(v2).toString();
        } catch (NullPointerException v1) {
            String v3_3 = com.chelpus.Utils.getText(2131165625);
            if (((com.android.vending.billing.InAppBillingService.LUCK.Perm) this.getItem(p11)).Name.contains("chelpus_")) {
                v3_3 = com.chelpus.Utils.getText(2131165193);
            }
            if (((com.android.vending.billing.InAppBillingService.LUCK.Perm) this.getItem(p11)).Name.contains("chelpus_")) {
                if (com.chelpus.Utils.isAds(((com.android.vending.billing.InAppBillingService.LUCK.Perm) this.getItem(p11)).Name)) {
                    v3_3 = com.chelpus.Utils.getText(2131165192);
                }
            }
            v4_1.append(com.chelpus.Utils.getColoredText(v3_3, "#ff888888", "italic"));
            return v5;
        } catch (NullPointerException v1) {
            String v3_4 = com.chelpus.Utils.getText(2131165625);
            if (((com.android.vending.billing.InAppBillingService.LUCK.Perm) this.getItem(p11)).Name.contains("chelpus_")) {
                v3_4 = com.chelpus.Utils.getText(2131165193);
            }
            if (((com.android.vending.billing.InAppBillingService.LUCK.Perm) this.getItem(p11)).Name.contains("chelpus_")) {
                if (com.chelpus.Utils.isAds(((com.android.vending.billing.InAppBillingService.LUCK.Perm) this.getItem(p11)).Name)) {
                    v3_4 = com.chelpus.Utils.getText(2131165192);
                }
            }
            v4_1.append(com.chelpus.Utils.getColoredText(v3_4, "#ff888888", "italic"));
            return v5;
        }
        if (v3_1 != null) {
            v4_1.append(com.chelpus.Utils.getColoredText(v3_1, "#ff888888", "italic"));
            return v5;
        } else {
            String v3_2 = com.chelpus.Utils.getText(2131165625);
            if (((com.android.vending.billing.InAppBillingService.LUCK.Perm) this.getItem(p11)).Name.contains("chelpus_")) {
                v3_2 = com.chelpus.Utils.getText(2131165193);
            }
            if ((((com.android.vending.billing.InAppBillingService.LUCK.Perm) this.getItem(p11)).Name.contains("chelpus_")) && (com.chelpus.Utils.isAds(((com.android.vending.billing.InAppBillingService.LUCK.Perm) this.getItem(p11)).Name))) {
                v3_2 = com.chelpus.Utils.getText(2131165192);
            }
            v4_1.append(com.chelpus.Utils.getColoredText(v3_2, "#ff888888", "italic"));
            return v5;
        }
    }
}
