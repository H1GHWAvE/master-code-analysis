package com.android.vending.billing.InAppBillingService.LUCK;
public class SetPrefs extends android.preference.PreferenceActivity {
    public android.content.Context context;
    android.preference.Preference$OnPreferenceChangeListener numberCheckListener;

    public SetPrefs()
    {
        this.numberCheckListener = new com.android.vending.billing.InAppBillingService.LUCK.SetPrefs$28(this);
        return;
    }

    static synthetic void access$000(com.android.vending.billing.InAppBillingService.LUCK.SetPrefs p0, android.content.Context p1, String p2, String p3)
    {
        p0.showMessage(p1, p2, p3);
        return;
    }

    static synthetic boolean access$100(com.android.vending.billing.InAppBillingService.LUCK.SetPrefs p1, Object p2)
    {
        return p1.numberCheck(p2);
    }

    public static void copyFolder(java.io.File p6, java.io.File p7)
    {
        if (!p6.isDirectory()) {
            p6.renameTo(p7);
        } else {
            p6.renameTo(p7);
            String[] v2 = p6.list();
            if (v2 != null) {
                int v5 = v2.length;
                int v4_1 = 0;
                while (v4_1 < v5) {
                    String v1 = v2[v4_1];
                    com.android.vending.billing.InAppBillingService.LUCK.SetPrefs.copyFolder(new java.io.File(p6, v1), new java.io.File(p7, v1));
                    v4_1++;
                }
            }
        }
        return;
    }

    private boolean numberCheck(Object p3)
    {
        if ((p3.toString().equals("")) || (!p3.toString().matches("\\d*"))) {
            this.showMessage(this, com.chelpus.Utils.getText(2131165748), com.chelpus.Utils.getText(2131165506));
            int v0_6 = 0;
        } else {
            v0_6 = 1;
        }
        return v0_6;
    }

    private void showMessage(android.content.Context p5, String p6, String p7)
    {
        android.app.Dialog v0_1 = new android.app.Dialog(p5);
        v0_1.setTitle(p6);
        v0_1.setContentView(2130968624);
        ((android.widget.TextView) v0_1.findViewById(2131558566)).setText(p7);
        ((android.widget.Button) v0_1.findViewById(2131558564)).setOnClickListener(new com.android.vending.billing.InAppBillingService.LUCK.SetPrefs$27(this, v0_1));
        v0_1.show();
        return;
    }

    protected void onCreate(android.os.Bundle p20)
    {
        this = super.onCreate(p20);
        this.context = this;
        android.content.SharedPreferences v11 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getSharedPreferences("config", 0);
        android.content.SharedPreferences v10 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getSharedPreferences("SetPrefs", 0);
        android.content.SharedPreferences$Editor v7 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getSharedPreferences("config", 0).edit();
        v7.putBoolean("Update0", 1);
        v7.commit();
        String v8 = v11.getString("force_language", "default");
        if (!v8.equals("default")) {
            java.util.Locale v4_0 = 0;
            String[] v13 = v8.split("_");
            if (v13.length == 1) {
                v4_0 = new java.util.Locale(v13[0]);
            }
            if (v13.length == 2) {
                v4_0 = new java.util.Locale(v13[0], v13[1], "");
            }
            if (v13.length == 3) {
                v4_0 = new java.util.Locale(v13[0], v13[1], v13[2]);
            }
            java.util.Locale.setDefault(v4_0);
            android.content.res.Configuration v2_1 = new android.content.res.Configuration();
            v2_1.locale = v4_0;
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().updateConfiguration(v2_1, com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDisplayMetrics());
        } else {
            java.util.Locale.setDefault(java.util.Locale.getDefault());
            android.content.res.Configuration v3_1 = new android.content.res.Configuration();
            v3_1.locale = android.content.res.Resources.getSystem().getConfiguration().locale;
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().updateConfiguration(v3_1, com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDisplayMetrics());
        }
        if (v11.getInt("orientstion", 3) == 1) {
            this.setRequestedOrientation(0);
        }
        if (v11.getInt("orientstion", 3) == 2) {
            this.setRequestedOrientation(1);
        }
        if (v11.getInt("orientstion", 3) == 3) {
            this.setRequestedOrientation(4);
        }
        android.preference.PreferenceManager v9 = this.getPreferenceManager();
        v9.setSharedPreferencesName("SetPrefs");
        v9.setSharedPreferencesMode(0);
        v10.edit().putString("viewsize", new StringBuilder().append("").append(v11.getInt("viewsize", 0)).toString()).commit();
        v10.edit().putString("root_force", new StringBuilder().append("").append(v11.getInt("root_force", 0)).toString()).commit();
        v10.edit().putBoolean("confirm_exit", v11.getBoolean("confirm_exit", 1)).commit();
        v10.edit().putString("orientstion", new StringBuilder().append("").append(v11.getInt("orientstion", 3)).toString()).commit();
        v10.edit().putString("sortby", new StringBuilder().append("").append(v11.getInt("sortby", 2)).toString()).commit();
        v10.edit().putString("language", new StringBuilder().append("").append(v11.getInt("language", 1)).toString()).commit();
        v10.edit().putBoolean("systemapp", v11.getBoolean("systemapp", 0)).commit();
        v10.edit().putString("apkname", new StringBuilder().append("").append(v11.getInt("apkname", 0)).toString()).commit();
        v10.edit().putBoolean("lvlapp", v11.getBoolean("lvlapp", 1)).commit();
        v10.edit().putBoolean("adsapp", v11.getBoolean("adsapp", 1)).commit();
        v10.edit().putBoolean("customapp", v11.getBoolean("customapp", 1)).commit();
        v10.edit().putBoolean("modifapp", v11.getBoolean("modifapp", 1)).commit();
        v10.edit().putBoolean("hide_notify", v11.getBoolean("hide_notify", 0)).commit();
        v10.edit().putBoolean("fixedapp", v11.getBoolean("fixedapp", 1)).commit();
        v10.edit().putBoolean("noneapp", v11.getBoolean("noneapp", 1)).commit();
        v10.edit().putString("path", v11.getString("basepath", "Noting")).commit();
        v10.edit().putString("extStorageDirectory", v11.getString("extStorageDirectory", "Noting")).commit();
        v10.edit().putBoolean("vibration", v11.getBoolean("vibration", 0)).commit();
        v10.edit().putBoolean("disable_autoupdate", v11.getBoolean("disable_autoupdate", 0)).commit();
        v10.edit().putBoolean("fast_start", v11.getBoolean("fast_start", 0)).commit();
        v10.edit().putBoolean("no_icon", v11.getBoolean("no_icon", 0)).commit();
        v10.edit().putBoolean("hide_title", v11.getBoolean("hide_title", 0)).commit();
        v10.edit().putString("days_on_up", new StringBuilder().append("").append(v11.getInt("days_on_up", 1)).toString()).commit();
        this.addPreferencesFromResource(2131034117);
        this.getPreferenceScreen().findPreference("days_on_up").setOnPreferenceChangeListener(this.numberCheckListener);
        this.findPreference("viewsize").setOnPreferenceChangeListener(new com.android.vending.billing.InAppBillingService.LUCK.SetPrefs$1(this));
        this.findPreference("root_force").setOnPreferenceChangeListener(new com.android.vending.billing.InAppBillingService.LUCK.SetPrefs$2(this));
        this.findPreference("orientstion").setOnPreferenceChangeListener(new com.android.vending.billing.InAppBillingService.LUCK.SetPrefs$3(this));
        this.findPreference("sortby").setOnPreferenceChangeListener(new com.android.vending.billing.InAppBillingService.LUCK.SetPrefs$4(this));
        this.findPreference("language").setOnPreferenceChangeListener(new com.android.vending.billing.InAppBillingService.LUCK.SetPrefs$5(this));
        this.findPreference("systemapp").setOnPreferenceChangeListener(new com.android.vending.billing.InAppBillingService.LUCK.SetPrefs$6(this));
        this.findPreference("confirm_exit").setOnPreferenceChangeListener(new com.android.vending.billing.InAppBillingService.LUCK.SetPrefs$7(this));
        this.findPreference("apkname").setOnPreferenceChangeListener(new com.android.vending.billing.InAppBillingService.LUCK.SetPrefs$8(this));
        this.findPreference("lvlapp").setOnPreferenceChangeListener(new com.android.vending.billing.InAppBillingService.LUCK.SetPrefs$9(this));
        this.findPreference("adsapp").setOnPreferenceChangeListener(new com.android.vending.billing.InAppBillingService.LUCK.SetPrefs$10(this));
        this.findPreference("customapp").setOnPreferenceChangeListener(new com.android.vending.billing.InAppBillingService.LUCK.SetPrefs$11(this));
        this.findPreference("modifapp").setOnPreferenceChangeListener(new com.android.vending.billing.InAppBillingService.LUCK.SetPrefs$12(this));
        this.findPreference("fixedapp").setOnPreferenceChangeListener(new com.android.vending.billing.InAppBillingService.LUCK.SetPrefs$13(this));
        this.findPreference("noneapp").setOnPreferenceChangeListener(new com.android.vending.billing.InAppBillingService.LUCK.SetPrefs$14(this));
        this.findPreference("hide_notify").setOnPreferenceChangeListener(new com.android.vending.billing.InAppBillingService.LUCK.SetPrefs$15(this));
        this.findPreference("vibration").setOnPreferenceChangeListener(new com.android.vending.billing.InAppBillingService.LUCK.SetPrefs$16(this));
        this.findPreference("disable_autoupdate").setOnPreferenceChangeListener(new com.android.vending.billing.InAppBillingService.LUCK.SetPrefs$17(this));
        this.findPreference("fast_start").setOnPreferenceChangeListener(new com.android.vending.billing.InAppBillingService.LUCK.SetPrefs$18(this));
        this.findPreference("no_icon").setOnPreferenceChangeListener(new com.android.vending.billing.InAppBillingService.LUCK.SetPrefs$19(this));
        this.findPreference("hide_title").setOnPreferenceChangeListener(new com.android.vending.billing.InAppBillingService.LUCK.SetPrefs$20(this));
        this.findPreference("path").setOnPreferenceChangeListener(new com.android.vending.billing.InAppBillingService.LUCK.SetPrefs$21(this));
        this.findPreference("help").setOnPreferenceClickListener(new com.android.vending.billing.InAppBillingService.LUCK.SetPrefs$22(this));
        this.findPreference("upd").setOnPreferenceClickListener(new com.android.vending.billing.InAppBillingService.LUCK.SetPrefs$23(this));
        this.findPreference("sendlog").setOnPreferenceClickListener(new com.android.vending.billing.InAppBillingService.LUCK.SetPrefs$24(this));
        this.findPreference("about").setOnPreferenceClickListener(new com.android.vending.billing.InAppBillingService.LUCK.SetPrefs$25(this));
        this.findPreference("language").setOnPreferenceClickListener(new com.android.vending.billing.InAppBillingService.LUCK.SetPrefs$26(this));
        return;
    }

    public boolean testPath(boolean p8, String p9)
    {
        int v1 = 0;
        System.out.println("test path.");
        try {
            if (new java.io.File(p9).exists()) {
                if (new java.io.File(p9).exists()) {
                    if (!new java.io.File(new StringBuilder().append(p9).append("/tmp.txt").toString()).createNewFile()) {
                        if (p8) {
                            this.showMessage(this, com.chelpus.Utils.getText(2131165748), com.chelpus.Utils.getText(2131165530));
                        }
                    } else {
                        new java.io.File(new StringBuilder().append(p9).append("/tmp.txt").toString()).delete();
                        v1 = 1;
                    }
                } else {
                    this.showMessage(this, com.chelpus.Utils.getText(2131165748), com.chelpus.Utils.getText(2131165530));
                }
            } else {
                new java.io.File(p9).mkdirs();
            }
        } catch (java.io.IOException v0) {
            if (!p8) {
            } else {
                this.showMessage(this, com.chelpus.Utils.getText(2131165748), com.chelpus.Utils.getText(2131165530));
            }
        }
        return v1;
    }
}
