package com.android.vending.billing.InAppBillingService.LUCK;
public class listAppsFragment extends android.support.v4.app.Fragment {
    public static final int ABOUT_DIALOG = 0;
    public static final int ADD_BOOT = 2;
    public static final int ADS_PATCH_CONTEXT_DIALOG = 17;
    public static final int APP_DIALOG = 6;
    public static final int BACKUP_SELECTOR = 26;
    public static final int CHECKABLE_LIST_ITEM = 3;
    public static final int CHILD_CATEGORY_SELECT_ITEM = 4;
    public static final int CHILD_ICON_CHANGE_SELECT_ITEM = 7;
    public static final int CHILD_NAME_SHEME_SELECT_ITEM = 6;
    public static final int CHILD_ORIENT_SELECT_ITEM = 2;
    public static final int CHILD_ROOT_SELECT_ITEM = 5;
    public static final int CHILD_SORT_SELECT_ITEM = 3;
    public static final int CHILD_TEXT_SIZE_SELECT_ITEM = 1;
    public static final int CLICKABLE_LIST_ITEM = 2;
    public static final int CONTEXT_DIALOG = 7;
    public static final int CONTEXT_DIALOG_CorePatches = 24;
    public static final int CONTEXT_DIALOG_STATIC = 38;
    public static final int CONTEXT_DIALOG_Xposed_Patches = 39;
    public static final int CREATEAPK_ADS_DIALOG = 9;
    public static final int CREATEAPK_DIALOG = 8;
    public static final int CREATEAPK_SUPPORT_DIALOG = 36;
    public static final int CREATE_APK = 0;
    public static final int CUSTOM2_DIALOG = 15;
    public static final int CUSTOM_PATCH = 1;
    public static final int CUSTOM_PATCH_SELECTOR = 16;
    public static final int Create_ADS_PATCH_CONTEXT_DIALOG = 18;
    public static final int Create_LVL_PATCH_CONTEXT_DIALOG = 19;
    public static final int Create_PERM_CONTEXT_DIALOG = 21;
    public static int CurentSelect = 0;
    public static final int Custom_PATCH_DIALOG = 4;
    public static final int DALVIK_CORE_PATCH = 37;
    public static final int DIALOG_BOOT_FILE = 3;
    public static final int DIALOG_FAILED_TO_COLLECT_LOGS = 3535122;
    public static final int DIALOG_PROGRESS_COLLECTING_LOG = 3255;
    public static final int DIALOG_REPORT_FORCE_CLOSE = 3535788;
    public static final int DIALOG_SEND_LOG = 345350;
    public static final int Disable_Activity_CONTEXT_DIALOG = 29;
    public static final int Disable_COMPONENTS_CONTEXT_DIALOG = 31;
    public static final int EXT_PATCH_DIALOG = 5;
    public static final int LOADING_PROGRESS_DIALOG = 23;
    public static final int LVL_PATCH_CONTEXT_DIALOG = 20;
    public static final int MARKET_INSTALL_DIALOG = 30;
    private static final long MILLIS_PER_MINUTE = 60000;
    public static final int PATCH_CONTEXT_DIALOG = 13;
    public static final int PATCH_DIALOG = 2;
    public static final int PERM_CONTEXT_DIALOG = 10;
    public static final int PROGRESS_DIALOG = 1;
    public static final int PROGRESS_DIALOG2 = 11;
    public static final int RESTORE_FROM_BACKUP = 28;
    private static final String SETTINGS_ORIENT = "orientstion";
    private static final int SETTINGS_ORIENT_LANDSCAPE = 1;
    private static final int SETTINGS_ORIENT_PORTRET = 2;
    private static final String SETTINGS_SORTBY = "sortby";
    private static final int SETTINGS_SORTBY_DEFAULT = 2;
    private static final int SETTINGS_SORTBY_NAME = 1;
    private static final int SETTINGS_SORTBY_STATUS = 2;
    private static final int SETTINGS_SORTBY_TIME = 3;
    public static final String SETTINGS_VIEWSIZE = "viewsize";
    public static final int SETTINGS_VIEWSIZE_DEFAULT = 0;
    private static final int SETTINGS_VIEWSIZE_SMALL = 0;
    public static final int SUPPORT_PATCH_CONTEXT_DIALOG = 34;
    public static final int SYS_INFO = 27;
    public static final int Safe_PERM_CONTEXT_DIALOG = 22;
    public static final int Safe_Signature_PERM_CONTEXT_DIALOG = 25;
    public static final int TITLE_LIST_ITEM = 1;
    public static android.widget.ArrayAdapter adapt = None;
    public static android.widget.ArrayAdapter adaptBind = None;
    public static boolean adapterSelect = False;
    public static int adapterSelectType = 0;
    public static com.android.vending.billing.InAppBillingService.LUCK.BootListItemAdapter adapter_boot = None;
    public static boolean addapps = False;
    public static int advancedFilter = 0;
    public static final int advanced_filter_apps_with_internet = 1931;
    public static final int advanced_filter_apps_with_without_internet = 1932;
    public static final int advanced_filter_for_apps_move2sd = 1929;
    public static final int advanced_filter_for_user_apps = 1930;
    static com.android.vending.billing.InAppBillingService.LUCK.dialogs.All_Dialogs all_d = None;
    public static int api = 0;
    public static boolean appDisabler = False;
    static com.android.vending.billing.InAppBillingService.LUCK.dialogs.App_Dialog app_d = None;
    public static android.content.Context appcontext = None;
    public static boolean asNeedUser = False;
    public static boolean asUser = False;
    public static String basepath = "None";
    public static android.database.sqlite.SQLiteDatabase billing_db = None;
    public static boolean binderWidget = False;
    public static boolean binder_process = False;
    public static java.util.ArrayList boot_pat = None;
    public static String[] bootlist = None;
    static com.android.vending.billing.InAppBillingService.LUCK.dialogs.Custom2_Dialog c2_d = None;
    static com.android.vending.billing.InAppBillingService.LUCK.dialogs.Create_Apk_Ads_Dialog c_a_a_d = None;
    static com.android.vending.billing.InAppBillingService.LUCK.dialogs.Create_Apk_Dialog c_a_d = None;
    static com.android.vending.billing.InAppBillingService.LUCK.dialogs.Create_Apk_Support_Dialog c_a_s_d = None;
    static com.android.vending.billing.InAppBillingService.LUCK.dialogs.Custom_Patch_Dialog c_p_d = None;
    public static java.util.ArrayList changedPermissions = None;
    public static boolean checktools = False;
    public static android.widget.ArrayAdapter component_adapt = None;
    public static android.content.SharedPreferences config = None;
    public static android.content.Context contextext = None;
    public static int countRoot = 0;
    public static final int create_SUPPORT_PATCH_CONTEXT_DIALOG = 35;
    public static java.io.File[] customlist;
    public static java.io.File customselect;
    public static String dalvikruncommand;
    public static String dalvikruncommandWithFramework;
    public static java.util.ArrayList data;
    public static com.android.vending.billing.InAppBillingService.LUCK.DatabaseHelper database;
    public static int days;
    public static boolean desktop_launch;
    public static int dialog_int;
    static com.android.vending.billing.InAppBillingService.LUCK.dialogs.Ext_Patch_Dialog e_p_d;
    public static String errorOutput;
    public static String extStorageDirectory;
    public static boolean fast_start;
    public static boolean fileNotSpace;
    public static com.android.vending.billing.InAppBillingService.LUCK.LuckyFileObserver fileOb;
    public static boolean filePatch3;
    public static com.android.vending.billing.InAppBillingService.LUCK.dialogs.FilterFragment filter;
    public static com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$TimerFirstRunTask firstRunTask;
    public static Boolean firstrun;
    public static com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment frag;
    public static int func;
    public static boolean goodMemory;
    public static android.os.Handler handler;
    public static android.os.Handler handlerToast;
    private static final char[] hex;
    public static boolean init;
    public static boolean install_market_to_system;
    private static android.content.Context instance;
    public static int lastExpandedGroupPosition;
    public static android.content.Intent launchActivity;
    public static String luckyPackage;
    public static android.widget.ExpandableListView lv;
    public static com.android.vending.billing.InAppBillingService.LUCK.LogCollector mLogCollector;
    static android.content.ServiceConnection mServiceConnL;
    static com.android.vending.billing.InAppBillingService.LUCK.dialogs.Market_Install_Dialog m_i_d;
    static android.view.Menu menu1;
    public static com.android.vending.billing.InAppBillingService.LUCK.MenuItemAdapter menu_adapter;
    static com.android.vending.billing.InAppBillingService.LUCK.dialogs.Menu_Dialog menu_d;
    static com.android.vending.billing.InAppBillingService.LUCK.dialogs.Menu_Dialog_Static menu_d_static;
    public static android.widget.ExpandableListView menu_lv;
    public static boolean menu_open;
    public static com.android.vending.billing.InAppBillingService.LUCK.patchActivity patchAct;
    public static com.android.vending.billing.InAppBillingService.LUCK.PatchData patchData;
    public static boolean patchOnBoot;
    public static boolean patchOnlyDalvikCore;
    public static android.widget.ArrayAdapter patch_adapt;
    static com.android.vending.billing.InAppBillingService.LUCK.dialogs.Patch_Dialog patch_d;
    public static android.widget.ArrayAdapter perm_adapt;
    public static android.content.pm.PackageManager pkgManager;
    public static String pkgNam;
    public static com.android.vending.billing.InAppBillingService.LUCK.PkgListItem pli;
    public static com.android.vending.billing.InAppBillingService.LUCK.PkgListItemAdapter plia;
    public static String plipack;
    public static com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog progress;
    public static com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog_2 progress2;
    public static com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog_Loading progress_loading;
    public static String rebuldApk;
    public static boolean refresh;
    public static android.content.res.Resources res;
    public static android.content.res.Resources resources;
    public static String result;
    public static String result_core_patch;
    public static boolean return_from_control_panel;
    public static boolean runResume;
    public static String runtime;
    static byte[] salt;
    public static String searchTag;
    public static java.util.ArrayList selectedApps;
    public static java.util.concurrent.Semaphore semaphoreRoot;
    public static boolean server;
    public static java.net.ServerSocket serverSocket;
    public static boolean sqlScan;
    public static boolean sqlScanLite;
    public static Boolean startUnderRoot;
    public static String str;
    public static boolean su;
    public static java.io.DataInputStream suErrorInputStream;
    public static java.io.DataInputStream suInputStream;
    public static java.io.DataOutputStream suOutputStream;
    public static Process suProcess;
    public static String supath;
    public static int textSize;
    public static String toolfilesdir;
    public static String[] tools;
    public static android.widget.TextView tvt;
    public static com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$TimerUnusedOdexTask unusedOdexTask;
    public static String verMarket;
    public static String version;
    public static int versionCodeLocal;
    public static String[] viewbootlist;
    public static boolean xposedNotify;
    public String ApkName;
    public String AppName;
    public String BuildVersionChanges;
    public String BuildVersionPath;
    public String Changes;
    public int FalseHttp;
    public String InstallAppPackageName;
    public String Mirror1;
    public String PackageName;
    public String Text;
    public int VersionCode;
    public String VersionName;
    private java.util.ArrayList activities;
    private com.android.vending.billing.InAppBillingService.LUCK.FileApkListItem apkitem;
    private java.io.File backup;
    android.view.View but;
    public int count;
    public com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$ItemFile current;
    String dalvikcache;
    String dalvikcache2;
    String datadir;
    public android.widget.ListView filebrowser;
    com.android.vending.licensing.ILicensingService mServiceL;
    public int maxcount;
    android.widget.TextView myPath;
    String package_name;
    private java.util.ArrayList permissions;
    String prefs_dir;
    java.io.File prefs_file;
    String prefs_name;
    int responseCode;
    String root;
    int start;
    public String urlpath;
    android.os.Vibrator vib;

    static listAppsFragment()
    {
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.verMarket = "";
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.asUser = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.asNeedUser = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.runResume = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.dialog_int = 255;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.runtime = "";
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.database = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.lastExpandedGroupPosition = -1;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.customselect = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.customlist = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.res = 0;
        byte[] v0_4 = new String[1];
        v0_4[0] = "empty";
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.bootlist = v0_4;
        byte[] v0_5 = new String[1];
        v0_5[0] = "empty";
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.viewbootlist = v0_5;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pkgNam = "beleberda";
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.searchTag = "beleberda";
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.countRoot = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.goodMemory = 1;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchData = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.addapps = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.rebuldApk = "";
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.dalvikruncommand = "";
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.dalvikruncommandWithFramework = "";
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.binder_process = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.mLogCollector = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.versionCodeLocal = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.return_from_control_panel = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.firstrun = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.fast_start = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.refresh = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.extStorageDirectory = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchOnBoot = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.desktop_launch = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.tools = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.checktools = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.errorOutput = "";
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.api = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.days = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adaptBind = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.result = "";
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.textSize = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.config = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.suProcess = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.suInputStream = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.suOutputStream = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.suErrorInputStream = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.semaphoreRoot = new java.util.concurrent.Semaphore(1);
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.appDisabler = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.binderWidget = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.handler = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.handlerToast = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.filter = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.advancedFilter = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchOnlyDalvikCore = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.install_market_to_system = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.serverSocket = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.server = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.selectedApps = new java.util.ArrayList();
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adapterSelect = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adapterSelectType = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.init = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.billing_db = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.menu_open = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.launchActivity = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pkgManager = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.resources = 0;
        byte[] v0_18 = new char[16];
        v0_18 = {48, 0, 49, 0, 50, 0, 51, 0, 52, 0, 53, 0, 54, 0, 55, 0};
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.hex = v0_18;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.result_core_patch = "";
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.changedPermissions = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.fileNotSpace = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.filePatch3 = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.xposedNotify = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.sqlScan = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.sqlScanLite = 0;
        byte[] v0_21 = new byte[20];
        v0_21 = {-33, 43, 12, -98, -78, -24, 65, -42, 28, 17, -112, 99, 66, -34, -11, 13, -99, 32, -62, 89};
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.salt = v0_21;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.app_d = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patch_d = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.menu_d = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.menu_d_static = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.c_p_d = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.c2_d = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.c_a_d = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.c_a_a_d = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.c_a_s_d = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.e_p_d = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.m_i_d = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.all_d = 0;
        return;
    }

    public listAppsFragment()
    {
        this.start = 0;
        this.VersionName = "";
        this.BuildVersionPath = "";
        this.BuildVersionChanges = "";
        this.Mirror1 = "";
        this.Text = "";
        this.Changes = "";
        this.FalseHttp = 0;
        this.dalvikcache = "";
        this.dalvikcache2 = "";
        this.but = 0;
        this.responseCode = 255;
        this.count = 0;
        this.maxcount = 0;
        this.package_name = "com.emulator.fpse";
        this.prefs_dir = "shared_prefs";
        this.prefs_name = "com.android.vending.licensing.ServerManagedPolicy.xml";
        this.backup = 0;
        this.apkitem = 0;
        this.datadir = "empty";
        return;
    }

    private void LvlAdsPatch()
    {
        try {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.showDialogLP(23);
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.progress_loading.setMessage(new StringBuilder().append(com.chelpus.Utils.getText(2131165747)).append("...").toString());
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.progress_loading.setCancelable(0);
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.progress_loading.setMax(6);
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.progress_loading.setTitle(com.chelpus.Utils.getText(2131165747));
        } catch (Exception v0) {
            v0.printStackTrace();
        }
        if (this.isAdded()) {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchAct = ((com.android.vending.billing.InAppBillingService.LUCK.patchActivity) this.getActivity());
        }
        Thread v1_1 = new Thread(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$15(this));
        v1_1.setPriority(10);
        v1_1.start();
        return;
    }

    static synthetic boolean access$000(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment p1, String p2, boolean p3)
    {
        return p1.copyData(p2, p3);
    }

    static synthetic java.util.Comparator access$100(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment p1)
    {
        return p1.getSortPref();
    }

    static synthetic void access$1200(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment p0, java.io.File p1, String p2, String p3, String p4, String p5, String p6, String p7)
    {
        p0.afterCustomPatch(p1, p2, p3, p4, p5, p6, p7);
        return;
    }

    static synthetic void access$1300(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment p0)
    {
        p0.getSignatureKeys();
        return;
    }

    static synthetic boolean access$1400(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment p1, Object p2)
    {
        return p1.numberCheck(p2);
    }

    static synthetic void access$1500(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment p0, String p1, String p2)
    {
        p0.moveToSystem(p1, p2);
        return;
    }

    static synthetic void access$1600(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment p0)
    {
        p0.cleanupService();
        return;
    }

    static synthetic void access$1700(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment p0)
    {
        p0.updateapp();
        return;
    }

    static synthetic java.io.File access$1800(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment p1)
    {
        return p1.backup;
    }

    static synthetic com.android.vending.billing.InAppBillingService.LUCK.FileApkListItem access$1900(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment p1)
    {
        return p1.apkitem;
    }

    static synthetic void access$200(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment p0, java.util.ArrayList p1, java.util.Comparator p2)
    {
        p0.populateAdapter(p1, p2);
        return;
    }

    static synthetic void access$2000(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment p0, String p1, String p2)
    {
        p0.installToSystem(p1, p2);
        return;
    }

    static synthetic void access$2100(android.content.Context p0, String p1, String p2)
    {
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.showMessageInfoStatic(p0, p1, p2);
        return;
    }

    static synthetic java.util.ArrayList access$2200(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment p1)
    {
        return p1.permissions;
    }

    static synthetic java.util.ArrayList access$2300(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment p1)
    {
        return p1.activities;
    }

    static synthetic void access$300(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment p0, int p1, String p2, String p3, String p4)
    {
        p0.showNotify(p1, p2, p3, p4);
        return;
    }

    static synthetic void access$400(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment p0, android.content.Context p1, String p2, String p3)
    {
        p0.showMessageInfo(p1, p2, p3);
        return;
    }

    static synthetic void access$500(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment p0, String p1)
    {
        p0.afterPatch(p1);
        return;
    }

    private void afterCustomPatch(java.io.File p26, String p27, String p28, String p29, String p30, String p31, String p32)
    {
        try {
            System.out.println(p32);
            String v20_1 = new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$18;
            v20_1(this);
            this.runToMain(v20_1);
            boolean v19 = com.chelpus.Utils.isWithFramework();
        } catch (Exception v7_2) {
            v7_2.printStackTrace();
            System.out.println(new StringBuilder().append("luuuuu ").append(v7_2).toString());
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str = p32;
            String v20_128 = new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$21;
            v20_128(this);
            this.runToMain(v20_128);
            return;
        }
        try {
            java.io.BufferedReader v3_1 = new java.io.BufferedReader(new java.io.InputStreamReader(new java.io.FileInputStream(p26), "UTF-8"));
            StringBuilder v0_6 = new String[2000];
            String[] v17 = v0_6;
            int v4 = 0;
        } catch (Exception v7_0) {
            v7_0.printStackTrace();
        }
        while(true) {
            String v5 = v3_1.readLine();
            if (v5 == null) {
                break;
            }
            v17[0] = v5;
            if ((v17[0].toUpperCase().contains("[")) && (v17[0].toUpperCase().contains("]"))) {
                v4 = 0;
            }
            if (v17[0].toUpperCase().contains("[COMPONENT]")) {
                v4 = 1;
            }
            if (v4 != 0) {
                if (v17[0].contains("enable")) {
                    try {
                        String v18_0 = new org.json.JSONObject(v17[0]).getString("enable");
                    } catch (Exception v7) {
                        System.out.println("Error get component!");
                    }
                    String v18_1 = v18_0.trim();
                    String v20_19 = new com.chelpus.Utils("");
                    StringBuilder v0_10 = new String[1];
                    String v21_7 = v0_10;
                    v21_7[0] = new StringBuilder().append("pm enable \'").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName).append("/").append(v18_1).append("\'").toString();
                    v20_19.cmdRoot(v21_7);
                }
                if (v17[0].contains("disable")) {
                    try {
                        String v18_2 = new org.json.JSONObject(v17[0]).getString("disable");
                    } catch (Exception v7) {
                        System.out.println("Error get component!");
                    }
                    String v18_3 = v18_2.trim();
                    String v20_26 = new com.chelpus.Utils("");
                    StringBuilder v0_17 = new String[1];
                    String v21_12 = v0_17;
                    v21_12[0] = new StringBuilder().append("pm disable \'").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName).append("/").append(v18_3).append("\'").toString();
                    v20_26.cmdRoot(v21_12);
                }
            }
        }
        if ((p32.contains("Patch on Reboot added!")) && (p26 != null)) {
            try {
                com.chelpus.Utils.copyFile(p26, new java.io.File(new StringBuilder().append(this.getContext().getDir("bootlist", 0)).append("/").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName).toString()));
            } catch (String v20) {
            }
        }
        p32.split("\n");
        String[] v12 = p32.split("\n");
        java.io.File v15_0 = 0;
        int v9_0 = 0;
        while (v9_0 < v12.length) {
            if (!v19) {
                if (v12[v9_0].equals("Open SqLite database")) {
                    v15_0 = new java.io.File(v12[(v9_0 + 1)]);
                }
                if (v12[v9_0].equals("Execute:")) {
                    try {
                        com.chelpus.Utils.run_all(new StringBuilder().append("chmod 777 ").append(p30).append("/").append(v15_0.getName()).toString());
                        android.database.sqlite.SQLiteDatabase v6 = android.database.sqlite.SQLiteDatabase.openDatabase(new StringBuilder().append(p30).append("/").append(v15_0.getName()).toString(), 0, 0);
                        v6.execSQL(v12[(v9_0 + 1)]);
                        System.out.println(v12[(v9_0 + 1)]);
                        v6.close();
                    } catch (Exception v7_1) {
                        System.out.println(new StringBuilder().append("LuckyPatcher: SQL error - ").append(v7_1).toString());
                    }
                }
            }
            v9_0++;
        }
        if (!v19) {
            int v9_1 = 0;
            while (v9_1 < v12.length) {
                if (v12[v9_1].equals("Open SqLite database")) {
                    java.io.File v15_2 = new java.io.File(v12[(v9_1 + 1)]);
                    java.io.File v16 = new java.io.File;
                    v16(new StringBuilder().append(p30).append("/").append(v15_2.getName()).toString());
                    if (v16.exists()) {
                        if ((v15_2 != null) && (com.chelpus.Utils.exists(new StringBuilder().append(v15_2.getAbsolutePath()).append("-wal").toString()))) {
                            com.chelpus.Utils.run_all(new StringBuilder().append("chmod 777 ").append(v15_2.getAbsolutePath()).append("-wal").toString());
                            com.chelpus.Utils.run_all(new StringBuilder().append("rm ").append(v15_2.getAbsolutePath()).append("-wal").toString());
                        }
                        if (com.chelpus.Utils.exists(new StringBuilder().append(v15_2.getAbsolutePath()).append("-shm").toString())) {
                            com.chelpus.Utils.run_all(new StringBuilder().append("chmod 777 ").append(v15_2.getAbsolutePath()).append("-shm").toString());
                            com.chelpus.Utils.run_all(new StringBuilder().append("rm ").append(v15_2.getAbsolutePath()).append("-shm").toString());
                        }
                        com.chelpus.Utils.copyFile(new StringBuilder().append(p30).append("/").append(v15_2.getName()).toString(), v15_2.getAbsolutePath(), 1, 1);
                        v16.delete();
                    }
                }
                v9_1++;
            }
        }
        String v20_41 = new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$19;
        v20_41(this);
        this.runToMain(v20_41);
        if (p27.contains("patch.by.sanx_com.maxmpz.audioplayer")) {
            this = this.pa();
        }
        if (p27.contains("patch_ren.by.ramzezzz_com.maxmpz.audioplayer.txt")) {
            this = this.pa2();
        }
        if (p27.contains("patch.by.sanx_com.emulator.fpse")) {
            this = this.fpse();
        }
        com.chelpus.Utils.kill(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName);
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str = p32;
        String v20_50 = new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$20;
        v20_50(this);
        this.runToMain(v20_50);
        return;
    }

    private void afterPatch(String p7)
    {
        System.out.println(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str);
        System.out.println("3");
        try {
            if (p7.contains("backup")) {
                new java.io.File(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).append("/Backup/").toString()).mkdirs();
                com.chelpus.Utils.copyFile(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchData.appdir, new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).append("/Backup/").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchData.pkg).append(".apk").toString(), 0, 0);
            }
        } catch (Exception v1) {
            System.out.println(new StringBuilder().append("LuckyPatcher LVLADS: ").append(v1).toString());
            v1.printStackTrace();
        }
        this.runToMain(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$16(this));
        return;
    }

    private static String byteArray2Hex(byte[] p6)
    {
        StringBuilder v1_1 = new StringBuilder((p6.length * 2));
        int v3 = p6.length;
        String v2_2 = 0;
        while (v2_2 < v3) {
            byte v0 = p6[v2_2];
            v1_1.append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.hex[((v0 & 240) >> 4)]);
            v1_1.append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.hex[(v0 & 15)]);
            v2_2++;
        }
        return v1_1.toString();
    }

    private static String bytes2String(byte[] p7)
    {
        StringBuilder v2_1 = new StringBuilder();
        int v4 = p7.length;
        int v3_0 = 0;
        while (v3_0 < v4) {
            String v1 = Integer.toHexString((p7[v3_0] & 255));
            if (v1.length() == 1) {
                v1 = new StringBuilder().append("0").append(v1).toString();
            }
            v2_1.append(v1);
            v3_0++;
        }
        return v2_1.toString();
    }

    private void cleanupService()
    {
        if (this.mServiceL != null) {
            try {
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().unbindService(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.mServiceConnL);
            } catch (int v0) {
            }
            this.mServiceL = 0;
        }
        return;
    }

    public static String[] consolePM()
    {
        java.util.ArrayList v5_1 = new java.util.ArrayList();
        try {
            Process v4 = Runtime.getRuntime().exec("pm list packages");
            v4.waitFor();
            java.io.DataInputStream v3_1 = new java.io.DataInputStream(v4.getInputStream());
            byte[] v0 = new byte[v3_1.available()];
            v3_1.read(v0);
            String v7_1 = new String(v0);
            v4.destroy();
            String[] v10 = v7_1.split("\n");
            boolean v12_1 = v10.length;
            int v11_4 = 0;
        } catch (Exception v1_2) {
            v1_2.printStackTrace();
            String[] v8 = new String[v5_1.size()];
            int v2 = 0;
            int v11_7 = v5_1.iterator();
            while (v11_7.hasNext()) {
                v8[v2] = ((String) v11_7.next());
                v2++;
            }
            return v8;
        } catch (Exception v1_1) {
            v1_1.printStackTrace();
        } catch (Exception v1_0) {
            System.out.println(new StringBuilder().append("LuckyPatcher (consolePM):").append(v1_0).toString());
            v1_0.printStackTrace();
        }
        while (v11_4 < v12_1) {
            String v9 = v10[v11_4];
            if (v9.startsWith("package:")) {
                v5_1.add(v9.substring(8));
            }
            v11_4++;
        }
    }

    private boolean copyData(String p13, boolean p14)
    {
        int v7 = 0;
        if (!new java.io.File(p13).exists()) {
            new java.io.File(p13).mkdirs();
        }
        int v5_0 = 0;
        try {
            String[] v4 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getAssets().list("");
        } catch (Exception v2_0) {
            v2_0.printStackTrace();
            return v7;
        }
        if (!p14) {
            v5_0 = new java.io.File(p13).list();
        }
        int v9 = v4.length;
        int v8_9 = 0;
        while (v8_9 < v9) {
            String v3 = v4[v8_9];
            if ((!v3.contains("dexopt-wrapper")) && ((!v3.equals("zip")) && ((!v3.equals("zipalign")) && ((!v3.equals("testkey.pk8")) && ((!v3.equals("testkey.x509.pem")) && ((!v3.equals("testkey.sbt")) && ((!v3.equals("busybox")) && ((!v3.equals("fonts")) && ((!v3.equals("xposed_init")) && ((!v3.equals("fonts")) && ((!v3.equals("images")) && (!v3.equals("sounds"))))))))))))) {
                int v1 = 0;
                if (p14) {
                    if (!v3.equals("AdsBlockList_user_edit.txt")) {
                        v1 = 0;
                    } else {
                        int v5_1 = new java.io.File(p13).list();
                        int v6_0 = 0;
                        while (v6_0 < v5_1.length) {
                            if (v3.equals(new java.io.File(v5_1[v6_0]).getName())) {
                                v1 = 1;
                            }
                            v6_0++;
                        }
                        v5_0 = 0;
                    }
                } else {
                    int v6_1 = 0;
                    try {
                        while (v6_1 < v5_0.length) {
                            if (v3.equals(new java.io.File(v5_0[v6_1]).getName())) {
                                v1 = 1;
                            }
                            v6_1++;
                        }
                    } catch (Exception v2) {
                        v1 = 0;
                    }
                }
                if (v1 == 0) {
                    try {
                        com.chelpus.Utils.getAssets(v3, p13);
                    } catch (Exception v2_1) {
                        v2_1.printStackTrace();
                        return v7;
                    } catch (java.io.FileNotFoundException v10) {
                    }
                }
            }
            v8_9++;
        }
        v7 = 1;
        return v7;
    }

    public static void createExpandMenu()
    {
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli != null) {
            try {
                String v0 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName, 0).applicationInfo.sourceDir;
            } catch (Exception v1_0) {
                v1_0.printStackTrace();
            } catch (Exception v1_1) {
                v1_1.printStackTrace();
            }
            if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su) && ((v0 != null) && (!v0.startsWith("/system/")))) {
                int v3_3 = new int[6];
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.childMenu = v3_3;
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.childMenu[0] = 2131165207;
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.childMenu[1] = 2131165322;
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.childMenu[2] = 2131165197;
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.childMenu[3] = 2131165731;
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.childMenu[4] = 2131165253;
                if (v0.startsWith("/mnt/asec/")) {
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.childMenu[5] = 2131165554;
                } else {
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.childMenu[5] = 2131165556;
                }
            }
            if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su) && ((v0 != null) && (v0.startsWith("/system/")))) {
                int v3_11 = new int[5];
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.childMenu = v3_11;
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.childMenu[0] = 2131165207;
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.childMenu[1] = 2131165322;
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.childMenu[2] = 2131165197;
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.childMenu[3] = 2131165731;
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.childMenu[4] = 2131165253;
            }
            if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su) {
                int v3_17 = new int[5];
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.childMenu = v3_17;
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.childMenu[0] = 2131165207;
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.childMenu[1] = 2131165322;
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.childMenu[2] = 2131165197;
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.childMenu[3] = 2131165731;
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.childMenu[4] = 2131165206;
            }
            if (v0 == null) {
                int v3_23 = new int[1];
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.childMenu = v3_23;
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.childMenu[0] = 2131165201;
            }
        }
        return;
    }

    public static void exitSu()
    {
        com.chelpus.Utils.exitRoot();
        return;
    }

    private void fpse()
    {
        com.google.android.vending.licensing.AESObfuscator v8_1 = new com.google.android.vending.licensing.AESObfuscator(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.salt, this.package_name, android.provider.Settings$Secure.getString(this.getContext().getContentResolver(), "android_id"));
        String v10_0 = Long.toString((System.currentTimeMillis() + 3.07348356965e-313));
        String v7_0 = Long.toString((System.currentTimeMillis() + 3.07348356965e-313));
        String v10_1 = v8_1.obfuscate(v10_0, "validityTimestamp");
        String v3_1 = v8_1.obfuscate("256", "lastResponse");
        String v7_1 = v8_1.obfuscate(v7_0, "retryUntil");
        String v4_1 = v8_1.obfuscate("10", "maxRetries");
        String v6_1 = v8_1.obfuscate("0", "retryCount");
        java.io.File v5_1 = new java.io.File(new StringBuilder().append(this.getContext().getFilesDir()).append("/").append(this.prefs_name).toString());
        java.io.File v11_1 = new java.io.File(new StringBuilder().append("/data/data/").append(this.package_name).append("/").append(this.prefs_dir).append("/").append(this.prefs_name).toString());
        java.io.File v12_1 = new java.io.File(new StringBuilder().append("/dbdata/databases/").append(this.package_name).append("/").append(this.prefs_dir).append("/").append(this.prefs_name).toString());
        try {
            v5_1.createNewFile();
            java.io.FileOutputStream v2_1 = new java.io.FileOutputStream(v5_1);
            org.xmlpull.v1.XmlSerializer v9 = android.util.Xml.newSerializer();
            v9.setOutput(v2_1, "UTF-8");
            v9.startDocument(0, Boolean.valueOf(1));
            v9.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", 1);
            v9.startTag(0, "map");
            v9.startTag(0, "string");
            v9.attribute("", "name", "validityTimestamp");
            v9.text(v10_1);
            v9.endTag(0, "string");
            v9.startTag(0, "string");
            v9.attribute("", "name", "retryCount");
            v9.text(v6_1);
            v9.endTag(0, "string");
            v9.startTag(0, "string");
            v9.attribute("", "name", "maxRetries");
            v9.text(v4_1);
            v9.endTag(0, "string");
            v9.startTag(0, "string");
            v9.attribute("", "name", "retryUntil");
            v9.text(v7_1);
            v9.endTag(0, "string");
            v9.startTag(0, "string");
            v9.attribute("", "name", "lastResponse");
            v9.text(v3_1);
            v9.endTag(0, "string");
            v9.endTag(0, "map");
            v9.endDocument();
            v9.flush();
            v2_1.close();
        } catch (String v13) {
        }
        com.chelpus.Utils.copyFile(new StringBuilder().append(this.getContext().getFilesDir()).append("/").append(this.prefs_name).toString(), v11_1.getAbsolutePath(), 0, 0);
        com.chelpus.Utils.copyFile(new StringBuilder().append(this.getContext().getFilesDir()).append("/").append(this.prefs_name).toString(), v12_1.getAbsolutePath(), 0, 0);
        if (new java.io.File(new StringBuilder().append(this.getContext().getFilesDir()).append("/").append(this.prefs_name).toString()).exists()) {
            new java.io.File(new StringBuilder().append(this.getContext().getFilesDir()).append("/").append(this.prefs_name).toString()).delete();
        }
        com.chelpus.Utils.run_all(new StringBuilder().append("chmod 777 ").append(v11_1.getAbsolutePath()).toString());
        com.chelpus.Utils.run_all(new StringBuilder().append("chmod 777 ").append(v12_1.getAbsolutePath()).toString());
        return;
    }

    public static android.content.SharedPreferences getConfig()
    {
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.config == null) {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.config = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getSharedPreferences("config", 4);
        }
        return com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.config;
    }

    public static android.content.Context getInstance()
    {
        try {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.instance = com.android.vending.billing.InAppBillingService.LUCK.LuckyApp.getInstance();
        } catch (Exception v0) {
            v0.printStackTrace();
            return com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.instance;
        }
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.instance != null) {
            return com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.instance;
        } else {
            System.out.println("LuckyApp Instance == null");
            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchAct == null) {
                if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.instance != null) {
                    return com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.instance;
                } else {
                    System.out.println("Return Instance == null");
                    return com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.instance;
                }
            } else {
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.instance = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchAct.getApplicationContext();
                if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.instance == null) {
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.instance = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchAct;
                }
                if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.instance != null) {
                    return com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.instance;
                } else {
                    System.out.println("Return Instance from acivity == null");
                    return com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.instance;
                }
            }
        }
    }

    public static android.content.Intent getLaunchIntent()
    {
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.launchActivity == null) {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.launchActivity = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getLaunchIntentForPackage(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.luckyPackage);
        }
        return com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.launchActivity;
    }

    public static String[] getPackages()
    {
        System.out.println("LuckyPatcher (getPackageManager install pkgs): start.");
        String[] v6 = 0;
        android.content.pm.PackageManager v8 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng();
        try {
            int v0 = v8.getInstalledPackages(0);
            try {
                v6 = new String[v0.size()];
                int v3_0 = 0;
                Exception v9_3 = v0.iterator();
            } catch (Exception v2_0) {
                v2_0.printStackTrace();
            }
            while (v9_3.hasNext()) {
                v6[v3_0] = ((android.content.pm.PackageInfo) v9_3.next()).packageName;
                v3_0++;
            }
            if ((v6 == null) || (v6.length == 0)) {
                System.out.println("LuckyPatcher (Intent PackageManager install pkgs): start.");
                try {
                    java.util.List v5 = v8.queryIntentActivities(new android.content.Intent("android.intent.action.MAIN"), 128);
                    v6 = new String[v5.size()];
                    int v3_1 = 0;
                    Exception v9_10 = v5.iterator();
                } catch (Exception v9) {
                }
                while (v9_10.hasNext()) {
                    v6[v3_1] = ((android.content.pm.ResolveInfo) v9_10.next()).activityInfo.packageName;
                    v3_1++;
                }
            }
            if ((v6 == null) || (v6.length == 0)) {
                System.out.println("LuckyPatcher (consolePM install pkgs): start.");
                v6 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.consolePM();
            }
            return v6;
        } catch (Exception v2_1) {
            System.out.println(new StringBuilder().append("LuckyPatcher (getPM install pkgs): ").append(v2_1).toString());
            v2_1.printStackTrace();
        }
    }

    public static android.content.pm.PackageManager getPkgMng()
    {
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pkgManager == null) {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pkgManager = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getPackageManager();
        }
        return com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pkgManager;
    }

    public static android.content.res.Resources getRes()
    {
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.resources == null) {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.resources = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getResources();
        }
        return com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.resources;
    }

    private void getSignatureKeys()
    {
        try {
            if ((new java.io.File(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir).append("/").append("zipalign").toString()).exists()) && (new java.io.File(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir).append("/").append("zipalign").toString()).length() == com.chelpus.Utils.getRawLength(2131099663))) {
                com.chelpus.Utils.chmod(new java.io.File(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir).append("/").append("zipalign").toString()), 777);
                com.chelpus.Utils.run_all(new StringBuilder().append("chmod 777 ").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir).append("/").append("zipalign").toString());
                com.chelpus.Utils.getAssets("testkey.pk8", new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).append("/Modified/Keys").toString());
                com.chelpus.Utils.getAssets("testkey.sbt", new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).append("/Modified/Keys").toString());
                com.chelpus.Utils.getAssets("testkey.x509.pem", new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).append("/Modified/Keys").toString());
            } else {
                com.chelpus.Utils.getRawToFile(2131099663, new java.io.File(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir).append("/zipalign").toString()));
                com.chelpus.Utils.chmod(new java.io.File(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getFilesDir()).append("/").append("zipalign").toString()), 777);
                com.chelpus.Utils.run_all(new StringBuilder().append("chmod 777 ").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getFilesDir()).append("/").append("zipalign").toString());
            }
        } catch (Exception v0) {
            v0.printStackTrace();
        }
        return;
    }

    public static int getSizeText()
    {
        int v2_1;
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.textSize != 0) {
            v2_1 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.textSize;
        } else {
            switch (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getSharedPreferences("config", 4).getInt("viewsize", 0)) {
                case 0:
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.textSize = 16973894;
                    break;
                case 1:
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.textSize = 16973892;
                    break;
                default:
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.textSize = 16973890;
            }
            v2_1 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.textSize;
        }
        return v2_1;
    }

    private java.util.Comparator getSortPref()
    {
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$byPkgTime v0_3;
        switch (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getInt("sortby", 2)) {
            case 1:
                v0_3 = new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$byPkgName(this);
                break;
            case 2:
            default:
                v0_3 = new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$byPkgStatus(this);
                break;
            case 3:
                v0_3 = new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$byPkgTime(this);
                break;
        }
        return v0_3;
    }

    public static java.util.ArrayList getStorage()
    {
        java.util.ArrayList v14_1 = new java.util.ArrayList();
        java.io.ByteArrayOutputStream v1_1 = new java.io.ByteArrayOutputStream();
        try {
            java.io.File v0_1 = new String[0];
            java.util.Iterator v15_1 = new ProcessBuilder(v0_1);
            java.io.File v0_3 = new String[1];
            java.io.File v16_3 = v0_3;
            v16_3[0] = "mount";
            Process v12 = v15_1.command(v16_3).redirectErrorStream(1).start();
            v12.waitFor();
            java.io.InputStream v6 = v12.getInputStream();
            byte[] v3 = new byte[8192];
        } catch (Exception v5_0) {
            v5_0.printStackTrace();
            String v13 = v1_1.toString();
            try {
                java.util.ArrayList v11 = com.chelpus.Utils.getMounts();
            } catch (Exception v5_1) {
                v5_1.printStackTrace();
            }
            String[] v10 = v13.split("\n");
            v14_1.clear();
            java.io.File v16_5 = v10.length;
            java.util.Iterator v15_7 = 0;
            while (v15_7 < v16_5) {
                String v9 = v10[v15_7];
                System.out.println(new StringBuilder().append("LuckyPatcher: mount line - ").append(v9).toString());
                if (((v9.toLowerCase().contains("vold")) && ((v9.toLowerCase().contains("vfat")) && (!v9.toLowerCase().contains("asec")))) || (v9.toLowerCase().contains("sdcard"))) {
                    String[] v2 = v9.split("\\s+");
                    if (!v2[1].equals("on")) {
                        v14_1.add(new java.io.File(v2[1]));
                    } else {
                        v14_1.add(new java.io.File(v2[2]));
                    }
                }
                v15_7++;
            }
            java.util.Iterator v15_8 = v14_1.iterator();
            while (v15_8.hasNext()) {
                System.out.println(new StringBuilder().append("LuckyPatcher: SDcard found to ").append(((java.io.File) v15_8.next())).toString());
            }
            try {
                if (v14_1.size() == 0) {
                    v14_1.clear();
                    java.util.Iterator v15_10 = v11.iterator();
                    while (v15_10.hasNext()) {
                        com.android.vending.billing.InAppBillingService.LUCK.Mount v8_1 = ((com.android.vending.billing.InAppBillingService.LUCK.Mount) v15_10.next());
                        if ((v8_1.getDevice().toString().toLowerCase().contains("vold")) && ((v8_1.getType().toLowerCase().contains("vfat")) && (!v8_1.getMountPoint().toString().toLowerCase().contains("asec")))) {
                            v14_1.add(v8_1.getMountPoint());
                        }
                    }
                    java.util.Iterator v15_11 = v14_1.iterator();
                    while (v15_11.hasNext()) {
                        System.out.println(new StringBuilder().append("LuckyPatcher: SDcard fount to ").append(((java.io.File) v15_11.next())).toString());
                    }
                }
            } catch (Exception v5_2) {
                v5_2.printStackTrace();
            }
            return v14_1;
        }
        while(true) {
            int v7 = v6.read(v3);
            if (v7 == -1) {
                break;
            }
            v1_1.write(v3, 0, v7);
        }
        v6.close();
    }

    public static Process getSu()
    {
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.suProcess != null) {
            try {
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.suOutputStream.writeBytes("echo chelpusstart!\n");
            } catch (Exception v0) {
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.exitSu();
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.suProcess = Runtime.getRuntime().exec("su");
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.suOutputStream = new java.io.DataOutputStream(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.suProcess.getOutputStream());
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.suInputStream = new java.io.DataInputStream(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.suProcess.getInputStream());
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.suErrorInputStream = new java.io.DataInputStream(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.suProcess.getErrorStream());
            }
        } else {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.suProcess = Runtime.getRuntime().exec("su");
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.suOutputStream = new java.io.DataOutputStream(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.suProcess.getOutputStream());
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.suInputStream = new java.io.DataInputStream(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.suProcess.getInputStream());
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.suErrorInputStream = new java.io.DataInputStream(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.suProcess.getErrorStream());
        }
        return com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.suProcess;
    }

    public static void init()
    {
        if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.init) {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.init = 1;
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.instance = com.android.vending.billing.InAppBillingService.LUCK.LuckyApp.getInstance();
            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.firstrun == null) {
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.firstrun = Boolean.valueOf(1);
            }
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.luckyPackage = com.android.vending.billing.InAppBillingService.LUCK.LuckyApp.getInstance().getApplicationInfo().packageName;
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.startUnderRoot = Boolean.valueOf(0);
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.api = android.os.Build$VERSION.SDK_INT;
            try {
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.versionCodeLocal = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(com.android.vending.billing.InAppBillingService.LUCK.LuckyApp.getInstance().getPackageName(), 0).versionCode;
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.version = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(com.android.vending.billing.InAppBillingService.LUCK.LuckyApp.getInstance().getPackageName(), 0).versionName;
            } catch (android.content.pm.PackageManager$NameNotFoundException v6_0) {
                v6_0.printStackTrace();
            }
            System.out.println(new StringBuilder().append("LuckyPatcher ").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.version).append(": Application Start!").toString());
            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getBoolean("fast_start", 0)) {
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.fast_start = 1;
            }
            String v8 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getString("force_language", "default");
            try {
                if (!v8.equals("default")) {
                    java.util.Locale v2_0 = 0;
                    String[] v11 = v8.split("_");
                    if (v11.length == 1) {
                        v2_0 = new java.util.Locale(v11[0]);
                    }
                    if (v11.length == 2) {
                        v2_0 = new java.util.Locale(v11[0], v11[1], "");
                    }
                    if (v11.length == 3) {
                        v2_0 = new java.util.Locale(v11[0], v11[1], v11[2]);
                    }
                    java.util.Locale.setDefault(v2_0);
                    android.content.res.Configuration v0_1 = new android.content.res.Configuration();
                    v0_1.locale = v2_0;
                    com.android.vending.billing.InAppBillingService.LUCK.LuckyApp.getInstance().getResources().updateConfiguration(v0_1, com.android.vending.billing.InAppBillingService.LUCK.LuckyApp.getInstance().getResources().getDisplayMetrics());
                } else {
                    java.util.Locale.setDefault(java.util.Locale.getDefault());
                    android.content.res.Configuration v1_1 = new android.content.res.Configuration();
                    v1_1.locale = android.content.res.Resources.getSystem().getConfiguration().locale;
                    com.android.vending.billing.InAppBillingService.LUCK.LuckyApp.getInstance().getResources().updateConfiguration(v1_1, com.android.vending.billing.InAppBillingService.LUCK.LuckyApp.getInstance().getResources().getDisplayMetrics());
                }
            } catch (android.content.pm.PackageManager$NameNotFoundException v6_1) {
                v6_1.printStackTrace();
            }
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.boot_pat = new java.util.ArrayList();
            try {
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir = com.android.vending.billing.InAppBillingService.LUCK.LuckyApp.getInstance().getFilesDir().getAbsolutePath();
            } catch (Thread v12) {
            }
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.supath = com.android.vending.billing.InAppBillingService.LUCK.LuckyApp.getInstance().getPackageCodePath();
            int v9 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getInt("root_force", 0);
            if ((v9 == 0) && ((new java.io.File("/system/bin/su").exists()) || (new java.io.File("/system/xbin/su").exists()))) {
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su = 1;
            }
            if (v9 == 1) {
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su = 1;
            }
            if (v9 == 2) {
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su = 0;
            }
            String v4 = "";
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.extStorageDirectory = android.os.Environment.getExternalStorageDirectory().getAbsolutePath();
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.extStorageDirectory).append("/LuckyPatcher").toString();
            try {
                if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.api > 18) {
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath = com.android.vending.billing.InAppBillingService.LUCK.LuckyApp.getInstance().getExternalFilesDir("LuckyPatcher").getAbsolutePath();
                }
            } catch (android.content.pm.PackageManager$NameNotFoundException v6_2) {
                v6_2.printStackTrace();
                new java.io.File(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.extStorageDirectory).append("/Android/data/").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.luckyPackage).append("/files/LuckyPatcher").toString()).mkdirs();
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.extStorageDirectory).append("/Android/data/").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.luckyPackage).append("/files/LuckyPatcher").toString();
            }
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.extStorageDirectory).append("/LuckyPatcher").toString();
            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getBoolean("manual_path", 0)) {
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getString("basepath", com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath);
            }
            if (new java.io.File("/system/bin/dalvikvm").exists()) {
                v4 = "/system/bin/dalvikvm";
            }
            if (new java.io.File("/system/bin/dalvikvm32").exists()) {
                v4 = "/system/bin/dalvikvm32";
            }
            if ((v4.equals("")) && (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su)) {
                java.io.File v5_1 = new java.io.File(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.LuckyApp.getInstance().getFilesDir()).append("/dalvikvm").toString());
                if (v5_1.exists()) {
                    v4 = v5_1.getAbsolutePath();
                    com.chelpus.Utils.run_all(new StringBuilder().append("chmod 777 ").append(v5_1.getAbsolutePath()).toString());
                    com.chelpus.Utils.run_all(new StringBuilder().append("chown 0.0 ").append(v5_1.getAbsolutePath()).toString());
                    com.chelpus.Utils.run_all(new StringBuilder().append("chown 0:0 ").append(v5_1.getAbsolutePath()).toString());
                } else {
                    if (com.chelpus.Utils.getRawToFile(2131099651, v5_1)) {
                        com.chelpus.Utils.run_all(new StringBuilder().append("chmod 777 ").append(v5_1.getAbsolutePath()).toString());
                        com.chelpus.Utils.run_all(new StringBuilder().append("chown 0.0 ").append(v5_1.getAbsolutePath()).toString());
                        com.chelpus.Utils.run_all(new StringBuilder().append("chown 0:0 ").append(v5_1.getAbsolutePath()).toString());
                        v4 = v5_1.getAbsolutePath();
                    }
                }
            }
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.dalvikruncommand = new StringBuilder().append(v4).append(" -Xbootclasspath:").append(System.getenv("BOOTCLASSPATH")).append(" -Xverify:none -Xdexopt:none -cp ").append(com.android.vending.billing.InAppBillingService.LUCK.LuckyApp.getInstance().getPackageCodePath()).append(" com.chelpus.root.utils").toString();
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.dalvikruncommandWithFramework = new StringBuilder().append(v4).append(" -Xbootclasspath:").append(System.getenv("BOOTCLASSPATH")).append(" -Xverify:none -Xdexopt:none -cp ").append(com.android.vending.billing.InAppBillingService.LUCK.LuckyApp.getInstance().getPackageCodePath()).append(" com.android.internal.util.WithFramework").append(" com.chelpus.root.utils").toString();
            if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.runtime.equals("")) || (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.runtime == null)) {
                try {
                    String v10 = com.chelpus.Utils.checkRuntimeFromCache(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getPackageName(), 0).applicationInfo.sourceDir);
                } catch (android.content.pm.PackageManager$NameNotFoundException v6_3) {
                    v6_3.printStackTrace();
                }
                System.out.println(new StringBuilder().append("Runtime:").append(v10).toString());
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.runtime = v10;
            }
            if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su) && (!new java.io.File("/data/lp/lp_utils").exists())) {
                System.out.println("Tools not found in /data. Try create.");
                java.io.File v7_1 = new java.io.File("/data/lp/lp_utils");
                if (!v7_1.exists()) {
                    new Thread(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$1(v7_1)).start();
                }
            }
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.days = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getInt("days_on_up", 1);
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getLaunchIntent();
        }
        return;
    }

    private void installToSystem(String p2, String p3)
    {
        this.runWithWait(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$89(this, p2, p3));
        return;
    }

    public static void integrate_to_system(java.util.ArrayList p22, boolean p23, boolean p24)
    {
        if ((p22 != null) && (p22.size() > 0)) {
            java.util.Iterator v16 = p22.iterator();
            while (v16.hasNext()) {
                com.android.vending.billing.InAppBillingService.LUCK.PkgListItem v11 = ((com.android.vending.billing.InAppBillingService.LUCK.PkgListItem) v16.next());
                try {
                    String v10 = v11.pkgName;
                } catch (android.content.pm.PackageManager$NameNotFoundException v7_1) {
                    v7_1.printStackTrace();
                }
                if (v10 != null) {
                    if (!p24) {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.runToMainStatic(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$101());
                    }
                    try {
                        String v1_0 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(v10, 0).applicationInfo.sourceDir;
                        String v6 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(v10, 0).applicationInfo.dataDir;
                    } catch (android.content.pm.PackageManager$NameNotFoundException v7_0) {
                        v7_0.printStackTrace();
                    }
                    if ((!v1_0.startsWith("/data/app/")) && ((!v1_0.startsWith("/mnt/asec")) && (!v1_0.startsWith("/data/priv-app")))) {
                        if ((p23) && (!p24)) {
                            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.runToMainStatic(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$110());
                        }
                    } else {
                        java.util.ArrayList v13_1 = new java.util.ArrayList();
                        com.chelpus.Utils.getFilesWithPkgName(v10, new java.io.File("/system/app"), v13_1);
                        com.chelpus.Utils.getFilesWithPkgName(v10, new java.io.File("/system/priv-app"), v13_1);
                        if (v13_1.size() <= 0) {
                        } else {
                            if (v13_1.size() != 1) {
                                int v3 = 0;
                                int v9 = 0;
                                int v14 = 0;
                                java.io.File v2 = 0;
                                String v15_24 = v13_1.iterator();
                                while (v15_24.hasNext()) {
                                    java.io.File v12_7 = ((java.io.File) v15_24.next());
                                    String v17_69 = new com.chelpus.Utils("");
                                    String v0_47 = new String[1];
                                    String v18_16 = v0_47;
                                    v18_16[0] = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.dalvikruncommand).append(".copyFileWithBackup \'").append(v1_0).append("\' \'").append(v12_7.getAbsolutePath()).append("\'").toString();
                                    String v5_0 = v17_69.cmdRoot(v18_16);
                                    System.out.println(v5_0);
                                    if (v5_0.contains("File copied!")) {
                                        com.chelpus.Utils.run_all(new StringBuilder().append("chmod 644 \'").append(v12_7.getAbsolutePath()).append("\'").toString());
                                        com.chelpus.Utils.run_all(new StringBuilder().append("chown 0.0 \'").append(v12_7.getAbsolutePath()).append("\'").toString());
                                        com.chelpus.Utils.run_all(new StringBuilder().append("chown 0:0 \'").append(v12_7.getAbsolutePath()).append("\'").toString());
                                        com.chelpus.Utils.run_all(new StringBuilder().append("chattr -ai \'").append(v12_7.getAbsolutePath()).append("\'").toString());
                                        v3 = 1;
                                        v2 = v12_7;
                                    }
                                    if (v5_0.contains("Length of Files not equals. Destination deleted!")) {
                                        v9 = 1;
                                        if (v3 == 0) {
                                            if (!p24) {
                                                String v17_99 = new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$106;
                                                v17_99(v11);
                                                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.runToMainStatic(v17_99);
                                            } else {
                                                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toast(new StringBuilder().append(v11.name).append(" - ").append(com.chelpus.Utils.getText(2131165503)).append(": ").append(com.chelpus.Utils.getText(2131165572)).toString());
                                            }
                                        } else {
                                            com.chelpus.Utils.run_all(new StringBuilder().append("chmod 644 \'").append(v12_7.getAbsolutePath()).append("\'").toString());
                                            com.chelpus.Utils.run_all(new StringBuilder().append("chown 0.0 \'").append(v12_7.getAbsolutePath()).append("\'").toString());
                                            com.chelpus.Utils.run_all(new StringBuilder().append("chown 0:0 \'").append(v12_7.getAbsolutePath()).append("\'").toString());
                                            com.chelpus.Utils.run_all(new StringBuilder().append("chattr -ai \'").append(v12_7.getAbsolutePath()).append("\'").toString());
                                        }
                                    }
                                    if (v5_0.contains("Backup not replace original!")) {
                                        v14 = 1;
                                        if (v3 == 0) {
                                            if (!p24) {
                                                String v17_134 = new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$107;
                                                v17_134(v11);
                                                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.runToMainStatic(v17_134);
                                            } else {
                                                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toast(new StringBuilder().append(v11.name).append(" - ").append(com.chelpus.Utils.getText(2131165503)).append(": ").append(com.chelpus.Utils.getText(2131165732)).toString());
                                            }
                                        } else {
                                            com.chelpus.Utils.run_all(new StringBuilder().append("chmod 644 \'").append(v12_7.getAbsolutePath()).append("\'").toString());
                                            com.chelpus.Utils.run_all(new StringBuilder().append("chown 0.0 \'").append(v12_7.getAbsolutePath()).append("\'").toString());
                                            com.chelpus.Utils.run_all(new StringBuilder().append("chown 0:0 \'").append(v12_7.getAbsolutePath()).append("\'").toString());
                                            com.chelpus.Utils.run_all(new StringBuilder().append("chattr -ai \'").append(v12_7.getAbsolutePath()).append("\'").toString());
                                        }
                                    }
                                }
                                if ((v3 == 0) || ((v14 != 0) || (v9 != 0))) {
                                    if (v3 == 0) {
                                    } else {
                                        String v15_26 = new com.chelpus.Utils("");
                                        String v0_7 = new String[1];
                                        String v17_8 = v0_7;
                                        v17_8[0] = new StringBuilder().append("pm uninstall -k ").append(v10).toString();
                                        v15_26.cmdRoot(v17_8);
                                        String v1_1 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(v10, 0).applicationInfo.sourceDir;
                                        if (!v2.getAbsolutePath().equals(v1_1)) {
                                            com.chelpus.Utils.run_all(new StringBuilder().append("rm \'").append(v1_1).append("\'").toString());
                                            String v17_13 = new com.chelpus.Utils("");
                                            String v0_14 = new String[1];
                                            String v18_3 = v0_14;
                                            v18_3[0] = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.dalvikruncommand).append(".rename \'").append(v2.getAbsolutePath()).append("\' \'").append(v1_1).append("\'").toString();
                                            System.out.println(v17_13.cmdRoot(v18_3));
                                            com.chelpus.Utils.run_all(new StringBuilder().append("chmod 644 \'").append(v1_1).append("\'").toString());
                                            com.chelpus.Utils.run_all(new StringBuilder().append("chown 0.0 \'").append(v1_1).append("\'").toString());
                                            com.chelpus.Utils.run_all(new StringBuilder().append("chown 0:0 \'").append(v1_1).append("\'").toString());
                                            com.chelpus.Utils.run_all(new StringBuilder().append("chattr -ai \'").append(v1_1).append("\'").toString());
                                            String v15_63 = v13_1.iterator();
                                            while (v15_63.hasNext()) {
                                                java.io.File v12_1 = ((java.io.File) v15_63.next());
                                                if (!v12_1.getAbsolutePath().equals(v1_1)) {
                                                    com.chelpus.Utils.run_all(new StringBuilder().append("rm \'").append(v12_1.getAbsolutePath()).append("\'").toString());
                                                }
                                            }
                                        } else {
                                            String v15_64 = v13_1.iterator();
                                            while (v15_64.hasNext()) {
                                                java.io.File v12_3 = ((java.io.File) v15_64.next());
                                                if (!v12_3.getAbsolutePath().equals(v1_1)) {
                                                    com.chelpus.Utils.run_all(new StringBuilder().append("rm \'").append(v12_3.getAbsolutePath()).append("\'").toString());
                                                }
                                                if ((p23) && (!p24)) {
                                                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.runToMainStatic(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$111());
                                                }
                                                return;
                                            }
                                        }
                                        if ((p23) && (!p24)) {
                                            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.runToMainStatic(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$109(v11));
                                        }
                                        if (!p24) {
                                        } else {
                                            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toast(new StringBuilder().append(v11.name).append(" - ").append(com.chelpus.Utils.getText(2131165503)).append(": ").append(com.chelpus.Utils.getText(2131165749)).toString());
                                        }
                                    }
                                } else {
                                    String v15_76 = new com.chelpus.Utils("");
                                    String v0_35 = new String[1];
                                    String v17_50 = v0_35;
                                    v17_50[0] = new StringBuilder().append("pm uninstall -k ").append(v10).toString();
                                    v15_76.cmdRoot(v17_50);
                                    String v1_2 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(v10, 0).applicationInfo.sourceDir;
                                    String v15_80 = v13_1.iterator();
                                    while (v15_80.hasNext()) {
                                        java.io.File v12_5 = ((java.io.File) v15_80.next());
                                        if (!v12_5.getAbsolutePath().equals(v1_2)) {
                                            com.chelpus.Utils.run_all(new StringBuilder().append("rm \'").append(v12_5.getAbsolutePath()).append("\'").toString());
                                        }
                                    }
                                    if ((p23) && (!p24)) {
                                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.runToMainStatic(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$108(v11));
                                    }
                                    if (!p24) {
                                    } else {
                                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toast(new StringBuilder().append(v11.name).append(" - ").append(com.chelpus.Utils.getText(2131165503)).append(": ").append(com.chelpus.Utils.getText(2131165749)).toString());
                                    }
                                }
                            } else {
                                String v17_167 = new com.chelpus.Utils;
                                v17_167("");
                                String v0_58 = new String[1];
                                String v18_67 = v0_58;
                                v18_67[0] = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.dalvikruncommand).append(".copyLibsFilesToSystemBackup ").append(v10).append(" ").append(v6).append(" ").append(v1_0).append(" ").append(((java.io.File) v13_1.get(0)).getAbsolutePath()).append(" copyLibs").toString();
                                if (v17_167.cmdRoot(v18_67).contains("In /system space not found!")) {
                                    String v18_68 = new com.chelpus.Utils;
                                    v18_68("");
                                    String v0_67 = new String[1];
                                    int v19_13 = v0_67;
                                    v19_13[0] = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.dalvikruncommand).append(".copyLibsFilesToSystemBackup ").append(v10).append(" ").append(v6).append(" ").append(v1_0).append(" ").append(((java.io.File) v13_1.get(0)).getAbsolutePath()).append(" deleteBigLibs").toString();
                                    System.out.println(v18_68.cmdRoot(v19_13));
                                    if (!p24) {
                                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.runToMainStatic(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$105(v11));
                                    } else {
                                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toast(new StringBuilder().append(v11.name).append(" - ").append(com.chelpus.Utils.getText(2131165503)).append(": ").append(com.chelpus.Utils.getText(2131165572)).toString());
                                    }
                                } else {
                                    String v17_176 = new com.chelpus.Utils;
                                    v17_176("");
                                    String v0_83 = new String[1];
                                    String v18_69 = v0_83;
                                    v18_69[0] = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.dalvikruncommand).append(".copyFileWithBackup \'").append(v1_0).append("\' \'").append(((java.io.File) v13_1.get(0)).getAbsolutePath()).append("\'").toString();
                                    String v5_1 = v17_176.cmdRoot(v18_69);
                                    System.out.println(v5_1);
                                    if (v5_1.contains("File copied!")) {
                                        com.chelpus.Utils.run_all(new StringBuilder().append("chmod 644 \'").append(((java.io.File) v13_1.get(0)).getAbsolutePath()).append("\'").toString());
                                        com.chelpus.Utils.run_all(new StringBuilder().append("chown 0.0 \'").append(((java.io.File) v13_1.get(0)).getAbsolutePath()).append("\'").toString());
                                        com.chelpus.Utils.run_all(new StringBuilder().append("chown 0:0 \'").append(((java.io.File) v13_1.get(0)).getAbsolutePath()).append("\'").toString());
                                        com.chelpus.Utils.run_all(new StringBuilder().append("chattr -ai \'").append(((java.io.File) v13_1.get(0)).getAbsolutePath()).append("\'").toString());
                                        String v15_194 = new com.chelpus.Utils("");
                                        String v0_103 = new String[1];
                                        String v17_191 = v0_103;
                                        v17_191[0] = new StringBuilder().append("pm uninstall -k ").append(v10).toString();
                                        v15_194.cmdRoot(v17_191);
                                        String v18_71 = new com.chelpus.Utils;
                                        v18_71("");
                                        String v0_107 = new String[1];
                                        int v19_20 = v0_107;
                                        v19_20[0] = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.dalvikruncommand).append(".copyLibsFilesToSystemBackup ").append(v10).append(" ").append(v6).append(" ").append(v1_0).append(" ").append(((java.io.File) v13_1.get(0)).getAbsolutePath()).append(" replaceOldLibs").toString();
                                        System.out.println(v18_71.cmdRoot(v19_20));
                                        if ((p23) && (!p24)) {
                                            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.runToMainStatic(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$102(v11));
                                        }
                                        if (p24) {
                                            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toast(new StringBuilder().append(v11.name).append(" - ").append(com.chelpus.Utils.getText(2131165503)).append(": ").append(com.chelpus.Utils.getText(2131165749)).toString());
                                        }
                                    }
                                    if (v5_1.contains("Length of Files not equals. Destination deleted!")) {
                                        String v18_72 = new com.chelpus.Utils;
                                        v18_72("");
                                        String v0_123 = new String[1];
                                        int v19_21 = v0_123;
                                        v19_21[0] = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.dalvikruncommand).append(".copyLibsFilesToSystemBackup ").append(v10).append(" ").append(v6).append(" ").append(v1_0).append(" ").append(((java.io.File) v13_1.get(0)).getAbsolutePath()).append(" deleteBigLibs").toString();
                                        System.out.println(v18_72.cmdRoot(v19_21));
                                        if (!p24) {
                                            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.runToMainStatic(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$103(v11));
                                        } else {
                                            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toast(new StringBuilder().append(v11.name).append(" - ").append(com.chelpus.Utils.getText(2131165503)).append(": ").append(com.chelpus.Utils.getText(2131165572)).toString());
                                        }
                                    }
                                    if (!v5_1.contains("Backup not replace original!")) {
                                    } else {
                                        if (!p24) {
                                            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.runToMainStatic(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$104(v11));
                                        } else {
                                            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toast(new StringBuilder().append(v11.name).append(" - ").append(com.chelpus.Utils.getText(2131165503)).append(": ").append(com.chelpus.Utils.getText(2131165732)).toString());
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private void moveToSystem(String p2, String p3)
    {
        this.runWithWait(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$90(this, p2, p3));
        return;
    }

    private boolean numberCheck(Object p3)
    {
        if ((p3.toString().equals("")) || (!p3.toString().matches("\\d*"))) {
            this.showMessage(com.chelpus.Utils.getText(2131165748), com.chelpus.Utils.getText(2131165506));
            int v0_6 = 0;
        } else {
            v0_6 = 1;
        }
        return v0_6;
    }

    private void pa()
    {
        if (com.chelpus.Utils.exists("/data/data/com.maxmpz.audioplayer/databases/folders.db-wal")) {
            com.chelpus.Utils.run_all("rm /data/data/com.maxmpz.audioplayer/databases/folders.db-wal");
        }
        if (com.chelpus.Utils.exists("/data/data/com.maxmpz.audioplayer/databases/folders.db-shm")) {
            com.chelpus.Utils.run_all("rm /data/data/com.maxmpz.audioplayer/databases/folders.db-shm");
        }
        String v12 = new StringBuilder().append(android.provider.Settings$Secure.getString(this.getContext().getContentResolver(), "android_id")).append(android.os.Build.FINGERPRINT).toString();
        String v0_0 = new byte[20];
        byte[] v19 = v0_0;
        v19 = {19, -31, -111, -52, 0, 41, 12, 98, 21, 88, 14, 2, -34, 94, 24, -9, 31, 22, -95, 99};
        com.google.android.vending.licensing.AESObfuscator v18 = new com.google.android.vending.licensing.AESObfuscator;
        v18(v19, "com.maxmpz.audioplayer", v12);
        com.google.android.vending.licensing.AESObfuscator.header = "com.maxmpz.audioplayer|";
        byte[] v11 = new byte[16];
        v11 = {16, 74, 71, -80, 32, 101, -47, 72, 117, -14, 0, -29, 70, 65, -12, 74};
        String v20_1 = v18.obfuscate("2147483645|1344880878|0|1407088878", "");
        if (!com.chelpus.Utils.isWithFramework()) {
            android.database.sqlite.SQLiteDatabase v2 = 0;
            try {
                com.chelpus.Utils.copyFile("/data/data/com.maxmpz.audioplayer/databases/folders.db", new StringBuilder().append(this.getContext().getFilesDir().getAbsolutePath()).append("/folders.db").toString(), 0, 0);
                String v14 = this.getContext().getFilesDir().getAbsolutePath();
                com.chelpus.Utils.run_all(new StringBuilder().append("chmod 777 ").append(v14).append("/folders.db").toString());
            } catch (Exception v13) {
                System.out.println(new StringBuilder().append("LuckyPatcher: SQL error - ").append(v13).toString());
                v2.close();
                if (new java.io.File(new StringBuilder().append(this.getContext().getFilesDir().getAbsolutePath()).append("/folders.db").toString()).exists()) {
                    new java.io.File(new StringBuilder().append(this.getContext().getFilesDir().getAbsolutePath()).append("/folders.db").toString()).delete();
                }
            }
            if (!new java.io.File(new StringBuilder().append(v14).append("/folders.db").toString()).exists()) {
                0.close();
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str = "SU Java-Code Running!\nYou must run Poweramp before patch!\nRun Poweramp and apply custom patch again!\n\nGood Luck!\nSaNX@forpda.ru";
            } else {
                v2 = android.database.sqlite.SQLiteDatabase.openDatabase(new StringBuilder().append(v14).append("/folders.db").toString(), 0, 0);
                String v4_19 = new String[4];
                v4_19[0] = "_id";
                v4_19[1] = "path";
                v4_19[2] = "path_hash";
                v4_19[3] = "updated_at";
                android.database.Cursor v10 = v2.query("storages", v4_19, 0, 0, 0, 0, 0);
                v10.moveToFirst();
                int v21 = 0;
                while(true) {
                    String v15 = v10.getString(v10.getColumnIndex("_id"));
                    String v16 = v10.getString(v10.getColumnIndex("path"));
                    if (!v10.moveToNext()) {
                        break;
                    }
                    if (v16.length() < 4) {
                        if (v16 == null) {
                            v16 = "/mnt/sdcard";
                        }
                        System.out.println(new StringBuilder().append("4 ").append(v16).toString());
                        if (v21 == 0) {
                            v2.execSQL(new StringBuilder().append("UPDATE storages SET path=\'").append(v16).append("\',path_hash=\'").append(v20_1).append("\',updated_at=\'").append(System.currentTimeMillis()).append("\'").toString());
                        }
                        v2.close();
                        com.chelpus.Utils.copyFile(new StringBuilder().append(this.getContext().getFilesDir().getAbsolutePath()).append("/folders.db").toString(), "/data/data/com.maxmpz.audioplayer/databases/folders.db", 1, 1);
                        if (!new java.io.File(new StringBuilder().append(v14).append("/folders.db").toString()).exists()) {
                            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str = "SU Java-Code Running!\nYou must run Poweramp before patch!\nRun Poweramp and apply custom patch again!\n\nGood Luck!\nSaNX@forpda.ru";
                        } else {
                            new java.io.File(new StringBuilder().append(v14).append("/folders.db").toString()).delete();
                        }
                        java.io.File v22 = new java.io.File;
                        v22("/data/data/com.maxmpz.audioplayer/shared_prefs/l.xml");
                        java.io.File v23 = new java.io.File;
                        v23("/dbdata/databases/com.maxmpz.audioplayer/shared_prefs/l.xml");
                        if (com.chelpus.Utils.exists(v22.getAbsolutePath())) {
                            com.chelpus.Utils.run_all(new StringBuilder().append("rm ").append(v22.getAbsolutePath()).toString());
                        }
                        if (com.chelpus.Utils.exists(v23.getAbsolutePath())) {
                            com.chelpus.Utils.run_all(new StringBuilder().append("rm ").append(v23.getAbsolutePath()).toString());
                        }
                        return;
                    } else {
                        System.out.println(new StringBuilder().append("2 ").append(v16).toString());
                    }
                    if (v16 == null) {
                        v16 = "/mnt/sdcard";
                    }
                    v2.execSQL(new StringBuilder().append("UPDATE storages SET path=\'").append(v16).append("\',path_hash=\'").append(v20_1).append("\',updated_at=\'").append(System.currentTimeMillis()).append("\' WHERE _id=\'").append(v15).append("\'").toString());
                    v21 = 1;
                }
                v10.close();
            }
        } else {
            String v3_85 = new com.chelpus.Utils("");
            String v4_79 = new String[1];
            v4_79[0] = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.dalvikruncommandWithFramework).append(".pa ").append(v20_1).append(" ").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir).toString();
            String v17 = v3_85.cmdRoot(v4_79);
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.startUnderRoot = Boolean.valueOf(0);
            System.out.println(v17);
        }
    }

    private void pa2()
    {
        com.chelpus.Utils v0_1 = new com.chelpus.Utils("");
        String[] v1_2 = new String[1];
        v1_2[0] = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.dalvikruncommand).append(".pa2 1").toString();
        v0_1.cmdRoot(v1_2);
        return;
    }

    public static void patch_dialog_text_builder(android.widget.TextView p10, boolean p11)
    {
        float v0 = 0;
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str != null) {
            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("com.android.vending dependencies removed\n")) {
                p10.append(com.chelpus.Utils.getColoredText(new StringBuilder().append("\n").append(com.chelpus.Utils.getText(2131165620)).append("\n").toString(), "#ff00ff73", "bold"));
                v0 = (0 + 1065353216);
            }
            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str == null) {
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str = " ";
            }
            if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("amazon patch N1!\n")) && (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("runpatchads"))) {
                p10.append(com.chelpus.Utils.getColoredText(new StringBuilder().append("\n").append(com.chelpus.Utils.getText(2131165618)).append("\n").toString(), "#ff00ff73", "bold"));
                v0 += 1065353216;
            }
            if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("samsung patch N")) && (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("runpatchads"))) {
                p10.append(com.chelpus.Utils.getColoredText(new StringBuilder().append("\n").append(com.chelpus.Utils.getText(2131165621)).append("\n").toString(), "#ff00ff73", "bold"));
                v0 += 1065353216;
            }
            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("Site from AdsBlockList blocked!")) {
                int v2_0 = 0;
                while (java.util.regex.Pattern.compile("Site from AdsBlockList blocked!").matcher(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str).find()) {
                    v2_0++;
                }
                p10.append(com.chelpus.Utils.getColoredText(new StringBuilder().append("\n").append(com.chelpus.Utils.getText(2131165673)).append(" (").append(v2_0).append(") ").append("\n").toString(), "#ff00ff73", "bold"));
                v0 += 1065353216;
            }
            if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("ads1 Fixed!\n")) || (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("support1 Fixed!\n"))) {
                p10.append(com.chelpus.Utils.getColoredText(new StringBuilder().append("\n").append(com.chelpus.Utils.getText(2131165604)).append("\n").toString(), "#ff00ff73", "bold"));
                v0 += 1065353216;
            }
            if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("ads2 Fixed!\n")) || (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("support2 Fixed!\n"))) {
                p10.append(com.chelpus.Utils.getColoredText(new StringBuilder().append("\n").append(com.chelpus.Utils.getText(2131165606)).append("\n").toString(), "#ff00ff73", "bold"));
                v0 += 1065353216;
            }
            if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("ads3 Fixed!\n")) || (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("support3 Fixed!\n"))) {
                p10.append(com.chelpus.Utils.getColoredText(new StringBuilder().append("\n").append(com.chelpus.Utils.getText(2131165608)).append("\n").toString(), "#ff00ff73", "bold"));
                v0 += 1065353216;
            }
            if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("ads4 Fixed!\n")) || (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("support4 Fixed!\n"))) {
                p10.append(com.chelpus.Utils.getColoredText(new StringBuilder().append("\n").append(com.chelpus.Utils.getText(2131165610)).append("\n").toString(), "#ff00ff73", "bold"));
                v0 += 1065353216;
            }
            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("ads5 Fixed!\n")) {
                p10.append(com.chelpus.Utils.getColoredText(new StringBuilder().append("\n").append(com.chelpus.Utils.getText(2131165612)).append("\n").toString(), "#ff00ff73", "bold"));
                v0 += 1065353216;
            }
            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("ads6 Fixed!\n")) {
                p10.append(com.chelpus.Utils.getColoredText(new StringBuilder().append("\n").append(com.chelpus.Utils.getText(2131165614)).append("\n").toString(), "#ff00ff73", "bold"));
                v0 += 1065353216;
            }
            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("lvl patch N1!\n")) {
                int v2_1 = 0;
                while (java.util.regex.Pattern.compile("lvl patch N1!\n").matcher(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str).find()) {
                    v2_1++;
                }
                p10.append(com.chelpus.Utils.getColoredText(new StringBuilder().append("\n").append(com.chelpus.Utils.getText(2131165604)).append(" (").append(v2_1).append(")").append("\n").toString(), "#ff00ff73", "bold"));
                v0 += 1065353216;
            }
            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("lvl patch N2!\n")) {
                int v2_2 = 0;
                while (java.util.regex.Pattern.compile("lvl patch N2!\n").matcher(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str).find()) {
                    v2_2++;
                }
                p10.append(com.chelpus.Utils.getColoredText(new StringBuilder().append("\n").append(com.chelpus.Utils.getText(2131165606)).append(" (").append(v2_2).append(")").append("\n").toString(), "#ff00ff73", "bold"));
                v0 += 1065353216;
            }
            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("lvl patch N3!\n")) {
                int v2_3 = 0;
                while (java.util.regex.Pattern.compile("lvl patch N3!\n").matcher(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str).find()) {
                    v2_3++;
                }
                p10.append(com.chelpus.Utils.getColoredText(new StringBuilder().append("\n").append(com.chelpus.Utils.getText(2131165608)).append(" (").append(v2_3).append(")").append("\n").toString(), "#ff00ff73", "bold"));
                v0 += 1065353216;
            }
            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("lvl patch N4!\n")) {
                int v2_4 = 0;
                while (java.util.regex.Pattern.compile("lvl patch N4!\n").matcher(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str).find()) {
                    v2_4++;
                }
                p10.append(com.chelpus.Utils.getColoredText(new StringBuilder().append("\n").append(com.chelpus.Utils.getText(2131165610)).append(" (").append(v2_4).append(")").append("\n").toString(), "#ff00ff73", "bold"));
                v0 += 1065353216;
            }
            int v2_5 = 0;
            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("lvl patch N5!\n")) {
                while (java.util.regex.Pattern.compile("lvl patch N5!\n").matcher(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str).find()) {
                    v2_5++;
                }
                p10.append(com.chelpus.Utils.getColoredText(new StringBuilder().append("\n").append(com.chelpus.Utils.getText(2131165612)).append(" (").append(v2_5).append(")").append("\n").toString(), "#ff00ff73", "bold"));
                v0 += 1065353216;
            }
            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("lvl patch N6!\n")) {
                int v3_0 = 0;
                while (java.util.regex.Pattern.compile("lvl patch N6!\n").matcher(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str).find()) {
                    v3_0++;
                }
                p10.append(com.chelpus.Utils.getColoredText(new StringBuilder().append("\n").append(com.chelpus.Utils.getText(2131165614)).append(" (").append(v3_0).append(")").append("\n").toString(), "#ff00ff73", "bold"));
                v0 += 1065353216;
            }
            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("lvl patch N7!\n")) {
                int v3_1 = 0;
                while (java.util.regex.Pattern.compile("lvl patch N7!\n").matcher(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str).find()) {
                    v3_1++;
                }
                p10.append(com.chelpus.Utils.getColoredText(new StringBuilder().append("\n").append(com.chelpus.Utils.getText(2131165616)).append(" (").append(v3_1).append(")").append("\n").toString(), "#ff00ff73", "bold"));
                v0 += 1065353216;
            }
            if ((!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("amazon patch N1!\n")) && ((!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("runpatchads")) && (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("runpatchsupport")))) {
                p10.append(com.chelpus.Utils.getColoredText(new StringBuilder().append("\n").append(com.chelpus.Utils.getText(2131165619)).append("\n").toString(), "#ffff0055", "bold"));
            }
            if ((!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("samsung patch N")) && ((!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("runpatchads")) && (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("runpatchsupport")))) {
                p10.append(com.chelpus.Utils.getColoredText(new StringBuilder().append("\n").append(com.chelpus.Utils.getText(2131165622)).append("\n").toString(), "#ffff0055", "bold"));
            }
            if ((!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("lvl patch N1!\n")) && ((!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("ads1 Fixed!\n")) && (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("support1 Fixed!\n")))) {
                p10.append(com.chelpus.Utils.getColoredText(new StringBuilder().append("\n").append(com.chelpus.Utils.getText(2131165605)).append("\n").toString(), "#ffff0055", "bold"));
            }
            if ((!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("lvl patch N2!\n")) && ((!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("ads2 Fixed!\n")) && (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("support2 Fixed!\n")))) {
                p10.append(com.chelpus.Utils.getColoredText(new StringBuilder().append("\n").append(com.chelpus.Utils.getText(2131165607)).append("\n").toString(), "#ffff0055", "bold"));
            }
            if ((!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("lvl patch N3!\n")) && ((!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("ads3 Fixed!\n")) && (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("support3 Fixed!\n")))) {
                p10.append(com.chelpus.Utils.getColoredText(new StringBuilder().append("\n").append(com.chelpus.Utils.getText(2131165609)).append("\n").toString(), "#ffff0055", "bold"));
            }
            if ((!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("lvl patch N4!\n")) && ((!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("ads4 Fixed!\n")) && (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("support4 Fixed!\n")))) {
                p10.append(com.chelpus.Utils.getColoredText(new StringBuilder().append("\n").append(com.chelpus.Utils.getText(2131165611)).append("\n").toString(), "#ffff0055", "bold"));
            }
            if ((!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("lvl patch N5!\n")) && ((!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("runpatchads")) && (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("runpatchsupport")))) {
                p10.append(com.chelpus.Utils.getColoredText(new StringBuilder().append("\n").append(com.chelpus.Utils.getText(2131165613)).append("\n").toString(), "#ffff0055", "bold"));
            }
            if ((!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("lvl patch N6!\n")) && ((!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("runpatchads")) && (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("runpatchsupport")))) {
                p10.append(com.chelpus.Utils.getColoredText(new StringBuilder().append("\n").append(com.chelpus.Utils.getText(2131165615)).append("\n").toString(), "#ffff0055", "bold"));
            }
            if ((!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("lvl patch N7!\n")) && ((!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("runpatchads")) && (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("runpatchsupport")))) {
                p10.append(com.chelpus.Utils.getColoredText(new StringBuilder().append("\n").append(com.chelpus.Utils.getText(2131165617)).append("\n").toString(), "#ffff0055", "bold"));
            }
            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("Error: Program files are not found!")) {
                p10.append(com.chelpus.Utils.getColoredText(new StringBuilder().append("\n").append(com.chelpus.Utils.getText(2131165185)).append("\n").toString(), "#ffff0055", "bold"));
            }
            if (p11) {
                p10.append(com.chelpus.Utils.getColoredText(new StringBuilder().append("\n").append(com.chelpus.Utils.getText(2131165792)).append("\n").toString(), "#fff0e442", "bold"));
            } else {
                if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("Error:")) || ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("Fixed")) || ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("Failed")) || ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("ads")) || ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("lvl patch")) || (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.contains("SU Java-Code Running!"))))))) {
                    int v6_0 = ((float) (Math.round(((v0 / 1088421888) * 1120403456)) - 1));
                    String v7_28 = new StringBuilder().append("\n").append(com.chelpus.Utils.getText(2131165478)).append(" ").append(v6_0).append(com.chelpus.Utils.getText(2131165477)).append("\n").toString();
                    if (v6_0 < 0) {
                        v7_28 = new StringBuilder().append("\n").append(com.chelpus.Utils.getText(2131165478)).append(" ").append(0).append(com.chelpus.Utils.getText(2131165628)).toString();
                    }
                    p10.append(com.chelpus.Utils.getColoredText(v7_28, "#fff0e442", "bold"));
                } else {
                    p10.setText(com.chelpus.Utils.getColoredText(new StringBuilder().append("\n").append(com.chelpus.Utils.getText(2131165567)).append("\n").toString(), "#ff00ffff", "bold"));
                }
            }
        }
        return;
    }

    private void populateAdapter(java.util.ArrayList p10, java.util.Comparator p11)
    {
        if ((p10 != null) && (p10.size() != 0)) {
            int v0;
            java.util.Collections.sort(p10, p11);
            if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getBoolean("no_icon", 0)) {
                v0 = 2130968626;
            } else {
                v0 = 2130968627;
            }
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia = new com.android.vending.billing.InAppBillingService.LUCK.PkgListItemAdapter(this.getContext(), v0, com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getInt("viewsize", 0), p10);
            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.menu_adapter == null) {
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.menu_adapter = new com.android.vending.billing.InAppBillingService.LUCK.MenuItemAdapter(this.getContext(), com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getInt("viewsize", 0), new java.util.ArrayList());
            }
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adapter_boot = new com.android.vending.billing.InAppBillingService.LUCK.BootListItemAdapter(this.getContext(), 2130968593, com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getInt("viewsize", 0), com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.boot_pat);
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.sorter = p11;
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(3);
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.lv.invalidate();
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.menu_lv.invalidate();
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.menu_lv.setGroupIndicator(0);
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.menu_lv.setAdapter(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.menu_adapter);
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.menu_lv.setOnGroupClickListener(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$37(this));
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.menu_lv.setOnChildClickListener(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$38(this));
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.lv.setAdapter(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia);
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.lv.setOnChildClickListener(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$39(this));
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.lv.setDividerHeight(2);
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.lv.setGroupIndicator(0);
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.lv.setClickable(1);
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.lv.setLongClickable(1);
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.lv.setOnGroupExpandListener(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$40(this));
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.lv.setOnGroupClickListener(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$41(this));
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.lv.setOnItemLongClickListener(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$42(this));
        } else {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.lv.invalidate();
        }
        return;
    }

    public static void removeDialogLP(int p2)
    {
        try {
            switch (p2) {
                case 1:
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.progress == null) {
                        return;
                    } else {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.progress.dismiss();
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.progress = 0;
                        return;
                    }
                case 2:
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patch_d == null) {
                        return;
                    } else {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patch_d.dismiss();
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patch_d = 0;
                        return;
                    }
                case 4:
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.c_p_d == null) {
                        return;
                    } else {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.c_p_d.dismiss();
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.c_p_d = 0;
                        return;
                    }
                case 6:
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.app_d == null) {
                        return;
                    } else {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.app_d.dismiss();
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.app_d = 0;
                        return;
                    }
                case 7:
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.menu_d == null) {
                        return;
                    } else {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.menu_d.dismiss();
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.menu_d = 0;
                        return;
                    }
                case 8:
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.c_a_d == null) {
                        return;
                    } else {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.c_a_d.dismiss();
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.c_a_d = 0;
                        return;
                    }
                case 11:
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.progress2 == null) {
                        return;
                    } else {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.progress2.dismiss();
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.progress2 = 0;
                        return;
                    }
                case 15:
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.c2_d == null) {
                        return;
                    } else {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.c2_d.dismiss();
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.c2_d = 0;
                        return;
                    }
                case 23:
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.progress_loading == null) {
                        return;
                    } else {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.progress_loading.dismiss();
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.progress_loading = 0;
                        return;
                    }
                case 38:
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.menu_d_static == null) {
                        return;
                    } else {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.menu_d_static.dismiss();
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.menu_d_static = 0;
                        return;
                    }
                default:
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.all_d == null) {
                        return;
                    } else {
                        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.all_d.dialog_int == p2) {
                            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.all_d.dismiss();
                        }
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.all_d = 0;
                        return;
                    }
            }
        } catch (Exception v0_1) {
        } catch (Exception v0_0) {
            v0_0.printStackTrace();
            return;
        }
        v0_1.printStackTrace();
        return;
    }

    public static void runToMainStatic(Runnable p1)
    {
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag == null) {
            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.handler != null) {
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.handler.post(p1);
            }
        } else {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchAct.runOnUiThread(p1);
        }
        return;
    }

    public static void showDialogLP(int p4)
    {
        try {
            switch (p4) {
                case 1:
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.progress == null) {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.progress = com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog.newInstance();
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.progress.showDialog();
                        return;
                    } else {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.progress.dismiss();
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.progress = 0;
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.progress = com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog.newInstance();
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.progress.showDialog();
                        return;
                    }
                case 2:
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patch_d != null) {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patch_d.dismiss();
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patch_d = 0;
                    }
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patch_d = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.Patch_Dialog();
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patch_d.showDialog();
                    return;
                case 4:
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.c_p_d != null) {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.c_p_d.dismiss();
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.c_p_d = 0;
                    }
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.c_p_d = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.Custom_Patch_Dialog();
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.c_p_d.showDialog();
                    return;
                case 5:
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.e_p_d != null) {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.e_p_d.dismiss();
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.e_p_d = 0;
                    }
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.e_p_d = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.Ext_Patch_Dialog();
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.e_p_d.showDialog();
                    return;
                case 6:
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.app_d != null) {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.app_d.dismiss();
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.app_d = 0;
                    }
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.app_d = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.App_Dialog();
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.app_d.showDialog();
                    return;
                case 7:
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.menu_d != null) {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.menu_d.dismiss();
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.menu_d = 0;
                    }
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.menu_d = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.Menu_Dialog();
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.menu_d.showDialog();
                    return;
                case 8:
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.c_a_d != null) {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.c_a_d.dismiss();
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.c_a_d = 0;
                    }
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.c_a_d = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.Create_Apk_Dialog();
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.c_a_d.showDialog();
                    return;
                case 9:
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.c_a_a_d != null) {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.c_a_a_d.dismiss();
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.c_a_a_d = 0;
                    }
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.c_a_a_d = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.Create_Apk_Ads_Dialog();
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.c_a_a_d.showDialog();
                    return;
                case 11:
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.progress2 == null) {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.progress2 = com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog_2.newInstance();
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.progress2.showDialog();
                        return;
                    } else {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.progress2.dismiss();
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.progress2 = 0;
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.progress2 = com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog_2.newInstance();
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.progress2.showDialog();
                        return;
                    }
                case 15:
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.c2_d != null) {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.c2_d.dismiss();
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.c2_d = 0;
                    }
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.c2_d = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.Custom2_Dialog();
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.c2_d.showDialog();
                    return;
                case 23:
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.progress_loading == null) {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.progress_loading = com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog_Loading.newInstance();
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.progress_loading.showDialog();
                        return;
                    } else {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.progress_loading.dismiss();
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.progress_loading = 0;
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.progress_loading = com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog_Loading.newInstance();
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.progress_loading.showDialog();
                        return;
                    }
                case 30:
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.m_i_d != null) {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.m_i_d.dismiss();
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.m_i_d = 0;
                    }
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.m_i_d = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.Market_Install_Dialog();
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.m_i_d.showDialog();
                    return;
                case 36:
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.c_a_s_d != null) {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.c_a_s_d.dismiss();
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.c_a_s_d = 0;
                    }
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.c_a_s_d = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.Create_Apk_Support_Dialog();
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.c_a_s_d.showDialog();
                    return;
                case 38:
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.menu_d_static != null) {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.menu_d_static.dismiss();
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.menu_d_static = 0;
                    }
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.menu_d_static = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.Menu_Dialog_Static();
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.menu_d_static.showDialog();
                    return;
                default:
                    System.out.println(new StringBuilder().append("dialog ").append(p4).toString());
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.all_d != null) {
                        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.all_d.isVisible()) {
                            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.all_d.dismiss();
                        }
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.all_d = 0;
                    }
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.all_d = new com.android.vending.billing.InAppBillingService.LUCK.dialogs.All_Dialogs();
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.dialog_int = p4;
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.all_d.dialog_int = p4;
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.all_d.showDialog();
                    return;
            }
        } catch (Exception v0_1) {
        } catch (Exception v0_0) {
            v0_0.printStackTrace();
            return;
        }
        v0_1.printStackTrace();
        return;
    }

    private void showMessageInfo(android.content.Context p20, String p21, String p22)
    {
        android.app.Dialog v8_1 = new android.app.Dialog(p20);
        android.os.StatFs v13_1 = new android.os.StatFs("/system");
        int v6 = ((v13_1.getBlockCount() * v13_1.getBlockSize()) / 1048576);
        int v4 = ((v13_1.getAvailableBlocks() * v13_1.getBlockSize()) / 1048576);
        int v2 = (v6 - v4);
        android.os.StatFs v14_1 = new android.os.StatFs("/data");
        int v7 = ((v14_1.getBlockCount() * v14_1.getBlockSize()) / 1048576);
        int v5 = ((v14_1.getAvailableBlocks() * v14_1.getBlockSize()) / 1048576);
        int v3 = (v6 - v4);
        v8_1.setCancelable(1);
        v8_1.setTitle(p21);
        v8_1.setContentView(2130968617);
        ((android.widget.TextView) v8_1.findViewById(2131558566)).setText(p22);
        ((android.widget.TextView) v8_1.findViewById(2131558559)).setText(new StringBuilder().append("System ROM (/system): ").append(v6).append("Mb (").append(v4).append("Mb free)").toString());
        android.widget.ProgressBar v11_1 = ((android.widget.ProgressBar) v8_1.findViewById(2131558560));
        v11_1.setMax(v6);
        v11_1.incrementProgressBy(v2);
        ((android.widget.TextView) v8_1.findViewById(2131558561)).setText(new StringBuilder().append("Internal SD (/data): ").append(v7).append("Mb (").append(v5).append("Mb free)").toString());
        android.widget.ProgressBar v12_1 = ((android.widget.ProgressBar) v8_1.findViewById(2131558562));
        v12_1.setMax(v7);
        v12_1.incrementProgressBy(v3);
        android.widget.Button v10_1 = ((android.widget.Button) v8_1.findViewById(2131558564));
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$121 v17_34 = new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$121;
        v17_34(this, v8_1);
        v10_1.setOnClickListener(v17_34);
        com.chelpus.Utils.showDialog(v8_1);
        return;
    }

    private static void showMessageInfoStatic(android.content.Context p18, String p19, String p20)
    {
        android.app.Dialog v7_1 = new android.app.Dialog(p18);
        android.os.StatFs v12_1 = new android.os.StatFs("/system");
        int v5 = ((v12_1.getBlockCount() * v12_1.getBlockSize()) / 1048576);
        int v3 = ((v12_1.getAvailableBlocks() * v12_1.getBlockSize()) / 1048576);
        int v1 = (v5 - v3);
        android.os.StatFs v13_1 = new android.os.StatFs("/data");
        int v6 = ((v13_1.getBlockCount() * v13_1.getBlockSize()) / 1048576);
        int v4 = ((v13_1.getAvailableBlocks() * v13_1.getBlockSize()) / 1048576);
        int v2 = (v5 - v3);
        v7_1.setCancelable(1);
        v7_1.setTitle(p19);
        v7_1.setContentView(2130968617);
        ((android.widget.TextView) v7_1.findViewById(2131558566)).setText(p20);
        ((android.widget.TextView) v7_1.findViewById(2131558559)).setText(new StringBuilder().append("System ROM (/system): ").append(v5).append("Mb (").append(v3).append("Mb free)").toString());
        android.widget.ProgressBar v10_1 = ((android.widget.ProgressBar) v7_1.findViewById(2131558560));
        v10_1.setMax(v5);
        v10_1.incrementProgressBy(v1);
        ((android.widget.TextView) v7_1.findViewById(2131558561)).setText(new StringBuilder().append("Internal SD (/data): ").append(v6).append("Mb (").append(v4).append("Mb free)").toString());
        android.widget.ProgressBar v11_1 = ((android.widget.ProgressBar) v7_1.findViewById(2131558562));
        v11_1.setMax(v6);
        v11_1.incrementProgressBy(v2);
        android.widget.Button v9_1 = ((android.widget.Button) v7_1.findViewById(2131558564));
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$123 v16_34 = new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$123;
        v16_34(v7_1);
        v9_1.setOnClickListener(v16_34);
        com.chelpus.Utils.showDialog(v7_1);
        return;
    }

    public static void showMessageStatic(String p1, String p2)
    {
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.runToMainStatic(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$122(p1, p2));
        return;
    }

    private void showNotify(int p15, String p16, String p17, String p18)
    {
        if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getBoolean("hide_notify", 0)) {
            long v9 = System.currentTimeMillis();
            android.support.v4.app.FragmentActivity v4 = this.getContext();
            android.app.PendingIntent v1 = android.app.PendingIntent.getActivity(this.getContext(), 0, new android.content.Intent(this.getContext(), com.android.vending.billing.InAppBillingService.LUCK.patchActivity), 0);
            android.app.NotificationManager v8_1 = ((android.app.NotificationManager) this.getContext().getSystemService("notification"));
            android.app.Notification v6_1 = new android.app.Notification(2130837552, p17, v9);
            new android.app.Notification();
            v6_1.setLatestEventInfo(v4, p16, p18, v1);
            v8_1.notify(p15, v6_1);
        }
        return;
    }

    public static void toast(String p2)
    {
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.handlerToast != null) {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.handlerToast.post(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$3(p2));
        }
        return;
    }

    public static void unzip(java.io.File p21)
    {
        int v7 = 0;
        int v8 = 0;
        try {
            java.io.FileInputStream v6_1 = new java.io.FileInputStream(p21);
            java.util.zip.ZipInputStream v16 = new java.util.zip.ZipInputStream;
            v16(v6_1);
            java.util.zip.ZipEntry v15 = v16.getNextEntry();
        } catch (Exception v4) {
            try {
                net.lingala.zip4j.core.ZipFile v17 = new net.lingala.zip4j.core.ZipFile;
                v17(p21);
                v17.extractFile("classes.dex", com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getFilesDir().getAbsolutePath());
                v17.extractFile("AndroidManifest.xml", com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getFilesDir().getAbsolutePath());
            } catch (Exception v5_0) {
                System.out.println(new StringBuilder().append("Error classes.dex decompress! ").append(v5_0).toString());
                System.out.println(new StringBuilder().append("Exception e1").append(v4.toString()).toString());
            } catch (Exception v5_1) {
                System.out.println(new StringBuilder().append("Error classes.dex decompress! ").append(v5_1).toString());
                System.out.println(new StringBuilder().append("Exception e1").append(v4.toString()).toString());
            }
            System.out.println(new StringBuilder().append("Exception e").append(v4.toString()).toString());
            return;
        }
        while ((v15 != null) && (1 != 0)) {
            String v11 = v15.getName();
            if (v11.equals("classes.dex")) {
                java.io.FileOutputStream v9_1 = new java.io.FileOutputStream(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getFilesDir().getAbsolutePath()).append("/").append("classes.dex").toString());
                byte[] v2 = new byte[2048];
                while(true) {
                    int v12 = v16.read(v2);
                    if (v12 == -1) {
                        break;
                    }
                    v9_1.write(v2, 0, v12);
                }
                v16.closeEntry();
                v9_1.close();
                v7 = 1;
            }
            if (v11.equals("AndroidManifest.xml")) {
                java.io.FileOutputStream v10_1 = new java.io.FileOutputStream(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getFilesDir().getAbsolutePath()).append("/").append("AndroidManifest.xml").toString());
                byte[] v3 = new byte[2048];
                while(true) {
                    int v13 = v16.read(v3);
                    if (v13 == -1) {
                        break;
                    }
                    v10_1.write(v3, 0, v13);
                }
                v16.closeEntry();
                v10_1.close();
                v8 = 1;
            }
            if ((v7 == 0) || (v8 == 0)) {
                v15 = v16.getNextEntry();
            } else {
                break;
            }
        }
        v16.close();
        v6_1.close();
        return;
    }

    private void updateapp()
    {
        Thread v0_1 = new Thread(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$5(this));
        v0_1.setPriority(1);
        v0_1.start();
        return;
    }

    public static void zip(String p13, String p14)
    {
        try {
            java.util.zip.ZipOutputStream v9_1 = new java.util.zip.ZipOutputStream(new java.io.BufferedOutputStream(new java.io.FileOutputStream(new StringBuilder().append(p14).append("/").append(p13).toString(), 0)));
            byte[] v2 = new byte[4096];
            java.io.FileInputStream v6_1 = new java.io.FileInputStream(new StringBuilder().append(p14).append("/classes.dex").toString());
            java.io.BufferedInputStream v8_1 = new java.io.BufferedInputStream(v6_1, 4096);
        } catch (Exception v4) {
            v4.printStackTrace();
            new java.io.File(new StringBuilder().append(p14).append("/AndroidManifest.xml").toString()).delete();
            new java.io.File(new StringBuilder().append(p14).append("/classes.dex").toString()).delete();
            return;
        }
        try {
            v9_1.putNextEntry(new java.util.zip.ZipEntry("classes.dex"));
        } catch (Exception v4) {
        }
        while(true) {
            int v1 = v8_1.read(v2, 0, 4096);
            if (v1 == -1) {
                break;
            }
            v9_1.write(v2, 0, v1);
        }
        v8_1.close();
        v9_1.closeEntry();
        v9_1.close();
        v6_1.close();
        new java.io.File(new StringBuilder().append(p14).append("/AndroidManifest.xml").toString()).delete();
        new java.io.File(new StringBuilder().append(p14).append("/classes.dex").toString()).delete();
        return;
    }

    public void addIgnoreOdex(com.android.vending.billing.InAppBillingService.LUCK.PkgListItem p9, String p10)
    {
        try {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str = "";
            String v1 = "not_system";
        } catch (com.android.vending.billing.InAppBillingService.LUCK.PatchData v3) {
            return;
        }
        if (p9.system) {
            v1 = "system";
        }
        String v2 = String.valueOf(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(p9.pkgName, 0).applicationInfo.uid);
        String v0 = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.dalvikruncommand).append(".odexrunpatch ").append(p9.pkgName).append(" ").append(p10).append(" ").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(p9.pkgName, 0).applicationInfo.sourceDir).append(" ").append(this.getContext().getFilesDir()).append(" ").append(v1).append(" ").append("a").append(" ").append(com.chelpus.Utils.getCurrentRuntimeValue()).append(" ").append(v2).append("\n").toString();
        if (p10.contains("copyDC")) {
            v0 = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.dalvikruncommand).append(".odexrunpatch ").append(p9.pkgName).append(" ").append(p10).append(" ").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(p9.pkgName, 0).applicationInfo.sourceDir).append(" ").append(this.getContext().getFilesDir()).append(" ").append(v1).append(" ").append("copyDC").append(" ").append(com.chelpus.Utils.getCurrentRuntimeValue()).append(" ").append(v2).append("\n").toString();
        }
        System.out.println(v0);
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchData = new com.android.vending.billing.InAppBillingService.LUCK.PatchData();
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchData.pli = p9;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchData.typePatch = "LVL";
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchData.appdir = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(p9.pkgName, 0).applicationInfo.sourceDir;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchData.pkg = p9.pkgName;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchData.system_app = v1;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchData.filesdir = this.getContext().getFilesDir().getAbsolutePath();
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchData.uid = String.valueOf(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(p9.pkgName, 0).applicationInfo.uid);
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchData.cmd1 = v0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchData.cmd2 = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.dalvikruncommand).append(".checkOdex ").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(p9.pkgName, 0).applicationInfo.sourceDir).append(" ").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getFilesDir()).append("\n").toString();
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchData.result = p10;
        this.LvlAdsPatch();
        return;
    }

    public void ads(com.android.vending.billing.InAppBillingService.LUCK.PkgListItem p9, String p10)
    {
        try {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str = "";
            String v1 = "not_system";
        } catch (com.android.vending.billing.InAppBillingService.LUCK.PatchData v3) {
            return;
        }
        if (p9.system) {
            v1 = "system";
        }
        String v2 = String.valueOf(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(p9.pkgName, 0).applicationInfo.uid);
        String v0 = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.dalvikruncommand).append(".runpatchads ").append(p9.pkgName).append(" ").append(p10).append(" ").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(p9.pkgName, 0).applicationInfo.sourceDir).append(" ").append(this.getContext().getFilesDir()).append(" ").append(v1).append(" ").append("a").append(" ").append(com.chelpus.Utils.getCurrentRuntimeValue()).append(" \'").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).append("/AdsBlockList.txt\'").append(" ").append(v2).toString();
        System.out.println(p10);
        if (p10.contains("copyDC")) {
            v0 = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.dalvikruncommand).append(".runpatchads ").append(p9.pkgName).append(" ").append(p10).append(" ").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(p9.pkgName, 0).applicationInfo.sourceDir).append(" ").append(this.getContext().getFilesDir()).append(" ").append(v1).append(" ").append("copyDC").append(" ").append(com.chelpus.Utils.getCurrentRuntimeValue()).append(" \'").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).append("/AdsBlockList.txt\'").append(" ").append(v2).toString();
            System.out.println(v0);
        }
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchData = new com.android.vending.billing.InAppBillingService.LUCK.PatchData();
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchData.pli = p9;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchData.typePatch = "ADS";
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchData.appdir = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(p9.pkgName, 0).applicationInfo.sourceDir;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchData.pkg = p9.pkgName;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchData.system_app = v1;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchData.filesdir = this.getContext().getFilesDir().getAbsolutePath();
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchData.uid = String.valueOf(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(p9.pkgName, 0).applicationInfo.uid);
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchData.cmd1 = v0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchData.cmd2 = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.dalvikruncommand).append(".checkOdex ").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(p9.pkgName, 0).applicationInfo.sourceDir).append(" ").append(this.getContext().getFilesDir()).toString();
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchData.result = p10;
        this.LvlAdsPatch();
        return;
    }

    public boolean apkpermissions(com.android.vending.billing.InAppBillingService.LUCK.PkgListItem p3, java.util.ArrayList p4, java.util.ArrayList p5)
    {
        this.permissions = p4;
        this.activities = p5;
        if (this.testSD(1)) {
            this.runWithWait(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$115(this));
        }
        return 1;
    }

    public boolean apkpermissions_safe(com.android.vending.billing.InAppBillingService.LUCK.PkgListItem p3, java.util.ArrayList p4, java.util.ArrayList p5)
    {
        this.permissions = p4;
        this.activities = p5;
        if (this.testSD(1)) {
            this.runWithWait(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$116(this));
        }
        return 1;
    }

    public void app_scanning()
    {
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia != null) {
            Thread v0_1 = new Thread(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$AppScanning(this));
            v0_1.setPriority(10);
            v0_1.start();
        }
        return;
    }

    public String backup(com.android.vending.billing.InAppBillingService.LUCK.PkgListItem p11)
    {
        String v2 = "";
        try {
            if (this.testSD(1)) {
                new java.io.File(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).append("/Backup/").toString()).mkdirs();
                String v0;
                if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getInt("apkname", 0) == 0) {
                    v0 = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).append("/Backup/").append(p11.name).append(".ver.").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(p11.pkgName, 0).versionName.replaceAll(" ", ".")).append(".build.").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(p11.pkgName, 0).versionCode).append(".apk").toString();
                } else {
                    v0 = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).append("/Backup/").append(p11.pkgName).append(".ver.").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(p11.pkgName, 0).versionName.replaceAll(" ", ".")).append(".build.").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(p11.pkgName, 0).versionCode).append(".apk").toString();
                }
                if (new java.io.File(v0).exists()) {
                    new java.io.File(v0).delete();
                }
                try {
                    com.chelpus.Utils.copyFile(new java.io.File(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(p11.pkgName, 0).applicationInfo.sourceDir), new java.io.File(v0));
                } catch (Exception v1_0) {
                    com.chelpus.Utils.copyFile(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(p11.pkgName, 0).applicationInfo.sourceDir, v0, 0, 0);
                    v1_0.printStackTrace();
                }
                if (!new java.io.File(v0).exists()) {
                    String v3_41 = new com.chelpus.Utils("");
                    String v4_42 = new String[1];
                    v4_42[0] = new StringBuilder().append("dd if=").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(p11.pkgName, 0).applicationInfo.sourceDir).append(" of=").append(v0).toString();
                    v3_41.cmdRoot(v4_42);
                    com.chelpus.Utils.run_all(new StringBuilder().append("chmod 777 ").append(v0).toString());
                }
                if (!new java.io.File(v0).exists()) {
                    this.showMessage(com.chelpus.Utils.getText(2131165748), com.chelpus.Utils.getText(2131165432));
                } else {
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.runToMain(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$91(this));
                    v2 = v0;
                }
            }
        } catch (Exception v1_1) {
            v1_1.printStackTrace();
            this.runToMain(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$92(this));
        } catch (Exception v1_2) {
            v1_2.printStackTrace();
        }
        return v2;
    }

    public void backup_data()
    {
        if (this.testSD(1)) {
            java.io.File v0_1 = new java.io.File(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).append("/Backup/Data/").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName).toString());
            v0_1.mkdirs();
            this.runWithWait(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$94(this, v0_1));
        }
        return;
    }

    public void backupselector()
    {
        java.util.ArrayList v2_1 = new java.util.ArrayList();
        String[] v1 = new java.io.File(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).append("/Backup").toString()).list();
        android.support.v4.app.FragmentActivity v4_5 = v1.length;
        int v3_2 = 0;
        while (v3_2 < v4_5) {
            String v0 = v1[v3_2];
            if ((new java.io.File(v0).getName().startsWith(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName).append(".ver").toString())) || ((new java.io.File(v0).getName().startsWith(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.name).append(".ver").toString())) || (new java.io.File(v0).getName().equals(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName).append(".apk").toString())))) {
                v2_1.add(new java.io.File(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).append("/Backup/").append(v0).toString()));
            }
            v3_2++;
        }
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adapt = new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$78(this, this.getContext(), 2130968605, v2_1);
        return;
    }

    public void bootadd(com.android.vending.billing.InAppBillingService.LUCK.PkgListItem p16, String p17)
    {
        new StringBuilder();
        java.io.File v2_1 = new java.io.File(new StringBuilder().append(this.getContext().getDir("bootlist", 0)).append("/bootlist").toString());
        if (p17.contains("ads")) {
            p16.boot_ads = 1;
            StringBuilder v8_2 = new StringBuilder();
            int v6_0 = 0;
            while (v6_0 < com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.data.length) {
                if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.data[v6_0].boot_ads) || ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.data[v6_0].boot_custom) || ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.data[v6_0].boot_lvl) || (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.data[v6_0].boot_manual)))) {
                    v8_2.append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.data[v6_0].pkgName);
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.data[v6_0].boot_ads) {
                        v8_2.append("%ads");
                    }
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.data[v6_0].boot_lvl) {
                        v8_2.append("%lvl");
                    }
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.data[v6_0].boot_custom) {
                        v8_2.append("%custom");
                    }
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.data[v6_0].boot_manual) {
                        v8_2.append("%object");
                    }
                    v8_2.append(",");
                }
                v6_0++;
            }
            v8_2.append("\n");
            try {
                java.io.FileOutputStream v5_1 = new java.io.FileOutputStream(v2_1);
                v5_1.write(v8_2.toString().getBytes());
                v5_1.close();
            } catch (java.io.FileNotFoundException v7) {
                android.widget.Toast.makeText(this.getContext(), "Error: bootlist file are not found!", 1).show();
            } catch (Exception v4_0) {
                android.widget.Toast.makeText(this.getContext(), new StringBuilder().append("").append(v4_0).toString(), 1).show();
            }
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.notifyDataSetChanged(p16);
        }
        if (p17.contains("lvl")) {
            p16.boot_lvl = 1;
            StringBuilder v8_4 = new StringBuilder();
            int v6_1 = 0;
            while (v6_1 < com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.data.length) {
                if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.data[v6_1].boot_ads) || ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.data[v6_1].boot_custom) || ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.data[v6_1].boot_lvl) || (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.data[v6_1].boot_manual)))) {
                    v8_4.append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.data[v6_1].pkgName);
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.data[v6_1].boot_ads) {
                        v8_4.append("%ads");
                    }
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.data[v6_1].boot_lvl) {
                        v8_4.append("%lvl");
                    }
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.data[v6_1].boot_custom) {
                        v8_4.append("%custom");
                    }
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.data[v6_1].boot_manual) {
                        v8_4.append("%object");
                    }
                    v8_4.append(",");
                }
                v6_1++;
            }
            v8_4.append("\n");
            try {
                java.io.FileOutputStream v5_3 = new java.io.FileOutputStream(v2_1);
                v5_3.write(v8_4.toString().getBytes());
                v5_3.close();
            } catch (java.io.FileNotFoundException v7) {
                android.widget.Toast.makeText(this.getContext(), "Error: bootlist file are not found!", 1).show();
            } catch (Exception v4_1) {
                android.widget.Toast.makeText(this.getContext(), new StringBuilder().append("").append(v4_1).toString(), 1).show();
            }
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.notifyDataSetChanged(p16);
        }
        if (p17.contains("custom")) {
            p16.boot_custom = 1;
            java.io.File v3 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.customselect;
            if (v3 != null) {
                try {
                    com.chelpus.Utils.copyFile(v3, new java.io.File(new StringBuilder().append(this.getContext().getDir("bootlist", 0)).append("/").append(p16.pkgName).toString()));
                } catch (String v9) {
                }
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.notifyDataSetChanged(p16);
            }
        }
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(3);
        this.refresh_boot();
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adapter_boot = new com.android.vending.billing.InAppBillingService.LUCK.BootListItemAdapter(this.getContext(), 2130968593, com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getInt("viewsize", 0), com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.boot_pat);
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adapter_boot.notifyDataSetChanged();
        return;
    }

    public void busyboxcopy()
    {
        int v3;
        java.io.File v4_1 = new java.io.File(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir).append("/").append("busybox").toString());
        if (!android.os.Build.CPU_ABI.toLowerCase().trim().equals("x86")) {
            if (!android.os.Build.CPU_ABI.toUpperCase().trim().equals("MIPS")) {
                v3 = 2131099648;
            } else {
                v3 = 2131099649;
            }
        } else {
            v3 = 2131099650;
        }
        long v1 = com.chelpus.Utils.getRawLength(v3);
        if ((!v4_1.exists()) || (v4_1.length() != v1)) {
            if (v4_1.length() != v1) {
                System.out.println(new StringBuilder().append("LuckyPatcher: busybox version updated. ").append(v1).append(" ").append(v4_1.length()).toString());
                if (v4_1.exists()) {
                    v4_1.delete();
                }
            }
            try {
                com.chelpus.Utils.getRawToFile(v3, new java.io.File(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir).append("/busybox").toString()));
            } catch (String v5) {
            }
            try {
                com.chelpus.Utils.chmod(new java.io.File(v4_1.getAbsolutePath()), 3583);
            } catch (Exception v0) {
                System.out.println(v0);
                v0.printStackTrace();
            }
            com.chelpus.Utils.run_all(new StringBuilder().append("chmod 06777 ").append(v4_1.getAbsolutePath()).toString());
            com.chelpus.Utils.run_all(new StringBuilder().append("chown 0.0 ").append(v4_1.getAbsolutePath()).toString());
            com.chelpus.Utils.run_all(new StringBuilder().append("chown 0:0 ").append(v4_1.getAbsolutePath()).toString());
        }
        return;
    }

    public void busyboxinstall()
    {
        try {
            com.chelpus.Utils.remount("/system", "rw");
            com.chelpus.Utils.copyFile(new StringBuilder().append(this.getContext().getFilesDir().getAbsolutePath()).append("/busybox").toString(), "/system/xbin/busybox", 0, 1);
            try {
                com.chelpus.Utils.remount("/system", "rw");
                com.chelpus.Utils.run_all("chown 0.2000 /system/xbin/busybox");
                com.chelpus.Utils.run_all("chown 0:2000 /system/xbin/busybox");
                com.chelpus.Utils.run_all("chmod 04755 /system/xbin/busybox");
                com.chelpus.Utils v1_11 = new com.chelpus.Utils("");
                String[] v2_9 = new String[1];
                v2_9[0] = "/system/xbin/busybox --install -s /system/xbin";
                v1_11.cmdRoot(v2_9);
            } catch (Exception v0) {
                v0.printStackTrace();
            }
            return;
        } catch (com.chelpus.Utils v1) {
        }
    }

    public void changeDayOnUp()
    {
        com.android.vending.billing.InAppBillingService.LUCK.AlertDlg v0_1 = new com.android.vending.billing.InAppBillingService.LUCK.AlertDlg(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext());
        v0_1.setTitle(2131165404);
        android.widget.LinearLayout v1_1 = ((android.widget.LinearLayout) android.view.View.inflate(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext(), 2130968597, 0));
        android.widget.EditText v2_1 = ((android.widget.EditText) v1_1.findViewById(2131558498));
        v2_1.setText(new StringBuilder().append("").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getInt("days_on_up", 1)).toString());
        v0_1.setView(v1_1);
        v0_1.setNegativeButton(2131165563, 0).setPositiveButton(2131165187, new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$46(this, v2_1)).create().show();
        return;
    }

    public void changeDefaultDir()
    {
        com.android.vending.billing.InAppBillingService.LUCK.AlertDlg v0_1 = new com.android.vending.billing.InAppBillingService.LUCK.AlertDlg(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext());
        v0_1.setTitle(2131165411);
        android.widget.LinearLayout v1_1 = ((android.widget.LinearLayout) android.view.View.inflate(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext(), 2130968596, 0));
        android.widget.EditText v2_1 = ((android.widget.EditText) v1_1.findViewById(2131558497));
        v2_1.setText(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getString("basepath", "/"));
        v0_1.setView(v1_1);
        v0_1.setNegativeButton(2131165563, 0).setPositiveButton(2131165187, new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$47(this, v2_1)).create().show();
        return;
    }

    public void cleardalvik()
    {
        com.chelpus.Utils.showDialogYesNo(com.chelpus.Utils.getText(2131165748), com.chelpus.Utils.getText(2131165533), new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$9(this), new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$9(this), 0);
        return;
    }

    public void cleardata_click()
    {
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getBoolean("vibration", 0)) {
            this.vib = ((android.os.Vibrator) this.getContext().getSystemService("vibrator"));
            this.vib.vibrate(50);
        }
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su) {
            com.chelpus.Utils.showDialogYesNo(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.name, com.chelpus.Utils.getText(2131165534), new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$55(this), new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$55(this), 0);
        } else {
            if (android.os.Build$VERSION.SDK_INT < 9) {
                android.content.Intent v1_1 = new android.content.Intent("android.intent.action.VIEW");
                v1_1.setClassName("com.android.settings", "com.android.settings.InstalledAppDetails");
                v1_1.putExtra("com.android.settings.ApplicationPkgName", com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName);
                v1_1.putExtra("pkg", com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName);
                v1_1.setFlags(131072);
                this.startActivity(v1_1);
            } else {
                android.content.Intent v1_3 = new android.content.Intent("android.settings.APPLICATION_DETAILS_SETTINGS", android.net.Uri.parse(new StringBuilder().append("package:").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName).toString()));
                v1_3.setFlags(131072);
                this.startActivity(v1_3);
            }
        }
        return;
    }

    public void cloneApplication()
    {
        if (this.testSD(1)) {
            this.runWithWait(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$27(this));
        }
        return;
    }

    public void collapseAll(int p5)
    {
        try {
            int v0 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.getGroupCount();
            int v2 = 0;
        } catch (Exception v1) {
            v1.printStackTrace();
            return;
        }
        while (v2 < v0) {
            if (v2 != p5) {
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.lv.collapseGroup(v2);
            }
            v2++;
        }
        return;
    }

    public void connectToLicensing()
    {
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.mServiceConnL = new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$79(this);
        try {
            if (this.mServiceL == null) {
                android.content.Intent v4_1 = new android.content.Intent(new String(com.google.android.vending.licensing.util.Base64.decode("Y29tLmFuZHJvaWQudmVuZGluZy5saWNlbnNpbmcuSUxpY2Vuc2luZ1NlcnZpY2U=")));
                v4_1.setPackage("com.android.vending");
                v4_1.putExtra("xexe", "lp");
                boolean v0 = 0;
                try {
                    if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().queryIntentServices(v4_1, 0).isEmpty()) {
                        int v9_11 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().queryIntentServices(v4_1, 0).iterator();
                        android.content.Intent v5 = v4_1;
                        try {
                            while (v9_11.hasNext()) {
                                android.content.Intent v4_2;
                                android.content.pm.ResolveInfo v7_1 = ((android.content.pm.ResolveInfo) v9_11.next());
                                if ((v7_1.serviceInfo.packageName == null) || (!v7_1.serviceInfo.packageName.equals("com.android.vending"))) {
                                    v4_2 = v5;
                                } else {
                                    android.content.ComponentName v2_1 = new android.content.ComponentName(v7_1.serviceInfo.packageName, v7_1.serviceInfo.name);
                                    v4_2 = new android.content.Intent();
                                    v4_2.setComponent(v2_1);
                                    v4_2.putExtra("xexe", "lp");
                                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.handler.post(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$80(this));
                                    v0 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().bindService(v4_2, com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.mServiceConnL, 1);
                                }
                                v5 = v4_2;
                            }
                        } catch (Exception v3_0) {
                            v3_0.printStackTrace();
                            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(11);
                        }
                    }
                } catch (Exception v3_0) {
                }
                if (!v0) {
                    this.cleanupService();
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(11);
                } else {
                }
            }
        } catch (Exception v3_1) {
            v3_1.printStackTrace();
            this.cleanupService();
        } catch (Exception v3_2) {
            v3_2.printStackTrace();
            this.cleanupService();
        }
        return;
    }

    public void contextCorePatch()
    {
        this.runWithWait(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$76(this));
        System.out.println("LuckyPatcher: Start core.jar test!");
        return;
    }

    public void contextXposedPatch()
    {
        this.runWithWait(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$77(this));
        System.out.println("LuckyPatcher: Start core.jar test!");
        return;
    }

    public void context_backup_menu()
    {
        java.util.ArrayList v0_1 = new java.util.ArrayList();
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.system) {
            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su) {
                v0_1.add(Integer.valueOf(2131165258));
            }
        } else {
            v0_1.add(Integer.valueOf(2131165257));
            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su) {
                v0_1.add(Integer.valueOf(2131165258));
            }
        }
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adapt = new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$66(this, this.getContext(), 2130968605, v0_1);
        return;
    }

    public void context_remove_saved_purchases()
    {
        java.util.ArrayList v1 = new com.google.android.finsky.billing.iab.DbHelper(this.getContext(), com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName).getItems();
        if (v1.size() == 0) {
            v1.add(new com.google.android.finsky.billing.iab.ItemsListItem(com.chelpus.Utils.getText(2131165424), "", ""));
        }
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adapt = new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$73(this, this.getContext(), 2130968605, v1);
        return;
    }

    public void context_restore_menu()
    {
        Integer v8_0 = 0;
        java.util.ArrayList v6_1 = new java.util.ArrayList();
        int v0 = 0;
        try {
            String[] v4 = new java.io.File(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).append("/Backup").toString()).list();
            android.support.v4.app.FragmentActivity v9_2 = 0;
        } catch (Exception v2) {
            System.out.println("LuckyPatcher error: backup files or directory not found!");
            String[] v5 = new java.io.File(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).append("/Backup/Data/").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName).toString()).list();
            int v1 = 0;
            if ((v5 != null) && (v5.length != 0)) {
                while (v8_0 < v5.length) {
                    if (v5[v8_0].endsWith(".lpbkp")) {
                        v1 = 1;
                    }
                    v8_0++;
                }
            }
            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.system) {
                if ((v1 != 0) && (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su)) {
                    v6_1.add(Integer.valueOf(2131165284));
                }
            } else {
                if (v0 != 0) {
                    v6_1.add(Integer.valueOf(2131165283));
                }
                if ((v1 != 0) && (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su)) {
                    v6_1.add(Integer.valueOf(2131165284));
                }
            }
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adapt = new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$67(this, this.getContext(), 2130968605, v6_1);
            return;
        }
        while (v9_2 < v4.length) {
            String v3 = v4[v9_2];
            if ((!new java.io.File(v3).getName().startsWith(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName)) && (!new java.io.File(v3).getName().startsWith(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.name))) {
                v9_2++;
            } else {
                v0 = 1;
                break;
            }
        }
    }

    public void contextads()
    {
        java.util.ArrayList v0_1 = new java.util.ArrayList();
        java.util.List v1 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().queryIntentServices(new android.content.Intent("com.google.android.gms.ads.identifier.service.START"), 0);
        if ((v1 != null) && (v1.size() > 0)) {
            v0_1.add(Integer.valueOf(2131165268));
        }
        v0_1.add(Integer.valueOf(2131165281));
        v0_1.add(Integer.valueOf(2131165266));
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adapt = new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$70(this, this.getContext(), 2130968605, v0_1);
        return;
    }

    public void contextboot()
    {
        java.util.ArrayList v0_1 = new java.util.ArrayList();
        v0_1.add(Integer.valueOf(2131165298));
        v0_1.add(Integer.valueOf(2131165299));
        v0_1.add(Integer.valueOf(2131165297));
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adapt = new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$63(this, this.getContext(), 2130968605, v0_1);
        return;
    }

    public void contextcreateapk()
    {
        java.util.ArrayList v0_1 = new java.util.ArrayList();
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.custom) {
            v0_1.add(Integer.valueOf(2131165385));
        }
        v0_1.add(Integer.valueOf(2131165386));
        v0_1.add(Integer.valueOf(2131165384));
        v0_1.add(Integer.valueOf(2131165389));
        v0_1.add(Integer.valueOf(2131165263));
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adapt = new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$72(this, this.getContext(), 2130968622, v0_1);
        return;
    }

    public void contextfix()
    {
        java.util.ArrayList v0_1 = new java.util.ArrayList();
        v0_1.add(Integer.valueOf(2131165324));
        if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.system) {
            v0_1.add(Integer.valueOf(2131165316));
        }
        if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.system) {
            v0_1.add(Integer.valueOf(2131165296));
        }
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adapt = new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$62(this, this.getContext(), 2130968605, v0_1);
        return;
    }

    public void contextlevel0()
    {
        this.runWithWait(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$59(this));
        return;
    }

    public void contextlevelpatch0()
    {
        this.runWithWait(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$60(this));
        return;
    }

    public void contextleveltools0()
    {
        this.runWithWait(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$61(this));
        return;
    }

    public void contextlvl()
    {
        java.util.ArrayList v0_1 = new java.util.ArrayList();
        v0_1.add(Integer.valueOf(2131165256));
        if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.system) {
            v0_1.add(Integer.valueOf(2131165317));
        }
        v0_1.add(Integer.valueOf(2131165367));
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adapt = new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$69(this, this.getContext(), 2130968605, v0_1);
        return;
    }

    public void contextmenu_click()
    {
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getBoolean("vibration", 0)) {
            this.vib = ((android.os.Vibrator) this.getContext().getSystemService("vibrator"));
            this.vib.vibrate(50);
        }
        try {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(6);
            this.contextlevelpatch0();
        } catch (Exception v0) {
            System.out.println(new StringBuilder().append("LuckyPatcher (Context menu click): ").append(v0).toString());
            v0.printStackTrace();
        }
        return;
    }

    public void contextmenutools_click()
    {
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getBoolean("vibration", 0)) {
            this.vib = ((android.os.Vibrator) this.getContext().getSystemService("vibrator"));
            this.vib.vibrate(50);
        }
        try {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(6);
            this.contextleveltools0();
        } catch (Exception v0) {
            System.out.println(new StringBuilder().append("LuckyPatcher (Context menu click): ").append(v0).toString());
            v0.printStackTrace();
        }
        return;
    }

    public void contextpermissions()
    {
        String v26 = 0;
        try {
            android.content.pm.PackageInfo v14 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName, 4096);
        } catch (android.content.pm.PackageManager$NameNotFoundException v11) {
            v11.printStackTrace();
        }
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pkgNam = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.searchTag = "package";
        java.util.ArrayList v28_1 = new java.util.ArrayList();
        String v3 = new StringBuilder().append(this.getContext().getDir("packages", 0).getAbsolutePath()).append("/packages.xml").toString();
        String v29_8 = new java.io.File;
        v29_8(v3);
        if (!v29_8.exists()) {
            android.widget.Toast.makeText(this.getContext(), "Error: packages.xml not found!", 1).show();
        }
        try {
            javax.xml.parsers.DocumentBuilder v8 = javax.xml.parsers.DocumentBuilderFactory.newInstance().newDocumentBuilder();
            String v29_12 = new java.io.FileInputStream;
            v29_12(v3);
            org.w3c.dom.Document v7 = v8.parse(v29_12);
            org.w3c.dom.NodeList v20_0 = v7.getElementsByTagName("package");
            int v13_0 = 0;
        } catch (javax.xml.parsers.ParserConfigurationException v21) {
            v21.printStackTrace();
            new java.util.ArrayList();
            if (v28_1.size() <= 0) {
                v28_1.add(new com.android.vending.billing.InAppBillingService.LUCK.Perm(com.chelpus.Utils.getText(2131165626), 0));
            }
            String v29_277 = new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$86;
            v29_277(this, v28_1);
            this.runToMain(v29_277);
            return;
        } catch (org.xml.sax.SAXException v25) {
            v25.printStackTrace();
        } catch (java.io.IOException v15) {
            v15.printStackTrace();
        }
        while (v13_0 < v20_0.getLength()) {
            org.w3c.dom.Node v18_0 = v20_0.item(v13_0);
            org.w3c.dom.Element v10_1 = ((org.w3c.dom.Element) v20_0.item(v13_0));
            if (!v10_1.getAttribute("name").equals(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName)) {
                v13_0++;
            } else {
                if (!v10_1.getAttribute("sharedUserId").isEmpty()) {
                    v26 = v10_1.getAttribute("sharedUserId");
                }
                org.w3c.dom.NodeList v24_0 = v18_0.getChildNodes();
                int v16_0 = 0;
                while (v16_0 < v24_0.getLength()) {
                    org.w3c.dom.Node v19_2 = v24_0.item(v16_0);
                    System.out.println(new StringBuilder().append("node:").append(v19_2.getNodeName()).toString());
                    if (v19_2.getNodeName().equals("perms")) {
                        org.w3c.dom.NodeList v23_6 = v19_2.getChildNodes();
                        int v17_6 = 0;
                        while (v17_6 < v23_6.getLength()) {
                            System.out.println(new StringBuilder().append("perm0:").append(v23_6.item(v17_6).getNodeName()).toString());
                            if (v23_6.item(v17_6).getNodeName().equals("item")) {
                                org.w3c.dom.Element v22_17 = ((org.w3c.dom.Element) v23_6.item(v17_6));
                                if (!v22_17.getAttribute("name").contains("chelpa.disabled.")) {
                                    com.android.vending.billing.InAppBillingService.LUCK.Perm v27_16 = new com.android.vending.billing.InAppBillingService.LUCK.Perm;
                                    v27_16(v22_17.getAttribute("name"), 1);
                                    v28_1.add(v27_16);
                                } else {
                                    com.android.vending.billing.InAppBillingService.LUCK.Perm v27_17 = new com.android.vending.billing.InAppBillingService.LUCK.Perm;
                                    v27_17(v22_17.getAttribute("name"), 0);
                                    v28_1.add(v27_17);
                                }
                                System.out.println(new StringBuilder().append("perm:").append(v22_17.getAttribute("name")).toString());
                            }
                            v17_6++;
                        }
                    }
                    if (v19_2.getNodeName().equals("disabled-components")) {
                        org.w3c.dom.NodeList v23_7 = v19_2.getChildNodes();
                        int v17_7 = 0;
                        while (v17_7 < v23_7.getLength()) {
                            System.out.println(new StringBuilder().append("perm0:").append(v23_7.item(v17_7).getNodeName()).toString());
                            if (v23_7.item(v17_7).getNodeName().equals("item")) {
                                org.w3c.dom.Element v22_15 = ((org.w3c.dom.Element) v23_7.item(v17_7));
                                if (!v22_15.getAttribute("name").contains("chelpa.disabled.")) {
                                    if ((v14.requestedPermissions != null) && (v14.requestedPermissions.length != 0)) {
                                        int v12_10 = 0;
                                        int v6_10 = 0;
                                        while (v6_10 < v14.requestedPermissions.length) {
                                            if (!v14.requestedPermissions[v6_10].equals(v22_15.getAttribute("name"))) {
                                                v6_10++;
                                            } else {
                                                v12_10 = 1;
                                                break;
                                            }
                                        }
                                        if (v12_10 != 0) {
                                            com.android.vending.billing.InAppBillingService.LUCK.Perm v27_14 = new com.android.vending.billing.InAppBillingService.LUCK.Perm;
                                            v27_14(v22_15.getAttribute("name"), 1);
                                            v28_1.add(v27_14);
                                        }
                                    }
                                } else {
                                    if ((v14.requestedPermissions != null) && (v14.requestedPermissions.length != 0)) {
                                        int v12_11 = 0;
                                        int v6_11 = 0;
                                        while (v6_11 < v14.requestedPermissions.length) {
                                            if (!v14.requestedPermissions[v6_11].equals(v22_15.getAttribute("name"))) {
                                                v6_11++;
                                            } else {
                                                v12_11 = 1;
                                                break;
                                            }
                                        }
                                        if (v12_11 != 0) {
                                            com.android.vending.billing.InAppBillingService.LUCK.Perm v27_15 = new com.android.vending.billing.InAppBillingService.LUCK.Perm;
                                            v27_15(v22_15.getAttribute("name"), 0);
                                            v28_1.add(v27_15);
                                        }
                                    }
                                }
                                System.out.println(new StringBuilder().append("perm:").append(v22_15.getAttribute("name")).toString());
                            }
                            v17_7++;
                        }
                    }
                    if (v19_2.getNodeName().equals("enabled-components")) {
                        org.w3c.dom.NodeList v23_8 = v19_2.getChildNodes();
                        int v17_8 = 0;
                        while (v17_8 < v23_8.getLength()) {
                            System.out.println(new StringBuilder().append("perm0:").append(v23_8.item(v17_8).getNodeName()).toString());
                            if (v23_8.item(v17_8).getNodeName().equals("item")) {
                                org.w3c.dom.Element v22_13 = ((org.w3c.dom.Element) v23_8.item(v17_8));
                                if (!v22_13.getAttribute("name").contains("chelpa.disabled.")) {
                                    if ((v14.requestedPermissions != null) && (v14.requestedPermissions.length != 0)) {
                                        int v12_8 = 0;
                                        int v6_8 = 0;
                                        while (v6_8 < v14.requestedPermissions.length) {
                                            if (!v14.requestedPermissions[v6_8].equals(v22_13.getAttribute("name"))) {
                                                v6_8++;
                                            } else {
                                                v12_8 = 1;
                                                break;
                                            }
                                        }
                                        if (v12_8 != 0) {
                                            com.android.vending.billing.InAppBillingService.LUCK.Perm v27_12 = new com.android.vending.billing.InAppBillingService.LUCK.Perm;
                                            v27_12(v22_13.getAttribute("name"), 1);
                                            v28_1.add(v27_12);
                                        }
                                    }
                                } else {
                                    if ((v14.requestedPermissions != null) && (v14.requestedPermissions.length != 0)) {
                                        int v12_9 = 0;
                                        int v6_9 = 0;
                                        while (v6_9 < v14.requestedPermissions.length) {
                                            if (!v14.requestedPermissions[v6_9].equals(v22_13.getAttribute("name"))) {
                                                v6_9++;
                                            } else {
                                                v12_9 = 1;
                                                break;
                                            }
                                        }
                                        if (v12_9 != 0) {
                                            com.android.vending.billing.InAppBillingService.LUCK.Perm v27_13 = new com.android.vending.billing.InAppBillingService.LUCK.Perm;
                                            v27_13(v22_13.getAttribute("name"), 0);
                                            v28_1.add(v27_13);
                                        }
                                    }
                                }
                                System.out.println(new StringBuilder().append("perm:").append(v22_13.getAttribute("name")).toString());
                            }
                            v17_8++;
                        }
                    }
                    v16_0++;
                }
            }
        }
        org.w3c.dom.NodeList v20_1 = v7.getElementsByTagName("updated-package");
        int v13_1 = 0;
        while (v13_1 < v20_1.getLength()) {
            org.w3c.dom.Node v18_1 = v20_1.item(v13_1);
            org.w3c.dom.Element v10_3 = ((org.w3c.dom.Element) v20_1.item(v13_1));
            if (!v10_3.getAttribute("name").equals(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName)) {
                v13_1++;
            } else {
                if (!v10_3.getAttribute("sharedUserId").isEmpty()) {
                    v26 = v10_3.getAttribute("sharedUserId");
                }
                org.w3c.dom.NodeList v24_1 = v18_1.getChildNodes();
                int v16_1 = 0;
                while (v16_1 < v24_1.getLength()) {
                    org.w3c.dom.Node v19_1 = v24_1.item(v16_1);
                    System.out.println(new StringBuilder().append("node:").append(v19_1.getNodeName()).toString());
                    if (v19_1.getNodeName().equals("perms")) {
                        org.w3c.dom.NodeList v23_3 = v19_1.getChildNodes();
                        int v17_3 = 0;
                        while (v17_3 < v23_3.getLength()) {
                            System.out.println(new StringBuilder().append("perm0:").append(v23_3.item(v17_3).getNodeName()).toString());
                            if (v23_3.item(v17_3).getNodeName().equals("item")) {
                                org.w3c.dom.Element v22_11 = ((org.w3c.dom.Element) v23_3.item(v17_3));
                                if (!v22_11.getAttribute("name").contains("chelpa.disabled.")) {
                                    com.android.vending.billing.InAppBillingService.LUCK.Perm v27_10 = new com.android.vending.billing.InAppBillingService.LUCK.Perm;
                                    v27_10(v22_11.getAttribute("name"), 1);
                                    v28_1.add(v27_10);
                                } else {
                                    com.android.vending.billing.InAppBillingService.LUCK.Perm v27_11 = new com.android.vending.billing.InAppBillingService.LUCK.Perm;
                                    v27_11(v22_11.getAttribute("name"), 0);
                                    v28_1.add(v27_11);
                                }
                                System.out.println(new StringBuilder().append("perm:").append(v22_11.getAttribute("name")).toString());
                            }
                            v17_3++;
                        }
                    }
                    if (v19_1.getNodeName().equals("disabled-components")) {
                        org.w3c.dom.NodeList v23_4 = v19_1.getChildNodes();
                        int v17_4 = 0;
                        while (v17_4 < v23_4.getLength()) {
                            System.out.println(new StringBuilder().append("perm0:").append(v23_4.item(v17_4).getNodeName()).toString());
                            if (v23_4.item(v17_4).getNodeName().equals("item")) {
                                org.w3c.dom.Element v22_9 = ((org.w3c.dom.Element) v23_4.item(v17_4));
                                if (!v22_9.getAttribute("name").contains("chelpa.disabled.")) {
                                    if ((v14.requestedPermissions != null) && (v14.requestedPermissions.length != 0)) {
                                        int v12_6 = 0;
                                        int v6_6 = 0;
                                        while (v6_6 < v14.requestedPermissions.length) {
                                            if (!v14.requestedPermissions[v6_6].equals(v22_9.getAttribute("name"))) {
                                                v6_6++;
                                            } else {
                                                v12_6 = 1;
                                                break;
                                            }
                                        }
                                        if (v12_6 != 0) {
                                            com.android.vending.billing.InAppBillingService.LUCK.Perm v27_8 = new com.android.vending.billing.InAppBillingService.LUCK.Perm;
                                            v27_8(v22_9.getAttribute("name"), 1);
                                            v28_1.add(v27_8);
                                        }
                                    }
                                } else {
                                    if ((v14.requestedPermissions != null) && (v14.requestedPermissions.length != 0)) {
                                        int v12_7 = 0;
                                        int v6_7 = 0;
                                        while (v6_7 < v14.requestedPermissions.length) {
                                            if (!v14.requestedPermissions[v6_7].equals(v22_9.getAttribute("name"))) {
                                                v6_7++;
                                            } else {
                                                v12_7 = 1;
                                                break;
                                            }
                                        }
                                        if (v12_7 != 0) {
                                            com.android.vending.billing.InAppBillingService.LUCK.Perm v27_9 = new com.android.vending.billing.InAppBillingService.LUCK.Perm;
                                            v27_9(v22_9.getAttribute("name"), 0);
                                            v28_1.add(v27_9);
                                        }
                                    }
                                }
                                System.out.println(new StringBuilder().append("perm:").append(v22_9.getAttribute("name")).toString());
                            }
                            v17_4++;
                        }
                    }
                    if (v19_1.getNodeName().equals("enabled-components")) {
                        org.w3c.dom.NodeList v23_5 = v19_1.getChildNodes();
                        int v17_5 = 0;
                        while (v17_5 < v23_5.getLength()) {
                            System.out.println(new StringBuilder().append("perm0:").append(v23_5.item(v17_5).getNodeName()).toString());
                            if (v23_5.item(v17_5).getNodeName().equals("item")) {
                                org.w3c.dom.Element v22_7 = ((org.w3c.dom.Element) v23_5.item(v17_5));
                                if (!v22_7.getAttribute("name").contains("chelpa.disabled.")) {
                                    if ((v14.requestedPermissions != null) && (v14.requestedPermissions.length != 0)) {
                                        int v12_4 = 0;
                                        int v6_4 = 0;
                                        while (v6_4 < v14.requestedPermissions.length) {
                                            if (!v14.requestedPermissions[v6_4].equals(v22_7.getAttribute("name"))) {
                                                v6_4++;
                                            } else {
                                                v12_4 = 1;
                                                break;
                                            }
                                        }
                                        if (v12_4 != 0) {
                                            com.android.vending.billing.InAppBillingService.LUCK.Perm v27_6 = new com.android.vending.billing.InAppBillingService.LUCK.Perm;
                                            v27_6(v22_7.getAttribute("name"), 1);
                                            v28_1.add(v27_6);
                                        }
                                    }
                                } else {
                                    if ((v14.requestedPermissions != null) && (v14.requestedPermissions.length != 0)) {
                                        int v12_5 = 0;
                                        int v6_5 = 0;
                                        while (v6_5 < v14.requestedPermissions.length) {
                                            if (!v14.requestedPermissions[v6_5].equals(v22_7.getAttribute("name"))) {
                                                v6_5++;
                                            } else {
                                                v12_5 = 1;
                                                break;
                                            }
                                        }
                                        if (v12_5 != 0) {
                                            com.android.vending.billing.InAppBillingService.LUCK.Perm v27_7 = new com.android.vending.billing.InAppBillingService.LUCK.Perm;
                                            v27_7(v22_7.getAttribute("name"), 0);
                                            v28_1.add(v27_7);
                                        }
                                    }
                                }
                                System.out.println(new StringBuilder().append("perm:").append(v22_7.getAttribute("name")).toString());
                            }
                            v17_5++;
                        }
                    }
                    v16_1++;
                }
            }
        }
        if (v26 == null) {
        } else {
            org.w3c.dom.NodeList v20_2 = v7.getElementsByTagName("shared-user");
            int v13_2 = 0;
            while (v13_2 < v20_2.getLength()) {
                org.w3c.dom.Node v18_2 = v20_2.item(v13_2);
                if (!((org.w3c.dom.Element) v20_2.item(v13_2)).getAttribute("userId").equals(v26)) {
                    v13_2++;
                } else {
                    org.w3c.dom.NodeList v24_2 = v18_2.getChildNodes();
                    int v16_2 = 0;
                    while (v16_2 < v24_2.getLength()) {
                        org.w3c.dom.Node v19_0 = v24_2.item(v16_2);
                        System.out.println(new StringBuilder().append("node:").append(v19_0.getNodeName()).toString());
                        if (v19_0.getNodeName().equals("perms")) {
                            org.w3c.dom.NodeList v23_0 = v19_0.getChildNodes();
                            int v17_0 = 0;
                            while (v17_0 < v23_0.getLength()) {
                                System.out.println(new StringBuilder().append("perm0:").append(v23_0.item(v17_0).getNodeName()).toString());
                                if (v23_0.item(v17_0).getNodeName().equals("item")) {
                                    org.w3c.dom.Element v22_5 = ((org.w3c.dom.Element) v23_0.item(v17_0));
                                    if (!v22_5.getAttribute("name").contains("chelpa.disabled.")) {
                                        com.android.vending.billing.InAppBillingService.LUCK.Perm v27_4 = new com.android.vending.billing.InAppBillingService.LUCK.Perm;
                                        v27_4(v22_5.getAttribute("name"), 1);
                                        v28_1.add(v27_4);
                                    } else {
                                        com.android.vending.billing.InAppBillingService.LUCK.Perm v27_5 = new com.android.vending.billing.InAppBillingService.LUCK.Perm;
                                        v27_5(v22_5.getAttribute("name"), 0);
                                        v28_1.add(v27_5);
                                    }
                                    System.out.println(new StringBuilder().append("perm:").append(v22_5.getAttribute("name")).toString());
                                }
                                v17_0++;
                            }
                        }
                        if (v19_0.getNodeName().equals("disabled-components")) {
                            org.w3c.dom.NodeList v23_1 = v19_0.getChildNodes();
                            int v17_1 = 0;
                            while (v17_1 < v23_1.getLength()) {
                                System.out.println(new StringBuilder().append("perm0:").append(v23_1.item(v17_1).getNodeName()).toString());
                                if (v23_1.item(v17_1).getNodeName().equals("item")) {
                                    org.w3c.dom.Element v22_3 = ((org.w3c.dom.Element) v23_1.item(v17_1));
                                    if (!v22_3.getAttribute("name").contains("chelpa.disabled.")) {
                                        if ((v14.requestedPermissions != null) && (v14.requestedPermissions.length != 0)) {
                                            int v12_2 = 0;
                                            int v6_2 = 0;
                                            while (v6_2 < v14.requestedPermissions.length) {
                                                if (!v14.requestedPermissions[v6_2].equals(v22_3.getAttribute("name"))) {
                                                    v6_2++;
                                                } else {
                                                    v12_2 = 1;
                                                    break;
                                                }
                                            }
                                            if (v12_2 != 0) {
                                                com.android.vending.billing.InAppBillingService.LUCK.Perm v27_2 = new com.android.vending.billing.InAppBillingService.LUCK.Perm;
                                                v27_2(v22_3.getAttribute("name"), 1);
                                                v28_1.add(v27_2);
                                            }
                                        }
                                    } else {
                                        if ((v14.requestedPermissions != null) && (v14.requestedPermissions.length != 0)) {
                                            int v12_3 = 0;
                                            int v6_3 = 0;
                                            while (v6_3 < v14.requestedPermissions.length) {
                                                if (!v14.requestedPermissions[v6_3].equals(v22_3.getAttribute("name"))) {
                                                    v6_3++;
                                                } else {
                                                    v12_3 = 1;
                                                    break;
                                                }
                                            }
                                            if (v12_3 != 0) {
                                                com.android.vending.billing.InAppBillingService.LUCK.Perm v27_3 = new com.android.vending.billing.InAppBillingService.LUCK.Perm;
                                                v27_3(v22_3.getAttribute("name"), 0);
                                                v28_1.add(v27_3);
                                            }
                                        }
                                    }
                                    System.out.println(new StringBuilder().append("perm:").append(v22_3.getAttribute("name")).toString());
                                }
                                v17_1++;
                            }
                        }
                        if (v19_0.getNodeName().equals("enabled-components")) {
                            org.w3c.dom.NodeList v23_2 = v19_0.getChildNodes();
                            int v17_2 = 0;
                            while (v17_2 < v23_2.getLength()) {
                                System.out.println(new StringBuilder().append("perm0:").append(v23_2.item(v17_2).getNodeName()).toString());
                                if (v23_2.item(v17_2).getNodeName().equals("item")) {
                                    org.w3c.dom.Element v22_1 = ((org.w3c.dom.Element) v23_2.item(v17_2));
                                    if (!v22_1.getAttribute("name").contains("chelpa.disabled.")) {
                                        if ((v14.requestedPermissions != null) && (v14.requestedPermissions.length != 0)) {
                                            int v12_0 = 0;
                                            int v6_0 = 0;
                                            while (v6_0 < v14.requestedPermissions.length) {
                                                if (!v14.requestedPermissions[v6_0].equals(v22_1.getAttribute("name"))) {
                                                    v6_0++;
                                                } else {
                                                    v12_0 = 1;
                                                    break;
                                                }
                                            }
                                            if (v12_0 != 0) {
                                                com.android.vending.billing.InAppBillingService.LUCK.Perm v27_0 = new com.android.vending.billing.InAppBillingService.LUCK.Perm;
                                                v27_0(v22_1.getAttribute("name"), 1);
                                                v28_1.add(v27_0);
                                            }
                                        }
                                    } else {
                                        if ((v14.requestedPermissions != null) && (v14.requestedPermissions.length != 0)) {
                                            int v12_1 = 0;
                                            int v6_1 = 0;
                                            while (v6_1 < v14.requestedPermissions.length) {
                                                if (!v14.requestedPermissions[v6_1].equals(v22_1.getAttribute("name"))) {
                                                    v6_1++;
                                                } else {
                                                    v12_1 = 1;
                                                    break;
                                                }
                                            }
                                            if (v12_1 != 0) {
                                                com.android.vending.billing.InAppBillingService.LUCK.Perm v27_1 = new com.android.vending.billing.InAppBillingService.LUCK.Perm;
                                                v27_1(v22_1.getAttribute("name"), 0);
                                                v28_1.add(v27_1);
                                            }
                                        }
                                    }
                                    System.out.println(new StringBuilder().append("perm:").append(v22_1.getAttribute("name")).toString());
                                }
                                v17_2++;
                            }
                        }
                        v16_2++;
                    }
                }
            }
        }
    }

    public void contextpermmenu()
    {
        java.util.ArrayList v0_1 = new java.util.ArrayList();
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.system) {
            v0_1.add(Integer.valueOf(2131165267));
            v0_1.add(Integer.valueOf(2131165286));
        } else {
            v0_1.add(Integer.valueOf(2131165267));
            v0_1.add(Integer.valueOf(2131165272));
            v0_1.add(Integer.valueOf(2131165286));
            v0_1.add(Integer.valueOf(2131165285));
        }
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adapt = new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$68(this, this.getContext(), 2130968605, v0_1);
        return;
    }

    public void contextselpatch()
    {
        java.util.ArrayList v0_1 = new java.util.ArrayList();
        v0_1.add(new com.android.vending.billing.InAppBillingService.LUCK.Patterns(com.chelpus.Utils.getText(2131165325), 0));
        v0_1.add(new com.android.vending.billing.InAppBillingService.LUCK.Patterns(com.chelpus.Utils.getText(2131165327), 0));
        v0_1.add(new com.android.vending.billing.InAppBillingService.LUCK.Patterns(com.chelpus.Utils.getText(2131165329), 0));
        v0_1.add(new com.android.vending.billing.InAppBillingService.LUCK.Patterns(com.chelpus.Utils.getText(2131165331), 0));
        v0_1.add(new com.android.vending.billing.InAppBillingService.LUCK.Patterns(com.chelpus.Utils.getText(2131165333), 0));
        v0_1.add(new com.android.vending.billing.InAppBillingService.LUCK.Patterns(com.chelpus.Utils.getText(2131165335), 0));
        v0_1.add(new com.android.vending.billing.InAppBillingService.LUCK.Patterns(com.chelpus.Utils.getText(2131165358), 0));
        v0_1.add(new com.android.vending.billing.InAppBillingService.LUCK.Patterns(com.chelpus.Utils.getText(2131165360), 0));
        v0_1.add(new com.android.vending.billing.InAppBillingService.LUCK.Patterns(com.chelpus.Utils.getText(2131165362), 0));
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patch_adapt != null) {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patch_adapt.notifyDataSetChanged();
        }
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patch_adapt = new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$83(this, this.getContext(), 2130968605, v0_1);
        return;
    }

    public void contextselpatchads(boolean p7)
    {
        java.util.ArrayList v4_1 = new java.util.ArrayList();
        v4_1.add(new com.android.vending.billing.InAppBillingService.LUCK.Patterns(com.chelpus.Utils.getText(2131165294), 1));
        v4_1.add(new com.android.vending.billing.InAppBillingService.LUCK.Patterns(com.chelpus.Utils.getText(2131165337), 1));
        v4_1.add(new com.android.vending.billing.InAppBillingService.LUCK.Patterns(com.chelpus.Utils.getText(2131165339), 0));
        v4_1.add(new com.android.vending.billing.InAppBillingService.LUCK.Patterns(com.chelpus.Utils.getText(2131165341), 1));
        v4_1.add(new com.android.vending.billing.InAppBillingService.LUCK.Patterns(com.chelpus.Utils.getText(2131165345), 0));
        v4_1.add(new com.android.vending.billing.InAppBillingService.LUCK.Patterns(com.chelpus.Utils.getText(2131165347), 1));
        v4_1.add(new com.android.vending.billing.InAppBillingService.LUCK.Patterns(com.chelpus.Utils.getText(2131165343), 1));
        v4_1.add(new com.android.vending.billing.InAppBillingService.LUCK.Patterns(com.chelpus.Utils.getText(2131165264), 0));
        v4_1.add(new com.android.vending.billing.InAppBillingService.LUCK.Patterns(com.chelpus.Utils.getText(2131165319), 0));
        if (p7) {
            v4_1.add(new com.android.vending.billing.InAppBillingService.LUCK.Patterns(com.chelpus.Utils.getText(2131165358), 0));
        }
        if (p7) {
            v4_1.add(new com.android.vending.billing.InAppBillingService.LUCK.Patterns(com.chelpus.Utils.getText(2131165360), 0));
        }
        if (p7) {
            v4_1.add(new com.android.vending.billing.InAppBillingService.LUCK.Patterns(com.chelpus.Utils.getText(2131165362), 0));
        }
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patch_adapt != null) {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patch_adapt.notifyDataSetChanged();
        }
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patch_adapt = new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$85(this, this.getContext(), 2130968605, v4_1, p7);
        return;
    }

    public void contextselpatchlvl(boolean p7)
    {
        java.util.ArrayList v4_1 = new java.util.ArrayList();
        v4_1.add(new com.android.vending.billing.InAppBillingService.LUCK.Patterns(com.chelpus.Utils.getText(2131165352), 1));
        v4_1.add(new com.android.vending.billing.InAppBillingService.LUCK.Patterns(com.chelpus.Utils.getText(2131165354), 0));
        v4_1.add(new com.android.vending.billing.InAppBillingService.LUCK.Patterns(com.chelpus.Utils.getText(2131165356), 0));
        v4_1.add(new com.android.vending.billing.InAppBillingService.LUCK.Patterns(com.chelpus.Utils.getText(2131165349), 0));
        v4_1.add(new com.android.vending.billing.InAppBillingService.LUCK.Patterns(com.chelpus.Utils.getText(2131165364), 0));
        v4_1.add(new com.android.vending.billing.InAppBillingService.LUCK.Patterns(com.chelpus.Utils.getText(2131165264), 0));
        if (p7) {
            v4_1.add(new com.android.vending.billing.InAppBillingService.LUCK.Patterns(com.chelpus.Utils.getText(2131165358), 0));
        }
        if (p7) {
            v4_1.add(new com.android.vending.billing.InAppBillingService.LUCK.Patterns(com.chelpus.Utils.getText(2131165360), 0));
        }
        if (p7) {
            v4_1.add(new com.android.vending.billing.InAppBillingService.LUCK.Patterns(com.chelpus.Utils.getText(2131165362), 0));
        }
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patch_adapt != null) {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patch_adapt.notifyDataSetChanged();
        }
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patch_adapt = new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$84(this, this.getContext(), 2130968605, v4_1, p7);
        return;
    }

    public void contextsupport(boolean p7)
    {
        java.util.ArrayList v4_1 = new java.util.ArrayList();
        v4_1.add(new com.android.vending.billing.InAppBillingService.LUCK.Patterns(com.chelpus.Utils.getText(2131165703), 1));
        v4_1.add(new com.android.vending.billing.InAppBillingService.LUCK.Patterns(com.chelpus.Utils.getText(2131165699), 1));
        v4_1.add(new com.android.vending.billing.InAppBillingService.LUCK.Patterns(com.chelpus.Utils.getText(2131165701), 0));
        if (!p7) {
            v4_1.add(new com.android.vending.billing.InAppBillingService.LUCK.Patterns(com.chelpus.Utils.getText(2131165358), 0));
            v4_1.add(new com.android.vending.billing.InAppBillingService.LUCK.Patterns(com.chelpus.Utils.getText(2131165360), 0));
            v4_1.add(new com.android.vending.billing.InAppBillingService.LUCK.Patterns(com.chelpus.Utils.getText(2131165362), 0));
        }
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patch_adapt != null) {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patch_adapt.notifyDataSetChanged();
        }
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patch_adapt = new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$71(this, this.getContext(), 2130968605, v4_1, p7);
        return;
    }

    public void contexttoolbarcreateapk()
    {
        java.util.ArrayList v2_1 = new java.util.ArrayList();
        if (com.chelpus.Utils.isCustomPatchesForPkg(com.chelpus.Utils.getApkPackageInfo(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.rebuldApk).packageName)) {
            v2_1.add(Integer.valueOf(2131165385));
        }
        v2_1.add(Integer.valueOf(2131165386));
        v2_1.add(Integer.valueOf(2131165384));
        v2_1.add(Integer.valueOf(2131165389));
        v2_1.add(Integer.valueOf(2131165263));
        v2_1.add(Integer.valueOf(2131165254));
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adapt = new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$74(this, this.getContext(), 2130968605, v2_1);
        return;
    }

    public void contexttoolbarcreateframework()
    {
        System.out.println("LuckyPatcher: Core patch start!");
        Thread v0_1 = new Thread(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$12(this));
        v0_1.setPriority(10);
        v0_1.start();
        return;
    }

    public void contexttruble()
    {
        java.util.ArrayList v0_1 = new java.util.ArrayList();
        v0_1.add(Integer.valueOf(2131165501));
        v0_1.add(Integer.valueOf(2131165735));
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adapt = new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$75(this, this.getContext(), 2130968605, v0_1);
        return;
    }

    public void corepatch(String p4)
    {
        System.out.println("LuckyPatcher: Core patch start!");
        Thread v0_1 = new Thread(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$13(this, p4));
        v0_1.setPriority(10);
        if (!com.chelpus.Utils.isInstalledOnSdCard(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getPackageName())) {
            v0_1.start();
        } else {
            this.showMessage(com.chelpus.Utils.getText(2131165748), com.chelpus.Utils.getText(2131165431));
        }
        return;
    }

    public void createapkads(String p2)
    {
        if (this.testSD(1)) {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.result = p2;
            this.runWithWait(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$25(this));
        }
        return;
    }

    void createapkcustom()
    {
        System.out.println("Create with custom patch.");
        if (this.testSD(1)) {
            this.runWithWait(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$24(this));
        }
        return;
    }

    public void createapklvl(String p2)
    {
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.result = p2;
        if (this.testSD(1)) {
            this.runWithWait(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$22(this));
        }
        return;
    }

    public boolean createapkpermissions(java.util.ArrayList p3, java.util.ArrayList p4)
    {
        this.permissions = p3;
        this.activities = p4;
        if (this.testSD(1)) {
            this.runWithWait(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$113(this));
        }
        return 1;
    }

    public void createapksupport(String p2)
    {
        if (this.testSD(1)) {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.result = p2;
            this.runWithWait(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$29(this));
        }
        return;
    }

    public void custom_patch_ker()
    {
        java.io.File v7_1 = new java.io.File(this.getContext().getDir("tmp", 0).getAbsolutePath(), "appp.apk");
        try {
            byte[] v8 = new byte[512000];
            java.io.InputStream v4 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().openRawResource(2131099655);
            java.io.FileOutputStream v5_1 = new java.io.FileOutputStream(v7_1);
            int v6 = v4.read(v8);
        } catch (String v9) {
            com.chelpus.Utils.dexoptcopy();
            com.chelpus.Utils.run_all(new StringBuilder().append("chown 0.0 ").append(this.getContext().getFilesDir().getAbsolutePath()).append("/dexopt-wrapper").toString());
            com.chelpus.Utils.run_all(new StringBuilder().append("chown 0:0 ").append(this.getContext().getFilesDir().getAbsolutePath()).append("/dexopt-wrapper").toString());
            com.chelpus.Utils.run_all(new StringBuilder().append("chmod 777 ").append(this.getContext().getFilesDir().getAbsolutePath()).append("/dexopt-wrapper").toString());
            try {
                String v1 = new StringBuilder().append(this.getContext().getFilesDir().getAbsolutePath()).append("/dexopt-wrapper ").append(v7_1.getAbsolutePath()).append(" ").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName, 0).applicationInfo.dataDir).append("/.ce0.odex").append("\n").toString();
            } catch (android.content.pm.PackageManager$NameNotFoundException v2_0) {
                v2_0.printStackTrace();
            }
            String v9_34 = new com.chelpus.Utils("");
            String v10_31 = new String[1];
            v10_31[0] = v1;
            v9_34.cmdRoot(v10_31);
            try {
                com.chelpus.Utils.run_all(new StringBuilder().append("chmod 777 ").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName, 0).applicationInfo.dataDir).append("/.ce0.odex").toString());
                com.chelpus.Utils.run_all(new StringBuilder().append("chown 0.0 ").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName, 0).applicationInfo.dataDir).append("/.ce0.odex").append("").toString());
                com.chelpus.Utils.run_all(new StringBuilder().append("chown 0:0 ").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName, 0).applicationInfo.dataDir).append("/.ce0.odex").append("").toString());
            } catch (android.content.pm.PackageManager$NameNotFoundException v2_1) {
                v2_1.printStackTrace();
            }
            v7_1.delete();
            return;
        }
        while (v6 != -1) {
            v5_1.write(v8, 0, v6);
            v6 = v4.read(v8);
        }
        v5_1.close();
    }

    void custompatch(com.android.vending.billing.InAppBillingService.LUCK.PkgListItem p14)
    {
        try {
            String v5 = this.getContext().getPackageCodePath();
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.func = 1;
            String v6 = "not_system";
        } catch (Exception v3) {
            v3.printStackTrace();
            System.out.println(new StringBuilder().append("Luckypatcher ").append(v3).toString());
            return;
        }
        if (p14.system) {
            v6 = "system";
        }
        String v1 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.customselect.toString();
        if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.customselect.getName().equals("ver.5.5.01_com.keramidas.TitaniumBackup.txt")) {
            new java.util.ArrayList().clear();
            new Thread(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$17(this)).start();
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str = "";
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$CustomPatch v2_1 = new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$CustomPatch(this);
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$CustomPatch.access$602(v2_1, com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(p14.pkgName, 0).applicationInfo.sourceDir);
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$CustomPatch.access$702(v2_1, v6);
            String v8 = String.valueOf(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(p14.pkgName, 0).applicationInfo.uid);
            String v4 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.dalvikruncommandWithFramework;
            if (!com.chelpus.Utils.isWithFramework()) {
                v4 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.dalvikruncommand;
            }
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$CustomPatch.access$802(v2_1, new StringBuilder().append(v4).append(".custompatch ").append(p14.pkgName).append(" ").append(v1).append(" ").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(p14.pkgName, 0).applicationInfo.sourceDir).append(" ").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).append(" ").append(this.getContext().getFilesDir()).append(" ").append(v5).append(" ").append(v6).append(" ").append(android.os.Build.CPU_ABI).append(" ").append("com.chelpus.root.utils").append(" ").append(com.chelpus.Utils.getCurrentRuntimeValue()).append(" ").append(v8).append("\n").toString());
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$CustomPatch.access$902(v2_1, this.getContext().getFilesDir().getAbsolutePath());
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$CustomPatch.access$1002(v2_1, String.valueOf(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(p14.pkgName, 0).applicationInfo.uid));
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$CustomPatch.access$1102(v2_1, new java.io.File(v1));
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.showDialogLP(23);
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.progress_loading.setMessage(new StringBuilder().append(com.chelpus.Utils.getText(2131165747)).append("...").toString());
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.progress_loading.setCancelable(0);
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.progress_loading.setMax(6);
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.progress_loading.setTitle(com.chelpus.Utils.getText(2131165747));
            Thread v7_1 = new Thread(v2_1);
            v7_1.setPriority(10);
            v7_1.start();
            return;
        } else {
            if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.customselect.getName().equals("ver.5.5.01_com.keramidas.TitaniumBackup.txt")) {
                return;
            } else {
                this.custom_patch_ker();
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.getItem(p14.pkgName).modified = 1;
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().edit().putBoolean(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.getItem(p14.pkgName).pkgName, 1).commit();
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.notifyDataSetChanged(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.getItem(p14.pkgName));
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str = "SU Java-Code Running!\nCongratulations! Titanium Backup cracked!\nRun Titanium Backup and check License State!\nIf Application not PRO, please reboot!\nGood Luck!\nChelpus.";
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(4);
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.showDialogLP(4);
                return;
            }
        }
    }

    public void custompatchselector()
    {
        java.util.ArrayList v0;
        if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.rebuldApk.equals("")) {
            v0 = com.chelpus.Utils.getCustomPatchesForPkg(com.chelpus.Utils.getApkInfo(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.rebuldApk).packageName);
        } else {
            v0 = com.chelpus.Utils.getCustomPatchesForPkg(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName);
        }
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adapt = new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$82(this, this.getContext(), 2130968605, v0);
        return;
    }

    public void delete_file()
    {
        if (this.testSD(1)) {
            com.chelpus.Utils.showDialogYesNo(com.chelpus.Utils.getText(2131165748), com.chelpus.Utils.getText(2131165409), new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$98(this), new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$98(this), 0);
        }
        return;
    }

    public void deodex(com.android.vending.billing.InAppBillingService.LUCK.PkgListItem p5)
    {
        com.chelpus.Utils.showDialogYesNo(com.chelpus.Utils.getText(2131165748), com.chelpus.Utils.getText(2131165535), new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$14(this, p5), new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$14(this, p5), 0);
        return;
    }

    public void dexopt_app(String p3, String p4, String p5)
    {
        this.runWithWait(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$64(this, p3, p5, p4));
        System.out.println("LuckyAppManager: Start core.jar test!");
        return;
    }

    public void dialog_reboot()
    {
        com.chelpus.Utils.showDialogYesNo(com.chelpus.Utils.getText(2131165748), com.chelpus.Utils.getText(2131165541), new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$10(this), new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$10(this), 0);
        return;
    }

    public void disable_package(String p3, boolean p4)
    {
        this.runWithWait(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$65(this, p4, p3));
        System.out.println("LuckyPatcher: Start core.jar test!");
        return;
    }

    public void disable_permission(com.android.vending.billing.InAppBillingService.LUCK.PkgListItem p36, com.android.vending.billing.InAppBillingService.LUCK.Perm p37)
    {
        String v22 = 0;
        if (new java.io.File(new StringBuilder().append(this.getContext().getDir("packages", 0).getAbsolutePath()).append("/packages1.xml").toString()).exists()) {
            try {
                String v19 = new StringBuilder().append(this.getContext().getDir("packages", 0).getAbsolutePath()).append("/packages1.xml").toString();
                String v29 = new StringBuilder().append(this.getContext().getDir("packages", 0).getAbsolutePath()).append("/packages.xml").toString();
                new com.chelpus.Utils("");
                com.chelpus.Utils.run_all(new StringBuilder().append("chmod 777 ").append(v19).toString());
                new com.chelpus.Utils("");
                com.chelpus.Utils.run_all(new StringBuilder().append("chmod 777 ").append(v29).toString());
            } catch (org.w3c.dom.Element v6_8) {
                v6_8.printStackTrace();
                java.io.File v24 = new java.io.File;
                v24(new StringBuilder().append(this.getContext().getDir("packages", 0)).append("/packages1.xml").toString());
                if (v24.exists()) {
                    v24.delete();
                }
                java.io.File v25 = new java.io.File;
                v25(new StringBuilder().append(this.getContext().getDir("packages", 0)).append("/packages.xml").toString());
                if (v25.exists()) {
                    new com.chelpus.Utils("");
                    try {
                        com.chelpus.Utils.copyFile(v25, v24);
                    } catch (org.w3c.dom.Element v6_9) {
                        v6_9.printStackTrace();
                    } catch (org.w3c.dom.Element v6_10) {
                        v6_10.printStackTrace();
                    }
                }
            }
            try {
                javax.xml.parsers.DocumentBuilder v4 = javax.xml.parsers.DocumentBuilderFactory.newInstance().newDocumentBuilder();
                java.util.ArrayList v30_23 = new java.io.FileInputStream;
                v30_23(v19);
                org.w3c.dom.Document v3 = v4.parse(v30_23);
                org.w3c.dom.NodeList v14_0 = v3.getElementsByTagName("package");
                int v7_0 = 0;
            } catch (org.w3c.dom.Element v6_6) {
                v6_6.printStackTrace();
            } catch (org.w3c.dom.Element v6_7) {
                v6_7.printStackTrace();
            } catch (javax.xml.parsers.ParserConfigurationException v15) {
                v15.printStackTrace();
            } catch (org.xml.sax.SAXException v21) {
                v21.printStackTrace();
            } catch (java.io.IOException v8) {
                v8.printStackTrace();
            }
            while (v7_0 < v14_0.getLength()) {
                org.w3c.dom.Node v11_0 = v14_0.item(v7_0);
                org.w3c.dom.Element v6_1 = ((org.w3c.dom.Element) v14_0.item(v7_0));
                if (!v6_1.getAttribute("name").equals(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName)) {
                    v7_0++;
                } else {
                    if (!v6_1.getAttribute("sharedUserId").isEmpty()) {
                        v22 = v6_1.getAttribute("sharedUserId");
                    }
                    org.w3c.dom.NodeList v18_0 = v11_0.getChildNodes();
                    int v9_0 = 0;
                    while (v9_0 < v18_0.getLength()) {
                        org.w3c.dom.Node v13_2 = v18_0.item(v9_0);
                        System.out.println(new StringBuilder().append("node:").append(v13_2.getNodeName()).toString());
                        if (v13_2.getNodeName().equals("perms")) {
                            org.w3c.dom.NodeList v17_6 = v13_2.getChildNodes();
                            int v10_6 = 0;
                            while (v10_6 < v17_6.getLength()) {
                                System.out.println(new StringBuilder().append("perm0:").append(v17_6.item(v10_6).getNodeName()).toString());
                                if (v17_6.item(v10_6).getNodeName().equals("item")) {
                                    org.w3c.dom.Element v16_17 = ((org.w3c.dom.Element) v17_6.item(v10_6));
                                    if (v16_17.getAttribute("name").equals(p37.Name)) {
                                        if (!v16_17.getAttribute("name").contains("chelpa.disabled.")) {
                                            v17_6.item(v10_6).getAttributes().getNamedItem("name").setTextContent(new StringBuilder().append("chelpa.disabled.").append(v16_17.getAttribute("name")).toString());
                                        } else {
                                            v17_6.item(v10_6).getAttributes().getNamedItem("name").setTextContent(v16_17.getAttribute("name").replaceAll("chelpa.disabled.", ""));
                                        }
                                        if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.changedPermissions.contains(p37.Name)) {
                                            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.changedPermissions.add(p37.Name);
                                        }
                                    }
                                }
                                v10_6++;
                            }
                        }
                        if (v13_2.getNodeName().equals("disabled-components")) {
                            org.w3c.dom.NodeList v17_7 = v13_2.getChildNodes();
                            int v10_7 = 0;
                            while (v10_7 < v17_7.getLength()) {
                                System.out.println(new StringBuilder().append("perm0:").append(v17_7.item(v10_7).getNodeName()).toString());
                                if (v17_7.item(v10_7).getNodeName().equals("item")) {
                                    org.w3c.dom.Element v16_15 = ((org.w3c.dom.Element) v17_7.item(v10_7));
                                    if (v16_15.getAttribute("name").equals(p37.Name)) {
                                        if (!v16_15.getAttribute("name").contains("chelpa.disabled.")) {
                                            v17_7.item(v10_7).getAttributes().getNamedItem("name").setTextContent(new StringBuilder().append("chelpa.disabled.").append(v16_15.getAttribute("name")).toString());
                                        } else {
                                            v17_7.item(v10_7).getAttributes().getNamedItem("name").setTextContent(v16_15.getAttribute("name").replaceAll("chelpa.disabled.", ""));
                                        }
                                        if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.changedPermissions.contains(p37.Name)) {
                                            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.changedPermissions.add(p37.Name);
                                        }
                                    }
                                }
                                v10_7++;
                            }
                        }
                        if (v13_2.getNodeName().equals("enabled-components")) {
                            org.w3c.dom.NodeList v17_8 = v13_2.getChildNodes();
                            int v10_8 = 0;
                            while (v10_8 < v17_8.getLength()) {
                                System.out.println(new StringBuilder().append("perm0:").append(v17_8.item(v10_8).getNodeName()).toString());
                                if (v17_8.item(v10_8).getNodeName().equals("item")) {
                                    org.w3c.dom.Element v16_13 = ((org.w3c.dom.Element) v17_8.item(v10_8));
                                    if (v16_13.getAttribute("name").equals(p37.Name)) {
                                        if (!v16_13.getAttribute("name").contains("chelpa.disabled.")) {
                                            v17_8.item(v10_8).getAttributes().getNamedItem("name").setTextContent(new StringBuilder().append("chelpa.disabled.").append(v16_13.getAttribute("name")).toString());
                                        } else {
                                            v17_8.item(v10_8).getAttributes().getNamedItem("name").setTextContent(v16_13.getAttribute("name").replaceAll("chelpa.disabled.", ""));
                                        }
                                        if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.changedPermissions.contains(p37.Name)) {
                                            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.changedPermissions.add(p37.Name);
                                        }
                                    }
                                }
                                v10_8++;
                            }
                        }
                        v9_0++;
                    }
                }
            }
            org.w3c.dom.NodeList v14_1 = v3.getElementsByTagName("updated-package");
            int v7_1 = 0;
            while (v7_1 < v14_1.getLength()) {
                org.w3c.dom.Node v11_1 = v14_1.item(v7_1);
                org.w3c.dom.Element v6_3 = ((org.w3c.dom.Element) v14_1.item(v7_1));
                if (!v6_3.getAttribute("name").equals(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName)) {
                    v7_1++;
                } else {
                    if (!v6_3.getAttribute("sharedUserId").isEmpty()) {
                        v22 = v6_3.getAttribute("sharedUserId");
                    }
                    org.w3c.dom.NodeList v18_1 = v11_1.getChildNodes();
                    int v9_1 = 0;
                    while (v9_1 < v18_1.getLength()) {
                        org.w3c.dom.Node v13_1 = v18_1.item(v9_1);
                        System.out.println(new StringBuilder().append("node:").append(v13_1.getNodeName()).toString());
                        if (v13_1.getNodeName().equals("perms")) {
                            org.w3c.dom.NodeList v17_3 = v13_1.getChildNodes();
                            int v10_3 = 0;
                            while (v10_3 < v17_3.getLength()) {
                                System.out.println(new StringBuilder().append("perm0:").append(v17_3.item(v10_3).getNodeName()).toString());
                                if (v17_3.item(v10_3).getNodeName().equals("item")) {
                                    org.w3c.dom.Element v16_11 = ((org.w3c.dom.Element) v17_3.item(v10_3));
                                    if (v16_11.getAttribute("name").equals(p37.Name)) {
                                        if (!v16_11.getAttribute("name").contains("chelpa.disabled.")) {
                                            v17_3.item(v10_3).getAttributes().getNamedItem("name").setTextContent(new StringBuilder().append("chelpa.disabled.").append(v16_11.getAttribute("name")).toString());
                                        } else {
                                            v17_3.item(v10_3).getAttributes().getNamedItem("name").setTextContent(v16_11.getAttribute("name").replaceAll("chelpa.disabled.", ""));
                                        }
                                        if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.changedPermissions.contains(p37.Name)) {
                                            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.changedPermissions.add(p37.Name);
                                        }
                                    }
                                }
                                v10_3++;
                            }
                        }
                        if (v13_1.getNodeName().equals("disabled-components")) {
                            org.w3c.dom.NodeList v17_4 = v13_1.getChildNodes();
                            int v10_4 = 0;
                            while (v10_4 < v17_4.getLength()) {
                                System.out.println(new StringBuilder().append("perm0:").append(v17_4.item(v10_4).getNodeName()).toString());
                                if (v17_4.item(v10_4).getNodeName().equals("item")) {
                                    org.w3c.dom.Element v16_9 = ((org.w3c.dom.Element) v17_4.item(v10_4));
                                    if (v16_9.getAttribute("name").equals(p37.Name)) {
                                        if (!v16_9.getAttribute("name").contains("chelpa.disabled.")) {
                                            v17_4.item(v10_4).getAttributes().getNamedItem("name").setTextContent(new StringBuilder().append("chelpa.disabled.").append(v16_9.getAttribute("name")).toString());
                                        } else {
                                            v17_4.item(v10_4).getAttributes().getNamedItem("name").setTextContent(v16_9.getAttribute("name").replaceAll("chelpa.disabled.", ""));
                                        }
                                        if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.changedPermissions.contains(p37.Name)) {
                                            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.changedPermissions.add(p37.Name);
                                        }
                                    }
                                }
                                v10_4++;
                            }
                        }
                        if (v13_1.getNodeName().equals("enabled-components")) {
                            org.w3c.dom.NodeList v17_5 = v13_1.getChildNodes();
                            int v10_5 = 0;
                            while (v10_5 < v17_5.getLength()) {
                                System.out.println(new StringBuilder().append("perm0:").append(v17_5.item(v10_5).getNodeName()).toString());
                                if (v17_5.item(v10_5).getNodeName().equals("item")) {
                                    org.w3c.dom.Element v16_7 = ((org.w3c.dom.Element) v17_5.item(v10_5));
                                    if (v16_7.getAttribute("name").equals(p37.Name)) {
                                        if (!v16_7.getAttribute("name").contains("chelpa.disabled.")) {
                                            v17_5.item(v10_5).getAttributes().getNamedItem("name").setTextContent(new StringBuilder().append("chelpa.disabled.").append(v16_7.getAttribute("name")).toString());
                                        } else {
                                            v17_5.item(v10_5).getAttributes().getNamedItem("name").setTextContent(v16_7.getAttribute("name").replaceAll("chelpa.disabled.", ""));
                                        }
                                        if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.changedPermissions.contains(p37.Name)) {
                                            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.changedPermissions.add(p37.Name);
                                        }
                                    }
                                }
                                v10_5++;
                            }
                        }
                        v9_1++;
                    }
                }
            }
            if (v22 != null) {
                org.w3c.dom.NodeList v14_2 = v3.getElementsByTagName("shared-user");
                int v7_2 = 0;
                while (v7_2 < v14_2.getLength()) {
                    org.w3c.dom.Node v11_2 = v14_2.item(v7_2);
                    if (!((org.w3c.dom.Element) v14_2.item(v7_2)).getAttribute("userId").equals(v22)) {
                        v7_2++;
                    } else {
                        org.w3c.dom.NodeList v18_2 = v11_2.getChildNodes();
                        int v9_2 = 0;
                        while (v9_2 < v18_2.getLength()) {
                            org.w3c.dom.Node v13_0 = v18_2.item(v9_2);
                            System.out.println(new StringBuilder().append("node:").append(v13_0.getNodeName()).toString());
                            if (v13_0.getNodeName().equals("perms")) {
                                org.w3c.dom.NodeList v17_0 = v13_0.getChildNodes();
                                int v10_0 = 0;
                                while (v10_0 < v17_0.getLength()) {
                                    System.out.println(new StringBuilder().append("perm0:").append(v17_0.item(v10_0).getNodeName()).toString());
                                    if (v17_0.item(v10_0).getNodeName().equals("item")) {
                                        org.w3c.dom.Element v16_5 = ((org.w3c.dom.Element) v17_0.item(v10_0));
                                        if (v16_5.getAttribute("name").equals(p37.Name)) {
                                            if (!v16_5.getAttribute("name").contains("chelpa.disabled.")) {
                                                v17_0.item(v10_0).getAttributes().getNamedItem("name").setTextContent(new StringBuilder().append("chelpa.disabled.").append(v16_5.getAttribute("name")).toString());
                                            } else {
                                                v17_0.item(v10_0).getAttributes().getNamedItem("name").setTextContent(v16_5.getAttribute("name").replaceAll("chelpa.disabled.", ""));
                                            }
                                            if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.changedPermissions.contains(p37.Name)) {
                                                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.changedPermissions.add(p37.Name);
                                            }
                                        }
                                    }
                                    v10_0++;
                                }
                            }
                            if (v13_0.getNodeName().equals("disabled-components")) {
                                org.w3c.dom.NodeList v17_1 = v13_0.getChildNodes();
                                int v10_1 = 0;
                                while (v10_1 < v17_1.getLength()) {
                                    System.out.println(new StringBuilder().append("perm0:").append(v17_1.item(v10_1).getNodeName()).toString());
                                    if (v17_1.item(v10_1).getNodeName().equals("item")) {
                                        org.w3c.dom.Element v16_3 = ((org.w3c.dom.Element) v17_1.item(v10_1));
                                        if (v16_3.getAttribute("name").equals(p37.Name)) {
                                            if (!v16_3.getAttribute("name").contains("chelpa.disabled.")) {
                                                v17_1.item(v10_1).getAttributes().getNamedItem("name").setTextContent(new StringBuilder().append("chelpa.disabled.").append(v16_3.getAttribute("name")).toString());
                                            } else {
                                                v17_1.item(v10_1).getAttributes().getNamedItem("name").setTextContent(v16_3.getAttribute("name").replaceAll("chelpa.disabled.", ""));
                                            }
                                            if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.changedPermissions.contains(p37.Name)) {
                                                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.changedPermissions.add(p37.Name);
                                            }
                                        }
                                    }
                                    v10_1++;
                                }
                            }
                            if (v13_0.getNodeName().equals("enabled-components")) {
                                org.w3c.dom.NodeList v17_2 = v13_0.getChildNodes();
                                int v10_2 = 0;
                                while (v10_2 < v17_2.getLength()) {
                                    System.out.println(new StringBuilder().append("perm0:").append(v17_2.item(v10_2).getNodeName()).toString());
                                    if (v17_2.item(v10_2).getNodeName().equals("item")) {
                                        org.w3c.dom.Element v16_1 = ((org.w3c.dom.Element) v17_2.item(v10_2));
                                        if (v16_1.getAttribute("name").equals(p37.Name)) {
                                            if (!v16_1.getAttribute("name").contains("chelpa.disabled.")) {
                                                v17_2.item(v10_2).getAttributes().getNamedItem("name").setTextContent(new StringBuilder().append("chelpa.disabled.").append(v16_1.getAttribute("name")).toString());
                                            } else {
                                                v17_2.item(v10_2).getAttributes().getNamedItem("name").setTextContent(v16_1.getAttribute("name").replaceAll("chelpa.disabled.", ""));
                                            }
                                            if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.changedPermissions.contains(p37.Name)) {
                                                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.changedPermissions.add(p37.Name);
                                            }
                                        }
                                    }
                                    v10_2++;
                                }
                            }
                            v9_2++;
                        }
                    }
                }
            }
            javax.xml.transform.Transformer v26 = javax.xml.transform.TransformerFactory.newInstance().newTransformer();
            javax.xml.transform.dom.DOMSource v23 = new javax.xml.transform.dom.DOMSource;
            v23(v3);
            javax.xml.transform.stream.StreamResult v20 = new javax.xml.transform.stream.StreamResult;
            java.util.ArrayList v30_50 = new java.io.File;
            v30_50(v29);
            v20(v30_50);
            v26.transform(v23, v20);
            System.out.println("Done");
        }
        return;
    }

    void download_custom_patches()
    {
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$DownloadFile v0_1 = new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$DownloadFile(this);
        v0_1.filename = "CustomPatches.zip";
        v0_1.on_post_run_cached = new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$87(this);
        v0_1.on_post_run_downloaded = new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$88(this);
        String[] v1_6 = new String[1];
        v1_6[0] = "CustomPatches.zip";
        v0_1.execute(v1_6);
        return;
    }

    public void expandAll()
    {
        int v0 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.getGroupCount();
        int v1 = 0;
        while (v1 < v0) {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.lv.expandGroup(v1);
            v1++;
        }
        return;
    }

    public void getComponents()
    {
        java.util.ArrayList v13_1 = new java.util.ArrayList();
        try {
            android.content.pm.PackageInfo v12 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName, 4111);
        } catch (RuntimeException v10) {
            v10.printStackTrace();
            try {
                v12 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName, 4110);
            } catch (android.content.pm.PackageManager$NameNotFoundException v11_0) {
                v11_0.printStackTrace();
            }
        } catch (android.content.pm.PackageManager$NameNotFoundException v11_1) {
            v11_1.printStackTrace();
        }
        if ((v12.activities != null) && (v12.activities.length != 0)) {
            v13_1.add(new com.android.vending.billing.InAppBillingService.LUCK.Components(com.chelpus.Utils.getText(2131165479), 1, 0, 0, 0, 0, 0, 1));
            int v9_0 = 0;
            while (v9_0 < v12.activities.length) {
                if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName, v12.activities[v9_0].name)) != 2) {
                    v13_1.add(new com.android.vending.billing.InAppBillingService.LUCK.Components(v12.activities[v9_0].name, 1, 0, 1, 0, 0, 0, 0));
                } else {
                    v13_1.add(new com.android.vending.billing.InAppBillingService.LUCK.Components(v12.activities[v9_0].name, 0, 0, 1, 0, 0, 0, 0));
                }
                v9_0++;
            }
        }
        if ((v12.receivers != null) && (v12.receivers.length != 0)) {
            v13_1.add(new com.android.vending.billing.InAppBillingService.LUCK.Components(com.chelpus.Utils.getText(2131165481), 1, 0, 0, 0, 0, 0, 1));
            int v9_1 = 0;
            while (v9_1 < v12.receivers.length) {
                if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName, v12.receivers[v9_1].name)) != 2) {
                    v13_1.add(new com.android.vending.billing.InAppBillingService.LUCK.Components(v12.receivers[v9_1].name, 1, 0, 0, 0, 0, 1, 0));
                } else {
                    v13_1.add(new com.android.vending.billing.InAppBillingService.LUCK.Components(v12.receivers[v9_1].name, 0, 0, 0, 0, 0, 1, 0));
                }
                v9_1++;
            }
        }
        if ((v12.providers != null) && (v12.providers.length != 0)) {
            v13_1.add(new com.android.vending.billing.InAppBillingService.LUCK.Components(com.chelpus.Utils.getText(2131165480), 1, 0, 0, 0, 0, 0, 1));
            int v9_2 = 0;
            while (v9_2 < v12.providers.length) {
                if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName, v12.providers[v9_2].name)) != 2) {
                    v13_1.add(new com.android.vending.billing.InAppBillingService.LUCK.Components(v12.providers[v9_2].name, 1, 0, 0, 1, 0, 0, 0));
                } else {
                    v13_1.add(new com.android.vending.billing.InAppBillingService.LUCK.Components(v12.providers[v9_2].name, 0, 0, 0, 1, 0, 0, 0));
                }
                v9_2++;
            }
        }
        if ((v12.services != null) && (v12.services.length != 0)) {
            v13_1.add(new com.android.vending.billing.InAppBillingService.LUCK.Components(com.chelpus.Utils.getText(2131165482), 1, 0, 0, 0, 0, 0, 1));
            int v9_3 = 0;
            while (v9_3 < v12.services.length) {
                if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName, v12.services[v9_3].name)) != 2) {
                    v13_1.add(new com.android.vending.billing.InAppBillingService.LUCK.Components(v12.services[v9_3].name, 1, 0, 0, 0, 1, 0, 0));
                } else {
                    v13_1.add(new com.android.vending.billing.InAppBillingService.LUCK.Components(v12.services[v9_3].name, 0, 0, 0, 0, 1, 0, 0));
                }
                v9_3++;
            }
        }
        if (v13_1.size() <= 0) {
            v13_1.add(new com.android.vending.billing.InAppBillingService.LUCK.Components(com.chelpus.Utils.getText(2131165626), 1, 0, 0, 0, 0, 0, 1));
        }
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.component_adapt != null) {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.component_adapt.notifyDataSetChanged();
        }
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.component_adapt = new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$120(this, this.getContext(), 2130968605, v13_1);
        return;
    }

    public android.support.v4.app.FragmentActivity getContext()
    {
        if (this.isAdded()) {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchAct = ((com.android.vending.billing.InAppBillingService.LUCK.patchActivity) this.getActivity());
        }
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchAct == null) {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchAct = ((com.android.vending.billing.InAppBillingService.LUCK.patchActivity) com.android.vending.billing.InAppBillingService.LUCK.LuckyApp.getInstance());
        }
        return com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchAct;
    }

    public void getDir(String p10, android.widget.ListView p11, boolean p12)
    {
        System.out.println("dir");
        this.myPath.setText(new StringBuilder().append("Location: ").append(p10).toString());
        java.util.ArrayList v4_1 = new java.util.ArrayList();
        java.io.File v0_1 = new java.io.File(p10);
        java.io.File[] v3 = v0_1.listFiles();
        // Both branches of the condition point to the same code.
        // if (v3 != null) {
            if (p10.equals(this.root)) {
                if ((!this.root.equals(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath)) && (p12)) {
                    v4_1.add(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$ItemFile(this, com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath));
                }
            } else {
                v4_1.add(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$ItemFile(this, this.root));
                v4_1.add(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$ItemFile(this, "../", v0_1.getParent()));
            }
            int v6_10 = v3.length;
            int v5_12 = 0;
            while (v5_12 < v6_10) {
                java.io.File v1 = v3[v5_12];
                if ((v1.canRead()) && ((v1.isDirectory()) || ((v1.toString().toLowerCase().endsWith(".apk")) || ((v1.toString().toLowerCase().endsWith(".odex")) || ((v1.toString().toLowerCase().endsWith(".jar")) || (v1.toString().toLowerCase().endsWith(".oat"))))))) {
                    v4_1.add(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$ItemFile(this, v1.toString()));
                }
                v5_12++;
            }
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$58 v2_1 = new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$58(this, this.getContext(), 2130968615, v4_1);
            v2_1.sort(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$byNameFile(this));
            p11.setAdapter(v2_1);
            v2_1.notifyDataSetChanged();
            return;
        // }
    }

    public void getLPActivity()
    {
        java.util.ArrayList v3_1 = new java.util.ArrayList();
        try {
            android.content.pm.PackageInfo v2 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName, 1);
        } catch (android.content.pm.PackageManager$NameNotFoundException v1) {
            v1.printStackTrace();
        }
        int v0 = 0;
        while (v0 < v2.activities.length) {
            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName, v2.activities[v0].name)) != 2) {
                v3_1.add(new com.android.vending.billing.InAppBillingService.LUCK.Perm(v2.activities[v0].name, 1));
            } else {
                v3_1.add(new com.android.vending.billing.InAppBillingService.LUCK.Perm(v2.activities[v0].name, 0));
            }
            v0++;
        }
        if (v3_1.size() <= 0) {
            v3_1.add(new com.android.vending.billing.InAppBillingService.LUCK.Perm(com.chelpus.Utils.getText(2131165626), 0));
        }
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.perm_adapt != null) {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.perm_adapt.notifyDataSetChanged();
        }
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.perm_adapt = new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$118(this, this.getContext(), 2130968605, v3_1);
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.perm_adapt.sort(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$119(this));
        return;
    }

    public void getpackagesxml()
    {
        java.io.File v2_1 = new java.io.File(new StringBuilder().append(this.getContext().getDir("packages", 0)).append("/packages1.xml").toString());
        if (v2_1.exists()) {
            v2_1.delete();
        }
        java.io.File v2_3 = new java.io.File(new StringBuilder().append(this.getContext().getDir("packages", 0)).append("/packages.xml").toString());
        if (v2_3.exists()) {
            v2_3.delete();
        }
        try {
            com.chelpus.Utils.copyFile("/data/system/packages.xml", new StringBuilder().append(this.getContext().getDir("packages", 0).getAbsolutePath()).append("/packages1.xml").toString(), 0, 1);
            com.chelpus.Utils.copyFile("/data/system/packages.xml", new StringBuilder().append(this.getContext().getDir("packages", 0).getAbsolutePath()).append("/packages.xml").toString(), 0, 1);
            com.chelpus.Utils.run_all(new StringBuilder().append("chmod 0777 ").append(this.getContext().getDir("packages", 0).getAbsolutePath()).append("/packages1.xml").toString());
            com.chelpus.Utils.run_all(new StringBuilder().append("chmod 0777 ").append(this.getContext().getDir("packages", 0).getAbsolutePath()).append("/packages.xml").toString());
        } catch (Exception v1_0) {
            android.widget.Toast.makeText(this.getContext(), new StringBuilder().append("").append(v1_0).toString(), 1).show();
        }
        String v0_0 = new StringBuilder().append(this.getContext().getDir("packages", 0).getAbsolutePath()).append("/packages.xml").toString();
        if ((!new java.io.File(v0_0).exists()) && (new java.io.File(v0_0).length() == 0)) {
            try {
                com.chelpus.Utils.copyFile("/dbdata/system/packages.xml", new StringBuilder().append(this.getContext().getDir("packages", 0).getAbsolutePath()).append("/packages1.xml").toString(), 0, 1);
                com.chelpus.Utils.copyFile("/dbdata/system/packages.xml", new StringBuilder().append(this.getContext().getDir("packages", 0).getAbsolutePath()).append("/packages.xml").toString(), 0, 1);
                com.chelpus.Utils.run_all(new StringBuilder().append("chmod 0777 ").append(this.getContext().getDir("packages", 0).getAbsolutePath()).append("/packages1.xml").toString());
                com.chelpus.Utils.run_all(new StringBuilder().append("chmod 0777 ").append(this.getContext().getDir("packages", 0).getAbsolutePath()).append("/packages.xml").toString());
            } catch (Exception v1_1) {
                android.widget.Toast.makeText(this.getContext(), new StringBuilder().append("").append(v1_1).toString(), 1).show();
            }
            if (!new java.io.File(new StringBuilder().append(this.getContext().getDir("packages", 0).getAbsolutePath()).append("/packages.xml").toString()).exists()) {
                android.widget.Toast.makeText(this.getContext(), "Error: packages.xml not found!", 1).show();
            }
        }
        return;
    }

    public void getpermissions()
    {
        java.util.ArrayList v8_1 = new java.util.ArrayList();
        if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.rebuldApk.equals("")) {
            android.content.pm.PackageManager v6_0 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng();
            android.content.pm.PackageInfo v3_0 = v6_0.getPackageArchiveInfo(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.rebuldApk, 4096);
            if (v3_0 != null) {
                android.content.pm.ApplicationInfo v0 = v3_0.applicationInfo;
                if (android.os.Build$VERSION.SDK_INT >= 8) {
                    v0.sourceDir = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.rebuldApk;
                    v0.publicSourceDir = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.rebuldApk;
                }
            }
            v8_1 = new java.util.ArrayList();
            try {
                String[] v7_0 = v3_0.requestedPermissions;
                System.out.println(v7_0);
            } catch (com.android.vending.billing.InAppBillingService.LUCK.Perm v9) {
                android.content.pm.PackageInfo v3_1 = v6_0.getPackageArchiveInfo(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.rebuldApk, 1);
                int v1_0 = 0;
                while (v1_0 < v3_1.activities.length) {
                    if (!v3_1.activities[v1_0].name.startsWith("disabled_")) {
                        v8_1.add(new com.android.vending.billing.InAppBillingService.LUCK.Perm(new StringBuilder().append("chelpus_").append(v3_1.activities[v1_0].name).toString(), 1));
                    }
                    if (v3_1.activities[v1_0].name.startsWith("disabled_")) {
                        v8_1.add(new com.android.vending.billing.InAppBillingService.LUCK.Perm(new StringBuilder().append("chelpus_").append(v3_1.activities[v1_0].name).toString(), 0));
                    }
                    v1_0++;
                }
            }
            if (v7_0 == null) {
            } else {
                String v10_3 = v7_0.length;
                com.android.vending.billing.InAppBillingService.LUCK.Perm v9_7 = 0;
                while (v9_7 < v10_3) {
                    String v4_0 = v7_0[v9_7];
                    System.out.println(v4_0);
                    if (!v4_0.startsWith("disabled_")) {
                        v8_1.add(new com.android.vending.billing.InAppBillingService.LUCK.Perm(v4_0, 1));
                    } else {
                        v8_1.add(new com.android.vending.billing.InAppBillingService.LUCK.Perm(new StringBuilder().append("chelpa_per_").append(v4_0.replace("disabled_", "")).toString(), 0));
                    }
                    v9_7++;
                }
            }
        } else {
            android.content.pm.PackageManager v6_1 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng();
            try {
                String[] v7_1 = v6_1.getPackageInfo(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName, 4096).requestedPermissions;
            } catch (android.content.pm.PackageManager$NameNotFoundException v2_0) {
                v2_0.printStackTrace();
                try {
                    android.content.pm.PackageInfo v5 = v6_1.getPackageInfo(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName, 1);
                    int v1_1 = 0;
                } catch (com.android.vending.billing.InAppBillingService.LUCK.Perm v9) {
                } catch (android.content.pm.PackageManager$NameNotFoundException v2_1) {
                    v2_1.printStackTrace();
                }
                while (v1_1 < v5.activities.length) {
                    if (!v5.activities[v1_1].name.startsWith("disabled_")) {
                        v8_1.add(new com.android.vending.billing.InAppBillingService.LUCK.Perm(new StringBuilder().append("chelpus_").append(v5.activities[v1_1].name).toString(), 1));
                    }
                    if (v5.activities[v1_1].name.startsWith("disabled_")) {
                        v8_1.add(new com.android.vending.billing.InAppBillingService.LUCK.Perm(new StringBuilder().append("chelpus_").append(v5.activities[v1_1].name).toString(), 0));
                    }
                    v1_1++;
                }
            } catch (com.android.vending.billing.InAppBillingService.LUCK.Perm v9) {
            }
            if (v7_1 == null) {
            } else {
                String v10_18 = v7_1.length;
                com.android.vending.billing.InAppBillingService.LUCK.Perm v9_26 = 0;
                while (v9_26 < v10_18) {
                    String v4_1 = v7_1[v9_26];
                    if (!v4_1.startsWith("disabled_")) {
                        v8_1.add(new com.android.vending.billing.InAppBillingService.LUCK.Perm(v4_1, 1));
                    } else {
                        v8_1.add(new com.android.vending.billing.InAppBillingService.LUCK.Perm(new StringBuilder().append("chelpa_per_").append(v4_1.replace("disabled_", "")).toString(), 0));
                    }
                    v9_26++;
                }
            }
        }
        if (v8_1.size() <= 0) {
            v8_1.add(new com.android.vending.billing.InAppBillingService.LUCK.Perm(com.chelpus.Utils.getText(2131165626), 0));
        }
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.perm_adapt != null) {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.perm_adapt.notifyDataSetChanged();
        }
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.perm_adapt = new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$117(this, this.getContext(), 2130968605, v8_1);
        return;
    }

    public void hideMenu()
    {
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.menu_open = 0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.menu_adapter.clear();
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.menu_lv.setVisibility(8);
        ((android.widget.LinearLayout) com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchAct.findViewById(2131558607)).setVisibility(0);
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.runResume) {
            this.onResume();
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.runResume = 0;
        }
        return;
    }

    public void install_system(com.android.vending.billing.InAppBillingService.LUCK.FileApkListItem p3)
    {
        if (!this.testSD(1)) {
            if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.progress2 != null) && (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.progress2.isShowing())) {
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.progress2.dismiss();
            }
        } else {
            this.datadir = "empty";
            String v0 = p3.pkgName;
            if (v0 != null) {
                this.runWithWait(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$100(this, v0, p3));
            }
        }
        return;
    }

    public void iptablescopy()
    {
        return;
    }

    public void launch_click()
    {
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getBoolean("vibration", 0)) {
            this.vib = ((android.os.Vibrator) this.getContext().getSystemService("vibrator"));
            this.vib.vibrate(50);
        }
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(6);
        new Thread(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$50(this)).start();
        return;
    }

    public void mod_market_check()
    {
        new Thread(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$81(this)).start();
        return;
    }

    public void move_to_internal_memory_click()
    {
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getBoolean("vibration", 0)) {
            this.vib = ((android.os.Vibrator) this.getContext().getSystemService("vibrator"));
            this.vib.vibrate(50);
        }
        com.chelpus.Utils.showDialogYesNo(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.name, com.chelpus.Utils.getText(2131165555), new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$52(this), new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$52(this), 0);
        return;
    }

    public void move_to_sdcard_click()
    {
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getBoolean("vibration", 0)) {
            this.vib = ((android.os.Vibrator) this.getContext().getSystemService("vibrator"));
            this.vib.vibrate(50);
        }
        com.chelpus.Utils.showDialogYesNo(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.name, com.chelpus.Utils.getText(2131165557), new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$53(this), new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$53(this), 0);
        return;
    }

    public void move_to_system_click()
    {
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getBoolean("vibration", 0)) {
            this.vib = ((android.os.Vibrator) this.getContext().getSystemService("vibrator"));
            this.vib.vibrate(50);
        }
        com.chelpus.Utils.showDialogYesNo(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.name, com.chelpus.Utils.getText(2131165559), new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$51(this), new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$51(this), 0);
        return;
    }

    public void new_method_lvl()
    {
        this.runWithWait(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$124(this));
        return;
    }

    public void odex(com.android.vending.billing.InAppBillingService.LUCK.PkgListItem p15)
    {
        int v5_0 = 0;
        try {
            String v4 = "not_system";
        } catch (Exception v0) {
            android.widget.Toast.makeText(this.getContext(), new StringBuilder().append("").append(v0).toString(), 1).show();
            return;
        }
        if (p15.system) {
            v4 = "system";
        }
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str = "";
        String v6_3 = new com.chelpus.Utils("");
        android.widget.Toast v7_2 = new String[1];
        v7_2[0] = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.dalvikruncommand).append(".odex ").append(p15.pkgName).append(" ").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(p15.pkgName, 0).applicationInfo.sourceDir).append(" ").append(v4).append(" ").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getFilesDir()).append(" ").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(p15.pkgName, 0).applicationInfo.uid).toString();
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str = v6_3.cmdRoot(v7_2);
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.split("\n");
        String[] v2 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str.split("\n");
        String v6_9 = v2.length;
        while (v5_0 < v6_9) {
            String v1 = v2[v5_0];
            if (v1.contains("Changes Fix to: ")) {
                v1.replace("Changes Fix to: ", "");
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.getItem(p15.pkgName).odex = 1;
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.notifyDataSetChanged(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.getItem(p15.pkgName));
                android.widget.Toast.makeText(this.getContext(), "Dalvik-cache fixed!", 1).show();
            }
            v5_0++;
        }
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.updateItem(p15.pkgName);
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.notifyDataSetChanged();
        return;
    }

    public void odex_all_system_app()
    {
        com.chelpus.Utils.showDialogYesNo(com.chelpus.Utils.getText(2131165748), com.chelpus.Utils.getText(2131165585), new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$6(this), new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$6(this), 0);
        return;
    }

    public void onActivityCreated(android.os.Bundle p2)
    {
        super.onActivityCreated(p2);
        this.setRetainInstance(1);
        com.android.vending.billing.InAppBillingService.LUCK.patchActivity.frag = this;
        this.setHasOptionsMenu(1);
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag = this;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.handler = new android.os.Handler();
        return;
    }

    public void onCreate(android.os.Bundle p7)
    {
        super.onCreate(p7);
        System.out.println("LuckyPatcher: create fragment.");
        this.setRetainInstance(1);
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.init();
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag = this;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.handler = new android.os.Handler();
        if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchOnBoot) {
            try {
                ((android.app.NotificationManager) this.getContext().getSystemService("notification")).cancelAll();
            } catch (SecurityException v0) {
                v0.printStackTrace();
            }
        }
        if (this.isAdded()) {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchAct = ((com.android.vending.billing.InAppBillingService.LUCK.patchActivity) super.getActivity());
        }
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.appcontext = super.getActivity();
        Thread v3_1 = new Thread(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$2(this));
        v3_1.setPriority(10);
        v3_1.start();
        return;
    }

    public void onCreateOptionsMenu(android.view.Menu p2, android.view.MenuInflater p3)
    {
        super.onCreateOptionsMenu(p2, p3);
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.menu1 = p2;
        p3.inflate(2131492864, p2);
        return;
    }

    public android.view.View onCreateView(android.view.LayoutInflater p7, android.view.ViewGroup p8, android.os.Bundle p9)
    {
        android.view.View v3;
        System.out.println("View created");
        if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su) {
            v3 = p7.inflate(2130968629, p8);
        } else {
            v3 = p7.inflate(2130968628, p8);
        }
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.lv = ((android.widget.ExpandableListView) v3.findViewById(2131558613));
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.menu_lv = ((android.widget.ExpandableListView) v3.findViewById(2131558606));
        android.widget.HorizontalScrollView v2_1 = ((android.widget.HorizontalScrollView) v3.findViewById(2131558614));
        android.widget.Button v0_1 = ((android.widget.Button) v2_1.findViewById(2131558615));
        if (v0_1 != null) {
            v0_1.setTextAppearance(this.getContext(), com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getSizeText());
            v0_1.setTextColor(android.graphics.Color.parseColor("#999999"));
        }
        android.widget.Button v0_3 = ((android.widget.Button) v2_1.findViewById(2131558616));
        if (v0_3 != null) {
            v0_3.setTextAppearance(this.getContext(), com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getSizeText());
            v0_3.setTextColor(android.graphics.Color.parseColor("#999999"));
        }
        android.widget.Button v0_5 = ((android.widget.Button) v2_1.findViewById(2131558474));
        if (v0_5 != null) {
            v0_5.setTextAppearance(this.getContext(), com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getSizeText());
            v0_5.setTextColor(android.graphics.Color.parseColor("#999999"));
        }
        android.widget.Button v0_7 = ((android.widget.Button) v2_1.findViewById(2131558617));
        if (v0_7 != null) {
            v0_7.setTextAppearance(this.getContext(), com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getSizeText());
            v0_7.setTextColor(android.graphics.Color.parseColor("#999999"));
        }
        android.widget.Button v0_9 = ((android.widget.Button) v2_1.findViewById(2131558618));
        if (v0_9 != null) {
            v0_9.setTextAppearance(this.getContext(), com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getSizeText());
            v0_9.setTextColor(android.graphics.Color.parseColor("#999999"));
        }
        android.widget.Button v0_11 = ((android.widget.Button) v2_1.findViewById(2131558619));
        if (v0_11 != null) {
            v0_11.setTextAppearance(this.getContext(), com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getSizeText());
            v0_11.setTextColor(android.graphics.Color.parseColor("#999999"));
        }
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.data != null) {
            this.populateAdapter(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.data, this.getSortPref());
        } else {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.sqlScan = 1;
            Thread v1_1 = new Thread(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$AppScanning(this));
            v1_1.setPriority(10);
            v1_1.start();
        }
        return v3;
    }

    public void onDestroy()
    {
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.desktop_launch = 0;
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su) {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.exitSu();
        }
        System.out.println("LuckyPatcher: EXIT!");
        System.runFinalizersOnExit(1);
        super.onDestroy();
        if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchOnBoot) {
            System.exit(0);
        }
        this.getContext().finish();
        return;
    }

    public void onDestroyView()
    {
        super.onDestroyView();
        return;
    }

    public boolean onOptionsItemSelected(android.view.MenuItem p4)
    {
        int v0_1;
        switch (p4.getItemId()) {
            case 2131558640:
                this.download_custom_patches();
                v0_1 = 1;
                break;
            case 2131558641:
                ((com.android.vending.billing.InAppBillingService.LUCK.patchActivity) this.getActivity()).toolbar_settings_click();
                v0_1 = 1;
                break;
            case 2131558642:
                this.contextXposedPatch();
                v0_1 = 1;
                break;
            case 2131558643:
                this.contexttruble();
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(7);
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.showDialogLP(7);
                v0_1 = 1;
                break;
            default:
                v0_1 = super.onOptionsItemSelected(p4);
        }
        return v0_1;
    }

    public void onPause()
    {
        super.onPause();
        System.out.println("onPause");
        if (this.isAdded()) {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchAct = ((com.android.vending.billing.InAppBillingService.LUCK.patchActivity) this.getActivity());
        }
        return;
    }

    public void onPrepareOptionsMenu(android.view.Menu p2)
    {
        if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su) {
            p2.removeItem(2131558643);
        }
        if (!com.chelpus.Utils.hasXposed()) {
            p2.removeItem(2131558642);
        }
        return;
    }

    public void onResume()
    {
        this = super.onResume();
        System.out.println("onResume()");
        try {
            if (this.isAdded()) {
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchAct = ((com.android.vending.billing.InAppBillingService.LUCK.patchActivity) this.getActivity());
            }
        } catch (Exception v8_0) {
            System.out.println("LuckyPatcher (onResume): sdcard not found!");
            v8_0.printStackTrace();
        }
        try {
            if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getBoolean("fast_start", 0)) {
                if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getBoolean("path_changed", 0)) {
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.refresh = 1;
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().edit().putBoolean("path_changed", 0).commit();
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().edit().putString("basepath", com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).commit();
                    if ((new java.io.File(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).mkdirs()) || (new java.io.File(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).exists())) {
                        android.widget.Toast.makeText(this.getActivity(), new StringBuilder().append("").append(com.chelpus.Utils.getText(2131165414)).append("\n").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).toString(), 0).show();
                    }
                }
                if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getBoolean("settings_change", 0)) {
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.refresh = 1;
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().edit().putBoolean("settings_change", 0).commit();
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.sqlScanLite = 1;
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.sqlScan = 0;
                    com.android.vending.billing.InAppBillingService.LUCK.PkgListItemAdapter v17_33 = new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$AppScanning;
                    v17_33(this);
                    Thread v12_1 = new Thread(v17_33);
                    v12_1.setPriority(10);
                    switch (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getInt("viewsize", 0)) {
                        case 0:
                            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.textSize = 16973894;
                            break;
                        case 1:
                            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.textSize = 16973892;
                            break;
                        default:
                            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.textSize = 16973890;
                    }
                    v12_1.start();
                    android.widget.HorizontalScrollView v15_1 = ((android.widget.HorizontalScrollView) this.getActivity().findViewById(2131558614));
                    android.widget.Button v7_1 = ((android.widget.Button) v15_1.findViewById(2131558615));
                    if (v7_1 != null) {
                        v7_1.setTextAppearance(this.getContext(), com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getSizeText());
                        v7_1.setTextColor(android.graphics.Color.parseColor("#999999"));
                    }
                    android.widget.Button v7_3 = ((android.widget.Button) v15_1.findViewById(2131558616));
                    if (v7_3 != null) {
                        v7_3.setTextAppearance(this.getContext(), com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getSizeText());
                        v7_3.setTextColor(android.graphics.Color.parseColor("#999999"));
                    }
                    android.widget.Button v7_5 = ((android.widget.Button) v15_1.findViewById(2131558474));
                    if (v7_5 != null) {
                        v7_5.setTextAppearance(this.getContext(), com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getSizeText());
                        v7_5.setTextColor(android.graphics.Color.parseColor("#999999"));
                    }
                    android.widget.Button v7_7 = ((android.widget.Button) v15_1.findViewById(2131558617));
                    if (v7_7 != null) {
                        v7_7.setTextAppearance(this.getContext(), com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getSizeText());
                        v7_7.setTextColor(android.graphics.Color.parseColor("#999999"));
                    }
                    android.widget.Button v7_9 = ((android.widget.Button) v15_1.findViewById(2131558618));
                    if (v7_9 != null) {
                        v7_9.setTextAppearance(this.getContext(), com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getSizeText());
                        v7_9.setTextColor(android.graphics.Color.parseColor("#999999"));
                    }
                    android.widget.Button v7_11 = ((android.widget.Button) v15_1.findViewById(2131558619));
                    if (v7_11 != null) {
                        v7_11.setTextAppearance(this.getContext(), com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getSizeText());
                        v7_11.setTextColor(android.graphics.Color.parseColor("#999999"));
                    }
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getBoolean("lang_change", 0)) {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().edit().putBoolean("lang_change", 0).commit();
                        String v11 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getString("force_language", "default");
                        if (!v11.equals("default")) {
                            java.util.Locale v6_0 = 0;
                            String[] v16 = v11.split("_");
                            if (v16.length == 1) {
                                v6_0 = new java.util.Locale(v16[0]);
                            }
                            if (v16.length == 2) {
                                v6_0 = new java.util.Locale(v16[0], v16[1], "");
                            }
                            if (v16.length == 3) {
                                v6_0 = new java.util.Locale(v16[0], v16[1], v16[2]);
                            }
                            java.util.Locale.setDefault(v6_0);
                            android.content.res.Configuration v4_1 = new android.content.res.Configuration();
                            v4_1.locale = v6_0;
                            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().updateConfiguration(v4_1, com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDisplayMetrics());
                            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.menu1.clear();
                            this.getActivity().getMenuInflater().inflate(2131492864, com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.menu1);
                            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.sqlScanLite = 1;
                            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.sqlScan = 0;
                            com.android.vending.billing.InAppBillingService.LUCK.PkgListItemAdapter v17_88 = new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$AppScanning;
                            v17_88(this);
                            Thread v13_1 = new Thread(v17_88);
                            v12_1.setPriority(10);
                            v13_1.start();
                            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.notifyDataSetChanged();
                        } else {
                            java.util.Locale.setDefault(java.util.Locale.getDefault());
                            android.content.res.Configuration v3_1 = new android.content.res.Configuration();
                            v3_1.locale = android.content.res.Resources.getSystem().getConfiguration().locale;
                            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().updateConfiguration(v3_1, com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDisplayMetrics());
                            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.menu1.clear();
                            this.getActivity().getMenuInflater().inflate(2131492864, com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.menu1);
                            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.sqlScanLite = 1;
                            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.sqlScan = 0;
                            com.android.vending.billing.InAppBillingService.LUCK.PkgListItemAdapter v17_100 = new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$AppScanning;
                            v17_100(this);
                            Thread v13_3 = new Thread(v17_100);
                            v12_1.setPriority(10);
                            v13_3.start();
                            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.notifyDataSetChanged();
                        }
                    }
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getInt("orientstion", 3) == 1) {
                        this.getActivity().setRequestedOrientation(0);
                    }
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getInt("orientstion", 3) == 2) {
                        this.getActivity().setRequestedOrientation(1);
                    }
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getInt("orientstion", 3) == 3) {
                        this.getActivity().setRequestedOrientation(4);
                    }
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getInt("sortby", 2) == 1) {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$byPkgTime v18_52 = new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$byPkgName;
                        v18_52(this);
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.sorter = v18_52;
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.sort();
                    }
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getInt("sortby", 2) == 2) {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$byPkgTime v18_55 = new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$byPkgStatus;
                        v18_55(this);
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.sorter = v18_55;
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.sort();
                    }
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getInt("sortby", 2) == 3) {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$byPkgTime v18_58 = new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$byPkgTime;
                        v18_58(this);
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.sorter = v18_58;
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.sort();
                    }
                }
            } else {
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.fast_start = 1;
            }
        } catch (Exception v8_1) {
            v8_1.printStackTrace();
        }
        return;
    }

    public void pinfocopy()
    {
        java.io.File v4_1 = new java.io.File(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir).append("/").append("p.apk").toString());
        long v1 = com.chelpus.Utils.getRawLength(2131099659);
        if ((!v4_1.exists()) || (v4_1.length() != v1)) {
            if (v4_1.length() != v1) {
                System.out.println(new StringBuilder().append("LuckyPatcher: p.info version updated. ").append(v1).append(" ").append(v4_1.length()).toString());
                if (v4_1.exists()) {
                    v4_1.delete();
                }
            }
            try {
                com.chelpus.Utils.getRawToFile(2131099659, new java.io.File(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir).append("/p.apk").toString()));
                try {
                    com.chelpus.Utils.chmod(new java.io.File(v4_1.getAbsolutePath()), 3583);
                } catch (Exception v0) {
                    System.out.println(v0);
                    v0.printStackTrace();
                }
                com.chelpus.Utils.run_all(new StringBuilder().append("chmod 06777 ").append(v4_1.getAbsolutePath()).toString());
                com.chelpus.Utils.run_all(new StringBuilder().append("chown 0.0 ").append(v4_1.getAbsolutePath()).toString());
                com.chelpus.Utils.run_all(new StringBuilder().append("chown 0:0 ").append(v4_1.getAbsolutePath()).toString());
            } catch (String v5) {
            }
        }
        return;
    }

    public void rebootcopy()
    {
        int v1;
        java.io.File v2_1 = new java.io.File(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir).append("/").append("reboot").toString());
        if (!android.os.Build.CPU_ABI.toLowerCase().trim().equals("x86")) {
            if (!android.os.Build.CPU_ABI.toUpperCase().trim().equals("MIPS")) {
                v1 = 2131099660;
            } else {
                v1 = 2131099661;
            }
        } else {
            v1 = 2131099662;
        }
        if ((!v2_1.exists()) || (v2_1.length() != com.chelpus.Utils.getRawLength(v1))) {
            try {
                com.chelpus.Utils.getRawToFile(v1, new java.io.File(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir).append("/reboot").toString()));
            } catch (String v3) {
            }
            try {
                com.chelpus.Utils.chmod(new java.io.File(v2_1.getAbsolutePath()), 777);
            } catch (Exception v0) {
                System.out.println(v0);
                v0.printStackTrace();
            }
            com.chelpus.Utils.run_all(new StringBuilder().append("chmod 777 ").append(v2_1.getAbsolutePath()).toString());
            com.chelpus.Utils.run_all(new StringBuilder().append("chown 0.0 ").append(v2_1.getAbsolutePath()).toString());
            com.chelpus.Utils.run_all(new StringBuilder().append("chown 0:0 ").append(v2_1.getAbsolutePath()).toString());
        }
        return;
    }

    public void refresh_boot()
    {
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.boot_pat = new java.util.ArrayList();
        String v29 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getString("patch_dalvik_on_boot_patterns", "");
        if ((!v29.equals("")) && ((v29.contains("patch1")) || ((v29.contains("patch1")) || (v29.contains("patch1"))))) {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.boot_pat.add(new com.android.vending.billing.InAppBillingService.LUCK.PkgListItem(this.getContext(), com.chelpus.Utils.getText(2131165754), com.chelpus.Utils.getText(2131165754), 0, 0, 0, "", 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0));
        }
        try {
            java.io.File[] v31 = this.getContext().getDir("bootlist", 0).listFiles();
            int v34 = v31.length;
            int v33_1 = 0;
        } catch (android.content.pm.PackageManager$NameNotFoundException v28) {
            System.out.println("LuckyPatcher Error: Refresh BootList");
            return;
        }
        while (v33_1 < v34) {
            java.io.File v26 = v31[v33_1];
            if (!v26.getName().endsWith("bootlist")) {
                try {
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(v26.getName(), 0);
                } catch (android.content.pm.PackageManager$NameNotFoundException v28) {
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.boot_pat.add(new com.android.vending.billing.InAppBillingService.LUCK.PkgListItem(this.getContext(), v26.getName(), v26.getName(), 0, 0, 0, "\ufffd\ufffd\ufffd\ufffd\ufffd\ufffd\ufffd\ufffd\ufffd\ufffd \ufffd\ufffd\ufffd\ufffd\ufffd\ufffd\ufffd", 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0));
                }
            }
            v33_1++;
        }
        int v30 = 0;
        while (v30 < com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.data.length) {
            if ((!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.data[v30].boot_custom) || (!new java.io.File(new StringBuilder().append(this.getContext().getDir("bootlist", 0)).append("/").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.data[v30].pkgName).toString()).exists())) {
                if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.data[v30].boot_ads) || ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.data[v30].boot_lvl) || (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.data[v30].boot_manual))) {
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.boot_pat.add(new com.android.vending.billing.InAppBillingService.LUCK.PkgListItem(this.getContext(), com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.data[v30]));
                }
            } else {
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.boot_pat.add(new com.android.vending.billing.InAppBillingService.LUCK.PkgListItem(this.getContext(), com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.data[v30]));
            }
            v30++;
        }
        return;
    }

    public void remove_all_saved_purchases()
    {
        com.chelpus.Utils.showDialogYesNo(com.chelpus.Utils.getText(2131165748), com.chelpus.Utils.getText(2131165637), new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$7(this), new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$7(this), 0);
        return;
    }

    public void removefixes()
    {
        com.chelpus.Utils.showDialogYesNo(com.chelpus.Utils.getText(2131165748), com.chelpus.Utils.getText(2131165542), new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$8(this), new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$8(this), 0);
        return;
    }

    public void resetBatchOperation()
    {
        this.runToMain(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$132(this));
        return;
    }

    public void restore(com.android.vending.billing.InAppBillingService.LUCK.PkgListItem p4, java.io.File p5)
    {
        if (this.testSD(1)) {
            this.backup = p5;
            new java.io.File(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).append("/Backup/").toString()).mkdirs();
            this.runWithWait(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$93(this));
        }
        return;
    }

    public void restore_data()
    {
        if (this.testSD(1)) {
            java.io.File v0_1 = new java.io.File(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).append("/Backup/Data/").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName).toString());
            v0_1.mkdirs();
            this.runWithWait(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$95(this, v0_1));
        }
        return;
    }

    public void runId(int p32)
    {
        try {
            switch (p32) {
                case 2131165211:
                case 2131165387:
                case 2131165388:
                default:
                    if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.menu_open) {
                        return;
                    } else {
                        this.hideMenu();
                        return;
                    }
                case 2131165224:
                    if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getPackageManager().getComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), com.google.android.finsky.billing.iab.MarketBillingService)) != 2) && (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getPackageManager().getComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), com.google.android.finsky.billing.iab.InAppBillingService)) != 2)) {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), com.google.android.finsky.billing.iab.MarketBillingService), 2, 1);
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), com.google.android.finsky.billing.iab.InAppBillingService), 2, 1);
                        com.chelpus.Utils.market_billing_services(1);
                    } else {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), com.google.android.finsky.billing.iab.MarketBillingService), 1, 1);
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), com.google.android.finsky.billing.iab.InAppBillingService), 1, 1);
                    }
                    android.content.Intent v11_1 = new android.content.Intent(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), com.android.vending.billing.InAppBillingService.LUCK.widgets.inapp_widget);
                    v11_1.setPackage(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getPackageName());
                    v11_1.setAction(com.android.vending.billing.InAppBillingService.LUCK.widgets.inapp_widget.ACTION_WIDGET_RECEIVER_Updater);
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().sendBroadcast(v11_1);
                    break;
                case 2131165243:
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(3);
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.showDialogLP(3);
                    break;
                case 2131165252:
                    this.cleardalvik();
                    break;
                case 2131165254:
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(7);
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.rebuldApk.equals("")) {
                        String[] v26_158 = com.chelpus.Utils.getText(2131165378);
                        int v27_68 = new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$35;
                        v27_68(this);
                        com.chelpus.Utils.showDialogYesNo(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.name, v26_158, v27_68, 0, 0);
                    } else {
                        com.chelpus.Utils v25_396 = new java.io.File(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.rebuldApk).getName();
                        String[] v26_161 = com.chelpus.Utils.getText(2131165378);
                        int v27_69 = new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$34;
                        v27_69(this);
                        com.chelpus.Utils.showDialogYesNo(v25_396, v26_161, v27_69, 0, 0);
                    }
                    break;
                case 2131165256:
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(20);
                    this.contextselpatchlvl(1);
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(7);
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.showDialogLP(20);
                    break;
                case 2131165257:
                    this.backup(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli);
                    break;
                case 2131165258:
                    this.backup_data();
                    break;
                case 2131165260:
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(7);
                    this.toolbar_addfree_on();
                    break;
                case 2131165261:
                    this.contextpermmenu();
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.showDialogLP(7);
                    break;
                case 2131165262:
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(7);
                    this.toolbar_addfree_clear();
                    break;
                case 2131165263:
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(21);
                    this.getpermissions();
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(7);
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.showDialogLP(21);
                    break;
                case 2131165265:
                    try {
                        this.dexopt_app(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName, 0).applicationInfo.sourceDir, com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName, new StringBuilder().append("").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName, 0).applicationInfo.uid).toString());
                    } catch (Exception v8_8) {
                        v8_8.printStackTrace();
                        this.showMessage(com.chelpus.Utils.getText(2131165748), com.chelpus.Utils.getText(2131165436));
                    }
                    break;
                case 2131165266:
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(29);
                    this.getLPActivity();
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(7);
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.showDialogLP(29);
                    break;
                case 2131165267:
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(31);
                    this.getComponents();
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(7);
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.showDialogLP(31);
                    break;
                case 2131165268:
                    if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su) {
                    } else {
                        System.out.println("Disable Google services ads");
                        try {
                            android.content.pm.PackageInfo v15_3 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo("com.google.android.gms", 4);
                        } catch (android.content.pm.PackageManager$NameNotFoundException v9_2) {
                            v9_2.printStackTrace();
                        }
                        if ((v15_3 == null) || ((v15_3.services == null) || (v15_3.services.length == 0))) {
                        } else {
                            int v7_3 = 0;
                            while (v7_3 < v15_3.services.length) {
                                if ((v15_3.services[v7_3].name.startsWith("com.google.android.gms.ads.identifier.")) && ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getComponentEnabledSetting(new android.content.ComponentName("com.google.android.gms", v15_3.services[v7_3].name)) != 2) && ((!v15_3.services[v7_3].name.startsWith("com.google.android.gms.ads.social")) && (!v15_3.services[v7_3].name.startsWith("com.google.android.gms.ads.jams."))))) {
                                    System.out.println(new StringBuilder().append("Disable Google services ads:").append(v15_3.services[v7_3].name).toString());
                                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.showDialogLP(11);
                                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.progress2.setCancelable(1);
                                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.progress2.setMessage(com.chelpus.Utils.getText(2131165747));
                                    com.chelpus.Utils v25_365 = new com.chelpus.Utils("");
                                    String v0_181 = new String[2];
                                    String[] v26_149 = v0_181;
                                    v26_149[0] = new StringBuilder().append("pm disable \'com.google.android.gms/").append(v15_3.services[v7_3].name).append("\'").toString();
                                    v26_149[1] = "skipOut";
                                    v25_365.cmdRoot(v26_149);
                                }
                                v7_3++;
                            }
                        }
                    }
                    break;
                case 2131165269:
                    try {
                        this.disable_package(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName, 0);
                    } catch (Exception v8_6) {
                        v8_6.printStackTrace();
                        this.showMessage(com.chelpus.Utils.getText(2131165748), com.chelpus.Utils.getText(2131165436));
                    }
                    break;
                case 2131165270:
                    if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su) {
                    } else {
                        System.out.println("Enable Google services ads");
                        try {
                            android.content.pm.PackageInfo v15_2 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo("com.google.android.gms", 4);
                        } catch (android.content.pm.PackageManager$NameNotFoundException v9_1) {
                            v9_1.printStackTrace();
                        }
                        if ((v15_2 == null) || ((v15_2.services == null) || (v15_2.services.length == 0))) {
                        } else {
                            int v7_2 = 0;
                            while (v7_2 < v15_2.services.length) {
                                if ((v15_2.services[v7_2].name.startsWith("com.google.android.gms.ads")) && (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getComponentEnabledSetting(new android.content.ComponentName("com.google.android.gms", v15_2.services[v7_2].name)) == 2)) {
                                    System.out.println(new StringBuilder().append("Enable Google services ads:").append(v15_2.services[v7_2].name).toString());
                                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.showDialogLP(11);
                                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.progress2.setCancelable(1);
                                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.progress2.setMessage(com.chelpus.Utils.getText(2131165747));
                                    com.chelpus.Utils v25_333 = new com.chelpus.Utils("");
                                    String v0_148 = new String[2];
                                    String[] v26_127 = v0_148;
                                    v26_127[0] = new StringBuilder().append("pm enable \'com.google.android.gms/").append(v15_2.services[v7_2].name).append("\'").toString();
                                    v26_127[1] = "skipOut";
                                    v25_333.cmdRoot(v26_127);
                                }
                                v7_2++;
                            }
                        }
                    }
                    break;
                case 2131165271:
                    try {
                        this.disable_package(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName, 1);
                    } catch (Exception v8_4) {
                        v8_4.printStackTrace();
                        this.showMessage(com.chelpus.Utils.getText(2131165748), com.chelpus.Utils.getText(2131165436));
                    }
                    break;
                case 2131165272:
                    com.chelpus.Utils v25_309 = new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$33;
                    v25_309(this);
                    this.runWithWait(v25_309);
                    break;
                case 2131165275:
                    com.android.vending.billing.InAppBillingService.LUCK.FileApkListItem v5_1 = new com.android.vending.billing.InAppBillingService.LUCK.FileApkListItem(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), new java.io.File(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.rebuldApk), 0);
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(7);
                    this.toolbar_restore(v5_1, 0);
                    break;
                case 2131165276:
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(7);
                    this.toolbar_install_system(new com.android.vending.billing.InAppBillingService.LUCK.FileApkListItem(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), new java.io.File(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.rebuldApk), 0));
                    break;
                case 2131165277:
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adapterSelectType = 2131165760;
                    this.selected_apps();
                    break;
                case 2131165278:
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adapterSelectType = 2131165761;
                    this.selected_apps();
                    break;
                case 2131165279:
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adapterSelectType = 2131165247;
                    this.selected_apps();
                    break;
                case 2131165280:
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.rebuldApk = this.current.full;
                    this.contexttoolbarcreateapk();
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(7);
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.showDialogLP(7);
                    break;
                case 2131165281:
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(17);
                    this.contextselpatchads(1);
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(7);
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.showDialogLP(17);
                    break;
                case 2131165282:
                    this.context_remove_saved_purchases();
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(7);
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.showDialogLP(7);
                    break;
                case 2131165283:
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(7);
                    this.backupselector();
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(26);
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.showDialogLP(26);
                    break;
                case 2131165284:
                    this.restore_data();
                    break;
                case 2131165285:
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(22);
                    this.getpermissions();
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(7);
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.showDialogLP(22);
                    break;
                case 2131165286:
                    if ((!com.chelpus.Utils.checkCoreJarPatch20()) && ((!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.system) || (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName, 0).applicationInfo.sourceDir.startsWith("/system/app")))) {
                        this.showMessage(com.chelpus.Utils.getText(2131165748), com.chelpus.Utils.getText(2131165649));
                    } else {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(25);
                        this.getpermissions();
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(7);
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.showDialogLP(25);
                    }
                    break;
                case 2131165287:
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(34);
                    this.contextsupport(0);
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(7);
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.showDialogLP(34);
                    break;
                case 2131165289:
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(7);
                    this.toolbar_addfree_off();
                    break;
                case 2131165290:
                    com.android.vending.billing.InAppBillingService.LUCK.FileApkListItem v6_1 = new com.android.vending.billing.InAppBillingService.LUCK.FileApkListItem(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), new java.io.File(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.rebuldApk), 0);
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(7);
                    this.toolbar_uninstall(v6_1);
                    break;
                case 2131165291:
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adapterSelectType = 2131165763;
                    this.selected_apps();
                    break;
                case 2131165292:
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(17);
                    this.contextads();
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(7);
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.showDialogLP(7);
                    break;
                case 2131165293:
                    this.context_backup_menu();
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(7);
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.showDialogLP(7);
                    break;
                case 2131165296:
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(7);
                    this.custompatchselector();
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adapt.getCount() > 1) {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.func = 2;
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(16);
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.showDialogLP(16);
                    } else {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(15);
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.customselect = ((java.io.File) com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adapt.getItem(0));
                        this.bootadd(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli, "custom");
                    }
                    break;
                case 2131165297:
                    this.bootadd(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli, "ads");
                    break;
                case 2131165298:
                    this.bootadd(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli, "custom");
                    break;
                case 2131165299:
                    this.bootadd(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli, "lvl");
                    break;
                case 2131165315:
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(7);
                    this.custompatchselector();
                    if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adapt.getCount() <= 0) || (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adapt.getCount() > 1)) {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.func = 1;
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.showDialogLP(16);
                    } else {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.func = 1;
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(15);
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.customselect = ((java.io.File) com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adapt.getItem(0));
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.showDialogLP(15);
                    }
                    break;
                case 2131165316:
                    this.deodex(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli);
                    break;
                case 2131165317:
                    this.runextendetpatch(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli);
                    break;
                case 2131165318:
                    this.contextfix();
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(7);
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.showDialogLP(7);
                    break;
                case 2131165321:
                    this.contextlvl();
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(7);
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.showDialogLP(7);
                    break;
                case 2131165323:
                    android.content.Intent v16 = new android.content.Intent;
                    v16(this.getContext(), com.android.vending.billing.InAppBillingService.LUCK.LivepatchActivity);
                    this.startActivity(v16);
                    break;
                case 2131165324:
                    this.odex(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli);
                    break;
                case 2131165366:
                    this.context_restore_menu();
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(7);
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.showDialogLP(7);
                    break;
                case 2131165367:
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(13);
                    this.contextselpatch();
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(7);
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.showDialogLP(13);
                    break;
                case 2131165375:
                    this.contextCorePatch();
                    break;
                case 2131165383:
                    this.contextcreateapk();
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(7);
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.showDialogLP(7);
                    break;
                case 2131165384:
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(18);
                    this.contextselpatchads(0);
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(7);
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.showDialogLP(18);
                    break;
                case 2131165385:
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(7);
                    this.custompatchselector();
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adapt.getCount() > 1) {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.func = 0;
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(15);
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(16);
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.showDialogLP(16);
                    } else {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(15);
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.customselect = ((java.io.File) com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adapt.getItem(0));
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.showDialogLP(15);
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.func = 0;
                    }
                    break;
                case 2131165386:
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(19);
                    this.contextselpatchlvl(0);
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(7);
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.showDialogLP(19);
                case 2131165389:
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(35);
                    this.contextsupport(1);
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(7);
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.showDialogLP(35);
                    break;
                case 2131165408:
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(7);
                    this.delete_file();
                    break;
                case 2131165413:
                    android.content.Intent v17 = new android.content.Intent;
                    v17(this.getContext(), com.android.vending.billing.InAppBillingService.LUCK.BinderActivity);
                    this.startActivity(v17);
                    break;
                case 2131165417:
                    if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su) {
                    } else {
                        android.content.pm.PackageInfo v15_1 = com.chelpus.Utils.getPkgInfo("com.android.vending", 4);
                        int v24 = 0;
                        if ((v15_1 == null) || ((v15_1.services == null) || (v15_1.services.length == 0))) {
                        } else {
                            int v7_1 = 0;
                            while (v7_1 < v15_1.services.length) {
                                try {
                                    if (((!v15_1.services[v7_1].name.endsWith("InAppBillingService")) && (!v15_1.services[v7_1].name.endsWith("MarketBillingService"))) || (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getComponentEnabledSetting(new android.content.ComponentName("com.android.vending", v15_1.services[v7_1].name)) == 1)) {
                                        if (((v15_1.services[v7_1].name.endsWith("InAppBillingService")) || (v15_1.services[v7_1].name.endsWith("MarketBillingService"))) && (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getComponentEnabledSetting(new android.content.ComponentName("com.android.vending", v15_1.services[v7_1].name)) == 1)) {
                                            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.showDialogLP(11);
                                            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.progress2.setCancelable(1);
                                            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.progress2.setMessage(com.chelpus.Utils.getText(2131165747));
                                            com.chelpus.Utils.market_billing_services(0);
                                            if (v24 == 0) {
                                                this.showMessage(com.chelpus.Utils.getText(2131165748), com.chelpus.Utils.getText(2131165543));
                                            }
                                            v24 = 1;
                                        }
                                    } else {
                                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.showDialogLP(11);
                                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.progress2.setCancelable(1);
                                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.progress2.setMessage(com.chelpus.Utils.getText(2131165747));
                                        com.chelpus.Utils.market_billing_services(1);
                                    }
                                    v7_1++;
                                } catch (Exception v8_2) {
                                    v8_2.printStackTrace();
                                }
                            }
                        }
                    }
                    break;
                case 2131165419:
                    if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su) {
                    } else {
                        android.content.pm.PackageInfo v15_0 = com.chelpus.Utils.getPkgInfo("com.android.vending", 4);
                        if ((v15_0 == null) || ((v15_0.services == null) || (v15_0.services.length == 0))) {
                        } else {
                            int v7_0 = 0;
                            while (v7_0 < v15_0.services.length) {
                                try {
                                    if ((!v15_0.services[v7_0].name.endsWith("LicensingService")) || (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getComponentEnabledSetting(new android.content.ComponentName("com.android.vending", v15_0.services[v7_0].name)) == 1)) {
                                        if ((v15_0.services[v7_0].name.endsWith("LicensingService")) && (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getComponentEnabledSetting(new android.content.ComponentName("com.android.vending", v15_0.services[v7_0].name)) == 1)) {
                                            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.showDialogLP(11);
                                            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.progress2.setCancelable(1);
                                            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.progress2.setMessage(com.chelpus.Utils.getText(2131165747));
                                            com.chelpus.Utils.market_licensing_services(0);
                                            this.showMessage(com.chelpus.Utils.getText(2131165748), com.chelpus.Utils.getText(2131165545));
                                        }
                                    } else {
                                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.showDialogLP(11);
                                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.progress2.setCancelable(1);
                                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.progress2.setMessage(com.chelpus.Utils.getText(2131165747));
                                        com.chelpus.Utils.market_licensing_services(1);
                                    }
                                    v7_0++;
                                } catch (Exception v8_1) {
                                    v8_1.printStackTrace();
                                }
                            }
                        }
                    }
                    break;
                case 2131165501:
                    android.content.Intent v19 = new android.content.Intent;
                    v19("android.intent.action.VIEW", android.net.Uri.parse("https://play.google.com/store/apps/details?id=eu.chainfire.supersu"));
                    v19.addFlags(1073741824);
                    this.startActivity(v19);
                    break;
                case 2131165504:
                    java.util.ArrayList v20 = new java.util.ArrayList;
                    v20(1);
                    v20.add(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli);
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.integrate_to_system(v20, 1, 0);
                    break;
                case 2131165511:
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getPackageManager().getComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), com.google.android.finsky.services.LicensingService)) != 2) {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), com.google.android.finsky.services.LicensingService), 2, 1);
                        com.chelpus.Utils.market_licensing_services(1);
                    } else {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), com.google.android.finsky.services.LicensingService), 1, 1);
                        this.showMessage(com.chelpus.Utils.getText(2131165748), com.chelpus.Utils.getText(2131165544));
                    }
                    android.content.Intent v12_1 = new android.content.Intent(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), com.android.vending.billing.InAppBillingService.LUCK.widgets.lvl_widget);
                    v12_1.setPackage(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getPackageName());
                    v12_1.setAction(com.android.vending.billing.InAppBillingService.LUCK.widgets.lvl_widget.ACTION_WIDGET_RECEIVER_Updater);
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().sendBroadcast(v12_1);
                    break;
                case 2131165520:
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(30);
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.showDialogLP(30);
                    break;
                case 2131165546:
                    this.mod_market_check();
                    break;
                case 2131165558:
                    this.move_to_system_click();
                    break;
                case 2131165561:
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(7);
                    this.new_method_lvl();
                    break;
                case 2131165584:
                    this.odex_all_system_app();
                    break;
                case 2131165624:
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(10);
                    this.getpackagesxml();
                    this.contextpermissions();
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(7);
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.showDialogLP(10);
                    break;
                case 2131165630:
                    this.dialog_reboot();
                    break;
                case 2131165636:
                    this.remove_all_saved_purchases();
                    break;
                case 2131165638:
                    this.removefixes();
                    break;
                case 2131165658:
                    this.setDefInstall();
                    break;
                case 2131165659:
                    this.setDefInstallAuto();
                    break;
                case 2131165660:
                    this.setDefInstallInternalMemory();
                    break;
                case 2131165661:
                    this.setDefInstallSDcard();
                    break;
                case 2131165662:
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().edit().putBoolean("switch_auto_backup_apk", 0).commit();
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().edit().putBoolean("switch_auto_backup_apk_only_gp", 0).commit();
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().edit().putBoolean("switch_auto_integrate_update", 0).commit();
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().edit().putBoolean("switch_auto_move_to_sd", 0).commit();
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().edit().putBoolean("switch_auto_move_to_internal", 0).commit();
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), com.google.android.finsky.services.LicensingService), 2, 1);
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), com.google.android.finsky.billing.iab.MarketBillingService), 2, 1);
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), com.google.android.finsky.billing.iab.InAppBillingService), 2, 1);
                    com.chelpus.Utils.market_billing_services(1);
                    com.chelpus.Utils.market_licensing_services(1);
                    android.content.Intent v13_1 = new android.content.Intent(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), com.android.vending.billing.InAppBillingService.LUCK.widgets.inapp_widget);
                    v13_1.setPackage(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getPackageName());
                    v13_1.setAction(com.android.vending.billing.InAppBillingService.LUCK.widgets.inapp_widget.ACTION_WIDGET_RECEIVER_Updater);
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().sendBroadcast(v13_1);
                    android.content.Intent v14_1 = new android.content.Intent(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), com.android.vending.billing.InAppBillingService.LUCK.widgets.lvl_widget);
                    v14_1.setPackage(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getPackageName());
                    v14_1.setAction(com.android.vending.billing.InAppBillingService.LUCK.widgets.lvl_widget.ACTION_WIDGET_RECEIVER_Updater);
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().sendBroadcast(v14_1);
                    com.chelpus.Utils v25_98 = new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$32;
                    v25_98(this);
                    this.runWithWait(v25_98);
                    break;
                case 2131165672:
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(7);
                    this.share_app();
                    break;
                case 2131165705:
                    if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getBoolean("switch_auto_backup_apk", 0)) {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().edit().putBoolean("switch_auto_backup_apk", 1).commit();
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().edit().putBoolean("switch_auto_backup_apk_only_gp", 0).commit();
                    } else {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().edit().putBoolean("switch_auto_backup_apk", 0).commit();
                    }
                    break;
                case 2131165707:
                    if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getBoolean("switch_auto_backup_apk_only_gp", 0)) {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().edit().putBoolean("switch_auto_backup_apk_only_gp", 1).commit();
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().edit().putBoolean("switch_auto_backup_apk", 0).commit();
                    } else {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().edit().putBoolean("switch_auto_backup_apk_only_gp", 0).commit();
                    }
                    break;
                case 2131165709:
                    if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getBoolean("switch_auto_integrate_update", 0)) {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().edit().putBoolean("switch_auto_integrate_update", 1).commit();
                    } else {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().edit().putBoolean("switch_auto_integrate_update", 0).commit();
                    }
                    break;
                case 2131165711:
                    if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getBoolean("switch_auto_move_to_internal", 0)) {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().edit().putBoolean("switch_auto_move_to_internal", 1).commit();
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().edit().putBoolean("switch_auto_move_to_sd", 0).commit();
                    } else {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().edit().putBoolean("switch_auto_move_to_internal", 0).commit();
                    }
                    break;
                case 2131165713:
                    if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getBoolean("switch_auto_move_to_sd", 0)) {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().edit().putBoolean("switch_auto_move_to_sd", 1).commit();
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().edit().putBoolean("switch_auto_move_to_internal", 0).commit();
                    } else {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().edit().putBoolean("switch_auto_move_to_sd", 0).commit();
                    }
                    break;
                case 2131165735:
                    android.content.Intent v18 = new android.content.Intent;
                    v18("android.intent.action.VIEW", android.net.Uri.parse("https://play.google.com/store/apps/details?id=stericson.busybox&feature"));
                    v18.addFlags(1073741824);
                    this.startActivity(v18);
                    break;
                case 2131165753:
                    this.contextXposedPatch();
                    break;
                case 2131165764:
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adapterSelectType = 2131165759;
                    this.selected_apps();
                    break;
                case 2131165765:
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.rebuldApk = this.current.full;
                    this.contexttoolbarcreateframework();
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(7);
                    break;
                case 2131165766:
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adapterSelectType = 2131165762;
                    this.selected_apps();
                    break;
                case 2131165785:
                    this.set_internet(0);
                    break;
                case 2131165786:
                    this.set_internet(1);
                    break;
                case 2131165793:
                    int v21 = 0;
                    if (!com.chelpus.Utils.isXposedEnabled()) {
                        v21 = 1;
                    } else {
                        new java.util.ArrayList();
                        org.json.JSONObject v23 = com.chelpus.Utils.readXposedParamBoolean();
                        boolean v4 = 0;
                        if (v23 != null) {
                            v4 = v23.optBoolean("patch4", 0);
                        }
                        if (!v4) {
                            v21 = 1;
                        }
                    }
                    if (v21 == 0) {
                    } else {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.showDialogLP(11);
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.progress2.setCancelable(1);
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.progress2.setMessage(com.chelpus.Utils.getText(2131165747));
                        com.chelpus.Utils v25_5 = new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$36;
                        v25_5(this);
                        this.runWithWait(v25_5);
                    }
                    break;
            }
        } catch (Exception v8_9) {
        }
        v8_9.printStackTrace();
        return;
    }

    public void runToMain(Runnable p2)
    {
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag == null) {
            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.handler != null) {
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.handler.post(p2);
            }
        } else {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getContext().runOnUiThread(p2);
        }
        return;
    }

    public void runUpdate()
    {
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$UpdateVersion v0_1 = new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$UpdateVersion(this);
        String[] v1_1 = new String[1];
        v1_1[0] = "novajaversia";
        v0_1.execute(v1_1);
        return;
    }

    public void runUpdateTask()
    {
        Thread v0_1 = new Thread(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$CheckUpdate(this));
        v0_1.setPriority(1);
        v0_1.start();
        return;
    }

    public void runWithWait(Runnable p3)
    {
        Thread v0_1 = new Thread(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$125(this, p3));
        v0_1.setPriority(10);
        v0_1.start();
        return;
    }

    public void runextendetpatch(com.android.vending.billing.InAppBillingService.LUCK.PkgListItem p8)
    {
        try {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plipack = p8.pkgName;
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.supath = this.getContext().getPackageCodePath();
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str = "";
            android.widget.Toast v1_5 = new com.chelpus.Utils("");
            String v2_2 = new String[1];
            v2_2[0] = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.dalvikruncommand).append(".nerorunpatch ").append(p8.pkgName).append(" search").toString();
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str = v1_5.cmdRoot(v2_2);
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(5);
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.showDialogLP(5);
        } catch (Exception v0) {
            android.widget.Toast.makeText(this.getContext(), new StringBuilder().append("").append(v0).toString(), 1).show();
        }
        return;
    }

    public void selectLanguage()
    {
        java.util.ArrayList v2_1 = new java.util.ArrayList();
        v2_1.add("en_US");
        v2_1.add("ar");
        v2_1.add("be");
        v2_1.add("bg");
        v2_1.add("cs");
        v2_1.add("da");
        v2_1.add("de");
        v2_1.add("el");
        v2_1.add("es");
        v2_1.add("fa");
        v2_1.add("fi");
        v2_1.add("fr");
        v2_1.add("gu");
        v2_1.add("he");
        v2_1.add("hi");
        v2_1.add("hu");
        v2_1.add("id");
        v2_1.add("in");
        v2_1.add("it");
        v2_1.add("iw");
        v2_1.add("ja");
        v2_1.add("km");
        v2_1.add("km_KH");
        v2_1.add("ko");
        v2_1.add("lt");
        v2_1.add("ml");
        v2_1.add("my");
        v2_1.add("nl");
        v2_1.add("pl");
        v2_1.add("pt");
        v2_1.add("pt_rBR");
        v2_1.add("ro");
        v2_1.add("ru");
        v2_1.add("sk");
        v2_1.add("tr");
        v2_1.add("ug");
        v2_1.add("uk");
        v2_1.add("vi");
        v2_1.add("zh_CN");
        v2_1.add("zh_HK");
        v2_1.add("zh_TW");
        com.android.vending.billing.InAppBillingService.LUCK.AlertDlg v1_1 = new com.android.vending.billing.InAppBillingService.LUCK.AlertDlg(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchAct);
        v1_1.setAdapter(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$48(this, com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchAct, 2130968605, v2_1), new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$49(this));
        v1_1.create().show();
        return;
    }

    public void selected_apps()
    {
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adapterSelect = 1;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.selectedApps.clear();
        ((android.widget.TextView) com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchAct.findViewById(2131558612)).setVisibility(0);
        ((android.widget.TextView) com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchAct.findViewById(2131558610)).setVisibility(0);
        android.widget.TextView v3_1 = ((android.widget.TextView) com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchAct.findViewById(2131558609));
        v3_1.setVisibility(0);
        android.widget.Button v0_1 = ((android.widget.Button) com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchAct.findViewById(2131558611));
        v0_1.setText(com.chelpus.Utils.getText(2131165248));
        System.out.println(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adapterSelectType);
        switch (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adapterSelectType) {
            case 2131165247:
                System.out.println(new StringBuilder().append("found").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adapterSelectType).toString());
                v3_1.setText(com.chelpus.Utils.getText(2131165721));
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.advancedFilter = 1929;
                v0_1.setOnClickListener(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$126(this));
                break;
            case 2131165759:
                v3_1.setText(com.chelpus.Utils.getText(2131165783));
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.advancedFilter = 1931;
                v0_1.setOnClickListener(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$129(this));
                break;
            case 2131165760:
                v3_1.setText(com.chelpus.Utils.getText(2131165719));
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.advancedFilter = 2131558427;
                v0_1.setOnClickListener(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$131(this));
                break;
            case 2131165761:
                System.out.println(new StringBuilder().append("found").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adapterSelectType).toString());
                v3_1.setText(com.chelpus.Utils.getText(2131165720));
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.advancedFilter = 2131558424;
                v0_1.setOnClickListener(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$127(this));
                break;
            case 2131165762:
                v3_1.setText(com.chelpus.Utils.getText(2131165784));
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.advancedFilter = 1932;
                v0_1.setOnClickListener(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$130(this));
                break;
            case 2131165763:
                v3_1.setText(com.chelpus.Utils.getText(2131165722));
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.advancedFilter = 1930;
                v0_1.setOnClickListener(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$128(this));
                break;
        }
        v0_1.setVisibility(0);
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.app_scanning();
        return;
    }

    public void sendLog()
    {
        Void[] v1_1 = new Void[0];
        new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$45(this).execute(v1_1);
        return;
    }

    public void setDefInstall()
    {
        java.util.ArrayList v0_1 = new java.util.ArrayList();
        v0_1.add(Integer.valueOf(2131165659));
        v0_1.add(Integer.valueOf(2131165660));
        v0_1.add(Integer.valueOf(2131165661));
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adapt = new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$11(this, this.getContext(), 2130968605, v0_1);
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.removeDialogLP(7);
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.showDialogLP(7);
        return;
    }

    public void setDefInstallAuto()
    {
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().edit().putInt("Install_location", 0).commit();
        if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su) {
            String[] v0_4 = new String[2];
            v0_4[0] = "pm setInstallLocation 0";
            v0_4[1] = "skipOut";
            com.chelpus.Utils.cmd(v0_4);
            String[] v0_5 = new String[2];
            v0_5[0] = "pm set-install-location 0";
            v0_5[1] = "skipOut";
            com.chelpus.Utils.cmd(v0_5);
        } else {
            String[] v0_7 = new com.chelpus.Utils("");
            String v1_6 = new String[2];
            v1_6[0] = "pm setInstallLocation 0";
            v1_6[1] = "skipOut";
            v0_7.cmdRoot(v1_6);
            String[] v0_9 = new com.chelpus.Utils("");
            String v1_8 = new String[2];
            v1_8[0] = "pm set-install-location 0";
            v1_8[1] = "skipOut";
            v0_9.cmdRoot(v1_8);
        }
        return;
    }

    public void setDefInstallInternalMemory()
    {
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().edit().putInt("Install_location", 1).commit();
        if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su) {
            String[] v0_4 = new String[2];
            v0_4[0] = "pm setInstallLocation 1";
            v0_4[1] = "skipOut";
            com.chelpus.Utils.cmd(v0_4);
            String[] v0_5 = new String[2];
            v0_5[0] = "pm set-install-location 1";
            v0_5[1] = "skipOut";
            com.chelpus.Utils.cmd(v0_5);
        } else {
            String[] v0_7 = new com.chelpus.Utils("");
            String v1_6 = new String[2];
            v1_6[0] = "pm setInstallLocation 1";
            v1_6[1] = "skipOut";
            v0_7.cmdRoot(v1_6);
            String[] v0_9 = new com.chelpus.Utils("");
            String v1_8 = new String[2];
            v1_8[0] = "pm set-install-location 1";
            v1_8[1] = "skipOut";
            v0_9.cmdRoot(v1_8);
        }
        return;
    }

    public void setDefInstallSDcard()
    {
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().edit().putInt("Install_location", 2).commit();
        if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su) {
            String[] v0_4 = new String[2];
            v0_4[0] = "pm setInstallLocation 2";
            v0_4[1] = "skipOut";
            com.chelpus.Utils.cmd(v0_4);
            String[] v0_5 = new String[2];
            v0_5[0] = "pm set-install-location 2";
            v0_5[1] = "skipOut";
            com.chelpus.Utils.cmd(v0_5);
        } else {
            String[] v0_7 = new com.chelpus.Utils("");
            String v1_6 = new String[2];
            v1_6[0] = "pm setInstallLocation 2";
            v1_6[1] = "skipOut";
            v0_7.cmdRoot(v1_6);
            String[] v0_9 = new com.chelpus.Utils("");
            String v1_8 = new String[2];
            v1_8[0] = "pm set-install-location 2";
            v1_8[1] = "skipOut";
            v0_9.cmdRoot(v1_8);
        }
        return;
    }

    public void setRootWidgetEnabled(boolean p12)
    {
        android.content.pm.PackageManager v6 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getPackageManager();
        if (v6 != null) {
            android.content.ComponentName v3_1 = new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), com.android.vending.billing.InAppBillingService.LUCK.widgets.AppDisablerWidget);
            int v0 = v6.getComponentEnabledSetting(v3_1);
            android.content.ComponentName v4_1 = new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), com.android.vending.billing.InAppBillingService.LUCK.widgets.BinderWidget);
            int v1 = v6.getComponentEnabledSetting(v4_1);
            android.content.ComponentName v5_1 = new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), com.android.vending.billing.InAppBillingService.LUCK.widgets.AndroidPatchWidget);
            v6.getComponentEnabledSetting(v5_1);
            if (!p12) {
                if ((v0 == 1) || (v1 == 1)) {
                    v6.setComponentEnabledSetting(v3_1, 2, 1);
                    v6.setComponentEnabledSetting(v4_1, 2, 1);
                    v6.setComponentEnabledSetting(v5_1, 2, 1);
                }
            } else {
                if ((v0 == 2) || (v1 == 2)) {
                    v6.setComponentEnabledSetting(v3_1, 1, 1);
                    v6.setComponentEnabledSetting(v4_1, 1, 1);
                    v6.setComponentEnabledSetting(v5_1, 1, 1);
                }
            }
        }
        return;
    }

    public void set_internet(boolean p3)
    {
        new Thread(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$31(this, p3)).start();
        return;
    }

    public void share_app()
    {
        if (this.testSD(1)) {
            com.chelpus.Utils.showDialogYesNo(com.chelpus.Utils.getText(2131165748), com.chelpus.Utils.getText(2131165671), new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$97(this), new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$97(this), 0);
        }
        return;
    }

    public void showAbout()
    {
        android.widget.LinearLayout v1_1 = ((android.widget.LinearLayout) android.view.View.inflate(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchAct, 2130968576, 0));
        android.widget.TextView v3_1 = ((android.widget.TextView) ((android.widget.LinearLayout) v1_1.findViewById(2131558401).findViewById(2131558403)).findViewById(2131558404));
        v3_1.append(com.chelpus.Utils.getColoredText(new StringBuilder().append(com.chelpus.Utils.getText(2131165737)).append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.version).append("\n").toString(), "#ffffffff", "bold"));
        v3_1.append(com.chelpus.Utils.getColoredText("----------------------------------\n\n", "#ffffffff", "bold"));
        v3_1.append(com.chelpus.Utils.getColoredText(new StringBuilder().append(com.chelpus.Utils.getText(2131165188)).append(" ").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).append("\n\n").toString(), "#ffffffff", "bold"));
        v3_1.append(com.chelpus.Utils.getColoredText(new StringBuilder().append(com.chelpus.Utils.getText(2131165629)).append("\n\n").toString(), "#ffff0000", "bold"));
        v3_1.append(com.chelpus.Utils.getColoredText(new StringBuilder().append(com.chelpus.Utils.getText(2131165189)).append("\n").toString(), "#ffffff00", "bold"));
        v3_1.append(com.chelpus.Utils.getColoredText("\nThanks to:\nUsed source code: pyler, Mikanoshi@4pda.ru\nSite WebIconSet.com - Smile icons\np0izn@xda - English translation\nJang Seung Hun - Korean translation\nDerDownloader7,Simon Plantone - German translation\nAndrew Q., Simoconfa - Italian translation\ntrBilimBEKo,Z.Zeynalov,Noss_bar - Turkish translation\nSpoetnic - Dutch translation\n\u67d2\u6708\u96ea@BNB520,\u5356\u840c\u5b50,Hiapk-\u5929\u4f7f - Simplified Chinese translation\neauland, foXaCe - French translation\nrhrarhra@Mobilism.org, alireza_simkesh@XDA - Persian translation\nAlftronics & TheCh - Portuguese translation\nCaio Fons\u00eaca - Portuguese (Brasil) translation\n\u6f22\u9ed1\u7389,Hiapk-\u5929\u4f7f - Traditional Chinese translation\ns_h - Hebrew translation\nXcooM - Polish translation\nGizmo[SK],pyler - Slovak translation\nHarmeet Singh - Hindi translation\nPROXIMO - Bulgarian translation\nCulum - Indonesian translation\nslycog@XDA - Custom Patch Help translate\nCheng Sokdara (homi3kh) - Khmer translation\nrkononenko, masterlist@forpda, Volodiimr - Ukrainian translation\nJeduRocks@XDA - Spanish translation\nPaul Diosteanu - Romanian translation\ngidano - Hungarian translation\nOrbit09, Renek - Czech translation\ni_Droidi@Twitter - Arabic translation\nbobo1704 - Greek translation\nJack_Rover, 6sto - Finnish translation\nWan Mohammad - Malay translation\nNguy\u1ec5n \u0110\u1ee9c L\u01b0\u1ee3ng - Vietnamese translation\nSOORAJ SR(fb.com/rsoorajs) - Malayalam translation\nXiveZ - Belarusian translation\nEkberjan - Uighur translation\nevildog1 - Danish translation\nHzy980512 - Japanese translation\nChelpus, maksnogin - Russian translation\nIcons to menu - Sergey Kamashki\nTemplate for ADS - And123\n\nSupport by email: lp.chelpus@gmail.com\nGood Luck!\nChelpuS", "#ffffffff", "bold"));
        new com.android.vending.billing.InAppBillingService.LUCK.AlertDlg(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchAct).setTitle(2131165191).setCancelable(1).setIcon(17301659).setPositiveButton(com.chelpus.Utils.getText(17039370), 0).setNeutralButton("Changelog", new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$44(this)).setView(v1_1).setOnCancelListener(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$43(this)).create().show();
        return;
    }

    public void showMenu()
    {
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.menu_open = 1;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.menu_adapter.notifyDataSetChanged();
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.menu_lv.setVisibility(0);
        ((android.widget.LinearLayout) com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchAct.findViewById(2131558607)).setVisibility(8);
        return;
    }

    public void showMessage(String p2, String p3)
    {
        this.runToMain(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$4(this, p2, p3));
        return;
    }

    public void show_app_manager_click()
    {
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getBoolean("vibration", 0)) {
            this.vib = ((android.os.Vibrator) this.getContext().getSystemService("vibrator"));
            this.vib.vibrate(50);
        }
        if (android.os.Build$VERSION.SDK_INT < 9) {
            android.content.Intent v0_1 = new android.content.Intent("android.intent.action.VIEW");
            v0_1.setClassName("com.android.settings", "com.android.settings.InstalledAppDetails");
            v0_1.putExtra("com.android.settings.ApplicationPkgName", com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName);
            v0_1.putExtra("pkg", com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName);
            v0_1.setFlags(131072);
            this.startActivity(v0_1);
        } else {
            android.content.Intent v0_3 = new android.content.Intent("android.settings.APPLICATION_DETAILS_SETTINGS", android.net.Uri.parse(new StringBuilder().append("package:").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.pkgName).toString()));
            v0_3.setFlags(131072);
            this.startActivity(v0_3);
        }
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.return_from_control_panel = 1;
        return;
    }

    public void show_integrate_to_system(com.android.vending.billing.InAppBillingService.LUCK.PkgListItem p5, boolean p6)
    {
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getBoolean("vibration", 0)) {
            this.vib = ((android.os.Vibrator) this.getContext().getSystemService("vibrator"));
            this.vib.vibrate(50);
        }
        com.chelpus.Utils.showDialogYesNo(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.name, com.chelpus.Utils.getText(2131165502), new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$112(this, p6), new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$112(this, p6), 0);
        return;
    }

    public void sqlitecopy()
    {
        java.io.File v0_1 = new java.io.File(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir).append("/").append("sqlite3").toString());
        if (!v0_1.exists()) {
            try {
                com.chelpus.Utils.getAssets("sqlite3", com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir);
            } catch (String v1) {
            }
            com.chelpus.Utils.run_all(new StringBuilder().append("chmod 777 ").append(v0_1.getAbsolutePath()).toString());
        }
        return;
    }

    public void support(com.android.vending.billing.InAppBillingService.LUCK.PkgListItem p9, String p10)
    {
        try {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.str = "";
            String v2 = "not_system";
        } catch (com.android.vending.billing.InAppBillingService.LUCK.PatchData v4) {
            return;
        }
        if (p9.system) {
            v2 = "system";
        }
        System.out.println(p10);
        String v3 = String.valueOf(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(p9.pkgName, 0).applicationInfo.uid);
        String v1 = "runpatchsupportOld";
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.api > 20) {
            v1 = "runpatchsupport";
        }
        String v0 = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.dalvikruncommand).append(".").append(v1).append(" ").append(p9.pkgName).append(" ").append(p10).append(" ").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(p9.pkgName, 0).applicationInfo.sourceDir).append(" ").append(this.getContext().getFilesDir()).append(" ").append(v2).append(" ").append("a").append(" ").append(com.chelpus.Utils.getCurrentRuntimeValue()).append(" ").append(v3).toString();
        System.out.println(p10);
        if (p10.contains("copyDC")) {
            v0 = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.dalvikruncommand).append(".").append(v1).append(" ").append(p9.pkgName).append(" ").append(p10).append(" ").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(p9.pkgName, 0).applicationInfo.sourceDir).append(" ").append(this.getContext().getFilesDir()).append(" ").append(v2).append(" ").append("copyDC").append(" ").append(com.chelpus.Utils.getCurrentRuntimeValue()).append(" ").append(v3).toString();
            System.out.println(v0);
        }
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchData = new com.android.vending.billing.InAppBillingService.LUCK.PatchData();
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchData.pli = p9;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchData.typePatch = "SUPPORT";
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchData.appdir = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(p9.pkgName, 0).applicationInfo.sourceDir;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchData.pkg = p9.pkgName;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchData.uid = String.valueOf(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(p9.pkgName, 0).applicationInfo.uid);
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchData.cmd1 = v0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchData.result = p10;
        this.LvlAdsPatch();
        return;
    }

    public boolean testPath(boolean p8, String p9)
    {
        int v1 = 0;
        System.out.println("test path.");
        try {
            if (new java.io.File(p9).exists()) {
                if (new java.io.File(p9).exists()) {
                    if (!new java.io.File(new StringBuilder().append(p9).append("/tmp.txt").toString()).createNewFile()) {
                        if (p8) {
                            this.showMessage(com.chelpus.Utils.getText(2131165748), com.chelpus.Utils.getText(2131165530));
                        }
                    } else {
                        new java.io.File(new StringBuilder().append(p9).append("/tmp.txt").toString()).delete();
                        v1 = 1;
                    }
                } else {
                    this.showMessage(com.chelpus.Utils.getText(2131165748), com.chelpus.Utils.getText(2131165530));
                }
            } else {
                new java.io.File(p9).mkdirs();
            }
        } catch (java.io.IOException v0) {
            if (!p8) {
            } else {
                this.showMessage(com.chelpus.Utils.getText(2131165748), com.chelpus.Utils.getText(2131165530));
            }
        }
        return v1;
    }

    public boolean testSD(boolean p9)
    {
        int v1 = 0;
        try {
            if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath.startsWith(this.getContext().getDir("sdcard", 0).getAbsolutePath())) {
                if (!new java.io.File(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).exists()) {
                    new java.io.File(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).mkdirs();
                }
                if (new java.io.File(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).exists()) {
                    new java.io.File(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).append("/tmp.txt").toString()).delete();
                    System.out.println(new StringBuilder().append("LuckyPatcher (sdcard test create file): ").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).toString());
                    if (!new java.io.File(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).append("/tmp.txt").toString()).createNewFile()) {
                        if (p9) {
                            this.showMessage(com.chelpus.Utils.getText(2131165748), com.chelpus.Utils.getText(2131165531));
                        }
                    } else {
                        System.out.println(new StringBuilder().append("LuckyPatcher (sdcard test create file true): ").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).toString());
                        new java.io.File(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).append("/tmp.txt").toString()).delete();
                        v1 = 1;
                    }
                } else {
                    System.out.println(new StringBuilder().append("LuckyPatcher (sdcard directory not found and not created): ").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).toString());
                }
            } else {
                System.out.println(new StringBuilder().append("LuckyPatcher (sdcard to internal memory): ").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).toString());
                if (p9) {
                    this.showMessage(com.chelpus.Utils.getText(2131165748), com.chelpus.Utils.getText(2131165531));
                }
            }
        } catch (java.io.IOException v0) {
            if (!p9) {
            } else {
                this.showMessage(com.chelpus.Utils.getText(2131165748), com.chelpus.Utils.getText(2131165531));
            }
        }
        return v1;
    }

    public void toolbar_addfree_clear()
    {
        com.chelpus.Utils.remount("/system", "rw");
        com.chelpus.Utils.run_all("chattr -ai /system/etc/hosts");
        android.widget.Toast v0_3 = new com.chelpus.Utils("");
        String v1_2 = new String[1];
        v1_2[0] = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.dalvikruncommand).append(".reworkhost").toString();
        v0_3.cmdRoot(v1_2);
        com.chelpus.Utils.remount("/system", "ro");
        android.widget.Toast.makeText(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), com.chelpus.Utils.getText(2131165491), 1).show();
        return;
    }

    public void toolbar_addfree_off()
    {
        this.runWithWait(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$57(this));
        return;
    }

    public void toolbar_addfree_on()
    {
        this.runWithWait(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$56(this));
        return;
    }

    public void toolbar_cloneApplication()
    {
        if (this.testSD(1)) {
            this.runWithWait(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$28(this));
        }
        return;
    }

    public void toolbar_createapkads(String p2)
    {
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.result = p2;
        if (this.testSD(1)) {
            this.runWithWait(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$26(this));
        }
        return;
    }

    public void toolbar_createapklvl(String p2)
    {
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.result = p2;
        if (this.testSD(1)) {
            this.runWithWait(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$23(this));
        }
        return;
    }

    public boolean toolbar_createapkpermissions(java.util.ArrayList p3, java.util.ArrayList p4)
    {
        this.permissions = p3;
        this.activities = p4;
        if (this.testSD(1)) {
            this.runWithWait(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$114(this));
        }
        return 1;
    }

    public void toolbar_createapksupport(String p2)
    {
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.result = p2;
        if (this.testSD(1)) {
            this.runWithWait(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$30(this));
        }
        return;
    }

    public void toolbar_install_system(com.android.vending.billing.InAppBillingService.LUCK.FileApkListItem p5)
    {
        if (this.testSD(1)) {
            com.chelpus.Utils.showDialogYesNo(com.chelpus.Utils.getText(2131165748), com.chelpus.Utils.getText(2131165559), new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$99(this, p5), new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$99(this, p5), 0);
        }
        return;
    }

    public void toolbar_refresh_click()
    {
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia != null) {
            Thread v0_1 = new Thread(new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$AppScanning(this));
            v0_1.setPriority(10);
            v0_1.start();
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.refresh = 1;
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia.refreshPkgs(1);
        }
        return;
    }

    public void toolbar_restore(com.android.vending.billing.InAppBillingService.LUCK.FileApkListItem p5, boolean p6)
    {
        if (this.testSD(1)) {
            this.apkitem = p5;
            new java.io.File(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).append("/Backup/").toString()).mkdirs();
            com.chelpus.Utils.showDialogYesNo(p5.name, new StringBuilder().append(com.chelpus.Utils.getText(2131165643)).append(" ").append(this.apkitem.name).append("?").toString(), new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$96(this), 0, 0);
        }
        return;
    }

    public void toolbar_uninstall(com.android.vending.billing.InAppBillingService.LUCK.FileApkListItem p11)
    {
        if (this.testSD(1)) {
            this.apkitem = p11;
            try {
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getPackageInfo(this.apkitem.pkgName, 0);
                android.content.Intent v1_1 = new android.content.Intent("android.intent.action.VIEW");
                v1_1.setDataAndType(android.net.Uri.fromFile(this.apkitem.backupfile), "application/vnd.android.package-archive");
                v1_1.setFlags(131072);
                this.startActivity(v1_1);
                android.content.Intent v3_1 = new android.content.Intent("android.intent.action.DELETE", android.net.Uri.parse(new StringBuilder().append("package:").append(this.apkitem.pkgName).toString()));
                v1_1.setFlags(131072);
                this.startActivity(v3_1);
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.addapps = 0;
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.refresh = 1;
            } catch (android.content.pm.PackageManager$NameNotFoundException v0) {
                android.content.Intent v1_3 = new android.content.Intent("android.intent.action.VIEW");
                v1_3.setDataAndType(android.net.Uri.fromFile(this.apkitem.backupfile), "application/vnd.android.package-archive");
                v1_3.setFlags(131072);
                this.startActivity(v1_3);
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.addapps = 0;
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.refresh = 1;
            }
        }
        return;
    }

    public void uninstall_click()
    {
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getBoolean("vibration", 0)) {
            this.vib = ((android.os.Vibrator) this.getContext().getSystemService("vibrator"));
            this.vib.vibrate(50);
        }
        com.chelpus.Utils.showDialogYesNo(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.pli.name, com.chelpus.Utils.getText(2131165730), new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$54(this), new com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment$54(this), 0);
        return;
    }

    public void vibrateEnd()
    {
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getBoolean("vibration", 0)) {
            this.vib = ((android.os.Vibrator) com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getSystemService("vibrator"));
            this.vib.vibrate(2500);
        }
        return;
    }
}
