package com.android.vending.billing.InAppBillingService.LUCK;
 class SetPrefs$26$2 implements android.widget.AdapterView$OnItemClickListener {
    final synthetic com.android.vending.billing.InAppBillingService.LUCK.SetPrefs$26 this$1;

    SetPrefs$26$2(com.android.vending.billing.InAppBillingService.LUCK.SetPrefs$26 p1)
    {
        this.this$1 = p1;
        return;
    }

    public void onItemClick(android.widget.AdapterView p9, android.view.View p10, int p11, long p12)
    {
        if (this.this$1.dialog6.isShowing()) {
            this.this$1.dialog6.dismiss();
        }
        android.content.SharedPreferences v3 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getSharedPreferences("config", 0);
        String[] v4 = ((String) p9.getItemAtPosition(p11)).split("_");
        java.util.Locale v1_0 = 0;
        if (v4.length == 1) {
            v1_0 = new java.util.Locale(v4[0]);
        }
        if (v4.length == 2) {
            v1_0 = new java.util.Locale(v4[0], v4[1], "");
        }
        if (v4.length == 3) {
            v1_0 = new java.util.Locale(v4[0], v4[1], v4[2]);
        }
        java.util.Locale.setDefault(v1_0);
        android.content.res.Configuration v0_1 = new android.content.res.Configuration();
        v0_1.locale = v1_0;
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().updateConfiguration(v0_1, com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getRes().getDisplayMetrics());
        v3.edit().putString("force_language", ((String) p9.getItemAtPosition(p11))).commit();
        android.content.Intent v2 = this.this$1.this$0.getIntent();
        v2.addFlags(131072);
        this.this$1.this$0.finish();
        this.this$1.this$0.startActivity(v2);
        v3.edit().putBoolean("settings_change", 1).commit();
        v3.edit().putBoolean("lang_change", 1).commit();
        return;
    }
}
