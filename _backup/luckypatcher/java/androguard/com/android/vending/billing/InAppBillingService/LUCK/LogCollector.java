package com.android.vending.billing.InAppBillingService.LUCK;
public class LogCollector {
    public static final String LINE_SEPARATOR = "
";
    private static final String LOG_TAG;
    private java.util.ArrayList mLastLogs;
    private String mPackageName;
    private java.util.regex.Pattern mPattern;
    private android.content.SharedPreferences mPrefs;

    static LogCollector()
    {
        com.android.vending.billing.InAppBillingService.LUCK.LogCollector.LOG_TAG = com.android.vending.billing.InAppBillingService.LUCK.LogCollector.getSimpleName();
        return;
    }

    public LogCollector()
    {
        this.mPackageName = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getPackageName();
        String v2_1 = new Object[1];
        v2_1[0] = this.mPackageName.replace(".", "\\.");
        this.mPattern = java.util.regex.Pattern.compile(String.format("(.*)E\\/AndroidRuntime\\(\\s*\\d+\\)\\:\\s*at\\s%s.*", v2_1));
        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance();
        this.mPrefs = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getSharedPreferences("LogCollector", 0);
        this.mLastLogs = new java.util.ArrayList();
        this.mPrefs.edit().clear().commit();
        return;
    }

    private void collectLog(java.util.List p18, String p19, String p20, String[] p21, boolean p22)
    {
        p18.clear();
        java.util.ArrayList v11_1 = new java.util.ArrayList();
        if (p19 == null) {
            p19 = "time";
        }
        v11_1.add("-v");
        v11_1.add(p19);
        if (p20 != null) {
            v11_1.add("-b");
            v11_1.add(p20);
        }
        if (p21 != null) {
            java.util.Collections.addAll(v11_1, p21);
        }
        try {
            if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su) && (!p22)) {
                Process v12_0 = Runtime.getRuntime().exec("su");
                java.io.DataOutputStream v10_1 = new java.io.DataOutputStream(v12_0.getOutputStream());
                java.io.DataInputStream v8_1 = new java.io.DataInputStream(v12_0.getErrorStream());
                byte[] v1_0 = new byte[v8_1.available()];
                v8_1.read(v1_0);
                System.out.println(new String(v1_0));
                java.io.BufferedReader v2_1 = new java.io.BufferedReader(new java.io.InputStreamReader(v12_0.getInputStream()));
                if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath.startsWith(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchAct.getDir("sdcard", 0).getAbsolutePath())) {
                    v10_1.writeBytes("logcat -d -v time\n");
                } else {
                    v10_1.writeBytes("logcat -d System.out:* *:S\n");
                }
                v10_1.flush();
                v10_1.close();
                try {
                    while(true) {
                        String v9_0 = v2_1.readLine();
                        p18.add(v9_0);
                    }
                    if (v2_1 != null) {
                        v2_1.close();
                    }
                    v12_0.destroy();
                    System.out.println("Collect logs no root.");
                    p18.add("\n\n\n*********************************** NO ROOT *******************************************************\n\n\n");
                    java.util.ArrayList v5_1 = new java.util.ArrayList();
                    v5_1.add("logcat");
                    v5_1.add("-d");
                    v5_1.addAll(v11_1);
                    String[] v3_0 = new String[v5_1.size()];
                    String[] v3_2 = ((String[]) v5_1.toArray(v3_0));
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath.startsWith(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchAct.getDir("sdcard", 0).getAbsolutePath())) {
                        String[] v4_0 = new String[4];
                        v4_0[0] = "logcat";
                        v4_0[1] = "-d";
                        v4_0[2] = "System.out:*";
                        v4_0[3] = "*:S";
                        v3_2 = v4_0;
                    }
                    Process v12_1 = Runtime.getRuntime().exec(v3_2);
                    java.io.BufferedReader v2_3 = new java.io.BufferedReader(new java.io.InputStreamReader(v12_1.getInputStream()));
                    while(true) {
                        String v9_1 = v2_3.readLine();
                        if (v9_1 == null) {
                            break;
                        }
                        p18.add(v9_1);
                    }
                    v12_1.destroy();
                    if (v2_3 == null) {
                        if (p18.size() == 0) {
                            Process v12_3 = Runtime.getRuntime().exec("su");
                            java.io.DataOutputStream v10_3 = new java.io.DataOutputStream(v12_3.getOutputStream());
                            java.io.DataInputStream v8_3 = new java.io.DataInputStream(v12_3.getErrorStream());
                            byte[] v1_1 = new byte[v8_3.available()];
                            v8_3.read(v1_1);
                            System.out.println(new String(v1_1));
                            java.io.BufferedReader v2_7 = new java.io.BufferedReader(new java.io.InputStreamReader(v12_3.getInputStream()));
                            if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath.startsWith(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchAct.getDir("sdcard", 0).getAbsolutePath())) {
                                v10_3.writeBytes("logcat -d\n");
                            } else {
                                v10_3.writeBytes("logcat -d System.out:* *:S\n");
                            }
                            v10_3.flush();
                            v10_3.close();
                            while(true) {
                                String v9_3 = v2_7.readLine();
                                if (v9_3 == null) {
                                    break;
                                }
                                p18.add(v9_3);
                            }
                            v12_3.destroy();
                            if (v2_7 != null) {
                                v2_7.close();
                            }
                        }
                    } else {
                        v2_3.close();
                    }
                } catch (Exception v6) {
                    v6.printStackTrace();
                }
                if (v9_0 == null) {
                }
            } else {
                System.out.println("Collect logs no root.");
                java.util.ArrayList v5_3 = new java.util.ArrayList();
                v5_3.add("logcat");
                v5_3.add("-d");
                v5_3.addAll(v11_1);
                String[] v3_3 = new String[v5_3.size()];
                String[] v3_5 = ((String[]) v5_3.toArray(v3_3));
                if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath.startsWith(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchAct.getDir("sdcard", 0).getAbsolutePath())) {
                    String[] v4_1 = new String[4];
                    v4_1[0] = "logcat";
                    v4_1[1] = "-d";
                    v4_1[2] = "System.out:*";
                    v4_1[3] = "*:S";
                    v3_5 = v4_1;
                }
                Process v12_2 = Runtime.getRuntime().exec(v3_5);
                java.io.BufferedReader v2_5 = new java.io.BufferedReader(new java.io.InputStreamReader(v12_2.getInputStream()));
                while(true) {
                    String v9_2 = v2_5.readLine();
                    if (v9_2 == null) {
                        break;
                    }
                    p18.add(v9_2);
                }
                v12_2.destroy();
                if (v2_5 == null) {
                } else {
                    v2_5.close();
                }
            }
        } catch (Exception v6) {
            try {
                Process v12_4 = Runtime.getRuntime().exec("su");
                java.io.DataOutputStream v10_5 = new java.io.DataOutputStream(v12_4.getOutputStream());
                java.io.DataInputStream v8_5 = new java.io.DataInputStream(v12_4.getErrorStream());
                byte[] v1_2 = new byte[v8_5.available()];
                v8_5.read(v1_2);
                System.out.println(new String(v1_2));
                java.io.BufferedReader v2_9 = new java.io.BufferedReader(new java.io.InputStreamReader(v12_4.getInputStream()));
            } catch (java.io.IOException v7) {
                v7.printStackTrace();
                return;
            }
            if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath.startsWith(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchAct.getDir("sdcard", 0).getAbsolutePath())) {
                v10_5.writeBytes("logcat -d\n");
            } else {
                v10_5.writeBytes("logcat -d System.out:* *:S\n");
            }
            v10_5.flush();
            v10_5.close();
        }
        return;
    }

    private String collectPhoneInfo()
    {
        Object[] v1_1 = new Object[3];
        v1_1[0] = android.os.Build.BRAND;
        v1_1[1] = android.os.Build.MODEL;
        v1_1[2] = android.os.Build$VERSION.RELEASE;
        return String.format("Carrier:%s\nModel:%s\nFirmware:%s\n", v1_1);
    }

    public boolean collect(android.content.Context p18, boolean p19)
    {
        java.util.ArrayList v2 = this.mLastLogs;
        if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su) {
            this.collectLog(v2, 0, 0, 0, 1);
        } else {
            this.collectLog(v2, 0, 0, 0, 0);
        }
        if ((p19) && (v2.size() > 0)) {
            try {
                String v15 = p18.getPackageManager().getPackageInfo(p18.getPackageName(), 0).versionName;
            } catch (java.io.IOException v8_0) {
                v8_0.printStackTrace();
            }
            StringBuilder v16 = new StringBuilder(new StringBuilder().append("Lucky Patcher ").append(v15).toString()).append("\n");
            String v14 = this.collectPhoneInfo();
            v16.append("\n").append(v14);
            try {
                while(true) {
                    byte[] v1_10 = v2.iterator();
                    if (!v1_10.hasNext()) {
                        break;
                    }
                    v16.append("\n").append(((String) v1_10.next()));
                    System.out.println("LuckyPatcher (LogCollector): try again collect log.");
                    v16 = new StringBuilder(v15).append("\n");
                    v16.append("\n").append(v14);
                }
                String v7 = v16.toString();
                new java.io.File("abrakakdabra");
                try {
                    java.io.File v13_1 = new java.io.File(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).append("/Log/log.txt").toString());
                    try {
                        new java.io.File(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).append("/Log").toString()).mkdirs();
                    } catch (java.io.IOException v8_1) {
                        v8_1.printStackTrace();
                    }
                    if (v13_1.exists()) {
                        v13_1.delete();
                    }
                    if (new java.io.File(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).append("/Log/log.zip").toString()).exists()) {
                        new java.io.File(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).append("/Log/log.zip").toString()).delete();
                    }
                    v13_1.createNewFile();
                    new java.io.FileOutputStream(v13_1).write(v7.getBytes());
                } catch (java.io.IOException v8_1) {
                }
            } catch (java.io.IOException v8) {
            }
            while (v1_10.hasNext()) {
            }
        }
        byte[] v1_35;
        if (v2.size() <= 0) {
            v1_35 = 0;
        } else {
            v1_35 = 1;
        }
        return v1_35;
    }

    public boolean hasForceCloseHappened()
    {
        String[] v4 = new String[1];
        v4[0] = "*:E";
        java.util.ArrayList v1_1 = new java.util.ArrayList();
        if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su) {
            this.collectLog(v1_1, "time", 0, v4, 1);
        } else {
            this.collectLog(v1_1, "time", 0, v4, 0);
        }
        int v8;
        if (v1_1.size() <= 0) {
            System.out.println("Clear all FC logcat prefs...");
            this.mPrefs.edit().clear().commit();
            v8 = 0;
        } else {
            v8 = 0;
            int v7 = 0;
            android.content.SharedPreferences$Editor v0_10 = v1_1.iterator();
            while (v0_10.hasNext()) {
                java.util.regex.Matcher v11 = this.mPattern.matcher(((String) v0_10.next()));
                android.content.SharedPreferences v12 = this.mPrefs;
                if (v11.matches()) {
                    v7 = 1;
                    String v13 = v11.group(1);
                    if (!v12.getBoolean(v13, 0)) {
                        v8 = 1;
                        v12.edit().putBoolean(v13, 1).commit();
                    }
                }
            }
            if ((v8 == 0) && (v7 == 0)) {
                System.out.println("Clear all FC logcat prefs...");
                this.mPrefs.edit().clear().commit();
            }
        }
        return v8;
    }

    public void sendLog(android.content.Context p16, String p17, String p18, String p19)
    {
        java.io.File v7_1 = new java.io.File(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).append("/Log/log.txt").toString());
        android.net.Uri.parse("file://");
        try {
            net.lingala.zip4j.core.ZipFile v10_1 = new net.lingala.zip4j.core.ZipFile(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).append("/Log/log.zip").toString());
            net.lingala.zip4j.model.ZipParameters v8_1 = new net.lingala.zip4j.model.ZipParameters();
            v8_1.setCompressionMethod(8);
            v8_1.setCompressionLevel(5);
            v10_1.addFile(v7_1, v8_1);
            android.net.Uri v9 = android.net.Uri.parse(new StringBuilder().append("file://").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).append("/Log/log.zip").toString());
        } catch (RuntimeException v3_0) {
            v3_0.printStackTrace();
        } catch (com.android.vending.billing.InAppBillingService.LUCK.patchActivity v11) {
        }
        String v2 = "not found";
        if (com.chelpus.Utils.exists("/system/bin/dalvikvm")) {
            v2 = "/system/bin/dalvikvm";
        }
        if (com.chelpus.Utils.exists("/system/bin/dalvikvm32")) {
            v2 = "/system/bin/dalvikvm32";
        }
        String v1_1;
        if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath.startsWith(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchAct.getDir("sdcard", 0).getAbsolutePath())) {
            v1_1 = new StringBuilder().append("Email contain log.txt with bugs by Lucky Patcher! Please describe the Problem and tap to send email.\n\n\nRoot:").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su).append("\n").append("Runtime: ").append(com.chelpus.Utils.getCurrentRuntimeValue()).append("\n").append("DeviceID: ").append(android.provider.Settings$Secure.getString(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getContentResolver(), "android_id")).append("\n").append(android.os.Build.BOARD).append("\n").append(android.os.Build.BRAND).append("\n").append(android.os.Build.CPU_ABI).append("\n").append(android.os.Build.DEVICE).append("\n").append(android.os.Build.DISPLAY).append("\n").append(android.os.Build.FINGERPRINT).append("\n").append("Lucky Patcher ver: ").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.version).append("\n").append("LP files directory: ").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).append("\n").append("dalvikvm file:").append(v2).append("\n\n").toString();
        } else {
            v1_1 = new StringBuilder().append("Email contain log.txt with bugs by Lucky Patcher! Please describe the Problem and tap to send email.\n\n\nRoot:").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su).append("\n").append("Runtime: ").append(com.chelpus.Utils.getCurrentRuntimeValue()).append("\n").append("DeviceID: ").append(android.provider.Settings$Secure.getString(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getContentResolver(), "android_id")).append("\n").append(android.os.Build.BOARD).append("\n").append(android.os.Build.BRAND).append("\n").append(android.os.Build.CPU_ABI).append("\n").append(android.os.Build.DEVICE).append("\n").append(android.os.Build.DISPLAY).append("\n").append(android.os.Build.FINGERPRINT).append("\n").append("Lucky Patcher ver: ").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.version).append("\n").append("LP files directory: ").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.basepath).append("\n").append("dalvikvm file:").append(v2).append("\n\n").append("").toString();
        }
        try {
            android.content.Intent v12_72 = System.getenv().entrySet().iterator();
        } catch (RuntimeException v3_1) {
            v3_1.printStackTrace();
            android.content.Intent v6_1 = new android.content.Intent("android.intent.action.SEND");
            v6_1.setType("text/plain");
            android.content.Intent v12_74 = new String[1];
            v12_74[0] = p17;
            v6_1.putExtra("android.intent.extra.EMAIL", v12_74);
            v6_1.putExtra("android.intent.extra.SUBJECT", p18);
            v6_1.putExtra("android.intent.extra.TEXT", v1_1);
            v6_1.putExtra("android.intent.extra.STREAM", v9);
            try {
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchAct.startActivity(android.content.Intent.createChooser(v6_1, "Send mail"));
            } catch (RuntimeException v3_2) {
                v3_2.printStackTrace();
            }
            return;
        }
        while (v12_72.hasNext()) {
            java.util.Map$Entry v4_1 = ((java.util.Map$Entry) v12_72.next());
            v1_1 = new StringBuilder().append(v1_1).append("\n").append(((String) v4_1.getKey())).append(":").append(((String) v4_1.getValue())).toString();
        }
    }
}
