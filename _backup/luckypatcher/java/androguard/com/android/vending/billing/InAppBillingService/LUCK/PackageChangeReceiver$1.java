package com.android.vending.billing.InAppBillingService.LUCK;
 class PackageChangeReceiver$1 implements java.lang.Runnable {
    final synthetic com.android.vending.billing.InAppBillingService.LUCK.PackageChangeReceiver this$0;
    final synthetic android.content.Context val$ctx;
    final synthetic android.content.Intent val$intent;

    PackageChangeReceiver$1(com.android.vending.billing.InAppBillingService.LUCK.PackageChangeReceiver p1, android.content.Intent p2, android.content.Context p3)
    {
        this.this$0 = p1;
        this.val$intent = p2;
        this.val$ctx = p3;
        return;
    }

    public void run()
    {
        System.out.println(this.val$intent.getAction());
        if (this.val$intent.getAction().equals("android.intent.action.PACKAGE_CHANGED")) {
            android.content.Intent v12_1 = new android.content.Intent(this.val$ctx, com.android.vending.billing.InAppBillingService.LUCK.widgets.AppDisablerWidget);
            v12_1.setAction(com.android.vending.billing.InAppBillingService.LUCK.widgets.AppDisablerWidget.ACTION_WIDGET_RECEIVER_Updater);
            this.val$ctx.sendBroadcast(v12_1);
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.init();
            try {
                System.out.println(this.val$intent.getData().toString());
            } catch (android.content.pm.PackageManager$NameNotFoundException v8_2) {
                v8_2.printStackTrace();
            }
            if (this.this$0.getPackageName(this.val$intent).equals(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPackage().getName())) {
                String[] v7_0 = this.val$intent.getStringArrayExtra("android.intent.extra.changed_component_name_list");
                if ((v7_0 != null) && ((v7_0.length > 0) && (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag != null))) {
                    System.out.println(new StringBuilder().append("update adapt ").append(v7_0[0]).toString());
                    if (v7_0[0].contains("com.android.vending.billing.InAppBillingService.LUCK.MainActivity")) {
                        try {
                            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su) {
                                android.content.Intent v11_1 = new android.content.Intent("android.intent.action.MAIN");
                                v11_1.addCategory("android.intent.category.HOME");
                                v11_1.addCategory("android.intent.category.DEFAULT");
                                android.support.v4.app.FragmentActivity v24_23 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().queryIntentActivities(v11_1, 0).iterator();
                                while (v24_23.hasNext()) {
                                    android.content.pm.ResolveInfo v22_1 = ((android.content.pm.ResolveInfo) v24_23.next());
                                    if (v22_1.activityInfo != null) {
                                        int v26_135 = new com.android.vending.billing.InAppBillingService.LUCK.PackageChangeReceiver$1$1;
                                        v26_135(this, v22_1.activityInfo.processName, v22_1.activityInfo.packageName);
                                        new Thread(v26_135).start();
                                    }
                                }
                            }
                        } catch (android.content.pm.PackageManager$NameNotFoundException v8_0) {
                            v8_0.printStackTrace();
                        }
                        com.android.vending.billing.InAppBillingService.LUCK.PackageChangeReceiver$1$9 v25_20 = new com.android.vending.billing.InAppBillingService.LUCK.PackageChangeReceiver$1$2;
                        v25_20(this);
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.runToMain(v25_20);
                    }
                }
            }
            if ((this.this$0.getPackageName(this.val$intent).equals("com.android.vending")) || (this.this$0.getPackageName(this.val$intent).equals("com.google.android.gms"))) {
                String[] v7_1 = this.val$intent.getStringArrayExtra("android.intent.extra.changed_component_name_list");
                if ((v7_1 != null) && (v7_1.length > 0)) {
                    if ((this.this$0.getPackageName(this.val$intent).equals("com.google.android.gms")) && (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag != null)) {
                        System.out.println(new StringBuilder().append("update adapt ").append(v7_1[0]).toString());
                        com.android.vending.billing.InAppBillingService.LUCK.PackageChangeReceiver$1$9 v25_34 = new com.android.vending.billing.InAppBillingService.LUCK.PackageChangeReceiver$1$3;
                        v25_34(this);
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.runToMain(v25_34);
                    }
                    if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.adapt != null) && (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag != null)) {
                        System.out.println(new StringBuilder().append("update adapt ").append(v7_1[0]).toString());
                        com.android.vending.billing.InAppBillingService.LUCK.PackageChangeReceiver$1$9 v25_40 = new com.android.vending.billing.InAppBillingService.LUCK.PackageChangeReceiver$1$4;
                        v25_40(this);
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.runToMain(v25_40);
                    }
                }
            }
            if (this.this$0.getPackageName(this.val$intent).equals(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getPackageName())) {
                String[] v7_2 = this.val$intent.getStringArrayExtra("android.intent.extra.changed_component_name_list");
                if ((v7_2 != null) && (v7_2.length > 0)) {
                    int v5 = 0;
                    int v16 = 0;
                    int v9 = 0;
                    int v10 = 0;
                    com.android.vending.billing.InAppBillingService.LUCK.PackageChangeReceiver$1$9 v25_45 = v7_2.length;
                    android.support.v4.app.FragmentActivity v24_48 = 0;
                    while (v24_48 < v25_45) {
                        String v6 = v7_2[v24_48];
                        if ((v6.equals("com.google.android.finsky.billing.iab.MarketBillingService")) || (v6.equals("com.google.android.finsky.billing.iab.InAppBillingService"))) {
                            System.out.println(v6);
                            android.content.Intent v14_1 = new android.content.Intent(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), com.android.vending.billing.InAppBillingService.LUCK.widgets.inapp_widget);
                            v14_1.setPackage(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getPackageName());
                            v14_1.setAction(com.android.vending.billing.InAppBillingService.LUCK.widgets.inapp_widget.ACTION_WIDGET_RECEIVER_Updater);
                            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().sendBroadcast(v14_1);
                            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), com.google.android.finsky.billing.iab.InAppBillingService)) != 1) {
                                v9 = 1;
                            } else {
                                v5 = 1;
                            }
                        }
                        if (v6.equals("com.licensinghack.LicensingService")) {
                            System.out.println(v6);
                            android.content.Intent v13_1 = new android.content.Intent(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), com.android.vending.billing.InAppBillingService.LUCK.widgets.lvl_widget);
                            v13_1.setPackage(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getPackageName());
                            v13_1.setAction(com.android.vending.billing.InAppBillingService.LUCK.widgets.lvl_widget.ACTION_WIDGET_RECEIVER_Updater);
                            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().sendBroadcast(v13_1);
                            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), com.google.android.finsky.services.LicensingService)) != 1) {
                                v10 = 1;
                            } else {
                                v16 = 1;
                            }
                        }
                        v24_48++;
                    }
                    if (v5 != 0) {
                        this.this$0.connectToBilling();
                    }
                    if (v16 != 0) {
                        this.this$0.connectToLicensing();
                    }
                    if (v9 != 0) {
                        com.android.vending.billing.InAppBillingService.LUCK.PackageChangeReceiver$1$9 v25_46 = new com.android.vending.billing.InAppBillingService.LUCK.PackageChangeReceiver$1$5;
                        v25_46(this);
                        new Thread(v25_46).start();
                    }
                    if (v10 != 0) {
                        com.android.vending.billing.InAppBillingService.LUCK.PackageChangeReceiver$1$9 v25_47 = new com.android.vending.billing.InAppBillingService.LUCK.PackageChangeReceiver$1$6;
                        v25_47(this);
                        new Thread(v25_47).start();
                    }
                }
            }
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.days = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getInt("days_on_up", 1);
            try {
                com.android.vending.billing.InAppBillingService.LUCK.PkgListItem v15_1 = new com.android.vending.billing.InAppBillingService.LUCK.PkgListItem(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), this.this$0.getPackageName(this.val$intent), com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.days, 0);
            } catch (android.content.pm.PackageManager$NameNotFoundException v8_1) {
                v8_1.printStackTrace();
                System.out.println("Item dont create. And dont add to database.");
            }
            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.database == null) {
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.database = new com.android.vending.billing.InAppBillingService.LUCK.DatabaseHelper(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance());
            }
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.database.savePackage(v15_1);
            if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.desktop_launch) && ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia != null) && ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag != null) && (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getActivity() != null)))) {
                android.support.v4.app.FragmentActivity v24_69 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getActivity();
                com.android.vending.billing.InAppBillingService.LUCK.PackageChangeReceiver$1$9 v25_53 = new com.android.vending.billing.InAppBillingService.LUCK.PackageChangeReceiver$1$7;
                v25_53(this, v15_1);
                v24_69.runOnUiThread(v25_53);
            }
        }
        if (this.val$intent.getAction().equals("android.intent.action.PACKAGE_REPLACED")) {
            android.content.Intent v12_3 = new android.content.Intent(this.val$ctx, com.android.vending.billing.InAppBillingService.LUCK.widgets.AppDisablerWidget);
            v12_3.setAction(com.android.vending.billing.InAppBillingService.LUCK.widgets.AppDisablerWidget.ACTION_WIDGET_RECEIVER_Updater);
            this.val$ctx.sendBroadcast(v12_3);
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.init();
            try {
                System.out.println(this.val$intent.getData().toString());
                String v19_0 = this.this$0.getPackageName(this.val$intent);
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().edit().putBoolean(v19_0, 0).commit();
            } catch (android.content.pm.PackageManager$NameNotFoundException v8_4) {
                v8_4.printStackTrace();
            }
            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su) {
                if (new java.io.File(new StringBuilder().append("/data/app/").append(v19_0).append("-1.odex").toString()).exists()) {
                    com.chelpus.Utils.run_all(new StringBuilder().append("rm /data/app/").append(v19_0).append("-1.odex").toString());
                }
                if (new java.io.File(new StringBuilder().append("/data/app/").append(v19_0).append("-2.odex").toString()).exists()) {
                    com.chelpus.Utils.run_all(new StringBuilder().append("rm /data/app/").append(v19_0).append("-2.odex").toString());
                }
                if (new java.io.File(new StringBuilder().append("/data/app/").append(v19_0).append(".odex").toString()).exists()) {
                    com.chelpus.Utils.run_all(new StringBuilder().append("rm /data/app/").append(v19_0).append(".odex").toString());
                }
            }
            if ((v19_0.contains("com.android.vending")) || (v19_0.contains(this.val$ctx.getPackageName()))) {
                if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getComponentEnabledSetting(new android.content.ComponentName(this.val$ctx, com.google.android.finsky.services.LicensingService)) != 2) {
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), com.google.android.finsky.services.LicensingService), 1, 1);
                } else {
                    com.chelpus.Utils.market_licensing_services(1);
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), com.google.android.finsky.services.LicensingService), 2, 1);
                }
                if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getComponentEnabledSetting(new android.content.ComponentName(this.val$ctx, com.google.android.finsky.billing.iab.MarketBillingService)) != 2) && (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getComponentEnabledSetting(new android.content.ComponentName(this.val$ctx, com.google.android.finsky.billing.iab.InAppBillingService)) != 2)) {
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), com.google.android.finsky.billing.iab.MarketBillingService), 1, 1);
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), com.google.android.finsky.billing.iab.InAppBillingService), 1, 1);
                } else {
                    com.chelpus.Utils.market_billing_services(1);
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), com.google.android.finsky.billing.iab.MarketBillingService), 2, 1);
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), com.google.android.finsky.billing.iab.InAppBillingService), 2, 1);
                }
            }
            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getBoolean("manual_path", 0)) {
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.days = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getInt("days_on_up", 1);
                try {
                    com.android.vending.billing.InAppBillingService.LUCK.PkgListItem v15_3 = new com.android.vending.billing.InAppBillingService.LUCK.PkgListItem(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), this.this$0.getPackageName(this.val$intent), com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.days, 0);
                } catch (android.content.pm.PackageManager$NameNotFoundException v8_3) {
                    v8_3.printStackTrace();
                    System.out.println("Item dont create. And dont add to database.");
                }
                if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.database == null) {
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.database = new com.android.vending.billing.InAppBillingService.LUCK.DatabaseHelper(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance());
                }
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.database.savePackage(v15_3);
                if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.desktop_launch) && ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia != null) && ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag != null) && (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getActivity() != null)))) {
                    android.support.v4.app.FragmentActivity v24_143 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getActivity();
                    com.android.vending.billing.InAppBillingService.LUCK.PackageChangeReceiver$1$9 v25_111 = new com.android.vending.billing.InAppBillingService.LUCK.PackageChangeReceiver$1$8;
                    v25_111(this, v15_3);
                    v24_143.runOnUiThread(v25_111);
                }
            }
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.refresh = 1;
        }
        if (this.val$intent.getAction().equals("android.intent.action.PACKAGE_ADDED")) {
            android.content.Intent v12_5 = new android.content.Intent(this.val$ctx, com.android.vending.billing.InAppBillingService.LUCK.widgets.AppDisablerWidget);
            v12_5.setAction(com.android.vending.billing.InAppBillingService.LUCK.widgets.AppDisablerWidget.ACTION_WIDGET_RECEIVER_Updater);
            this.val$ctx.sendBroadcast(v12_5);
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.init();
            try {
                String v19_1 = this.this$0.getPackageName(this.val$intent);
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().edit().putBoolean(v19_1, 0).commit();
            } catch (android.content.pm.PackageManager$NameNotFoundException v8_6) {
                v8_6.printStackTrace();
            }
            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getBoolean("switch_auto_backup_apk", 0)) {
                System.out.println("Backup app on update");
                if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getBoolean("switch_auto_backup_apk_only_gp", 0)) {
                    this.this$0.backup(v19_1);
                }
            }
            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getBoolean("switch_auto_backup_apk_only_gp", 0)) {
                System.out.println("Backup app on update");
                try {
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getPackageManager().getInstallerPackageName(v19_1).equals("com.android.vending")) {
                        this.this$0.backup(v19_1);
                    }
                } catch (android.support.v4.app.FragmentActivity v24) {
                }
            }
            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.handlerToast == null) {
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.handlerToast = this.this$0.handler;
            }
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.days = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getInt("days_on_up", 1);
            com.android.vending.billing.InAppBillingService.LUCK.PkgListItem v15_5 = new com.android.vending.billing.InAppBillingService.LUCK.PkgListItem(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), this.this$0.getPackageName(this.val$intent), com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.days, 0);
            try {
                String v4 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getPackageManager().getPackageInfo(v15_5.pkgName, 0).applicationInfo.sourceDir;
            } catch (android.content.pm.PackageManager$NameNotFoundException v8_5) {
                v8_5.printStackTrace();
            }
            if ((v15_5.system) && ((v4.startsWith("/data")) && ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getBoolean("switch_auto_integrate_update", 0)) && (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su)))) {
                System.out.println("Integrate update to system on update app");
                java.util.ArrayList v20_1 = new java.util.ArrayList();
                v20_1.add(v15_5);
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.integrate_to_system(v20_1, 0, 1);
            }
            if ((!v15_5.system) && ((v4.startsWith("/data/")) && ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getBoolean("switch_auto_move_to_sd", 0)) && (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su)))) {
                android.support.v4.app.FragmentActivity v24_195 = new com.chelpus.Utils("");
                com.android.vending.billing.InAppBillingService.LUCK.PackageChangeReceiver$1$9 v0_158 = new String[1];
                com.android.vending.billing.InAppBillingService.LUCK.PackageChangeReceiver$1$9 v25_133 = v0_158;
                v25_133[0] = new StringBuilder().append("pm install -r -s -i com.android.vending ").append(v4).toString();
                v24_195.cmdRoot(v25_133);
            }
            if ((!v15_5.system) && ((v4.startsWith("/mnt/")) && ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getBoolean("switch_auto_move_to_internal", 0)) && (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su)))) {
                android.support.v4.app.FragmentActivity v24_203 = new com.chelpus.Utils("");
                com.android.vending.billing.InAppBillingService.LUCK.PackageChangeReceiver$1$9 v0_163 = new String[1];
                com.android.vending.billing.InAppBillingService.LUCK.PackageChangeReceiver$1$9 v25_137 = v0_163;
                v25_137[0] = new StringBuilder().append("pm install -r -f -i com.android.vending ").append(v4).toString();
                v24_203.cmdRoot(v25_137);
            }
            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su) {
                if (new java.io.File(new StringBuilder().append("/data/app/").append(v19_1).append("-1.odex").toString()).exists()) {
                    com.chelpus.Utils.run_all(new StringBuilder().append("rm /data/app/").append(v19_1).append("-1.odex").toString());
                }
                if (new java.io.File(new StringBuilder().append("/data/app/").append(v19_1).append("-2.odex").toString()).exists()) {
                    com.chelpus.Utils.run_all(new StringBuilder().append("rm /data/app/").append(v19_1).append("-2.odex").toString());
                }
                if (new java.io.File(new StringBuilder().append("/data/app/").append(v19_1).append(".odex").toString()).exists()) {
                    com.chelpus.Utils.run_all(new StringBuilder().append("rm /data/app/").append(v19_1).append(".odex").toString());
                }
            }
            if (v19_1.contains("com.android.vending")) {
                if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getComponentEnabledSetting(new android.content.ComponentName(this.val$ctx, com.google.android.finsky.services.LicensingService)) != 2) {
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), com.google.android.finsky.services.LicensingService), 1, 1);
                } else {
                    com.chelpus.Utils.market_licensing_services(1);
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), com.google.android.finsky.services.LicensingService), 2, 1);
                }
                if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getComponentEnabledSetting(new android.content.ComponentName(this.val$ctx, com.google.android.finsky.billing.iab.MarketBillingService)) != 2) && (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getComponentEnabledSetting(new android.content.ComponentName(this.val$ctx, com.google.android.finsky.billing.iab.InAppBillingService)) != 2)) {
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), com.google.android.finsky.billing.iab.MarketBillingService), 1, 1);
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), com.google.android.finsky.billing.iab.InAppBillingService), 1, 1);
                } else {
                    com.chelpus.Utils.market_billing_services(1);
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), com.google.android.finsky.billing.iab.MarketBillingService), 2, 1);
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), com.google.android.finsky.billing.iab.InAppBillingService), 2, 1);
                }
            }
            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getBoolean("manual_path", 0)) {
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.days = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().getInt("days_on_up", 1);
                if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.database == null) {
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.database = new com.android.vending.billing.InAppBillingService.LUCK.DatabaseHelper(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance());
                }
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.database.savePackage(v15_5);
                if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.desktop_launch) && ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia != null) && ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag != null) && (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getActivity() != null)))) {
                    android.support.v4.app.FragmentActivity v24_262 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getActivity();
                    com.android.vending.billing.InAppBillingService.LUCK.PackageChangeReceiver$1$9 v25_186 = new com.android.vending.billing.InAppBillingService.LUCK.PackageChangeReceiver$1$9;
                    v25_186(this, v15_5);
                    v24_262.runOnUiThread(v25_186);
                }
            }
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.refresh = 1;
        }
        if (this.val$intent.getAction().equals("android.intent.action.PACKAGE_REMOVED")) {
            android.content.Intent v12_7 = new android.content.Intent(this.val$ctx, com.android.vending.billing.InAppBillingService.LUCK.widgets.AppDisablerWidget);
            v12_7.setAction(com.android.vending.billing.InAppBillingService.LUCK.widgets.AppDisablerWidget.ACTION_WIDGET_RECEIVER_Updater);
            this.val$ctx.sendBroadcast(v12_7);
            System.out.println(new StringBuilder().append("delete trigger ").append(this.val$intent.getBooleanExtra("android.intent.extra.REPLACING", 0)).toString());
            if (!this.val$intent.getBooleanExtra("android.intent.extra.REPLACING", 0)) {
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.init();
                try {
                    String v19_2 = this.this$0.getPackageName(this.val$intent);
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getConfig().edit().remove(v19_2).commit();
                } catch (android.content.pm.PackageManager$NameNotFoundException v8_7) {
                    v8_7.printStackTrace();
                }
                if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.su) {
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.api >= 21) {
                        if (new java.io.File(new StringBuilder().append("/data/app/").append(v19_2).append("-1/arm").toString()).exists()) {
                            com.chelpus.Utils.run_all(new StringBuilder().append("rm -rf /data/app/").append(v19_2).append("-1").toString());
                        }
                        if (new java.io.File(new StringBuilder().append("/data/app/").append(v19_2).append("-2/arm").toString()).exists()) {
                            com.chelpus.Utils.run_all(new StringBuilder().append("rm -rf /data/app/").append(v19_2).append("-2").toString());
                        }
                        if (new java.io.File(new StringBuilder().append("/data/app/").append(v19_2).append("-3/arm").toString()).exists()) {
                            com.chelpus.Utils.run_all(new StringBuilder().append("rm -rf /data/app/").append(v19_2).append("-3").toString());
                        }
                        if (new java.io.File(new StringBuilder().append("/data/app/").append(v19_2).append("-4/arm").toString()).exists()) {
                            com.chelpus.Utils.run_all(new StringBuilder().append("rm -rf /data/app/").append(v19_2).append("-4").toString());
                        }
                        if (new java.io.File(new StringBuilder().append("/data/app/").append(v19_2).append("-1/x86").toString()).exists()) {
                            com.chelpus.Utils.run_all(new StringBuilder().append("rm -rf /data/app/").append(v19_2).append("-1").toString());
                        }
                        if (new java.io.File(new StringBuilder().append("/data/app/").append(v19_2).append("-2/x86").toString()).exists()) {
                            com.chelpus.Utils.run_all(new StringBuilder().append("rm -rf /data/app/").append(v19_2).append("-2").toString());
                        }
                        if (new java.io.File(new StringBuilder().append("/data/app/").append(v19_2).append("-3/x86").toString()).exists()) {
                            com.chelpus.Utils.run_all(new StringBuilder().append("rm -rf /data/app/").append(v19_2).append("-3").toString());
                        }
                        if (new java.io.File(new StringBuilder().append("/data/app/").append(v19_2).append("-4/x86").toString()).exists()) {
                            com.chelpus.Utils.run_all(new StringBuilder().append("rm -rf /data/app/").append(v19_2).append("-4").toString());
                        }
                    }
                    if (new java.io.File(new StringBuilder().append("/data/app/").append(v19_2).append("-1.odex").toString()).exists()) {
                        com.chelpus.Utils.run_all(new StringBuilder().append("rm /data/app/").append(v19_2).append("-1.odex").toString());
                    }
                    if (new java.io.File(new StringBuilder().append("/data/app/").append(v19_2).append("-2.odex").toString()).exists()) {
                        com.chelpus.Utils.run_all(new StringBuilder().append("rm /data/app/").append(v19_2).append("-2.odex").toString());
                    }
                    if (new java.io.File(new StringBuilder().append("/data/app/").append(v19_2).append(".odex").toString()).exists()) {
                        com.chelpus.Utils.run_all(new StringBuilder().append("rm /data/app/").append(v19_2).append(".odex").toString());
                    }
                }
                if (v19_2.contains("com.android.vending")) {
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getComponentEnabledSetting(new android.content.ComponentName(this.val$ctx, com.google.android.finsky.services.LicensingService)) != 2) {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), com.google.android.finsky.services.LicensingService), 1, 1);
                    } else {
                        com.chelpus.Utils.market_licensing_services(1);
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), com.google.android.finsky.services.LicensingService), 2, 1);
                    }
                    if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getComponentEnabledSetting(new android.content.ComponentName(this.val$ctx, com.google.android.finsky.billing.iab.MarketBillingService)) != 2) && (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getComponentEnabledSetting(new android.content.ComponentName(this.val$ctx, com.google.android.finsky.billing.iab.InAppBillingService)) != 2)) {
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), com.google.android.finsky.billing.iab.MarketBillingService), 1, 1);
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), com.google.android.finsky.billing.iab.InAppBillingService), 1, 1);
                    } else {
                        com.chelpus.Utils.market_billing_services(1);
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), com.google.android.finsky.billing.iab.MarketBillingService), 2, 1);
                        com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().setComponentEnabledSetting(new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), com.google.android.finsky.billing.iab.InAppBillingService), 2, 1);
                    }
                }
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.refresh = 1;
                String v18 = this.this$0.getPackageName(this.val$intent);
                if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.database == null) {
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.database = new com.android.vending.billing.InAppBillingService.LUCK.DatabaseHelper(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance());
                }
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.database.deletePackage(v18);
                if ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.desktop_launch) && ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.plia != null) && ((com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag != null) && (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getActivity() != null)))) {
                    android.support.v4.app.FragmentActivity v24_406 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.frag.getActivity();
                    com.android.vending.billing.InAppBillingService.LUCK.PackageChangeReceiver$1$9 v25_308 = new com.android.vending.billing.InAppBillingService.LUCK.PackageChangeReceiver$1$10;
                    v25_308(this, v18);
                    v24_406.runOnUiThread(v25_308);
                }
            }
        }
        if ((!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.patchOnBoot) && (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.desktop_launch)) {
            System.out.println("LP - exit.");
        }
        return;
    }
}
