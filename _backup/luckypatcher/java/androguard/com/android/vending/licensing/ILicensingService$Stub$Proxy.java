package com.android.vending.licensing;
 class ILicensingService$Stub$Proxy implements com.android.vending.licensing.ILicensingService {
    private android.os.IBinder mRemote;

    ILicensingService$Stub$Proxy(android.os.IBinder p1)
    {
        this.mRemote = p1;
        return;
    }

    public android.os.IBinder asBinder()
    {
        return this.mRemote;
    }

    public void checkLicense(long p6, String p8, com.android.vending.licensing.ILicenseResultListener p9)
    {
        android.os.IBinder v1_0 = 0;
        android.os.Parcel v0 = android.os.Parcel.obtain();
        try {
            v0.writeInterfaceToken("com.android.vending.licensing.ILicensingService");
            v0.writeLong(p6);
            v0.writeString(p8);
        } catch (android.os.IBinder v1_1) {
            v0.recycle();
            throw v1_1;
        }
        if (p9 != null) {
            v1_0 = p9.asBinder();
        }
        v0.writeStrongBinder(v1_0);
        this.mRemote.transact(1, v0, 0, 1);
        v0.recycle();
        return;
    }

    public String getInterfaceDescriptor()
    {
        return "com.android.vending.licensing.ILicensingService";
    }
}
