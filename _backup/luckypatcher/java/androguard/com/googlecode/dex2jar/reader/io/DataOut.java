package com.googlecode.dex2jar.reader.io;
public interface DataOut {

    public abstract void writeByte();

    public abstract void writeBytes();

    public abstract void writeInt();

    public abstract void writeShort();
}
