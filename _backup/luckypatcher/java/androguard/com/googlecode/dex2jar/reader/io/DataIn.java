package com.googlecode.dex2jar.reader.io;
public interface DataIn {

    public abstract int getCurrentPosition();

    public abstract void move();

    public abstract void pop();

    public abstract void push();

    public abstract void pushMove();

    public abstract int readByte();

    public abstract byte[] readBytes();

    public abstract int readIntx();

    public abstract long readLeb128();

    public abstract int readShortx();

    public abstract int readUByte();

    public abstract int readUIntx();

    public abstract long readULeb128();

    public abstract int readUShortx();

    public abstract void skip();
}
