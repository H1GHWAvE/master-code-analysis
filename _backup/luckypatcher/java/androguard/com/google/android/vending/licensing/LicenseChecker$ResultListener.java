package com.google.android.vending.licensing;
public class LicenseChecker$ResultListener extends com.google.android.vending.licensing.ILicenseResultListener$Stub {
    private static final int ERROR_CONTACTING_SERVER = 257;
    private static final int ERROR_INVALID_PACKAGE_NAME = 258;
    private static final int ERROR_NON_MATCHING_UID = 259;
    private Runnable mOnTimeout;
    private final com.google.android.vending.licensing.LicenseValidator mValidator;
    final synthetic com.google.android.vending.licensing.LicenseChecker this$0;

    public LicenseChecker$ResultListener(com.google.android.vending.licensing.LicenseChecker p2, com.google.android.vending.licensing.LicenseValidator p3)
    {
        this.this$0 = p2;
        this.mValidator = p3;
        this.mOnTimeout = new com.google.android.vending.licensing.LicenseChecker$ResultListener$1(this, p2);
        this.startTimeout();
        return;
    }

    static synthetic com.google.android.vending.licensing.LicenseValidator access$000(com.google.android.vending.licensing.LicenseChecker$ResultListener p1)
    {
        return p1.mValidator;
    }

    static synthetic void access$400(com.google.android.vending.licensing.LicenseChecker$ResultListener p0)
    {
        p0.clearTimeout();
        return;
    }

    private void clearTimeout()
    {
        android.util.Log.i("LicenseChecker", "Clearing timeout.");
        com.google.android.vending.licensing.LicenseChecker.access$600(this.this$0).removeCallbacks(this.mOnTimeout);
        return;
    }

    private void startTimeout()
    {
        android.util.Log.i("LicenseChecker", "Start monitoring timeout.");
        com.google.android.vending.licensing.LicenseChecker.access$600(this.this$0).postDelayed(this.mOnTimeout, 10000);
        return;
    }

    public void verifyLicense(int p3, String p4, String p5)
    {
        this.this$0.signedData = p4;
        this.this$0.signature = p5;
        this.this$0.responseCode = p3;
        com.google.android.vending.licensing.LicenseChecker.access$600(this.this$0).post(new com.google.android.vending.licensing.LicenseChecker$ResultListener$2(this, p3, p4, p5));
        return;
    }
}
