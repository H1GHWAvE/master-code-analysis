package com.google.android.vending.licensing;
public interface DeviceLimiter {

    public abstract int isDeviceAllowed();
}
