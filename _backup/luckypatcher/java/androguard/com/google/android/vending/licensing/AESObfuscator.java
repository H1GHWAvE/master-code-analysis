package com.google.android.vending.licensing;
public class AESObfuscator implements com.google.android.vending.licensing.Obfuscator {
    private static final String CIPHER_ALGORITHM = "AES/CBC/PKCS5Padding";
    public static byte[] IV = None;
    private static final String KEYGEN_ALGORITHM = "PBEWITHSHAAND256BITAES-CBC-BC";
    private static final String UTF8 = "UTF-8";
    public static String header;
    private javax.crypto.Cipher mDecryptor;
    private javax.crypto.Cipher mEncryptor;

    static AESObfuscator()
    {
        String v0_1 = new byte[16];
        v0_1 = {16, 74, 71, -80, 32, 101, -47, 72, 117, -14, 0, -29, 70, 65, -12, 74};
        com.google.android.vending.licensing.AESObfuscator.IV = v0_1;
        com.google.android.vending.licensing.AESObfuscator.header = "com.android.vending.licensing.AESObfuscator-1|";
        return;
    }

    public AESObfuscator(byte[] p10, String p11, String p12)
    {
        try {
            javax.crypto.spec.SecretKeySpec v3_1 = new javax.crypto.spec.SecretKeySpec(javax.crypto.SecretKeyFactory.getInstance("PBEWITHSHAAND256BITAES-CBC-BC").generateSecret(new javax.crypto.spec.PBEKeySpec(new StringBuilder().append(p11).append(p12).toString().toCharArray(), p10, 1024, 256)).getEncoded(), "AES");
            this.mEncryptor = javax.crypto.Cipher.getInstance("AES/CBC/PKCS5Padding");
            this.mEncryptor.init(1, v3_1, new javax.crypto.spec.IvParameterSpec(com.google.android.vending.licensing.AESObfuscator.IV));
            this.mDecryptor = javax.crypto.Cipher.getInstance("AES/CBC/PKCS5Padding");
            this.mDecryptor.init(2, v3_1, new javax.crypto.spec.IvParameterSpec(com.google.android.vending.licensing.AESObfuscator.IV));
            return;
        } catch (java.security.GeneralSecurityException v0) {
            throw new RuntimeException("Invalid environment", v0);
        }
    }

    public String obfuscate(String p5, String p6)
    {
        RuntimeException v1_2;
        if (p5 != null) {
            try {
                v1_2 = com.google.android.vending.licensing.util.Base64.encode(this.mEncryptor.doFinal(new StringBuilder().append(com.google.android.vending.licensing.AESObfuscator.header).append(p6).append(p5).toString().getBytes("UTF-8")));
            } catch (java.security.GeneralSecurityException v0_1) {
                throw new RuntimeException("Invalid environment", v0_1);
            } catch (java.security.GeneralSecurityException v0_0) {
                throw new RuntimeException("Invalid environment", v0_0);
            }
        } else {
            v1_2 = 0;
        }
        return v1_2;
    }

    public String unobfuscate(String p7, String p8)
    {
        String v3_10;
        if (p7 != null) {
            try {
                String v2_1 = new String(this.mDecryptor.doFinal(com.google.android.vending.licensing.util.Base64.decode(p7)), "UTF-8");
            } catch (java.io.UnsupportedEncodingException v0_0) {
                throw new RuntimeException("Invalid environment", v0_0);
            } catch (java.io.UnsupportedEncodingException v0_2) {
                throw new com.google.android.vending.licensing.ValidationException(new StringBuilder().append(v0_2.getMessage()).append(":").append(p7).toString());
            } catch (java.io.UnsupportedEncodingException v0_3) {
                throw new com.google.android.vending.licensing.ValidationException(new StringBuilder().append(v0_3.getMessage()).append(":").append(p7).toString());
            } catch (java.io.UnsupportedEncodingException v0_1) {
                throw new com.google.android.vending.licensing.ValidationException(new StringBuilder().append(v0_1.getMessage()).append(":").append(p7).toString());
            }
            if (v2_1.indexOf(new StringBuilder().append(com.google.android.vending.licensing.AESObfuscator.header).append(p8).toString()) == 0) {
                v3_10 = v2_1.substring((com.google.android.vending.licensing.AESObfuscator.header.length() + p8.length()), v2_1.length());
            } else {
                throw new com.google.android.vending.licensing.ValidationException(new StringBuilder().append("Header not found (invalid data or key):").append(p7).toString());
            }
        } else {
            v3_10 = 0;
        }
        return v3_10;
    }
}
