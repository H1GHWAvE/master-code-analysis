package com.google.android.vending.licensing;
public abstract class ILicenseResultListener$Stub extends android.os.Binder implements com.google.android.vending.licensing.ILicenseResultListener {
    private static final String DESCRIPTOR = "com.android.vending.licensing.ILicenseResultListener";
    static final int TRANSACTION_verifyLicense = 1;

    public ILicenseResultListener$Stub()
    {
        this.attachInterface(this, "com.android.vending.licensing.ILicenseResultListener");
        return;
    }

    public static com.google.android.vending.licensing.ILicenseResultListener asInterface(android.os.IBinder p2)
    {
        com.google.android.vending.licensing.ILicenseResultListener v0_2;
        if (p2 != null) {
            com.google.android.vending.licensing.ILicenseResultListener v0_0 = p2.queryLocalInterface("com.android.vending.licensing.ILicenseResultListener");
            if ((v0_0 == null) || (!(v0_0 instanceof com.google.android.vending.licensing.ILicenseResultListener))) {
                v0_2 = new com.google.android.vending.licensing.ILicenseResultListener$Stub$Proxy(p2);
            } else {
                v0_2 = ((com.google.android.vending.licensing.ILicenseResultListener) v0_0);
            }
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    public android.os.IBinder asBinder()
    {
        return this;
    }

    public boolean onTransact(int p6, android.os.Parcel p7, android.os.Parcel p8, int p9)
    {
        boolean v3 = 1;
        switch (p6) {
            case 1:
                p7.enforceInterface("com.android.vending.licensing.ILicenseResultListener");
                this.verifyLicense(p7.readInt(), p7.readString(), p7.readString());
                break;
            case 1598968902:
                p8.writeString("com.android.vending.licensing.ILicenseResultListener");
                break;
            default:
                v3 = super.onTransact(p6, p7, p8, p9);
        }
        return v3;
    }
}
