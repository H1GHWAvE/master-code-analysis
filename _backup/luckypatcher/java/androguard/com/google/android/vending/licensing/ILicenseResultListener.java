package com.google.android.vending.licensing;
public interface ILicenseResultListener implements android.os.IInterface {

    public abstract void verifyLicense();
}
