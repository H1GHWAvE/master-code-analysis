package com.google.android.finsky.billing.iab.google.util;
public class Inventory {
    java.util.Map mPurchaseMap;
    java.util.Map mSkuMap;

    Inventory()
    {
        this.mSkuMap = new java.util.HashMap();
        this.mPurchaseMap = new java.util.HashMap();
        return;
    }

    void addPurchase(com.google.android.finsky.billing.iab.google.util.Purchase p3)
    {
        this.mPurchaseMap.put(p3.getSku(), p3);
        return;
    }

    void addSkuDetails(com.google.android.finsky.billing.iab.google.util.SkuDetails p3)
    {
        this.mSkuMap.put(p3.getSku(), p3);
        return;
    }

    public void erasePurchase(String p2)
    {
        if (this.mPurchaseMap.containsKey(p2)) {
            this.mPurchaseMap.remove(p2);
        }
        return;
    }

    java.util.List getAllOwnedSkus()
    {
        return new java.util.ArrayList(this.mPurchaseMap.keySet());
    }

    java.util.List getAllOwnedSkus(String p5)
    {
        java.util.ArrayList v1_1 = new java.util.ArrayList();
        java.util.Iterator v2_2 = this.mPurchaseMap.values().iterator();
        while (v2_2.hasNext()) {
            com.google.android.finsky.billing.iab.google.util.Purchase v0_1 = ((com.google.android.finsky.billing.iab.google.util.Purchase) v2_2.next());
            if (v0_1.getItemType().equals(p5)) {
                v1_1.add(v0_1.getSku());
            }
        }
        return v1_1;
    }

    java.util.List getAllPurchases()
    {
        return new java.util.ArrayList(this.mPurchaseMap.values());
    }

    public com.google.android.finsky.billing.iab.google.util.Purchase getPurchase(String p2)
    {
        return ((com.google.android.finsky.billing.iab.google.util.Purchase) this.mPurchaseMap.get(p2));
    }

    public com.google.android.finsky.billing.iab.google.util.SkuDetails getSkuDetails(String p2)
    {
        return ((com.google.android.finsky.billing.iab.google.util.SkuDetails) this.mSkuMap.get(p2));
    }

    public boolean hasDetails(String p2)
    {
        return this.mSkuMap.containsKey(p2);
    }

    public boolean hasPurchase(String p2)
    {
        return this.mPurchaseMap.containsKey(p2);
    }
}
