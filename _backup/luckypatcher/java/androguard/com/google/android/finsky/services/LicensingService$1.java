package com.google.android.finsky.services;
 class LicensingService$1 implements android.content.ServiceConnection {
    final synthetic com.google.android.finsky.services.LicensingService this$0;
    final synthetic long val$nonce;
    final synthetic String val$pkgName;

    LicensingService$1(com.google.android.finsky.services.LicensingService p1, String p2, long p3)
    {
        this.this$0 = p1;
        this.val$pkgName = p2;
        this.val$nonce = p3;
        return;
    }

    public void onServiceConnected(android.content.ComponentName p7, android.os.IBinder p8)
    {
        System.out.println("Licensing service try to connect.");
        this.this$0.mService = com.android.vending.licensing.ILicensingService$Stub.asInterface(p8);
        try {
            System.out.println(new StringBuilder().append("Calling checkLicense on service for ").append(this.val$pkgName).toString());
            this.this$0.mService.checkLicense(this.val$nonce, this.val$pkgName, new com.google.android.finsky.services.LicensingService$1$1(this));
        } catch (android.os.RemoteException v0) {
            v0.printStackTrace();
            this.this$0.mSetupDone = 1;
        }
        return;
    }

    public void onServiceDisconnected(android.content.ComponentName p3)
    {
        System.out.println("Licensing service disconnected.");
        this.this$0.mService = 0;
        return;
    }
}
