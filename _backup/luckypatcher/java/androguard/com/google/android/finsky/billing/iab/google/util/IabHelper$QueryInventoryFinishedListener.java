package com.google.android.finsky.billing.iab.google.util;
public interface IabHelper$QueryInventoryFinishedListener {

    public abstract void onQueryInventoryFinished();
}
