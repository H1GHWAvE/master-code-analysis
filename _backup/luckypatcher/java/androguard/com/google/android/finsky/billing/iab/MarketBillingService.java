package com.google.android.finsky.billing.iab;
public class MarketBillingService extends android.app.Service {
    public static final String TAG = "BillingHack";
    private static android.content.Context context;
    private static String dev_pay;
    private static String item;
    private static java.util.Random sRandom;
    private final com.android.vending.billing.IMarketBillingService$Stub mBinder;
    com.google.android.finsky.billing.iab.MarketBillingService$BillingNotifier mNotifier;
    android.content.pm.PackageManager mPackageManager;

    static MarketBillingService()
    {
        com.google.android.finsky.billing.iab.MarketBillingService.item = "";
        com.google.android.finsky.billing.iab.MarketBillingService.dev_pay = "";
        com.google.android.finsky.billing.iab.MarketBillingService.sRandom = new java.util.Random();
        com.google.android.finsky.billing.iab.MarketBillingService.sRandom = new java.util.Random();
        return;
    }

    public MarketBillingService()
    {
        this.mBinder = new com.google.android.finsky.billing.iab.MarketBillingService$1(this);
        this.mNotifier = new com.google.android.finsky.billing.iab.MarketBillingService$BillingNotifier(this, this);
        return;
    }

    static synthetic String access$000()
    {
        return com.google.android.finsky.billing.iab.MarketBillingService.item;
    }

    static synthetic String access$002(String p0)
    {
        com.google.android.finsky.billing.iab.MarketBillingService.item = p0;
        return p0;
    }

    static synthetic String access$100()
    {
        return com.google.android.finsky.billing.iab.MarketBillingService.dev_pay;
    }

    static synthetic String access$102(String p0)
    {
        com.google.android.finsky.billing.iab.MarketBillingService.dev_pay = p0;
        return p0;
    }

    static synthetic java.util.Random access$200()
    {
        return com.google.android.finsky.billing.iab.MarketBillingService.sRandom;
    }

    public static android.content.Intent findReceiverName(android.content.Context p7, String p8, android.content.Intent p9)
    {
        java.util.ListIterator v0 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().queryBroadcastReceivers(p9, 0).listIterator();
        while (v0.hasNext()) {
            android.content.pm.ResolveInfo v1_0 = v0.next();
            if ((((android.content.pm.ResolveInfo) v1_0).activityInfo != null) && (p8.equals(((android.content.pm.ResolveInfo) v1_0).activityInfo.packageName))) {
                p9.setClassName(p8, ((android.content.pm.ResolveInfo) v1_0).activityInfo.name);
                p9.setPackage(p8);
                android.content.Intent v3 = p9;
            }
            return v3;
        }
        System.out.println(new StringBuilder().append("Cannot find billing receiver in package \'").append(p8).append("\' for action: ").append(p9.getAction()).toString());
        v3 = 0;
        return v3;
    }

    public static boolean sendResponseCode(android.content.Context p5, String p6, long p7, int p9)
    {
        int v1;
        android.content.Intent v0 = com.google.android.finsky.billing.iab.MarketBillingService.findReceiverName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance(), p6, new android.content.Intent("com.android.vending.billing.RESPONSE_CODE"));
        if (v0 != null) {
            v0.putExtra("request_id", p7);
            v0.putExtra("response_code", p9);
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().sendBroadcast(v0);
            v1 = 1;
        } else {
            v1 = 0;
        }
        return v1;
    }

    public android.os.IBinder onBind(android.content.Intent p2)
    {
        return this.mBinder;
    }

    public void onCreate()
    {
        super.onCreate();
        com.google.android.finsky.billing.iab.MarketBillingService.context = this;
        return;
    }
}
