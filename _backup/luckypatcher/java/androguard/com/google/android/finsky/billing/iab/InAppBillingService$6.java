package com.google.android.finsky.billing.iab;
 class InAppBillingService$6 extends com.google.android.finsky.billing.iab.google.util.IInAppBillingService$Stub {
    final java.util.ArrayList productIDinapp;
    final java.util.ArrayList productIDsubs;
    final synthetic com.google.android.finsky.billing.iab.InAppBillingService this$0;

    InAppBillingService$6(com.google.android.finsky.billing.iab.InAppBillingService p2)
    {
        this.this$0 = p2;
        this.productIDinapp = new java.util.ArrayList();
        this.productIDsubs = new java.util.ArrayList();
        return;
    }

    public int consumePurchase(int p3, String p4, String p5)
    {
        android.util.Log.d("BillingHack", "consumePurchase");
        return 0;
    }

    public android.os.Bundle getBuyIntent(int p11, String p12, String p13, String p14, String p15)
    {
        android.os.Bundle v1_1 = new android.os.Bundle();
        v1_1.putInt("RESPONSE_CODE", 0);
        android.content.Intent v3_1 = new android.content.Intent();
        v3_1.setClass(this.this$0.getApplicationContext(), com.google.android.finsky.billing.iab.BuyActivity);
        v3_1.setAction("com.google.android.finsky.billing.iab.BUY");
        v3_1.addFlags(3);
        v3_1.putExtra("packageName", p12);
        v3_1.putExtra("product", p13);
        v3_1.putExtra("payload", p15);
        v3_1.putExtra("Type", p14);
        java.util.ArrayList v4 = new com.google.android.finsky.billing.iab.DbHelper(this.this$0.mContext, p12).getItems();
        if (v4.size() != 0) {
            String v6_12 = v4.iterator();
            while (v6_12.hasNext()) {
                com.google.android.finsky.billing.iab.ItemsListItem v0_1 = ((com.google.android.finsky.billing.iab.ItemsListItem) v6_12.next());
                if ((v0_1.pData.equals("auto.repeat.LP")) && (v0_1.itemID.equals(p13))) {
                    v3_1.putExtra("autorepeat", v0_1.pSignature);
                }
            }
        }
        v1_1.putParcelable("BUY_INTENT", android.app.PendingIntent.getActivity(this.this$0.getApplicationContext(), 0, v3_1, 134217728));
        return v1_1;
    }

    public android.os.Bundle getBuyIntentToReplaceSkus(int p7, String p8, java.util.List p9, String p10, String p11, String p12)
    {
        System.out.println("LuckyPatcher: use api 5 getBuyIntentToReplaceSkus");
        android.os.Bundle v0_1 = new android.os.Bundle();
        v0_1.putInt("RESPONSE_CODE", 0);
        android.content.Intent v1_1 = new android.content.Intent();
        v1_1.setClass(this.this$0.getApplicationContext(), com.google.android.finsky.billing.iab.BuyActivity);
        v1_1.setAction("com.google.android.finsky.billing.iab.BUY");
        v1_1.addFlags(3);
        v1_1.putExtra("packageName", p8);
        v1_1.putExtra("product", p10);
        v1_1.putExtra("payload", p12);
        v1_1.putExtra("Type", p11);
        v0_1.putParcelable("BUY_INTENT", android.app.PendingIntent.getActivity(this.this$0.getApplicationContext(), 0, v1_1, 134217728));
        return v0_1;
    }

    public android.os.Bundle getPurchases(int p11, String p12, String p13, String p14)
    {
        android.util.Log.d("BillingHack", "getPurchases");
        java.util.ArrayList v5 = new com.google.android.finsky.billing.iab.DbHelper(this.this$0.mContext, p12).getItems();
        java.util.ArrayList v4_1 = new java.util.ArrayList();
        java.util.ArrayList v2_1 = new java.util.ArrayList();
        java.util.ArrayList v6_1 = new java.util.ArrayList();
        if (v5.size() != 0) {
            String v7_4 = v5.iterator();
            while (v7_4.hasNext()) {
                com.google.android.finsky.billing.iab.ItemsListItem v0_1 = ((com.google.android.finsky.billing.iab.ItemsListItem) v7_4.next());
                if (!v0_1.pData.equals("auto.repeat.LP")) {
                    v4_1.add(v0_1.itemID);
                    v2_1.add(v0_1.pData);
                    if (!v0_1.pSignature.equals("1")) {
                        v6_1.add("");
                    } else {
                        v6_1.add(com.chelpus.Utils.gen_sha1withrsa(v0_1.pData));
                    }
                }
            }
        }
        android.os.Bundle v1_1 = new android.os.Bundle();
        v1_1.putInt("RESPONSE_CODE", 0);
        v1_1.putStringArrayList("INAPP_PURCHASE_ITEM_LIST", v4_1);
        v1_1.putStringArrayList("INAPP_PURCHASE_DATA_LIST", v2_1);
        v1_1.putStringArrayList("INAPP_DATA_SIGNATURE_LIST", v6_1);
        return v1_1;
    }

    public android.os.Bundle getSkuDetails(int p31, String p32, String p33, android.os.Bundle p34)
    {
        android.util.Log.d("BillingHack", "getSkuDetails");
        try {
            if ((this.this$0.mSetupDone) || (this.this$0.skipSetupDone)) {
                new android.os.Bundle().putInt("RESPONSE_CODE", 0);
                if ((!this.this$0.mSetupDone) || ((this.this$0.mService == null) || (this.this$0.skipSetupDone))) {
                    java.util.ArrayList v24 = p34.getStringArrayList("ITEM_ID_LIST");
                    java.util.ArrayList v15_1 = new java.util.ArrayList();
                    if ((!this.this$0.mSetupDone) || ((this.this$0.mService == null) || (this.this$0.skipSetupDone))) {
                        System.out.println("Dont Connect to google billing");
                        java.util.Iterator v26_31 = v24.iterator();
                        while (v26_31.hasNext()) {
                            String v23_3 = ((String) v26_31.next());
                            if (p33.equals("subs")) {
                                int v9_2 = 0;
                                String v27_58 = this.productIDsubs.iterator();
                                while (v27_58.hasNext()) {
                                    if (v23_3.equals(((String) v27_58.next()))) {
                                        v9_2 = 1;
                                    }
                                    android.os.Bundle v6_3 = new android.os.Bundle();
                                    v6_3.putInt("RESPONSE_CODE", 0);
                                    v6_3.putStringArrayList("DETAILS_LIST", v15_1);
                                    android.os.Bundle v14 = v6_3;
                                }
                                if (v9_2 == 0) {
                                    this.productIDsubs.add(v23_3);
                                }
                            }
                            if (p33.equals("inapp")) {
                                int v9_3 = 0;
                                String v27_63 = this.productIDinapp.iterator();
                                while (v27_63.hasNext()) {
                                    if (v23_3.equals(((String) v27_63.next()))) {
                                        v9_3 = 1;
                                    }
                                }
                                if (v9_3 == 0) {
                                    this.productIDinapp.add(v23_3);
                                }
                            }
                        }
                        java.util.Iterator v26_32 = v24.iterator();
                        while (v26_32.hasNext()) {
                            String v23_1 = ((String) v26_32.next());
                            String[] v25 = v23_1.split("\\.");
                            String v13 = "";
                            if ((v25 == null) || (v25.length == 0)) {
                                v13 = v23_1.replaceAll("_", " ");
                            } else {
                                if (v25.length >= 2) {
                                    v13 = new StringBuilder().append(v25[(v25.length - 2)]).append(" ").append(v25[(v25.length - 1)]).toString();
                                }
                                if (v25.length == 1) {
                                    v13 = v25[0].replaceAll("_", " ");
                                }
                            }
                            String v18 = new StringBuilder().append("$0.").append(0).toString();
                            String v27_33 = new com.google.android.finsky.billing.iab.DbHelper(this.this$0.mContext, p32).getItems().iterator();
                            while (v27_33.hasNext()) {
                                if (((com.google.android.finsky.billing.iab.ItemsListItem) v27_33.next()).itemID.equals(v23_1)) {
                                    v18 = "Purchased";
                                }
                            }
                            if (!p33.equals("subs")) {
                                org.json.JSONObject v19_1 = new org.json.JSONObject();
                                try {
                                    v19_1.put("productId", v23_1);
                                    v19_1.put("type", p33);
                                    v19_1.put("price", v18);
                                    v19_1.put("title", v13);
                                    v19_1.put("description", v13);
                                    v19_1.put("price_amount_micros", (1000000 * 0));
                                    v19_1.put("price_currency_code", "USD");
                                } catch (org.json.JSONException v8_2) {
                                    v8_2.printStackTrace();
                                }
                                v15_1.add(v19_1.toString());
                            } else {
                                int v9_1 = 0;
                                String v27_45 = this.productIDinapp.iterator();
                                while (v27_45.hasNext()) {
                                    if (((String) v27_45.next()).equals(v23_1)) {
                                        v9_1 = 1;
                                    }
                                }
                                if (v9_1 != 0) {
                                    System.out.println(new StringBuilder().append("skip ").append(v23_1).append(" ").append(p33).toString());
                                } else {
                                    org.json.JSONObject v19_3 = new org.json.JSONObject();
                                    try {
                                        v19_3.put("productId", v23_1);
                                        v19_3.put("type", p33);
                                        v19_3.put("price", v18);
                                        v19_3.put("title", v13);
                                        v19_3.put("description", v13);
                                        v19_3.put("price_amount_micros", (1000000 * 0));
                                        v19_3.put("price_currency_code", "USD");
                                    } catch (org.json.JSONException v8_3) {
                                        v8_3.printStackTrace();
                                    }
                                    v15_1.add(v19_3.toString());
                                }
                            }
                        }
                    }
                } else {
                    try {
                        System.out.println("Connect to google billing");
                        v14 = this.this$0.mService.getSkuDetails(p31, p32, p33, p34);
                        java.util.ArrayList v21 = v14.getStringArrayList("DETAILS_LIST");
                        p34.getStringArrayList("ITEM_ID_LIST");
                        int v22 = v14.getInt("RESPONSE_CODE");
                        System.out.println(v22);
                    } catch (org.json.JSONException v8_0) {
                        v8_0.printStackTrace();
                    }
                    if (v22 == 0) {
                        if ((v21 != null) && (v21.size() != 0)) {
                            java.util.Iterator v26_21 = v21.iterator();
                            while (v26_21.hasNext()) {
                                org.json.JSONObject v12_1 = new org.json.JSONObject(((String) v26_21.next()));
                                if (v12_1.getString("type").equals("inapp")) {
                                    int v9_0 = 0;
                                    String v27_8 = this.productIDinapp.iterator();
                                    while (v27_8.hasNext()) {
                                        if (v12_1.get("productId").equals(((String) v27_8.next()))) {
                                            v9_0 = 1;
                                        }
                                    }
                                    if (v9_0 == 0) {
                                        this.productIDinapp.add(v12_1.getString("productId"));
                                    }
                                }
                            }
                        }
                    } else {
                        this.this$0.skipSetupDone = 1;
                    }
                }
            } else {
                this.this$0.connectToBilling(p32);
            }
        } catch (org.json.JSONException v8_1) {
            v8_1.printStackTrace();
        }
        return v14;
    }

    public int isBillingSupported(int p3, String p4, String p5)
    {
        android.util.Log.d("BillingHack", "isBillingSupported");
        this.this$0.startGoogleBilling();
        return 0;
    }
}
