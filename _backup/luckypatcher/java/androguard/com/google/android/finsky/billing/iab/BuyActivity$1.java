package com.google.android.finsky.billing.iab;
 class BuyActivity$1 implements android.view.View$OnClickListener {
    final synthetic com.google.android.finsky.billing.iab.BuyActivity this$0;
    final synthetic android.widget.CheckBox val$check;
    final synthetic android.widget.CheckBox val$check2;
    final synthetic android.widget.CheckBox val$check3;
    final synthetic String val$pData;

    BuyActivity$1(com.google.android.finsky.billing.iab.BuyActivity p1, android.widget.CheckBox p2, String p3, android.widget.CheckBox p4, android.widget.CheckBox p5)
    {
        this.this$0 = p1;
        this.val$check = p2;
        this.val$pData = p3;
        this.val$check2 = p4;
        this.val$check3 = p5;
        return;
    }

    public void onClick(android.view.View p9)
    {
        String v3;
        String v4;
        android.content.Intent v0_1 = new android.content.Intent();
        android.os.Bundle v2_1 = new android.os.Bundle();
        if (this.val$check.isChecked()) {
            v3 = "";
            v4 = "";
        } else {
            v4 = com.chelpus.Utils.gen_sha1withrsa(this.val$pData);
            v3 = "1";
        }
        if (this.val$check2.isChecked()) {
            new com.google.android.finsky.billing.iab.DbHelper(this.this$0.getApplicationContext(), this.this$0.packageName).saveItem(new com.google.android.finsky.billing.iab.ItemsListItem(this.this$0.productId, this.val$pData, v3));
        }
        if (this.val$check3.isChecked()) {
            new com.google.android.finsky.billing.iab.DbHelper(this.this$0.getApplicationContext(), this.this$0.packageName).saveItem(new com.google.android.finsky.billing.iab.ItemsListItem(this.this$0.productId, "auto.repeat.LP", v3));
        }
        v2_1.putInt("RESPONSE_CODE", 0);
        v2_1.putString("INAPP_PURCHASE_DATA", this.val$pData);
        v2_1.putString("INAPP_DATA_SIGNATURE", v4);
        v0_1.putExtras(v2_1);
        this.this$0.setResult(-1, v0_1);
        this.this$0.finish();
        return;
    }
}
