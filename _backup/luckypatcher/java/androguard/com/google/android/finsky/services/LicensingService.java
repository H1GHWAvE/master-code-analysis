package com.google.android.finsky.services;
public class LicensingService extends android.app.Service {
    static android.content.ServiceConnection mServiceConn;
    private final com.android.vending.licensing.ILicensingService$Stub mBinder;
    public com.google.android.vending.licensing.LicenseChecker mChecker;
    private com.android.vending.licensing.ILicenseResultListener mListener;
    com.android.vending.licensing.ILicensingService mService;
    boolean mSetupDone;
    public int responseCode;
    public String signature;
    public String signedData;

    public LicensingService()
    {
        this.mSetupDone = 0;
        this.responseCode = 255;
        this.signature = "";
        this.signedData = "";
        this.mBinder = new com.google.android.finsky.services.LicensingService$2(this);
        return;
    }

    private void cleanupService()
    {
        if (this.mService != null) {
            try {
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().unbindService(com.google.android.finsky.services.LicensingService.mServiceConn);
            } catch (int v0) {
            }
            this.mService = 0;
        }
        return;
    }

    public void connectToLicensing(long p18, String p20)
    {
        com.google.android.finsky.services.LicensingService.mServiceConn = new com.google.android.finsky.services.LicensingService$1(this, p20, p18);
        try {
            if (this.mService == null) {
                android.content.Intent v9_1 = new android.content.Intent(new String(com.google.android.vending.licensing.util.Base64.decode("Y29tLmFuZHJvaWQudmVuZGluZy5saWNlbnNpbmcuSUxpY2Vuc2luZ1NlcnZpY2U=")));
                v9_1.setPackage("com.android.vending");
                v9_1.putExtra("xexe", "lp");
                boolean v4 = 0;
                if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().queryIntentServices(v9_1, 0).isEmpty()) {
                    new com.chelpus.Utils("w").waitLP(5000);
                }
                if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().queryIntentServices(v9_1, 0).isEmpty()) {
                    java.io.PrintStream v13_16 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().queryIntentServices(v9_1, 0).iterator();
                    while (v13_16.hasNext()) {
                        android.content.pm.ResolveInfo v11_1 = ((android.content.pm.ResolveInfo) v13_16.next());
                        if ((v11_1.serviceInfo.packageName != null) && (v11_1.serviceInfo.packageName.equals("com.android.vending"))) {
                            android.content.ComponentName v6_1 = new android.content.ComponentName(v11_1.serviceInfo.packageName, v11_1.serviceInfo.name);
                            android.content.Intent v9_3 = new android.content.Intent();
                            v9_3.setComponent(v6_1);
                            v9_3.putExtra("xexe", "lp");
                            v4 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().bindService(v9_3, com.google.android.finsky.services.LicensingService.mServiceConn, 1);
                        }
                    }
                }
                if (!v4) {
                    this = this.cleanupService();
                } else {
                    int v8 = 0;
                    while (!this.mSetupDone) {
                        new com.chelpus.Utils("w").waitLP(2000);
                        if (v8 > 10) {
                            break;
                        }
                        v8++;
                    }
                    System.out.println("Stop licensing");
                    this = this.cleanupService();
                }
            }
        } catch (com.google.android.vending.licensing.util.Base64DecoderException v7) {
            this = this.cleanupService();
        } catch (com.google.android.vending.licensing.util.Base64DecoderException v7) {
            v7.printStackTrace();
            this = this.cleanupService();
        }
        return;
    }

    public android.os.IBinder onBind(android.content.Intent p2)
    {
        return this.mBinder;
    }
}
