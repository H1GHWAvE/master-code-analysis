package com.google.android.finsky.billing.iab.google.util;
public interface IabHelper$OnIabPurchaseFinishedListener {

    public abstract void onIabPurchaseFinished();
}
