package com.google.android.finsky.billing.iab;
 class InAppBillingService$5 implements android.content.ServiceConnection {
    final synthetic com.google.android.finsky.billing.iab.InAppBillingService this$0;
    final synthetic String val$pkgName;

    InAppBillingService$5(com.google.android.finsky.billing.iab.InAppBillingService p1, String p2)
    {
        this.this$0 = p1;
        this.val$pkgName = p2;
        return;
    }

    public void onServiceConnected(android.content.ComponentName p7, android.os.IBinder p8)
    {
        System.out.println("Billing service try to connect.");
        this.this$0.mService = com.google.android.finsky.billing.iab.google.util.IInAppBillingService$Stub.asInterface(p8);
        try {
            int v1_0 = this.this$0.mService.isBillingSupported(3, this.val$pkgName, "inapp");
        } catch (android.os.RemoteException v0) {
            v0.printStackTrace();
            return;
        }
        if (v1_0 == 0) {
            // Both branches of the condition point to the same code.
            // if (this.this$0.mService.isBillingSupported(3, this.val$pkgName, "subs") != 0) {
                System.out.println("Billing service connected.");
                this.this$0.mSetupDone = 1;
                return;
            // }
        } else {
            System.out.println(v1_0);
            this.this$0.skipSetupDone = 1;
            return;
        }
    }

    public void onServiceDisconnected(android.content.ComponentName p3)
    {
        System.out.println("Billing service disconnected.");
        this.this$0.mService = 0;
        this.this$0.mSetupDone = 0;
        return;
    }
}
