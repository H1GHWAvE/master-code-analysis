package com.google.android.finsky.billing.iab.google.util;
 class IabHelper$2 implements java.lang.Runnable {
    final synthetic com.google.android.finsky.billing.iab.google.util.IabHelper this$0;
    final synthetic android.os.Handler val$handler;
    final synthetic com.google.android.finsky.billing.iab.google.util.IabHelper$QueryInventoryFinishedListener val$listener;
    final synthetic java.util.List val$moreSkus;
    final synthetic boolean val$querySkuDetails;

    IabHelper$2(com.google.android.finsky.billing.iab.google.util.IabHelper p1, boolean p2, java.util.List p3, com.google.android.finsky.billing.iab.google.util.IabHelper$QueryInventoryFinishedListener p4, android.os.Handler p5)
    {
        this.this$0 = p1;
        this.val$querySkuDetails = p2;
        this.val$moreSkus = p3;
        this.val$listener = p4;
        this.val$handler = p5;
        return;
    }

    public void run()
    {
        com.google.android.finsky.billing.iab.google.util.IabResult v3_1 = new com.google.android.finsky.billing.iab.google.util.IabResult(0, "Inventory refresh successful.");
        try {
            com.google.android.finsky.billing.iab.google.util.Inventory v1 = this.this$0.queryInventory(this.val$querySkuDetails, this.val$moreSkus);
        } catch (com.google.android.finsky.billing.iab.google.util.IabException v0) {
            v3_1 = v0.getResult();
        }
        this.this$0.flagEndAsync();
        if ((!this.this$0.mDisposed) && (this.val$listener != null)) {
            this.val$handler.post(new com.google.android.finsky.billing.iab.google.util.IabHelper$2$1(this, v3_1, v1));
        }
        return;
    }
}
