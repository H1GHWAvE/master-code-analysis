package com.google.android.finsky.billing.iab.google.util;
 class IabHelper$3 implements java.lang.Runnable {
    final synthetic com.google.android.finsky.billing.iab.google.util.IabHelper this$0;
    final synthetic android.os.Handler val$handler;
    final synthetic com.google.android.finsky.billing.iab.google.util.IabHelper$OnConsumeMultiFinishedListener val$multiListener;
    final synthetic java.util.List val$purchases;
    final synthetic com.google.android.finsky.billing.iab.google.util.IabHelper$OnConsumeFinishedListener val$singleListener;

    IabHelper$3(com.google.android.finsky.billing.iab.google.util.IabHelper p1, java.util.List p2, com.google.android.finsky.billing.iab.google.util.IabHelper$OnConsumeFinishedListener p3, android.os.Handler p4, com.google.android.finsky.billing.iab.google.util.IabHelper$OnConsumeMultiFinishedListener p5)
    {
        this.this$0 = p1;
        this.val$purchases = p2;
        this.val$singleListener = p3;
        this.val$handler = p4;
        this.val$multiListener = p5;
        return;
    }

    public void run()
    {
        java.util.ArrayList v2_1 = new java.util.ArrayList();
        android.os.Handler v3_1 = this.val$purchases.iterator();
        while (v3_1.hasNext()) {
            com.google.android.finsky.billing.iab.google.util.Purchase v1_1 = ((com.google.android.finsky.billing.iab.google.util.Purchase) v3_1.next());
            try {
                this.this$0.consume(v1_1);
                v2_1.add(new com.google.android.finsky.billing.iab.google.util.IabResult(0, new StringBuilder().append("Successful consume of sku ").append(v1_1.getSku()).toString()));
            } catch (com.google.android.finsky.billing.iab.google.util.IabException v0) {
                v2_1.add(v0.getResult());
            }
        }
        this.this$0.flagEndAsync();
        if ((!this.this$0.mDisposed) && (this.val$singleListener != null)) {
            this.val$handler.post(new com.google.android.finsky.billing.iab.google.util.IabHelper$3$1(this, v2_1));
        }
        if ((!this.this$0.mDisposed) && (this.val$multiListener != null)) {
            this.val$handler.post(new com.google.android.finsky.billing.iab.google.util.IabHelper$3$2(this, v2_1));
        }
        return;
    }
}
