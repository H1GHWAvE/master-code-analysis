package com.google.android.finsky.billing.iab.google.util;
 class IabHelper$1 implements android.content.ServiceConnection {
    final synthetic com.google.android.finsky.billing.iab.google.util.IabHelper this$0;
    final synthetic com.google.android.finsky.billing.iab.google.util.IabHelper$OnIabSetupFinishedListener val$listener;

    IabHelper$1(com.google.android.finsky.billing.iab.google.util.IabHelper p1, com.google.android.finsky.billing.iab.google.util.IabHelper$OnIabSetupFinishedListener p2)
    {
        this.this$0 = p1;
        this.val$listener = p2;
        return;
    }

    public void onServiceConnected(android.content.ComponentName p8, android.os.IBinder p9)
    {
        if (!this.this$0.mDisposed) {
            this.this$0.logDebug("Billing service connected.");
            this.this$0.mService = com.google.android.finsky.billing.iab.google.util.IInAppBillingService$Stub.asInterface(p9);
            String v1 = this.this$0.mContext.getPackageName();
            try {
                this.this$0.logDebug("Checking for in-app billing 3 support.");
                int v2_0 = this.this$0.mService.isBillingSupported(3, v1, "inapp");
            } catch (android.os.RemoteException v0) {
                if (this.val$listener != null) {
                    this.val$listener.onIabSetupFinished(new com.google.android.finsky.billing.iab.google.util.IabResult(-1001, "RemoteException while setting up in-app billing."));
                }
                v0.printStackTrace();
            }
            if (v2_0 == 0) {
                this.this$0.logDebug(new StringBuilder().append("In-app billing version 3 supported for ").append(v1).toString());
                int v2_1 = this.this$0.mService.isBillingSupported(3, v1, "subs");
                if (v2_1 != 0) {
                    this.this$0.logDebug(new StringBuilder().append("Subscriptions NOT AVAILABLE. Response: ").append(v2_1).toString());
                } else {
                    this.this$0.logDebug("Subscriptions AVAILABLE.");
                    this.this$0.mSubscriptionsSupported = 1;
                }
                this.this$0.mSetupDone = 1;
                if (this.val$listener != null) {
                    this.val$listener.onIabSetupFinished(new com.google.android.finsky.billing.iab.google.util.IabResult(0, "Setup successful."));
                }
            } else {
                if (this.val$listener != null) {
                    this.val$listener.onIabSetupFinished(new com.google.android.finsky.billing.iab.google.util.IabResult(v2_0, "Error checking for billing v3 support."));
                }
                this.this$0.mSubscriptionsSupported = 0;
            }
        }
        return;
    }

    public void onServiceDisconnected(android.content.ComponentName p3)
    {
        this.this$0.logDebug("Billing service disconnected.");
        this.this$0.mService = 0;
        return;
    }
}
