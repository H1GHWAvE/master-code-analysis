package com.google.android.finsky.billing.iab;
public class BuyActivity extends android.app.Activity {
    public static final String BUY_INTENT = "com.google.android.finsky.billing.iab.BUY";
    public static final String EXTRA_DEV_PAYLOAD = "payload";
    public static final String EXTRA_PACKAGENAME = "packageName";
    public static final String EXTRA_PRODUCT_ID = "product";
    public static final String TAG = "BillingHack";
    android.os.Bundle bundle;
    public com.google.android.finsky.billing.iab.BuyActivity context;
    String devPayload;
    String packageName;
    String productId;
    String type;

    public BuyActivity()
    {
        this.context = 0;
        this.packageName = "";
        this.productId = "";
        this.devPayload = "";
        this.type = "";
        this.bundle = 0;
        return;
    }

    public void onConfigurationChanged(android.content.res.Configuration p3)
    {
        if (p3.orientation != 2) {
            this.setRequestedOrientation(1);
        } else {
            this.setRequestedOrientation(0);
        }
        super.onConfigurationChanged(p3);
        return;
    }

    protected void onCreate(android.os.Bundle p24)
    {
        this = super.onCreate(p24);
        this.context = this;
        if ((!"com.google.android.finsky.billing.iab.BUY".equals(this.getIntent().getAction())) && (p24 == null)) {
            this.finish();
        } else {
            android.util.Log.d("BillingHack", "Buy intent!");
            System.out.println(this.getRequestedOrientation());
            if (p24 == null) {
                this.packageName = this.getIntent().getExtras().getString("packageName");
                this.productId = this.getIntent().getExtras().getString("product");
                this.devPayload = this.getIntent().getExtras().getString("payload");
                this.type = this.getIntent().getExtras().getString("Type");
            } else {
                this.packageName = p24.getString("packageName");
                this.productId = p24.getString("product");
                this.devPayload = p24.getString("payload");
                this.type = p24.getString("Type");
            }
            System.out.println(this.type);
            org.json.JSONObject v14_1 = new org.json.JSONObject();
            try {
                v14_1.put("orderId", new StringBuilder().append((com.chelpus.Utils.getRandom(7.832953389245686e-242, nan) + com.chelpus.Utils.getRandom(0, 9))).append(".").append(com.chelpus.Utils.getRandom(4.940656458412465e-309, 5.431165199810527e-308)).toString());
                v14_1.put("packageName", this.packageName);
                v14_1.put("productId", this.productId);
                v14_1.put("purchaseTime", new Long(System.currentTimeMillis()));
                v14_1.put("purchaseState", new Integer(0));
                v14_1.put("developerPayload", this.devPayload);
                v14_1.put("purchaseToken", com.chelpus.Utils.getRandomStringLowerCase(24));
            } catch (org.json.JSONException v12) {
                v12.printStackTrace();
            }
            String v5 = v14_1.toString();
            String v8 = this.getIntent().getExtras().getString("autorepeat");
            if (v8 != null) {
                String v15;
                android.content.Intent v11_1 = new android.content.Intent();
                android.os.Bundle v13_1 = new android.os.Bundle();
                if (!v8.equals("1")) {
                    v15 = "";
                } else {
                    v15 = com.chelpus.Utils.gen_sha1withrsa(v5);
                }
                v13_1.putInt("RESPONSE_CODE", 0);
                v13_1.putString("INAPP_PURCHASE_DATA", v5);
                v13_1.putString("INAPP_DATA_SIGNATURE", v15);
                v11_1.putExtras(v13_1);
                this.setResult(-1, v11_1);
                this.finish();
            }
            this.setContentView(2130968594);
            android.widget.Button v10_1 = ((android.widget.Button) this.findViewById(2131558407));
            android.widget.Button v9_1 = ((android.widget.Button) this.findViewById(2131558408));
            android.widget.CheckBox v4_1 = ((android.widget.CheckBox) this.findViewById(2131558483));
            if ((!com.chelpus.Utils.checkCoreJarPatch11()) && (!com.chelpus.Utils.isRebuildedOrOdex(this.packageName, this))) {
                v4_1.setChecked(1);
            }
            android.widget.CheckBox v6_1 = ((android.widget.CheckBox) this.findViewById(2131558484));
            android.widget.CheckBox v7_1 = ((android.widget.CheckBox) this.findViewById(2131558485));
            android.widget.TextView v16_1 = ((android.widget.TextView) this.findViewById(2131558471));
            v16_1.setText(com.chelpus.Utils.getText(2131165222));
            v16_1.append(new StringBuilder().append("\n").append(com.chelpus.Utils.getText(2131165223)).toString());
            v6_1.setChecked(0);
            v7_1.setChecked(0);
            v10_1.setOnClickListener(new com.google.android.finsky.billing.iab.BuyActivity$1(this, v4_1, v5, v6_1, v7_1));
            v9_1.setOnClickListener(new com.google.android.finsky.billing.iab.BuyActivity$2(this));
        }
        return;
    }

    protected void onRestoreInstanceState(android.os.Bundle p3)
    {
        System.out.println("load instance");
        this.packageName = p3.getString("packageName");
        super.onRestoreInstanceState(p3);
        return;
    }

    protected void onSaveInstanceState(android.os.Bundle p3)
    {
        System.out.println("save instance");
        p3.putString("packageName", this.packageName);
        p3.putString("product", this.productId);
        p3.putString("payload", this.devPayload);
        p3.putString("Type", this.type);
        super.onSaveInstanceState(p3);
        return;
    }
}
