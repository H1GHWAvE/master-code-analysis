package com.google.android.finsky.billing.iab.google.util;
public abstract class IInAppBillingService$Stub extends android.os.Binder implements com.google.android.finsky.billing.iab.google.util.IInAppBillingService {
    private static final String DESCRIPTOR = "com.android.vending.billing.IInAppBillingService";
    static final int TRANSACTION_consumePurchase = 5;
    static final int TRANSACTION_getBuyIntent = 3;
    static final int TRANSACTION_getBuyIntentToReplaceSkus = 6;
    static final int TRANSACTION_getPurchases = 4;
    static final int TRANSACTION_getSkuDetails = 2;
    static final int TRANSACTION_isBillingSupported = 1;

    public IInAppBillingService$Stub()
    {
        this.attachInterface(this, "com.android.vending.billing.IInAppBillingService");
        return;
    }

    public static com.google.android.finsky.billing.iab.google.util.IInAppBillingService asInterface(android.os.IBinder p2)
    {
        com.google.android.finsky.billing.iab.google.util.IInAppBillingService v0_2;
        if (p2 != null) {
            com.google.android.finsky.billing.iab.google.util.IInAppBillingService v0_0 = p2.queryLocalInterface("com.android.vending.billing.IInAppBillingService");
            if ((v0_0 == null) || (!(v0_0 instanceof com.google.android.finsky.billing.iab.google.util.IInAppBillingService))) {
                v0_2 = new com.google.android.finsky.billing.iab.google.util.IInAppBillingService$Stub$Proxy(p2);
            } else {
                v0_2 = ((com.google.android.finsky.billing.iab.google.util.IInAppBillingService) v0_0);
            }
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    public android.os.IBinder asBinder()
    {
        return this;
    }

    public boolean onTransact(int p12, android.os.Parcel p13, android.os.Parcel p14, int p15)
    {
        int v0_1;
        switch (p12) {
            case 1:
                p13.enforceInterface("com.android.vending.billing.IInAppBillingService");
                android.os.Bundle v7_5 = this.isBillingSupported(p13.readInt(), p13.readString(), p13.readString());
                p14.writeNoException();
                p14.writeInt(v7_5);
                v0_1 = 1;
                break;
            case 2:
                int v4_3;
                p13.enforceInterface("com.android.vending.billing.IInAppBillingService");
                int v1_4 = p13.readInt();
                String v2_4 = p13.readString();
                java.util.ArrayList v3_4 = p13.readString();
                if (p13.readInt() == 0) {
                    v4_3 = 0;
                } else {
                    v4_3 = ((android.os.Bundle) android.os.Bundle.CREATOR.createFromParcel(p13));
                }
                android.os.Bundle v7_4 = this.getSkuDetails(v1_4, v2_4, v3_4, v4_3);
                p14.writeNoException();
                if (v7_4 == null) {
                    p14.writeInt(0);
                } else {
                    p14.writeInt(1);
                    v7_4.writeToParcel(p14, 1);
                }
                v0_1 = 1;
                break;
            case 3:
                p13.enforceInterface("com.android.vending.billing.IInAppBillingService");
                android.os.Bundle v7_3 = this.getBuyIntent(p13.readInt(), p13.readString(), p13.readString(), p13.readString(), p13.readString());
                p14.writeNoException();
                if (v7_3 == null) {
                    p14.writeInt(0);
                } else {
                    p14.writeInt(1);
                    v7_3.writeToParcel(p14, 1);
                }
                v0_1 = 1;
                break;
            case 4:
                p13.enforceInterface("com.android.vending.billing.IInAppBillingService");
                android.os.Bundle v7_2 = this.getPurchases(p13.readInt(), p13.readString(), p13.readString(), p13.readString());
                p14.writeNoException();
                if (v7_2 == null) {
                    p14.writeInt(0);
                } else {
                    p14.writeInt(1);
                    v7_2.writeToParcel(p14, 1);
                }
                v0_1 = 1;
                break;
            case 5:
                p13.enforceInterface("com.android.vending.billing.IInAppBillingService");
                android.os.Bundle v7_1 = this.consumePurchase(p13.readInt(), p13.readString(), p13.readString());
                p14.writeNoException();
                p14.writeInt(v7_1);
                v0_1 = 1;
                break;
            case 6:
                p13.enforceInterface("com.android.vending.billing.IInAppBillingService");
                android.os.Bundle v7_0 = this.getBuyIntentToReplaceSkus(p13.readInt(), p13.readString(), p13.readArrayList(this.getClass().getClassLoader()), p13.readString(), p13.readString(), p13.readString());
                p14.writeNoException();
                if (v7_0 == null) {
                    p14.writeInt(0);
                } else {
                    p14.writeInt(1);
                    v7_0.writeToParcel(p14, 1);
                }
                v0_1 = 1;
                break;
            case 1598968902:
                p14.writeString("com.android.vending.billing.IInAppBillingService");
                v0_1 = 1;
                break;
            default:
                v0_1 = super.onTransact(p12, p13, p14, p15);
        }
        return v0_1;
    }
}
