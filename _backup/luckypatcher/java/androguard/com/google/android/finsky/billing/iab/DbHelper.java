package com.google.android.finsky.billing.iab;
public class DbHelper extends android.database.sqlite.SQLiteOpenHelper {
    public static android.content.Context contextdb = None;
    static final String dbName = "BillingRestoreTransactions";
    public static boolean getPackage = False;
    static final String itemID = "itemID";
    static final String pData = "Data";
    static final String pSignature = "Signature";
    static String packageTable;
    public static boolean savePackage;

    static DbHelper()
    {
        com.google.android.finsky.billing.iab.DbHelper.packageTable = "Packages";
        com.google.android.finsky.billing.iab.DbHelper.contextdb = 0;
        com.google.android.finsky.billing.iab.DbHelper.getPackage = 0;
        com.google.android.finsky.billing.iab.DbHelper.savePackage = 0;
        return;
    }

    public DbHelper(android.content.Context p6)
    {
        this(p6, "BillingRestoreTransactions", 0, 41);
        com.google.android.finsky.billing.iab.DbHelper.contextdb = p6;
        com.google.android.finsky.billing.iab.DbHelper.packageTable = com.google.android.finsky.billing.iab.DbHelper.packageTable;
        try {
            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.billing_db != null) {
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.billing_db.close();
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.billing_db = this.getWritableDatabase();
                System.out.println(new StringBuilder().append("SQLite base version is ").append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.billing_db.getVersion()).toString());
                if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.billing_db.getVersion() != 41) {
                    System.out.println("SQL delete and recreate.");
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.billing_db.execSQL(new StringBuilder().append("DROP TABLE IF EXISTS ").append(com.google.android.finsky.billing.iab.DbHelper.packageTable).toString());
                    this.onCreate(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.billing_db);
                }
                this.onCreate(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.billing_db);
            } else {
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.billing_db = this.getWritableDatabase();
            }
        } catch (android.database.sqlite.SQLiteException v0) {
            v0.printStackTrace();
        }
        return;
    }

    public DbHelper(android.content.Context p5, String p6)
    {
        this(p5, "BillingRestoreTransactions", 0, 41);
        com.google.android.finsky.billing.iab.DbHelper.contextdb = p5;
        com.google.android.finsky.billing.iab.DbHelper.packageTable = p6;
        try {
            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.billing_db != null) {
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.billing_db.close();
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.billing_db = this.getWritableDatabase();
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.billing_db = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.billing_db;
                if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.billing_db.getVersion() != 41) {
                    System.out.println("SQL delete and recreate.");
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.billing_db.execSQL(new StringBuilder().append("DROP TABLE IF EXISTS ").append(p6).toString());
                    this.onCreate(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.billing_db);
                }
                this.onCreate(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.billing_db);
            } else {
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.billing_db = this.getWritableDatabase();
            }
        } catch (android.database.sqlite.SQLiteException v0) {
            v0.printStackTrace();
        }
        return;
    }

    public void deleteAll()
    {
        return;
    }

    public void deleteItem(com.google.android.finsky.billing.iab.ItemsListItem p6)
    {
        try {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.billing_db.delete(new StringBuilder().append("\'").append(com.google.android.finsky.billing.iab.DbHelper.packageTable).append("\'").toString(), new StringBuilder().append("itemID = \'").append(p6.itemID).append("\'").toString(), 0);
        } catch (Exception v0) {
            System.out.println(new StringBuilder().append("LuckyPatcher-Error: deletePackage ").append(v0).toString());
        }
        return;
    }

    public void deleteItem(String p6)
    {
        try {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.billing_db.delete(new StringBuilder().append("\'").append(com.google.android.finsky.billing.iab.DbHelper.packageTable).append("\'").toString(), new StringBuilder().append("itemID = \'").append(p6).append("\'").toString(), 0);
        } catch (Exception v0) {
            System.out.println(new StringBuilder().append("LuckyPatcher-Error: deletePackage ").append(v0).toString());
        }
        return;
    }

    public java.util.ArrayList getItems()
    {
        java.util.ArrayList v14_1 = new java.util.ArrayList();
        v14_1.clear();
        try {
            String v1_5 = new StringBuilder().append("\'").append(com.google.android.finsky.billing.iab.DbHelper.packageTable).append("\'").toString();
            String v2_4 = new String[3];
            v2_4[0] = "itemID";
            v2_4[1] = "Data";
            v2_4[2] = "Signature";
            android.database.Cursor v8 = com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.billing_db.query(v1_5, v2_4, 0, 0, 0, 0, 0);
            v8.moveToFirst();
            try {
                do {
                    v14_1.add(new com.google.android.finsky.billing.iab.ItemsListItem(v8.getString(v8.getColumnIndexOrThrow("itemID")), v8.getString(v8.getColumnIndex("Data")), v8.getString(v8.getColumnIndex("Signature"))));
                } while(
    public boolean isOpen()
    {
        int v0_2;
        if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.billing_db.isOpen()) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public void onCreate(android.database.sqlite.SQLiteDatabase p3)
    {
        p3.execSQL(new StringBuilder().append("CREATE TABLE IF NOT EXISTS \'").append(com.google.android.finsky.billing.iab.DbHelper.packageTable).append("\' (").append("itemID").append(" TEXT PRIMARY KEY, ").append("Data").append(" TEXT, ").append("Signature").append(" TEXT").append(");").toString());
        return;
    }

    public void onUpgrade(android.database.sqlite.SQLiteDatabase p3, int p4, int p5)
    {
        p3.execSQL(new StringBuilder().append("DROP TABLE IF EXISTS \'").append(com.google.android.finsky.billing.iab.DbHelper.packageTable).append("\'").toString());
        this.onCreate(p3);
        return;
    }

    public void saveItem(com.google.android.finsky.billing.iab.ItemsListItem p7)
    {
        try {
            com.google.android.finsky.billing.iab.DbHelper.savePackage = 1;
            android.content.ContentValues v0_1 = new android.content.ContentValues();
            v0_1.put("itemID", p7.itemID);
            v0_1.put("Data", p7.pData);
            v0_1.put("Signature", p7.pSignature);
        } catch (Exception v1) {
            com.google.android.finsky.billing.iab.DbHelper.savePackage = 0;
            System.out.println(new StringBuilder().append("LuckyPatcher-Error: savePackage ").append(v1).toString());
            return;
        }
        try {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.billing_db.insertOrThrow(new StringBuilder().append("\'").append(com.google.android.finsky.billing.iab.DbHelper.packageTable).append("\'").toString(), "itemID", v0_1);
        } catch (Exception v1) {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.billing_db.replace(new StringBuilder().append("\'").append(com.google.android.finsky.billing.iab.DbHelper.packageTable).append("\'").toString(), 0, v0_1);
        }
        com.google.android.finsky.billing.iab.DbHelper.savePackage = 0;
        com.google.android.finsky.billing.iab.DbHelper.savePackage = 0;
        return;
    }

    public void updatePackage(java.util.List p1)
    {
        return;
    }
}
