package com.google.android.finsky.billing.iab.google.util;
public interface IabHelper$OnConsumeMultiFinishedListener {

    public abstract void onConsumeMultiFinished();
}
