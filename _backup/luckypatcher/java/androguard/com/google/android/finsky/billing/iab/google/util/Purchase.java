package com.google.android.finsky.billing.iab.google.util;
public class Purchase {
    String mDeveloperPayload;
    String mItemType;
    String mOrderId;
    String mOriginalJson;
    String mPackageName;
    int mPurchaseState;
    long mPurchaseTime;
    String mSignature;
    String mSku;
    String mToken;

    public Purchase(String p4, String p5, String p6)
    {
        this.mItemType = p4;
        this.mOriginalJson = p5;
        org.json.JSONObject v0_1 = new org.json.JSONObject(this.mOriginalJson);
        this.mOrderId = v0_1.optString("orderId");
        this.mPackageName = v0_1.optString("packageName");
        this.mSku = v0_1.optString("productId");
        this.mPurchaseTime = v0_1.optLong("purchaseTime");
        this.mPurchaseState = v0_1.optInt("purchaseState");
        this.mDeveloperPayload = v0_1.optString("developerPayload");
        this.mToken = v0_1.optString("token", v0_1.optString("purchaseToken"));
        this.mSignature = p6;
        return;
    }

    public String getDeveloperPayload()
    {
        return this.mDeveloperPayload;
    }

    public String getItemType()
    {
        return this.mItemType;
    }

    public String getOrderId()
    {
        return this.mOrderId;
    }

    public String getOriginalJson()
    {
        return this.mOriginalJson;
    }

    public String getPackageName()
    {
        return this.mPackageName;
    }

    public int getPurchaseState()
    {
        return this.mPurchaseState;
    }

    public long getPurchaseTime()
    {
        return this.mPurchaseTime;
    }

    public String getSignature()
    {
        return this.mSignature;
    }

    public String getSku()
    {
        return this.mSku;
    }

    public String getToken()
    {
        return this.mToken;
    }

    public String toString()
    {
        return new StringBuilder().append("PurchaseInfo(type:").append(this.mItemType).append("):").append(this.mOriginalJson).toString();
    }
}
