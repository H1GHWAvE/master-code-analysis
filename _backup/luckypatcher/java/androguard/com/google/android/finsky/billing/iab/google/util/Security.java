package com.google.android.finsky.billing.iab.google.util;
public class Security {
    private static final String KEY_FACTORY_ALGORITHM = "RSA";
    private static final String SIGNATURE_ALGORITHM = "SHA1withRSA";
    private static final String TAG = "IABUtil/Security";

    public Security()
    {
        return;
    }

    public static java.security.PublicKey generatePublicKey(String p5)
    {
        try {
            return java.security.KeyFactory.getInstance("RSA").generatePublic(new java.security.spec.X509EncodedKeySpec(com.google.android.finsky.billing.iab.google.util.Base64.decode(p5)));
        } catch (com.google.android.finsky.billing.iab.google.util.Base64DecoderException v1_1) {
            android.util.Log.e("IABUtil/Security", "Invalid key specification.");
            throw new IllegalArgumentException(v1_1);
        } catch (com.google.android.finsky.billing.iab.google.util.Base64DecoderException v1_2) {
            throw new RuntimeException(v1_2);
        } catch (com.google.android.finsky.billing.iab.google.util.Base64DecoderException v1_0) {
            android.util.Log.e("IABUtil/Security", "Base64 decoding failed.");
            throw new IllegalArgumentException(v1_0);
        }
    }

    public static boolean verify(java.security.PublicKey p5, String p6, String p7)
    {
        int v2 = 0;
        try {
            java.security.Signature v1 = java.security.Signature.getInstance("SHA1withRSA");
            v1.initVerify(p5);
            v1.update(p6.getBytes());
        } catch (com.google.android.finsky.billing.iab.google.util.Base64DecoderException v0) {
            android.util.Log.e("IABUtil/Security", "Base64 decoding failed.");
            return v2;
        } catch (com.google.android.finsky.billing.iab.google.util.Base64DecoderException v0) {
            android.util.Log.e("IABUtil/Security", "NoSuchAlgorithmException.");
            return v2;
        } catch (com.google.android.finsky.billing.iab.google.util.Base64DecoderException v0) {
            android.util.Log.e("IABUtil/Security", "Invalid key specification.");
            return v2;
        } catch (com.google.android.finsky.billing.iab.google.util.Base64DecoderException v0) {
            android.util.Log.e("IABUtil/Security", "Signature exception.");
            return v2;
        }
        if (v1.verify(com.google.android.finsky.billing.iab.google.util.Base64.decode(p7))) {
            v2 = 1;
            return v2;
        } else {
            android.util.Log.e("IABUtil/Security", "Signature verification failed.");
            return v2;
        }
    }

    public static boolean verifyPurchase(String p5, String p6, String p7)
    {
        int v2 = 0;
        if (p6 != null) {
            if ((android.text.TextUtils.isEmpty(p7)) || (com.google.android.finsky.billing.iab.google.util.Security.verify(com.google.android.finsky.billing.iab.google.util.Security.generatePublicKey(p5), p6, p7))) {
                v2 = 1;
            } else {
                android.util.Log.w("IABUtil/Security", "signature does not match data.");
            }
        } else {
            android.util.Log.e("IABUtil/Security", "data is null");
        }
        return v2;
    }
}
