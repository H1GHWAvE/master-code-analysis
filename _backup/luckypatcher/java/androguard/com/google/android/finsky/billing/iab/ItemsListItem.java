package com.google.android.finsky.billing.iab;
public class ItemsListItem {
    public String itemID;
    public String pData;
    public String pSignature;

    public ItemsListItem(String p1, String p2, String p3)
    {
        this.itemID = p1;
        this.pData = p2;
        this.pSignature = p3;
        return;
    }
}
