package com.google.android.finsky.billing.iab;
 class BuyMarketActivity$1 implements android.view.View$OnClickListener {
    final synthetic com.google.android.finsky.billing.iab.BuyMarketActivity this$0;

    BuyMarketActivity$1(com.google.android.finsky.billing.iab.BuyMarketActivity p1)
    {
        this.this$0 = p1;
        return;
    }

    public void onClick(android.view.View p8)
    {
        if (this.this$0.check.isChecked()) {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getSharedPreferences("config", 4).edit().putBoolean("UnSign", 1).commit();
        } else {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getSharedPreferences("config", 4).edit().putBoolean("UnSign", 0).commit();
        }
        if (this.this$0.check2.isChecked()) {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getSharedPreferences("config", 4).edit().putBoolean("SavePurchase", 1).commit();
        } else {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getSharedPreferences("config", 4).edit().putBoolean("SavePurchase", 0).commit();
        }
        if (this.this$0.check3.isChecked()) {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getSharedPreferences("config", 4).edit().putBoolean("AutoRepeat", 1).commit();
        } else {
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getInstance().getSharedPreferences("config", 4).edit().putBoolean("AutoRepeat", 0).commit();
        }
        android.content.Intent v0_1 = new android.content.Intent("com.android.vending.billing.IN_APP_NOTIFY");
        v0_1.setPackage(this.this$0.packageName);
        v0_1.putExtra("notification_id", new StringBuilder().append("").append(com.chelpus.Utils.getRandom(7.832953389245686e-242, nan)).toString());
        this.this$0.sendBroadcast(v0_1);
        this.this$0.finish();
        return;
    }
}
