package com.chelpus;
 class XSupport$33 extends de.robv.android.xposed.XC_MethodHook {
    final synthetic com.chelpus.XSupport this$0;

    XSupport$33(com.chelpus.XSupport p1)
    {
        this.this$0 = p1;
        return;
    }

    protected void beforeHookedMethod(de.robv.android.xposed.XC_MethodHook$MethodHookParam p8)
    {
        this.this$0.loadPrefs();
        if ((com.chelpus.XSupport.enable) && (com.chelpus.XSupport.patch4)) {
            android.content.Intent v1_1 = ((android.content.Intent) p8.args[0]);
            if (v1_1 != null) {
                try {
                    if ((((v1_1.getAction() != null) && (com.chelpus.Utils.isMarketIntent(v1_1.getAction()))) || ((v1_1.getComponent() != null) && ((v1_1.getComponent().toString().contains("com.android.vending")) && (v1_1.getComponent().toString().toLowerCase().contains("inappbillingservice"))))) && (this.this$0.checkIntentRework(p8, v1_1, 0, 2))) {
                        android.content.ComponentName v0_1 = new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPackage().getName(), com.google.android.finsky.billing.iab.InAppBillingService.getName());
                        android.content.Intent v3_1 = new android.content.Intent();
                        v3_1.setComponent(v0_1);
                        p8.args[0] = v3_1;
                    }
                } catch (Object[] v4) {
                }
                if ((((v1_1.getAction() != null) && (v1_1.getAction().toLowerCase().equals("com.android.vending.billing.marketbillingservice.bind"))) || ((v1_1.getComponent() != null) && ((v1_1.getComponent().toString().contains("com.android.vending")) && (v1_1.getComponent().toString().toLowerCase().contains("marketbillingservice"))))) && (this.this$0.checkIntentRework(p8, v1_1, 0, 2))) {
                    android.content.ComponentName v0_3 = new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPackage().getName(), com.google.android.finsky.billing.iab.MarketBillingService.getName());
                    android.content.Intent v3_3 = new android.content.Intent();
                    v3_3.setComponent(v0_3);
                    p8.args[0] = v3_3;
                }
                if ((((v1_1.getAction() != null) && (v1_1.getAction().toLowerCase().equals("com.android.vending.licensing.ilicensingservice"))) || ((v1_1.getComponent() != null) && ((v1_1.getComponent().toString().contains("com.android.vending")) && (v1_1.getComponent().toString().toLowerCase().contains("licensingservice"))))) && (this.this$0.checkIntentRework(p8, v1_1, 1, 2))) {
                    android.content.ComponentName v0_5 = new android.content.ComponentName(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPackage().getName(), com.google.android.finsky.services.LicensingService.getName());
                    android.content.Intent v3_5 = new android.content.Intent();
                    v3_5.setComponent(v0_5);
                    p8.args[0] = v3_5;
                }
                if ((v1_1.getAction() != null) && (v1_1.getAction().equals("com.android.vending.billing.InAppBillingService.LUCK.ITestServiceInterface.BIND"))) {
                    android.content.Intent v2_1 = new android.content.Intent("com.android.vending.billing.InAppBillingService.LUCK.ITestServiceInterface.BIND");
                    v2_1.setPackage(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPackage().getName());
                    p8.args[0] = v2_1;
                }
            }
        }
        return;
    }
}
