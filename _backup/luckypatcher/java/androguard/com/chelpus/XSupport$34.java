package com.chelpus;
 class XSupport$34 extends de.robv.android.xposed.XC_MethodHook {
    final synthetic com.chelpus.XSupport this$0;

    XSupport$34(com.chelpus.XSupport p1)
    {
        this.this$0 = p1;
        return;
    }

    protected void afterHookedMethod(de.robv.android.xposed.XC_MethodHook$MethodHookParam p14)
    {
        this.this$0.ctx = ((android.content.Context) p14.args[0]);
        this.this$0.loadPrefs();
        if ((com.chelpus.XSupport.enable) && (com.chelpus.XSupport.hide)) {
            this.this$0.forHide = Boolean.valueOf(0);
            if ((this.this$0.ctx != null) && ((!this.this$0.ctx.getPackageName().equals(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPackage().getName())) && ((!this.this$0.ctx.getPackageName().equals("de.robv.android.xposed.installer")) && ((!this.this$0.ctx.getPackageName().contains("supersu")) && ((!this.this$0.ctx.getPackageName().contains("superuser")) && (!this.this$0.ctx.getPackageName().contains("pro.burgerz.wsm.manager"))))))) {
                this.this$0.forHide = Boolean.valueOf(1);
                try {
                    com.chelpus.XSupport v5_30 = p14.thisObject;
                    String v7_1 = new Object[2];
                    v7_1[0] = this.this$0.ctx.getPackageName();
                    v7_1[1] = Integer.valueOf(0);
                    android.content.pm.ApplicationInfo v1_1 = ((android.content.pm.ApplicationInfo) de.robv.android.xposed.XposedHelpers.callMethod(v5_30, "getApplicationInfo", v7_1));
                } catch (Exception v0) {
                    v0.printStackTrace();
                    this.this$0.forHide = Boolean.valueOf(0);
                }
                if (v1_1 != null) {
                    if ((v1_1.flags & 1) == 0) {
                        this.this$0.forHide = Boolean.valueOf(1);
                    } else {
                        this.this$0.forHide = Boolean.valueOf(0);
                        return;
                    }
                }
                android.content.Intent v2_1 = new android.content.Intent("android.intent.action.MAIN");
                v2_1.addCategory("android.intent.category.HOME");
                v2_1.addCategory("android.intent.category.DEFAULT");
                com.chelpus.XSupport v5_38 = p14.thisObject;
                String v7_2 = new Object[2];
                v7_2[0] = v2_1;
                v7_2[1] = Integer.valueOf(0);
                com.chelpus.XSupport v5_39 = ((java.util.List) de.robv.android.xposed.XposedHelpers.callMethod(v5_38, "queryIntentActivities", v7_2)).iterator();
                while (v5_39.hasNext()) {
                    if (((android.content.pm.ResolveInfo) v5_39.next()).activityInfo.packageName.equals(this.this$0.ctx.getPackageName())) {
                        this.this$0.forHide = Boolean.valueOf(0);
                        break;
                    }
                }
            }
        }
        return;
    }
}
