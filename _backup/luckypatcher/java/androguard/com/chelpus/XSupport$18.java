package com.chelpus;
 class XSupport$18 extends de.robv.android.xposed.XC_MethodHook {
    final synthetic com.chelpus.XSupport this$0;

    XSupport$18(com.chelpus.XSupport p1)
    {
        this.this$0 = p1;
        return;
    }

    protected void beforeHookedMethod(de.robv.android.xposed.XC_MethodHook$MethodHookParam p4)
    {
        this.this$0.loadPrefs();
        if ((com.chelpus.XSupport.enable) && (com.chelpus.XSupport.hide)) {
            String v0_1 = ((String) p4.args[0]);
            if ((v0_1 != null) && ((v0_1.equals(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPackage().getName())) && (this.this$0.checkForHide(p4)))) {
                p4.setResult(0);
            }
        }
        return;
    }
}
