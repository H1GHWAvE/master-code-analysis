package com.chelpus;
final class Utils$14 implements java.lang.Runnable {
    final synthetic boolean val$enable;

    Utils$14(boolean p1)
    {
        this.val$enable = p1;
        return;
    }

    public void run()
    {
        new java.util.ArrayList();
        android.content.pm.PackageInfo v2 = com.chelpus.Utils.getPkgInfo("com.android.vending", 4);
        if ((v2 != null) && ((v2.services != null) && (v2.services.length != 0))) {
            int v0 = 0;
            while (v0 < v2.services.length) {
                try {
                    if (this.val$enable) {
                        if ((v2.services[v0].name.endsWith("LicensingService")) && (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getComponentEnabledSetting(new android.content.ComponentName("com.android.vending", v2.services[v0].name)) != 1)) {
                            com.chelpus.Utils v4_14 = new com.chelpus.Utils("");
                            String[] v5_6 = new String[2];
                            v5_6[0] = new StringBuilder().append("pm enable \'com.android.vending/").append(v2.services[v0].name).append("\'").toString();
                            v5_6[1] = "skipOut";
                            v4_14.cmdRoot(v5_6);
                        }
                    } else {
                        if ((v2.services[v0].name.endsWith("LicensingService")) && (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPkgMng().getComponentEnabledSetting(new android.content.ComponentName("com.android.vending", v2.services[v0].name)) != 2)) {
                            com.chelpus.Utils v4_22 = new com.chelpus.Utils("");
                            String[] v5_12 = new String[2];
                            v5_12[0] = new StringBuilder().append("pm disable \'com.android.vending/").append(v2.services[v0].name).append("\'").toString();
                            v5_12[1] = "skipOut";
                            v4_22.cmdRoot(v5_12);
                        }
                    }
                    v0++;
                } catch (Exception v1) {
                    v1.printStackTrace();
                }
            }
        }
        return;
    }
}
