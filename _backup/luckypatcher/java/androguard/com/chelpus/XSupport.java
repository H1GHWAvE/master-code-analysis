package com.chelpus;
public class XSupport implements de.robv.android.xposed.IXposedHookZygoteInit, de.robv.android.xposed.IXposedHookLoadPackage {
    public static boolean enable;
    public static boolean hide;
    public static boolean patch1;
    public static boolean patch2;
    public static boolean patch3;
    public static boolean patch4;
    android.content.Context PMcontext;
    public boolean checkDuplicatedPermissions;
    public boolean checkPermissions;
    public boolean checkSignatures;
    android.content.Context ctx;
    public String currentPkgInApp;
    public boolean disableCheckSignatures;
    Boolean forHide;
    Boolean hideFromThisApp;
    public long initialize;
    public boolean installUnsignedApps;
    android.content.Context mContext;
    public de.robv.android.xposed.XSharedPreferences prefs;
    boolean skip1;
    boolean skip2;
    boolean skip3;
    boolean skipGB;
    public boolean verifyApps;

    static XSupport()
    {
        com.chelpus.XSupport.patch1 = 1;
        com.chelpus.XSupport.patch2 = 1;
        com.chelpus.XSupport.patch3 = 1;
        com.chelpus.XSupport.patch4 = 1;
        com.chelpus.XSupport.hide = 0;
        com.chelpus.XSupport.enable = 1;
        return;
    }

    public XSupport()
    {
        this.currentPkgInApp = 0;
        this.initialize = 0;
        this.ctx = 0;
        this.forHide = 0;
        this.mContext = 0;
        this.PMcontext = 0;
        this.hideFromThisApp = 0;
        this.skip1 = 0;
        this.skip2 = 0;
        this.skip3 = 0;
        this.skipGB = 0;
        return;
    }

    public boolean checkForHide(de.robv.android.xposed.XC_MethodHook$MethodHookParam p15)
    {
        int v9_17;
        int v9_0 = p15.thisObject;
        Object[] v11_1 = new Object[1];
        v11_1[0] = Integer.valueOf(android.os.Binder.getCallingUid());
        String v6_1 = ((String) de.robv.android.xposed.XposedHelpers.callMethod(v9_0, "getNameForUid", v11_1));
        if ((!v6_1.contains(":")) && ((!v6_1.contains(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPackage().getName())) && ((!v6_1.contains("de.robv.android.xposed.installer")) && ((!v6_1.contains("supersu")) && ((!v6_1.contains("superuser")) && (!v6_1.contains("pro.burgerz.wsm.manager"))))))) {
            int v1 = 1;
            android.content.pm.PackageManager v7 = this.PMcontext.getPackageManager();
            try {
                android.content.pm.ApplicationInfo v2 = v7.getApplicationInfo(v6_1, 0);
            } catch (android.content.pm.PackageManager$NameNotFoundException v0) {
                v0.printStackTrace();
                v9_17 = 0;
            }
            if (v2 != null) {
                if ((v2.flags & 1) == 0) {
                    v1 = 1;
                } else {
                    v9_17 = 0;
                    return v9_17;
                }
            }
            if (v1 != 0) {
                android.content.Intent v3_1 = new android.content.Intent("android.intent.action.MAIN");
                v3_1.addCategory("android.intent.category.HOME");
                v3_1.addCategory("android.intent.category.DEFAULT");
                int v9_24 = v7.queryIntentActivities(v3_1, 0).iterator();
                while (v9_24.hasNext()) {
                    if (((android.content.pm.ResolveInfo) v9_24.next()).activityInfo.packageName.equals(v6_1)) {
                        v9_17 = 0;
                    }
                    return v9_17;
                }
                if (v1 != 0) {
                    v9_17 = 1;
                    return v9_17;
                }
            }
            v9_17 = 0;
        } else {
            v9_17 = 0;
        }
        return v9_17;
    }

    public boolean checkForHideApp(de.robv.android.xposed.XC_MethodHook$MethodHookParam p15)
    {
        int v9_17;
        int v9_0 = p15.thisObject;
        Object[] v11_1 = new Object[1];
        v11_1[0] = Integer.valueOf(android.os.Binder.getCallingUid());
        String v6_1 = ((String) de.robv.android.xposed.XposedHelpers.callMethod(v9_0, "getNameForUid", v11_1));
        if ((!v6_1.contains(":")) && ((!v6_1.contains(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPackage().getName())) && ((!v6_1.contains("de.robv.android.xposed.installer")) && ((!v6_1.contains("supersu")) && ((!v6_1.contains("superuser")) && (!v6_1.contains("pro.burgerz.wsm.manager"))))))) {
            int v1 = 1;
            android.content.pm.PackageManager v7 = this.PMcontext.getPackageManager();
            try {
                android.content.pm.PackageInfo v2 = v7.getPackageInfo(v6_1, 0);
            } catch (android.content.pm.PackageManager$NameNotFoundException v0) {
                v0.printStackTrace();
                v9_17 = 0;
            }
            if (v2 != null) {
                if ((v2.applicationInfo.flags & 1) == 0) {
                    v1 = 1;
                } else {
                    v9_17 = 0;
                    return v9_17;
                }
            }
            if (v1 != 0) {
                android.content.Intent v3_1 = new android.content.Intent("android.intent.action.MAIN");
                v3_1.addCategory("android.intent.category.HOME");
                v3_1.addCategory("android.intent.category.DEFAULT");
                int v9_25 = v7.queryIntentActivities(v3_1, 0).iterator();
                while (v9_25.hasNext()) {
                    if (((android.content.pm.ResolveInfo) v9_25.next()).activityInfo.packageName.equals(v6_1)) {
                        v9_17 = 0;
                    }
                    return v9_17;
                }
                if (v1 != 0) {
                    v9_17 = 1;
                    return v9_17;
                }
            }
            v9_17 = 0;
        } else {
            v9_17 = 0;
        }
        return v9_17;
    }

    public boolean checkIntentRework(de.robv.android.xposed.XC_MethodHook$MethodHookParam p11, android.content.Intent p12, int p13, int p14)
    {
        int v3_29;
        if (p14 != 0) {
            if (p14 == 1) {
                if (p13 == 0) {
                    String v7_5 = new Object[0];
                    if (((android.content.pm.PackageManager) de.robv.android.xposed.XposedHelpers.callMethod(p11.thisObject, "getPackageManager", v7_5)).getComponentEnabledSetting(new android.content.ComponentName(com.chelpus.Common.PACKAGE_NAME, com.google.android.finsky.billing.iab.InAppBillingService.getName())) == 2) {
                        String v7_7 = new Object[0];
                        if (((android.content.pm.PackageManager) de.robv.android.xposed.XposedHelpers.callMethod(p11.thisObject, "getPackageManager", v7_7)).getComponentEnabledSetting(new android.content.ComponentName(com.chelpus.Common.PACKAGE_NAME, com.google.android.finsky.billing.iab.MarketBillingService.getName())) == 2) {
                            if (p13 == 1) {
                                String v7_9 = new Object[0];
                                if (((android.content.pm.PackageManager) de.robv.android.xposed.XposedHelpers.callMethod(p11.thisObject, "getPackageManager", v7_9)).getComponentEnabledSetting(new android.content.ComponentName(com.chelpus.Common.PACKAGE_NAME, com.google.android.finsky.services.LicensingService.getName())) != 2) {
                                    try {
                                        int v2_2 = p12.getStringExtra("xexe");
                                    } catch (Exception v1) {
                                        System.out.println("skip inapp xposed queryIntentServices");
                                        v2_2 = 0;
                                    }
                                    if ((v2_2 != 0) && (v2_2.equals("lp"))) {
                                        v3_29 = 0;
                                        return v3_29;
                                    } else {
                                        v3_29 = 1;
                                        return v3_29;
                                    }
                                }
                            }
                            if (p14 == 2) {
                                if ((p13 != 0) || ((this.PMcontext.getPackageManager().getComponentEnabledSetting(new android.content.ComponentName(com.chelpus.Common.PACKAGE_NAME, com.google.android.finsky.billing.iab.InAppBillingService.getName())) == 2) && (this.PMcontext.getPackageManager().getComponentEnabledSetting(new android.content.ComponentName(com.chelpus.Common.PACKAGE_NAME, com.google.android.finsky.billing.iab.MarketBillingService.getName())) == 2))) {
                                    if ((p13 == 1) && (this.PMcontext.getPackageManager().getComponentEnabledSetting(new android.content.ComponentName(com.chelpus.Common.PACKAGE_NAME, com.google.android.finsky.services.LicensingService.getName())) != 2)) {
                                        try {
                                            int v2_0 = p12.getStringExtra("xexe");
                                        } catch (Exception v1) {
                                            System.out.println("skip inapp xposed queryIntentServices");
                                            v2_0 = 0;
                                        }
                                        if ((v2_0 != 0) && (v2_0.equals("lp"))) {
                                            v3_29 = 0;
                                            return v3_29;
                                        } else {
                                            v3_29 = 1;
                                            return v3_29;
                                        }
                                    }
                                } else {
                                    try {
                                        int v2_1 = p12.getStringExtra("xexe");
                                    } catch (Exception v1) {
                                        System.out.println("skip inapp xposed queryIntentServices");
                                        v2_1 = 0;
                                    }
                                    if ((v2_1 != 0) && (v2_1.equals("lp"))) {
                                        v3_29 = 0;
                                        return v3_29;
                                    } else {
                                        v3_29 = 1;
                                        return v3_29;
                                    }
                                }
                            }
                            v3_29 = 0;
                            return v3_29;
                        }
                    }
                    try {
                        int v2_3 = p12.getStringExtra("xexe");
                    } catch (Exception v1) {
                        System.out.println("skip inapp xposed queryIntentServices");
                        v2_3 = 0;
                    }
                    if ((v2_3 != 0) && (v2_3.equals("lp"))) {
                        v3_29 = 0;
                        return v3_29;
                    } else {
                        v3_29 = 1;
                        return v3_29;
                    }
                }
            }
        } else {
            try {
                String v7_1 = new Object[0];
                android.content.Context v0_1 = ((android.content.Context) de.robv.android.xposed.XposedHelpers.callMethod(p11.thisObject, "getBaseContext", v7_1));
            } catch (Exception v1) {
                v0_1 = ((android.content.Context) de.robv.android.xposed.XposedHelpers.getObjectField(p11.thisObject, "mBase"));
            }
            if (v0_1 == null) {
            } else {
                if ((p13 != 0) || ((v0_1.getPackageManager().getComponentEnabledSetting(new android.content.ComponentName(com.chelpus.Common.PACKAGE_NAME, com.google.android.finsky.billing.iab.InAppBillingService.getName())) == 2) && (v0_1.getPackageManager().getComponentEnabledSetting(new android.content.ComponentName(com.chelpus.Common.PACKAGE_NAME, com.google.android.finsky.billing.iab.MarketBillingService.getName())) == 2))) {
                    if ((p13 != 1) || (v0_1.getPackageManager().getComponentEnabledSetting(new android.content.ComponentName(com.chelpus.Common.PACKAGE_NAME, com.google.android.finsky.services.LicensingService.getName())) == 2)) {
                    } else {
                        if ((p12.getStringExtra("xexe") != null) && (p12.getStringExtra("xexe").equals("lp"))) {
                            v3_29 = 0;
                        } else {
                            v3_29 = 1;
                        }
                    }
                } else {
                    if ((p12.getStringExtra("xexe") != null) && (p12.getStringExtra("xexe").equals("lp"))) {
                        v3_29 = 0;
                    } else {
                        v3_29 = 1;
                    }
                }
            }
        }
        return v3_29;
    }

    public void handleLoadPackage(de.robv.android.xposed.callbacks.XC_LoadPackage$LoadPackageParam p11)
    {
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPackage().getName().equals(p11.packageName)) {
            com.chelpus.XSupport$23 v3_1 = p11.classLoader;
            Object[] v5_0 = new Object[1];
            v5_0[0] = new com.chelpus.XSupport$7(this);
            de.robv.android.xposed.XposedHelpers.findAndHookMethod("com.chelpus.Utils", v3_1, "isXposedEnabled", v5_0);
        }
        Object[] v5_2 = new Object[4];
        v5_2[0] = android.content.Intent;
        v5_2[1] = android.content.ServiceConnection;
        v5_2[2] = Integer.TYPE;
        v5_2[3] = new com.chelpus.XSupport$8(this);
        de.robv.android.xposed.XposedHelpers.findAndHookMethod("android.content.ContextWrapper", 0, "bindService", v5_2);
        de.robv.android.xposed.XposedBridge.hookAllMethods(de.robv.android.xposed.XposedHelpers.findClass("android.app.ContextImpl", p11.classLoader), "queryIntentServices", new com.chelpus.XSupport$9(this));
        if (("android".equals(p11.packageName)) && (p11.processName.equals("android"))) {
            if (android.os.Build$VERSION.SDK_INT <= 10) {
                de.robv.android.xposed.XposedBridge.hookAllConstructors(de.robv.android.xposed.XposedHelpers.findClass("com.android.server.PackageManagerService", p11.classLoader), new com.chelpus.XSupport$24(this));
                de.robv.android.xposed.XposedBridge.hookAllMethods(de.robv.android.xposed.XposedHelpers.findClass("com.android.server.PackageManagerService", p11.classLoader), "getPackageInfo", new com.chelpus.XSupport$25(this));
                de.robv.android.xposed.XposedBridge.hookAllMethods(de.robv.android.xposed.XposedHelpers.findClass("com.android.server.PackageManagerService", p11.classLoader), "getApplicationInfo", new com.chelpus.XSupport$26(this));
                de.robv.android.xposed.XposedBridge.hookAllMethods(de.robv.android.xposed.XposedHelpers.findClass("android.content.pm.PackageParser", p11.classLoader), "generatePackageInfo", new com.chelpus.XSupport$27(this));
                de.robv.android.xposed.XposedBridge.hookAllMethods(de.robv.android.xposed.XposedHelpers.findClass("android.content.pm.PackageParser", p11.classLoader), "generateApplicationInfo", new com.chelpus.XSupport$28(this));
                de.robv.android.xposed.XposedBridge.hookAllMethods(de.robv.android.xposed.XposedHelpers.findClass("com.android.server.PackageManagerService", p11.classLoader), "getInstalledApplications", new com.chelpus.XSupport$29(this));
                de.robv.android.xposed.XposedBridge.hookAllMethods(de.robv.android.xposed.XposedHelpers.findClass("com.android.server.PackageManagerService", p11.classLoader), "getInstalledPackages", new com.chelpus.XSupport$30(this));
                de.robv.android.xposed.XposedBridge.hookAllMethods(de.robv.android.xposed.XposedHelpers.findClass("com.android.server.PackageManagerService", p11.classLoader), "getPreferredPackages", new com.chelpus.XSupport$31(this));
                de.robv.android.xposed.XposedBridge.hookAllMethods(de.robv.android.xposed.XposedHelpers.findClass("com.android.server.PackageManagerService", p11.classLoader), "checkSignaturesLP", new com.chelpus.XSupport$32(this));
                de.robv.android.xposed.XposedBridge.hookAllMethods(de.robv.android.xposed.XposedHelpers.findClass("com.android.server.PackageManagerService", p11.classLoader), "queryIntentServices", new com.chelpus.XSupport$33(this));
            } else {
                Class v1 = de.robv.android.xposed.XposedHelpers.findClass("com.android.server.pm.PackageManagerService", p11.classLoader);
                de.robv.android.xposed.XposedBridge.hookAllConstructors(de.robv.android.xposed.XposedHelpers.findClass("com.android.server.pm.PackageManagerService", p11.classLoader), new com.chelpus.XSupport$10(this));
                if (!com.chelpus.Common.LOLLIPOP_NEWER) {
                    if (!com.chelpus.Common.JB_MR1_NEWER) {
                        de.robv.android.xposed.XposedBridge.hookAllMethods(v1, "installPackageWithVerification", new com.chelpus.XSupport$13(this));
                    } else {
                        de.robv.android.xposed.XposedBridge.hookAllMethods(v1, "installPackageWithVerificationAndEncryption", new com.chelpus.XSupport$12(this));
                    }
                } else {
                    de.robv.android.xposed.XposedBridge.hookAllMethods(v1, "installPackageAsUser", new com.chelpus.XSupport$11(this));
                }
                de.robv.android.xposed.XposedBridge.hookAllMethods(de.robv.android.xposed.XposedHelpers.findClass("com.android.server.pm.PackageManagerService", p11.classLoader), "queryIntentServices", new com.chelpus.XSupport$14(this));
                if (com.chelpus.Common.LOLLIPOP_NEWER) {
                    de.robv.android.xposed.XposedBridge.hookAllMethods(v1, "checkUpgradeKeySetLP", new com.chelpus.XSupport$15(this));
                }
                de.robv.android.xposed.XposedBridge.hookAllMethods(v1, "verifySignaturesLP", new com.chelpus.XSupport$16(this));
                de.robv.android.xposed.XposedBridge.hookAllMethods(v1, "compareSignatures", new com.chelpus.XSupport$17(this));
                de.robv.android.xposed.XposedBridge.hookAllMethods(v1, "getPackageInfo", new com.chelpus.XSupport$18(this));
                de.robv.android.xposed.XposedBridge.hookAllMethods(v1, "getApplicationInfo", new com.chelpus.XSupport$19(this));
                de.robv.android.xposed.XposedBridge.hookAllMethods(v1, "generatePackageInfo", new com.chelpus.XSupport$20(this));
                de.robv.android.xposed.XposedBridge.hookAllMethods(v1, "getInstalledApplications", new com.chelpus.XSupport$21(this));
                de.robv.android.xposed.XposedBridge.hookAllMethods(v1, "getInstalledPackages", new com.chelpus.XSupport$22(this));
                de.robv.android.xposed.XposedBridge.hookAllMethods(v1, "getPreferredPackages", new com.chelpus.XSupport$23(this));
            }
        }
        if (android.os.Build$VERSION.SDK_INT > 10) {
            de.robv.android.xposed.XposedBridge.hookAllConstructors(de.robv.android.xposed.XposedHelpers.findClass("android.app.ApplicationPackageManager", p11.classLoader), new com.chelpus.XSupport$34(this));
            de.robv.android.xposed.XposedBridge.hookAllMethods(de.robv.android.xposed.XposedHelpers.findClass("android.app.ApplicationPackageManager", p11.classLoader), "getPackageInfo", new com.chelpus.XSupport$35(this));
            de.robv.android.xposed.XposedBridge.hookAllMethods(de.robv.android.xposed.XposedHelpers.findClass("android.app.ApplicationPackageManager", p11.classLoader), "getApplicationInfo", new com.chelpus.XSupport$36(this));
            de.robv.android.xposed.XposedBridge.hookAllMethods(de.robv.android.xposed.XposedHelpers.findClass("android.app.ApplicationPackageManager", p11.classLoader), "getInstalledApplications", new com.chelpus.XSupport$37(this));
            de.robv.android.xposed.XposedBridge.hookAllMethods(de.robv.android.xposed.XposedHelpers.findClass("android.app.ApplicationPackageManager", p11.classLoader), "getInstalledPackages", new com.chelpus.XSupport$38(this));
            de.robv.android.xposed.XposedBridge.hookAllMethods(de.robv.android.xposed.XposedHelpers.findClass("android.app.ApplicationPackageManager", p11.classLoader), "getPreferredPackages", new com.chelpus.XSupport$39(this));
        }
        return;
    }

    public void initZygote(de.robv.android.xposed.IXposedHookZygoteInit$StartupParam p13)
    {
        de.robv.android.xposed.XposedHelpers.findClass("android.content.pm.PackageParser", 0);
        de.robv.android.xposed.XposedHelpers.findClass("java.util.jar.JarVerifier$VerifierEntry", 0);
        this.disableCheckSignatures = 1;
        if (android.os.Build$VERSION.SDK_INT > 18) {
            try {
                de.robv.android.xposed.XposedBridge.hookAllMethods(de.robv.android.xposed.XposedHelpers.findClass("com.android.org.conscrypt.OpenSSLSignature", 0), "engineVerify", new com.chelpus.XSupport$1(this));
            } catch (ClassNotFoundException v3) {
            }
        }
        if ((android.os.Build$VERSION.SDK_INT > 14) && (android.os.Build$VERSION.SDK_INT < 19)) {
            try {
                de.robv.android.xposed.XposedBridge.hookAllMethods(de.robv.android.xposed.XposedHelpers.findClass("org.apache.harmony.xnet.provider.jsse.OpenSSLSignature", 0), "engineVerify", new com.chelpus.XSupport$2(this));
            } catch (ClassNotFoundException v3) {
            }
        }
        if (android.os.Build$VERSION.SDK_INT == 10) {
            try {
                de.robv.android.xposed.XposedBridge.hookAllMethods(de.robv.android.xposed.XposedHelpers.findClass("org.bouncycastle.jce.provider.JDKDigestSignature", 0), "engineVerify", new com.chelpus.XSupport$3(this));
            } catch (ClassNotFoundException v3) {
            }
        }
        Object[] v5_0 = new Object[3];
        v5_0[0] = byte[];
        v5_0[1] = byte[];
        v5_0[2] = new com.chelpus.XSupport$4(this);
        de.robv.android.xposed.XposedHelpers.findAndHookMethod("java.security.MessageDigest", 0, "isEqual", v5_0);
        Object[] v5_1 = new Object[2];
        v5_1[0] = byte[];
        v5_1[1] = new com.chelpus.XSupport$5(this);
        de.robv.android.xposed.XposedHelpers.findAndHookMethod("java.security.Signature", 0, "verify", v5_1);
        Object[] v5_3 = new Object[4];
        v5_3[0] = byte[];
        v5_3[1] = Integer.TYPE;
        v5_3[2] = Integer.TYPE;
        v5_3[3] = new com.chelpus.XSupport$6(this);
        de.robv.android.xposed.XposedHelpers.findAndHookMethod("java.security.Signature", 0, "verify", v5_3);
        return;
    }

    public void loadPrefs()
    {
        if ((this.initialize == 0) || (this.initialize != new java.io.File("/data/lp/xposed").lastModified())) {
            if ((this.initialize != 0) && (this.initialize != new java.io.File("/data/lp/xposed").lastModified())) {
                System.out.println("Update settings xposed");
            }
            this.initialize = new java.io.File("/data/lp/xposed").lastModified();
            new java.util.ArrayList();
            try {
                org.json.JSONObject v2 = com.chelpus.Utils.readXposedParamBoolean();
            } catch (org.json.JSONException v0) {
                v0.printStackTrace();
            }
            if (v2 != null) {
                com.chelpus.XSupport.patch1 = v2.optBoolean("patch1", 1);
                com.chelpus.XSupport.patch2 = v2.optBoolean("patch2", 1);
                com.chelpus.XSupport.patch3 = v2.optBoolean("patch3", 1);
                com.chelpus.XSupport.patch4 = v2.optBoolean("patch4", 1);
                com.chelpus.XSupport.hide = v2.optBoolean("hide", 0);
                com.chelpus.XSupport.enable = v2.optBoolean("module_on", 1);
            }
        }
        return;
    }
}
