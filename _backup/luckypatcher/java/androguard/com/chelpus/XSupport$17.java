package com.chelpus;
 class XSupport$17 extends de.robv.android.xposed.XC_MethodHook {
    final synthetic com.chelpus.XSupport this$0;

    XSupport$17(com.chelpus.XSupport p1)
    {
        this.this$0 = p1;
        return;
    }

    protected void beforeHookedMethod(de.robv.android.xposed.XC_MethodHook$MethodHookParam p13)
    {
        this.this$0.loadPrefs();
        if ((com.chelpus.XSupport.enable) && (com.chelpus.XSupport.patch3)) {
            try {
                String v2_1 = com.google.android.finsky.billing.iab.google.util.Base64.encode(this.this$0.PMcontext.getPackageManager().getPackageInfo("android", 64).signatures[0].toByteArray()).replaceAll("\n", "");
                int v6 = 0;
                android.content.pm.Signature[] v4_1 = ((android.content.pm.Signature[]) ((android.content.pm.Signature[]) p13.args[0]));
                android.content.pm.Signature[] v5_1 = ((android.content.pm.Signature[]) ((android.content.pm.Signature[]) p13.args[1]));
            } catch (Exception v1) {
                v1.printStackTrace();
            }
            if ((v4_1 != null) && (v4_1.length > 0)) {
                int v7_18 = 0;
                while (v7_18 < v4_1.length) {
                    if (com.google.android.finsky.billing.iab.google.util.Base64.encode(v4_1[v7_18].toByteArray()).replaceAll("\n", "").equals(v2_1)) {
                        v6 = 1;
                    }
                    v7_18++;
                }
            }
            if ((v5_1 != null) && (v5_1.length > 0)) {
                int v7_20 = 0;
                while (v7_20 < v5_1.length) {
                    if (com.google.android.finsky.billing.iab.google.util.Base64.encode(v5_1[v7_20].toByteArray()).replaceAll("\n", "").equals(v2_1)) {
                        v6 = 1;
                    }
                    v7_20++;
                }
            }
            if (v6 == 0) {
                p13.setResult(Integer.valueOf(0));
            }
        }
        return;
    }
}
