package com.chelpus.root.utils;
public class odexSystemApps {

    public odexSystemApps()
    {
        return;
    }

    public static void main(String[] p17)
    {
        com.chelpus.Utils.startRootJava(new com.chelpus.root.utils.odexSystemApps$1());
        java.io.File[] v4_0 = new java.io.File("/system/app").listFiles();
        int v11_0 = v4_0.length;
        int v10_0 = 0;
        while (v10_0 < v11_0) {
            java.io.File v6_1;
            java.io.File v3_1 = v4_0[v10_0];
            try {
                if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.api < 21) {
                    if ((v3_1.isFile()) && (v3_1.getAbsolutePath().endsWith(".apk"))) {
                        v6_1 = new java.io.File(com.chelpus.Utils.getOdexForCreate(v3_1.getAbsolutePath(), "0"));
                        java.io.File v1_2 = com.chelpus.Utils.getFileDalvikCache(v3_1.getAbsolutePath());
                        if ((!v6_1.exists()) && ((v1_2 != null) && (v1_2.exists()))) {
                            com.chelpus.Utils.copyFile(v1_2, v6_1);
                            if (v1_2.length() == v6_1.length()) {
                                String[] v9_41 = new String[3];
                                v9_41[0] = "chmod";
                                v9_41[1] = "644";
                                v9_41[2] = v6_1.getAbsolutePath();
                                com.chelpus.Utils.cmdParam(v9_41);
                                String[] v9_43 = new String[3];
                                v9_43[0] = "chown";
                                v9_43[1] = "0.0";
                                v9_43[2] = v6_1.getAbsolutePath();
                                com.chelpus.Utils.cmdParam(v9_43);
                                String[] v9_45 = new String[3];
                                v9_45[0] = "chown";
                                v9_45[1] = "0:0";
                                v9_45[2] = v6_1.getAbsolutePath();
                                com.chelpus.Utils.cmdParam(v9_45);
                                v1_2.delete();
                            } else {
                                v6_1.delete();
                                System.out.println("Not enought space!");
                            }
                        }
                    }
                } else {
                    if (v3_1.isDirectory()) {
                        java.io.File[] v0_1 = v3_1.listFiles();
                        if ((v0_1 != null) && (v0_1.length > 0)) {
                            int v12_30 = v0_1.length;
                            String[] v9_49 = 0;
                            while (v9_49 < v12_30) {
                                java.io.File v5_1 = v0_1[v9_49];
                                try {
                                    if ((v5_1.isFile()) && (v5_1.getAbsolutePath().endsWith(".apk"))) {
                                        v6_1 = new java.io.File(com.chelpus.Utils.getOdexForCreate(v5_1.getAbsolutePath(), "0"));
                                        java.io.File v1_3 = com.chelpus.Utils.getFileDalvikCache(v5_1.getAbsolutePath());
                                        if ((!v6_1.exists()) && ((v1_3 != null) && (v1_3.exists()))) {
                                            com.chelpus.Utils.copyFile(v1_3, v6_1);
                                            if (v1_3.length() == v6_1.length()) {
                                                String[] v13_47 = new String[3];
                                                v13_47[0] = "chmod";
                                                v13_47[1] = "644";
                                                v13_47[2] = v6_1.getAbsolutePath();
                                                com.chelpus.Utils.cmdParam(v13_47);
                                                String[] v13_49 = new String[3];
                                                v13_49[0] = "chown";
                                                v13_49[1] = "0.0";
                                                v13_49[2] = v6_1.getAbsolutePath();
                                                com.chelpus.Utils.cmdParam(v13_49);
                                                String[] v13_51 = new String[3];
                                                v13_51[0] = "chown";
                                                v13_51[1] = "0:0";
                                                v13_51[2] = v6_1.getAbsolutePath();
                                                com.chelpus.Utils.cmdParam(v13_51);
                                                v1_3.delete();
                                            } else {
                                                v6_1.delete();
                                                System.out.println("Not enought space!");
                                            }
                                        }
                                    }
                                    v9_49++;
                                } catch (Exception v2_3) {
                                    v6_1.delete();
                                    System.out.println("IO Exception!");
                                    v2_3.printStackTrace();
                                }
                            }
                        }
                    }
                }
                v10_0++;
            } catch (Exception v2_2) {
                v6_1.delete();
                System.out.println("IO Exception!");
                v2_2.printStackTrace();
            }
        }
        java.io.File v7_3 = new java.io.File("/system/priv-app");
        if (v7_3.exists()) {
            java.io.File[] v4_1 = v7_3.listFiles();
            int v11_1 = v4_1.length;
            int v10_1 = 0;
            while (v10_1 < v11_1) {
                java.io.File v3_0 = v4_1[v10_1];
                try {
                    if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.api < 21) {
                        if ((v3_0.isFile()) && (v3_0.getAbsolutePath().endsWith(".apk"))) {
                            v6_1 = new java.io.File(com.chelpus.Utils.getOdexForCreate(v3_0.getAbsolutePath(), "0"));
                            java.io.File v1_0 = com.chelpus.Utils.getFileDalvikCache(v3_0.getAbsolutePath());
                            if ((!v6_1.exists()) && ((v1_0 != null) && (v1_0.exists()))) {
                                com.chelpus.Utils.copyFile(v1_0, v6_1);
                                if (v1_0.length() == v6_1.length()) {
                                    String[] v9_19 = new String[3];
                                    v9_19[0] = "chmod";
                                    v9_19[1] = "644";
                                    v9_19[2] = v6_1.getAbsolutePath();
                                    com.chelpus.Utils.cmdParam(v9_19);
                                    String[] v9_21 = new String[3];
                                    v9_21[0] = "chown";
                                    v9_21[1] = "0.0";
                                    v9_21[2] = v6_1.getAbsolutePath();
                                    com.chelpus.Utils.cmdParam(v9_21);
                                    String[] v9_23 = new String[3];
                                    v9_23[0] = "chown";
                                    v9_23[1] = "0:0";
                                    v9_23[2] = v6_1.getAbsolutePath();
                                    com.chelpus.Utils.cmdParam(v9_23);
                                    v1_0.delete();
                                } else {
                                    v6_1.delete();
                                    System.out.println("Not enought space!");
                                }
                            }
                        }
                    } else {
                        if (v3_0.isDirectory()) {
                            java.io.File[] v0_0 = v3_0.listFiles();
                            if ((v0_0 != null) && (v0_0.length > 0)) {
                                int v12_14 = v0_0.length;
                                String[] v9_27 = 0;
                                while (v9_27 < v12_14) {
                                    java.io.File v5_0 = v0_0[v9_27];
                                    try {
                                        if ((v5_0.isFile()) && (v5_0.getAbsolutePath().endsWith(".apk"))) {
                                            v6_1 = new java.io.File(com.chelpus.Utils.getOdexForCreate(v5_0.getAbsolutePath(), "0"));
                                            java.io.File v1_1 = com.chelpus.Utils.getFileDalvikCache(v5_0.getAbsolutePath());
                                            if ((!v6_1.exists()) && ((v1_1 != null) && (v1_1.exists()))) {
                                                com.chelpus.Utils.copyFile(v1_1, v6_1);
                                                if (v1_1.length() == v6_1.length()) {
                                                    String[] v13_20 = new String[3];
                                                    v13_20[0] = "chmod";
                                                    v13_20[1] = "644";
                                                    v13_20[2] = v6_1.getAbsolutePath();
                                                    com.chelpus.Utils.cmdParam(v13_20);
                                                    String[] v13_22 = new String[3];
                                                    v13_22[0] = "chown";
                                                    v13_22[1] = "0.0";
                                                    v13_22[2] = v6_1.getAbsolutePath();
                                                    com.chelpus.Utils.cmdParam(v13_22);
                                                    String[] v13_24 = new String[3];
                                                    v13_24[0] = "chown";
                                                    v13_24[1] = "0:0";
                                                    v13_24[2] = v6_1.getAbsolutePath();
                                                    com.chelpus.Utils.cmdParam(v13_24);
                                                    v1_1.delete();
                                                } else {
                                                    v6_1.delete();
                                                    System.out.println("Not enought space!");
                                                }
                                            }
                                        }
                                        v9_27++;
                                    } catch (Exception v2_1) {
                                        v6_1.delete();
                                        System.out.println("IO Exception!");
                                        v2_1.printStackTrace();
                                    }
                                }
                            }
                        }
                    }
                    v10_1++;
                } catch (Exception v2_0) {
                    v6_1.delete();
                    System.out.println("IO Exception!");
                    v2_0.printStackTrace();
                }
            }
        }
        com.chelpus.Utils.exitFromRootJava();
        return;
    }
}
