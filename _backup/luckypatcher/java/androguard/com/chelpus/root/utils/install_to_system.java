package com.chelpus.root.utils;
public class install_to_system {
    public static String CPU_ABI;
    public static String CPU_ABI2;
    public static String appfile;
    public static String datadir;
    public static String pkgName;
    public static String toolsfiles;

    static install_to_system()
    {
        com.chelpus.root.utils.install_to_system.appfile = "/sdcard/app.apk";
        com.chelpus.root.utils.install_to_system.datadir = "/data/data/";
        com.chelpus.root.utils.install_to_system.toolsfiles = "";
        com.chelpus.root.utils.install_to_system.pkgName = "";
        com.chelpus.root.utils.install_to_system.CPU_ABI = "";
        com.chelpus.root.utils.install_to_system.CPU_ABI2 = "";
        return;
    }

    public install_to_system()
    {
        return;
    }

    public static java.util.ArrayList getLibs(java.io.File p19)
    {
        java.util.ArrayList v8_1 = new java.util.ArrayList();
        String v10 = new StringBuilder().append(com.chelpus.root.utils.install_to_system.toolsfiles).append("/tmp/").toString();
        java.io.File v9_1 = new java.io.File(new StringBuilder().append(com.chelpus.root.utils.install_to_system.toolsfiles).append("/tmp/lib/").toString());
        if (v9_1.exists()) {
            try {
                new com.chelpus.Utils("").deleteFolder(v9_1);
            } catch (java.io.IOException v4) {
                v4.printStackTrace();
            }
        }
        if (!v9_1.exists()) {
            new com.chelpus.root.utils.install_to_system$Decompress(p19.getAbsolutePath(), v10).unzip();
        }
        java.io.File v1_1 = new java.io.File(new StringBuilder().append(com.chelpus.root.utils.install_to_system.toolsfiles).append("/tmp/lib").toString());
        if (v1_1.exists()) {
            java.io.File[] v5 = v1_1.listFiles();
            int v14_0 = v5.length;
            int v13_6 = 0;
            while (v13_6 < v14_0) {
                java.io.File v3_1 = v5[v13_6];
                System.out.println(new StringBuilder().append("LuckyPatcher: directory in lib found - ").append(v3_1).toString());
                if ((v3_1.isDirectory()) && (v3_1.getName().equals(com.chelpus.root.utils.install_to_system.CPU_ABI))) {
                    System.out.println(new StringBuilder().append("LuckyPatcher: - ").append(v3_1).append(" ").append(v3_1.getName()).append(" ").append(com.chelpus.root.utils.install_to_system.CPU_ABI).toString());
                    java.io.File[] v7_1 = v3_1.listFiles();
                    int v15_17 = v7_1.length;
                    int v12_33 = 0;
                    while (v12_33 < v15_17) {
                        java.io.File v6_1 = v7_1[v12_33];
                        if ((v6_1.isFile()) && (v6_1.toString().endsWith(".so"))) {
                            System.out.println(v6_1);
                            v8_1.add(v6_1);
                            System.out.println(new StringBuilder().append("LuckyPatcher: found lib file - ").append(com.chelpus.root.utils.install_to_system.CPU_ABI).append(" ").append(v6_1).toString());
                        }
                        v12_33++;
                    }
                }
                v13_6++;
            }
            System.out.println(v8_1.size());
            if (v8_1.size() == 0) {
                int v14_1 = v5.length;
                int v13_8 = 0;
                while (v13_8 < v14_1) {
                    java.io.File v3_0 = v5[v13_8];
                    if ((v3_0.isDirectory()) && (v3_0.getName().equals(com.chelpus.root.utils.install_to_system.CPU_ABI2))) {
                        java.io.File[] v7_0 = v3_0.listFiles();
                        int v15_1 = v7_0.length;
                        int v12_26 = 0;
                        while (v12_26 < v15_1) {
                            java.io.File v6_0 = v7_0[v12_26];
                            if ((v6_0.isFile()) && (v6_0.getName().endsWith(".so"))) {
                                v8_1.add(v6_0);
                                System.out.println(new StringBuilder().append("LuckyPatcher: found lib file - ").append(com.chelpus.root.utils.install_to_system.CPU_ABI2).append(" ").append(v6_0).toString());
                            }
                            v12_26++;
                        }
                    }
                    v13_8++;
                }
            }
        }
        return v8_1;
    }

    public static void main(String[] p20)
    {
        int v9 = 0;
        try {
            com.chelpus.Utils.startRootJava(new com.chelpus.root.utils.install_to_system$1());
            System.out.println(p20[1]);
            com.chelpus.root.utils.install_to_system.appfile = p20[1];
            com.chelpus.root.utils.install_to_system.datadir = p20[2];
            com.chelpus.root.utils.install_to_system.toolsfiles = p20[3];
            com.chelpus.root.utils.install_to_system.pkgName = p20[0];
            com.chelpus.root.utils.install_to_system.CPU_ABI = p20[4];
            com.chelpus.root.utils.install_to_system.CPU_ABI2 = p20[5];
            String v11 = "/system/app/";
            try {
                if ((new java.io.File("/system/priv-app").exists()) && (new java.io.File("/system/priv-app").list() != null)) {
                    v11 = "/system/priv-app/";
                }
            } catch (java.io.IOException v4_0) {
                v4_0.printStackTrace();
            }
            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.api >= 21) {
                v11 = new StringBuilder().append("/system/priv-app/").append(com.chelpus.root.utils.install_to_system.pkgName).append("/").toString();
                if (!new java.io.File(v11).exists()) {
                    new java.io.File(v11).mkdirs();
                    java.io.PrintStream v14_33 = new String[3];
                    v14_33[0] = "chmod";
                    v14_33[1] = "755";
                    v14_33[2] = new StringBuilder().append("/system/priv-app/").append(com.chelpus.root.utils.install_to_system.pkgName).toString();
                    com.chelpus.Utils.cmdParam(v14_33);
                }
            }
            java.io.File v1_1 = new java.io.File(com.chelpus.root.utils.install_to_system.appfile);
            java.util.ArrayList v8 = com.chelpus.root.utils.install_to_system.getLibs(v1_1);
            String v7 = "/system/lib/";
            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.api >= 21) {
                if ((com.chelpus.root.utils.install_to_system.CPU_ABI.toLowerCase().contains("x86")) || (com.chelpus.root.utils.install_to_system.CPU_ABI2.contains("x86"))) {
                    v7 = new StringBuilder().append("/system/priv-app/").append(com.chelpus.root.utils.install_to_system.pkgName).append("/lib/x86/").toString();
                }
                if ((com.chelpus.root.utils.install_to_system.CPU_ABI.toLowerCase().contains("arm")) || (com.chelpus.root.utils.install_to_system.CPU_ABI2.contains("arm"))) {
                    v7 = new StringBuilder().append("/system/priv-app/").append(com.chelpus.root.utils.install_to_system.pkgName).append("/lib/arm/").toString();
                }
            }
            if (v8.size() > 0) {
                java.io.PrintStream v14_57 = v8.iterator();
                while (v14_57.hasNext()) {
                    java.io.File v5_1 = ((java.io.File) v14_57.next());
                    if (!new java.io.File(v7).exists()) {
                        new java.io.File(v7).mkdirs();
                        String v15_29 = new String[3];
                        v15_29[0] = "chmod";
                        v15_29[1] = "755";
                        v15_29[2] = new StringBuilder().append("/system/priv-app/").append(com.chelpus.root.utils.install_to_system.pkgName).append("/lib").toString();
                        com.chelpus.Utils.cmdParam(v15_29);
                    }
                    com.chelpus.Utils.setPermissionDir(new StringBuilder().append("/system/priv-app/").append(com.chelpus.root.utils.install_to_system.pkgName).toString(), v7, "755");
                    com.chelpus.Utils.copyFile(v5_1, new java.io.File(new StringBuilder().append(v7).append(v5_1.getName()).toString()));
                    com.chelpus.root.utils.install_to_system.run_all(new StringBuilder().append("chmod 0644 ").append(v7).append(v5_1.getName()).toString());
                    com.chelpus.root.utils.install_to_system.run_all(new StringBuilder().append("chown 0.0 ").append(v7).append(v5_1.getName()).toString());
                    com.chelpus.root.utils.install_to_system.run_all(new StringBuilder().append("chown 0:0 ").append(v7).append(v5_1.getName()).toString());
                    if (v5_1.length() != new java.io.File(new StringBuilder().append(v7).append(v5_1.getName()).toString()).length()) {
                        java.io.PrintStream v14_60 = v8.iterator();
                        while (v14_60.hasNext()) {
                            new java.io.File(new StringBuilder().append(v7).append(((java.io.File) v14_60.next()).getName()).toString()).delete();
                        }
                        System.out.println("In /system space not found!");
                        v9 = 1;
                        break;
                    } else {
                        System.out.println(new StringBuilder().append("LuckyPatcher: copy lib ").append(v7).append(v5_1.getName()).toString());
                    }
                }
            }
            if (v9 != 0) {
                new java.io.File(new StringBuilder().append(v11).append(com.chelpus.root.utils.install_to_system.pkgName).append(".apk").toString()).delete();
            } else {
                if (!new java.io.File(new StringBuilder().append(v11).append(com.chelpus.root.utils.install_to_system.pkgName).append(".odex").toString()).exists()) {
                    if (!com.chelpus.Utils.dalvikvm_copyFile(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir, com.chelpus.root.utils.install_to_system.appfile, new StringBuilder().append(v11).append(com.chelpus.root.utils.install_to_system.pkgName).append(".apk").toString())) {
                        System.out.println("In /system space not found!");
                        new java.io.File(new StringBuilder().append(v11).append(com.chelpus.root.utils.install_to_system.pkgName).append(".apk").toString()).delete();
                    }
                } else {
                    new java.io.File(new StringBuilder().append(v11).append(com.chelpus.root.utils.install_to_system.pkgName).append(".odex").toString()).delete();
                }
                if (v1_1.length() != new java.io.File(new StringBuilder().append(v11).append(com.chelpus.root.utils.install_to_system.pkgName).append(".apk").toString()).length()) {
                    new java.io.File(new StringBuilder().append(v11).append(com.chelpus.root.utils.install_to_system.pkgName).append(".apk").toString()).delete();
                    System.out.println("In /system space not found!");
                    java.io.PrintStream v14_87 = v8.iterator();
                    while (v14_87.hasNext()) {
                        new java.io.File(new StringBuilder().append(v7).append(((java.io.File) v14_87.next()).getName()).toString()).delete();
                    }
                } else {
                    com.chelpus.root.utils.install_to_system.run_all(new StringBuilder().append("chmod 0644 ").append(v11).append(com.chelpus.root.utils.install_to_system.pkgName).append(".apk").toString());
                    com.chelpus.root.utils.install_to_system.run_all(new StringBuilder().append("chown 0.0 ").append(v11).append(com.chelpus.root.utils.install_to_system.pkgName).append(".apk").toString());
                    com.chelpus.root.utils.install_to_system.run_all(new StringBuilder().append("chown 0:0 ").append(v11).append(com.chelpus.root.utils.install_to_system.pkgName).append(".apk").toString());
                }
            }
            java.io.File v12_1 = new java.io.File(new StringBuilder().append(com.chelpus.root.utils.install_to_system.toolsfiles).append("/tmp").toString());
            if (v12_1.exists()) {
                com.chelpus.Utils v13_1 = new com.chelpus.Utils("");
                try {
                    v13_1.deleteFolder(v12_1);
                    v13_1.deleteFolder(new java.io.File(com.chelpus.root.utils.install_to_system.datadir));
                } catch (java.io.IOException v4_2) {
                    v4_2.printStackTrace();
                }
            }
            java.io.File v2 = com.chelpus.Utils.getFileDalvikCache(new StringBuilder().append(v11).append(com.chelpus.root.utils.install_to_system.pkgName).append(".apk").toString());
            if (v2 == null) {
                com.chelpus.Utils.exitFromRootJava();
                return;
            } else {
                v2.delete();
                com.chelpus.Utils.exitFromRootJava();
                return;
            }
        } catch (java.io.IOException v4_3) {
            System.out.println(new StringBuilder().append("LuckyPatcher Error move to System: ").append(v4_3).toString());
            v4_3.printStackTrace();
            com.chelpus.Utils.exitFromRootJava();
            return;
        }
    }

    private static void run_all(String p6)
    {
        try {
            String v6_1 = new String(p6.getBytes(), "ISO-8859-1");
        } catch (java.io.UnsupportedEncodingException v1) {
            v1.printStackTrace();
        }
        try {
            Process v2_0 = Runtime.getRuntime().exec(new StringBuilder().append(v6_1).append("\n").toString());
            v2_0.waitFor();
            v2_0.destroy();
        } catch (Exception v3) {
        }
        try {
            Process v2_1 = Runtime.getRuntime().exec(new StringBuilder().append("toolbox ").append(v6_1).append("\n").toString());
            v2_1.waitFor();
            v2_1.destroy();
            try {
                Process v2_2 = Runtime.getRuntime().exec(new StringBuilder().append("/system/bin/failsafe/toolbox ").append(v6_1).append("\n").toString());
                v2_2.waitFor();
                v2_2.destroy();
                try {
                    Process v2_3 = Runtime.getRuntime().exec(new StringBuilder().append("busybox ").append(v6_1).append("\n").toString());
                    v2_3.waitFor();
                    v2_3.destroy();
                } catch (Exception v3) {
                }
                try {
                    Process v2_4 = Runtime.getRuntime().exec(new StringBuilder().append(com.chelpus.root.utils.install_to_system.toolsfiles).append("/busybox ").append(v6_1).append("\n").toString());
                    v2_4.waitFor();
                    v2_4.destroy();
                } catch (Exception v3) {
                }
                return;
            } catch (Exception v3) {
            }
        } catch (Exception v3) {
        }
    }
}
