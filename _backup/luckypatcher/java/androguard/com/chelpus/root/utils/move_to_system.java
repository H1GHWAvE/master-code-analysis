package com.chelpus.root.utils;
public class move_to_system {
    public static String datadir;
    public static String dirapp;
    public static String index;
    public static String libDir;
    public static String pkgName;
    public static String toolsfiles;

    static move_to_system()
    {
        com.chelpus.root.utils.move_to_system.dirapp = "/data/app/";
        com.chelpus.root.utils.move_to_system.datadir = "/data/data/";
        com.chelpus.root.utils.move_to_system.toolsfiles = "";
        com.chelpus.root.utils.move_to_system.pkgName = "";
        com.chelpus.root.utils.move_to_system.libDir = "";
        com.chelpus.root.utils.move_to_system.index = "";
        return;
    }

    public move_to_system()
    {
        return;
    }

    public static java.util.ArrayList getLibs(java.io.File p18, String p19, int p20)
    {
        java.util.ArrayList v7_1 = new java.util.ArrayList();
        if (p19.startsWith("/mnt/")) {
            java.io.File v18_1 = com.chelpus.Utils.getDirs(new java.io.File(p19));
            com.chelpus.Utils.remount(v18_1.getAbsolutePath(), "rw");
            if (v18_1.exists()) {
                String[] v4_0 = v18_1.list();
                int v11_0 = v4_0.length;
                int v10_1 = 0;
                while (v10_1 < v11_0) {
                    String v3_0 = v4_0[v10_1];
                    System.out.println(new StringBuilder().append("LuckyPatcher: file found in data dir - ").append(v18_1).append("/").append(v3_0).toString());
                    if ((new java.io.File(new StringBuilder().append(v18_1).append("/").append(v3_0).toString()).isDirectory()) && (new StringBuilder().append(v18_1).append("/").append(v3_0).toString().equals(new StringBuilder().append(v18_1).append("/").append("lib").toString()))) {
                        String[] v6_0 = new java.io.File(new StringBuilder().append(v18_1).append("/").append(v3_0).toString()).list();
                        int v12_26 = v6_0.length;
                        int v9_20 = 0;
                        while (v9_20 < v12_26) {
                            java.io.File v5_0 = v6_0[v9_20];
                            if ((new java.io.File(new StringBuilder().append(v18_1).append("/").append(v3_0).append("/").append(v5_0).toString()).isFile()) && (!v5_0.contains("libjnigraphics.so"))) {
                                v7_1.add(new StringBuilder().append(v18_1).append("/").append(v3_0).append("/").append(v5_0).toString());
                                System.out.println(new StringBuilder().append("LuckyPatcher: found lib - ").append(v18_1).append("/").append(v3_0).append("/").append(v5_0).toString());
                            }
                            v9_20++;
                        }
                    }
                    v10_1++;
                }
            }
        } else {
            if (p20 < 21) {
                if (p18.exists()) {
                    String[] v4_1 = p18.list();
                    int v11_1 = v4_1.length;
                    int v10_2 = 0;
                    while (v10_2 < v11_1) {
                        String v3_1 = v4_1[v10_2];
                        System.out.println(new StringBuilder().append("LuckyPatcher: file found in data dir - ").append(p18).append("/").append(v3_1).toString());
                        if ((new java.io.File(new StringBuilder().append(p18).append("/").append(v3_1).toString()).isDirectory()) && (new StringBuilder().append(p18).append("/").append(v3_1).toString().equals(new StringBuilder().append(p18).append("/").append("lib").toString()))) {
                            String[] v6_1 = new java.io.File(new StringBuilder().append(p18).append("/").append(v3_1).toString()).list();
                            int v12_53 = v6_1.length;
                            int v9_38 = 0;
                            while (v9_38 < v12_53) {
                                java.io.File v5_1 = v6_1[v9_38];
                                if ((new java.io.File(new StringBuilder().append(p18).append("/").append(v3_1).append("/").append(v5_1).toString()).isFile()) && (!v5_1.contains("libjnigraphics.so"))) {
                                    v7_1.add(new StringBuilder().append(p18).append("/").append(v3_1).append("/").append(v5_1).toString());
                                    System.out.println(new StringBuilder().append("LuckyPatcher: found lib - ").append(p18).append("/").append(v3_1).append("/").append(v5_1).toString());
                                }
                                v9_38++;
                            }
                        }
                        v10_2++;
                    }
                }
            } else {
                java.io.File v18_2 = com.chelpus.Utils.getDirs(new java.io.File(p19));
                if (v18_2.exists()) {
                    String[] v4_2 = v18_2.listFiles();
                    int v12_54 = v4_2.length;
                    int v11_2 = 0;
                    while (v11_2 < v12_54) {
                        String v3_2 = v4_2[v11_2];
                        System.out.println(new StringBuilder().append("LuckyPatcher: file found in data dir - ").append(v3_2).toString());
                        if ((v3_2.isDirectory()) && (v3_2.getAbsolutePath().equals(new StringBuilder().append(v18_2).append("/").append("lib").toString()))) {
                            java.io.File[] v2 = v3_2.listFiles();
                            if ((v2 != null) && (v2.length > 0)) {
                                java.io.PrintStream v13_43 = v2.length;
                                int v10_14 = 0;
                                while (v10_14 < v13_43) {
                                    java.io.File[] v8 = v2[v10_14].listFiles();
                                    if ((v8 != null) && (v8.length > 0)) {
                                        int v14_38 = v8.length;
                                        int v9_52 = 0;
                                        while (v9_52 < v14_38) {
                                            java.io.File v5_2 = v8[v9_52];
                                            v7_1.add(v5_2.getAbsolutePath());
                                            com.chelpus.root.utils.move_to_system.index = com.chelpus.Utils.getDirs(v5_2).getAbsolutePath().replace(v18_2.getAbsolutePath(), "");
                                            com.chelpus.root.utils.move_to_system.libDir = new StringBuilder().append("/system/priv-app/").append(com.chelpus.root.utils.move_to_system.pkgName).append(com.chelpus.root.utils.move_to_system.index).append("/").toString();
                                            System.out.println(new StringBuilder().append("libdir").append(com.chelpus.root.utils.move_to_system.libDir).toString());
                                            new java.io.File(com.chelpus.root.utils.move_to_system.libDir).mkdirs();
                                            System.out.println(new StringBuilder().append("LuckyPatcher: found lib - ").append(v5_2.getAbsolutePath()).toString());
                                            v9_52++;
                                        }
                                    }
                                    v10_14++;
                                }
                            }
                        }
                        v11_2++;
                    }
                }
            }
        }
        return v7_1;
    }

    public static void main(String[] p16)
    {
        int v6 = 0;
        String v9 = "/system/app/";
        try {
            if ((new java.io.File("/system/priv-app").exists()) && (new java.io.File("/system/priv-app").list() != null)) {
                v9 = "/system/priv-app/";
            }
            try {
                com.chelpus.Utils.startRootJava(new com.chelpus.root.utils.move_to_system$1());
                com.chelpus.root.utils.move_to_system.dirapp = p16[1];
                com.chelpus.root.utils.move_to_system.datadir = p16[2];
                com.chelpus.root.utils.move_to_system.toolsfiles = p16[3];
                com.chelpus.root.utils.move_to_system.pkgName = p16[0];
                java.io.File v0_1 = new java.io.File(com.chelpus.root.utils.move_to_system.dirapp);
            } catch (Exception v2_3) {
                System.out.println(new StringBuilder().append("LuckyPatcher Error move to System: ").append(v2_3).toString());
                v2_3.printStackTrace();
                com.chelpus.Utils.exitFromRootJava();
                return;
            }
            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.api >= 21) {
                v9 = new StringBuilder().append("/system/priv-app/").append(com.chelpus.root.utils.move_to_system.pkgName).append("/").toString();
                if (!new java.io.File(v9).exists()) {
                    new java.io.File(v9).mkdirs();
                    java.io.File v10_29 = new String[3];
                    v10_29[0] = "chmod";
                    v10_29[1] = "755";
                    v10_29[2] = new StringBuilder().append("/system/priv-app/").append(com.chelpus.root.utils.move_to_system.pkgName).toString();
                    com.chelpus.Utils.cmdParam(v10_29);
                }
            }
            com.chelpus.root.utils.move_to_system.libDir = "/system/lib/";
            java.util.ArrayList v5 = com.chelpus.root.utils.move_to_system.getLibs(new java.io.File(com.chelpus.root.utils.move_to_system.datadir), com.chelpus.root.utils.move_to_system.dirapp, com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.api);
            if (v5.size() > 0) {
                java.io.File v10_34 = v5.iterator();
                while (v10_34.hasNext()) {
                    String v7_1 = new java.io.File(((String) v10_34.next()));
                    if (!new java.io.File(com.chelpus.root.utils.move_to_system.libDir).exists()) {
                        new java.io.File(com.chelpus.root.utils.move_to_system.libDir).mkdirs();
                    }
                    com.chelpus.Utils.setPermissionDir(new StringBuilder().append("/system/priv-app/").append(com.chelpus.root.utils.move_to_system.pkgName).toString(), new StringBuilder().append("/system/priv-app/").append(com.chelpus.root.utils.move_to_system.pkgName).append(com.chelpus.root.utils.move_to_system.index).toString(), "755");
                    com.chelpus.Utils.copyFile(v7_1, new java.io.File(new StringBuilder().append(com.chelpus.root.utils.move_to_system.libDir).append(v7_1.getName()).toString()));
                    com.chelpus.root.utils.move_to_system.run_all(new StringBuilder().append("chmod 0644 ").append(com.chelpus.root.utils.move_to_system.libDir).append(v7_1.getName()).toString());
                    com.chelpus.root.utils.move_to_system.run_all(new StringBuilder().append("chown 0.0 ").append(com.chelpus.root.utils.move_to_system.libDir).append(v7_1.getName()).toString());
                    com.chelpus.root.utils.move_to_system.run_all(new StringBuilder().append("chown 0:0 ").append(com.chelpus.root.utils.move_to_system.libDir).append(v7_1.getName()).toString());
                    if (v7_1.length() != new java.io.File(new StringBuilder().append(com.chelpus.root.utils.move_to_system.libDir).append(v7_1.getName()).toString()).length()) {
                        java.io.File v10_35 = v5.iterator();
                        while (v10_35.hasNext()) {
                            java.io.File v8_3 = new java.io.File(((String) v10_35.next()));
                            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.api >= 21) {
                                if (com.chelpus.root.utils.move_to_system.libDir.startsWith("/system/priv-app/")) {
                                    new com.chelpus.Utils("").deleteFolder(new java.io.File(com.chelpus.root.utils.move_to_system.libDir));
                                }
                            } else {
                                new java.io.File(new StringBuilder().append(com.chelpus.root.utils.move_to_system.libDir).append(v8_3.getName()).toString()).delete();
                            }
                        }
                        System.out.println("In /system space not found!");
                        v6 = 1;
                        break;
                    } else {
                        System.out.println(new StringBuilder().append("LuckyPatcher: copy lib ").append(com.chelpus.root.utils.move_to_system.libDir).append(v7_1.getName()).toString());
                    }
                }
            }
            if (v6 != 0) {
                new java.io.File(new StringBuilder().append(v9).append(com.chelpus.root.utils.move_to_system.pkgName).append(".apk").toString()).delete();
                com.chelpus.Utils.exitFromRootJava();
                return;
            } else {
                if (!new java.io.File(new StringBuilder().append(v9).append(com.chelpus.root.utils.move_to_system.pkgName).append(".odex").toString()).exists()) {
                    String v1 = new StringBuilder().append(v9).append(com.chelpus.root.utils.move_to_system.pkgName).append(".apk").toString();
                    String v7_2 = com.chelpus.root.utils.move_to_system.dirapp;
                    java.io.File v10_50 = new String[3];
                    v10_50[0] = "dd";
                    v10_50[1] = new StringBuilder().append("if=").append(v7_2).toString();
                    v10_50[2] = new StringBuilder().append("of=").append(v1).toString();
                    com.chelpus.Utils.cmdParam(v10_50);
                    if ((!new java.io.File(v1).exists()) || (v0_1.length() != new java.io.File(new StringBuilder().append(v9).append(com.chelpus.root.utils.move_to_system.pkgName).append(".apk").toString()).length())) {
                        java.io.File v10_57 = new String[4];
                        v10_57[0] = "toolbox";
                        v10_57[1] = "dd";
                        v10_57[2] = new StringBuilder().append("if=").append(v7_2).toString();
                        v10_57[3] = new StringBuilder().append("of=").append(v1).toString();
                        com.chelpus.Utils.cmdParam(v10_57);
                    }
                    if ((!new java.io.File(v1).exists()) || (v0_1.length() != new java.io.File(new StringBuilder().append(v9).append(com.chelpus.root.utils.move_to_system.pkgName).append(".apk").toString()).length())) {
                        java.io.File v10_64 = new String[4];
                        v10_64[0] = "busybox";
                        v10_64[1] = "dd";
                        v10_64[2] = new StringBuilder().append("if=").append(v7_2).toString();
                        v10_64[3] = new StringBuilder().append("of=").append(v1).toString();
                        com.chelpus.Utils.cmdParam(v10_64);
                    }
                    if ((!new java.io.File(v1).exists()) || (v0_1.length() != new java.io.File(new StringBuilder().append(v9).append(com.chelpus.root.utils.move_to_system.pkgName).append(".apk").toString()).length())) {
                        java.io.File v10_71 = new String[4];
                        v10_71[0] = new StringBuilder().append(com.chelpus.root.utils.move_to_system.toolsfiles).append("/busybox").toString();
                        v10_71[1] = "dd";
                        v10_71[2] = new StringBuilder().append("if=").append(v7_2).toString();
                        v10_71[3] = new StringBuilder().append("of=").append(v1).toString();
                        com.chelpus.Utils.cmdParam(v10_71);
                    }
                    if ((!new java.io.File(v1).exists()) || (v0_1.length() != new java.io.File(new StringBuilder().append(v9).append(com.chelpus.root.utils.move_to_system.pkgName).append(".apk").toString()).length())) {
                        java.io.File v10_78 = new String[5];
                        v10_78[0] = new StringBuilder().append(com.chelpus.root.utils.move_to_system.toolsfiles).append("/busybox").toString();
                        v10_78[1] = "cp";
                        v10_78[2] = "-fp";
                        v10_78[3] = v7_2;
                        v10_78[4] = v1;
                        com.chelpus.Utils.cmdParam(v10_78);
                    }
                    if ((!new java.io.File(v1).exists()) || (v0_1.length() != new java.io.File(new StringBuilder().append(v9).append(com.chelpus.root.utils.move_to_system.pkgName).append(".apk").toString()).length())) {
                        com.chelpus.Utils.copyFile(v0_1, new java.io.File(new StringBuilder().append(v9).append(com.chelpus.root.utils.move_to_system.pkgName).append(".apk").toString()));
                    }
                } else {
                    new java.io.File(new StringBuilder().append(v9).append(com.chelpus.root.utils.move_to_system.pkgName).append(".odex").toString()).delete();
                }
                if (v0_1.length() != new java.io.File(new StringBuilder().append(v9).append(com.chelpus.root.utils.move_to_system.pkgName).append(".apk").toString()).length()) {
                    new java.io.File(new StringBuilder().append(v9).append(com.chelpus.root.utils.move_to_system.pkgName).append(".apk").toString()).delete();
                    System.out.println("In /system space not found!");
                    java.io.File v10_94 = v5.iterator();
                    while (v10_94.hasNext()) {
                        new java.io.File(new StringBuilder().append(com.chelpus.root.utils.move_to_system.libDir).append(new java.io.File(((String) v10_94.next())).getName()).toString()).delete();
                    }
                    com.chelpus.Utils.exitFromRootJava();
                    return;
                } else {
                    if (com.chelpus.root.utils.move_to_system.dirapp.startsWith("/mnt/")) {
                        java.io.File v10_98 = new String[3];
                        v10_98[0] = "pm";
                        v10_98[1] = "uninstall";
                        v10_98[2] = com.chelpus.root.utils.move_to_system.pkgName;
                        com.chelpus.Utils.cmdParam(v10_98);
                    } else {
                        v0_1.delete();
                        com.chelpus.root.utils.move_to_system.removeLibs(new java.io.File(com.chelpus.root.utils.move_to_system.datadir), com.chelpus.root.utils.move_to_system.dirapp, com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.api);
                    }
                    com.chelpus.root.utils.move_to_system.run_all(new StringBuilder().append("chmod 0644 ").append(v9).append(com.chelpus.root.utils.move_to_system.pkgName).append(".apk").toString());
                    com.chelpus.root.utils.move_to_system.run_all(new StringBuilder().append("chown 0.0 ").append(v9).append(com.chelpus.root.utils.move_to_system.pkgName).append(".apk").toString());
                    com.chelpus.root.utils.move_to_system.run_all(new StringBuilder().append("chown 0:0 ").append(v9).append(com.chelpus.root.utils.move_to_system.pkgName).append(".apk").toString());
                    com.chelpus.Utils.exitFromRootJava();
                    return;
                }
            }
        } catch (Exception v2_0) {
            v2_0.printStackTrace();
        }
    }

    public static void removeLibs(java.io.File p13, String p14, int p15)
    {
        int v5 = 0;
        if (p15 >= 21) {
            java.io.File v13_1 = com.chelpus.Utils.getDirs(new java.io.File(p14));
            if (com.chelpus.Utils.getDirs(new java.io.File(p14)).exists()) {
                String[] v2_0 = v13_1.listFiles();
                int v6_7 = v2_0.length;
                while (v5 < v6_7) {
                    String v1_0 = v2_0[v5];
                    System.out.println(new StringBuilder().append("LuckyPatcher: file found in data dir - ").append(v13_1).append("/").append(v1_0).toString());
                    if ((v1_0.isDirectory()) && (v1_0.getAbsolutePath().endsWith("/lib"))) {
                        try {
                            new com.chelpus.Utils("").deleteFolder(v1_0);
                        } catch (java.io.IOException v0) {
                            v0.printStackTrace();
                        }
                    }
                    v5++;
                }
            }
        } else {
            if ((!p14.startsWith("/mnt/")) && (p13.exists())) {
                String[] v2_1 = p13.list();
                String v8_9 = v2_1.length;
                com.chelpus.Utils v7_6 = 0;
                while (v7_6 < v8_9) {
                    String v1_1 = v2_1[v7_6];
                    System.out.println(new StringBuilder().append("LuckyPatcher: file found in data dir - ").append(p13).append("/").append(v1_1).toString());
                    if ((new java.io.File(new StringBuilder().append(p13).append("/").append(v1_1).toString()).isDirectory()) && (new StringBuilder().append(p13).append("/").append(v1_1).toString().equals(new StringBuilder().append(p13).append("/").append("lib").toString()))) {
                        String[] v4 = new java.io.File(new StringBuilder().append(p13).append("/").append(v1_1).toString()).list();
                        if ((v4 != null) && (v4.length > 0)) {
                            int v9_28 = v4.length;
                            int v6_25 = 0;
                            while (v6_25 < v9_28) {
                                String v3 = v4[v6_25];
                                if (new java.io.File(new StringBuilder().append(p13).append("/").append(v1_1).append("/").append(v3).toString()).isFile()) {
                                    new java.io.File(new StringBuilder().append(p13).append("/").append(v1_1).append("/").append(v3).toString()).delete();
                                    System.out.println(new StringBuilder().append("LuckyPatcher: remove lib - ").append(p13).append("/").append(v1_1).append("/").append(v3).toString());
                                }
                                v6_25++;
                            }
                        }
                        new java.io.File(new StringBuilder().append(p13).append("/").append(v1_1).toString()).delete();
                    }
                    v7_6++;
                }
            }
        }
        return;
    }

    private static void run_all(String p4)
    {
        try {
            Process v0_0 = Runtime.getRuntime().exec(new StringBuilder().append(p4).append("\n").toString());
            v0_0.waitFor();
            v0_0.destroy();
            try {
                Process v0_1 = Runtime.getRuntime().exec(new StringBuilder().append("toolbox ").append(p4).append("\n").toString());
                v0_1.waitFor();
                v0_1.destroy();
            } catch (Exception v1) {
            }
            try {
                Process v0_2 = Runtime.getRuntime().exec(new StringBuilder().append("/system/bin/failsafe/toolbox ").append(p4).append("\n").toString());
                v0_2.waitFor();
                v0_2.destroy();
            } catch (Exception v1) {
            }
            try {
                Process v0_3 = Runtime.getRuntime().exec(new StringBuilder().append("busybox ").append(p4).append("\n").toString());
                v0_3.waitFor();
                v0_3.destroy();
            } catch (Exception v1) {
            }
            try {
                Process v0_4 = Runtime.getRuntime().exec(new StringBuilder().append(com.chelpus.root.utils.move_to_system.toolsfiles).append("/busybox ").append(p4).append("\n").toString());
                v0_4.waitFor();
                v0_4.destroy();
            } catch (Exception v1) {
            }
            return;
        } catch (Exception v1) {
        }
    }
}
