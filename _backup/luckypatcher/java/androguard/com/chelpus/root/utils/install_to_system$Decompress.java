package com.chelpus.root.utils;
public class install_to_system$Decompress {
    private String _location;
    private String _zipFile;

    public install_to_system$Decompress(String p2, String p3)
    {
        this._zipFile = p2;
        this._location = p3;
        this._dirChecker("");
        return;
    }

    private void _dirChecker(String p4)
    {
        java.io.File v0_1 = new java.io.File(new StringBuilder().append(this._location).append(p4).toString());
        if (!v0_1.isDirectory()) {
            v0_1.mkdirs();
        }
        return;
    }

    public void unzip()
    {
        try {
            java.io.FileInputStream v10_1 = new java.io.FileInputStream(this._zipFile);
            java.util.zip.ZipInputStream v16 = new java.util.zip.ZipInputStream;
            v16(v10_1);
        } catch (Exception v6) {
            System.out.println(new StringBuilder().append("Decompressunzip ").append(v6).toString());
            v6.printStackTrace();
            try {
                net.lingala.zip4j.core.ZipFile v17_1 = new net.lingala.zip4j.core.ZipFile(this._zipFile);
                int v18_26 = v17_1.getFileHeaders().iterator();
            } catch (Exception v7_0) {
                v7_0.printStackTrace();
                return;
            } catch (Exception v7_1) {
                v7_1.printStackTrace();
                return;
            }
        }
        while(true) {
            java.util.zip.ZipEntry v15 = v16.getNextEntry();
            if (v15 == null) {
                break;
            }
            if (!v15.isDirectory()) {
                if ((v15.getName().endsWith(".so")) && (!v15.getName().contains("libjnigraphics.so"))) {
                    String[] v14 = v15.getName().split("\\/+");
                    String v5 = "";
                    int v12 = 0;
                    while (v12 < (v14.length - 1)) {
                        if (!v14[v12].equals("")) {
                            v5 = new StringBuilder().append(v5).append("/").append(v14[v12]).toString();
                        }
                        v12++;
                    }
                    this._dirChecker(v5);
                    java.io.FileOutputStream v11_1 = new java.io.FileOutputStream(new StringBuilder().append(this._location).append(v15.getName()).toString());
                    byte[] v4 = new byte[1024];
                    while(true) {
                        int v13 = v16.read(v4);
                        if (v13 == -1) {
                            break;
                        }
                        v11_1.write(v4, 0, v13);
                    }
                    v16.closeEntry();
                    v11_1.close();
                }
            } else {
                this._dirChecker(v15.getName());
            }
        }
        v16.close();
        v10_1.close();
        return;
    }
}
