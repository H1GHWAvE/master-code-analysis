package com.chelpus.root.utils;
public class uninstall {
    public static String datadir;
    public static String dirapp;
    public static boolean odexpatch;
    public static boolean system;

    static uninstall()
    {
        com.chelpus.root.utils.uninstall.dirapp = "/data/app/";
        com.chelpus.root.utils.uninstall.datadir = "/data/data/";
        com.chelpus.root.utils.uninstall.system = 0;
        com.chelpus.root.utils.uninstall.odexpatch = 0;
        return;
    }

    public uninstall()
    {
        return;
    }

    public static java.util.ArrayList getLibs(java.io.File p18)
    {
        int v10 = 0;
        int v8 = 0;
        java.util.ArrayList v12_1 = new java.util.ArrayList();
        try {
            java.io.FileInputStream v9_1 = new java.io.FileInputStream(p18);
        } catch (boolean v15_0) {
            if (v10 != 0) {
                try {
                    v10.close();
                } catch (boolean v16) {
                }
            }
            if (v8 != 0) {
                try {
                    v8.close();
                } catch (boolean v16) {
                }
            }
            throw v15_0;
        } catch (java.io.IOException v2) {
            try {
                boolean v15_16 = new net.lingala.zip4j.core.ZipFile(p18).getFileHeaders().iterator();
            } catch (Exception v3_1) {
                v3_1.printStackTrace();
                if (v10 != 0) {
                    try {
                        v10.close();
                    } catch (boolean v15) {
                    }
                }
                if (v8 == 0) {
                    return v12_1;
                } else {
                    try {
                        v8.close();
                    } catch (boolean v15) {
                    }
                    return v12_1;
                }
                java.util.zip.ZipEntry v4 = java.util.zip.ZipInputStream v11_1.getNextEntry();
                while (v4 != null) {
                    if (v4.getName().startsWith("lib/")) {
                        String[] v13_0 = v4.getName().split("/");
                        String v7_0 = v13_0[(v13_0.length - 1)];
                        if ((v12_1.isEmpty()) && ((!v7_0.equals("")) && (!v7_0.contains("libjnigraphics.so")))) {
                            v12_1.add(v7_0);
                        }
                        if ((!v12_1.contains(v7_0)) && ((!v7_0.equals("")) && (!v7_0.contains("libjnigraphics.so")))) {
                            v12_1.add(v7_0);
                        }
                    }
                }
                if (v11_1 != null) {
                    try {
                        v11_1.close();
                    } catch (boolean v15) {
                    }
                }
                if (v9_1 == null) {
                    return v12_1;
                } else {
                    try {
                        v9_1.close();
                    } catch (boolean v15) {
                    }
                    return v12_1;
                }
            } catch (Exception v3_0) {
                v3_0.printStackTrace();
            }
        }
        try {
            v11_1 = new java.util.zip.ZipInputStream(v9_1);
            try {
                v4 = v11_1.getNextEntry();
            } catch (boolean v15_0) {
                v8 = v9_1;
                v10 = v11_1;
            } catch (java.io.IOException v2) {
                v8 = v9_1;
                v10 = v11_1;
            }
        } catch (java.io.IOException v2) {
            v8 = v9_1;
        } catch (boolean v15_0) {
            v8 = v9_1;
        }
    }

    public static void main(String[] p13)
    {
        try {
            com.chelpus.Utils.startRootJava(new com.chelpus.root.utils.uninstall$1());
            com.chelpus.root.utils.uninstall.dirapp = p13[1];
            com.chelpus.root.utils.uninstall.system = 1;
            com.chelpus.root.utils.uninstall.datadir = p13[2];
            java.io.File v1_1 = new java.io.File(com.chelpus.root.utils.uninstall.dirapp);
            java.io.File v2_1 = new java.io.File(com.chelpus.Utils.getPlaceForOdex(com.chelpus.root.utils.uninstall.dirapp, 1));
            System.out.println("Start getLibs!");
            java.util.ArrayList v7 = com.chelpus.root.utils.uninstall.getLibs(v1_1);
            System.out.println("Start delete lib!");
        } catch (Exception v4_2) {
            System.out.println(new StringBuilder().append("LuckyPatcher Error uninstall: ").append(v4_2).toString());
            v4_2.printStackTrace();
            com.chelpus.Utils.exitFromRootJava();
            return;
        }
        if (!v7.isEmpty()) {
            java.io.PrintStream v9_13 = v7.iterator();
            while (v9_13.hasNext()) {
                java.io.File v6_1 = new java.io.File(new StringBuilder().append("/system/lib/").append(((String) v9_13.next())).toString());
                if (v6_1.exists()) {
                    v6_1.delete();
                }
            }
        }
        System.out.println("Start delete data directory!");
        com.chelpus.Utils v8_1 = new com.chelpus.Utils("uninstall system");
        try {
            System.out.println("Start delete dir!");
            v8_1.deleteFolder(new java.io.File(com.chelpus.root.utils.uninstall.datadir));
            v8_1.deleteFolder(new java.io.File(com.chelpus.root.utils.uninstall.datadir.replace("/data/data/", "/dbdata/databases/")));
        } catch (Exception v4_0) {
            System.out.println(new StringBuilder().append("LuckyPatcher Error uninstall: ").append(v4_0).toString());
            v4_0.printStackTrace();
        }
        System.out.println("Start delete dc!");
        try {
            java.io.File v3 = com.chelpus.Utils.getFileDalvikCache(com.chelpus.root.utils.uninstall.dirapp);
        } catch (Exception v4_1) {
            System.out.println(new StringBuilder().append("Error: Exception e").append(v4_1.toString()).toString());
            v4_1.printStackTrace();
            System.out.println("Start delete apk!");
            java.io.File v0_1 = new java.io.File(com.chelpus.root.utils.uninstall.dirapp);
            if (!v0_1.exists()) {
                com.chelpus.Utils.exitFromRootJava();
                return;
            } else {
                v0_1.delete();
                System.out.println(new StringBuilder().append("Delete apk:").append(com.chelpus.root.utils.uninstall.dirapp).toString());
                com.chelpus.Utils.exitFromRootJava();
                return;
            }
        }
        if (v3 == null) {
            System.out.println("dalvik-cache not found.");
        } else {
            v3.delete();
            System.out.println(new StringBuilder().append("Dalvik-cache ").append(v3).append(" deleted.").toString());
        }
        System.out.println("Start delete odex.");
        if (!v2_1.exists()) {
        } else {
            v2_1.delete();
        }
    }
}
