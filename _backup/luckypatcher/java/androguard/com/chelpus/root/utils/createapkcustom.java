package com.chelpus.root.utils;
public class createapkcustom {
    static final int BUFFER = 2048;
    private static final int all = 4;
    public static String appdir = "None";
    private static final int armeabi = 0;
    private static final int armeabiv7a = 1;
    private static final int beginTag = 0;
    public static java.util.ArrayList classesFiles = None;
    private static final int classesTag = 1;
    public static java.io.File crkapk = None;
    public static String dir = "None";
    public static String dir2 = "None";
    private static final int endTag = 4;
    private static final int fileInApkTag = 10;
    public static boolean goodResult = False;
    private static String group = "None";
    private static final int libTagALL = 2;
    private static final int libTagARMEABI = 6;
    private static final int libTagARMEABIV7A = 7;
    private static final int libTagMIPS = 8;
    private static final int libTagx86 = 9;
    private static java.util.ArrayList libs = None;
    public static java.io.File localFile2 = None;
    public static boolean manualpatch = False;
    private static final int mips = 2;
    public static boolean multidex = False;
    public static boolean multilib_patch = False;
    public static String packageName = "None";
    private static final int packageTag = 5;
    private static java.util.ArrayList pat = None;
    private static java.util.ArrayList patchedLibs = None;
    public static boolean patchteil = False;
    private static java.io.PrintStream print = None;
    public static String sddir = "None";
    private static java.util.ArrayList search = None;
    private static java.util.ArrayList ser = None;
    public static int tag = 0;
    public static String tooldir = "None";
    public static boolean unpack = False;
    private static final int x86 = 3;

    static createapkcustom()
    {
        com.chelpus.root.utils.createapkcustom.pat = 0;
        com.chelpus.root.utils.createapkcustom.ser = 0;
        com.chelpus.root.utils.createapkcustom.search = 0;
        com.chelpus.root.utils.createapkcustom.patchteil = 0;
        com.chelpus.root.utils.createapkcustom.unpack = 0;
        com.chelpus.root.utils.createapkcustom.manualpatch = 0;
        com.chelpus.root.utils.createapkcustom.dir = "/sdcard/";
        com.chelpus.root.utils.createapkcustom.dir2 = "/sdcard/";
        com.chelpus.root.utils.createapkcustom.sddir = "/sdcard/";
        com.chelpus.root.utils.createapkcustom.appdir = "/sdcard/";
        com.chelpus.root.utils.createapkcustom.tooldir = "/sdcard/";
        com.chelpus.root.utils.createapkcustom.packageName = "";
        com.chelpus.root.utils.createapkcustom.libs = new java.util.ArrayList();
        com.chelpus.root.utils.createapkcustom.patchedLibs = new java.util.ArrayList();
        com.chelpus.root.utils.createapkcustom.group = "";
        com.chelpus.root.utils.createapkcustom.classesFiles = new java.util.ArrayList();
        com.chelpus.root.utils.createapkcustom.multidex = 0;
        com.chelpus.root.utils.createapkcustom.goodResult = 0;
        com.chelpus.root.utils.createapkcustom.multilib_patch = 0;
        return;
    }

    public createapkcustom()
    {
        return;
    }

    static synthetic java.io.PrintStream access$000()
    {
        return com.chelpus.root.utils.createapkcustom.print;
    }

    private static final void calcChecksum(byte[] p5, int p6)
    {
        java.util.zip.Adler32 v1_1 = new java.util.zip.Adler32();
        v1_1.update(p5, 12, (p5.length - (p6 + 12)));
        int v0 = ((int) v1_1.getValue());
        p5[(p6 + 8)] = ((byte) v0);
        p5[(p6 + 9)] = ((byte) (v0 >> 8));
        p5[(p6 + 10)] = ((byte) (v0 >> 16));
        p5[(p6 + 11)] = ((byte) (v0 >> 24));
        return;
    }

    private static final void calcSignature(byte[] p8, int p9)
    {
        try {
            java.security.MessageDigest v2 = java.security.MessageDigest.getInstance("SHA-1");
            v2.update(p8, 32, (p8.length - (p9 + 32)));
            try {
                int v0 = v2.digest(p8, (p9 + 12), 20);
            } catch (java.security.DigestException v1) {
                throw new RuntimeException(v1);
            }
            if (v0 == 20) {
                return;
            } else {
                throw new RuntimeException(new StringBuilder().append("unexpected digest write:").append(v0).append("bytes").toString());
            }
        } catch (java.security.NoSuchAlgorithmException v3) {
            throw new RuntimeException(v3);
        }
    }

    public static void clearTemp()
    {
        try {
            java.io.File v1_1 = new java.io.File(new StringBuilder().append(com.chelpus.root.utils.createapkcustom.sddir).append("/Modified/classes.dex.apk").toString());
        } catch (Exception v0) {
            com.chelpus.root.utils.createapkcustom.print.println(new StringBuilder().append("").append(v0.toString()).toString());
            return;
        }
        if (v1_1.exists()) {
            v1_1.delete();
        }
        new com.chelpus.Utils("createcustompatch").deleteFolder(new java.io.File(new StringBuilder().append(com.chelpus.root.utils.createapkcustom.sddir).append("/tmp/").toString()));
        return;
    }

    public static String extractFile(java.io.File p5, String p6)
    {
        return new com.chelpus.root.utils.createapkcustom$Decompress(p5.getAbsolutePath(), new StringBuilder().append(com.chelpus.root.utils.createapkcustom.sddir).append("/tmp/").toString()).unzip(p6);
    }

    public static void extractLibs(java.io.File p6)
    {
        String v3 = p6.getAbsolutePath();
        String v2 = new StringBuilder().append(com.chelpus.root.utils.createapkcustom.sddir).append("/tmp/").toString();
        if (!new java.io.File(new StringBuilder().append(com.chelpus.root.utils.createapkcustom.sddir).append("/tmp/lib/").toString()).exists()) {
            new com.chelpus.root.utils.createapkcustom$Decompress(v3, v2).unzip();
        }
        return;
    }

    public static void fixadler(java.io.File p5)
    {
        try {
            java.io.FileInputStream v2_1 = new java.io.FileInputStream(p5);
            byte[] v0 = new byte[v2_1.available()];
            v2_1.read(v0);
            com.chelpus.root.utils.createapkcustom.calcSignature(v0, 0);
            com.chelpus.root.utils.createapkcustom.calcChecksum(v0, 0);
            v2_1.close();
            java.io.FileOutputStream v3_1 = new java.io.FileOutputStream(p5);
            v3_1.write(v0);
            v3_1.close();
        } catch (Exception v1) {
            v1.printStackTrace();
        }
        return;
    }

    public static void getClassesDex()
    {
        try {
            java.io.File v0_1 = new java.io.File(com.chelpus.root.utils.createapkcustom.appdir);
            com.chelpus.root.utils.createapkcustom.crkapk = new java.io.File(new StringBuilder().append(com.chelpus.root.utils.createapkcustom.sddir).append("/Modified/").append(com.chelpus.root.utils.createapkcustom.packageName).append(".apk").toString());
            com.chelpus.Utils.copyFile(v0_1, com.chelpus.root.utils.createapkcustom.crkapk);
            com.chelpus.root.utils.createapkcustom.unzip(com.chelpus.root.utils.createapkcustom.crkapk);
        } catch (java.io.FileNotFoundException v3) {
            com.chelpus.root.utils.createapkcustom.print.println("Error LP: unzip classes.dex fault!\n\n");
            return;
        } catch (Exception v2) {
            com.chelpus.root.utils.createapkcustom.print.println(new StringBuilder().append("Extract classes.dex error: ").append(v2.toString()).toString());
            return;
        }
        if ((com.chelpus.root.utils.createapkcustom.classesFiles != null) && (com.chelpus.root.utils.createapkcustom.classesFiles.size() != 0)) {
            if ((com.chelpus.root.utils.createapkcustom.classesFiles == null) || (com.chelpus.root.utils.createapkcustom.classesFiles.size() <= 0)) {
                return;
            } else {
                java.io.FileNotFoundException v4_12 = com.chelpus.root.utils.createapkcustom.classesFiles.iterator();
                while (v4_12.hasNext()) {
                    if (!((java.io.File) v4_12.next()).exists()) {
                        throw new java.io.FileNotFoundException();
                    }
                }
                return;
            }
        } else {
            throw new java.io.FileNotFoundException();
        }
    }

    public static String getFileFromApk(String p5)
    {
        try {
            java.io.File v0_1 = new java.io.File(com.chelpus.root.utils.createapkcustom.appdir);
            com.chelpus.root.utils.createapkcustom.crkapk = new java.io.File(new StringBuilder().append(com.chelpus.root.utils.createapkcustom.sddir).append("/Modified/").append(com.chelpus.root.utils.createapkcustom.packageName).append(".apk").toString());
        } catch (Exception v1) {
            com.chelpus.root.utils.createapkcustom.print.println(new StringBuilder().append("Lib select error: ").append(v1.toString()).toString());
            String v2_7 = "";
            return v2_7;
        }
        if (!com.chelpus.root.utils.createapkcustom.crkapk.exists()) {
            com.chelpus.Utils.copyFile(v0_1, com.chelpus.root.utils.createapkcustom.crkapk);
        }
        v2_7 = com.chelpus.root.utils.createapkcustom.extractFile(com.chelpus.root.utils.createapkcustom.crkapk, p5);
        return v2_7;
    }

    public static String main(String[] p55)
    {
        com.android.vending.billing.InAppBillingService.LUCK.LogOutputStream v39 = new com.android.vending.billing.InAppBillingService.LUCK.LogOutputStream;
        v39("System.out");
        com.chelpus.root.utils.createapkcustom.print = new java.io.PrintStream(v39);
        com.chelpus.root.utils.createapkcustom.print.println("SU Java-Code Running!");
        com.chelpus.root.utils.createapkcustom.patchedLibs.clear();
        com.chelpus.root.utils.createapkcustom.packageName = p55[0];
        com.chelpus.root.utils.createapkcustom.appdir = p55[2];
        com.chelpus.root.utils.createapkcustom.sddir = p55[3];
        com.chelpus.root.utils.createapkcustom.tooldir = p55[4];
        com.chelpus.root.utils.createapkcustom.clearTemp();
        com.chelpus.root.utils.createapkcustom.manualpatch = 0;
        String v26 = "";
        String v12 = "";
        int v23 = 0;
        int v22 = 0;
        com.chelpus.root.utils.createapkcustom.getClassesDex();
        try {
            java.io.FileInputStream v27 = new java.io.FileInputStream;
            v27(p55[1]);
            java.io.InputStreamReader v28 = new java.io.InputStreamReader;
            v28(v27, "UTF-8");
            java.io.BufferedReader v13_1 = new java.io.BufferedReader(v28);
            java.util.ArrayList v0_5 = new String[1000];
            String[] v47 = v0_5;
            java.util.ArrayList v0_6 = new String[1];
            v0_6[0] = "";
            byte[] v3 = 0;
            int[] v4 = 0;
            int v42_0 = 1;
            int v45 = 1;
            int v32 = 0;
            int v11 = 0;
            int v36 = 0;
            int v25 = 0;
            com.chelpus.root.utils.createapkcustom.pat = new java.util.ArrayList();
            com.chelpus.root.utils.createapkcustom.ser = new java.util.ArrayList();
            com.chelpus.root.utils.createapkcustom.search = new java.util.ArrayList();
            int v40 = 0;
        } catch (java.io.IOException v20) {
            com.chelpus.root.utils.createapkcustom.print.println(new StringBuilder().append("Patch process Error LP: ").append(v20).toString());
            com.chelpus.root.utils.createapkcustom.clearTemp();
            int v42_1 = v39.allresult;
            try {
                v39.close();
            } catch (Exception v18_6) {
                v18_6.printStackTrace();
            }
            return v42_1;
        } catch (java.io.FileNotFoundException v19) {
            com.chelpus.root.utils.createapkcustom.print.println("Custom Patch not Found!\n");
        } catch (InterruptedException v21) {
            v21.printStackTrace();
        }
        while(true) {
            String v16 = v13_1.readLine();
            if (v16 == null) {
                break;
            }
            if (!v16.equals("")) {
                v16 = com.chelpus.Utils.apply_TAGS(v16, com.chelpus.root.utils.createapkcustom.packageName);
            }
            v47[v40] = v16;
            if ((v11 != 0) && ((v47[v40].contains("[")) || ((v47[v40].contains("]")) || (v47[v40].contains("{"))))) {
                com.chelpus.root.utils.createapkcustom.print.println(new StringBuilder().append("").append(v12).append("\n").toString());
                v11 = 0;
            }
            if (v11 != 0) {
                v12 = new StringBuilder().append(v12).append("\n").append(v47[v40]).toString();
            }
            if ((v47[v40].contains("[")) && (v47[v40].contains("]"))) {
                switch (com.chelpus.root.utils.createapkcustom.tag) {
                    case 1:
                        if (com.chelpus.root.utils.createapkcustom.pat.size() > 0) {
                            com.chelpus.root.utils.createapkcustom.getClassesDex();
                            if ((com.chelpus.root.utils.createapkcustom.classesFiles != null) && (com.chelpus.root.utils.createapkcustom.classesFiles.size() > 0)) {
                                if (com.chelpus.root.utils.createapkcustom.classesFiles.size() > 1) {
                                    com.chelpus.root.utils.createapkcustom.multidex = 1;
                                }
                                boolean v2_106 = com.chelpus.root.utils.createapkcustom.classesFiles.iterator();
                                while (v2_106.hasNext()) {
                                    java.io.File v14_1 = ((java.io.File) v2_106.next());
                                    com.chelpus.root.utils.createapkcustom.localFile2 = v14_1;
                                    com.chelpus.root.utils.createapkcustom.print.println("---------------------------------");
                                    com.chelpus.root.utils.createapkcustom.print.println(new StringBuilder().append("Patch for ").append(v14_1.getName()).append(":").toString());
                                    com.chelpus.root.utils.createapkcustom.print.println("---------------------------------\n");
                                    if (!com.chelpus.root.utils.createapkcustom.manualpatch) {
                                        v42_0 = com.chelpus.root.utils.createapkcustom.patchProcess(com.chelpus.root.utils.createapkcustom.pat);
                                    }
                                    if (v42_0 == 0) {
                                        v45 = 0;
                                    }
                                }
                            }
                            com.chelpus.root.utils.createapkcustom.multidex = 0;
                            com.chelpus.root.utils.createapkcustom.goodResult = 0;
                            com.chelpus.root.utils.createapkcustom.ser.clear();
                            com.chelpus.root.utils.createapkcustom.pat.clear();
                            com.chelpus.root.utils.createapkcustom.tag = 200;
                        }
                    case 2:
                        boolean v2_91 = com.chelpus.root.utils.createapkcustom.libs.iterator();
                        while (v2_91.hasNext()) {
                            String v31_9 = ((String) v2_91.next());
                            com.chelpus.root.utils.createapkcustom.localFile2 = new java.io.File(v31_9);
                            com.chelpus.root.utils.createapkcustom.print.println("---------------------------");
                            com.chelpus.root.utils.createapkcustom.print.println(new StringBuilder().append("Patch for libraries \n").append(com.chelpus.root.utils.createapkcustom.localFile2.getPath().replace(new StringBuilder().append(com.chelpus.root.utils.createapkcustom.sddir).append("/tmp").toString(), "")).append(":").toString());
                            com.chelpus.root.utils.createapkcustom.print.println("---------------------------\n");
                            if (!com.chelpus.root.utils.createapkcustom.manualpatch) {
                                v42_0 = com.chelpus.root.utils.createapkcustom.patchProcess(com.chelpus.root.utils.createapkcustom.pat);
                            }
                            if (v42_0 == 0) {
                                v45 = 0;
                            }
                            com.chelpus.root.utils.createapkcustom.patchedLibs.add(v31_9);
                        }
                        com.chelpus.root.utils.createapkcustom.multilib_patch = 0;
                        com.chelpus.root.utils.createapkcustom.goodResult = 0;
                        com.chelpus.root.utils.createapkcustom.ser.clear();
                        com.chelpus.root.utils.createapkcustom.pat.clear();
                        com.chelpus.root.utils.createapkcustom.tag = 200;
                    case 3:
                    case 4:
                    case 5:
                    default:
                        break;
                    case 3:
                    case 4:
                    case 5:
                        break;
                    case 3:
                    case 4:
                    case 5:
                        break;
                    case 6:
                        boolean v2_84 = com.chelpus.root.utils.createapkcustom.libs.iterator();
                        while (v2_84.hasNext()) {
                            String v31_7 = ((String) v2_84.next());
                            com.chelpus.root.utils.createapkcustom.localFile2 = new java.io.File(v31_7);
                            com.chelpus.root.utils.createapkcustom.print.println("--------------------------------");
                            com.chelpus.root.utils.createapkcustom.print.println(new StringBuilder().append("Patch for (armeabi) libraries \n").append(com.chelpus.root.utils.createapkcustom.localFile2.getPath().replace(new StringBuilder().append(com.chelpus.root.utils.createapkcustom.sddir).append("/tmp").toString(), "")).append(":").toString());
                            com.chelpus.root.utils.createapkcustom.print.println("--------------------------------\n");
                            if (!com.chelpus.root.utils.createapkcustom.manualpatch) {
                                v42_0 = com.chelpus.root.utils.createapkcustom.patchProcess(com.chelpus.root.utils.createapkcustom.pat);
                            }
                            if (v42_0 == 0) {
                                v45 = 0;
                            }
                            com.chelpus.root.utils.createapkcustom.patchedLibs.add(v31_7);
                        }
                        com.chelpus.root.utils.createapkcustom.multilib_patch = 0;
                        com.chelpus.root.utils.createapkcustom.goodResult = 0;
                        com.chelpus.root.utils.createapkcustom.ser.clear();
                        com.chelpus.root.utils.createapkcustom.pat.clear();
                        com.chelpus.root.utils.createapkcustom.tag = 200;
                        break;
                    case 7:
                        boolean v2_77 = com.chelpus.root.utils.createapkcustom.libs.iterator();
                        while (v2_77.hasNext()) {
                            String v31_5 = ((String) v2_77.next());
                            com.chelpus.root.utils.createapkcustom.localFile2 = new java.io.File(v31_5);
                            com.chelpus.root.utils.createapkcustom.print.println("---------------------------------------");
                            com.chelpus.root.utils.createapkcustom.print.println(new StringBuilder().append("Patch for (armeabi-v7a) libraries \n").append(com.chelpus.root.utils.createapkcustom.localFile2.getPath().replace(new StringBuilder().append(com.chelpus.root.utils.createapkcustom.sddir).append("/tmp").toString(), "")).append(":").toString());
                            com.chelpus.root.utils.createapkcustom.print.println("---------------------------------------\n");
                            if (!com.chelpus.root.utils.createapkcustom.manualpatch) {
                                v42_0 = com.chelpus.root.utils.createapkcustom.patchProcess(com.chelpus.root.utils.createapkcustom.pat);
                            }
                            if (v42_0 == 0) {
                                v45 = 0;
                            }
                            com.chelpus.root.utils.createapkcustom.patchedLibs.add(v31_5);
                        }
                        com.chelpus.root.utils.createapkcustom.multilib_patch = 0;
                        com.chelpus.root.utils.createapkcustom.goodResult = 0;
                        com.chelpus.root.utils.createapkcustom.ser.clear();
                        com.chelpus.root.utils.createapkcustom.pat.clear();
                        com.chelpus.root.utils.createapkcustom.tag = 200;
                        break;
                    case 8:
                        boolean v2_70 = com.chelpus.root.utils.createapkcustom.libs.iterator();
                        while (v2_70.hasNext()) {
                            String v31_3 = ((String) v2_70.next());
                            com.chelpus.root.utils.createapkcustom.localFile2 = new java.io.File(v31_3);
                            com.chelpus.root.utils.createapkcustom.print.println("---------------------------");
                            com.chelpus.root.utils.createapkcustom.print.println(new StringBuilder().append("Patch for (MIPS) libraries \n").append(com.chelpus.root.utils.createapkcustom.localFile2.getPath().replace(new StringBuilder().append(com.chelpus.root.utils.createapkcustom.sddir).append("/tmp").toString(), "")).append(":").toString());
                            com.chelpus.root.utils.createapkcustom.print.println("---------------------------\n");
                            if (!com.chelpus.root.utils.createapkcustom.manualpatch) {
                                v42_0 = com.chelpus.root.utils.createapkcustom.patchProcess(com.chelpus.root.utils.createapkcustom.pat);
                            }
                            if (v42_0 == 0) {
                                v45 = 0;
                            }
                            com.chelpus.root.utils.createapkcustom.patchedLibs.add(v31_3);
                        }
                        com.chelpus.root.utils.createapkcustom.multilib_patch = 0;
                        com.chelpus.root.utils.createapkcustom.goodResult = 0;
                        com.chelpus.root.utils.createapkcustom.ser.clear();
                        com.chelpus.root.utils.createapkcustom.pat.clear();
                        com.chelpus.root.utils.createapkcustom.tag = 200;
                        break;
                    case 9:
                        boolean v2_63 = com.chelpus.root.utils.createapkcustom.libs.iterator();
                        while (v2_63.hasNext()) {
                            String v31_1 = ((String) v2_63.next());
                            com.chelpus.root.utils.createapkcustom.localFile2 = new java.io.File(v31_1);
                            com.chelpus.root.utils.createapkcustom.print.println("---------------------------");
                            com.chelpus.root.utils.createapkcustom.print.println(new StringBuilder().append("Patch for (x86) libraries \n").append(com.chelpus.root.utils.createapkcustom.localFile2.getPath().replace(new StringBuilder().append(com.chelpus.root.utils.createapkcustom.sddir).append("/tmp").toString(), "")).append(":").toString());
                            com.chelpus.root.utils.createapkcustom.print.println("---------------------------\n");
                            if (!com.chelpus.root.utils.createapkcustom.manualpatch) {
                                v42_0 = com.chelpus.root.utils.createapkcustom.patchProcess(com.chelpus.root.utils.createapkcustom.pat);
                            }
                            if (v42_0 == 0) {
                                v45 = 0;
                            }
                            com.chelpus.root.utils.createapkcustom.patchedLibs.add(v31_1);
                        }
                        com.chelpus.root.utils.createapkcustom.multilib_patch = 0;
                        com.chelpus.root.utils.createapkcustom.goodResult = 0;
                        com.chelpus.root.utils.createapkcustom.ser.clear();
                        com.chelpus.root.utils.createapkcustom.pat.clear();
                        com.chelpus.root.utils.createapkcustom.tag = 200;
                        break;
                    case 10:
                        com.chelpus.root.utils.createapkcustom.print.println("---------------------------");
                        com.chelpus.root.utils.createapkcustom.print.println(new StringBuilder().append("Patch for file from apk\n").append(com.chelpus.root.utils.createapkcustom.localFile2.getPath().replace(new StringBuilder().append(com.chelpus.root.utils.createapkcustom.sddir).append("/tmp").toString(), "")).append(":").toString());
                        com.chelpus.root.utils.createapkcustom.print.println("---------------------------\n");
                        if (!com.chelpus.root.utils.createapkcustom.manualpatch) {
                            v42_0 = com.chelpus.root.utils.createapkcustom.patchProcess(com.chelpus.root.utils.createapkcustom.pat);
                        }
                        if (v42_0 == 0) {
                            v45 = 0;
                        }
                        com.chelpus.root.utils.createapkcustom.patchedLibs.add(com.chelpus.root.utils.createapkcustom.localFile2.getAbsolutePath());
                        com.chelpus.root.utils.createapkcustom.ser.clear();
                        com.chelpus.root.utils.createapkcustom.pat.clear();
                        com.chelpus.root.utils.createapkcustom.tag = 200;
                        break;
                }
            }
            if (v47[v40].contains("[BEGIN]")) {
                com.chelpus.root.utils.createapkcustom.tag = 0;
                v11 = 1;
            }
            if ((v47[v40].contains("[CLASSES]")) || (v47[v40].contains("[ODEX]"))) {
                com.chelpus.root.utils.createapkcustom.tag = 1;
            }
            if (v47[v40].contains("[PACKAGE]")) {
                com.chelpus.root.utils.createapkcustom.tag = 5;
            }
            if (v32 != 0) {
                com.chelpus.root.utils.createapkcustom.ser.clear();
                com.chelpus.root.utils.createapkcustom.pat.clear();
                org.json.JSONObject v29_0 = new org.json.JSONObject;
                v29_0(v47[v40]);
                String v48_0 = v29_0.getString("name");
                switch (com.chelpus.root.utils.createapkcustom.tag) {
                    case 2:
                        com.chelpus.root.utils.createapkcustom.libs.clear();
                        com.chelpus.root.utils.createapkcustom.libs = com.chelpus.root.utils.createapkcustom.searchlib(4, v48_0);
                    case 3:
                    case 4:
                    case 5:
                    default:
                        break;
                    case 3:
                    case 4:
                    case 5:
                        break;
                    case 3:
                    case 4:
                    case 5:
                        break;
                    case 6:
                        com.chelpus.root.utils.createapkcustom.libs.clear();
                        com.chelpus.root.utils.createapkcustom.libs = com.chelpus.root.utils.createapkcustom.searchlib(0, v48_0);
                        break;
                    case 7:
                        com.chelpus.root.utils.createapkcustom.libs.clear();
                        com.chelpus.root.utils.createapkcustom.libs = com.chelpus.root.utils.createapkcustom.searchlib(1, v48_0);
                        break;
                    case 8:
                        com.chelpus.root.utils.createapkcustom.libs.clear();
                        com.chelpus.root.utils.createapkcustom.libs = com.chelpus.root.utils.createapkcustom.searchlib(2, v48_0);
                        break;
                    case 9:
                        com.chelpus.root.utils.createapkcustom.libs.clear();
                        com.chelpus.root.utils.createapkcustom.libs = com.chelpus.root.utils.createapkcustom.searchlib(3, v48_0);
                        break;
                }
                v32 = 0;
            }
            if (v25 != 0) {
                com.chelpus.root.utils.createapkcustom.ser.clear();
                com.chelpus.root.utils.createapkcustom.pat.clear();
                org.json.JSONObject v29_1 = new org.json.JSONObject;
                v29_1(v47[v40]);
                String v48_1 = v29_1.getString("name");
                String v24 = com.chelpus.root.utils.createapkcustom.getFileFromApk(v48_1);
                if (!new java.io.File(v24).exists()) {
                    com.chelpus.root.utils.createapkcustom.print.println("file for patch not found in apk.");
                } else {
                    com.chelpus.root.utils.createapkcustom.localFile2 = new java.io.File(v24);
                }
                v25 = 0;
            }
            if (v47[v40].contains("[LIB-ARMEABI]")) {
                com.chelpus.root.utils.createapkcustom.tag = 6;
                com.chelpus.root.utils.createapkcustom.unpack = 0;
                v25 = 0;
                v32 = 1;
            }
            if (v47[v40].contains("[LIB-ARMEABI-V7A]")) {
                com.chelpus.root.utils.createapkcustom.tag = 7;
                com.chelpus.root.utils.createapkcustom.unpack = 0;
                v25 = 0;
                v32 = 1;
            }
            if (v47[v40].contains("[LIB-MIPS]")) {
                com.chelpus.root.utils.createapkcustom.tag = 8;
                com.chelpus.root.utils.createapkcustom.unpack = 0;
                v25 = 0;
                v32 = 1;
            }
            if (v47[v40].contains("[LIB-X86]")) {
                com.chelpus.root.utils.createapkcustom.tag = 9;
                com.chelpus.root.utils.createapkcustom.unpack = 0;
                v25 = 0;
                v32 = 1;
            }
            if (v47[v40].contains("[LIB]")) {
                com.chelpus.root.utils.createapkcustom.tag = 2;
                com.chelpus.root.utils.createapkcustom.unpack = 0;
                v25 = 0;
                v32 = 1;
            }
            if (v47[v40].contains("[FILE_IN_APK]")) {
                com.chelpus.root.utils.createapkcustom.tag = 10;
                com.chelpus.root.utils.createapkcustom.unpack = 0;
                v32 = 0;
                v25 = 1;
            }
            if (v47[v40].contains("group")) {
                org.json.JSONObject v29_2 = new org.json.JSONObject;
                v29_2(v47[v40]);
                com.chelpus.root.utils.createapkcustom.group = v29_2.getString("group");
            }
            if (v47[v40].contains("original")) {
                if (v36 != 0) {
                    v45 = com.chelpus.root.utils.createapkcustom.searchProcess(com.chelpus.root.utils.createapkcustom.ser);
                    v36 = 0;
                }
                try {
                    org.json.JSONObject v29_3 = new org.json.JSONObject;
                    v29_3(v47[v40]);
                    String v48_2 = v29_3.getString("original");
                } catch (Exception v18) {
                    com.chelpus.root.utils.createapkcustom.print.println("Error LP: Error original hex read!");
                }
                String v48_3 = v48_2.trim();
                v48_3.split("[ \t]+");
                String[] v38_1 = v48_3.split("[ \t]+");
                v4 = new int[v38_1.length];
                v3 = new byte[v38_1.length];
                int v46_0 = 0;
                try {
                    while (v46_0 < v38_1.length) {
                        if ((v38_1[v46_0].contains("*")) && (!v38_1[v46_0].contains("**"))) {
                            v23 = 1;
                            v38_1[v46_0] = "60";
                        }
                        if ((!v38_1[v46_0].contains("**")) && (!v38_1[v46_0].matches("\\?+"))) {
                            v4[v46_0] = 0;
                        } else {
                            v38_1[v46_0] = "60";
                            v4[v46_0] = 1;
                        }
                        if ((v38_1[v46_0].contains("W")) || ((v38_1[v46_0].contains("w")) || ((v38_1[v46_0].contains("R")) || (v38_1[v46_0].contains("r"))))) {
                            v4[v46_0] = (Integer.valueOf(v38_1[v46_0].toLowerCase().replace("w", "").replace("r", "")).intValue() + 2);
                            v38_1[v46_0] = "60";
                        }
                        v3[v46_0] = Integer.valueOf(v38_1[v46_0], 16).byteValue();
                        v46_0++;
                    }
                } catch (Exception v18_0) {
                    com.chelpus.root.utils.createapkcustom.print.println(new StringBuilder().append(" ").append(v18_0).toString());
                }
            }
            if (v47[v40].contains("\"object\"")) {
                try {
                    org.json.JSONObject v29_4 = new org.json.JSONObject;
                    v29_4(v47[v40]);
                    String v50_0 = v29_4.getString("object");
                } catch (Exception v18) {
                    com.chelpus.root.utils.createapkcustom.print.println("Error LP: Error number by object!");
                }
                Process v35 = Runtime.getRuntime().exec(new StringBuilder().append("dalvikvm -Xverify:none -Xdexopt:none -cp ").append(p55[5]).append(" ").append(p55[6]).append(".createnerorunpatch ").append(p55[0]).append(" ").append("object").append(v50_0).append(" ").append(com.chelpus.root.utils.createapkcustom.sddir).append(" ").append(com.chelpus.root.utils.createapkcustom.tooldir).append("\n").toString());
                v35.waitFor();
                java.io.DataInputStream v34 = new java.io.DataInputStream;
                v34(v35.getInputStream());
                byte[] v10 = new byte[v34.available()];
                v34.read(v10);
                String v43 = new String;
                v43(v10);
                v35.destroy();
                if (!v43.contains("Done")) {
                    com.chelpus.root.utils.createapkcustom.print.println("Object not found!\n\n");
                    v45 = 0;
                } else {
                    com.chelpus.root.utils.createapkcustom.print.println("Object patched!\n\n");
                    v45 = 1;
                }
                com.chelpus.root.utils.createapkcustom.fixadler(com.chelpus.root.utils.createapkcustom.localFile2);
                com.chelpus.root.utils.createapkcustom.manualpatch = 1;
            }
            if (v47[v40].contains("search")) {
                try {
                    org.json.JSONObject v29_5 = new org.json.JSONObject;
                    v29_5(v47[v40]);
                    String v50_1 = v29_5.getString("search");
                } catch (Exception v18) {
                    com.chelpus.root.utils.createapkcustom.print.println("Error LP: Error search hex read!");
                }
                String v50_2 = v50_1.trim();
                v50_2.split("[ \t]+");
                String[] v38_2 = v50_2.split("[ \t]+");
                v4 = new int[v38_2.length];
                v3 = new byte[v38_2.length];
                int v46_1 = 0;
                try {
                    while (v46_1 < v38_2.length) {
                        if ((v38_2[v46_1].contains("*")) && (!v38_2[v46_1].contains("**"))) {
                            v23 = 1;
                            v38_2[v46_1] = "60";
                        }
                        if ((!v38_2[v46_1].contains("**")) && (!v38_2[v46_1].matches("\\?+"))) {
                            v4[v46_1] = 0;
                        } else {
                            v38_2[v46_1] = "60";
                            v4[v46_1] = 1;
                        }
                        if (v38_2[v46_1].toUpperCase().contains("R")) {
                            v4[v46_1] = (Integer.valueOf(v38_2[v46_1].replace("R", "")).intValue() + 2);
                            v38_2[v46_1] = "60";
                        }
                        v3[v46_1] = Integer.valueOf(v38_2[v46_1], 16).byteValue();
                        v46_1++;
                    }
                } catch (Exception v18_1) {
                    com.chelpus.root.utils.createapkcustom.print.println(new StringBuilder().append("search pattern read: ").append(v18_1).toString());
                }
                if (v23 == 0) {
                    v36 = 1;
                    try {
                        com.android.vending.billing.InAppBillingService.LUCK.SearchItem v33 = new com.android.vending.billing.InAppBillingService.LUCK.SearchItem;
                        v33(v3, v4);
                        boolean v2_287 = new byte[v3.length];
                        v33.repByte = v2_287;
                        com.chelpus.root.utils.createapkcustom.ser.add(v33);
                    } catch (Exception v18_2) {
                        com.chelpus.root.utils.createapkcustom.print.println(new StringBuilder().append(" ").append(v18_2).toString());
                    }
                } else {
                    v42_0 = 0;
                    com.chelpus.root.utils.createapkcustom.print.println("Error LP: Patterns to search not valid!\n");
                }
            }
            if (v47[v40].contains("replaced")) {
                try {
                    org.json.JSONObject v29_6 = new org.json.JSONObject;
                    v29_6(v47[v40]);
                    String v49_0 = v29_6.getString("replaced");
                } catch (Exception v18) {
                    com.chelpus.root.utils.createapkcustom.print.println("Error LP: Error replaced hex read!");
                }
                String v49_1 = v49_0.trim();
                v49_1.split("[ \t]+");
                String[] v41_0 = v49_1.split("[ \t]+");
                int[] v6_0 = new int[v41_0.length];
                byte[] v5_0 = new byte[v41_0.length];
                int v46_2 = 0;
                try {
                    while (v46_2 < v41_0.length) {
                        if ((v41_0[v46_2].contains("*")) && (!v41_0[v46_2].contains("**"))) {
                            v23 = 1;
                            v41_0[v46_2] = "60";
                        }
                        if ((!v41_0[v46_2].contains("**")) && (!v41_0[v46_2].matches("\\?+"))) {
                            v6_0[v46_2] = 1;
                        } else {
                            v41_0[v46_2] = "60";
                            v6_0[v46_2] = 0;
                        }
                        if (v41_0[v46_2].toLowerCase().contains("sq")) {
                            v41_0[v46_2] = "60";
                            v6_0[v46_2] = 253;
                        }
                        if ((v41_0[v46_2].contains("s1")) || (v41_0[v46_2].contains("S1"))) {
                            v41_0[v46_2] = "60";
                            v6_0[v46_2] = 254;
                        }
                        if ((v41_0[v46_2].contains("s0")) || (v41_0[v46_2].contains("S0"))) {
                            v41_0[v46_2] = "60";
                            v6_0[v46_2] = 255;
                        }
                        if ((v41_0[v46_2].contains("W")) || ((v41_0[v46_2].contains("w")) || ((v41_0[v46_2].contains("R")) || (v41_0[v46_2].contains("R"))))) {
                            v6_0[v46_2] = (Integer.valueOf(v41_0[v46_2].toLowerCase().replace("w", "").replace("r", "")).intValue() + 2);
                            v41_0[v46_2] = "60";
                        }
                        v5_0[v46_2] = Integer.valueOf(v41_0[v46_2], 16).byteValue();
                        v46_2++;
                    }
                } catch (Exception v18_3) {
                    com.chelpus.root.utils.createapkcustom.print.println(new StringBuilder().append(" ").append(v18_3).toString());
                }
                if ((v6_0.length != v4.length) || ((v3.length != v5_0.length) || ((v5_0.length < 4) || (v3.length < 4)))) {
                    v23 = 1;
                }
                if (v23 != 0) {
                    v42_0 = 0;
                    com.chelpus.root.utils.createapkcustom.print.println("Error LP: Patterns original and replaced not valid!\n- Dimensions of the original hex-string and repleced must be >3.\n- Dimensions of the original hex-string and repleced must be equal.\n- Pattern hex must be: AF 11 4B ** AA **\nCheck the template file and try again!");
                }
                if (v23 == 0) {
                    com.chelpus.root.utils.createapkcustom.pat.add(new com.android.vending.billing.InAppBillingService.LUCK.PatchesItem(v3, v4, v5_0, v6_0, com.chelpus.root.utils.createapkcustom.group, 0));
                    com.chelpus.root.utils.createapkcustom.group = "";
                }
            }
            if (v47[v40].contains("insert")) {
                try {
                    org.json.JSONObject v29_7 = new org.json.JSONObject;
                    v29_7(v47[v40]);
                    String v49_2 = v29_7.getString("insert");
                } catch (Exception v18) {
                    com.chelpus.Utils.sendFromRoot("Error LP: Error insert hex read!");
                }
                String v49_3 = v49_2.trim();
                v49_3.split("[ \t]+");
                String[] v41_1 = v49_3.split("[ \t]+");
                int[] v6_1 = new int[v41_1.length];
                byte[] v5_1 = new byte[v41_1.length];
                int v46_3 = 0;
                try {
                    while (v46_3 < v41_1.length) {
                        if ((v41_1[v46_3].contains("*")) && (!v41_1[v46_3].contains("**"))) {
                            v23 = 1;
                            v41_1[v46_3] = "60";
                        }
                        if ((!v41_1[v46_3].contains("**")) && (!v41_1[v46_3].matches("\\?+"))) {
                            v6_1[v46_3] = 1;
                        } else {
                            v41_1[v46_3] = "60";
                            v6_1[v46_3] = 0;
                        }
                        if (v41_1[v46_3].toLowerCase().contains("sq")) {
                            v41_1[v46_3] = "60";
                            v6_1[v46_3] = 253;
                        }
                        if ((v41_1[v46_3].contains("s1")) || (v41_1[v46_3].contains("S1"))) {
                            v41_1[v46_3] = "60";
                            v6_1[v46_3] = 254;
                        }
                        if ((v41_1[v46_3].contains("s0")) || (v41_1[v46_3].contains("S0"))) {
                            v41_1[v46_3] = "60";
                            v6_1[v46_3] = 255;
                        }
                        if ((v41_1[v46_3].contains("W")) || ((v41_1[v46_3].contains("w")) || ((v41_1[v46_3].contains("R")) || (v41_1[v46_3].contains("R"))))) {
                            v6_1[v46_3] = (Integer.valueOf(v41_1[v46_3].toLowerCase().replace("w", "").replace("r", "")).intValue() + 2);
                            v41_1[v46_3] = "60";
                        }
                        v5_1[v46_3] = Integer.valueOf(v41_1[v46_3], 16).byteValue();
                        v46_3++;
                    }
                } catch (Exception v18_4) {
                    com.chelpus.Utils.sendFromRoot(new StringBuilder().append(" ").append(v18_4).toString());
                }
                if ((v5_1.length < 4) || (v3.length < 4)) {
                    v23 = 1;
                }
                if (v23 != 0) {
                    v42_0 = 0;
                    com.chelpus.Utils.sendFromRoot("Error LP: Dimensions of the original hex-string and repleced must be >3.\n- Pattern hex must be: AF 11 4B ** AA **\nCheck the template file and try again!");
                }
                if (v23 == 0) {
                    if ((!com.chelpus.root.utils.createapkcustom.multilib_patch) && (!com.chelpus.root.utils.createapkcustom.multidex)) {
                        com.chelpus.root.utils.createapkcustom.pat.add(new com.android.vending.billing.InAppBillingService.LUCK.PatchesItem(v3, v4, v5_1, v6_1, com.chelpus.root.utils.createapkcustom.group, 1));
                    } else {
                        com.chelpus.root.utils.createapkcustom.pat.add(new com.android.vending.billing.InAppBillingService.LUCK.PatchesItem(v3, v4, v5_1, v6_1, "all_lib", 1));
                    }
                    com.chelpus.root.utils.createapkcustom.group = "";
                }
            }
            if (v47[v40].contains("replace_from_file")) {
                org.json.JSONObject v29_8 = new org.json.JSONObject;
                v29_8(v47[v40]);
                String v49_4 = v29_8.getString("replace_from_file");
                java.io.File v9_1 = new java.io.File(new StringBuilder().append(com.chelpus.Utils.getDirs(new java.io.File(p55[1]))).append("/").append(v49_4.trim()).toString());
                int v30 = ((int) v9_1.length());
                byte[] v5_2 = new byte[v30];
                java.io.FileInputStream v17 = new java.io.FileInputStream;
                v17(v9_1);
                while (v17.read(v5_2) > 0) {
                }
                int[] v6_2 = new int[v30];
                java.util.Arrays.fill(v6_2, 1);
                if (v23 != 0) {
                    v42_0 = 0;
                    com.chelpus.root.utils.createapkcustom.print.println("Error LP: Patterns original and replaced not valid!\n- Dimensions of the original hex-string and repleced must be >3.\n- Dimensions of the original hex-string and repleced must be equal.\n- Pattern hex must be: AF 11 4B ** AA **\nCheck the template file and try again!");
                }
                if (v23 == 0) {
                    com.chelpus.root.utils.createapkcustom.pat.add(new com.android.vending.billing.InAppBillingService.LUCK.PatchesItem(v3, v4, v5_2, v6_2, com.chelpus.root.utils.createapkcustom.group, 0));
                    com.chelpus.root.utils.createapkcustom.group = "";
                }
            }
            if (v22 != 0) {
                v26 = new StringBuilder().append(v26).append("\n").append(v47[v40]).toString();
            }
            if (v16.contains("[END]")) {
                com.chelpus.root.utils.createapkcustom.tag = 4;
                v22 = 1;
            }
            v40++;
        }
        if (com.chelpus.root.utils.createapkcustom.patchedLibs.size() > 0) {
            com.chelpus.root.utils.createapkcustom.zipLib(com.chelpus.root.utils.createapkcustom.patchedLibs);
        }
        if (v45 != 0) {
            com.chelpus.root.utils.createapkcustom.print.println(new StringBuilder().append("").append(v26).toString());
        }
        if (v45 == 0) {
            if (!com.chelpus.root.utils.createapkcustom.patchteil) {
                com.chelpus.root.utils.createapkcustom.print.println("Custom Patch not valid for this Version of the Programm or already patched. ");
            } else {
                com.chelpus.root.utils.createapkcustom.print.println("Not all patterns are replaced, but the program can work, test it!\nCustom Patch not valid for this Version of the Programm or already patched. ");
            }
        }
        com.chelpus.root.utils.createapkcustom.clearTemp();
        v27.close();
    }

    public static boolean patchProcess(java.util.ArrayList p24)
    {
        try {
            java.nio.channels.FileChannel v2 = new java.io.RandomAccessFile(com.chelpus.root.utils.createapkcustom.localFile2, "rw").getChannel();
            java.nio.MappedByteBuffer v13 = v2.map(java.nio.channels.FileChannel$MapMode.READ_WRITE, 0, ((long) ((int) v2.size())));
            p24.toArray();
            int v0_1 = new com.android.vending.billing.InAppBillingService.LUCK.PatchesItem[p24.size()];
            com.android.vending.billing.InAppBillingService.LUCK.PatchesItem[] v20_1 = ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItem[]) ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItem[]) p24.toArray(v0_1)));
            int v11 = -1;
            try {
                while (v13.hasRemaining()) {
                    v13.position((v11 + 1));
                    v11 = v13.position();
                    byte v10 = v13.get();
                    int v14_0 = 0;
                    while (v14_0 < v20_1.length) {
                        v13.position(v11);
                        if ((v10 == v20_1[v14_0].origByte[0]) || ((v20_1[v14_0].origMask[0] == 1) || ((v20_1[v14_0].origMask[0] > 1) && (v10 == ((Byte) com.chelpus.root.utils.createapkcustom.search.get((v20_1[v14_0].origMask[0] - 2))).byteValue())))) {
                            if (v20_1[v14_0].repMask[0] == 0) {
                                v20_1[v14_0].repByte[0] = v10;
                            }
                            try {
                                if ((v20_1[v14_0].repMask[0] > 1) && (v20_1[v14_0].repMask[0] < 253)) {
                                    v20_1[v14_0].repByte[0] = ((Byte) com.chelpus.root.utils.createapkcustom.search.get((v20_1[v14_0].repMask[0] - 2))).byteValue();
                                }
                                if (v20_1[v14_0].repMask[0] == 253) {
                                    v20_1[v14_0].repByte[0] = ((byte) ((v10 & 15) + ((v10 & 15) * 16)));
                                }
                                if (v20_1[v14_0].repMask[0] == 254) {
                                    v20_1[v14_0].repByte[0] = ((byte) ((v10 & 15) + 16));
                                }
                                if (v20_1[v14_0].repMask[0] == 255) {
                                    v20_1[v14_0].repByte[0] = ((byte) (v10 & 15));
                                }
                                int v15 = 1;
                                v13.position((v11 + 1));
                                byte v21 = v13.get();
                                while (((v21 == v20_1[v14_0].origByte[v15]) || ((v20_1[v14_0].origMask[v15] > 1) && (v21 == ((Byte) com.chelpus.root.utils.createapkcustom.search.get((v20_1[v14_0].origMask[v15] - 2))).byteValue()))) || (v20_1[v14_0].origMask[v15] == 1)) {
                                    try {
                                        if (v20_1[v14_0].repMask[v15] != 0) {
                                            if ((v20_1[v14_0].repMask[v15] > 1) && (v20_1[v14_0].repMask[v15] < 253)) {
                                                v20_1[v14_0].repByte[v15] = ((Byte) com.chelpus.root.utils.createapkcustom.search.get((v20_1[v14_0].repMask[v15] - 2))).byteValue();
                                            }
                                        } else {
                                            v20_1[v14_0].repByte[v15] = v21;
                                        }
                                        if (v20_1[v14_0].repMask[0] == 253) {
                                            v20_1[v14_0].repByte[0] = ((byte) ((v10 & 15) + ((v10 & 15) * 16)));
                                        }
                                        if (v20_1[v14_0].repMask[v15] == 254) {
                                            v20_1[v14_0].repByte[v15] = ((byte) ((v21 & 15) + 16));
                                        }
                                        if (v20_1[v14_0].repMask[v15] == 255) {
                                            v20_1[v14_0].repByte[v15] = ((byte) (v21 & 15));
                                        }
                                        v15++;
                                        if (v15 != v20_1[v14_0].origByte.length) {
                                            v21 = v13.get();
                                        } else {
                                            if (v20_1[v14_0].insert) {
                                                int v18 = v13.position();
                                                int v16 = (((int) v2.size()) - v18);
                                                byte[] v8 = new byte[v16];
                                                v13.get(v8, 0, v16);
                                                java.nio.ByteBuffer v9_0 = java.nio.ByteBuffer.wrap(v8);
                                                v2.position(((long) ((v20_1[v14_0].repByte.length - v20_1[v14_0].origByte.length) + v18)));
                                                v2.write(v9_0);
                                                v13 = v2.map(java.nio.channels.FileChannel$MapMode.READ_WRITE, 0, ((long) ((int) v2.size())));
                                                v13.position(v18);
                                            }
                                            v2.position(((long) v11));
                                            v2.write(java.nio.ByteBuffer.wrap(v20_1[v14_0].repByte));
                                            v13.force();
                                            com.chelpus.root.utils.createapkcustom.print.println(new StringBuilder().append("\nPattern N").append((v14_0 + 1)).append(": Patch done! \n(Offset: ").append(Integer.toHexString(v11)).append(")\n").toString());
                                            v20_1[v14_0].result = 1;
                                            com.chelpus.root.utils.createapkcustom.patchteil = 1;
                                            break;
                                        }
                                    } catch (Exception v12) {
                                        int v23_2 = (v20_1[v14_0].repMask[v15] - 2);
                                        com.chelpus.root.utils.createapkcustom.print.println(new StringBuilder().append("Byte N").append(v23_2).append(" not found! Please edit search pattern for byte ").append(v23_2).append(".").toString());
                                    }
                                }
                            } catch (Exception v12) {
                                int v23_0 = (v20_1[v14_0].repMask[0] - 2);
                                com.chelpus.root.utils.createapkcustom.print.println(new StringBuilder().append("Byte N").append(v23_0).append(" not found! Please edit search pattern for byte ").append(v23_0).append(".").toString());
                            }
                        }
                        v14_0++;
                    }
                }
            } catch (Exception v12) {
                com.chelpus.root.utils.createapkcustom.print.println("Byte by search not found! Please edit pattern for search.\n");
            } catch (int v3) {
            }
            v2.close();
            int v14_1 = 0;
            while (v14_1 < v20_1.length) {
                if (v20_1[v14_1].result) {
                    if (((com.chelpus.root.utils.createapkcustom.multidex) || (com.chelpus.root.utils.createapkcustom.multilib_patch)) && (!v20_1[v14_1].group.equals(""))) {
                        com.chelpus.root.utils.createapkcustom.goodResult = 1;
                    }
                } else {
                    int v22 = 0;
                    if (!v20_1[v14_1].group.equals("")) {
                        int v23_3 = 0;
                        while (v23_3 < v20_1.length) {
                            if ((v20_1[v14_1].group.equals(v20_1[v23_3].group)) && (v20_1[v23_3].result)) {
                                if ((com.chelpus.root.utils.createapkcustom.multidex) || (com.chelpus.root.utils.createapkcustom.multilib_patch)) {
                                    com.chelpus.root.utils.createapkcustom.goodResult = 1;
                                }
                                v22 = 1;
                            }
                            v23_3++;
                        }
                    }
                    if ((v22 == 0) && (!com.chelpus.root.utils.createapkcustom.goodResult)) {
                        if (!com.chelpus.root.utils.createapkcustom.multidex) {
                            if (!com.chelpus.root.utils.createapkcustom.multilib_patch) {
                                com.chelpus.root.utils.createapkcustom.print.println(new StringBuilder().append("\nPattern N").append((v14_1 + 1)).append(":\nError LP: Pattern not found!\nor patch is already applied?!\n").toString());
                            } else {
                                if ((!com.chelpus.root.utils.createapkcustom.goodResult) && (com.chelpus.root.utils.createapkcustom.localFile2.equals(new java.io.File(((String) com.chelpus.root.utils.createapkcustom.libs.get((com.chelpus.root.utils.createapkcustom.libs.size() - 1))))))) {
                                    com.chelpus.root.utils.createapkcustom.print.println(new StringBuilder().append("\nPattern N").append((v14_1 + 1)).append(":\nError LP: Pattern not found!\nor patch is already applied?!\n").toString());
                                }
                            }
                        } else {
                            if ((!com.chelpus.root.utils.createapkcustom.goodResult) && (com.chelpus.root.utils.createapkcustom.localFile2.equals(com.chelpus.root.utils.createapkcustom.classesFiles.get((com.chelpus.root.utils.createapkcustom.classesFiles.size() - 1))))) {
                                com.chelpus.root.utils.createapkcustom.print.println(new StringBuilder().append("\nPattern N").append((v14_1 + 1)).append(":\nError LP: Pattern not found!\nor patch is already applied?!\n").toString());
                            }
                        }
                    }
                }
                v14_1++;
            }
            if (com.chelpus.root.utils.createapkcustom.tag == 1) {
                com.chelpus.root.utils.createapkcustom.fixadler(com.chelpus.root.utils.createapkcustom.localFile2);
            }
            return 1;
        } catch (Exception v12) {
            com.chelpus.root.utils.createapkcustom.print.println(new StringBuilder().append("Exception e").append(v12.toString()).toString());
        } catch (java.io.FileNotFoundException v17) {
            com.chelpus.root.utils.createapkcustom.print.println("Error LP: Program files are not found!\nMove Program to internal storage.");
        } catch (int v3) {
        }
    }

    public static boolean searchProcess(java.util.ArrayList p22)
    {
        int v17 = 1;
        try {
            java.nio.channels.FileChannel v2 = new java.io.RandomAccessFile(com.chelpus.root.utils.createapkcustom.localFile2, "rw").getChannel();
            java.nio.MappedByteBuffer v11 = v2.map(java.nio.channels.FileChannel$MapMode.READ_WRITE, 0, ((long) ((int) v2.size())));
            p22.toArray();
            int v0_1 = new com.android.vending.billing.InAppBillingService.LUCK.SearchItem[p22.size()];
            com.android.vending.billing.InAppBillingService.LUCK.SearchItem[] v18_1 = ((com.android.vending.billing.InAppBillingService.LUCK.SearchItem[]) ((com.android.vending.billing.InAppBillingService.LUCK.SearchItem[]) p22.toArray(v0_1)));
            long v14 = 0;
        } catch (Exception v10_2) {
            com.chelpus.root.utils.createapkcustom.print.println(new StringBuilder().append("Exception e").append(v10_2.toString()).toString());
            return v17;
        } catch (java.io.FileNotFoundException v16) {
            com.chelpus.root.utils.createapkcustom.print.println("Error LP: Program files are not found!\nMove Program to internal storage.");
            return v17;
        } catch (Exception v10_1) {
            com.chelpus.root.utils.createapkcustom.print.println(new StringBuilder().append("Exception e").append(v10_1.toString()).toString());
            return v17;
        }
        try {
            while (v11.hasRemaining()) {
                int v9 = v11.position();
                byte v8 = v11.get();
                int v12_0 = 0;
                while (v12_0 < v18_1.length) {
                    v11.position(v9);
                    if ((!v18_1[v12_0].result) && ((v8 == v18_1[v12_0].origByte[0]) || (v18_1[v12_0].origMask[0] != 0))) {
                        if (v18_1[v12_0].origMask[0] != 0) {
                            v18_1[v12_0].repByte[0] = v8;
                        }
                        int v13 = 1;
                        v11.position((v9 + 1));
                        byte v19 = v11.get();
                        while (((!v18_1[v12_0].result) && (v19 == v18_1[v12_0].origByte[v13])) || (v18_1[v12_0].origMask[v13] != 0)) {
                            if (v18_1[v12_0].origMask[v13] > 0) {
                                v18_1[v12_0].repByte[v13] = v19;
                            }
                            v13++;
                            if (v13 != v18_1[v12_0].origByte.length) {
                                v19 = v11.get();
                            } else {
                                v18_1[v12_0].result = 1;
                                com.chelpus.root.utils.createapkcustom.patchteil = 1;
                                break;
                            }
                        }
                    }
                    v12_0++;
                }
                v11.position((v9 + 1));
                v14++;
            }
        } catch (Exception v10_0) {
            com.chelpus.root.utils.createapkcustom.print.println(new StringBuilder().append("").append(v10_0).toString());
        }
        v2.close();
        int v12_1 = 0;
        while (v12_1 < v18_1.length) {
            if (!v18_1[v12_1].result) {
                com.chelpus.root.utils.createapkcustom.print.println(new StringBuilder().append("Bytes by serach N").append((v12_1 + 1)).append(":\nError LP: Bytes not found!").toString());
                v17 = 0;
            }
            v12_1++;
        }
        int v12_2 = 0;
        while (v12_2 < v18_1.length) {
            if (v18_1[v12_2].result) {
                com.chelpus.root.utils.createapkcustom.print.println(new StringBuilder().append("\nBytes by search N").append((v12_2 + 1)).append(":").toString());
            }
            int v20 = 0;
            while (v20 < v18_1[v12_2].origMask.length) {
                if (v18_1[v12_2].origMask[v20] > 1) {
                    int v21 = (v18_1[v12_2].origMask[v20] - 2);
                    try {
                        com.chelpus.root.utils.createapkcustom.search.set(v21, Byte.valueOf(v18_1[v12_2].repByte[v20]));
                    } catch (Exception v10) {
                        com.chelpus.root.utils.createapkcustom.search.add(v21, Byte.valueOf(v18_1[v12_2].repByte[v20]));
                    }
                    if (v18_1[v12_2].result) {
                        com.chelpus.root.utils.createapkcustom.print.print(new StringBuilder().append("R").append(v21).append("=").append(Integer.toHexString(((Byte) com.chelpus.root.utils.createapkcustom.search.get(v21)).byteValue()).toUpperCase()).append(" ").toString());
                    }
                }
                v20++;
            }
            com.chelpus.root.utils.createapkcustom.print.println("");
            v12_2++;
        }
        return v17;
    }

    public static java.util.ArrayList searchlib(int p16, String p17)
    {
        java.util.ArrayList v10_1 = new java.util.ArrayList();
        try {
            java.io.File v1_1 = new java.io.File(com.chelpus.root.utils.createapkcustom.appdir);
            com.chelpus.root.utils.createapkcustom.crkapk = new java.io.File(new StringBuilder().append(com.chelpus.root.utils.createapkcustom.sddir).append("/Modified/").append(com.chelpus.root.utils.createapkcustom.packageName).append(".apk").toString());
        } catch (Exception v7_1) {
            com.chelpus.root.utils.createapkcustom.print.println(new StringBuilder().append("Lib not found!").append(v7_1.toString()).toString());
            return v10_1;
        } catch (Exception v7_0) {
            com.chelpus.root.utils.createapkcustom.print.println(new StringBuilder().append("Lib select error: ").append(v7_0.toString()).toString());
            return v10_1;
        }
        if (!com.chelpus.root.utils.createapkcustom.crkapk.exists()) {
            com.chelpus.Utils.copyFile(v1_1, com.chelpus.root.utils.createapkcustom.crkapk);
        }
        com.chelpus.root.utils.createapkcustom.extractLibs(com.chelpus.root.utils.createapkcustom.crkapk);
        if (!p17.trim().equals("*")) {
            switch (p16) {
                case 0:
                    v10_1.clear();
                    String v2 = new StringBuilder().append(com.chelpus.root.utils.createapkcustom.sddir).append("/tmp/lib/armeabi/").append(p17).toString();
                    if (!new java.io.File(new StringBuilder().append(com.chelpus.root.utils.createapkcustom.sddir).append("/tmp/lib/armeabi/").append(p17).toString()).exists()) {
                        throw new java.io.FileNotFoundException();
                    } else {
                        v10_1.add(v2);
                        return v10_1;
                    }
                case 1:
                    v10_1.clear();
                    String v3 = new StringBuilder().append(com.chelpus.root.utils.createapkcustom.sddir).append("/tmp/lib/armeabi-v7a/").append(p17).toString();
                    if (!new java.io.File(new StringBuilder().append(com.chelpus.root.utils.createapkcustom.sddir).append("/tmp/lib/armeabi-v7a/").append(p17).toString()).exists()) {
                        throw new java.io.FileNotFoundException();
                    } else {
                        v10_1.add(v3);
                        return v10_1;
                    }
                case 2:
                    v10_1.clear();
                    String v4 = new StringBuilder().append(com.chelpus.root.utils.createapkcustom.sddir).append("/tmp/lib/mips/").append(p17).toString();
                    if (!new java.io.File(new StringBuilder().append(com.chelpus.root.utils.createapkcustom.sddir).append("/tmp/lib/mips/").append(p17).toString()).exists()) {
                        throw new java.io.FileNotFoundException();
                    } else {
                        v10_1.add(v4);
                        return v10_1;
                    }
                case 3:
                    v10_1.clear();
                    String v5 = new StringBuilder().append(com.chelpus.root.utils.createapkcustom.sddir).append("/tmp/lib/x86/").append(p17).toString();
                    if (!new java.io.File(new StringBuilder().append(com.chelpus.root.utils.createapkcustom.sddir).append("/tmp/lib/x86/").append(p17).toString()).exists()) {
                        throw new java.io.FileNotFoundException();
                    } else {
                        v10_1.add(v5);
                        return v10_1;
                    }
                case 4:
                    String v6_0 = new StringBuilder().append(com.chelpus.root.utils.createapkcustom.sddir).append("/tmp/lib/armeabi/").append(p17).toString();
                    if (new java.io.File(v6_0).exists()) {
                        v10_1.add(v6_0);
                    }
                    String v6_1 = new StringBuilder().append(com.chelpus.root.utils.createapkcustom.sddir).append("/tmp/lib/armeabi-v7a/").append(p17).toString();
                    if (new java.io.File(v6_1).exists()) {
                        v10_1.add(v6_1);
                    }
                    String v6_2 = new StringBuilder().append(com.chelpus.root.utils.createapkcustom.sddir).append("/tmp/lib/mips/").append(p17).toString();
                    if (new java.io.File(v6_2).exists()) {
                        v10_1.add(v6_2);
                    }
                    String v6_3 = new StringBuilder().append(com.chelpus.root.utils.createapkcustom.sddir).append("/tmp/lib/x86/").append(p17).toString();
                    if (!new java.io.File(v6_3).exists()) {
                        return v10_1;
                    } else {
                        v10_1.add(v6_3);
                        return v10_1;
                    }
                default:
                    return v10_1;
            }
        } else {
            com.chelpus.root.utils.createapkcustom.multilib_patch = 1;
            java.util.ArrayList v9_1 = new java.util.ArrayList();
            new com.chelpus.Utils("").findFileEndText(new java.io.File(new StringBuilder().append(com.chelpus.root.utils.createapkcustom.sddir).append("/tmp/lib/").toString()), ".so", v9_1);
            if (v9_1.size() <= 0) {
                return v10_1;
            } else {
                boolean v11_85 = v9_1.iterator();
                while (v11_85.hasNext()) {
                    java.io.File v8_1 = ((java.io.File) v11_85.next());
                    if (v8_1.length() > 0) {
                        v10_1.add(v8_1.getAbsolutePath());
                    }
                }
                return v10_1;
            }
        }
    }

    public static void unzip(java.io.File p13)
    {
        com.chelpus.root.utils.createapkcustom.classesFiles.clear();
        try {
            java.io.FileInputStream v3_1 = new java.io.FileInputStream(p13);
            java.util.zip.ZipInputStream v7_1 = new java.util.zip.ZipInputStream(v3_1);
        } catch (Exception v1) {
            try {
                new net.lingala.zip4j.core.ZipFile(p13).extractFile("classes.dex", new StringBuilder().append(com.chelpus.root.utils.createapkcustom.sddir).append("/Modified/").toString());
                com.chelpus.root.utils.createapkcustom.classesFiles.add(new java.io.File(new StringBuilder().append(com.chelpus.root.utils.createapkcustom.sddir).append("/Modified/").append("classes.dex").toString()));
            } catch (Exception v2_1) {
                com.chelpus.root.utils.createapkcustom.print.println(new StringBuilder().append("Error LP: Error classes.dex decompress! ").append(v2_1).toString());
                com.chelpus.root.utils.createapkcustom.print.println(new StringBuilder().append("Exception e1").append(v1.toString()).toString());
            } catch (Exception v2_0) {
                com.chelpus.root.utils.createapkcustom.print.println(new StringBuilder().append("Error LP: Error classes.dex decompress! ").append(v2_0).toString());
                com.chelpus.root.utils.createapkcustom.print.println(new StringBuilder().append("Exception e1").append(v1.toString()).toString());
            }
            return;
        }
        while(true) {
            java.util.zip.ZipEntry v6 = v7_1.getNextEntry();
            if (v6 == null) {
                v7_1.close();
                v3_1.close();
                return;
            } else {
                if ((v6.getName().toLowerCase().startsWith("classes")) && ((v6.getName().endsWith(".dex")) && (!v6.getName().contains("/")))) {
                    java.io.FileOutputStream v4_1 = new java.io.FileOutputStream(new StringBuilder().append(com.chelpus.root.utils.createapkcustom.sddir).append("/Modified/").append(v6.getName()).toString());
                    byte[] v0 = new byte[1024];
                    while(true) {
                        int v5 = v7_1.read(v0);
                        if (v5 == -1) {
                            break;
                        }
                    }
                    com.chelpus.root.utils.createapkcustom.classesFiles.add(new java.io.File(new StringBuilder().append(com.chelpus.root.utils.createapkcustom.sddir).append("/Modified/").append(v6.getName()).toString()));
                    v7_1.closeEntry();
                    v4_1.close();
                }
            }
        }
        v4_1.write(v0, 0, v5);
    }

    public static void zipLib(java.util.ArrayList p7)
    {
        try {
            java.util.ArrayList v2_1 = new java.util.ArrayList();
            java.io.File v3_0 = p7.iterator();
        } catch (Exception v0) {
            com.chelpus.root.utils.createapkcustom.print.println(new StringBuilder().append("Error LP: Error libs compress! ").append(v0).toString());
            return;
        }
        while (v3_0.hasNext()) {
            v2_1.add(new com.android.vending.billing.InAppBillingService.LUCK.AddFilesItem(((String) v3_0.next()), new StringBuilder().append(com.chelpus.root.utils.createapkcustom.sddir).append("/tmp/").toString()));
        }
        com.chelpus.Utils.addFilesToZip(com.chelpus.root.utils.createapkcustom.crkapk.getAbsolutePath(), new StringBuilder().append(com.chelpus.root.utils.createapkcustom.crkapk.getAbsolutePath()).append("checlpis.zip").toString(), v2_1);
        com.chelpus.root.utils.createapkcustom.crkapk.delete();
        if (com.chelpus.root.utils.createapkcustom.crkapk.exists()) {
            return;
        } else {
            new java.io.File(new StringBuilder().append(com.chelpus.root.utils.createapkcustom.crkapk.getAbsolutePath()).append("checlpis.zip").toString()).renameTo(com.chelpus.root.utils.createapkcustom.crkapk);
            return;
        }
    }
}
