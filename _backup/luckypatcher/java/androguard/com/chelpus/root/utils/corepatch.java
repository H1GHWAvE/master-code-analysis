package com.chelpus.root.utils;
public class corepatch {
    public static final byte[] MAGIC;
    public static int adler;
    public static java.nio.MappedByteBuffer fileBytes;
    public static byte[] lastByteReplace;
    public static int lastPatchPosition;
    public static boolean not_found_bytes_for_patch;
    public static boolean onlyDalvik;
    public static String toolfilesdir;

    static corepatch()
    {
        String v0_1 = new byte[8];
        v0_1 = {100, 101, 121, 10, 48, 51, 53, 0};
        com.chelpus.root.utils.corepatch.MAGIC = v0_1;
        com.chelpus.root.utils.corepatch.toolfilesdir = "";
        com.chelpus.root.utils.corepatch.onlyDalvik = 0;
        com.chelpus.root.utils.corepatch.not_found_bytes_for_patch = 0;
        com.chelpus.root.utils.corepatch.fileBytes = 0;
        com.chelpus.root.utils.corepatch.lastPatchPosition = 0;
        com.chelpus.root.utils.corepatch.lastByteReplace = 0;
        return;
    }

    public corepatch()
    {
        return;
    }

    private static boolean applyPatch(int p6, byte p7, byte[] p8, byte[] p9, byte[] p10, byte[] p11, boolean p12)
    {
        java.nio.MappedByteBuffer v2_0 = 1;
        if ((p8 == null) || ((p7 != p8[0]) || (!p12))) {
            v2_0 = 0;
        } else {
            if (p11[0] == 0) {
                p10[0] = p7;
            }
            int v0 = 1;
            com.chelpus.root.utils.corepatch.fileBytes.position((p6 + 1));
            byte v1 = com.chelpus.root.utils.corepatch.fileBytes.get();
            while ((v1 == p8[v0]) || (p9[v0] == 1)) {
                if (p11[v0] == 0) {
                    p10[v0] = v1;
                }
                if (p11[v0] == 3) {
                    p10[v0] = ((byte) (v1 & 15));
                }
                if (p11[v0] == 2) {
                    p10[v0] = ((byte) ((v1 & 15) + ((v1 & 15) * 16)));
                }
                v0++;
                if (v0 != p8.length) {
                    v1 = com.chelpus.root.utils.corepatch.fileBytes.get();
                } else {
                    com.chelpus.root.utils.corepatch.fileBytes.position(p6);
                    com.chelpus.root.utils.corepatch.fileBytes.put(p10);
                    com.chelpus.root.utils.corepatch.fileBytes.force();
                }
            }
            com.chelpus.root.utils.corepatch.fileBytes.position((p6 + 1));
        }
        return v2_0;
    }

    private static boolean applyPatchCounter(int p6, byte p7, byte[] p8, byte[] p9, byte[] p10, byte[] p11, int p12, int p13, boolean p14)
    {
        java.nio.MappedByteBuffer v2_0 = 1;
        if ((p8 == null) || ((p7 != p8[0]) || (!p14))) {
            v2_0 = 0;
        } else {
            if (p11[0] == 0) {
                p10[0] = p7;
            }
            int v0 = 1;
            com.chelpus.root.utils.corepatch.fileBytes.position((p6 + 1));
            byte v1 = com.chelpus.root.utils.corepatch.fileBytes.get();
            while ((v1 == p8[v0]) || (p9[v0] == 1)) {
                if (p11[v0] == 0) {
                    p10[v0] = v1;
                }
                if (p11[v0] == 3) {
                    p10[v0] = ((byte) (v1 & 15));
                }
                if (p11[v0] == 2) {
                    p10[v0] = ((byte) ((v1 & 15) + ((v1 & 15) * 16)));
                }
                v0++;
                if (v0 != p8.length) {
                    v1 = com.chelpus.root.utils.corepatch.fileBytes.get();
                } else {
                    if (p12 >= p13) {
                        com.chelpus.root.utils.corepatch.lastPatchPosition = p6;
                        byte[] v4_18 = new byte[p10.length];
                        com.chelpus.root.utils.corepatch.lastByteReplace = v4_18;
                        System.arraycopy(p10, 0, com.chelpus.root.utils.corepatch.lastByteReplace, 0, p10.length);
                    }
                }
            }
            com.chelpus.root.utils.corepatch.fileBytes.position((p6 + 1));
        }
        return v2_0;
    }

    public static void main(String[] p332)
    {
        com.chelpus.Utils.startRootJava(new com.chelpus.root.utils.corepatch$1());
        int v154 = 0;
        int v63_0 = 0;
        int v75_0 = 0;
        int v87_0 = 0;
        byte[] v182 = 0;
        byte[] v264 = 0;
        byte[] v212 = 0;
        byte[] v310 = 0;
        byte[] v5 = 0;
        byte[] v6 = 0;
        byte[] v7 = 0;
        byte[] v8 = 0;
        byte[] v11 = 0;
        byte[] v12 = 0;
        byte[] v13 = 0;
        byte[] v14 = 0;
        byte[] v17 = 0;
        byte[] v18 = 0;
        byte[] v19 = 0;
        byte[] v20 = 0;
        byte[] v23 = 0;
        byte[] v24 = 0;
        byte[] v25 = 0;
        byte[] v26 = 0;
        byte[] v29 = 0;
        byte[] v30 = 0;
        byte[] v31 = 0;
        byte[] v32 = 0;
        byte[] v53 = 0;
        byte[] v54 = 0;
        byte[] v55 = 0;
        byte[] v56 = 0;
        byte[] v35 = 0;
        byte[] v36 = 0;
        byte[] v37 = 0;
        byte[] v38 = 0;
        byte[] v41 = 0;
        byte[] v42 = 0;
        byte[] v43 = 0;
        byte[] v44 = 0;
        byte[] v47 = 0;
        byte[] v48 = 0;
        byte[] v49 = 0;
        byte[] v50 = 0;
        byte[] v59 = 0;
        byte[] v60 = 0;
        byte[] v61 = 0;
        byte[] v62 = 0;
        byte[] v65 = 0;
        byte[] v66 = 0;
        byte[] v67 = 0;
        byte[] v68 = 0;
        byte[] v71 = 0;
        byte[] v72 = 0;
        byte[] v73 = 0;
        byte[] v74 = 0;
        byte[] v77 = 0;
        byte[] v78 = 0;
        byte[] v79 = 0;
        byte[] v80 = 0;
        byte[] v83 = 0;
        byte[] v84 = 0;
        byte[] v85 = 0;
        byte[] v86 = 0;
        byte[] v89 = 0;
        byte[] v90 = 0;
        byte[] v91 = 0;
        byte[] v92 = 0;
        byte[] v95 = 0;
        byte[] v96 = 0;
        byte[] v97 = 0;
        byte[] v98 = 0;
        byte[] v101 = 0;
        byte[] v102 = 0;
        byte[] v103 = 0;
        byte[] v104 = 0;
        byte[] v107 = 0;
        byte[] v108 = 0;
        byte[] v109 = 0;
        byte[] v110 = 0;
        byte[] v113 = 0;
        byte[] v114 = 0;
        byte[] v115 = 0;
        byte[] v116 = 0;
        byte[] v119 = 0;
        byte[] v120 = 0;
        byte[] v121 = 0;
        byte[] v122 = 0;
        byte[] v125 = 0;
        byte[] v126 = 0;
        byte[] v127 = 0;
        byte[] v128 = 0;
        byte[] v131 = 0;
        byte[] v132 = 0;
        byte[] v133 = 0;
        byte[] v134 = 0;
        byte[] v137 = 0;
        byte[] v138 = 0;
        byte[] v139 = 0;
        byte[] v140 = 0;
        byte[] v195 = 0;
        byte[] v276 = 0;
        byte[] v225 = 0;
        byte[] v322 = 0;
        byte[] v196 = 0;
        byte[] v277 = 0;
        byte[] v226 = 0;
        byte[] v323 = 0;
        byte[] v197 = 0;
        byte[] v278 = 0;
        byte[] v227 = 0;
        byte[] v324 = 0;
        byte[] v198 = 0;
        byte[] v279 = 0;
        byte[] v228 = 0;
        byte[] v325 = 0;
        byte[] v199 = 0;
        byte[] v280 = 0;
        byte[] v229 = 0;
        byte[] v326 = 0;
        byte[] v200 = 0;
        byte[] v281 = 0;
        byte[] v230 = 0;
        byte[] v327 = 0;
        byte[] v201 = 0;
        byte[] v282 = 0;
        byte[] v231 = 0;
        byte[] v328 = 0;
        int v162 = 0;
        byte[] v165 = 0;
        byte[] v166 = 0;
        byte[] v167 = 0;
        byte[] v168 = 0;
        int v170 = 0;
        byte[] v192 = 0;
        byte[] v273 = 0;
        byte[] v222 = 0;
        byte[] v319 = 0;
        byte[] v193 = 0;
        byte[] v274 = 0;
        byte[] v223 = 0;
        byte[] v320 = 0;
        byte[] v194 = 0;
        byte[] v275 = 0;
        byte[] v224 = 0;
        byte[] v321 = 0;
        byte[] v143 = 0;
        byte[] v144 = 0;
        byte[] v145 = 0;
        byte[] v146 = 0;
        byte[] v185 = 0;
        byte[] v215 = 0;
        byte[] v186 = 0;
        byte[] v267 = 0;
        byte[] v216 = 0;
        byte[] v313 = 0;
        byte[] v187 = 0;
        byte[] v268 = 0;
        byte[] v217 = 0;
        byte[] v314 = 0;
        byte[] v188 = 0;
        byte[] v269 = 0;
        byte[] v218 = 0;
        byte[] v315 = 0;
        byte[] v189 = 0;
        byte[] v270 = 0;
        byte[] v219 = 0;
        byte[] v316 = 0;
        byte[] v190 = 0;
        byte[] v271 = 0;
        byte[] v220 = 0;
        byte[] v317 = 0;
        byte[] v191 = 0;
        byte[] v272 = 0;
        byte[] v221 = 0;
        byte[] v318 = 0;
        byte[] v172 = 0;
        byte[] v254 = 0;
        byte[] v202 = 0;
        byte[] v300 = 0;
        byte[] v173 = 0;
        byte[] v255 = 0;
        byte[] v203 = 0;
        byte[] v301 = 0;
        byte[] v174 = 0;
        byte[] v256 = 0;
        byte[] v204 = 0;
        byte[] v302 = 0;
        byte[] v175 = 0;
        byte[] v257 = 0;
        byte[] v205 = 0;
        byte[] v303 = 0;
        byte[] v176 = 0;
        byte[] v258 = 0;
        byte[] v206 = 0;
        byte[] v304 = 0;
        byte[] v177 = 0;
        byte[] v259 = 0;
        byte[] v207 = 0;
        byte[] v305 = 0;
        byte[] v178 = 0;
        byte[] v260 = 0;
        byte[] v208 = 0;
        byte[] v306 = 0;
        byte[] v179 = 0;
        byte[] v261 = 0;
        byte[] v209 = 0;
        byte[] v307 = 0;
        byte[] v180 = 0;
        byte[] v262 = 0;
        byte[] v210 = 0;
        byte[] v308 = 0;
        byte[] v181 = 0;
        byte[] v263 = 0;
        byte[] v211 = 0;
        byte[] v309 = 0;
        byte[] v183 = 0;
        byte[] v265 = 0;
        byte[] v213 = 0;
        byte[] v311 = 0;
        byte[] v184 = 0;
        byte[] v266 = 0;
        byte[] v214 = 0;
        byte[] v312 = 0;
        try {
            System.out.println(p332[0]);
            System.out.println(p332[1]);
            System.out.println(p332[2]);
            System.out.println(p332[3]);
            System.out.println(p332[4]);
        } catch (Exception v240_0) {
            v240_0.printStackTrace();
            if (p332[3] != null) {
                com.chelpus.root.utils.corepatch.toolfilesdir = p332[3];
            }
            if ((!p332[4].equals("framework")) && (!p332[4].equals("OnlyDalvik"))) {
                com.chelpus.Utils.remount("/system", "rw");
            }
            if (((p332[1].contains("core.odex")) || ((p332[1].contains("core.jar")) || ((p332[1].contains("core-libart.jar")) || (p332[1].contains("boot.oat"))))) && (p332[4].equals("framework"))) {
                v154 = 1;
                v63_0 = 1;
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.startUnderRoot = Boolean.valueOf(0);
                p332[0] = "patch";
            }
            if (((p332[1].contains("services.jar")) || (p332[1].contains("services.odex"))) && (p332[4].equals("framework"))) {
                v75_0 = 1;
                v87_0 = 0;
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.startUnderRoot = Boolean.valueOf(0);
                p332[0] = "patch";
            }
            if (p332[0].contains("patch")) {
                if (p332[0].contains("_patch1")) {
                    v154 = 1;
                }
                int v296_0;
                if (!p332[0].contains("_patch2")) {
                    v296_0 = v63_0;
                } else {
                    v296_0 = 1;
                }
                int v297_0;
                if (!p332[0].contains("_patch3")) {
                    v297_0 = v75_0;
                } else {
                    v297_0 = 1;
                }
                int v298_0;
                if (!p332[0].contains("_patch4")) {
                    v298_0 = v87_0;
                } else {
                    v298_0 = 1;
                }
                v5 = new byte["11 90 11 99 01 29 0F D1 01 26 28 1C C0 68 39 1C".split("[ \t]+").length];
                v6 = new byte["11 90 11 99 01 29 0F D1 01 26 28 1C C0 68 39 1C".split("[ \t]+").length];
                v7 = new byte["?? ?? 01 21 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v8 = new byte["?? ?? 01 21 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                com.chelpus.Utils.convertStringToArraysPatch("11 90 11 99 01 29 0F D1 01 26 28 1C C0 68 39 1C", "?? ?? 01 21 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??", v5, v6, v7, v8);
                com.chelpus.Utils.convertStringToArraysPatch("11 90 11 99 01 29 0F D1 01 26 28 1C C0 68 39 1C", "?? ?? 01 21 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??", v5, v6, v7, v8);
                v11 = new byte["39 1C 08 68 52 46 D0 F8 CC 01 D0 F8 ?? E0 F0 47 05 1C 01 3C 00 F0 04 80 28 1C 06 B0 BD E8 E0 85 D9 F8 ?? E2 F0 47 F7 E7".split("[ \t]+").length];
                v12 = new byte["39 1C 08 68 52 46 D0 F8 CC 01 D0 F8 ?? E0 F0 47 05 1C 01 3C 00 F0 04 80 28 1C 06 B0 BD E8 E0 85 D9 F8 ?? E2 F0 47 F7 E7".split("[ \t]+").length];
                v13 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 01 20 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v14 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 01 20 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                com.chelpus.Utils.convertStringToArraysPatch("39 1C 08 68 52 46 D0 F8 CC 01 D0 F8 ?? E0 F0 47 05 1C 01 3C 00 F0 04 80 28 1C 06 B0 BD E8 E0 85 D9 F8 ?? E2 F0 47 F7 E7", "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 01 20 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??", v11, v12, v13, v14);
                int v0_82 = new byte["08 68 3A 1C D0 F8 D0 01 43 46 D0 F8 ?? E0 F0 47 05 1C 01 3C 00 F0 04 80 28 1C 09 B0 BD E8 E0 8D D9 F8 ?? E2 F0 47 F7 E7".split("[ \t]+").length];
                v17 = v0_82;
                int v0_83 = new byte["08 68 3A 1C D0 F8 D0 01 43 46 D0 F8 ?? E0 F0 47 05 1C 01 3C 00 F0 04 80 28 1C 09 B0 BD E8 E0 8D D9 F8 ?? E2 F0 47 F7 E7".split("[ \t]+").length];
                v18 = v0_83;
                int v0_84 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 01 20 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v19 = v0_84;
                int v0_85 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 01 20 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v20 = v0_85;
                com.chelpus.Utils.convertStringToArraysPatch("08 68 3A 1C D0 F8 D0 01 43 46 D0 F8 ?? E0 F0 47 05 1C 01 3C 00 F0 04 80 28 1C 09 B0 BD E8 E0 8D D9 F8 ?? E2 F0 47 F7 E7", "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 01 20 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??", v17, v18, v19, v20);
                int v0_86 = new byte["56 45 00 F0 07 80 01 3C 00 F0 31 80 05 98".split("[ \t]+").length];
                v23 = v0_86;
                int v0_87 = new byte["56 45 00 F0 07 80 01 3C 00 F0 31 80 05 98".split("[ \t]+").length];
                v24 = v0_87;
                int v0_88 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 01 20".split("[ \t]+").length];
                v25 = v0_88;
                int v0_89 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 01 20".split("[ \t]+").length];
                v26 = v0_89;
                com.chelpus.Utils.convertStringToArraysPatch("56 45 00 F0 07 80 01 3C 00 F0 31 80 05 98", "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 01 20", v23, v24, v25, v26);
                int v0_90 = new byte["89 44 24 40 8B 5C 24 40 83 FB 01 75 32 BE 01 00 00 00 8B C5 8B 40 0C 8B CF".split("[ \t]+").length];
                v29 = v0_90;
                int v0_91 = new byte["89 44 24 40 8B 5C 24 40 83 FB 01 75 32 BE 01 00 00 00 8B C5 8B 40 0C 8B CF".split("[ \t]+").length];
                v30 = v0_91;
                int v0_92 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 90 90 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v31 = v0_92;
                int v0_93 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 90 90 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v32 = v0_93;
                com.chelpus.Utils.convertStringToArraysPatch("89 44 24 40 8B 5C 24 40 83 FB 01 75 32 BE 01 00 00 00 8B C5 8B 40 0C 8B CF", "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 90 90 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??", v29, v30, v31, v32);
                int v0_94 = new byte["8B 54 24 38 8B CF 8B 01 8B 80 CC 01 00 00 FF 50 28 64 66 83 3D 00 00 00 00 00 8B E8 75 12 8B C5".split("[ \t]+").length];
                v35 = v0_94;
                int v0_95 = new byte["8B 54 24 38 8B CF 8B 01 8B 80 CC 01 00 00 FF 50 28 64 66 83 3D 00 00 00 00 00 8B E8 75 12 8B C5".split("[ \t]+").length];
                v36 = v0_95;
                int v0_96 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? B0 01".split("[ \t]+").length];
                v37 = v0_96;
                int v0_97 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? B0 01".split("[ \t]+").length];
                v38 = v0_97;
                com.chelpus.Utils.convertStringToArraysPatch("8B 54 24 38 8B CF 8B 01 8B 80 CC 01 00 00 FF 50 28 64 66 83 3D 00 00 00 00 00 8B E8 75 12 8B C5", "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? B0 01", v35, v36, v37, v38);
                int v0_98 = new byte["8B 80 D0 01 00 00 8B D7 FF 50 28 64 66 83 3D 00 00 00 00 00 8B E8 75 12 8B C5 8B 6C 24 20".split("[ \t]+").length];
                v41 = v0_98;
                int v0_99 = new byte["8B 80 D0 01 00 00 8B D7 FF 50 28 64 66 83 3D 00 00 00 00 00 8B E8 75 12 8B C5 8B 6C 24 20".split("[ \t]+").length];
                v42 = v0_99;
                int v0_100 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? B0 01 ?? ?? ?? ??".split("[ \t]+").length];
                v43 = v0_100;
                int v0_101 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? B0 01 ?? ?? ?? ??".split("[ \t]+").length];
                v44 = v0_101;
                com.chelpus.Utils.convertStringToArraysPatch("8B 80 D0 01 00 00 8B D7 FF 50 28 64 66 83 3D 00 00 00 00 00 8B E8 75 12 8B C5 8B 6C 24 20", "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? B0 01 ?? ?? ?? ??", v41, v42, v43, v44);
                int v0_102 = new byte["33 D2 89 54 24 10 8B 73 08 8B 44 24 38 8B 48 08 89 4C 24 18 3B F1 74 23 64 66 83 3D 00 00 00 00 00 0F 85 80 00 00 00".split("[ \t]+").length];
                v47 = v0_102;
                int v0_103 = new byte["33 D2 89 54 24 10 8B 73 08 8B 44 24 38 8B 48 08 89 4C 24 18 3B F1 74 23 64 66 83 3D 00 00 00 00 00 0F 85 80 00 00 00".split("[ \t]+").length];
                v48 = v0_103;
                int v0_104 = new byte["B3 01 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v49 = v0_104;
                int v0_105 = new byte["B3 01 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v50 = v0_105;
                com.chelpus.Utils.convertStringToArraysPatch("33 D2 89 54 24 10 8B 73 08 8B 44 24 38 8B 48 08 89 4C 24 18 3B F1 74 23 64 66 83 3D 00 00 00 00 00 0F 85 80 00 00 00", "B3 01 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??", v47, v48, v49, v50);
                int v0_106 = new byte["E5 03 1A AA C0 03 3F D6 FB 03 00 2A 7F 07 00 71 61 02 00 54 35 00 80 52 E1 03 16 AA 2A 00 40 B9 E0 03 14 AA".split("[ \t]+").length];
                v53 = v0_106;
                int v0_107 = new byte["E5 03 1A AA C0 03 3F D6 FB 03 00 2A 7F 07 00 71 61 02 00 54 35 00 80 52 E1 03 16 AA 2A 00 40 B9 E0 03 14 AA".split("[ \t]+").length];
                v54 = v0_107;
                int v0_108 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 35 00 80 52 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v55 = v0_108;
                int v0_109 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 35 00 80 52 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v56 = v0_109;
                com.chelpus.Utils.convertStringToArraysPatch("E5 03 1A AA C0 03 3F D6 FB 03 00 2A 7F 07 00 71 61 02 00 54 35 00 80 52 E1 03 16 AA 2A 00 40 B9 E0 03 14 AA", "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 35 00 80 52 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??", v53, v54, v55, v56);
                int v0_110 = new byte["F7 03 01 AA F9 03 02 AA FA 03 1F 2A F5 0A 40 B9 38 0B 40 B9 BF 02 18 6B".split("[ \t]+").length];
                v59 = v0_110;
                int v0_111 = new byte["F7 03 01 AA F9 03 02 AA FA 03 1F 2A F5 0A 40 B9 38 0B 40 B9 BF 02 18 6B".split("[ \t]+").length];
                v60 = v0_111;
                int v0_112 = new byte["?? ?? ?? ?? ?? ?? ?? ?? 3A 00 80 52 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v61 = v0_112;
                int v0_113 = new byte["?? ?? ?? ?? ?? ?? ?? ?? 3A 00 80 52 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v62 = v0_113;
                com.chelpus.Utils.convertStringToArraysPatch("F7 03 01 AA F9 03 02 AA FA 03 1F 2A F5 0A 40 B9 38 0B 40 B9 BF 02 18 6B", "?? ?? ?? ?? ?? ?? ?? ?? 3A 00 80 52 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??", v59, v60, v61, v62);
                int v0_114 = new byte["CD F8 30 C0 78 B9 1A 99 31 B9 01 3C 00 F0 ?? 80 0B 98".split("[ \t]+").length];
                v65 = v0_114;
                int v0_115 = new byte["CD F8 30 C0 78 B9 1A 99 31 B9 01 3C 00 F0 ?? 80 0B 98".split("[ \t]+").length];
                v66 = v0_115;
                int v0_116 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 00 20".split("[ \t]+").length];
                v67 = v0_116;
                int v0_117 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 00 20".split("[ \t]+").length];
                v68 = v0_117;
                com.chelpus.Utils.convertStringToArraysPatch("CD F8 30 C0 78 B9 1A 99 31 B9 01 3C 00 F0 ?? 80 0B 98", "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 00 20", v65, v66, v67, v68);
                int v0_118 = new byte["00 2D 40 F0 ?? 80 16 99 CD 69 DA F8 E0 20 0B 92 0B 9B 9D 42".split("[ \t]+").length];
                v71 = v0_118;
                int v0_119 = new byte["00 2D 40 F0 ?? 80 16 99 CD 69 DA F8 E0 20 0B 92 0B 9B 9D 42".split("[ \t]+").length];
                v72 = v0_119;
                int v0_120 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 9B ??".split("[ \t]+").length];
                v73 = v0_120;
                int v0_121 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 9B ??".split("[ \t]+").length];
                v74 = v0_121;
                com.chelpus.Utils.convertStringToArraysPatch("00 2D 40 F0 ?? 80 16 99 CD 69 DA F8 E0 20 0B 92 0B 9B 9D 42", "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 9B ??", v71, v72, v73, v74);
                int v0_122 = new byte["2D E9 E0 4D 8D B0 07 1C 00 90 15 91 16 92 17 93 16 9A 56 6A 31 1C 08 68 D0 F8 BC 01 D0 F8 28 E0 F0 47".split("[ \t]+").length];
                v77 = v0_122;
                int v0_123 = new byte["2D E9 E0 4D 8D B0 07 1C 00 90 15 91 16 92 17 93 16 9A 56 6A 31 1C 08 68 D0 F8 BC 01 D0 F8 28 E0 F0 47".split("[ \t]+").length];
                v78 = v0_123;
                int v0_124 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 01 20 0D B0 BD E8 E0 8D ?? ??".split("[ \t]+").length];
                v79 = v0_124;
                int v0_125 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 01 20 0D B0 BD E8 E0 8D ?? ??".split("[ \t]+").length];
                v80 = v0_125;
                com.chelpus.Utils.convertStringToArraysPatch("2D E9 E0 4D 8D B0 07 1C 00 90 15 91 16 92 17 93 16 9A 56 6A 31 1C 08 68 D0 F8 BC 01 D0 F8 28 E0 F0 47", "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 01 20 0D B0 BD E8 E0 8D ?? ??", v77, v78, v79, v80);
                int v0_126 = new byte["1C BF D9 F8 B0 E1 F0 47 BF F3 5B 8F 31 1C 08 68 D0 F8 ?? 02 D0 F8 28 E0 F0 47 14 90 14 ?? 00 ?? ?? D1".split("[ \t]+").length];
                v83 = v0_126;
                int v0_127 = new byte["1C BF D9 F8 B0 E1 F0 47 BF F3 5B 8F 31 1C 08 68 D0 F8 ?? 02 D0 F8 28 E0 F0 47 14 90 14 ?? 00 ?? ?? D1".split("[ \t]+").length];
                v84 = v0_127;
                int v0_128 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 00 20 ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v85 = v0_128;
                int v0_129 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 00 20 ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v86 = v0_129;
                com.chelpus.Utils.convertStringToArraysPatch("1C BF D9 F8 B0 E1 F0 47 BF F3 5B 8F 31 1C 08 68 D0 F8 ?? 02 D0 F8 28 E0 F0 47 14 90 14 ?? 00 ?? ?? D1", "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 00 20 ?? ?? ?? ?? ?? ?? ?? ??", v83, v84, v85, v86);
                int v0_130 = new byte["F0 47 82 46 BA F1 00 0F ?? D1 01 3C ?? F0 ?? ?? 28 ?? 00 2B".split("[ \t]+").length];
                v89 = v0_130;
                int v0_131 = new byte["F0 47 82 46 BA F1 00 0F ?? D1 01 3C ?? F0 ?? ?? 28 ?? 00 2B".split("[ \t]+").length];
                v90 = v0_131;
                int v0_132 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? E0 ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v91 = v0_132;
                int v0_133 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? E0 ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v92 = v0_133;
                com.chelpus.Utils.convertStringToArraysPatch("F0 47 82 46 BA F1 00 0F ?? D1 01 3C ?? F0 ?? ?? 28 ?? 00 2B", "?? ?? ?? ?? ?? ?? ?? ?? ?? E0 ?? ?? ?? ?? ?? ?? ?? ?? ?? ??", v89, v90, v91, v92);
                int v0_134 = new byte["F8 0C 00 D0 F8 ?? E0 F0 47 80 46 B8 F1 00 0F ?? D1 D9 F8 ?? ?? 39 1C".split("[ \t]+").length];
                v95 = v0_134;
                int v0_135 = new byte["F8 0C 00 D0 F8 ?? E0 F0 47 80 46 B8 F1 00 0F ?? D1 D9 F8 ?? ?? 39 1C".split("[ \t]+").length];
                v96 = v0_135;
                int v0_136 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? E0 ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v97 = v0_136;
                int v0_137 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? E0 ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v98 = v0_137;
                com.chelpus.Utils.convertStringToArraysPatch("F8 0C 00 D0 F8 ?? E0 F0 47 80 46 B8 F1 00 0F ?? D1 D9 F8 ?? ?? 39 1C", "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? E0 ?? ?? ?? ?? ?? ??", v95, v96, v97, v98);
                int v0_138 = new byte["50 F8 0C 00 D0 F8 ?? E0 F0 47 06 1C ?? BB D9 F8 24 E1 29 1C ?? ?? ?? ?? F0 47".split("[ \t]+").length];
                v101 = v0_138;
                int v0_139 = new byte["50 F8 0C 00 D0 F8 ?? E0 F0 47 06 1C ?? BB D9 F8 24 E1 29 1C ?? ?? ?? ?? F0 47".split("[ \t]+").length];
                v102 = v0_139;
                int v0_140 = new byte["?? ?? ?? ?? ?? ?? ?? ?? 01 20 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v103 = v0_140;
                int v0_141 = new byte["?? ?? ?? ?? ?? ?? ?? ?? 01 20 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v104 = v0_141;
                com.chelpus.Utils.convertStringToArraysPatch("50 F8 0C 00 D0 F8 ?? E0 F0 47 06 1C ?? BB D9 F8 24 E1 29 1C ?? ?? ?? ?? F0 47", "?? ?? ?? ?? ?? ?? ?? ?? 01 20 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??", v101, v102, v103, v104);
                int v0_142 = new byte["BA 01 00 00 00 89 54 24 28 BB FD FF FF FF 89 5C 24 30 8B 4C 24 54 33 C0 89 44 24 2C 85 C9 75 44 8B 54 24 58 85 D2 75 23".split("[ \t]+").length];
                v107 = v0_142;
                int v0_143 = new byte["BA 01 00 00 00 89 54 24 28 BB FD FF FF FF 89 5C 24 30 8B 4C 24 54 33 C0 89 44 24 2C 85 C9 75 44 8B 54 24 58 85 D2 75 23".split("[ \t]+").length];
                v108 = v0_143;
                int v0_144 = new byte["?? 00 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 90 90 ?? ?? ?? ?? ?? ?? 90 90".split("[ \t]+").length];
                v109 = v0_144;
                int v0_145 = new byte["?? 00 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 90 90 ?? ?? ?? ?? ?? ?? 90 90".split("[ \t]+").length];
                v110 = v0_145;
                com.chelpus.Utils.convertStringToArraysPatch("BA 01 00 00 00 89 54 24 28 BB FD FF FF FF 89 5C 24 30 8B 4C 24 54 33 C0 89 44 24 2C 85 C9 75 44 8B 54 24 58 85 D2 75 23", "?? 00 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 90 90 ?? ?? ?? ?? ?? ?? 90 90", v107, v108, v109, v110);
                int v0_146 = new byte["85 ED 0F 85 ?? ?? 00 00 8B 54 24 58 8B 6A 1C 8B 5C 24 24 8B 83 ?? 00 00 00 89 44 24 38 8B 4C 24 38 3B E9".split("[ \t]+").length];
                v113 = v0_146;
                int v0_147 = new byte["85 ED 0F 85 ?? ?? 00 00 8B 54 24 58 8B 6A 1C 8B 5C 24 24 8B 83 ?? 00 00 00 89 44 24 38 8B 4C 24 38 3B E9".split("[ \t]+").length];
                v114 = v0_147;
                int v0_148 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ED".split("[ \t]+").length];
                v115 = v0_148;
                int v0_149 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ED".split("[ \t]+").length];
                v116 = v0_149;
                com.chelpus.Utils.convertStringToArraysPatch("85 ED 0F 85 ?? ?? 00 00 8B 54 24 58 8B 6A 1C 8B 5C 24 24 8B 83 ?? 00 00 00 89 44 24 38 8B 4C 24 38 3B E9", "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ED", v113, v114, v115, v116);
                int v0_150 = new byte["3D 00 80 52 42 00 80 12 E2 3B 00 B9 E3 03 1F 2A E3 37 00 B9 3B 02 00 B5 9C 01 00 B5".split("[ \t]+").length];
                v119 = v0_150;
                int v0_151 = new byte["3D 00 80 52 42 00 80 12 E2 3B 00 B9 E3 03 1F 2A E3 37 00 B9 3B 02 00 B5 9C 01 00 B5".split("[ \t]+").length];
                v120 = v0_151;
                int v0_152 = new byte["FD 03 1F 2A ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? FD 03 1F 2A FD 03 1F 2A".split("[ \t]+").length];
                v121 = v0_152;
                int v0_153 = new byte["FD 03 1F 2A ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? FD 03 1F 2A FD 03 1F 2A".split("[ \t]+").length];
                v122 = v0_153;
                com.chelpus.Utils.convertStringToArraysPatch("3D 00 80 52 42 00 80 12 E2 3B 00 B9 E3 03 1F 2A E3 37 00 B9 3B 02 00 B5 9C 01 00 B5", "FD 03 1F 2A ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? FD 03 1F 2A FD 03 1F 2A", v119, v120, v121, v122);
                int v0_154 = new byte["3C 00 80 52 42 00 80 12 E2 3B 00 B9 E3 03 1F 2A E3 37 00 B9 3A 02 00 B5 9B 01 00 B5".split("[ \t]+").length];
                v125 = v0_154;
                int v0_155 = new byte["3C 00 80 52 42 00 80 12 E2 3B 00 B9 E3 03 1F 2A E3 37 00 B9 3A 02 00 B5 9B 01 00 B5".split("[ \t]+").length];
                v126 = v0_155;
                int v0_156 = new byte["FC 03 1F 2A ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? FC 03 1F 2A FC 03 1F 2A".split("[ \t]+").length];
                v127 = v0_156;
                int v0_157 = new byte["FC 03 1F 2A ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? FC 03 1F 2A FC 03 1F 2A".split("[ \t]+").length];
                v128 = v0_157;
                com.chelpus.Utils.convertStringToArraysPatch("3C 00 80 52 42 00 80 12 E2 3B 00 B9 E3 03 1F 2A E3 37 00 B9 3A 02 00 B5 9B 01 00 B5", "FC 03 1F 2A ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? FC 03 1F 2A FC 03 1F 2A", v125, v126, v127, v128);
                int v0_158 = new byte["E2 33 00 B9 E3 33 40 B9 9F 02 03 6B 6A 13 00 54 A0 16 40 B9".split("[ \t]+").length];
                v131 = v0_158;
                int v0_159 = new byte["E2 33 00 B9 E3 33 40 B9 9F 02 03 6B 6A 13 00 54 A0 16 40 B9".split("[ \t]+").length];
                v132 = v0_159;
                int v0_160 = new byte["?? ?? ?? ?? E3 03 14 2A ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v133 = v0_160;
                int v0_161 = new byte["?? ?? ?? ?? E3 03 14 2A ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v134 = v0_161;
                com.chelpus.Utils.convertStringToArraysPatch("E2 33 00 B9 E3 33 40 B9 9F 02 03 6B 6A 13 00 54 A0 16 40 B9", "?? ?? ?? ?? E3 03 14 2A ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??", v131, v132, v133, v134);
                int v0_162 = new byte["D6 02 19 12 56 01 00 35 E1 03 18 AA E0 03 15 AA 00 0C 40 B9 E2 03 1B AA".split("[ \t]+").length];
                v137 = v0_162;
                int v0_163 = new byte["D6 02 19 12 56 01 00 35 E1 03 18 AA E0 03 15 AA 00 0C 40 B9 E2 03 1B AA".split("[ \t]+").length];
                v138 = v0_163;
                int v0_164 = new byte["D6 02 19 32 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v139 = v0_164;
                int v0_165 = new byte["D6 02 19 32 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v140 = v0_165;
                com.chelpus.Utils.convertStringToArraysPatch("D6 02 19 12 56 01 00 35 E1 03 18 AA E0 03 15 AA 00 0C 40 B9 E2 03 1B AA", "D6 02 19 32 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??", v137, v138, v139, v140);
                int v0_166 = new byte[16];
                v195 = v0_166;
                v195 = {57, 102, 8, 0, 57, 102, 4, 0, 18, 22, 15, 6, 18, -10, 40, -2};
                int v0_167 = new byte[16];
                v276 = v0_167;
                v276 = {0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
                int v0_169 = new byte[16];
                v225 = v0_169;
                v225 = {57, 102, 8, 0, 57, 102, 4, 0, 18, 22, 18, 6, 18, 6, 15, 6};
                int v0_170 = new byte[16];
                v322 = v0_170;
                v322 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1};
                int v0_172 = new byte[24];
                v196 = v0_172;
                v196 = {10, 102, 57, 102, 102, 102, 26, 102, 102, 102, 26, 102, 102, 102, 113, 102, 102, 102, 102, 102, 19, 102, -19, -1};
                int v0_173 = new byte[24];
                v277 = v0_173;
                v277 = {0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 0};
                int v0_175 = new byte[24];
                v226 = v0_175;
                v226 = {18, 102, 56, 102, 102, 102, 26, 102, 102, 102, 26, 102, 102, 102, 113, 102, 102, 102, 102, 102, 19, 102, -19, -1};
                int v0_176 = new byte[24];
                v323 = v0_176;
                v323 = {1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
                int v0_178 = new byte[19];
                v197 = v0_178;
                v197 = {-128, 0, 57, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 53, -121, 102, 102, 26};
                int v0_179 = new byte[19];
                v278 = v0_179;
                v278 = {0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0};
                int v0_181 = new byte[19];
                v227 = v0_181;
                v227 = {-128, 0, 57, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 53, -120, 102, 102, 26};
                int v0_182 = new byte[19];
                v324 = v0_182;
                v324 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0};
                int v0_184 = new byte[14];
                v198 = v0_184;
                v198 = {57, 102, 7, 0, 57, 102, 3, 0, 15, 6, 18, -10, 40, -2};
                int v0_185 = new byte[14];
                v279 = v0_185;
                v279 = {0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0};
                int v0_187 = new byte[14];
                v228 = v0_187;
                v228 = {57, 102, 7, 0, 57, 102, 3, 0, 18, 6, 18, 6, 15, 6};
                int v0_188 = new byte[14];
                v325 = v0_188;
                v325 = {0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1};
                int v0_190 = new byte[20];
                v199 = v0_190;
                v199 = {84, 102, 102, 102, 110, 32, 102, 102, 102, 102, 10, 102, 56, 4, 4, 0, 18, 20, 15, 4};
                int v0_191 = new byte[20];
                v280 = v0_191;
                v280 = {0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0};
                int v0_193 = new byte[20];
                v229 = v0_193;
                v229 = {84, 102, 102, 102, 110, 32, 102, 102, 102, 102, 18, 102, 57, 4, 4, 0, 18, 20, 15, 4};
                int v0_194 = new byte[20];
                v326 = v0_194;
                v326 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0};
                int v0_196 = new byte[14];
                v200 = v0_196;
                v200 = {10, 102, 57, 102, 102, 102, 34, 102, 102, 102, 19, 102, -19, -1};
                int v0_197 = new byte[14];
                v281 = v0_197;
                v281 = {0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 0};
                int v0_199 = new byte[14];
                v230 = v0_199;
                v230 = {18, 102, 56, 102, 102, 102, 34, 102, 102, 102, 19, 102, -19, -1};
                int v0_200 = new byte[14];
                v327 = v0_200;
                v327 = {1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
                int v0_202 = new byte[28];
                v201 = v0_202;
                v201 = {29, 102, 102, 102, 102, 102, 102, 102, 12, 102, 57, 102, 102, 0, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 12, 102, 30, 102};
                int v0_203 = new byte[28];
                v282 = v0_203;
                v282 = {0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1};
                int v0_205 = new byte[28];
                v231 = v0_205;
                v231 = {29, 102, 102, 102, 102, 102, 102, 102, 12, 102, 51, 102, 102, 0, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 12, 102, 30, 102};
                int v0_206 = new byte[28];
                v328 = v0_206;
                v328 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
                v162 = 2;
                int v0_208 = new byte[38];
                v165 = v0_208;
                v165 = {29, 102, 102, 102, 102, 102, 102, 102, 12, 102, 57, 102, 102, 0, 8, 102, 102, 102, 102, 102, 102, 102, 8, 102, 102, 102, 8, 102, 102, 102, 2, 102, 102, 102, 2, 102, 102, 102};
                int v0_209 = new byte[38];
                v166 = v0_209;
                v166 = {0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1};
                int v0_210 = new byte[38];
                v167 = v0_210;
                v167 = {29, 102, 102, 102, 102, 102, 102, 102, 12, 102, 51, 102, 102, 0, 8, 102, 102, 102, 102, 102, 102, 102, 8, 102, 102, 102, 8, 102, 102, 102, 2, 102, 102, 102, 2, 102, 102, 102};
                int v0_211 = new byte[38];
                v168 = v0_211;
                v168 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
                v170 = 2;
                int v0_212 = new byte[29];
                v192 = v0_212;
                v192 = {56, 102, 102, 102, 8, 102, 102, 102, 8, 102, 102, 102, 102, 102, 102, 102, 102, 102, 10, 102, 57, 102, 102, 102, 56, 102, 102, 102, 26};
                int v0_213 = new byte[29];
                v273 = v0_213;
                v273 = {0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0};
                int v0_215 = new byte[29];
                v222 = v0_215;
                v222 = {56, 102, 102, 102, 8, 102, 102, 102, 8, 102, 102, 102, 102, 102, 102, 102, 102, 102, 18, 102, 56, 102, 102, 102, 56, 102, 102, 102, 26};
                int v0_216 = new byte[29];
                v319 = v0_216;
                v319 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0};
                int v0_218 = new byte[29];
                v193 = v0_218;
                v193 = {56, 102, 102, 0, 8, 102, 102, 102, 102, 102, 102, 102, 102, 102, 12, 102, 102, 102, 102, 102, 102, 102, 10, 102, 57, 102, 102, 102, 56};
                int v0_219 = new byte[29];
                v274 = v0_219;
                v274 = {0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0};
                int v0_221 = new byte[29];
                v223 = v0_221;
                v223 = {56, 102, 102, 0, 8, 102, 102, 102, 102, 102, 102, 102, 102, 102, 12, 102, 102, 102, 102, 102, 102, 102, 18, 102, 56, 102, 102, 102, 56};
                int v0_222 = new byte[29];
                v320 = v0_222;
                v320 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0};
                int v0_224 = new byte[25];
                v194 = v0_224;
                v194 = {56, 102, 102, 102, 8, 102, 102, 102, 102, 102, 102, 102, 102, 102, 10, 102, 57, 102, 102, 102, 56, 102, 102, 102, 26};
                int v0_225 = new byte[25];
                v275 = v0_225;
                v275 = {0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0};
                int v0_227 = new byte[25];
                v224 = v0_227;
                v224 = {56, 102, 102, 102, 8, 102, 102, 102, 102, 102, 102, 102, 102, 102, 18, 102, 56, 102, 102, 102, 56, 102, 102, 102, 26};
                int v0_228 = new byte[25];
                v321 = v0_228;
                v321 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0};
                int v0_230 = new byte["D5 ?? 80 00 39 ?? ?? ?? 71 20 ?? ?? ?? ?? 54 ?? ?? ?? 52 ?? ?? ?? DD ?? ?? 01 38 ?? ?? 00".split("[ \t]+").length];
                v143 = v0_230;
                int v0_231 = new byte["D5 ?? 80 00 39 ?? ?? ?? 71 20 ?? ?? ?? ?? 54 ?? ?? ?? 52 ?? ?? ?? DD ?? ?? 01 38 ?? ?? 00".split("[ \t]+").length];
                v144 = v0_231;
                int v0_232 = new byte["D6 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v145 = v0_232;
                int v0_233 = new byte["D6 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v146 = v0_233;
                com.chelpus.Utils.convertStringToArraysPatch("D5 ?? 80 00 39 ?? ?? ?? 71 20 ?? ?? ?? ?? 54 ?? ?? ?? 52 ?? ?? ?? DD ?? ?? 01 38 ?? ?? 00", "D6 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??", v143, v144, v145, v146);
                int v0_234 = new byte[35];
                v172 = v0_234;
                v172 = {32, 102, 102, 18, 49, 50, 16, 10, 0, 34, 0, 102, 102, 26, 1, 102, 102, 112, 32, 102, 102, 102, 102, 39, 0, 102, 32, 102, 102, 102, 102, 10, 0, 15, 0};
                int v0_235 = new byte[35];
                v254 = v0_235;
                v254 = {0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0};
                int v0_236 = new byte[35];
                v202 = v0_236;
                v202 = {32, 102, 102, 18, 49, 50, 16, 10, 0, 34, 0, 102, 102, 26, 1, 102, 102, 112, 32, 102, 102, 102, 102, 39, 0, 102, 32, 102, 102, 102, 102, 18, 16, 15, 0};
                int v0_237 = new byte[35];
                v300 = v0_237;
                v300 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0};
                int v0_239 = new byte[36];
                v173 = v0_239;
                v173 = {82, 32, 102, 102, 18, 49, 50, 16, 10, 0, 34, 0, 102, 102, 26, 1, 102, 102, 112, 32, 102, 102, 102, 102, 39, 0, 110, 32, 102, 102, 102, 102, 10, 0, 15, 0};
                int v0_240 = new byte[36];
                v255 = v0_240;
                v255 = {0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0};
                int v0_241 = new byte[36];
                v203 = v0_241;
                v203 = {82, 32, 102, 102, 18, 49, 50, 16, 10, 0, 34, 0, 102, 102, 26, 1, 102, 102, 112, 32, 102, 102, 102, 102, 39, 0, 110, 32, 102, 102, 102, 102, 18, 16, 15, 0};
                int v0_242 = new byte[36];
                v301 = v0_242;
                v301 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0};
                int v0_244 = new byte[30];
                v174 = v0_244;
                v174 = {18, 3, 33, 65, 33, 102, 50, 102, 102, 0, 102, 102, 15, 1, 18, 0, 33, 102, 53, 102, 102, 0, 72, 102, 4, 0, 72, 102, 5, 0};
                int v0_245 = new byte[30];
                v256 = v0_245;
                v256 = {0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0};
                int v0_247 = new byte[30];
                v204 = v0_247;
                v204 = {18, 19, 33, 65, 33, 102, 50, 102, 102, 0, 102, 102, 15, 1, 18, 0, 33, 102, 53, 102, 102, 0, 72, 102, 4, 0, 72, 102, 5, 0};
                int v0_248 = new byte[30];
                v302 = v0_248;
                v302 = {1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
                int v0_250 = new byte[28];
                v175 = v0_250;
                v175 = {18, 1, 33, 66, 33, 102, 50, 102, 102, 0, 15, 1, 18, 0, 33, 102, 53, 102, 102, 0, 72, 102, 4, 0, 72, 102, 5, 0};
                int v0_251 = new byte[28];
                v257 = v0_251;
                v257 = {0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0};
                int v0_253 = new byte[28];
                v205 = v0_253;
                v205 = {18, 17, 33, 66, 33, 102, 50, 102, 102, 0, 15, 1, 18, 0, 33, 102, 53, 102, 102, 0, 72, 102, 4, 0, 72, 102, 5, 0};
                int v0_254 = new byte[28];
                v303 = v0_254;
                v303 = {1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
                int v0_256 = new byte[43];
                v176 = v0_256;
                v176 = {32, 102, 102, 18, 49, 50, 16, 14, 0, 34, 0, 102, 102, 26, 1, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 112, 32, 102, 102, 102, 102, 39, 0, 102, 32, 102, 102, 102, 102, 10, 0, 15, 0};
                int v0_257 = new byte[43];
                v258 = v0_257;
                v258 = {0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0};
                int v0_259 = new byte[43];
                v206 = v0_259;
                v206 = {32, 102, 102, 18, 49, 50, 16, 14, 0, 34, 0, 102, 102, 26, 1, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 112, 32, 102, 102, 102, 102, 39, 0, 102, 32, 102, 102, 102, 102, 18, 16, 15, 0};
                int v0_260 = new byte[43];
                v304 = v0_260;
                v304 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0};
                int v0_262 = new byte[44];
                v177 = v0_262;
                v177 = {56, 102, 102, 102, 58, 102, 102, 102, 58, 102, 102, 102, -112, 102, 102, 102, 33, 102, 55, 102, 102, 102, 34, 102, 102, 102, 112, 102, 102, 102, 102, 102, 39, 102, 102, 102, 102, 102, 102, 102, 10, 0, 15, 0};
                int v0_263 = new byte[44];
                v259 = v0_263;
                v259 = {0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0};
                int v0_265 = new byte[44];
                v207 = v0_265;
                v207 = {56, 102, 102, 102, 58, 102, 102, 102, 58, 102, 102, 102, -112, 102, 102, 102, 33, 102, 55, 102, 102, 102, 34, 102, 102, 102, 112, 102, 102, 102, 102, 102, 39, 102, 102, 102, 102, 102, 102, 102, 18, 16, 15, 0};
                int v0_266 = new byte[44];
                v305 = v0_266;
                v305 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0};
                int v0_268 = new byte[70];
                v178 = v0_268;
                v178 = {56, 102, 102, 102, 58, 102, 102, 102, 58, 102, 102, 102, -112, 102, 102, 102, 33, 102, 55, 102, 102, 102, 34, 102, 102, 102, 112, 102, 102, 102, 102, 102, 39, 102, 33, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 10, 0, 15, 0};
                int v0_269 = new byte[70];
                v260 = v0_269;
                v260 = {0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0};
                int v0_271 = new byte[70];
                v208 = v0_271;
                v208 = {56, 102, 102, 102, 58, 102, 102, 102, 58, 102, 102, 102, -112, 102, 102, 102, 33, 102, 55, 102, 102, 102, 34, 102, 102, 102, 112, 102, 102, 102, 102, 102, 39, 102, 33, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 18, 16, 15, 0};
                int v0_272 = new byte[70];
                v306 = v0_272;
                v306 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0};
                int v0_274 = new byte[56];
                v179 = v0_274;
                v179 = {56, 102, 102, 102, 58, 102, 102, 102, 58, 102, 102, 102, -112, 102, 102, 102, 33, 102, 55, 102, 102, 102, 34, 102, 102, 102, 26, 102, 102, 102, 113, 102, 102, 102, 102, 102, 12, 102, 112, 102, 102, 102, 102, 102, 39, 102, 102, 102, 102, 102, 102, 102, 10, 0, 15, 0};
                int v0_275 = new byte[56];
                v261 = v0_275;
                v261 = {0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0};
                int v0_277 = new byte[56];
                v209 = v0_277;
                v209 = {56, 102, 102, 102, 58, 102, 102, 102, 58, 102, 102, 102, -112, 102, 102, 102, 33, 102, 55, 102, 102, 102, 34, 102, 102, 102, 26, 102, 102, 102, 113, 102, 102, 102, 102, 102, 12, 102, 112, 102, 102, 102, 102, 102, 39, 102, 102, 102, 102, 102, 102, 102, 18, 16, 15, 0};
                int v0_278 = new byte[56];
                v307 = v0_278;
                v307 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1};
                int v0_280 = new byte[41];
                v180 = v0_280;
                v180 = {32, 102, 102, 18, 49, 50, 16, 10, 0, 34, 0, 102, 102, 26, 1, 102, 102, 112, 32, 102, 102, 102, 102, 39, 0, 102, 102, 102, 102, 102, 102, 102, 32, 102, 102, 102, 102, 10, 0, 15, 0};
                int v0_281 = new byte[41];
                v262 = v0_281;
                v262 = {0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0};
                int v0_283 = new byte[41];
                v210 = v0_283;
                v210 = {32, 102, 102, 18, 49, 50, 16, 10, 0, 34, 0, 102, 102, 26, 1, 102, 102, 112, 32, 102, 102, 102, 102, 39, 0, 102, 102, 102, 102, 102, 102, 102, 32, 102, 102, 102, 102, 18, 16, 15, 0};
                int v0_284 = new byte[41];
                v308 = v0_284;
                v308 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0};
                int v0_286 = new byte[50];
                v181 = v0_286;
                v181 = {56, 102, 102, 102, 58, 102, 102, 102, 58, 102, 102, 102, -112, 102, 102, 102, 33, 102, 55, 102, 102, 102, 34, 102, 102, 102, 112, 102, 102, 102, 102, 102, 39, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 10, 0, 15, 0};
                int v0_287 = new byte[50];
                v263 = v0_287;
                v263 = {0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0};
                int v0_289 = new byte[50];
                v211 = v0_289;
                v211 = {56, 102, 102, 102, 58, 102, 102, 102, 58, 102, 102, 102, -112, 102, 102, 102, 33, 102, 55, 102, 102, 102, 34, 102, 102, 102, 112, 102, 102, 102, 102, 102, 39, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 18, 16, 15, 0};
                int v0_290 = new byte[50];
                v309 = v0_290;
                v309 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0};
                int v0_292 = new byte[26];
                v183 = v0_292;
                v183 = {18, 2, 33, 83, 33, 102, 50, 102, 102, 0, 15, 2, 18, 1, 18, 0, 33, 102, 53, 102, 102, 0, 72, 102, 5, 0};
                int v0_293 = new byte[26];
                v265 = v0_293;
                v265 = {0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0};
                int v0_295 = new byte[26];
                v213 = v0_295;
                v213 = {18, 18, 33, 83, 33, 102, 50, 102, 102, 0, 15, 2, 18, 1, 18, 0, 33, 102, 53, 102, 102, 0, 72, 102, 5, 0};
                int v0_296 = new byte[26];
                v311 = v0_296;
                v311 = {1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
                int v0_298 = new byte[36];
                v184 = v0_298;
                v184 = {-14, 32, 102, 102, 18, 49, 50, 16, 10, 0, 34, 0, 102, 102, 26, 1, 102, 102, 112, 32, 102, 102, 102, 102, 39, 0, -8, 32, 102, 102, 102, 102, 10, 0, 15, 0};
                int v0_299 = new byte[36];
                v266 = v0_299;
                v266 = {0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0};
                int v0_301 = new byte[36];
                v214 = v0_301;
                v214 = {-14, 32, 102, 102, 18, 49, 50, 16, 10, 0, 34, 0, 102, 102, 26, 1, 102, 102, 112, 32, 102, 102, 102, 102, 39, 0, -8, 32, 102, 102, 102, 102, 18, 16, 15, 0};
                int v0_302 = new byte[36];
                v312 = v0_302;
                v312 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0};
                v87_0 = v298_0;
                v75_0 = v297_0;
                v63_0 = v296_0;
            }
            if (p332[0].contains("restore")) {
                int v296_1;
                if (!p332[0].contains("restoreCore")) {
                    v296_1 = v63_0;
                } else {
                    v154 = 1;
                    v296_1 = 1;
                }
                int v298_1;
                int v297_1;
                if (!p332[0].contains("restoreServices")) {
                    v298_1 = v87_0;
                    v297_1 = v75_0;
                } else {
                    v298_1 = 1;
                    v297_1 = 1;
                }
                v5 = new byte["11 90 01 21 01 29 0F D1 01 26 28 1C C0 68 39 1C".split("[ \t]+").length];
                v6 = new byte["11 90 01 21 01 29 0F D1 01 26 28 1C C0 68 39 1C".split("[ \t]+").length];
                v7 = new byte["?? ?? 11 99 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v8 = new byte["?? ?? 11 99 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                com.chelpus.Utils.convertStringToArraysPatch("11 90 01 21 01 29 0F D1 01 26 28 1C C0 68 39 1C", "?? ?? 11 99 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??", v5, v6, v7, v8);
                com.chelpus.Utils.convertStringToArraysPatch("11 90 01 21 01 29 0F D1 01 26 28 1C C0 68 39 1C", "?? ?? 11 99 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??", v5, v6, v7, v8);
                v11 = new byte["39 1C 08 68 52 46 D0 F8 CC 01 D0 F8 ?? E0 F0 47 05 1C 01 3C 00 F0 04 80 01 20 06 B0 BD E8 E0 85 D9 F8 ?? E2 F0 47 F7 E7".split("[ \t]+").length];
                v12 = new byte["39 1C 08 68 52 46 D0 F8 CC 01 D0 F8 ?? E0 F0 47 05 1C 01 3C 00 F0 04 80 01 20 06 B0 BD E8 E0 85 D9 F8 ?? E2 F0 47 F7 E7".split("[ \t]+").length];
                v13 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 28 1C ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v14 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 28 1C ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                com.chelpus.Utils.convertStringToArraysPatch("39 1C 08 68 52 46 D0 F8 CC 01 D0 F8 ?? E0 F0 47 05 1C 01 3C 00 F0 04 80 01 20 06 B0 BD E8 E0 85 D9 F8 ?? E2 F0 47 F7 E7", "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 28 1C ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??", v11, v12, v13, v14);
                int v0_307 = new byte["08 68 3A 1C D0 F8 D0 01 43 46 D0 F8 ?? E0 F0 47 05 1C 01 3C 00 F0 04 80 01 20 09 B0 BD E8 E0 8D D9 F8 ?? E2 F0 47 F7 E7".split("[ \t]+").length];
                v17 = v0_307;
                int v0_308 = new byte["08 68 3A 1C D0 F8 D0 01 43 46 D0 F8 ?? E0 F0 47 05 1C 01 3C 00 F0 04 80 01 20 09 B0 BD E8 E0 8D D9 F8 ?? E2 F0 47 F7 E7".split("[ \t]+").length];
                v18 = v0_308;
                int v0_309 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 28 1C ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v19 = v0_309;
                int v0_310 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 28 1C ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v20 = v0_310;
                com.chelpus.Utils.convertStringToArraysPatch("08 68 3A 1C D0 F8 D0 01 43 46 D0 F8 ?? E0 F0 47 05 1C 01 3C 00 F0 04 80 01 20 09 B0 BD E8 E0 8D D9 F8 ?? E2 F0 47 F7 E7", "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 28 1C ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??", v17, v18, v19, v20);
                int v0_311 = new byte["56 45 00 F0 07 80 01 3C 00 F0 31 80 01 20".split("[ \t]+").length];
                v23 = v0_311;
                int v0_312 = new byte["56 45 00 F0 07 80 01 3C 00 F0 31 80 01 20".split("[ \t]+").length];
                v24 = v0_312;
                int v0_313 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 05 98".split("[ \t]+").length];
                v25 = v0_313;
                int v0_314 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 05 98".split("[ \t]+").length];
                v26 = v0_314;
                com.chelpus.Utils.convertStringToArraysPatch("56 45 00 F0 07 80 01 3C 00 F0 31 80 01 20", "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 05 98", v23, v24, v25, v26);
                int v0_315 = new byte["89 44 24 40 8B 5C 24 40 83 FB 01 90 90 BE 01 00 00 00 8B C5 8B 40 0C 8B CF".split("[ \t]+").length];
                v29 = v0_315;
                int v0_316 = new byte["89 44 24 40 8B 5C 24 40 83 FB 01 90 90 BE 01 00 00 00 8B C5 8B 40 0C 8B CF".split("[ \t]+").length];
                v30 = v0_316;
                int v0_317 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 75 32 BE ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v31 = v0_317;
                int v0_318 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 75 32 BE ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v32 = v0_318;
                com.chelpus.Utils.convertStringToArraysPatch("89 44 24 40 8B 5C 24 40 83 FB 01 90 90 BE 01 00 00 00 8B C5 8B 40 0C 8B CF", "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 75 32 BE ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??", v29, v30, v31, v32);
                int v0_319 = new byte["8B 54 24 38 8B CF 8B 01 8B 80 CC 01 00 00 FF 50 28 64 66 83 3D 00 00 00 00 00 8B E8 75 12 B0 01".split("[ \t]+").length];
                v35 = v0_319;
                int v0_320 = new byte["8B 54 24 38 8B CF 8B 01 8B 80 CC 01 00 00 FF 50 28 64 66 83 3D 00 00 00 00 00 8B E8 75 12 B0 01".split("[ \t]+").length];
                v36 = v0_320;
                int v0_321 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 8B C5".split("[ \t]+").length];
                v37 = v0_321;
                int v0_322 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 8B C5".split("[ \t]+").length];
                v38 = v0_322;
                com.chelpus.Utils.convertStringToArraysPatch("8B 54 24 38 8B CF 8B 01 8B 80 CC 01 00 00 FF 50 28 64 66 83 3D 00 00 00 00 00 8B E8 75 12 B0 01", "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 8B C5", v35, v36, v37, v38);
                int v0_323 = new byte["8B 80 D0 01 00 00 8B D7 FF 50 28 64 66 83 3D 00 00 00 00 00 8B E8 75 12 B0 01 8B 6C 24 20".split("[ \t]+").length];
                v41 = v0_323;
                int v0_324 = new byte["8B 80 D0 01 00 00 8B D7 FF 50 28 64 66 83 3D 00 00 00 00 00 8B E8 75 12 B0 01 8B 6C 24 20".split("[ \t]+").length];
                v42 = v0_324;
                int v0_325 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 8B C5 ?? ?? ?? ??".split("[ \t]+").length];
                v43 = v0_325;
                int v0_326 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 8B C5 ?? ?? ?? ??".split("[ \t]+").length];
                v44 = v0_326;
                com.chelpus.Utils.convertStringToArraysPatch("8B 80 D0 01 00 00 8B D7 FF 50 28 64 66 83 3D 00 00 00 00 00 8B E8 75 12 B0 01 8B 6C 24 20", "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 8B C5 ?? ?? ?? ??", v41, v42, v43, v44);
                int v0_327 = new byte["B3 01 89 54 24 10 8B 73 08 8B 44 24 38 8B 48 08 89 4C 24 18 3B F1 74 23 64 66 83 3D 00 00 00 00 00 0F 85 80 00 00 00".split("[ \t]+").length];
                v47 = v0_327;
                int v0_328 = new byte["B3 01 89 54 24 10 8B 73 08 8B 44 24 38 8B 48 08 89 4C 24 18 3B F1 74 23 64 66 83 3D 00 00 00 00 00 0F 85 80 00 00 00".split("[ \t]+").length];
                v48 = v0_328;
                int v0_329 = new byte["33 D2 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v49 = v0_329;
                int v0_330 = new byte["33 D2 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v50 = v0_330;
                com.chelpus.Utils.convertStringToArraysPatch("B3 01 89 54 24 10 8B 73 08 8B 44 24 38 8B 48 08 89 4C 24 18 3B F1 74 23 64 66 83 3D 00 00 00 00 00 0F 85 80 00 00 00", "33 D2 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??", v47, v48, v49, v50);
                int v0_331 = new byte["E5 03 1A AA C0 03 3F D6 FB 03 00 2A 7F 07 00 71 35 00 80 52 35 00 80 52 E1 03 16 AA 2A 00 40 B9 E0 03 14 AA".split("[ \t]+").length];
                v53 = v0_331;
                int v0_332 = new byte["E5 03 1A AA C0 03 3F D6 FB 03 00 2A 7F 07 00 71 35 00 80 52 35 00 80 52 E1 03 16 AA 2A 00 40 B9 E0 03 14 AA".split("[ \t]+").length];
                v54 = v0_332;
                int v0_333 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 61 02 00 54 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v55 = v0_333;
                int v0_334 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 61 02 00 54 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v56 = v0_334;
                com.chelpus.Utils.convertStringToArraysPatch("E5 03 1A AA C0 03 3F D6 FB 03 00 2A 7F 07 00 71 35 00 80 52 35 00 80 52 E1 03 16 AA 2A 00 40 B9 E0 03 14 AA", "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 61 02 00 54 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??", v53, v54, v55, v56);
                int v0_335 = new byte["F7 03 01 AA F9 03 02 AA 3A 00 80 52 F5 0A 40 B9 38 0B 40 B9 BF 02 18 6B".split("[ \t]+").length];
                v59 = v0_335;
                int v0_336 = new byte["F7 03 01 AA F9 03 02 AA 3A 00 80 52 F5 0A 40 B9 38 0B 40 B9 BF 02 18 6B".split("[ \t]+").length];
                v60 = v0_336;
                int v0_337 = new byte["?? ?? ?? ?? ?? ?? ?? ?? FA 03 1F 2A ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v61 = v0_337;
                int v0_338 = new byte["?? ?? ?? ?? ?? ?? ?? ?? FA 03 1F 2A ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v62 = v0_338;
                com.chelpus.Utils.convertStringToArraysPatch("F7 03 01 AA F9 03 02 AA 3A 00 80 52 F5 0A 40 B9 38 0B 40 B9 BF 02 18 6B", "?? ?? ?? ?? ?? ?? ?? ?? FA 03 1F 2A ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??", v59, v60, v61, v62);
                int v0_339 = new byte["CD F8 30 C0 78 B9 1A 99 31 B9 01 3C 00 F0 ?? 80 00 20".split("[ \t]+").length];
                v65 = v0_339;
                int v0_340 = new byte["CD F8 30 C0 78 B9 1A 99 31 B9 01 3C 00 F0 ?? 80 00 20".split("[ \t]+").length];
                v66 = v0_340;
                int v0_341 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 0B 98".split("[ \t]+").length];
                v67 = v0_341;
                int v0_342 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 0B 98".split("[ \t]+").length];
                v68 = v0_342;
                com.chelpus.Utils.convertStringToArraysPatch("CD F8 30 C0 78 B9 1A 99 31 B9 01 3C 00 F0 ?? 80 00 20", "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 0B 98", v65, v66, v67, v68);
                int v0_343 = new byte["00 2D 40 F0 ?? 80 16 99 CD 69 DA F8 E0 20 0B 92 0B 9B 9B 42".split("[ \t]+").length];
                v71 = v0_343;
                int v0_344 = new byte["00 2D 40 F0 ?? 80 16 99 CD 69 DA F8 E0 20 0B 92 0B 9B 9B 42".split("[ \t]+").length];
                v72 = v0_344;
                int v0_345 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 9D ??".split("[ \t]+").length];
                v73 = v0_345;
                int v0_346 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 9D ??".split("[ \t]+").length];
                v74 = v0_346;
                com.chelpus.Utils.convertStringToArraysPatch("00 2D 40 F0 ?? 80 16 99 CD 69 DA F8 E0 20 0B 92 0B 9B 9B 42", "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 9D ??", v71, v72, v73, v74);
                int v0_347 = new byte["2D E9 E0 4D 8D B0 07 1C 00 90 15 91 16 92 17 93 16 9A 56 6A 31 1C 08 68 01 20 0D B0 BD E8 E0 8D F0 47".split("[ \t]+").length];
                v77 = v0_347;
                int v0_348 = new byte["2D E9 E0 4D 8D B0 07 1C 00 90 15 91 16 92 17 93 16 9A 56 6A 31 1C 08 68 01 20 0D B0 BD E8 E0 8D F0 47".split("[ \t]+").length];
                v78 = v0_348;
                int v0_349 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? D0 F8 BC 01 D0 F8 28 E0 ?? ??".split("[ \t]+").length];
                v79 = v0_349;
                int v0_350 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? D0 F8 BC 01 D0 F8 28 E0 ?? ??".split("[ \t]+").length];
                v80 = v0_350;
                com.chelpus.Utils.convertStringToArraysPatch("2D E9 E0 4D 8D B0 07 1C 00 90 15 91 16 92 17 93 16 9A 56 6A 31 1C 08 68 01 20 0D B0 BD E8 E0 8D F0 47", "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? D0 F8 BC 01 D0 F8 28 E0 ?? ??", v77, v78, v79, v80);
                int v0_351 = new byte["1C BF D9 F8 B0 E1 F0 47 BF F3 5B 8F 31 1C 08 68 D0 F8 ?? 02 D0 F8 28 E0 00 20 14 90 14 ?? 00 ?? ?? D1".split("[ \t]+").length];
                v83 = v0_351;
                int v0_352 = new byte["1C BF D9 F8 B0 E1 F0 47 BF F3 5B 8F 31 1C 08 68 D0 F8 ?? 02 D0 F8 28 E0 00 20 14 90 14 ?? 00 ?? ?? D1".split("[ \t]+").length];
                v84 = v0_352;
                int v0_353 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? F0 47 ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v85 = v0_353;
                int v0_354 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? F0 47 ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v86 = v0_354;
                com.chelpus.Utils.convertStringToArraysPatch("1C BF D9 F8 B0 E1 F0 47 BF F3 5B 8F 31 1C 08 68 D0 F8 ?? 02 D0 F8 28 E0 00 20 14 90 14 ?? 00 ?? ?? D1", "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? F0 47 ?? ?? ?? ?? ?? ?? ?? ??", v83, v84, v85, v86);
                int v0_355 = new byte["F0 47 82 46 BA F1 00 0F ?? E0 01 3C ?? F0 ?? ?? 28 ?? 00 2B".split("[ \t]+").length];
                v89 = v0_355;
                int v0_356 = new byte["F0 47 82 46 BA F1 00 0F ?? E0 01 3C ?? F0 ?? ?? 28 ?? 00 2B".split("[ \t]+").length];
                v90 = v0_356;
                int v0_357 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? D1 ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v91 = v0_357;
                int v0_358 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? D1 ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v92 = v0_358;
                com.chelpus.Utils.convertStringToArraysPatch("F0 47 82 46 BA F1 00 0F ?? E0 01 3C ?? F0 ?? ?? 28 ?? 00 2B", "?? ?? ?? ?? ?? ?? ?? ?? ?? D1 ?? ?? ?? ?? ?? ?? ?? ?? ?? ??", v89, v90, v91, v92);
                int v0_359 = new byte["F8 0C 00 D0 F8 ?? E0 F0 47 80 46 B8 F1 00 0F ?? E0 D9 F8 ?? ?? 39 1C".split("[ \t]+").length];
                v95 = v0_359;
                int v0_360 = new byte["F8 0C 00 D0 F8 ?? E0 F0 47 80 46 B8 F1 00 0F ?? E0 D9 F8 ?? ?? 39 1C".split("[ \t]+").length];
                v96 = v0_360;
                int v0_361 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? D1 ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v97 = v0_361;
                int v0_362 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? D1 ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v98 = v0_362;
                com.chelpus.Utils.convertStringToArraysPatch("F8 0C 00 D0 F8 ?? E0 F0 47 80 46 B8 F1 00 0F ?? E0 D9 F8 ?? ?? 39 1C", "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? D1 ?? ?? ?? ?? ?? ??", v95, v96, v97, v98);
                int v0_363 = new byte["50 F8 0C 00 D0 F8 ?? E0 01 20 06 1C ?? BB D9 F8 24 E1 29 1C ?? ?? ?? ?? F0 47".split("[ \t]+").length];
                v101 = v0_363;
                int v0_364 = new byte["50 F8 0C 00 D0 F8 ?? E0 01 20 06 1C ?? BB D9 F8 24 E1 29 1C ?? ?? ?? ?? F0 47".split("[ \t]+").length];
                v102 = v0_364;
                int v0_365 = new byte["?? ?? ?? ?? ?? ?? ?? ?? F0 47 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v103 = v0_365;
                int v0_366 = new byte["?? ?? ?? ?? ?? ?? ?? ?? F0 47 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v104 = v0_366;
                com.chelpus.Utils.convertStringToArraysPatch("50 F8 0C 00 D0 F8 ?? E0 01 20 06 1C ?? BB D9 F8 24 E1 29 1C ?? ?? ?? ?? F0 47", "?? ?? ?? ?? ?? ?? ?? ?? F0 47 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??", v101, v102, v103, v104);
                int v0_367 = new byte["BA 00 00 00 00 89 54 24 28 BB FD FF FF FF 89 5C 24 30 8B 4C 24 54 33 C0 89 44 24 2C 85 C9 90 90 8B 54 24 58 85 D2 90 90".split("[ \t]+").length];
                v107 = v0_367;
                int v0_368 = new byte["BA 00 00 00 00 89 54 24 28 BB FD FF FF FF 89 5C 24 30 8B 4C 24 54 33 C0 89 44 24 2C 85 C9 90 90 8B 54 24 58 85 D2 90 90".split("[ \t]+").length];
                v108 = v0_368;
                int v0_369 = new byte["?? 01 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 75 44 ?? ?? ?? ?? ?? ?? 75 23".split("[ \t]+").length];
                v109 = v0_369;
                int v0_370 = new byte["?? 01 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 75 44 ?? ?? ?? ?? ?? ?? 75 23".split("[ \t]+").length];
                v110 = v0_370;
                com.chelpus.Utils.convertStringToArraysPatch("BA 00 00 00 00 89 54 24 28 BB FD FF FF FF 89 5C 24 30 8B 4C 24 54 33 C0 89 44 24 2C 85 C9 90 90 8B 54 24 58 85 D2 90 90", "?? 01 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 75 44 ?? ?? ?? ?? ?? ?? 75 23", v107, v108, v109, v110);
                int v0_371 = new byte["85 ED 0F 85 ?? ?? 00 00 8B 54 24 58 8B 6A 1C 8B 5C 24 24 8B 83 ?? 00 00 00 89 44 24 38 8B 4C 24 38 3B ED".split("[ \t]+").length];
                v113 = v0_371;
                int v0_372 = new byte["85 ED 0F 85 ?? ?? 00 00 8B 54 24 58 8B 6A 1C 8B 5C 24 24 8B 83 ?? 00 00 00 89 44 24 38 8B 4C 24 38 3B ED".split("[ \t]+").length];
                v114 = v0_372;
                int v0_373 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? E9".split("[ \t]+").length];
                v115 = v0_373;
                int v0_374 = new byte["?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? E9".split("[ \t]+").length];
                v116 = v0_374;
                com.chelpus.Utils.convertStringToArraysPatch("85 ED 0F 85 ?? ?? 00 00 8B 54 24 58 8B 6A 1C 8B 5C 24 24 8B 83 ?? 00 00 00 89 44 24 38 8B 4C 24 38 3B ED", "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? E9", v113, v114, v115, v116);
                int v0_375 = new byte["FD 03 1F 2A 42 00 80 12 E2 3B 00 B9 E3 03 1F 2A E3 37 00 B9 FD 03 1F 2A FD 03 1F 2A".split("[ \t]+").length];
                v119 = v0_375;
                int v0_376 = new byte["FD 03 1F 2A 42 00 80 12 E2 3B 00 B9 E3 03 1F 2A E3 37 00 B9 FD 03 1F 2A FD 03 1F 2A".split("[ \t]+").length];
                v120 = v0_376;
                int v0_377 = new byte["3D 00 80 52 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 3B 02 00 B5 9C 01 00 B5".split("[ \t]+").length];
                v121 = v0_377;
                int v0_378 = new byte["3D 00 80 52 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 3B 02 00 B5 9C 01 00 B5".split("[ \t]+").length];
                v122 = v0_378;
                com.chelpus.Utils.convertStringToArraysPatch("FD 03 1F 2A 42 00 80 12 E2 3B 00 B9 E3 03 1F 2A E3 37 00 B9 FD 03 1F 2A FD 03 1F 2A", "3D 00 80 52 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 3B 02 00 B5 9C 01 00 B5", v119, v120, v121, v122);
                int v0_379 = new byte["FC 03 1F 2A 42 00 80 12 E2 3B 00 B9 E3 03 1F 2A E3 37 00 B9 FC 03 1F 2A FC 03 1F 2A".split("[ \t]+").length];
                v125 = v0_379;
                int v0_380 = new byte["FC 03 1F 2A 42 00 80 12 E2 3B 00 B9 E3 03 1F 2A E3 37 00 B9 FC 03 1F 2A FC 03 1F 2A".split("[ \t]+").length];
                v126 = v0_380;
                int v0_381 = new byte["3C 00 80 52 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 3A 02 00 B5 9B 01 00 B5".split("[ \t]+").length];
                v127 = v0_381;
                int v0_382 = new byte["3C 00 80 52 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 3A 02 00 B5 9B 01 00 B5".split("[ \t]+").length];
                v128 = v0_382;
                com.chelpus.Utils.convertStringToArraysPatch("FC 03 1F 2A 42 00 80 12 E2 3B 00 B9 E3 03 1F 2A E3 37 00 B9 FC 03 1F 2A FC 03 1F 2A", "3C 00 80 52 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 3A 02 00 B5 9B 01 00 B5", v125, v126, v127, v128);
                int v0_383 = new byte["E2 33 00 B9 E3 03 14 2A 9F 02 03 6B 6A 13 00 54 A0 16 40 B9".split("[ \t]+").length];
                v131 = v0_383;
                int v0_384 = new byte["E2 33 00 B9 E3 03 14 2A 9F 02 03 6B 6A 13 00 54 A0 16 40 B9".split("[ \t]+").length];
                v132 = v0_384;
                int v0_385 = new byte["?? ?? ?? ?? E3 33 40 B9 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v133 = v0_385;
                int v0_386 = new byte["?? ?? ?? ?? E3 33 40 B9 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v134 = v0_386;
                com.chelpus.Utils.convertStringToArraysPatch("E2 33 00 B9 E3 03 14 2A 9F 02 03 6B 6A 13 00 54 A0 16 40 B9", "?? ?? ?? ?? E3 33 40 B9 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??", v131, v132, v133, v134);
                int v0_387 = new byte["D6 02 19 32 56 01 00 35 E1 03 18 AA E0 03 15 AA 00 0C 40 B9 E2 03 1B AA".split("[ \t]+").length];
                v137 = v0_387;
                int v0_388 = new byte["D6 02 19 32 56 01 00 35 E1 03 18 AA E0 03 15 AA 00 0C 40 B9 E2 03 1B AA".split("[ \t]+").length];
                v138 = v0_388;
                int v0_389 = new byte["D6 02 19 12 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v139 = v0_389;
                int v0_390 = new byte["D6 02 19 12 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v140 = v0_390;
                com.chelpus.Utils.convertStringToArraysPatch("D6 02 19 32 56 01 00 35 E1 03 18 AA E0 03 15 AA 00 0C 40 B9 E2 03 1B AA", "D6 02 19 12 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??", v137, v138, v139, v140);
                int v0_391 = new byte[35];
                v182 = v0_391;
                v182 = {32, 102, 102, 18, 16, 15, 0, 0, 0, 34, 0, 102, 102, 26, 1, 102, 102, 112, 32, 102, 102, 102, 102, 39, 0, 102, 32, 102, 102, 102, 102, 10, 0, 15, 0};
                int v0_392 = new byte[35];
                v264 = v0_392;
                v264 = {0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0};
                int v0_394 = new byte[35];
                v212 = v0_394;
                v212 = {32, 102, 102, 18, 49, 50, 16, 10, 0, 34, 0, 102, 102, 26, 1, 102, 102, 112, 32, 102, 102, 102, 102, 39, 0, 102, 32, 102, 102, 102, 102, 10, 0, 15, 0};
                int v0_395 = new byte[35];
                v310 = v0_395;
                v310 = {0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
                int v0_397 = new byte[36];
                v185 = v0_397;
                v185 = {82, 32, 102, 102, 18, 16, 15, 0, 0, 0, 34, 0, 102, 102, 26, 1, 102, 102, 112, 32, 102, 102, 102, 102, 39, 0, 110, 32, 102, 102, 102, 102, 10, 0, 15, 0};
                int v0_398 = new byte[36];
                v215 = v0_398;
                v215 = {82, 32, 102, 102, 18, 49, 50, 16, 10, 0, 34, 0, 102, 102, 26, 1, 102, 102, 112, 32, 102, 102, 102, 102, 39, 0, 110, 32, 102, 102, 102, 102, 10, 0, 15, 0};
                int v0_399 = new byte[30];
                v186 = v0_399;
                v186 = {18, 16, 15, 0, 33, 102, 50, 102, 102, 0, 102, 102, 15, 1, 18, 0, 33, 102, 53, 102, 102, 0, 72, 102, 4, 0, 72, 102, 5, 0};
                int v0_400 = new byte[30];
                v267 = v0_400;
                v267 = {0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0};
                int v0_402 = new byte[30];
                v216 = v0_402;
                v216 = {18, 3, 33, 65, 33, 102, 50, 102, 102, 0, 102, 102, 15, 1, 18, 0, 33, 102, 53, 102, 102, 0, 72, 102, 4, 0, 72, 102, 5, 0};
                int v0_403 = new byte[30];
                v313 = v0_403;
                v313 = {1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
                int v0_405 = new byte[28];
                v187 = v0_405;
                v187 = {18, 16, 15, 0, 33, 102, 50, 102, 102, 0, 15, 1, 18, 0, 33, 102, 53, 102, 102, 0, 72, 102, 4, 0, 72, 102, 5, 0};
                int v0_406 = new byte[28];
                v268 = v0_406;
                v268 = {0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0};
                int v0_408 = new byte[28];
                v217 = v0_408;
                v217 = {18, 1, 33, 66, 33, 102, 50, 102, 102, 0, 15, 1, 18, 0, 33, 102, 53, 102, 102, 0, 72, 102, 4, 0, 72, 102, 5, 0};
                int v0_409 = new byte[28];
                v314 = v0_409;
                v314 = {1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
                int v0_411 = new byte[43];
                v188 = v0_411;
                v188 = {32, 102, 102, 18, 16, 15, 0, 0, 0, 34, 0, 102, 102, 26, 1, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 112, 32, 102, 102, 102, 102, 39, 0, 102, 32, 102, 102, 102, 102, 10, 0, 15, 0};
                int v0_412 = new byte[43];
                v269 = v0_412;
                v269 = {0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0};
                int v0_414 = new byte[43];
                v218 = v0_414;
                v218 = {32, 102, 102, 18, 49, 50, 16, 14, 0, 34, 0, 102, 102, 26, 1, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 112, 32, 102, 102, 102, 102, 39, 0, 102, 32, 102, 102, 102, 102, 10, 0, 15, 0};
                int v0_415 = new byte[43];
                v315 = v0_415;
                v315 = {0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
                int v0_417 = new byte[30];
                v189 = v0_417;
                v189 = {82, 32, 102, 102, 18, 16, 15, 0, 0, 0, 34, 0, 102, 102, 26, 1, 102, 102, 112, 32, 102, 102, 102, 102, 39, 0, 56, 3, 11, 0};
                int v0_418 = new byte[30];
                v270 = v0_418;
                v270 = {0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0};
                int v0_420 = new byte[30];
                v219 = v0_420;
                v219 = {82, 32, 102, 102, 18, 49, 50, 16, 10, 0, 34, 0, 102, 102, 26, 1, 102, 102, 112, 32, 102, 102, 102, 102, 39, 0, 56, 3, 11, 0};
                int v0_421 = new byte[30];
                v316 = v0_421;
                v316 = {0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
                int v0_423 = new byte[30];
                v190 = v0_423;
                v190 = {-14, 32, 102, 102, 18, 16, 15, 0, 0, 0, 34, 0, 102, 102, 26, 1, 102, 102, 112, 32, 102, 102, 102, 102, 39, 0, 56, 3, 11, 0};
                int v0_424 = new byte[30];
                v271 = v0_424;
                v271 = {0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0};
                int v0_426 = new byte[30];
                v220 = v0_426;
                v220 = {-14, 32, 102, 102, 18, 49, 50, 16, 10, 0, 34, 0, 102, 102, 26, 1, 102, 102, 112, 32, 102, 102, 102, 102, 39, 0, 56, 3, 11, 0};
                int v0_427 = new byte[30];
                v317 = v0_427;
                v317 = {0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
                int v0_429 = new byte[38];
                v191 = v0_429;
                v191 = {-14, 32, 102, 102, 18, 16, 15, 0, 0, 0, 34, 0, 102, 102, 26, 1, 102, 102, 113, 16, 102, 102, 102, 102, 12, 1, 112, 32, 102, 102, 102, 102, 39, 0, 56, 3, 11, 0};
                int v0_430 = new byte[38];
                v272 = v0_430;
                v272 = {0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0};
                int v0_432 = new byte[38];
                v221 = v0_432;
                v221 = {-14, 32, 102, 102, 18, 49, 50, 16, 14, 0, 34, 0, 102, 102, 26, 1, 102, 102, 113, 16, 102, 102, 102, 102, 12, 1, 112, 32, 102, 102, 102, 102, 39, 0, 56, 3, 11, 0};
                int v0_433 = new byte[38];
                v318 = v0_433;
                v318 = {0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
                int v0_435 = new byte[16];
                v195 = v0_435;
                v195 = {57, 102, 8, 0, 57, 102, 4, 0, 18, 22, 18, 6, 18, 6, 15, 6};
                int v0_436 = new byte[16];
                v276 = v0_436;
                v276 = {0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
                int v0_438 = new byte[16];
                v225 = v0_438;
                v225 = {57, 102, 8, 0, 57, 102, 4, 0, 18, 22, 15, 6, 18, -10, 40, -2};
                int v0_439 = new byte[16];
                v322 = v0_439;
                v322 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1};
                int v0_441 = new byte[24];
                v196 = v0_441;
                v196 = {18, 102, 56, 102, 102, 102, 26, 102, 102, 102, 26, 102, 102, 102, 113, 102, 102, 102, 102, 102, 19, 102, -19, -1};
                int v0_442 = new byte[24];
                v277 = v0_442;
                v277 = {0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 0};
                int v0_444 = new byte[24];
                v226 = v0_444;
                v226 = {10, 102, 57, 102, 102, 102, 26, 102, 102, 102, 26, 102, 102, 102, 113, 102, 102, 102, 102, 102, 19, 102, -19, -1};
                int v0_445 = new byte[24];
                v323 = v0_445;
                v323 = {1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
                int v0_447 = new byte[19];
                v197 = v0_447;
                v197 = {-128, 0, 57, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 53, -120, 102, 102, 26};
                int v0_448 = new byte[19];
                v278 = v0_448;
                v278 = {0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0};
                int v0_450 = new byte[19];
                v227 = v0_450;
                v227 = {-128, 0, 57, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 53, -121, 102, 102, 26};
                int v0_451 = new byte[19];
                v324 = v0_451;
                v324 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0};
                int v0_453 = new byte[14];
                v198 = v0_453;
                v198 = {57, 102, 7, 0, 57, 102, 3, 0, 18, 6, 18, 6, 15, 6};
                int v0_454 = new byte[14];
                v279 = v0_454;
                v279 = {0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0};
                int v0_456 = new byte[14];
                v228 = v0_456;
                v228 = {57, 102, 7, 0, 57, 102, 3, 0, 15, 6, 18, -10, 40, -2};
                int v0_457 = new byte[14];
                v325 = v0_457;
                v325 = {0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1};
                int v0_459 = new byte[20];
                v199 = v0_459;
                v199 = {84, 102, 102, 102, 110, 32, 102, 102, 102, 102, 18, 102, 57, 4, 4, 0, 18, 20, 15, 4};
                int v0_460 = new byte[20];
                v280 = v0_460;
                v280 = {0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0};
                int v0_462 = new byte[20];
                v229 = v0_462;
                v229 = {84, 102, 102, 102, 110, 32, 102, 102, 102, 102, 10, 102, 56, 4, 4, 0, 18, 20, 15, 4};
                int v0_463 = new byte[20];
                v326 = v0_463;
                v326 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0};
                int v0_465 = new byte[14];
                v200 = v0_465;
                v200 = {18, 102, 56, 102, 102, 102, 34, 102, 102, 102, 19, 102, -19, -1};
                int v0_466 = new byte[14];
                v281 = v0_466;
                v281 = {0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 0};
                int v0_468 = new byte[14];
                v230 = v0_468;
                v230 = {10, 102, 57, 102, 102, 102, 34, 102, 102, 102, 19, 102, -19, -1};
                int v0_469 = new byte[14];
                v327 = v0_469;
                v327 = {1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
                int v0_471 = new byte[28];
                v201 = v0_471;
                v201 = {29, 102, 102, 102, 102, 102, 102, 102, 12, 102, 51, 102, 102, 0, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 12, 102, 30, 102};
                int v0_472 = new byte[28];
                v282 = v0_472;
                v282 = {0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1};
                int v0_474 = new byte[28];
                v231 = v0_474;
                v231 = {29, 102, 102, 102, 102, 102, 102, 102, 12, 102, 57, 102, 102, 0, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 12, 102, 30, 102};
                int v0_475 = new byte[28];
                v328 = v0_475;
                v328 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
                v162 = 0;
                int v0_477 = new byte[38];
                v165 = v0_477;
                v165 = {29, 102, 102, 102, 102, 102, 102, 102, 12, 102, 51, 102, 102, 0, 8, 102, 102, 102, 102, 102, 102, 102, 8, 102, 102, 102, 8, 102, 102, 102, 2, 102, 102, 102, 2, 102, 102, 102};
                int v0_478 = new byte[38];
                v166 = v0_478;
                v166 = {0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1};
                int v0_479 = new byte[38];
                v167 = v0_479;
                v167 = {29, 102, 102, 102, 102, 102, 102, 102, 12, 102, 57, 102, 102, 0, 8, 102, 102, 102, 102, 102, 102, 102, 8, 102, 102, 102, 8, 102, 102, 102, 2, 102, 102, 102, 2, 102, 102, 102};
                int v0_480 = new byte[38];
                v168 = v0_480;
                v168 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
                v170 = 0;
                int v0_481 = new byte[29];
                v192 = v0_481;
                v192 = {56, 102, 102, 102, 8, 102, 102, 102, 8, 102, 102, 102, 102, 102, 102, 102, 102, 102, 18, 102, 56, 102, 102, 102, 56, 102, 102, 102, 26};
                int v0_482 = new byte[29];
                v273 = v0_482;
                v273 = {0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0};
                int v0_484 = new byte[29];
                v222 = v0_484;
                v222 = {56, 102, 102, 102, 8, 102, 102, 102, 8, 102, 102, 102, 102, 102, 102, 102, 102, 102, 10, 102, 57, 102, 102, 102, 56, 102, 102, 102, 26};
                int v0_485 = new byte[29];
                v319 = v0_485;
                v319 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0};
                int v0_487 = new byte[29];
                v193 = v0_487;
                v193 = {56, 102, 102, 0, 8, 102, 102, 102, 102, 102, 102, 102, 102, 102, 12, 102, 102, 102, 102, 102, 102, 102, 18, 102, 56, 102, 102, 102, 56};
                int v0_488 = new byte[29];
                v274 = v0_488;
                v274 = {0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0};
                int v0_490 = new byte[29];
                v223 = v0_490;
                v223 = {56, 102, 102, 0, 8, 102, 102, 102, 102, 102, 102, 102, 102, 102, 12, 102, 102, 102, 102, 102, 102, 102, 10, 102, 57, 102, 102, 102, 56};
                int v0_491 = new byte[29];
                v320 = v0_491;
                v320 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0};
                int v0_493 = new byte[25];
                v194 = v0_493;
                v194 = {56, 102, 102, 102, 8, 102, 102, 102, 102, 102, 102, 102, 102, 102, 18, 102, 56, 102, 102, 102, 56, 102, 102, 102, 26};
                int v0_494 = new byte[25];
                v275 = v0_494;
                v275 = {0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0};
                int v0_496 = new byte[25];
                v224 = v0_496;
                v224 = {56, 102, 102, 102, 8, 102, 102, 102, 102, 102, 102, 102, 102, 102, 10, 102, 57, 102, 102, 102, 56, 102, 102, 102, 26};
                int v0_497 = new byte[25];
                v321 = v0_497;
                v321 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0};
                int v0_499 = new byte["D6 ?? 80 00 39 ?? ?? ?? 71 20 ?? ?? ?? ?? 54 ?? ?? ?? 52 ?? ?? ?? DD ?? ?? 01 38 ?? ?? 00".split("[ \t]+").length];
                v143 = v0_499;
                int v0_500 = new byte["D6 ?? 80 00 39 ?? ?? ?? 71 20 ?? ?? ?? ?? 54 ?? ?? ?? 52 ?? ?? ?? DD ?? ?? 01 38 ?? ?? 00".split("[ \t]+").length];
                v144 = v0_500;
                int v0_501 = new byte["D5 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v145 = v0_501;
                int v0_502 = new byte["D5 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??".split("[ \t]+").length];
                v146 = v0_502;
                com.chelpus.Utils.convertStringToArraysPatch("D6 ?? 80 00 39 ?? ?? ?? 71 20 ?? ?? ?? ?? 54 ?? ?? ?? 52 ?? ?? ?? DD ?? ?? 01 38 ?? ?? 00", "D5 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ??", v143, v144, v145, v146);
                int v0_503 = new byte[35];
                v172 = v0_503;
                v172 = {32, 102, 102, 18, 49, 50, 16, 10, 0, 34, 0, 102, 102, 26, 1, 102, 102, 112, 32, 102, 102, 102, 102, 39, 0, 102, 32, 102, 102, 102, 102, 18, 16, 15, 0};
                int v0_504 = new byte[35];
                v254 = v0_504;
                v254 = {0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0};
                int v0_505 = new byte[35];
                v202 = v0_505;
                v202 = {32, 102, 102, 18, 49, 50, 16, 10, 0, 34, 0, 102, 102, 26, 1, 102, 102, 112, 32, 102, 102, 102, 102, 39, 0, 102, 32, 102, 102, 102, 102, 10, 0, 15, 0};
                int v0_506 = new byte[35];
                v300 = v0_506;
                v300 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0};
                int v0_508 = new byte[36];
                v173 = v0_508;
                v173 = {82, 32, 102, 102, 18, 49, 50, 16, 10, 0, 34, 0, 102, 102, 26, 1, 102, 102, 112, 32, 102, 102, 102, 102, 39, 0, 110, 32, 102, 102, 102, 102, 18, 16, 15, 0};
                int v0_509 = new byte[36];
                v255 = v0_509;
                v255 = {0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0};
                int v0_510 = new byte[36];
                v203 = v0_510;
                v203 = {82, 32, 102, 102, 18, 49, 50, 16, 10, 0, 34, 0, 102, 102, 26, 1, 102, 102, 112, 32, 102, 102, 102, 102, 39, 0, 110, 32, 102, 102, 102, 102, 10, 0, 15, 0};
                int v0_511 = new byte[36];
                v301 = v0_511;
                v301 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0};
                int v0_513 = new byte[30];
                v174 = v0_513;
                v174 = {18, 19, 33, 65, 33, 102, 50, 102, 102, 0, 102, 102, 15, 1, 18, 0, 33, 102, 53, 102, 102, 0, 72, 102, 4, 0, 72, 102, 5, 0};
                int v0_514 = new byte[30];
                v256 = v0_514;
                v256 = {0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0};
                int v0_516 = new byte[30];
                v204 = v0_516;
                v204 = {18, 3, 33, 65, 33, 102, 50, 102, 102, 0, 102, 102, 15, 1, 18, 0, 33, 102, 53, 102, 102, 0, 72, 102, 4, 0, 72, 102, 5, 0};
                int v0_517 = new byte[30];
                v302 = v0_517;
                v302 = {1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
                int v0_519 = new byte[28];
                v175 = v0_519;
                v175 = {18, 17, 33, 66, 33, 102, 50, 102, 102, 0, 15, 1, 18, 0, 33, 102, 53, 102, 102, 0, 72, 102, 4, 0, 72, 102, 5, 0};
                int v0_520 = new byte[28];
                v257 = v0_520;
                v257 = {0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0};
                int v0_522 = new byte[28];
                v205 = v0_522;
                v205 = {18, 1, 33, 66, 33, 102, 50, 102, 102, 0, 15, 1, 18, 0, 33, 102, 53, 102, 102, 0, 72, 102, 4, 0, 72, 102, 5, 0};
                int v0_523 = new byte[28];
                v303 = v0_523;
                v303 = {1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
                int v0_525 = new byte[43];
                v176 = v0_525;
                v176 = {32, 102, 102, 18, 49, 50, 16, 14, 0, 34, 0, 102, 102, 26, 1, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 112, 32, 102, 102, 102, 102, 39, 0, 102, 32, 102, 102, 102, 102, 18, 16, 15, 0};
                int v0_526 = new byte[43];
                v258 = v0_526;
                v258 = {0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0};
                int v0_528 = new byte[43];
                v206 = v0_528;
                v206 = {32, 102, 102, 18, 49, 50, 16, 14, 0, 34, 0, 102, 102, 26, 1, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 112, 32, 102, 102, 102, 102, 39, 0, 102, 32, 102, 102, 102, 102, 10, 0, 15, 0};
                int v0_529 = new byte[43];
                v304 = v0_529;
                v304 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0};
                int v0_531 = new byte[44];
                v177 = v0_531;
                v177 = {56, 102, 102, 102, 58, 102, 102, 102, 58, 102, 102, 102, -112, 102, 102, 102, 33, 102, 55, 102, 102, 102, 34, 102, 102, 102, 112, 102, 102, 102, 102, 102, 39, 102, 102, 102, 102, 102, 102, 102, 18, 16, 15, 0};
                int v0_532 = new byte[44];
                v259 = v0_532;
                v259 = {0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0};
                int v0_534 = new byte[44];
                v207 = v0_534;
                v207 = {56, 102, 102, 102, 58, 102, 102, 102, 58, 102, 102, 102, -112, 102, 102, 102, 33, 102, 55, 102, 102, 102, 34, 102, 102, 102, 112, 102, 102, 102, 102, 102, 39, 102, 102, 102, 102, 102, 102, 102, 10, 0, 15, 0};
                int v0_535 = new byte[44];
                v305 = v0_535;
                v305 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0};
                int v0_537 = new byte[70];
                v178 = v0_537;
                v178 = {56, 102, 102, 102, 58, 102, 102, 102, 58, 102, 102, 102, -112, 102, 102, 102, 33, 102, 55, 102, 102, 102, 34, 102, 102, 102, 112, 102, 102, 102, 102, 102, 39, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 18, 16, 15, 0};
                int v0_538 = new byte[70];
                v260 = v0_538;
                v260 = {0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0};
                int v0_540 = new byte[70];
                v208 = v0_540;
                v208 = {56, 102, 102, 102, 58, 102, 102, 102, 58, 102, 102, 102, -112, 102, 102, 102, 33, 102, 55, 102, 102, 102, 34, 102, 102, 102, 112, 102, 102, 102, 102, 102, 39, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 10, 0, 15, 0};
                int v0_541 = new byte[70];
                v306 = v0_541;
                v306 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0};
                int v0_543 = new byte[56];
                v179 = v0_543;
                v179 = {56, 102, 102, 102, 58, 102, 102, 102, 58, 102, 102, 102, -112, 102, 102, 102, 33, 102, 55, 102, 102, 102, 34, 102, 102, 102, 26, 102, 102, 102, 113, 102, 102, 102, 102, 102, 12, 102, 112, 102, 102, 102, 102, 102, 39, 102, 102, 102, 102, 102, 102, 102, 18, 16, 15, 0};
                int v0_544 = new byte[56];
                v261 = v0_544;
                v261 = {0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0};
                int v0_546 = new byte[56];
                v209 = v0_546;
                v209 = {56, 102, 102, 102, 58, 102, 102, 102, 58, 102, 102, 102, -112, 102, 102, 102, 33, 102, 55, 102, 102, 102, 34, 102, 102, 102, 26, 102, 102, 102, 113, 102, 102, 102, 102, 102, 12, 102, 112, 102, 102, 102, 102, 102, 39, 102, 102, 102, 102, 102, 102, 102, 10, 0, 15, 0};
                int v0_547 = new byte[56];
                v307 = v0_547;
                v307 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1};
                int v0_549 = new byte[41];
                v180 = v0_549;
                v180 = {32, 102, 102, 18, 49, 50, 16, 10, 0, 34, 0, 102, 102, 26, 1, 102, 102, 112, 32, 102, 102, 102, 102, 39, 0, 102, 102, 102, 102, 102, 102, 102, 32, 102, 102, 102, 102, 18, 16, 15, 0};
                int v0_550 = new byte[41];
                v262 = v0_550;
                v262 = {0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0};
                int v0_552 = new byte[41];
                v210 = v0_552;
                v210 = {32, 102, 102, 18, 49, 50, 16, 10, 0, 34, 0, 102, 102, 26, 1, 102, 102, 112, 32, 102, 102, 102, 102, 39, 0, 102, 102, 102, 102, 102, 102, 102, 32, 102, 102, 102, 102, 10, 0, 15, 0};
                int v0_553 = new byte[41];
                v308 = v0_553;
                v308 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0};
                int v0_555 = new byte[50];
                v181 = v0_555;
                v181 = {56, 102, 102, 102, 58, 102, 102, 102, 58, 102, 102, 102, -112, 102, 102, 102, 33, 102, 55, 102, 102, 102, 34, 102, 102, 102, 112, 102, 102, 102, 102, 102, 39, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 18, 16, 15, 0};
                int v0_556 = new byte[50];
                v263 = v0_556;
                v263 = {0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0};
                int v0_558 = new byte[50];
                v211 = v0_558;
                v211 = {56, 102, 102, 102, 58, 102, 102, 102, 58, 102, 102, 102, -112, 102, 102, 102, 33, 102, 55, 102, 102, 102, 34, 102, 102, 102, 112, 102, 102, 102, 102, 102, 39, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 102, 10, 0, 15, 0};
                int v0_559 = new byte[50];
                v309 = v0_559;
                v309 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0};
                int v0_561 = new byte[26];
                v183 = v0_561;
                v183 = {18, 18, 33, 83, 33, 102, 50, 102, 102, 0, 15, 2, 18, 1, 18, 0, 33, 102, 53, 102, 102, 0, 72, 102, 5, 0};
                int v0_562 = new byte[26];
                v265 = v0_562;
                v265 = {0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0};
                int v0_564 = new byte[26];
                v213 = v0_564;
                v213 = {18, 2, 33, 83, 33, 102, 50, 102, 102, 0, 15, 2, 18, 1, 18, 0, 33, 102, 53, 102, 102, 0, 72, 102, 5, 0};
                int v0_565 = new byte[26];
                v311 = v0_565;
                v311 = {1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
                int v0_567 = new byte[36];
                v184 = v0_567;
                v184 = {-14, 32, 102, 102, 18, 49, 50, 16, 10, 0, 34, 0, 102, 102, 26, 1, 102, 102, 112, 32, 102, 102, 102, 102, 39, 0, -8, 32, 102, 102, 102, 102, 18, 16, 15, 0};
                int v0_568 = new byte[36];
                v266 = v0_568;
                v266 = {0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0};
                int v0_570 = new byte[36];
                v214 = v0_570;
                v214 = {-14, 32, 102, 102, 18, 49, 50, 16, 10, 0, 34, 0, 102, 102, 26, 1, 102, 102, 112, 32, 102, 102, 102, 102, 39, 0, -8, 32, 102, 102, 102, 102, 10, 0, 15, 0};
                int v0_571 = new byte[36];
                v312 = v0_571;
                v312 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0};
                v87_0 = v298_1;
                v75_0 = v297_1;
                v63_0 = v296_1;
            }
            if ((p332[0].contains("restore")) || ((p332[0].contains("patch")) || (p332[0].contains("all")))) {
                java.util.ArrayList v243_1 = new java.util.ArrayList();
                v243_1.clear();
                if ((v154 != 0) || (v63_0 != 0)) {
                    java.util.ArrayList v283_1 = new java.util.ArrayList();
                    if (new java.io.File("/system/framework/x86/boot.oat").exists()) {
                        v283_1.add(new java.io.File("/system/framework/x86/boot.oat"));
                    }
                    if (new java.io.File("/system/framework/arm64/boot.oat").exists()) {
                        v283_1.add(new java.io.File("/system/framework/arm64/boot.oat"));
                    }
                    if (new java.io.File("/system/framework/arm/boot.oat").exists()) {
                        v283_1.add(new java.io.File("/system/framework/arm/boot.oat"));
                    }
                    if ((v283_1.size() > 0) && (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.api >= 21)) {
                        int v295_0 = 0;
                        int v289_0 = 0;
                        int v290_0 = 0;
                        int v291_0 = 0;
                        java.util.Iterator v64_2 = v283_1.iterator();
                        while (v64_2.hasNext()) {
                            java.io.File v241_1 = ((java.io.File) v64_2.next());
                            System.out.println(new StringBuilder().append("oat file for patch:").append(v241_1.getAbsolutePath()).toString());
                            int v331 = 0;
                            int v284 = 0;
                            int v285 = 0;
                            int v286 = 0;
                            try {
                                java.nio.channels.FileChannel v147_4 = new java.io.RandomAccessFile(v241_1, "rw").getChannel();
                                com.chelpus.root.utils.corepatch.fileBytes = v147_4.map(java.nio.channels.FileChannel$MapMode.READ_WRITE, 0, ((long) ((int) v147_4.size())));
                                com.chelpus.root.utils.corepatch.fileBytes.position(4120);
                                int v299_3 = com.chelpus.Utils.convertFourBytesToInt(com.chelpus.root.utils.corepatch.fileBytes.get(), com.chelpus.root.utils.corepatch.fileBytes.get(), com.chelpus.root.utils.corepatch.fileBytes.get(), com.chelpus.root.utils.corepatch.fileBytes.get());
                                System.out.println(new StringBuilder().append("Start position:").append(v299_3).toString());
                                com.chelpus.root.utils.corepatch.fileBytes.position(v299_3);
                            } catch (Exception v240_16) {
                                v240_16.printStackTrace();
                            }
                            try {
                                while (com.chelpus.root.utils.corepatch.fileBytes.hasRemaining()) {
                                    int v148_7 = com.chelpus.root.utils.corepatch.fileBytes.position();
                                    byte v149_7 = com.chelpus.root.utils.corepatch.fileBytes.get();
                                    if (com.chelpus.root.utils.corepatch.applyPatch(v148_7, v149_7, v5, v6, v7, v8, v154)) {
                                        if (p332[0].contains("patch")) {
                                            System.out.println(new StringBuilder().append("position:").append(v148_7).toString());
                                            System.out.println("Oat Core1uni patched!\n");
                                        }
                                        if (p332[0].contains("restore")) {
                                            System.out.println("Oat Core1uni restored!\n");
                                        }
                                        v295_0 = 1;
                                        v331 = 1;
                                        v284 = 1;
                                        v285 = 1;
                                    }
                                    if (com.chelpus.root.utils.corepatch.applyPatch(v148_7, v149_7, v11, v12, v13, v14, v154)) {
                                        if (p332[0].contains("patch")) {
                                            System.out.println(new StringBuilder().append("position:").append(v148_7).toString());
                                            System.out.println("Oat Core11 patched!\n");
                                        }
                                        if (p332[0].contains("restore")) {
                                            System.out.println("Oat Core11 restored!\n");
                                        }
                                        v289_0 = 1;
                                        v284 = 1;
                                    }
                                    if (com.chelpus.root.utils.corepatch.applyPatch(v148_7, v149_7, v29, v30, v31, v32, v154)) {
                                        if (p332[0].contains("patch")) {
                                            System.out.println(new StringBuilder().append("position:").append(v148_7).toString());
                                            System.out.println("Oat Core1uni patched!\n");
                                        }
                                        if (p332[0].contains("restore")) {
                                            System.out.println("Oat Core1uni restored!\n");
                                        }
                                        v289_0 = 1;
                                        v284 = 1;
                                        v285 = 1;
                                        v331 = 1;
                                    }
                                    if (com.chelpus.root.utils.corepatch.applyPatch(v148_7, v149_7, v53, v54, v55, v56, v154)) {
                                        if (p332[0].contains("patch")) {
                                            System.out.println(new StringBuilder().append("position:").append(v148_7).toString());
                                            System.out.println("Oat Core1uni patched!\n");
                                        }
                                        if (p332[0].contains("restore")) {
                                            System.out.println("Oat Core1uni restored!\n");
                                        }
                                        v289_0 = 1;
                                        v331 = 1;
                                        v284 = 1;
                                        v285 = 1;
                                    }
                                    if (com.chelpus.root.utils.corepatch.applyPatch(v148_7, v149_7, v35, v36, v37, v38, v154)) {
                                        if (p332[0].contains("patch")) {
                                            System.out.println(new StringBuilder().append("position:").append(v148_7).toString());
                                            System.out.println("Oat Core11 patched!\n");
                                        }
                                        if (p332[0].contains("restore")) {
                                            System.out.println("Oat Core11 restored!\n");
                                        }
                                        v289_0 = 1;
                                        v284 = 1;
                                    }
                                    if (com.chelpus.root.utils.corepatch.applyPatch(v148_7, v149_7, v17, v18, v19, v20, v154)) {
                                        if (p332[0].contains("patch")) {
                                            System.out.println("Oat Core12 patched!\n");
                                            System.out.println(new StringBuilder().append("position:").append(v148_7).toString());
                                        }
                                        if (p332[0].contains("restore")) {
                                            System.out.println("Oat Core12 restored!\n");
                                        }
                                        v290_0 = 1;
                                        v285 = 1;
                                    }
                                    if (com.chelpus.root.utils.corepatch.applyPatch(v148_7, v149_7, v41, v42, v43, v44, v154)) {
                                        if (p332[0].contains("patch")) {
                                            System.out.println("Oat Core12 patched!\n");
                                            System.out.println(new StringBuilder().append("position:").append(v148_7).toString());
                                        }
                                        if (p332[0].contains("restore")) {
                                            System.out.println("Oat Core12 restored!\n");
                                        }
                                        v290_0 = 1;
                                        v285 = 1;
                                    }
                                    if (com.chelpus.root.utils.corepatch.applyPatch(v148_7, v149_7, v23, v24, v25, v26, v63_0)) {
                                        if (p332[0].contains("patch")) {
                                            System.out.println("Oat Core2 patched!\n");
                                            System.out.println(new StringBuilder().append("position:").append(v148_7).toString());
                                        }
                                        if (p332[0].contains("restore")) {
                                            System.out.println("Oat Core2 restored!\n");
                                        }
                                        v291_0 = 1;
                                        v286 = 1;
                                    }
                                    if (com.chelpus.root.utils.corepatch.applyPatch(v148_7, v149_7, v47, v48, v49, v50, v63_0)) {
                                        if (p332[0].contains("patch")) {
                                            System.out.println("Oat Core2 patched!\n");
                                            System.out.println(new StringBuilder().append("position:").append(v148_7).toString());
                                        }
                                        if (p332[0].contains("restore")) {
                                            System.out.println("Oat Core2 restored!\n");
                                        }
                                        v291_0 = 1;
                                        v286 = 1;
                                    }
                                    if (com.chelpus.root.utils.corepatch.applyPatch(v148_7, v149_7, v59, v60, v61, v62, v63_0)) {
                                        if (p332[0].contains("patch")) {
                                            System.out.println("Oat Core2 patched!\n");
                                            System.out.println(new StringBuilder().append("position:").append(v148_7).toString());
                                        }
                                        if (p332[0].contains("restore")) {
                                            System.out.println("Oat Core2 restored!\n");
                                        }
                                        v291_0 = 1;
                                        v286 = 1;
                                    }
                                    if (((v154 != 0) && ((v63_0 != 0) && ((v331 != 0) && (v286 != 0)))) || (((v154 != 0) && ((v63_0 != 0) && ((v331 != 0) && ((v284 != 0) && ((v285 != 0) && (v286 != 0)))))) || (((v154 != 0) && ((v63_0 == 0) && (v331 != 0))) || (((v154 != 0) && ((v63_0 == 0) && ((v331 != 0) && ((v284 != 0) && (v285 != 0))))) || ((v154 == 0) && ((v63_0 != 0) && (v286 != 0))))))) {
                                        break;
                                    }
                                    com.chelpus.root.utils.corepatch.fileBytes.position((v148_7 + 1));
                                }
                            } catch (Exception v240_15) {
                                System.out.println(new StringBuilder().append("").append(v240_15).toString());
                            }
                            v147_4.close();
                        }
                        if ((v295_0 != 0) || ((v289_0 != 0) || ((v290_0 != 0) || (v291_0 != 0)))) {
                            new java.io.File("/data/dalvik-cache/arm/system@framework@boot.oat").delete();
                            new java.io.File("/data/dalvik-cache/arm/system@framework@boot.art").delete();
                            new java.io.File("/data/dalvik-cache/arm64/system@framework@boot.oat").delete();
                            new java.io.File("/data/dalvik-cache/arm64/system@framework@boot.art").delete();
                            new java.io.File("/data/dalvik-cache/x86/system@framework@boot.oat").delete();
                            new java.io.File("/data/dalvik-cache/x86/system@framework@boot.art").delete();
                            java.io.PrintStream v9_910 = new String[1];
                            v9_910[0] = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir).append("/reboot").toString();
                            com.chelpus.Utils.cmdParam(v9_910);
                        }
                    }
                    try {
                        if (!p332[4].contains("framework")) {
                            if (!p332[4].contains("ART")) {
                                try {
                                    if (!com.chelpus.root.utils.corepatch.onlyDalvik) {
                                        java.io.File v252_0 = new java.io.File;
                                        v252_0(p332[1]);
                                        if (v252_0.exists()) {
                                            if (v252_0.toString().contains("system@framework@core.jar@classes.dex")) {
                                                java.io.File v234_0 = new java.io.File;
                                                v234_0("/system/framework/core.odex");
                                                if ((v234_0.exists()) && (v234_0.length() == 0)) {
                                                    v234_0.delete();
                                                }
                                            }
                                            v243_1.add(v252_0);
                                            java.util.Iterator v64_3 = v243_1.iterator();
                                            while (v64_3.hasNext()) {
                                                java.io.File v251_0 = ((java.io.File) v64_3.next());
                                                System.out.println(new StringBuilder().append("file for patch: ").append(v251_0.getAbsolutePath()).append(" size:").append(v251_0.length()).toString());
                                                java.nio.channels.FileChannel v147_0 = new java.io.RandomAccessFile(v251_0, "rw").getChannel();
                                                com.chelpus.root.utils.corepatch.fileBytes = v147_0.map(java.nio.channels.FileChannel$MapMode.READ_WRITE, 0, ((long) ((int) v147_0.size())));
                                                int v287 = 0;
                                                if (v251_0.getName().contains("boot.oat")) {
                                                    int v289_1 = 0;
                                                    int v290_1 = 0;
                                                    int v291_1 = 0;
                                                    com.chelpus.root.utils.corepatch.fileBytes.position(4120);
                                                    int v299_0 = com.chelpus.Utils.convertFourBytesToInt(com.chelpus.root.utils.corepatch.fileBytes.get(), com.chelpus.root.utils.corepatch.fileBytes.get(), com.chelpus.root.utils.corepatch.fileBytes.get(), com.chelpus.root.utils.corepatch.fileBytes.get());
                                                    System.out.println(new StringBuilder().append("Start position:").append(v299_0).toString());
                                                    com.chelpus.root.utils.corepatch.fileBytes.position(v299_0);
                                                    try {
                                                        while (com.chelpus.root.utils.corepatch.fileBytes.hasRemaining()) {
                                                            int v148_0 = com.chelpus.root.utils.corepatch.fileBytes.position();
                                                            byte v149_0 = com.chelpus.root.utils.corepatch.fileBytes.get();
                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v148_0, v149_0, v5, v6, v7, v8, v154)) {
                                                                if (p332[0].contains("patch")) {
                                                                    System.out.println(new StringBuilder().append("position:").append(v148_0).toString());
                                                                    System.out.println("Oat Core1uni patched!\n");
                                                                }
                                                                if (p332[0].contains("restore")) {
                                                                    System.out.println("Oat Core1uni restored!\n");
                                                                }
                                                                v289_1 = 1;
                                                                v287 = 1;
                                                            }
                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v148_0, v149_0, v11, v12, v13, v14, v154)) {
                                                                if (p332[0].contains("patch")) {
                                                                    System.out.println(new StringBuilder().append("position:").append(v148_0).toString());
                                                                    System.out.println("Oat Core11 patched!\n");
                                                                }
                                                                if (p332[0].contains("restore")) {
                                                                    System.out.println("Oat Core11 restored!\n");
                                                                }
                                                                v289_1 = 1;
                                                                v287 = 1;
                                                            }
                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v148_0, v149_0, v29, v30, v31, v32, v154)) {
                                                                if (p332[0].contains("patch")) {
                                                                    System.out.println(new StringBuilder().append("position:").append(v148_0).toString());
                                                                    System.out.println("Oat Core1uni patched!\n");
                                                                }
                                                                if (p332[0].contains("restore")) {
                                                                    System.out.println("Oat Core1uni restored!\n");
                                                                }
                                                                v289_1 = 1;
                                                                v287 = 1;
                                                            }
                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v148_0, v149_0, v53, v54, v55, v56, v154)) {
                                                                if (p332[0].contains("patch")) {
                                                                    System.out.println(new StringBuilder().append("position:").append(v148_0).toString());
                                                                    System.out.println("Oat Core1uni patched!\n");
                                                                }
                                                                if (p332[0].contains("restore")) {
                                                                    System.out.println("Oat Core1uni restored!\n");
                                                                }
                                                                v289_1 = 1;
                                                                v287 = 1;
                                                            }
                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v148_0, v149_0, v35, v36, v37, v38, v154)) {
                                                                if (p332[0].contains("patch")) {
                                                                    System.out.println(new StringBuilder().append("position:").append(v148_0).toString());
                                                                    System.out.println("Oat Core11 patched!\n");
                                                                }
                                                                if (p332[0].contains("restore")) {
                                                                    System.out.println("Oat Core11 restored!\n");
                                                                }
                                                                v289_1 = 1;
                                                                v287 = 1;
                                                            }
                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v148_0, v149_0, v17, v18, v19, v20, v154)) {
                                                                if (p332[0].contains("patch")) {
                                                                    System.out.println("Oat Core12 patched!\n");
                                                                    System.out.println(new StringBuilder().append("position:").append(v148_0).toString());
                                                                }
                                                                if (p332[0].contains("restore")) {
                                                                    System.out.println("Oat Core12 restored!\n");
                                                                }
                                                                v290_1 = 1;
                                                                v287 = 1;
                                                            }
                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v148_0, v149_0, v41, v42, v43, v44, v154)) {
                                                                if (p332[0].contains("patch")) {
                                                                    System.out.println("Oat Core12 patched!\n");
                                                                    System.out.println(new StringBuilder().append("position:").append(v148_0).toString());
                                                                }
                                                                if (p332[0].contains("restore")) {
                                                                    System.out.println("Oat Core12 restored!\n");
                                                                }
                                                                v290_1 = 1;
                                                                v287 = 1;
                                                            }
                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v148_0, v149_0, v23, v24, v25, v26, v63_0)) {
                                                                if (p332[0].contains("patch")) {
                                                                    System.out.println("Oat Core2 patched!\n");
                                                                    System.out.println(new StringBuilder().append("position:").append(v148_0).toString());
                                                                }
                                                                if (p332[0].contains("restore")) {
                                                                    System.out.println("Oat Core2 restored!\n");
                                                                }
                                                                v291_1 = 1;
                                                                v287 = 1;
                                                            }
                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v148_0, v149_0, v47, v48, v49, v50, v63_0)) {
                                                                if (p332[0].contains("patch")) {
                                                                    System.out.println("Oat Core2 patched!\n");
                                                                    System.out.println(new StringBuilder().append("position:").append(v148_0).toString());
                                                                }
                                                                if (p332[0].contains("restore")) {
                                                                    System.out.println("Oat Core2 restored!\n");
                                                                }
                                                                v291_1 = 1;
                                                                v287 = 1;
                                                            }
                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v148_0, v149_0, v59, v60, v61, v62, v63_0)) {
                                                                if (p332[0].contains("patch")) {
                                                                    System.out.println("Oat Core2 patched!\n");
                                                                    System.out.println(new StringBuilder().append("position:").append(v148_0).toString());
                                                                }
                                                                if (p332[0].contains("restore")) {
                                                                    System.out.println("Oat Core2 restored!\n");
                                                                }
                                                                v291_1 = 1;
                                                                v287 = 1;
                                                            }
                                                            if (((v154 == 0) || (((v63_0 & 0) == 0) || ((v289_1 == 0) || ((v290_1 == 0) || (v291_1 == 0))))) && (((v154 == 0) || ((v63_0 != 0) || ((0 == 0) || ((v289_1 == 0) || (v290_1 == 0))))) && ((v154 != 0) || ((v63_0 == 0) || (v291_1 == 0))))) {
                                                                com.chelpus.root.utils.corepatch.fileBytes.position((v148_0 + 1));
                                                            } else {
                                                                v287 = 1;
                                                                break;
                                                            }
                                                        }
                                                    } catch (Exception v240_2) {
                                                        System.out.println(new StringBuilder().append("").append(v240_2).toString());
                                                    }
                                                } else {
                                                    long v249_0 = 0;
                                                    try {
                                                        while (com.chelpus.root.utils.corepatch.fileBytes.hasRemaining()) {
                                                            int v148_1 = com.chelpus.root.utils.corepatch.fileBytes.position();
                                                            byte v149_1 = com.chelpus.root.utils.corepatch.fileBytes.get();
                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v148_1, v149_1, v182, v264, v212, v310, v154)) {
                                                                if (p332[0].contains("patch")) {
                                                                    System.out.println("Core patched!\n");
                                                                }
                                                                if (p332[0].contains("restore")) {
                                                                    System.out.println("Core restored!\n");
                                                                }
                                                                v287 = 1;
                                                            }
                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v148_1, v149_1, v185, v264, v215, v310, v154)) {
                                                                if (p332[0].contains("patch")) {
                                                                    System.out.println("Core patched!\n");
                                                                }
                                                                if (p332[0].contains("restore")) {
                                                                    System.out.println("Core restored!\n");
                                                                }
                                                                v287 = 1;
                                                            }
                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v148_1, v149_1, v186, v267, v216, v313, v63_0)) {
                                                                if (p332[0].contains("patch")) {
                                                                    System.out.println("Core unsigned install patched!\n");
                                                                }
                                                                if (p332[0].contains("restore")) {
                                                                    System.out.println("Core unsigned install restored!\n");
                                                                }
                                                                v287 = 1;
                                                            }
                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v148_1, v149_1, v187, v268, v217, v314, v63_0)) {
                                                                if (p332[0].contains("patch")) {
                                                                    System.out.println("Core unsigned install patched!\n");
                                                                }
                                                                if (p332[0].contains("restore")) {
                                                                    System.out.println("Core unsigned install restored!\n");
                                                                }
                                                                v287 = 1;
                                                            }
                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v148_1, v149_1, v188, v269, v218, v315, v154)) {
                                                                if (p332[0].contains("patch")) {
                                                                    System.out.println("Core patched!\n");
                                                                }
                                                                if (p332[0].contains("restore")) {
                                                                    System.out.println("Core restored!\n");
                                                                }
                                                                v287 = 1;
                                                            }
                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v148_1, v149_1, v189, v270, v219, v316, v154)) {
                                                                if (p332[0].contains("patch")) {
                                                                    System.out.println("Core 2 patched!\n");
                                                                }
                                                                if (p332[0].contains("restore")) {
                                                                    System.out.println("Core 2 restored!\n");
                                                                }
                                                                v287 = 1;
                                                            }
                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v148_1, v149_1, v190, v271, v220, v317, v154)) {
                                                                if (p332[0].contains("patch")) {
                                                                    System.out.println("Core 2 patched!\n");
                                                                }
                                                                if (p332[0].contains("restore")) {
                                                                    System.out.println("Core 2 restored!\n");
                                                                }
                                                                v287 = 1;
                                                            }
                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v148_1, v149_1, v191, v272, v221, v318, v154)) {
                                                                if (p332[0].contains("patch")) {
                                                                    System.out.println("Core 2 patched!\n");
                                                                }
                                                                if (p332[0].contains("restore")) {
                                                                    System.out.println("Core 2 restored!\n");
                                                                }
                                                                v287 = 1;
                                                            }
                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v148_1, v149_1, v172, v254, v202, v300, v154)) {
                                                                if (p332[0].contains("patch")) {
                                                                    System.out.println("Core patched!\n");
                                                                }
                                                                if (p332[0].contains("restore")) {
                                                                    System.out.println("Core restored!\n");
                                                                }
                                                                v287 = 1;
                                                            }
                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v148_1, v149_1, v173, v255, v203, v301, v154)) {
                                                                if (p332[0].contains("patch")) {
                                                                    System.out.println("Core patched!\n");
                                                                }
                                                                if (p332[0].contains("restore")) {
                                                                    System.out.println("Core restored!\n");
                                                                }
                                                                v287 = 1;
                                                            }
                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v148_1, v149_1, v174, v256, v204, v302, v63_0)) {
                                                                if (p332[0].contains("patch")) {
                                                                    System.out.println("Core unsigned install patched!\n");
                                                                }
                                                                if (p332[0].contains("restore")) {
                                                                    System.out.println("Core unsigned install restored!\n");
                                                                }
                                                                v287 = 1;
                                                            }
                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v148_1, v149_1, v175, v257, v205, v303, v63_0)) {
                                                                if (p332[0].contains("patch")) {
                                                                    System.out.println("Core unsigned install patched!\n");
                                                                }
                                                                if (p332[0].contains("restore")) {
                                                                    System.out.println("Core unsigned install restored!\n");
                                                                }
                                                                v287 = 1;
                                                            }
                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v148_1, v149_1, v183, v265, v213, v311, v63_0)) {
                                                                if (p332[0].contains("patch")) {
                                                                    System.out.println("Core unsigned install patched!\n");
                                                                }
                                                                if (p332[0].contains("restore")) {
                                                                    System.out.println("Core unsigned install restored!\n");
                                                                }
                                                                v287 = 1;
                                                            }
                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v148_1, v149_1, v184, v266, v214, v312, v154)) {
                                                                if (p332[0].contains("patch")) {
                                                                    System.out.println("Core patched!\n");
                                                                }
                                                                if (p332[0].contains("restore")) {
                                                                    System.out.println("Core restored!\n");
                                                                }
                                                                v287 = 1;
                                                            }
                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v148_1, v149_1, v176, v258, v206, v304, v154)) {
                                                                if (p332[0].contains("patch")) {
                                                                    System.out.println("Core patched!\n");
                                                                }
                                                                if (p332[0].contains("restore")) {
                                                                    System.out.println("Core restored!\n");
                                                                }
                                                                v287 = 1;
                                                            }
                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v148_1, v149_1, v177, v259, v207, v305, v154)) {
                                                                if (p332[0].contains("patch")) {
                                                                    System.out.println("Core 2 patched!\n");
                                                                }
                                                                if (p332[0].contains("restore")) {
                                                                    System.out.println("Core 2 restored!\n");
                                                                }
                                                                v287 = 1;
                                                            }
                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v148_1, v149_1, v178, v260, v208, v306, v154)) {
                                                                if (p332[0].contains("patch")) {
                                                                    System.out.println("Core 2 patched!\n");
                                                                }
                                                                if (p332[0].contains("restore")) {
                                                                    System.out.println("Core 2 restored!\n");
                                                                }
                                                                v287 = 1;
                                                            }
                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v148_1, v149_1, v179, v261, v209, v307, v154)) {
                                                                if (p332[0].contains("patch")) {
                                                                    System.out.println("Core 2 patched!\n");
                                                                }
                                                                if (p332[0].contains("restore")) {
                                                                    System.out.println("Core 2 restored!\n");
                                                                }
                                                                v287 = 1;
                                                            }
                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v148_1, v149_1, v180, v262, v210, v308, v154)) {
                                                                if (p332[0].contains("patch")) {
                                                                    System.out.println("Core 2 patched!\n");
                                                                }
                                                                if (p332[0].contains("restore")) {
                                                                    System.out.println("Core 2 restored!\n");
                                                                }
                                                                v287 = 1;
                                                            }
                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v148_1, v149_1, v181, v263, v211, v309, v154)) {
                                                                if (p332[0].contains("patch")) {
                                                                    System.out.println("Core 2 patched!\n");
                                                                }
                                                                if (p332[0].contains("restore")) {
                                                                    System.out.println("Core 2 restored!\n");
                                                                }
                                                                v287 = 1;
                                                            }
                                                            com.chelpus.root.utils.corepatch.fileBytes.position((v148_1 + 1));
                                                            v249_0++;
                                                        }
                                                    } catch (Exception v240_3) {
                                                        System.out.println(new StringBuilder().append("").append(v240_3).toString());
                                                    }
                                                }
                                                v147_0.close();
                                                if (!p332[4].contains("framework")) {
                                                    if (!v251_0.toString().endsWith("/classes.dex")) {
                                                        com.chelpus.Utils.fixadlerOdex(v251_0, "/system/framework/core.jar");
                                                    } else {
                                                        com.chelpus.Utils.fixadler(v251_0);
                                                    }
                                                    if (!p332[4].contains("ART")) {
                                                        if (!com.chelpus.root.utils.corepatch.onlyDalvik) {
                                                            if ((v251_0.toString().contains("system@framework@core.jar@classes.dex")) && (v287 != 0)) {
                                                                System.out.println("LuckyPatcher: dalvik-cache patched! ");
                                                                java.io.File v235 = new java.io.File;
                                                                v235("/system/framework/core.patched");
                                                                if (!com.chelpus.Utils.copyFile(v251_0.getAbsolutePath(), "/system/framework/core.patched", 1, 0)) {
                                                                    v235.delete();
                                                                    System.out.println("LuckyPatcher: not space to system for odex core.jar! ");
                                                                    System.out.println("LuckyPatcher: odex not equal lenght patched! Not enougth space in /system/!");
                                                                } else {
                                                                    java.io.PrintStream v9_1304 = new String[3];
                                                                    v9_1304[0] = "chmod";
                                                                    v9_1304[1] = "0644";
                                                                    v9_1304[2] = "/system/framework/core.patched";
                                                                    com.chelpus.Utils.run_all_no_root(v9_1304);
                                                                    java.io.PrintStream v9_1306 = new String[3];
                                                                    v9_1306[0] = "chown";
                                                                    v9_1306[1] = "0.0";
                                                                    v9_1306[2] = "/system/framework/core.patched";
                                                                    com.chelpus.Utils.run_all_no_root(v9_1306);
                                                                    java.io.PrintStream v9_1308 = new String[3];
                                                                    v9_1308[0] = "chown";
                                                                    v9_1308[1] = "0:0";
                                                                    v9_1308[2] = "/system/framework/core.patched";
                                                                    com.chelpus.Utils.run_all_no_root(v9_1308);
                                                                }
                                                            }
                                                            java.io.File v233_0 = new java.io.File;
                                                            v233_0("/system/framework/core.patched");
                                                            if (v233_0.exists()) {
                                                                System.out.println("LuckyPatcher: root found core.patched! ");
                                                            }
                                                            java.io.File v233_1 = new java.io.File;
                                                            v233_1("/system/framework/core.odex");
                                                            if (v233_1.exists()) {
                                                                System.out.println("LuckyPatcher: root found core.odex! ");
                                                            }
                                                        }
                                                    } else {
                                                        if ((v251_0.toString().contains("/classes.dex")) && (v287 != 0)) {
                                                            System.out.println("start");
                                                            if (!com.chelpus.Utils.copyFile("/system/framework/core-libart.jar", "/system/framework/core-libart.backup", 1, 0)) {
                                                                new java.io.File("/system/framework/core-libart.backup").delete();
                                                                System.out.println("LuckyPatcher: odex not equal lenght patched! Not enougth space in /system/!");
                                                            } else {
                                                                System.out.println("good space");
                                                                new java.io.File("/system/framework/core-libart.backup").delete();
                                                                java.util.ArrayList v242_1 = new java.util.ArrayList();
                                                                System.out.println("add files");
                                                                v242_1.add(new com.android.vending.billing.InAppBillingService.LUCK.AddFilesItem("/data/app/classes.dex", "/data/app/"));
                                                                com.chelpus.Utils.addFilesToZip("/system/framework/core-libart.jar", "/system/framework/core-libart.backup", v242_1);
                                                                System.out.println("add files finish");
                                                                java.io.PrintStream v9_1332 = new String[3];
                                                                v9_1332[0] = "chmod";
                                                                v9_1332[1] = "0644";
                                                                v9_1332[2] = "/system/framework/core-libart.backup";
                                                                com.chelpus.Utils.run_all_no_root(v9_1332);
                                                                java.io.PrintStream v9_1334 = new String[3];
                                                                v9_1334[0] = "chown";
                                                                v9_1334[1] = "0:0";
                                                                v9_1334[2] = "/system/framework/core-libart.backup";
                                                                com.chelpus.Utils.run_all_no_root(v9_1334);
                                                                java.io.PrintStream v9_1336 = new String[3];
                                                                v9_1336[0] = "chmod";
                                                                v9_1336[1] = "0.0";
                                                                v9_1336[2] = "/system/framework/core-libart.backup";
                                                                com.chelpus.Utils.run_all_no_root(v9_1336);
                                                                java.io.PrintStream v9_1338 = new String[2];
                                                                v9_1338[0] = "rm";
                                                                v9_1338[1] = "/system/framework/core-libart.jar";
                                                                com.chelpus.Utils.run_all_no_root(v9_1338);
                                                                if (new java.io.File("/system/framework/core-libart.jar").exists()) {
                                                                    new java.io.File("/system/framework/core-libart.jar").delete();
                                                                }
                                                                java.io.PrintStream v9_1345 = new String[3];
                                                                v9_1345[0] = "mv";
                                                                v9_1345[1] = "/system/framework/core.backup";
                                                                v9_1345[2] = "/system/framework/core-libart.jar";
                                                                com.chelpus.Utils.run_all_no_root(v9_1345);
                                                                if (!new java.io.File("/system/framework/core-libart.jar").exists()) {
                                                                    new java.io.File("/system/framework/core-libart.backup").renameTo(new java.io.File("/system/framework/core-libart.jar"));
                                                                }
                                                                java.io.PrintStream v9_1352 = new String[3];
                                                                v9_1352[0] = "chmod";
                                                                v9_1352[1] = "0644";
                                                                v9_1352[2] = "/system/framework/core-libart.jar";
                                                                com.chelpus.Utils.run_all_no_root(v9_1352);
                                                                java.io.PrintStream v9_1354 = new String[3];
                                                                v9_1354[0] = "chown";
                                                                v9_1354[1] = "0:0";
                                                                v9_1354[2] = "/system/framework/core-libart.jar";
                                                                com.chelpus.Utils.run_all_no_root(v9_1354);
                                                                java.io.PrintStream v9_1356 = new String[3];
                                                                v9_1356[0] = "chmod";
                                                                v9_1356[1] = "0.0";
                                                                v9_1356[2] = "/system/framework/core-libart.jar";
                                                                com.chelpus.Utils.run_all_no_root(v9_1356);
                                                                java.io.File v238 = com.chelpus.Utils.getFileDalvikCache("/system/framework/core-libart.jar");
                                                                if (v238 != null) {
                                                                    java.io.PrintStream v9_1359 = new String[2];
                                                                    v9_1359[0] = "rm";
                                                                    v9_1359[1] = v238.getAbsolutePath();
                                                                    com.chelpus.Utils.run_all_no_root(v9_1359);
                                                                    if (v238.exists()) {
                                                                        v238.delete();
                                                                    }
                                                                }
                                                                new java.io.File("/data/dalvik-cache/arm/system@framework@arm@boot.oat").delete();
                                                                new java.io.File("/data/dalvik-cache/arm/system@framework@arm@boot.art").delete();
                                                                new java.io.File("/data/dalvik-cache/arm64/system@framework@arm@boot.oat").delete();
                                                                new java.io.File("/data/dalvik-cache/arm64/system@framework@arm@boot.art").delete();
                                                                new java.io.File("/data/dalvik-cache/x86/system@framework@arm@boot.oat").delete();
                                                                new java.io.File("/data/dalvik-cache/x86/system@framework@arm@boot.art").delete();
                                                                System.out.println("finish");
                                                            }
                                                        }
                                                    }
                                                } else {
                                                    if (v251_0.toString().endsWith("/classes.dex")) {
                                                        com.chelpus.root.utils.corepatch.not_found_bytes_for_patch = 0;
                                                        com.chelpus.Utils.fixadler(v251_0);
                                                        if (v287 == 0) {
                                                            com.chelpus.root.utils.corepatch.not_found_bytes_for_patch = 1;
                                                        } else {
                                                            String v239_2 = p332[1].replace(".jar", "-patched.jar");
                                                            String v330_2 = p332[1];
                                                            new java.io.File(v239_2).delete();
                                                            java.util.ArrayList v242_3 = new java.util.ArrayList();
                                                            System.out.println("add files");
                                                            v242_3.add(new com.android.vending.billing.InAppBillingService.LUCK.AddFilesItem(v251_0.getAbsolutePath(), new StringBuilder().append(com.chelpus.Utils.getDirs(v251_0).getAbsolutePath()).append("/").toString()));
                                                            com.chelpus.Utils.addFilesToZip(v330_2, v239_2, v242_3);
                                                            System.out.println("add files finish");
                                                            new java.io.File(v330_2).delete();
                                                        }
                                                    }
                                                    if (v251_0.toString().endsWith("/core.odex")) {
                                                        com.chelpus.Utils.fixadlerOdex(v251_0, 0);
                                                        if (v287 != 0) {
                                                            v251_0.renameTo(new java.io.File(v251_0.getAbsolutePath().replace("/core.odex", "/core-patched.odex")));
                                                        }
                                                    }
                                                    if ((v251_0.toString().endsWith("/boot.oat")) && (v287 != 0)) {
                                                        v251_0.renameTo(new java.io.File(v251_0.getAbsolutePath().replace("/boot.oat", "/boot-patched.oat")));
                                                    }
                                                }
                                            }
                                            if ((v75_0 == 0) && (v87_0 == 0)) {
                                                com.chelpus.Utils.exitFromRootJava();
                                                return;
                                            } else {
                                                System.out.println("Start patch for services.jar");
                                                String v247 = "";
                                                if (new java.io.File("/system/framework/arm/services.odex").exists()) {
                                                    v247 = "/arm";
                                                }
                                                if (new java.io.File("/system/framework/arm64/services.odex").exists()) {
                                                    v247 = "/arm64";
                                                }
                                                if (new java.io.File("/system/framework/x86/services.odex").exists()) {
                                                    v247 = "/x86";
                                                }
                                                if (new java.io.File("/system/framework/arm/services.odex.xz").exists()) {
                                                    v247 = "/arm";
                                                }
                                                if (new java.io.File("/system/framework/arm64/services.odex.xz").exists()) {
                                                    v247 = "/arm64";
                                                }
                                                if (new java.io.File("/system/framework/x86/services.odex.xz").exists()) {
                                                    v247 = "/x86";
                                                }
                                                if ((!v247.equals("")) && (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.api >= 21)) {
                                                    int v289_2 = 0;
                                                    int v290_2 = 0;
                                                    int v291_2 = 0;
                                                    int v292_0 = 0;
                                                    int v293_0 = 0;
                                                    int v294_0 = 0;
                                                    java.io.File v244 = new java.io.File;
                                                    v244(new StringBuilder().append("/system/framework").append(v247).append("/services.odex").toString());
                                                    int v246 = 1;
                                                    if (new java.io.File(new StringBuilder().append("/system/framework").append(v247).append("/services.odex.xz").toString()).exists()) {
                                                        System.out.println("try unpack services.odex.xz");
                                                        if (com.chelpus.Utils.XZDecompress(new java.io.File(new StringBuilder().append("/system/framework").append(v247).append("/services.odex.xz").toString()), new StringBuilder().append("/system/framework").append(v247).toString())) {
                                                            java.io.PrintStream v9_1441 = new String[3];
                                                            v9_1441[0] = "chmod";
                                                            v9_1441[1] = "644";
                                                            v9_1441[2] = new StringBuilder().append("/system/framework").append(v247).append("/services.odex").toString();
                                                            com.chelpus.Utils.run_all_no_root(v9_1441);
                                                            java.io.PrintStream v9_1443 = new String[3];
                                                            v9_1443[0] = "chown";
                                                            v9_1443[1] = "0:0";
                                                            v9_1443[2] = new StringBuilder().append("/system/framework").append(v247).append("/services.odex").toString();
                                                            com.chelpus.Utils.run_all_no_root(v9_1443);
                                                            java.io.PrintStream v9_1445 = new String[3];
                                                            v9_1445[0] = "chown";
                                                            v9_1445[1] = "0.0";
                                                            v9_1445[2] = new StringBuilder().append("/system/framework").append(v247).append("/services.odex").toString();
                                                            com.chelpus.Utils.run_all_no_root(v9_1445);
                                                        } else {
                                                            v246 = 0;
                                                            new java.io.File(new StringBuilder().append("/system/framework").append(v247).append("/services.odex").toString()).delete();
                                                            System.out.println("not enought space for unpack services.odex.xz");
                                                        }
                                                    }
                                                    if (v246 == 0) {
                                                        System.out.println("LuckyPatcher: odex not equal lenght patched! Not enougth space in /system/!");
                                                    } else {
                                                        try {
                                                            java.nio.channels.FileChannel v147_1 = new java.io.RandomAccessFile(v244, "rw").getChannel();
                                                            com.chelpus.root.utils.corepatch.fileBytes = v147_1.map(java.nio.channels.FileChannel$MapMode.READ_WRITE, 0, ((long) ((int) v147_1.size())));
                                                            com.chelpus.root.utils.corepatch.fileBytes.position(4120);
                                                            int v299_1 = com.chelpus.Utils.convertFourBytesToInt(com.chelpus.root.utils.corepatch.fileBytes.get(), com.chelpus.root.utils.corepatch.fileBytes.get(), com.chelpus.root.utils.corepatch.fileBytes.get(), com.chelpus.root.utils.corepatch.fileBytes.get());
                                                            System.out.println(new StringBuilder().append("Start position:").append(v299_1).toString());
                                                            com.chelpus.root.utils.corepatch.fileBytes.position(v299_1);
                                                            try {
                                                                while (com.chelpus.root.utils.corepatch.fileBytes.hasRemaining()) {
                                                                    int v148_2 = com.chelpus.root.utils.corepatch.fileBytes.position();
                                                                    byte v149_2 = com.chelpus.root.utils.corepatch.fileBytes.get();
                                                                    if (com.chelpus.root.utils.corepatch.applyPatch(v148_2, v149_2, v65, v66, v67, v68, v75_0)) {
                                                                        if (p332[0].contains("patch")) {
                                                                            System.out.println("Core4 patched!\n");
                                                                        }
                                                                        if (p332[0].contains("restore")) {
                                                                            System.out.println("Core4 restored!\n");
                                                                        }
                                                                        v289_2 = 1;
                                                                    }
                                                                    if (com.chelpus.root.utils.corepatch.applyPatch(v148_2, v149_2, v71, v72, v73, v74, v75_0)) {
                                                                        if (p332[0].contains("patch")) {
                                                                            System.out.println("Core4 patched!InstallLocationPolice\n");
                                                                        }
                                                                        if (p332[0].contains("restore")) {
                                                                            System.out.println("Core4 restored!InstallLocationPolice\n");
                                                                        }
                                                                        v290_2 = 1;
                                                                    }
                                                                    if (com.chelpus.root.utils.corepatch.applyPatch(v148_2, v149_2, v77, v78, v79, v80, v75_0)) {
                                                                        if (p332[0].contains("patch")) {
                                                                            System.out.println("Core4 patched!checkUpgradeKeySetLP\n");
                                                                        }
                                                                        if (p332[0].contains("restore")) {
                                                                            System.out.println("Core4 restored!checkUpgradeKeySetLP\n");
                                                                        }
                                                                        v291_2 = 1;
                                                                    }
                                                                    if (com.chelpus.root.utils.corepatch.applyPatch(v148_2, v149_2, v83, v84, v85, v86, v87_0)) {
                                                                        if (p332[0].contains("patch")) {
                                                                            System.out.println("Core4 patched!queryIntentServices\n");
                                                                        }
                                                                        if (p332[0].contains("restore")) {
                                                                            System.out.println("Core4 restored!queryIntentServices\n");
                                                                        }
                                                                        v292_0 = 1;
                                                                    }
                                                                    if (com.chelpus.root.utils.corepatch.applyPatch(v148_2, v149_2, v89, v90, v91, v92, v87_0)) {
                                                                        if (p332[0].contains("patch")) {
                                                                            System.out.println("Core4 patched!buildResolveList\n");
                                                                        }
                                                                        if (p332[0].contains("restore")) {
                                                                            System.out.println("Core4 restored!buildResolveList\n");
                                                                        }
                                                                        v293_0 = 1;
                                                                    }
                                                                    if (com.chelpus.root.utils.corepatch.applyPatch(v148_2, v149_2, v95, v96, v97, v98, v75_0)) {
                                                                        if (p332[0].contains("patch")) {
                                                                            System.out.println("Core4 patched!FixForCM\n");
                                                                        }
                                                                        if (p332[0].contains("restore")) {
                                                                            System.out.println("Core4 restored!FixForCM\n");
                                                                        }
                                                                        v294_0 = 1;
                                                                    }
                                                                    if (com.chelpus.root.utils.corepatch.applyPatch(v148_2, v149_2, v101, v102, v103, v104, v75_0)) {
                                                                        if (p332[0].contains("patch")) {
                                                                            System.out.println("Core4 patched!FixForCM\n");
                                                                        }
                                                                        if (p332[0].contains("restore")) {
                                                                            System.out.println("Core4 restored!FixForCM\n");
                                                                        }
                                                                        v294_0 = 1;
                                                                    }
                                                                    if (com.chelpus.root.utils.corepatch.applyPatch(v148_2, v149_2, v107, v108, v109, v110, v75_0)) {
                                                                        if (p332[0].contains("patch")) {
                                                                            System.out.println("Core4 patched!\n");
                                                                        }
                                                                        if (p332[0].contains("restore")) {
                                                                            System.out.println("Core4 restored!\n");
                                                                        }
                                                                        v289_2 = 1;
                                                                    }
                                                                    if (com.chelpus.root.utils.corepatch.applyPatch(v148_2, v149_2, v113, v114, v115, v116, v75_0)) {
                                                                        if (p332[0].contains("patch")) {
                                                                            System.out.println("Core4 patched!InstallLocationPolice\n");
                                                                        }
                                                                        if (p332[0].contains("restore")) {
                                                                            System.out.println("Core4 restored!InstallLocationPolice\n");
                                                                        }
                                                                        v290_2 = 1;
                                                                    }
                                                                    if (com.chelpus.root.utils.corepatch.applyPatch(v148_2, v149_2, v131, v132, v133, v134, v75_0)) {
                                                                        if (p332[0].contains("patch")) {
                                                                            System.out.println("Core4 patched!InstallLocationPolice\n");
                                                                        }
                                                                        if (p332[0].contains("restore")) {
                                                                            System.out.println("Core4 restored!InstallLocationPolice\n");
                                                                        }
                                                                        v290_2 = 1;
                                                                    }
                                                                    if (com.chelpus.root.utils.corepatch.applyPatch(v148_2, v149_2, v137, v138, v139, v140, v75_0)) {
                                                                        if (p332[0].contains("patch")) {
                                                                            System.out.println("Core4 patched!InstallLocationPolice\n");
                                                                        }
                                                                        if (p332[0].contains("restore")) {
                                                                            System.out.println("Core4 restored!InstallLocationPolice\n");
                                                                        }
                                                                        v290_2 = 1;
                                                                    }
                                                                    if (com.chelpus.root.utils.corepatch.applyPatch(v148_2, v149_2, v119, v120, v121, v122, v75_0)) {
                                                                        if (p332[0].contains("patch")) {
                                                                            System.out.println("Core4 patched!\n");
                                                                        }
                                                                        if (p332[0].contains("restore")) {
                                                                            System.out.println("Core4 restored!\n");
                                                                        }
                                                                        v289_2 = 1;
                                                                    }
                                                                    if (com.chelpus.root.utils.corepatch.applyPatch(v148_2, v149_2, v125, v126, v127, v128, v75_0)) {
                                                                        if (p332[0].contains("patch")) {
                                                                            System.out.println("Core4 patched!\n");
                                                                        }
                                                                        if (p332[0].contains("restore")) {
                                                                            System.out.println("Core4 restored!\n");
                                                                        }
                                                                        v289_2 = 1;
                                                                    }
                                                                    if ((v75_0 == 0) || ((v289_2 == 0) || ((v290_2 == 0) || ((v291_2 == 0) || ((v292_0 == 0) || ((v293_0 == 0) || (v294_0 == 0))))))) {
                                                                        com.chelpus.root.utils.corepatch.fileBytes.position((v148_2 + 1));
                                                                    } else {
                                                                        new java.io.File("/data/dalvik-cache/arm/system@framework@services.jar@classes.dex").delete();
                                                                        new java.io.File("/data/dalvik-cache/arm/system@framework@services.jar@classes.art").delete();
                                                                        new java.io.File("/data/dalvik-cache/arm64/system@framework@services.jar@classes.dex").delete();
                                                                        new java.io.File("/data/dalvik-cache/arm64/system@framework@services.jar@classes.art").delete();
                                                                        new java.io.File("/data/dalvik-cache/x86/system@framework@services.jar@classes.dex").delete();
                                                                        new java.io.File("/data/dalvik-cache/x86/system@framework@services.jar@classes.art").delete();
                                                                        java.io.PrintStream v9_1595 = new String[1];
                                                                        v9_1595[0] = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir).append("/reboot").toString();
                                                                        com.chelpus.Utils.cmdParam(v9_1595);
                                                                        break;
                                                                    }
                                                                }
                                                            } catch (Exception v240_6) {
                                                                System.out.println(new StringBuilder().append("").append(v240_6).toString());
                                                            }
                                                            v147_1.close();
                                                        } catch (Exception v240_7) {
                                                            v240_7.printStackTrace();
                                                        }
                                                    }
                                                    if ((v289_2 != 0) || ((v290_2 != 0) || ((v291_2 != 0) || ((v292_0 != 0) || ((v293_0 != 0) || (v294_0 != 0)))))) {
                                                        new java.io.File("/data/dalvik-cache/arm/system@framework@services.jar@classes.dex").delete();
                                                        new java.io.File("/data/dalvik-cache/arm/system@framework@services.jar@classes.art").delete();
                                                        new java.io.File("/data/dalvik-cache/arm64/system@framework@services.jar@classes.dex").delete();
                                                        new java.io.File("/data/dalvik-cache/arm64/system@framework@services.jar@classes.art").delete();
                                                        new java.io.File("/data/dalvik-cache/x86/system@framework@services.jar@classes.dex").delete();
                                                        new java.io.File("/data/dalvik-cache/x86/system@framework@services.jar@classes.art").delete();
                                                        java.io.PrintStream v9_1610 = new String[1];
                                                        v9_1610[0] = new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir).append("/reboot").toString();
                                                        com.chelpus.Utils.cmdParam(v9_1610);
                                                    }
                                                }
                                                try {
                                                    if (!p332[4].contains("framework")) {
                                                        if (!p332[4].contains("ART")) {
                                                            if (!com.chelpus.root.utils.corepatch.onlyDalvik) {
                                                                System.out.println(new StringBuilder().append("Vhodjashij file ").append(p332[2]).toString());
                                                                java.io.File v252_1 = new java.io.File;
                                                                v252_1(p332[2]);
                                                                if (v252_1.exists()) {
                                                                    java.io.File v251_1;
                                                                    if (!v252_1.toString().contains("system@framework@services.jar@classes.dex")) {
                                                                        v251_1 = v252_1;
                                                                    } else {
                                                                        System.out.println(new StringBuilder().append("Vhodjashij file byl dalvick-cache ").append(p332[2]).toString());
                                                                        java.io.File v329_2 = new java.io.File;
                                                                        v329_2("/system/framework/services.jar");
                                                                        com.chelpus.root.utils.corepatch.unzip(v329_2, "/data/app");
                                                                        java.io.File v232_2 = new java.io.File;
                                                                        v232_2("/data/app/classes.dex");
                                                                        if (!v232_2.exists()) {
                                                                            v251_1 = v252_1;
                                                                        } else {
                                                                            v251_1 = v232_2;
                                                                        }
                                                                        java.io.File v234_1 = new java.io.File;
                                                                        v234_1("/system/framework/services.odex");
                                                                        if ((v234_1.exists()) && (v234_1.length() == 0)) {
                                                                            v234_1.delete();
                                                                        }
                                                                    }
                                                                    System.out.println(new StringBuilder().append("Add file for patch ").append(v251_1.toString()).toString());
                                                                    v243_1.add(v251_1);
                                                                    java.io.PrintStream v9_1681 = v243_1.iterator();
                                                                    while (v9_1681.hasNext()) {
                                                                        java.io.File v245_3 = ((java.io.File) v9_1681.next());
                                                                        System.out.println(new StringBuilder().append("Start patch for ").append(v245_3.toString()).toString());
                                                                        java.io.File v251_2 = v245_3;
                                                                        int v248 = 0;
                                                                        if (com.chelpus.Utils.isELFfiles(v251_2)) {
                                                                            v248 = 1;
                                                                        }
                                                                        java.nio.channels.FileChannel v147_2 = new java.io.RandomAccessFile(v251_2, "rw").getChannel();
                                                                        com.chelpus.root.utils.corepatch.fileBytes = v147_2.map(java.nio.channels.FileChannel$MapMode.READ_WRITE, 0, ((long) ((int) v147_2.size())));
                                                                        int v288 = 0;
                                                                        int v236_0 = 0;
                                                                        int v169_0 = 0;
                                                                        if (v248 != 0) {
                                                                            int v289_3 = 0;
                                                                            int v290_3 = 0;
                                                                            int v291_3 = 0;
                                                                            int v292_1 = 0;
                                                                            int v293_1 = 0;
                                                                            int v294_1 = 0;
                                                                            com.chelpus.root.utils.corepatch.fileBytes.position(4120);
                                                                            int v299_2 = com.chelpus.Utils.convertFourBytesToInt(com.chelpus.root.utils.corepatch.fileBytes.get(), com.chelpus.root.utils.corepatch.fileBytes.get(), com.chelpus.root.utils.corepatch.fileBytes.get(), com.chelpus.root.utils.corepatch.fileBytes.get());
                                                                            System.out.println(new StringBuilder().append("Start position:").append(v299_2).toString());
                                                                            com.chelpus.root.utils.corepatch.fileBytes.position(v299_2);
                                                                            try {
                                                                                while (com.chelpus.root.utils.corepatch.fileBytes.hasRemaining()) {
                                                                                    int v148_3 = com.chelpus.root.utils.corepatch.fileBytes.position();
                                                                                    byte v149_3 = com.chelpus.root.utils.corepatch.fileBytes.get();
                                                                                    if (com.chelpus.root.utils.corepatch.applyPatch(v148_3, v149_3, v65, v66, v67, v68, v75_0)) {
                                                                                        if (p332[0].contains("patch")) {
                                                                                            System.out.println("Core4 patched!\n");
                                                                                        }
                                                                                        if (p332[0].contains("restore")) {
                                                                                            System.out.println("Core4 restored!\n");
                                                                                        }
                                                                                        v289_3 = 1;
                                                                                        v288 = 1;
                                                                                    }
                                                                                    if (com.chelpus.root.utils.corepatch.applyPatch(v148_3, v149_3, v71, v72, v73, v74, v75_0)) {
                                                                                        if (p332[0].contains("patch")) {
                                                                                            System.out.println("Core4 patched!InstallLocationPolice\n");
                                                                                        }
                                                                                        if (p332[0].contains("restore")) {
                                                                                            System.out.println("Core4 restored!InstallLocationPolice\n");
                                                                                        }
                                                                                        v290_3 = 1;
                                                                                        v288 = 1;
                                                                                    }
                                                                                    if (com.chelpus.root.utils.corepatch.applyPatch(v148_3, v149_3, v77, v78, v79, v80, v75_0)) {
                                                                                        if (p332[0].contains("patch")) {
                                                                                            System.out.println("Core4 patched!checkUpgradeKeySetLP\n");
                                                                                        }
                                                                                        if (p332[0].contains("restore")) {
                                                                                            System.out.println("Core4 restored!checkUpgradeKeySetLP\n");
                                                                                        }
                                                                                        v291_3 = 1;
                                                                                        v288 = 1;
                                                                                    }
                                                                                    if (com.chelpus.root.utils.corepatch.applyPatch(v148_3, v149_3, v83, v84, v85, v86, v87_0)) {
                                                                                        if (p332[0].contains("patch")) {
                                                                                            System.out.println("Core4 patched!queryIntentServices\n");
                                                                                        }
                                                                                        if (p332[0].contains("restore")) {
                                                                                            System.out.println("Core4 restored!queryIntentServices\n");
                                                                                        }
                                                                                        v292_1 = 1;
                                                                                        v288 = 1;
                                                                                    }
                                                                                    if (com.chelpus.root.utils.corepatch.applyPatch(v148_3, v149_3, v89, v90, v91, v92, v87_0)) {
                                                                                        if (p332[0].contains("patch")) {
                                                                                            System.out.println("Core4 patched!buildResolveList\n");
                                                                                        }
                                                                                        if (p332[0].contains("restore")) {
                                                                                            System.out.println("Core4 restored!buildResolveList\n");
                                                                                        }
                                                                                        v293_1 = 1;
                                                                                        v288 = 1;
                                                                                    }
                                                                                    if (com.chelpus.root.utils.corepatch.applyPatch(v148_3, v149_3, v95, v96, v97, v98, v75_0)) {
                                                                                        if (p332[0].contains("patch")) {
                                                                                            System.out.println("Core4 patched!FixForCM\n");
                                                                                        }
                                                                                        if (p332[0].contains("restore")) {
                                                                                            System.out.println("Core4 restored!FixForCM\n");
                                                                                        }
                                                                                        v294_1 = 1;
                                                                                        v288 = 1;
                                                                                    }
                                                                                    if (com.chelpus.root.utils.corepatch.applyPatch(v148_3, v149_3, v101, v102, v103, v104, v75_0)) {
                                                                                        if (p332[0].contains("patch")) {
                                                                                            System.out.println("Core4 patched!FixForCM\n");
                                                                                        }
                                                                                        if (p332[0].contains("restore")) {
                                                                                            System.out.println("Core4 restored!FixForCM\n");
                                                                                        }
                                                                                        v294_1 = 1;
                                                                                        v288 = 1;
                                                                                    }
                                                                                    if (com.chelpus.root.utils.corepatch.applyPatch(v148_3, v149_3, v107, v108, v109, v110, v75_0)) {
                                                                                        if (p332[0].contains("patch")) {
                                                                                            System.out.println("Core4 patched!\n");
                                                                                        }
                                                                                        if (p332[0].contains("restore")) {
                                                                                            System.out.println("Core4 restored!\n");
                                                                                        }
                                                                                        v289_3 = 1;
                                                                                        v288 = 1;
                                                                                    }
                                                                                    if (com.chelpus.root.utils.corepatch.applyPatch(v148_3, v149_3, v119, v120, v121, v122, v75_0)) {
                                                                                        if (p332[0].contains("patch")) {
                                                                                            System.out.println("Core4 patched!\n");
                                                                                        }
                                                                                        if (p332[0].contains("restore")) {
                                                                                            System.out.println("Core4 restored!\n");
                                                                                        }
                                                                                        v289_3 = 1;
                                                                                        v288 = 1;
                                                                                    }
                                                                                    if (com.chelpus.root.utils.corepatch.applyPatch(v148_3, v149_3, v125, v126, v127, v128, v75_0)) {
                                                                                        if (p332[0].contains("patch")) {
                                                                                            System.out.println("Core4 patched!\n");
                                                                                        }
                                                                                        if (p332[0].contains("restore")) {
                                                                                            System.out.println("Core4 restored!\n");
                                                                                        }
                                                                                        v289_3 = 1;
                                                                                        v288 = 1;
                                                                                    }
                                                                                    if (com.chelpus.root.utils.corepatch.applyPatch(v148_3, v149_3, v113, v114, v115, v116, v75_0)) {
                                                                                        if (p332[0].contains("patch")) {
                                                                                            System.out.println("Core4 patched!InstallLocationPolice\n");
                                                                                        }
                                                                                        if (p332[0].contains("restore")) {
                                                                                            System.out.println("Core4 restored!InstallLocationPolice\n");
                                                                                        }
                                                                                        v290_3 = 1;
                                                                                        v288 = 1;
                                                                                    }
                                                                                    if (com.chelpus.root.utils.corepatch.applyPatch(v148_3, v149_3, v131, v132, v133, v134, v75_0)) {
                                                                                        if (p332[0].contains("patch")) {
                                                                                            System.out.println("Core4 patched!InstallLocationPolice\n");
                                                                                        }
                                                                                        if (p332[0].contains("restore")) {
                                                                                            System.out.println("Core4 restored!InstallLocationPolice\n");
                                                                                        }
                                                                                        v290_3 = 1;
                                                                                        v288 = 1;
                                                                                    }
                                                                                    if (com.chelpus.root.utils.corepatch.applyPatch(v148_3, v149_3, v137, v138, v139, v140, v75_0)) {
                                                                                        if (p332[0].contains("patch")) {
                                                                                            System.out.println("Core4 patched!InstallLocationPolice\n");
                                                                                        }
                                                                                        if (p332[0].contains("restore")) {
                                                                                            System.out.println("Core4 restored!InstallLocationPolice\n");
                                                                                        }
                                                                                        v290_3 = 1;
                                                                                        v288 = 1;
                                                                                    }
                                                                                    if ((v75_0 != 0) && ((v289_3 != 0) && ((v290_3 != 0) && ((v291_3 != 0) && ((v292_1 != 0) && ((v293_1 != 0) && (v294_1 != 0))))))) {
                                                                                        break;
                                                                                    }
                                                                                    com.chelpus.root.utils.corepatch.fileBytes.position((v148_3 + 1));
                                                                                }
                                                                            } catch (Exception v240_8) {
                                                                                System.out.println(new StringBuilder().append("").append(v240_8).toString());
                                                                            }
                                                                        } else {
                                                                            com.chelpus.root.utils.corepatch.lastByteReplace = 0;
                                                                            com.chelpus.root.utils.corepatch.lastPatchPosition = 0;
                                                                            long v249_1 = 0;
                                                                            while (com.chelpus.root.utils.corepatch.fileBytes.hasRemaining()) {
                                                                                int v148_4 = com.chelpus.root.utils.corepatch.fileBytes.position();
                                                                                byte v149_4 = com.chelpus.root.utils.corepatch.fileBytes.get();
                                                                                if (com.chelpus.root.utils.corepatch.applyPatch(v148_4, v149_4, v195, v276, v225, v322, v75_0)) {
                                                                                    if (p332[0].contains("patch")) {
                                                                                        System.out.println("Core4 patched!\n");
                                                                                    }
                                                                                    if (p332[0].contains("restore")) {
                                                                                        System.out.println("Core4 restored!\n");
                                                                                    }
                                                                                    v288 = 1;
                                                                                }
                                                                                if (com.chelpus.root.utils.corepatch.applyPatch(v148_4, v149_4, v198, v279, v228, v325, v75_0)) {
                                                                                    if (p332[0].contains("patch")) {
                                                                                        System.out.println("Core4 patched!\n");
                                                                                    }
                                                                                    if (p332[0].contains("restore")) {
                                                                                        System.out.println("Core4 restored!\n");
                                                                                    }
                                                                                    v288 = 1;
                                                                                }
                                                                                if (com.chelpus.root.utils.corepatch.applyPatch(v148_4, v149_4, v199, v280, v229, v326, v75_0)) {
                                                                                    if (p332[0].contains("patch")) {
                                                                                        System.out.println("Core4 patched!checkUpgradeKeySetLP\n");
                                                                                    }
                                                                                    if (p332[0].contains("restore")) {
                                                                                        System.out.println("Core4 restored!checkUpgradeKeySetLP\n");
                                                                                    }
                                                                                    v288 = 1;
                                                                                }
                                                                                if (com.chelpus.root.utils.corepatch.applyPatch(v148_4, v149_4, v200, v281, v230, v327, v75_0)) {
                                                                                    if (p332[0].contains("patch")) {
                                                                                        System.out.println("Core4 patched!\nCM12");
                                                                                    }
                                                                                    if (p332[0].contains("restore")) {
                                                                                        System.out.println("Core4 restored!\nCM12");
                                                                                    }
                                                                                    v288 = 1;
                                                                                }
                                                                                if (com.chelpus.root.utils.corepatch.applyPatchCounter(v148_4, v149_4, v201, v282, v231, v328, v236_0, v162, v87_0)) {
                                                                                    v236_0++;
                                                                                    if (p332[0].contains("patch")) {
                                                                                        System.out.println("Core4 patched!\nRemove Package from intent");
                                                                                    }
                                                                                    if (p332[0].contains("restore")) {
                                                                                        System.out.println("Core4 restored!\nRemove Package from intent");
                                                                                    }
                                                                                    v288 = 1;
                                                                                }
                                                                                if (com.chelpus.root.utils.corepatch.applyPatchCounter(v148_4, v149_4, v165, v166, v167, v168, v169_0, v170, v87_0)) {
                                                                                    v169_0++;
                                                                                    if (p332[0].contains("patch")) {
                                                                                        System.out.println("Core4 patched!\nRemove Package from intent");
                                                                                    }
                                                                                    if (p332[0].contains("restore")) {
                                                                                        System.out.println("Core4 restored!\nRemove Package from intent");
                                                                                    }
                                                                                    v288 = 1;
                                                                                }
                                                                                if (com.chelpus.root.utils.corepatch.applyPatch(v148_4, v149_4, v192, v273, v222, v319, v87_0)) {
                                                                                    if (p332[0].contains("patch")) {
                                                                                        System.out.println("Core4 patched!\nbuildResolveList");
                                                                                    }
                                                                                    if (p332[0].contains("restore")) {
                                                                                        System.out.println("Core4 restored!\nbuildResolveList");
                                                                                    }
                                                                                    v288 = 1;
                                                                                }
                                                                                if (com.chelpus.root.utils.corepatch.applyPatch(v148_4, v149_4, v193, v274, v223, v320, v87_0)) {
                                                                                    if (p332[0].contains("patch")) {
                                                                                        System.out.println("Core4 patched!\nbuildResolveList");
                                                                                    }
                                                                                    if (p332[0].contains("restore")) {
                                                                                        System.out.println("Core4 restored!\nbuildResolveList");
                                                                                    }
                                                                                    v288 = 1;
                                                                                }
                                                                                if (com.chelpus.root.utils.corepatch.applyPatch(v148_4, v149_4, v194, v275, v224, v321, v87_0)) {
                                                                                    if (p332[0].contains("patch")) {
                                                                                        System.out.println("Core4 patched!\nbuildResolveList");
                                                                                    }
                                                                                    if (p332[0].contains("restore")) {
                                                                                        System.out.println("Core4 restored!\nbuildResolveList");
                                                                                    }
                                                                                    v288 = 1;
                                                                                }
                                                                                if (com.chelpus.root.utils.corepatch.applyPatch(v148_4, v149_4, v196, v277, v226, v323, v75_0)) {
                                                                                    if (p332[0].contains("patch")) {
                                                                                        System.out.println("Core4 patched!\nCM11");
                                                                                    }
                                                                                    if (p332[0].contains("restore")) {
                                                                                        System.out.println("Core4 restored!\nCM11");
                                                                                    }
                                                                                    v288 = 1;
                                                                                }
                                                                                if (com.chelpus.root.utils.corepatch.applyPatch(v148_4, v149_4, v197, v278, v227, v324, v75_0)) {
                                                                                    if (p332[0].contains("patch")) {
                                                                                        System.out.println("Core4 policy patched!\n");
                                                                                    }
                                                                                    if (p332[0].contains("restore")) {
                                                                                        System.out.println("Core4 policy restored!\n");
                                                                                    }
                                                                                    v288 = 1;
                                                                                }
                                                                                if (com.chelpus.root.utils.corepatch.applyPatch(v148_4, v149_4, v143, v144, v145, v146, v75_0)) {
                                                                                    if (p332[0].contains("patch")) {
                                                                                        System.out.println("Core4 policy patched!\n");
                                                                                    }
                                                                                    if (p332[0].contains("restore")) {
                                                                                        System.out.println("Core4 policy restored!\n");
                                                                                    }
                                                                                    v288 = 1;
                                                                                }
                                                                                com.chelpus.root.utils.corepatch.fileBytes.position((v148_4 + 1));
                                                                                v249_1++;
                                                                            }
                                                                            if ((com.chelpus.root.utils.corepatch.lastPatchPosition > 0) && (com.chelpus.root.utils.corepatch.lastByteReplace != null)) {
                                                                                com.chelpus.root.utils.corepatch.fileBytes.position(com.chelpus.root.utils.corepatch.lastPatchPosition);
                                                                                com.chelpus.root.utils.corepatch.fileBytes.put(com.chelpus.root.utils.corepatch.lastByteReplace);
                                                                                com.chelpus.root.utils.corepatch.fileBytes.force();
                                                                            }
                                                                        }
                                                                        v147_2.close();
                                                                        if (!p332[4].contains("framework")) {
                                                                            if (!v251_2.toString().endsWith("/classes.dex")) {
                                                                                com.chelpus.Utils.fixadlerOdex(v251_2, "/system/framework/services.jar");
                                                                            } else {
                                                                                com.chelpus.Utils.fixadler(v251_2);
                                                                            }
                                                                            if ((v251_2.toString().contains("system@framework@services.jar@classes.dex")) && (v288 != 0)) {
                                                                                System.out.println("LuckyPatcher: dalvik-cache patched! ");
                                                                            }
                                                                            if ((v251_2.toString().contains("/classes.dex")) && (v288 != 0)) {
                                                                                System.out.println("start");
                                                                                if (!com.chelpus.Utils.copyFile("/system/framework/services.jar", "/system/framework/services.backup", 1, 0)) {
                                                                                    new java.io.File("/system/framework/services.backup").delete();
                                                                                    v252_1 = new java.io.File;
                                                                                    v252_1(p332[2]);
                                                                                    java.nio.channels.FileChannel v147_3 = new java.io.RandomAccessFile(v252_1, "rw").getChannel();
                                                                                    com.chelpus.root.utils.corepatch.fileBytes = v147_3.map(java.nio.channels.FileChannel$MapMode.READ_WRITE, 0, ((long) ((int) v147_3.size())));
                                                                                    int v236_1 = 0;
                                                                                    int v169_1 = 0;
                                                                                    com.chelpus.root.utils.corepatch.lastByteReplace = 0;
                                                                                    com.chelpus.root.utils.corepatch.lastPatchPosition = 0;
                                                                                    long v249_2 = 0;
                                                                                    try {
                                                                                        while (com.chelpus.root.utils.corepatch.fileBytes.hasRemaining()) {
                                                                                            int v148_5 = com.chelpus.root.utils.corepatch.fileBytes.position();
                                                                                            byte v149_5 = com.chelpus.root.utils.corepatch.fileBytes.get();
                                                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v148_5, v149_5, v195, v276, v225, v322, v75_0)) {
                                                                                                if (p332[0].contains("patch")) {
                                                                                                    System.out.println("Core4 patched!\n");
                                                                                                }
                                                                                                if (p332[0].contains("restore")) {
                                                                                                    System.out.println("Core4 restored!\n");
                                                                                                }
                                                                                            }
                                                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v148_5, v149_5, v198, v279, v228, v325, v75_0)) {
                                                                                                if (p332[0].contains("patch")) {
                                                                                                    System.out.println("Core4 patched!\n");
                                                                                                }
                                                                                                if (p332[0].contains("restore")) {
                                                                                                    System.out.println("Core4 restored!\n");
                                                                                                }
                                                                                            }
                                                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v148_5, v149_5, v199, v280, v229, v326, v75_0)) {
                                                                                                if (p332[0].contains("patch")) {
                                                                                                    System.out.println("Core4 patched!checkUpgradeKeySetLP\n");
                                                                                                }
                                                                                                if (p332[0].contains("restore")) {
                                                                                                    System.out.println("Core4 restored!checkUpgradeKeySetLP\n");
                                                                                                }
                                                                                            }
                                                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v148_5, v149_5, v200, v281, v230, v327, v75_0)) {
                                                                                                if (p332[0].contains("patch")) {
                                                                                                    System.out.println("Core4 patched!\nCM12");
                                                                                                }
                                                                                                if (p332[0].contains("restore")) {
                                                                                                    System.out.println("Core4 restored!\nCM12");
                                                                                                }
                                                                                            }
                                                                                            if (com.chelpus.root.utils.corepatch.applyPatchCounter(v148_5, v149_5, v201, v282, v231, v328, v236_1, v162, v87_0)) {
                                                                                                v236_1++;
                                                                                                if (p332[0].contains("patch")) {
                                                                                                    System.out.println("Core4 patched!\nRemove Package from intent");
                                                                                                }
                                                                                                if (p332[0].contains("restore")) {
                                                                                                    System.out.println("Core4 restored!\nRemove Package from intent");
                                                                                                }
                                                                                            }
                                                                                            if (com.chelpus.root.utils.corepatch.applyPatchCounter(v148_5, v149_5, v165, v166, v167, v168, v169_1, v170, v87_0)) {
                                                                                                v169_1++;
                                                                                                if (p332[0].contains("patch")) {
                                                                                                    System.out.println("Core4 patched!\nRemove Package from intent");
                                                                                                }
                                                                                                if (p332[0].contains("restore")) {
                                                                                                    System.out.println("Core4 restored!\nRemove Package from intent");
                                                                                                }
                                                                                            }
                                                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v148_5, v149_5, v192, v273, v222, v319, v87_0)) {
                                                                                                if (p332[0].contains("patch")) {
                                                                                                    System.out.println("Core4 patched!\nbuildResolveList");
                                                                                                }
                                                                                                if (p332[0].contains("restore")) {
                                                                                                    System.out.println("Core4 restored!\nbuildResolveList");
                                                                                                }
                                                                                            }
                                                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v148_5, v149_5, v193, v274, v223, v320, v87_0)) {
                                                                                                if (p332[0].contains("patch")) {
                                                                                                    System.out.println("Core4 patched!\nbuildResolveList");
                                                                                                }
                                                                                                if (p332[0].contains("restore")) {
                                                                                                    System.out.println("Core4 restored!\nbuildResolveList");
                                                                                                }
                                                                                            }
                                                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v148_5, v149_5, v194, v275, v224, v321, v87_0)) {
                                                                                                if (p332[0].contains("patch")) {
                                                                                                    System.out.println("Core4 patched!\nbuildResolveList");
                                                                                                }
                                                                                                if (p332[0].contains("restore")) {
                                                                                                    System.out.println("Core4 restored!\nbuildResolveList");
                                                                                                }
                                                                                            }
                                                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v148_5, v149_5, v196, v277, v226, v323, v75_0)) {
                                                                                                if (p332[0].contains("patch")) {
                                                                                                    System.out.println("Core4 patched!\nCM11");
                                                                                                }
                                                                                                if (p332[0].contains("restore")) {
                                                                                                    System.out.println("Core4 restored!\nCM11");
                                                                                                }
                                                                                            }
                                                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v148_5, v149_5, v197, v278, v227, v324, v75_0)) {
                                                                                                if (p332[0].contains("patch")) {
                                                                                                    System.out.println("Core4 policy patched!\n");
                                                                                                }
                                                                                                if (p332[0].contains("restore")) {
                                                                                                    System.out.println("Core4 policy restored!\n");
                                                                                                }
                                                                                            }
                                                                                            if (com.chelpus.root.utils.corepatch.applyPatch(v148_5, v149_5, v143, v144, v145, v146, v75_0)) {
                                                                                                if (p332[0].contains("patch")) {
                                                                                                    System.out.println("Core4 policy patched!\n");
                                                                                                }
                                                                                                if (p332[0].contains("restore")) {
                                                                                                    System.out.println("Core4 policy restored!\n");
                                                                                                }
                                                                                            }
                                                                                            com.chelpus.root.utils.corepatch.fileBytes.position((v148_5 + 1));
                                                                                            v249_2++;
                                                                                        }
                                                                                    } catch (Exception v240_10) {
                                                                                        System.out.println(new StringBuilder().append("").append(v240_10).toString());
                                                                                    }
                                                                                    if ((com.chelpus.root.utils.corepatch.lastPatchPosition > 0) && (com.chelpus.root.utils.corepatch.lastByteReplace != null)) {
                                                                                        com.chelpus.root.utils.corepatch.fileBytes.position(com.chelpus.root.utils.corepatch.lastPatchPosition);
                                                                                        com.chelpus.root.utils.corepatch.fileBytes.put(com.chelpus.root.utils.corepatch.lastByteReplace);
                                                                                        com.chelpus.root.utils.corepatch.fileBytes.force();
                                                                                    }
                                                                                    v147_3.close();
                                                                                    if (!v252_1.toString().endsWith("/classes.dex")) {
                                                                                        com.chelpus.Utils.fixadlerOdex(v252_1, "/system/framework/services.jar");
                                                                                    } else {
                                                                                        com.chelpus.Utils.fixadler(v252_1);
                                                                                    }
                                                                                    System.out.println("LuckyPatcher: odex not equal lenght patched! Not enougth space in /system/!");
                                                                                } else {
                                                                                    System.out.println("good space");
                                                                                    new java.io.File("/system/framework/services.backup").delete();
                                                                                    java.util.ArrayList v242_5 = new java.util.ArrayList();
                                                                                    System.out.println("add files");
                                                                                    v242_5.add(new com.android.vending.billing.InAppBillingService.LUCK.AddFilesItem("/data/app/classes.dex", "/data/app/"));
                                                                                    try {
                                                                                        com.chelpus.Utils.addFilesToZip("/system/framework/services.jar", "/system/framework/services.backup", v242_5);
                                                                                        System.out.println("add files finish");
                                                                                        java.io.PrintStream v10_929 = new String[3];
                                                                                        v10_929[0] = "chmod";
                                                                                        v10_929[1] = "0644";
                                                                                        v10_929[2] = "/system/framework/services.backup";
                                                                                        com.chelpus.Utils.run_all_no_root(v10_929);
                                                                                        java.io.PrintStream v10_931 = new String[3];
                                                                                        v10_931[0] = "chown";
                                                                                        v10_931[1] = "0:0";
                                                                                        v10_931[2] = "/system/framework/services.backup";
                                                                                        com.chelpus.Utils.run_all_no_root(v10_931);
                                                                                        java.io.PrintStream v10_933 = new String[3];
                                                                                        v10_933[0] = "chmod";
                                                                                        v10_933[1] = "0.0";
                                                                                        v10_933[2] = "/system/framework/services.backup";
                                                                                        com.chelpus.Utils.run_all_no_root(v10_933);
                                                                                        java.io.File v234_2 = new java.io.File;
                                                                                        v234_2("/system/framework/services.odex");
                                                                                        java.io.File v237 = com.chelpus.Utils.getFileDalvikCache("/system/framework/services.jar");
                                                                                    } catch (Exception v240_13) {
                                                                                        v240_13.printStackTrace();
                                                                                        System.out.println("LuckyPatcher: odex not equal lenght patched! Not enougth space in /system/!");
                                                                                        new java.io.File("/system/framework/services.backup").delete();
                                                                                        System.out.println("finish");
                                                                                    }
                                                                                    if (v234_2.exists()) {
                                                                                        System.out.println("fix odex na osnove rebuild services");
                                                                                        com.chelpus.Utils.fixadlerOdex(v234_2, "/system/framework/services.backup");
                                                                                    }
                                                                                    new java.io.File(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir).append("/ClearDalvik.on").toString()).createNewFile();
                                                                                    new java.io.File("/system/framework/services.jar").delete();
                                                                                    java.io.PrintStream v10_944 = new String[2];
                                                                                    v10_944[0] = "rm";
                                                                                    v10_944[1] = "/system/framework/services.jar";
                                                                                    com.chelpus.Utils.run_all_no_root(v10_944);
                                                                                    new java.io.File("/system/framework/services.backup").renameTo(new java.io.File("/system/framework/services.jar"));
                                                                                    if (!new java.io.File("/system/framework/services.jar").exists()) {
                                                                                        java.io.PrintStream v10_951 = new String[3];
                                                                                        v10_951[0] = "mv";
                                                                                        v10_951[1] = "/system/framework/services.backup";
                                                                                        v10_951[2] = "/system/framework/services.jar";
                                                                                        com.chelpus.Utils.run_all_no_root(v10_951);
                                                                                    }
                                                                                    try {
                                                                                        if (v237 == null) {
                                                                                            new java.io.File("/data/dalvik-cache/arm/system@framework@arm@boot.oat").delete();
                                                                                            new java.io.File("/data/dalvik-cache/arm/system@framework@arm@boot.art").delete();
                                                                                            new java.io.File("/data/dalvik-cache/arm64/system@framework@arm@boot.oat").delete();
                                                                                            new java.io.File("/data/dalvik-cache/arm64/system@framework@arm@boot.art").delete();
                                                                                            new java.io.File("/data/dalvik-cache/x86/system@framework@arm@boot.oat").delete();
                                                                                            new java.io.File("/data/dalvik-cache/x86/system@framework@arm@boot.art").delete();
                                                                                        } else {
                                                                                            java.io.PrintStream v10_953 = new String[2];
                                                                                            v10_953[0] = "rm";
                                                                                            v10_953[1] = v237.getAbsolutePath();
                                                                                            com.chelpus.Utils.run_all_no_root(v10_953);
                                                                                            if (!v237.exists()) {
                                                                                            } else {
                                                                                                v237.delete();
                                                                                            }
                                                                                        }
                                                                                        java.io.PrintStream v10_968 = new String[2];
                                                                                        v10_968[0] = "rm";
                                                                                        v10_968[1] = "/data/dalvik-cache/*.dex";
                                                                                        com.chelpus.Utils.run_all_no_root(v10_968);
                                                                                        java.io.PrintStream v10_970 = new String[2];
                                                                                        v10_970[0] = "rm";
                                                                                        v10_970[1] = "/data/dalvik-cache/*.oat";
                                                                                        com.chelpus.Utils.run_all_no_root(v10_970);
                                                                                        java.io.PrintStream v10_972 = new String[2];
                                                                                        v10_972[0] = "rm";
                                                                                        v10_972[1] = "/data/dalvik-cache/*.art";
                                                                                        com.chelpus.Utils.run_all_no_root(v10_972);
                                                                                        java.io.PrintStream v10_974 = new String[2];
                                                                                        v10_974[0] = "rm";
                                                                                        v10_974[1] = "/data/dalvik-cache/arm/*.dex";
                                                                                        com.chelpus.Utils.run_all_no_root(v10_974);
                                                                                        java.io.PrintStream v10_976 = new String[2];
                                                                                        v10_976[0] = "rm";
                                                                                        v10_976[1] = "/data/dalvik-cache/arm/*.art";
                                                                                        com.chelpus.Utils.run_all_no_root(v10_976);
                                                                                        java.io.PrintStream v10_978 = new String[2];
                                                                                        v10_978[0] = "rm";
                                                                                        v10_978[1] = "/data/dalvik-cache/arm/*.oat";
                                                                                        com.chelpus.Utils.run_all_no_root(v10_978);
                                                                                        java.io.PrintStream v10_980 = new String[2];
                                                                                        v10_980[0] = "rm";
                                                                                        v10_980[1] = "/data/dalvik-cache/arm64/*.dex";
                                                                                        com.chelpus.Utils.run_all_no_root(v10_980);
                                                                                        java.io.PrintStream v10_982 = new String[2];
                                                                                        v10_982[0] = "rm";
                                                                                        v10_982[1] = "/data/dalvik-cache/arm64/*.art";
                                                                                        com.chelpus.Utils.run_all_no_root(v10_982);
                                                                                        java.io.PrintStream v10_984 = new String[2];
                                                                                        v10_984[0] = "rm";
                                                                                        v10_984[1] = "/data/dalvik-cache/arm64/*.oat";
                                                                                        com.chelpus.Utils.run_all_no_root(v10_984);
                                                                                        java.io.PrintStream v10_986 = new String[2];
                                                                                        v10_986[0] = "rm";
                                                                                        v10_986[1] = "/data/dalvik-cache/x86/*.dex";
                                                                                        com.chelpus.Utils.run_all_no_root(v10_986);
                                                                                        java.io.PrintStream v10_988 = new String[2];
                                                                                        v10_988[0] = "rm";
                                                                                        v10_988[1] = "/data/dalvik-cache/x86/*.art";
                                                                                        com.chelpus.Utils.run_all_no_root(v10_988);
                                                                                        java.io.PrintStream v10_990 = new String[2];
                                                                                        v10_990[0] = "rm";
                                                                                        v10_990[1] = "/data/dalvik-cache/x86/*.oat";
                                                                                        com.chelpus.Utils.run_all_no_root(v10_990);
                                                                                    } catch (Exception v240_12) {
                                                                                        v240_12.printStackTrace();
                                                                                    }
                                                                                }
                                                                            }
                                                                            if (!com.chelpus.root.utils.corepatch.onlyDalvik) {
                                                                                java.io.File v233_2 = new java.io.File;
                                                                                v233_2("/system/framework/services.patched");
                                                                                if (v233_2.exists()) {
                                                                                    System.out.println("LuckyPatcher: root found services.patched! ");
                                                                                }
                                                                                java.io.File v233_3 = new java.io.File;
                                                                                v233_3("/system/framework/services.odex");
                                                                                if (v233_3.exists()) {
                                                                                    System.out.println("LuckyPatcher: root found services.odex! ");
                                                                                }
                                                                                if (p332[0].contains("restore")) {
                                                                                    new java.io.File("/system/framework/patch3.done").delete();
                                                                                }
                                                                            }
                                                                        } else {
                                                                            System.out.println("Rebuild file!");
                                                                            if (v251_2.toString().endsWith("/classes.dex")) {
                                                                                com.chelpus.Utils.fixadler(v251_2);
                                                                                if (v288 == 0) {
                                                                                    com.chelpus.root.utils.corepatch.not_found_bytes_for_patch = 1;
                                                                                } else {
                                                                                    com.chelpus.root.utils.corepatch.not_found_bytes_for_patch = 0;
                                                                                    String v239_4 = p332[1].replace("/services.jar", "/services-patched.jar");
                                                                                    String v330_4 = p332[1];
                                                                                    new java.io.File(v239_4).delete();
                                                                                    java.util.ArrayList v242_7 = new java.util.ArrayList();
                                                                                    System.out.println("add files");
                                                                                    v242_7.add(new com.android.vending.billing.InAppBillingService.LUCK.AddFilesItem(v251_2.getAbsolutePath(), new StringBuilder().append(com.chelpus.Utils.getDirs(v251_2).getAbsolutePath()).append("/").toString()));
                                                                                    com.chelpus.Utils.addFilesToZip(v330_4, v239_4, v242_7);
                                                                                    System.out.println("add files finish");
                                                                                    new java.io.File(v330_4).delete();
                                                                                }
                                                                            }
                                                                            if (v251_2.toString().endsWith("/services.odex")) {
                                                                                if (!com.chelpus.Utils.isELFfiles(v251_2)) {
                                                                                    com.chelpus.Utils.fixadlerOdex(v251_2, 0);
                                                                                }
                                                                                if (v288 != 0) {
                                                                                    v251_2.renameTo(new java.io.File(v251_2.getAbsolutePath().replace("/services.odex", "/services-patched.odex")));
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    com.chelpus.Utils.exitFromRootJava();
                                                                    return;
                                                                } else {
                                                                    throw new java.io.FileNotFoundException();
                                                                }
                                                            } else {
                                                                v252_1 = new java.io.File;
                                                                v252_1(p332[2]);
                                                                if (v252_1.exists()) {
                                                                    v243_1.add(v252_1);
                                                                } else {
                                                                    throw new java.io.FileNotFoundException();
                                                                }
                                                            }
                                                        } else {
                                                            if ((new java.io.File("/system/framework/services.odex").exists()) && (new java.io.File("/system/framework/services.odex").length() != 0)) {
                                                                System.out.println("Add services.odex for patch");
                                                                v243_1.add(new java.io.File("/system/framework/services.odex"));
                                                            }
                                                            if (!com.chelpus.Utils.classes_test(new java.io.File("/system/framework/services.jar"))) {
                                                            } else {
                                                                System.out.println("services.jar contain classes,dex");
                                                                java.io.File v329_3 = new java.io.File;
                                                                v329_3("/system/framework/services.jar");
                                                                com.chelpus.root.utils.corepatch.unzip(v329_3, "/data/app");
                                                                java.io.File v232_3 = new java.io.File;
                                                                v232_3("/data/app/classes.dex");
                                                                if (!v232_3.exists()) {
                                                                } else {
                                                                    System.out.println("Add classes.dex for patch");
                                                                    v243_1.add(v232_3);
                                                                }
                                                            }
                                                        }
                                                    } else {
                                                        if ((p332[1].contains("services.jar")) && (com.chelpus.Utils.classes_test(new java.io.File(p332[1])))) {
                                                            java.io.File v329_4 = new java.io.File;
                                                            v329_4(p332[1]);
                                                            com.chelpus.root.utils.corepatch.unzip(v329_4, com.chelpus.Utils.getDirs(v329_4).getAbsolutePath());
                                                            java.io.File v232_4 = new java.io.File;
                                                            v232_4(new StringBuilder().append(com.chelpus.Utils.getDirs(v329_4).getAbsolutePath()).append("/classes.dex").toString());
                                                            if (v232_4.exists()) {
                                                                v243_1.add(v232_4);
                                                            }
                                                        }
                                                        if (!p332[1].contains("services.odex")) {
                                                        } else {
                                                            v243_1.add(new java.io.File(p332[1]));
                                                        }
                                                    }
                                                } catch (java.io.FileNotFoundException v253) {
                                                } catch (Exception v240_11) {
                                                    System.out.println(new StringBuilder().append("Exception e").append(v240_11.toString()).toString());
                                                    com.chelpus.Utils.exitFromRootJava();
                                                    return;
                                                }
                                                System.out.println("Error: services.odex not found!\n\nPlease Odex services.jar and try again!");
                                                com.chelpus.Utils.exitFromRootJava();
                                                return;
                                            }
                                        } else {
                                            throw new java.io.FileNotFoundException();
                                        }
                                    } else {
                                        System.out.println(new StringBuilder().append("OnlyDalvik: add for patch ").append(p332[1]).toString());
                                        v252_0 = new java.io.File;
                                        v252_0(p332[1]);
                                        if (v252_0.exists()) {
                                            v243_1.add(v252_0);
                                        } else {
                                            throw new java.io.FileNotFoundException();
                                        }
                                    }
                                } catch (Exception v240_1) {
                                    System.out.println(new StringBuilder().append("Exception e").append(v240_1.toString()).toString());
                                }
                            } else {
                                if ((new java.io.File("/system/framework/core-libart.odex").exists()) && (new java.io.File("/system/framework/core-libart.odex").length() != 0)) {
                                    v243_1.add(new java.io.File("/system/framework/core-libart.odex"));
                                }
                                if (!com.chelpus.Utils.classes_test(new java.io.File("/system/framework/core-libart.jar"))) {
                                } else {
                                    java.io.File v329_0 = new java.io.File;
                                    v329_0("/system/framework/core-libart.jar");
                                    com.chelpus.root.utils.corepatch.unzip(v329_0, "/data/app");
                                    java.io.File v232_0 = new java.io.File;
                                    v232_0("/data/app/classes.dex");
                                    if (!v232_0.exists()) {
                                    } else {
                                        v243_1.add(v232_0);
                                    }
                                }
                            }
                        } else {
                            if (((p332[1].contains("core.jar")) || (p332[1].contains("core-libart.jar"))) && (com.chelpus.Utils.classes_test(new java.io.File(p332[1])))) {
                                java.io.File v329_1 = new java.io.File;
                                v329_1(p332[1]);
                                com.chelpus.root.utils.corepatch.unzip(v329_1, com.chelpus.Utils.getDirs(v329_1).getAbsolutePath());
                                java.io.File v232_1 = new java.io.File;
                                v232_1(new StringBuilder().append(com.chelpus.Utils.getDirs(v329_1).getAbsolutePath()).append("/classes.dex").toString());
                                if (v232_1.exists()) {
                                    v243_1.add(v232_1);
                                }
                            }
                            if ((!p332[1].contains("core.odex")) && (!p332[1].contains("boot.oat"))) {
                            } else {
                                v243_1.add(new java.io.File(p332[1]));
                            }
                        }
                    } catch (java.io.FileNotFoundException v253) {
                    }
                    System.out.println("Error: core.odex not found!\n\nPlease Odex core.jar and try again!");
                }
            }
            com.chelpus.Utils.exitFromRootJava();
            return;
        }
        if ((p332[4] == null) || (!p332[4].equals("OnlyDalvik"))) {
        } else {
            com.chelpus.root.utils.corepatch.onlyDalvik = 1;
        }
    }

    public static void unzip(java.io.File p18, String p19)
    {
        int v6 = 0;
        try {
            java.io.FileInputStream v5_1 = new java.io.FileInputStream(p18);
            java.util.zip.ZipInputStream v12_1 = new java.util.zip.ZipInputStream(v5_1);
            java.util.zip.ZipEntry v11 = v12_1.getNextEntry();
        } catch (Exception v3) {
            try {
                new net.lingala.zip4j.core.ZipFile(p18).extractFile("classes.dex", p19);
            } catch (Exception v4_0) {
                System.out.println(new StringBuilder().append("Error classes.dex decompress! ").append(v4_0).toString());
                System.out.println(new StringBuilder().append("Exception e1").append(v3.toString()).toString());
            } catch (Exception v4_1) {
                System.out.println(new StringBuilder().append("Error classes.dex decompress! ").append(v4_1).toString());
                System.out.println(new StringBuilder().append("Exception e1").append(v3.toString()).toString());
            }
            System.out.println(new StringBuilder().append("Exception e").append(v3.toString()).toString());
            return;
        }
        while ((v11 != null) && (1 != 0)) {
            if (v11.getName().equals("classes.dex")) {
                java.io.FileOutputStream v7_1 = new java.io.FileOutputStream(new StringBuilder().append(p19).append("/").append("classes.dex").toString());
                byte[] v1 = new byte[2048];
                while(true) {
                    int v9 = v12_1.read(v1);
                    if (v9 == -1) {
                        break;
                    }
                    v7_1.write(v1, 0, v9);
                }
                String[] v14_11 = new String[3];
                v14_11[0] = "chmod";
                v14_11[1] = "777";
                v14_11[2] = new StringBuilder().append(p19).append("/").append("classes.dex").toString();
                com.chelpus.Utils.run_all_no_root(v14_11);
                String[] v14_13 = new String[3];
                v14_13[0] = "chown";
                v14_13[1] = "0.0";
                v14_13[2] = new StringBuilder().append(p19).append("/").append("classes.dex").toString();
                com.chelpus.Utils.run_all_no_root(v14_13);
                String[] v14_15 = new String[3];
                v14_15[0] = "chown";
                v14_15[1] = "0:0";
                v14_15[2] = new StringBuilder().append(p19).append("/").append("classes.dex").toString();
                com.chelpus.Utils.run_all_no_root(v14_15);
                v12_1.closeEntry();
                v7_1.close();
                v6 = 1;
            }
            if (v6 == 0) {
                v11 = v12_1.getNextEntry();
            } else {
                break;
            }
        }
        v12_1.close();
        v5_1.close();
        return;
    }
}
