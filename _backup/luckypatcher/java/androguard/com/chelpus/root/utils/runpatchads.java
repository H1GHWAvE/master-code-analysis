package com.chelpus.root.utils;
public class runpatchads {
    private static boolean ART;
    public static String AdsBlockFile;
    public static String appdir;
    public static java.util.ArrayList classesFiles;
    private static boolean copyDC;
    private static boolean createAPK;
    public static java.io.File crkapk;
    private static boolean dependencies;
    public static String dir;
    public static String dir2;
    public static String dirapp;
    private static boolean fileblock;
    public static java.util.ArrayList filestopatch;
    private static boolean full_offline;
    private static boolean pattern1;
    private static boolean pattern2;
    private static boolean pattern3;
    private static boolean pattern4;
    private static boolean pattern5;
    private static boolean pattern6;
    public static java.io.PrintStream print;
    public static String result;
    public static String sddir;
    public static byte[][] sites;
    public static boolean system;
    public static String uid;

    static runpatchads()
    {
        com.chelpus.root.utils.runpatchads.createAPK = 0;
        com.chelpus.root.utils.runpatchads.fileblock = 1;
        com.chelpus.root.utils.runpatchads.pattern1 = 1;
        com.chelpus.root.utils.runpatchads.pattern2 = 1;
        com.chelpus.root.utils.runpatchads.pattern3 = 1;
        com.chelpus.root.utils.runpatchads.pattern4 = 1;
        com.chelpus.root.utils.runpatchads.pattern5 = 1;
        com.chelpus.root.utils.runpatchads.pattern6 = 1;
        com.chelpus.root.utils.runpatchads.dependencies = 1;
        com.chelpus.root.utils.runpatchads.full_offline = 1;
        com.chelpus.root.utils.runpatchads.copyDC = 0;
        com.chelpus.root.utils.runpatchads.ART = 0;
        com.chelpus.root.utils.runpatchads.dirapp = "/data/app/";
        com.chelpus.root.utils.runpatchads.system = 0;
        com.chelpus.root.utils.runpatchads.uid = "";
        com.chelpus.root.utils.runpatchads.dir = "/sdcard/";
        com.chelpus.root.utils.runpatchads.dir2 = "/sdcard/";
        com.chelpus.root.utils.runpatchads.filestopatch = 0;
        com.chelpus.root.utils.runpatchads.sddir = "/sdcard/";
        com.chelpus.root.utils.runpatchads.appdir = "/sdcard/";
        com.chelpus.root.utils.runpatchads.AdsBlockFile = "";
        com.chelpus.root.utils.runpatchads.classesFiles = new java.util.ArrayList();
        return;
    }

    public runpatchads()
    {
        return;
    }

    public static boolean byteverify(java.nio.MappedByteBuffer p5, int p6, byte p7, byte[] p8, byte[] p9, byte[] p10, byte[] p11, String p12, boolean p13)
    {
        int v2_0 = 1;
        if ((p7 != p8[0]) || (!p13)) {
            v2_0 = 0;
        } else {
            if (p11[0] == 0) {
                p10[0] = p7;
            }
            int v0 = 1;
            p5.position((p6 + 1));
            byte v1 = p5.get();
            while ((v1 == p8[v0]) || (p9[v0] == 1)) {
                if (p11[v0] == 0) {
                    p10[v0] = v1;
                }
                v0++;
                if (v0 != p8.length) {
                    v1 = p5.get();
                } else {
                    p5.position(p6);
                    p5.put(p10);
                    p5.force();
                    com.chelpus.Utils.sendFromRoot(p12);
                }
            }
            p5.position((p6 + 1));
        }
        return v2_0;
    }

    private static final void calcChecksum(byte[] p5, int p6)
    {
        java.util.zip.Adler32 v1_1 = new java.util.zip.Adler32();
        v1_1.update(p5, 12, (p5.length - (p6 + 12)));
        int v0 = ((int) v1_1.getValue());
        p5[(p6 + 8)] = ((byte) v0);
        p5[(p6 + 9)] = ((byte) (v0 >> 8));
        p5[(p6 + 10)] = ((byte) (v0 >> 16));
        p5[(p6 + 11)] = ((byte) (v0 >> 24));
        return;
    }

    private static final void calcSignature(byte[] p8, int p9)
    {
        try {
            java.security.MessageDigest v2 = java.security.MessageDigest.getInstance("SHA-1");
            v2.update(p8, 32, (p8.length - (p9 + 32)));
        } catch (java.security.NoSuchAlgorithmException v3) {
            throw new RuntimeException(v3);
        }
        try {
            int v0 = v2.digest(p8, (p9 + 12), 20);
        } catch (java.security.DigestException v1) {
            throw new RuntimeException(v1);
        }
        if (v0 == 20) {
            return;
        } else {
            throw new RuntimeException(new StringBuilder().append("unexpected digest write:").append(v0).append("bytes").toString());
        }
    }

    public static void clearTemp()
    {
        try {
            java.io.File v2_1 = new java.io.File(new StringBuilder().append(com.chelpus.root.utils.runpatchads.dir).append("/AndroidManifest.xml").toString());
        } catch (Exception v1) {
            com.chelpus.Utils.sendFromRoot(new StringBuilder().append("").append(v1.toString()).toString());
            return;
        }
        if (v2_1.exists()) {
            v2_1.delete();
        }
        if ((com.chelpus.root.utils.runpatchads.classesFiles != null) && (com.chelpus.root.utils.runpatchads.classesFiles.size() > 0)) {
            boolean v4_9 = com.chelpus.root.utils.runpatchads.classesFiles.iterator();
            while (v4_9.hasNext()) {
                java.io.File v0_1 = ((java.io.File) v4_9.next());
                if (v0_1.exists()) {
                    v0_1.delete();
                }
            }
        }
        java.io.File v2_3 = new java.io.File(new StringBuilder().append(com.chelpus.root.utils.runpatchads.dir).append("/classes.dex").toString());
        if (v2_3.exists()) {
            v2_3.delete();
        }
        java.io.File v2_5 = new java.io.File(new StringBuilder().append(com.chelpus.root.utils.runpatchads.dir).append("/classes.dex.apk").toString());
        if (!v2_5.exists()) {
            return;
        } else {
            v2_5.delete();
            return;
        }
    }

    public static void clearTempSD()
    {
        try {
            java.io.File v1_1 = new java.io.File(new StringBuilder().append(com.chelpus.root.utils.runpatchads.sddir).append("/Modified/classes.dex.apk").toString());
        } catch (Exception v0) {
            com.chelpus.Utils.sendFromRoot(new StringBuilder().append("").append(v0.toString()).toString());
            return;
        }
        if (!v1_1.exists()) {
            return;
        } else {
            v1_1.delete();
            return;
        }
    }

    public static void fixadler(java.io.File p5)
    {
        try {
            java.io.FileInputStream v2_1 = new java.io.FileInputStream(p5);
            byte[] v0 = new byte[v2_1.available()];
            v2_1.read(v0);
            com.chelpus.root.utils.runpatchads.calcSignature(v0, 0);
            com.chelpus.root.utils.runpatchads.calcChecksum(v0, 0);
            v2_1.close();
            java.io.FileOutputStream v3_1 = new java.io.FileOutputStream(p5);
            v3_1.write(v0);
            v3_1.close();
        } catch (Exception v1) {
            v1.printStackTrace();
        }
        return;
    }

    public static void main(String[] p63)
    {
        com.android.vending.billing.InAppBillingService.LUCK.LogOutputStream v47 = new com.android.vending.billing.InAppBillingService.LUCK.LogOutputStream;
        v47("System.out");
        com.chelpus.root.utils.runpatchads.print = new java.io.PrintStream(v47);
        com.chelpus.root.utils.runpatchads.print.println("Ads-Code Running!");
        com.chelpus.Utils.startRootJava(new com.chelpus.root.utils.runpatchads$1());
        com.chelpus.Utils.sendFromRoot("Ads-Code Running!");
        com.chelpus.Utils.kill(p63[0]);
        java.util.ArrayList v2_1 = new java.util.ArrayList();
        com.chelpus.root.utils.runpatchads.fileblock = 1;
        com.chelpus.root.utils.runpatchads.pattern1 = 1;
        com.chelpus.root.utils.runpatchads.pattern2 = 1;
        com.chelpus.root.utils.runpatchads.pattern3 = 1;
        com.chelpus.root.utils.runpatchads.pattern4 = 1;
        com.chelpus.root.utils.runpatchads.pattern5 = 1;
        com.chelpus.root.utils.runpatchads.pattern6 = 1;
        com.chelpus.root.utils.runpatchads.dependencies = 1;
        com.chelpus.root.utils.runpatchads.full_offline = 1;
        com.chelpus.root.utils.runpatchads.filestopatch = new java.util.ArrayList();
        try {
            java.io.File[] v33 = new java.io.File(p63[3]).listFiles();
            int v11_3 = v33.length;
            int v10_22 = 0;
        } catch (Exception v26_0) {
            v26_0.printStackTrace();
            try {
                if (p63[1].contains("pattern0")) {
                    if (!p63[1].contains("pattern1")) {
                        com.chelpus.root.utils.runpatchads.pattern1 = 0;
                    }
                    if (!p63[1].contains("pattern2")) {
                        com.chelpus.root.utils.runpatchads.pattern2 = 0;
                    }
                    if (!p63[1].contains("pattern3")) {
                        com.chelpus.root.utils.runpatchads.pattern3 = 0;
                    }
                    if (!p63[1].contains("pattern4")) {
                        com.chelpus.root.utils.runpatchads.pattern4 = 0;
                    }
                    if (!p63[1].contains("pattern5")) {
                        com.chelpus.root.utils.runpatchads.pattern5 = 0;
                    }
                    if (!p63[1].contains("pattern6")) {
                        com.chelpus.root.utils.runpatchads.pattern6 = 0;
                    }
                    if (!p63[1].contains("dependencies")) {
                        com.chelpus.root.utils.runpatchads.dependencies = 0;
                    }
                    if (!p63[1].contains("fulloffline")) {
                        com.chelpus.root.utils.runpatchads.full_offline = 0;
                    }
                    if ((p63[6] != null) && (p63[6].contains("createAPK"))) {
                        com.chelpus.root.utils.runpatchads.createAPK = 1;
                    }
                    if ((p63[6] != null) && (p63[6].contains("ART"))) {
                        com.chelpus.root.utils.runpatchads.ART = 1;
                    }
                    if (p63[6] != null) {
                        com.chelpus.Utils.sendFromRoot(p63[6]);
                    }
                    if (p63[7] != null) {
                        com.chelpus.Utils.sendFromRoot(new StringBuilder().append("1 ").append(p63[7]).toString());
                        com.chelpus.root.utils.runpatchads.AdsBlockFile = p63[7];
                    }
                    if (p63[8] == null) {
                        com.chelpus.Utils.sendFromRoot("fignya");
                    } else {
                        com.chelpus.root.utils.runpatchads.uid = p63[8];
                    }
                } else {
                    com.chelpus.root.utils.runpatchads.fileblock = 0;
                }
                try {
                    if (p63[5].contains("copyDC")) {
                        com.chelpus.root.utils.runpatchads.copyDC = 1;
                    }
                } catch (int v10) {
                } catch (int v10) {
                }
                if (com.chelpus.root.utils.runpatchads.createAPK) {
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.startUnderRoot = Boolean.valueOf(0);
                }
                java.util.ArrayList v53_1 = new java.util.ArrayList();
                java.util.ArrayList v3_1 = new java.util.ArrayList();
                java.util.ArrayList v4_1 = new java.util.ArrayList();
                java.util.ArrayList v5_1 = new java.util.ArrayList();
                java.util.ArrayList v6_1 = new java.util.ArrayList();
                java.util.ArrayList v7_1 = new java.util.ArrayList();
                java.util.ArrayList v8_1 = new java.util.ArrayList();
                v3_1.add("1A ?? FF FF");
                v4_1.add("1A ?? ?? ??");
                v5_1.add(Boolean.valueOf(com.chelpus.root.utils.runpatchads.pattern5));
                v6_1.add("(offline intekekt 0)");
                v7_1.add("search_offline");
                v8_1.add(Boolean.valueOf(0));
                v3_1.add("1B ?? FF FF FF FF");
                v4_1.add("1B ?? ?? ?? ?? ??");
                v5_1.add(Boolean.valueOf(com.chelpus.root.utils.runpatchads.pattern5));
                v6_1.add("(offline intekekt 0)");
                v7_1.add("search_offline");
                v8_1.add(Boolean.valueOf(0));
                v3_1.add("6E 10 FF FF ?? 00 0C ??");
                v4_1.add("6E 10 ?? ?? ?? 00 12 S0");
                v5_1.add(Boolean.valueOf(0));
                v6_1.add("ads5 Fixed!\noffline (sha intekekt 4)");
                v7_1.add("search_offline");
                v8_1.add(Boolean.valueOf(0));
                v3_1.add("6E 10 FF FF ?? 00 0C ??");
                v4_1.add("6E 10 ?? ?? ?? 00 12 S0");
                v5_1.add(Boolean.valueOf(com.chelpus.root.utils.runpatchads.full_offline));
                v6_1.add("ads5 Fixed!\nfull_offline (sha intekekt 4)");
                v7_1.add("");
                v8_1.add(Boolean.valueOf(0));
                v3_1.add("12 F1 12 E2 1A ?? ?? ?? 70 ??");
                v4_1.add("12 31 12 32 1A ?? ?? ?? 70 ??");
                v5_1.add(Boolean.valueOf(com.chelpus.root.utils.runpatchads.pattern1));
                v6_1.add("ads1 Fixed!\n");
                v7_1.add("");
                v8_1.add(Boolean.valueOf(1));
                v3_1.add("13 ?? d8 02 13 ?? 5a 00 1A ?? ?? ?? 70 ??");
                v4_1.add("13 ?? 03 00 13 ?? 03 00 1A ?? ?? ?? 70 ??");
                v5_1.add(Boolean.valueOf(com.chelpus.root.utils.runpatchads.pattern1));
                v6_1.add("ads1 Fixed!\n");
                v7_1.add("");
                v8_1.add(Boolean.valueOf(1));
                v3_1.add("13 ?? d4 01 13 ?? 3c 00 1A ?? ?? ?? 70 ??");
                v4_1.add("13 ?? 03 00 13 ?? 03 00 1A ?? ?? ?? 70 ??");
                v5_1.add(Boolean.valueOf(com.chelpus.root.utils.runpatchads.pattern1));
                v6_1.add("ads1 Fixed!\n");
                v7_1.add("");
                v8_1.add(Boolean.valueOf(1));
                v3_1.add("13 ?? 2c 01 13 ?? fa 00 1A ?? ?? ?? 70 ??");
                v4_1.add("13 ?? 03 00 13 ?? 03 00 1A ?? ?? ?? 70 ??");
                v5_1.add(Boolean.valueOf(com.chelpus.root.utils.runpatchads.pattern1));
                v6_1.add("ads1 Fixed!\n");
                v7_1.add("");
                v8_1.add(Boolean.valueOf(1));
                v3_1.add("13 ?? 40 01 13 ?? 32 00 1A ?? ?? ?? 70 ??");
                v4_1.add("13 ?? 03 00 13 ?? 03 00 1A ?? ?? ?? 70 ??");
                v5_1.add(Boolean.valueOf(com.chelpus.root.utils.runpatchads.pattern1));
                v6_1.add("ads1 Fixed!\n");
                v7_1.add("");
                v8_1.add(Boolean.valueOf(1));
                v3_1.add("13 ?? 40 01 ?? ?? ?? ?? 13 ?? 32 00 1A ?? ?? ?? 70 ??");
                v4_1.add("13 ?? 03 00 ?? ?? ?? ?? 13 ?? 03 00 1A ?? ?? ?? 70 ??");
                v5_1.add(Boolean.valueOf(com.chelpus.root.utils.runpatchads.pattern1));
                v6_1.add("ads1 Fixed!\n");
                v7_1.add("");
                v8_1.add(Boolean.valueOf(1));
                v3_1.add("13 ?? A0 00 13 ?? 58 02 1A ?? ?? ?? 70 ??");
                v4_1.add("13 ?? 03 00 13 ?? 03 00 1A ?? ?? ?? 70 ??");
                v5_1.add(Boolean.valueOf(com.chelpus.root.utils.runpatchads.pattern1));
                v6_1.add("ads1 Fixed!\n");
                v7_1.add("");
                v8_1.add(Boolean.valueOf(1));
                v3_1.add("13 00 11 00 6E ?? ?? ?? ?? ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 38 ?? 0D 00 12 E1 12 E2 6E ?? ?? ?? ?? ?? 12 E1 12 E2 6E ?? ?? ?? ?? ?? 0E 00 12 E0 12 E1 6E 40");
                v4_1.add("13 00 11 00 6E ?? ?? ?? ?? ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 38 ?? 0D 00 12 11 12 12 6E ?? ?? ?? ?? ?? 12 11 12 12 6E ?? ?? ?? ?? ?? 0E 00 12 10 12 11 6E 40");
                v5_1.add(Boolean.valueOf(com.chelpus.root.utils.runpatchads.pattern1));
                v6_1.add("ads1 Fixed!\n");
                v7_1.add("");
                v8_1.add(Boolean.valueOf(0));
                v3_1.add("13 00 11 00 6E ?? ?? ?? ?? ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 38 ?? 0D 00 12 E0 12 E1 6E ?? ?? ?? ?? ?? 12 E0 12 E1 6E ?? ?? ?? ?? ?? 0E 00 12 E0 12 E1 6E 40");
                v4_1.add("13 00 11 00 6E ?? ?? ?? ?? ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 38 ?? 0D 00 12 10 12 11 6E ?? ?? ?? ?? ?? 12 10 12 11 6E ?? ?? ?? ?? ?? 0E 00 12 10 12 11 6E 40");
                v5_1.add(Boolean.valueOf(com.chelpus.root.utils.runpatchads.pattern1));
                v6_1.add("ads1 Fixed!\n");
                v7_1.add("");
                v8_1.add(Boolean.valueOf(0));
                v3_1.add("6E ?? ?? ?? ?? ?? 39 ?? ?? ?? 22 ?? ?? ?? 70 ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 0A ?? ?? ?? ?? ?? ?? ?? 0A ?? 6E ?? ?? ?? ?? ?? 0A ?? ?? ?? ?? ?? ?? ?? 0A ?? D8 ?? ?? FE D8 ?? ?? FE 6E ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 0E 00");
                v4_1.add("6E ?? ?? ?? ?? ?? 39 ?? ?? ?? 22 ?? ?? ?? 70 ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 12 ?? ?? ?? ?? ?? ?? ?? 0A ?? 6E ?? ?? ?? ?? ?? 12 ?? ?? ?? ?? ?? ?? ?? 0A ?? 12 ?? 00 00 12 ?? 00 00 6E ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 0E 00");
                v5_1.add(Boolean.valueOf(com.chelpus.root.utils.runpatchads.pattern1));
                v6_1.add("ads1 Fixed!\n");
                v7_1.add("");
                v8_1.add(Boolean.valueOf(0));
                v3_1.add("12 04 39 06 0A 00 22 00 ?? ?? 1A 01 ?? ?? 70 20 ?? ?? ?? ?? 27 00 71 10 ?? ?? ?? ?? 70 20 ?? ?? ?? ?? 71 10 ?? ?? ?? ?? 5B ?? ?? ?? 59 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 5B");
                v4_1.add("0E 00 39 06 0A 00 22 00 ?? ?? 1A 01 ?? ?? 70 20 ?? ?? ?? ?? 27 00 71 10 ?? ?? ?? ?? 70 20 ?? ?? ?? ?? 71 10 ?? ?? ?? ?? 5B ?? ?? ?? 59 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 5B");
                v5_1.add(Boolean.valueOf(com.chelpus.root.utils.runpatchads.pattern1));
                v6_1.add("ads1 Fixed!\nIMAdView");
                v7_1.add("");
                v8_1.add(Boolean.valueOf(0));
                v3_1.add("12 F1 12 E2 1A ?? ?? ?? 70 ??");
                v4_1.add("12 01 12 02 1A ?? ?? ?? 70 ??");
                v5_1.add(Boolean.valueOf(com.chelpus.root.utils.runpatchads.pattern1));
                v6_1.add("ads1 Fixed!\n");
                v7_1.add("");
                v8_1.add(Boolean.valueOf(1));
                v3_1.add("13 ?? d4 01 13 ?? 3c 00 1A ?? ?? ?? 70 ??");
                v4_1.add("13 ?? 00 00 13 ?? 00 00 1A ?? ?? ?? 70 ??");
                v5_1.add(Boolean.valueOf(com.chelpus.root.utils.runpatchads.pattern2));
                v6_1.add("ads2 Fixed!\n");
                v7_1.add("");
                v8_1.add(Boolean.valueOf(1));
                v3_1.add("13 ?? 2c 01 13 ?? fa 00 1A ?? ?? ?? 70 ??");
                v4_1.add("13 ?? 00 00 13 ?? 00 00 1A ?? ?? ?? 70 ??");
                v5_1.add(Boolean.valueOf(com.chelpus.root.utils.runpatchads.pattern2));
                v6_1.add("ads2 Fixed!\n");
                v7_1.add("");
                v8_1.add(Boolean.valueOf(1));
                v3_1.add("13 ?? 40 01 13 ?? 32 00 1A ?? ?? ?? 70 ??");
                v4_1.add("13 ?? 00 00 13 ?? 00 00 1A ?? ?? ?? 70 ??");
                v5_1.add(Boolean.valueOf(com.chelpus.root.utils.runpatchads.pattern2));
                v6_1.add("ads2 Fixed!\n");
                v7_1.add("");
                v8_1.add(Boolean.valueOf(1));
                v3_1.add("13 ?? 40 01 ?? ?? ?? ?? 13 ?? 32 00 1A ?? ?? ?? 70 ??");
                v4_1.add("13 ?? 00 00 ?? ?? ?? ?? 13 ?? 00 00 1A ?? ?? ?? 70 ??");
                v5_1.add(Boolean.valueOf(com.chelpus.root.utils.runpatchads.pattern2));
                v6_1.add("ads2 Fixed!\n");
                v7_1.add("");
                v8_1.add(Boolean.valueOf(1));
                v3_1.add("13 ?? d8 02 13 ?? 5a 00 1A ?? ?? ?? 70 ??");
                v4_1.add("13 ?? 00 00 13 ?? 00 00 1A ?? ?? ?? 70 ??");
                v5_1.add(Boolean.valueOf(com.chelpus.root.utils.runpatchads.pattern2));
                v6_1.add("ads2 Fixed!\n");
                v7_1.add("");
                v8_1.add(Boolean.valueOf(1));
                v3_1.add("13 ?? A0 00 13 ?? 58 02 1A ?? ?? ?? 70 ??");
                v4_1.add("13 ?? 00 00 13 ?? 00 00 1A ?? ?? ?? 70 ??");
                v5_1.add(Boolean.valueOf(com.chelpus.root.utils.runpatchads.pattern2));
                v6_1.add("ads2 Fixed!\n");
                v7_1.add("");
                v8_1.add(Boolean.valueOf(1));
                v3_1.add("13 00 11 00 6E ?? ?? ?? ?? ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 38 ?? 0D 00 12 E1 12 E2 6E ?? ?? ?? ?? ?? 12 E1 12 E2 6E ?? ?? ?? ?? ?? 0E 00 12 E0 12 E1 6E 40");
                v4_1.add("13 00 11 00 6E ?? ?? ?? ?? ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 38 ?? 0D 00 12 11 12 12 6E ?? ?? ?? ?? ?? 12 11 12 12 6E ?? ?? ?? ?? ?? 0E 00 12 10 12 11 6E 40");
                v5_1.add(Boolean.valueOf(com.chelpus.root.utils.runpatchads.pattern2));
                v6_1.add("ads2 Fixed!\n");
                v7_1.add("");
                v8_1.add(Boolean.valueOf(0));
                v3_1.add("13 00 11 00 6E ?? ?? ?? ?? ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 38 ?? 0D 00 12 E0 12 E1 6E ?? ?? ?? ?? ?? 12 E0 12 E1 6E ?? ?? ?? ?? ?? 0E 00 12 E0 12 E1 6E 40");
                v4_1.add("13 00 11 00 6E ?? ?? ?? ?? ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 38 ?? 0D 00 12 10 12 11 6E ?? ?? ?? ?? ?? 12 10 12 11 6E ?? ?? ?? ?? ?? 0E 00 12 10 12 11 6E 40");
                v5_1.add(Boolean.valueOf(com.chelpus.root.utils.runpatchads.pattern2));
                v6_1.add("ads2 Fixed!\n");
                v7_1.add("");
                v8_1.add(Boolean.valueOf(0));
                v3_1.add("6E ?? ?? ?? ?? ?? 39 ?? ?? ?? 22 ?? ?? ?? 70 ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 0A ?? ?? ?? ?? ?? ?? ?? 0A ?? 6E ?? ?? ?? ?? ?? 0A ?? ?? ?? ?? ?? ?? ?? 0A ?? D8 ?? ?? FE D8 ?? ?? FE 6E ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 0E 00");
                v4_1.add("6E ?? ?? ?? ?? ?? 39 ?? ?? ?? 22 ?? ?? ?? 70 ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 12 ?? ?? ?? ?? ?? ?? ?? 0A ?? 6E ?? ?? ?? ?? ?? 12 ?? ?? ?? ?? ?? ?? ?? 0A ?? 12 ?? 00 00 12 ?? 00 00 6E ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 0E 00");
                v5_1.add(Boolean.valueOf(com.chelpus.root.utils.runpatchads.pattern2));
                v6_1.add("ads2 Fixed!\n");
                v7_1.add("");
                v8_1.add(Boolean.valueOf(0));
                v3_1.add("12 04 39 06 0A 00 22 00 ?? ?? 1A 01 ?? ?? 70 20 ?? ?? ?? ?? 27 00 71 10 ?? ?? ?? ?? 70 20 ?? ?? ?? ?? 71 10 ?? ?? ?? ?? 5B ?? ?? ?? 59 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 5B");
                v4_1.add("0E 00 39 06 0A 00 22 00 ?? ?? 1A 01 ?? ?? 70 20 ?? ?? ?? ?? 27 00 71 10 ?? ?? ?? ?? 70 20 ?? ?? ?? ?? 71 10 ?? ?? ?? ?? 5B ?? ?? ?? 59 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 5B");
                v5_1.add(Boolean.valueOf(com.chelpus.root.utils.runpatchads.pattern2));
                v6_1.add("ads2 Fixed!\nIMAdView");
                v7_1.add("");
                v8_1.add(Boolean.valueOf(0));
                v3_1.add("55 ?? ?? ?? 39 00 ?? ?? 22 ?? ?? ?? 54 ?? ?? ?? 70 ?? ?? ?? ?? ?? 54 ?? ?? ?? 72 ?? ?? ?? ?? ?? 0C 01 54");
                v4_1.add("55 ?? ?? ?? 32 00 ?? ?? 22 ?? ?? ?? 54 ?? ?? ?? 70 ?? ?? ?? ?? ?? 54 ?? ?? ?? 72 ?? ?? ?? ?? ?? 0C 01 54");
                v5_1.add(Boolean.valueOf(com.chelpus.root.utils.runpatchads.pattern3));
                v6_1.add("ads3 Fixed!\n");
                v7_1.add("");
                v8_1.add(Boolean.valueOf(0));
                v3_1.add("1D ?? 54 ?? ?? ?? 39 ?? ?? ?? 5B ?? ?? ?? 12 00 5C ?? ?? ?? 22 ?? ?? ?? 70 ?? ?? ?? ?? ?? 5B ?? ?? ?? 54 ?? ?? ?? 6E 10");
                v4_1.add("1D ?? 54 ?? ?? ?? 39 ?? ?? ?? 5B ?? ?? ?? 12 10 5C ?? ?? ?? 22 ?? ?? ?? 70 ?? ?? ?? ?? ?? 5B ?? ?? ?? 54 ?? ?? ?? 6E 10");
                v5_1.add(Boolean.valueOf(com.chelpus.root.utils.runpatchads.pattern3));
                v6_1.add("ads3 Fixed!\n");
                v7_1.add("");
                v8_1.add(Boolean.valueOf(0));
                v3_1.add("1D ?? 70 ?? ?? ?? ?? ?? 0A ?? 38 ?? ?? ?? 1A ?? ?? ?? 71 ?? ?? ?? ?? ?? 1E ?? 0E 00 71");
                v4_1.add("1D ?? 70 ?? ?? ?? ?? ?? 0A ?? 33 00 ?? ?? 1A ?? ?? ?? 71 ?? ?? ?? ?? ?? 1E ?? 0E 00 71");
                v5_1.add(Boolean.valueOf(com.chelpus.root.utils.runpatchads.pattern3));
                v6_1.add("ads3 Fixed!\n");
                v7_1.add("");
                v8_1.add(Boolean.valueOf(0));
                v3_1.add("1D 01 54 10 ?? ?? 38 00 ?? ?? 12 10 1E 01 0F 00 12 00 28 ?? ?? ?? 1E 01 27 00");
                v4_1.add("1D 01 54 10 ?? ?? 33 00 ?? ?? 12 10 1E 01 0F 00 12 00 28 ?? ?? ?? 1E 01 27 00");
                v5_1.add(Boolean.valueOf(com.chelpus.root.utils.runpatchads.pattern3));
                v6_1.add("ads3 Fixed!\n");
                v7_1.add("");
                v8_1.add(Boolean.valueOf(0));
                v3_1.add("71 10 ?? ?? ?? ?? 15 03 FF FF 07 60 07 71 07 82 07 94 07 A5 76 06");
                v4_1.add("71 10 ?? ?? ?? ?? 15 03 00 FF 07 60 07 71 07 82 07 94 07 A5 76 06");
                v5_1.add(Boolean.valueOf(com.chelpus.root.utils.runpatchads.pattern4));
                v6_1.add("ads4 Fixed!\n");
                v7_1.add("");
                v8_1.add(Boolean.valueOf(0));
                v3_1.add("71 10 ?? ?? ?? ?? 15 03 FF FF 07 60 07 71 07 82 07 94 07 A5 74 06");
                v4_1.add("71 10 ?? ?? ?? ?? 15 03 00 FF 07 60 07 71 07 82 07 94 07 A5 74 06");
                v5_1.add(Boolean.valueOf(com.chelpus.root.utils.runpatchads.pattern4));
                v6_1.add("ads4 Fixed!\n");
                v7_1.add("");
                v8_1.add(Boolean.valueOf(0));
                v3_1.add("12 ?? 12 ?? 6E ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0C ?? 1A ?? ?? ?? 6E ?? ?? ?? ?? ?? 0A ?? 33 ?? ?? ?? 1A ?? ?? ?? 71");
                v4_1.add("12 ?? 12 ?? 6E ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0C ?? 1A ?? ?? ?? 6E ?? ?? ?? ?? ?? 0A ?? 33 00 ?? ?? 1A ?? ?? ?? 71");
                v5_1.add(Boolean.valueOf(com.chelpus.root.utils.runpatchads.pattern4));
                v6_1.add("ads4 Fixed!\n");
                v7_1.add("");
                v8_1.add(Boolean.valueOf(0));
                v3_1.add("1A 00 ?? ?? 6E 20 ?? ?? ?? ?? 0C ?? 1F ?? ?? ?? 6E 10 ?? ?? ?? ?? 0C 00 39 00 04 00 12 00 11 00");
                v4_1.add("1A 00 ?? ?? 6E 20 ?? ?? ?? ?? 0C 01 1F ?? ?? ?? 6E 10 ?? ?? ?? ?? 0C 00 33 00 ?? ?? 12 00 11 00");
                v5_1.add(Boolean.valueOf(com.chelpus.root.utils.runpatchads.pattern5));
                v6_1.add("ads5 Fixed!\noffline");
                v7_1.add("");
                v8_1.add(Boolean.valueOf(0));
                v3_1.add("6E ?? ?? ?? ?? ?? 0C ?? 38 ?? ?? 00 6E ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0A ?? 33 ?? ?? 00 6E ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0A");
                v4_1.add("6E ?? ?? ?? ?? ?? 0C ?? 32 00 ?? 00 6E ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0A ?? 33 ?? ?? 00 6E ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0A");
                v5_1.add(Boolean.valueOf(com.chelpus.root.utils.runpatchads.pattern5));
                v6_1.add("ads5 Fixed!\noffline");
                v7_1.add("");
                v8_1.add(Boolean.valueOf(0));
                v3_1.add("12 ?? 12 ?? 1A ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 1F ?? ?? ?? 39 ?? ?? ?? 0F ?? 6E ?? ?? ?? ?? ?? 0C ?? 38 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0A ?? 33 ?? ?? ?? 01 ?? 28 ?? 01 ?? 28");
                v4_1.add("12 ?? 12 ?? 1A ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 1F ?? ?? ?? 33 00 ?? ?? 0F ?? 6E ?? ?? ?? ?? ?? 0C ?? 38 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0A ?? 33 ?? ?? ?? 01 ?? 28 ?? 01 ?? 28");
                v5_1.add(Boolean.valueOf(com.chelpus.root.utils.runpatchads.pattern5));
                v6_1.add("ads5 Fixed!\noffline");
                v7_1.add("");
                v8_1.add(Boolean.valueOf(0));
                v3_1.add("12 ?? 12 ?? 1A ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 1F ?? ?? ?? 39 ?? ?? ?? 01 ?? 0F ?? 6E ?? ?? ?? ?? ?? 0C ?? 38 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0A ?? 33 ?? ?? ?? 01 ?? 28 ?? 01 ?? 28");
                v4_1.add("12 ?? 12 ?? 1A ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 1F ?? ?? ?? 33 00 ?? ?? 01 ?? 0F ?? 6E ?? ?? ?? ?? ?? 0C ?? 38 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0A ?? 33 ?? ?? ?? 01 ?? 28 ?? 01 ?? 28");
                v5_1.add(Boolean.valueOf(com.chelpus.root.utils.runpatchads.pattern5));
                v6_1.add("ads5 Fixed!\noffline");
                v7_1.add("");
                v8_1.add(Boolean.valueOf(0));
                v3_1.add("12 15 6E 10 ?? ?? ?? ?? 0C 01 1A ?? ?? ?? 6E 20 ?? ?? ?? ?? 0A 04 12 ?? 33 ?? ?? ?? 0F 05 6E 10");
                v4_1.add("12 05 6E 10 ?? ?? ?? ?? 0C 01 1A ?? ?? ?? 6E 20 ?? ?? ?? ?? 0A 04 12 ?? 33 ?? ?? ?? 0F 05 6E 10");
                v5_1.add(Boolean.valueOf(com.chelpus.root.utils.runpatchads.pattern5));
                v6_1.add("ads5 Fixed!\noffline");
                v7_1.add("");
                v8_1.add(Boolean.valueOf(0));
                v3_1.add("54 ?? ?? ?? 1A ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 1F ?? ?? ?? 1A ?? ?? ?? 54 ?? ?? ?? 1A ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 1F ?? ?? ?? 6E ?? ?? ?? ?? ?? 0A ?? 38 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 38");
                v4_1.add("54 ?? ?? ?? 1A ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 1F ?? ?? ?? 1A ?? ?? ?? 54 ?? ?? ?? 1A ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 1F ?? ?? ?? 6E ?? ?? ?? ?? ?? 0A ?? 32 00 ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 38");
                v5_1.add(Boolean.valueOf(com.chelpus.root.utils.runpatchads.pattern5));
                v6_1.add("ads5 Fixed!\noffline");
                v7_1.add("");
                v8_1.add(Boolean.valueOf(0));
                v3_1.add("12 01 39 04 04 00 01 10 0F 00 1A ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 1F ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 39 ?? ?? ?? 01 10 28");
                v4_1.add("12 01 33 00 ?? ?? 01 10 0F 00 1A ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 1F ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 39 ?? ?? ?? 01 10 28");
                v5_1.add(Boolean.valueOf(com.chelpus.root.utils.runpatchads.pattern5));
                v6_1.add("ads5 Fixed!\noffline");
                v7_1.add("");
                v8_1.add(Boolean.valueOf(0));
                v3_1.add("12 01 6E ?? ?? ?? ?? ?? 0C ?? 1A ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 1F ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 38 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0A ?? 38");
                v4_1.add("12 01 6E ?? ?? ?? ?? ?? 0C ?? 1A ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 1F ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 32 00 ?? ?? 6E ?? ?? ?? ?? ?? 0A ?? 38");
                v5_1.add(Boolean.valueOf(com.chelpus.root.utils.runpatchads.pattern5));
                v6_1.add("ads5 Fixed!\noffline");
                v7_1.add("");
                v8_1.add(Boolean.valueOf(0));
                v3_1.add("6E ?? ?? ?? ?? ?? 0C ?? 1A ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 1F ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 38 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0A ?? 38 ?? ?? ?? 12 ?? 0F ?? 12 00 28");
                v4_1.add("6E ?? ?? ?? ?? ?? 0C ?? 1A ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 1F ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 32 00 ?? ?? 6E ?? ?? ?? ?? ?? 0A ?? 32 00 ?? ?? 12 ?? 0F ?? 12 00 28");
                v5_1.add(Boolean.valueOf(com.chelpus.root.utils.runpatchads.pattern5));
                v6_1.add("ads5 Fixed!\noffline");
                v7_1.add("");
                v8_1.add(Boolean.valueOf(0));
                v3_1.add("54 ?? ?? ?? 39 ?? ?? ?? 1A ?? ?? ?? 1A ?? ?? ?? 71 ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 0E 00 1A");
                v4_1.add("54 ?? ?? ?? 33 00 ?? ?? 1A ?? ?? ?? 1A ?? ?? ?? 71 ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 0E 00 1A");
                v5_1.add(Boolean.valueOf(com.chelpus.root.utils.runpatchads.pattern6));
                v6_1.add("ads6 Fixed!\n");
                v7_1.add("");
                v8_1.add(Boolean.valueOf(0));
                v3_1.add("12 00 62 ?? ?? ?? 38 ?? ?? 00 62 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 39 ?? ?? 00");
                v4_1.add("12 00 62 ?? ?? ?? 32 00 ?? 00 62 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 39 ?? ?? 00");
                v5_1.add(Boolean.valueOf(com.chelpus.root.utils.runpatchads.pattern6));
                v6_1.add("ads6 Fixed!\nangry offline");
                v7_1.add("");
                v8_1.add(Boolean.valueOf(0));
                if (!p63[0].contains("com.buak.Link2SD")) {
                    v3_1.add("00 05 2E 6F 64 65 78 00");
                    v4_1.add("00 05 2E 6F 64 65 79 00");
                    v5_1.add(Boolean.valueOf(com.chelpus.root.utils.runpatchads.pattern6));
                    v6_1.add("ads6 Fixed!\n");
                    v7_1.add("");
                    v8_1.add(Boolean.valueOf(1));
                }
                v3_1.add("00 04 6F 64 65 78 00");
                v4_1.add("00 04 6F 64 65 79 00");
                v5_1.add(Boolean.valueOf(com.chelpus.root.utils.runpatchads.pattern6));
                v6_1.add("ads6 Fixed!\n");
                v7_1.add("");
                v8_1.add(Boolean.valueOf(1));
                v3_1.add("54 ?? ?? ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 22 ?? ?? ?? 12 E3 12 E4 70 ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 54 ?? ?? ?? 54");
                v4_1.add("54 ?? ?? ?? 54 ?? ?? ?? 71 ?? ?? ?? ?? ?? 0C ?? 22 ?? ?? ?? 12 03 12 04 70 ?? ?? ?? ?? ?? 6E ?? ?? ?? ?? ?? 54 ?? ?? ?? 54");
                v5_1.add(Boolean.valueOf(com.chelpus.root.utils.runpatchads.pattern6));
                v6_1.add("ads6 Fixed!\n");
                v7_1.add("");
                v8_1.add(Boolean.valueOf(0));
                v3_1.add("12 08 54 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0C ?? 39 ?? 04 00 01 ?? 0F 05 1A ?? ?? ?? 12");
                v4_1.add("12 08 54 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0C ?? 33 00 ?? ?? 01 ?? 0F 05 1A ?? ?? ?? 12");
                v5_1.add(Boolean.valueOf(com.chelpus.root.utils.runpatchads.pattern6));
                v6_1.add("ads6 Fixed!\n");
                v7_1.add("");
                v8_1.add(Boolean.valueOf(0));
                if (com.chelpus.root.utils.runpatchads.ART) {
                    v3_1.add("13 ?? 09 00 12 ?? 12 ?? 6E ?? ?? ?? ?? ?? 0C ??");
                    v4_1.add("12 S0 00 00 12 S0 12 S0 6E ?? ?? ?? ?? ?? 0C ??");
                    v5_1.add(Boolean.valueOf(com.chelpus.root.utils.runpatchads.dependencies));
                    v6_1.add("com.android.vending dependencies removed\n");
                    v7_1.add("");
                    v8_1.add(Boolean.valueOf(1));
                    v3_1.add("12 ?? 12 ?? 13 ?? 09 00 6E ?? ?? ?? ?? ?? 0C ??");
                    v4_1.add("12 S0 12 S0 12 S0 00 00 6E ?? ?? ?? ?? ?? 0C ??");
                    v5_1.add(Boolean.valueOf(com.chelpus.root.utils.runpatchads.dependencies));
                    v6_1.add("com.android.vending dependencies removed\n");
                    v7_1.add("");
                    v8_1.add(Boolean.valueOf(1));
                } else {
                    v3_1.add("13 ?? 09 00 12 ?? 12 ?? 6E ?? ?? ?? ?? ?? 0C ??");
                    v4_1.add("12 00 0F 00 12 ?? 12 ?? 6E ?? ?? ?? ?? ?? 0C ??");
                    v5_1.add(Boolean.valueOf(com.chelpus.root.utils.runpatchads.dependencies));
                    v6_1.add("com.android.vending dependencies removed\n");
                    v7_1.add("");
                    v8_1.add(Boolean.valueOf(1));
                    v3_1.add("12 ?? 12 ?? 13 ?? 09 00 6E ?? ?? ?? ?? ?? 0C ??");
                    v4_1.add("12 ?? 12 ?? 12 00 0F 00 6E ?? ?? ?? ?? ?? 0C ??");
                    v5_1.add(Boolean.valueOf(com.chelpus.root.utils.runpatchads.dependencies));
                    v6_1.add("com.android.vending dependencies removed\n");
                    v7_1.add("");
                    v8_1.add(Boolean.valueOf(1));
                }
                com.chelpus.Utils.convertToPatchItemAuto(v2_1, v3_1, v4_1, v5_1, v6_1, v7_1, v8_1, Boolean.valueOf(0));
                if (com.chelpus.root.utils.runpatchads.fileblock) {
                    try {
                        com.chelpus.Utils.sendFromRoot(com.chelpus.root.utils.runpatchads.AdsBlockFile);
                        java.io.FileInputStream v50_0 = new java.io.FileInputStream;
                        v50_0(com.chelpus.root.utils.runpatchads.AdsBlockFile);
                        java.io.BufferedReader v52_0 = new java.io.BufferedReader;
                        v52_0(new java.io.InputStreamReader(v50_0));
                    } catch (Exception v26_1) {
                        v26_1.printStackTrace();
                        try {
                            java.io.FileInputStream v50_1 = new java.io.FileInputStream;
                            v50_1(com.chelpus.root.utils.runpatchads.AdsBlockFile.replace("AdsBlockList.txt", "AdsBlockList_user_edit.txt"));
                            java.io.BufferedReader v52_1 = new java.io.BufferedReader;
                            v52_1(new java.io.InputStreamReader(v50_1));
                        } catch (Exception v26_2) {
                            v26_2.printStackTrace();
                            if (!v53_1.isEmpty()) {
                                int v10_532 = new byte[][v53_1.size()];
                                com.chelpus.root.utils.runpatchads.sites = v10_532;
                                com.chelpus.root.utils.runpatchads.sites = ((byte[][]) ((byte[][]) v53_1.toArray(com.chelpus.root.utils.runpatchads.sites)));
                            }
                        }
                        do {
                            String v37_2 = v52_1.readLine();
                            if (v37_2 == null) {
                                v50_1.close();
                            } else {
                                String v37_3 = v37_2.trim();
                            }
                        } while(v37_3.equals(""));
                        v53_1.add(v37_3.getBytes("UTF-8"));
                    }
                    if (com.chelpus.root.utils.runpatchads.fileblock) {
                        do {
                            String v37_0 = v52_0.readLine();
                            if (v37_0 != null) {
                                String v37_1 = v37_0.trim();
                            }
                        } while(v37_1.equals(""));
                        v53_1.add(v37_1.getBytes("UTF-8"));
                    }
                    v50_0.close();
                }
                try {
                    if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.startUnderRoot.booleanValue()) {
                        if ((com.chelpus.root.utils.runpatchads.createAPK) || (com.chelpus.root.utils.runpatchads.ART)) {
                            if (com.chelpus.root.utils.runpatchads.createAPK) {
                                String v43 = p63[0];
                                com.chelpus.root.utils.runpatchads.appdir = p63[2];
                                com.chelpus.root.utils.runpatchads.sddir = p63[5];
                                com.chelpus.root.utils.runpatchads.clearTempSD();
                                java.io.File v15_1 = new java.io.File(com.chelpus.root.utils.runpatchads.appdir);
                                com.chelpus.root.utils.runpatchads.unzipSD(v15_1);
                                com.chelpus.root.utils.runpatchads.crkapk = new java.io.File(new StringBuilder().append(com.chelpus.root.utils.runpatchads.sddir).append("/Modified/").append(v43).append(".apk").toString());
                                com.chelpus.Utils.copyFile(v15_1, com.chelpus.root.utils.runpatchads.crkapk);
                                if ((com.chelpus.root.utils.runpatchads.classesFiles != null) && (com.chelpus.root.utils.runpatchads.classesFiles.size() != 0)) {
                                    com.chelpus.root.utils.runpatchads.filestopatch.clear();
                                    int v10_558 = com.chelpus.root.utils.runpatchads.classesFiles.iterator();
                                    while (v10_558.hasNext()) {
                                        java.io.File v17_3 = ((java.io.File) v10_558.next());
                                        if (v17_3.exists()) {
                                            com.chelpus.root.utils.runpatchads.filestopatch.add(v17_3);
                                        } else {
                                            throw new java.io.FileNotFoundException();
                                        }
                                    }
                                } else {
                                    throw new java.io.FileNotFoundException();
                                }
                            }
                            if (com.chelpus.root.utils.runpatchads.ART) {
                                com.chelpus.Utils.sendFromRoot("ART mode create dex enabled.");
                                com.chelpus.root.utils.runpatchads.appdir = p63[2];
                                com.chelpus.root.utils.runpatchads.sddir = p63[3];
                                com.chelpus.root.utils.runpatchads.clearTempSD();
                                com.chelpus.root.utils.runpatchads.unzipART(new java.io.File(com.chelpus.root.utils.runpatchads.appdir));
                                if ((com.chelpus.root.utils.runpatchads.classesFiles != null) && (com.chelpus.root.utils.runpatchads.classesFiles.size() != 0)) {
                                    com.chelpus.root.utils.runpatchads.filestopatch.clear();
                                    int v10_572 = com.chelpus.root.utils.runpatchads.classesFiles.iterator();
                                    while (v10_572.hasNext()) {
                                        java.io.File v17_1 = ((java.io.File) v10_572.next());
                                        if (v17_1.exists()) {
                                            com.chelpus.root.utils.runpatchads.filestopatch.add(v17_1);
                                        } else {
                                            throw new java.io.FileNotFoundException();
                                        }
                                    }
                                } else {
                                    throw new java.io.FileNotFoundException();
                                }
                            }
                        } else {
                            com.chelpus.root.utils.runpatchads.dir = p63[3];
                            com.chelpus.root.utils.runpatchads.dirapp = p63[2];
                            com.chelpus.root.utils.runpatchads.clearTemp();
                            if (p63[4].equals("not_system")) {
                                com.chelpus.root.utils.runpatchads.system = 0;
                            }
                            if (p63[4].equals("system")) {
                                com.chelpus.root.utils.runpatchads.system = 1;
                            }
                            com.chelpus.root.utils.runpatchads.filestopatch.clear();
                            com.chelpus.Utils.sendFromRoot("CLASSES mode create odex enabled.");
                            com.chelpus.root.utils.runpatchads.appdir = p63[2];
                            com.chelpus.root.utils.runpatchads.sddir = p63[3];
                            com.chelpus.root.utils.runpatchads.clearTempSD();
                            java.io.File v15_5 = new java.io.File(com.chelpus.root.utils.runpatchads.appdir);
                            com.chelpus.Utils.sendFromRoot("Get classes.dex.");
                            com.chelpus.root.utils.runpatchads.print.println("Get classes.dex.");
                            com.chelpus.root.utils.runpatchads.unzipART(v15_5);
                            if ((com.chelpus.root.utils.runpatchads.classesFiles != null) && (com.chelpus.root.utils.runpatchads.classesFiles.size() != 0)) {
                                com.chelpus.root.utils.runpatchads.filestopatch.clear();
                                int v10_608 = com.chelpus.root.utils.runpatchads.classesFiles.iterator();
                                while (v10_608.hasNext()) {
                                    java.io.File v17_5 = ((java.io.File) v10_608.next());
                                    if (v17_5.exists()) {
                                        com.chelpus.root.utils.runpatchads.filestopatch.add(v17_5);
                                    } else {
                                        throw new java.io.FileNotFoundException();
                                    }
                                }
                                String v40 = com.chelpus.Utils.getPlaceForOdex(p63[2], 1);
                                java.io.File v39_1 = new java.io.File(v40);
                                if (v39_1.exists()) {
                                    v39_1.delete();
                                }
                                java.io.File v39_2 = new java.io.File;
                                v39_2(v40.replace("-1", "-2"));
                                if (v39_2.exists()) {
                                    v39_2.delete();
                                }
                                java.io.File v39_3 = new java.io.File;
                                v39_3(v40.replace("-2", "-1"));
                                if (v39_3.exists()) {
                                    v39_3.delete();
                                }
                            } else {
                                throw new java.io.FileNotFoundException();
                            }
                        }
                        String v60_0 = com.chelpus.root.utils.runpatchads.filestopatch.iterator();
                        while (v60_0.hasNext()) {
                            java.io.File v32_1 = ((java.io.File) v60_0.next());
                            com.chelpus.Utils.sendFromRoot("Find string id.");
                            java.util.ArrayList v56_1 = new java.util.ArrayList();
                            v56_1.add("phone");
                            v56_1.add("Landroid/net/ConnectivityManager;");
                            v56_1.add("getActiveNetworkInfo");
                            com.chelpus.Utils.sendFromRoot("String analysis.");
                            com.chelpus.root.utils.runpatchads.print.println("String analysis.");
                            java.util.ArrayList v42 = com.chelpus.Utils.getStringIds(v32_1.getAbsolutePath(), v56_1, 0);
                            int v27 = 0;
                            java.util.ArrayList v18_1 = new java.util.ArrayList();
                            v18_1.add(new com.android.vending.billing.InAppBillingService.LUCK.CommandItem("Landroid/net/ConnectivityManager;", "getActiveNetworkInfo"));
                            int v11_45 = v42.iterator();
                            while (v11_45.hasNext()) {
                                com.android.vending.billing.InAppBillingService.LUCK.StringItem v35_1 = ((com.android.vending.billing.InAppBillingService.LUCK.StringItem) v11_45.next());
                                int v10_803 = v18_1.iterator();
                                while (v10_803.hasNext()) {
                                    com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto v36_5 = ((com.android.vending.billing.InAppBillingService.LUCK.CommandItem) v10_803.next());
                                    if (v36_5.object.equals(v35_1.str)) {
                                        v36_5.Object = v35_1.offset;
                                    }
                                    if (v36_5.method.equals(v35_1.str)) {
                                        v36_5.Method = v35_1.offset;
                                    }
                                }
                                if (v35_1.str.equals("phone")) {
                                    if (v35_1.bits32) {
                                        ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(0)).pattern = 0;
                                        ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(1)).origByte[2] = v35_1.offset[0];
                                        ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(1)).origByte[3] = v35_1.offset[1];
                                        ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(1)).origByte[4] = v35_1.offset[2];
                                        ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(1)).origByte[5] = v35_1.offset[3];
                                    } else {
                                        ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(0)).origByte[2] = v35_1.offset[0];
                                        ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(0)).origByte[3] = v35_1.offset[1];
                                        ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(1)).pattern = 0;
                                    }
                                    v27 = 1;
                                }
                            }
                            if ((v27 != 0) || (com.chelpus.root.utils.runpatchads.full_offline)) {
                                com.chelpus.Utils.sendFromRoot("Parse data for patch.");
                                com.chelpus.root.utils.runpatchads.print.println("Parse data for patch.");
                                com.chelpus.Utils.getMethodsIds(v32_1.getAbsolutePath(), v18_1, 0);
                                System.out.println(Integer.toHexString(((com.android.vending.billing.InAppBillingService.LUCK.CommandItem) v18_1.get(0)).index_command[0]));
                                System.out.println(Integer.toHexString(((com.android.vending.billing.InAppBillingService.LUCK.CommandItem) v18_1.get(0)).index_command[1]));
                                int v11_50 = v18_1.iterator();
                                while (v11_50.hasNext()) {
                                    com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto v36_3 = ((com.android.vending.billing.InAppBillingService.LUCK.CommandItem) v11_50.next());
                                    if (v36_3.object.equals("Landroid/net/ConnectivityManager;")) {
                                        if (!v36_3.found_index_command) {
                                            ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(2)).pattern = 0;
                                            ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(3)).pattern = 0;
                                        } else {
                                            System.out.println("save to command");
                                            ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(2)).origByte[2] = v36_3.index_command[0];
                                            ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(2)).origByte[3] = v36_3.index_command[1];
                                            ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(3)).origByte[2] = v36_3.index_command[0];
                                            ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(3)).origByte[3] = v36_3.index_command[1];
                                        }
                                    }
                                }
                                if (v27 == 0) {
                                    ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(0)).pattern = 0;
                                    ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(1)).pattern = 0;
                                    ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(2)).pattern = 0;
                                }
                            }
                            com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto v0_27 = new com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto[v2_1.size()];
                            com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto[] v44 = v0_27;
                            int v59 = 0;
                            int v10_657 = v2_1.iterator();
                            while (v10_657.hasNext()) {
                                v44[v59] = ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v10_657.next());
                                v59++;
                            }
                            int v20 = 0;
                            if (com.chelpus.root.utils.runpatchads.fileblock) {
                                com.chelpus.Utils.sendFromRoot("String analysis.");
                                com.chelpus.root.utils.runpatchads.print.println("String analysis.");
                                v20 = com.chelpus.Utils.setStringIds(v32_1.getAbsolutePath(), com.chelpus.root.utils.runpatchads.sites, 0, 64);
                            }
                            while (v20 > 0) {
                                v20--;
                                com.chelpus.Utils.sendFromRoot("Site from AdsBlockList blocked!");
                                com.chelpus.root.utils.runpatchads.print.println("Site from AdsBlockList blocked!");
                            }
                            int v10_664 = new StringBuilder().append("Relaced strings:");
                            int v11_58 = v32_1.getAbsolutePath();
                            String v12_16 = new String[1];
                            v12_16[0] = "com.google.android.gms.ads.identifier.service.START";
                            String[] v14_2 = new String[1];
                            v14_2[0] = "com.google.android.gms.ads.identifier.service.STAPT";
                            com.chelpus.Utils.sendFromRoot(v10_664.append(com.chelpus.Utils.replaceStringIds(v11_58, v12_16, 0, v14_2)).toString());
                            long v57 = System.currentTimeMillis();
                            java.nio.channels.FileChannel v9_1 = new java.io.RandomAccessFile(v32_1, "rw").getChannel();
                            com.chelpus.Utils.sendFromRoot(new StringBuilder().append("Size file:").append(v9_1.size()).toString());
                            java.nio.MappedByteBuffer v31 = v9_1.map(java.nio.channels.FileChannel$MapMode.READ_WRITE, 0, ((long) ((int) v9_1.size())));
                            int v54 = 0;
                            int v46 = 0;
                            try {
                                while (v31.hasRemaining()) {
                                    if ((!com.chelpus.root.utils.runpatchads.createAPK) && ((v31.position() - v46) > 149999)) {
                                        com.chelpus.Utils.sendFromRoot(new StringBuilder().append("Progress size:").append(v31.position()).toString());
                                        v46 = v31.position();
                                    }
                                    int v22 = v31.position();
                                    byte v21 = v31.get();
                                    int v16 = 0;
                                    while (v16 < v44.length) {
                                        com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto v45 = v44[v16];
                                        v31.position(v22);
                                        if ((v45.markerTrig) && (v16 == 2)) {
                                            v54++;
                                            if (v54 >= 574) {
                                                v45.markerTrig = 0;
                                                int v10_689 = v2_1.iterator();
                                                while (v10_689.hasNext()) {
                                                    com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto v23_1 = ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v10_689.next());
                                                    if (v23_1.marker.equals(v45.marker)) {
                                                        v23_1.markerTrig = 0;
                                                    }
                                                }
                                                v54 = 0;
                                                v31.position((v22 + 1));
                                            } else {
                                                if (v21 == v45.origByte[0]) {
                                                    if (v45.repMask[0] == 0) {
                                                        v45.repByte[0] = v21;
                                                    }
                                                    int v34_0 = 1;
                                                    v31.position((v22 + 1));
                                                    byte v49_0 = v31.get();
                                                    while ((v49_0 == v45.origByte[v34_0]) || ((v45.origMask[v34_0] == 1) || ((v45.origMask[v34_0] == 20) || ((v45.origMask[v34_0] == 21) || (v45.origMask[v34_0] == 23))))) {
                                                        if (v45.repMask[v34_0] == 0) {
                                                            v45.repByte[v34_0] = v49_0;
                                                        }
                                                        if (v45.repMask[v34_0] == 20) {
                                                            v45.repByte[v34_0] = ((byte) (v49_0 & 15));
                                                        }
                                                        if (v45.repMask[v34_0] == 21) {
                                                            v45.repByte[v34_0] = ((byte) ((v49_0 & 15) + 16));
                                                        }
                                                        v34_0++;
                                                        if (v34_0 != v45.origByte.length) {
                                                            v49_0 = v31.get();
                                                        } else {
                                                            v31.position(v22);
                                                            v31.put(v45.repByte);
                                                            v31.force();
                                                            com.chelpus.Utils.sendFromRoot(v45.resultText);
                                                            com.chelpus.root.utils.runpatchads.print.println(v45.resultText);
                                                            v45.result = 1;
                                                            v45.markerTrig = 0;
                                                            int v10_723 = v2_1.iterator();
                                                            while (v10_723.hasNext()) {
                                                                com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto v23_5 = ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v10_723.next());
                                                                if (v23_5.marker.equals(v45.marker)) {
                                                                    v23_5.markerTrig = 0;
                                                                }
                                                            }
                                                            v54 = 0;
                                                            v31.position((v22 + 1));
                                                            break;
                                                        }
                                                    }
                                                }
                                                v31.position((v22 + 1));
                                            }
                                        }
                                        if ((!v45.markerTrig) && ((v21 == v45.origByte[0]) && (v45.pattern))) {
                                            if (v45.repMask[0] == 0) {
                                                v45.repByte[0] = v21;
                                            }
                                            int v34_1 = 1;
                                            v31.position((v22 + 1));
                                            byte v49_1 = v31.get();
                                            while ((v49_1 == v45.origByte[v34_1]) || ((v45.origMask[v34_1] == 1) || ((v45.origMask[v34_1] == 20) || ((v45.origMask[v34_1] == 21) || (v45.origMask[v34_1] == 23))))) {
                                                if (v45.repMask[v34_1] == 0) {
                                                    v45.repByte[v34_1] = v49_1;
                                                }
                                                if (v45.repMask[v34_1] == 20) {
                                                    v45.repByte[v34_1] = ((byte) (v49_1 & 15));
                                                }
                                                if (v45.repMask[v34_1] == 21) {
                                                    v45.repByte[v34_1] = ((byte) ((v49_1 & 15) + 16));
                                                }
                                                v34_1++;
                                                if (v34_1 != v45.origByte.length) {
                                                    v49_1 = v31.get();
                                                } else {
                                                    v31.position(v22);
                                                    v31.put(v45.repByte);
                                                    v31.force();
                                                    com.chelpus.Utils.sendFromRoot(v45.resultText);
                                                    com.chelpus.root.utils.runpatchads.print.println(v45.resultText);
                                                    v45.result = 1;
                                                    if (v45.marker.equals("")) {
                                                        break;
                                                    }
                                                    v45.markerTrig = 1;
                                                    int v10_762 = v2_1.iterator();
                                                    while (v10_762.hasNext()) {
                                                        com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto v23_3 = ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v10_762.next());
                                                        if (v23_3.marker.equals(v45.marker)) {
                                                            v23_3.markerTrig = 1;
                                                        }
                                                    }
                                                }
                                            }
                                            v31.position((v22 + 1));
                                        }
                                        v16++;
                                    }
                                    if (0 == 0) {
                                        v31.position((v22 + 1));
                                    }
                                }
                            } catch (Exception v26_3) {
                                com.chelpus.Utils.sendFromRoot(new StringBuilder().append("").append(v26_3).toString());
                            }
                            v9_1.close();
                            com.chelpus.Utils.sendFromRoot(new StringBuilder().append("").append(((System.currentTimeMillis() - v57) / 1000)).toString());
                            com.chelpus.Utils.sendFromRoot("Analise Results:");
                        }
                    } else {
                        com.chelpus.Utils.remount(p63[2], "RW");
                    }
                } catch (java.io.FileNotFoundException v38) {
                    com.chelpus.Utils.sendFromRoot("Error: Program files are not found!\n\nCheck the location dalvik-cache to solve problems!\n\nDefault: /data/dalvik-cache/*");
                } catch (Exception v26_4) {
                    com.chelpus.Utils.sendFromRoot(new StringBuilder().append("Patch Process Exception: ").append(v26_4.toString()).toString());
                }
                int v10_848 = com.chelpus.root.utils.runpatchads.filestopatch.iterator();
                while (v10_848.hasNext()) {
                    com.chelpus.Utils.fixadler(((java.io.File) v10_848.next()));
                    com.chelpus.root.utils.runpatchads.clearTempSD();
                }
                if (!com.chelpus.root.utils.runpatchads.createAPK) {
                    int v51 = com.chelpus.Utils.create_ODEX_root(p63[3], com.chelpus.root.utils.runpatchads.classesFiles, p63[2], com.chelpus.root.utils.runpatchads.uid, com.chelpus.Utils.getOdexForCreate(p63[2], com.chelpus.root.utils.runpatchads.uid));
                    com.chelpus.Utils.sendFromRoot(new StringBuilder().append("chelpus_return_").append(v51).toString());
                    if ((v51 == 0) && (!com.chelpus.root.utils.runpatchads.ART)) {
                        com.chelpus.Utils.afterPatch(p63[1], p63[2], com.chelpus.Utils.getPlaceForOdex(p63[2], 1), com.chelpus.root.utils.runpatchads.uid, p63[3]);
                    }
                }
                if (!com.chelpus.root.utils.runpatchads.createAPK) {
                    com.chelpus.Utils.exitFromRootJava();
                }
                com.chelpus.root.utils.runpatchads.result = v47.allresult;
                return;
            } catch (int v10) {
            } catch (int v10) {
            }
        }
        while (v10_22 < v11_3) {
            java.io.File v30 = v33[v10_22];
            if ((v30.isFile()) && ((!v30.getName().equals("busybox")) && ((!v30.getName().equals("reboot")) && (!v30.getName().equals("dalvikvm"))))) {
                v30.delete();
            }
            v10_22++;
        }
    }

    public static void unzipART(java.io.File p22)
    {
        int v8 = 0;
        try {
            java.io.FileInputStream v6_1 = new java.io.FileInputStream(p22);
            java.util.zip.ZipInputStream v16 = new java.util.zip.ZipInputStream;
            v16(v6_1);
            java.util.zip.ZipEntry v15 = v16.getNextEntry();
        } catch (Exception v4) {
            try {
                net.lingala.zip4j.core.ZipFile v17 = new net.lingala.zip4j.core.ZipFile;
                v17(p22);
                v17.extractFile("classes.dex", com.chelpus.root.utils.runpatchads.sddir);
                com.chelpus.root.utils.runpatchads.classesFiles.add(new java.io.File(new StringBuilder().append(com.chelpus.root.utils.runpatchads.sddir).append("/").append("classes.dex").toString()));
                StringBuilder v0_23 = new String[3];
                String[] v18_33 = v0_23;
                v18_33[0] = "chmod";
                v18_33[1] = "777";
                v18_33[2] = new StringBuilder().append(com.chelpus.root.utils.runpatchads.sddir).append("/").append("classes.dex").toString();
                com.chelpus.Utils.cmdParam(v18_33);
                v17.extractFile("AndroidManifest.xml", com.chelpus.root.utils.runpatchads.sddir);
                StringBuilder v0_25 = new String[3];
                String[] v18_36 = v0_25;
                v18_36[0] = "chmod";
                v18_36[1] = "777";
                v18_36[2] = new StringBuilder().append(com.chelpus.root.utils.runpatchads.sddir).append("/").append("AndroidManifest.xml").toString();
                com.chelpus.Utils.cmdParam(v18_36);
            } catch (Exception v5_0) {
                com.chelpus.Utils.sendFromRoot(new StringBuilder().append("Error classes.dex decompress! ").append(v5_0).toString());
                com.chelpus.Utils.sendFromRoot(new StringBuilder().append("Exception e1").append(v4.toString()).toString());
            } catch (Exception v5_1) {
                com.chelpus.Utils.sendFromRoot(new StringBuilder().append("Error classes.dex decompress! ").append(v5_1).toString());
                com.chelpus.Utils.sendFromRoot(new StringBuilder().append("Exception e1").append(v4.toString()).toString());
            }
            com.chelpus.Utils.sendFromRoot(new StringBuilder().append("Exception e").append(v4.toString()).toString());
            return;
        }
        while ((v15 != null) && (1 != 0)) {
            String v11 = v15.getName();
            if ((v11.toLowerCase().startsWith("classes")) && ((v11.endsWith(".dex")) && (!v11.contains("/")))) {
                java.io.FileOutputStream v9_1 = new java.io.FileOutputStream(new StringBuilder().append(com.chelpus.root.utils.runpatchads.sddir).append("/").append(v11).toString());
                byte[] v2 = new byte[2048];
                while(true) {
                    int v12 = v16.read(v2);
                    if (v12 == -1) {
                        break;
                    }
                    v9_1.write(v2, 0, v12);
                }
                v16.closeEntry();
                v9_1.close();
                com.chelpus.root.utils.runpatchads.classesFiles.add(new java.io.File(new StringBuilder().append(com.chelpus.root.utils.runpatchads.sddir).append("/").append(v11).toString()));
                StringBuilder v0_11 = new String[3];
                String[] v18_16 = v0_11;
                v18_16[0] = "chmod";
                v18_16[1] = "777";
                v18_16[2] = new StringBuilder().append(com.chelpus.root.utils.runpatchads.sddir).append("/").append(v11).toString();
                com.chelpus.Utils.cmdParam(v18_16);
            }
            if (v11.equals("AndroidManifest.xml")) {
                java.io.FileOutputStream v10_1 = new java.io.FileOutputStream(new StringBuilder().append(com.chelpus.root.utils.runpatchads.sddir).append("/").append("AndroidManifest.xml").toString());
                byte[] v3 = new byte[2048];
                while(true) {
                    int v13 = v16.read(v3);
                    if (v13 == -1) {
                        break;
                    }
                    v10_1.write(v3, 0, v13);
                }
                v16.closeEntry();
                v10_1.close();
                StringBuilder v0_19 = new String[3];
                String[] v18_28 = v0_19;
                v18_28[0] = "chmod";
                v18_28[1] = "777";
                v18_28[2] = new StringBuilder().append(com.chelpus.root.utils.runpatchads.sddir).append("/").append("AndroidManifest.xml").toString();
                com.chelpus.Utils.cmdParam(v18_28);
                v8 = 1;
            }
            if ((0 == 0) || (v8 == 0)) {
                v15 = v16.getNextEntry();
            } else {
                break;
            }
        }
        v16.close();
        v6_1.close();
        return;
    }

    public static void unzipSD(java.io.File p13)
    {
        try {
            java.io.FileInputStream v3_1 = new java.io.FileInputStream(p13);
            java.util.zip.ZipInputStream v7_1 = new java.util.zip.ZipInputStream(v3_1);
        } catch (Exception v1) {
            try {
                new net.lingala.zip4j.core.ZipFile(p13).extractFile("classes.dex", new StringBuilder().append(com.chelpus.root.utils.runpatchads.sddir).append("/Modified/").toString());
                com.chelpus.root.utils.runpatchads.classesFiles.add(new java.io.File(new StringBuilder().append(com.chelpus.root.utils.runpatchads.sddir).append("/Modified/").append("classes.dex").toString()));
            } catch (Exception v2_1) {
                com.chelpus.Utils.sendFromRoot(new StringBuilder().append("Error classes.dex decompress! ").append(v2_1).toString());
                com.chelpus.Utils.sendFromRoot(new StringBuilder().append("Exception e1").append(v1.toString()).toString());
            } catch (Exception v2_0) {
                com.chelpus.Utils.sendFromRoot(new StringBuilder().append("Error classes.dex decompress! ").append(v2_0).toString());
                com.chelpus.Utils.sendFromRoot(new StringBuilder().append("Exception e1").append(v1.toString()).toString());
            }
            return;
        }
        while(true) {
            java.util.zip.ZipEntry v6 = v7_1.getNextEntry();
            if (v6 == null) {
                v7_1.close();
                v3_1.close();
                return;
            } else {
                if ((v6.getName().toLowerCase().startsWith("classes")) && ((v6.getName().endsWith(".dex")) && (!v6.getName().contains("/")))) {
                    java.io.FileOutputStream v4_1 = new java.io.FileOutputStream(new StringBuilder().append(com.chelpus.root.utils.runpatchads.sddir).append("/Modified/").append(v6.getName()).toString());
                    byte[] v0 = new byte[1024];
                    while(true) {
                        int v5 = v7_1.read(v0);
                        if (v5 == -1) {
                            break;
                        }
                    }
                    v7_1.closeEntry();
                    v4_1.close();
                    com.chelpus.root.utils.runpatchads.classesFiles.add(new java.io.File(new StringBuilder().append(com.chelpus.root.utils.runpatchads.sddir).append("/Modified/").append(v6.getName()).toString()));
                }
            }
        }
        v4_1.write(v0, 0, v5);
    }
}
