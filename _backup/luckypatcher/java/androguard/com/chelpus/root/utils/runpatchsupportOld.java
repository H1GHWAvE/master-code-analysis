package com.chelpus.root.utils;
public class runpatchsupportOld {
    private static boolean ART;
    public static String appdir;
    public static java.util.ArrayList classesFiles;
    private static boolean copyDC;
    private static boolean createAPK;
    public static java.io.File crkapk;
    public static String dir;
    public static String dir2;
    public static String dirapp;
    public static java.util.ArrayList filestopatch;
    private static boolean pattern1;
    private static boolean pattern2;
    private static boolean pattern3;
    public static java.io.PrintStream print;
    public static String result;
    public static String sddir;
    public static boolean system;
    public static String uid;

    static runpatchsupportOld()
    {
        com.chelpus.root.utils.runpatchsupportOld.createAPK = 0;
        com.chelpus.root.utils.runpatchsupportOld.pattern1 = 1;
        com.chelpus.root.utils.runpatchsupportOld.pattern2 = 1;
        com.chelpus.root.utils.runpatchsupportOld.pattern3 = 1;
        com.chelpus.root.utils.runpatchsupportOld.copyDC = 0;
        com.chelpus.root.utils.runpatchsupportOld.ART = 0;
        com.chelpus.root.utils.runpatchsupportOld.dirapp = "/data/app/";
        com.chelpus.root.utils.runpatchsupportOld.system = 0;
        com.chelpus.root.utils.runpatchsupportOld.uid = "";
        com.chelpus.root.utils.runpatchsupportOld.dir = "/sdcard/";
        com.chelpus.root.utils.runpatchsupportOld.dir2 = "/sdcard/";
        com.chelpus.root.utils.runpatchsupportOld.filestopatch = 0;
        com.chelpus.root.utils.runpatchsupportOld.sddir = "/sdcard/";
        com.chelpus.root.utils.runpatchsupportOld.appdir = "/sdcard/";
        com.chelpus.root.utils.runpatchsupportOld.classesFiles = new java.util.ArrayList();
        return;
    }

    public runpatchsupportOld()
    {
        return;
    }

    public static boolean byteverify(java.nio.MappedByteBuffer p5, int p6, byte p7, byte[] p8, byte[] p9, byte[] p10, byte[] p11, String p12, boolean p13)
    {
        int v2_0 = 1;
        if ((p7 != p8[0]) || (!p13)) {
            v2_0 = 0;
        } else {
            if (p11[0] == 0) {
                p10[0] = p7;
            }
            int v0 = 1;
            p5.position((p6 + 1));
            byte v1 = p5.get();
            while ((v1 == p8[v0]) || (p9[v0] == 1)) {
                if (p11[v0] == 0) {
                    p10[v0] = v1;
                }
                v0++;
                if (v0 != p8.length) {
                    v1 = p5.get();
                } else {
                    p5.position(p6);
                    p5.put(p10);
                    p5.force();
                    com.chelpus.Utils.sendFromRoot(p12);
                }
            }
            p5.position((p6 + 1));
        }
        return v2_0;
    }

    private static final void calcChecksum(byte[] p5, int p6)
    {
        java.util.zip.Adler32 v1_1 = new java.util.zip.Adler32();
        v1_1.update(p5, 12, (p5.length - (p6 + 12)));
        int v0 = ((int) v1_1.getValue());
        p5[(p6 + 8)] = ((byte) v0);
        p5[(p6 + 9)] = ((byte) (v0 >> 8));
        p5[(p6 + 10)] = ((byte) (v0 >> 16));
        p5[(p6 + 11)] = ((byte) (v0 >> 24));
        return;
    }

    private static final void calcSignature(byte[] p8, int p9)
    {
        try {
            java.security.MessageDigest v2 = java.security.MessageDigest.getInstance("SHA-1");
            v2.update(p8, 32, (p8.length - (p9 + 32)));
        } catch (java.security.NoSuchAlgorithmException v3) {
            throw new RuntimeException(v3);
        }
        try {
            int v0 = v2.digest(p8, (p9 + 12), 20);
        } catch (java.security.DigestException v1) {
            throw new RuntimeException(v1);
        }
        if (v0 == 20) {
            return;
        } else {
            throw new RuntimeException(new StringBuilder().append("unexpected digest write:").append(v0).append("bytes").toString());
        }
    }

    public static void clearTemp()
    {
        try {
            java.io.File v2_1 = new java.io.File(new StringBuilder().append(com.chelpus.root.utils.runpatchsupportOld.dir).append("/AndroidManifest.xml").toString());
        } catch (Exception v1) {
            com.chelpus.Utils.sendFromRoot(new StringBuilder().append("").append(v1.toString()).toString());
            return;
        }
        if (v2_1.exists()) {
            v2_1.delete();
        }
        if ((com.chelpus.root.utils.runpatchsupportOld.classesFiles != null) && (com.chelpus.root.utils.runpatchsupportOld.classesFiles.size() > 0)) {
            boolean v4_9 = com.chelpus.root.utils.runpatchsupportOld.classesFiles.iterator();
            while (v4_9.hasNext()) {
                java.io.File v0_1 = ((java.io.File) v4_9.next());
                if (v0_1.exists()) {
                    v0_1.delete();
                }
            }
        }
        java.io.File v2_3 = new java.io.File(new StringBuilder().append(com.chelpus.root.utils.runpatchsupportOld.dir).append("/classes.dex").toString());
        if (v2_3.exists()) {
            v2_3.delete();
        }
        java.io.File v2_5 = new java.io.File(new StringBuilder().append(com.chelpus.root.utils.runpatchsupportOld.dir).append("/classes.dex.apk").toString());
        if (!v2_5.exists()) {
            return;
        } else {
            v2_5.delete();
            return;
        }
    }

    public static void clearTempSD()
    {
        try {
            java.io.File v1_1 = new java.io.File(new StringBuilder().append(com.chelpus.root.utils.runpatchsupportOld.sddir).append("/Modified/classes.dex.apk").toString());
        } catch (Exception v0) {
            com.chelpus.Utils.sendFromRoot(new StringBuilder().append("").append(v0.toString()).toString());
            return;
        }
        if (!v1_1.exists()) {
            return;
        } else {
            v1_1.delete();
            return;
        }
    }

    public static void fixadler(java.io.File p5)
    {
        try {
            java.io.FileInputStream v2_1 = new java.io.FileInputStream(p5);
            byte[] v0 = new byte[v2_1.available()];
            v2_1.read(v0);
            com.chelpus.root.utils.runpatchsupportOld.calcSignature(v0, 0);
            com.chelpus.root.utils.runpatchsupportOld.calcChecksum(v0, 0);
            v2_1.close();
            java.io.FileOutputStream v3_1 = new java.io.FileOutputStream(p5);
            v3_1.write(v0);
            v3_1.close();
        } catch (Exception v1) {
            v1.printStackTrace();
        }
        return;
    }

    public static void main(String[] p69)
    {
        com.android.vending.billing.InAppBillingService.LUCK.LogOutputStream v54 = new com.android.vending.billing.InAppBillingService.LUCK.LogOutputStream;
        v54("System.out");
        com.chelpus.root.utils.runpatchsupportOld.print = new java.io.PrintStream(v54);
        com.chelpus.Utils.startRootJava(new com.chelpus.root.utils.runpatchsupportOld$1());
        com.chelpus.Utils.kill(p69[0]);
        com.chelpus.root.utils.runpatchsupportOld.print.println("Support-Code Running!");
        java.util.ArrayList v2_1 = new java.util.ArrayList();
        com.chelpus.root.utils.runpatchsupportOld.pattern1 = 1;
        com.chelpus.root.utils.runpatchsupportOld.pattern2 = 1;
        com.chelpus.root.utils.runpatchsupportOld.pattern3 = 1;
        com.chelpus.root.utils.runpatchsupportOld.filestopatch = new java.util.ArrayList();
        try {
            java.io.File[] v36 = new java.io.File(p69[3]).listFiles();
            int v11_3 = v36.length;
            int v10_15 = 0;
        } catch (Exception v28_0) {
            v28_0.printStackTrace();
            try {
                if (p69[1].contains("pattern0")) {
                    if (!p69[1].contains("pattern1")) {
                        com.chelpus.root.utils.runpatchsupportOld.pattern2 = 0;
                    }
                    if (!p69[1].contains("pattern2")) {
                        com.chelpus.root.utils.runpatchsupportOld.pattern3 = 0;
                    }
                    if ((p69[6] != null) && (p69[6].contains("createAPK"))) {
                        com.chelpus.root.utils.runpatchsupportOld.createAPK = 1;
                    }
                    if ((p69[6] != null) && (p69[6].contains("ART"))) {
                        com.chelpus.root.utils.runpatchsupportOld.ART = 1;
                    }
                    if (p69[6] != null) {
                        com.chelpus.Utils.sendFromRoot(p69[6]);
                    }
                    if (p69[7] != null) {
                        com.chelpus.root.utils.runpatchsupportOld.uid = p69[7];
                    }
                    System.out.println(new StringBuilder().append("uid:").append(com.chelpus.root.utils.runpatchsupportOld.uid).toString());
                } else {
                    com.chelpus.root.utils.runpatchsupportOld.pattern1 = 0;
                }
                try {
                    if (p69[5].contains("copyDC")) {
                        com.chelpus.root.utils.runpatchsupportOld.copyDC = 1;
                    }
                } catch (int v10) {
                } catch (int v10) {
                }
                if (com.chelpus.root.utils.runpatchsupportOld.createAPK) {
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.startUnderRoot = Boolean.valueOf(0);
                }
                java.util.ArrayList v3_1 = new java.util.ArrayList();
                java.util.ArrayList v4_1 = new java.util.ArrayList();
                java.util.ArrayList v5_1 = new java.util.ArrayList();
                java.util.ArrayList v6_1 = new java.util.ArrayList();
                java.util.ArrayList v7_1 = new java.util.ArrayList();
                java.util.ArrayList v8_1 = new java.util.ArrayList();
                v3_1.add("1A ?? FF FF");
                v4_1.add("1A ?? ?? ??");
                v5_1.add(Boolean.valueOf(1));
                v6_1.add("(pak intekekt 0)");
                v7_1.add("search_pack");
                v8_1.add(Boolean.valueOf(0));
                v3_1.add("1B ?? FF FF FF FF");
                v4_1.add("1B ?? ?? ?? ?? ??");
                v5_1.add(Boolean.valueOf(1));
                v6_1.add("(pak intekekt 0)");
                v7_1.add("search_pack");
                v8_1.add(Boolean.valueOf(0));
                v3_1.add("1A ?? FF FF");
                v4_1.add("1A ?? ?? ??");
                v5_1.add(Boolean.valueOf(1));
                v6_1.add("(sha intekekt 2)");
                v7_1.add("search");
                v8_1.add(Boolean.valueOf(0));
                v3_1.add("1B ?? FF FF FF FF");
                v4_1.add("1B ?? ?? ?? ?? ??");
                v5_1.add(Boolean.valueOf(1));
                v6_1.add("(sha intekekt 2 32 bit)");
                v7_1.add("search");
                v8_1.add(Boolean.valueOf(0));
                v3_1.add("0A ?? 39 ?? ?? 00");
                v4_1.add("12 S1 39 ?? ?? 00");
                v5_1.add(Boolean.valueOf(0));
                v6_1.add("support2 Fixed!\n(sha intekekt 3)");
                v7_1.add("search");
                v8_1.add(Boolean.valueOf(0));
                v3_1.add("1A ?? FF FF 6E 20 ?? ?? ?? ?? 0C ??");
                v4_1.add("1A ?? ?? ?? 00 00 00 00 00 00 00 00");
                v5_1.add(Boolean.valueOf(0));
                v6_1.add("support1 Fixed!\n(pak intekekt)");
                v7_1.add("search_pack");
                v8_1.add(Boolean.valueOf(0));
                v3_1.add("1A ?? FF FF 6E 20 ?? ?? ?? ??");
                v4_1.add("1A ?? ?? ?? 00 00 00 00 00 00");
                v5_1.add(Boolean.valueOf(0));
                v6_1.add("support1 Fixed!\n(pak intekekt)");
                v7_1.add("search_pack");
                v8_1.add(Boolean.valueOf(0));
                v3_1.add("1A ?? FF FF 6E 20 ?? ?? ?? ??");
                v4_1.add("1A ?? ?? ?? 00 00 00 00 00 00");
                v5_1.add(Boolean.valueOf(0));
                v6_1.add("support1 Fixed!\n(pak intekekt)");
                v7_1.add("search_pack");
                v8_1.add(Boolean.valueOf(1));
                v3_1.add("1B ?? FF FF FF FF 6E 20 ?? ?? ?? ?? 0C ??");
                v4_1.add("1B ?? ?? ?? ?? ?? 00 00 00 00 00 00 00 00");
                v5_1.add(Boolean.valueOf(0));
                v6_1.add("support1 Fixed!\n(pak intekekt 32 bit)");
                v7_1.add("search_pack");
                v8_1.add(Boolean.valueOf(0));
                v3_1.add("1B ?? FF FF FF FF 6E 20 ?? ?? ?? ??");
                v4_1.add("1B ?? ?? ?? ?? ?? 00 00 00 00 00 00");
                v5_1.add(Boolean.valueOf(0));
                v6_1.add("support1 Fixed!\n(pak intekekt 32 bit)");
                v7_1.add("search_pack");
                v8_1.add(Boolean.valueOf(0));
                v3_1.add("1B ?? FF FF FF FF 6E 20 ?? ?? ?? ??");
                v4_1.add("1B ?? ?? ?? ?? ?? 00 00 00 00 00 00");
                v5_1.add(Boolean.valueOf(0));
                v6_1.add("support1 Fixed!\n(pak intekekt 32 bit)");
                v7_1.add("search_pack");
                v8_1.add(Boolean.valueOf(1));
                v3_1.add("6E 20 FF FF ?? 00 0A ??");
                v4_1.add("6E 20 ?? ?? ?? 00 12 S1");
                v5_1.add(Boolean.valueOf(0));
                v6_1.add("support2 Fixed!\n(sha intekekt 4)");
                v7_1.add("");
                v8_1.add(Boolean.valueOf(0));
                v3_1.add("6E 20 FF FF ?? 00");
                v4_1.add("00 00 00 00 00 00");
                v5_1.add(Boolean.valueOf(0));
                v6_1.add("support3 Fixed!\n(intent for free)");
                v7_1.add("");
                v8_1.add(Boolean.valueOf(0));
                v3_1.add("0A ?? 39 ?? ?? ??");
                v4_1.add("12 S1 39 ?? ?? ??");
                v5_1.add(Boolean.valueOf(0));
                v6_1.add("support1 Fixed!\n(ev1)");
                v7_1.add("search_sign_ver");
                v8_1.add(Boolean.valueOf(0));
                v3_1.add("0A ?? 38 ?? ?? ??");
                v4_1.add("12 S1 38 ?? ?? ??");
                v5_1.add(Boolean.valueOf(0));
                v6_1.add("support1 Fixed!\n(ev1)");
                v7_1.add("search_sign_ver");
                v8_1.add(Boolean.valueOf(0));
                v3_1.add("1C ?? FF FF");
                v4_1.add("1C ?? ?? ??");
                v5_1.add(Boolean.valueOf(0));
                v6_1.add("support1 Fixed!\n(si)");
                v7_1.add("search_sign_ver");
                v8_1.add(Boolean.valueOf(0));
                v3_1.add("23 ?? ?? ?? 1C ?? ?? ?? 12 ?? 4D ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0C ?? 1F ?? ?? ?? 6E ?? ?? ?? ?? ?? 0A ?? 39 ?? ?? ?? 1A ?? ?? ?? 1A");
                v4_1.add("23 ?? ?? ?? 1C ?? ?? ?? 12 ?? 4D ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0C ?? 1F ?? ?? ?? 6E ?? ?? ?? ?? ?? 12 S1 39 ?? ?? ?? 1A ?? ?? ?? 1A");
                v5_1.add(Boolean.valueOf(1));
                v6_1.add("support3 Fixed!\n(s)");
                v7_1.add("");
                v8_1.add(Boolean.valueOf(0));
                v3_1.add("1A ?? ?? ?? 6E ?? ?? ?? ?? ?? 54 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 12 ?? 6E ?? ?? ?? ?? ?? 0C ?? 38 ?? ?? ?? 72 ?? ?? ?? ?? ?? 0A ?? 39 ?? ?? ?? 54 ?? ?? ?? 54 ?? ?? ?? 12 ?? 6E");
                v4_1.add("1A ?? ?? ?? 00 00 00 00 00 00 54 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 12 ?? 6E ?? ?? ?? ?? ?? 0C ?? 38 ?? ?? ?? 72 ?? ?? ?? ?? ?? 0A ?? 39 ?? ?? ?? 54 ?? ?? ?? 54 ?? ?? ?? 12 ?? 6E");
                v5_1.add(Boolean.valueOf(com.chelpus.root.utils.runpatchsupportOld.pattern2));
                v6_1.add("support4 Fixed!\n(pak)");
                v7_1.add("");
                v8_1.add(Boolean.valueOf(0));
                v3_1.add("1A ?? ?? ?? 6E ?? ?? ?? ?? ?? 54 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0C ?? 72 ?? ?? ?? ?? ?? 0A ?? 39 ?? ?? ?? 54 ?? ?? ?? 54 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0E 00");
                v4_1.add("1A ?? ?? ?? 00 00 00 00 00 00 54 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0C ?? 72 ?? ?? ?? ?? ?? 0A ?? 39 ?? ?? ?? 54 ?? ?? ?? 54 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0E 00");
                v5_1.add(Boolean.valueOf(com.chelpus.root.utils.runpatchsupportOld.pattern2));
                v6_1.add("support4 Fixed!\n(pak)");
                v7_1.add("");
                v8_1.add(Boolean.valueOf(0));
                v3_1.add("1A ?? ?? ?? 6E ?? ?? ?? ?? ?? 54 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 12 ?? 6E ?? ?? ?? ?? ?? 0C ?? 72 ?? ?? ?? ?? ?? 0A ?? 39 ?? ?? ?? 54 ?? ?? ?? 54 ?? ?? ?? 12 ?? 6E ?? ?? ?? ?? ?? 0E 00");
                v4_1.add("1A ?? ?? ?? 00 00 00 00 00 00 54 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 12 ?? 6E ?? ?? ?? ?? ?? 0C ?? 72 ?? ?? ?? ?? ?? 0A ?? 39 ?? ?? ?? 54 ?? ?? ?? 54 ?? ?? ?? 12 ?? 6E ?? ?? ?? ?? ?? 0E 00");
                v5_1.add(Boolean.valueOf(com.chelpus.root.utils.runpatchsupportOld.pattern2));
                v6_1.add("support4 Fixed!\n(pak)");
                v7_1.add("");
                v8_1.add(Boolean.valueOf(0));
                if (p69[0].contains("com.jetappfactory.")) {
                    v3_1.add("71 30 ?? ?? ?? ?? 0C ?? 6E 20 ?? ?? ?? ?? 54 ?? ?? ?? 28 ??");
                    v4_1.add("71 30 ?? ?? ?? ?? 0C ?? 00 00 00 00 00 00 54 ?? ?? ?? 28 ??");
                    v5_1.add(Boolean.valueOf(com.chelpus.root.utils.runpatchsupportOld.pattern2));
                    v6_1.add("support4 Fixed!\n(pak)");
                    v7_1.add("");
                    v8_1.add(Boolean.valueOf(0));
                }
                com.chelpus.Utils.convertToPatchItemAuto(v2_1, v3_1, v4_1, v5_1, v6_1, v7_1, v8_1, Boolean.valueOf(0));
                try {
                    if (!com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.startUnderRoot.booleanValue()) {
                        if ((com.chelpus.root.utils.runpatchsupportOld.createAPK) || (com.chelpus.root.utils.runpatchsupportOld.ART)) {
                            if (com.chelpus.root.utils.runpatchsupportOld.createAPK) {
                                String v48 = p69[0];
                                com.chelpus.root.utils.runpatchsupportOld.appdir = p69[2];
                                com.chelpus.root.utils.runpatchsupportOld.sddir = p69[5];
                                com.chelpus.root.utils.runpatchsupportOld.clearTempSD();
                                java.io.File v16_0 = new java.io.File;
                                v16_0(com.chelpus.root.utils.runpatchsupportOld.appdir);
                                com.chelpus.root.utils.runpatchsupportOld.unzipSD(v16_0);
                                com.chelpus.root.utils.runpatchsupportOld.crkapk = new java.io.File(new StringBuilder().append(com.chelpus.root.utils.runpatchsupportOld.sddir).append("/Modified/").append(v48).append(".apk").toString());
                                com.chelpus.Utils.copyFile(v16_0, com.chelpus.root.utils.runpatchsupportOld.crkapk);
                                java.io.File v15_1 = new java.io.File(new StringBuilder().append(com.chelpus.root.utils.runpatchsupportOld.sddir).append("/Modified/AndroidManifest.xml").toString());
                                if (v15_1.exists()) {
                                    if (!new com.android.vending.billing.InAppBillingService.LUCK.AxmlExample().changeTargetApi(v15_1, "19")) {
                                        v15_1.delete();
                                    }
                                    if ((com.chelpus.root.utils.runpatchsupportOld.classesFiles != null) && (com.chelpus.root.utils.runpatchsupportOld.classesFiles.size() != 0)) {
                                        com.chelpus.root.utils.runpatchsupportOld.filestopatch.clear();
                                        int v10_257 = com.chelpus.root.utils.runpatchsupportOld.classesFiles.iterator();
                                        while (v10_257.hasNext()) {
                                            java.io.File v18_3 = ((java.io.File) v10_257.next());
                                            if (v18_3.exists()) {
                                                com.chelpus.root.utils.runpatchsupportOld.filestopatch.add(v18_3);
                                            } else {
                                                throw new java.io.FileNotFoundException();
                                            }
                                        }
                                    } else {
                                        throw new java.io.FileNotFoundException();
                                    }
                                } else {
                                    throw new java.io.FileNotFoundException();
                                }
                            }
                            if (com.chelpus.root.utils.runpatchsupportOld.ART) {
                                com.chelpus.Utils.sendFromRoot("ART mode create dex enabled.");
                                com.chelpus.root.utils.runpatchsupportOld.appdir = p69[2];
                                com.chelpus.root.utils.runpatchsupportOld.sddir = p69[3];
                                com.chelpus.root.utils.runpatchsupportOld.clearTempSD();
                                java.io.File v16_1 = new java.io.File;
                                v16_1(com.chelpus.root.utils.runpatchsupportOld.appdir);
                                com.chelpus.root.utils.runpatchsupportOld.unzipART(v16_1);
                                if ((com.chelpus.root.utils.runpatchsupportOld.classesFiles != null) && (com.chelpus.root.utils.runpatchsupportOld.classesFiles.size() != 0)) {
                                    com.chelpus.root.utils.runpatchsupportOld.filestopatch.clear();
                                    int v10_271 = com.chelpus.root.utils.runpatchsupportOld.classesFiles.iterator();
                                    while (v10_271.hasNext()) {
                                        java.io.File v18_1 = ((java.io.File) v10_271.next());
                                        if (v18_1.exists()) {
                                            com.chelpus.root.utils.runpatchsupportOld.filestopatch.add(v18_1);
                                        } else {
                                            throw new java.io.FileNotFoundException();
                                        }
                                    }
                                } else {
                                    throw new java.io.FileNotFoundException();
                                }
                            }
                        } else {
                            com.chelpus.root.utils.runpatchsupportOld.dir = p69[3];
                            com.chelpus.root.utils.runpatchsupportOld.dirapp = p69[2];
                            com.chelpus.root.utils.runpatchsupportOld.clearTemp();
                            if (p69[4].equals("not_system")) {
                                com.chelpus.root.utils.runpatchsupportOld.system = 0;
                            }
                            if (p69[4].equals("system")) {
                                com.chelpus.root.utils.runpatchsupportOld.system = 1;
                            }
                            com.chelpus.root.utils.runpatchsupportOld.filestopatch.clear();
                            com.chelpus.Utils.sendFromRoot("CLASSES mode create odex enabled.");
                            com.chelpus.root.utils.runpatchsupportOld.appdir = p69[2];
                            com.chelpus.root.utils.runpatchsupportOld.sddir = p69[3];
                            com.chelpus.root.utils.runpatchsupportOld.clearTempSD();
                            java.io.File v16_2 = new java.io.File;
                            v16_2(com.chelpus.root.utils.runpatchsupportOld.appdir);
                            com.chelpus.Utils.sendFromRoot("Get classes.dex.");
                            com.chelpus.root.utils.runpatchsupportOld.print.println("Get classes.dex.");
                            com.chelpus.root.utils.runpatchsupportOld.unzipART(v16_2);
                            if ((com.chelpus.root.utils.runpatchsupportOld.classesFiles != null) && (com.chelpus.root.utils.runpatchsupportOld.classesFiles.size() != 0)) {
                                com.chelpus.root.utils.runpatchsupportOld.filestopatch.clear();
                                int v10_309 = com.chelpus.root.utils.runpatchsupportOld.classesFiles.iterator();
                                while (v10_309.hasNext()) {
                                    java.io.File v18_5 = ((java.io.File) v10_309.next());
                                    if (v18_5.exists()) {
                                        com.chelpus.root.utils.runpatchsupportOld.filestopatch.add(v18_5);
                                    } else {
                                        throw new java.io.FileNotFoundException();
                                    }
                                }
                                String v45 = com.chelpus.Utils.getPlaceForOdex(p69[2], 1);
                                java.io.File v44_1 = new java.io.File(v45);
                                if (v44_1.exists()) {
                                    v44_1.delete();
                                }
                                java.io.File v44_2 = new java.io.File;
                                v44_2(v45.replace("-1", "-2"));
                                if (v44_2.exists()) {
                                    v44_2.delete();
                                }
                                java.io.File v44_3 = new java.io.File;
                                v44_3(v45.replace("-2", "-1"));
                                if (v44_3.exists()) {
                                    v44_3.delete();
                                }
                            } else {
                                throw new java.io.FileNotFoundException();
                            }
                        }
                        String v68_0 = com.chelpus.root.utils.runpatchsupportOld.filestopatch.iterator();
                        while (v68_0.hasNext()) {
                            java.util.ArrayList v47;
                            java.io.File v35_1 = ((java.io.File) v68_0.next());
                            com.chelpus.Utils.sendFromRoot("Find string id.");
                            java.util.ArrayList v63_1 = new java.util.ArrayList();
                            v63_1.add("com.android.vending");
                            v63_1.add("SHA1withRSA");
                            v63_1.add("com.android.vending.billing.InAppBillingService.BIND");
                            v63_1.add("Ljava/security/Signature;");
                            v63_1.add("verify");
                            v63_1.add("Landroid/content/Intent;");
                            v63_1.add("setPackage");
                            v63_1.add("engineVerify");
                            v63_1.add("Ljava/security/SignatureSpi;");
                            com.chelpus.Utils.sendFromRoot("String analysis.");
                            com.chelpus.root.utils.runpatchsupportOld.print.println("String analysis.");
                            if (com.chelpus.root.utils.runpatchsupportOld.copyDC) {
                                v47 = com.chelpus.Utils.getStringIds(v35_1.getAbsolutePath(), v63_1, 0);
                            } else {
                                v47 = com.chelpus.Utils.getStringIds(v35_1.getAbsolutePath(), v63_1, 0);
                            }
                            int v30 = 0;
                            int v31 = 0;
                            int v32 = 0;
                            int v29 = 0;
                            int v59 = 0;
                            java.util.ArrayList v66_1 = new java.util.ArrayList();
                            v66_1.add(new com.android.vending.billing.InAppBillingService.LUCK.TypesItem("Ljava/security/Signature;"));
                            v66_1.add(new com.android.vending.billing.InAppBillingService.LUCK.TypesItem("Ljava/security/SignatureSpi;"));
                            java.util.ArrayList v19_1 = new java.util.ArrayList();
                            v19_1.add(new com.android.vending.billing.InAppBillingService.LUCK.CommandItem("Ljava/security/Signature;", "verify"));
                            v19_1.add(new com.android.vending.billing.InAppBillingService.LUCK.CommandItem("Landroid/content/Intent;", "setPackage"));
                            int v11_47 = v47.iterator();
                            while (v11_47.hasNext()) {
                                com.android.vending.billing.InAppBillingService.LUCK.StringItem v39_1 = ((com.android.vending.billing.InAppBillingService.LUCK.StringItem) v11_47.next());
                                int v10_643 = v66_1.iterator();
                                while (v10_643.hasNext()) {
                                    com.android.vending.billing.InAppBillingService.LUCK.TypesItem v40_9 = ((com.android.vending.billing.InAppBillingService.LUCK.TypesItem) v10_643.next());
                                    if (v40_9.type.equals(v39_1.str)) {
                                        v40_9.Type = v39_1.offset;
                                    }
                                }
                                int v10_644 = v19_1.iterator();
                                while (v10_644.hasNext()) {
                                    com.android.vending.billing.InAppBillingService.LUCK.TypesItem v40_7 = ((com.android.vending.billing.InAppBillingService.LUCK.CommandItem) v10_644.next());
                                    if (v40_7.object.equals(v39_1.str)) {
                                        v40_7.Object = v39_1.offset;
                                    }
                                    if (v40_7.method.equals(v39_1.str)) {
                                        v40_7.Method = v39_1.offset;
                                    }
                                }
                                if (v39_1.str.equals("com.android.vending")) {
                                    ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(5)).origByte[2] = v39_1.offset[0];
                                    ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(5)).origByte[3] = v39_1.offset[1];
                                    ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(6)).origByte[2] = v39_1.offset[0];
                                    ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(6)).origByte[3] = v39_1.offset[1];
                                    ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(7)).origByte[2] = v39_1.offset[0];
                                    ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(7)).origByte[3] = v39_1.offset[1];
                                    ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(8)).origByte[2] = v39_1.offset[0];
                                    ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(8)).origByte[3] = v39_1.offset[1];
                                    ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(8)).origByte[4] = v39_1.offset[2];
                                    ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(8)).origByte[5] = v39_1.offset[3];
                                    ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(9)).origByte[2] = v39_1.offset[0];
                                    ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(9)).origByte[3] = v39_1.offset[1];
                                    ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(9)).origByte[4] = v39_1.offset[2];
                                    ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(9)).origByte[5] = v39_1.offset[3];
                                    ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(10)).origByte[2] = v39_1.offset[0];
                                    ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(10)).origByte[3] = v39_1.offset[1];
                                    ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(10)).origByte[4] = v39_1.offset[2];
                                    ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(10)).origByte[5] = v39_1.offset[3];
                                    v30 = 1;
                                }
                                if (v39_1.str.equals("com.android.vending.billing.InAppBillingService.BIND")) {
                                    System.out.println(new StringBuilder().append("c.a.v.b.i ").append(v39_1.bits32).append(" ").append(v39_1.offset[0]).append(" ").append(v39_1.offset[1]).append(" ").append(v39_1.offset[2]).append(" ").append(v39_1.offset[3]).toString());
                                    ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(0)).origByte[2] = v39_1.offset[0];
                                    ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(0)).origByte[3] = v39_1.offset[1];
                                    ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(1)).origByte[2] = v39_1.offset[0];
                                    ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(1)).origByte[3] = v39_1.offset[1];
                                    ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(1)).origByte[4] = v39_1.offset[2];
                                    ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(1)).origByte[5] = v39_1.offset[3];
                                    v31 = 1;
                                }
                                if (v39_1.str.equals("SHA1withRSA")) {
                                    ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(2)).origByte[2] = v39_1.offset[0];
                                    ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(2)).origByte[3] = v39_1.offset[1];
                                    ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(3)).origByte[2] = v39_1.offset[0];
                                    ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(3)).origByte[3] = v39_1.offset[1];
                                    ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(3)).origByte[4] = v39_1.offset[2];
                                    ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(3)).origByte[5] = v39_1.offset[3];
                                    v32 = 1;
                                }
                                if (v39_1.str.equals("engineVerify")) {
                                    v29 = 1;
                                }
                                if (v39_1.str.equals("Ljava/security/SignatureSpi;")) {
                                    v59 = 1;
                                }
                            }
                            if ((v30 == 0) || (v31 == 0)) {
                                ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(0)).pattern = 0;
                                ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(1)).pattern = 0;
                                ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(5)).pattern = 0;
                                ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(6)).pattern = 0;
                                ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(7)).pattern = 0;
                                ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(8)).pattern = 0;
                                ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(8)).marker = "";
                                ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(9)).pattern = 0;
                                ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(9)).marker = "";
                                ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(10)).pattern = 0;
                                ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(10)).marker = "";
                            }
                            if (v32 == 0) {
                                ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(2)).pattern = 0;
                                ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(3)).pattern = 0;
                                ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(4)).pattern = 0;
                            }
                            com.chelpus.Utils.sendFromRoot("Parse data for patch.");
                            com.chelpus.root.utils.runpatchsupportOld.print.println("Parse data for patch.");
                            com.chelpus.Utils.getMethodsIds(v35_1.getAbsolutePath(), v19_1, 0);
                            int v11_64 = v19_1.iterator();
                            while (v11_64.hasNext()) {
                                com.android.vending.billing.InAppBillingService.LUCK.TypesItem v40_5 = ((com.android.vending.billing.InAppBillingService.LUCK.CommandItem) v11_64.next());
                                if (v40_5.found_index_command) {
                                    if (v40_5.object.equals("Ljava/security/Signature;")) {
                                        ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(11)).origByte[2] = v40_5.index_command[0];
                                        ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(11)).origByte[3] = v40_5.index_command[1];
                                        ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(11)).pattern = 1;
                                    }
                                    if ((v40_5.object.equals("Landroid/content/Intent;")) && (com.chelpus.root.utils.runpatchsupportOld.pattern3)) {
                                        ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(12)).origByte[2] = v40_5.index_command[0];
                                        ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(12)).origByte[3] = v40_5.index_command[1];
                                        ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(12)).pattern = 1;
                                    }
                                }
                            }
                            if ((v29 != 0) && (v59 != 0)) {
                                com.chelpus.Utils.getTypesIds(v35_1.getAbsolutePath(), v66_1, 0);
                                int v11_66 = v66_1.iterator();
                                while (v11_66.hasNext()) {
                                    com.android.vending.billing.InAppBillingService.LUCK.TypesItem v40_3 = ((com.android.vending.billing.InAppBillingService.LUCK.TypesItem) v11_66.next());
                                    if ((v40_3.found_id_type) && (v40_3.type.equals("Ljava/security/SignatureSpi;"))) {
                                        ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(15)).origByte[2] = v40_3.id_type[0];
                                        ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(15)).origByte[3] = v40_3.id_type[1];
                                        ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v2_1.get(15)).pattern = 1;
                                    }
                                }
                            }
                            com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto v0_31 = new com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto[v2_1.size()];
                            com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto[] v49 = v0_31;
                            int v67 = 0;
                            int v10_394 = v2_1.iterator();
                            while (v10_394.hasNext()) {
                                v49[v67] = ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v10_394.next());
                                v67++;
                            }
                            com.chelpus.Utils.sendFromRoot("Set Strings.");
                            com.chelpus.root.utils.runpatchsupportOld.print.println("Set Strings.");
                            java.util.ArrayList v58_1 = new java.util.ArrayList();
                            v58_1.add("com.android.vending.billing.InAppBillingService.BIND".getBytes());
                            com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto v0_33 = new byte[][v58_1.size()];
                            int v21 = com.chelpus.Utils.setStringIds(v35_1.getAbsolutePath(), ((byte[][]) ((byte[][]) v58_1.toArray(v0_33))), 0, 76);
                            while (v21 > 0) {
                                v21--;
                                com.chelpus.Utils.sendFromRoot("Reworked inapp string!");
                                com.chelpus.root.utils.runpatchsupportOld.print.println("Reworked inapp string!");
                            }
                            long v64 = System.currentTimeMillis();
                            java.nio.channels.FileChannel v9_1 = new java.io.RandomAccessFile(v35_1, "rw").getChannel();
                            com.chelpus.Utils.sendFromRoot(new StringBuilder().append("Size file:").append(v9_1.size()).toString());
                            java.nio.MappedByteBuffer v34 = v9_1.map(java.nio.channels.FileChannel$MapMode.READ_WRITE, 0, ((long) ((int) v9_1.size())));
                            int v60 = 0;
                            int v62 = 0;
                            int v61 = 0;
                            int v53 = 0;
                            long v41 = 0;
                            try {
                                while (v34.hasRemaining()) {
                                    if ((!com.chelpus.root.utils.runpatchsupportOld.createAPK) && ((v34.position() - v53) > 149999)) {
                                        com.chelpus.Utils.sendFromRoot(new StringBuilder().append("Progress size:").append(v34.position()).toString());
                                        v53 = v34.position();
                                    }
                                    int v23 = v34.position();
                                    byte v22 = v34.get();
                                    int v38 = 0;
                                    int v17 = 0;
                                    while (v17 < v49.length) {
                                        com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto v52 = v49[v17];
                                        v34.position(v23);
                                        if ((v52.markerTrig) && ((v17 == 5) || ((v17 == 6) || ((v17 == 7) || ((v17 == 8) || ((v17 == 9) || (v17 == 10))))))) {
                                            if (v38 == 0) {
                                                v62++;
                                                v38 = 1;
                                            }
                                            if (v62 >= 60) {
                                                v52.markerTrig = 0;
                                                int v10_431 = v2_1.iterator();
                                                while (v10_431.hasNext()) {
                                                    com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto v24_1 = ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v10_431.next());
                                                    if (v24_1.marker.equals(v52.marker)) {
                                                        v24_1.markerTrig = 0;
                                                    }
                                                }
                                                v62 = 0;
                                                v34.position((v23 + 1));
                                            } else {
                                                if (v22 == v52.origByte[0]) {
                                                    if (v52.repMask[0] == 0) {
                                                        v52.repByte[0] = v22;
                                                    }
                                                    int v37_0 = 1;
                                                    v34.position((v23 + 1));
                                                    byte v55_0 = v34.get();
                                                    while ((v55_0 == v52.origByte[v37_0]) || ((v52.origMask[v37_0] == 1) || ((v52.origMask[v37_0] == 20) || ((v52.origMask[v37_0] == 21) || (v52.origMask[v37_0] == 23))))) {
                                                        if (v52.repMask[v37_0] == 0) {
                                                            v52.repByte[v37_0] = v55_0;
                                                        }
                                                        if (v52.repMask[v37_0] == 20) {
                                                            v52.repByte[v37_0] = ((byte) (v55_0 & 15));
                                                        }
                                                        if (v52.repMask[v37_0] == 21) {
                                                            v52.repByte[v37_0] = ((byte) ((v55_0 & 15) + 16));
                                                        }
                                                        v37_0++;
                                                        if (v37_0 != v52.origByte.length) {
                                                            v55_0 = v34.get();
                                                        } else {
                                                            v34.position(v23);
                                                            v34.put(v52.repByte);
                                                            v34.force();
                                                            com.chelpus.Utils.sendFromRoot(v52.resultText);
                                                            com.chelpus.root.utils.runpatchsupportOld.print.println(v52.resultText);
                                                            v52.result = 1;
                                                            v52.markerTrig = 0;
                                                            int v10_465 = v2_1.iterator();
                                                            while (v10_465.hasNext()) {
                                                                com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto v24_13 = ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v10_465.next());
                                                                if (v24_13.marker.equals(v52.marker)) {
                                                                    v24_13.markerTrig = 0;
                                                                }
                                                            }
                                                            v62 = 0;
                                                            v34.position((v23 + 1));
                                                            break;
                                                        }
                                                    }
                                                }
                                                v34.position((v23 + 1));
                                            }
                                        }
                                        if ((v52.markerTrig) && ((v17 == 13) || (v17 == 14))) {
                                            com.chelpus.root.utils.runpatchsupportOld.print.println("search jump");
                                            if (v38 == 0) {
                                                v61++;
                                                v38 = 1;
                                            }
                                            if (v61 >= 90) {
                                                v52.markerTrig = 0;
                                                int v10_473 = v2_1.iterator();
                                                while (v10_473.hasNext()) {
                                                    com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto v24_3 = ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v10_473.next());
                                                    if (v24_3.marker.equals(v52.marker)) {
                                                        v24_3.markerTrig = 0;
                                                    }
                                                }
                                                v61 = 0;
                                                v34.position((v23 + 1));
                                            } else {
                                                if (v22 == v52.origByte[0]) {
                                                    if (v52.repMask[0] == 0) {
                                                        v52.repByte[0] = v22;
                                                    }
                                                    int v37_1 = 1;
                                                    v34.position((v23 + 1));
                                                    byte v55_1 = v34.get();
                                                    while ((v55_1 == v52.origByte[v37_1]) || ((v52.origMask[v37_1] == 1) || ((v52.origMask[v37_1] == 20) || ((v52.origMask[v37_1] == 21) || (v52.origMask[v37_1] == 23))))) {
                                                        if (v52.repMask[v37_1] == 0) {
                                                            v52.repByte[v37_1] = v55_1;
                                                        }
                                                        if (v52.repMask[v37_1] == 20) {
                                                            v52.repByte[v37_1] = ((byte) (v55_1 & 15));
                                                        }
                                                        if (v52.repMask[v37_1] == 21) {
                                                            v52.repByte[v37_1] = ((byte) ((v55_1 & 15) + 16));
                                                        }
                                                        v37_1++;
                                                        if (v37_1 != v52.origByte.length) {
                                                            v55_1 = v34.get();
                                                        } else {
                                                            v34.position(v23);
                                                            v34.put(v52.repByte);
                                                            v34.force();
                                                            com.chelpus.Utils.sendFromRoot(v52.resultText);
                                                            com.chelpus.root.utils.runpatchsupportOld.print.println(v52.resultText);
                                                            v52.result = 1;
                                                            v52.markerTrig = 0;
                                                            int v10_507 = v2_1.iterator();
                                                            while (v10_507.hasNext()) {
                                                                com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto v24_11 = ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v10_507.next());
                                                                if (v24_11.marker.equals(v52.marker)) {
                                                                    v24_11.markerTrig = 0;
                                                                }
                                                            }
                                                            v61 = 0;
                                                            v34.position((v23 + 1));
                                                            break;
                                                        }
                                                    }
                                                }
                                                v34.position((v23 + 1));
                                            }
                                        }
                                        if ((v52.markerTrig) && (v17 == 4)) {
                                            v60++;
                                            if (v60 >= 90) {
                                                v52.markerTrig = 0;
                                                int v10_513 = v2_1.iterator();
                                                while (v10_513.hasNext()) {
                                                    com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto v24_5 = ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v10_513.next());
                                                    if (v24_5.marker.equals(v52.marker)) {
                                                        v24_5.markerTrig = 0;
                                                    }
                                                }
                                                v60 = 0;
                                                v34.position((v23 + 1));
                                            } else {
                                                if (v22 == v52.origByte[0]) {
                                                    if (v52.repMask[0] == 0) {
                                                        v52.repByte[0] = v22;
                                                    }
                                                    int v37_2 = 1;
                                                    v34.position((v23 + 1));
                                                    byte v55_2 = v34.get();
                                                    while ((v55_2 == v52.origByte[v37_2]) || ((v52.origMask[v37_2] == 1) || ((v52.origMask[v37_2] == 20) || ((v52.origMask[v37_2] == 21) || (v52.origMask[v37_2] == 23))))) {
                                                        if (v52.repMask[v37_2] == 0) {
                                                            v52.repByte[v37_2] = v55_2;
                                                        }
                                                        if (v52.repMask[v37_2] == 20) {
                                                            v52.repByte[v37_2] = ((byte) (v55_2 & 15));
                                                        }
                                                        if (v52.repMask[v37_2] == 21) {
                                                            v52.repByte[v37_2] = ((byte) ((v55_2 & 15) + 16));
                                                        }
                                                        v37_2++;
                                                        if (v37_2 != v52.origByte.length) {
                                                            v55_2 = v34.get();
                                                        } else {
                                                            v34.position(v23);
                                                            v34.put(v52.repByte);
                                                            v34.force();
                                                            com.chelpus.Utils.sendFromRoot(v52.resultText);
                                                            com.chelpus.root.utils.runpatchsupportOld.print.println(v52.resultText);
                                                            v52.result = 1;
                                                            v52.markerTrig = 0;
                                                            int v10_547 = v2_1.iterator();
                                                            while (v10_547.hasNext()) {
                                                                com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto v24_9 = ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v10_547.next());
                                                                if (v24_9.marker.equals(v52.marker)) {
                                                                    v24_9.markerTrig = 0;
                                                                }
                                                            }
                                                            v60 = 0;
                                                            v34.position((v23 + 1));
                                                            break;
                                                        }
                                                    }
                                                }
                                                v34.position((v23 + 1));
                                            }
                                        }
                                        if ((!v52.markerTrig) && ((v22 == v52.origByte[0]) && (v52.pattern))) {
                                            if (v52.repMask[0] == 0) {
                                                v52.repByte[0] = v22;
                                            }
                                            int v37_3 = 1;
                                            v34.position((v23 + 1));
                                            byte v55_3 = v34.get();
                                            while ((v55_3 == v52.origByte[v37_3]) || ((v52.origMask[v37_3] == 1) || ((v52.origMask[v37_3] == 20) || ((v52.origMask[v37_3] == 21) || (v52.origMask[v37_3] == 23))))) {
                                                if (v52.repMask[v37_3] == 0) {
                                                    v52.repByte[v37_3] = v55_3;
                                                }
                                                if (v52.repMask[v37_3] == 20) {
                                                    v52.repByte[v37_3] = ((byte) (v55_3 & 15));
                                                }
                                                if (v52.repMask[v37_3] == 21) {
                                                    v52.repByte[v37_3] = ((byte) ((v55_3 & 15) + 16));
                                                }
                                                v37_3++;
                                                if (v37_3 != v52.origByte.length) {
                                                    v55_3 = v34.get();
                                                } else {
                                                    v34.position(v23);
                                                    v34.put(v52.repByte);
                                                    v34.force();
                                                    com.chelpus.Utils.sendFromRoot(v52.resultText);
                                                    com.chelpus.root.utils.runpatchsupportOld.print.println(v52.resultText);
                                                    v52.result = 1;
                                                    if (v52.marker.equals("")) {
                                                        break;
                                                    }
                                                    v52.markerTrig = 1;
                                                    int v10_586 = v2_1.iterator();
                                                    while (v10_586.hasNext()) {
                                                        com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto v24_7 = ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto) v10_586.next());
                                                        if (v24_7.marker.equals(v52.marker)) {
                                                            v24_7.markerTrig = 1;
                                                        }
                                                    }
                                                }
                                            }
                                            v34.position((v23 + 1));
                                        }
                                        v17++;
                                    }
                                    v34.position((v23 + 1));
                                    v41++;
                                }
                            } catch (Exception v28_2) {
                                com.chelpus.Utils.sendFromRoot(new StringBuilder().append("").append(v28_2).toString());
                            }
                            v9_1.close();
                            com.chelpus.Utils.sendFromRoot(new StringBuilder().append("").append(((System.currentTimeMillis() - v64) / 1000)).toString());
                            com.chelpus.Utils.sendFromRoot("Analise Results:");
                        }
                    } else {
                        com.chelpus.Utils.remount(p69[2], "RW");
                    }
                } catch (Exception v28_3) {
                    com.chelpus.Utils.sendFromRoot(new StringBuilder().append("Patch Process Exception: ").append(v28_3.toString()).toString());
                } catch (java.io.FileNotFoundException v43) {
                    com.chelpus.Utils.sendFromRoot("Error: Program files are not found!\n\nCheck the location dalvik-cache to solve problems!\n\nDefault: /data/dalvik-cache/*");
                }
                int v10_787 = com.chelpus.root.utils.runpatchsupportOld.filestopatch.iterator();
                while (v10_787.hasNext()) {
                    com.chelpus.Utils.fixadler(((java.io.File) v10_787.next()));
                    com.chelpus.root.utils.runpatchsupportOld.clearTempSD();
                }
                if (!com.chelpus.root.utils.runpatchsupportOld.createAPK) {
                    com.chelpus.Utils.sendFromRoot("Create ODEX:");
                    int v56 = com.chelpus.Utils.create_ODEX_root(p69[3], com.chelpus.root.utils.runpatchsupportOld.classesFiles, p69[2], com.chelpus.root.utils.runpatchsupportOld.uid, com.chelpus.Utils.getOdexForCreate(p69[2], com.chelpus.root.utils.runpatchsupportOld.uid));
                    com.chelpus.Utils.sendFromRoot(new StringBuilder().append("chelpus_return_").append(v56).toString());
                    if ((v56 == 0) && (!com.chelpus.root.utils.runpatchsupportOld.ART)) {
                        com.chelpus.Utils.afterPatch(p69[1], p69[2], com.chelpus.Utils.getPlaceForOdex(p69[2], 1), com.chelpus.root.utils.runpatchsupportOld.uid, p69[3]);
                    }
                }
                com.chelpus.Utils.sendFromRoot("Optional Steps After Patch:");
                if (!com.chelpus.root.utils.runpatchsupportOld.createAPK) {
                    com.chelpus.Utils.exitFromRootJava();
                }
                com.chelpus.root.utils.runpatchsupportOld.result = v54.allresult;
                return;
            } catch (int v10) {
            } catch (int v10) {
            }
        }
        while (v10_15 < v11_3) {
            java.io.File v33 = v36[v10_15];
            if ((v33.isFile()) && ((!v33.getName().equals("busybox")) && ((!v33.getName().equals("reboot")) && (!v33.getName().equals("dalvikvm"))))) {
                v33.delete();
            }
            v10_15++;
        }
    }

    public static void unzipART(java.io.File p22)
    {
        int v8 = 0;
        try {
            java.io.FileInputStream v6_1 = new java.io.FileInputStream(p22);
            java.util.zip.ZipInputStream v16 = new java.util.zip.ZipInputStream;
            v16(v6_1);
            java.util.zip.ZipEntry v15 = v16.getNextEntry();
        } catch (Exception v4) {
            try {
                net.lingala.zip4j.core.ZipFile v17 = new net.lingala.zip4j.core.ZipFile;
                v17(p22);
                v17.extractFile("classes.dex", com.chelpus.root.utils.runpatchsupportOld.sddir);
                com.chelpus.root.utils.runpatchsupportOld.classesFiles.add(new java.io.File(new StringBuilder().append(com.chelpus.root.utils.runpatchsupportOld.sddir).append("/").append("classes.dex").toString()));
                StringBuilder v0_23 = new String[3];
                String[] v18_33 = v0_23;
                v18_33[0] = "chmod";
                v18_33[1] = "777";
                v18_33[2] = new StringBuilder().append(com.chelpus.root.utils.runpatchsupportOld.sddir).append("/").append("classes.dex").toString();
                com.chelpus.Utils.cmdParam(v18_33);
                v17.extractFile("AndroidManifest.xml", com.chelpus.root.utils.runpatchsupportOld.sddir);
                StringBuilder v0_25 = new String[3];
                String[] v18_36 = v0_25;
                v18_36[0] = "chmod";
                v18_36[1] = "777";
                v18_36[2] = new StringBuilder().append(com.chelpus.root.utils.runpatchsupportOld.sddir).append("/").append("AndroidManifest.xml").toString();
                com.chelpus.Utils.cmdParam(v18_36);
            } catch (Exception v5_1) {
                com.chelpus.Utils.sendFromRoot(new StringBuilder().append("Error classes.dex decompress! ").append(v5_1).toString());
                com.chelpus.Utils.sendFromRoot(new StringBuilder().append("Exception e1").append(v4.toString()).toString());
            } catch (Exception v5_0) {
                com.chelpus.Utils.sendFromRoot(new StringBuilder().append("Error classes.dex decompress! ").append(v5_0).toString());
                com.chelpus.Utils.sendFromRoot(new StringBuilder().append("Exception e1").append(v4.toString()).toString());
            }
            com.chelpus.Utils.sendFromRoot(new StringBuilder().append("Exception e").append(v4.toString()).toString());
            return;
        }
        while ((v15 != null) && (1 != 0)) {
            String v11 = v15.getName();
            if ((v11.toLowerCase().startsWith("classes")) && ((v11.endsWith(".dex")) && (!v11.contains("/")))) {
                java.io.FileOutputStream v9_1 = new java.io.FileOutputStream(new StringBuilder().append(com.chelpus.root.utils.runpatchsupportOld.sddir).append("/").append(v11).toString());
                byte[] v2 = new byte[2048];
                while(true) {
                    int v12 = v16.read(v2);
                    if (v12 == -1) {
                        break;
                    }
                    v9_1.write(v2, 0, v12);
                }
                v16.closeEntry();
                v9_1.close();
                com.chelpus.root.utils.runpatchsupportOld.classesFiles.add(new java.io.File(new StringBuilder().append(com.chelpus.root.utils.runpatchsupportOld.sddir).append("/").append(v11).toString()));
                StringBuilder v0_11 = new String[3];
                String[] v18_16 = v0_11;
                v18_16[0] = "chmod";
                v18_16[1] = "777";
                v18_16[2] = new StringBuilder().append(com.chelpus.root.utils.runpatchsupportOld.sddir).append("/").append(v11).toString();
                com.chelpus.Utils.cmdParam(v18_16);
            }
            if (v11.equals("AndroidManifest.xml")) {
                java.io.FileOutputStream v10_1 = new java.io.FileOutputStream(new StringBuilder().append(com.chelpus.root.utils.runpatchsupportOld.sddir).append("/").append("AndroidManifest.xml").toString());
                byte[] v3 = new byte[2048];
                while(true) {
                    int v13 = v16.read(v3);
                    if (v13 == -1) {
                        break;
                    }
                    v10_1.write(v3, 0, v13);
                }
                v16.closeEntry();
                v10_1.close();
                StringBuilder v0_19 = new String[3];
                String[] v18_28 = v0_19;
                v18_28[0] = "chmod";
                v18_28[1] = "777";
                v18_28[2] = new StringBuilder().append(com.chelpus.root.utils.runpatchsupportOld.sddir).append("/").append("AndroidManifest.xml").toString();
                com.chelpus.Utils.cmdParam(v18_28);
                v8 = 1;
            }
            if ((0 == 0) || (v8 == 0)) {
                v15 = v16.getNextEntry();
            } else {
                break;
            }
        }
        v16.close();
        v6_1.close();
        return;
    }

    public static void unzipSD(java.io.File p15)
    {
        try {
            java.io.FileInputStream v4_1 = new java.io.FileInputStream(p15);
            java.util.zip.ZipInputStream v8_1 = new java.util.zip.ZipInputStream(v4_1);
            int v1 = 0;
        } catch (Exception v2) {
            try {
                net.lingala.zip4j.core.ZipFile v9_1 = new net.lingala.zip4j.core.ZipFile(p15);
                v9_1.extractFile("classes.dex", new StringBuilder().append(com.chelpus.root.utils.runpatchsupportOld.sddir).append("/Modified/").toString());
                com.chelpus.root.utils.runpatchsupportOld.classesFiles.add(new java.io.File(new StringBuilder().append(com.chelpus.root.utils.runpatchsupportOld.sddir).append("/Modified/").append("classes.dex").toString()));
                v9_1.extractFile("AndroidManifest.xml", new StringBuilder().append(com.chelpus.root.utils.runpatchsupportOld.sddir).append("/Modified/").toString());
            } catch (Exception v3_0) {
                com.chelpus.Utils.sendFromRoot(new StringBuilder().append("Error classes.dex decompress! ").append(v3_0).toString());
                com.chelpus.Utils.sendFromRoot(new StringBuilder().append("Exception e1").append(v2.toString()).toString());
            } catch (Exception v3_1) {
                com.chelpus.Utils.sendFromRoot(new StringBuilder().append("Error classes.dex decompress! ").append(v3_1).toString());
                com.chelpus.Utils.sendFromRoot(new StringBuilder().append("Exception e1").append(v2.toString()).toString());
            }
            com.chelpus.Utils.sendFromRoot(new StringBuilder().append("Decompressunzip ").append(v2).toString());
            return;
        }
        do {
            java.util.zip.ZipEntry v7 = v8_1.getNextEntry();
            if (v7 == null) {
                v8_1.close();
                v4_1.close();
                return;
            } else {
                if ((v7.getName().toLowerCase().startsWith("classes")) && ((v7.getName().endsWith(".dex")) && (!v7.getName().contains("/")))) {
                    java.io.FileOutputStream v5_1 = new java.io.FileOutputStream(new StringBuilder().append(com.chelpus.root.utils.runpatchsupportOld.sddir).append("/Modified/").append(v7.getName()).toString());
                    byte[] v0_0 = new byte[1024];
                    while(true) {
                        int v6_0 = v8_1.read(v0_0);
                        if (v6_0 == -1) {
                            break;
                        }
                        v5_1.write(v0_0, 0, v6_0);
                    }
                    com.chelpus.root.utils.runpatchsupportOld.classesFiles.add(new java.io.File(new StringBuilder().append(com.chelpus.root.utils.runpatchsupportOld.sddir).append("/Modified/").append(v7.getName()).toString()));
                    v1 = 1;
                    if (!com.chelpus.root.utils.runpatchsupportOld.createAPK) {
                        v8_1.closeEntry();
                        v5_1.close();
                    }
                }
                if (v7.getName().equals("AndroidManifest.xml")) {
                    java.io.FileOutputStream v5_3 = new java.io.FileOutputStream(new StringBuilder().append(com.chelpus.root.utils.runpatchsupportOld.sddir).append("/Modified/").append("AndroidManifest.xml").toString());
                    byte[] v0_1 = new byte[1024];
                    while(true) {
                        int v6_1 = v8_1.read(v0_1);
                        if (v6_1 == -1) {
                            break;
                        }
                        v5_3.write(v0_1, 0, v6_1);
                    }
                }
            }
        } while(v1 == 0);
        v8_1.closeEntry();
        v5_3.close();
    }
}
