package com.chelpus.root.utils;
public class custompatch {
    public static boolean ART = False;
    static final int BUFFER = 2048;
    public static final byte[] MAGIC = None;
    public static boolean OdexPatch = False;
    public static int adler = 0;
    public static boolean armv7 = False;
    public static java.util.ArrayList arrayFile2 = None;
    private static final int beginTag = 0;
    public static java.util.ArrayList classesFiles = None;
    private static final int classesTag = 1;
    public static boolean convert = False;
    private static String dataBase = "None";
    private static boolean dataBaseExist = False;
    public static String dir = "None";
    public static String dir2 = "None";
    public static String dirapp = "None";
    private static final int endTag = 4;
    private static final int fileInApkTag = 14;
    public static boolean fixunpack = False;
    public static boolean goodResult = False;
    private static String group = "None";
    private static final int libTagALL = 2;
    private static final int libTagARMEABI = 6;
    private static final int libTagARMEABIV7A = 7;
    private static final int libTagMIPS = 8;
    private static final int libTagx86 = 9;
    public static java.io.File localFile2 = None;
    public static String log = "None";
    public static boolean manualpatch = False;
    public static boolean multidex = False;
    public static boolean multilib_patch = False;
    public static boolean odex = False;
    private static final int odexTag = 10;
    public static boolean odexpatch = False;
    private static final int odexpatchTag = 11;
    private static final int otherfilesTag = 3;
    private static final int packageTag = 5;
    private static java.util.ArrayList pat = None;
    public static boolean patchteil = False;
    public static String pkgName = "None";
    public static String sddir = "None";
    private static java.util.ArrayList search = None;
    private static String searchStr = "None";
    private static java.util.ArrayList ser = None;
    private static final int set_copy_file_Tag = 15;
    private static final int set_permissions_Tag = 13;
    private static final int sqlTag = 12;
    public static boolean system;
    public static int tag;
    public static String uid;
    public static boolean unpack;
    private static boolean withFramework;

    static custompatch()
    {
        java.util.ArrayList v0_1 = new byte[8];
        v0_1 = {100, 101, 121, 10, 48, 51, 53, 0};
        com.chelpus.root.utils.custompatch.MAGIC = v0_1;
        com.chelpus.root.utils.custompatch.arrayFile2 = new java.util.ArrayList();
        com.chelpus.root.utils.custompatch.pat = 0;
        com.chelpus.root.utils.custompatch.ser = 0;
        com.chelpus.root.utils.custompatch.search = 0;
        com.chelpus.root.utils.custompatch.patchteil = 0;
        com.chelpus.root.utils.custompatch.unpack = 0;
        com.chelpus.root.utils.custompatch.fixunpack = 0;
        com.chelpus.root.utils.custompatch.manualpatch = 0;
        com.chelpus.root.utils.custompatch.odex = 0;
        com.chelpus.root.utils.custompatch.dir = "/sdcard/";
        com.chelpus.root.utils.custompatch.dir2 = "/sdcard/";
        com.chelpus.root.utils.custompatch.dirapp = "/data/app/";
        com.chelpus.root.utils.custompatch.sddir = "/data/app/";
        com.chelpus.root.utils.custompatch.uid = "";
        com.chelpus.root.utils.custompatch.system = 0;
        com.chelpus.root.utils.custompatch.odexpatch = 0;
        com.chelpus.root.utils.custompatch.OdexPatch = 0;
        com.chelpus.root.utils.custompatch.armv7 = 0;
        com.chelpus.root.utils.custompatch.ART = 0;
        com.chelpus.root.utils.custompatch.convert = 0;
        com.chelpus.root.utils.custompatch.dataBaseExist = 0;
        com.chelpus.root.utils.custompatch.dataBase = "";
        com.chelpus.root.utils.custompatch.searchStr = "";
        com.chelpus.root.utils.custompatch.group = "";
        com.chelpus.root.utils.custompatch.withFramework = 1;
        com.chelpus.root.utils.custompatch.pkgName = "";
        com.chelpus.root.utils.custompatch.log = "";
        com.chelpus.root.utils.custompatch.classesFiles = new java.util.ArrayList();
        com.chelpus.root.utils.custompatch.multidex = 0;
        com.chelpus.root.utils.custompatch.goodResult = 0;
        com.chelpus.root.utils.custompatch.multilib_patch = 0;
        return;
    }

    public custompatch()
    {
        return;
    }

    public static void addToLog(String p2)
    {
        com.chelpus.root.utils.custompatch.log = new StringBuilder().append(com.chelpus.root.utils.custompatch.log).append(p2).append("\n").toString();
        return;
    }

    public static void clearTemp()
    {
        try {
            java.io.File v2_1 = new java.io.File(new StringBuilder().append(com.chelpus.root.utils.custompatch.dir).append("/AndroidManifest.xml").toString());
        } catch (Exception v1) {
            com.chelpus.root.utils.custompatch.addToLog(new StringBuilder().append("").append(v1.toString()).toString());
            return;
        }
        if (v2_1.exists()) {
            v2_1.delete();
        }
        if ((com.chelpus.root.utils.custompatch.classesFiles != null) && (com.chelpus.root.utils.custompatch.classesFiles.size() > 0)) {
            boolean v4_9 = com.chelpus.root.utils.custompatch.classesFiles.iterator();
            while (v4_9.hasNext()) {
                java.io.File v2_21 = new java.io.File(new StringBuilder().append(com.chelpus.root.utils.custompatch.dir).append("/").append(((java.io.File) v4_9.next()).getName()).toString());
                if (v2_21.exists()) {
                    v2_21.delete();
                }
            }
        }
        java.io.File v2_3 = new java.io.File(new StringBuilder().append(com.chelpus.root.utils.custompatch.dir).append("/classes.dex").toString());
        if (v2_3.exists()) {
            v2_3.delete();
        }
        java.io.File v2_5 = new java.io.File(new StringBuilder().append(com.chelpus.root.utils.custompatch.dir).append("/classes1.dex").toString());
        if (v2_5.exists()) {
            v2_5.delete();
        }
        java.io.File v2_7 = new java.io.File(new StringBuilder().append(com.chelpus.root.utils.custompatch.dir).append("/classes2.dex").toString());
        if (v2_7.exists()) {
            v2_7.delete();
        }
        java.io.File v2_9 = new java.io.File(new StringBuilder().append(com.chelpus.root.utils.custompatch.dir).append("/classes3.dex").toString());
        if (v2_9.exists()) {
            v2_9.delete();
        }
        java.io.File v2_11 = new java.io.File(new StringBuilder().append(com.chelpus.root.utils.custompatch.dir).append("/classes4.dex").toString());
        if (v2_11.exists()) {
            v2_11.delete();
        }
        java.io.File v2_13 = new java.io.File(new StringBuilder().append(com.chelpus.root.utils.custompatch.dir).append("/classes5.dex").toString());
        if (v2_13.exists()) {
            v2_13.delete();
        }
        java.io.File v2_15 = new java.io.File(new StringBuilder().append(com.chelpus.root.utils.custompatch.dir).append("/classes6.dex").toString());
        if (v2_15.exists()) {
            v2_15.delete();
        }
        java.io.File v2_17 = new java.io.File(new StringBuilder().append(com.chelpus.root.utils.custompatch.dir).append("/classes.dex.apk").toString());
        if (v2_17.exists()) {
            v2_17.delete();
        }
        java.io.File v2_19 = new java.io.File(new StringBuilder().append(com.chelpus.root.utils.custompatch.dir).append("/classes.dex.dex").toString());
        if (!v2_19.exists()) {
            return;
        } else {
            v2_19.delete();
            return;
        }
    }

    public static void main(String[] p73)
    {
        try {
            com.chelpus.root.utils.custompatch.addToLog(new StringBuilder().append("SU Java-Code Running! ").append(new com.chelpus.root.utils.custompatch$1().getClass().getEnclosingClass().getName()).toString());
            com.chelpus.Utils.startRootJava(0);
            com.chelpus.root.utils.custompatch.pkgName = p73[0];
            try {
                com.chelpus.Utils.kill(com.chelpus.root.utils.custompatch.pkgName);
                com.chelpus.Utils.remount(p73[2], "rw");
            } catch (Exception v29_0) {
                v29_0.printStackTrace();
            }
            try {
                android.util.Log.d("", "");
            } catch (Exception v29) {
                com.chelpus.root.utils.custompatch.addToLog("withoutFramework");
                com.chelpus.root.utils.custompatch.withFramework = 0;
            }
            if ((p73[9] == null) || (!p73[9].contains("ART"))) {
                if (p73[10] != null) {
                    com.chelpus.root.utils.custompatch.uid = p73[10];
                }
            } else {
                com.chelpus.root.utils.custompatch.ART = 1;
            }
            java.io.File[] v34 = new java.io.File(p73[4]).listFiles();
            String v7_11 = v34.length;
            boolean v2_26 = 0;
            while (v2_26 < v7_11) {
                java.io.File v32 = v34[v2_26];
                if ((v32.isFile()) && ((!v32.getName().equals("busybox")) && ((!v32.getName().equals("reboot")) && (!v32.getName().equals("dalvikvm"))))) {
                    v32.delete();
                }
                v2_26++;
            }
            com.chelpus.root.utils.custompatch.sddir = p73[3];
            com.chelpus.root.utils.custompatch.dir = p73[4];
            com.chelpus.root.utils.custompatch.dir2 = p73[4];
            com.chelpus.root.utils.custompatch.dirapp = p73[2];
            com.chelpus.root.utils.custompatch.clearTemp();
            String v22 = "";
            String v35 = "";
            String v15 = "";
            int v31 = 0;
            int v30 = 0;
            int v21 = 0;
            if (p73[6].equals("not_system")) {
                com.chelpus.root.utils.custompatch.system = 0;
            }
            if (p73[6].equals("system")) {
                com.chelpus.root.utils.custompatch.system = 1;
            }
            if (com.chelpus.root.utils.custompatch.system) {
                java.io.File v10_1 = new java.io.File(com.chelpus.root.utils.custompatch.dirapp);
                java.io.File v11_1 = new java.io.File(com.chelpus.Utils.getPlaceForOdex(com.chelpus.root.utils.custompatch.dirapp, 1));
                if ((v10_1.exists()) && ((v11_1.exists()) && (!com.chelpus.Utils.classes_test(v10_1)))) {
                    com.chelpus.root.utils.custompatch.odexpatch = 1;
                    com.chelpus.root.utils.custompatch.localFile2 = v11_1;
                    com.chelpus.root.utils.custompatch.addToLog("\nOdex Application.\nOnly ODEX patch is enabled.\n");
                }
            }
            com.chelpus.root.utils.custompatch.OdexPatch = 0;
            try {
                java.io.FileInputStream v37 = new java.io.FileInputStream;
                v37(p73[1]);
                java.io.InputStreamReader v39 = new java.io.InputStreamReader;
                v39(v37, "UTF-8");
                java.io.BufferedReader v17 = new java.io.BufferedReader;
                v17(v39);
            } catch (boolean v2) {
                com.chelpus.root.utils.custompatch.searchDalvik(p73[2]);
                try {
                    java.io.FileInputStream v36 = new java.io.FileInputStream;
                    v36(p73[1]);
                    java.io.InputStreamReader v38 = new java.io.InputStreamReader;
                    v38(v36, "UTF-8");
                    java.io.BufferedReader v16 = new java.io.BufferedReader;
                    v16(v38);
                    java.util.ArrayList v0_10 = new String[2000];
                    String[] v65 = v0_10;
                    java.util.ArrayList v0_11 = new String[1];
                    v0_11[0] = "";
                    byte[] v3 = 0;
                    int[] v4 = 0;
                    String v33 = "";
                    String v56 = "";
                    String v54 = "";
                    int v60 = 1;
                    int v63 = 1;
                    int v43 = 0;
                    int v14 = 0;
                    int v48 = 0;
                    int v53 = 0;
                    int v55 = 0;
                    int v20 = 0;
                    com.chelpus.root.utils.custompatch.pat = new java.util.ArrayList();
                    com.chelpus.root.utils.custompatch.ser = new java.util.ArrayList();
                    com.chelpus.root.utils.custompatch.search = new java.util.ArrayList();
                    int v57 = 0;
                } catch (Exception v29) {
                    com.chelpus.root.utils.custompatch.addToLog("Custom Patch not Found. It\'s problem with root. Don\'t have access to SD card from root.  If you use SuperSu, try disable \"mount namespace separation\" in SuperSu. If it not help, please update SuperSu and update binary file su from SuperSu.");
                    com.chelpus.Utils.sendFromRootCP(com.chelpus.root.utils.custompatch.log);
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.startUnderRoot = Boolean.valueOf(0);
                    com.chelpus.Utils.exitFromRootJava();
                    return;
                } catch (Exception v29_12) {
                    v29_12.printStackTrace();
                    com.chelpus.root.utils.custompatch.addToLog(new StringBuilder().append("").append(v29_12).toString());
                    com.chelpus.Utils.sendFromRootCP(com.chelpus.root.utils.custompatch.log);
                    com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.startUnderRoot = Boolean.valueOf(0);
                    com.chelpus.Utils.exitFromRootJava();
                    return;
                }
                while(true) {
                    String v23 = v16.readLine();
                    if (v23 == null) {
                        break;
                    }
                    if (!v23.equals("")) {
                        v23 = com.chelpus.Utils.apply_TAGS(v23, com.chelpus.root.utils.custompatch.pkgName);
                    }
                    v65[v57] = v23;
                    if (v65[v57].toUpperCase().contains("[PACKAGE]")) {
                        com.chelpus.root.utils.custompatch.tag = 5;
                        com.chelpus.root.utils.custompatch.unpack = 1;
                        java.io.File v9_1 = new java.io.File(p73[2]);
                        if (!v9_1.exists()) {
                            v9_1 = new java.io.File(p73[2].replace("-1/", "-2/"));
                            if (v9_1.exists()) {
                                p73[2] = p73[2].replace("-1/", "-2/");
                            }
                        }
                        if (!v9_1.exists()) {
                            v9_1 = new java.io.File(p73[2].replace("-1/", "/"));
                            if (v9_1.exists()) {
                                p73[2] = p73[2].replace("-1/", "");
                            }
                        }
                        com.chelpus.root.utils.custompatch.unzip(v9_1);
                        if ((!com.chelpus.root.utils.custompatch.odexpatch) && (!com.chelpus.root.utils.custompatch.OdexPatch)) {
                            String v50 = com.chelpus.Utils.getPlaceForOdex(p73[2], 1);
                            java.io.File v49_1 = new java.io.File(v50);
                            if (v49_1.exists()) {
                                v49_1.delete();
                            }
                            java.io.File v49_2 = new java.io.File;
                            v49_2(v50.replace("-1", "-2"));
                            if (v49_2.exists()) {
                                v49_2.delete();
                            }
                            java.io.File v49_3 = new java.io.File;
                            v49_3(v50.replace("-2", "-1"));
                            if (v49_3.exists()) {
                                v49_3.delete();
                            }
                        }
                    }
                    if ((v14 != 0) && ((v65[v57].contains("[")) || ((v65[v57].contains("]")) || (v65[v57].contains("{"))))) {
                        com.chelpus.root.utils.custompatch.addToLog(new StringBuilder().append("").append(v15).append("\n").toString());
                        v14 = 0;
                    }
                    if (v14 != 0) {
                        v15 = new StringBuilder().append(v15).append("\n").append(v65[v57]).toString();
                    }
                    if ((v65[v57].contains("[")) && (v65[v57].contains("]"))) {
                        switch (com.chelpus.root.utils.custompatch.tag) {
                            case 1:
                                if (!com.chelpus.root.utils.custompatch.odexpatch) {
                                    if (com.chelpus.root.utils.custompatch.unpack) {
                                        if ((com.chelpus.root.utils.custompatch.classesFiles != null) && (com.chelpus.root.utils.custompatch.classesFiles.size() > 0)) {
                                            if (com.chelpus.root.utils.custompatch.classesFiles.size() > 1) {
                                                com.chelpus.root.utils.custompatch.multidex = 1;
                                            }
                                            boolean v2_362 = com.chelpus.root.utils.custompatch.classesFiles.iterator();
                                            while (v2_362.hasNext()) {
                                                java.io.File v18_1 = ((java.io.File) v2_362.next());
                                                com.chelpus.root.utils.custompatch.localFile2 = v18_1;
                                                if (com.chelpus.root.utils.custompatch.pat.size() > 0) {
                                                    com.chelpus.root.utils.custompatch.addToLog("---------------------------------");
                                                    com.chelpus.root.utils.custompatch.addToLog(new StringBuilder().append("Patch for ").append(v18_1.getName()).append(":").toString());
                                                    com.chelpus.root.utils.custompatch.addToLog("---------------------------------\n");
                                                    if (!com.chelpus.root.utils.custompatch.searchStr.equals("")) {
                                                        com.chelpus.root.utils.custompatch.addToLog(com.chelpus.root.utils.custompatch.searchStr);
                                                    }
                                                    if (!com.chelpus.root.utils.custompatch.manualpatch) {
                                                        v60 = com.chelpus.root.utils.custompatch.patchProcess(com.chelpus.root.utils.custompatch.pat);
                                                    }
                                                    if (v60 == 0) {
                                                        v63 = 0;
                                                    }
                                                }
                                            }
                                            com.chelpus.root.utils.custompatch.multidex = 0;
                                            com.chelpus.root.utils.custompatch.goodResult = 0;
                                            com.chelpus.root.utils.custompatch.ser.clear();
                                            com.chelpus.root.utils.custompatch.pat.clear();
                                            com.chelpus.root.utils.custompatch.tag = 200;
                                            com.chelpus.root.utils.custompatch.searchStr = "";
                                        }
                                    } else {
                                        com.chelpus.root.utils.custompatch.searchDalvik(p73[2]);
                                        if ((new java.io.File(com.chelpus.Utils.getPlaceForOdex(com.chelpus.root.utils.custompatch.dirapp, 1)).exists()) && (!com.chelpus.root.utils.custompatch.localFile2.getName().endsWith(".odex"))) {
                                            new java.io.File(com.chelpus.Utils.getPlaceForOdex(com.chelpus.root.utils.custompatch.dirapp, 1)).delete();
                                            com.chelpus.root.utils.custompatch.addToLog("---------------------------------");
                                            com.chelpus.root.utils.custompatch.addToLog("odex file removed before\npatch for dalvik-cache...");
                                            com.chelpus.root.utils.custompatch.addToLog("---------------------------------\n");
                                        }
                                        if (com.chelpus.root.utils.custompatch.pat.size() > 0) {
                                            com.chelpus.root.utils.custompatch.addToLog("---------------------------------");
                                            com.chelpus.root.utils.custompatch.addToLog("Patch for dalvik-cache:");
                                            com.chelpus.root.utils.custompatch.addToLog("---------------------------------\n");
                                            if (!com.chelpus.root.utils.custompatch.searchStr.equals("")) {
                                                com.chelpus.root.utils.custompatch.addToLog(com.chelpus.root.utils.custompatch.searchStr);
                                            }
                                            if (!com.chelpus.root.utils.custompatch.manualpatch) {
                                                v60 = com.chelpus.root.utils.custompatch.patchProcess(com.chelpus.root.utils.custompatch.pat);
                                            }
                                            if (v60 == 0) {
                                                v63 = 0;
                                            }
                                            com.chelpus.root.utils.custompatch.ser.clear();
                                            com.chelpus.root.utils.custompatch.pat.clear();
                                            com.chelpus.root.utils.custompatch.tag = 200;
                                            com.chelpus.root.utils.custompatch.searchStr = "";
                                        }
                                    }
                                } else {
                                    com.chelpus.root.utils.custompatch.localFile2 = new java.io.File(com.chelpus.Utils.getPlaceForOdex(com.chelpus.root.utils.custompatch.dirapp, 1));
                                    if (com.chelpus.root.utils.custompatch.pat.size() > 0) {
                                        com.chelpus.root.utils.custompatch.addToLog("---------------------------------");
                                        com.chelpus.root.utils.custompatch.addToLog("classes.dex not found!\nApply patch for odex:");
                                        com.chelpus.root.utils.custompatch.addToLog("---------------------------------\n");
                                        if (!com.chelpus.root.utils.custompatch.searchStr.equals("")) {
                                            com.chelpus.root.utils.custompatch.addToLog(com.chelpus.root.utils.custompatch.searchStr);
                                        }
                                        if (!com.chelpus.root.utils.custompatch.manualpatch) {
                                            v60 = com.chelpus.root.utils.custompatch.patchProcess(com.chelpus.root.utils.custompatch.pat);
                                        }
                                        if (v60 == 0) {
                                            v63 = 0;
                                        }
                                        com.chelpus.root.utils.custompatch.ser.clear();
                                        com.chelpus.root.utils.custompatch.pat.clear();
                                        com.chelpus.root.utils.custompatch.tag = 200;
                                        com.chelpus.root.utils.custompatch.searchStr = "";
                                    }
                                }
                            case 2:
                                com.chelpus.root.utils.custompatch.convert = 0;
                                if ((com.chelpus.root.utils.custompatch.pat.size() > 0) && ((com.chelpus.root.utils.custompatch.arrayFile2 != null) && (com.chelpus.root.utils.custompatch.arrayFile2.size() > 0))) {
                                    boolean v2_345 = com.chelpus.root.utils.custompatch.arrayFile2.iterator();
                                    while (v2_345.hasNext()) {
                                        com.chelpus.root.utils.custompatch.localFile2 = ((java.io.File) v2_345.next());
                                        com.chelpus.root.utils.custompatch.addToLog("---------------------------");
                                        com.chelpus.root.utils.custompatch.addToLog(new StringBuilder().append("Patch for libraries \n").append(com.chelpus.root.utils.custompatch.localFile2.getAbsolutePath()).append(":").toString());
                                        com.chelpus.root.utils.custompatch.addToLog("---------------------------\n");
                                        if (!com.chelpus.root.utils.custompatch.searchStr.equals("")) {
                                            com.chelpus.root.utils.custompatch.addToLog(com.chelpus.root.utils.custompatch.searchStr);
                                        }
                                        v60 = com.chelpus.root.utils.custompatch.patchProcess(com.chelpus.root.utils.custompatch.pat);
                                        if (v60 == 0) {
                                            v63 = 0;
                                        }
                                    }
                                    com.chelpus.root.utils.custompatch.multilib_patch = 0;
                                    com.chelpus.root.utils.custompatch.goodResult = 0;
                                    com.chelpus.root.utils.custompatch.arrayFile2.clear();
                                }
                                com.chelpus.root.utils.custompatch.ser.clear();
                                com.chelpus.root.utils.custompatch.pat.clear();
                                com.chelpus.root.utils.custompatch.tag = 200;
                                com.chelpus.root.utils.custompatch.searchStr = "";
                                break;
                            case 3:
                                com.chelpus.root.utils.custompatch.convert = 0;
                                if (com.chelpus.root.utils.custompatch.pat.size() > 0) {
                                    if (!com.chelpus.root.utils.custompatch.manualpatch) {
                                        v60 = com.chelpus.root.utils.custompatch.patchProcess(com.chelpus.root.utils.custompatch.pat);
                                    }
                                    if (v60 == 0) {
                                        v63 = 0;
                                    }
                                }
                                com.chelpus.root.utils.custompatch.ser.clear();
                                com.chelpus.root.utils.custompatch.pat.clear();
                                com.chelpus.root.utils.custompatch.tag = 200;
                                com.chelpus.root.utils.custompatch.searchStr = "";
                            case 4:
                            case 5:
                            default:
                                break;
                            case 4:
                            case 5:
                                break;
                            case 6:
                                com.chelpus.root.utils.custompatch.convert = 0;
                                if ((com.chelpus.root.utils.custompatch.pat.size() > 0) && ((com.chelpus.root.utils.custompatch.arrayFile2 != null) && (com.chelpus.root.utils.custompatch.arrayFile2.size() > 0))) {
                                    boolean v2_321 = com.chelpus.root.utils.custompatch.arrayFile2.iterator();
                                    while (v2_321.hasNext()) {
                                        com.chelpus.root.utils.custompatch.localFile2 = ((java.io.File) v2_321.next());
                                        com.chelpus.root.utils.custompatch.addToLog("--------------------------------");
                                        com.chelpus.root.utils.custompatch.addToLog(new StringBuilder().append("Patch for (armeabi) libraries \n").append(com.chelpus.root.utils.custompatch.localFile2.getName()).append(":").toString());
                                        com.chelpus.root.utils.custompatch.addToLog("--------------------------------\n");
                                        if (!com.chelpus.root.utils.custompatch.searchStr.equals("")) {
                                            com.chelpus.root.utils.custompatch.addToLog(com.chelpus.root.utils.custompatch.searchStr);
                                        }
                                        v60 = com.chelpus.root.utils.custompatch.patchProcess(com.chelpus.root.utils.custompatch.pat);
                                        if (v60 == 0) {
                                            v63 = 0;
                                        }
                                    }
                                    com.chelpus.root.utils.custompatch.multilib_patch = 0;
                                    com.chelpus.root.utils.custompatch.goodResult = 0;
                                    com.chelpus.root.utils.custompatch.arrayFile2.clear();
                                }
                                com.chelpus.root.utils.custompatch.ser.clear();
                                com.chelpus.root.utils.custompatch.pat.clear();
                                com.chelpus.root.utils.custompatch.tag = 200;
                                com.chelpus.root.utils.custompatch.searchStr = "";
                                break;
                            case 7:
                                com.chelpus.root.utils.custompatch.convert = 0;
                                if ((com.chelpus.root.utils.custompatch.pat.size() > 0) && ((com.chelpus.root.utils.custompatch.arrayFile2 != null) && (com.chelpus.root.utils.custompatch.arrayFile2.size() > 0))) {
                                    boolean v2_306 = com.chelpus.root.utils.custompatch.arrayFile2.iterator();
                                    while (v2_306.hasNext()) {
                                        com.chelpus.root.utils.custompatch.localFile2 = ((java.io.File) v2_306.next());
                                        com.chelpus.root.utils.custompatch.addToLog("---------------------------------------");
                                        com.chelpus.root.utils.custompatch.addToLog(new StringBuilder().append("Patch for (armeabi-v7a) libraries \n").append(com.chelpus.root.utils.custompatch.localFile2.getName()).append(":").toString());
                                        com.chelpus.root.utils.custompatch.addToLog("---------------------------------------\n");
                                        if (!com.chelpus.root.utils.custompatch.searchStr.equals("")) {
                                            com.chelpus.root.utils.custompatch.addToLog(com.chelpus.root.utils.custompatch.searchStr);
                                        }
                                        v60 = com.chelpus.root.utils.custompatch.patchProcess(com.chelpus.root.utils.custompatch.pat);
                                        if (v60 == 0) {
                                            v63 = 0;
                                        }
                                    }
                                    com.chelpus.root.utils.custompatch.multilib_patch = 0;
                                    com.chelpus.root.utils.custompatch.goodResult = 0;
                                    com.chelpus.root.utils.custompatch.arrayFile2.clear();
                                }
                                com.chelpus.root.utils.custompatch.ser.clear();
                                com.chelpus.root.utils.custompatch.pat.clear();
                                com.chelpus.root.utils.custompatch.tag = 200;
                                com.chelpus.root.utils.custompatch.searchStr = "";
                                break;
                            case 8:
                                com.chelpus.root.utils.custompatch.convert = 0;
                                if ((com.chelpus.root.utils.custompatch.pat.size() > 0) && ((com.chelpus.root.utils.custompatch.arrayFile2 != null) && (com.chelpus.root.utils.custompatch.arrayFile2.size() > 0))) {
                                    boolean v2_291 = com.chelpus.root.utils.custompatch.arrayFile2.iterator();
                                    while (v2_291.hasNext()) {
                                        com.chelpus.root.utils.custompatch.localFile2 = ((java.io.File) v2_291.next());
                                        com.chelpus.root.utils.custompatch.addToLog("---------------------------");
                                        com.chelpus.root.utils.custompatch.addToLog(new StringBuilder().append("Patch for (MIPS) libraries \n").append(com.chelpus.root.utils.custompatch.localFile2.getName()).append(":").toString());
                                        com.chelpus.root.utils.custompatch.addToLog("---------------------------\n");
                                        if (!com.chelpus.root.utils.custompatch.searchStr.equals("")) {
                                            com.chelpus.root.utils.custompatch.addToLog(com.chelpus.root.utils.custompatch.searchStr);
                                        }
                                        v60 = com.chelpus.root.utils.custompatch.patchProcess(com.chelpus.root.utils.custompatch.pat);
                                        if (v60 == 0) {
                                            v63 = 0;
                                        }
                                    }
                                    com.chelpus.root.utils.custompatch.multilib_patch = 0;
                                    com.chelpus.root.utils.custompatch.goodResult = 0;
                                    com.chelpus.root.utils.custompatch.arrayFile2.clear();
                                }
                                com.chelpus.root.utils.custompatch.ser.clear();
                                com.chelpus.root.utils.custompatch.pat.clear();
                                com.chelpus.root.utils.custompatch.tag = 200;
                                com.chelpus.root.utils.custompatch.searchStr = "";
                                break;
                            case 9:
                                com.chelpus.root.utils.custompatch.convert = 0;
                                if ((com.chelpus.root.utils.custompatch.pat.size() > 0) && ((com.chelpus.root.utils.custompatch.arrayFile2 != null) && (com.chelpus.root.utils.custompatch.arrayFile2.size() > 0))) {
                                    boolean v2_276 = com.chelpus.root.utils.custompatch.arrayFile2.iterator();
                                    while (v2_276.hasNext()) {
                                        com.chelpus.root.utils.custompatch.localFile2 = ((java.io.File) v2_276.next());
                                        com.chelpus.root.utils.custompatch.addToLog("---------------------------");
                                        com.chelpus.root.utils.custompatch.addToLog(new StringBuilder().append("Patch for (x86) libraries \n").append(com.chelpus.root.utils.custompatch.localFile2.getName()).append(":").toString());
                                        com.chelpus.root.utils.custompatch.addToLog("---------------------------\n");
                                        if (!com.chelpus.root.utils.custompatch.searchStr.equals("")) {
                                            com.chelpus.root.utils.custompatch.addToLog(com.chelpus.root.utils.custompatch.searchStr);
                                        }
                                        v60 = com.chelpus.root.utils.custompatch.patchProcess(com.chelpus.root.utils.custompatch.pat);
                                        if (v60 == 0) {
                                            v63 = 0;
                                        }
                                    }
                                    com.chelpus.root.utils.custompatch.multilib_patch = 0;
                                    com.chelpus.root.utils.custompatch.goodResult = 0;
                                    com.chelpus.root.utils.custompatch.arrayFile2.clear();
                                }
                                com.chelpus.root.utils.custompatch.ser.clear();
                                com.chelpus.root.utils.custompatch.pat.clear();
                                com.chelpus.root.utils.custompatch.tag = 200;
                                com.chelpus.root.utils.custompatch.searchStr = "";
                                break;
                            case 10:
                                com.chelpus.root.utils.custompatch.convert = 0;
                                if (com.chelpus.root.utils.custompatch.pat.size() > 0) {
                                    com.chelpus.root.utils.custompatch.addToLog("---------------------------------");
                                    com.chelpus.root.utils.custompatch.addToLog("Dalvik-cache fixed to odex!\nPatch for odex:");
                                    com.chelpus.root.utils.custompatch.addToLog("---------------------------------\n");
                                    if (!com.chelpus.root.utils.custompatch.searchStr.equals("")) {
                                        com.chelpus.root.utils.custompatch.addToLog(com.chelpus.root.utils.custompatch.searchStr);
                                    }
                                    v60 = com.chelpus.root.utils.custompatch.patchProcess(com.chelpus.root.utils.custompatch.pat);
                                    if (v60 == 0) {
                                        v63 = 0;
                                    }
                                }
                                com.chelpus.root.utils.custompatch.addToLog("---------------------------------");
                                com.chelpus.root.utils.custompatch.addToLog("Dalvik-cache fixed to odex!");
                                com.chelpus.root.utils.custompatch.addToLog("---------------------------------\n");
                                if (!com.chelpus.root.utils.custompatch.searchStr.equals("")) {
                                    com.chelpus.root.utils.custompatch.addToLog(com.chelpus.root.utils.custompatch.searchStr);
                                }
                                if (!com.chelpus.root.utils.custompatch.unpack) {
                                    com.chelpus.root.utils.custompatch.searchDalvik(p73[2]);
                                }
                                com.chelpus.root.utils.custompatch.searchDalvikOdex(p73[0], p73[2]);
                                if (com.chelpus.root.utils.custompatch.localFile2.exists()) {
                                    v21 = 1;
                                    v22 = com.chelpus.root.utils.custompatch.localFile2.getAbsolutePath();
                                }
                                com.chelpus.root.utils.custompatch.ser.clear();
                                com.chelpus.root.utils.custompatch.pat.clear();
                                com.chelpus.root.utils.custompatch.tag = 200;
                                com.chelpus.root.utils.custompatch.searchStr = "";
                                break;
                            case 11:
                                com.chelpus.root.utils.custompatch.convert = 0;
                                com.chelpus.root.utils.custompatch.addToLog("---------------------------");
                                com.chelpus.root.utils.custompatch.addToLog("Patch for odex:");
                                com.chelpus.root.utils.custompatch.addToLog("---------------------------\n");
                                if (!com.chelpus.root.utils.custompatch.searchStr.equals("")) {
                                    com.chelpus.root.utils.custompatch.addToLog(com.chelpus.root.utils.custompatch.searchStr);
                                }
                                com.chelpus.root.utils.custompatch.localFile2 = new java.io.File(com.chelpus.Utils.getPlaceForOdex(com.chelpus.root.utils.custompatch.dirapp, 1));
                                if (!com.chelpus.root.utils.custompatch.localFile2.exists()) {
                                    com.chelpus.root.utils.custompatch.addToLog("Odex not found! Please use befor other Patch, and after run Custom Patch!");
                                }
                                if (com.chelpus.root.utils.custompatch.localFile2.length() == 0) {
                                    com.chelpus.root.utils.custompatch.localFile2.delete();
                                    com.chelpus.root.utils.custompatch.addToLog("Odex not found! Please use befor other Patch, and after run Custom Patch!");
                                }
                                com.chelpus.root.utils.custompatch.unpack = 0;
                                com.chelpus.root.utils.custompatch.odex = 0;
                                if (com.chelpus.root.utils.custompatch.pat.size() > 0) {
                                    v60 = com.chelpus.root.utils.custompatch.patchProcess(com.chelpus.root.utils.custompatch.pat);
                                    if (v60 == 0) {
                                        v63 = 0;
                                    }
                                }
                                com.chelpus.root.utils.custompatch.ser.clear();
                                com.chelpus.root.utils.custompatch.pat.clear();
                                com.chelpus.root.utils.custompatch.tag = 200;
                                com.chelpus.root.utils.custompatch.searchStr = "";
                                break;
                            case 12:
                                com.chelpus.root.utils.custompatch.convert = 0;
                                com.chelpus.root.utils.custompatch.ser.clear();
                                com.chelpus.root.utils.custompatch.pat.clear();
                                com.chelpus.root.utils.custompatch.tag = 200;
                                com.chelpus.root.utils.custompatch.searchStr = "";
                                break;
                            case 13:
                                v55 = 0;
                                if (com.chelpus.root.utils.custompatch.localFile2.exists()) {
                                    com.chelpus.root.utils.custompatch.addToLog("---------------------------");
                                    com.chelpus.root.utils.custompatch.addToLog(new StringBuilder().append("Set permissions ").append(v56).append(" for file:\n").append(v33).toString());
                                    com.chelpus.root.utils.custompatch.addToLog("---------------------------\n");
                                    boolean v2_210 = new String[3];
                                    v2_210[0] = "chmod";
                                    v2_210[1] = new StringBuilder().append("").append(v56).toString();
                                    v2_210[2] = com.chelpus.root.utils.custompatch.localFile2.getAbsolutePath();
                                    com.chelpus.Utils.run_all_no_root(v2_210);
                                }
                                break;
                            case 14:
                                com.chelpus.root.utils.custompatch.addToLog("---------------------------");
                                com.chelpus.root.utils.custompatch.addToLog("Patch for file from apk:\n");
                                com.chelpus.root.utils.custompatch.addToLog("---------------------------\n");
                                com.chelpus.root.utils.custompatch.addToLog("You must run rebuild for this application with custom patch, then patch will work.\n");
                                break;
                            case 15:
                                v20 = 0;
                                java.io.File v27 = com.chelpus.Utils.getDirs(new java.io.File(v54));
                                v27.mkdirs();
                                boolean v2_159 = new String[3];
                                v2_159[0] = "chmod";
                                v2_159[1] = "777";
                                v2_159[2] = v27.getAbsolutePath();
                                com.chelpus.Utils.run_all_no_root(v2_159);
                                v27.getAbsolutePath();
                                com.chelpus.root.utils.custompatch.addToLog("---------------------------");
                                com.chelpus.root.utils.custompatch.addToLog(new StringBuilder().append("Copy file ").append(v33).append(" to:\n").append(v54).toString());
                                com.chelpus.root.utils.custompatch.addToLog("---------------------------\n");
                                if (new java.io.File(new StringBuilder().append(com.chelpus.root.utils.custompatch.sddir).append("/").append(v33).toString()).exists()) {
                                    com.chelpus.Utils.copyFile(new java.io.File(new StringBuilder().append(com.chelpus.root.utils.custompatch.sddir).append("/").append(v33).toString()), new java.io.File(v54));
                                    if ((!new java.io.File(v54).exists()) || (new java.io.File(v54).length() != new java.io.File(new StringBuilder().append(com.chelpus.root.utils.custompatch.sddir).append("/").append(v33).toString()).length())) {
                                        com.chelpus.root.utils.custompatch.addToLog("File copied with error. Try again.");
                                    } else {
                                        com.chelpus.root.utils.custompatch.addToLog("File copied success.");
                                    }
                                    boolean v2_185 = new String[3];
                                    v2_185[0] = "chmod";
                                    v2_185[1] = "777";
                                    v2_185[2] = v54;
                                    com.chelpus.Utils.run_all_no_root(v2_185);
                                } else {
                                    com.chelpus.root.utils.custompatch.addToLog(new StringBuilder().append("Error: File ").append(v33).append(" not found to dir\n").append(com.chelpus.root.utils.custompatch.sddir).append("/").toString());
                                }
                                break;
                        }
                    }
                    if (v65[v57].toUpperCase().contains("[BEGIN]")) {
                        com.chelpus.root.utils.custompatch.tag = 0;
                        v14 = 1;
                    }
                    if (v65[v57].toUpperCase().contains("[CLASSES]")) {
                        com.chelpus.root.utils.custompatch.tag = 1;
                        if (com.chelpus.root.utils.custompatch.unpack) {
                            com.chelpus.root.utils.custompatch.localFile2 = new java.io.File(new StringBuilder().append(com.chelpus.root.utils.custompatch.dir).append("/").append("classes.dex").toString());
                        }
                    }
                    if (v65[v57].toUpperCase().contains("[ODEX]")) {
                        com.chelpus.root.utils.custompatch.tag = 10;
                    }
                    if (v65[v57].toUpperCase().contains("[SQLITE]")) {
                        com.chelpus.root.utils.custompatch.tag = 12;
                    }
                    if (com.chelpus.root.utils.custompatch.tag == 12) {
                        com.chelpus.root.utils.custompatch.ser.clear();
                        com.chelpus.root.utils.custompatch.pat.clear();
                        if (v65[v57].contains("database")) {
                            try {
                                org.json.JSONObject v40_0 = new org.json.JSONObject;
                                v40_0(v65[v57]);
                                String v66_0 = v40_0.getString("database");
                                java.io.File v25 = new java.io.File;
                                v25(v66_0);
                                boolean v2_439 = new String[3];
                                v2_439[0] = "chmod";
                                v2_439[1] = "777";
                                v2_439[2] = v66_0;
                                com.chelpus.Utils.run_all_no_root(v2_439);
                            } catch (Exception v29) {
                                com.chelpus.root.utils.custompatch.addToLog("Error LP: Error Name of Database read!");
                            }
                            if (!com.chelpus.root.utils.custompatch.withFramework) {
                                com.chelpus.Utils.copyFile(v25, new java.io.File(new StringBuilder().append(com.chelpus.root.utils.custompatch.dir).append("/").append(v25.getName()).toString()));
                            }
                            if (v25.exists()) {
                                com.chelpus.root.utils.custompatch.dataBaseExist = 1;
                            } else {
                                com.chelpus.root.utils.custompatch.localFile2 = 0;
                                com.chelpus.root.utils.custompatch.searchfile(p73[0], v25.getName());
                                if ((com.chelpus.root.utils.custompatch.localFile2 == null) || (!com.chelpus.root.utils.custompatch.localFile2.exists())) {
                                } else {
                                    v25 = com.chelpus.root.utils.custompatch.localFile2;
                                    boolean v2_452 = new String[3];
                                    v2_452[0] = "chmod";
                                    v2_452[1] = "777";
                                    v2_452[2] = v66_0;
                                    com.chelpus.Utils.run_all_no_root(v2_452);
                                    if (!com.chelpus.root.utils.custompatch.withFramework) {
                                        com.chelpus.Utils.copyFile(v25, new java.io.File(new StringBuilder().append(com.chelpus.root.utils.custompatch.dir).append("/").append(v25.getName()).toString()));
                                    }
                                    com.chelpus.root.utils.custompatch.dataBaseExist = 1;
                                }
                            }
                            if (com.chelpus.root.utils.custompatch.dataBaseExist) {
                                com.chelpus.root.utils.custompatch.dataBase = v25.getAbsolutePath();
                            }
                            com.chelpus.root.utils.custompatch.addToLog("---------------------------");
                            com.chelpus.root.utils.custompatch.addToLog(new StringBuilder().append("Open SqLite database\n").append(v25.getAbsolutePath()).toString());
                            com.chelpus.root.utils.custompatch.addToLog("---------------------------\n");
                        }
                        if ((v65[v57].contains("execute")) && (com.chelpus.root.utils.custompatch.dataBaseExist)) {
                            try {
                                org.json.JSONObject v40_1 = new org.json.JSONObject;
                                v40_1(v65[v57]);
                                String v66_1 = v40_1.getString("execute");
                                com.chelpus.root.utils.custompatch.addToLog(new StringBuilder().append("Execute:\n").append(v66_1).toString());
                            } catch (Exception v29) {
                                com.chelpus.root.utils.custompatch.addToLog("Error LP: Error SQL exec read!");
                            }
                            if (com.chelpus.root.utils.custompatch.withFramework) {
                                try {
                                    android.database.sqlite.SQLiteDatabase v26 = android.database.sqlite.SQLiteDatabase.openDatabase(com.chelpus.root.utils.custompatch.dataBase, 0, 0);
                                    v26.execSQL(v66_1);
                                    v26.close();
                                } catch (Exception v29_3) {
                                    System.out.println(new StringBuilder().append("LuckyPatcher: SQL error - ").append(v29_3).toString());
                                }
                            }
                        }
                    }
                    if (v53 != 0) {
                        com.chelpus.root.utils.custompatch.ser.clear();
                        com.chelpus.root.utils.custompatch.pat.clear();
                        org.json.JSONObject v40_2 = new org.json.JSONObject;
                        v40_2(v65[v57]);
                        String v66_2 = v40_2.getString("name");
                        com.chelpus.root.utils.custompatch.addToLog("---------------------------");
                        com.chelpus.root.utils.custompatch.addToLog(new StringBuilder().append("Patch for file \n").append(v66_2).append(":").toString());
                        com.chelpus.root.utils.custompatch.addToLog("---------------------------\n");
                        com.chelpus.root.utils.custompatch.searchfile(p73[0], v66_2);
                        v53 = 0;
                    }
                    if (v55 != 0) {
                        if (v65[v57].contains("file_name")) {
                            org.json.JSONObject v40_3 = new org.json.JSONObject;
                            v40_3(v65[v57]);
                            v33 = v40_3.getString("file_name");
                            com.chelpus.root.utils.custompatch.searchfile(p73[0], v33);
                        }
                        if ((v65[v57].contains("permissions")) && ((v65[v57].contains("{")) && ((!v65[v57].contains("[")) && (!v65[v57].contains("]"))))) {
                            org.json.JSONObject v40_4 = new org.json.JSONObject;
                            v40_4(v65[v57]);
                            v56 = v40_4.getString("permissions");
                        }
                    }
                    if (v20 != 0) {
                        if (v65[v57].contains("file_name")) {
                            org.json.JSONObject v40_5 = new org.json.JSONObject;
                            v40_5(v65[v57]);
                            v33 = v40_5.getString("file_name");
                        }
                        if ((v65[v57].contains("\"to\":")) && ((v65[v57].contains("{")) && ((!v65[v57].contains("[")) && (!v65[v57].contains("]"))))) {
                            org.json.JSONObject v40_6 = new org.json.JSONObject;
                            v40_6(v65[v57]);
                            v54 = v40_6.getString("to");
                        }
                    }
                    if (v43 != 0) {
                        com.chelpus.root.utils.custompatch.ser.clear();
                        com.chelpus.root.utils.custompatch.pat.clear();
                        org.json.JSONObject v40_7 = new org.json.JSONObject;
                        v40_7(v65[v57]);
                        String v66_3 = v40_7.getString("name");
                        com.chelpus.root.utils.custompatch.arrayFile2.clear();
                        com.chelpus.root.utils.custompatch.arrayFile2 = com.chelpus.root.utils.custompatch.searchlib(p73[0], v66_3, p73[2]);
                        v43 = 0;
                    }
                    if (v65[v57].toUpperCase().contains("[LIB]")) {
                        com.chelpus.root.utils.custompatch.tag = 2;
                        v43 = 1;
                        v53 = 0;
                        v55 = 0;
                        com.chelpus.root.utils.custompatch.odex = 0;
                        com.chelpus.root.utils.custompatch.unpack = 0;
                        v20 = 0;
                    }
                    if (v65[v57].toUpperCase().contains("[LIB-ARMEABI]")) {
                        if ((!p73[7].toLowerCase().equals("armeabi")) && ((!p73[7].toLowerCase().equals("armeabi-v7a")) || (com.chelpus.root.utils.custompatch.armv7))) {
                            com.chelpus.root.utils.custompatch.tag = 200;
                        } else {
                            com.chelpus.root.utils.custompatch.tag = 6;
                            v43 = 1;
                            v53 = 0;
                            v55 = 0;
                            com.chelpus.root.utils.custompatch.odex = 0;
                            com.chelpus.root.utils.custompatch.unpack = 0;
                            v20 = 0;
                        }
                    }
                    if (v65[v57].toUpperCase().contains("[LIB-ARMEABI-V7A]")) {
                        if (!p73[7].toLowerCase().equals("armeabi-v7a")) {
                            com.chelpus.root.utils.custompatch.tag = 200;
                        } else {
                            com.chelpus.root.utils.custompatch.tag = 7;
                            v43 = 1;
                            v53 = 0;
                            v55 = 0;
                            com.chelpus.root.utils.custompatch.odex = 0;
                            com.chelpus.root.utils.custompatch.unpack = 0;
                            v20 = 0;
                        }
                    }
                    if (v65[v57].toUpperCase().contains("[LIB-MIPS]")) {
                        if (!p73[7].toLowerCase().equals("mips")) {
                            com.chelpus.root.utils.custompatch.tag = 200;
                        } else {
                            com.chelpus.root.utils.custompatch.tag = 8;
                            v43 = 1;
                            v53 = 0;
                            v55 = 0;
                            com.chelpus.root.utils.custompatch.odex = 0;
                            com.chelpus.root.utils.custompatch.unpack = 0;
                            v20 = 0;
                        }
                    }
                    if (v65[v57].toUpperCase().contains("[LIB-X86]")) {
                        if (!p73[7].toLowerCase().equals("x86")) {
                            com.chelpus.root.utils.custompatch.tag = 200;
                        } else {
                            com.chelpus.root.utils.custompatch.tag = 9;
                            v43 = 1;
                            v53 = 0;
                            v55 = 0;
                            com.chelpus.root.utils.custompatch.odex = 0;
                            com.chelpus.root.utils.custompatch.unpack = 0;
                            v20 = 0;
                        }
                    }
                    if (v65[v57].toUpperCase().contains("[OTHER FILES]")) {
                        com.chelpus.root.utils.custompatch.tag = 3;
                        v43 = 0;
                        v53 = 1;
                        v55 = 0;
                        com.chelpus.root.utils.custompatch.odex = 0;
                        com.chelpus.root.utils.custompatch.unpack = 0;
                        v20 = 0;
                    }
                    if (v65[v57].toUpperCase().contains("[SET_PERMISSIONS]")) {
                        com.chelpus.root.utils.custompatch.tag = 13;
                        v43 = 0;
                        v53 = 0;
                        v55 = 1;
                        com.chelpus.root.utils.custompatch.odex = 0;
                        com.chelpus.root.utils.custompatch.unpack = 0;
                        v20 = 0;
                    }
                    if (v65[v57].toUpperCase().contains("[COPY_FILE]")) {
                        com.chelpus.root.utils.custompatch.tag = 15;
                        v43 = 0;
                        v53 = 0;
                        v55 = 0;
                        com.chelpus.root.utils.custompatch.odex = 0;
                        com.chelpus.root.utils.custompatch.unpack = 0;
                        v20 = 1;
                    }
                    if (v65[v57].toUpperCase().contains("[ODEX-PATCH]")) {
                        com.chelpus.root.utils.custompatch.tag = 11;
                        com.chelpus.root.utils.custompatch.unpack = 0;
                        v20 = 0;
                        v43 = 0;
                        v53 = 0;
                        v55 = 0;
                        com.chelpus.root.utils.custompatch.odex = 1;
                    }
                    if (v65[v57].toUpperCase().contains("[FILE_IN_APK]")) {
                        com.chelpus.root.utils.custompatch.tag = 14;
                        com.chelpus.root.utils.custompatch.unpack = 0;
                        v43 = 0;
                        v53 = 0;
                        v55 = 0;
                        com.chelpus.root.utils.custompatch.odex = 0;
                        v20 = 0;
                    }
                    if ((v65[v57].contains("group")) && ((v65[v57].contains("{")) && (v65[v57].contains("}")))) {
                        org.json.JSONObject v40_8 = new org.json.JSONObject;
                        v40_8(v65[v57]);
                        com.chelpus.root.utils.custompatch.group = v40_8.getString("group");
                    }
                    if ((v65[v57].contains("original")) && ((v65[v57].contains("{")) && (v65[v57].contains("}")))) {
                        if (v48 != 0) {
                            v63 = com.chelpus.root.utils.custompatch.searchProcess(com.chelpus.root.utils.custompatch.ser);
                            v48 = 0;
                        }
                        try {
                            org.json.JSONObject v40_9 = new org.json.JSONObject;
                            v40_9(v65[v57]);
                            String v66_4 = v40_9.getString("original");
                        } catch (Exception v29) {
                            com.chelpus.root.utils.custompatch.addToLog("Error LP: Error original hex read!");
                        }
                        String v66_5 = v66_4.trim();
                        if (com.chelpus.root.utils.custompatch.convert) {
                            v66_5 = com.chelpus.Utils.rework(v66_5);
                        }
                        v66_5.split("[ \t]+");
                        String[] v52_1 = v66_5.split("[ \t]+");
                        v4 = new int[v52_1.length];
                        v3 = new byte[v52_1.length];
                        int v64_0 = 0;
                        try {
                            while (v64_0 < v52_1.length) {
                                if ((v52_1[v64_0].contains("*")) && (!v52_1[v64_0].contains("**"))) {
                                    v31 = 1;
                                    v52_1[v64_0] = "60";
                                }
                                if ((!v52_1[v64_0].contains("**")) && (!v52_1[v64_0].matches("\\?+"))) {
                                    v4[v64_0] = 0;
                                } else {
                                    v52_1[v64_0] = "60";
                                    v4[v64_0] = 1;
                                }
                                if ((v52_1[v64_0].contains("W")) || ((v52_1[v64_0].contains("w")) || ((v52_1[v64_0].contains("R")) || (v52_1[v64_0].contains("r"))))) {
                                    v4[v64_0] = (Integer.valueOf(v52_1[v64_0].toLowerCase().replace("w", "").replace("r", "")).intValue() + 2);
                                    v52_1[v64_0] = "60";
                                }
                                v3[v64_0] = Integer.valueOf(v52_1[v64_0], 16).byteValue();
                                v64_0++;
                            }
                        } catch (Exception v29_6) {
                            com.chelpus.root.utils.custompatch.addToLog(new StringBuilder().append(" ").append(v29_6).toString());
                        }
                    }
                    if ((v65[v57].contains("\"object\"")) && ((v65[v57].contains("{")) && (v65[v57].contains("}")))) {
                        try {
                            org.json.JSONObject v40_10 = new org.json.JSONObject;
                            v40_10(v65[v57]);
                            String v68_0 = v40_10.getString("object");
                        } catch (Exception v29) {
                            com.chelpus.root.utils.custompatch.addToLog("Error LP: Error number by object!");
                        }
                        Process v47 = Runtime.getRuntime().exec(new StringBuilder().append("dalvikvm -Xverify:none -Xdexopt:none -cp ").append(p73[5]).append(" ").append(p73[8]).append(".nerorunpatch ").append(p73[0]).append(" ").append("object").append(v68_0).append("\n").toString());
                        v47.waitFor();
                        java.io.DataInputStream v46 = new java.io.DataInputStream;
                        v46(v47.getInputStream());
                        byte[] v13 = new byte[v46.available()];
                        v46.read(v13);
                        String v61 = new String;
                        v61(v13);
                        v47.destroy();
                        if (!v61.contains("Done")) {
                            com.chelpus.root.utils.custompatch.addToLog("Object not found!\n\n");
                            v63 = 0;
                        } else {
                            com.chelpus.root.utils.custompatch.addToLog("Object patched!\n\n");
                            v63 = 1;
                        }
                        com.chelpus.root.utils.custompatch.manualpatch = 1;
                    }
                    if ((v65[v57].contains("search")) && ((v65[v57].contains("{")) && (v65[v57].contains("}")))) {
                        try {
                            org.json.JSONObject v40_11 = new org.json.JSONObject;
                            v40_11(v65[v57]);
                            String v68_1 = v40_11.getString("search");
                        } catch (Exception v29) {
                            com.chelpus.root.utils.custompatch.addToLog("Error LP: Error search hex read!");
                        }
                        String v68_2 = v68_1.trim();
                        if (com.chelpus.root.utils.custompatch.convert) {
                            v68_2 = com.chelpus.Utils.rework(v68_2);
                        }
                        v68_2.split("[ \t]+");
                        String[] v52_2 = v68_2.split("[ \t]+");
                        v4 = new int[v52_2.length];
                        v3 = new byte[v52_2.length];
                        int v64_1 = 0;
                        try {
                            while (v64_1 < v52_2.length) {
                                if ((v52_2[v64_1].contains("*")) && (!v52_2[v64_1].contains("**"))) {
                                    v31 = 1;
                                    v52_2[v64_1] = "60";
                                }
                                if ((!v52_2[v64_1].contains("**")) && (!v52_2[v64_1].matches("\\?+"))) {
                                    v4[v64_1] = 0;
                                } else {
                                    v52_2[v64_1] = "60";
                                    v4[v64_1] = 1;
                                }
                                if (v52_2[v64_1].toUpperCase().contains("R")) {
                                    v4[v64_1] = (Integer.valueOf(v52_2[v64_1].replace("R", "")).intValue() + 2);
                                    v52_2[v64_1] = "60";
                                }
                                v3[v64_1] = Integer.valueOf(v52_2[v64_1], 16).byteValue();
                                v64_1++;
                            }
                        } catch (Exception v29_7) {
                            com.chelpus.root.utils.custompatch.addToLog(new StringBuilder().append(" ").append(v29_7).toString());
                        }
                        if (v31 == 0) {
                            v48 = 1;
                            try {
                                com.android.vending.billing.InAppBillingService.LUCK.SearchItem v45 = new com.android.vending.billing.InAppBillingService.LUCK.SearchItem;
                                v45(v3, v4);
                                boolean v2_751 = new byte[v3.length];
                                v45.repByte = v2_751;
                                com.chelpus.root.utils.custompatch.ser.add(v45);
                            } catch (Exception v29_8) {
                                v29_8.printStackTrace();
                                com.chelpus.root.utils.custompatch.addToLog(new StringBuilder().append(" ").append(v29_8).toString());
                            }
                        } else {
                            v60 = 0;
                            com.chelpus.root.utils.custompatch.addToLog("Error LP: Patterns to search not valid!\n");
                        }
                    }
                    if ((v65[v57].contains("replaced")) && ((v65[v57].contains("{")) && (v65[v57].contains("}")))) {
                        try {
                            org.json.JSONObject v40_12 = new org.json.JSONObject;
                            v40_12(v65[v57]);
                            String v67_0 = v40_12.getString("replaced");
                        } catch (Exception v29) {
                            com.chelpus.root.utils.custompatch.addToLog("Error LP: Error replaced hex read!");
                        }
                        String v67_1 = v67_0.trim();
                        if (com.chelpus.root.utils.custompatch.convert) {
                            v67_1 = com.chelpus.Utils.rework(v67_1);
                        }
                        v67_1.split("[ \t]+");
                        String[] v58_0 = v67_1.split("[ \t]+");
                        int[] v6_0 = new int[v58_0.length];
                        byte[] v5_0 = new byte[v58_0.length];
                        int v64_2 = 0;
                        try {
                            while (v64_2 < v58_0.length) {
                                if ((v58_0[v64_2].contains("*")) && (!v58_0[v64_2].contains("**"))) {
                                    v31 = 1;
                                    v58_0[v64_2] = "60";
                                }
                                if ((!v58_0[v64_2].contains("**")) && (!v58_0[v64_2].matches("\\?+"))) {
                                    v6_0[v64_2] = 1;
                                } else {
                                    v58_0[v64_2] = "60";
                                    v6_0[v64_2] = 0;
                                }
                                if (v58_0[v64_2].toLowerCase().contains("sq")) {
                                    v58_0[v64_2] = "60";
                                    v6_0[v64_2] = 253;
                                }
                                if ((v58_0[v64_2].contains("s1")) || (v58_0[v64_2].contains("S1"))) {
                                    v58_0[v64_2] = "60";
                                    v6_0[v64_2] = 254;
                                }
                                if ((v58_0[v64_2].contains("s0")) || (v58_0[v64_2].contains("S0"))) {
                                    v58_0[v64_2] = "60";
                                    v6_0[v64_2] = 255;
                                }
                                if ((v58_0[v64_2].contains("W")) || ((v58_0[v64_2].contains("w")) || ((v58_0[v64_2].contains("R")) || (v58_0[v64_2].contains("R"))))) {
                                    v6_0[v64_2] = (Integer.valueOf(v58_0[v64_2].toLowerCase().replace("w", "").replace("r", "")).intValue() + 2);
                                    v58_0[v64_2] = "60";
                                }
                                v5_0[v64_2] = Integer.valueOf(v58_0[v64_2], 16).byteValue();
                                v64_2++;
                            }
                        } catch (Exception v29_9) {
                            com.chelpus.root.utils.custompatch.addToLog(new StringBuilder().append(" ").append(v29_9).toString());
                        }
                        if ((v6_0.length != v4.length) || ((v3.length != v5_0.length) || ((v5_0.length < 4) || (v3.length < 4)))) {
                            v31 = 1;
                        }
                        if (v31 != 0) {
                            v60 = 0;
                            com.chelpus.root.utils.custompatch.addToLog("Error LP: Patterns original and replaced not valid!\n- Dimensions of the original hex-string and repleced must be >3.\n- Dimensions of the original hex-string and repleced must be equal.\n- Pattern hex must be: AF 11 4B ** AA **\nCheck the template file and try again!");
                        }
                        if (v31 == 0) {
                            if ((!com.chelpus.root.utils.custompatch.multilib_patch) && (!com.chelpus.root.utils.custompatch.multidex)) {
                                com.chelpus.root.utils.custompatch.pat.add(new com.android.vending.billing.InAppBillingService.LUCK.PatchesItem(v3, v4, v5_0, v6_0, com.chelpus.root.utils.custompatch.group, 0));
                            } else {
                                com.chelpus.root.utils.custompatch.pat.add(new com.android.vending.billing.InAppBillingService.LUCK.PatchesItem(v3, v4, v5_0, v6_0, "all_lib", 0));
                            }
                            com.chelpus.root.utils.custompatch.group = "";
                        }
                    }
                    if ((v65[v57].contains("insert")) && ((v65[v57].contains("{")) && (v65[v57].contains("}")))) {
                        try {
                            org.json.JSONObject v40_13 = new org.json.JSONObject;
                            v40_13(v65[v57]);
                            String v67_2 = v40_13.getString("insert");
                        } catch (Exception v29) {
                            com.chelpus.root.utils.custompatch.addToLog("Error LP: Error insert hex read!");
                        }
                        String v67_3 = v67_2.trim();
                        if (com.chelpus.root.utils.custompatch.convert) {
                            v67_3 = com.chelpus.Utils.rework(v67_3);
                        }
                        v67_3.split("[ \t]+");
                        String[] v58_1 = v67_3.split("[ \t]+");
                        int[] v6_1 = new int[v58_1.length];
                        byte[] v5_1 = new byte[v58_1.length];
                        int v64_3 = 0;
                        try {
                            while (v64_3 < v58_1.length) {
                                if ((v58_1[v64_3].contains("*")) && (!v58_1[v64_3].contains("**"))) {
                                    v31 = 1;
                                    v58_1[v64_3] = "60";
                                }
                                if ((!v58_1[v64_3].contains("**")) && (!v58_1[v64_3].matches("\\?+"))) {
                                    v6_1[v64_3] = 1;
                                } else {
                                    v58_1[v64_3] = "60";
                                    v6_1[v64_3] = 0;
                                }
                                if (v58_1[v64_3].toLowerCase().contains("sq")) {
                                    v58_1[v64_3] = "60";
                                    v6_1[v64_3] = 253;
                                }
                                if ((v58_1[v64_3].contains("s1")) || (v58_1[v64_3].contains("S1"))) {
                                    v58_1[v64_3] = "60";
                                    v6_1[v64_3] = 254;
                                }
                                if ((v58_1[v64_3].contains("s0")) || (v58_1[v64_3].contains("S0"))) {
                                    v58_1[v64_3] = "60";
                                    v6_1[v64_3] = 255;
                                }
                                if ((v58_1[v64_3].contains("W")) || ((v58_1[v64_3].contains("w")) || ((v58_1[v64_3].contains("R")) || (v58_1[v64_3].contains("R"))))) {
                                    v6_1[v64_3] = (Integer.valueOf(v58_1[v64_3].toLowerCase().replace("w", "").replace("r", "")).intValue() + 2);
                                    v58_1[v64_3] = "60";
                                }
                                v5_1[v64_3] = Integer.valueOf(v58_1[v64_3], 16).byteValue();
                                v64_3++;
                            }
                        } catch (Exception v29_10) {
                            com.chelpus.root.utils.custompatch.addToLog(new StringBuilder().append(" ").append(v29_10).toString());
                        }
                        if ((v5_1.length < 4) || (v3.length < 4)) {
                            v31 = 1;
                        }
                        if (v31 != 0) {
                            v60 = 0;
                            com.chelpus.root.utils.custompatch.addToLog("Error LP: Dimensions of the original hex-string and repleced must be >3.\n- Pattern hex must be: AF 11 4B ** AA **\nCheck the template file and try again!");
                        }
                        if (v31 == 0) {
                            com.chelpus.root.utils.custompatch.pat.add(new com.android.vending.billing.InAppBillingService.LUCK.PatchesItem(v3, v4, v5_1, v6_1, com.chelpus.root.utils.custompatch.group, 1));
                            com.chelpus.root.utils.custompatch.group = "";
                        }
                    }
                    if ((v65[v57].contains("replace_from_file")) && ((v65[v57].contains("{")) && (v65[v57].contains("}")))) {
                        org.json.JSONObject v40_14 = new org.json.JSONObject;
                        v40_14(v65[v57]);
                        String v67_4 = v40_14.getString("replace_from_file");
                        java.io.File v12_1 = new java.io.File(new StringBuilder().append(com.chelpus.Utils.getDirs(new java.io.File(p73[1]))).append("/").append(v67_4.trim()).toString());
                        int v41 = ((int) v12_1.length());
                        byte[] v5_2 = new byte[v41];
                        java.io.FileInputStream v24 = new java.io.FileInputStream;
                        v24(v12_1);
                        while (v24.read(v5_2) > 0) {
                        }
                        v24.close();
                        int[] v6_2 = new int[v41];
                        java.util.Arrays.fill(v6_2, 1);
                        if (v31 != 0) {
                            v60 = 0;
                            com.chelpus.root.utils.custompatch.addToLog("Error LP: Patterns original and replaced not valid!\n- Dimensions of the original hex-string and repleced must be >3.\n- Dimensions of the original hex-string and repleced must be equal.\n- Pattern hex must be: AF 11 4B ** AA **\nCheck the template file and try again!");
                        }
                        if (v31 == 0) {
                            com.chelpus.root.utils.custompatch.pat.add(new com.android.vending.billing.InAppBillingService.LUCK.PatchesItem(v3, v4, v5_2, v6_2, com.chelpus.root.utils.custompatch.group, 0));
                            com.chelpus.root.utils.custompatch.group = "";
                        }
                    }
                    if (v65[v57].toUpperCase().contains("[ADD-BOOT]")) {
                        com.chelpus.root.utils.custompatch.addToLog("Patch on Reboot added!");
                    }
                    if (v30 != 0) {
                        v35 = new StringBuilder().append(v35).append("\n").append(v65[v57]).toString();
                    }
                    if (v23.contains("[END]")) {
                        com.chelpus.root.utils.custompatch.tag = 4;
                        v30 = 1;
                    }
                    v57++;
                }
                v16.close();
                if (v63 != 0) {
                    com.chelpus.root.utils.custompatch.addToLog(new StringBuilder().append("").append(v35).toString());
                }
                if (v63 == 0) {
                    if (!com.chelpus.root.utils.custompatch.patchteil) {
                        com.chelpus.root.utils.custompatch.addToLog("Custom Patch not valid for this Version of the Programm or already patched. ");
                    } else {
                        com.chelpus.root.utils.custompatch.addToLog("Not all patterns are replaced, but the program can work, test it!\nCustom Patch not valid for this Version of the Programm or already patched. ");
                    }
                }
                if (v21 != 0) {
                    com.chelpus.root.utils.custompatch.addToLog(new StringBuilder().append("Changes Fix to: ").append(v22).toString());
                }
                if (com.chelpus.root.utils.custompatch.fixunpack) {
                    com.chelpus.Utils.sendFromRoot("Analise Results:");
                    int v59 = com.chelpus.Utils.create_ODEX_root(com.chelpus.root.utils.custompatch.dir, com.chelpus.root.utils.custompatch.classesFiles, com.chelpus.root.utils.custompatch.dirapp, com.chelpus.root.utils.custompatch.uid, com.chelpus.Utils.getOdexForCreate(com.chelpus.root.utils.custompatch.dirapp, com.chelpus.root.utils.custompatch.uid));
                    if (v59 != 0) {
                        com.chelpus.root.utils.custompatch.addToLog(new StringBuilder().append("Create odex error ").append(v59).toString());
                    }
                    if (v59 == 0) {
                        com.chelpus.root.utils.custompatch.addToLog(new StringBuilder().append("\n~Package reworked!~\nChanges Fix to: ").append(com.chelpus.Utils.getPlaceForOdex(p73[2], 1)).toString());
                    }
                }
                v38.close();
                v36.close();
                com.chelpus.Utils.sendFromRootCP(com.chelpus.root.utils.custompatch.log);
                com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.startUnderRoot = Boolean.valueOf(0);
                com.chelpus.Utils.exitFromRootJava();
                return;
            }
            while(true) {
                String v44 = v17.readLine();
                if (v44 == null) {
                    break;
                }
                if (v44.toUpperCase().contains("[ODEX-PATCH]")) {
                    com.chelpus.root.utils.custompatch.OdexPatch = 1;
                }
                if (v44.toUpperCase().contains("[LIB-ARMEABI-V7A]")) {
                    com.chelpus.root.utils.custompatch.armv7 = 1;
                }
            }
            v17.close();
            v37.close();
        } catch (Exception v29_13) {
            v29_13.printStackTrace();
            com.chelpus.Utils.sendFromRootCP(com.chelpus.root.utils.custompatch.log);
            com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.startUnderRoot = Boolean.valueOf(0);
            com.chelpus.Utils.exitFromRootJava();
            return;
        }
    }

    public static boolean patchProcess(java.util.ArrayList p30)
    {
        int v23 = 1;
        if ((p30 != null) && (p30.size() > 0)) {
            int v3_1 = p30.iterator();
            while (v3_1.hasNext()) {
                ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItem) v3_1.next()).result = 0;
            }
        }
        int v3_3 = new String[3];
        v3_3[0] = "chmod";
        v3_3[1] = "777";
        v3_3[2] = com.chelpus.root.utils.custompatch.localFile2.getAbsolutePath();
        com.chelpus.Utils.run_all_no_root(v3_3);
        try {
            java.nio.channels.FileChannel v2 = new java.io.RandomAccessFile(com.chelpus.root.utils.custompatch.localFile2, "rw").getChannel();
            com.chelpus.Utils.sendFromRootCP(new StringBuilder().append("Size file:").append(v2.size()).toString());
            java.nio.MappedByteBuffer v16 = v2.map(java.nio.channels.FileChannel$MapMode.READ_WRITE, 0, ((long) ((int) v2.size())));
            p30.toArray();
            java.nio.MappedByteBuffer v0_1 = new com.android.vending.billing.InAppBillingService.LUCK.PatchesItem[p30.size()];
            com.android.vending.billing.InAppBillingService.LUCK.PatchesItem[] v25_1 = ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItem[]) ((com.android.vending.billing.InAppBillingService.LUCK.PatchesItem[]) p30.toArray(v0_1)));
            int v13 = -1;
            int v26 = 0;
            int v8 = 0;
            try {
                while ((v16.hasRemaining()) && (v8 == 0)) {
                    if ((v16.position() - v26) > 512000) {
                        com.chelpus.Utils.sendFromRootCP(new StringBuilder().append("Progress size:").append(v16.position()).toString());
                        v26 = v16.position();
                    }
                    v16.position((v13 + 1));
                    v13 = v16.position();
                    byte v12 = v16.get();
                    int v17_0 = 0;
                    while (v17_0 < v25_1.length) {
                        v16.position(v13);
                        if ((v12 == v25_1[v17_0].origByte[0]) || ((v25_1[v17_0].origMask[0] == 1) || ((v25_1[v17_0].origMask[0] > 1) && (v12 == ((Byte) com.chelpus.root.utils.custompatch.search.get((v25_1[v17_0].origMask[0] - 2))).byteValue())))) {
                            if (v25_1[v17_0].repMask[0] == 0) {
                                v25_1[v17_0].repByte[0] = v12;
                            }
                            try {
                                if ((v25_1[v17_0].repMask[0] > 1) && (v25_1[v17_0].repMask[0] < 253)) {
                                    v25_1[v17_0].repByte[0] = ((Byte) com.chelpus.root.utils.custompatch.search.get((v25_1[v17_0].repMask[0] - 2))).byteValue();
                                }
                                if (v25_1[v17_0].repMask[0] == 253) {
                                    v25_1[v17_0].repByte[0] = ((byte) ((v12 & 15) + ((v12 & 15) * 16)));
                                }
                                if (v25_1[v17_0].repMask[0] == 254) {
                                    v25_1[v17_0].repByte[0] = ((byte) ((v12 & 15) + 16));
                                }
                                if (v25_1[v17_0].repMask[0] == 255) {
                                    v25_1[v17_0].repByte[0] = ((byte) (v12 & 15));
                                }
                                int v18 = 1;
                                v16.position((v13 + 1));
                                byte v27 = v16.get();
                                while (((v27 == v25_1[v17_0].origByte[v18]) || ((v25_1[v17_0].origMask[v18] > 1) && (v27 == ((Byte) com.chelpus.root.utils.custompatch.search.get((v25_1[v17_0].origMask[v18] - 2))).byteValue()))) || (v25_1[v17_0].origMask[v18] == 1)) {
                                    try {
                                        if (v25_1[v17_0].repMask[v18] != 0) {
                                            if ((v25_1[v17_0].repMask[v18] > 1) && (v25_1[v17_0].repMask[v18] < 253)) {
                                                v25_1[v17_0].repByte[v18] = ((Byte) com.chelpus.root.utils.custompatch.search.get((v25_1[v17_0].repMask[v18] - 2))).byteValue();
                                            }
                                        } else {
                                            v25_1[v17_0].repByte[v18] = v27;
                                        }
                                        if (v25_1[v17_0].repMask[0] == 253) {
                                            v25_1[v17_0].repByte[0] = ((byte) ((v12 & 15) + ((v12 & 15) * 16)));
                                        }
                                        if (v25_1[v17_0].repMask[v18] == 254) {
                                            v25_1[v17_0].repByte[v18] = ((byte) ((v27 & 15) + 16));
                                        }
                                        if (v25_1[v17_0].repMask[v18] == 255) {
                                            v25_1[v17_0].repByte[v18] = ((byte) (v27 & 15));
                                        }
                                        v18++;
                                        if (v18 != v25_1[v17_0].origByte.length) {
                                            v27 = v16.get();
                                        } else {
                                            int v15 = 0;
                                            int v4_62 = v25_1[v17_0].origMask;
                                            int v3_145 = 0;
                                            while (v3_145 < v4_62.length) {
                                                if (v4_62[v3_145] != 0) {
                                                    v3_145++;
                                                } else {
                                                    v15 = 1;
                                                    break;
                                                }
                                            }
                                            if (v15 == 0) {
                                                v8 = 1;
                                            }
                                            if (v25_1[v17_0].insert) {
                                                int v22 = v16.position();
                                                int v20 = (((int) v2.size()) - v22);
                                                byte[] v10 = new byte[v20];
                                                v16.get(v10, 0, v20);
                                                java.nio.ByteBuffer v11_0 = java.nio.ByteBuffer.wrap(v10);
                                                v2.position(((long) ((v25_1[v17_0].repByte.length - v25_1[v17_0].origByte.length) + v22)));
                                                v2.write(v11_0);
                                                v16 = v2.map(java.nio.channels.FileChannel$MapMode.READ_WRITE, 0, ((long) ((int) v2.size())));
                                                v16.position(v22);
                                            }
                                            v2.position(((long) v13));
                                            v2.write(java.nio.ByteBuffer.wrap(v25_1[v17_0].repByte));
                                            v16.force();
                                            com.chelpus.root.utils.custompatch.addToLog(new StringBuilder().append("\nPattern N").append((v17_0 + 1)).append(": Patch done! \n(Offset: ").append(Integer.toHexString(v13)).append(")\n").toString());
                                            v25_1[v17_0].result = 1;
                                            com.chelpus.root.utils.custompatch.patchteil = 1;
                                            break;
                                        }
                                    } catch (Exception v14) {
                                        int v29_2 = (v25_1[v17_0].repMask[v18] - 2);
                                        com.chelpus.root.utils.custompatch.addToLog(new StringBuilder().append("Byte N").append(v29_2).append(" not found! Please edit search pattern for byte ").append(v29_2).append(".").toString());
                                    }
                                }
                            } catch (Exception v14) {
                                int v29_0 = (v25_1[v17_0].repMask[0] - 2);
                                com.chelpus.root.utils.custompatch.addToLog(new StringBuilder().append("Byte N").append(v29_0).append(" not found! Please edit search pattern for byte ").append(v29_0).append(".").toString());
                            }
                        }
                        v17_0++;
                    }
                }
            } catch (Exception v14_0) {
                v14_0.printStackTrace();
                com.chelpus.root.utils.custompatch.addToLog("Byte by search not found! Please edit pattern for search.\n");
            } catch (int v3) {
            }
            v2.close();
            com.chelpus.Utils.sendFromRootCP("Analise Results:");
            int v17_1 = 0;
            while (v17_1 < v25_1.length) {
                if (v25_1[v17_1].result) {
                    if (((com.chelpus.root.utils.custompatch.multidex) || (com.chelpus.root.utils.custompatch.multilib_patch)) && (!v25_1[v17_1].group.equals(""))) {
                        com.chelpus.root.utils.custompatch.goodResult = 1;
                    }
                } else {
                    int v28 = 0;
                    if (!v25_1[v17_1].group.equals("")) {
                        int v4_75 = v25_1.length;
                        int v3_185 = 0;
                        while (v3_185 < v4_75) {
                            com.android.vending.billing.InAppBillingService.LUCK.PatchesItem v24 = v25_1[v3_185];
                            if ((v25_1[v17_1].group.equals(v24.group)) && (v24.result)) {
                                if ((com.chelpus.root.utils.custompatch.multidex) || (com.chelpus.root.utils.custompatch.multilib_patch)) {
                                    com.chelpus.root.utils.custompatch.goodResult = 1;
                                }
                                v28 = 1;
                            }
                            v3_185++;
                        }
                    }
                    if ((v28 == 0) && (!com.chelpus.root.utils.custompatch.goodResult)) {
                        if (!com.chelpus.root.utils.custompatch.multidex) {
                            if (!com.chelpus.root.utils.custompatch.multilib_patch) {
                                com.chelpus.root.utils.custompatch.addToLog(new StringBuilder().append("\nPattern N").append((v17_1 + 1)).append(":\nError LP: Pattern not found!\nor patch is already applied?!\n").toString());
                                v23 = 0;
                            } else {
                                if ((!com.chelpus.root.utils.custompatch.goodResult) && (com.chelpus.root.utils.custompatch.localFile2.equals(com.chelpus.root.utils.custompatch.arrayFile2.get((com.chelpus.root.utils.custompatch.arrayFile2.size() - 1))))) {
                                    com.chelpus.root.utils.custompatch.addToLog(new StringBuilder().append("\nPattern N").append((v17_1 + 1)).append(":\nError LP: Pattern not found!\nor patch is already applied?!\n").toString());
                                    v23 = 0;
                                }
                            }
                        } else {
                            if ((!com.chelpus.root.utils.custompatch.goodResult) && (com.chelpus.root.utils.custompatch.localFile2.equals(com.chelpus.root.utils.custompatch.classesFiles.get((com.chelpus.root.utils.custompatch.classesFiles.size() - 1))))) {
                                com.chelpus.root.utils.custompatch.addToLog(new StringBuilder().append("\nPattern N").append((v17_1 + 1)).append(":\nError LP: Pattern not found!\nor patch is already applied?!\n").toString());
                                v23 = 0;
                            }
                        }
                    }
                }
                v17_1++;
            }
            if ((((com.chelpus.root.utils.custompatch.tag == 1) && (!com.chelpus.root.utils.custompatch.unpack)) || (com.chelpus.root.utils.custompatch.tag == 10)) && (!com.chelpus.root.utils.custompatch.ART)) {
                com.chelpus.Utils.fixadlerOdex(com.chelpus.root.utils.custompatch.localFile2, com.chelpus.root.utils.custompatch.dirapp);
            }
            if ((com.chelpus.root.utils.custompatch.unpack) && ((!com.chelpus.root.utils.custompatch.odexpatch) && (!com.chelpus.root.utils.custompatch.OdexPatch))) {
                com.chelpus.Utils.fixadler(com.chelpus.root.utils.custompatch.localFile2);
                com.chelpus.root.utils.custompatch.fixunpack = 1;
            }
            return v23;
        } catch (Exception v14_1) {
            com.chelpus.root.utils.custompatch.addToLog(new StringBuilder().append("Exception e").append(v14_1.toString()).toString());
        } catch (int v3) {
        } catch (java.io.FileNotFoundException v21) {
            com.chelpus.root.utils.custompatch.addToLog("Error LP: Program files are not found!\nMove Program to internal storage.");
        }
    }

    public static void searchDalvik(String p6)
    {
        java.io.File v0 = com.chelpus.Utils.getFileDalvikCache(p6);
        try {
            if (v0 == null) {
                if (com.chelpus.root.utils.custompatch.odexpatch) {
                    com.chelpus.root.utils.custompatch.localFile2 = new java.io.File(com.chelpus.Utils.getPlaceForOdex(com.chelpus.root.utils.custompatch.dirapp, 1));
                }
                try {
                    if (!com.chelpus.root.utils.custompatch.localFile2.exists()) {
                        com.chelpus.root.utils.custompatch.localFile2 = new java.io.File(com.chelpus.Utils.getPlaceForOdex(com.chelpus.root.utils.custompatch.dirapp, 1));
                    }
                } catch (Exception v1) {
                    com.chelpus.root.utils.custompatch.localFile2 = new java.io.File(com.chelpus.Utils.getPlaceForOdex(com.chelpus.root.utils.custompatch.dirapp, 1));
                }
                if (!com.chelpus.root.utils.custompatch.localFile2.exists()) {
                    throw new java.io.FileNotFoundException();
                }
            } else {
                com.chelpus.root.utils.custompatch.localFile2 = v0;
            }
        } catch (java.io.FileNotFoundException v2) {
            if (!com.chelpus.root.utils.custompatch.system) {
                com.chelpus.root.utils.custompatch.addToLog("Error LP: Program files are not found!\n\nCheck the location dalvik-cache to solve problems!\n\nDefault: /data/dalvik-cache/*");
            }
        } catch (Exception v1) {
            com.chelpus.root.utils.custompatch.addToLog(new StringBuilder().append("").append(v1).toString());
        }
        return;
    }

    public static void searchDalvikOdex(String p7, String p8)
    {
        com.chelpus.root.utils.custompatch.searchDalvik(p7);
        try {
            if (com.chelpus.root.utils.custompatch.odexpatch) {
                com.chelpus.root.utils.custompatch.localFile2 = new java.io.File(com.chelpus.Utils.getPlaceForOdex(com.chelpus.root.utils.custompatch.dirapp, 1));
            } else {
                String v1 = com.chelpus.Utils.getOdexForCreate(p8, com.chelpus.root.utils.custompatch.uid);
                java.io.File v0_1 = new java.io.File(v1);
                if (v0_1.exists()) {
                    v0_1.delete();
                }
                java.io.File v0_3 = new java.io.File(v1.replace("-2", "-1"));
                if (v0_3.exists()) {
                    v0_3.delete();
                }
                java.io.File v0_5 = new java.io.File(v1.replace("-1", "-2"));
                if (v0_5.exists()) {
                    v0_5.delete();
                }
                java.io.File v0_7 = new java.io.File(v1.replace("-2", ""));
                if (v0_7.exists()) {
                    v0_7.delete();
                }
                java.io.File v0_9 = new java.io.File(v1.replace("-1", ""));
                if (v0_9.exists()) {
                    v0_9.delete();
                }
                java.io.File v0_11 = new java.io.File(v1);
                com.chelpus.Utils.copyFile(com.chelpus.root.utils.custompatch.localFile2, v0_11);
                if ((v0_11.exists()) && (v0_11.length() == com.chelpus.root.utils.custompatch.localFile2.length())) {
                    String[] v3_22 = new String[3];
                    v3_22[0] = "chmod";
                    v3_22[1] = "644";
                    v3_22[2] = v0_11.getAbsolutePath();
                    com.chelpus.Utils.run_all_no_root(v3_22);
                    String[] v3_24 = new String[3];
                    v3_24[0] = "chown";
                    v3_24[1] = new StringBuilder().append("1000.").append(com.chelpus.root.utils.custompatch.uid).toString();
                    v3_24[2] = v0_11.getAbsolutePath();
                    com.chelpus.Utils.run_all_no_root(v3_24);
                    String[] v3_26 = new String[3];
                    v3_26[0] = "chown";
                    v3_26[1] = new StringBuilder().append("1000:").append(com.chelpus.root.utils.custompatch.uid).toString();
                    v3_26[2] = v0_11.getAbsolutePath();
                    com.chelpus.Utils.run_all_no_root(v3_26);
                    com.chelpus.root.utils.custompatch.localFile2 = v0_11;
                }
            }
        } catch (Exception v2) {
            v2.printStackTrace();
            com.chelpus.root.utils.custompatch.addToLog(new StringBuilder().append("Exception e").append(v2.toString()).toString());
        }
        return;
    }

    public static boolean searchProcess(java.util.ArrayList p24)
    {
        int v19 = 1;
        com.chelpus.root.utils.custompatch.searchStr = "";
        if ((p24 != null) && (p24.size() > 0)) {
            String v3_2 = p24.iterator();
            while (v3_2.hasNext()) {
                ((com.android.vending.billing.InAppBillingService.LUCK.SearchItem) v3_2.next()).result = 0;
            }
        }
        try {
            java.nio.channels.FileChannel v2 = new java.io.RandomAccessFile(com.chelpus.root.utils.custompatch.localFile2, "rw").getChannel();
            java.nio.MappedByteBuffer v12 = v2.map(java.nio.channels.FileChannel$MapMode.READ_WRITE, 0, ((long) ((int) v2.size())));
            p24.toArray();
            int v0_1 = new com.android.vending.billing.InAppBillingService.LUCK.SearchItem[p24.size()];
            com.android.vending.billing.InAppBillingService.LUCK.SearchItem[] v20_1 = ((com.android.vending.billing.InAppBillingService.LUCK.SearchItem[]) ((com.android.vending.billing.InAppBillingService.LUCK.SearchItem[]) p24.toArray(v0_1)));
            long v16 = 0;
        } catch (java.io.FileNotFoundException v18) {
            com.chelpus.root.utils.custompatch.addToLog("Error LP: Program files are not found!\nMove Program to internal storage.");
            return v19;
        } catch (Exception v11_2) {
            com.chelpus.root.utils.custompatch.addToLog(new StringBuilder().append("Exception e").append(v11_2.toString()).toString());
            return v19;
        } catch (Exception v11_1) {
            com.chelpus.root.utils.custompatch.addToLog(new StringBuilder().append("Exception e").append(v11_1.toString()).toString());
            return v19;
        }
        try {
            while (v12.hasRemaining()) {
                int v10 = v12.position();
                byte v9 = v12.get();
                int v13_0 = 0;
                while (v13_0 < v20_1.length) {
                    v12.position(v10);
                    if ((!v20_1[v13_0].result) && ((v9 == v20_1[v13_0].origByte[0]) || (v20_1[v13_0].origMask[0] != 0))) {
                        if (v20_1[v13_0].origMask[0] != 0) {
                            v20_1[v13_0].repByte[0] = v9;
                        }
                        int v14 = 1;
                        v12.position((v10 + 1));
                        byte v21 = v12.get();
                        while (((!v20_1[v13_0].result) && (v21 == v20_1[v13_0].origByte[v14])) || (v20_1[v13_0].origMask[v14] != 0)) {
                            if (v20_1[v13_0].origMask[v14] > 0) {
                                v20_1[v13_0].repByte[v14] = v21;
                            }
                            v14++;
                            if (v14 != v20_1[v13_0].origByte.length) {
                                v21 = v12.get();
                            } else {
                                v20_1[v13_0].result = 1;
                                com.chelpus.root.utils.custompatch.patchteil = 1;
                                break;
                            }
                        }
                    }
                    v13_0++;
                }
                v12.position((v10 + 1));
                v16++;
            }
        } catch (Exception v11_0) {
            com.chelpus.root.utils.custompatch.addToLog(new StringBuilder().append("Search byte error: ").append(v11_0).toString());
        }
        v2.close();
        int v13_1 = 0;
        while (v13_1 < v20_1.length) {
            if (!v20_1[v13_1].result) {
                com.chelpus.root.utils.custompatch.searchStr = new StringBuilder().append(com.chelpus.root.utils.custompatch.searchStr).append("Bytes by serach N").append((v13_1 + 1)).append(":\nError LP: Bytes not found!").append("\n").toString();
                v19 = 0;
            }
            v13_1++;
        }
        int v13_2 = 0;
        while (v13_2 < v20_1.length) {
            if (v20_1[v13_2].result) {
                com.chelpus.root.utils.custompatch.searchStr = new StringBuilder().append(com.chelpus.root.utils.custompatch.searchStr).append("\nBytes by search N").append((v13_2 + 1)).append(":").append("\n").toString();
            }
            int v22 = 0;
            while (v22 < v20_1[v13_2].origMask.length) {
                if (v20_1[v13_2].origMask[v22] > 1) {
                    int v23 = (v20_1[v13_2].origMask[v22] - 2);
                    try {
                        com.chelpus.root.utils.custompatch.search.set(v23, Byte.valueOf(v20_1[v13_2].repByte[v22]));
                    } catch (Exception v11) {
                        com.chelpus.root.utils.custompatch.search.add(v23, Byte.valueOf(v20_1[v13_2].repByte[v22]));
                    }
                    if (v20_1[v13_2].result) {
                        byte[] v8 = new byte[1];
                        v8[0] = ((Byte) com.chelpus.root.utils.custompatch.search.get(v23)).byteValue();
                        com.chelpus.root.utils.custompatch.searchStr = new StringBuilder().append(com.chelpus.root.utils.custompatch.searchStr).append("R").append(v23).append("=").append(com.chelpus.Utils.bytesToHex(v8).toUpperCase()).append(" ").toString();
                    }
                }
                v22++;
            }
            com.chelpus.root.utils.custompatch.searchStr = new StringBuilder().append(com.chelpus.root.utils.custompatch.searchStr).append("\n").toString();
            v13_2++;
        }
        return v19;
    }

    public static void searchfile(String p11, String p12)
    {
        try {
            if ((!new java.io.File(p12).exists()) || (p12.startsWith("/mnt/sdcard"))) {
                if ((!p12.startsWith("/mnt/sdcard")) && (!p12.startsWith("/sdcard"))) {
                    if ((!p12.startsWith(new StringBuilder().append("/data/data/").append(com.chelpus.root.utils.custompatch.pkgName).append("/shared_prefs/").toString())) && ((!p12.startsWith(new StringBuilder().append("/dbdata/databases/").append(com.chelpus.root.utils.custompatch.pkgName).append("/shared_prefs/").toString())) && (!p12.startsWith("/shared_prefs/")))) {
                        com.chelpus.root.utils.custompatch.localFile2 = new java.io.File(new StringBuilder().append("/data/data/").append(p11).append(p12).toString());
                        if (!com.chelpus.root.utils.custompatch.localFile2.exists()) {
                            com.chelpus.root.utils.custompatch.localFile2 = new java.io.File(new StringBuilder().append("/mnt/asec/").append(p11).append("-1").append(p12).toString());
                        }
                        if (!com.chelpus.root.utils.custompatch.localFile2.exists()) {
                            com.chelpus.root.utils.custompatch.localFile2 = new java.io.File(new StringBuilder().append("/mnt/asec/").append(p11).append("-2").append(p12).toString());
                        }
                        if (!com.chelpus.root.utils.custompatch.localFile2.exists()) {
                            com.chelpus.root.utils.custompatch.localFile2 = new java.io.File(new StringBuilder().append("/mnt/asec/").append(p11).append(p12).toString());
                        }
                        if (!com.chelpus.root.utils.custompatch.localFile2.exists()) {
                            com.chelpus.root.utils.custompatch.localFile2 = new java.io.File(new StringBuilder().append("/sd-ext/data/").append(p11).append(p12).toString());
                        }
                        if (!com.chelpus.root.utils.custompatch.localFile2.exists()) {
                            com.chelpus.root.utils.custompatch.localFile2 = new java.io.File(new StringBuilder().append("/data/sdext2/").append(p11).append(p12).toString());
                        }
                        if (!com.chelpus.root.utils.custompatch.localFile2.exists()) {
                            com.chelpus.root.utils.custompatch.localFile2 = new java.io.File(new StringBuilder().append("/data/sdext1/").append(p11).append(p12).toString());
                        }
                        if (!com.chelpus.root.utils.custompatch.localFile2.exists()) {
                            com.chelpus.root.utils.custompatch.localFile2 = new java.io.File(new StringBuilder().append("/data/sdext/").append(p11).append(p12).toString());
                        }
                        if (!com.chelpus.root.utils.custompatch.localFile2.exists()) {
                            String v2 = new com.chelpus.Utils("fgh").findFile(new java.io.File(new StringBuilder().append("/data/data/").append(p11).toString()), p12);
                            if (!v2.equals("")) {
                                com.chelpus.root.utils.custompatch.localFile2 = new java.io.File(v2);
                            }
                        }
                        if (!com.chelpus.root.utils.custompatch.localFile2.exists()) {
                            throw new java.io.FileNotFoundException();
                        }
                    } else {
                        String v3 = "";
                        if (p12.startsWith(new StringBuilder().append("/data/data/").append(com.chelpus.root.utils.custompatch.pkgName).append("/shared_prefs/").toString())) {
                            v3 = new StringBuilder().append("/data/data/").append(com.chelpus.root.utils.custompatch.pkgName).append("/shared_prefs/").toString();
                        }
                        if (p12.startsWith(new StringBuilder().append("/dbdata/databases/").append(com.chelpus.root.utils.custompatch.pkgName).append("/shared_prefs/").toString())) {
                            v3 = new StringBuilder().append("/dbdata/databases/").append(com.chelpus.root.utils.custompatch.pkgName).append("/shared_prefs/").toString();
                        }
                        if (p12.startsWith("/shared_prefs/")) {
                            v3 = "/shared_prefs/";
                        }
                        com.chelpus.root.utils.custompatch.localFile2 = new java.io.File(p12.replace(v3, new StringBuilder().append("/data/data/").append(com.chelpus.root.utils.custompatch.pkgName).append("/shared_prefs/").toString()));
                        if (!com.chelpus.root.utils.custompatch.localFile2.exists()) {
                            com.chelpus.root.utils.custompatch.localFile2 = new java.io.File(p12.replace(v3, new StringBuilder().append("/dbdata/databases/").append(com.chelpus.root.utils.custompatch.pkgName).append("/shared_prefs/").toString()));
                        }
                        if (!com.chelpus.root.utils.custompatch.localFile2.exists()) {
                            throw new java.io.FileNotFoundException();
                        }
                    }
                } else {
                    java.util.ArrayList v5_1 = new java.util.ArrayList();
                    v5_1.add("/mnt/sdcard/");
                    v5_1.add("/storage/emulated/legacy/");
                    v5_1.add("/storage/emulated/0/");
                    v5_1.add("/storage/sdcard0/");
                    v5_1.add("/storage/sdcard/");
                    v5_1.add("/storage/sdcard1/");
                    v5_1.add("/sdcard/");
                    java.io.FileNotFoundException v7_111 = v5_1.iterator();
                    while (v7_111.hasNext()) {
                        String v4_1 = ((String) v7_111.next());
                        com.chelpus.root.utils.custompatch.localFile2 = new java.io.File(p12.replace("/mnt/sdcard/", v4_1));
                        if (com.chelpus.root.utils.custompatch.localFile2.exists()) {
                            try {
                                new java.io.File(new StringBuilder().append(v4_1).append("test.tmp").toString()).createNewFile();
                            } catch (Exception v0_0) {
                                v0_0.printStackTrace();
                                com.chelpus.root.utils.custompatch.localFile2 = new java.io.File("/figjvaja_papka");
                            }
                            if (!new java.io.File(new StringBuilder().append(v4_1).append("test.tmp").toString()).exists()) {
                                com.chelpus.root.utils.custompatch.localFile2 = new java.io.File("/figjvaja_papka");
                            } else {
                                new java.io.File(new StringBuilder().append(v4_1).append("test.tmp").toString()).delete();
                                break;
                            }
                        }
                    }
                    if (!com.chelpus.root.utils.custompatch.localFile2.exists()) {
                        throw new java.io.FileNotFoundException();
                    }
                }
            } else {
                com.chelpus.root.utils.custompatch.localFile2 = new java.io.File(p12);
            }
        } catch (java.io.FileNotFoundException v1) {
            com.chelpus.root.utils.custompatch.addToLog(new StringBuilder().append("Error LP: ").append(p12).append(" are not found!\n\nRun the application file to appear in the folder with the data.!\n").toString());
        } catch (Exception v0_1) {
            com.chelpus.root.utils.custompatch.addToLog(new StringBuilder().append("Exception e").append(v0_1.toString()).toString());
        }
        return;
    }

    public static java.util.ArrayList searchlib(String p24, String p25, String p26)
    {
        int v13_1 = new java.util.ArrayList();
        try {
            java.util.ArrayList v6_1 = new java.util.ArrayList();
            v6_1.add("/data/data/");
            v6_1.add("/mnt/asec/");
            v6_1.add("/sd-ext/data/");
            v6_1.add("/data/sdext2/");
            v6_1.add("/data/sdext1/");
            v6_1.add("/data/sdext/");
            java.util.ArrayList v16_1 = new java.util.ArrayList();
            v16_1.add(p24);
            v16_1.add(new StringBuilder().append(p24).append("-1").toString());
            v16_1.add(new StringBuilder().append(p24).append("-2").toString());
        } catch (Exception v8) {
            com.chelpus.root.utils.custompatch.addToLog(new StringBuilder().append("Exception e").append(v8.toString()).toString());
            v13_1 = 0;
            return v13_1;
        } catch (java.io.FileNotFoundException v14) {
            com.chelpus.root.utils.custompatch.addToLog(new StringBuilder().append("Error LP: ").append(com.chelpus.root.utils.custompatch.localFile2).append(" are not found!\n\nCheck the location libraries to solve problems!\n").toString());
            v13_1 = 0;
            return v13_1;
        }
        if (!p25.trim().equals("*")) {
            com.chelpus.root.utils.custompatch.localFile2 = new java.io.File(new StringBuilder().append("/data/data/").append(p24).append("/lib/").append(p25).toString());
            if (com.chelpus.root.utils.custompatch.localFile2.exists()) {
                com.chelpus.Utils.addFileToList(com.chelpus.root.utils.custompatch.localFile2, v13_1);
            } else {
                int v18_23 = v6_1.iterator();
                while (v18_23.hasNext()) {
                    String v7_2 = ((String) v18_23.next());
                    java.util.Iterator v19_57 = v16_1.iterator();
                    while (v19_57.hasNext()) {
                        java.io.File v4_1 = new java.io.File(new StringBuilder().append(v7_2).append(((String) v19_57.next())).toString());
                        if (v4_1.exists()) {
                            String v17_0 = new com.chelpus.Utils("sdf").findFile(v4_1, p25);
                            if (!v17_0.equals("")) {
                                String v20_26 = new java.io.File;
                                v20_26(v17_0);
                                com.chelpus.root.utils.custompatch.localFile2 = v20_26;
                                com.chelpus.Utils.addFileToList(com.chelpus.root.utils.custompatch.localFile2, v13_1);
                                com.chelpus.root.utils.custompatch.addToLog(new StringBuilder().append("Found lib:").append(v17_0).toString());
                            }
                        }
                    }
                }
            }
            int v18_24 = new java.io.File;
            v18_24(p26);
            String v7_0 = com.chelpus.Utils.getDirs(v18_24);
            if (new java.io.File(new StringBuilder().append(v7_0.getAbsoluteFile()).append("/lib").toString()).exists()) {
                com.chelpus.root.utils.custompatch.localFile2 = new java.io.File(new StringBuilder().append(v7_0.getAbsoluteFile()).append("/lib/arm/").append(p25).toString());
                if (com.chelpus.root.utils.custompatch.localFile2.exists()) {
                    com.chelpus.Utils.addFileToList(com.chelpus.root.utils.custompatch.localFile2, v13_1);
                }
                com.chelpus.root.utils.custompatch.localFile2 = new java.io.File(new StringBuilder().append(v7_0.getAbsoluteFile()).append("/lib/x86/").append(p25).toString());
                if (com.chelpus.root.utils.custompatch.localFile2.exists()) {
                    com.chelpus.Utils.addFileToList(com.chelpus.root.utils.custompatch.localFile2, v13_1);
                }
                com.chelpus.root.utils.custompatch.localFile2 = new java.io.File(new StringBuilder().append(v7_0.getAbsoluteFile()).append("/lib/mips/").append(p25).toString());
                if (com.chelpus.root.utils.custompatch.localFile2.exists()) {
                    com.chelpus.Utils.addFileToList(com.chelpus.root.utils.custompatch.localFile2, v13_1);
                }
            }
            if (!com.chelpus.root.utils.custompatch.localFile2.exists()) {
                com.chelpus.root.utils.custompatch.localFile2 = new java.io.File(new StringBuilder().append("/system/lib/").append(p25).toString());
            }
            if (v13_1.size() != 0) {
                int v18_48 = new java.io.File;
                v18_48(p26);
                String v12_0 = v18_48.getName().replace(p24, "").replace(".apk", "");
                if (!new java.io.File(new StringBuilder().append("/data/app-lib/").append(p24).append(v12_0).append("/").append(p25).toString()).exists()) {
                    return v13_1;
                } else {
                    com.chelpus.Utils.addFileToList(new java.io.File(new StringBuilder().append("/data/app-lib/").append(p24).append(v12_0).append("/").append(p25).toString()), v13_1);
                    return v13_1;
                }
            } else {
                throw new java.io.FileNotFoundException();
            }
        } else {
            com.chelpus.root.utils.custompatch.multilib_patch = 1;
            java.io.File[] v11_0 = new java.io.File(new StringBuilder().append("/data/data/").append(p24).append("/lib/").toString()).listFiles();
            if ((v11_0 != null) && (v11_0.length > 0)) {
                java.util.Iterator v19_64 = v11_0.length;
                int v18_62 = 0;
                while (v18_62 < v19_64) {
                    java.io.File v9_5 = v11_0[v18_62];
                    if ((v9_5.length() > 0) && (v9_5.getName().endsWith(".so"))) {
                        com.chelpus.Utils.addFileToList(v9_5, v13_1);
                    }
                    v18_62++;
                }
            }
            int v18_63 = new java.io.File;
            v18_63(p26);
            String v12_1 = v18_63.getName().replace(p24, "").replace(".apk", "");
            if (new java.io.File("/data/app-lib").exists()) {
                java.io.File[] v11_1 = new java.io.File(new StringBuilder().append("/data/app-lib/").append(p24).append(v12_1).append("/").toString()).listFiles();
                if ((v11_1 != null) && (v11_1.length > 0)) {
                    java.util.Iterator v19_75 = v11_1.length;
                    int v18_72 = 0;
                    while (v18_72 < v19_75) {
                        java.io.File v9_4 = v11_1[v18_72];
                        if ((v9_4.length() > 0) && (v9_4.getName().endsWith(".so"))) {
                            com.chelpus.Utils.addFileToList(v9_4, v13_1);
                        }
                        v18_72++;
                    }
                }
            }
            int v18_73 = v6_1.iterator();
            while (v18_73.hasNext()) {
                String v7_4 = ((String) v18_73.next());
                java.io.File v4_3 = new java.io.File(new StringBuilder().append(v7_4).append(p24).append(v12_1).append("/").toString());
                if (v4_3.exists()) {
                    com.chelpus.Utils v3_3 = new com.chelpus.Utils("sdf");
                    java.util.ArrayList v10_1 = new java.util.ArrayList();
                    String v17_1 = v3_3.findFileEndText(v4_3, ".so", v10_1);
                    if ((!v17_1.equals("")) && (v10_1.size() > 0)) {
                        java.util.Iterator v19_90 = v10_1.iterator();
                        while (v19_90.hasNext()) {
                            com.chelpus.Utils.addFileToList(((java.io.File) v19_90.next()), v13_1);
                            com.chelpus.root.utils.custompatch.addToLog(new StringBuilder().append("Found lib:").append(v17_1).toString());
                        }
                    }
                }
                java.io.File v5_1 = new java.io.File(new StringBuilder().append(v7_4).append(p24).append("/").toString());
                if (v5_1.exists()) {
                    com.chelpus.Utils v3_5 = new com.chelpus.Utils("sdf");
                    java.util.ArrayList v10_3 = new java.util.ArrayList();
                    String v17_2 = v3_5.findFileEndText(v5_1, ".so", v10_3);
                    if ((!v17_2.equals("")) && (v10_3.size() > 0)) {
                        java.util.Iterator v19_103 = v10_3.iterator();
                        while (v19_103.hasNext()) {
                            com.chelpus.Utils.addFileToList(((java.io.File) v19_103.next()), v13_1);
                            com.chelpus.root.utils.custompatch.addToLog(new StringBuilder().append("Found lib:").append(v17_2).toString());
                        }
                    }
                }
            }
            return v13_1;
        }
    }

    public static void unzip(java.io.File p22)
    {
        com.chelpus.root.utils.custompatch.classesFiles.clear();
        int v8 = 0;
        try {
            java.io.FileInputStream v6_1 = new java.io.FileInputStream(p22);
            java.util.zip.ZipInputStream v16 = new java.util.zip.ZipInputStream;
            v16(v6_1);
            java.util.zip.ZipEntry v15 = v16.getNextEntry();
        } catch (Exception v4) {
            v4.printStackTrace();
            try {
                net.lingala.zip4j.core.ZipFile v17 = new net.lingala.zip4j.core.ZipFile;
                v17(p22);
                v17.extractFile("classes.dex", com.chelpus.root.utils.custompatch.dir);
                StringBuilder v0_23 = new String[3];
                String[] v18_33 = v0_23;
                v18_33[0] = "chmod";
                v18_33[1] = "777";
                v18_33[2] = new StringBuilder().append(com.chelpus.root.utils.custompatch.dir).append("/").append("classes.dex").toString();
                com.chelpus.Utils.run_all_no_root(v18_33);
                com.chelpus.root.utils.custompatch.classesFiles.add(new java.io.File(new StringBuilder().append(com.chelpus.root.utils.custompatch.dir).append("/").append("classes.dex").toString()));
                v17.extractFile("AndroidManifest.xml", com.chelpus.root.utils.custompatch.dir);
                StringBuilder v0_25 = new String[3];
                String[] v18_37 = v0_25;
                v18_37[0] = "chmod";
                v18_37[1] = "777";
                v18_37[2] = new StringBuilder().append(com.chelpus.root.utils.custompatch.dir).append("/").append("AndroidManifest.xml").toString();
                com.chelpus.Utils.run_all_no_root(v18_37);
            } catch (Exception v5_1) {
                v5_1.printStackTrace();
                com.chelpus.root.utils.custompatch.addToLog(new StringBuilder().append("Error LP: Error classes.dex decompress! ").append(v5_1).toString());
                com.chelpus.root.utils.custompatch.addToLog(new StringBuilder().append("Exception e1").append(v4.toString()).toString());
            } catch (Exception v5_0) {
                v5_0.printStackTrace();
                com.chelpus.root.utils.custompatch.addToLog(new StringBuilder().append("Error LP: Error classes.dex decompress! ").append(v5_0).toString());
                com.chelpus.root.utils.custompatch.addToLog(new StringBuilder().append("Exception e1").append(v4.toString()).toString());
            }
            com.chelpus.root.utils.custompatch.addToLog(new StringBuilder().append("Exception e").append(v4.toString()).toString());
            return;
        }
        while ((v15 != null) && (1 != 0)) {
            String v11 = v15.getName();
            if ((v11.toLowerCase().startsWith("classes")) && ((v11.endsWith(".dex")) && (!v11.contains("/")))) {
                java.io.FileOutputStream v9_1 = new java.io.FileOutputStream(new StringBuilder().append(com.chelpus.root.utils.custompatch.dir).append("/").append(v11).toString());
                byte[] v2 = new byte[2048];
                while(true) {
                    int v12 = v16.read(v2);
                    if (v12 == -1) {
                        break;
                    }
                    v9_1.write(v2, 0, v12);
                }
                v16.closeEntry();
                v9_1.close();
                com.chelpus.root.utils.custompatch.classesFiles.add(new java.io.File(new StringBuilder().append(com.chelpus.root.utils.custompatch.dir).append("/").append(v11).toString()));
                StringBuilder v0_11 = new String[3];
                String[] v18_17 = v0_11;
                v18_17[0] = "chmod";
                v18_17[1] = "777";
                v18_17[2] = new StringBuilder().append(com.chelpus.root.utils.custompatch.dir).append("/").append(v11).toString();
                com.chelpus.Utils.run_all_no_root(v18_17);
            }
            if (v11.equals("AndroidManifest.xml")) {
                java.io.FileOutputStream v10_1 = new java.io.FileOutputStream(new StringBuilder().append(com.chelpus.root.utils.custompatch.dir).append("/").append("AndroidManifest.xml").toString());
                byte[] v3 = new byte[2048];
                while(true) {
                    int v13 = v16.read(v3);
                    if (v13 == -1) {
                        break;
                    }
                    v10_1.write(v3, 0, v13);
                }
                v16.closeEntry();
                v10_1.close();
                StringBuilder v0_19 = new String[3];
                String[] v18_29 = v0_19;
                v18_29[0] = "chmod";
                v18_29[1] = "777";
                v18_29[2] = new StringBuilder().append(com.chelpus.root.utils.custompatch.dir).append("/").append("AndroidManifest.xml").toString();
                com.chelpus.Utils.run_all_no_root(v18_29);
                v8 = 1;
            }
            if ((0 == 0) || (v8 == 0)) {
                v15 = v16.getNextEntry();
            } else {
                break;
            }
        }
        v16.close();
        v6_1.close();
        return;
    }
}
