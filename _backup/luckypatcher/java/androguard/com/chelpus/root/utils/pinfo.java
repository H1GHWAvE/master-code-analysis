package com.chelpus.root.utils;
public class pinfo {
    public static java.util.ArrayList classesFiles;
    public static String toolfilesdir;

    static pinfo()
    {
        com.chelpus.root.utils.pinfo.toolfilesdir = "";
        com.chelpus.root.utils.pinfo.classesFiles = new java.util.ArrayList();
        return;
    }

    public pinfo()
    {
        return;
    }

    public static void main(String[] p13)
    {
        com.chelpus.Utils.startRootJava(new com.chelpus.root.utils.pinfo$1());
        String v4 = p13[0];
        com.chelpus.root.utils.pinfo.toolfilesdir = p13[1];
        String v5 = p13[2];
        if ((p13[3] == null) || (!p13[3].contains("recovery"))) {
            com.chelpus.root.utils.pinfo.unzipART(new java.io.File(new StringBuilder().append(com.chelpus.root.utils.pinfo.toolfilesdir).append("/p.apk").toString()));
            String v3_0 = com.chelpus.Utils.getOdexForCreate(v4, v5);
            java.io.File v1_0 = com.chelpus.Utils.getFileDalvikCache(v4);
            if ((v1_0 != null) && (v1_0.exists())) {
                v1_0.renameTo(new java.io.File(new StringBuilder().append(v1_0.getAbsolutePath()).append("_lpbackup").toString()));
                com.chelpus.Utils.dalvikvm_copyFile(com.chelpus.root.utils.pinfo.toolfilesdir, new StringBuilder().append(v1_0.getAbsolutePath()).append("_lpbackup").toString(), v1_0.getAbsolutePath());
                java.io.File v6_12 = new String[3];
                v6_12[0] = "chmod";
                v6_12[1] = "777";
                v6_12[2] = v1_0.getAbsolutePath();
                com.chelpus.Utils.run_all_no_root(v6_12);
                java.io.File v6_13 = new String[3];
                v6_13[0] = "chown";
                v6_13[1] = "0:0";
                v6_13[2] = v1_0.getAbsolutePath();
                com.chelpus.Utils.run_all_no_root(v6_13);
                java.io.File v6_14 = new String[3];
                v6_14[0] = "chown";
                v6_14[1] = "0.0";
                v6_14[2] = v1_0.getAbsolutePath();
                com.chelpus.Utils.run_all_no_root(v6_14);
            }
            com.chelpus.Utils.remount(v3_0, "rw");
            if (new java.io.File(v3_0).exists()) {
                new java.io.File(v3_0).renameTo(new java.io.File(new StringBuilder().append(v3_0).append("_lpbackup").toString()));
            }
            com.chelpus.Utils.create_ODEX_root(com.chelpus.root.utils.pinfo.toolfilesdir, com.chelpus.root.utils.pinfo.classesFiles, v4, v5, v3_0);
        } else {
            try {
                String v3_1 = com.chelpus.Utils.getOdexForCreate(v4, v5);
                com.chelpus.Utils.remount(v3_1, "rw");
                java.io.File v1_1 = com.chelpus.Utils.getFileDalvikCache(v4);
                java.io.File v0_1 = new java.io.File(new StringBuilder().append(v1_1.getAbsolutePath()).append("_lpbackup").toString());
            } catch (Exception v2_1) {
                v2_1.printStackTrace();
            }
            if ((v0_1 == null) || (!v0_1.exists())) {
                try {
                    com.chelpus.Utils.create_DC_root(com.chelpus.root.utils.pinfo.toolfilesdir, v4, v5, com.chelpus.Utils.getOdexForCreate(v4, v5));
                } catch (Exception v2_0) {
                    v2_0.printStackTrace();
                }
            } else {
                v0_1.renameTo(v1_1);
                new java.io.File(v3_1).delete();
            }
            if (new java.io.File(new StringBuilder().append(v3_1).append("_lpbackup").toString()).exists()) {
                new java.io.File(new StringBuilder().append(v3_1).append("_lpbackup").toString()).renameTo(new java.io.File(v3_1));
            }
        }
        com.chelpus.Utils.exitFromRootJava();
        return;
    }

    public static void unzipART(java.io.File p22)
    {
        int v8 = 0;
        try {
            java.io.FileInputStream v6_1 = new java.io.FileInputStream(p22);
            java.util.zip.ZipInputStream v16 = new java.util.zip.ZipInputStream;
            v16(v6_1);
            java.util.zip.ZipEntry v15 = v16.getNextEntry();
        } catch (Exception v4) {
            try {
                net.lingala.zip4j.core.ZipFile v17 = new net.lingala.zip4j.core.ZipFile;
                v17(p22);
                v17.extractFile("classes.dex", com.chelpus.root.utils.pinfo.toolfilesdir);
                com.chelpus.root.utils.pinfo.classesFiles.add(new java.io.File(new StringBuilder().append(com.chelpus.root.utils.pinfo.toolfilesdir).append("/").append("classes.dex").toString()));
                StringBuilder v0_24 = new String[3];
                String[] v18_33 = v0_24;
                v18_33[0] = "chmod";
                v18_33[1] = "777";
                v18_33[2] = new StringBuilder().append(com.chelpus.root.utils.pinfo.toolfilesdir).append("/").append("classes.dex").toString();
                com.chelpus.Utils.cmdParam(v18_33);
                v17.extractFile("AndroidManifest.xml", com.chelpus.root.utils.pinfo.toolfilesdir);
                StringBuilder v0_26 = new String[3];
                String[] v18_36 = v0_26;
                v18_36[0] = "chmod";
                v18_36[1] = "777";
                v18_36[2] = new StringBuilder().append(com.chelpus.root.utils.pinfo.toolfilesdir).append("/").append("AndroidManifest.xml").toString();
                com.chelpus.Utils.cmdParam(v18_36);
            } catch (Exception v5_0) {
                com.chelpus.Utils.sendFromRoot(new StringBuilder().append("Error classes.dex decompress! ").append(v5_0).toString());
                com.chelpus.Utils.sendFromRoot(new StringBuilder().append("Exception e1").append(v4.toString()).toString());
            } catch (Exception v5_1) {
                com.chelpus.Utils.sendFromRoot(new StringBuilder().append("Error classes.dex decompress! ").append(v5_1).toString());
                com.chelpus.Utils.sendFromRoot(new StringBuilder().append("Exception e1").append(v4.toString()).toString());
            }
            com.chelpus.Utils.sendFromRoot(new StringBuilder().append("Exception e").append(v4.toString()).toString());
            return;
        }
        while ((v15 != null) && (1 != 0)) {
            String v11 = v15.getName();
            if ((v11.startsWith("classes")) && ((v11.endsWith(".dex")) && (!v11.contains("/")))) {
                java.io.FileOutputStream v9_1 = new java.io.FileOutputStream(new StringBuilder().append(com.chelpus.root.utils.pinfo.toolfilesdir).append("/").append(v11).toString());
                byte[] v2 = new byte[2048];
                while(true) {
                    int v12 = v16.read(v2);
                    if (v12 == -1) {
                        break;
                    }
                    v9_1.write(v2, 0, v12);
                }
                v16.closeEntry();
                v9_1.close();
                com.chelpus.root.utils.pinfo.classesFiles.add(new java.io.File(new StringBuilder().append(com.chelpus.root.utils.pinfo.toolfilesdir).append("/").append(v11).toString()));
                StringBuilder v0_12 = new String[3];
                String[] v18_16 = v0_12;
                v18_16[0] = "chmod";
                v18_16[1] = "777";
                v18_16[2] = new StringBuilder().append(com.chelpus.root.utils.pinfo.toolfilesdir).append("/").append(v11).toString();
                com.chelpus.Utils.cmdParam(v18_16);
            }
            if (v11.equals("AndroidManifest.xml")) {
                java.io.FileOutputStream v10_1 = new java.io.FileOutputStream(new StringBuilder().append(com.chelpus.root.utils.pinfo.toolfilesdir).append("/").append("AndroidManifest.xml").toString());
                byte[] v3 = new byte[2048];
                while(true) {
                    int v13 = v16.read(v3);
                    if (v13 == -1) {
                        break;
                    }
                    v10_1.write(v3, 0, v13);
                }
                v16.closeEntry();
                v10_1.close();
                StringBuilder v0_20 = new String[3];
                String[] v18_28 = v0_20;
                v18_28[0] = "chmod";
                v18_28[1] = "777";
                v18_28[2] = new StringBuilder().append(com.chelpus.root.utils.pinfo.toolfilesdir).append("/").append("AndroidManifest.xml").toString();
                com.chelpus.Utils.cmdParam(v18_28);
                v8 = 1;
            }
            if ((0 == 0) || (v8 == 0)) {
                v15 = v16.getNextEntry();
            } else {
                break;
            }
        }
        v16.close();
        v6_1.close();
        return;
    }
}
