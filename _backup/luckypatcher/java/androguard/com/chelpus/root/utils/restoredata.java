package com.chelpus.root.utils;
public class restoredata {

    public restoredata()
    {
        return;
    }

    public static void ExtractAllFilesWithInputStreams(net.lingala.zip4j.core.ZipFile p21, String p22, String p23)
    {
        net.lingala.zip4j.io.ZipInputStream v8 = 0;
        java.io.FileOutputStream v9_0 = 0;
        try {
            if (!p21.isEncrypted()) {
                java.io.PrintStream v16_2 = p21.getFileHeaders().iterator();
                int v10 = 0;
                try {
                    while (v16_2.hasNext()) {
                        net.lingala.zip4j.model.FileHeader v6 = ((net.lingala.zip4j.model.FileHeader) v16_2.next());
                        if (v6 == null) {
                            System.err.println("fileheader is null. Shouldn\'t be here");
                            v9_0 = v10;
                        } else {
                            java.io.File v11_1 = new java.io.File(new StringBuilder().append(p22).append(System.getProperty("file.separator")).append(v6.getFileName()).toString());
                            if (!v6.isDirectory()) {
                                java.io.File v14 = v11_1.getParentFile();
                                if (!v14.exists()) {
                                    v14.mkdirs();
                                    StringBuilder v0_5 = new String[3];
                                    java.io.PrintStream v17_11 = v0_5;
                                    v17_11[0] = "chmod";
                                    v17_11[1] = "771";
                                    v17_11[2] = v14.getAbsolutePath();
                                    com.chelpus.Utils.cmdParam(v17_11);
                                    StringBuilder v0_7 = new String[3];
                                    java.io.PrintStream v17_13 = v0_7;
                                    v17_13[0] = "chown";
                                    v17_13[1] = new StringBuilder().append("0:").append(p23).toString();
                                    v17_13[2] = v14.getAbsolutePath();
                                    com.chelpus.Utils.cmdParam(v17_13);
                                    StringBuilder v0_10 = new String[3];
                                    java.io.PrintStream v17_15 = v0_10;
                                    v17_15[0] = "chown";
                                    v17_15[1] = new StringBuilder().append("0.").append(p23).toString();
                                    v17_15[2] = v14.getAbsolutePath();
                                    com.chelpus.Utils.cmdParam(v17_15);
                                }
                                v8 = p21.getInputStream(v6);
                                v9_0 = new java.io.FileOutputStream(v11_1);
                                byte[] v3 = new byte[4096];
                                while(true) {
                                    int v15 = v8.read(v3);
                                    if (v15 == -1) {
                                        break;
                                    }
                                    v9_0.write(v3, 0, v15);
                                }
                                com.chelpus.root.utils.restoredata.closeFileHandlers(v8, v9_0);
                                net.lingala.zip4j.unzip.UnzipUtil.applyFileAttributes(v6, v11_1);
                                StringBuilder v0_16 = new String[3];
                                java.io.PrintStream v17_19 = v0_16;
                                v17_19[0] = "chmod";
                                v17_19[1] = "771";
                                v17_19[2] = v11_1.getAbsolutePath();
                                com.chelpus.Utils.cmdParam(v17_19);
                                StringBuilder v0_18 = new String[3];
                                java.io.PrintStream v17_21 = v0_18;
                                v17_21[0] = "chown";
                                v17_21[1] = new StringBuilder().append("0:").append(p23).toString();
                                v17_21[2] = v11_1.getAbsolutePath();
                                com.chelpus.Utils.cmdParam(v17_21);
                                StringBuilder v0_21 = new String[3];
                                java.io.PrintStream v17_23 = v0_21;
                                v17_23[0] = "chown";
                                v17_23[1] = new StringBuilder().append("0.").append(p23).toString();
                                v17_23[2] = v11_1.getAbsolutePath();
                                com.chelpus.Utils.cmdParam(v17_23);
                                System.out.println(new StringBuilder().append("Done extracting: ").append(v6.getFileName()).toString());
                            } else {
                                v11_1.mkdirs();
                                StringBuilder v0_25 = new String[3];
                                java.io.PrintStream v17_36 = v0_25;
                                v17_36[0] = "chmod";
                                v17_36[1] = "771";
                                v17_36[2] = v11_1.getAbsolutePath();
                                com.chelpus.Utils.cmdParam(v17_36);
                                StringBuilder v0_27 = new String[3];
                                java.io.PrintStream v17_38 = v0_27;
                                v17_38[0] = "chown";
                                v17_38[1] = new StringBuilder().append("0:").append(p23).toString();
                                v17_38[2] = v11_1.getAbsolutePath();
                                com.chelpus.Utils.cmdParam(v17_38);
                                StringBuilder v0_30 = new String[3];
                                java.io.PrintStream v17_40 = v0_30;
                                v17_40[0] = "chown";
                                v17_40[1] = new StringBuilder().append("0.").append(p23).toString();
                                v17_40[2] = v11_1.getAbsolutePath();
                                com.chelpus.Utils.cmdParam(v17_40);
                            }
                        }
                        v10 = v9_0;
                    }
                    try {
                        com.chelpus.root.utils.restoredata.closeFileHandlers(v8, v10);
                    } catch (java.io.IOException v5_0) {
                        v5_0.printStackTrace();
                        System.out.println("error");
                    }
                } catch (java.io.PrintStream v16_4) {
                    v9_0 = v10;
                    try {
                        com.chelpus.root.utils.restoredata.closeFileHandlers(v8, v9_0);
                    } catch (java.io.IOException v5_9) {
                        v5_9.printStackTrace();
                        System.out.println("error");
                    }
                    throw v16_4;
                } catch (java.io.IOException v5_1) {
                    v9_0 = v10;
                    v5_1.printStackTrace();
                    System.out.println("error");
                    try {
                        com.chelpus.root.utils.restoredata.closeFileHandlers(v8, v9_0);
                    } catch (java.io.IOException v5_5) {
                        v5_5.printStackTrace();
                        System.out.println("error");
                    }
                } catch (java.io.IOException v5_4) {
                    v9_0 = v10;
                    v5_4.printStackTrace();
                    System.out.println("error");
                    try {
                        com.chelpus.root.utils.restoredata.closeFileHandlers(v8, v9_0);
                    } catch (java.io.IOException v5_8) {
                        v5_8.printStackTrace();
                        System.out.println("error");
                    }
                } catch (java.io.IOException v5_2) {
                    v9_0 = v10;
                    v5_2.printStackTrace();
                    System.out.println("error");
                    try {
                        com.chelpus.root.utils.restoredata.closeFileHandlers(v8, v9_0);
                    } catch (java.io.IOException v5_6) {
                        v5_6.printStackTrace();
                        System.out.println("error");
                    }
                } catch (java.io.IOException v5_3) {
                    v9_0 = v10;
                    v5_3.printStackTrace();
                    System.out.println("error");
                    try {
                        com.chelpus.root.utils.restoredata.closeFileHandlers(v8, v9_0);
                    } catch (java.io.IOException v5_7) {
                        v5_7.printStackTrace();
                        System.out.println("error");
                    }
                }
            } else {
                p21.setPassword("password");
            }
        } catch (java.io.IOException v5_1) {
        } catch (java.io.IOException v5_3) {
        } catch (java.io.IOException v5_2) {
        } catch (java.io.PrintStream v16_4) {
        }
        return;
    }

    private static void closeFileHandlers(net.lingala.zip4j.io.ZipInputStream p0, java.io.OutputStream p1)
    {
        if (p1 != 0) {
            p1.close();
        }
        if (p0 != 0) {
            p0.close();
        }
        return;
    }

    public static void copyFolder(java.io.File p11, java.io.File p12, String p13)
    {
        int v4_0 = 0;
        if (!p11.isDirectory()) {
            com.chelpus.Utils.copyFile(p11, p12);
            int v5_1 = new String[3];
            v5_1[0] = "chmod";
            v5_1[1] = "771";
            v5_1[2] = p12.getAbsolutePath();
            com.chelpus.Utils.cmdParam(v5_1);
            int v5_2 = new String[3];
            v5_2[0] = "chown";
            v5_2[1] = new StringBuilder().append("0:").append(p13).toString();
            v5_2[2] = p12.getAbsolutePath();
            com.chelpus.Utils.cmdParam(v5_2);
            int v5_3 = new String[3];
            v5_3[0] = "chown";
            v5_3[1] = new StringBuilder().append("0.").append(p13).toString();
            v5_3[2] = p12.getAbsolutePath();
            com.chelpus.Utils.cmdParam(v5_3);
        } else {
            if (!p12.exists()) {
                p12.mkdir();
                int v5_5 = new String[3];
                v5_5[0] = "chmod";
                v5_5[1] = "771";
                v5_5[2] = p12.getAbsolutePath();
                com.chelpus.Utils.cmdParam(v5_5);
                int v5_6 = new String[3];
                v5_6[0] = "chown";
                v5_6[1] = new StringBuilder().append("0:").append(p13).toString();
                v5_6[2] = p12.getAbsolutePath();
                com.chelpus.Utils.cmdParam(v5_6);
                int v5_7 = new String[3];
                v5_7[0] = "chown";
                v5_7[1] = new StringBuilder().append("0.").append(p13).toString();
                v5_7[2] = p12.getAbsolutePath();
                com.chelpus.Utils.cmdParam(v5_7);
                System.out.println(new StringBuilder().append("Directory copied from ").append(p11).append("  to ").append(p12).toString());
            }
            String[] v2 = p11.list();
            if (v2.length > 0) {
                int v5_10 = v2.length;
                while (v4_0 < v5_10) {
                    String v1 = v2[v4_0];
                    com.chelpus.root.utils.restoredata.copyFolder(new java.io.File(p11, v1), new java.io.File(p12, v1), p13);
                    v4_0++;
                }
            }
        }
        return;
    }

    public static void main(String[] p17)
    {
        com.chelpus.Utils.startRootJava(new com.chelpus.root.utils.restoredata$1());
        String v7 = p17[0];
        String v3 = p17[1];
        String v0 = p17[2];
        String v10 = p17[3];
        String v8 = p17[4];
        try {
            java.io.File v1_1 = new java.io.File(new StringBuilder().append(v0).append("/data.lpbkp").toString());
            System.out.println(v1_1.getAbsolutePath());
        } catch (Exception v6_5) {
            v6_5.printStackTrace();
            System.out.println(new StringBuilder().append("Exception e").append(v6_5.toString()).toString());
            System.out.println("error");
            com.chelpus.Utils.exitFromRootJava();
            return;
        }
        if (v1_1.exists()) {
            java.io.File v4_1 = new java.io.File(v3);
            try {
                net.lingala.zip4j.core.ZipFile v11_1 = new net.lingala.zip4j.core.ZipFile(v1_1);
                System.out.println(v4_1.getAbsolutePath());
                com.chelpus.root.utils.restoredata.ExtractAllFilesWithInputStreams(v11_1, v4_1.getAbsolutePath(), v10);
            } catch (Exception v6_0) {
                v6_0.printStackTrace();
                System.out.println("error");
            }
        }
        if (!new java.io.File(new StringBuilder().append("/dbdata/databases/").append(v7).toString()).exists()) {
            java.io.File v5_1 = new java.io.File(new StringBuilder().append(v0).append("/dbdata.lpbkp").toString());
            if (v5_1.exists()) {
                try {
                    com.chelpus.root.utils.restoredata.ExtractAllFilesWithInputStreams(new net.lingala.zip4j.core.ZipFile(v5_1), v3, v10);
                } catch (Exception v6_1) {
                    v6_1.printStackTrace();
                    System.out.println("error");
                }
            }
        } else {
            java.io.File v5_3 = new java.io.File(new StringBuilder().append(v0).append("/dbdata.lpbkp").toString());
            if (!v5_3.exists()) {
                if (new java.io.File(new StringBuilder().append(v3).append("/shared_prefs").toString()).exists()) {
                    com.chelpus.root.utils.restoredata.copyFolder(new java.io.File(new StringBuilder().append(v3).append("/shared_prefs").toString()), new java.io.File(new StringBuilder().append("/dbdata/databases/").append(v7).append("/shared_prefs").toString()), v10);
                }
            } else {
                try {
                    com.chelpus.root.utils.restoredata.ExtractAllFilesWithInputStreams(new net.lingala.zip4j.core.ZipFile(v5_3), new StringBuilder().append("/dbdata/databases/").append(v7).toString(), v10);
                } catch (Exception v6_2) {
                    v6_2.printStackTrace();
                    System.out.println("error");
                }
            }
        }
        new java.io.File(new StringBuilder().append(v8).append("/").toString()).mkdirs();
        java.io.File v2_1 = new java.io.File(new StringBuilder().append(v0).append("/sddata.lpbkp").toString());
        System.out.println(v2_1.getAbsolutePath());
        if (!v2_1.exists()) {
            com.chelpus.Utils.exitFromRootJava();
            return;
        } else {
            java.io.File v9_1 = new java.io.File(v8);
            try {
                net.lingala.zip4j.core.ZipFile v11_3 = new net.lingala.zip4j.core.ZipFile(v2_1);
                System.out.println(v9_1.getAbsolutePath());
                com.chelpus.root.utils.restoredata.ExtractAllFilesWithInputStreams(v11_3, v9_1.getAbsolutePath(), v10);
            } catch (Exception v6_3) {
                v6_3.printStackTrace();
            }
            com.chelpus.Utils.exitFromRootJava();
            return;
        }
    }
}
