package com.chelpus.root.utils;
public class copyLibsFilesToSystemBackup {
    public static String index;
    public static String libDir;
    public static boolean nospace;
    public static String pkgName;
    public static String sys_apk_dir;

    static copyLibsFilesToSystemBackup()
    {
        com.chelpus.root.utils.copyLibsFilesToSystemBackup.libDir = "";
        com.chelpus.root.utils.copyLibsFilesToSystemBackup.pkgName = "";
        com.chelpus.root.utils.copyLibsFilesToSystemBackup.index = "";
        com.chelpus.root.utils.copyLibsFilesToSystemBackup.sys_apk_dir = "";
        com.chelpus.root.utils.copyLibsFilesToSystemBackup.nospace = 0;
        return;
    }

    public copyLibsFilesToSystemBackup()
    {
        return;
    }

    public static java.util.ArrayList getLibs(java.io.File p19, String p20)
    {
        java.util.ArrayList v8_1 = new java.util.ArrayList();
        if (p20.startsWith("/mnt/")) {
            java.io.File v19_1 = com.chelpus.Utils.getDirs(new java.io.File(p20));
            com.chelpus.Utils.remount(v19_1.getAbsolutePath(), "rw");
            if (v19_1.exists()) {
                String[] v5_0 = v19_1.list();
                int v12_0 = v5_0.length;
                int v11_1 = 0;
                while (v11_1 < v12_0) {
                    String v4_0 = v5_0[v11_1];
                    System.out.println(new StringBuilder().append("LuckyPatcher: file found in data dir - ").append(v19_1).append("/").append(v4_0).toString());
                    if ((new java.io.File(new StringBuilder().append(v19_1).append("/").append(v4_0).toString()).isDirectory()) && (new StringBuilder().append(v19_1).append("/").append(v4_0).toString().equals(new StringBuilder().append(v19_1).append("/").append("lib").toString()))) {
                        String[] v7_0 = new java.io.File(new StringBuilder().append(v19_1).append("/").append(v4_0).toString()).list();
                        int v13_26 = v7_0.length;
                        int v10_20 = 0;
                        while (v10_20 < v13_26) {
                            java.io.File v6_0 = v7_0[v10_20];
                            if ((new java.io.File(new StringBuilder().append(v19_1).append("/").append(v4_0).append("/").append(v6_0).toString()).isFile()) && (!v6_0.contains("libjnigraphics.so"))) {
                                v8_1.add(new StringBuilder().append(v19_1).append("/").append(v4_0).append("/").append(v6_0).toString());
                                System.out.println(new StringBuilder().append("LuckyPatcher: found lib - ").append(v19_1).append("/").append(v4_0).append("/").append(v6_0).toString());
                            }
                            v10_20++;
                        }
                    }
                    v11_1++;
                }
            }
        } else {
            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.api < 21) {
                if (p19.exists()) {
                    String[] v5_1 = p19.list();
                    int v12_1 = v5_1.length;
                    int v11_3 = 0;
                    while (v11_3 < v12_1) {
                        String v4_1 = v5_1[v11_3];
                        System.out.println(new StringBuilder().append("LuckyPatcher: file found in data dir - ").append(p19).append("/").append(v4_1).append("id dir:").append(new java.io.File(new StringBuilder().append(p19).append("/").append(v4_1).toString()).isDirectory()).toString());
                        if ((new java.io.File(new StringBuilder().append(p19).append("/").append(v4_1).toString()).isDirectory()) && (new StringBuilder().append(p19).append("/").append(v4_1).toString().equals(new StringBuilder().append(p19).append("/").append("lib").toString()))) {
                            String[] v7_1 = new java.io.File(new StringBuilder().append(p19).append("/").append(v4_1).toString()).list();
                            int v13_55 = v7_1.length;
                            int v10_38 = 0;
                            while (v10_38 < v13_55) {
                                java.io.File v6_1 = v7_1[v10_38];
                                if ((new java.io.File(new StringBuilder().append(p19).append("/").append(v4_1).append("/").append(v6_1).toString()).isFile()) && (!v6_1.contains("libjnigraphics.so"))) {
                                    v8_1.add(new StringBuilder().append(p19).append("/").append(v4_1).append("/").append(v6_1).toString());
                                    System.out.println(new StringBuilder().append("LuckyPatcher: found lib - ").append(p19).append("/").append(v4_1).append("/").append(v6_1).append(" is dir:").append(new java.io.File(new StringBuilder().append(p19).append("/").append(v4_1).toString()).isDirectory()).toString());
                                }
                                v10_38++;
                            }
                        }
                        v11_3++;
                    }
                }
            } else {
                java.io.File v19_2 = com.chelpus.Utils.getDirs(new java.io.File(p20));
                if (v19_2.exists()) {
                    String[] v5_2 = v19_2.listFiles();
                    int v13_56 = v5_2.length;
                    int v12_2 = 0;
                    while (v12_2 < v13_56) {
                        String v4_2 = v5_2[v12_2];
                        System.out.println(new StringBuilder().append("LuckyPatcher: file found in data dir - ").append(v4_2).append(" is dir:").append(v4_2.isDirectory()).toString());
                        if ((v4_2.isDirectory()) && (v4_2.getAbsolutePath().equals(new StringBuilder().append(v19_2).append("/").append("lib").toString()))) {
                            java.io.File[] v3 = v4_2.listFiles();
                            if ((v3 != null) && (v3.length > 0)) {
                                java.io.PrintStream v14_49 = v3.length;
                                int v11_17 = 0;
                                while (v11_17 < v14_49) {
                                    java.io.File[] v9 = v3[v11_17].listFiles();
                                    if ((v9 != null) && (v9.length > 0)) {
                                        int v15_46 = v9.length;
                                        int v10_52 = 0;
                                        while (v10_52 < v15_46) {
                                            java.io.File v6_2 = v9[v10_52];
                                            v8_1.add(v6_2.getAbsolutePath());
                                            com.chelpus.root.utils.copyLibsFilesToSystemBackup.index = com.chelpus.Utils.getDirs(v6_2).getAbsolutePath().replace(v19_2.getAbsolutePath(), "");
                                            com.chelpus.root.utils.copyLibsFilesToSystemBackup.libDir = new StringBuilder().append(com.chelpus.root.utils.copyLibsFilesToSystemBackup.sys_apk_dir).append(com.chelpus.root.utils.copyLibsFilesToSystemBackup.index).append("/").toString();
                                            System.out.println(new StringBuilder().append("libdir").append(com.chelpus.root.utils.copyLibsFilesToSystemBackup.libDir).toString());
                                            new java.io.File(com.chelpus.root.utils.copyLibsFilesToSystemBackup.libDir).mkdirs();
                                            System.out.println(new StringBuilder().append("LuckyPatcher: found lib - ").append(v6_2.getAbsolutePath()).toString());
                                            v10_52++;
                                        }
                                    }
                                    v11_17++;
                                }
                            }
                        }
                        v12_2++;
                    }
                }
            }
        }
        return v8_1;
    }

    public static void main(String[] p19)
    {
        com.chelpus.Utils.startRootJava(new com.chelpus.root.utils.copyLibsFilesToSystemBackup$1());
        com.chelpus.root.utils.copyLibsFilesToSystemBackup.pkgName = p19[0];
        String v1 = p19[1];
        String v0 = p19[2];
        com.chelpus.root.utils.copyLibsFilesToSystemBackup.sys_apk_dir = com.chelpus.Utils.getDirs(new java.io.File(p19[3])).getAbsolutePath();
        String v5 = p19[4];
        if (v5.equals("copyLibs")) {
            System.out.println("copyLibs");
            com.chelpus.root.utils.copyLibsFilesToSystemBackup.libDir = "/system/lib/";
            java.util.ArrayList v9_0 = com.chelpus.root.utils.copyLibsFilesToSystemBackup.getLibs(new java.io.File(v1), v0);
            if (v9_0.size() > 0) {
                int v12_18 = v9_0.iterator();
                while (v12_18.hasNext()) {
                    java.io.File v10_1 = new java.io.File(((String) v12_18.next()));
                    if (!new java.io.File(com.chelpus.root.utils.copyLibsFilesToSystemBackup.libDir).exists()) {
                        new java.io.File(com.chelpus.root.utils.copyLibsFilesToSystemBackup.libDir).mkdirs();
                    }
                    com.chelpus.Utils.setPermissionDir(com.chelpus.root.utils.copyLibsFilesToSystemBackup.sys_apk_dir, new StringBuilder().append(com.chelpus.root.utils.copyLibsFilesToSystemBackup.sys_apk_dir).append(com.chelpus.root.utils.copyLibsFilesToSystemBackup.index).toString(), "755");
                    com.chelpus.Utils.copyFile(v10_1, new java.io.File(new StringBuilder().append(com.chelpus.root.utils.copyLibsFilesToSystemBackup.libDir).append(v10_1.getName()).append(".chelpus").toString()));
                    com.chelpus.root.utils.copyLibsFilesToSystemBackup.run_all(new StringBuilder().append("chmod 0644 ").append(com.chelpus.root.utils.copyLibsFilesToSystemBackup.libDir).append(v10_1.getName()).append(".chelpus").toString());
                    com.chelpus.root.utils.copyLibsFilesToSystemBackup.run_all(new StringBuilder().append("chown 0.0 ").append(com.chelpus.root.utils.copyLibsFilesToSystemBackup.libDir).append(v10_1.getName()).append(".chelpus").toString());
                    com.chelpus.root.utils.copyLibsFilesToSystemBackup.run_all(new StringBuilder().append("chown 0:0 ").append(com.chelpus.root.utils.copyLibsFilesToSystemBackup.libDir).append(v10_1.getName()).append(".chelpus").toString());
                    if (v10_1.length() != new java.io.File(new StringBuilder().append(com.chelpus.root.utils.copyLibsFilesToSystemBackup.libDir).append(v10_1.getName()).append(".chelpus").toString()).length()) {
                        int v12_19 = v9_0.iterator();
                        while (v12_19.hasNext()) {
                            java.io.File v11_3 = new java.io.File(((String) v12_19.next()));
                            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.api >= 21) {
                                if (com.chelpus.root.utils.copyLibsFilesToSystemBackup.libDir.startsWith("/system/priv-app/")) {
                                    try {
                                        new com.chelpus.Utils("").deleteFolder(new java.io.File(com.chelpus.root.utils.copyLibsFilesToSystemBackup.libDir));
                                    } catch (java.io.IOException v2) {
                                        v2.printStackTrace();
                                    }
                                }
                            } else {
                                new java.io.File(new StringBuilder().append(com.chelpus.root.utils.copyLibsFilesToSystemBackup.libDir).append(v11_3.getName()).append(".chelpus").toString()).delete();
                            }
                        }
                        System.out.println("In /system space not found!");
                        com.chelpus.root.utils.copyLibsFilesToSystemBackup.nospace = 1;
                        break;
                    } else {
                        System.out.println(new StringBuilder().append("LuckyPatcher: copy lib ").append(com.chelpus.root.utils.copyLibsFilesToSystemBackup.libDir).append(v10_1.getName()).append(".chelpus").toString());
                    }
                }
            }
        }
        if (v5.equals("replaceOldLibs")) {
            System.out.println("replaceOldLibs");
            com.chelpus.root.utils.copyLibsFilesToSystemBackup.libDir = "/system/lib/";
            if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.api >= 21) {
                if (new java.io.File(new StringBuilder().append(com.chelpus.root.utils.copyLibsFilesToSystemBackup.sys_apk_dir).append("/lib/x86").toString()).exists()) {
                    com.chelpus.root.utils.copyLibsFilesToSystemBackup.libDir = new StringBuilder().append(com.chelpus.root.utils.copyLibsFilesToSystemBackup.sys_apk_dir).append("/lib/x86/").toString();
                }
                if (new java.io.File(new StringBuilder().append(com.chelpus.root.utils.copyLibsFilesToSystemBackup.sys_apk_dir).append("/lib/arm").toString()).exists()) {
                    com.chelpus.root.utils.copyLibsFilesToSystemBackup.libDir = new StringBuilder().append(com.chelpus.root.utils.copyLibsFilesToSystemBackup.sys_apk_dir).append("/lib/arm/").toString();
                }
                if (new java.io.File(new StringBuilder().append(com.chelpus.root.utils.copyLibsFilesToSystemBackup.sys_apk_dir).append("/lib/arm64").toString()).exists()) {
                    com.chelpus.root.utils.copyLibsFilesToSystemBackup.libDir = new StringBuilder().append(com.chelpus.root.utils.copyLibsFilesToSystemBackup.sys_apk_dir).append("/lib/arm64/").toString();
                }
                if (new java.io.File(new StringBuilder().append(com.chelpus.root.utils.copyLibsFilesToSystemBackup.sys_apk_dir).append("/lib/mips").toString()).exists()) {
                    com.chelpus.root.utils.copyLibsFilesToSystemBackup.libDir = new StringBuilder().append(com.chelpus.root.utils.copyLibsFilesToSystemBackup.sys_apk_dir).append("/lib/mips/").toString();
                }
            }
            new java.util.ArrayList();
            java.io.File[] v4 = new java.io.File(com.chelpus.root.utils.copyLibsFilesToSystemBackup.libDir).listFiles();
            if ((v4 != null) && (v4.length > 0)) {
                com.chelpus.Utils v13_68 = v4.length;
                int v12_62 = 0;
                while (v12_62 < v13_68) {
                    java.io.File v3 = v4[v12_62];
                    if ((v3.isFile()) && (v3.getName().endsWith(".chelpus"))) {
                        System.out.println(new StringBuilder().append("Replace system lib:").append(v3.getAbsolutePath().replace(".chelpus", "")).toString());
                        new java.io.File(v3.getAbsolutePath().replace(".chelpus", "")).delete();
                        v3.renameTo(new java.io.File(v3.getAbsolutePath().replace(".chelpus", "")));
                    }
                    v12_62++;
                }
            }
            com.chelpus.root.utils.copyLibsFilesToSystemBackup.removeLibs(new java.io.File(v1), v0);
        }
        if (v5.equals("deleteBigLibs")) {
            System.out.println("deleteBigLibs");
            com.chelpus.root.utils.copyLibsFilesToSystemBackup.libDir = "/system/lib/";
            java.util.ArrayList v9_1 = com.chelpus.root.utils.copyLibsFilesToSystemBackup.getLibs(new java.io.File(v1), v0);
            if (v9_1.size() > 0) {
                int v12_72 = v9_1.iterator();
                while (v12_72.hasNext()) {
                    new java.io.File(new StringBuilder().append(com.chelpus.root.utils.copyLibsFilesToSystemBackup.libDir).append(new java.io.File(((String) v12_72.next())).getName()).append(".chelpus").toString()).delete();
                }
            }
        }
        com.chelpus.Utils.exitFromRootJava();
        return;
    }

    public static void removeLibs(java.io.File p13, String p14)
    {
        int v5 = 0;
        if (com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.api >= 21) {
            System.out.println("remove libs in data for api 21");
            java.io.File v13_1 = com.chelpus.Utils.getDirs(new java.io.File(p14));
            if (com.chelpus.Utils.getDirs(new java.io.File(p14)).exists()) {
                String[] v2_0 = v13_1.listFiles();
                int v6_8 = v2_0.length;
                while (v5 < v6_8) {
                    String v1_0 = v2_0[v5];
                    System.out.println(new StringBuilder().append("LuckyPatcher: file found in data dir - ").append(v13_1).append("/").append(v1_0).toString());
                    if ((v1_0.isDirectory()) && (v1_0.getAbsolutePath().endsWith("/lib"))) {
                        try {
                            new com.chelpus.Utils("").deleteFolder(v1_0);
                        } catch (java.io.IOException v0) {
                            v0.printStackTrace();
                        }
                    }
                    v5++;
                }
            }
        } else {
            if (!p14.startsWith("/mnt/")) {
                System.out.println("remove libs in data");
                if (p13.exists()) {
                    String[] v2_1 = p13.list();
                    String v8_9 = v2_1.length;
                    int v7_9 = 0;
                    while (v7_9 < v8_9) {
                        String v1_1 = v2_1[v7_9];
                        System.out.println(new StringBuilder().append("LuckyPatcher: file found in data dir - ").append(p13).append("/").append(v1_1).toString());
                        if ((!new java.io.File(new StringBuilder().append(p13).append("/").append(v1_1).toString()).isDirectory()) || (!new StringBuilder().append(p13).append("/").append(v1_1).toString().equals(new StringBuilder().append(p13).append("/").append("lib").toString()))) {
                            System.out.println(new StringBuilder().append("file not dir lib").append(p13).append("/").append(v1_1).append(" is File:").append(new java.io.File(new StringBuilder().append(p13).append("/").append(v1_1).toString()).isFile()).toString());
                            if (new StringBuilder().append(p13).append("/").append(v1_1).toString().equals(new StringBuilder().append(p13).append("/").append("lib").toString())) {
                                System.out.println(new StringBuilder().append("delete file").append(p13).append("/").append(v1_1).toString());
                                new java.io.File(new StringBuilder().append(p13).append("/").append(v1_1).toString()).delete();
                            }
                        } else {
                            String[] v4 = new java.io.File(new StringBuilder().append(p13).append("/").append(v1_1).toString()).list();
                            if ((v4 != null) && (v4.length > 0)) {
                                int v9_57 = v4.length;
                                int v6_38 = 0;
                                while (v6_38 < v9_57) {
                                    String v3 = v4[v6_38];
                                    if (new java.io.File(new StringBuilder().append(p13).append("/").append(v1_1).append("/").append(v3).toString()).isFile()) {
                                        new java.io.File(new StringBuilder().append(p13).append("/").append(v1_1).append("/").append(v3).toString()).delete();
                                        System.out.println(new StringBuilder().append("LuckyPatcher: remove lib - ").append(p13).append("/").append(v1_1).append("/").append(v3).toString());
                                    }
                                    v6_38++;
                                }
                            }
                            System.out.println(new StringBuilder().append("delete dir").append(p13).append("/").append(v1_1).toString());
                            new java.io.File(new StringBuilder().append(p13).append("/").append(v1_1).toString()).delete();
                        }
                        v7_9++;
                    }
                }
            }
        }
        return;
    }

    private static void run_all(String p4)
    {
        try {
            Process v0_0 = Runtime.getRuntime().exec(new StringBuilder().append(p4).append("\n").toString());
            v0_0.waitFor();
            v0_0.destroy();
            try {
                Process v0_1 = Runtime.getRuntime().exec(new StringBuilder().append("toolbox ").append(p4).append("\n").toString());
                v0_1.waitFor();
                v0_1.destroy();
            } catch (Exception v1) {
            }
            try {
                Process v0_2 = Runtime.getRuntime().exec(new StringBuilder().append("/system/bin/failsafe/toolbox ").append(p4).append("\n").toString());
                v0_2.waitFor();
                v0_2.destroy();
                try {
                    Process v0_3 = Runtime.getRuntime().exec(new StringBuilder().append("busybox ").append(p4).append("\n").toString());
                    v0_3.waitFor();
                    v0_3.destroy();
                } catch (Exception v1) {
                }
                try {
                    Process v0_4 = Runtime.getRuntime().exec(new StringBuilder().append(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.toolfilesdir).append("/busybox ").append(p4).append("\n").toString());
                    v0_4.waitFor();
                    v0_4.destroy();
                } catch (Exception v1) {
                }
                return;
            } catch (Exception v1) {
            }
        } catch (Exception v1) {
        }
    }
}
