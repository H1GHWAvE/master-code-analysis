package com.chelpus.root.utils;
public class createapkcustom$Decompress {
    private String _location;
    private String _zipFile;

    public createapkcustom$Decompress(String p2, String p3)
    {
        this._zipFile = p2;
        this._location = p3;
        this._dirChecker("");
        return;
    }

    private void _dirChecker(String p4)
    {
        java.io.File v0_1 = new java.io.File(new StringBuilder().append(this._location).append(p4).toString());
        if (v0_1.isFile()) {
            v0_1.delete();
        }
        if (!v0_1.exists()) {
            v0_1.mkdirs();
        }
        return;
    }

    public String unzip(String p21)
    {
        try {
            String v17_21;
            java.io.FileInputStream v9_1 = new java.io.FileInputStream(this._zipFile);
            java.util.zip.ZipInputStream v15_1 = new java.util.zip.ZipInputStream(v9_1);
        } catch (Exception v5) {
            com.chelpus.root.utils.createapkcustom.access$000().println(new StringBuilder().append("Decompressunzip ").append(v5).toString());
            try {
                net.lingala.zip4j.core.ZipFile v16_1 = new net.lingala.zip4j.core.ZipFile(this._zipFile);
                java.util.List v8 = v16_1.getFileHeaders();
                int v11_1 = 0;
            } catch (Exception v6_1) {
                v6_1.printStackTrace();
                v17_21 = "";
                return v17_21;
            } catch (Exception v6_0) {
                v6_0.printStackTrace();
            }
        }
        do {
            java.util.zip.ZipEntry v14 = v15_1.getNextEntry();
            if (v14 == null) {
                v15_1.close();
                v9_1.close();
            } else {
                if (!v14.isDirectory()) {
                    if (p21.startsWith("/")) {
                        p21 = p21.replaceFirst("/", "");
                    }
                } else {
                    this._dirChecker(v14.getName());
                }
            }
        } while(!v14.getName().equals(p21));
        String[] v13 = v14.getName().split("\\/+");
        String v4 = "";
        int v11_0 = 0;
        while (v11_0 < (v13.length - 1)) {
            if (!v13[v11_0].equals("")) {
                v4 = new StringBuilder().append(v4).append("/").append(v13[v11_0]).toString();
            }
            v11_0++;
        }
        this._dirChecker(v4);
        java.io.FileOutputStream v10_1 = new java.io.FileOutputStream(new StringBuilder().append(this._location).append(v14.getName()).toString());
        byte[] v3 = new byte[1024];
        while(true) {
            int v12 = v15_1.read(v3);
            if (v12 == -1) {
                break;
            }
            v10_1.write(v3, 0, v12);
        }
        v15_1.closeEntry();
        v10_1.close();
        v15_1.close();
        v9_1.close();
        v17_21 = new StringBuilder().append(this._location).append(v14.getName()).toString();
        return v17_21;
    }

    public void unzip()
    {
        try {
            java.io.FileInputStream v7_1 = new java.io.FileInputStream(this._zipFile);
            java.util.zip.ZipInputStream v13_1 = new java.util.zip.ZipInputStream(v7_1);
        } catch (Exception v3) {
            com.chelpus.root.utils.createapkcustom.access$000().println(new StringBuilder().append("Decompressunzip ").append(v3).toString());
            try {
                net.lingala.zip4j.core.ZipFile v14_1 = new net.lingala.zip4j.core.ZipFile(this._zipFile);
                java.util.List v6 = v14_1.getFileHeaders();
                int v9_1 = 0;
            } catch (Exception v4_0) {
                v4_0.printStackTrace();
                return;
            } catch (Exception v4_1) {
                v4_1.printStackTrace();
                return;
            }
        }
        while(true) {
            java.util.zip.ZipEntry v12 = v13_1.getNextEntry();
            if (v12 == null) {
                break;
            }
            if (!v12.isDirectory()) {
                if (v12.getName().endsWith(".so")) {
                    String[] v11 = v12.getName().split("\\/+");
                    String v2 = "";
                    int v9_0 = 0;
                    while (v9_0 < (v11.length - 1)) {
                        if (!v11[v9_0].equals("")) {
                            v2 = new StringBuilder().append(v2).append("/").append(v11[v9_0]).toString();
                        }
                        v9_0++;
                    }
                    this._dirChecker(v2);
                    java.io.FileOutputStream v8_1 = new java.io.FileOutputStream(new StringBuilder().append(this._location).append(v12.getName()).toString());
                    byte[] v1 = new byte[1024];
                    while(true) {
                        int v10 = v13_1.read(v1);
                        if (v10 == -1) {
                            break;
                        }
                        v8_1.write(v1, 0, v10);
                    }
                    v13_1.closeEntry();
                    v8_1.close();
                }
            } else {
                this._dirChecker(v12.getName());
            }
        }
        v13_1.close();
        v7_1.close();
        return;
    }
}
