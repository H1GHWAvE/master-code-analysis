package com.chelpus;
 class XSupport$35 extends de.robv.android.xposed.XC_MethodHook {
    final synthetic com.chelpus.XSupport this$0;

    XSupport$35(com.chelpus.XSupport p1)
    {
        this.this$0 = p1;
        return;
    }

    protected void beforeHookedMethod(de.robv.android.xposed.XC_MethodHook$MethodHookParam p4)
    {
        this.this$0.loadPrefs();
        if ((this.this$0.forHide != null) && ((this.this$0.forHide.booleanValue()) && ((com.chelpus.XSupport.enable) && (com.chelpus.XSupport.hide)))) {
            String v0_1 = ((String) p4.args[0]);
            if ((v0_1 != null) && (v0_1.equals(com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.getPackage().getName()))) {
                p4.setThrowable(new android.content.pm.PackageManager$NameNotFoundException());
            }
        }
        return;
    }
}
