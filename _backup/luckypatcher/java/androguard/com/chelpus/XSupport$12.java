package com.chelpus;
 class XSupport$12 extends de.robv.android.xposed.XC_MethodHook {
    final synthetic com.chelpus.XSupport this$0;

    XSupport$12(com.chelpus.XSupport p1)
    {
        this.this$0 = p1;
        return;
    }

    protected void beforeHookedMethod(de.robv.android.xposed.XC_MethodHook$MethodHookParam p5)
    {
        this.this$0.loadPrefs();
        if ((com.chelpus.XSupport.enable) && (com.chelpus.XSupport.patch3)) {
            int v1;
            if (!com.chelpus.Common.JB_MR1_NEWER) {
                v1 = 1;
            } else {
                v1 = 2;
            }
            int v0_0 = ((Integer) p5.args[v1]).intValue();
            if ((v0_0 & 128) == 0) {
                p5.args[v1] = Integer.valueOf((v0_0 | 128));
            }
        }
        return;
    }
}
