package com.chelpus;
 class XSupport$13 extends de.robv.android.xposed.XC_MethodHook {
    final synthetic com.chelpus.XSupport this$0;

    XSupport$13(com.chelpus.XSupport p1)
    {
        this.this$0 = p1;
        return;
    }

    protected void beforeHookedMethod(de.robv.android.xposed.XC_MethodHook$MethodHookParam p6)
    {
        this.this$0.loadPrefs();
        if ((com.chelpus.XSupport.enable) && (com.chelpus.XSupport.patch3)) {
            int v2 = 1;
            if (android.os.Build$VERSION.SDK_INT >= 14) {
                v2 = 2;
            }
            try {
                int v1_0 = ((Integer) p6.args[v2]).intValue();
            } catch (ClassCastException v0) {
                v1_0 = ((Integer) p6.args[1]).intValue();
                v2 = 1;
            }
            if ((v1_0 & 128) == 0) {
                p6.args[v2] = Integer.valueOf((v1_0 | 128));
            }
        }
        return;
    }
}
