package net.lingala.zip4j.core;
public class ZipFile {
    private String file;
    private String fileNameCharset;
    private boolean isEncrypted;
    private int mode;
    private net.lingala.zip4j.progress.ProgressMonitor progressMonitor;
    private boolean runInThread;
    private net.lingala.zip4j.model.ZipModel zipModel;

    public ZipFile(java.io.File p4)
    {
        if (p4 != null) {
            this.file = p4.getPath();
            this.mode = 2;
            this.progressMonitor = new net.lingala.zip4j.progress.ProgressMonitor();
            this.runInThread = 0;
            return;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("Input zip file parameter is not null", 1);
        }
    }

    public ZipFile(String p2)
    {
        this(new java.io.File(p2));
        return;
    }

    private void addFolder(java.io.File p4, net.lingala.zip4j.model.ZipParameters p5, boolean p6)
    {
        this.checkZipModel();
        if (this.zipModel != null) {
            if ((!p6) || (!this.zipModel.isSplitArchive())) {
                new net.lingala.zip4j.zip.ZipEngine(this.zipModel).addFolderToZip(p4, p5, this.progressMonitor, this.runInThread);
                return;
            } else {
                throw new net.lingala.zip4j.exception.ZipException("This is a split archive. Zip file format does not allow updating split/spanned files");
            }
        } else {
            throw new net.lingala.zip4j.exception.ZipException("internal error: zip model is null");
        }
    }

    private void checkZipModel()
    {
        if (this.zipModel == null) {
            if (!net.lingala.zip4j.util.Zip4jUtil.checkFileExists(this.file)) {
                this.createNewZipModel();
            } else {
                this.readZipInfo();
            }
        }
        return;
    }

    private void createNewZipModel()
    {
        this.zipModel = new net.lingala.zip4j.model.ZipModel();
        this.zipModel.setZipFile(this.file);
        this.zipModel.setFileNameCharset(this.fileNameCharset);
        return;
    }

    private void readZipInfo()
    {
        if (net.lingala.zip4j.util.Zip4jUtil.checkFileExists(this.file)) {
            if (net.lingala.zip4j.util.Zip4jUtil.checkFileReadAccess(this.file)) {
                if (this.mode == 2) {
                    java.io.RandomAccessFile v2 = 0;
                    try {
                        java.io.RandomAccessFile v3_1 = new java.io.RandomAccessFile(new java.io.File(this.file), "r");
                        try {
                            if (this.zipModel == null) {
                                this.zipModel = new net.lingala.zip4j.core.HeaderReader(v3_1).readAllHeaders(this.fileNameCharset);
                                if (this.zipModel != null) {
                                    this.zipModel.setZipFile(this.file);
                                }
                            }
                        } catch (java.io.IOException v4_14) {
                            v2 = v3_1;
                            if (v2 != null) {
                                try {
                                    v2.close();
                                } catch (java.io.IOException v5) {
                                }
                            }
                            throw v4_14;
                        } catch (java.io.FileNotFoundException v0) {
                            v2 = v3_1;
                            throw new net.lingala.zip4j.exception.ZipException(v0);
                        }
                        if (v3_1 != null) {
                            try {
                                v3_1.close();
                            } catch (java.io.IOException v4) {
                            }
                        }
                        return;
                    } catch (java.io.IOException v4_14) {
                    } catch (java.io.FileNotFoundException v0) {
                    }
                } else {
                    throw new net.lingala.zip4j.exception.ZipException("Invalid mode");
                }
            } else {
                throw new net.lingala.zip4j.exception.ZipException("no read access for the input zip file");
            }
        } else {
            throw new net.lingala.zip4j.exception.ZipException("zip file does not exist");
        }
    }

    public void addFile(java.io.File p2, net.lingala.zip4j.model.ZipParameters p3)
    {
        java.util.ArrayList v0_1 = new java.util.ArrayList();
        v0_1.add(p2);
        this.addFiles(v0_1, p3);
        return;
    }

    public void addFiles(java.util.ArrayList p4, net.lingala.zip4j.model.ZipParameters p5)
    {
        this.checkZipModel();
        if (this.zipModel != null) {
            if (p4 != null) {
                if (net.lingala.zip4j.util.Zip4jUtil.checkArrayListTypes(p4, 1)) {
                    if (p5 != null) {
                        if (this.progressMonitor.getState() != 1) {
                            if ((!net.lingala.zip4j.util.Zip4jUtil.checkFileExists(this.file)) || (!this.zipModel.isSplitArchive())) {
                                new net.lingala.zip4j.zip.ZipEngine(this.zipModel).addFiles(p4, p5, this.progressMonitor, this.runInThread);
                                return;
                            } else {
                                throw new net.lingala.zip4j.exception.ZipException("Zip file already exists. Zip file format does not allow updating split/spanned files");
                            }
                        } else {
                            throw new net.lingala.zip4j.exception.ZipException("invalid operation - Zip4j is in busy state");
                        }
                    } else {
                        throw new net.lingala.zip4j.exception.ZipException("input parameters are null, cannot add files to zip");
                    }
                } else {
                    throw new net.lingala.zip4j.exception.ZipException("One or more elements in the input ArrayList is not of type File");
                }
            } else {
                throw new net.lingala.zip4j.exception.ZipException("input file ArrayList is null, cannot add files");
            }
        } else {
            throw new net.lingala.zip4j.exception.ZipException("internal error: zip model is null");
        }
    }

    public void addFolder(java.io.File p3, net.lingala.zip4j.model.ZipParameters p4)
    {
        if (p3 != null) {
            if (p4 != null) {
                this.addFolder(p3, p4, 1);
                return;
            } else {
                throw new net.lingala.zip4j.exception.ZipException("input parameters are null, cannot add folder to zip file");
            }
        } else {
            throw new net.lingala.zip4j.exception.ZipException("input path is null, cannot add folder to zip file");
        }
    }

    public void addFolder(String p3, net.lingala.zip4j.model.ZipParameters p4)
    {
        if (net.lingala.zip4j.util.Zip4jUtil.isStringNotNullAndNotEmpty(p3)) {
            this.addFolder(new java.io.File(p3), p4);
            return;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("input path is null or empty, cannot add folder to zip file");
        }
    }

    public void addStream(java.io.InputStream p4, net.lingala.zip4j.model.ZipParameters p5)
    {
        if (p4 != null) {
            if (p5 != null) {
                this.setRunInThread(0);
                this.checkZipModel();
                if (this.zipModel != null) {
                    if ((!net.lingala.zip4j.util.Zip4jUtil.checkFileExists(this.file)) || (!this.zipModel.isSplitArchive())) {
                        new net.lingala.zip4j.zip.ZipEngine(this.zipModel).addStreamToZip(p4, p5);
                        return;
                    } else {
                        throw new net.lingala.zip4j.exception.ZipException("Zip file already exists. Zip file format does not allow updating split/spanned files");
                    }
                } else {
                    throw new net.lingala.zip4j.exception.ZipException("internal error: zip model is null");
                }
            } else {
                throw new net.lingala.zip4j.exception.ZipException("zip parameters are null");
            }
        } else {
            throw new net.lingala.zip4j.exception.ZipException("inputstream is null, cannot add file to zip");
        }
    }

    public void createZipFile(java.io.File p7, net.lingala.zip4j.model.ZipParameters p8)
    {
        java.util.ArrayList v1_1 = new java.util.ArrayList();
        v1_1.add(p7);
        this.createZipFile(v1_1, p8, 0, -1);
        return;
    }

    public void createZipFile(java.io.File p7, net.lingala.zip4j.model.ZipParameters p8, boolean p9, long p10)
    {
        java.util.ArrayList v1_1 = new java.util.ArrayList();
        v1_1.add(p7);
        this.createZipFile(v1_1, p8, p9, p10);
        return;
    }

    public void createZipFile(java.util.ArrayList p7, net.lingala.zip4j.model.ZipParameters p8)
    {
        this.createZipFile(p7, p8, 0, -1);
        return;
    }

    public void createZipFile(java.util.ArrayList p4, net.lingala.zip4j.model.ZipParameters p5, boolean p6, long p7)
    {
        if (net.lingala.zip4j.util.Zip4jUtil.isStringNotNullAndNotEmpty(this.file)) {
            if (!net.lingala.zip4j.util.Zip4jUtil.checkFileExists(this.file)) {
                if (p4 != null) {
                    if (net.lingala.zip4j.util.Zip4jUtil.checkArrayListTypes(p4, 1)) {
                        this.createNewZipModel();
                        this.zipModel.setSplitArchive(p6);
                        this.zipModel.setSplitLength(p7);
                        this.addFiles(p4, p5);
                        return;
                    } else {
                        throw new net.lingala.zip4j.exception.ZipException("One or more elements in the input ArrayList is not of type File");
                    }
                } else {
                    throw new net.lingala.zip4j.exception.ZipException("input file ArrayList is null, cannot create zip file");
                }
            } else {
                throw new net.lingala.zip4j.exception.ZipException(new StringBuilder().append("zip file: ").append(this.file).append(" already exists. To add files to existing zip file use addFile method").toString());
            }
        } else {
            throw new net.lingala.zip4j.exception.ZipException("zip file path is empty");
        }
    }

    public void createZipFileFromFolder(java.io.File p4, net.lingala.zip4j.model.ZipParameters p5, boolean p6, long p7)
    {
        if (p4 != null) {
            if (p5 != null) {
                if (!net.lingala.zip4j.util.Zip4jUtil.checkFileExists(this.file)) {
                    this.createNewZipModel();
                    this.zipModel.setSplitArchive(p6);
                    if (p6) {
                        this.zipModel.setSplitLength(p7);
                    }
                    this.addFolder(p4, p5, 0);
                    return;
                } else {
                    throw new net.lingala.zip4j.exception.ZipException(new StringBuilder().append("zip file: ").append(this.file).append(" already exists. To add files to existing zip file use addFolder method").toString());
                }
            } else {
                throw new net.lingala.zip4j.exception.ZipException("input parameters are null, cannot create zip file from folder");
            }
        } else {
            throw new net.lingala.zip4j.exception.ZipException("folderToAdd is null, cannot create zip file from folder");
        }
    }

    public void createZipFileFromFolder(String p7, net.lingala.zip4j.model.ZipParameters p8, boolean p9, long p10)
    {
        if (net.lingala.zip4j.util.Zip4jUtil.isStringNotNullAndNotEmpty(p7)) {
            this.createZipFileFromFolder(new java.io.File(p7), p8, p9, p10);
            return;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("folderToAdd is empty or null, cannot create Zip File from folder");
        }
    }

    public void extractAll(String p2)
    {
        this.extractAll(p2, 0);
        return;
    }

    public void extractAll(String p4, net.lingala.zip4j.model.UnzipParameters p5)
    {
        if (net.lingala.zip4j.util.Zip4jUtil.isStringNotNullAndNotEmpty(p4)) {
            if (net.lingala.zip4j.util.Zip4jUtil.checkOutputFolder(p4)) {
                if (this.zipModel == null) {
                    this.readZipInfo();
                }
                if (this.zipModel != null) {
                    if (this.progressMonitor.getState() != 1) {
                        new net.lingala.zip4j.unzip.Unzip(this.zipModel).extractAll(p5, p4, this.progressMonitor, this.runInThread);
                        return;
                    } else {
                        throw new net.lingala.zip4j.exception.ZipException("invalid operation - Zip4j is in busy state");
                    }
                } else {
                    throw new net.lingala.zip4j.exception.ZipException("Internal error occurred when extracting zip file");
                }
            } else {
                throw new net.lingala.zip4j.exception.ZipException("invalid output path");
            }
        } else {
            throw new net.lingala.zip4j.exception.ZipException("output path is null or invalid");
        }
    }

    public void extractFile(String p2, String p3)
    {
        this.extractFile(p2, p3, 0);
        return;
    }

    public void extractFile(String p2, String p3, net.lingala.zip4j.model.UnzipParameters p4)
    {
        this.extractFile(p2, p3, p4, 0);
        return;
    }

    public void extractFile(String p8, String p9, net.lingala.zip4j.model.UnzipParameters p10, String p11)
    {
        if (net.lingala.zip4j.util.Zip4jUtil.isStringNotNullAndNotEmpty(p8)) {
            if (net.lingala.zip4j.util.Zip4jUtil.isStringNotNullAndNotEmpty(p9)) {
                this.readZipInfo();
                net.lingala.zip4j.model.FileHeader v0 = net.lingala.zip4j.util.Zip4jUtil.getFileHeader(this.zipModel, p8);
                if (v0 != null) {
                    if (this.progressMonitor.getState() != 1) {
                        v0.extractFile(this.zipModel, p9, p10, p11, this.progressMonitor, this.runInThread);
                        return;
                    } else {
                        throw new net.lingala.zip4j.exception.ZipException("invalid operation - Zip4j is in busy state");
                    }
                } else {
                    throw new net.lingala.zip4j.exception.ZipException("file header not found for given file name, cannot extract file");
                }
            } else {
                throw new net.lingala.zip4j.exception.ZipException("destination string path is empty or null, cannot extract file");
            }
        } else {
            throw new net.lingala.zip4j.exception.ZipException("file to extract is null or empty, cannot extract file");
        }
    }

    public void extractFile(net.lingala.zip4j.model.FileHeader p2, String p3)
    {
        this.extractFile(p2, p3, 0);
        return;
    }

    public void extractFile(net.lingala.zip4j.model.FileHeader p2, String p3, net.lingala.zip4j.model.UnzipParameters p4)
    {
        this.extractFile(p2, p3, p4, 0);
        return;
    }

    public void extractFile(net.lingala.zip4j.model.FileHeader p8, String p9, net.lingala.zip4j.model.UnzipParameters p10, String p11)
    {
        if (p8 != null) {
            if (net.lingala.zip4j.util.Zip4jUtil.isStringNotNullAndNotEmpty(p9)) {
                this.readZipInfo();
                if (this.progressMonitor.getState() != 1) {
                    p8.extractFile(this.zipModel, p9, p10, p11, this.progressMonitor, this.runInThread);
                    return;
                } else {
                    throw new net.lingala.zip4j.exception.ZipException("invalid operation - Zip4j is in busy state");
                }
            } else {
                throw new net.lingala.zip4j.exception.ZipException("destination path is empty or null, cannot extract file");
            }
        } else {
            throw new net.lingala.zip4j.exception.ZipException("input file header is null, cannot extract file");
        }
    }

    public String getComment()
    {
        return this.getComment(0);
    }

    public String getComment(String p4)
    {
        if (p4 == null) {
            if (!net.lingala.zip4j.util.Zip4jUtil.isSupportedCharset("windows-1254")) {
                p4 = net.lingala.zip4j.util.InternalZipConstants.CHARSET_DEFAULT;
            } else {
                p4 = "windows-1254";
            }
        }
        if (!net.lingala.zip4j.util.Zip4jUtil.checkFileExists(this.file)) {
            throw new net.lingala.zip4j.exception.ZipException("zip file does not exist, cannot read comment");
        } else {
            this.checkZipModel();
            if (this.zipModel != null) {
                if (this.zipModel.getEndCentralDirRecord() != null) {
                    if ((this.zipModel.getEndCentralDirRecord().getCommentBytes() != null) && (this.zipModel.getEndCentralDirRecord().getCommentBytes().length > 0)) {
                        try {
                            net.lingala.zip4j.exception.ZipException v1_17 = new String(this.zipModel.getEndCentralDirRecord().getCommentBytes(), p4);
                        } catch (java.io.UnsupportedEncodingException v0) {
                            throw new net.lingala.zip4j.exception.ZipException(v0);
                        }
                    } else {
                        v1_17 = 0;
                    }
                    return v1_17;
                } else {
                    throw new net.lingala.zip4j.exception.ZipException("end of central directory record is null, cannot read comment");
                }
            } else {
                throw new net.lingala.zip4j.exception.ZipException("zip model is null, cannot read comment");
            }
        }
    }

    public java.io.File getFile()
    {
        return new java.io.File(this.file);
    }

    public net.lingala.zip4j.model.FileHeader getFileHeader(String p3)
    {
        if (net.lingala.zip4j.util.Zip4jUtil.isStringNotNullAndNotEmpty(p3)) {
            net.lingala.zip4j.model.FileHeader v0_5;
            this.readZipInfo();
            if ((this.zipModel != null) && (this.zipModel.getCentralDirectory() != null)) {
                v0_5 = net.lingala.zip4j.util.Zip4jUtil.getFileHeader(this.zipModel, p3);
            } else {
                v0_5 = 0;
            }
            return v0_5;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("input file name is emtpy or null, cannot get FileHeader");
        }
    }

    public java.util.List getFileHeaders()
    {
        java.util.ArrayList v0_5;
        this.readZipInfo();
        if ((this.zipModel != null) && (this.zipModel.getCentralDirectory() != null)) {
            v0_5 = this.zipModel.getCentralDirectory().getFileHeaders();
        } else {
            v0_5 = 0;
        }
        return v0_5;
    }

    public net.lingala.zip4j.io.ZipInputStream getInputStream(net.lingala.zip4j.model.FileHeader p4)
    {
        if (p4 != null) {
            this.checkZipModel();
            if (this.zipModel != null) {
                return new net.lingala.zip4j.unzip.Unzip(this.zipModel).getInputStream(p4);
            } else {
                throw new net.lingala.zip4j.exception.ZipException("zip model is null, cannot get inputstream");
            }
        } else {
            throw new net.lingala.zip4j.exception.ZipException("FileHeader is null, cannot get InputStream");
        }
    }

    public net.lingala.zip4j.progress.ProgressMonitor getProgressMonitor()
    {
        return this.progressMonitor;
    }

    public java.util.ArrayList getSplitZipFiles()
    {
        this.checkZipModel();
        return net.lingala.zip4j.util.Zip4jUtil.getSplitZipFiles(this.zipModel);
    }

    public boolean isEncrypted()
    {
        if (this.zipModel == null) {
            this.readZipInfo();
            if (this.zipModel == null) {
                throw new net.lingala.zip4j.exception.ZipException("Zip Model is null");
            }
        }
        if ((this.zipModel.getCentralDirectory() != null) && (this.zipModel.getCentralDirectory().getFileHeaders() != null)) {
            java.util.ArrayList v1 = this.zipModel.getCentralDirectory().getFileHeaders();
            int v2 = 0;
            while (v2 < v1.size()) {
                net.lingala.zip4j.model.FileHeader v0_1 = ((net.lingala.zip4j.model.FileHeader) v1.get(v2));
                if ((v0_1 == null) || (!v0_1.isEncrypted())) {
                    v2++;
                } else {
                    this.isEncrypted = 1;
                    break;
                }
            }
            return this.isEncrypted;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("invalid zip file");
        }
    }

    public boolean isRunInThread()
    {
        return this.runInThread;
    }

    public boolean isSplitArchive()
    {
        if (this.zipModel == null) {
            this.readZipInfo();
            if (this.zipModel == null) {
                throw new net.lingala.zip4j.exception.ZipException("Zip Model is null");
            }
        }
        return this.zipModel.isSplitArchive();
    }

    public boolean isValidZipFile()
    {
        try {
            this.readZipInfo();
            int v1 = 1;
        } catch (Exception v0) {
            v1 = 0;
        }
        return v1;
    }

    public void mergeSplitFiles(java.io.File p5)
    {
        if (p5 != null) {
            if (!p5.exists()) {
                this.checkZipModel();
                if (this.zipModel != null) {
                    net.lingala.zip4j.util.ArchiveMaintainer v0_1 = new net.lingala.zip4j.util.ArchiveMaintainer();
                    v0_1.initProgressMonitorForMergeOp(this.zipModel, this.progressMonitor);
                    v0_1.mergeSplitZipFiles(this.zipModel, p5, this.progressMonitor, this.runInThread);
                    return;
                } else {
                    throw new net.lingala.zip4j.exception.ZipException("zip model is null, corrupt zip file?");
                }
            } else {
                throw new net.lingala.zip4j.exception.ZipException("output Zip File already exists");
            }
        } else {
            throw new net.lingala.zip4j.exception.ZipException("outputZipFile is null, cannot merge split files");
        }
    }

    public void removeFile(String p5)
    {
        if (net.lingala.zip4j.util.Zip4jUtil.isStringNotNullAndNotEmpty(p5)) {
            if ((this.zipModel == null) && (net.lingala.zip4j.util.Zip4jUtil.checkFileExists(this.file))) {
                this.readZipInfo();
            }
            if (!this.zipModel.isSplitArchive()) {
                net.lingala.zip4j.model.FileHeader v0 = net.lingala.zip4j.util.Zip4jUtil.getFileHeader(this.zipModel, p5);
                if (v0 != null) {
                    this.removeFile(v0);
                    return;
                } else {
                    throw new net.lingala.zip4j.exception.ZipException(new StringBuilder().append("could not find file header for file: ").append(p5).toString());
                }
            } else {
                throw new net.lingala.zip4j.exception.ZipException("Zip file format does not allow updating split/spanned files");
            }
        } else {
            throw new net.lingala.zip4j.exception.ZipException("file name is empty or null, cannot remove file");
        }
    }

    public void removeFile(net.lingala.zip4j.model.FileHeader p5)
    {
        if (p5 != null) {
            if ((this.zipModel == null) && (net.lingala.zip4j.util.Zip4jUtil.checkFileExists(this.file))) {
                this.readZipInfo();
            }
            if (!this.zipModel.isSplitArchive()) {
                net.lingala.zip4j.util.ArchiveMaintainer v0_1 = new net.lingala.zip4j.util.ArchiveMaintainer();
                v0_1.initProgressMonitorForRemoveOp(this.zipModel, p5, this.progressMonitor);
                v0_1.removeZipFile(this.zipModel, p5, this.progressMonitor, this.runInThread);
                return;
            } else {
                throw new net.lingala.zip4j.exception.ZipException("Zip file format does not allow updating split/spanned files");
            }
        } else {
            throw new net.lingala.zip4j.exception.ZipException("file header is null, cannot remove file");
        }
    }

    public void setComment(String p4)
    {
        if (p4 != null) {
            if (net.lingala.zip4j.util.Zip4jUtil.checkFileExists(this.file)) {
                this.readZipInfo();
                if (this.zipModel != null) {
                    if (this.zipModel.getEndCentralDirRecord() != null) {
                        new net.lingala.zip4j.util.ArchiveMaintainer().setComment(this.zipModel, p4);
                        return;
                    } else {
                        throw new net.lingala.zip4j.exception.ZipException("end of central directory is null, cannot set comment");
                    }
                } else {
                    throw new net.lingala.zip4j.exception.ZipException("zipModel is null, cannot update zip file");
                }
            } else {
                throw new net.lingala.zip4j.exception.ZipException("zip file does not exist, cannot set comment for zip file");
            }
        } else {
            throw new net.lingala.zip4j.exception.ZipException("input comment is null, cannot update zip file");
        }
    }

    public void setFileNameCharset(String p4)
    {
        if (net.lingala.zip4j.util.Zip4jUtil.isStringNotNullAndNotEmpty(p4)) {
            if (net.lingala.zip4j.util.Zip4jUtil.isSupportedCharset(p4)) {
                this.fileNameCharset = p4;
                return;
            } else {
                throw new net.lingala.zip4j.exception.ZipException(new StringBuilder().append("unsupported charset: ").append(p4).toString());
            }
        } else {
            throw new net.lingala.zip4j.exception.ZipException("null or empty charset name");
        }
    }

    public void setPassword(String p2)
    {
        if (net.lingala.zip4j.util.Zip4jUtil.isStringNotNullAndNotEmpty(p2)) {
            this.setPassword(p2.toCharArray());
            return;
        } else {
            throw new NullPointerException();
        }
    }

    public void setPassword(char[] p4)
    {
        if (this.zipModel == null) {
            this.readZipInfo();
            if (this.zipModel == null) {
                throw new net.lingala.zip4j.exception.ZipException("Zip Model is null");
            }
        }
        if ((this.zipModel.getCentralDirectory() != null) && (this.zipModel.getCentralDirectory().getFileHeaders() != null)) {
            int v0 = 0;
            while (v0 < this.zipModel.getCentralDirectory().getFileHeaders().size()) {
                if ((this.zipModel.getCentralDirectory().getFileHeaders().get(v0) != null) && (((net.lingala.zip4j.model.FileHeader) this.zipModel.getCentralDirectory().getFileHeaders().get(v0)).isEncrypted())) {
                    ((net.lingala.zip4j.model.FileHeader) this.zipModel.getCentralDirectory().getFileHeaders().get(v0)).setPassword(p4);
                }
                v0++;
            }
            return;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("invalid zip file");
        }
    }

    public void setRunInThread(boolean p1)
    {
        this.runInThread = p1;
        return;
    }
}
