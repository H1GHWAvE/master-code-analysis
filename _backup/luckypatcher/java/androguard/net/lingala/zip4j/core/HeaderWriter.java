package net.lingala.zip4j.core;
public class HeaderWriter {
    private final int ZIP64_EXTRA_BUF;

    public HeaderWriter()
    {
        this.ZIP64_EXTRA_BUF = 50;
        return;
    }

    private byte[] byteArrayListToByteArray(java.util.List p5)
    {
        if (p5 != null) {
            byte[] v1;
            if (p5.size() > 0) {
                v1 = new byte[p5.size()];
                int v0 = 0;
                while (v0 < p5.size()) {
                    v1[v0] = Byte.parseByte(((String) p5.get(v0)));
                    v0++;
                }
            } else {
                v1 = 0;
            }
            return v1;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("input byte array list is null, cannot conver to byte array");
        }
    }

    private void copyByteArrayToArrayList(byte[] p4, java.util.List p5)
    {
        if ((p5 != null) && (p4 != null)) {
            int v0 = 0;
            while (v0 < p4.length) {
                p5.add(Byte.toString(p4[v0]));
                v0++;
            }
            return;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("one of the input parameters is null, cannot copy byte array to array list");
        }
    }

    private int countNumberOfFileHeaderEntriesOnDisk(java.util.ArrayList p6, int p7)
    {
        if (p6 != null) {
            int v2 = 0;
            int v1 = 0;
            while (v1 < p6.size()) {
                if (((net.lingala.zip4j.model.FileHeader) p6.get(v1)).getDiskNumberStart() == p7) {
                    v2++;
                }
                v1++;
            }
            return v2;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("file headers are null, cannot calculate number of entries on this disk");
        }
    }

    private void processHeaderData(net.lingala.zip4j.model.ZipModel p8, java.io.OutputStream p9)
    {
        int v1 = 0;
        try {
            if ((p9 instanceof net.lingala.zip4j.io.SplitOutputStream)) {
                p8.getEndCentralDirRecord().setOffsetOfStartOfCentralDir(((net.lingala.zip4j.io.SplitOutputStream) p9).getFilePointer());
                v1 = ((net.lingala.zip4j.io.SplitOutputStream) p9).getCurrSplitFileCounter();
            }
        } catch (java.io.IOException v2) {
            throw new net.lingala.zip4j.exception.ZipException(v2);
        }
        if (p8.isZip64Format()) {
            if (p8.getZip64EndCentralDirRecord() == null) {
                p8.setZip64EndCentralDirRecord(new net.lingala.zip4j.model.Zip64EndCentralDirRecord());
            }
            if (p8.getZip64EndCentralDirLocator() == null) {
                p8.setZip64EndCentralDirLocator(new net.lingala.zip4j.model.Zip64EndCentralDirLocator());
            }
            p8.getZip64EndCentralDirLocator().setNoOfDiskStartOfZip64EndOfCentralDirRec(v1);
            p8.getZip64EndCentralDirLocator().setTotNumberOfDiscs((v1 + 1));
        }
        p8.getEndCentralDirRecord().setNoOfThisDisk(v1);
        p8.getEndCentralDirRecord().setNoOfThisDiskStartOfCentralDir(v1);
        return;
    }

    private void updateCompressedSizeInLocalFileHeader(net.lingala.zip4j.io.SplitOutputStream p8, net.lingala.zip4j.model.LocalFileHeader p9, long p10, long p12, byte[] p14, boolean p15)
    {
        if (p8 != null) {
            try {
                if (!p9.isWriteComprSizeInZip64ExtraRecord()) {
                    p8.seek((p10 + p12));
                    p8.write(p14);
                } else {
                    if (p14.length == 8) {
                        long v1 = (((((((((p10 + p12) + 4) + 4) + 2) + 2) + ((long) p9.getFileNameLength())) + 2) + 2) + 8);
                        if (p12 == 22) {
                            v1 += 8;
                        }
                        p8.seek(v1);
                        p8.write(p14);
                    } else {
                        throw new net.lingala.zip4j.exception.ZipException("attempting to write a non 8-byte compressed size block for a zip64 file");
                    }
                }
            } catch (java.io.IOException v0) {
                throw new net.lingala.zip4j.exception.ZipException(v0);
            }
            return;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("invalid output stream, cannot update compressed size for local file header");
        }
    }

    private int writeCentralDirectory(net.lingala.zip4j.model.ZipModel p7, java.io.OutputStream p8, java.util.List p9)
    {
        if ((p7 != null) && (p8 != null)) {
            if ((p7.getCentralDirectory() != null) && ((p7.getCentralDirectory().getFileHeaders() != null) && (p7.getCentralDirectory().getFileHeaders().size() > 0))) {
                int v2 = 0;
                int v1 = 0;
                while (v1 < p7.getCentralDirectory().getFileHeaders().size()) {
                    v2 += this.writeFileHeader(p7, ((net.lingala.zip4j.model.FileHeader) p7.getCentralDirectory().getFileHeaders().get(v1)), p8, p9);
                    v1++;
                }
            } else {
                v2 = 0;
            }
            return v2;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("input parameters is null, cannot write central directory");
        }
    }

    private void writeEndOfCentralDirectoryRecord(net.lingala.zip4j.model.ZipModel p13, java.io.OutputStream p14, int p15, long p16, java.util.List p18)
    {
        if ((p13 != null) && (p14 != null)) {
            try {
                byte[] v8 = new byte[2];
                byte[] v4 = new byte[4];
                byte[] v5 = new byte[8];
                net.lingala.zip4j.util.Raw.writeIntLittleEndian(v4, 0, ((int) p13.getEndCentralDirRecord().getSignature()));
                this.copyByteArrayToArrayList(v4, p18);
                net.lingala.zip4j.util.Raw.writeShortLittleEndian(v8, 0, ((short) p13.getEndCentralDirRecord().getNoOfThisDisk()));
                this.copyByteArrayToArrayList(v8, p18);
                net.lingala.zip4j.util.Raw.writeShortLittleEndian(v8, 0, ((short) p13.getEndCentralDirRecord().getNoOfThisDiskStartOfCentralDir()));
                this.copyByteArrayToArrayList(v8, p18);
            } catch (Exception v3) {
                throw new net.lingala.zip4j.exception.ZipException(v3);
            }
            if ((p13.getCentralDirectory() != null) && (p13.getCentralDirectory().getFileHeaders() != null)) {
                int v7;
                int v6 = p13.getCentralDirectory().getFileHeaders().size();
                if (!p13.isSplitArchive()) {
                    v7 = v6;
                } else {
                    v7 = this.countNumberOfFileHeaderEntriesOnDisk(p13.getCentralDirectory().getFileHeaders(), p13.getEndCentralDirRecord().getNoOfThisDisk());
                }
                net.lingala.zip4j.util.Raw.writeShortLittleEndian(v8, 0, ((short) v7));
                this.copyByteArrayToArrayList(v8, p18);
                net.lingala.zip4j.util.Raw.writeShortLittleEndian(v8, 0, ((short) v6));
                this.copyByteArrayToArrayList(v8, p18);
                net.lingala.zip4j.util.Raw.writeIntLittleEndian(v4, 0, p15);
                this.copyByteArrayToArrayList(v4, p18);
                if (p16 <= 2.1219957905e-314) {
                    net.lingala.zip4j.util.Raw.writeLongLittleEndian(v5, 0, p16);
                    System.arraycopy(v5, 0, v4, 0, 4);
                    this.copyByteArrayToArrayList(v4, p18);
                } else {
                    net.lingala.zip4j.util.Raw.writeLongLittleEndian(v5, 0, 2.1219957905e-314);
                    System.arraycopy(v5, 0, v4, 0, 4);
                    this.copyByteArrayToArrayList(v4, p18);
                }
                int v2 = 0;
                if (p13.getEndCentralDirRecord().getComment() != null) {
                    v2 = p13.getEndCentralDirRecord().getCommentLength();
                }
                net.lingala.zip4j.util.Raw.writeShortLittleEndian(v8, 0, ((short) v2));
                this.copyByteArrayToArrayList(v8, p18);
                if (v2 > 0) {
                    this.copyByteArrayToArrayList(p13.getEndCentralDirRecord().getCommentBytes(), p18);
                }
                return;
            } else {
                throw new net.lingala.zip4j.exception.ZipException("invalid central directory/file headers, cannot write end of central directory record");
            }
        } else {
            throw new net.lingala.zip4j.exception.ZipException("zip model or output stream is null, cannot write end of central directory record");
        }
    }

    private int writeFileHeader(net.lingala.zip4j.model.ZipModel p24, net.lingala.zip4j.model.FileHeader p25, java.io.OutputStream p26, java.util.List p27)
    {
        if ((p25 != null) && (p26 != null)) {
            try {
                byte[] v15 = new byte[2];
                byte[] v12 = new byte[4];
                byte[] v13 = new byte[8];
                byte[] v9 = new byte[2];
                v9 = {0, 0};
                byte[] v8 = new byte[4];
                v8 = {0, 0, 0, 0};
                int v17 = 0;
                int v18 = 0;
                net.lingala.zip4j.util.Raw.writeIntLittleEndian(v12, 0, p25.getSignature());
                this.copyByteArrayToArrayList(v12, p27);
                net.lingala.zip4j.util.Raw.writeShortLittleEndian(v15, 0, ((short) p25.getVersionMadeBy()));
                this.copyByteArrayToArrayList(v15, p27);
                net.lingala.zip4j.util.Raw.writeShortLittleEndian(v15, 0, ((short) p25.getVersionNeededToExtract()));
                this.copyByteArrayToArrayList(v15, p27);
                this.copyByteArrayToArrayList(p25.getGeneralPurposeFlag(), p27);
                net.lingala.zip4j.util.Raw.writeShortLittleEndian(v15, 0, ((short) p25.getCompressionMethod()));
                this.copyByteArrayToArrayList(v15, p27);
                net.lingala.zip4j.util.Raw.writeIntLittleEndian(v12, 0, p25.getLastModFileTime());
                this.copyByteArrayToArrayList(v12, p27);
                net.lingala.zip4j.util.Raw.writeIntLittleEndian(v12, 0, ((int) p25.getCrc32()));
                this.copyByteArrayToArrayList(v12, p27);
            } catch (Exception v7) {
                int v19_59 = new net.lingala.zip4j.exception.ZipException;
                v19_59(v7);
                throw v19_59;
            }
            if ((p25.getCompressedSize() < 2.1219957905e-314) && ((p25.getUncompressedSize() + 50) < 2.1219957905e-314)) {
                net.lingala.zip4j.util.Raw.writeLongLittleEndian(v13, 0, p25.getCompressedSize());
                System.arraycopy(v13, 0, v12, 0, 4);
                this.copyByteArrayToArrayList(v12, p27);
                net.lingala.zip4j.util.Raw.writeLongLittleEndian(v13, 0, p25.getUncompressedSize());
                System.arraycopy(v13, 0, v12, 0, 4);
                this.copyByteArrayToArrayList(v12, p27);
            } else {
                net.lingala.zip4j.util.Raw.writeLongLittleEndian(v13, 0, 2.1219957905e-314);
                System.arraycopy(v13, 0, v12, 0, 4);
                this.copyByteArrayToArrayList(v12, p27);
                this.copyByteArrayToArrayList(v12, p27);
                v17 = 1;
            }
            net.lingala.zip4j.util.Raw.writeShortLittleEndian(v15, 0, ((short) p25.getFileNameLength()));
            this.copyByteArrayToArrayList(v15, p27);
            byte[] v14 = new byte[4];
            if (p25.getOffsetLocalHeader() <= 2.1219957905e-314) {
                net.lingala.zip4j.util.Raw.writeLongLittleEndian(v13, 0, p25.getOffsetLocalHeader());
                System.arraycopy(v13, 0, v14, 0, 4);
            } else {
                net.lingala.zip4j.util.Raw.writeLongLittleEndian(v13, 0, 2.1219957905e-314);
                System.arraycopy(v13, 0, v14, 0, 4);
                v18 = 1;
            }
            int v10 = 0;
            if ((v17 != 0) || (v18 != 0)) {
                v10 = (0 + 4);
                if (v17 != 0) {
                    v10 += 16;
                }
                if (v18 != 0) {
                    v10 += 8;
                }
            }
            if (p25.getAesExtraDataRecord() != null) {
                v10 += 11;
            }
            net.lingala.zip4j.util.Raw.writeShortLittleEndian(v15, 0, ((short) v10));
            this.copyByteArrayToArrayList(v15, p27);
            this.copyByteArrayToArrayList(v9, p27);
            net.lingala.zip4j.util.Raw.writeShortLittleEndian(v15, 0, ((short) p25.getDiskNumberStart()));
            this.copyByteArrayToArrayList(v15, p27);
            this.copyByteArrayToArrayList(v9, p27);
            if (p25.getExternalFileAttr() == null) {
                this.copyByteArrayToArrayList(v8, p27);
            } else {
                this.copyByteArrayToArrayList(p25.getExternalFileAttr(), p27);
            }
            int v16_17;
            this.copyByteArrayToArrayList(v14, p27);
            if (!net.lingala.zip4j.util.Zip4jUtil.isStringNotNullAndNotEmpty(p24.getFileNameCharset())) {
                this.copyByteArrayToArrayList(net.lingala.zip4j.util.Zip4jUtil.convertCharset(p25.getFileName()), p27);
                v16_17 = (net.lingala.zip4j.util.Zip4jUtil.getEncodedStringLength(p25.getFileName()) + 46);
            } else {
                byte[] v11 = p25.getFileName().getBytes(p24.getFileNameCharset());
                this.copyByteArrayToArrayList(v11, p27);
                v16_17 = (v11.length + 46);
            }
            if ((v17 != 0) || (v18 != 0)) {
                p24.setZip64Format(1);
                net.lingala.zip4j.util.Raw.writeShortLittleEndian(v15, 0, 1);
                this.copyByteArrayToArrayList(v15, p27);
                int v16_18 = (v16_17 + 2);
                int v5 = 0;
                if (v17 != 0) {
                    v5 = (0 + 16);
                }
                if (v18 != 0) {
                    v5 += 8;
                }
                net.lingala.zip4j.util.Raw.writeShortLittleEndian(v15, 0, ((short) v5));
                this.copyByteArrayToArrayList(v15, p27);
                v16_17 = (v16_18 + 2);
                if (v17 != 0) {
                    net.lingala.zip4j.util.Raw.writeLongLittleEndian(v13, 0, p25.getUncompressedSize());
                    this.copyByteArrayToArrayList(v13, p27);
                    int v16_19 = (v16_17 + 8);
                    net.lingala.zip4j.util.Raw.writeLongLittleEndian(v13, 0, p25.getCompressedSize());
                    this.copyByteArrayToArrayList(v13, p27);
                    v16_17 = (v16_19 + 8);
                }
                if (v18 != 0) {
                    net.lingala.zip4j.util.Raw.writeLongLittleEndian(v13, 0, p25.getOffsetLocalHeader());
                    this.copyByteArrayToArrayList(v13, p27);
                    v16_17 += 8;
                }
            }
            if (p25.getAesExtraDataRecord() != null) {
                net.lingala.zip4j.model.AESExtraDataRecord v3 = p25.getAesExtraDataRecord();
                net.lingala.zip4j.util.Raw.writeShortLittleEndian(v15, 0, ((short) ((int) v3.getSignature())));
                this.copyByteArrayToArrayList(v15, p27);
                net.lingala.zip4j.util.Raw.writeShortLittleEndian(v15, 0, ((short) v3.getDataSize()));
                this.copyByteArrayToArrayList(v15, p27);
                net.lingala.zip4j.util.Raw.writeShortLittleEndian(v15, 0, ((short) v3.getVersionNumber()));
                this.copyByteArrayToArrayList(v15, p27);
                this.copyByteArrayToArrayList(v3.getVendorID().getBytes(), p27);
                byte[] v4 = new byte[1];
                v4[0] = ((byte) v3.getAesStrength());
                this.copyByteArrayToArrayList(v4, p27);
                net.lingala.zip4j.util.Raw.writeShortLittleEndian(v15, 0, ((short) v3.getCompressionMethod()));
                this.copyByteArrayToArrayList(v15, p27);
                v16_17 += 11;
            }
            return v16_17;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("input parameters is null, cannot write local file header");
        }
    }

    private void writeZip64EndOfCentralDirectoryLocator(net.lingala.zip4j.model.ZipModel p8, java.io.OutputStream p9, java.util.List p10)
    {
        if ((p8 != null) && (p9 != null)) {
            try {
                byte[] v1 = new byte[4];
                byte[] v2 = new byte[8];
                net.lingala.zip4j.util.Raw.writeIntLittleEndian(v1, 0, 117853008);
                this.copyByteArrayToArrayList(v1, p10);
                net.lingala.zip4j.util.Raw.writeIntLittleEndian(v1, 0, p8.getZip64EndCentralDirLocator().getNoOfDiskStartOfZip64EndOfCentralDirRec());
                this.copyByteArrayToArrayList(v1, p10);
                net.lingala.zip4j.util.Raw.writeLongLittleEndian(v2, 0, p8.getZip64EndCentralDirLocator().getOffsetZip64EndOfCentralDirRec());
                this.copyByteArrayToArrayList(v2, p10);
                net.lingala.zip4j.util.Raw.writeIntLittleEndian(v1, 0, p8.getZip64EndCentralDirLocator().getTotNumberOfDiscs());
                this.copyByteArrayToArrayList(v1, p10);
                return;
            } catch (net.lingala.zip4j.exception.ZipException v3) {
                throw v3;
            } catch (Exception v0) {
                throw new net.lingala.zip4j.exception.ZipException(v0);
            }
        } else {
            throw new net.lingala.zip4j.exception.ZipException("zip model or output stream is null, cannot write zip64 end of central directory locator");
        }
    }

    private void writeZip64EndOfCentralDirectoryRecord(net.lingala.zip4j.model.ZipModel p14, java.io.OutputStream p15, int p16, long p17, java.util.List p19)
    {
        if ((p14 != null) && (p15 != null)) {
            try {
                byte[] v8 = new byte[2];
                byte[] v3 = new byte[2];
                v3 = {0, 0};
                byte[] v4 = new byte[4];
                byte[] v5 = new byte[8];
                net.lingala.zip4j.util.Raw.writeIntLittleEndian(v4, 0, 101075792);
                this.copyByteArrayToArrayList(v4, p19);
                net.lingala.zip4j.util.Raw.writeLongLittleEndian(v5, 0, 44);
                this.copyByteArrayToArrayList(v5, p19);
            } catch (net.lingala.zip4j.exception.ZipException v9) {
                throw v9;
            } catch (Exception v2) {
                throw new net.lingala.zip4j.exception.ZipException(v2);
            }
            if ((p14.getCentralDirectory() == null) || ((p14.getCentralDirectory().getFileHeaders() == null) || (p14.getCentralDirectory().getFileHeaders().size() <= 0))) {
                this.copyByteArrayToArrayList(v3, p19);
                this.copyByteArrayToArrayList(v3, p19);
            } else {
                net.lingala.zip4j.util.Raw.writeShortLittleEndian(v8, 0, ((short) ((net.lingala.zip4j.model.FileHeader) p14.getCentralDirectory().getFileHeaders().get(0)).getVersionMadeBy()));
                this.copyByteArrayToArrayList(v8, p19);
                net.lingala.zip4j.util.Raw.writeShortLittleEndian(v8, 0, ((short) ((net.lingala.zip4j.model.FileHeader) p14.getCentralDirectory().getFileHeaders().get(0)).getVersionNeededToExtract()));
                this.copyByteArrayToArrayList(v8, p19);
            }
            net.lingala.zip4j.util.Raw.writeIntLittleEndian(v4, 0, p14.getEndCentralDirRecord().getNoOfThisDisk());
            this.copyByteArrayToArrayList(v4, p19);
            net.lingala.zip4j.util.Raw.writeIntLittleEndian(v4, 0, p14.getEndCentralDirRecord().getNoOfThisDiskStartOfCentralDir());
            this.copyByteArrayToArrayList(v4, p19);
            int v7 = 0;
            if ((p14.getCentralDirectory() != null) && (p14.getCentralDirectory().getFileHeaders() != null)) {
                int v6 = p14.getCentralDirectory().getFileHeaders().size();
                if (!p14.isSplitArchive()) {
                    v7 = v6;
                } else {
                    this.countNumberOfFileHeaderEntriesOnDisk(p14.getCentralDirectory().getFileHeaders(), p14.getEndCentralDirRecord().getNoOfThisDisk());
                }
                net.lingala.zip4j.util.Raw.writeLongLittleEndian(v5, 0, ((long) v7));
                this.copyByteArrayToArrayList(v5, p19);
                net.lingala.zip4j.util.Raw.writeLongLittleEndian(v5, 0, ((long) v6));
                this.copyByteArrayToArrayList(v5, p19);
                net.lingala.zip4j.util.Raw.writeLongLittleEndian(v5, 0, ((long) p16));
                this.copyByteArrayToArrayList(v5, p19);
                net.lingala.zip4j.util.Raw.writeLongLittleEndian(v5, 0, p17);
                this.copyByteArrayToArrayList(v5, p19);
                return;
            } else {
                throw new net.lingala.zip4j.exception.ZipException("invalid central directory/file headers, cannot write end of central directory record");
            }
        } else {
            throw new net.lingala.zip4j.exception.ZipException("zip model or output stream is null, cannot write zip64 end of central directory record");
        }
    }

    private void writeZipHeaderBytes(net.lingala.zip4j.model.ZipModel p5, java.io.OutputStream p6, byte[] p7)
    {
        if (p7 != null) {
            try {
                if ((!(p6 instanceof net.lingala.zip4j.io.SplitOutputStream)) || (!((net.lingala.zip4j.io.SplitOutputStream) p6).checkBuffSizeAndStartNextSplitFile(p7.length))) {
                    p6.write(p7);
                } else {
                    this.finalizeZipFile(p5, p6);
                }
            } catch (java.io.IOException v1) {
                throw new net.lingala.zip4j.exception.ZipException(v1);
            }
            return;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("invalid buff to write as zip headers");
        }
    }

    public void finalizeZipFile(net.lingala.zip4j.model.ZipModel p10, java.io.OutputStream p11)
    {
        if ((p10 != null) && (p11 != null)) {
            try {
                this.processHeaderData(p10, p11);
                long v5 = p10.getEndCentralDirRecord().getOffsetOfStartOfCentralDir();
                java.util.ArrayList v7_1 = new java.util.ArrayList();
                int v4 = this.writeCentralDirectory(p10, p11, v7_1);
            } catch (Exception v8_1) {
                throw v8_1;
            } catch (Exception v8_0) {
                throw new net.lingala.zip4j.exception.ZipException(v8_0);
            }
            if (p10.isZip64Format()) {
                if (p10.getZip64EndCentralDirRecord() == null) {
                    p10.setZip64EndCentralDirRecord(new net.lingala.zip4j.model.Zip64EndCentralDirRecord());
                }
                if (p10.getZip64EndCentralDirLocator() == null) {
                    p10.setZip64EndCentralDirLocator(new net.lingala.zip4j.model.Zip64EndCentralDirLocator());
                }
                p10.getZip64EndCentralDirLocator().setOffsetZip64EndOfCentralDirRec((((long) v4) + v5));
                if (!(p11 instanceof net.lingala.zip4j.io.SplitOutputStream)) {
                    p10.getZip64EndCentralDirLocator().setNoOfDiskStartOfZip64EndOfCentralDirRec(0);
                    p10.getZip64EndCentralDirLocator().setTotNumberOfDiscs(1);
                } else {
                    p10.getZip64EndCentralDirLocator().setNoOfDiskStartOfZip64EndOfCentralDirRec(((net.lingala.zip4j.io.SplitOutputStream) p11).getCurrSplitFileCounter());
                    p10.getZip64EndCentralDirLocator().setTotNumberOfDiscs((((net.lingala.zip4j.io.SplitOutputStream) p11).getCurrSplitFileCounter() + 1));
                }
                this.writeZip64EndOfCentralDirectoryRecord(p10, p11, v4, v5, v7_1);
                this.writeZip64EndOfCentralDirectoryLocator(p10, p11, v7_1);
            }
            this.writeEndOfCentralDirectoryRecord(p10, p11, v4, v5, v7_1);
            this.writeZipHeaderBytes(p10, p11, this.byteArrayListToByteArray(v7_1));
            return;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("input parameters is null, cannot finalize zip file");
        }
    }

    public void finalizeZipFileWithoutValidations(net.lingala.zip4j.model.ZipModel p9, java.io.OutputStream p10)
    {
        if ((p9 != null) && (p10 != null)) {
            try {
                java.util.ArrayList v6_1 = new java.util.ArrayList();
                long v4 = p9.getEndCentralDirRecord().getOffsetOfStartOfCentralDir();
                int v3 = this.writeCentralDirectory(p9, p10, v6_1);
            } catch (Exception v7_1) {
                throw v7_1;
            } catch (Exception v7_0) {
                throw new net.lingala.zip4j.exception.ZipException(v7_0);
            }
            if (p9.isZip64Format()) {
                if (p9.getZip64EndCentralDirRecord() == null) {
                    p9.setZip64EndCentralDirRecord(new net.lingala.zip4j.model.Zip64EndCentralDirRecord());
                }
                if (p9.getZip64EndCentralDirLocator() == null) {
                    p9.setZip64EndCentralDirLocator(new net.lingala.zip4j.model.Zip64EndCentralDirLocator());
                }
                p9.getZip64EndCentralDirLocator().setOffsetZip64EndOfCentralDirRec((((long) v3) + v4));
                this.writeZip64EndOfCentralDirectoryRecord(p9, p10, v3, v4, v6_1);
                this.writeZip64EndOfCentralDirectoryLocator(p9, p10, v6_1);
            }
            this.writeEndOfCentralDirectoryRecord(p9, p10, v3, v4, v6_1);
            this.writeZipHeaderBytes(p9, p10, this.byteArrayListToByteArray(v6_1));
            return;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("input parameters is null, cannot finalize zip file without validations");
        }
    }

    public void updateLocalFileHeader(net.lingala.zip4j.model.LocalFileHeader p20, long p21, int p23, net.lingala.zip4j.model.ZipModel p24, byte[] p25, int p26, net.lingala.zip4j.io.SplitOutputStream p27)
    {
        if ((p20 != null) && ((p21 >= 0) && (p24 != null))) {
            int v10 = 0;
            try {
                net.lingala.zip4j.io.SplitOutputStream v2;
                if (p26 == p27.getCurrSplitFileCounter()) {
                    v2 = p27;
                } else {
                    String v15_1;
                    java.io.File v18 = new java.io.File;
                    v18(p24.getZipFile());
                    String v17 = v18.getParent();
                    String v16 = net.lingala.zip4j.util.Zip4jUtil.getZipFileNameWithoutExt(v18.getName());
                    String v15_0 = new StringBuilder().append(v17).append(System.getProperty("file.separator")).toString();
                    if (p26 >= 9) {
                        v15_1 = new StringBuilder().append(v15_0).append(v16).append(".z").append((p26 + 1)).toString();
                    } else {
                        v15_1 = new StringBuilder().append(v15_0).append(v16).append(".z0").append((p26 + 1)).toString();
                    }
                    v10 = 1;
                    v2 = new net.lingala.zip4j.io.SplitOutputStream(new java.io.File(v15_1));
                }
            } catch (Exception v14) {
                throw new net.lingala.zip4j.exception.ZipException(v14);
            }
            long v11 = v2.getFilePointer();
            switch (p23) {
                case 14:
                    v2.seek((((long) p23) + p21));
                    v2.write(p25);
                    break;
                case 18:
                case 22:
                    this.updateCompressedSizeInLocalFileHeader(v2, p20, p21, ((long) p23), p25, p24.isZip64Format());
                    break;
            }
            if (v10 == 0) {
                p27.seek(v11);
            } else {
                v2.close();
            }
            return;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("invalid input parameters, cannot update local file header");
        }
    }

    public int writeExtendedLocalHeader(net.lingala.zip4j.model.LocalFileHeader p13, java.io.OutputStream p14)
    {
        if ((p13 != null) && (p14 != null)) {
            java.util.ArrayList v0_1 = new java.util.ArrayList();
            byte[] v4 = new byte[4];
            net.lingala.zip4j.util.Raw.writeIntLittleEndian(v4, 0, 134695760);
            this.copyByteArrayToArrayList(v4, v0_1);
            net.lingala.zip4j.util.Raw.writeIntLittleEndian(v4, 0, ((int) p13.getCrc32()));
            this.copyByteArrayToArrayList(v4, v0_1);
            long v1 = p13.getCompressedSize();
            if (v1 >= 2147483647) {
                v1 = 2147483647;
            }
            net.lingala.zip4j.util.Raw.writeIntLittleEndian(v4, 0, ((int) v1));
            this.copyByteArrayToArrayList(v4, v0_1);
            long v5 = p13.getUncompressedSize();
            if (v5 >= 2147483647) {
                v5 = 2147483647;
            }
            net.lingala.zip4j.util.Raw.writeIntLittleEndian(v4, 0, ((int) v5));
            this.copyByteArrayToArrayList(v4, v0_1);
            byte[] v3 = this.byteArrayListToByteArray(v0_1);
            p14.write(v3);
            return v3.length;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("input parameters is null, cannot write extended local header");
        }
    }

    public int writeLocalFileHeader(net.lingala.zip4j.model.ZipModel p23, net.lingala.zip4j.model.LocalFileHeader p24, java.io.OutputStream p25)
    {
        if (p24 != null) {
            try {
                java.util.ArrayList v5_1 = new java.util.ArrayList();
                byte[] v14 = new byte[2];
                byte[] v11 = new byte[4];
                byte[] v13 = new byte[8];
                byte[] v8 = new byte[8];
                v8 = {0, 0, 0, 0, 0, 0, 0, 0};
                net.lingala.zip4j.util.Raw.writeIntLittleEndian(v11, 0, p24.getSignature());
                this.copyByteArrayToArrayList(v11, v5_1);
                net.lingala.zip4j.util.Raw.writeShortLittleEndian(v14, 0, ((short) p24.getVersionNeededToExtract()));
                this.copyByteArrayToArrayList(v14, v5_1);
                this.copyByteArrayToArrayList(p24.getGeneralPurposeFlag(), v5_1);
                net.lingala.zip4j.util.Raw.writeShortLittleEndian(v14, 0, ((short) p24.getCompressionMethod()));
                this.copyByteArrayToArrayList(v14, v5_1);
                net.lingala.zip4j.util.Raw.writeIntLittleEndian(v11, 0, p24.getLastModFileTime());
                this.copyByteArrayToArrayList(v11, v5_1);
                net.lingala.zip4j.util.Raw.writeIntLittleEndian(v11, 0, ((int) p24.getCrc32()));
                this.copyByteArrayToArrayList(v11, v5_1);
                int v17 = 0;
            } catch (Exception v7_1) {
                throw v7_1;
            } catch (Exception v7_0) {
                int v18_43 = new net.lingala.zip4j.exception.ZipException;
                v18_43(v7_0);
                throw v18_43;
            }
            if ((50 + p24.getUncompressedSize()) < 2.1219957905e-314) {
                net.lingala.zip4j.util.Raw.writeLongLittleEndian(v13, 0, p24.getCompressedSize());
                System.arraycopy(v13, 0, v11, 0, 4);
                this.copyByteArrayToArrayList(v11, v5_1);
                net.lingala.zip4j.util.Raw.writeLongLittleEndian(v13, 0, p24.getUncompressedSize());
                System.arraycopy(v13, 0, v11, 0, 4);
                this.copyByteArrayToArrayList(v11, v5_1);
                p24.setWriteComprSizeInZip64ExtraRecord(0);
            } else {
                net.lingala.zip4j.util.Raw.writeLongLittleEndian(v13, 0, 2.1219957905e-314);
                System.arraycopy(v13, 0, v11, 0, 4);
                this.copyByteArrayToArrayList(v11, v5_1);
                this.copyByteArrayToArrayList(v11, v5_1);
                p23.setZip64Format(1);
                v17 = 1;
                p24.setWriteComprSizeInZip64ExtraRecord(1);
            }
            net.lingala.zip4j.util.Raw.writeShortLittleEndian(v14, 0, ((short) p24.getFileNameLength()));
            this.copyByteArrayToArrayList(v14, v5_1);
            int v9 = 0;
            if (v17 != 0) {
                v9 = (0 + 20);
            }
            if (p24.getAesExtraDataRecord() != null) {
                v9 += 11;
            }
            net.lingala.zip4j.util.Raw.writeShortLittleEndian(v14, 0, ((short) v9));
            this.copyByteArrayToArrayList(v14, v5_1);
            if (!net.lingala.zip4j.util.Zip4jUtil.isStringNotNullAndNotEmpty(p23.getFileNameCharset())) {
                this.copyByteArrayToArrayList(net.lingala.zip4j.util.Zip4jUtil.convertCharset(p24.getFileName()), v5_1);
            } else {
                this.copyByteArrayToArrayList(p24.getFileName().getBytes(p23.getFileNameCharset()), v5_1);
            }
            if (v17 != 0) {
                net.lingala.zip4j.util.Raw.writeShortLittleEndian(v14, 0, 1);
                this.copyByteArrayToArrayList(v14, v5_1);
                net.lingala.zip4j.util.Raw.writeShortLittleEndian(v14, 0, 16);
                this.copyByteArrayToArrayList(v14, v5_1);
                net.lingala.zip4j.util.Raw.writeLongLittleEndian(v13, 0, p24.getUncompressedSize());
                this.copyByteArrayToArrayList(v13, v5_1);
                this.copyByteArrayToArrayList(v8, v5_1);
            }
            if (p24.getAesExtraDataRecord() != null) {
                net.lingala.zip4j.model.AESExtraDataRecord v3 = p24.getAesExtraDataRecord();
                net.lingala.zip4j.util.Raw.writeShortLittleEndian(v14, 0, ((short) ((int) v3.getSignature())));
                this.copyByteArrayToArrayList(v14, v5_1);
                net.lingala.zip4j.util.Raw.writeShortLittleEndian(v14, 0, ((short) v3.getDataSize()));
                this.copyByteArrayToArrayList(v14, v5_1);
                net.lingala.zip4j.util.Raw.writeShortLittleEndian(v14, 0, ((short) v3.getVersionNumber()));
                this.copyByteArrayToArrayList(v14, v5_1);
                this.copyByteArrayToArrayList(v3.getVendorID().getBytes(), v5_1);
                byte[] v4 = new byte[1];
                v4[0] = ((byte) v3.getAesStrength());
                this.copyByteArrayToArrayList(v4, v5_1);
                net.lingala.zip4j.util.Raw.writeShortLittleEndian(v14, 0, ((short) v3.getCompressionMethod()));
                this.copyByteArrayToArrayList(v14, v5_1);
            }
            byte[] v12 = this.byteArrayListToByteArray(v5_1);
            p25.write(v12);
            return v12.length;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("input parameters are null, cannot write local file header");
        }
    }
}
