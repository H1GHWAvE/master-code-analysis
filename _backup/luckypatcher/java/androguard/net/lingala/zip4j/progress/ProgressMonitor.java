package net.lingala.zip4j.progress;
public class ProgressMonitor {
    public static final int OPERATION_ADD = 0;
    public static final int OPERATION_CALC_CRC = 3;
    public static final int OPERATION_EXTRACT = 1;
    public static final int OPERATION_MERGE = 4;
    public static final int OPERATION_NONE = 255;
    public static final int OPERATION_REMOVE = 2;
    public static final int RESULT_CANCELLED = 3;
    public static final int RESULT_ERROR = 2;
    public static final int RESULT_SUCCESS = 0;
    public static final int RESULT_WORKING = 1;
    public static final int STATE_BUSY = 1;
    public static final int STATE_READY;
    private boolean cancelAllTasks;
    private int currentOperation;
    private Throwable exception;
    private String fileName;
    private boolean pause;
    private int percentDone;
    private int result;
    private int state;
    private long totalWork;
    private long workCompleted;

    public ProgressMonitor()
    {
        this.reset();
        this.percentDone = 0;
        return;
    }

    public void cancelAllTasks()
    {
        this.cancelAllTasks = 1;
        return;
    }

    public void endProgressMonitorError(Throwable p2)
    {
        this.reset();
        this.result = 2;
        this.exception = p2;
        return;
    }

    public void endProgressMonitorSuccess()
    {
        this.reset();
        this.result = 0;
        return;
    }

    public void fullReset()
    {
        this.reset();
        this.exception = 0;
        this.result = 0;
        return;
    }

    public int getCurrentOperation()
    {
        return this.currentOperation;
    }

    public Throwable getException()
    {
        return this.exception;
    }

    public String getFileName()
    {
        return this.fileName;
    }

    public int getPercentDone()
    {
        return this.percentDone;
    }

    public int getResult()
    {
        return this.result;
    }

    public int getState()
    {
        return this.state;
    }

    public long getTotalWork()
    {
        return this.totalWork;
    }

    public long getWorkCompleted()
    {
        return this.workCompleted;
    }

    public boolean isCancelAllTasks()
    {
        return this.cancelAllTasks;
    }

    public boolean isPause()
    {
        return this.pause;
    }

    public void reset()
    {
        this.currentOperation = -1;
        this.state = 0;
        this.fileName = 0;
        this.totalWork = 0;
        this.workCompleted = 0;
        this.percentDone = 0;
        return;
    }

    public void setCurrentOperation(int p1)
    {
        this.currentOperation = p1;
        return;
    }

    public void setException(Throwable p1)
    {
        this.exception = p1;
        return;
    }

    public void setFileName(String p1)
    {
        this.fileName = p1;
        return;
    }

    public void setPause(boolean p1)
    {
        this.pause = p1;
        return;
    }

    public void setPercentDone(int p1)
    {
        this.percentDone = p1;
        return;
    }

    public void setResult(int p1)
    {
        this.result = p1;
        return;
    }

    public void setState(int p1)
    {
        this.state = p1;
        return;
    }

    public void setTotalWork(long p1)
    {
        this.totalWork = p1;
        return;
    }

    public void updateWorkCompleted(long p6)
    {
        this.workCompleted = (this.workCompleted + p6);
        if (this.totalWork > 0) {
            this.percentDone = ((int) ((this.workCompleted * 100) / this.totalWork));
            if (this.percentDone > 100) {
                this.percentDone = 100;
            }
        }
        while (this.pause) {
            new com.chelpus.Utils("w").waitLP(150);
        }
        return;
    }
}
