package net.lingala.zip4j.model;
public class DigitalSignature {
    private int headerSignature;
    private String signatureData;
    private int sizeOfData;

    public DigitalSignature()
    {
        return;
    }

    public int getHeaderSignature()
    {
        return this.headerSignature;
    }

    public String getSignatureData()
    {
        return this.signatureData;
    }

    public int getSizeOfData()
    {
        return this.sizeOfData;
    }

    public void setHeaderSignature(int p1)
    {
        this.headerSignature = p1;
        return;
    }

    public void setSignatureData(String p1)
    {
        this.signatureData = p1;
        return;
    }

    public void setSizeOfData(int p1)
    {
        this.sizeOfData = p1;
        return;
    }
}
