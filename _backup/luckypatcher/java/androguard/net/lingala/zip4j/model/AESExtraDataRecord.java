package net.lingala.zip4j.model;
public class AESExtraDataRecord {
    private int aesStrength;
    private int compressionMethod;
    private int dataSize;
    private long signature;
    private String vendorID;
    private int versionNumber;

    public AESExtraDataRecord()
    {
        this.signature = -1;
        this.dataSize = -1;
        this.versionNumber = -1;
        this.vendorID = 0;
        this.aesStrength = -1;
        this.compressionMethod = -1;
        return;
    }

    public int getAesStrength()
    {
        return this.aesStrength;
    }

    public int getCompressionMethod()
    {
        return this.compressionMethod;
    }

    public int getDataSize()
    {
        return this.dataSize;
    }

    public long getSignature()
    {
        return this.signature;
    }

    public String getVendorID()
    {
        return this.vendorID;
    }

    public int getVersionNumber()
    {
        return this.versionNumber;
    }

    public void setAesStrength(int p1)
    {
        this.aesStrength = p1;
        return;
    }

    public void setCompressionMethod(int p1)
    {
        this.compressionMethod = p1;
        return;
    }

    public void setDataSize(int p1)
    {
        this.dataSize = p1;
        return;
    }

    public void setSignature(long p1)
    {
        this.signature = p1;
        return;
    }

    public void setVendorID(String p1)
    {
        this.vendorID = p1;
        return;
    }

    public void setVersionNumber(int p1)
    {
        this.versionNumber = p1;
        return;
    }
}
