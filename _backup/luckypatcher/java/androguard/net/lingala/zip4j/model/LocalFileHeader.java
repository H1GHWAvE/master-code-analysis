package net.lingala.zip4j.model;
public class LocalFileHeader {
    private net.lingala.zip4j.model.AESExtraDataRecord aesExtraDataRecord;
    private long compressedSize;
    private int compressionMethod;
    private long crc32;
    private byte[] crcBuff;
    private boolean dataDescriptorExists;
    private int encryptionMethod;
    private java.util.ArrayList extraDataRecords;
    private byte[] extraField;
    private int extraFieldLength;
    private String fileName;
    private int fileNameLength;
    private boolean fileNameUTF8Encoded;
    private byte[] generalPurposeFlag;
    private boolean isEncrypted;
    private int lastModFileTime;
    private long offsetStartOfData;
    private char[] password;
    private int signature;
    private long uncompressedSize;
    private int versionNeededToExtract;
    private boolean writeComprSizeInZip64ExtraRecord;
    private net.lingala.zip4j.model.Zip64ExtendedInfo zip64ExtendedInfo;

    public LocalFileHeader()
    {
        this.encryptionMethod = -1;
        this.writeComprSizeInZip64ExtraRecord = 0;
        this.crc32 = 0;
        this.uncompressedSize = 0;
        return;
    }

    public net.lingala.zip4j.model.AESExtraDataRecord getAesExtraDataRecord()
    {
        return this.aesExtraDataRecord;
    }

    public long getCompressedSize()
    {
        return this.compressedSize;
    }

    public int getCompressionMethod()
    {
        return this.compressionMethod;
    }

    public long getCrc32()
    {
        return this.crc32;
    }

    public byte[] getCrcBuff()
    {
        return this.crcBuff;
    }

    public int getEncryptionMethod()
    {
        return this.encryptionMethod;
    }

    public java.util.ArrayList getExtraDataRecords()
    {
        return this.extraDataRecords;
    }

    public byte[] getExtraField()
    {
        return this.extraField;
    }

    public int getExtraFieldLength()
    {
        return this.extraFieldLength;
    }

    public String getFileName()
    {
        return this.fileName;
    }

    public int getFileNameLength()
    {
        return this.fileNameLength;
    }

    public byte[] getGeneralPurposeFlag()
    {
        return this.generalPurposeFlag;
    }

    public int getLastModFileTime()
    {
        return this.lastModFileTime;
    }

    public long getOffsetStartOfData()
    {
        return this.offsetStartOfData;
    }

    public char[] getPassword()
    {
        return this.password;
    }

    public int getSignature()
    {
        return this.signature;
    }

    public long getUncompressedSize()
    {
        return this.uncompressedSize;
    }

    public int getVersionNeededToExtract()
    {
        return this.versionNeededToExtract;
    }

    public net.lingala.zip4j.model.Zip64ExtendedInfo getZip64ExtendedInfo()
    {
        return this.zip64ExtendedInfo;
    }

    public boolean isDataDescriptorExists()
    {
        return this.dataDescriptorExists;
    }

    public boolean isEncrypted()
    {
        return this.isEncrypted;
    }

    public boolean isFileNameUTF8Encoded()
    {
        return this.fileNameUTF8Encoded;
    }

    public boolean isWriteComprSizeInZip64ExtraRecord()
    {
        return this.writeComprSizeInZip64ExtraRecord;
    }

    public void setAesExtraDataRecord(net.lingala.zip4j.model.AESExtraDataRecord p1)
    {
        this.aesExtraDataRecord = p1;
        return;
    }

    public void setCompressedSize(long p1)
    {
        this.compressedSize = p1;
        return;
    }

    public void setCompressionMethod(int p1)
    {
        this.compressionMethod = p1;
        return;
    }

    public void setCrc32(long p1)
    {
        this.crc32 = p1;
        return;
    }

    public void setCrcBuff(byte[] p1)
    {
        this.crcBuff = p1;
        return;
    }

    public void setDataDescriptorExists(boolean p1)
    {
        this.dataDescriptorExists = p1;
        return;
    }

    public void setEncrypted(boolean p1)
    {
        this.isEncrypted = p1;
        return;
    }

    public void setEncryptionMethod(int p1)
    {
        this.encryptionMethod = p1;
        return;
    }

    public void setExtraDataRecords(java.util.ArrayList p1)
    {
        this.extraDataRecords = p1;
        return;
    }

    public void setExtraField(byte[] p1)
    {
        this.extraField = p1;
        return;
    }

    public void setExtraFieldLength(int p1)
    {
        this.extraFieldLength = p1;
        return;
    }

    public void setFileName(String p1)
    {
        this.fileName = p1;
        return;
    }

    public void setFileNameLength(int p1)
    {
        this.fileNameLength = p1;
        return;
    }

    public void setFileNameUTF8Encoded(boolean p1)
    {
        this.fileNameUTF8Encoded = p1;
        return;
    }

    public void setGeneralPurposeFlag(byte[] p1)
    {
        this.generalPurposeFlag = p1;
        return;
    }

    public void setLastModFileTime(int p1)
    {
        this.lastModFileTime = p1;
        return;
    }

    public void setOffsetStartOfData(long p1)
    {
        this.offsetStartOfData = p1;
        return;
    }

    public void setPassword(char[] p1)
    {
        this.password = p1;
        return;
    }

    public void setSignature(int p1)
    {
        this.signature = p1;
        return;
    }

    public void setUncompressedSize(long p1)
    {
        this.uncompressedSize = p1;
        return;
    }

    public void setVersionNeededToExtract(int p1)
    {
        this.versionNeededToExtract = p1;
        return;
    }

    public void setWriteComprSizeInZip64ExtraRecord(boolean p1)
    {
        this.writeComprSizeInZip64ExtraRecord = p1;
        return;
    }

    public void setZip64ExtendedInfo(net.lingala.zip4j.model.Zip64ExtendedInfo p1)
    {
        this.zip64ExtendedInfo = p1;
        return;
    }
}
