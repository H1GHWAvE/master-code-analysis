package net.lingala.zip4j.model;
public class Zip64ExtendedInfo {
    private long compressedSize;
    private int diskNumberStart;
    private int header;
    private long offsetLocalHeader;
    private int size;
    private long unCompressedSize;

    public Zip64ExtendedInfo()
    {
        this.compressedSize = -1;
        this.unCompressedSize = -1;
        this.offsetLocalHeader = -1;
        this.diskNumberStart = -1;
        return;
    }

    public long getCompressedSize()
    {
        return this.compressedSize;
    }

    public int getDiskNumberStart()
    {
        return this.diskNumberStart;
    }

    public int getHeader()
    {
        return this.header;
    }

    public long getOffsetLocalHeader()
    {
        return this.offsetLocalHeader;
    }

    public int getSize()
    {
        return this.size;
    }

    public long getUnCompressedSize()
    {
        return this.unCompressedSize;
    }

    public void setCompressedSize(long p1)
    {
        this.compressedSize = p1;
        return;
    }

    public void setDiskNumberStart(int p1)
    {
        this.diskNumberStart = p1;
        return;
    }

    public void setHeader(int p1)
    {
        this.header = p1;
        return;
    }

    public void setOffsetLocalHeader(long p1)
    {
        this.offsetLocalHeader = p1;
        return;
    }

    public void setSize(int p1)
    {
        this.size = p1;
        return;
    }

    public void setUnCompressedSize(long p1)
    {
        this.unCompressedSize = p1;
        return;
    }
}
