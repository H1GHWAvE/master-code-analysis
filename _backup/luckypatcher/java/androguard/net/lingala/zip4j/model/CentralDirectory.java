package net.lingala.zip4j.model;
public class CentralDirectory {
    private net.lingala.zip4j.model.DigitalSignature digitalSignature;
    private java.util.ArrayList fileHeaders;

    public CentralDirectory()
    {
        return;
    }

    public net.lingala.zip4j.model.DigitalSignature getDigitalSignature()
    {
        return this.digitalSignature;
    }

    public java.util.ArrayList getFileHeaders()
    {
        return this.fileHeaders;
    }

    public void setDigitalSignature(net.lingala.zip4j.model.DigitalSignature p1)
    {
        this.digitalSignature = p1;
        return;
    }

    public void setFileHeaders(java.util.ArrayList p1)
    {
        this.fileHeaders = p1;
        return;
    }
}
