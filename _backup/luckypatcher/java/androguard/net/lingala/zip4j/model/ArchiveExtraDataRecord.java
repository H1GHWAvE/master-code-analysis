package net.lingala.zip4j.model;
public class ArchiveExtraDataRecord {
    private String extraFieldData;
    private int extraFieldLength;
    private int signature;

    public ArchiveExtraDataRecord()
    {
        return;
    }

    public String getExtraFieldData()
    {
        return this.extraFieldData;
    }

    public int getExtraFieldLength()
    {
        return this.extraFieldLength;
    }

    public int getSignature()
    {
        return this.signature;
    }

    public void setExtraFieldData(String p1)
    {
        this.extraFieldData = p1;
        return;
    }

    public void setExtraFieldLength(int p1)
    {
        this.extraFieldLength = p1;
        return;
    }

    public void setSignature(int p1)
    {
        this.signature = p1;
        return;
    }
}
