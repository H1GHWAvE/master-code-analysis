package net.lingala.zip4j.model;
public class ZipModel implements java.lang.Cloneable {
    private net.lingala.zip4j.model.ArchiveExtraDataRecord archiveExtraDataRecord;
    private net.lingala.zip4j.model.CentralDirectory centralDirectory;
    private java.util.List dataDescriptorList;
    private long end;
    private net.lingala.zip4j.model.EndCentralDirRecord endCentralDirRecord;
    private String fileNameCharset;
    private boolean isNestedZipFile;
    private boolean isZip64Format;
    private java.util.List localFileHeaderList;
    private boolean splitArchive;
    private long splitLength;
    private long start;
    private net.lingala.zip4j.model.Zip64EndCentralDirLocator zip64EndCentralDirLocator;
    private net.lingala.zip4j.model.Zip64EndCentralDirRecord zip64EndCentralDirRecord;
    private String zipFile;

    public ZipModel()
    {
        this.splitLength = -1;
        return;
    }

    public Object clone()
    {
        return super.clone();
    }

    public net.lingala.zip4j.model.ArchiveExtraDataRecord getArchiveExtraDataRecord()
    {
        return this.archiveExtraDataRecord;
    }

    public net.lingala.zip4j.model.CentralDirectory getCentralDirectory()
    {
        return this.centralDirectory;
    }

    public java.util.List getDataDescriptorList()
    {
        return this.dataDescriptorList;
    }

    public long getEnd()
    {
        return this.end;
    }

    public net.lingala.zip4j.model.EndCentralDirRecord getEndCentralDirRecord()
    {
        return this.endCentralDirRecord;
    }

    public String getFileNameCharset()
    {
        return this.fileNameCharset;
    }

    public java.util.List getLocalFileHeaderList()
    {
        return this.localFileHeaderList;
    }

    public long getSplitLength()
    {
        return this.splitLength;
    }

    public long getStart()
    {
        return this.start;
    }

    public net.lingala.zip4j.model.Zip64EndCentralDirLocator getZip64EndCentralDirLocator()
    {
        return this.zip64EndCentralDirLocator;
    }

    public net.lingala.zip4j.model.Zip64EndCentralDirRecord getZip64EndCentralDirRecord()
    {
        return this.zip64EndCentralDirRecord;
    }

    public String getZipFile()
    {
        return this.zipFile;
    }

    public boolean isNestedZipFile()
    {
        return this.isNestedZipFile;
    }

    public boolean isSplitArchive()
    {
        return this.splitArchive;
    }

    public boolean isZip64Format()
    {
        return this.isZip64Format;
    }

    public void setArchiveExtraDataRecord(net.lingala.zip4j.model.ArchiveExtraDataRecord p1)
    {
        this.archiveExtraDataRecord = p1;
        return;
    }

    public void setCentralDirectory(net.lingala.zip4j.model.CentralDirectory p1)
    {
        this.centralDirectory = p1;
        return;
    }

    public void setDataDescriptorList(java.util.List p1)
    {
        this.dataDescriptorList = p1;
        return;
    }

    public void setEnd(long p1)
    {
        this.end = p1;
        return;
    }

    public void setEndCentralDirRecord(net.lingala.zip4j.model.EndCentralDirRecord p1)
    {
        this.endCentralDirRecord = p1;
        return;
    }

    public void setFileNameCharset(String p1)
    {
        this.fileNameCharset = p1;
        return;
    }

    public void setLocalFileHeaderList(java.util.List p1)
    {
        this.localFileHeaderList = p1;
        return;
    }

    public void setNestedZipFile(boolean p1)
    {
        this.isNestedZipFile = p1;
        return;
    }

    public void setSplitArchive(boolean p1)
    {
        this.splitArchive = p1;
        return;
    }

    public void setSplitLength(long p1)
    {
        this.splitLength = p1;
        return;
    }

    public void setStart(long p1)
    {
        this.start = p1;
        return;
    }

    public void setZip64EndCentralDirLocator(net.lingala.zip4j.model.Zip64EndCentralDirLocator p1)
    {
        this.zip64EndCentralDirLocator = p1;
        return;
    }

    public void setZip64EndCentralDirRecord(net.lingala.zip4j.model.Zip64EndCentralDirRecord p1)
    {
        this.zip64EndCentralDirRecord = p1;
        return;
    }

    public void setZip64Format(boolean p1)
    {
        this.isZip64Format = p1;
        return;
    }

    public void setZipFile(String p1)
    {
        this.zipFile = p1;
        return;
    }
}
