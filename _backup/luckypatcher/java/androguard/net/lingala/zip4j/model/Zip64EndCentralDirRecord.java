package net.lingala.zip4j.model;
public class Zip64EndCentralDirRecord {
    private byte[] extensibleDataSector;
    private int noOfThisDisk;
    private int noOfThisDiskStartOfCentralDir;
    private long offsetStartCenDirWRTStartDiskNo;
    private long signature;
    private long sizeOfCentralDir;
    private long sizeOfZip64EndCentralDirRec;
    private long totNoOfEntriesInCentralDir;
    private long totNoOfEntriesInCentralDirOnThisDisk;
    private int versionMadeBy;
    private int versionNeededToExtract;

    public Zip64EndCentralDirRecord()
    {
        return;
    }

    public byte[] getExtensibleDataSector()
    {
        return this.extensibleDataSector;
    }

    public int getNoOfThisDisk()
    {
        return this.noOfThisDisk;
    }

    public int getNoOfThisDiskStartOfCentralDir()
    {
        return this.noOfThisDiskStartOfCentralDir;
    }

    public long getOffsetStartCenDirWRTStartDiskNo()
    {
        return this.offsetStartCenDirWRTStartDiskNo;
    }

    public long getSignature()
    {
        return this.signature;
    }

    public long getSizeOfCentralDir()
    {
        return this.sizeOfCentralDir;
    }

    public long getSizeOfZip64EndCentralDirRec()
    {
        return this.sizeOfZip64EndCentralDirRec;
    }

    public long getTotNoOfEntriesInCentralDir()
    {
        return this.totNoOfEntriesInCentralDir;
    }

    public long getTotNoOfEntriesInCentralDirOnThisDisk()
    {
        return this.totNoOfEntriesInCentralDirOnThisDisk;
    }

    public int getVersionMadeBy()
    {
        return this.versionMadeBy;
    }

    public int getVersionNeededToExtract()
    {
        return this.versionNeededToExtract;
    }

    public void setExtensibleDataSector(byte[] p1)
    {
        this.extensibleDataSector = p1;
        return;
    }

    public void setNoOfThisDisk(int p1)
    {
        this.noOfThisDisk = p1;
        return;
    }

    public void setNoOfThisDiskStartOfCentralDir(int p1)
    {
        this.noOfThisDiskStartOfCentralDir = p1;
        return;
    }

    public void setOffsetStartCenDirWRTStartDiskNo(long p1)
    {
        this.offsetStartCenDirWRTStartDiskNo = p1;
        return;
    }

    public void setSignature(long p1)
    {
        this.signature = p1;
        return;
    }

    public void setSizeOfCentralDir(long p1)
    {
        this.sizeOfCentralDir = p1;
        return;
    }

    public void setSizeOfZip64EndCentralDirRec(long p1)
    {
        this.sizeOfZip64EndCentralDirRec = p1;
        return;
    }

    public void setTotNoOfEntriesInCentralDir(long p1)
    {
        this.totNoOfEntriesInCentralDir = p1;
        return;
    }

    public void setTotNoOfEntriesInCentralDirOnThisDisk(long p1)
    {
        this.totNoOfEntriesInCentralDirOnThisDisk = p1;
        return;
    }

    public void setVersionMadeBy(int p1)
    {
        this.versionMadeBy = p1;
        return;
    }

    public void setVersionNeededToExtract(int p1)
    {
        this.versionNeededToExtract = p1;
        return;
    }
}
