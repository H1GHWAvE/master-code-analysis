package net.lingala.zip4j.model;
public class ExtraDataRecord {
    private byte[] data;
    private long header;
    private int sizeOfData;

    public ExtraDataRecord()
    {
        return;
    }

    public byte[] getData()
    {
        return this.data;
    }

    public long getHeader()
    {
        return this.header;
    }

    public int getSizeOfData()
    {
        return this.sizeOfData;
    }

    public void setData(byte[] p1)
    {
        this.data = p1;
        return;
    }

    public void setHeader(long p1)
    {
        this.header = p1;
        return;
    }

    public void setSizeOfData(int p1)
    {
        this.sizeOfData = p1;
        return;
    }
}
