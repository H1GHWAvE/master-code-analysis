package net.lingala.zip4j.model;
public class UnzipParameters {
    private boolean ignoreAllFileAttributes;
    private boolean ignoreArchiveFileAttribute;
    private boolean ignoreDateTimeAttributes;
    private boolean ignoreHiddenFileAttribute;
    private boolean ignoreReadOnlyFileAttribute;
    private boolean ignoreSystemFileAttribute;

    public UnzipParameters()
    {
        return;
    }

    public boolean isIgnoreAllFileAttributes()
    {
        return this.ignoreAllFileAttributes;
    }

    public boolean isIgnoreArchiveFileAttribute()
    {
        return this.ignoreArchiveFileAttribute;
    }

    public boolean isIgnoreDateTimeAttributes()
    {
        return this.ignoreDateTimeAttributes;
    }

    public boolean isIgnoreHiddenFileAttribute()
    {
        return this.ignoreHiddenFileAttribute;
    }

    public boolean isIgnoreReadOnlyFileAttribute()
    {
        return this.ignoreReadOnlyFileAttribute;
    }

    public boolean isIgnoreSystemFileAttribute()
    {
        return this.ignoreSystemFileAttribute;
    }

    public void setIgnoreAllFileAttributes(boolean p1)
    {
        this.ignoreAllFileAttributes = p1;
        return;
    }

    public void setIgnoreArchiveFileAttribute(boolean p1)
    {
        this.ignoreArchiveFileAttribute = p1;
        return;
    }

    public void setIgnoreDateTimeAttributes(boolean p1)
    {
        this.ignoreDateTimeAttributes = p1;
        return;
    }

    public void setIgnoreHiddenFileAttribute(boolean p1)
    {
        this.ignoreHiddenFileAttribute = p1;
        return;
    }

    public void setIgnoreReadOnlyFileAttribute(boolean p1)
    {
        this.ignoreReadOnlyFileAttribute = p1;
        return;
    }

    public void setIgnoreSystemFileAttribute(boolean p1)
    {
        this.ignoreSystemFileAttribute = p1;
        return;
    }
}
