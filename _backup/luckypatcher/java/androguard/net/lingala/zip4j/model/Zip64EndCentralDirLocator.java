package net.lingala.zip4j.model;
public class Zip64EndCentralDirLocator {
    private int noOfDiskStartOfZip64EndOfCentralDirRec;
    private long offsetZip64EndOfCentralDirRec;
    private long signature;
    private int totNumberOfDiscs;

    public Zip64EndCentralDirLocator()
    {
        return;
    }

    public int getNoOfDiskStartOfZip64EndOfCentralDirRec()
    {
        return this.noOfDiskStartOfZip64EndOfCentralDirRec;
    }

    public long getOffsetZip64EndOfCentralDirRec()
    {
        return this.offsetZip64EndOfCentralDirRec;
    }

    public long getSignature()
    {
        return this.signature;
    }

    public int getTotNumberOfDiscs()
    {
        return this.totNumberOfDiscs;
    }

    public void setNoOfDiskStartOfZip64EndOfCentralDirRec(int p1)
    {
        this.noOfDiskStartOfZip64EndOfCentralDirRec = p1;
        return;
    }

    public void setOffsetZip64EndOfCentralDirRec(long p1)
    {
        this.offsetZip64EndOfCentralDirRec = p1;
        return;
    }

    public void setSignature(long p1)
    {
        this.signature = p1;
        return;
    }

    public void setTotNumberOfDiscs(int p1)
    {
        this.totNumberOfDiscs = p1;
        return;
    }
}
