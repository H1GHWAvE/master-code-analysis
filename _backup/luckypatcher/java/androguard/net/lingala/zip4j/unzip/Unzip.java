package net.lingala.zip4j.unzip;
public class Unzip {
    private net.lingala.zip4j.model.ZipModel zipModel;

    public Unzip(net.lingala.zip4j.model.ZipModel p3)
    {
        if (p3 != null) {
            this.zipModel = p3;
            return;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("ZipModel is null");
        }
    }

    static synthetic void access$000(net.lingala.zip4j.unzip.Unzip p0, java.util.ArrayList p1, net.lingala.zip4j.model.UnzipParameters p2, net.lingala.zip4j.progress.ProgressMonitor p3, String p4)
    {
        p0.initExtractAll(p1, p2, p3, p4);
        return;
    }

    static synthetic void access$100(net.lingala.zip4j.unzip.Unzip p0, net.lingala.zip4j.model.FileHeader p1, String p2, net.lingala.zip4j.model.UnzipParameters p3, String p4, net.lingala.zip4j.progress.ProgressMonitor p5)
    {
        p0.initExtractFile(p1, p2, p3, p4, p5);
        return;
    }

    private long calculateTotalWork(java.util.ArrayList p9)
    {
        if (p9 != null) {
            long v2 = 0;
            int v1 = 0;
            while (v1 < p9.size()) {
                net.lingala.zip4j.model.FileHeader v0_1 = ((net.lingala.zip4j.model.FileHeader) p9.get(v1));
                if ((v0_1.getZip64ExtendedInfo() == null) || (v0_1.getZip64ExtendedInfo().getUnCompressedSize() <= 0)) {
                    v2 += v0_1.getCompressedSize();
                } else {
                    v2 += v0_1.getZip64ExtendedInfo().getCompressedSize();
                }
                v1++;
            }
            return v2;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("fileHeaders is null, cannot calculate total work");
        }
    }

    private void checkOutputDirectoryStructure(net.lingala.zip4j.model.FileHeader p9, String p10, String p11)
    {
        if ((p9 != null) && (net.lingala.zip4j.util.Zip4jUtil.isStringNotNullAndNotEmpty(p10))) {
            String v3 = p9.getFileName();
            if (net.lingala.zip4j.util.Zip4jUtil.isStringNotNullAndNotEmpty(p11)) {
                v3 = p11;
            }
            if (net.lingala.zip4j.util.Zip4jUtil.isStringNotNullAndNotEmpty(v3)) {
                try {
                    java.io.File v5_1 = new java.io.File(new java.io.File(new StringBuilder().append(p10).append(v3).toString()).getParent());
                } catch (Exception v1) {
                    throw new net.lingala.zip4j.exception.ZipException(v1);
                }
                if (!v5_1.exists()) {
                    v5_1.mkdirs();
                }
            }
            return;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("Cannot check output directory structure...one of the parameters was null");
        }
    }

    private void initExtractAll(java.util.ArrayList p8, net.lingala.zip4j.model.UnzipParameters p9, net.lingala.zip4j.progress.ProgressMonitor p10, String p11)
    {
        int v6 = 0;
        while (v6 < p8.size()) {
            this.initExtractFile(((net.lingala.zip4j.model.FileHeader) p8.get(v6)), p11, p9, 0, p10);
            if (!p10.isCancelAllTasks()) {
                v6++;
            } else {
                p10.setResult(3);
                p10.setState(0);
                break;
            }
        }
        return;
    }

    private void initExtractFile(net.lingala.zip4j.model.FileHeader p8, String p9, net.lingala.zip4j.model.UnzipParameters p10, String p11, net.lingala.zip4j.progress.ProgressMonitor p12)
    {
        if (p8 != null) {
            try {
                p12.setFileName(p8.getFileName());
            } catch (Exception v1_3) {
                p12.endProgressMonitorError(v1_3);
                throw v1_3;
            } catch (Exception v1_2) {
                p12.endProgressMonitorError(v1_2);
                throw new net.lingala.zip4j.exception.ZipException(v1_2);
            }
            if (!p9.endsWith(net.lingala.zip4j.util.InternalZipConstants.FILE_SEPARATOR)) {
                p9 = new StringBuilder().append(p9).append(net.lingala.zip4j.util.InternalZipConstants.FILE_SEPARATOR).toString();
            }
            try {
                if (!p8.isDirectory()) {
                    this.checkOutputDirectoryStructure(p8, p9, p11);
                    try {
                        new net.lingala.zip4j.unzip.UnzipEngine(this.zipModel, p8).unzipFile(p12, p9, p11, p10);
                    } catch (Exception v1_0) {
                        p12.endProgressMonitorError(v1_0);
                        throw new net.lingala.zip4j.exception.ZipException(v1_0);
                    }
                } else {
                    String v3 = p8.getFileName();
                    if (net.lingala.zip4j.util.Zip4jUtil.isStringNotNullAndNotEmpty(v3)) {
                        java.io.File v2_1 = new java.io.File(new StringBuilder().append(p9).append(v3).toString());
                        if (!v2_1.exists()) {
                            v2_1.mkdirs();
                        }
                    }
                }
            } catch (Exception v1_1) {
                p12.endProgressMonitorError(v1_1);
                throw new net.lingala.zip4j.exception.ZipException(v1_1);
            }
            return;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("fileHeader is null");
        }
    }

    public void extractAll(net.lingala.zip4j.model.UnzipParameters p9, String p10, net.lingala.zip4j.progress.ProgressMonitor p11, boolean p12)
    {
        net.lingala.zip4j.model.CentralDirectory v7 = this.zipModel.getCentralDirectory();
        if ((v7 != null) && (v7.getFileHeaders() != null)) {
            java.util.ArrayList v3 = v7.getFileHeaders();
            p11.setCurrentOperation(1);
            p11.setTotalWork(this.calculateTotalWork(v3));
            p11.setState(1);
            if (!p12) {
                this.initExtractAll(v3, p9, p11, p10);
            } else {
                new net.lingala.zip4j.unzip.Unzip$1(this, "Zip4j", v3, p9, p11, p10).start();
            }
            return;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("invalid central directory in zipModel");
        }
    }

    public void extractFile(net.lingala.zip4j.model.FileHeader p9, String p10, net.lingala.zip4j.model.UnzipParameters p11, String p12, net.lingala.zip4j.progress.ProgressMonitor p13, boolean p14)
    {
        if (p9 != null) {
            p13.setCurrentOperation(1);
            p13.setTotalWork(p9.getCompressedSize());
            p13.setState(1);
            p13.setPercentDone(0);
            p13.setFileName(p9.getFileName());
            if (!p14) {
                this.initExtractFile(p9, p10, p11, p12, p13);
                p13.endProgressMonitorSuccess();
            } else {
                new net.lingala.zip4j.unzip.Unzip$2(this, "Zip4j", p9, p10, p11, p12, p13).start();
            }
            return;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("fileHeader is null");
        }
    }

    public net.lingala.zip4j.io.ZipInputStream getInputStream(net.lingala.zip4j.model.FileHeader p3)
    {
        return new net.lingala.zip4j.unzip.UnzipEngine(this.zipModel, p3).getInputStream();
    }
}
