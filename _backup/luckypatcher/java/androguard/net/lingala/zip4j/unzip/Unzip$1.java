package net.lingala.zip4j.unzip;
 class Unzip$1 extends java.lang.Thread {
    final synthetic net.lingala.zip4j.unzip.Unzip this$0;
    final synthetic java.util.ArrayList val$fileHeaders;
    final synthetic String val$outPath;
    final synthetic net.lingala.zip4j.progress.ProgressMonitor val$progressMonitor;
    final synthetic net.lingala.zip4j.model.UnzipParameters val$unzipParameters;

    Unzip$1(net.lingala.zip4j.unzip.Unzip p1, String p2, java.util.ArrayList p3, net.lingala.zip4j.model.UnzipParameters p4, net.lingala.zip4j.progress.ProgressMonitor p5, String p6)
    {
        this.this$0 = p1;
        this.val$fileHeaders = p3;
        this.val$unzipParameters = p4;
        this.val$progressMonitor = p5;
        this.val$outPath = p6;
        this(p2);
        return;
    }

    public void run()
    {
        try {
            net.lingala.zip4j.unzip.Unzip.access$000(this.this$0, this.val$fileHeaders, this.val$unzipParameters, this.val$progressMonitor, this.val$outPath);
            this.val$progressMonitor.endProgressMonitorSuccess();
        } catch (net.lingala.zip4j.exception.ZipException v0) {
        }
        return;
    }
}
