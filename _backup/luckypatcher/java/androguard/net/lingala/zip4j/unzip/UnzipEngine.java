package net.lingala.zip4j.unzip;
public class UnzipEngine {
    private java.util.zip.CRC32 crc;
    private int currSplitFileCounter;
    private net.lingala.zip4j.crypto.IDecrypter decrypter;
    private net.lingala.zip4j.model.FileHeader fileHeader;
    private net.lingala.zip4j.model.LocalFileHeader localFileHeader;
    private net.lingala.zip4j.model.ZipModel zipModel;

    public UnzipEngine(net.lingala.zip4j.model.ZipModel p3, net.lingala.zip4j.model.FileHeader p4)
    {
        this.currSplitFileCounter = 0;
        if ((p3 != null) && (p4 != null)) {
            this.zipModel = p3;
            this.fileHeader = p4;
            this.crc = new java.util.zip.CRC32();
            return;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("Invalid parameters passed to StoreUnzip. One or more of the parameters were null");
        }
    }

    private int calculateAESSaltLength(net.lingala.zip4j.model.AESExtraDataRecord p3)
    {
        if (p3 != null) {
            int v0_1;
            switch (p3.getAesStrength()) {
                case 1:
                    v0_1 = 8;
                    break;
                case 2:
                    v0_1 = 12;
                    break;
                case 3:
                    v0_1 = 16;
                    break;
                default:
                    throw new net.lingala.zip4j.exception.ZipException("unable to determine salt length: invalid aes key strength");
            }
            return v0_1;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("unable to determine salt length: AESExtraDataRecord is null");
        }
    }

    private boolean checkLocalHeader()
    {
        try {
            java.io.RandomAccessFile v2 = this.checkSplitFile();
        } catch (int v4_12) {
            if (v2 != null) {
                try {
                    v2.close();
                } catch (Exception v5) {
                } catch (Exception v5) {
                }
            }
            throw v4_12;
        } catch (java.io.FileNotFoundException v0) {
            throw new net.lingala.zip4j.exception.ZipException(v0);
        }
        if (v2 == null) {
            v2 = new java.io.RandomAccessFile(new java.io.File(this.zipModel.getZipFile()), "r");
        }
        this.localFileHeader = new net.lingala.zip4j.core.HeaderReader(v2).readLocalFileHeader(this.fileHeader);
        if (this.localFileHeader != null) {
            int v4_7;
            if (this.localFileHeader.getCompressionMethod() == this.fileHeader.getCompressionMethod()) {
                v4_7 = 1;
                if (v2 != null) {
                    try {
                        v2.close();
                    } catch (Exception v5) {
                    } catch (Exception v5) {
                    }
                }
            } else {
                v4_7 = 0;
                if (v2 != null) {
                    try {
                        v2.close();
                    } catch (Exception v5) {
                    } catch (Exception v5) {
                    }
                }
            }
            return v4_7;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("error reading local file header. Is this a valid zip file?");
        }
    }

    private java.io.RandomAccessFile checkSplitFile()
    {
        java.io.RandomAccessFile v4_0;
        if (!this.zipModel.isSplitArchive()) {
            v4_0 = 0;
        } else {
            String v3;
            int v1 = this.fileHeader.getDiskNumberStart();
            this.currSplitFileCounter = (v1 + 1);
            String v0 = this.zipModel.getZipFile();
            if (v1 != this.zipModel.getEndCentralDirRecord().getNoOfThisDisk()) {
                if (v1 < 9) {
                    v3 = new StringBuilder().append(v0.substring(0, v0.lastIndexOf("."))).append(".z0").append((v1 + 1)).toString();
                } else {
                    v3 = new StringBuilder().append(v0.substring(0, v0.lastIndexOf("."))).append(".z").append((v1 + 1)).toString();
                }
            } else {
                v3 = this.zipModel.getZipFile();
            }
            try {
                v4_0 = new java.io.RandomAccessFile(v3, "r");
            } catch (java.io.IOException v2_0) {
                throw new net.lingala.zip4j.exception.ZipException(v2_0);
            } catch (java.io.IOException v2_1) {
                throw new net.lingala.zip4j.exception.ZipException(v2_1);
            }
            if (this.currSplitFileCounter == 1) {
                byte[] v5 = new byte[4];
                v4_0.read(v5);
                if (((long) net.lingala.zip4j.util.Raw.readIntLittleEndian(v5, 0)) != 134695760) {
                    throw new net.lingala.zip4j.exception.ZipException("invalid first part split file signature");
                }
            }
        }
        return v4_0;
    }

    private void closeStreams(java.io.InputStream p4, java.io.OutputStream p5)
    {
        if (p4 == 0) {
            if (p5 != 0) {
                try {
                    p5.close();
                } catch (java.io.IOException v1) {
                }
            }
        } else {
            try {
                p4.close();
            } catch (java.io.IOException v1_6) {
                if (p5 != 0) {
                    try {
                        p5.close();
                    } catch (String v2) {
                    }
                }
                throw v1_6;
            } catch (java.io.IOException v0) {
                if (v0 != null) {
                    if (net.lingala.zip4j.util.Zip4jUtil.isStringNotNullAndNotEmpty(v0.getMessage())) {
                        if (v0.getMessage().indexOf(" - Wrong Password?") >= 0) {
                            throw new net.lingala.zip4j.exception.ZipException(v0.getMessage());
                        }
                    }
                }
                if (p5 != 0) {
                    try {
                        p5.close();
                    } catch (java.io.IOException v1) {
                    }
                }
            }
        }
        return;
    }

    private java.io.RandomAccessFile createFileHandler(String p5)
    {
        if ((this.zipModel != null) && (net.lingala.zip4j.util.Zip4jUtil.isStringNotNullAndNotEmpty(this.zipModel.getZipFile()))) {
            try {
                java.io.RandomAccessFile v1_1;
                if (!this.zipModel.isSplitArchive()) {
                    v1_1 = new java.io.RandomAccessFile(new java.io.File(this.zipModel.getZipFile()), p5);
                } else {
                    v1_1 = this.checkSplitFile();
                }
            } catch (Exception v0_1) {
                throw new net.lingala.zip4j.exception.ZipException(v0_1);
            } catch (Exception v0_0) {
                throw new net.lingala.zip4j.exception.ZipException(v0_0);
            }
            return v1_1;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("input parameter is null in getFilePointer");
        }
    }

    private byte[] getAESPasswordVerifier(java.io.RandomAccessFile p4)
    {
        try {
            byte[] v1 = new byte[2];
            p4.read(v1);
            return v1;
        } catch (java.io.IOException v0) {
            throw new net.lingala.zip4j.exception.ZipException(v0);
        }
    }

    private byte[] getAESSalt(java.io.RandomAccessFile p6)
    {
        byte[] v2;
        if (this.localFileHeader.getAesExtraDataRecord() != null) {
            try {
                v2 = new byte[this.calculateAESSaltLength(this.localFileHeader.getAesExtraDataRecord())];
                p6.seek(this.localFileHeader.getOffsetStartOfData());
                p6.read(v2);
            } catch (java.io.IOException v1) {
                throw new net.lingala.zip4j.exception.ZipException(v1);
            }
        } else {
            v2 = 0;
        }
        return v2;
    }

    private String getOutputFileNameWithPath(String p4, String p5)
    {
        String v0;
        if (!net.lingala.zip4j.util.Zip4jUtil.isStringNotNullAndNotEmpty(p5)) {
            v0 = this.fileHeader.getFileName();
        } else {
            v0 = p5;
        }
        return new StringBuilder().append(p4).append(System.getProperty("file.separator")).append(v0).toString();
    }

    private java.io.FileOutputStream getOutputStream(String p6, String p7)
    {
        if (net.lingala.zip4j.util.Zip4jUtil.isStringNotNullAndNotEmpty(p6)) {
            try {
                java.io.File v1_1 = new java.io.File(this.getOutputFileNameWithPath(p6, p7));
            } catch (java.io.FileNotFoundException v0) {
                throw new net.lingala.zip4j.exception.ZipException(v0);
            }
            if (!v1_1.getParentFile().exists()) {
                v1_1.getParentFile().mkdirs();
            }
            if (v1_1.exists()) {
                v1_1.delete();
            }
            return new java.io.FileOutputStream(v1_1);
        } else {
            throw new net.lingala.zip4j.exception.ZipException("invalid output path");
        }
    }

    private byte[] getStandardDecrypterHeaderBytes(java.io.RandomAccessFile p5)
    {
        try {
            byte[] v1 = new byte[12];
            p5.seek(this.localFileHeader.getOffsetStartOfData());
            p5.read(v1, 0, 12);
            return v1;
        } catch (Exception v0_1) {
            throw new net.lingala.zip4j.exception.ZipException(v0_1);
        } catch (Exception v0_0) {
            throw new net.lingala.zip4j.exception.ZipException(v0_0);
        }
    }

    private void init(java.io.RandomAccessFile p4)
    {
        if (this.localFileHeader != null) {
            try {
                this.initDecrypter(p4);
                return;
            } catch (Exception v0_1) {
                throw v0_1;
            } catch (Exception v0_0) {
                throw new net.lingala.zip4j.exception.ZipException(v0_0);
            }
        } else {
            throw new net.lingala.zip4j.exception.ZipException("local file header is null, cannot initialize input stream");
        }
    }

    private void initDecrypter(java.io.RandomAccessFile p5)
    {
        if (this.localFileHeader != null) {
            if (this.localFileHeader.isEncrypted()) {
                if (this.localFileHeader.getEncryptionMethod() != 0) {
                    if (this.localFileHeader.getEncryptionMethod() != 99) {
                        throw new net.lingala.zip4j.exception.ZipException("unsupported encryption method");
                    } else {
                        this.decrypter = new net.lingala.zip4j.crypto.AESDecrypter(this.localFileHeader, this.getAESSalt(p5), this.getAESPasswordVerifier(p5));
                    }
                } else {
                    this.decrypter = new net.lingala.zip4j.crypto.StandardDecrypter(this.fileHeader, this.getStandardDecrypterHeaderBytes(p5));
                }
            }
            return;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("local file header is null, cannot init decrypter");
        }
    }

    public void checkCRC()
    {
        if (this.fileHeader != null) {
            if (this.fileHeader.getEncryptionMethod() != 99) {
                if ((this.crc.getValue() & 2.1219957905e-314) != this.fileHeader.getCrc32()) {
                    String v3 = new StringBuilder().append("invalid CRC for file: ").append(this.fileHeader.getFileName()).toString();
                    if ((this.localFileHeader.isEncrypted()) && (this.localFileHeader.getEncryptionMethod() == 0)) {
                        v3 = new StringBuilder().append(v3).append(" - Wrong Password?").toString();
                    }
                    throw new net.lingala.zip4j.exception.ZipException(v3);
                }
            } else {
                if ((this.decrypter != null) && ((this.decrypter instanceof net.lingala.zip4j.crypto.AESDecrypter))) {
                    byte[] v5 = ((net.lingala.zip4j.crypto.AESDecrypter) this.decrypter).getCalculatedAuthenticationBytes();
                    byte[] v4 = ((net.lingala.zip4j.crypto.AESDecrypter) this.decrypter).getStoredMac();
                    byte[] v2 = new byte[10];
                    if ((v2 != null) && (v4 != null)) {
                        System.arraycopy(v5, 0, v2, 0, 10);
                        if (!java.util.Arrays.equals(v2, v4)) {
                            throw new net.lingala.zip4j.exception.ZipException(new StringBuilder().append("invalid CRC (MAC) for file: ").append(this.fileHeader.getFileName()).toString());
                        }
                    } else {
                        throw new net.lingala.zip4j.exception.ZipException(new StringBuilder().append("CRC (MAC) check failed for ").append(this.fileHeader.getFileName()).toString());
                    }
                }
            }
        }
        return;
    }

    public net.lingala.zip4j.crypto.IDecrypter getDecrypter()
    {
        return this.decrypter;
    }

    public net.lingala.zip4j.model.FileHeader getFileHeader()
    {
        return this.fileHeader;
    }

    public net.lingala.zip4j.io.ZipInputStream getInputStream()
    {
        if (this.fileHeader != null) {
            try {
                java.io.RandomAccessFile v1 = this.createFileHandler("r");
            } catch (Exception v8_1) {
                if (v1 != null) {
                    try {
                        v1.close();
                    } catch (net.lingala.zip4j.io.ZipInputStream v0) {
                    }
                }
                throw v8_1;
            } catch (Exception v8_0) {
                if (v1 != null) {
                    try {
                        v1.close();
                    } catch (net.lingala.zip4j.io.ZipInputStream v0) {
                    }
                }
                throw new net.lingala.zip4j.exception.ZipException(v8_0);
            }
            if (this.checkLocalHeader()) {
                this.init(v1);
                long v4 = this.localFileHeader.getCompressedSize();
                long v2 = this.localFileHeader.getOffsetStartOfData();
                if (this.localFileHeader.isEncrypted()) {
                    if (this.localFileHeader.getEncryptionMethod() != 99) {
                        if (this.localFileHeader.getEncryptionMethod() == 0) {
                            v4 -= 12;
                            v2 += 12;
                        }
                    } else {
                        if (!(this.decrypter instanceof net.lingala.zip4j.crypto.AESDecrypter)) {
                            throw new net.lingala.zip4j.exception.ZipException(new StringBuilder().append("invalid decryptor when trying to calculate compressed size for AES encrypted file: ").append(this.fileHeader.getFileName()).toString());
                        } else {
                            v4 -= ((long) ((((net.lingala.zip4j.crypto.AESDecrypter) this.decrypter).getPasswordVerifierLength() + ((net.lingala.zip4j.crypto.AESDecrypter) this.decrypter).getSaltLength()) + 10));
                            v2 += ((long) (((net.lingala.zip4j.crypto.AESDecrypter) this.decrypter).getPasswordVerifierLength() + ((net.lingala.zip4j.crypto.AESDecrypter) this.decrypter).getSaltLength()));
                        }
                    }
                }
                int v7 = this.fileHeader.getCompressionMethod();
                if (this.fileHeader.getEncryptionMethod() == 99) {
                    if (this.fileHeader.getAesExtraDataRecord() == null) {
                        throw new net.lingala.zip4j.exception.ZipException(new StringBuilder().append("AESExtraDataRecord does not exist for AES encrypted file: ").append(this.fileHeader.getFileName()).toString());
                    } else {
                        v7 = this.fileHeader.getAesExtraDataRecord().getCompressionMethod();
                    }
                }
                net.lingala.zip4j.io.ZipInputStream v0_39;
                v1.seek(v2);
                switch (v7) {
                    case 0:
                        v0_39 = new net.lingala.zip4j.io.ZipInputStream(new net.lingala.zip4j.io.PartInputStream(v1, v2, v4, this));
                        break;
                    case 8:
                        v0_39 = new net.lingala.zip4j.io.ZipInputStream(new net.lingala.zip4j.io.InflaterInputStream(v1, v2, v4, this));
                        break;
                    default:
                        throw new net.lingala.zip4j.exception.ZipException("compression type not supported");
                }
                return v0_39;
            } else {
                throw new net.lingala.zip4j.exception.ZipException("local header and file header do not match");
            }
        } else {
            throw new net.lingala.zip4j.exception.ZipException("file header is null, cannot get inputstream");
        }
    }

    public net.lingala.zip4j.model.LocalFileHeader getLocalFileHeader()
    {
        return this.localFileHeader;
    }

    public net.lingala.zip4j.model.ZipModel getZipModel()
    {
        return this.zipModel;
    }

    public java.io.RandomAccessFile startNextSplitFile()
    {
        String v2;
        String v0 = this.zipModel.getZipFile();
        if (this.currSplitFileCounter != this.zipModel.getEndCentralDirRecord().getNoOfThisDisk()) {
            if (this.currSplitFileCounter < 9) {
                v2 = new StringBuilder().append(v0.substring(0, v0.lastIndexOf("."))).append(".z0").append((this.currSplitFileCounter + 1)).toString();
            } else {
                v2 = new StringBuilder().append(v0.substring(0, v0.lastIndexOf("."))).append(".z").append((this.currSplitFileCounter + 1)).toString();
            }
        } else {
            v2 = this.zipModel.getZipFile();
        }
        this.currSplitFileCounter = (this.currSplitFileCounter + 1);
        try {
            if (net.lingala.zip4j.util.Zip4jUtil.checkFileExists(v2)) {
                return new java.io.RandomAccessFile(v2, "r");
            } else {
                throw new java.io.IOException(new StringBuilder().append("zip split file does not exist: ").append(v2).toString());
            }
        } catch (net.lingala.zip4j.exception.ZipException v1) {
            throw new java.io.IOException(v1.getMessage());
        }
    }

    public void unzipFile(net.lingala.zip4j.progress.ProgressMonitor p9, String p10, String p11, net.lingala.zip4j.model.UnzipParameters p12)
    {
        if ((this.zipModel != null) && ((this.fileHeader != null) && (net.lingala.zip4j.util.Zip4jUtil.isStringNotNullAndNotEmpty(p10)))) {
            try {
                byte[] v0 = new byte[4096];
                net.lingala.zip4j.io.ZipInputStream v2 = this.getInputStream();
                java.io.FileOutputStream v3 = this.getOutputStream(p10, p11);
            } catch (Exception v1_1) {
                throw new net.lingala.zip4j.exception.ZipException(v1_1);
            } catch (int v5_13) {
                this.closeStreams(v2, v3);
                throw v5_13;
            } catch (Exception v1_0) {
                throw new net.lingala.zip4j.exception.ZipException(v1_0);
            }
            do {
                int v4 = v2.read(v0);
                if (v4 == -1) {
                    this.closeStreams(v2, v3);
                    net.lingala.zip4j.unzip.UnzipUtil.applyFileAttributes(this.fileHeader, new java.io.File(this.getOutputFileNameWithPath(p10, p11)), p12);
                    this.closeStreams(v2, v3);
                } else {
                    v3.write(v0, 0, v4);
                    p9.updateWorkCompleted(((long) v4));
                }
                return;
            } while(!p9.isCancelAllTasks());
            p9.setResult(3);
            p9.setState(0);
            this.closeStreams(v2, v3);
            return;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("Invalid parameters passed during unzipping file. One or more of the parameters were null");
        }
    }

    public void updateCRC(int p2)
    {
        this.crc.update(p2);
        return;
    }

    public void updateCRC(byte[] p2, int p3, int p4)
    {
        if (p2 != null) {
            this.crc.update(p2, p3, p4);
        }
        return;
    }
}
