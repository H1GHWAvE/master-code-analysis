package net.lingala.zip4j.unzip;
 class Unzip$2 extends java.lang.Thread {
    final synthetic net.lingala.zip4j.unzip.Unzip this$0;
    final synthetic net.lingala.zip4j.model.FileHeader val$fileHeader;
    final synthetic String val$newFileName;
    final synthetic String val$outPath;
    final synthetic net.lingala.zip4j.progress.ProgressMonitor val$progressMonitor;
    final synthetic net.lingala.zip4j.model.UnzipParameters val$unzipParameters;

    Unzip$2(net.lingala.zip4j.unzip.Unzip p1, String p2, net.lingala.zip4j.model.FileHeader p3, String p4, net.lingala.zip4j.model.UnzipParameters p5, String p6, net.lingala.zip4j.progress.ProgressMonitor p7)
    {
        this.this$0 = p1;
        this.val$fileHeader = p3;
        this.val$outPath = p4;
        this.val$unzipParameters = p5;
        this.val$newFileName = p6;
        this.val$progressMonitor = p7;
        this(p2);
        return;
    }

    public void run()
    {
        try {
            net.lingala.zip4j.unzip.Unzip.access$100(this.this$0, this.val$fileHeader, this.val$outPath, this.val$unzipParameters, this.val$newFileName, this.val$progressMonitor);
            this.val$progressMonitor.endProgressMonitorSuccess();
        } catch (net.lingala.zip4j.exception.ZipException v0) {
        }
        return;
    }
}
