package net.lingala.zip4j.unzip;
public class UnzipUtil {

    public UnzipUtil()
    {
        return;
    }

    public static void applyFileAttributes(net.lingala.zip4j.model.FileHeader p1, java.io.File p2)
    {
        net.lingala.zip4j.unzip.UnzipUtil.applyFileAttributes(p1, p2, 0);
        return;
    }

    public static void applyFileAttributes(net.lingala.zip4j.model.FileHeader p8, java.io.File p9, net.lingala.zip4j.model.UnzipParameters p10)
    {
        if (p8 != null) {
            if (p9 != null) {
                if (net.lingala.zip4j.util.Zip4jUtil.checkFileExists(p9)) {
                    if ((p10 == null) || (!p10.isIgnoreDateTimeAttributes())) {
                        net.lingala.zip4j.unzip.UnzipUtil.setFileLastModifiedTime(p8, p9);
                    }
                    if (p10 != null) {
                        if (!p10.isIgnoreAllFileAttributes()) {
                            int v6;
                            if (p10.isIgnoreReadOnlyFileAttribute()) {
                                v6 = 0;
                            } else {
                                v6 = 1;
                            }
                            int v3_0;
                            if (p10.isIgnoreHiddenFileAttribute()) {
                                v3_0 = 0;
                            } else {
                                v3_0 = 1;
                            }
                            int v4_0;
                            if (p10.isIgnoreArchiveFileAttribute()) {
                                v4_0 = 0;
                            } else {
                                v4_0 = 1;
                            }
                            int v5_0;
                            if (p10.isIgnoreSystemFileAttribute()) {
                                v5_0 = 0;
                            } else {
                                v5_0 = 1;
                            }
                            net.lingala.zip4j.unzip.UnzipUtil.setFileAttributes(p8, p9, v6, v3_0, v4_0, v5_0);
                        } else {
                            net.lingala.zip4j.unzip.UnzipUtil.setFileAttributes(p8, p9, 0, 0, 0, 0);
                        }
                    } else {
                        net.lingala.zip4j.unzip.UnzipUtil.setFileAttributes(p8, p9, 1, 1, 1, 1);
                    }
                    return;
                } else {
                    throw new net.lingala.zip4j.exception.ZipException("cannot set file properties: file doesnot exist");
                }
            } else {
                throw new net.lingala.zip4j.exception.ZipException("cannot set file properties: output file is null");
            }
        } else {
            throw new net.lingala.zip4j.exception.ZipException("cannot set file properties: file header is null");
        }
    }

    private static void setFileAttributes(net.lingala.zip4j.model.FileHeader p4, java.io.File p5, boolean p6, boolean p7, boolean p8, boolean p9)
    {
        if (p4 != null) {
            byte[] v1 = p4.getExternalFileAttr();
            if (v1 != null) {
                switch (v1[0]) {
                    case 1:
                        if (p6) {
                            net.lingala.zip4j.util.Zip4jUtil.setFileReadOnly(p5);
                        }
                        break;
                    case 2:
                    case 18:
                        if (p7) {
                            net.lingala.zip4j.util.Zip4jUtil.setFileHidden(p5);
                        }
                        break;
                    case 3:
                        if (p6) {
                            net.lingala.zip4j.util.Zip4jUtil.setFileReadOnly(p5);
                        }
                        if (p7) {
                            net.lingala.zip4j.util.Zip4jUtil.setFileHidden(p5);
                        }
                        break;
                    case 32:
                    case 48:
                        if (p8) {
                            net.lingala.zip4j.util.Zip4jUtil.setFileArchive(p5);
                        }
                        break;
                    case 33:
                        if (p8) {
                            net.lingala.zip4j.util.Zip4jUtil.setFileArchive(p5);
                        }
                        if (p6) {
                            net.lingala.zip4j.util.Zip4jUtil.setFileReadOnly(p5);
                        }
                        break;
                    case 34:
                    case 50:
                        if (p8) {
                            net.lingala.zip4j.util.Zip4jUtil.setFileArchive(p5);
                        }
                        if (p7) {
                            net.lingala.zip4j.util.Zip4jUtil.setFileHidden(p5);
                        }
                        break;
                    case 35:
                        if (p8) {
                            net.lingala.zip4j.util.Zip4jUtil.setFileArchive(p5);
                        }
                        if (p6) {
                            net.lingala.zip4j.util.Zip4jUtil.setFileReadOnly(p5);
                        }
                        if (p7) {
                            net.lingala.zip4j.util.Zip4jUtil.setFileHidden(p5);
                        }
                        break;
                    case 38:
                        if (p6) {
                            net.lingala.zip4j.util.Zip4jUtil.setFileReadOnly(p5);
                        }
                        if (p7) {
                            net.lingala.zip4j.util.Zip4jUtil.setFileHidden(p5);
                        }
                        if (p9) {
                            net.lingala.zip4j.util.Zip4jUtil.setFileSystemMode(p5);
                        }
                        break;
                    default:
                }
            }
            return;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("invalid file header. cannot set file attributes");
        }
    }

    private static void setFileLastModifiedTime(net.lingala.zip4j.model.FileHeader p2, java.io.File p3)
    {
        if ((p2.getLastModFileTime() > 0) && (p3.exists())) {
            p3.setLastModified(net.lingala.zip4j.util.Zip4jUtil.dosToJavaTme(p2.getLastModFileTime()));
        }
        return;
    }
}
