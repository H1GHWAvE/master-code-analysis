package net.lingala.zip4j.io;
public class SplitOutputStream extends java.io.OutputStream {
    private long bytesWrittenForThisPart;
    private int currSplitFileCounter;
    private java.io.File outFile;
    private java.io.RandomAccessFile raf;
    private long splitLength;
    private java.io.File zipFile;

    public SplitOutputStream(java.io.File p3)
    {
        this(p3, -1);
        return;
    }

    public SplitOutputStream(java.io.File p5, long p6)
    {
        if ((p6 < 0) || (p6 >= 65536)) {
            this.raf = new java.io.RandomAccessFile(p5, "rw");
            this.splitLength = p6;
            this.outFile = p5;
            this.zipFile = p5;
            this.currSplitFileCounter = 0;
            this.bytesWrittenForThisPart = 0;
            return;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("split length less than minimum allowed split length of 65536 Bytes");
        }
    }

    public SplitOutputStream(String p2)
    {
        int v0_1;
        if (!net.lingala.zip4j.util.Zip4jUtil.isStringNotNullAndNotEmpty(p2)) {
            v0_1 = 0;
        } else {
            v0_1 = new java.io.File(p2);
        }
        this(v0_1);
        return;
    }

    public SplitOutputStream(String p2, long p3)
    {
        int v0_1;
        if (net.lingala.zip4j.util.Zip4jUtil.isStringNotNullAndNotEmpty(p2)) {
            v0_1 = 0;
        } else {
            v0_1 = new java.io.File(p2);
        }
        this(v0_1, p3);
        return;
    }

    private boolean isHeaderData(byte[] p9)
    {
        int v3 = 0;
        if ((p9 != null) && (p9.length >= 4)) {
            int v2 = net.lingala.zip4j.util.Raw.readIntLittleEndian(p9, 0);
            long[] v0 = net.lingala.zip4j.util.Zip4jUtil.getAllHeaderSignatures();
            if ((v0 != null) && (v0.length > 0)) {
                int v1 = 0;
                while (v1 < v0.length) {
                    if ((v0[v1] == 134695760) || (v0[v1] != ((long) v2))) {
                        v1++;
                    } else {
                        v3 = 1;
                        break;
                    }
                }
            }
        }
        return v3;
    }

    private void startNextSplitFile()
    {
        try {
            String v2;
            String v4 = net.lingala.zip4j.util.Zip4jUtil.getZipFileNameWithoutExt(this.outFile.getName());
            String v3 = this.zipFile.getAbsolutePath();
        } catch (net.lingala.zip4j.exception.ZipException v1) {
            throw new java.io.IOException(v1.getMessage());
        }
        if (this.outFile.getParent() != null) {
            v2 = new StringBuilder().append(this.outFile.getParent()).append(System.getProperty("file.separator")).toString();
        } else {
            v2 = "";
        }
        java.io.File v0_1;
        if (this.currSplitFileCounter >= 9) {
            v0_1 = new java.io.File(new StringBuilder().append(v2).append(v4).append(".z").append((this.currSplitFileCounter + 1)).toString());
        } else {
            v0_1 = new java.io.File(new StringBuilder().append(v2).append(v4).append(".z0").append((this.currSplitFileCounter + 1)).toString());
        }
        this.raf.close();
        if (!v0_1.exists()) {
            if (this.zipFile.renameTo(v0_1)) {
                this.zipFile = new java.io.File(v3);
                this.raf = new java.io.RandomAccessFile(this.zipFile, "rw");
                this.currSplitFileCounter = (this.currSplitFileCounter + 1);
                return;
            } else {
                throw new java.io.IOException("cannot rename newly created split file");
            }
        } else {
            throw new java.io.IOException(new StringBuilder().append("split file: ").append(v0_1.getName()).append(" already exists in the current directory, cannot rename this file").toString());
        }
    }

    public boolean checkBuffSizeAndStartNextSplitFile(int p4)
    {
        if (p4 >= 0) {
            net.lingala.zip4j.exception.ZipException v1_1;
            if (this.isBuffSizeFitForCurrSplitFile(p4)) {
                v1_1 = 0;
            } else {
                try {
                    this.startNextSplitFile();
                    this.bytesWrittenForThisPart = 0;
                    v1_1 = 1;
                } catch (java.io.IOException v0) {
                    throw new net.lingala.zip4j.exception.ZipException(v0);
                }
            }
            return v1_1;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("negative buffersize for checkBuffSizeAndStartNextSplitFile");
        }
    }

    public void close()
    {
        if (this.raf != null) {
            this.raf.close();
        }
        return;
    }

    public void flush()
    {
        return;
    }

    public int getCurrSplitFileCounter()
    {
        return this.currSplitFileCounter;
    }

    public long getFilePointer()
    {
        return this.raf.getFilePointer();
    }

    public long getSplitLength()
    {
        return this.splitLength;
    }

    public boolean isBuffSizeFitForCurrSplitFile(int p6)
    {
        int v0_0 = 1;
        if (p6 >= 0) {
            if ((this.splitLength >= 65536) && ((this.bytesWrittenForThisPart + ((long) p6)) > this.splitLength)) {
                v0_0 = 0;
            }
            return v0_0;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("negative buffersize for isBuffSizeFitForCurrSplitFile");
        }
    }

    public boolean isSplitZipFile()
    {
        int v0_2;
        if (this.splitLength == -1) {
            v0_2 = 0;
        } else {
            v0_2 = 1;
        }
        return v0_2;
    }

    public void seek(long p2)
    {
        this.raf.seek(p2);
        return;
    }

    public void write(int p5)
    {
        byte[] v0 = new byte[1];
        v0[0] = ((byte) p5);
        this.write(v0, 0, 1);
        return;
    }

    public void write(byte[] p3)
    {
        this.write(p3, 0, p3.length);
        return;
    }

    public void write(byte[] p9, int p10, int p11)
    {
        if (p11 > 0) {
            if (this.splitLength == -1) {
                this.raf.write(p9, p10, p11);
                this.bytesWrittenForThisPart = (this.bytesWrittenForThisPart + ((long) p11));
            } else {
                if (this.splitLength >= 65536) {
                    if (this.bytesWrittenForThisPart < this.splitLength) {
                        if ((this.bytesWrittenForThisPart + ((long) p11)) <= this.splitLength) {
                            this.raf.write(p9, p10, p11);
                            this.bytesWrittenForThisPart = (this.bytesWrittenForThisPart + ((long) p11));
                        } else {
                            if (!this.isHeaderData(p9)) {
                                this.raf.write(p9, p10, ((int) (this.splitLength - this.bytesWrittenForThisPart)));
                                this.startNextSplitFile();
                                this.raf.write(p9, (((int) (this.splitLength - this.bytesWrittenForThisPart)) + p10), ((int) (((long) p11) - (this.splitLength - this.bytesWrittenForThisPart))));
                                this.bytesWrittenForThisPart = (((long) p11) - (this.splitLength - this.bytesWrittenForThisPart));
                            } else {
                                this.startNextSplitFile();
                                this.raf.write(p9, p10, p11);
                                this.bytesWrittenForThisPart = ((long) p11);
                            }
                        }
                    } else {
                        this.startNextSplitFile();
                        this.raf.write(p9, p10, p11);
                        this.bytesWrittenForThisPart = ((long) p11);
                    }
                } else {
                    throw new java.io.IOException("split length less than minimum allowed split length of 65536 Bytes");
                }
            }
        }
        return;
    }
}
