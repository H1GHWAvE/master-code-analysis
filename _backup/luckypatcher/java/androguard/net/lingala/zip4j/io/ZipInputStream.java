package net.lingala.zip4j.io;
public class ZipInputStream extends java.io.InputStream {
    private net.lingala.zip4j.io.BaseInputStream is;

    public ZipInputStream(net.lingala.zip4j.io.BaseInputStream p1)
    {
        this.is = p1;
        return;
    }

    public int available()
    {
        return this.is.available();
    }

    public void close()
    {
        this.close(0);
        return;
    }

    public void close(boolean p4)
    {
        try {
            this.is.close();
        } catch (net.lingala.zip4j.exception.ZipException v0) {
            throw new java.io.IOException(v0.getMessage());
        }
        if ((!p4) && (this.is.getUnzipEngine() != null)) {
            this.is.getUnzipEngine().checkCRC();
        }
        return;
    }

    public int read()
    {
        int v0 = this.is.read();
        if (v0 != -1) {
            this.is.getUnzipEngine().updateCRC(v0);
        }
        return v0;
    }

    public int read(byte[] p3)
    {
        return this.read(p3, 0, p3.length);
    }

    public int read(byte[] p3, int p4, int p5)
    {
        int v0 = this.is.read(p3, p4, p5);
        if ((v0 > 0) && (this.is.getUnzipEngine() != null)) {
            this.is.getUnzipEngine().updateCRC(p3, p4, v0);
        }
        return v0;
    }

    public long skip(long p3)
    {
        return this.is.skip(p3);
    }
}
