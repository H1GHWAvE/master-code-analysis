package net.lingala.zip4j.io;
public class CipherOutputStream extends net.lingala.zip4j.io.BaseOutputStream {
    private long bytesWrittenForThisFile;
    protected java.util.zip.CRC32 crc;
    private net.lingala.zip4j.crypto.IEncrypter encrypter;
    protected net.lingala.zip4j.model.FileHeader fileHeader;
    protected net.lingala.zip4j.model.LocalFileHeader localFileHeader;
    protected java.io.OutputStream outputStream;
    private byte[] pendingBuffer;
    private int pendingBufferLength;
    private java.io.File sourceFile;
    private long totalBytesRead;
    private long totalBytesWritten;
    protected net.lingala.zip4j.model.ZipModel zipModel;
    protected net.lingala.zip4j.model.ZipParameters zipParameters;

    public CipherOutputStream(java.io.OutputStream p4, net.lingala.zip4j.model.ZipModel p5)
    {
        this.outputStream = p4;
        this.initZipModel(p5);
        this.crc = new java.util.zip.CRC32();
        this.totalBytesWritten = 0;
        this.bytesWrittenForThisFile = 0;
        int v0_3 = new byte[16];
        this.pendingBuffer = v0_3;
        this.pendingBufferLength = 0;
        this.totalBytesRead = 0;
        return;
    }

    private void createFileHeader()
    {
        this.fileHeader = new net.lingala.zip4j.model.FileHeader();
        this.fileHeader.setSignature(33639248);
        this.fileHeader.setVersionMadeBy(20);
        this.fileHeader.setVersionNeededToExtract(20);
        if ((!this.zipParameters.isEncryptFiles()) || (this.zipParameters.getEncryptionMethod() != 99)) {
            this.fileHeader.setCompressionMethod(this.zipParameters.getCompressionMethod());
        } else {
            this.fileHeader.setCompressionMethod(99);
            this.fileHeader.setAesExtraDataRecord(this.generateAESExtraDataRecord(this.zipParameters));
        }
        if (this.zipParameters.isEncryptFiles()) {
            this.fileHeader.setEncrypted(1);
            this.fileHeader.setEncryptionMethod(this.zipParameters.getEncryptionMethod());
        }
        String v2;
        if (!this.zipParameters.isSourceExternalStream()) {
            this.fileHeader.setLastModFileTime(((int) net.lingala.zip4j.util.Zip4jUtil.javaToDosTime(net.lingala.zip4j.util.Zip4jUtil.getLastModifiedFileTime(this.sourceFile, this.zipParameters.getTimeZone()))));
            this.fileHeader.setUncompressedSize(this.sourceFile.length());
            v2 = net.lingala.zip4j.util.Zip4jUtil.getRelativeFileName(this.sourceFile.getAbsolutePath(), this.zipParameters.getRootFolderInZip(), this.zipParameters.getDefaultFolderPath());
        } else {
            this.fileHeader.setLastModFileTime(((int) net.lingala.zip4j.util.Zip4jUtil.javaToDosTime(System.currentTimeMillis())));
            if (net.lingala.zip4j.util.Zip4jUtil.isStringNotNullAndNotEmpty(this.zipParameters.getFileNameInZip())) {
                v2 = this.zipParameters.getFileNameInZip();
            } else {
                throw new net.lingala.zip4j.exception.ZipException("fileNameInZip is null or empty");
            }
        }
        if (net.lingala.zip4j.util.Zip4jUtil.isStringNotNullAndNotEmpty(v2)) {
            this.fileHeader.setFileName(v2);
            if (!net.lingala.zip4j.util.Zip4jUtil.isStringNotNullAndNotEmpty(this.zipModel.getFileNameCharset())) {
                this.fileHeader.setFileNameLength(net.lingala.zip4j.util.Zip4jUtil.getEncodedStringLength(v2));
            } else {
                this.fileHeader.setFileNameLength(net.lingala.zip4j.util.Zip4jUtil.getEncodedStringLength(v2, this.zipModel.getFileNameCharset()));
            }
            if (!(this.outputStream instanceof net.lingala.zip4j.io.SplitOutputStream)) {
                this.fileHeader.setDiskNumberStart(0);
            } else {
                this.fileHeader.setDiskNumberStart(((net.lingala.zip4j.io.SplitOutputStream) this.outputStream).getCurrSplitFileCounter());
            }
            int v1 = 0;
            if (!this.zipParameters.isSourceExternalStream()) {
                v1 = this.getFileAttributes(this.sourceFile);
            }
            byte[] v0 = new byte[4];
            v0[0] = ((byte) v1);
            v0[1] = 0;
            v0[2] = 0;
            v0[3] = 0;
            this.fileHeader.setExternalFileAttr(v0);
            if (!this.zipParameters.isSourceExternalStream()) {
                this.fileHeader.setDirectory(this.sourceFile.isDirectory());
            } else {
                if ((!v2.endsWith("/")) && (!v2.endsWith("\\"))) {
                    net.lingala.zip4j.model.FileHeader v8_56 = 0;
                } else {
                    v8_56 = 1;
                }
                this.fileHeader.setDirectory(v8_56);
            }
            if (!this.fileHeader.isDirectory()) {
                if (!this.zipParameters.isSourceExternalStream()) {
                    long v3 = net.lingala.zip4j.util.Zip4jUtil.getFileLengh(this.sourceFile);
                    if (this.zipParameters.getCompressionMethod() != 0) {
                        this.fileHeader.setCompressedSize(0);
                    } else {
                        if (this.zipParameters.getEncryptionMethod() != 0) {
                            if (this.zipParameters.getEncryptionMethod() != 99) {
                                this.fileHeader.setCompressedSize(0);
                            } else {
                                int v6;
                                switch (this.zipParameters.getAesKeyStrength()) {
                                    case 1:
                                        v6 = 8;
                                        break;
                                    case 2:
                                    default:
                                        throw new net.lingala.zip4j.exception.ZipException("invalid aes key strength, cannot determine key sizes");
                                        break;
                                    case 3:
                                        v6 = 16;
                                        break;
                                }
                                this.fileHeader.setCompressedSize((((((long) v6) + v3) + 10) + 2));
                            }
                        } else {
                            this.fileHeader.setCompressedSize((12 + v3));
                        }
                    }
                    this.fileHeader.setUncompressedSize(v3);
                }
            } else {
                this.fileHeader.setCompressedSize(0);
                this.fileHeader.setUncompressedSize(0);
            }
            if ((this.zipParameters.isEncryptFiles()) && (this.zipParameters.getEncryptionMethod() == 0)) {
                this.fileHeader.setCrc32(((long) this.zipParameters.getSourceFileCRC()));
            }
            byte[] v7 = new byte[2];
            v7[0] = net.lingala.zip4j.util.Raw.bitArrayToByte(this.generateGeneralPurposeBitArray(this.fileHeader.isEncrypted(), this.zipParameters.getCompressionMethod()));
            boolean v5 = net.lingala.zip4j.util.Zip4jUtil.isStringNotNullAndNotEmpty(this.zipModel.getFileNameCharset());
            if (((!v5) || (!this.zipModel.getFileNameCharset().equalsIgnoreCase("UTF8"))) && ((v5) || (!net.lingala.zip4j.util.Zip4jUtil.detectCharSet(this.fileHeader.getFileName()).equals("UTF8")))) {
                v7[1] = 0;
            } else {
                v7[1] = 8;
            }
            this.fileHeader.setGeneralPurposeFlag(v7);
            return;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("fileName is null or empty. unable to create file header");
        }
    }

    private void createLocalFileHeader()
    {
        if (this.fileHeader != null) {
            this.localFileHeader = new net.lingala.zip4j.model.LocalFileHeader();
            this.localFileHeader.setSignature(67324752);
            this.localFileHeader.setVersionNeededToExtract(this.fileHeader.getVersionNeededToExtract());
            this.localFileHeader.setCompressionMethod(this.fileHeader.getCompressionMethod());
            this.localFileHeader.setLastModFileTime(this.fileHeader.getLastModFileTime());
            this.localFileHeader.setUncompressedSize(this.fileHeader.getUncompressedSize());
            this.localFileHeader.setFileNameLength(this.fileHeader.getFileNameLength());
            this.localFileHeader.setFileName(this.fileHeader.getFileName());
            this.localFileHeader.setEncrypted(this.fileHeader.isEncrypted());
            this.localFileHeader.setEncryptionMethod(this.fileHeader.getEncryptionMethod());
            this.localFileHeader.setAesExtraDataRecord(this.fileHeader.getAesExtraDataRecord());
            this.localFileHeader.setCrc32(this.fileHeader.getCrc32());
            this.localFileHeader.setCompressedSize(this.fileHeader.getCompressedSize());
            this.localFileHeader.setGeneralPurposeFlag(((byte[]) ((byte[]) this.fileHeader.getGeneralPurposeFlag().clone())));
            return;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("file header is null, cannot create local file header");
        }
    }

    private void encryptAndWrite(byte[] p6, int p7, int p8)
    {
        if (this.encrypter != null) {
            try {
                this.encrypter.encryptData(p6, p7, p8);
            } catch (net.lingala.zip4j.exception.ZipException v0) {
                throw new java.io.IOException(v0.getMessage());
            }
        }
        this.outputStream.write(p6, p7, p8);
        this.totalBytesWritten = (this.totalBytesWritten + ((long) p8));
        this.bytesWrittenForThisFile = (this.bytesWrittenForThisFile + ((long) p8));
        return;
    }

    private net.lingala.zip4j.model.AESExtraDataRecord generateAESExtraDataRecord(net.lingala.zip4j.model.ZipParameters p6)
    {
        if (p6 != null) {
            net.lingala.zip4j.model.AESExtraDataRecord v0_1 = new net.lingala.zip4j.model.AESExtraDataRecord();
            v0_1.setSignature(39169);
            v0_1.setDataSize(7);
            v0_1.setVendorID("AE");
            v0_1.setVersionNumber(2);
            if (p6.getAesKeyStrength() != 1) {
                if (p6.getAesKeyStrength() != 3) {
                    throw new net.lingala.zip4j.exception.ZipException("invalid AES key strength, cannot generate AES Extra data record");
                } else {
                    v0_1.setAesStrength(3);
                }
            } else {
                v0_1.setAesStrength(1);
            }
            v0_1.setCompressionMethod(p6.getCompressionMethod());
            return v0_1;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("zip parameters are null, cannot generate AES Extra Data record");
        }
    }

    private int[] generateGeneralPurposeBitArray(boolean p5, int p6)
    {
        int[] v0 = new int[8];
        if (!p5) {
            v0[0] = 0;
        } else {
            v0[0] = 1;
        }
        if (p6 != 8) {
            v0[1] = 0;
            v0[2] = 0;
        }
        v0[3] = 1;
        return v0;
    }

    private int getFileAttributes(java.io.File p3)
    {
        int v0_0 = 0;
        if (p3 != null) {
            if (p3.exists()) {
                if (!p3.isDirectory()) {
                    if ((p3.canWrite()) || (!p3.isHidden())) {
                        if (p3.canWrite()) {
                            if (p3.isHidden()) {
                                v0_0 = 2;
                            }
                        } else {
                            v0_0 = 1;
                        }
                    } else {
                        v0_0 = 3;
                    }
                } else {
                    if (!p3.isHidden()) {
                        v0_0 = 16;
                    } else {
                        v0_0 = 18;
                    }
                }
            }
            return v0_0;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("input file is null, cannot get file attributes");
        }
    }

    private void initEncrypter()
    {
        if (this.zipParameters.isEncryptFiles()) {
            switch (this.zipParameters.getEncryptionMethod()) {
                case 0:
                    this.encrypter = new net.lingala.zip4j.crypto.StandardEncrypter(this.zipParameters.getPassword(), ((this.localFileHeader.getLastModFileTime() & 65535) << 16));
                    break;
                case 99:
                    this.encrypter = new net.lingala.zip4j.crypto.AESEncrpyter(this.zipParameters.getPassword(), this.zipParameters.getAesKeyStrength());
                    break;
                default:
                    throw new net.lingala.zip4j.exception.ZipException("invalid encprytion method");
            }
        } else {
            this.encrypter = 0;
        }
        return;
    }

    private void initZipModel(net.lingala.zip4j.model.ZipModel p5)
    {
        if (p5 != null) {
            this.zipModel = p5;
        } else {
            this.zipModel = new net.lingala.zip4j.model.ZipModel();
        }
        if (this.zipModel.getEndCentralDirRecord() == null) {
            this.zipModel.setEndCentralDirRecord(new net.lingala.zip4j.model.EndCentralDirRecord());
        }
        if (this.zipModel.getCentralDirectory() == null) {
            this.zipModel.setCentralDirectory(new net.lingala.zip4j.model.CentralDirectory());
        }
        if (this.zipModel.getCentralDirectory().getFileHeaders() == null) {
            this.zipModel.getCentralDirectory().setFileHeaders(new java.util.ArrayList());
        }
        if (this.zipModel.getLocalFileHeaderList() == null) {
            this.zipModel.setLocalFileHeaderList(new java.util.ArrayList());
        }
        if (((this.outputStream instanceof net.lingala.zip4j.io.SplitOutputStream)) && (((net.lingala.zip4j.io.SplitOutputStream) this.outputStream).isSplitZipFile())) {
            this.zipModel.setSplitArchive(1);
            this.zipModel.setSplitLength(((net.lingala.zip4j.io.SplitOutputStream) this.outputStream).getSplitLength());
        }
        this.zipModel.getEndCentralDirRecord().setSignature(101010256);
        return;
    }

    public void close()
    {
        if (this.outputStream != null) {
            this.outputStream.close();
        }
        return;
    }

    public void closeEntry()
    {
        if (this.pendingBufferLength != 0) {
            this.encryptAndWrite(this.pendingBuffer, 0, this.pendingBufferLength);
            this.pendingBufferLength = 0;
        }
        if ((this.zipParameters.isEncryptFiles()) && (this.zipParameters.getEncryptionMethod() == 99)) {
            if (!(this.encrypter instanceof net.lingala.zip4j.crypto.AESEncrpyter)) {
                throw new net.lingala.zip4j.exception.ZipException("invalid encrypter for AES encrypted file");
            } else {
                this.outputStream.write(((net.lingala.zip4j.crypto.AESEncrpyter) this.encrypter).getFinalMac());
                this.bytesWrittenForThisFile = (this.bytesWrittenForThisFile + 10);
                this.totalBytesWritten = (this.totalBytesWritten + 10);
            }
        }
        this.fileHeader.setCompressedSize(this.bytesWrittenForThisFile);
        this.localFileHeader.setCompressedSize(this.bytesWrittenForThisFile);
        if (this.zipParameters.isSourceExternalStream()) {
            this.fileHeader.setUncompressedSize(this.totalBytesRead);
            if (this.localFileHeader.getUncompressedSize() != this.totalBytesRead) {
                this.localFileHeader.setUncompressedSize(this.totalBytesRead);
            }
        }
        long v0 = this.crc.getValue();
        if ((this.fileHeader.isEncrypted()) && (this.fileHeader.getEncryptionMethod() == 99)) {
            v0 = 0;
        }
        if ((!this.zipParameters.isEncryptFiles()) || (this.zipParameters.getEncryptionMethod() != 99)) {
            this.fileHeader.setCrc32(v0);
            this.localFileHeader.setCrc32(v0);
        } else {
            this.fileHeader.setCrc32(0);
            this.localFileHeader.setCrc32(0);
        }
        this.zipModel.getLocalFileHeaderList().add(this.localFileHeader);
        this.zipModel.getCentralDirectory().getFileHeaders().add(this.fileHeader);
        this.totalBytesWritten = (this.totalBytesWritten + ((long) new net.lingala.zip4j.core.HeaderWriter().writeExtendedLocalHeader(this.localFileHeader, this.outputStream)));
        this.crc.reset();
        this.bytesWrittenForThisFile = 0;
        this.encrypter = 0;
        this.totalBytesRead = 0;
        return;
    }

    public void decrementCompressedFileSize(int p5)
    {
        if ((p5 > 0) && (((long) p5) <= this.bytesWrittenForThisFile)) {
            this.bytesWrittenForThisFile = (this.bytesWrittenForThisFile - ((long) p5));
        }
        return;
    }

    public void finish()
    {
        this.zipModel.getEndCentralDirRecord().setOffsetOfStartOfCentralDir(this.totalBytesWritten);
        new net.lingala.zip4j.core.HeaderWriter().finalizeZipFile(this.zipModel, this.outputStream);
        return;
    }

    public java.io.File getSourceFile()
    {
        return this.sourceFile;
    }

    public void putNextEntry(java.io.File p12, net.lingala.zip4j.model.ZipParameters p13)
    {
        if ((p13.isSourceExternalStream()) || (p12 != null)) {
            if ((p13.isSourceExternalStream()) || (net.lingala.zip4j.util.Zip4jUtil.checkFileExists(p12))) {
                try {
                    this.sourceFile = p12;
                    this.zipParameters = ((net.lingala.zip4j.model.ZipParameters) p13.clone());
                } catch (Exception v0_0) {
                    throw new net.lingala.zip4j.exception.ZipException(v0_0);
                } catch (Exception v0_2) {
                    throw new net.lingala.zip4j.exception.ZipException(v0_2);
                } catch (Exception v0_1) {
                    throw v0_1;
                }
                if (p13.isSourceExternalStream()) {
                    if (net.lingala.zip4j.util.Zip4jUtil.isStringNotNullAndNotEmpty(this.zipParameters.getFileNameInZip())) {
                        if ((this.zipParameters.getFileNameInZip().endsWith("/")) || (this.zipParameters.getFileNameInZip().endsWith("\\"))) {
                            this.zipParameters.setEncryptFiles(0);
                            this.zipParameters.setEncryptionMethod(-1);
                            this.zipParameters.setCompressionMethod(0);
                        }
                    } else {
                        throw new net.lingala.zip4j.exception.ZipException("file name is empty for external stream");
                    }
                } else {
                    if (this.sourceFile.isDirectory()) {
                        this.zipParameters.setEncryptFiles(0);
                        this.zipParameters.setEncryptionMethod(-1);
                        this.zipParameters.setCompressionMethod(0);
                    }
                }
                this.createFileHeader();
                this.createLocalFileHeader();
                if ((this.zipModel.isSplitArchive()) && ((this.zipModel.getCentralDirectory() == null) || ((this.zipModel.getCentralDirectory().getFileHeaders() == null) || (this.zipModel.getCentralDirectory().getFileHeaders().size() == 0)))) {
                    byte[] v3 = new byte[4];
                    net.lingala.zip4j.util.Raw.writeIntLittleEndian(v3, 0, 134695760);
                    this.outputStream.write(v3);
                    this.totalBytesWritten = (this.totalBytesWritten + 4);
                }
                if (!(this.outputStream instanceof net.lingala.zip4j.io.SplitOutputStream)) {
                    if (this.totalBytesWritten != 4) {
                        this.fileHeader.setOffsetLocalHeader(this.totalBytesWritten);
                    } else {
                        this.fileHeader.setOffsetLocalHeader(4);
                    }
                } else {
                    if (this.totalBytesWritten != 4) {
                        this.fileHeader.setOffsetLocalHeader(((net.lingala.zip4j.io.SplitOutputStream) this.outputStream).getFilePointer());
                    } else {
                        this.fileHeader.setOffsetLocalHeader(4);
                    }
                }
                this.totalBytesWritten = (this.totalBytesWritten + ((long) new net.lingala.zip4j.core.HeaderWriter().writeLocalFileHeader(this.zipModel, this.localFileHeader, this.outputStream)));
                if (this.zipParameters.isEncryptFiles()) {
                    this.initEncrypter();
                    if (this.encrypter != null) {
                        if (p13.getEncryptionMethod() != 0) {
                            if (p13.getEncryptionMethod() == 99) {
                                byte[] v5 = ((net.lingala.zip4j.crypto.AESEncrpyter) this.encrypter).getSaltBytes();
                                byte[] v4 = ((net.lingala.zip4j.crypto.AESEncrpyter) this.encrypter).getDerivedPasswordVerifier();
                                this.outputStream.write(v5);
                                this.outputStream.write(v4);
                                this.totalBytesWritten = (this.totalBytesWritten + ((long) (v5.length + v4.length)));
                                this.bytesWrittenForThisFile = (this.bytesWrittenForThisFile + ((long) (v5.length + v4.length)));
                            }
                        } else {
                            byte[] v1 = ((net.lingala.zip4j.crypto.StandardEncrypter) this.encrypter).getHeaderBytes();
                            this.outputStream.write(v1);
                            this.totalBytesWritten = (this.totalBytesWritten + ((long) v1.length));
                            this.bytesWrittenForThisFile = (this.bytesWrittenForThisFile + ((long) v1.length));
                        }
                    }
                }
                this.crc.reset();
                return;
            } else {
                throw new net.lingala.zip4j.exception.ZipException("input file does not exist");
            }
        } else {
            throw new net.lingala.zip4j.exception.ZipException("input file is null");
        }
    }

    public void setSourceFile(java.io.File p1)
    {
        this.sourceFile = p1;
        return;
    }

    protected void updateTotalBytesRead(int p5)
    {
        if (p5 > 0) {
            this.totalBytesRead = (this.totalBytesRead + ((long) p5));
        }
        return;
    }

    public void write(int p5)
    {
        byte[] v0 = new byte[1];
        v0[0] = ((byte) p5);
        this.write(v0, 0, 1);
        return;
    }

    public void write(byte[] p3)
    {
        if (p3 != null) {
            if (p3.length != 0) {
                this.write(p3, 0, p3.length);
            }
            return;
        } else {
            throw new NullPointerException();
        }
    }

    public void write(byte[] p5, int p6, int p7)
    {
        if (p7 != 0) {
            if ((this.zipParameters.isEncryptFiles()) && (this.zipParameters.getEncryptionMethod() == 99)) {
                if (this.pendingBufferLength != 0) {
                    if (p7 < (16 - this.pendingBufferLength)) {
                        System.arraycopy(p5, p6, this.pendingBuffer, this.pendingBufferLength, p7);
                        this.pendingBufferLength = (this.pendingBufferLength + p7);
                        return;
                    } else {
                        System.arraycopy(p5, p6, this.pendingBuffer, this.pendingBufferLength, (16 - this.pendingBufferLength));
                        this.encryptAndWrite(this.pendingBuffer, 0, this.pendingBuffer.length);
                        p6 = (16 - this.pendingBufferLength);
                        p7 -= p6;
                        this.pendingBufferLength = 0;
                    }
                }
                if ((p7 != 0) && ((p7 % 16) != 0)) {
                    System.arraycopy(p5, ((p7 + p6) - (p7 % 16)), this.pendingBuffer, 0, (p7 % 16));
                    this.pendingBufferLength = (p7 % 16);
                    p7 -= this.pendingBufferLength;
                }
            }
            if (p7 != 0) {
                this.encryptAndWrite(p5, p6, p7);
            }
        }
        return;
    }
}
