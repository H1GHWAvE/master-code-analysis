package net.lingala.zip4j.io;
public class ZipOutputStream extends net.lingala.zip4j.io.DeflaterOutputStream {

    public ZipOutputStream(java.io.OutputStream p2)
    {
        this(p2, 0);
        return;
    }

    public ZipOutputStream(java.io.OutputStream p1, net.lingala.zip4j.model.ZipModel p2)
    {
        this(p1, p2);
        return;
    }

    public void write(int p5)
    {
        byte[] v0 = new byte[1];
        v0[0] = ((byte) p5);
        this.write(v0, 0, 1);
        return;
    }

    public void write(byte[] p3)
    {
        this.write(p3, 0, p3.length);
        return;
    }

    public void write(byte[] p2, int p3, int p4)
    {
        this.crc.update(p2, p3, p4);
        this.updateTotalBytesRead(p4);
        super.write(p2, p3, p4);
        return;
    }
}
