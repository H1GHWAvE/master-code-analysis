package net.lingala.zip4j.crypto;
public class AESEncrpyter implements net.lingala.zip4j.crypto.IEncrypter {
    private int KEY_LENGTH;
    private int MAC_LENGTH;
    private final int PASSWORD_VERIFIER_LENGTH;
    private int SALT_LENGTH;
    private net.lingala.zip4j.crypto.engine.AESEngine aesEngine;
    private byte[] aesKey;
    private byte[] counterBlock;
    private byte[] derivedPasswordVerifier;
    private boolean finished;
    private byte[] iv;
    private int keyStrength;
    private int loopCount;
    private net.lingala.zip4j.crypto.PBKDF2.MacBasedPRF mac;
    private byte[] macKey;
    private int nonce;
    private char[] password;
    private byte[] saltBytes;

    public AESEncrpyter(char[] p5, int p6)
    {
        this.PASSWORD_VERIFIER_LENGTH = 2;
        this.nonce = 1;
        this.loopCount = 0;
        if ((p5 != null) && (p5.length != 0)) {
            if ((p6 == 1) || (p6 == 3)) {
                this.password = p5;
                this.keyStrength = p6;
                this.finished = 0;
                net.lingala.zip4j.exception.ZipException v0_3 = new byte[16];
                this.counterBlock = v0_3;
                net.lingala.zip4j.exception.ZipException v0_4 = new byte[16];
                this.iv = v0_4;
                this.init();
                return;
            } else {
                throw new net.lingala.zip4j.exception.ZipException("Invalid key strength in AES encrypter constructor");
            }
        } else {
            throw new net.lingala.zip4j.exception.ZipException("input password is empty or null in AES encrypter constructor");
        }
    }

    private byte[] deriveKey(byte[] p7, char[] p8)
    {
        try {
            return new net.lingala.zip4j.crypto.PBKDF2.PBKDF2Engine(new net.lingala.zip4j.crypto.PBKDF2.PBKDF2Parameters("HmacSHA1", "ISO-8859-1", p7, 1000)).deriveKey(p8, ((this.KEY_LENGTH + this.MAC_LENGTH) + 2));
        } catch (Exception v1_2) {
            throw new net.lingala.zip4j.exception.ZipException(v1_2);
        }
    }

    private static byte[] generateSalt(int p7)
    {
        if ((p7 == 8) || (p7 == 16)) {
            int v3 = 0;
            if (p7 == 8) {
                v3 = 2;
            }
            if (p7 == 16) {
                v3 = 4;
            }
            byte[] v4 = new byte[p7];
            int v1 = 0;
            while (v1 < v3) {
                int v0 = new java.util.Random().nextInt();
                v4[((v1 * 4) + 0)] = ((byte) (v0 >> 24));
                v4[((v1 * 4) + 1)] = ((byte) (v0 >> 16));
                v4[((v1 * 4) + 2)] = ((byte) (v0 >> 8));
                v4[((v1 * 4) + 3)] = ((byte) v0);
                v1++;
            }
            return v4;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("invalid salt size, cannot generate salt");
        }
    }

    private void init()
    {
        switch (this.keyStrength) {
            case 1:
                this.KEY_LENGTH = 16;
                this.MAC_LENGTH = 16;
                this.SALT_LENGTH = 8;
                break;
            case 2:
            default:
                throw new net.lingala.zip4j.exception.ZipException("invalid aes key strength, cannot determine key sizes");
                break;
            case 3:
                this.KEY_LENGTH = 32;
                this.MAC_LENGTH = 32;
                this.SALT_LENGTH = 16;
                break;
        }
        this.saltBytes = net.lingala.zip4j.crypto.AESEncrpyter.generateSalt(this.SALT_LENGTH);
        byte[] v0 = this.deriveKey(this.saltBytes, this.password);
        if ((v0 != null) && (v0.length == ((this.KEY_LENGTH + this.MAC_LENGTH) + 2))) {
            net.lingala.zip4j.crypto.PBKDF2.MacBasedPRF v1_7 = new byte[this.KEY_LENGTH];
            this.aesKey = v1_7;
            net.lingala.zip4j.crypto.PBKDF2.MacBasedPRF v1_9 = new byte[this.MAC_LENGTH];
            this.macKey = v1_9;
            net.lingala.zip4j.crypto.PBKDF2.MacBasedPRF v1_10 = new byte[2];
            this.derivedPasswordVerifier = v1_10;
            System.arraycopy(v0, 0, this.aesKey, 0, this.KEY_LENGTH);
            System.arraycopy(v0, this.KEY_LENGTH, this.macKey, 0, this.MAC_LENGTH);
            System.arraycopy(v0, (this.KEY_LENGTH + this.MAC_LENGTH), this.derivedPasswordVerifier, 0, 2);
            this.aesEngine = new net.lingala.zip4j.crypto.engine.AESEngine(this.aesKey);
            this.mac = new net.lingala.zip4j.crypto.PBKDF2.MacBasedPRF("HmacSHA1");
            this.mac.init(this.macKey);
            return;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("invalid key generated, cannot decrypt file");
        }
    }

    public int encryptData(byte[] p3)
    {
        if (p3 != null) {
            return this.encryptData(p3, 0, p3.length);
        } else {
            throw new net.lingala.zip4j.exception.ZipException("input bytes are null, cannot perform AES encrpytion");
        }
    }

    public int encryptData(byte[] p7, int p8, int p9)
    {
        if (!this.finished) {
            if ((p9 % 16) != 0) {
                this.finished = 1;
            }
            int v0 = p8;
            while (v0 < (p8 + p9)) {
                int v2_6;
                if ((v0 + 16) > (p8 + p9)) {
                    v2_6 = ((p8 + p9) - v0);
                } else {
                    v2_6 = 16;
                }
                this.loopCount = v2_6;
                net.lingala.zip4j.util.Raw.prepareBuffAESIVBytes(this.iv, this.nonce, 16);
                this.aesEngine.processBlock(this.iv, this.counterBlock);
                int v1 = 0;
                while (v1 < this.loopCount) {
                    p7[(v0 + v1)] = ((byte) (p7[(v0 + v1)] ^ this.counterBlock[v1]));
                    v1++;
                }
                this.mac.update(p7, v0, this.loopCount);
                this.nonce = (this.nonce + 1);
                v0 += 16;
            }
            return p9;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("AES Encrypter is in finished state (A non 16 byte block has already been passed to encrypter)");
        }
    }

    public byte[] getDerivedPasswordVerifier()
    {
        return this.derivedPasswordVerifier;
    }

    public byte[] getFinalMac()
    {
        byte[] v0 = new byte[10];
        System.arraycopy(this.mac.doFinal(), 0, v0, 0, 10);
        return v0;
    }

    public int getPasswordVeriifierLength()
    {
        return 2;
    }

    public byte[] getSaltBytes()
    {
        return this.saltBytes;
    }

    public int getSaltLength()
    {
        return this.SALT_LENGTH;
    }

    public void setDerivedPasswordVerifier(byte[] p1)
    {
        this.derivedPasswordVerifier = p1;
        return;
    }

    public void setSaltBytes(byte[] p1)
    {
        this.saltBytes = p1;
        return;
    }
}
