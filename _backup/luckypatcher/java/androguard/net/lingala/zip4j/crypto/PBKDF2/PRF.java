package net.lingala.zip4j.crypto.PBKDF2;
interface PRF {

    public abstract byte[] doFinal();

    public abstract int getHLen();

    public abstract void init();
}
