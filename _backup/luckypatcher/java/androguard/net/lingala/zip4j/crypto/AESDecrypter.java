package net.lingala.zip4j.crypto;
public class AESDecrypter implements net.lingala.zip4j.crypto.IDecrypter {
    private int KEY_LENGTH;
    private int MAC_LENGTH;
    private final int PASSWORD_VERIFIER_LENGTH;
    private int SALT_LENGTH;
    private net.lingala.zip4j.crypto.engine.AESEngine aesEngine;
    private byte[] aesKey;
    private byte[] counterBlock;
    private byte[] derivedPasswordVerifier;
    private byte[] iv;
    private net.lingala.zip4j.model.LocalFileHeader localFileHeader;
    private int loopCount;
    private net.lingala.zip4j.crypto.PBKDF2.MacBasedPRF mac;
    private byte[] macKey;
    private int nonce;
    private byte[] storedMac;

    public AESDecrypter(net.lingala.zip4j.model.LocalFileHeader p3, byte[] p4, byte[] p5)
    {
        this.PASSWORD_VERIFIER_LENGTH = 2;
        this.nonce = 1;
        this.loopCount = 0;
        if (p3 != null) {
            this.localFileHeader = p3;
            this.storedMac = 0;
            byte[] v0_4 = new byte[16];
            this.iv = v0_4;
            byte[] v0_5 = new byte[16];
            this.counterBlock = v0_5;
            this.init(p4, p5);
            return;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("one of the input parameters is null in AESDecryptor Constructor");
        }
    }

    private byte[] deriveKey(byte[] p7, char[] p8)
    {
        try {
            return new net.lingala.zip4j.crypto.PBKDF2.PBKDF2Engine(new net.lingala.zip4j.crypto.PBKDF2.PBKDF2Parameters("HmacSHA1", "ISO-8859-1", p7, 1000)).deriveKey(p8, ((this.KEY_LENGTH + this.MAC_LENGTH) + 2));
        } catch (Exception v1_2) {
            throw new net.lingala.zip4j.exception.ZipException(v1_2);
        }
    }

    private void init(byte[] p9, byte[] p10)
    {
        if (this.localFileHeader != null) {
            net.lingala.zip4j.model.AESExtraDataRecord v0 = this.localFileHeader.getAesExtraDataRecord();
            if (v0 != null) {
                switch (v0.getAesStrength()) {
                    case 1:
                        this.KEY_LENGTH = 16;
                        this.MAC_LENGTH = 16;
                        this.SALT_LENGTH = 8;
                        break;
                    case 2:
                        this.KEY_LENGTH = 24;
                        this.MAC_LENGTH = 24;
                        this.SALT_LENGTH = 12;
                        break;
                    case 3:
                        this.KEY_LENGTH = 32;
                        this.MAC_LENGTH = 32;
                        this.SALT_LENGTH = 16;
                        break;
                    default:
                        throw new net.lingala.zip4j.exception.ZipException(new StringBuilder().append("invalid aes key strength for file: ").append(this.localFileHeader.getFileName()).toString());
                }
                if ((this.localFileHeader.getPassword() != null) && (this.localFileHeader.getPassword().length > 0)) {
                    byte[] v1 = this.deriveKey(p9, this.localFileHeader.getPassword());
                    if ((v1 != null) && (v1.length == ((this.KEY_LENGTH + this.MAC_LENGTH) + 2))) {
                        net.lingala.zip4j.crypto.PBKDF2.MacBasedPRF v2_14 = new byte[this.KEY_LENGTH];
                        this.aesKey = v2_14;
                        net.lingala.zip4j.crypto.PBKDF2.MacBasedPRF v2_16 = new byte[this.MAC_LENGTH];
                        this.macKey = v2_16;
                        net.lingala.zip4j.crypto.PBKDF2.MacBasedPRF v2_17 = new byte[2];
                        this.derivedPasswordVerifier = v2_17;
                        System.arraycopy(v1, 0, this.aesKey, 0, this.KEY_LENGTH);
                        System.arraycopy(v1, this.KEY_LENGTH, this.macKey, 0, this.MAC_LENGTH);
                        System.arraycopy(v1, (this.KEY_LENGTH + this.MAC_LENGTH), this.derivedPasswordVerifier, 0, 2);
                        if (this.derivedPasswordVerifier != null) {
                            if (java.util.Arrays.equals(p10, this.derivedPasswordVerifier)) {
                                this.aesEngine = new net.lingala.zip4j.crypto.engine.AESEngine(this.aesKey);
                                this.mac = new net.lingala.zip4j.crypto.PBKDF2.MacBasedPRF("HmacSHA1");
                                this.mac.init(this.macKey);
                                return;
                            } else {
                                throw new net.lingala.zip4j.exception.ZipException(new StringBuilder().append("Wrong Password for file: ").append(this.localFileHeader.getFileName()).toString(), 5);
                            }
                        } else {
                            throw new net.lingala.zip4j.exception.ZipException("invalid derived password verifier for AES");
                        }
                    } else {
                        throw new net.lingala.zip4j.exception.ZipException("invalid derived key");
                    }
                } else {
                    throw new net.lingala.zip4j.exception.ZipException("empty or null password provided for AES Decryptor");
                }
            } else {
                throw new net.lingala.zip4j.exception.ZipException("invalid aes extra data record - in init method of AESDecryptor");
            }
        } else {
            throw new net.lingala.zip4j.exception.ZipException("invalid file header in init method of AESDecryptor");
        }
    }

    public int decryptData(byte[] p3)
    {
        return this.decryptData(p3, 0, p3.length);
    }

    public int decryptData(byte[] p8, int p9, int p10)
    {
        if (this.aesEngine != null) {
            int v1 = p9;
            while (v1 < (p9 + p10)) {
                try {
                    int v3_4;
                    if ((v1 + 16) > (p9 + p10)) {
                        v3_4 = ((p9 + p10) - v1);
                    } else {
                        v3_4 = 16;
                    }
                    this.loopCount = v3_4;
                    this.mac.update(p8, v1, this.loopCount);
                    net.lingala.zip4j.util.Raw.prepareBuffAESIVBytes(this.iv, this.nonce, 16);
                    this.aesEngine.processBlock(this.iv, this.counterBlock);
                    int v2 = 0;
                    while (v2 < this.loopCount) {
                        p8[(v1 + v2)] = ((byte) (p8[(v1 + v2)] ^ this.counterBlock[v2]));
                        v2++;
                    }
                    this.nonce = (this.nonce + 1);
                    v1 += 16;
                } catch (Exception v0_1) {
                    throw v0_1;
                } catch (Exception v0_0) {
                    throw new net.lingala.zip4j.exception.ZipException(v0_0);
                }
            }
            return p10;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("AES not initialized properly");
        }
    }

    public byte[] getCalculatedAuthenticationBytes()
    {
        return this.mac.doFinal();
    }

    public int getPasswordVerifierLength()
    {
        return 2;
    }

    public int getSaltLength()
    {
        return this.SALT_LENGTH;
    }

    public byte[] getStoredMac()
    {
        return this.storedMac;
    }

    public void setStoredMac(byte[] p1)
    {
        this.storedMac = p1;
        return;
    }
}
