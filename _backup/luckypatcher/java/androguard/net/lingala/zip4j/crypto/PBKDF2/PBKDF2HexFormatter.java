package net.lingala.zip4j.crypto.PBKDF2;
 class PBKDF2HexFormatter {

    PBKDF2HexFormatter()
    {
        return;
    }

    public boolean fromString(net.lingala.zip4j.crypto.PBKDF2.PBKDF2Parameters p9, String p10)
    {
        int v4 = 0;
        if ((p9 != null) && (p10 != null)) {
            String[] v2 = p10.split(":");
            if ((v2 != null) && (v2.length == 3)) {
                byte[] v3 = net.lingala.zip4j.crypto.PBKDF2.BinTools.hex2bin(v2[0]);
                int v1 = Integer.parseInt(v2[1]);
                byte[] v0 = net.lingala.zip4j.crypto.PBKDF2.BinTools.hex2bin(v2[2]);
                p9.setSalt(v3);
                p9.setIterationCount(v1);
                p9.setDerivedKey(v0);
            } else {
                v4 = 1;
            }
        } else {
            v4 = 1;
        }
        return v4;
    }

    public String toString(net.lingala.zip4j.crypto.PBKDF2.PBKDF2Parameters p4)
    {
        return new StringBuilder().append(net.lingala.zip4j.crypto.PBKDF2.BinTools.bin2hex(p4.getSalt())).append(":").append(String.valueOf(p4.getIterationCount())).append(":").append(net.lingala.zip4j.crypto.PBKDF2.BinTools.bin2hex(p4.getDerivedKey())).toString();
    }
}
