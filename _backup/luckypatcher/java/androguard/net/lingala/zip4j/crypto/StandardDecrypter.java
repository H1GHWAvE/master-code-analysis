package net.lingala.zip4j.crypto;
public class StandardDecrypter implements net.lingala.zip4j.crypto.IDecrypter {
    private byte[] crc;
    private net.lingala.zip4j.model.FileHeader fileHeader;
    private net.lingala.zip4j.crypto.engine.ZipCryptoEngine zipCryptoEngine;

    public StandardDecrypter(net.lingala.zip4j.model.FileHeader p3, byte[] p4)
    {
        net.lingala.zip4j.crypto.engine.ZipCryptoEngine v0_1 = new byte[4];
        this.crc = v0_1;
        if (p3 != null) {
            this.fileHeader = p3;
            this.zipCryptoEngine = new net.lingala.zip4j.crypto.engine.ZipCryptoEngine();
            this.init(p4);
            return;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("one of more of the input parameters were null in StandardDecryptor");
        }
    }

    public int decryptData(byte[] p3)
    {
        return this.decryptData(p3, 0, p3.length);
    }

    public int decryptData(byte[] p6, int p7, int p8)
    {
        if ((p7 >= 0) && (p8 >= 0)) {
            int v1 = p7;
            while (v1 < (p7 + p8)) {
                try {
                    int v2_1 = ((this.zipCryptoEngine.decryptByte() ^ (p6[v1] & 255)) & 255);
                    this.zipCryptoEngine.updateKeys(((byte) v2_1));
                    p6[v1] = ((byte) v2_1);
                    v1++;
                } catch (Exception v0) {
                    throw new net.lingala.zip4j.exception.ZipException(v0);
                }
            }
            return p8;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("one of the input parameters were null in standard decrpyt data");
        }
    }

    public void init(byte[] p12)
    {
        byte[] v0 = this.fileHeader.getCrcBuff();
        this.crc[3] = ((byte) (v0[3] & 255));
        this.crc[2] = ((byte) ((v0[3] >> 8) & 255));
        this.crc[1] = ((byte) ((v0[3] >> 16) & 255));
        this.crc[0] = ((byte) ((v0[3] >> 24) & 255));
        if ((this.crc[2] <= 0) && ((this.crc[1] <= 0) && (this.crc[0] <= 0))) {
            if ((this.fileHeader.getPassword() != null) && (this.fileHeader.getPassword().length > 0)) {
                this.zipCryptoEngine.initKeys(this.fileHeader.getPassword());
                try {
                    byte v3 = p12[0];
                    int v2 = 0;
                } catch (Exception v1) {
                    throw new net.lingala.zip4j.exception.ZipException(v1);
                }
                while (v2 < 12) {
                    this.zipCryptoEngine.updateKeys(((byte) (this.zipCryptoEngine.decryptByte() ^ v3)));
                    if ((v2 + 1) != 12) {
                        v3 = p12[(v2 + 1)];
                    }
                    v2++;
                }
                return;
            } else {
                throw new net.lingala.zip4j.exception.ZipException("Wrong password!", 5);
            }
        } else {
            throw new IllegalStateException("Invalid CRC in File Header");
        }
    }
}
