package net.lingala.zip4j.util;
public class Zip4jUtil {

    public Zip4jUtil()
    {
        return;
    }

    public static boolean checkArrayListTypes(java.util.ArrayList p4, int p5)
    {
        int v2_0 = 1;
        if (p4 != null) {
            if (p4.size() > 0) {
                int v1 = 0;
                switch (p5) {
                    case 1:
                        int v0_1 = 0;
                        while (v0_1 < p4.size()) {
                            if ((p4.get(v0_1) instanceof java.io.File)) {
                                v0_1++;
                            } else {
                                v1 = 1;
                                break;
                            }
                        }
                        break;
                    case 2:
                        int v0_0 = 0;
                        while (v0_0 < p4.size()) {
                            if ((p4.get(v0_0) instanceof String)) {
                                v0_0++;
                            } else {
                                v1 = 1;
                                break;
                            }
                        }
                        break;
                }
                if (v1 != 0) {
                    v2_0 = 0;
                }
            }
            return v2_0;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("input arraylist is null, cannot check types");
        }
    }

    public static boolean checkFileExists(java.io.File p2)
    {
        if (p2 != null) {
            return p2.exists();
        } else {
            throw new net.lingala.zip4j.exception.ZipException("cannot check if file exists: input file is null");
        }
    }

    public static boolean checkFileExists(String p3)
    {
        if (net.lingala.zip4j.util.Zip4jUtil.isStringNotNullAndNotEmpty(p3)) {
            return net.lingala.zip4j.util.Zip4jUtil.checkFileExists(new java.io.File(p3));
        } else {
            throw new net.lingala.zip4j.exception.ZipException("path is null");
        }
    }

    public static boolean checkFileReadAccess(String p5)
    {
        if (net.lingala.zip4j.util.Zip4jUtil.isStringNotNullAndNotEmpty(p5)) {
            if (net.lingala.zip4j.util.Zip4jUtil.checkFileExists(p5)) {
                try {
                    return new java.io.File(p5).canRead();
                } catch (Exception v0) {
                    throw new net.lingala.zip4j.exception.ZipException("cannot read zip file");
                }
            } else {
                throw new net.lingala.zip4j.exception.ZipException(new StringBuilder().append("file does not exist: ").append(p5).toString());
            }
        } else {
            throw new net.lingala.zip4j.exception.ZipException("path is null");
        }
    }

    public static boolean checkFileWriteAccess(String p5)
    {
        if (net.lingala.zip4j.util.Zip4jUtil.isStringNotNullAndNotEmpty(p5)) {
            if (net.lingala.zip4j.util.Zip4jUtil.checkFileExists(p5)) {
                try {
                    return new java.io.File(p5).canWrite();
                } catch (Exception v0) {
                    throw new net.lingala.zip4j.exception.ZipException("cannot read zip file");
                }
            } else {
                throw new net.lingala.zip4j.exception.ZipException(new StringBuilder().append("file does not exist: ").append(p5).toString());
            }
        } else {
            throw new net.lingala.zip4j.exception.ZipException("path is null");
        }
    }

    public static boolean checkOutputFolder(String p5)
    {
        if (net.lingala.zip4j.util.Zip4jUtil.isStringNotNullAndNotEmpty(p5)) {
            java.io.File v1_1 = new java.io.File(p5);
            if (!v1_1.exists()) {
                try {
                    v1_1.mkdirs();
                } catch (Exception v0) {
                    throw new net.lingala.zip4j.exception.ZipException("Cannot create destination folder");
                }
                if (v1_1.isDirectory()) {
                    if (!v1_1.canWrite()) {
                        throw new net.lingala.zip4j.exception.ZipException("no write access to destination folder");
                    }
                } else {
                    throw new net.lingala.zip4j.exception.ZipException("output folder is not valid");
                }
            } else {
                if (v1_1.isDirectory()) {
                    if (!v1_1.canWrite()) {
                        throw new net.lingala.zip4j.exception.ZipException("no write access to output folder");
                    }
                } else {
                    throw new net.lingala.zip4j.exception.ZipException("output folder is not valid");
                }
            }
            return 1;
        } else {
            throw new net.lingala.zip4j.exception.ZipException(new NullPointerException("output path is null"));
        }
    }

    public static byte[] convertCharset(String p5)
    {
        try {
            byte[] v1;
            String v0 = net.lingala.zip4j.util.Zip4jUtil.detectCharSet(p5);
        } catch (java.io.UnsupportedEncodingException v3) {
            v1 = p5.getBytes();
            return v1;
        } catch (Exception v2) {
            throw new net.lingala.zip4j.exception.ZipException(v2);
        }
        if (!v0.equals("Cp850")) {
            if (!v0.equals("UTF8")) {
                v1 = p5.getBytes();
                return v1;
            } else {
                v1 = p5.getBytes("UTF8");
                return v1;
            }
        } else {
            v1 = p5.getBytes("Cp850");
            return v1;
        }
    }

    public static String decodeFileName(byte[] p3, boolean p4)
    {
        String v1_0;
        if (!p4) {
            v1_0 = net.lingala.zip4j.util.Zip4jUtil.getCp850EncodedString(p3);
        } else {
            try {
                v1_0 = new String(p3, "UTF8");
            } catch (java.io.UnsupportedEncodingException v0) {
                v1_0 = new String(p3);
            }
        }
        return v1_0;
    }

    public static String detectCharSet(String p5)
    {
        if (p5 != null) {
            try {
                String v3_6;
                if (!p5.equals(new String(p5.getBytes("Cp850"), "Cp850"))) {
                    if (!p5.equals(new String(p5.getBytes("UTF8"), "UTF8"))) {
                        v3_6 = net.lingala.zip4j.util.InternalZipConstants.CHARSET_DEFAULT;
                    } else {
                        v3_6 = "UTF8";
                    }
                } else {
                    v3_6 = "Cp850";
                }
            } catch (Exception v1) {
                v3_6 = net.lingala.zip4j.util.InternalZipConstants.CHARSET_DEFAULT;
            } catch (Exception v1) {
                v3_6 = net.lingala.zip4j.util.InternalZipConstants.CHARSET_DEFAULT;
            }
            return v3_6;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("input string is null, cannot detect charset");
        }
    }

    public static long dosToJavaTme(int p9)
    {
        int v6 = ((p9 & 31) * 2);
        int v5 = ((p9 >> 5) & 63);
        int v4 = ((p9 >> 11) & 31);
        int v3 = ((p9 >> 16) & 31);
        int v2 = (((p9 >> 21) & 15) - 1);
        int v1 = (((p9 >> 25) & 127) + 1980);
        java.util.Calendar v0 = java.util.Calendar.getInstance();
        v0.set(v1, v2, v3, v4, v5, v6);
        v0.set(14, 0);
        return v0.getTime().getTime();
    }

    public static String getAbsoluteFilePath(String p3)
    {
        if (net.lingala.zip4j.util.Zip4jUtil.isStringNotNullAndNotEmpty(p3)) {
            return new java.io.File(p3).getAbsolutePath();
        } else {
            throw new net.lingala.zip4j.exception.ZipException("filePath is null or empty, cannot get absolute file path");
        }
    }

    public static long[] getAllHeaderSignatures()
    {
        long[] v0 = new long[11];
        v0[0] = 67324752;
        v0[1] = 134695760;
        v0[2] = 33639248;
        v0[3] = 101010256;
        v0[4] = 84233040;
        v0[5] = 134630224;
        v0[6] = 134695760;
        v0[7] = 117853008;
        v0[8] = 101075792;
        v0[9] = 1;
        v0[10] = 39169;
        return v0;
    }

    public static String getCp850EncodedString(byte[] p3)
    {
        try {
            String v1_1 = new String(p3, "Cp850");
        } catch (java.io.UnsupportedEncodingException v0) {
            v1_1 = new String(p3);
        }
        return v1_1;
    }

    public static int getEncodedStringLength(String p3)
    {
        if (net.lingala.zip4j.util.Zip4jUtil.isStringNotNullAndNotEmpty(p3)) {
            return net.lingala.zip4j.util.Zip4jUtil.getEncodedStringLength(p3, net.lingala.zip4j.util.Zip4jUtil.detectCharSet(p3));
        } else {
            throw new net.lingala.zip4j.exception.ZipException("input string is null, cannot calculate encoded String length");
        }
    }

    public static int getEncodedStringLength(String p4, String p5)
    {
        if (net.lingala.zip4j.util.Zip4jUtil.isStringNotNullAndNotEmpty(p4)) {
            if (net.lingala.zip4j.util.Zip4jUtil.isStringNotNullAndNotEmpty(p5)) {
                try {
                    java.nio.ByteBuffer v0;
                    if (!p5.equals("Cp850")) {
                        if (!p5.equals("UTF8")) {
                            v0 = java.nio.ByteBuffer.wrap(p4.getBytes(p5));
                        } else {
                            v0 = java.nio.ByteBuffer.wrap(p4.getBytes("UTF8"));
                        }
                    } else {
                        v0 = java.nio.ByteBuffer.wrap(p4.getBytes("Cp850"));
                    }
                } catch (Exception v1) {
                    v0 = java.nio.ByteBuffer.wrap(p4.getBytes());
                } catch (Exception v1) {
                    throw new net.lingala.zip4j.exception.ZipException(v1);
                }
                return v0.limit();
            } else {
                throw new net.lingala.zip4j.exception.ZipException("encoding is not defined, cannot calculate string length");
            }
        } else {
            throw new net.lingala.zip4j.exception.ZipException("input string is null, cannot calculate encoded String length");
        }
    }

    public static net.lingala.zip4j.model.FileHeader getFileHeader(net.lingala.zip4j.model.ZipModel p4, String p5)
    {
        if (p4 != null) {
            if (net.lingala.zip4j.util.Zip4jUtil.isStringNotNullAndNotEmpty(p5)) {
                net.lingala.zip4j.model.FileHeader v0 = net.lingala.zip4j.util.Zip4jUtil.getFileHeaderWithExactMatch(p4, p5);
                if (v0 == null) {
                    String v5_1 = p5.replaceAll("\\\\", "/");
                    v0 = net.lingala.zip4j.util.Zip4jUtil.getFileHeaderWithExactMatch(p4, v5_1);
                    if (v0 == null) {
                        v0 = net.lingala.zip4j.util.Zip4jUtil.getFileHeaderWithExactMatch(p4, v5_1.replaceAll("/", "\\\\"));
                    }
                }
                return v0;
            } else {
                throw new net.lingala.zip4j.exception.ZipException(new StringBuilder().append("file name is null, cannot determine file header for fileName: ").append(p5).toString());
            }
        } else {
            throw new net.lingala.zip4j.exception.ZipException(new StringBuilder().append("zip model is null, cannot determine file header for fileName: ").append(p5).toString());
        }
    }

    public static net.lingala.zip4j.model.FileHeader getFileHeaderWithExactMatch(net.lingala.zip4j.model.ZipModel p7, String p8)
    {
        if (p7 != null) {
            if (net.lingala.zip4j.util.Zip4jUtil.isStringNotNullAndNotEmpty(p8)) {
                if (p7.getCentralDirectory() != null) {
                    if (p7.getCentralDirectory().getFileHeaders() != null) {
                        net.lingala.zip4j.exception.ZipException v0_0;
                        if (p7.getCentralDirectory().getFileHeaders().size() > 0) {
                            java.util.ArrayList v1 = p7.getCentralDirectory().getFileHeaders();
                            int v3 = 0;
                            while (v3 < v1.size()) {
                                v0_0 = ((net.lingala.zip4j.model.FileHeader) v1.get(v3));
                                String v2 = v0_0.getFileName();
                                if ((!net.lingala.zip4j.util.Zip4jUtil.isStringNotNullAndNotEmpty(v2)) || (!p8.equalsIgnoreCase(v2))) {
                                    v3++;
                                }
                            }
                            v0_0 = 0;
                        } else {
                            v0_0 = 0;
                        }
                        return v0_0;
                    } else {
                        throw new net.lingala.zip4j.exception.ZipException(new StringBuilder().append("file Headers are null, cannot determine file header with exact match for fileName: ").append(p8).toString());
                    }
                } else {
                    throw new net.lingala.zip4j.exception.ZipException(new StringBuilder().append("central directory is null, cannot determine file header with exact match for fileName: ").append(p8).toString());
                }
            } else {
                throw new net.lingala.zip4j.exception.ZipException(new StringBuilder().append("file name is null, cannot determine file header with exact match for fileName: ").append(p8).toString());
            }
        } else {
            throw new net.lingala.zip4j.exception.ZipException(new StringBuilder().append("zip model is null, cannot determine file header with exact match for fileName: ").append(p8).toString());
        }
    }

    public static long getFileLengh(java.io.File p2)
    {
        if (p2 != null) {
            long v0_1;
            if (!p2.isDirectory()) {
                v0_1 = p2.length();
            } else {
                v0_1 = -1;
            }
            return v0_1;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("input file is null, cannot calculate file length");
        }
    }

    public static long getFileLengh(String p2)
    {
        if (net.lingala.zip4j.util.Zip4jUtil.isStringNotNullAndNotEmpty(p2)) {
            return net.lingala.zip4j.util.Zip4jUtil.getFileLengh(new java.io.File(p2));
        } else {
            throw new net.lingala.zip4j.exception.ZipException("invalid file name");
        }
    }

    public static String getFileNameFromFilePath(java.io.File p2)
    {
        if (p2 != null) {
            String v0_1;
            if (!p2.isDirectory()) {
                v0_1 = p2.getName();
            } else {
                v0_1 = 0;
            }
            return v0_1;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("input file is null, cannot get file name");
        }
    }

    public static java.util.ArrayList getFilesInDirectoryRec(java.io.File p8, boolean p9)
    {
        if (p8 != null) {
            java.util.ArrayList v5_1 = new java.util.ArrayList();
            java.util.List v3 = java.util.Arrays.asList(p8.listFiles());
            if (p8.canRead()) {
                int v4 = 0;
                while (v4 < v3.size()) {
                    java.io.File v1_1 = ((java.io.File) v3.get(v4));
                    if ((v1_1.isHidden()) && (!p9)) {
                        break;
                    }
                    v5_1.add(v1_1);
                    if (v1_1.isDirectory()) {
                        v5_1.addAll(net.lingala.zip4j.util.Zip4jUtil.getFilesInDirectoryRec(v1_1, p9));
                    }
                    v4++;
                }
            }
            return v5_1;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("input path is null, cannot read files in the directory");
        }
    }

    public static int getIndexOfFileHeader(net.lingala.zip4j.model.ZipModel p7, net.lingala.zip4j.model.FileHeader p8)
    {
        if ((p7 != null) && (p8 != null)) {
            if (p7.getCentralDirectory() != null) {
                if (p7.getCentralDirectory().getFileHeaders() != null) {
                    int v4;
                    if (p7.getCentralDirectory().getFileHeaders().size() > 0) {
                        String v2 = p8.getFileName();
                        if (net.lingala.zip4j.util.Zip4jUtil.isStringNotNullAndNotEmpty(v2)) {
                            java.util.ArrayList v1 = p7.getCentralDirectory().getFileHeaders();
                            v4 = 0;
                            while (v4 < v1.size()) {
                                String v3 = ((net.lingala.zip4j.model.FileHeader) v1.get(v4)).getFileName();
                                if ((!net.lingala.zip4j.util.Zip4jUtil.isStringNotNullAndNotEmpty(v3)) || (!v2.equalsIgnoreCase(v3))) {
                                    v4++;
                                }
                            }
                            v4 = -1;
                        } else {
                            throw new net.lingala.zip4j.exception.ZipException("file name in file header is empty or null, cannot determine index of file header");
                        }
                    } else {
                        v4 = -1;
                    }
                    return v4;
                } else {
                    throw new net.lingala.zip4j.exception.ZipException("file Headers are null, cannot determine index of file header");
                }
            } else {
                throw new net.lingala.zip4j.exception.ZipException("central directory is null, ccannot determine index of file header");
            }
        } else {
            throw new net.lingala.zip4j.exception.ZipException("input parameters is null, cannot determine index of file header");
        }
    }

    public static long getLastModifiedFileTime(java.io.File p2, java.util.TimeZone p3)
    {
        if (p2 != null) {
            if (p2.exists()) {
                return p2.lastModified();
            } else {
                throw new net.lingala.zip4j.exception.ZipException("input file does not exist, cannot read last modified file time");
            }
        } else {
            throw new net.lingala.zip4j.exception.ZipException("input file is null, cannot read last modified file time");
        }
    }

    public static String getRelativeFileName(String p9, String p10, String p11)
    {
        if (net.lingala.zip4j.util.Zip4jUtil.isStringNotNullAndNotEmpty(p9)) {
            String v1;
            if (!net.lingala.zip4j.util.Zip4jUtil.isStringNotNullAndNotEmpty(p11)) {
                java.io.File v2_1 = new java.io.File(p9);
                if (!v2_1.isDirectory()) {
                    v1 = net.lingala.zip4j.util.Zip4jUtil.getFileNameFromFilePath(new java.io.File(p9));
                } else {
                    v1 = new StringBuilder().append(v2_1.getName()).append("/").toString();
                }
            } else {
                String v4 = new java.io.File(p11).getPath();
                if (!v4.endsWith(net.lingala.zip4j.util.InternalZipConstants.FILE_SEPARATOR)) {
                    v4 = new StringBuilder().append(v4).append(net.lingala.zip4j.util.InternalZipConstants.FILE_SEPARATOR).toString();
                }
                String v6_0 = p9.substring(v4.length());
                if (v6_0.startsWith(System.getProperty("file.separator"))) {
                    v6_0 = v6_0.substring(1);
                }
                String v6_1;
                java.io.File v5_1 = new java.io.File(p9);
                if (!v5_1.isDirectory()) {
                    v6_1 = new StringBuilder().append(v6_0.substring(0, v6_0.lastIndexOf(v5_1.getName())).replaceAll("\\\\", "/")).append(v5_1.getName()).toString();
                } else {
                    v6_1 = new StringBuilder().append(v6_0.replaceAll("\\\\", "/")).append("/").toString();
                }
                v1 = v6_1;
            }
            if (net.lingala.zip4j.util.Zip4jUtil.isStringNotNullAndNotEmpty(p10)) {
                v1 = new StringBuilder().append(p10).append(v1).toString();
            }
            if (net.lingala.zip4j.util.Zip4jUtil.isStringNotNullAndNotEmpty(v1)) {
                return v1;
            } else {
                throw new net.lingala.zip4j.exception.ZipException("Error determining file name");
            }
        } else {
            throw new net.lingala.zip4j.exception.ZipException("input file path/name is empty, cannot calculate relative file name");
        }
    }

    public static java.util.ArrayList getSplitZipFiles(net.lingala.zip4j.model.ZipModel p9)
    {
        if (p9 != null) {
            java.util.ArrayList v5_1;
            if (p9.getEndCentralDirRecord() != null) {
                v5_1 = new java.util.ArrayList();
                String v0 = p9.getZipFile();
                String v6 = new java.io.File(v0).getName();
                if (net.lingala.zip4j.util.Zip4jUtil.isStringNotNullAndNotEmpty(v0)) {
                    if (p9.isSplitArchive()) {
                        int v3 = p9.getEndCentralDirRecord().getNoOfThisDisk();
                        if (v3 != 0) {
                            int v2 = 0;
                            while (v2 <= v3) {
                                if (v2 != v3) {
                                    String v1 = ".z0";
                                    if (v2 > 9) {
                                        v1 = ".z";
                                    }
                                    String v4_0;
                                    if (v6.indexOf(".") < 0) {
                                        v4_0 = v0;
                                    } else {
                                        v4_0 = v0.substring(0, v0.lastIndexOf("."));
                                    }
                                    v5_1.add(new StringBuilder().append(v4_0).append(v1).append((v2 + 1)).toString());
                                } else {
                                    v5_1.add(p9.getZipFile());
                                }
                                v2++;
                            }
                        } else {
                            v5_1.add(v0);
                        }
                    } else {
                        v5_1.add(v0);
                    }
                } else {
                    throw new net.lingala.zip4j.exception.ZipException("cannot get split zip files: zipfile is null");
                }
            } else {
                v5_1 = 0;
            }
            return v5_1;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("cannot get split zip files: zipmodel is null");
        }
    }

    public static String getZipFileNameWithoutExt(String p3)
    {
        if (net.lingala.zip4j.util.Zip4jUtil.isStringNotNullAndNotEmpty(p3)) {
            String v0 = p3;
            if (p3.indexOf(System.getProperty("file.separator")) >= 0) {
                v0 = p3.substring(p3.lastIndexOf(System.getProperty("file.separator")));
            }
            if (v0.indexOf(".") > 0) {
                v0 = v0.substring(0, v0.lastIndexOf("."));
            }
            return v0;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("zip file name is empty or null, cannot determine zip file name");
        }
    }

    public static boolean isStringNotNullAndNotEmpty(String p1)
    {
        if ((p1 != null) && (p1.trim().length() > 0)) {
            int v0_2 = 1;
        } else {
            v0_2 = 0;
        }
        return v0_2;
    }

    public static boolean isSupportedCharset(String p3)
    {
        if (net.lingala.zip4j.util.Zip4jUtil.isStringNotNullAndNotEmpty(p3)) {
            try {
                new String("a".getBytes(), p3);
                net.lingala.zip4j.exception.ZipException v1_2 = 1;
            } catch (Exception v0) {
                v1_2 = 0;
            } catch (Exception v0) {
                throw new net.lingala.zip4j.exception.ZipException(v0);
            }
            return v1_2;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("charset is null or empty, cannot check if it is supported");
        }
    }

    public static boolean isWindows()
    {
        int v1_4;
        if (System.getProperty("os.name").toLowerCase().indexOf("win") < 0) {
            v1_4 = 0;
        } else {
            v1_4 = 1;
        }
        return v1_4;
    }

    public static long javaToDosTime(long p4)
    {
        long v2_9;
        java.util.Calendar v0 = java.util.Calendar.getInstance();
        v0.setTimeInMillis(p4);
        int v1 = v0.get(1);
        if (v1 >= 1980) {
            v2_9 = ((long) (((((((v1 + -1980) << 25) | ((v0.get(2) + 1) << 21)) | (v0.get(5) << 16)) | (v0.get(11) << 11)) | (v0.get(12) << 5)) | (v0.get(13) >> 1)));
        } else {
            v2_9 = 2162688;
        }
        return v2_9;
    }

    public static void setFileArchive(java.io.File p0)
    {
        return;
    }

    public static void setFileHidden(java.io.File p0)
    {
        return;
    }

    public static void setFileReadOnly(java.io.File p2)
    {
        if (p2 != null) {
            if (p2.exists()) {
                p2.setReadOnly();
            }
            return;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("input file is null. cannot set read only file attribute");
        }
    }

    public static void setFileSystemMode(java.io.File p0)
    {
        return;
    }
}
