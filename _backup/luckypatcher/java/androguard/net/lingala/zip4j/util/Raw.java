package net.lingala.zip4j.util;
public class Raw {

    public Raw()
    {
        return;
    }

    public static byte bitArrayToByte(int[] p8)
    {
        if (p8 != null) {
            if (p8.length == 8) {
                if (net.lingala.zip4j.util.Raw.checkBits(p8)) {
                    int v1 = 0;
                    int v0 = 0;
                    while (v0 < p8.length) {
                        v1 = ((int) (((double) v1) + (Math.pow(2.0, ((double) v0)) * ((double) p8[v0]))));
                        v0++;
                    }
                    return ((byte) v1);
                } else {
                    throw new net.lingala.zip4j.exception.ZipException("invalid bits provided, bits contain other values than 0 or 1");
                }
            } else {
                throw new net.lingala.zip4j.exception.ZipException("invalid bit array length, cannot calculate byte");
            }
        } else {
            throw new net.lingala.zip4j.exception.ZipException("bit array is null, cannot calculate byte from bits");
        }
    }

    private static boolean checkBits(int[] p3)
    {
        int v1 = 1;
        int v0 = 0;
        while (v0 < p3.length) {
            if ((p3[v0] == 0) || (p3[v0] == 1)) {
                v0++;
            } else {
                v1 = 0;
                break;
            }
        }
        return v1;
    }

    public static byte[] convertCharArrayToByteArray(char[] p3)
    {
        if (p3 != null) {
            byte[] v0 = new byte[p3.length];
            int v1 = 0;
            while (v1 < p3.length) {
                v0[v1] = ((byte) p3[v1]);
                v1++;
            }
            return v0;
        } else {
            throw new NullPointerException();
        }
    }

    public static void prepareBuffAESIVBytes(byte[] p3, int p4, int p5)
    {
        p3[0] = ((byte) p4);
        p3[1] = ((byte) (p4 >> 8));
        p3[2] = ((byte) (p4 >> 16));
        p3[3] = ((byte) (p4 >> 24));
        p3[4] = 0;
        p3[5] = 0;
        p3[6] = 0;
        p3[7] = 0;
        p3[8] = 0;
        p3[9] = 0;
        p3[10] = 0;
        p3[11] = 0;
        p3[12] = 0;
        p3[13] = 0;
        p3[14] = 0;
        p3[15] = 0;
        return;
    }

    public static int readIntLittleEndian(byte[] p3, int p4)
    {
        return (((p3[p4] & 255) | ((p3[(p4 + 1)] & 255) << 8)) | (((p3[(p4 + 2)] & 255) | ((p3[(p4 + 3)] & 255) << 8)) << 16));
    }

    public static int readLeInt(java.io.DataInput p4, byte[] p5)
    {
        try {
            p4.readFully(p5, 0, 4);
            return (((p5[0] & 255) | ((p5[1] & 255) << 8)) | (((p5[2] & 255) | ((p5[3] & 255) << 8)) << 16));
        } catch (java.io.IOException v0) {
            throw new net.lingala.zip4j.exception.ZipException(v0);
        }
    }

    public static long readLongLittleEndian(byte[] p5, int p6)
    {
        return (((((((((((((((0 | ((long) (p5[(p6 + 7)] & 255))) << 8) | ((long) (p5[(p6 + 6)] & 255))) << 8) | ((long) (p5[(p6 + 5)] & 255))) << 8) | ((long) (p5[(p6 + 4)] & 255))) << 8) | ((long) (p5[(p6 + 3)] & 255))) << 8) | ((long) (p5[(p6 + 2)] & 255))) << 8) | ((long) (p5[(p6 + 1)] & 255))) << 8) | ((long) (p5[p6] & 255)));
    }

    public static final short readShortBigEndian(byte[] p2, int p3)
    {
        return ((short) ((p2[(p3 + 1)] & 255) | ((short) (((short) ((p2[p3] & 255) | 0)) << 8))));
    }

    public static int readShortLittleEndian(byte[] p2, int p3)
    {
        return ((p2[p3] & 255) | ((p2[(p3 + 1)] & 255) << 8));
    }

    public static byte[] toByteArray(int p3)
    {
        byte[] v0 = new byte[4];
        v0[0] = ((byte) p3);
        v0[1] = ((byte) (p3 >> 8));
        v0[2] = ((byte) (p3 >> 16));
        v0[3] = ((byte) (p3 >> 24));
        return v0;
    }

    public static byte[] toByteArray(int p4, int p5)
    {
        byte[] v2 = new byte[p5];
        byte[] v1 = net.lingala.zip4j.util.Raw.toByteArray(p4);
        int v0 = 0;
        while ((v0 < v1.length) && (v0 < p5)) {
            v2[v0] = v1[v0];
            v0++;
        }
        return v2;
    }

    public static final void writeIntLittleEndian(byte[] p2, int p3, int p4)
    {
        p2[(p3 + 3)] = ((byte) (p4 >> 24));
        p2[(p3 + 2)] = ((byte) (p4 >> 16));
        p2[(p3 + 1)] = ((byte) (p4 >> 8));
        p2[p3] = ((byte) (p4 & 255));
        return;
    }

    public static void writeLongLittleEndian(byte[] p3, int p4, long p5)
    {
        p3[(p4 + 7)] = ((byte) ((int) (p5 >> 56)));
        p3[(p4 + 6)] = ((byte) ((int) (p5 >> 48)));
        p3[(p4 + 5)] = ((byte) ((int) (p5 >> 40)));
        p3[(p4 + 4)] = ((byte) ((int) (p5 >> 32)));
        p3[(p4 + 3)] = ((byte) ((int) (p5 >> 24)));
        p3[(p4 + 2)] = ((byte) ((int) (p5 >> 16)));
        p3[(p4 + 1)] = ((byte) ((int) (p5 >> 8)));
        p3[p4] = ((byte) ((int) (255 & p5)));
        return;
    }

    public static final void writeShortLittleEndian(byte[] p2, int p3, short p4)
    {
        p2[(p3 + 1)] = ((byte) (p4 >> 8));
        p2[p3] = ((byte) (p4 & 255));
        return;
    }
}
