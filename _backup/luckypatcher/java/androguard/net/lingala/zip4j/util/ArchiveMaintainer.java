package net.lingala.zip4j.util;
public class ArchiveMaintainer {

    public ArchiveMaintainer()
    {
        return;
    }

    static synthetic void access$000(net.lingala.zip4j.util.ArchiveMaintainer p0, net.lingala.zip4j.model.ZipModel p1, java.io.File p2, net.lingala.zip4j.progress.ProgressMonitor p3)
    {
        p0.initMergeSplitZipFile(p1, p2, p3);
        return;
    }

    private long calculateTotalWorkForMergeOp(net.lingala.zip4j.model.ZipModel p12)
    {
        long v5 = 0;
        if (p12.isSplitArchive()) {
            int v4 = p12.getEndCentralDirRecord().getNoOfThisDisk();
            String v0 = p12.getZipFile();
            int v1 = 0;
            while (v1 <= v4) {
                String v2;
                if (0 != p12.getEndCentralDirRecord().getNoOfThisDisk()) {
                    if (0 < 9) {
                        v2 = new StringBuilder().append(v0.substring(0, v0.lastIndexOf("."))).append(".z0").append(1).toString();
                    } else {
                        v2 = new StringBuilder().append(v0.substring(0, v0.lastIndexOf("."))).append(".z").append(1).toString();
                    }
                } else {
                    v2 = p12.getZipFile();
                }
                v5 += net.lingala.zip4j.util.Zip4jUtil.getFileLengh(new java.io.File(v2));
                v1++;
            }
        }
        return v5;
    }

    private long calculateTotalWorkForRemoveOp(net.lingala.zip4j.model.ZipModel p5, net.lingala.zip4j.model.FileHeader p6)
    {
        return (net.lingala.zip4j.util.Zip4jUtil.getFileLengh(new java.io.File(p5.getZipFile())) - p6.getCompressedSize());
    }

    private void copyFile(java.io.RandomAccessFile p14, java.io.OutputStream p15, long p16, long p18, net.lingala.zip4j.progress.ProgressMonitor p20)
    {
        if ((p14 != null) && (p15 != null)) {
            if (p16 >= 0) {
                if (p18 >= 0) {
                    if (p16 <= p18) {
                        if (p16 != p18) {
                            if (!p20.isCancelAllTasks()) {
                                try {
                                    byte[] v2;
                                    p14.seek(p16);
                                    long v3 = 0;
                                    long v5 = (p18 - p16);
                                } catch (Exception v7_1) {
                                    throw new net.lingala.zip4j.exception.ZipException(v7_1);
                                } catch (Exception v7_0) {
                                    throw new net.lingala.zip4j.exception.ZipException(v7_0);
                                }
                                if ((p18 - p16) >= 4096) {
                                    v2 = new byte[4096];
                                } else {
                                    v2 = new byte[((int) (p18 - p16))];
                                }
                                while(true) {
                                    int v8 = p14.read(v2);
                                    if (v8 != -1) {
                                        p15.write(v2, 0, v8);
                                        p20.updateWorkCompleted(((long) v8));
                                        if (p20.isCancelAllTasks()) {
                                            break;
                                        }
                                        v3 += ((long) v8);
                                        if (v3 != v5) {
                                            if ((((long) v2.length) + v3) > v5) {
                                                v2 = new byte[((int) (v5 - v3))];
                                            }
                                        }
                                    }
                                }
                                p20.setResult(3);
                            } else {
                                p20.setResult(3);
                                p20.setState(0);
                            }
                        }
                        return;
                    } else {
                        throw new net.lingala.zip4j.exception.ZipException("start offset is greater than end offset, cannot copy file");
                    }
                } else {
                    throw new net.lingala.zip4j.exception.ZipException("end offset is negative, cannot copy file");
                }
            } else {
                throw new net.lingala.zip4j.exception.ZipException("starting offset is negative, cannot copy file");
            }
        } else {
            throw new net.lingala.zip4j.exception.ZipException("input or output stream is null, cannot copy file");
        }
    }

    private java.io.RandomAccessFile createFileHandler(net.lingala.zip4j.model.ZipModel p5, String p6)
    {
        if ((p5 != null) && (net.lingala.zip4j.util.Zip4jUtil.isStringNotNullAndNotEmpty(p5.getZipFile()))) {
            try {
                return new java.io.RandomAccessFile(new java.io.File(p5.getZipFile()), p6);
            } catch (java.io.FileNotFoundException v0) {
                throw new net.lingala.zip4j.exception.ZipException(v0);
            }
        } else {
            throw new net.lingala.zip4j.exception.ZipException("input parameter is null in getFilePointer, cannot create file handler to remove file");
        }
    }

    private java.io.RandomAccessFile createSplitZipFileHandler(net.lingala.zip4j.model.ZipModel p8, int p9)
    {
        if (p8 != null) {
            if (p9 >= 0) {
                try {
                    String v2;
                    String v0 = p8.getZipFile();
                } catch (Exception v1_1) {
                    throw new net.lingala.zip4j.exception.ZipException(v1_1);
                } catch (Exception v1_0) {
                    throw new net.lingala.zip4j.exception.ZipException(v1_0);
                }
                if (p9 != p8.getEndCentralDirRecord().getNoOfThisDisk()) {
                    if (p9 < 9) {
                        v2 = new StringBuilder().append(v0.substring(0, v0.lastIndexOf("."))).append(".z0").append((p9 + 1)).toString();
                    } else {
                        v2 = new StringBuilder().append(v0.substring(0, v0.lastIndexOf("."))).append(".z").append((p9 + 1)).toString();
                    }
                } else {
                    v2 = p8.getZipFile();
                }
                java.io.File v3_1 = new java.io.File(v2);
                if (net.lingala.zip4j.util.Zip4jUtil.checkFileExists(v3_1)) {
                    return new java.io.RandomAccessFile(v3_1, "r");
                } else {
                    throw new net.lingala.zip4j.exception.ZipException(new StringBuilder().append("split file does not exist: ").append(v2).toString());
                }
            } else {
                throw new net.lingala.zip4j.exception.ZipException("invlaid part number, cannot create split file handler");
            }
        } else {
            throw new net.lingala.zip4j.exception.ZipException("zip model is null, cannot create split file handler");
        }
    }

    private void initMergeSplitZipFile(net.lingala.zip4j.model.ZipModel p24, java.io.File p25, net.lingala.zip4j.progress.ProgressMonitor p26)
    {
        if (p24 != null) {
            if (p24.isSplitArchive()) {
                java.io.OutputStream v5 = 0;
                java.io.RandomAccessFile v4 = 0;
                java.util.ArrayList v14_1 = new java.util.ArrayList();
                long v20 = 0;
                int v18 = 0;
                try {
                    int v22 = p24.getEndCentralDirRecord().getNoOfThisDisk();
                } catch (java.io.IOException v3_24) {
                    if (v5 != null) {
                        try {
                            v5.close();
                        } catch (long v6) {
                        }
                    }
                    if (v4 != null) {
                        try {
                            v4.close();
                        } catch (long v6) {
                        }
                    }
                    throw v3_24;
                } catch (Exception v12_0) {
                    p26.endProgressMonitorError(v12_0);
                    throw new net.lingala.zip4j.exception.ZipException(v12_0);
                } catch (Exception v12_1) {
                    p26.endProgressMonitorError(v12_1);
                    throw new net.lingala.zip4j.exception.ZipException(v12_1);
                }
                if (v22 > 0) {
                    v5 = this.prepareOutputStreamForMerge(p25);
                    int v16 = 0;
                    while (v16 <= v22) {
                        v4 = this.createSplitZipFileHandler(p24, v16);
                        int v19 = 0;
                        Long v13_1 = new Long(v4.length());
                        if ((v16 == 0) && ((p24.getCentralDirectory() != null) && ((p24.getCentralDirectory().getFileHeaders() != null) && (p24.getCentralDirectory().getFileHeaders().size() > 0)))) {
                            byte[] v11 = new byte[4];
                            v4.seek(0);
                            v4.read(v11);
                            if (((long) net.lingala.zip4j.util.Raw.readIntLittleEndian(v11, 0)) == 134695760) {
                                v19 = 4;
                                v18 = 1;
                            }
                        }
                        if (v16 == v22) {
                            v13_1 = new Long(p24.getEndCentralDirRecord().getOffsetOfStartOfCentralDir());
                        }
                        this.copyFile(v4, v5, ((long) v19), v13_1.longValue(), p26);
                        v20 += (v13_1.longValue() - ((long) v19));
                        if (!p26.isCancelAllTasks()) {
                            v14_1.add(v13_1);
                            try {
                                v4.close();
                            } catch (java.io.IOException v3) {
                            }
                            v16++;
                        } else {
                            p26.setResult(3);
                            p26.setState(0);
                            if (v5 != null) {
                                try {
                                    v5.close();
                                } catch (java.io.IOException v3) {
                                }
                            }
                            if (v4 != null) {
                                try {
                                    v4.close();
                                } catch (java.io.IOException v3) {
                                }
                            }
                        }
                        return;
                    }
                    net.lingala.zip4j.model.ZipModel v17_1 = ((net.lingala.zip4j.model.ZipModel) p24.clone());
                    v17_1.getEndCentralDirRecord().setOffsetOfStartOfCentralDir(v20);
                    this.updateSplitZipModel(v17_1, v14_1, v18);
                    new net.lingala.zip4j.core.HeaderWriter().finalizeZipFileWithoutValidations(v17_1, v5);
                    p26.endProgressMonitorSuccess();
                    if (v5 != null) {
                        try {
                            v5.close();
                        } catch (java.io.IOException v3) {
                        }
                    }
                    if (v4 != null) {
                        try {
                            v4.close();
                        } catch (java.io.IOException v3) {
                        }
                    }
                    return;
                } else {
                    throw new net.lingala.zip4j.exception.ZipException("corrupt zip model, archive not a split zip file");
                }
            } else {
                Exception v12_3 = new net.lingala.zip4j.exception.ZipException("archive not a split zip file");
                p26.endProgressMonitorError(v12_3);
                throw v12_3;
            }
        } else {
            Exception v12_5 = new net.lingala.zip4j.exception.ZipException("one of the input parameters is null, cannot merge split zip file");
            p26.endProgressMonitorError(v12_5);
            throw v12_5;
        }
    }

    private java.io.OutputStream prepareOutputStreamForMerge(java.io.File p4)
    {
        if (p4 != null) {
            try {
                return new java.io.FileOutputStream(p4);
            } catch (Exception v0_1) {
                throw new net.lingala.zip4j.exception.ZipException(v0_1);
            } catch (Exception v0_0) {
                throw new net.lingala.zip4j.exception.ZipException(v0_0);
            }
        } else {
            throw new net.lingala.zip4j.exception.ZipException("outFile is null, cannot create outputstream");
        }
    }

    private void restoreFileName(java.io.File p4, String p5)
    {
        if (!p4.delete()) {
            throw new net.lingala.zip4j.exception.ZipException("cannot delete old zip file");
        } else {
            if (new java.io.File(p5).renameTo(p4)) {
                return;
            } else {
                throw new net.lingala.zip4j.exception.ZipException("cannot rename modified zip file");
            }
        }
    }

    private void updateSplitEndCentralDirectory(net.lingala.zip4j.model.ZipModel p4)
    {
        try {
            if (p4 != null) {
                if (p4.getCentralDirectory() != null) {
                    p4.getEndCentralDirRecord().setNoOfThisDisk(0);
                    p4.getEndCentralDirRecord().setNoOfThisDiskStartOfCentralDir(0);
                    p4.getEndCentralDirRecord().setTotNoOfEntriesInCentralDir(p4.getCentralDirectory().getFileHeaders().size());
                    p4.getEndCentralDirRecord().setTotNoOfEntriesInCentralDirOnThisDisk(p4.getCentralDirectory().getFileHeaders().size());
                    return;
                } else {
                    throw new net.lingala.zip4j.exception.ZipException("corrupt zip model - getCentralDirectory, cannot update split zip model");
                }
            } else {
                throw new net.lingala.zip4j.exception.ZipException("zip model is null - cannot update end of central directory for split zip model");
            }
        } catch (Exception v0_1) {
            throw v0_1;
        } catch (Exception v0_0) {
            throw new net.lingala.zip4j.exception.ZipException(v0_0);
        }
    }

    private void updateSplitFileHeader(net.lingala.zip4j.model.ZipModel p13, java.util.ArrayList p14, boolean p15)
    {
        try {
            if (p13.getCentralDirectory() != null) {
                int v1 = p13.getCentralDirectory().getFileHeaders().size();
                int v6 = 0;
                if (p15) {
                    v6 = 4;
                }
                int v2 = 0;
                while (v2 < v1) {
                    long v4 = 0;
                    int v3 = 0;
                    while (v3 < ((net.lingala.zip4j.model.FileHeader) p13.getCentralDirectory().getFileHeaders().get(v2)).getDiskNumberStart()) {
                        v4 += ((Long) p14.get(v3)).longValue();
                        v3++;
                    }
                    ((net.lingala.zip4j.model.FileHeader) p13.getCentralDirectory().getFileHeaders().get(v2)).setOffsetLocalHeader(((((net.lingala.zip4j.model.FileHeader) p13.getCentralDirectory().getFileHeaders().get(v2)).getOffsetLocalHeader() + v4) - ((long) v6)));
                    ((net.lingala.zip4j.model.FileHeader) p13.getCentralDirectory().getFileHeaders().get(v2)).setDiskNumberStart(0);
                    v2++;
                }
                return;
            } else {
                throw new net.lingala.zip4j.exception.ZipException("corrupt zip model - getCentralDirectory, cannot update split zip model");
            }
        } catch (Exception v0_1) {
            throw v0_1;
        } catch (Exception v0_0) {
            throw new net.lingala.zip4j.exception.ZipException(v0_0);
        }
    }

    private void updateSplitZip64EndCentralDirLocator(net.lingala.zip4j.model.ZipModel p7, java.util.ArrayList p8)
    {
        if (p7 != null) {
            if (p7.getZip64EndCentralDirLocator() != null) {
                p7.getZip64EndCentralDirLocator().setNoOfDiskStartOfZip64EndOfCentralDirRec(0);
                long v1 = 0;
                int v0 = 0;
                while (v0 < p8.size()) {
                    v1 += ((Long) p8.get(v0)).longValue();
                    v0++;
                }
                p7.getZip64EndCentralDirLocator().setOffsetZip64EndOfCentralDirRec((p7.getZip64EndCentralDirLocator().getOffsetZip64EndOfCentralDirRec() + v1));
                p7.getZip64EndCentralDirLocator().setTotNumberOfDiscs(1);
            }
            return;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("zip model is null, cannot update split Zip64 end of central directory locator");
        }
    }

    private void updateSplitZip64EndCentralDirRec(net.lingala.zip4j.model.ZipModel p7, java.util.ArrayList p8)
    {
        if (p7 != null) {
            if (p7.getZip64EndCentralDirRecord() != null) {
                p7.getZip64EndCentralDirRecord().setNoOfThisDisk(0);
                p7.getZip64EndCentralDirRecord().setNoOfThisDiskStartOfCentralDir(0);
                p7.getZip64EndCentralDirRecord().setTotNoOfEntriesInCentralDirOnThisDisk(((long) p7.getEndCentralDirRecord().getTotNoOfEntriesInCentralDir()));
                long v1 = 0;
                int v0 = 0;
                while (v0 < p8.size()) {
                    v1 += ((Long) p8.get(v0)).longValue();
                    v0++;
                }
                p7.getZip64EndCentralDirRecord().setOffsetStartCenDirWRTStartDiskNo((p7.getZip64EndCentralDirRecord().getOffsetStartCenDirWRTStartDiskNo() + v1));
            }
            return;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("zip model is null, cannot update split Zip64 end of central directory record");
        }
    }

    private void updateSplitZipModel(net.lingala.zip4j.model.ZipModel p3, java.util.ArrayList p4, boolean p5)
    {
        if (p3 != null) {
            p3.setSplitArchive(0);
            this.updateSplitFileHeader(p3, p4, p5);
            this.updateSplitEndCentralDirectory(p3);
            if (p3.isZip64Format()) {
                this.updateSplitZip64EndCentralDirLocator(p3, p4);
                this.updateSplitZip64EndCentralDirRec(p3, p4);
            }
            return;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("zip model is null, cannot update split zip model");
        }
    }

    public void initProgressMonitorForMergeOp(net.lingala.zip4j.model.ZipModel p3, net.lingala.zip4j.progress.ProgressMonitor p4)
    {
        if (p3 != null) {
            p4.setCurrentOperation(4);
            p4.setFileName(p3.getZipFile());
            p4.setTotalWork(this.calculateTotalWorkForMergeOp(p3));
            p4.setState(1);
            return;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("zip model is null, cannot calculate total work for merge op");
        }
    }

    public void initProgressMonitorForRemoveOp(net.lingala.zip4j.model.ZipModel p3, net.lingala.zip4j.model.FileHeader p4, net.lingala.zip4j.progress.ProgressMonitor p5)
    {
        if ((p3 != null) && ((p4 != null) && (p5 != null))) {
            p5.setCurrentOperation(2);
            p5.setFileName(p4.getFileName());
            p5.setTotalWork(this.calculateTotalWorkForRemoveOp(p3, p4));
            p5.setState(1);
            return;
        } else {
            throw new net.lingala.zip4j.exception.ZipException("one of the input parameters is null, cannot calculate total work");
        }
    }

    public java.util.HashMap initRemoveZipFile(net.lingala.zip4j.model.ZipModel p42, net.lingala.zip4j.model.FileHeader p43, net.lingala.zip4j.progress.ProgressMonitor p44)
    {
        if ((p43 != null) && (p42 != null)) {
            java.io.File v39 = 0;
            java.io.RandomAccessFile v4 = 0;
            String v38 = 0;
            int v35_1 = new java.util.HashMap();
            try {
                int v26 = net.lingala.zip4j.util.Zip4jUtil.getIndexOfFileHeader(p42, p43);
            } catch (net.lingala.zip4j.exception.ZipException v3_81) {
                try {
                    if (v4 != null) {
                        v4.close();
                    }
                } catch (java.io.IOException v20) {
                    throw new net.lingala.zip4j.exception.ZipException("cannot close input stream or output stream when trying to delete a file from zip file");
                }
                if (net.lingala.zip4j.io.SplitOutputStream v5_1 != null) {
                    v5_1.close();
                }
                if (0 == 0) {
                    java.io.File v28_2 = new java.io.File;
                    v28_2(v38);
                    v28_2.delete();
                } else {
                    this.restoreFileName(v39, v38);
                }
                throw v3_81;
            } catch (java.io.IOException v20_0) {
                v5_1 = 0;
                p44.endProgressMonitorError(v20_0);
                throw new net.lingala.zip4j.exception.ZipException(v20_0);
            } catch (java.io.IOException v20_1) {
                v5_1 = 0;
                p44.endProgressMonitorError(v20_1);
                throw v20_1;
            } catch (net.lingala.zip4j.exception.ZipException v3_81) {
                v5_1 = 0;
            }
            if (v26 >= 0) {
                if (!p42.isSplitArchive()) {
                    v38 = new StringBuilder().append(p42.getZipFile()).append((System.currentTimeMillis() % 1000)).toString();
                    java.io.File v37_1 = new java.io.File(v38);
                    while (v37_1.exists()) {
                        v38 = new StringBuilder().append(p42.getZipFile()).append((System.currentTimeMillis() % 1000)).toString();
                        v37_1 = new java.io.File(v38);
                    }
                    try {
                        v5_1 = new net.lingala.zip4j.io.SplitOutputStream(new java.io.File(v38));
                    } catch (java.io.FileNotFoundException v21) {
                        throw new net.lingala.zip4j.exception.ZipException(v21);
                    }
                    try {
                        java.io.File v40 = new java.io.File;
                        v40(p42.getZipFile());
                    } catch (java.io.IOException v20_1) {
                    } catch (java.io.IOException v20_0) {
                    }
                    try {
                        v4 = this.createFileHandler(p42, "r");
                        net.lingala.zip4j.core.HeaderReader v23 = new net.lingala.zip4j.core.HeaderReader;
                        v23(v4);
                    } catch (java.io.IOException v20_1) {
                        v39 = v40;
                    }
                    if (v23.readLocalFileHeader(p43) != null) {
                        long v15 = p43.getOffsetLocalHeader();
                        if ((p43.getZip64ExtendedInfo() != null) && (p43.getZip64ExtendedInfo().getOffsetLocalHeader() != -1)) {
                            v15 = p43.getZip64ExtendedInfo().getOffsetLocalHeader();
                        }
                        long v30 = -1;
                        long v8 = p42.getEndCentralDirRecord().getOffsetOfStartOfCentralDir();
                        if ((p42.isZip64Format()) && (p42.getZip64EndCentralDirRecord() != null)) {
                            v8 = p42.getZip64EndCentralDirRecord().getOffsetStartCenDirWRTStartDiskNo();
                        }
                        java.util.ArrayList v22 = p42.getCentralDirectory().getFileHeaders();
                        if (v26 != (v22.size() - 1)) {
                            net.lingala.zip4j.model.FileHeader v29_1 = ((net.lingala.zip4j.model.FileHeader) v22.get((v26 + 1)));
                            if (v29_1 != null) {
                                v30 = (v29_1.getOffsetLocalHeader() - 1);
                                if ((v29_1.getZip64ExtendedInfo() != null) && (v29_1.getZip64ExtendedInfo().getOffsetLocalHeader() != -1)) {
                                    v30 = (v29_1.getZip64ExtendedInfo().getOffsetLocalHeader() - 1);
                                }
                            }
                        } else {
                            v30 = (v8 - 1);
                        }
                        if ((v15 >= 0) && (v30 >= 0)) {
                            if (v26 != 0) {
                                if (v26 != (v22.size() - 1)) {
                                    this.copyFile(v4, v5_1, 0, v15, p44);
                                    this.copyFile(v4, v5_1, (1 + v30), v8, p44);
                                } else {
                                    this.copyFile(v4, v5_1, 0, v15, p44);
                                }
                            } else {
                                if (p42.getCentralDirectory().getFileHeaders().size() > 1) {
                                    this.copyFile(v4, v5_1, (1 + v30), v8, p44);
                                }
                            }
                            if (!p44.isCancelAllTasks()) {
                                p42.getEndCentralDirRecord().setOffsetOfStartOfCentralDir(((net.lingala.zip4j.io.SplitOutputStream) v5_1).getFilePointer());
                                p42.getEndCentralDirRecord().setTotNoOfEntriesInCentralDir((p42.getEndCentralDirRecord().getTotNoOfEntriesInCentralDir() - 1));
                                p42.getEndCentralDirRecord().setTotNoOfEntriesInCentralDirOnThisDisk((p42.getEndCentralDirRecord().getTotNoOfEntriesInCentralDirOnThisDisk() - 1));
                                p42.getCentralDirectory().getFileHeaders().remove(v26);
                                int v25 = v26;
                                while (v25 < p42.getCentralDirectory().getFileHeaders().size()) {
                                    long v32 = ((net.lingala.zip4j.model.FileHeader) p42.getCentralDirectory().getFileHeaders().get(v25)).getOffsetLocalHeader();
                                    if ((((net.lingala.zip4j.model.FileHeader) p42.getCentralDirectory().getFileHeaders().get(v25)).getZip64ExtendedInfo() != null) && (((net.lingala.zip4j.model.FileHeader) p42.getCentralDirectory().getFileHeaders().get(v25)).getZip64ExtendedInfo().getOffsetLocalHeader() != -1)) {
                                        v32 = ((net.lingala.zip4j.model.FileHeader) p42.getCentralDirectory().getFileHeaders().get(v25)).getZip64ExtendedInfo().getOffsetLocalHeader();
                                    }
                                    ((net.lingala.zip4j.model.FileHeader) p42.getCentralDirectory().getFileHeaders().get(v25)).setOffsetLocalHeader(((v32 - (v30 - v15)) - 1));
                                    v25++;
                                }
                                new net.lingala.zip4j.core.HeaderWriter().finalizeZipFile(p42, v5_1);
                                v35_1.put("offsetCentralDir", Long.toString(p42.getEndCentralDirRecord().getOffsetOfStartOfCentralDir()));
                                try {
                                    if (v4 != null) {
                                        v4.close();
                                    }
                                } catch (java.io.IOException v20) {
                                    throw new net.lingala.zip4j.exception.ZipException("cannot close input stream or output stream when trying to delete a file from zip file");
                                }
                                if (v5_1 != null) {
                                    v5_1.close();
                                }
                                if (1 == 0) {
                                    java.io.File v28_0 = new java.io.File;
                                    v28_0(v38);
                                    v28_0.delete();
                                } else {
                                    this.restoreFileName(v40, v38);
                                }
                            } else {
                                p44.setResult(3);
                                p44.setState(0);
                                v35_1 = 0;
                                try {
                                    if (v4 != null) {
                                        v4.close();
                                    }
                                } catch (java.io.IOException v20) {
                                    throw new net.lingala.zip4j.exception.ZipException("cannot close input stream or output stream when trying to delete a file from zip file");
                                }
                                if (v5_1 != null) {
                                    v5_1.close();
                                }
                                if (0 == 0) {
                                    java.io.File v28_1 = new java.io.File;
                                    v28_1(v38);
                                    v28_1.delete();
                                } else {
                                    this.restoreFileName(v40, v38);
                                }
                            }
                            return v35_1;
                        } else {
                            throw new net.lingala.zip4j.exception.ZipException("invalid offset for start and end of local file, cannot remove file");
                        }
                    } else {
                        throw new net.lingala.zip4j.exception.ZipException("invalid local file header, cannot remove file from archive");
                    }
                } else {
                    throw new net.lingala.zip4j.exception.ZipException("This is a split archive. Zip file format does not allow updating split/spanned files");
                }
            } else {
                throw new net.lingala.zip4j.exception.ZipException("file header not found in zip model, cannot remove file");
            }
        } else {
            throw new net.lingala.zip4j.exception.ZipException("input parameters is null in maintain zip file, cannot remove file from archive");
        }
    }

    public void mergeSplitZipFiles(net.lingala.zip4j.model.ZipModel p7, java.io.File p8, net.lingala.zip4j.progress.ProgressMonitor p9, boolean p10)
    {
        if (!p10) {
            this.initMergeSplitZipFile(p7, p8, p9);
        } else {
            new net.lingala.zip4j.util.ArchiveMaintainer$2(this, "Zip4j", p7, p8, p9).start();
        }
        return;
    }

    public java.util.HashMap removeZipFile(net.lingala.zip4j.model.ZipModel p8, net.lingala.zip4j.model.FileHeader p9, net.lingala.zip4j.progress.ProgressMonitor p10, boolean p11)
    {
        java.util.HashMap v6;
        if (!p11) {
            v6 = this.initRemoveZipFile(p8, p9, p10);
            p10.endProgressMonitorSuccess();
        } else {
            new net.lingala.zip4j.util.ArchiveMaintainer$1(this, "Zip4j", p8, p9, p10).start();
            v6 = 0;
        }
        return v6;
    }

    public void setComment(net.lingala.zip4j.model.ZipModel p11, String p12)
    {
        if (p12 != null) {
            if (p11 != null) {
                String v3 = p12;
                byte[] v0 = p12.getBytes();
                int v1 = p12.length();
                if (net.lingala.zip4j.util.Zip4jUtil.isSupportedCharset("windows-1254")) {
                    try {
                        String v4_1 = new String(p12.getBytes("windows-1254"), "windows-1254");
                    } catch (java.io.IOException v2) {
                        v3 = p12;
                        v0 = p12.getBytes();
                        v1 = p12.length();
                    }
                    try {
                        v0 = v4_1.getBytes("windows-1254");
                        v1 = v4_1.length();
                        v3 = v4_1;
                    } catch (java.io.IOException v2) {
                    }
                }
                if (v1 <= 65535) {
                    p11.getEndCentralDirRecord().setComment(v3);
                    p11.getEndCentralDirRecord().setCommentBytes(v0);
                    p11.getEndCentralDirRecord().setCommentLength(v1);
                    net.lingala.zip4j.io.SplitOutputStream v6 = 0;
                    try {
                        net.lingala.zip4j.core.HeaderWriter v5_1 = new net.lingala.zip4j.core.HeaderWriter();
                        net.lingala.zip4j.io.SplitOutputStream v7_1 = new net.lingala.zip4j.io.SplitOutputStream(p11.getZipFile());
                        try {
                            if (!p11.isZip64Format()) {
                                v7_1.seek(p11.getEndCentralDirRecord().getOffsetOfStartOfCentralDir());
                            } else {
                                v7_1.seek(p11.getZip64EndCentralDirRecord().getOffsetStartCenDirWRTStartDiskNo());
                            }
                        } catch (java.io.IOException v2_0) {
                            v6 = v7_1;
                            throw new net.lingala.zip4j.exception.ZipException(v2_0);
                        } catch (java.io.IOException v2_1) {
                            v6 = v7_1;
                            throw new net.lingala.zip4j.exception.ZipException(v2_1);
                        } catch (java.io.IOException v8_19) {
                            v6 = v7_1;
                            if (v6 != null) {
                                try {
                                    v6.close();
                                } catch (java.io.IOException v9) {
                                }
                            }
                            throw v8_19;
                        }
                        v5_1.finalizeZipFileWithoutValidations(p11, v7_1);
                        if (v7_1 != null) {
                            try {
                                v7_1.close();
                            } catch (java.io.IOException v8) {
                            }
                        }
                        return;
                    } catch (java.io.IOException v2_0) {
                    } catch (java.io.IOException v2_1) {
                    } catch (java.io.IOException v8_19) {
                    }
                } else {
                    throw new net.lingala.zip4j.exception.ZipException("comment length exceeds maximum length");
                }
            } else {
                throw new net.lingala.zip4j.exception.ZipException("zipModel is null, cannot update Zip file with comment");
            }
        } else {
            throw new net.lingala.zip4j.exception.ZipException("comment is null, cannot update Zip file with comment");
        }
    }
}
