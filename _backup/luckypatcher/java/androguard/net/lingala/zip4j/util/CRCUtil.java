package net.lingala.zip4j.util;
public class CRCUtil {
    private static final int BUF_SIZE = 16384;

    public CRCUtil()
    {
        return;
    }

    public static long computeFileCRC(String p2)
    {
        return net.lingala.zip4j.util.CRCUtil.computeFileCRC(p2, 0);
    }

    public static long computeFileCRC(String p8, net.lingala.zip4j.progress.ProgressMonitor p9)
    {
        if (net.lingala.zip4j.util.Zip4jUtil.isStringNotNullAndNotEmpty(p8)) {
            java.io.FileInputStream v3 = 0;
            try {
                net.lingala.zip4j.util.Zip4jUtil.checkFileReadAccess(p8);
                java.io.FileInputStream v4_1 = new java.io.FileInputStream(new java.io.File(p8));
                try {
                    byte[] v0 = new byte[16384];
                    java.util.zip.CRC32 v1_1 = new java.util.zip.CRC32();
                } catch (java.io.IOException v2_0) {
                    v3 = v4_1;
                    throw new net.lingala.zip4j.exception.ZipException(v2_0);
                } catch (java.io.IOException v2_1) {
                    v3 = v4_1;
                    throw new net.lingala.zip4j.exception.ZipException(v2_1);
                } catch (net.lingala.zip4j.exception.ZipException v6_15) {
                    v3 = v4_1;
                    if (v3 != null) {
                        try {
                            v3.close();
                        } catch (java.io.IOException v2) {
                            throw new net.lingala.zip4j.exception.ZipException("error while closing the file after calculating crc");
                        }
                    }
                    throw v6_15;
                }
                do {
                    net.lingala.zip4j.exception.ZipException v6_5;
                    int v5 = v4_1.read(v0);
                    if (v5 == -1) {
                        v6_5 = v1_1.getValue();
                        if (v4_1 != null) {
                            try {
                                v4_1.close();
                            } catch (java.io.IOException v2) {
                                throw new net.lingala.zip4j.exception.ZipException("error while closing the file after calculating crc");
                            }
                        }
                    } else {
                        v1_1.update(v0, 0, v5);
                        if (p9 != null) {
                            p9.updateWorkCompleted(((long) v5));
                        }
                    }
                    return v6_5;
                } while(!p9.isCancelAllTasks());
                p9.setResult(3);
                p9.setState(0);
                v6_5 = 0;
                if (v4_1 != null) {
                    try {
                        v4_1.close();
                    } catch (java.io.IOException v2) {
                        throw new net.lingala.zip4j.exception.ZipException("error while closing the file after calculating crc");
                    }
                }
                return v6_5;
            } catch (net.lingala.zip4j.exception.ZipException v6_15) {
            } catch (java.io.IOException v2_0) {
            } catch (java.io.IOException v2_1) {
            }
        } else {
            throw new net.lingala.zip4j.exception.ZipException("input file is null or empty, cannot calculate CRC for the file");
        }
    }
}
