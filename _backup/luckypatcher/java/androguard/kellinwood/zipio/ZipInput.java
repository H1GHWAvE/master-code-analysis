package kellinwood.zipio;
public class ZipInput {
    static kellinwood.logging.LoggerInterface log;
    kellinwood.zipio.CentralEnd centralEnd;
    long fileLength;
    java.io.RandomAccessFile in;
    public String inputFilename;
    java.util.jar.Manifest manifest;
    int scanIterations;
    java.util.Map zioEntries;

    public ZipInput(String p4)
    {
        this.in = 0;
        this.scanIterations = 0;
        this.zioEntries = new java.util.LinkedHashMap();
        this.inputFilename = p4;
        this.in = new java.io.RandomAccessFile(new java.io.File(this.inputFilename), "r");
        this.fileLength = this.in.length();
        return;
    }

    private void doRead()
    {
        try {
            this.in.seek(this.scanForEOCDR(256));
            this.centralEnd = kellinwood.zipio.CentralEnd.read(this);
            boolean v0 = kellinwood.zipio.ZipInput.getLogger().isDebugEnabled();
        } catch (Throwable v5) {
            v5.printStackTrace();
            return;
        }
        if (v0) {
            kellinwood.logging.LoggerInterface v6_4 = kellinwood.zipio.ZipInput.getLogger();
            Object[] v8_1 = new Object[1];
            v8_1[0] = Integer.valueOf(this.scanIterations);
            v6_4.debug(String.format("EOCD found in %d iterations", v8_1));
            kellinwood.logging.LoggerInterface v6_5 = kellinwood.zipio.ZipInput.getLogger();
            Object[] v8_3 = new Object[4];
            v8_3[0] = Short.valueOf(this.centralEnd.totalCentralEntries);
            v8_3[1] = Integer.valueOf(this.centralEnd.centralDirectorySize);
            v8_3[2] = Integer.valueOf(this.centralEnd.centralStartOffset);
            v8_3[3] = Integer.valueOf(this.centralEnd.centralStartOffset);
            v6_5.debug(String.format("Directory entries=%d, size=%d, offset=%d/0x%08x", v8_3));
            kellinwood.zipio.ZipListingHelper.listHeader(kellinwood.zipio.ZipInput.getLogger());
        }
        this.in.seek(((long) this.centralEnd.centralStartOffset));
        int v2 = 0;
        while (v2 < this.centralEnd.totalCentralEntries) {
            kellinwood.zipio.ZioEntry v1 = kellinwood.zipio.ZioEntry.read(this);
            this.zioEntries.put(v1.getName(), v1);
            if (v0) {
                kellinwood.zipio.ZipListingHelper.listEntry(kellinwood.zipio.ZipInput.getLogger(), v1);
            }
            v2++;
        }
        return;
    }

    private static kellinwood.logging.LoggerInterface getLogger()
    {
        if (kellinwood.zipio.ZipInput.log == null) {
            kellinwood.zipio.ZipInput.log = kellinwood.logging.LoggerManager.getLogger(kellinwood.zipio.ZipInput.getName());
        }
        return kellinwood.zipio.ZipInput.log;
    }

    public static kellinwood.zipio.ZipInput read(String p1)
    {
        return new kellinwood.zipio.ZipInput(p1).doRead();
    }

    public void close()
    {
        if (this.in != null) {
            try {
                this.in.close();
            } catch (Throwable v0) {
            }
        }
        return;
    }

    public java.util.Map getEntries()
    {
        return this.zioEntries;
    }

    public kellinwood.zipio.ZioEntry getEntry(String p2)
    {
        return ((kellinwood.zipio.ZioEntry) this.zioEntries.get(p2));
    }

    public long getFileLength()
    {
        return this.fileLength;
    }

    public long getFilePointer()
    {
        return this.in.getFilePointer();
    }

    public String getFilename()
    {
        return this.inputFilename;
    }

    public java.util.jar.Manifest getManifest()
    {
        if (this.manifest == null) {
            kellinwood.zipio.ZioEntry v0_1 = ((kellinwood.zipio.ZioEntry) this.zioEntries.get("META-INF/MANIFEST.MF"));
            if (v0_1 != null) {
                this.manifest = new java.util.jar.Manifest(v0_1.getInputStream());
            }
        }
        return this.manifest;
    }

    public java.util.Collection list(String p10)
    {
        if (p10.endsWith("/")) {
            if (p10.startsWith("/")) {
                p10 = p10.substring(1);
            }
            Object[] v6_0 = new Object[1];
            v6_0[0] = p10;
            java.util.regex.Pattern v4 = java.util.regex.Pattern.compile(String.format("^%s([^/]+/?).*", v6_0));
            java.util.TreeSet v3_1 = new java.util.TreeSet();
            java.util.Iterator v0 = this.zioEntries.keySet().iterator();
            while (v0.hasNext()) {
                java.util.regex.Matcher v1 = v4.matcher(((String) v0.next()));
                if (v1.matches()) {
                    v3_1.add(v1.group(1));
                }
            }
            return v3_1;
        } else {
            throw new IllegalArgumentException("Invalid path -- does not end with \'/\'");
        }
    }

    public int read(byte[] p2, int p3, int p4)
    {
        return this.in.read(p2, p3, p4);
    }

    public byte readByte()
    {
        return this.in.readByte();
    }

    public byte[] readBytes(int p4)
    {
        byte[] v0 = new byte[p4];
        int v1 = 0;
        while (v1 < p4) {
            v0[v1] = this.in.readByte();
            v1++;
        }
        return v0;
    }

    public int readInt()
    {
        int v1 = 0;
        int v0 = 0;
        while (v0 < 4) {
            v1 |= (this.in.readUnsignedByte() << (v0 * 8));
            v0++;
        }
        return v1;
    }

    public short readShort()
    {
        short v1 = 0;
        int v0 = 0;
        while (v0 < 2) {
            v1 = ((short) ((this.in.readUnsignedByte() << (v0 * 8)) | v1));
            v0++;
        }
        return v1;
    }

    public String readString(int p4)
    {
        byte[] v0 = new byte[p4];
        int v1 = 0;
        while (v1 < p4) {
            v0[v1] = this.in.readByte();
            v1++;
        }
        return new String(v0);
    }

    public long scanForEOCDR(int p9)
    {
        if ((((long) p9) <= this.fileLength) && (p9 <= 65536)) {
            int v2 = ((int) Math.min(this.fileLength, ((long) p9)));
            byte[] v1 = new byte[v2];
            this.in.seek((this.fileLength - ((long) v2)));
            this.in.readFully(v1);
            int v0 = (v2 - 22);
            while (v0 >= 0) {
                this.scanIterations = (this.scanIterations + 1);
                if ((v1[v0] != 80) || ((v1[(v0 + 1)] != 75) || ((v1[(v0 + 2)] != 5) || (v1[(v0 + 3)] != 6)))) {
                    v0--;
                } else {
                    long v3_8 = ((this.fileLength - ((long) v2)) + ((long) v0));
                }
                return v3_8;
            }
            v3_8 = this.scanForEOCDR((p9 * 2));
            return v3_8;
        } else {
            throw new IllegalStateException(new StringBuilder().append("End of central directory not found in ").append(this.inputFilename).toString());
        }
    }

    public void seek(long p2)
    {
        this.in.seek(p2);
        return;
    }
}
