package kellinwood.zipio;
public class ZipListingHelper {
    static java.text.DateFormat dateFormat;

    static ZipListingHelper()
    {
        kellinwood.zipio.ZipListingHelper.dateFormat = new java.text.SimpleDateFormat("MM-dd-yy HH:mm");
        return;
    }

    public ZipListingHelper()
    {
        return;
    }

    public static void listEntry(kellinwood.logging.LoggerInterface p8, kellinwood.zipio.ZioEntry p9)
    {
        int v0 = 0;
        if (p9.getSize() > 0) {
            v0 = (((p9.getSize() - p9.getCompressedSize()) * 100) / p9.getSize());
        }
        String v1_7;
        Object[] v3 = new Object[7];
        v3[0] = Integer.valueOf(p9.getSize());
        if (p9.getCompression() != 0) {
            v1_7 = "Defl:N";
        } else {
            v1_7 = "Stored";
        }
        v3[1] = v1_7;
        v3[2] = Integer.valueOf(p9.getCompressedSize());
        v3[3] = Integer.valueOf(v0);
        v3[4] = kellinwood.zipio.ZipListingHelper.dateFormat.format(new java.util.Date(p9.getTime()));
        v3[5] = Integer.valueOf(p9.getCrc32());
        v3[6] = p9.getName();
        p8.debug(String.format("%8d  %6s %8d %4d%% %s  %08x  %s", v3));
        return;
    }

    public static void listHeader(kellinwood.logging.LoggerInterface p1)
    {
        p1.debug(" Length   Method    Size  Ratio   Date   Time   CRC-32    Name");
        p1.debug("--------  ------  ------- -----   ----   ----   ------    ----");
        return;
    }
}
