package kellinwood.security.zipsigner;
public interface ResourceAdapter {

    public abstract varargs String getString();
}
