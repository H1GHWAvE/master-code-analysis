package kellinwood.security.zipsigner;
public class ZipSigner {
    private static final String CERT_RSA_NAME = "META-INF/CERT.RSA";
    private static final String CERT_SF_NAME = "META-INF/CERT.SF";
    public static final String KEY_NONE = "none";
    public static final String KEY_TESTKEY = "testkey";
    public static final String MODE_AUTO = "auto";
    public static final String MODE_AUTO_NONE = "auto-none";
    public static final String MODE_AUTO_TESTKEY = "auto-testkey";
    public static final String[] SUPPORTED_KEY_MODES;
    static kellinwood.logging.LoggerInterface log;
    private static java.util.regex.Pattern stripPattern;
    java.util.Map autoKeyDetect;
    kellinwood.security.zipsigner.ZipSigner$AutoKeyObservable autoKeyObservable;
    private boolean canceled;
    kellinwood.security.zipsigner.KeySet keySet;
    String keymode;
    java.util.Map loadedKeys;
    private kellinwood.security.zipsigner.ProgressHelper progressHelper;
    private kellinwood.security.zipsigner.ResourceAdapter resourceAdapter;

    static ZipSigner()
    {
        kellinwood.security.zipsigner.ZipSigner.log = 0;
        kellinwood.security.zipsigner.ZipSigner.stripPattern = java.util.regex.Pattern.compile("^META-INF/(.*)[.](SF|RSA|DSA)$");
        String[] v0_4 = new String[8];
        v0_4[0] = "auto-testkey";
        v0_4[1] = "auto";
        v0_4[2] = "auto-none";
        v0_4[3] = "media";
        v0_4[4] = "platform";
        v0_4[5] = "shared";
        v0_4[6] = "testkey";
        v0_4[7] = "none";
        kellinwood.security.zipsigner.ZipSigner.SUPPORTED_KEY_MODES = v0_4;
        return;
    }

    public ZipSigner()
    {
        this.canceled = 0;
        this.progressHelper = new kellinwood.security.zipsigner.ProgressHelper();
        this.resourceAdapter = new kellinwood.security.zipsigner.DefaultResourceAdapter();
        this.loadedKeys = new java.util.HashMap();
        this.keySet = 0;
        this.keymode = "testkey";
        this.autoKeyDetect = new java.util.HashMap();
        this.autoKeyObservable = new kellinwood.security.zipsigner.ZipSigner$AutoKeyObservable();
        this.autoKeyDetect.put("aa9852bc5a53272ac8031d49b65e4b0e", "media");
        this.autoKeyDetect.put("e60418c4b638f20d0721e115674ca11f", "platform");
        this.autoKeyDetect.put("3e24e49741b60c215c010dc6048fca7d", "shared");
        this.autoKeyDetect.put("dab2cead827ef5313f28e22b6fa8479f", "testkey");
        return;
    }

    private java.util.jar.Manifest addDigestsToManifest(java.util.Map p23)
    {
        java.util.jar.Manifest v10_0 = 0;
        kellinwood.zipio.ZioEntry v12_1 = ((kellinwood.zipio.ZioEntry) p23.get("META-INF/MANIFEST.MF"));
        if (v12_1 != null) {
            v10_0 = new java.util.jar.Manifest();
            v10_0.read(v12_1.getInputStream());
        }
        java.util.jar.Manifest v16_1 = new java.util.jar.Manifest();
        java.util.jar.Attributes v11 = v16_1.getMainAttributes();
        if (v10_0 == null) {
            v11.putValue("Manifest-Version", "1.0");
            v11.putValue("Created-By", "1.0 (Android SignApk)");
        } else {
            v11.putAll(v10_0.getMainAttributes());
        }
        java.security.MessageDigest v13 = java.security.MessageDigest.getInstance("SHA1");
        byte[] v3 = new byte[512];
        java.util.TreeMap v4_1 = new java.util.TreeMap();
        v4_1.putAll(p23);
        boolean v6 = kellinwood.security.zipsigner.ZipSigner.getLogger().isDebugEnabled();
        if (v6) {
            kellinwood.security.zipsigner.ZipSigner.getLogger().debug("Manifest entries:");
        }
        java.util.Iterator v8 = v4_1.values().iterator();
        while (v8.hasNext()) {
            kellinwood.zipio.ZioEntry v7_1 = ((kellinwood.zipio.ZioEntry) v8.next());
            if (this.canceled) {
                break;
            }
            String v14 = v7_1.getName();
            if (v6) {
                kellinwood.security.zipsigner.ZipSigner.getLogger().debug(v14);
            }
            if ((!v7_1.isDirectory()) && ((!v14.equals("META-INF/MANIFEST.MF")) && ((!v14.equals("META-INF/CERT.SF")) && ((!v14.equals("META-INF/CERT.RSA")) && ((kellinwood.security.zipsigner.ZipSigner.stripPattern == null) || (!kellinwood.security.zipsigner.ZipSigner.stripPattern.matcher(v14).matches())))))) {
                java.util.Map v0_19 = new Object[0];
                this.progressHelper.progress(0, this.resourceAdapter.getString(kellinwood.security.zipsigner.ResourceAdapter$Item.GENERATING_MANIFEST, v0_19));
                java.io.InputStream v5 = v7_1.getInputStream();
                while(true) {
                    int v15 = v5.read(v3);
                    if (v15 <= 0) {
                        break;
                    }
                    v13.update(v3, 0, v15);
                }
                java.util.jar.Attributes v2_0 = 0;
                if (v10_0 != null) {
                    java.util.jar.Attributes v9 = v10_0.getAttributes(v14);
                    if (v9 != null) {
                        v2_0 = new java.util.jar.Attributes(v9);
                    }
                }
                if (v2_0 == null) {
                    v2_0 = new java.util.jar.Attributes();
                }
                v2_0.putValue("SHA1-Digest", kellinwood.security.zipsigner.Base64.encode(v13.digest()));
                v16_1.getEntries().put(v14, v2_0);
            }
        }
        return v16_1;
    }

    private void copyFiles(java.util.Map p11, kellinwood.zipio.ZipOutput p12)
    {
        int v0 = 1;
        java.util.Iterator v1 = p11.values().iterator();
        while (v1.hasNext()) {
            kellinwood.zipio.ZioEntry v2_1 = ((kellinwood.zipio.ZioEntry) v1.next());
            if (this.canceled) {
                break;
            }
            kellinwood.security.zipsigner.ProgressHelper v3_3 = this.progressHelper;
            String v4_0 = this.resourceAdapter;
            Object[] v6_1 = new Object[2];
            v6_1[0] = Integer.valueOf(v0);
            v6_1[1] = Integer.valueOf(p11.size());
            v3_3.progress(0, v4_0.getString(kellinwood.security.zipsigner.ResourceAdapter$Item.COPYING_ZIP_ENTRY, v6_1));
            v0++;
            p12.write(v2_1);
        }
        return;
    }

    private void copyFiles(java.util.jar.Manifest p16, java.util.Map p17, kellinwood.zipio.ZipOutput p18, long p19)
    {
        java.util.ArrayList v7_1 = new java.util.ArrayList(p16.getEntries().keySet());
        java.util.Collections.sort(v7_1);
        int v3 = 1;
        java.util.Iterator v4 = v7_1.iterator();
        while (v4.hasNext()) {
            String v6_1 = ((String) v4.next());
            if (this.canceled) {
                break;
            }
            kellinwood.security.zipsigner.ProgressHelper v8_3 = this.progressHelper;
            String v10_0 = this.resourceAdapter;
            Object[] v12_1 = new Object[2];
            v12_1[0] = Integer.valueOf(v3);
            v12_1[1] = Integer.valueOf(v7_1.size());
            v8_3.progress(0, v10_0.getString(kellinwood.security.zipsigner.ResourceAdapter$Item.COPYING_ZIP_ENTRY, v12_1));
            v3++;
            kellinwood.zipio.ZioEntry v5_1 = ((kellinwood.zipio.ZioEntry) p17.get(v6_1));
            v5_1.setTime(p19);
            p18.write(v5_1);
        }
        return;
    }

    private java.security.spec.KeySpec decryptPrivateKey(byte[] p9, String p10)
    {
        try {
            javax.crypto.EncryptedPrivateKeyInfo v1_1 = new javax.crypto.EncryptedPrivateKeyInfo(p9);
            javax.crypto.SecretKey v3 = javax.crypto.SecretKeyFactory.getInstance(v1_1.getAlgName()).generateSecret(new javax.crypto.spec.PBEKeySpec(p10.toCharArray()));
            javax.crypto.Cipher v0 = javax.crypto.Cipher.getInstance(v1_1.getAlgName());
            v0.init(2, v3, v1_1.getAlgParameters());
            try {
                kellinwood.logging.LoggerInterface v6_5 = v1_1.getKeySpec(v0);
            } catch (java.security.spec.InvalidKeySpecException v2) {
                kellinwood.security.zipsigner.ZipSigner.getLogger().error("signapk: Password for private key may be bad.");
                throw v2;
            }
            return v6_5;
        } catch (java.security.spec.InvalidKeySpecException v2) {
            v6_5 = 0;
            return v6_5;
        }
    }

    private void generateSignatureFile(java.util.jar.Manifest p14, java.io.OutputStream p15)
    {
        p15.write("Signature-Version: 1.0\r\n".getBytes());
        p15.write("Created-By: 1.0 (Android SignApk)\r\n".getBytes());
        java.security.MessageDigest v5 = java.security.MessageDigest.getInstance("SHA1");
        java.io.PrintStream v7_1 = new java.io.PrintStream(new java.security.DigestOutputStream(new java.io.ByteArrayOutputStream(), v5), 1, "UTF-8");
        p14.write(v7_1);
        v7_1.flush();
        p15.write(new StringBuilder().append("SHA1-Digest-Manifest: ").append(kellinwood.security.zipsigner.Base64.encode(v5.digest())).append("\r\n\r\n").toString().getBytes());
        java.util.Iterator v3 = p14.getEntries().entrySet().iterator();
        while (v3.hasNext()) {
            java.util.Map$Entry v2_1 = ((java.util.Map$Entry) v3.next());
            if (this.canceled) {
                break;
            }
            Object[] v11 = new Object[0];
            this.progressHelper.progress(0, this.resourceAdapter.getString(kellinwood.security.zipsigner.ResourceAdapter$Item.GENERATING_SIGNATURE_FILE, v11));
            String v6 = new StringBuilder().append("Name: ").append(((String) v2_1.getKey())).append("\r\n").toString();
            v7_1.print(v6);
            java.util.Iterator v4 = ((java.util.jar.Attributes) v2_1.getValue()).entrySet().iterator();
            while (v4.hasNext()) {
                java.util.Map$Entry v0_1 = ((java.util.Map$Entry) v4.next());
                v7_1.print(new StringBuilder().append(v0_1.getKey()).append(": ").append(v0_1.getValue()).append("\r\n").toString());
            }
            v7_1.print("\r\n");
            v7_1.flush();
            p15.write(v6.getBytes());
            p15.write(new StringBuilder().append("SHA1-Digest: ").append(kellinwood.security.zipsigner.Base64.encode(v5.digest())).append("\r\n\r\n").toString().getBytes());
        }
        return;
    }

    public static kellinwood.logging.LoggerInterface getLogger()
    {
        if (kellinwood.security.zipsigner.ZipSigner.log == null) {
            kellinwood.security.zipsigner.ZipSigner.log = kellinwood.logging.LoggerManager.getLogger(kellinwood.security.zipsigner.ZipSigner.getName());
        }
        return kellinwood.security.zipsigner.ZipSigner.log;
    }

    public static String[] getSupportedKeyModes()
    {
        return kellinwood.security.zipsigner.ZipSigner.SUPPORTED_KEY_MODES;
    }

    private void writeSignatureBlock(kellinwood.security.zipsigner.KeySet p16, byte[] p17, java.io.OutputStream p18)
    {
        if (p16.getSigBlockTemplate() == null) {
            try {
                Class v2 = Class.forName("kellinwood.security.zipsigner.optional.SignatureBlockGenerator");
                String v12_1 = new Class[2];
                v12_1[0] = kellinwood.security.zipsigner.KeySet;
                Class v14_2 = new byte[1];
                v12_1[1] = v14_2.getClass();
                reflect.Method v3 = v2.getMethod("generate", v12_1);
                String v12_3 = new Object[2];
                v12_3[0] = p16;
                v12_3[1] = p17;
                p18.write(((byte[]) ((byte[]) v3.invoke(0, v12_3))));
            } catch (Exception v10) {
                throw new RuntimeException(v10.getMessage(), v10);
            }
        } else {
            kellinwood.security.zipsigner.ZipSignature v7_1 = new kellinwood.security.zipsigner.ZipSignature();
            v7_1.initSign(p16.getPrivateKey());
            v7_1.update(p17);
            byte[] v8 = v7_1.sign();
            p18.write(p16.getSigBlockTemplate());
            p18.write(v8);
            if (kellinwood.security.zipsigner.ZipSigner.getLogger().isDebugEnabled()) {
                java.security.MessageDigest v4 = java.security.MessageDigest.getInstance("SHA1");
                v4.update(p17);
                kellinwood.security.zipsigner.ZipSigner.getLogger().debug(new StringBuilder().append("Sig File SHA1: \n").append(kellinwood.security.zipsigner.HexDumpEncoder.encode(v4.digest())).toString());
                kellinwood.security.zipsigner.ZipSigner.getLogger().debug(new StringBuilder().append("Signature: \n").append(kellinwood.security.zipsigner.HexDumpEncoder.encode(v8)).toString());
                javax.crypto.Cipher v1 = javax.crypto.Cipher.getInstance("RSA/ECB/PKCS1Padding");
                v1.init(2, p16.getPublicKey());
                kellinwood.security.zipsigner.ZipSigner.getLogger().debug(new StringBuilder().append("Signature Decrypted: \n").append(kellinwood.security.zipsigner.HexDumpEncoder.encode(v1.doFinal(v8))).toString());
            }
        }
        return;
    }

    public void addAutoKeyObserver(java.util.Observer p2)
    {
        this.autoKeyObservable.addObserver(p2);
        return;
    }

    public void addProgressListener(kellinwood.security.zipsigner.ProgressListener p2)
    {
        this.progressHelper.addProgressListener(p2);
        return;
    }

    protected String autoDetectKey(String p20, java.util.Map p21)
    {
        boolean v4 = kellinwood.security.zipsigner.ZipSigner.getLogger().isDebugEnabled();
        if (p20.startsWith("auto")) {
            String v10_0 = 0;
            java.util.Iterator v8 = p21.entrySet().iterator();
            while (v8.hasNext()) {
                java.util.Map$Entry v5_1 = ((java.util.Map$Entry) v8.next());
                String v7_1 = ((String) v5_1.getKey());
                if ((v7_1.startsWith("META-INF/")) && (v7_1.endsWith(".RSA"))) {
                    java.security.MessageDigest v12 = java.security.MessageDigest.getInstance("MD5");
                    byte[] v6 = ((kellinwood.zipio.ZioEntry) v5_1.getValue()).getData();
                    if (v6.length < 1458) {
                        break;
                    }
                    v12.update(v6, 0, 1458);
                    byte[] v14 = v12.digest();
                    StringBuilder v3_1 = new StringBuilder();
                    byte[] v1 = v14;
                    int v11 = v1.length;
                    int v9 = 0;
                    while (v9 < v11) {
                        Object[] v0_9 = new Object[1];
                        String v16_7 = v0_9;
                        v16_7[0] = Byte.valueOf(v1[v9]);
                        v3_1.append(String.format("%02x", v16_7));
                        v9++;
                    }
                    String v13 = v3_1.toString();
                    v10_0 = ((String) this.autoKeyDetect.get(v13));
                    if (v4) {
                        if (v10_0 == null) {
                            kellinwood.logging.LoggerInterface v15_15 = kellinwood.security.zipsigner.ZipSigner.getLogger();
                            Object[] v0_5 = new Object[1];
                            Object[] v17_1 = v0_5;
                            v17_1[0] = v13;
                            v15_15.debug(String.format("Auto key determination failed for md5=%s", v17_1));
                        } else {
                            kellinwood.logging.LoggerInterface v15_16 = kellinwood.security.zipsigner.ZipSigner.getLogger();
                            Object[] v0_7 = new Object[2];
                            Object[] v17_3 = v0_7;
                            v17_3[0] = v10_0;
                            v17_3[1] = v13;
                            v15_16.debug(String.format("Auto-determined key=%s using md5=%s", v17_3));
                        }
                    }
                    if (v10_0 != null) {
                        p20 = v10_0;
                        return p20;
                    }
                }
            }
            if (!p20.equals("auto-testkey")) {
                if (!p20.equals("auto-none")) {
                    p20 = 0;
                } else {
                    if (v4) {
                        kellinwood.security.zipsigner.ZipSigner.getLogger().debug("Unable to determine key, returning: none");
                    }
                    p20 = "none";
                }
            } else {
                if (v4) {
                    kellinwood.security.zipsigner.ZipSigner.getLogger().debug(new StringBuilder().append("Falling back to key=").append(v10_0).toString());
                }
                p20 = "testkey";
            }
        }
        return p20;
    }

    public void cancel()
    {
        this.canceled = 1;
        return;
    }

    public kellinwood.security.zipsigner.KeySet getKeySet()
    {
        return this.keySet;
    }

    public String getKeymode()
    {
        return this.keymode;
    }

    public kellinwood.security.zipsigner.ResourceAdapter getResourceAdapter()
    {
        return this.resourceAdapter;
    }

    public boolean isCanceled()
    {
        return this.canceled;
    }

    public void issueLoadingCertAndKeysProgressEvent()
    {
        Object[] v4_1 = new Object[0];
        this.progressHelper.progress(1, this.resourceAdapter.getString(kellinwood.security.zipsigner.ResourceAdapter$Item.LOADING_CERTIFICATE_AND_KEY, v4_1));
        return;
    }

    public void loadKeys(String p7)
    {
        this.keySet = ((kellinwood.security.zipsigner.KeySet) this.loadedKeys.get(p7));
        if (this.keySet == null) {
            this.keySet = new kellinwood.security.zipsigner.KeySet();
            this.keySet.setName(p7);
            this.loadedKeys.put(p7, this.keySet);
            if (!"none".equals(p7)) {
                this.issueLoadingCertAndKeysProgressEvent();
                this.keySet.setPrivateKey(this.readPrivateKey(this.getClass().getResource(new StringBuilder().append("/keys/").append(p7).append(".pk8").toString()), 0));
                this.keySet.setPublicKey(this.readPublicKey(this.getClass().getResource(new StringBuilder().append("/keys/").append(p7).append(".x509.pem").toString())));
                java.net.URL v2 = this.getClass().getResource(new StringBuilder().append("/keys/").append(p7).append(".sbt").toString());
                if (v2 != null) {
                    this.keySet.setSigBlockTemplate(this.readContentAsBytes(v2));
                }
            }
        }
        return;
    }

    public void loadProvider(String p4)
    {
        java.security.Security.insertProviderAt(((java.security.Provider) Class.forName(p4).newInstance()), 1);
        return;
    }

    public byte[] readContentAsBytes(java.io.InputStream p6)
    {
        java.io.ByteArrayOutputStream v0_1 = new java.io.ByteArrayOutputStream();
        byte[] v1 = new byte[2048];
        int v3 = p6.read(v1);
        while (v3 != -1) {
            v0_1.write(v1, 0, v3);
            v3 = p6.read(v1);
        }
        return v0_1.toByteArray();
    }

    public byte[] readContentAsBytes(java.net.URL p2)
    {
        return this.readContentAsBytes(p2.openStream());
    }

    public java.security.PrivateKey readPrivateKey(java.net.URL p6, String p7)
    {
        java.io.DataInputStream v2_1 = new java.io.DataInputStream(p6.openStream());
        try {
            byte[] v0 = this.readContentAsBytes(v2_1);
            java.security.spec.PKCS8EncodedKeySpec v3_0 = this.decryptPrivateKey(v0, p7);
        } catch (java.security.PrivateKey v4_6) {
            v2_1.close();
            throw v4_6;
        }
        if (v3_0 == null) {
            v3_0 = new java.security.spec.PKCS8EncodedKeySpec(v0);
        }
        try {
            java.security.PrivateKey v4_3 = java.security.KeyFactory.getInstance("RSA").generatePrivate(v3_0);
            v2_1.close();
        } catch (java.security.spec.InvalidKeySpecException v1) {
            v4_3 = java.security.KeyFactory.getInstance("DSA").generatePrivate(v3_0);
            v2_1.close();
        }
        return v4_3;
    }

    public java.security.cert.X509Certificate readPublicKey(java.net.URL p4)
    {
        java.io.InputStream v1 = p4.openStream();
        try {
            Throwable v2_2 = ((java.security.cert.X509Certificate) java.security.cert.CertificateFactory.getInstance("X.509").generateCertificate(v1));
            v1.close();
            return v2_2;
        } catch (Throwable v2_3) {
            v1.close();
            throw v2_3;
        }
    }

    public declared_synchronized void removeProgressListener(kellinwood.security.zipsigner.ProgressListener p2)
    {
        try {
            this.progressHelper.removeProgressListener(p2);
            return;
        } catch (Throwable v0_1) {
            throw v0_1;
        }
    }

    public void resetCanceled()
    {
        this.canceled = 0;
        return;
    }

    public void setKeymode(String p4)
    {
        if (kellinwood.security.zipsigner.ZipSigner.getLogger().isDebugEnabled()) {
            kellinwood.security.zipsigner.ZipSigner.getLogger().debug(new StringBuilder().append("setKeymode: ").append(p4).toString());
        }
        this.keymode = p4;
        if (!this.keymode.startsWith("auto")) {
            this.progressHelper.initProgress();
            this.loadKeys(this.keymode);
        } else {
            this.keySet = 0;
        }
        return;
    }

    public void setKeys(String p7, java.security.cert.X509Certificate p8, java.security.PrivateKey p9, String p10, byte[] p11)
    {
        this.keySet = new kellinwood.security.zipsigner.KeySet(p7, p8, p9, p10, p11);
        return;
    }

    public void setKeys(String p2, java.security.cert.X509Certificate p3, java.security.PrivateKey p4, byte[] p5)
    {
        this.keySet = new kellinwood.security.zipsigner.KeySet(p2, p3, p4, p5);
        return;
    }

    public void setResourceAdapter(kellinwood.security.zipsigner.ResourceAdapter p1)
    {
        this.resourceAdapter = p1;
        return;
    }

    public void signZip(String p9, String p10)
    {
        if (!new java.io.File(p9).getCanonicalFile().equals(new java.io.File(p10).getCanonicalFile())) {
            this.progressHelper.initProgress();
            Object[] v7_1 = new Object[0];
            this.progressHelper.progress(1, this.resourceAdapter.getString(kellinwood.security.zipsigner.ResourceAdapter$Item.PARSING_CENTRAL_DIRECTORY, v7_1));
            this.signZip(kellinwood.zipio.ZipInput.read(p9).getEntries(), new java.io.FileOutputStream(p10), p10);
            return;
        } else {
            kellinwood.security.zipsigner.ResourceAdapter$Item v6_1 = new Object[0];
            throw new IllegalArgumentException(this.resourceAdapter.getString(kellinwood.security.zipsigner.ResourceAdapter$Item.INPUT_SAME_AS_OUTPUT_ERROR, v6_1));
        }
    }

    public void signZip(java.net.URL p10, String p11, String p12, String p13, String p14, String p15, String p16)
    {
        this.signZip(p10, p11, p12.toCharArray(), p13, p14.toCharArray(), "SHA1withRSA", p15, p16);
        return;
    }

    public void signZip(java.net.URL p13, String p14, char[] p15, String p16, char[] p17, String p18, String p19, String p20)
    {
        if (p14 == null) {
            try {
                p14 = java.security.KeyStore.getDefaultType();
            } catch (Throwable v2_0) {
                if (0 != 0) {
                    0.close();
                }
                throw v2_0;
            }
        }
        java.security.KeyStore v10 = java.security.KeyStore.getInstance(p14);
        java.io.InputStream v11_1 = p13.openStream();
        v10.load(v11_1, p15);
        this.setKeys("custom", ((java.security.cert.X509Certificate) v10.getCertificate(p16)), ((java.security.PrivateKey) v10.getKey(p16, p17)), p18, 0);
        this.signZip(p19, p20);
        if (v11_1 != null) {
            v11_1.close();
        }
        return;
    }

    public void signZip(java.util.Map p24, java.io.OutputStream p25, String p26)
    {
        boolean v8 = kellinwood.security.zipsigner.ZipSigner.getLogger().isDebugEnabled();
        this.progressHelper.initProgress();
        if (this.keySet == null) {
            if (this.keymode.startsWith("auto")) {
                String v11 = this.autoDetectKey(this.keymode, p24);
                if (v11 != null) {
                    this.autoKeyObservable.notifyObservers(v11);
                    this.loadKeys(v11);
                } else {
                    String v4_56 = this.resourceAdapter;
                    String v0_77 = new Object[1];
                    kellinwood.security.zipsigner.ResourceAdapter$Item v20_6 = v0_77;
                    String v22_0 = new java.io.File;
                    v22_0(p26);
                    v20_6[0] = v22_0.getName();
                    throw new kellinwood.security.zipsigner.AutoKeyException(v4_56.getString(kellinwood.security.zipsigner.ResourceAdapter$Item.AUTO_KEY_SELECTION_ERROR, v20_6));
                }
            } else {
                throw new IllegalStateException("No keys configured for signing the file!");
            }
        }
        try {
            int v5_1 = new kellinwood.zipio.ZipOutput(p25);
            try {
                if (!"none".equals(this.keySet.getName())) {
                    int v14_0 = 0;
                    java.util.Iterator v10 = p24.values().iterator();
                    while (v10.hasNext()) {
                        kellinwood.zipio.ZioEntry v9_1 = ((kellinwood.zipio.ZioEntry) v10.next());
                        String v12 = v9_1.getName();
                        if ((!v9_1.isDirectory()) && ((!v12.equals("META-INF/MANIFEST.MF")) && ((!v12.equals("META-INF/CERT.SF")) && ((!v12.equals("META-INF/CERT.RSA")) && ((kellinwood.security.zipsigner.ZipSigner.stripPattern == null) || (!kellinwood.security.zipsigner.ZipSigner.stripPattern.matcher(v12).matches())))))) {
                            v14_0 += 3;
                        }
                    }
                    this.progressHelper.setProgressTotalItems((v14_0 + 1));
                    this.progressHelper.setProgressCurrentItem(0);
                    long v6 = (this.keySet.getPublicKey().getNotBefore().getTime() + 3600000);
                    java.util.jar.Manifest v3 = this.addDigestsToManifest(p24);
                    if (!this.canceled) {
                        kellinwood.zipio.ZioEntry v17_0 = new kellinwood.zipio.ZioEntry;
                        v17_0("META-INF/MANIFEST.MF");
                        v17_0.setTime(v6);
                        v3.write(v17_0.getOutputStream());
                        v5_1.write(v17_0);
                        kellinwood.zipio.ZioEntry v17_1 = new kellinwood.zipio.ZioEntry;
                        v17_1("META-INF/CERT.SF");
                        v17_1.setTime(v6);
                        java.io.ByteArrayOutputStream v13_1 = new java.io.ByteArrayOutputStream();
                        this.generateSignatureFile(v3, v13_1);
                        if (!this.canceled) {
                            byte[] v15 = v13_1.toByteArray();
                            if (v8) {
                                kellinwood.logging.LoggerInterface v2_22 = kellinwood.security.zipsigner.ZipSigner.getLogger();
                                String v4_6 = new StringBuilder().append("Signature File: \n");
                                String v19_2 = new String;
                                v19_2(v15);
                                v2_22.debug(v4_6.append(v19_2).append("\n").append(kellinwood.security.zipsigner.HexDumpEncoder.encode(v15)).toString());
                            }
                            v17_1.getOutputStream().write(v15);
                            v5_1.write(v17_1);
                            String v0_30 = new Object[0];
                            this.progressHelper.progress(0, this.resourceAdapter.getString(kellinwood.security.zipsigner.ResourceAdapter$Item.GENERATING_SIGNATURE_BLOCK, v0_30));
                            kellinwood.zipio.ZioEntry v17_2 = new kellinwood.zipio.ZioEntry;
                            v17_2("META-INF/CERT.RSA");
                            v17_2.setTime(v6);
                            this.writeSignatureBlock(this.keySet, v15, v17_2.getOutputStream());
                            v5_1.write(v17_2);
                            if (!this.canceled) {
                                this.copyFiles(v3, p24, v5_1, v6);
                                if (!this.canceled) {
                                    v5_1.close();
                                    if ((this.canceled) && (p26 != null)) {
                                        try {
                                            new java.io.File(p26).delete();
                                        } catch (Throwable v16_0) {
                                            kellinwood.security.zipsigner.ZipSigner.getLogger().warning(new StringBuilder().append(v16_0.getClass().getName()).append(":").append(v16_0.getMessage()).toString());
                                        }
                                    }
                                } else {
                                    v5_1.close();
                                    if ((this.canceled) && (p26 != null)) {
                                        try {
                                            new java.io.File(p26).delete();
                                        } catch (Throwable v16_1) {
                                            kellinwood.security.zipsigner.ZipSigner.getLogger().warning(new StringBuilder().append(v16_1.getClass().getName()).append(":").append(v16_1.getMessage()).toString());
                                        }
                                    }
                                }
                            } else {
                                v5_1.close();
                                if ((this.canceled) && (p26 != null)) {
                                    try {
                                        new java.io.File(p26).delete();
                                    } catch (Throwable v16_2) {
                                        kellinwood.security.zipsigner.ZipSigner.getLogger().warning(new StringBuilder().append(v16_2.getClass().getName()).append(":").append(v16_2.getMessage()).toString());
                                    }
                                }
                            }
                        } else {
                            v5_1.close();
                            if ((this.canceled) && (p26 != null)) {
                                try {
                                    new java.io.File(p26).delete();
                                } catch (Throwable v16_3) {
                                    kellinwood.security.zipsigner.ZipSigner.getLogger().warning(new StringBuilder().append(v16_3.getClass().getName()).append(":").append(v16_3.getMessage()).toString());
                                }
                            }
                        }
                    } else {
                        v5_1.close();
                        if ((this.canceled) && (p26 != null)) {
                            try {
                                new java.io.File(p26).delete();
                            } catch (Throwable v16_4) {
                                kellinwood.security.zipsigner.ZipSigner.getLogger().warning(new StringBuilder().append(v16_4.getClass().getName()).append(":").append(v16_4.getMessage()).toString());
                            }
                        }
                    }
                } else {
                    this.progressHelper.setProgressTotalItems(p24.size());
                    this.progressHelper.setProgressCurrentItem(0);
                    this.copyFiles(p24, v5_1);
                    v5_1.close();
                    if ((this.canceled) && (p26 != null)) {
                        try {
                            new java.io.File(p26).delete();
                        } catch (Throwable v16_6) {
                            kellinwood.security.zipsigner.ZipSigner.getLogger().warning(new StringBuilder().append(v16_6.getClass().getName()).append(":").append(v16_6.getMessage()).toString());
                        }
                    }
                }
            } catch (kellinwood.logging.LoggerInterface v2_7) {
                v5_1.close();
                if (this.canceled) {
                    if (p26 != null) {
                        try {
                            new java.io.File(p26).delete();
                        } catch (Throwable v16_5) {
                            kellinwood.security.zipsigner.ZipSigner.getLogger().warning(new StringBuilder().append(v16_5.getClass().getName()).append(":").append(v16_5.getMessage()).toString());
                        }
                    }
                }
                throw v2_7;
            }
            return;
        } catch (kellinwood.logging.LoggerInterface v2_7) {
            v5_1 = 0;
        }
    }

    public void signZip(java.util.Map p2, String p3)
    {
        this.progressHelper.initProgress();
        this.signZip(p2, new java.io.FileOutputStream(p3), p3);
        return;
    }
}
