package kellinwood.security.zipsigner.optional;
public class KeyStoreFileManager {
    static kellinwood.logging.LoggerInterface logger;
    static java.security.Provider provider;

    static KeyStoreFileManager()
    {
        kellinwood.security.zipsigner.optional.KeyStoreFileManager.provider = new org.spongycastle.jce.provider.BouncyCastleProvider();
        kellinwood.security.zipsigner.optional.KeyStoreFileManager.logger = kellinwood.logging.LoggerManager.getLogger(kellinwood.security.zipsigner.optional.KeyStoreFileManager.getName());
        java.security.Security.addProvider(kellinwood.security.zipsigner.optional.KeyStoreFileManager.getProvider());
        return;
    }

    public KeyStoreFileManager()
    {
        return;
    }

    public static boolean containsKey(String p2, String p3, String p4)
    {
        return kellinwood.security.zipsigner.optional.KeyStoreFileManager.loadKeyStore(p2, p3).containsAlias(p4);
    }

    static void copyFile(java.io.File p10, java.io.File p11, boolean p12)
    {
        if ((!p11.exists()) || (!p11.isDirectory())) {
            java.io.FileInputStream v3_1 = new java.io.FileInputStream(p10);
            try {
                java.io.FileOutputStream v5_1 = new java.io.FileOutputStream(p11);
            } catch (long v6_5) {
                try {
                    v3_1.close();
                } catch (String v7) {
                }
                throw v6_5;
            }
            try {
                byte[] v0 = new byte[4096];
                long v1 = 0;
            } catch (long v6_4) {
                try {
                    v5_1.close();
                } catch (String v7) {
                }
                throw v6_4;
            }
            while(true) {
                int v4 = v3_1.read(v0);
                if (-1 == v4) {
                    break;
                }
                v5_1.write(v0, 0, v4);
                v1 += ((long) v4);
            }
            try {
                v5_1.close();
            } catch (long v6) {
            }
            try {
                v3_1.close();
            } catch (long v6) {
            }
            if (p10.length() == p11.length()) {
                if (p12) {
                    p11.setLastModified(p10.lastModified());
                }
                return;
            } else {
                throw new java.io.IOException(new StringBuilder().append("Failed to copy full contents from \'").append(p10).append("\' to \'").append(p11).append("\'").toString());
            }
        } else {
            throw new java.io.IOException(new StringBuilder().append("Destination \'").append(p11).append("\' exists but is a directory").toString());
        }
    }

    public static java.security.KeyStore createKeyStore(String p3, char[] p4)
    {
        kellinwood.security.zipsigner.optional.JksKeyStore v0_1;
        if (!p3.toLowerCase().endsWith(".bks")) {
            v0_1 = new kellinwood.security.zipsigner.optional.JksKeyStore();
        } else {
            v0_1 = java.security.KeyStore.getInstance("bks", new org.spongycastle.jce.provider.BouncyCastleProvider());
        }
        v0_1.load(0, p4);
        return v0_1;
    }

    public static void deleteKey(String p1, String p2, String p3)
    {
        java.security.KeyStore v0 = kellinwood.security.zipsigner.optional.KeyStoreFileManager.loadKeyStore(p1, p2);
        v0.deleteEntry(p3);
        kellinwood.security.zipsigner.optional.KeyStoreFileManager.writeKeyStore(v0, p1, p2);
        return;
    }

    public static java.security.KeyStore$Entry getKeyEntry(String p5, String p6, String p7, String p8)
    {
        java.security.KeyStore$PasswordProtection v2 = 0;
        try {
            java.security.KeyStore v1 = kellinwood.security.zipsigner.optional.KeyStoreFileManager.loadKeyStore(p5, p6);
            char[] v0 = kellinwood.security.zipsigner.optional.PasswordObfuscator.getInstance().decodeAliasPassword(p5, p7, p8);
            java.security.KeyStore$PasswordProtection v3_1 = new java.security.KeyStore$PasswordProtection(v0);
            try {
                Throwable v4_2 = v1.getEntry(p7, v3_1);
            } catch (Throwable v4_1) {
                v2 = v3_1;
                if (v0 != null) {
                    kellinwood.security.zipsigner.optional.PasswordObfuscator.flush(v0);
                }
                if (v2 != null) {
                    v2.destroy();
                }
                throw v4_1;
            }
            if (v0 != null) {
                kellinwood.security.zipsigner.optional.PasswordObfuscator.flush(v0);
            }
            if (v3_1 != null) {
                v3_1.destroy();
            }
            return v4_2;
        } catch (Throwable v4_1) {
        }
    }

    public static java.security.Provider getProvider()
    {
        return kellinwood.security.zipsigner.optional.KeyStoreFileManager.provider;
    }

    public static java.security.KeyStore loadKeyStore(String p2, String p3)
    {
        char[] v0 = 0;
        if (p3 != null) {
            try {
                v0 = kellinwood.security.zipsigner.optional.PasswordObfuscator.getInstance().decodeKeystorePassword(p2, p3);
            } catch (Throwable v1_1) {
                if (v0 != null) {
                    kellinwood.security.zipsigner.optional.PasswordObfuscator.flush(v0);
                }
                throw v1_1;
            }
        }
        Throwable v1_2 = kellinwood.security.zipsigner.optional.KeyStoreFileManager.loadKeyStore(p2, v0);
        if (v0 != null) {
            kellinwood.security.zipsigner.optional.PasswordObfuscator.flush(v0);
        }
        return v1_2;
    }

    public static java.security.KeyStore loadKeyStore(String p8, char[] p9)
    {
        try {
            java.security.KeyStore v3_1 = new kellinwood.security.zipsigner.optional.JksKeyStore();
            try {
                java.io.FileInputStream v1_1 = new java.io.FileInputStream(p8);
                v3_1.load(v1_1, p9);
                v1_1.close();
            } catch (Exception v4) {
                try {
                    java.security.KeyStore v2 = java.security.KeyStore.getInstance("bks", kellinwood.security.zipsigner.optional.KeyStoreFileManager.getProvider());
                    java.io.FileInputStream v1_3 = new java.io.FileInputStream(p8);
                    v2.load(v1_3, p9);
                    v1_3.close();
                    v3_1 = v2;
                } catch (Exception v0) {
                    throw new RuntimeException(new StringBuilder().append("Failed to load keystore: ").append(v0.getMessage()).toString(), v0);
                }
            }
            return v3_1;
        } catch (Exception v4) {
        }
    }

    public static String renameKey(String p6, String p7, String p8, String p9, String p10)
    {
        char[] v2 = 0;
        try {
            java.security.KeyStore v3 = kellinwood.security.zipsigner.optional.KeyStoreFileManager.loadKeyStore(p6, p7);
        } catch (java.security.cert.Certificate[] v4_7) {
            kellinwood.security.zipsigner.optional.PasswordObfuscator.flush(v2);
            throw v4_7;
        }
        if ((v3 instanceof kellinwood.security.zipsigner.optional.JksKeyStore)) {
            p9 = p9.toLowerCase();
        }
        if (!v3.containsAlias(p9)) {
            v2 = kellinwood.security.zipsigner.optional.PasswordObfuscator.getInstance().decodeAliasPassword(p6, p8, p10);
            java.security.Key v1 = v3.getKey(p8, v2);
            java.security.cert.Certificate[] v4_4 = new java.security.cert.Certificate[1];
            v4_4[0] = v3.getCertificate(p8);
            v3.setKeyEntry(p9, v1, v2, v4_4);
            v3.deleteEntry(p8);
            kellinwood.security.zipsigner.optional.KeyStoreFileManager.writeKeyStore(v3, p6, p7);
            kellinwood.security.zipsigner.optional.PasswordObfuscator.flush(v2);
            return p9;
        } else {
            throw new kellinwood.security.zipsigner.optional.KeyNameConflictException();
        }
    }

    public static void renameTo(java.io.File p3, java.io.File p4)
    {
        kellinwood.security.zipsigner.optional.KeyStoreFileManager.copyFile(p3, p4, 1);
        if (p3.delete()) {
            return;
        } else {
            throw new java.io.IOException(new StringBuilder().append("Failed to delete ").append(p3).toString());
        }
    }

    public static void setProvider(java.security.Provider p1)
    {
        if (kellinwood.security.zipsigner.optional.KeyStoreFileManager.provider != null) {
            java.security.Security.removeProvider(kellinwood.security.zipsigner.optional.KeyStoreFileManager.provider.getName());
        }
        kellinwood.security.zipsigner.optional.KeyStoreFileManager.provider = p1;
        java.security.Security.addProvider(p1);
        return;
    }

    public static void validateKeyPassword(String p3, String p4, String p5)
    {
        try {
            java.security.KeyStore v0 = kellinwood.security.zipsigner.optional.KeyStoreFileManager.loadKeyStore(p3, ((char[]) 0));
            char[] v1 = kellinwood.security.zipsigner.optional.PasswordObfuscator.getInstance().decodeAliasPassword(p3, p4, p5);
            v0.getKey(p4, v1);
        } catch (Throwable v2_3) {
            if (v1 != null) {
                kellinwood.security.zipsigner.optional.PasswordObfuscator.flush(v1);
            }
            throw v2_3;
        }
        if (v1 != null) {
            kellinwood.security.zipsigner.optional.PasswordObfuscator.flush(v1);
        }
        return;
    }

    public static void validateKeystorePassword(String p2, String p3)
    {
        try {
            kellinwood.security.zipsigner.optional.KeyStoreFileManager.loadKeyStore(p2, p3);
        } catch (Throwable v1) {
            if (0 != 0) {
                kellinwood.security.zipsigner.optional.PasswordObfuscator.flush(0);
            }
            throw v1;
        }
        if (0 != 0) {
            kellinwood.security.zipsigner.optional.PasswordObfuscator.flush(0);
        }
        return;
    }

    public static void writeKeyStore(java.security.KeyStore p2, String p3, String p4)
    {
        try {
            char[] v0 = kellinwood.security.zipsigner.optional.PasswordObfuscator.getInstance().decodeKeystorePassword(p3, p4);
            kellinwood.security.zipsigner.optional.KeyStoreFileManager.writeKeyStore(p2, p3, v0);
        } catch (Throwable v1_1) {
            if (v0 != null) {
                kellinwood.security.zipsigner.optional.PasswordObfuscator.flush(v0);
            }
            throw v1_1;
        }
        if (v0 != null) {
            kellinwood.security.zipsigner.optional.PasswordObfuscator.flush(v0);
        }
        return;
    }

    public static void writeKeyStore(java.security.KeyStore p9, String p10, char[] p11)
    {
        java.io.File v1_1 = new java.io.File(p10);
        try {
            if (!v1_1.exists()) {
                java.io.FileOutputStream v0_1 = new java.io.FileOutputStream(p10);
                p9.store(v0_1, p11);
                v0_1.close();
            } else {
                java.io.File v4 = java.io.File.createTempFile(v1_1.getName(), 0, v1_1.getParentFile());
                java.io.FileOutputStream v0_3 = new java.io.FileOutputStream(v4);
                p9.store(v0_3, p11);
                v0_3.flush();
                v0_3.close();
                kellinwood.security.zipsigner.optional.KeyStoreFileManager.renameTo(v4, v1_1);
            }
        } catch (Exception v5) {
            try {
                java.io.PrintWriter v3_1 = new java.io.PrintWriter(new java.io.FileWriter(java.io.File.createTempFile("zipsigner-error", ".log", v1_1.getParentFile())));
                v5.printStackTrace(v3_1);
                v3_1.flush();
                v3_1.close();
            } catch (Exception v6) {
            }
            throw v5;
        }
        return;
    }
}
