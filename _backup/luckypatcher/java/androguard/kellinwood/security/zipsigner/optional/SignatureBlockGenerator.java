package kellinwood.security.zipsigner.optional;
public class SignatureBlockGenerator {

    public SignatureBlockGenerator()
    {
        return;
    }

    public static byte[] generate(kellinwood.security.zipsigner.KeySet p14, byte[] p15)
    {
        try {
            java.util.ArrayList v0_1 = new java.util.ArrayList();
            org.spongycastle.cms.CMSProcessableByteArray v7_1 = new org.spongycastle.cms.CMSProcessableByteArray(p15);
            v0_1.add(p14.getPublicKey());
            org.spongycastle.cert.jcajce.JcaCertStore v1_1 = new org.spongycastle.cert.jcajce.JcaCertStore(v0_1);
            org.spongycastle.cms.CMSSignedDataGenerator v3_1 = new org.spongycastle.cms.CMSSignedDataGenerator();
            org.spongycastle.operator.ContentSigner v8 = new org.spongycastle.operator.jcajce.JcaContentSignerBuilder(p14.getSignatureAlgorithm()).setProvider("SC").build(p14.getPrivateKey());
            org.spongycastle.cms.jcajce.JcaSignerInfoGeneratorBuilder v6_1 = new org.spongycastle.cms.jcajce.JcaSignerInfoGeneratorBuilder(new org.spongycastle.operator.jcajce.JcaDigestCalculatorProviderBuilder().setProvider("SC").build());
            v6_1.setDirectSignature(1);
            v3_1.addSignerInfoGenerator(v6_1.build(v8, p14.getPublicKey()));
            v3_1.addCertificates(v1_1);
            return v3_1.generate(v7_1, 0).toASN1Structure().getEncoded("DER");
        } catch (Exception v11) {
            throw new RuntimeException(v11.getMessage(), v11);
        }
    }
}
