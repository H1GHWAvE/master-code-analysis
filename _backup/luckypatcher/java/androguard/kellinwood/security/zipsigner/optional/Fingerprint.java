package kellinwood.security.zipsigner.optional;
public class Fingerprint {
    static kellinwood.logging.LoggerInterface logger;

    static Fingerprint()
    {
        kellinwood.security.zipsigner.optional.Fingerprint.logger = kellinwood.logging.LoggerManager.getLogger(kellinwood.security.zipsigner.optional.Fingerprint.getName());
        return;
    }

    public Fingerprint()
    {
        return;
    }

    public static String base64Fingerprint(String p5, byte[] p6)
    {
        String v1 = 0;
        try {
            byte[] v0 = kellinwood.security.zipsigner.optional.Fingerprint.calcDigest(p5, p6);
        } catch (Exception v2) {
            kellinwood.security.zipsigner.optional.Fingerprint.logger.error(v2.getMessage(), v2);
            return v1;
        }
        if (v0 != null) {
            v1 = kellinwood.security.zipsigner.Base64.encode(v0);
            return v1;
        } else {
            return v1;
        }
    }

    static byte[] calcDigest(String p5, byte[] p6)
    {
        try {
            java.security.MessageDigest v0 = java.security.MessageDigest.getInstance(p5);
            v0.update(p6);
            byte[] v1 = v0.digest();
        } catch (Exception v2) {
            kellinwood.security.zipsigner.optional.Fingerprint.logger.error(v2.getMessage(), v2);
        }
        return v1;
    }

    public static String hexFingerprint(String p10, byte[] p11)
    {
        try {
            int v2_5;
            byte[] v1 = kellinwood.security.zipsigner.optional.Fingerprint.calcDigest(p10, p11);
        } catch (Exception v8) {
            kellinwood.security.zipsigner.optional.Fingerprint.logger.error(v8.getMessage(), v8);
            v2_5 = 0;
            return v2_5;
        }
        if (v1 != null) {
            byte[] v4 = new byte[(v1.length * 2)];
            new org.spongycastle.util.encoders.HexTranslator().encode(v1, 0, v1.length, v4, 0);
            StringBuilder v6_1 = new StringBuilder();
            int v7 = 0;
            while (v7 < v4.length) {
                v6_1.append(((char) v4[v7]));
                v6_1.append(((char) v4[(v7 + 1)]));
                if (v7 != (v4.length - 2)) {
                    v6_1.append(58);
                }
                v7 += 2;
            }
            v2_5 = v6_1.toString().toUpperCase();
            return v2_5;
        } else {
            v2_5 = 0;
            return v2_5;
        }
    }
}
