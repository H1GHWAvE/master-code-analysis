package kellinwood.security.zipsigner;
public class HexDumpEncoder {
    static kellinwood.security.zipsigner.HexEncoder encoder;

    static HexDumpEncoder()
    {
        kellinwood.security.zipsigner.HexDumpEncoder.encoder = new kellinwood.security.zipsigner.HexEncoder();
        return;
    }

    public HexDumpEncoder()
    {
        return;
    }

    public static String encode(byte[] p15)
    {
        try {
            java.io.ByteArrayOutputStream v0_1 = new java.io.ByteArrayOutputStream();
            kellinwood.security.zipsigner.HexDumpEncoder.encoder.encode(p15, 0, p15.length, v0_1);
            byte[] v3 = v0_1.toByteArray();
            StringBuilder v4_1 = new StringBuilder();
            int v6 = 0;
        } catch (java.io.IOException v10) {
            throw new IllegalStateException(new StringBuilder().append(v10.getClass().getName()).append(": ").append(v10.getMessage()).toString());
        }
        while (v6 < v3.length) {
            int v9 = Math.min((v6 + 32), v3.length);
            StringBuilder v5_1 = new StringBuilder();
            StringBuilder v1_1 = new StringBuilder();
            Object[] v12_3 = new Object[1];
            v12_3[0] = Integer.valueOf((v6 / 2));
            v5_1.append(String.format("%08x: ", v12_3));
            int v7 = v6;
            while (v7 < v9) {
                v5_1.append(Character.valueOf(((char) v3[v7])));
                v5_1.append(Character.valueOf(((char) v3[(v7 + 1)])));
                if (((v7 + 2) % 4) == 0) {
                    v5_1.append(32);
                }
                byte v2 = p15[(v7 / 2)];
                if ((v2 < 32) || (v2 >= 127)) {
                    v1_1.append(46);
                } else {
                    v1_1.append(Character.valueOf(((char) v2)));
                }
                v7 += 2;
            }
            v4_1.append(v5_1.toString());
            int v8 = v5_1.length();
            while (v8 < 50) {
                v4_1.append(32);
                v8++;
            }
            v4_1.append("  ");
            v4_1.append(v1_1);
            v4_1.append("\n");
            v6 += 32;
        }
        return v4_1.toString();
    }
}
