package kellinwood.security.zipsigner;
public class ProgressHelper {
    private java.util.ArrayList listeners;
    private int progressCurrentItem;
    private kellinwood.security.zipsigner.ProgressEvent progressEvent;
    private int progressTotalItems;

    public ProgressHelper()
    {
        this.progressTotalItems = 0;
        this.progressCurrentItem = 0;
        this.progressEvent = new kellinwood.security.zipsigner.ProgressEvent();
        this.listeners = new java.util.ArrayList();
        return;
    }

    public declared_synchronized void addProgressListener(kellinwood.security.zipsigner.ProgressListener p3)
    {
        try {
            java.util.ArrayList v0_1 = ((java.util.ArrayList) this.listeners.clone());
            v0_1.add(p3);
            this.listeners = v0_1;
            return;
        } catch (Throwable v1_1) {
            throw v1_1;
        }
    }

    public int getProgressCurrentItem()
    {
        return this.progressCurrentItem;
    }

    public int getProgressTotalItems()
    {
        return this.progressTotalItems;
    }

    public void initProgress()
    {
        this.progressTotalItems = 10000;
        this.progressCurrentItem = 0;
        return;
    }

    public void progress(int p6, String p7)
    {
        int v2;
        this.progressCurrentItem = (this.progressCurrentItem + 1);
        if (this.progressTotalItems != 0) {
            v2 = ((this.progressCurrentItem * 100) / this.progressTotalItems);
        } else {
            v2 = 0;
        }
        java.util.Iterator v0 = this.listeners.iterator();
        while (v0.hasNext()) {
            kellinwood.security.zipsigner.ProgressListener v1_1 = ((kellinwood.security.zipsigner.ProgressListener) v0.next());
            this.progressEvent.setMessage(p7);
            this.progressEvent.setPercentDone(v2);
            this.progressEvent.setPriority(p6);
            v1_1.onProgress(this.progressEvent);
        }
        return;
    }

    public declared_synchronized void removeProgressListener(kellinwood.security.zipsigner.ProgressListener p3)
    {
        try {
            java.util.ArrayList v0_1 = ((java.util.ArrayList) this.listeners.clone());
            v0_1.remove(p3);
            this.listeners = v0_1;
            return;
        } catch (Throwable v1_1) {
            throw v1_1;
        }
    }

    public void setProgressCurrentItem(int p1)
    {
        this.progressCurrentItem = p1;
        return;
    }

    public void setProgressTotalItems(int p1)
    {
        this.progressTotalItems = p1;
        return;
    }
}
