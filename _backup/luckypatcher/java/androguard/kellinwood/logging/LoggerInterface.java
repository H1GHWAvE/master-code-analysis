package kellinwood.logging;
public interface LoggerInterface {
    public static final String DEBUG = "DEBUG";
    public static final String ERROR = "ERROR";
    public static final String INFO = "INFO";
    public static final String WARNING = "WARNING";

    public abstract void debug();

    public abstract void debug();

    public abstract void error();

    public abstract void error();

    public abstract void info();

    public abstract void info();

    public abstract boolean isDebugEnabled();

    public abstract boolean isErrorEnabled();

    public abstract boolean isInfoEnabled();

    public abstract boolean isWarningEnabled();

    public abstract void warning();

    public abstract void warning();
}
