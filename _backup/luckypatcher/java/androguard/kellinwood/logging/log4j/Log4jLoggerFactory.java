package kellinwood.logging.log4j;
public class Log4jLoggerFactory implements kellinwood.logging.LoggerFactory {

    public Log4jLoggerFactory()
    {
        return;
    }

    public kellinwood.logging.LoggerInterface getLogger(String p2)
    {
        return new kellinwood.logging.log4j.Log4jLogger(p2);
    }
}
