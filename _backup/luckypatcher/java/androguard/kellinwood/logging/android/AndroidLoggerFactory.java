package kellinwood.logging.android;
public class AndroidLoggerFactory implements kellinwood.logging.LoggerFactory {

    public AndroidLoggerFactory()
    {
        return;
    }

    public kellinwood.logging.LoggerInterface getLogger(String p2)
    {
        return new kellinwood.logging.android.AndroidLogger(p2);
    }
}
