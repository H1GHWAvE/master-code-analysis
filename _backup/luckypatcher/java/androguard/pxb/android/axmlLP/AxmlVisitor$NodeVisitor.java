package pxb.android.axmlLP;
public abstract class AxmlVisitor$NodeVisitor {
    protected pxb.android.axmlLP.AxmlVisitor$NodeVisitor nv;

    public AxmlVisitor$NodeVisitor()
    {
        return;
    }

    public AxmlVisitor$NodeVisitor(pxb.android.axmlLP.AxmlVisitor$NodeVisitor p1)
    {
        this.nv = p1;
        return;
    }

    public void attr(String p7, String p8, int p9, int p10, Object p11)
    {
        if (this.nv != null) {
            this.nv.attr(p7, p8, p9, p10, p11);
        }
        return;
    }

    public pxb.android.axmlLP.AxmlVisitor$NodeVisitor child(String p2, String p3)
    {
        int v0_1;
        if (this.nv == null) {
            v0_1 = 0;
        } else {
            v0_1 = this.nv.child(p2, p3);
        }
        return v0_1;
    }

    public void end()
    {
        if (this.nv != null) {
            this.nv.end();
        }
        return;
    }

    public void line(int p2)
    {
        if (this.nv != null) {
            this.nv.line(p2);
        }
        return;
    }

    public void text(int p2, String p3)
    {
        if (this.nv != null) {
            this.nv.text(p2, p3);
        }
        return;
    }
}
