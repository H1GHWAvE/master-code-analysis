package pxb.android.axmlLP;
 class AxmlWriter$Attr {
    public pxb.android.axmlLP.StringItem name;
    public pxb.android.axmlLP.StringItem ns;
    public int resourceId;
    public int type;
    public Object value;

    public AxmlWriter$Attr(pxb.android.axmlLP.StringItem p1, pxb.android.axmlLP.StringItem p2, int p3, int p4, Object p5)
    {
        this.ns = p1;
        this.name = p2;
        this.resourceId = p3;
        this.type = p4;
        this.value = p5;
        return;
    }

    public void prepare(pxb.android.axmlLP.AxmlWriter p3)
    {
        this.ns = p3.updateNs(this.ns);
        if (this.name != null) {
            if (this.resourceId == -1) {
                this.name = p3.update(this.name);
            } else {
                this.name = p3.updateWithResourceId(this.name, this.resourceId);
            }
        }
        if ((this.value instanceof pxb.android.axmlLP.StringItem)) {
            this.value = p3.update(((pxb.android.axmlLP.StringItem) this.value));
        }
        return;
    }
}
