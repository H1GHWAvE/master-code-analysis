package pxb.android.axmlLP;
 class AxmlWriter$NodeImpl extends pxb.android.axmlLP.AxmlVisitor$NodeVisitor {
    private java.util.Map attrs;
    private java.util.List children;
    private int line;
    private pxb.android.axmlLP.StringItem name;
    private pxb.android.axmlLP.StringItem ns;
    private pxb.android.axmlLP.StringItem text;
    private int textLineNumber;

    public AxmlWriter$NodeImpl(String p3, String p4)
    {
        pxb.android.axmlLP.StringItem v0_5;
        pxb.android.axmlLP.StringItem v1_0 = 0;
        this(0);
        this.attrs = new java.util.HashMap();
        this.children = new java.util.ArrayList();
        if (p3 != null) {
            v0_5 = new pxb.android.axmlLP.StringItem(p3);
        } else {
            v0_5 = 0;
        }
        this.ns = v0_5;
        if (p4 != null) {
            v1_0 = new pxb.android.axmlLP.StringItem(p4);
        }
        this.name = v1_0;
        return;
    }

    public void attr(String p9, String p10, int p11, int p12, Object p13)
    {
        if (p10 != null) {
            pxb.android.axmlLP.AxmlWriter$Attr v0_0;
            java.util.Map v6 = this.attrs;
            pxb.android.axmlLP.StringItem v1_1 = new StringBuilder();
            if (p9 != null) {
                v0_0 = p9;
            } else {
                v0_0 = "zzz";
            }
            pxb.android.axmlLP.StringItem v1_4;
            String v7 = v1_1.append(v0_0).append(".").append(p10).toString();
            if (p9 != null) {
                v1_4 = new pxb.android.axmlLP.StringItem(p9);
            } else {
                v1_4 = 0;
            }
            String v5_0;
            pxb.android.axmlLP.StringItem v2_1 = new pxb.android.axmlLP.StringItem(p10);
            if (p12 != 3) {
                v5_0 = p13;
            } else {
                v5_0 = new pxb.android.axmlLP.StringItem(((String) p13));
            }
            v6.put(v7, new pxb.android.axmlLP.AxmlWriter$Attr(v1_4, v2_1, p11, p12, v5_0));
            return;
        } else {
            throw new RuntimeException("name can\'t be null");
        }
    }

    public pxb.android.axmlLP.AxmlVisitor$NodeVisitor child(String p3, String p4)
    {
        pxb.android.axmlLP.AxmlWriter$NodeImpl v0_1 = new pxb.android.axmlLP.AxmlWriter$NodeImpl(p3, p4);
        this.children.add(v0_1);
        return v0_1;
    }

    public void end()
    {
        return;
    }

    public void line(int p1)
    {
        this.line = p1;
        return;
    }

    public int prepare(pxb.android.axmlLP.AxmlWriter p6)
    {
        this.ns = p6.updateNs(this.ns);
        this.name = p6.update(this.name);
        pxb.android.axmlLP.StringItem v3_5 = this.sortedAttrs().iterator();
        while (v3_5.hasNext()) {
            ((pxb.android.axmlLP.AxmlWriter$Attr) v3_5.next()).prepare(p6);
        }
        this.text = p6.update(this.text);
        int v2 = ((this.attrs.size() * 20) + 60);
        pxb.android.axmlLP.StringItem v3_12 = this.children.iterator();
        while (v3_12.hasNext()) {
            v2 += ((pxb.android.axmlLP.AxmlWriter$NodeImpl) v3_12.next()).prepare(p6);
        }
        if (this.text != null) {
            v2 += 28;
        }
        return v2;
    }

    java.util.List sortedAttrs()
    {
        java.util.ArrayList v0_1 = new java.util.ArrayList(this.attrs.values());
        java.util.Collections.sort(v0_1, new pxb.android.axmlLP.AxmlWriter$NodeImpl$1(this));
        return v0_1;
    }

    public void text(int p2, String p3)
    {
        this.text = new pxb.android.axmlLP.StringItem(p3);
        this.textLineNumber = p2;
        return;
    }

    void write(com.googlecode.dex2jar.reader.io.DataOut p8)
    {
        boolean v3_7;
        int v4 = -1;
        p8.writeInt(1048834);
        p8.writeInt(((this.attrs.size() * 20) + 36));
        p8.writeInt(this.line);
        p8.writeInt(-1);
        if (this.ns == null) {
            v3_7 = -1;
        } else {
            v3_7 = this.ns.index;
        }
        p8.writeInt(v3_7);
        p8.writeInt(this.name.index);
        p8.writeInt(1310740);
        p8.writeShort(this.attrs.size());
        p8.writeShort(0);
        p8.writeShort(0);
        p8.writeShort(0);
        java.util.Iterator v6 = this.sortedAttrs().iterator();
        while (v6.hasNext()) {
            boolean v3_33;
            pxb.android.axmlLP.AxmlWriter$Attr v0_1 = ((pxb.android.axmlLP.AxmlWriter$Attr) v6.next());
            if (v0_1.ns != null) {
                v3_33 = v0_1.ns.index;
            } else {
                v3_33 = -1;
            }
            boolean v3_38;
            p8.writeInt(v3_33);
            p8.writeInt(v0_1.name.index);
            if (!(v0_1.value instanceof pxb.android.axmlLP.StringItem)) {
                v3_38 = -1;
            } else {
                v3_38 = ((pxb.android.axmlLP.StringItem) v0_1.value).index;
            }
            p8.writeInt(v3_38);
            p8.writeInt(((v0_1.type << 24) | 8));
            Object v2 = v0_1.value;
            if (!(v2 instanceof pxb.android.axmlLP.StringItem)) {
                if (!(v2 instanceof Boolean)) {
                    p8.writeInt(((Integer) v0_1.value).intValue());
                } else {
                    boolean v3_51;
                    if (!Boolean.TRUE.equals(v2)) {
                        v3_51 = 0;
                    } else {
                        v3_51 = -1;
                    }
                    p8.writeInt(v3_51);
                }
            } else {
                p8.writeInt(((pxb.android.axmlLP.StringItem) v0_1.value).index);
            }
        }
        if (this.text != null) {
            p8.writeInt(1048836);
            p8.writeInt(28);
            p8.writeInt(this.textLineNumber);
            p8.writeInt(-1);
            p8.writeInt(this.text.index);
            p8.writeInt(8);
            p8.writeInt(0);
        }
        boolean v3_24 = this.children.iterator();
        while (v3_24.hasNext()) {
            ((pxb.android.axmlLP.AxmlWriter$NodeImpl) v3_24.next()).write(p8);
        }
        p8.writeInt(1048835);
        p8.writeInt(24);
        p8.writeInt(-1);
        p8.writeInt(-1);
        if (this.ns != null) {
            v4 = this.ns.index;
        }
        p8.writeInt(v4);
        p8.writeInt(this.name.index);
        return;
    }
}
