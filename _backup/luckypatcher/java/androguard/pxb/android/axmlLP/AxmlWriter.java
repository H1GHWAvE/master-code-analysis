package pxb.android.axmlLP;
public class AxmlWriter extends pxb.android.axmlLP.AxmlVisitor {
    private java.util.List firsts;
    private java.util.Map nses;
    private java.util.List otherString;
    private java.util.Map resourceId2Str;
    private java.util.List resourceIds;
    private java.util.List resourceString;
    private pxb.android.axmlLP.StringItems stringItems;

    public AxmlWriter()
    {
        this.firsts = new java.util.ArrayList(3);
        this.nses = new java.util.HashMap();
        this.otherString = new java.util.ArrayList();
        this.resourceId2Str = new java.util.HashMap();
        this.resourceIds = new java.util.ArrayList();
        this.resourceString = new java.util.ArrayList();
        this.stringItems = new pxb.android.axmlLP.StringItems();
        return;
    }

    private int prepare()
    {
        int v5_0 = ((this.nses.size() * 24) * 2);
        int v7_4 = this.firsts.iterator();
        while (v7_4.hasNext()) {
            v5_0 += ((pxb.android.axmlLP.AxmlWriter$NodeImpl) v7_4.next()).prepare(this);
        }
        int v0 = 0;
        java.util.List v8_1 = this.nses.entrySet().iterator();
        while (v8_1.hasNext()) {
            java.util.Map$Entry v2_1 = ((java.util.Map$Entry) v8_1.next());
            pxb.android.axmlLP.AxmlWriter$Ns v4_1 = ((pxb.android.axmlLP.AxmlWriter$Ns) v2_1.getValue());
            if (v4_1 == null) {
                pxb.android.axmlLP.StringItem v10_1 = new Object[1];
                int v1 = (v0 + 1);
                v10_1[0] = Integer.valueOf(v0);
                v4_1 = new pxb.android.axmlLP.AxmlWriter$Ns(new pxb.android.axmlLP.StringItem(String.format("axml_auto_%02d", v10_1)), new pxb.android.axmlLP.StringItem(((String) v2_1.getKey())), 0);
                v2_1.setValue(v4_1);
                v0 = v1;
            }
            v4_1.prefix = this.update(v4_1.prefix);
            v4_1.uri = this.update(v4_1.uri);
        }
        this.stringItems.addAll(this.resourceString);
        this.resourceString = 0;
        this.stringItems.addAll(this.otherString);
        this.otherString = 0;
        this.stringItems.prepare();
        int v6 = this.stringItems.getSize();
        if ((v6 % 4) != 0) {
            v6 += (4 - (v6 % 4));
        }
        return ((v5_0 + (v6 + 8)) + ((this.resourceIds.size() * 4) + 8));
    }

    public void end()
    {
        return;
    }

    public pxb.android.axmlLP.AxmlVisitor$NodeVisitor first(String p3, String p4)
    {
        pxb.android.axmlLP.AxmlWriter$NodeImpl v0_1 = new pxb.android.axmlLP.AxmlWriter$NodeImpl(p3, p4);
        this.firsts.add(v0_1);
        return v0_1;
    }

    public void ns(String p5, String p6, int p7)
    {
        this.nses.put(p6, new pxb.android.axmlLP.AxmlWriter$Ns(new pxb.android.axmlLP.StringItem(p5), new pxb.android.axmlLP.StringItem(p6), p7));
        return;
    }

    public byte[] toByteArray()
    {
        java.io.ByteArrayOutputStream v4_1 = new java.io.ByteArrayOutputStream();
        com.googlecode.dex2jar.reader.io.LeDataOut v5_1 = new com.googlecode.dex2jar.reader.io.LeDataOut(v4_1);
        int v7 = this.prepare();
        v5_1.writeInt(524291);
        v5_1.writeInt((v7 + 8));
        int v9 = this.stringItems.getSize();
        int v6 = 0;
        if ((v9 % 4) != 0) {
            v6 = (4 - (v9 % 4));
        }
        v5_1.writeInt(1835009);
        v5_1.writeInt(((v9 + v6) + 8));
        this.stringItems.write(v5_1);
        byte[] v10_9 = new byte[v6];
        v5_1.writeBytes(v10_9);
        v5_1.writeInt(524672);
        v5_1.writeInt(((this.resourceIds.size() * 4) + 8));
        byte[] v10_16 = this.resourceIds.iterator();
        while (v10_16.hasNext()) {
            v5_1.writeInt(((Integer) v10_16.next()).intValue());
        }
        java.util.Stack v8_1 = new java.util.Stack();
        byte[] v10_19 = this.nses.entrySet().iterator();
        while (v10_19.hasNext()) {
            pxb.android.axmlLP.AxmlWriter$Ns v3_3 = ((pxb.android.axmlLP.AxmlWriter$Ns) ((java.util.Map$Entry) v10_19.next()).getValue());
            v8_1.push(v3_3);
            v5_1.writeInt(1048832);
            v5_1.writeInt(24);
            v5_1.writeInt(-1);
            v5_1.writeInt(-1);
            v5_1.writeInt(v3_3.prefix.index);
            v5_1.writeInt(v3_3.uri.index);
        }
        byte[] v10_21 = this.firsts.iterator();
        while (v10_21.hasNext()) {
            ((pxb.android.axmlLP.AxmlWriter$NodeImpl) v10_21.next()).write(v5_1);
        }
        while (v8_1.size() > 0) {
            pxb.android.axmlLP.AxmlWriter$Ns v3_1 = ((pxb.android.axmlLP.AxmlWriter$Ns) v8_1.pop());
            v5_1.writeInt(1048833);
            v5_1.writeInt(24);
            v5_1.writeInt(v3_1.ln);
            v5_1.writeInt(-1);
            v5_1.writeInt(v3_1.prefix.index);
            v5_1.writeInt(v3_1.uri.index);
        }
        return v4_1.toByteArray();
    }

    pxb.android.axmlLP.StringItem update(pxb.android.axmlLP.StringItem p4)
    {
        pxb.android.axmlLP.StringItem v0_0;
        if (p4 != null) {
            int v1 = this.otherString.indexOf(p4);
            if (v1 >= 0) {
                v0_0 = ((pxb.android.axmlLP.StringItem) this.otherString.get(v1));
            } else {
                v0_0 = new pxb.android.axmlLP.StringItem(p4.data);
                this.otherString.add(v0_0);
            }
        } else {
            v0_0 = 0;
        }
        return v0_0;
    }

    pxb.android.axmlLP.StringItem updateNs(pxb.android.axmlLP.StringItem p4)
    {
        pxb.android.axmlLP.StringItem v1 = 0;
        if (p4 != null) {
            String v0 = p4.data;
            if (!this.nses.containsKey(v0)) {
                this.nses.put(v0, 0);
            }
            v1 = this.update(p4);
        }
        return v1;
    }

    pxb.android.axmlLP.StringItem updateWithResourceId(pxb.android.axmlLP.StringItem p5, int p6)
    {
        pxb.android.axmlLP.StringItem v1_1 = ((pxb.android.axmlLP.StringItem) this.resourceId2Str.get(Integer.valueOf(p6)));
        if (v1_1 == null) {
            pxb.android.axmlLP.StringItem v0_1 = new pxb.android.axmlLP.StringItem(p5.data);
            this.resourceIds.add(Integer.valueOf(p6));
            this.resourceString.add(v0_1);
            this.resourceId2Str.put(Integer.valueOf(p6), v0_1);
            v1_1 = v0_1;
        }
        return v1_1;
    }
}
