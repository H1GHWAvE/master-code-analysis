package pxb.android.axmlLP;
 class StringItem {
    public String data;
    public int dataOffset;
    public int index;

    public StringItem()
    {
        return;
    }

    public StringItem(String p1)
    {
        this.data = p1;
        return;
    }

    public boolean equals(Object p4)
    {
        return ((pxb.android.axmlLP.StringItem) p4).data.equals(this.data);
    }

    public int hashCode()
    {
        return this.data.hashCode();
    }

    public String toString()
    {
        Object[] v1_1 = new Object[2];
        v1_1[0] = Integer.valueOf(this.index);
        v1_1[1] = this.data;
        return String.format("S%04d %s", v1_1);
    }
}
