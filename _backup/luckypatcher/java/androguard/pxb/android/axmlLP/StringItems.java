package pxb.android.axmlLP;
 class StringItems extends java.util.ArrayList {
    byte[] stringData;

    StringItems()
    {
        return;
    }

    public int getSize()
    {
        return ((((this.size() * 4) + 20) + this.stringData.length) + 0);
    }

    public void prepare()
    {
        java.io.ByteArrayOutputStream v0_1 = new java.io.ByteArrayOutputStream();
        int v2 = 0;
        int v8 = 0;
        v0_1.reset();
        java.util.HashMap v6_1 = new java.util.HashMap();
        byte[] v10_0 = this.iterator();
        while (v10_0.hasNext()) {
            pxb.android.axmlLP.StringItem v4_1 = ((pxb.android.axmlLP.StringItem) v10_0.next());
            int v3 = (v2 + 1);
            v4_1.index = v2;
            String v9 = v4_1.data;
            Integer v7_1 = ((Integer) v6_1.get(v9));
            if (v7_1 == null) {
                v4_1.dataOffset = v8;
                v6_1.put(v9, Integer.valueOf(v8));
                int v5 = v9.length();
                byte[] v1 = v9.getBytes("UTF-16LE");
                v0_1.write(v5);
                v0_1.write((v5 >> 8));
                v0_1.write(v1);
                v0_1.write(0);
                v0_1.write(0);
                v8 += (v1.length + 4);
            } else {
                v4_1.dataOffset = v7_1.intValue();
            }
            v2 = v3;
        }
        this.stringData = v0_1.toByteArray();
        return;
    }

    public void read(com.googlecode.dex2jar.reader.io.DataIn p23, int p24)
    {
        p23.getCurrentPosition();
        int v12 = p23.readIntx();
        int v16 = p23.readIntx();
        int v6 = p23.readIntx();
        p23.readIntx();
        int v17 = p23.readIntx();
        int v7 = 0;
        while (v7 < v12) {
            pxb.android.axmlLP.StringItem v14_1 = new pxb.android.axmlLP.StringItem();
            v14_1.index = v7;
            v14_1.dataOffset = p23.readIntx();
            this.add(v14_1);
            v7++;
        }
        java.util.TreeMap v15_1 = new java.util.TreeMap();
        if (v16 == 0) {
            int v5;
            if (v17 != 0) {
                v5 = v17;
            } else {
                v5 = p24;
            }
            int v2 = p23.getCurrentPosition();
            if ((v6 & 256) == 0) {
                int v10_0 = v2;
                while (v10_0 < v5) {
                    byte[] v4 = p23.readBytes((p23.readShortx() * 2));
                    p23.skip(2);
                    String v19_0 = new String;
                    v19_0(v4, "UTF-16LE");
                    v15_1.put(Integer.valueOf((v10_0 - v2)), v19_0);
                    v10_0 = p23.getCurrentPosition();
                }
            } else {
                int v10_1 = v2;
                while (v10_1 < v5) {
                    java.io.ByteArrayOutputStream v3_1 = new java.io.ByteArrayOutputStream((((int) p23.readLeb128()) + 10));
                    int v11 = p23.readByte();
                    while (v11 != 0) {
                        v3_1.write(v11);
                        v11 = p23.readByte();
                    }
                    v15_1.put(Integer.valueOf((v10_1 - v2)), new String(v3_1.toByteArray(), "UTF-8"));
                    v10_1 = p23.getCurrentPosition();
                }
            }
            // Both branches of the condition point to the same code.
            // if (v17 == 0) {
                String v21_0 = this.iterator();
                while (v21_0.hasNext()) {
                    pxb.android.axmlLP.StringItem v8_1 = ((pxb.android.axmlLP.StringItem) v21_0.next());
                    v8_1.data = ((String) v15_1.get(Integer.valueOf(v8_1.dataOffset)));
                }
                return;
            // }
        } else {
            throw new RuntimeException();
        }
    }

    public void write(com.googlecode.dex2jar.reader.io.DataOut p4)
    {
        p4.writeInt(this.size());
        p4.writeInt(0);
        p4.writeInt(0);
        p4.writeInt(((this.size() * 4) + 28));
        p4.writeInt(0);
        byte[] v1_4 = this.iterator();
        while (v1_4.hasNext()) {
            p4.writeInt(((pxb.android.axmlLP.StringItem) v1_4.next()).dataOffset);
        }
        p4.writeBytes(this.stringData);
        return;
    }
}
