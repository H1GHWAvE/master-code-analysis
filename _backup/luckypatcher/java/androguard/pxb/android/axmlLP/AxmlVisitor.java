package pxb.android.axmlLP;
public class AxmlVisitor {
    public static final int TYPE_FIRST_INT = 16;
    public static final int TYPE_INT_BOOLEAN = 18;
    public static final int TYPE_INT_HEX = 17;
    public static final int TYPE_REFERENCE = 1;
    public static final int TYPE_STRING = 3;
    protected pxb.android.axmlLP.AxmlVisitor av;

    public AxmlVisitor()
    {
        return;
    }

    public AxmlVisitor(pxb.android.axmlLP.AxmlVisitor p1)
    {
        this.av = p1;
        return;
    }

    public void end()
    {
        if (this.av != null) {
            this.av.end();
        }
        return;
    }

    public pxb.android.axmlLP.AxmlVisitor$NodeVisitor first(String p2, String p3)
    {
        int v0_1;
        if (this.av == null) {
            v0_1 = 0;
        } else {
            v0_1 = this.av.first(p2, p3);
        }
        return v0_1;
    }

    public void ns(String p2, String p3, int p4)
    {
        if (this.av != null) {
            this.av.ns(p2, p3, p4);
        }
        return;
    }
}
