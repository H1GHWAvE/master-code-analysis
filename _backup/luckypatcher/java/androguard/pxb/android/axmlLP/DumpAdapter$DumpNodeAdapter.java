package pxb.android.axmlLP;
public class DumpAdapter$DumpNodeAdapter extends pxb.android.axmlLP.AxmlVisitor$NodeVisitor {
    protected int deep;
    protected java.util.Map nses;

    public DumpAdapter$DumpNodeAdapter(pxb.android.axmlLP.AxmlVisitor$NodeVisitor p2)
    {
        this(p2);
        this.deep = 0;
        this.nses = 0;
        return;
    }

    public DumpAdapter$DumpNodeAdapter(pxb.android.axmlLP.AxmlVisitor$NodeVisitor p1, int p2, java.util.Map p3)
    {
        this(p1);
        this.deep = p2;
        this.nses = p3;
        return;
    }

    public void attr(String p9, String p10, int p11, int p12, Object p13)
    {
        int v0 = 0;
        while (v0 < this.deep) {
            System.out.print("  ");
            v0++;
        }
        if (p9 != null) {
            Object[] v3_0 = new Object[1];
            v3_0[0] = this.getPrefix(p9);
            System.out.print(String.format("%s:", v3_0));
        }
        System.out.print(p10);
        if (p11 != -1) {
            Object[] v3_1 = new Object[1];
            v3_1[0] = Integer.valueOf(p11);
            System.out.print(String.format("(%08x)", v3_1));
        }
        if (!(p13 instanceof String)) {
            if (!(p13 instanceof Boolean)) {
                Object[] v3_2 = new Object[2];
                v3_2[0] = Integer.valueOf(p12);
                v3_2[1] = p13;
                System.out.print(String.format("=[%08x]%08x", v3_2));
            } else {
                Object[] v3_3 = new Object[2];
                v3_3[0] = Integer.valueOf(p12);
                v3_3[1] = p13;
                System.out.print(String.format("=[%08x]\"%b\"", v3_3));
            }
        } else {
            Object[] v3_4 = new Object[2];
            v3_4[0] = Integer.valueOf(p12);
            v3_4[1] = p13;
            System.out.print(String.format("=[%08x]\"%s\"", v3_4));
        }
        System.out.println();
        super.attr(p9, p10, p11, p12, p13);
        return;
    }

    public pxb.android.axmlLP.AxmlVisitor$NodeVisitor child(String p6, String p7)
    {
        int v0 = 0;
        while (v0 < this.deep) {
            System.out.print("  ");
            v0++;
        }
        System.out.print("<");
        if (p6 != null) {
            System.out.println(new StringBuilder().append(this.getPrefix(p6)).append(":").toString());
        }
        int v2_4;
        System.out.println(p7);
        pxb.android.axmlLP.AxmlVisitor$NodeVisitor v1 = super.child(p6, p7);
        if (v1 == null) {
            v2_4 = 0;
        } else {
            v2_4 = new pxb.android.axmlLP.DumpAdapter$DumpNodeAdapter(v1, (this.deep + 1), this.nses);
        }
        return v2_4;
    }

    protected String getPrefix(String p3)
    {
        String v0_1;
        if (this.nses == null) {
            v0_1 = p3;
        } else {
            v0_1 = ((String) this.nses.get(p3));
            if (v0_1 == null) {
            }
        }
        return v0_1;
    }

    public void text(int p4, String p5)
    {
        int v0 = 0;
        while (v0 < (this.deep + 1)) {
            System.out.print("  ");
            v0++;
        }
        System.out.println(p5);
        super.text(p4, p5);
        return;
    }
}
