package pxb.android.axml;
public class Axml$Node extends pxb.android.axml.NodeVisitor {
    public java.util.List attrs;
    public java.util.List children;
    public Integer ln;
    public String name;
    public String ns;
    public pxb.android.axml.Axml$Node$Text text;

    public Axml$Node()
    {
        this.attrs = new java.util.ArrayList();
        this.children = new java.util.ArrayList();
        return;
    }

    public void accept(pxb.android.axml.NodeVisitor p4)
    {
        pxb.android.axml.NodeVisitor v0 = p4.child(this.ns, this.name);
        this.acceptB(v0);
        v0.end();
        return;
    }

    public void acceptB(pxb.android.axml.NodeVisitor p5)
    {
        if (this.text != null) {
            this.text.accept(p5);
        }
        java.util.Iterator v2_0 = this.attrs.iterator();
        while (v2_0.hasNext()) {
            ((pxb.android.axml.Axml$Node$Attr) v2_0.next()).accept(p5);
        }
        if (this.ln != null) {
            p5.line(this.ln.intValue());
        }
        java.util.Iterator v2_1 = this.children.iterator();
        while (v2_1.hasNext()) {
            ((pxb.android.axml.Axml$Node) v2_1.next()).accept(p5);
        }
        return;
    }

    public void attr(String p3, String p4, int p5, int p6, Object p7)
    {
        pxb.android.axml.Axml$Node$Attr v0_1 = new pxb.android.axml.Axml$Node$Attr();
        v0_1.name = p4;
        v0_1.ns = p3;
        v0_1.resourceId = p5;
        v0_1.type = p6;
        v0_1.value = p7;
        this.attrs.add(v0_1);
        return;
    }

    public pxb.android.axml.NodeVisitor child(String p3, String p4)
    {
        pxb.android.axml.Axml$Node v0_1 = new pxb.android.axml.Axml$Node();
        v0_1.name = p4;
        v0_1.ns = p3;
        this.children.add(v0_1);
        return v0_1;
    }

    public void line(int p2)
    {
        this.ln = Integer.valueOf(p2);
        return;
    }

    public void text(int p2, String p3)
    {
        pxb.android.axml.Axml$Node$Text v0_1 = new pxb.android.axml.Axml$Node$Text();
        v0_1.ln = p2;
        v0_1.text = p3;
        this.text = v0_1;
        return;
    }
}
