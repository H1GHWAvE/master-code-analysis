package pxb.android.axml;
public class DumpAdapter extends pxb.android.axml.AxmlVisitor {
    protected int deep;
    protected java.util.Map nses;

    public DumpAdapter()
    {
        this(0);
        return;
    }

    public DumpAdapter(pxb.android.axml.NodeVisitor p3)
    {
        this(p3, 0, new java.util.HashMap());
        return;
    }

    public DumpAdapter(pxb.android.axml.NodeVisitor p1, int p2, java.util.Map p3)
    {
        this(p1);
        this.deep = p2;
        this.nses = p3;
        return;
    }

    public void attr(String p10, String p11, int p12, int p13, Object p14)
    {
        int v0 = 0;
        while (v0 < this.deep) {
            System.out.print("  ");
            v0++;
        }
        if (p10 != null) {
            Object[] v4_0 = new Object[1];
            v4_0[0] = this.getPrefix(p10);
            System.out.print(String.format("%s:", v4_0));
        }
        System.out.print(p11);
        if (p12 != -1) {
            Object[] v4_1 = new Object[1];
            v4_1[0] = Integer.valueOf(p12);
            System.out.print(String.format("(%08x)", v4_1));
        }
        if (!(p14 instanceof String)) {
            if (!(p14 instanceof Boolean)) {
                if (!(p14 instanceof pxb.android.axml.ValueWrapper)) {
                    if (p13 != 1) {
                        Object[] v4_2 = new Object[2];
                        v4_2[0] = Integer.valueOf(p13);
                        v4_2[1] = p14;
                        System.out.print(String.format("=[%08x]%08x", v4_2));
                    } else {
                        Object[] v4_3 = new Object[2];
                        v4_3[0] = Integer.valueOf(p13);
                        v4_3[1] = p14;
                        System.out.print(String.format("=[%08x]@%08x", v4_3));
                    }
                } else {
                    Object[] v4_5 = new Object[3];
                    v4_5[0] = Integer.valueOf(p13);
                    v4_5[1] = Integer.valueOf(((pxb.android.axml.ValueWrapper) p14).ref);
                    v4_5[2] = ((pxb.android.axml.ValueWrapper) p14).raw;
                    System.out.print(String.format("=[%08x]@%08x, raw: \"%s\"", v4_5));
                }
            } else {
                Object[] v4_6 = new Object[2];
                v4_6[0] = Integer.valueOf(p13);
                v4_6[1] = p14;
                System.out.print(String.format("=[%08x]\"%b\"", v4_6));
            }
        } else {
            Object[] v4_7 = new Object[2];
            v4_7[0] = Integer.valueOf(p13);
            v4_7[1] = p14;
            System.out.print(String.format("=[%08x]\"%s\"", v4_7));
        }
        System.out.println();
        super.attr(p10, p11, p12, p13, p14);
        return;
    }

    public pxb.android.axml.NodeVisitor child(String p6, String p7)
    {
        int v0 = 0;
        while (v0 < this.deep) {
            System.out.print("  ");
            v0++;
        }
        System.out.print("<");
        if (p6 != null) {
            System.out.print(new StringBuilder().append(this.getPrefix(p6)).append(":").toString());
        }
        int v2_4;
        System.out.println(p7);
        pxb.android.axml.NodeVisitor v1 = super.child(p6, p7);
        if (v1 == null) {
            v2_4 = 0;
        } else {
            v2_4 = new pxb.android.axml.DumpAdapter(v1, (this.deep + 1), this.nses);
        }
        return v2_4;
    }

    protected String getPrefix(String p3)
    {
        String v0_1;
        if (this.nses == null) {
            v0_1 = p3;
        } else {
            v0_1 = ((String) this.nses.get(p3));
            if (v0_1 == null) {
            }
        }
        return v0_1;
    }

    public void ns(String p4, String p5, int p6)
    {
        System.out.println(new StringBuilder().append(p4).append("=").append(p5).toString());
        this.nses.put(p5, p4);
        super.ns(p4, p5, p6);
        return;
    }

    public void text(int p4, String p5)
    {
        int v0 = 0;
        while (v0 < (this.deep + 1)) {
            System.out.print("  ");
            v0++;
        }
        System.out.print("T: ");
        System.out.println(p5);
        super.text(p4, p5);
        return;
    }
}
