package pxb.android.axml;
public class AxmlReader {
    public static final pxb.android.axml.NodeVisitor EMPTY_VISITOR;
    final pxb.android.axml.AxmlParser parser;

    static AxmlReader()
    {
        pxb.android.axml.AxmlReader.EMPTY_VISITOR = new pxb.android.axml.AxmlReader$1();
        return;
    }

    public AxmlReader(byte[] p2)
    {
        this.parser = new pxb.android.axml.AxmlParser(p2);
        return;
    }

    public void accept(pxb.android.axml.AxmlVisitor p10)
    {
        java.util.Stack v7_1 = new java.util.Stack();
        pxb.android.axml.NodeVisitor v0_0 = p10;
        do {
            switch (this.parser.next()) {
                case 2:
                    v7_1.push(v0_0);
                    v0_0 = v0_0.child(this.parser.getNamespaceUri(), this.parser.getName());
                    if (v0_0 == null) {
                        v0_0 = pxb.android.axml.AxmlReader.EMPTY_VISITOR;
                    } else {
                        if (v0_0 != pxb.android.axml.AxmlReader.EMPTY_VISITOR) {
                            v0_0.line(this.parser.getLineNumber());
                            int v6 = 0;
                        }
                    }
                    break;
                case 3:
                    v0_0.end();
                    v0_0 = ((pxb.android.axml.NodeVisitor) v7_1.pop());
                    break;
                case 4:
                    p10.ns(this.parser.getNamespacePrefix(), this.parser.getNamespaceUri(), this.parser.getLineNumber());
                case 6:
                    v0_0.text(this.parser.getLineNumber(), this.parser.getText());
                    break;
                case 7:
                    return;
                default:
            }
        } while(v6 >= this.parser.getAttrCount());
        v0_0.attr(this.parser.getAttrNs(v6), this.parser.getAttrName(v6), this.parser.getAttrResId(v6), this.parser.getAttrType(v6), this.parser.getAttrValue(v6));
        v6++;
        while (v6 < this.parser.getAttrCount()) {
        }
    }
}
