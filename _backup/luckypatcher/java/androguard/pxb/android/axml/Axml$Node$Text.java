package pxb.android.axml;
public class Axml$Node$Text {
    public int ln;
    public String text;

    public Axml$Node$Text()
    {
        return;
    }

    public void accept(pxb.android.axml.NodeVisitor p3)
    {
        p3.text(this.ln, this.text);
        return;
    }
}
