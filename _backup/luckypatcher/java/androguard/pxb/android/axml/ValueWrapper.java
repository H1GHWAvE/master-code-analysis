package pxb.android.axml;
public class ValueWrapper {
    public static final int CLASS = 3;
    public static final int ID = 1;
    public static final int STYLE = 2;
    public final String raw;
    public final int ref;
    public final int type;

    private ValueWrapper(int p1, int p2, String p3)
    {
        this.type = p1;
        this.raw = p3;
        this.ref = p2;
        return;
    }

    public static pxb.android.axml.ValueWrapper wrapClass(int p2, String p3)
    {
        return new pxb.android.axml.ValueWrapper(3, p2, p3);
    }

    public static pxb.android.axml.ValueWrapper wrapId(int p2, String p3)
    {
        return new pxb.android.axml.ValueWrapper(1, p2, p3);
    }

    public static pxb.android.axml.ValueWrapper wrapStyle(int p2, String p3)
    {
        return new pxb.android.axml.ValueWrapper(2, p2, p3);
    }

    public pxb.android.axml.ValueWrapper replaceRaw(String p4)
    {
        return new pxb.android.axml.ValueWrapper(this.type, this.ref, p4);
    }
}
