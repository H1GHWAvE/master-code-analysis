package pxb.android.axml;
public class Axml extends pxb.android.axml.AxmlVisitor {
    public java.util.List firsts;
    public java.util.List nses;

    public Axml()
    {
        this.firsts = new java.util.ArrayList();
        this.nses = new java.util.ArrayList();
        return;
    }

    public void accept(pxb.android.axml.AxmlVisitor p5)
    {
        java.util.Iterator v1_0 = this.nses.iterator();
        while (v1_0.hasNext()) {
            ((pxb.android.axml.Axml$Ns) v1_0.next()).accept(p5);
        }
        java.util.Iterator v1_1 = this.firsts.iterator();
        while (v1_1.hasNext()) {
            ((pxb.android.axml.Axml$Node) v1_1.next()).accept(p5);
        }
        return;
    }

    public pxb.android.axml.NodeVisitor child(String p3, String p4)
    {
        pxb.android.axml.Axml$Node v0_1 = new pxb.android.axml.Axml$Node();
        v0_1.name = p4;
        v0_1.ns = p3;
        this.firsts.add(v0_1);
        return v0_1;
    }

    public void ns(String p3, String p4, int p5)
    {
        pxb.android.axml.Axml$Ns v0_1 = new pxb.android.axml.Axml$Ns();
        v0_1.prefix = p3;
        v0_1.uri = p4;
        v0_1.ln = p5;
        this.nses.add(v0_1);
        return;
    }
}
