package pxb.android.axml;
public class AxmlParser implements pxb.android.ResConst {
    public static final int END_FILE = 7;
    public static final int END_NS = 5;
    public static final int END_TAG = 3;
    public static final int START_FILE = 1;
    public static final int START_NS = 4;
    public static final int START_TAG = 2;
    public static final int TEXT = 6;
    private int attributeCount;
    private java.nio.IntBuffer attrs;
    private int classAttribute;
    private int fileSize;
    private int idAttribute;
    private java.nio.ByteBuffer in;
    private int lineNumber;
    private int nameIdx;
    private int nsIdx;
    private int prefixIdx;
    private int[] resourceIds;
    private String[] strings;
    private int styleAttribute;
    private int textIdx;

    public AxmlParser(java.nio.ByteBuffer p2)
    {
        this.fileSize = -1;
        this.in = p2.order(java.nio.ByteOrder.LITTLE_ENDIAN);
        return;
    }

    public AxmlParser(byte[] p2)
    {
        this(java.nio.ByteBuffer.wrap(p2));
        return;
    }

    public int getAttrCount()
    {
        return this.attributeCount;
    }

    public String getAttrName(int p4)
    {
        return this.strings[this.attrs.get(((p4 * 5) + 1))];
    }

    public String getAttrNs(int p4)
    {
        int v1_1;
        int v0 = this.attrs.get(((p4 * 5) + 0));
        if (v0 < 0) {
            v1_1 = 0;
        } else {
            v1_1 = this.strings[v0];
        }
        return v1_1;
    }

    String getAttrRawString(int p4)
    {
        int v1_1;
        int v0 = this.attrs.get(((p4 * 5) + 2));
        if (v0 < 0) {
            v1_1 = 0;
        } else {
            v1_1 = this.strings[v0];
        }
        return v1_1;
    }

    public int getAttrResId(int p4)
    {
        int v1_4;
        if (this.resourceIds == null) {
            v1_4 = -1;
        } else {
            int v0 = this.attrs.get(((p4 * 5) + 1));
            if ((v0 < 0) || (v0 >= this.resourceIds.length)) {
            } else {
                v1_4 = this.resourceIds[v0];
            }
        }
        return v1_4;
    }

    public int getAttrType(int p3)
    {
        return (this.attrs.get(((p3 * 5) + 3)) >> 24);
    }

    public Object getAttrValue(int p4)
    {
        Boolean v1_6;
        int v0 = this.attrs.get(((p4 * 5) + 4));
        if (p4 != this.idAttribute) {
            if (p4 != this.styleAttribute) {
                if (p4 != this.classAttribute) {
                    switch (this.getAttrType(p4)) {
                        case 3:
                            v1_6 = this.strings[v0];
                            break;
                        case 18:
                            Boolean v1_5;
                            if (v0 == 0) {
                                v1_5 = 0;
                            } else {
                                v1_5 = 1;
                            }
                            v1_6 = Boolean.valueOf(v1_5);
                            break;
                        default:
                            v1_6 = Integer.valueOf(v0);
                    }
                } else {
                    v1_6 = pxb.android.axml.ValueWrapper.wrapClass(v0, this.getAttrRawString(p4));
                }
            } else {
                v1_6 = pxb.android.axml.ValueWrapper.wrapStyle(v0, this.getAttrRawString(p4));
            }
        } else {
            v1_6 = pxb.android.axml.ValueWrapper.wrapId(v0, this.getAttrRawString(p4));
        }
        return v1_6;
    }

    public int getAttributeCount()
    {
        return this.attributeCount;
    }

    public int getLineNumber()
    {
        return this.lineNumber;
    }

    public String getName()
    {
        return this.strings[this.nameIdx];
    }

    public String getNamespacePrefix()
    {
        return this.strings[this.prefixIdx];
    }

    public String getNamespaceUri()
    {
        int v0_1;
        if (this.nsIdx < 0) {
            v0_1 = 0;
        } else {
            v0_1 = this.strings[this.nsIdx];
        }
        return v0_1;
    }

    public String getText()
    {
        return this.strings[this.textIdx];
    }

    public int next()
    {
        int v1;
        if (this.fileSize >= 0) {
            int v4 = this.in.position();
            while (v4 < this.fileSize) {
                int v6_0 = (this.in.getInt() & 65535);
                int v5 = this.in.getInt();
                switch (v6_0) {
                    case 1:
                        this.strings = pxb.android.StringItems.read(this.in);
                        this.in.position((v4 + v5));
                        break;
                    case 256:
                        this.lineNumber = this.in.getInt();
                        this.in.getInt();
                        this.prefixIdx = this.in.getInt();
                        this.nsIdx = this.in.getInt();
                        v1 = 4;
                        this.in.position((v4 + v5));
                        break;
                    case 257:
                        this.in.position((v4 + v5));
                        v1 = 5;
                        break;
                    case 258:
                        this.lineNumber = this.in.getInt();
                        this.in.getInt();
                        this.nsIdx = this.in.getInt();
                        this.nameIdx = this.in.getInt();
                        if (this.in.getInt() == 1310740) {
                            this.attributeCount = (this.in.getShort() & 65535);
                            this.idAttribute = ((this.in.getShort() & 65535) - 1);
                            this.classAttribute = ((this.in.getShort() & 65535) - 1);
                            this.styleAttribute = ((this.in.getShort() & 65535) - 1);
                            this.attrs = this.in.asIntBuffer();
                            v1 = 2;
                        } else {
                            throw new RuntimeException();
                        }
                        break;
                    case 259:
                        this.in.position((v4 + v5));
                        v1 = 3;
                        break;
                    case 260:
                        this.lineNumber = this.in.getInt();
                        this.in.getInt();
                        this.textIdx = this.in.getInt();
                        this.in.getInt();
                        this.in.getInt();
                        v1 = 6;
                        break;
                    case 384:
                        int v0 = ((v5 / 4) - 2);
                        java.nio.ByteBuffer v7_7 = new int[v0];
                        this.resourceIds = v7_7;
                        int v3 = 0;
                        while (v3 < v0) {
                            this.resourceIds[v3] = this.in.getInt();
                            v3++;
                        }
                        this.in.position((v4 + v5));
                        break;
                    default:
                        throw new RuntimeException();
                }
                v4 = this.in.position();
            }
            v1 = 7;
        } else {
            if ((this.in.getInt() & 65535) == 3) {
                this.fileSize = this.in.getInt();
                v1 = 1;
            } else {
                throw new RuntimeException();
            }
        }
        return v1;
    }
}
