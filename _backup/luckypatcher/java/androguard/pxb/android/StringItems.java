package pxb.android;
public class StringItems extends java.util.ArrayList {
    static final int UTF8_FLAG = 256;
    byte[] stringData;
    private boolean useUTF8;

    public StringItems()
    {
        this.useUTF8 = 1;
        return;
    }

    public static String[] read(java.nio.ByteBuffer p23)
    {
        int v17 = (p23.position() - 8);
        int v12 = p23.getInt();
        p23.getInt();
        int v6 = p23.getInt();
        int v13 = p23.getInt();
        int v16 = p23.getInt();
        int[] v9 = new int[v12];
        String[] v14 = new String[v12];
        int v7_0 = 0;
        while (v7_0 < v12) {
            v9[v7_0] = p23.getInt();
            v7_0++;
        }
        if (v16 != 0) {
            System.err.println(new StringBuilder().append("ignore style offset at 0x").append(Integer.toHexString(v17)).toString());
        }
        int v4 = (v17 + v13);
        int v7_1 = 0;
        while (v7_1 < v9.length) {
            String v10_1;
            p23.position((v9[v7_1] + v4));
            if ((v6 & 256) == 0) {
                v10_1 = new String(p23.array(), p23.position(), (pxb.android.StringItems.u16length(p23) * 2), "UTF-16LE");
            } else {
                pxb.android.StringItems.u8length(p23);
                int v18 = pxb.android.StringItems.u8length(p23);
                int v11 = p23.position();
                int v5 = v18;
                while (p23.get((v11 + v5)) != 0) {
                    v5++;
                }
                v10_1 = new String(p23.array(), v11, v5, "UTF-8");
            }
            v14[v7_1] = v10_1;
            v7_1++;
        }
        return v14;
    }

    static int u16length(java.nio.ByteBuffer p4)
    {
        int v0 = (p4.getShort() & 65535);
        if (v0 > 32767) {
            v0 = (((v0 & 32767) << 8) | (p4.getShort() & 65535));
        }
        return v0;
    }

    static int u8length(java.nio.ByteBuffer p3)
    {
        int v0 = (p3.get() & 255);
        if ((v0 & 128) != 0) {
            v0 = (((v0 & 127) << 8) | (p3.get() & 255));
        }
        return v0;
    }

    public int getSize()
    {
        return ((((this.size() * 4) + 20) + this.stringData.length) + 0);
    }

    public void prepare()
    {
        java.util.Iterator v5_0 = this.iterator();
        while (v5_0.hasNext()) {
            if (((pxb.android.StringItem) v5_0.next()).data.length() > 32767) {
                this.useUTF8 = 0;
            }
        }
        java.io.ByteArrayOutputStream v1_1 = new java.io.ByteArrayOutputStream();
        int v3 = 0;
        int v10 = 0;
        v1_1.reset();
        java.util.HashMap v8_1 = new java.util.HashMap();
        java.util.Iterator v5_1 = this.iterator();
        while (v5_1.hasNext()) {
            pxb.android.StringItem v6_1 = ((pxb.android.StringItem) v5_1.next());
            int v4 = (v3 + 1);
            v6_1.index = v3;
            String v12 = v6_1.data;
            Integer v9_1 = ((Integer) v8_1.get(v12));
            if (v9_1 == null) {
                v6_1.dataOffset = v10;
                v8_1.put(v12, Integer.valueOf(v10));
                if (!this.useUTF8) {
                    int v7_0 = v12.length();
                    byte[] v2_0 = v12.getBytes("UTF-16LE");
                    if (v7_0 > 32767) {
                        int v14 = ((v7_0 >> 16) | 32768);
                        v1_1.write(v14);
                        v1_1.write((v14 >> 8));
                        v10 += 2;
                    }
                    v1_1.write(v7_0);
                    v1_1.write((v7_0 >> 8));
                    v1_1.write(v2_0);
                    v1_1.write(0);
                    v1_1.write(0);
                    v10 += (v2_0.length + 4);
                } else {
                    int v7_1 = v12.length();
                    byte[] v2_1 = v12.getBytes("UTF-8");
                    int v13 = v2_1.length;
                    if (v7_1 > 127) {
                        v10++;
                        v1_1.write(((v7_1 >> 8) | 128));
                    }
                    v1_1.write(v7_1);
                    if (v13 > 127) {
                        v10++;
                        v1_1.write(((v13 >> 8) | 128));
                    }
                    v1_1.write(v13);
                    v1_1.write(v2_1);
                    v1_1.write(0);
                    v10 += (v13 + 3);
                }
            } else {
                v6_1.dataOffset = v9_1.intValue();
            }
            v3 = v4;
        }
        this.stringData = v1_1.toByteArray();
        return;
    }

    public void write(java.nio.ByteBuffer p5)
    {
        byte[] v2_2;
        p5.putInt(this.size());
        p5.putInt(0);
        if (!this.useUTF8) {
            v2_2 = 0;
        } else {
            v2_2 = 256;
        }
        p5.putInt(v2_2);
        p5.putInt(((this.size() * 4) + 28));
        p5.putInt(0);
        java.util.Iterator v0 = this.iterator();
        while (v0.hasNext()) {
            p5.putInt(((pxb.android.StringItem) v0.next()).dataOffset);
        }
        p5.put(this.stringData);
        return;
    }
}
