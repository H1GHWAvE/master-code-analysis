package pxb.android.arsc;
public class Config {
    public final int entryCount;
    public final byte[] id;
    public java.util.Map resources;
    int wChunkSize;
    int wEntryStart;
    int wPosition;

    public Config(byte[] p2, int p3)
    {
        this.resources = new java.util.TreeMap();
        this.id = p2;
        this.entryCount = p3;
        return;
    }
}
