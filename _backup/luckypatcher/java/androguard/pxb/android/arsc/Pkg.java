package pxb.android.arsc;
public class Pkg {
    public final int id;
    public String name;
    public java.util.TreeMap types;

    public Pkg(int p2, String p3)
    {
        this.types = new java.util.TreeMap();
        this.id = p2;
        this.name = p3;
        return;
    }

    public pxb.android.arsc.Type getType(int p4, String p5, int p6)
    {
        pxb.android.arsc.Type v0_1 = ((pxb.android.arsc.Type) this.types.get(Integer.valueOf(p4)));
        if (v0_1 == null) {
            v0_1 = new pxb.android.arsc.Type();
            v0_1.id = p4;
            v0_1.name = p5;
            RuntimeException v1_1 = new pxb.android.arsc.ResSpec[p6];
            v0_1.specs = v1_1;
            this.types.put(Integer.valueOf(p4), v0_1);
        } else {
            if (p5 != null) {
                if (v0_1.name != null) {
                    if (!p5.endsWith(v0_1.name)) {
                        throw new RuntimeException();
                    }
                } else {
                    v0_1.name = p5;
                }
                if (v0_1.specs.length != p6) {
                    throw new RuntimeException();
                }
            }
        }
        return v0_1;
    }
}
