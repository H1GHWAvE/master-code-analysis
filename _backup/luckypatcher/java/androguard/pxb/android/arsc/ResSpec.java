package pxb.android.arsc;
public class ResSpec {
    public int flags;
    public final int id;
    public String name;

    public ResSpec(int p1)
    {
        this.id = p1;
        return;
    }

    public void updateName(String p3)
    {
        String v0 = this.name;
        if (v0 != null) {
            if (!p3.equals(v0)) {
                throw new RuntimeException();
            }
        } else {
            this.name = p3;
        }
        return;
    }
}
