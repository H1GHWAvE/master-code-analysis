package pxb.android.arsc;
 class ArscParser$Chunk {
    public final int headSize;
    public final int location;
    public final int size;
    final synthetic pxb.android.arsc.ArscParser this$0;
    public final int type;

    public ArscParser$Chunk(pxb.android.arsc.ArscParser p5)
    {
        this.this$0 = p5;
        this.location = pxb.android.arsc.ArscParser.access$000(p5).position();
        this.type = (pxb.android.arsc.ArscParser.access$000(p5).getShort() & 65535);
        this.headSize = (pxb.android.arsc.ArscParser.access$000(p5).getShort() & 65535);
        this.size = pxb.android.arsc.ArscParser.access$000(p5).getInt();
        Object[] v1_2 = new Object[4];
        v1_2[0] = Integer.valueOf(this.location);
        v1_2[1] = Integer.valueOf(this.type);
        v1_2[2] = Integer.valueOf(this.headSize);
        v1_2[3] = Integer.valueOf(this.size);
        pxb.android.arsc.ArscParser.access$100("[%08x]type: %04x, headsize: %04x, size:%08x", v1_2);
        return;
    }
}
