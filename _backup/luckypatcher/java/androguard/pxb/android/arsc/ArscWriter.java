package pxb.android.arsc;
public class ArscWriter implements pxb.android.ResConst {
    private java.util.List ctxs;
    private java.util.List pkgs;
    private java.util.Map strTable;
    private pxb.android.StringItems strTable0;

    public ArscWriter(java.util.List p3)
    {
        this.ctxs = new java.util.ArrayList(5);
        this.strTable = new java.util.TreeMap();
        this.strTable0 = new pxb.android.StringItems();
        this.pkgs = p3;
        return;
    }

    private static varargs void D(String p0, Object[] p1)
    {
        return;
    }

    private void addString(String p3)
    {
        if (!this.strTable.containsKey(p3)) {
            pxb.android.StringItem v0_1 = new pxb.android.StringItem(p3);
            this.strTable.put(p3, v0_1);
            this.strTable0.add(v0_1);
        }
        return;
    }

    private int count()
    {
        int v15_0 = this.strTable0.getSize();
        if ((v15_0 % 4) != 0) {
            v15_0 += (4 - (v15_0 % 4));
        }
        int v13_1 = ((v15_0 + 8) + 12);
        java.util.Iterator v8 = this.ctxs.iterator();
        while (v8.hasNext()) {
            pxb.android.arsc.ArscWriter$PkgCtx v5_1 = ((pxb.android.arsc.ArscWriter$PkgCtx) v8.next());
            v5_1.offset = v13_1;
            v5_1.typeStringOff = ((0 + 268) + 16);
            int v15_1 = v5_1.typeNames0.getSize();
            if ((v15_1 % 4) != 0) {
                v15_1 += (4 - (v15_1 % 4));
            }
            int v12_3 = ((v15_1 + 8) + 284);
            v5_1.keyStringOff = v12_3;
            int v15_2 = v5_1.keyNames0.getSize();
            if ((v15_2 % 4) != 0) {
                v15_2 += (4 - (v15_2 % 4));
            }
            int v12_4 = (v12_3 + (v15_2 + 8));
            java.util.Iterator v9 = v5_1.pkg.types.values().iterator();
            while (v9.hasNext()) {
                pxb.android.arsc.Type v16_1 = ((pxb.android.arsc.Type) v9.next());
                v16_1.wPosition = (v13_1 + v12_4);
                v12_4 += ((v16_1.specs.length * 4) + 16);
                java.util.Iterator v10 = v16_1.configs.iterator();
                while (v10.hasNext()) {
                    pxb.android.arsc.Config v3_1 = ((pxb.android.arsc.Config) v10.next());
                    v3_1.wPosition = (v12_4 + v13_1);
                    int v4 = v12_4;
                    int v14 = v3_1.id.length;
                    if ((v14 % 4) != 0) {
                        v14 += (4 - (v14 % 4));
                    }
                    if ((((v12_4 + 20) + v14) - v4) <= 56) {
                        v12_4 = ((v4 + 56) + (v3_1.entryCount * 4));
                        v3_1.wEntryStart = (v12_4 - v4);
                        int v7 = v12_4;
                        java.util.Iterator v11 = v3_1.resources.values().iterator();
                        while (v11.hasNext()) {
                            pxb.android.arsc.ResEntry v6_1 = ((pxb.android.arsc.ResEntry) v11.next());
                            v6_1.wOffset = (v12_4 - v7);
                            int v12_8 = (v12_4 + 8);
                            if (!(v6_1.value instanceof pxb.android.arsc.BagValue)) {
                                v12_4 = (v12_8 + 8);
                            } else {
                                v12_4 = (v12_8 + ((((pxb.android.arsc.BagValue) v6_1.value).map.size() * 12) + 8));
                            }
                        }
                        v3_1.wChunkSize = (v12_4 - v4);
                    } else {
                        throw new RuntimeException("config id  too big");
                    }
                }
            }
            v5_1.pkgSize = v12_4;
            v13_1 += v12_4;
        }
        return v13_1;
    }

    public static varargs void main(String[] p5)
    {
        if (p5.length >= 2) {
            pxb.android.axml.Util.writeFile(new pxb.android.arsc.ArscWriter(new pxb.android.arsc.ArscParser(pxb.android.axml.Util.readFile(new java.io.File(p5[0]))).parse()).toByteArray(), new java.io.File(p5[1]));
        } else {
            System.err.println("asrc-write-test in.arsc out.arsc");
        }
        return;
    }

    private java.util.List prepare()
    {
        java.util.Iterator v4 = this.pkgs.iterator();
        while (v4.hasNext()) {
            pxb.android.arsc.Pkg v10_1 = ((pxb.android.arsc.Pkg) v4.next());
            pxb.android.arsc.ArscWriter$PkgCtx v2_1 = new pxb.android.arsc.ArscWriter$PkgCtx(0);
            v2_1.pkg = v10_1;
            this.ctxs.add(v2_1);
            java.util.Iterator v5 = v10_1.types.values().iterator();
            while (v5.hasNext()) {
                pxb.android.arsc.Type v12_1 = ((pxb.android.arsc.Type) v5.next());
                v2_1.addTypeName((v12_1.id - 1), v12_1.name);
                pxb.android.arsc.ResSpec[] v0 = v12_1.specs;
                int v8 = v0.length;
                java.util.Iterator v6_0 = 0;
                while (v6_0 < v8) {
                    v2_1.addKeyName(v0[v6_0].name);
                    v6_0++;
                }
                java.util.Iterator v6_1 = v12_1.configs.iterator();
                while (v6_1.hasNext()) {
                    java.util.Iterator v7 = ((pxb.android.arsc.Config) v6_1.next()).resources.values().iterator();
                    while (v7.hasNext()) {
                        pxb.android.arsc.Value v9_0 = ((pxb.android.arsc.ResEntry) v7.next()).value;
                        if (!(v9_0 instanceof pxb.android.arsc.BagValue)) {
                            this.travelValue(((pxb.android.arsc.Value) v9_0));
                        } else {
                            this.travelBagValue(((pxb.android.arsc.BagValue) v9_0));
                        }
                    }
                }
            }
            v2_1.keyNames0.prepare();
            v2_1.typeNames0.addAll(v2_1.typeNames);
            v2_1.typeNames0.prepare();
        }
        this.strTable0.prepare();
        return this.ctxs;
    }

    private void travelBagValue(pxb.android.arsc.BagValue p4)
    {
        java.util.Iterator v1 = p4.map.iterator();
        while (v1.hasNext()) {
            this.travelValue(((pxb.android.arsc.Value) ((java.util.Map$Entry) v1.next()).getValue()));
        }
        return;
    }

    private void travelValue(pxb.android.arsc.Value p2)
    {
        if (p2.raw != null) {
            this.addString(p2.raw);
        }
        return;
    }

    private void write(java.nio.ByteBuffer p32, int p33)
    {
        p32.putInt(786434);
        p32.putInt(p33);
        p32.putInt(this.ctxs.size());
        int v24_0 = this.strTable0.getSize();
        int v20_0 = 0;
        if ((v24_0 % 4) != 0) {
            v20_0 = (4 - (v24_0 % 4));
        }
        p32.putInt(1835009);
        p32.putInt(((v24_0 + v20_0) + 8));
        this.strTable0.write(p32);
        void v0_12 = new byte[v20_0];
        p32.put(v0_12);
        java.util.Iterator v12 = this.ctxs.iterator();
        while (v12.hasNext()) {
            pxb.android.arsc.ArscWriter$PkgCtx v21_1 = ((pxb.android.arsc.ArscWriter$PkgCtx) v12.next());
            if (p32.position() == v21_1.offset) {
                int v5 = p32.position();
                p32.putInt(18612736);
                p32.putInt(v21_1.pkgSize);
                p32.putInt(v21_1.pkg.id);
                int v19 = p32.position();
                p32.put(v21_1.pkg.name.getBytes("UTF-16LE"));
                p32.position((v19 + 256));
                p32.putInt(v21_1.typeStringOff);
                p32.putInt(v21_1.typeNames0.size());
                p32.putInt(v21_1.keyStringOff);
                p32.putInt(v21_1.keyNames0.size());
                if ((p32.position() - v5) == v21_1.typeStringOff) {
                    int v24_1 = v21_1.typeNames0.getSize();
                    int v20_1 = 0;
                    if ((v24_1 % 4) != 0) {
                        v20_1 = (4 - (v24_1 % 4));
                    }
                    p32.putInt(1835009);
                    p32.putInt(((v24_1 + v20_1) + 8));
                    v21_1.typeNames0.write(p32);
                    void v0_59 = new byte[v20_1];
                    p32.put(v0_59);
                    if ((p32.position() - v5) == v21_1.keyStringOff) {
                        int v24_2 = v21_1.keyNames0.getSize();
                        int v20_2 = 0;
                        if ((v24_2 % 4) != 0) {
                            v20_2 = (4 - (v24_2 % 4));
                        }
                        p32.putInt(1835009);
                        p32.putInt(((v24_2 + v20_2) + 8));
                        v21_1.keyNames0.write(p32);
                        void v0_72 = new byte[v20_2];
                        p32.put(v0_72);
                        java.util.Iterator v13 = v21_1.pkg.types.values().iterator();
                        while (v13.hasNext()) {
                            pxb.android.arsc.Type v25_1 = ((pxb.android.arsc.Type) v13.next());
                            void v0_79 = new Object[2];
                            String v28_5 = v0_79;
                            v28_5[0] = Integer.valueOf(p32.position());
                            v28_5[1] = v25_1.name;
                            pxb.android.arsc.ArscWriter.D("[%08x]write spec", v28_5);
                            if (v25_1.wPosition == p32.position()) {
                                p32.putInt(1049090);
                                p32.putInt(((v25_1.specs.length * 4) + 16));
                                p32.putInt(v25_1.id);
                                p32.putInt(v25_1.specs.length);
                                pxb.android.arsc.ResSpec[] v3 = v25_1.specs;
                                int v18 = v3.length;
                                java.util.Iterator v14_0 = 0;
                                while (v14_0 < v18) {
                                    p32.putInt(v3[v14_0].flags);
                                    v14_0++;
                                }
                                java.util.Iterator v14_1 = v25_1.configs.iterator();
                                while (v14_1.hasNext()) {
                                    pxb.android.arsc.Config v6_1 = ((pxb.android.arsc.Config) v14_1.next());
                                    void v0_105 = new Object[1];
                                    String v28_8 = v0_105;
                                    v28_8[0] = Integer.valueOf(p32.position());
                                    pxb.android.arsc.ArscWriter.D("[%08x]write config", v28_8);
                                    int v26 = p32.position();
                                    if (v6_1.wPosition == v26) {
                                        p32.putInt(3670529);
                                        p32.putInt(v6_1.wChunkSize);
                                        p32.putInt(v25_1.id);
                                        p32.putInt(v25_1.specs.length);
                                        p32.putInt(v6_1.wEntryStart);
                                        void v0_122 = new Object[1];
                                        String v28_10 = v0_122;
                                        v28_10[0] = Integer.valueOf(p32.position());
                                        pxb.android.arsc.ArscWriter.D("[%08x]write config ids", v28_10);
                                        p32.put(v6_1.id);
                                        int v22 = v6_1.id.length;
                                        int v20_3 = 0;
                                        if ((v22 % 4) != 0) {
                                            v20_3 = (4 - (v22 % 4));
                                        }
                                        void v0_129 = new byte[v20_3];
                                        p32.put(v0_129);
                                        p32.position((v26 + 56));
                                        void v0_133 = new Object[1];
                                        String v28_12 = v0_133;
                                        v28_12[0] = Integer.valueOf(p32.position());
                                        pxb.android.arsc.ArscWriter.D("[%08x]write config entry offsets", v28_12);
                                        int v11 = 0;
                                        while (v11 < v6_1.entryCount) {
                                            pxb.android.arsc.ResEntry v8_1 = ((pxb.android.arsc.ResEntry) v6_1.resources.get(Integer.valueOf(v11)));
                                            if (v8_1 != null) {
                                                p32.putInt(v8_1.wOffset);
                                            } else {
                                                p32.putInt(-1);
                                            }
                                            v11++;
                                        }
                                        if ((p32.position() - v26) == v6_1.wEntryStart) {
                                            void v0_139 = new Object[1];
                                            String v28_15 = v0_139;
                                            v28_15[0] = Integer.valueOf(p32.position());
                                            pxb.android.arsc.ArscWriter.D("[%08x]write config entrys", v28_15);
                                            java.util.Iterator v15 = v6_1.resources.values().iterator();
                                            while (v15.hasNext()) {
                                                pxb.android.arsc.Value v27_89;
                                                pxb.android.arsc.ResEntry v7_1 = ((pxb.android.arsc.ResEntry) v15.next());
                                                void v0_142 = new Object[1];
                                                String v28_17 = v0_142;
                                                v28_17[0] = Integer.valueOf(p32.position());
                                                pxb.android.arsc.ArscWriter.D("[%08x]ResTable_entry", v28_17);
                                                boolean v17 = (v7_1.value instanceof pxb.android.arsc.BagValue);
                                                if (!v17) {
                                                    v27_89 = 8;
                                                } else {
                                                    v27_89 = 16;
                                                }
                                                int v10_1;
                                                p32.putShort(((short) v27_89));
                                                int v10_0 = v7_1.flag;
                                                if (!v17) {
                                                    v10_1 = (v10_0 & -2);
                                                } else {
                                                    v10_1 = (v10_0 | 1);
                                                }
                                                p32.putShort(((short) v10_1));
                                                p32.putInt(((pxb.android.StringItem) v21_1.keyNames.get(v7_1.spec.name)).index);
                                                if (!v17) {
                                                    this.writeValue(((pxb.android.arsc.Value) v7_1.value), p32);
                                                } else {
                                                    pxb.android.arsc.BagValue v4_1 = ((pxb.android.arsc.BagValue) v7_1.value);
                                                    p32.putInt(v4_1.parent);
                                                    p32.putInt(v4_1.map.size());
                                                    java.util.Iterator v16 = v4_1.map.iterator();
                                                    while (v16.hasNext()) {
                                                        java.util.Map$Entry v9_1 = ((java.util.Map$Entry) v16.next());
                                                        p32.putInt(((Integer) v9_1.getKey()).intValue());
                                                        this.writeValue(((pxb.android.arsc.Value) v9_1.getValue()), p32);
                                                    }
                                                }
                                            }
                                        } else {
                                            throw new RuntimeException();
                                        }
                                    } else {
                                        throw new RuntimeException();
                                    }
                                }
                            } else {
                                throw new RuntimeException();
                            }
                        }
                    } else {
                        throw new RuntimeException();
                    }
                } else {
                    throw new RuntimeException();
                }
            } else {
                throw new RuntimeException();
            }
        }
        return;
    }

    private void writeValue(pxb.android.arsc.Value p3, java.nio.ByteBuffer p4)
    {
        p4.putShort(8);
        p4.put(0);
        p4.put(((byte) p3.type));
        if (p3.type != 3) {
            p4.putInt(p3.data);
        } else {
            p4.putInt(((pxb.android.StringItem) this.strTable.get(p3.raw)).index);
        }
        return;
    }

    public byte[] toByteArray()
    {
        this.prepare();
        int v1 = this.count();
        java.nio.ByteBuffer v0 = java.nio.ByteBuffer.allocate(v1).order(java.nio.ByteOrder.LITTLE_ENDIAN);
        this.write(v0, v1);
        return v0.array();
    }
}
