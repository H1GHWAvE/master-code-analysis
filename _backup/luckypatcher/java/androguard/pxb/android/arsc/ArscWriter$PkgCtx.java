package pxb.android.arsc;
 class ArscWriter$PkgCtx {
    java.util.Map keyNames;
    pxb.android.StringItems keyNames0;
    public int keyStringOff;
    int offset;
    pxb.android.arsc.Pkg pkg;
    int pkgSize;
    java.util.List typeNames;
    pxb.android.StringItems typeNames0;
    int typeStringOff;

    private ArscWriter$PkgCtx()
    {
        this.keyNames = new java.util.HashMap();
        this.keyNames0 = new pxb.android.StringItems();
        this.typeNames = new java.util.ArrayList();
        this.typeNames0 = new pxb.android.StringItems();
        return;
    }

    synthetic ArscWriter$PkgCtx(pxb.android.arsc.ArscWriter$1 p1)
    {
        return;
    }

    public void addKeyName(String p3)
    {
        if (!this.keyNames.containsKey(p3)) {
            pxb.android.StringItem v0_1 = new pxb.android.StringItem(p3);
            this.keyNames.put(p3, v0_1);
            this.keyNames0.add(v0_1);
        }
        return;
    }

    public void addTypeName(int p4, String p5)
    {
        while (this.typeNames.size() <= p4) {
            this.typeNames.add(0);
        }
        if (((pxb.android.StringItem) this.typeNames.get(p4)) != null) {
            throw new RuntimeException();
        } else {
            this.typeNames.set(p4, new pxb.android.StringItem(p5));
            return;
        }
    }
}
