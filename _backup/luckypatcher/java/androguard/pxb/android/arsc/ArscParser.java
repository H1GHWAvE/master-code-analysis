package pxb.android.arsc;
public class ArscParser implements pxb.android.ResConst {
    private static final boolean DEBUG = False;
    static final int ENGRY_FLAG_PUBLIC = 2;
    static final short ENTRY_FLAG_COMPLEX = 1;
    public static final int TYPE_STRING = 3;
    private int fileSize;
    private java.nio.ByteBuffer in;
    private String[] keyNamesX;
    private pxb.android.arsc.Pkg pkg;
    private java.util.List pkgs;
    private String[] strings;
    private String[] typeNamesX;

    public ArscParser(byte[] p3)
    {
        this.fileSize = -1;
        this.pkgs = new java.util.ArrayList();
        this.in = java.nio.ByteBuffer.wrap(p3).order(java.nio.ByteOrder.LITTLE_ENDIAN);
        return;
    }

    private static varargs void D(String p0, Object[] p1)
    {
        return;
    }

    static synthetic java.nio.ByteBuffer access$000(pxb.android.arsc.ArscParser p1)
    {
        return p1.in;
    }

    static synthetic void access$100(String p0, Object[] p1)
    {
        pxb.android.arsc.ArscParser.D(p0, p1);
        return;
    }

    private void readEntry(pxb.android.arsc.Config p14, pxb.android.arsc.ResSpec p15)
    {
        Object v10_1 = new Object[1];
        v10_1[0] = Integer.valueOf(this.in.position());
        pxb.android.arsc.ArscParser.D("[%08x]read ResTable_entry", v10_1);
        Object v10_3 = new Object[1];
        v10_3[0] = Integer.valueOf(this.in.getShort());
        pxb.android.arsc.ArscParser.D("ResTable_entry %d", v10_3);
        short v3 = this.in.getShort();
        p15.updateName(this.keyNamesX[this.in.getInt()]);
        pxb.android.arsc.ResEntry v7_1 = new pxb.android.arsc.ResEntry(v3, p15);
        if ((v3 & 1) == 0) {
            v7_1.value = this.readValue();
        } else {
            int v6 = this.in.getInt();
            int v1 = this.in.getInt();
            pxb.android.arsc.BagValue v0_1 = new pxb.android.arsc.BagValue(v6);
            int v4 = 0;
            while (v4 < v1) {
                v0_1.map.add(new java.util.AbstractMap$SimpleEntry(Integer.valueOf(this.in.getInt()), this.readValue()));
                v4++;
            }
            v7_1.value = v0_1;
        }
        p14.resources.put(Integer.valueOf(p15.id), v7_1);
        return;
    }

    private void readPackage(java.nio.ByteBuffer p29)
    {
        int v14 = (p29.getInt() % 255);
        int v12 = (p29.position() + 256);
        StringBuilder v16 = new StringBuilder;
        v16(32);
        int v9_0 = 0;
        while (v9_0 < 128) {
            short v15 = p29.getShort();
            if (v15 == 0) {
                break;
            }
            v16.append(((char) v15));
            v9_0++;
        }
        String v11 = v16.toString();
        p29.position(v12);
        int v24_5 = new pxb.android.arsc.Pkg;
        v24_5(v14, v11);
        this.pkg = v24_5;
        this.pkgs.add(this.pkg);
        p29.getInt();
        p29.getInt();
        p29.getInt();
        p29.getInt();
        pxb.android.arsc.ArscParser$Chunk v3_1 = new pxb.android.arsc.ArscParser$Chunk(this);
        if (v3_1.type == 1) {
            this.typeNamesX = pxb.android.StringItems.read(p29);
            p29.position((v3_1.location + v3_1.size));
            pxb.android.arsc.ArscParser$Chunk v3_3 = new pxb.android.arsc.ArscParser$Chunk(this);
            if (v3_3.type == 1) {
                this.keyNamesX = pxb.android.StringItems.read(p29);
                p29.position((v3_3.location + v3_3.size));
                while (p29.hasRemaining()) {
                    pxb.android.arsc.ArscParser$Chunk v3_5 = new pxb.android.arsc.ArscParser$Chunk(this);
                    switch (v3_5.type) {
                        case 513:
                            void v0_41 = new Object[1];
                            int v25_11 = v0_41;
                            v25_11[0] = Integer.valueOf((p29.position() - 8));
                            pxb.android.arsc.ArscParser.D("[%08x]read config", v25_11);
                            int v21_1 = (p29.get() & 255);
                            p29.get();
                            p29.getShort();
                            int v7_1 = p29.getInt();
                            pxb.android.arsc.Type v20_1 = this.pkg.getType(v21_1, this.typeNamesX[(v21_1 - 1)], v7_1);
                            int v6 = p29.getInt();
                            void v0_50 = new Object[1];
                            int v25_15 = v0_50;
                            v25_15[0] = Integer.valueOf(p29.position());
                            pxb.android.arsc.ArscParser.D("[%08x]read config id", v25_15);
                            byte[] v5 = new byte[p29.getInt()];
                            p29.position(p29.position());
                            p29.get(v5);
                            pxb.android.arsc.Config v4_1 = new pxb.android.arsc.Config(v5, v7_1);
                            p29.position((v3_5.location + v3_5.headSize));
                            void v0_58 = new Object[1];
                            int v25_18 = v0_58;
                            v25_18[0] = Integer.valueOf(p29.position());
                            pxb.android.arsc.ArscParser.D("[%08x]read config entry offset", v25_18);
                            int[] v8 = new int[v7_1];
                            int v9_2 = 0;
                            while (v9_2 < v7_1) {
                                v8[v9_2] = p29.getInt();
                                v9_2++;
                            }
                            void v0_60 = new Object[1];
                            int v25_20 = v0_60;
                            v25_20[0] = Integer.valueOf(p29.position());
                            pxb.android.arsc.ArscParser.D("[%08x]read config entrys", v25_20);
                            int v9_3 = 0;
                            while (v9_3 < v8.length) {
                                if (v8[v9_3] != -1) {
                                    p29.position(((v3_5.location + v6) + v8[v9_3]));
                                    this.readEntry(v4_1, v20_1.getSpec(v9_3));
                                }
                                v9_3++;
                            }
                            v20_1.addConfig(v4_1);
                            p29.position((v3_5.location + v3_5.size));
                            break;
                        case 514:
                            void v0_30 = new Object[1];
                            int v25_6 = v0_30;
                            v25_6[0] = Integer.valueOf((p29.position() - 8));
                            pxb.android.arsc.ArscParser.D("[%08x]read spec", v25_6);
                            int v21_0 = (p29.get() & 255);
                            p29.get();
                            p29.getShort();
                            int v7_0 = p29.getInt();
                            pxb.android.arsc.Type v20_0 = this.pkg.getType(v21_0, this.typeNamesX[(v21_0 - 1)], v7_0);
                            int v9_1 = 0;
                            while (v9_1 < v7_0) {
                                v20_0.getSpec(v9_1).flags = p29.getInt();
                                v9_1++;
                            }
                            break;
                    }
                }
                return;
            } else {
                throw new RuntimeException();
            }
        } else {
            throw new RuntimeException();
        }
    }

    private Object readValue()
    {
        this.in.getShort();
        this.in.get();
        int v3 = (this.in.get() & 255);
        int v0 = this.in.getInt();
        String v1 = 0;
        if (v3 == 3) {
            v1 = this.strings[v0];
        }
        return new pxb.android.arsc.Value(v3, v0, v1);
    }

    public java.util.List parse()
    {
        if (this.fileSize < 0) {
            pxb.android.arsc.ArscParser$Chunk v1_1 = new pxb.android.arsc.ArscParser$Chunk(this);
            if (v1_1.type == 2) {
                this.fileSize = v1_1.size;
                this.in.getInt();
            } else {
                throw new RuntimeException();
            }
        }
        while (this.in.hasRemaining()) {
            pxb.android.arsc.ArscParser$Chunk v0_1 = new pxb.android.arsc.ArscParser$Chunk(this);
            switch (v0_1.type) {
                case 1:
                    this.strings = pxb.android.StringItems.read(this.in);
                    break;
                case 512:
                    this.readPackage(this.in);
                    break;
            }
            this.in.position((v0_1.location + v0_1.size));
        }
        return this.pkgs;
    }
}
