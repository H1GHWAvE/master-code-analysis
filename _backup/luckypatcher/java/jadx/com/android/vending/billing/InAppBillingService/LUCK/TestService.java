package com.android.vending.billing.InAppBillingService.LUCK;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;
import com.android.vending.billing.InAppBillingService.LUCK.ITestServiceInterface.Stub;

public class TestService extends Service {
    private final Stub mBinder = new C06541();
    ITestServiceInterface mService;

    class C06541 extends Stub {
        C06541() {
        }

        public boolean checkService() throws RemoteException {
            return true;
        }
    }

    public IBinder onBind(Intent intent) {
        return this.mBinder;
    }
}
