package com.android.vending.billing.InAppBillingService.LUCK;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnKeyListener;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import com.android.vending.billing.InAppBillingService.LUCK.widgets.BinderWidget;
import com.chelpus.Utils;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import net.lingala.zip4j.util.InternalZipConstants;

public class BinderActivity extends Activity {
    public ArrayList<BindItem> bindes = null;
    public Context context;
    private ItemFile current;
    private ListView filebrowser;
    public ListView lv = null;
    private TextView myPath;
    public Dialog pp4 = null;
    private String root;
    public int sizeText = 0;

    class C00782 implements OnClickListener {

        class C00774 implements OnCancelListener {
            C00774() {
            }

            public void onCancel(DialogInterface dialog) {
            }
        }

        C00782() {
        }

        public void onClick(View v) {
            final RelativeLayout d = (RelativeLayout) View.inflate(BinderActivity.this.context, C0149R.layout.binderdialog, null);
            final EditText ed1 = (EditText) d.findViewById(C0149R.id.editText1);
            final EditText ed2 = (EditText) d.findViewById(C0149R.id.editText2);
            Button but_browser1 = (Button) d.findViewById(C0149R.id.button_browser1);
            Button but_browser2 = (Button) d.findViewById(C0149R.id.button_browser2);
            OnClickListener clickListener2 = new OnClickListener() {
                public void onClick(View v) {
                    String datadir = ed1.getText().toString();
                    String targetdir = ed2.getText().toString();
                    if (datadir.endsWith(InternalZipConstants.ZIP_FILE_SEPARATOR)) {
                        datadir = datadir.trim();
                    } else {
                        datadir = datadir.trim() + InternalZipConstants.ZIP_FILE_SEPARATOR;
                    }
                    if (targetdir.endsWith(InternalZipConstants.ZIP_FILE_SEPARATOR)) {
                        targetdir = targetdir.trim();
                    } else {
                        targetdir = targetdir.trim() + InternalZipConstants.ZIP_FILE_SEPARATOR;
                    }
                    if (!new File(datadir.trim()).exists() || targetdir.equals(InternalZipConstants.ZIP_FILE_SEPARATOR) || datadir.equals(InternalZipConstants.ZIP_FILE_SEPARATOR)) {
                        Toast.makeText(BinderActivity.this.getApplication().getApplicationContext(), Utils.getText(C0149R.string.bind_error), 1).show();
                        return;
                    }
                    if (listAppsFragment.su) {
                        final String send_datadir = datadir;
                        final String send_targetdir = targetdir;
                        new Thread(new Runnable() {
                            public void run() {
                                Utils.verify_bind_and_run("mount", "-o bind '" + send_datadir + "' '" + send_targetdir + "'", send_datadir, send_targetdir);
                            }
                        }).start();
                    } else {
                        Utils.cmd("umount '" + targetdir + "'");
                        Utils.cmd("mount -o bind '" + datadir + "' '" + targetdir + "'");
                    }
                    boolean found = false;
                    Iterator it = BinderActivity.this.bindes.iterator();
                    while (it.hasNext()) {
                        BindItem item = (BindItem) it.next();
                        if (item.TargetDir.equals(targetdir)) {
                            item.SourceDir = datadir;
                            item.TargetDir = targetdir;
                            found = true;
                        }
                    }
                    if (!found) {
                        BinderActivity.this.bindes.add(new BindItem(datadir, targetdir));
                    }
                    BinderActivity.savetoFile(BinderActivity.this.bindes, BinderActivity.this.context);
                    listAppsFragment.adaptBind.notifyDataSetChanged();
                    BinderActivity.this.lv.setAdapter(listAppsFragment.adaptBind);
                    if (BinderActivity.this.pp4.isShowing()) {
                        BinderActivity.this.pp4.dismiss();
                    }
                }
            };
            OnClickListener clickListener_browser1 = new OnClickListener() {

                class C00651 implements OnKeyListener {
                    C00651() {
                    }

                    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                        System.out.println(event);
                        if (keyCode == 4 && event.getAction() == 1) {
                            try {
                                if (((ItemFile) BinderActivity.this.filebrowser.getAdapter().getItem(1)).file.equals("../")) {
                                    BinderActivity.this.getDir(((ItemFile) BinderActivity.this.filebrowser.getAdapter().getItem(1)).path, BinderActivity.this.filebrowser);
                                } else {
                                    dialog.dismiss();
                                }
                            } catch (IndexOutOfBoundsException e) {
                                dialog.dismiss();
                            } catch (Exception e2) {
                                dialog.dismiss();
                            }
                        }
                        return true;
                    }
                }

                class C00694 implements OnItemClickListener {
                    C00694() {
                    }

                    public void onItemClick(AdapterView<?> l, View arg1, int position, long id) {
                        ItemFile item = (ItemFile) l.getAdapter().getItem(position);
                        File file = new File(item.full);
                        if (file.isDirectory()) {
                            BinderActivity.this.current = item;
                            if (!file.canRead() || file.listFiles() == null) {
                                Utils.showDialog(new Builder(BinderActivity.this.context).setIcon(17301659).setTitle("[" + file.getName() + "] folder can't be read!").setPositiveButton("OK", null).create());
                                return;
                            }
                            BinderActivity.this.filebrowser = (ListView) l;
                            BinderActivity.this.getDir(new File(item.full).getPath(), (ListView) l);
                        }
                    }
                }

                public void onClick(View v) {
                    final LinearLayout dListView = (LinearLayout) View.inflate(BinderActivity.this.context, C0149R.layout.binder_file_browser, null);
                    AlertDlg tempdialogbuilder = new AlertDlg(BinderActivity.this.context, true);
                    tempdialogbuilder.setView(dListView);
                    tempdialogbuilder.setCancelable(false);
                    final Dialog tempdialog = tempdialogbuilder.create();
                    tempdialog.setOnKeyListener(new C00651());
                    Utils.showDialog(tempdialog);
                    BinderActivity.this.myPath = (TextView) tempdialog.findViewById(C0149R.id.path);
                    Button select_but = (Button) tempdialog.findViewById(C0149R.id.button_select);
                    Button create_but = (Button) tempdialog.findViewById(C0149R.id.button_create_dir);
                    BinderActivity.this.myPath.setTextAppearance(BinderActivity.this.context, listAppsFragment.getSizeText());
                    create_but.setOnClickListener(new OnClickListener() {
                        public void onClick(View v) {
                            final Dialog dialog = new Dialog(BinderActivity.this.context);
                            dialog.setTitle(Utils.getText(C0149R.string.enter_name_dir));
                            RelativeLayout d1 = (RelativeLayout) View.inflate(BinderActivity.this.context, C0149R.layout.binder_create_dir, null);
                            dialog.setContentView(d1);
                            final EditText ed = (EditText) d1.findViewById(C0149R.id.new_dir);
                            ((Button) dialog.findViewById(C0149R.id.buttonCreate)).setOnClickListener(new OnClickListener() {
                                public void onClick(View v) {
                                    String dir_name = ed.getText().toString().trim().replaceAll(InternalZipConstants.ZIP_FILE_SEPARATOR, BuildConfig.VERSION_NAME) + InternalZipConstants.ZIP_FILE_SEPARATOR;
                                    if (dir_name.replaceAll(" ", BuildConfig.VERSION_NAME).equals(BuildConfig.VERSION_NAME)) {
                                        Toast.makeText(listAppsFragment.getInstance(), Utils.getText(C0149R.string.create_dir_error), 1).show();
                                        return;
                                    }
                                    String full_dir_name = BuildConfig.VERSION_NAME;
                                    if (BinderActivity.this.current.path.endsWith(InternalZipConstants.ZIP_FILE_SEPARATOR)) {
                                        full_dir_name = BinderActivity.this.current.path + dir_name;
                                    } else {
                                        full_dir_name = BinderActivity.this.current.path + InternalZipConstants.ZIP_FILE_SEPARATOR + dir_name;
                                    }
                                    new File(full_dir_name).mkdirs();
                                    if (Utils.exists(full_dir_name) || new File(full_dir_name).exists()) {
                                        dialog.cancel();
                                        BinderActivity.this.getDir(full_dir_name, (ListView) dListView.findViewById(C0149R.id.list));
                                        return;
                                    }
                                    Toast.makeText(listAppsFragment.getInstance(), Utils.getText(C0149R.string.create_dir_error_2), 1).show();
                                }
                            });
                            Utils.showDialog(dialog);
                        }
                    });
                    select_but.setOnClickListener(new OnClickListener() {
                        public void onClick(View v) {
                            if (BinderActivity.this.current.path.endsWith(InternalZipConstants.ZIP_FILE_SEPARATOR)) {
                                ed1.setText(BinderActivity.this.current.path);
                            } else {
                                ed1.setText(BinderActivity.this.current.path + InternalZipConstants.ZIP_FILE_SEPARATOR);
                            }
                            if (tempdialog.isShowing()) {
                                tempdialog.dismiss();
                            }
                        }
                    });
                    BinderActivity.this.root = InternalZipConstants.ZIP_FILE_SEPARATOR;
                    ((ListView) dListView.findViewById(C0149R.id.list)).setOnItemClickListener(new C00694());
                    BinderActivity.this.filebrowser = (ListView) dListView.findViewById(C0149R.id.list);
                    try {
                        BinderActivity.this.getDir(BinderActivity.this.root, (ListView) dListView.findViewById(C0149R.id.list));
                    } catch (Exception e) {
                        try {
                            BinderActivity.this.root = new File(listAppsFragment.basepath).getParent();
                            BinderActivity.this.getDir(BinderActivity.this.root, (ListView) d.findViewById(C0149R.id.list));
                        } catch (Exception e2) {
                            BinderActivity.this.root = listAppsFragment.basepath;
                            BinderActivity.this.getDir(BinderActivity.this.root, (ListView) d.findViewById(C0149R.id.list));
                        }
                    }
                }
            };
            OnClickListener clickListener_browser2 = new OnClickListener() {

                class C00711 implements OnKeyListener {
                    C00711() {
                    }

                    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                        System.out.println(event);
                        if (keyCode == 4 && event.getAction() == 1) {
                            try {
                                if (((ItemFile) BinderActivity.this.filebrowser.getAdapter().getItem(1)).file.equals("../")) {
                                    BinderActivity.this.getDir(((ItemFile) BinderActivity.this.filebrowser.getAdapter().getItem(1)).path, BinderActivity.this.filebrowser);
                                } else {
                                    dialog.dismiss();
                                }
                            } catch (IndexOutOfBoundsException e) {
                                dialog.dismiss();
                            } catch (Exception e2) {
                                dialog.dismiss();
                            }
                        }
                        return true;
                    }
                }

                class C00754 implements OnItemClickListener {
                    C00754() {
                    }

                    public void onItemClick(AdapterView<?> l, View arg1, int position, long id) {
                        ItemFile item = (ItemFile) l.getAdapter().getItem(position);
                        File file = new File(item.full);
                        if (file.isDirectory()) {
                            BinderActivity.this.current = item;
                            if (!file.canRead() || file.listFiles() == null) {
                                Utils.showDialog(new Builder(BinderActivity.this.context).setIcon(17301659).setTitle("[" + file.getName() + "] folder can't be read!").setPositiveButton("OK", null).create());
                                return;
                            }
                            BinderActivity.this.filebrowser = (ListView) l;
                            BinderActivity.this.getDir(new File(item.full).getPath(), (ListView) l);
                        }
                    }
                }

                public void onClick(View v) {
                    final LinearLayout dListView = (LinearLayout) View.inflate(BinderActivity.this.context, C0149R.layout.binder_file_browser, null);
                    final Dialog tempdialog = new AlertDlg(BinderActivity.this.context, true).setView(dListView).create();
                    tempdialog.setCancelable(false);
                    tempdialog.setOnKeyListener(new C00711());
                    Utils.showDialog(tempdialog);
                    BinderActivity.this.myPath = (TextView) tempdialog.findViewById(C0149R.id.path);
                    Button select_but = (Button) tempdialog.findViewById(C0149R.id.button_select);
                    ((Button) tempdialog.findViewById(C0149R.id.button_create_dir)).setOnClickListener(new OnClickListener() {
                        public void onClick(View v) {
                            final Dialog dialog = new Dialog(BinderActivity.this.context);
                            dialog.setTitle(Utils.getText(C0149R.string.enter_name_dir));
                            RelativeLayout d1 = (RelativeLayout) View.inflate(BinderActivity.this.context, C0149R.layout.binder_create_dir, null);
                            dialog.setContentView(d1);
                            final EditText ed = (EditText) d1.findViewById(C0149R.id.new_dir);
                            ((Button) dialog.findViewById(C0149R.id.buttonCreate)).setOnClickListener(new OnClickListener() {
                                public void onClick(View v) {
                                    String dir_name = ed.getText().toString().trim().replaceAll(InternalZipConstants.ZIP_FILE_SEPARATOR, BuildConfig.VERSION_NAME) + InternalZipConstants.ZIP_FILE_SEPARATOR;
                                    if (dir_name.replaceAll(" ", BuildConfig.VERSION_NAME).equals(BuildConfig.VERSION_NAME)) {
                                        Toast.makeText(listAppsFragment.getInstance(), Utils.getText(C0149R.string.create_dir_error), 1).show();
                                        return;
                                    }
                                    String full_dir_name = BuildConfig.VERSION_NAME;
                                    if (BinderActivity.this.current.path.endsWith(InternalZipConstants.ZIP_FILE_SEPARATOR)) {
                                        full_dir_name = BinderActivity.this.current.path + dir_name;
                                    } else {
                                        full_dir_name = BinderActivity.this.current.path + InternalZipConstants.ZIP_FILE_SEPARATOR + dir_name;
                                    }
                                    new File(full_dir_name).mkdirs();
                                    if (Utils.exists(full_dir_name) || new File(full_dir_name).exists()) {
                                        dialog.dismiss();
                                        try {
                                            BinderActivity.this.getDir(full_dir_name, (ListView) dListView.findViewById(C0149R.id.list));
                                            return;
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                            return;
                                        }
                                    }
                                    Toast.makeText(listAppsFragment.getInstance(), Utils.getText(C0149R.string.create_dir_error_2), 1).show();
                                }
                            });
                            Utils.showDialog(dialog);
                        }
                    });
                    select_but.setOnClickListener(new OnClickListener() {
                        public void onClick(View v) {
                            if (BinderActivity.this.current.path.endsWith(InternalZipConstants.ZIP_FILE_SEPARATOR)) {
                                ed2.setText(BinderActivity.this.current.path);
                            } else {
                                ed2.setText(BinderActivity.this.current.path + InternalZipConstants.ZIP_FILE_SEPARATOR);
                            }
                            if (tempdialog.isShowing()) {
                                tempdialog.dismiss();
                            }
                        }
                    });
                    BinderActivity.this.root = InternalZipConstants.ZIP_FILE_SEPARATOR;
                    ((ListView) dListView.findViewById(C0149R.id.list)).setOnItemClickListener(new C00754());
                    BinderActivity.this.filebrowser = (ListView) dListView.findViewById(C0149R.id.list);
                    try {
                        BinderActivity.this.getDir(BinderActivity.this.root, (ListView) dListView.findViewById(C0149R.id.list));
                    } catch (Exception e) {
                        try {
                            BinderActivity.this.root = new File(listAppsFragment.basepath).getParent();
                            BinderActivity.this.getDir(BinderActivity.this.root, (ListView) d.findViewById(C0149R.id.list));
                        } catch (Exception e2) {
                            BinderActivity.this.root = listAppsFragment.basepath;
                            BinderActivity.this.getDir(BinderActivity.this.root, (ListView) d.findViewById(C0149R.id.list));
                        }
                    }
                }
            };
            Button button = (Button) d.findViewById(C0149R.id.button1);
            but_browser1.setOnClickListener(clickListener_browser1);
            but_browser2.setOnClickListener(clickListener_browser2);
            button.setOnClickListener(clickListener2);
            ed1.setText("/mnt/sdcard/external_sd/Android/");
            ed2.setText("/mnt/sdcard/Android/");
            BinderActivity.this.pp4 = new AlertDlg(BinderActivity.this.context).setOnCancelListener(new C00774()).setCancelable(true).setIcon(17301659).setView(d).create();
            Utils.showDialog(BinderActivity.this.pp4);
        }
    }

    public class ItemFile {
        public String file = BuildConfig.VERSION_NAME;
        public String full = BuildConfig.VERSION_NAME;
        public String path = BuildConfig.VERSION_NAME;

        public ItemFile(String pathfile) {
            this.full = pathfile;
            this.path = new File(pathfile).getPath();
            this.file = new File(pathfile).getName();
        }

        public ItemFile(String filename, String path1) {
            this.full = path1;
            this.path = path1;
            this.file = filename;
        }
    }

    private final class byNameFile implements Comparator<ItemFile> {
        private byNameFile() {
        }

        public int compare(ItemFile a, ItemFile b) {
            if (a == null || b == null) {
                try {
                    throw new ClassCastException();
                } catch (Exception e) {
                    return 0;
                }
            } else if (new File(a.full).isDirectory() && new File(b.full).isDirectory()) {
                return a.full.compareToIgnoreCase(b.full);
            } else {
                if (new File(a.full).isDirectory() && !new File(b.full).isDirectory()) {
                    return -1;
                }
                if (!new File(a.full).isDirectory() && !new File(b.full).isDirectory()) {
                    return a.full.compareToIgnoreCase(b.full);
                }
                if (new File(a.full).isDirectory() || !new File(b.full).isDirectory()) {
                    return a.full.compareToIgnoreCase(b.full);
                }
                return 1;
            }
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = this;
        File bindfile = new File(getDir("binder", 0) + "/bind.txt");
        if (!bindfile.exists()) {
            try {
                bindfile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        this.bindes = getBindes(this.context);
        setContentView(C0149R.layout.binder);
        Button button = (Button) findViewById(C0149R.id.newbindbutton);
        this.lv = (ListView) findViewById(C0149R.id.bindlistView);
        listAppsFragment.adaptBind = new ArrayAdapter<BindItem>(this, C0149R.layout.binditemview, this.bindes) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View row = convertView;
                if (row == null) {
                    row = ((Activity) BinderActivity.this.context).getLayoutInflater().inflate(C0149R.layout.binditemview, parent, false);
                }
                TextView textView = (TextView) row.findViewById(C0149R.id.source);
                TextView textView2 = (TextView) row.findViewById(C0149R.id.target);
                textView.setTextAppearance(getContext(), listAppsFragment.getSizeText());
                textView2.setTextAppearance(getContext(), listAppsFragment.getSizeText());
                Button button_del = (Button) row.findViewById(C0149R.id.button_delete);
                final ToggleButton togglebtn = (ToggleButton) row.findViewById(C0149R.id.toggleButton_off);
                final ProgressBar progress = (ProgressBar) row.findViewById(C0149R.id.progressBar1);
                final int pos1 = position;
                togglebtn.setOnClickListener(new OnClickListener() {

                    class C00581 implements Runnable {

                        class C00571 implements Runnable {
                            C00571() {
                            }

                            public void run() {
                                listAppsFragment.adaptBind.notifyDataSetChanged();
                                BinderActivity.this.lv.setAdapter(listAppsFragment.adaptBind);
                                progress.setVisibility(8);
                                Intent i = new Intent(BinderActivity.this.context, BinderWidget.class);
                                i.setAction(BinderWidget.ACTION_WIDGET_RECEIVER_Updater);
                                BinderActivity.this.context.sendBroadcast(i);
                            }
                        }

                        C00581() {
                        }

                        public void run() {
                            if (((BindItem) listAppsFragment.adaptBind.getItem(pos1)).TargetDir.startsWith("~chelpus_disabled~")) {
                                ((BindItem) listAppsFragment.adaptBind.getItem(pos1)).TargetDir = ((BindItem) listAppsFragment.adaptBind.getItem(pos1)).TargetDir.replace("~chelpus_disabled~", BuildConfig.VERSION_NAME);
                                Utils.verify_bind_and_run("mount", "-o bind '" + ((BindItem) listAppsFragment.adaptBind.getItem(pos1)).SourceDir + "' '" + ((BindItem) listAppsFragment.adaptBind.getItem(pos1)).TargetDir + "'", ((BindItem) listAppsFragment.adaptBind.getItem(pos1)).SourceDir, ((BindItem) listAppsFragment.adaptBind.getItem(pos1)).TargetDir);
                            } else {
                                if (listAppsFragment.su) {
                                    Utils.run_all("umount -f '" + ((BindItem) listAppsFragment.adaptBind.getItem(pos1)).TargetDir + "'");
                                    Utils.run_all("umount -l '" + ((BindItem) listAppsFragment.adaptBind.getItem(pos1)).TargetDir + "'");
                                } else {
                                    Utils.cmd("umount '" + ((BindItem) listAppsFragment.adaptBind.getItem(pos1)).TargetDir + "'");
                                }
                                ((BindItem) listAppsFragment.adaptBind.getItem(pos1)).TargetDir = "~chelpus_disabled~" + ((BindItem) listAppsFragment.adaptBind.getItem(pos1)).TargetDir;
                            }
                            BinderActivity.savetoFile(BinderActivity.this.bindes, BinderActivity.this.context);
                            BinderActivity.this.runOnUiThread(new C00571());
                        }
                    }

                    public void onClick(View v) {
                        RelativeLayout d = (RelativeLayout) View.inflate(BinderActivity.this.context, C0149R.layout.binderdialog, null);
                        togglebtn.setEnabled(false);
                        progress.setVisibility(0);
                        Thread trig = new Thread(new C00581());
                        trig.setPriority(10);
                        trig.start();
                    }
                });
                togglebtn.setChecked(Utils.checkBind((BindItem) getItem(position)));
                button_del.setOnClickListener(new OnClickListener() {

                    class C00601 implements DialogInterface.OnClickListener {
                        C00601() {
                        }

                        public void onClick(DialogInterface dialog, int which) {
                            try {
                                if (listAppsFragment.su) {
                                    Utils.run_all("umount -f " + ((BindItem) listAppsFragment.adaptBind.getItem(pos1)).TargetDir);
                                    Utils.run_all("umount -l " + ((BindItem) listAppsFragment.adaptBind.getItem(pos1)).TargetDir);
                                } else {
                                    Utils.cmd("umount " + ((BindItem) listAppsFragment.adaptBind.getItem(pos1)).TargetDir);
                                }
                                listAppsFragment.adaptBind.remove(listAppsFragment.adaptBind.getItem(pos1));
                                BinderActivity.savetoFile(BinderActivity.this.bindes, BinderActivity.this.context);
                                listAppsFragment.adaptBind.notifyDataSetChanged();
                                BinderActivity.this.lv.setAdapter(listAppsFragment.adaptBind);
                                Intent i = new Intent(BinderActivity.this.context, BinderWidget.class);
                                i.setAction(BinderWidget.ACTION_WIDGET_RECEIVER_Updater);
                                BinderActivity.this.context.sendBroadcast(i);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    public void onClick(View v) {
                        Utils.showDialog(new Builder(BinderActivity.this.context).setCancelable(false).setTitle(Utils.getText(C0149R.string.warning)).setMessage(Utils.getText(C0149R.string.bind_delete)).setPositiveButton(Utils.getText(C0149R.string.ok), new C00601()).setNegativeButton(Utils.getText(C0149R.string.no), null).create());
                    }
                });
                textView.setTextAppearance(BinderActivity.this.context, BinderActivity.this.sizeText);
                textView2.setTextAppearance(BinderActivity.this.context, BinderActivity.this.sizeText);
                textView.setText(Utils.getColoredText(Utils.getText(C0149R.string.bind_source) + LogCollector.LINE_SEPARATOR, "#ff00ff00", "bold"));
                textView.append(Utils.getColoredText(((BindItem) getItem(position)).SourceDir, "#ffffffff", "italic"));
                textView2.setText(Utils.getColoredText(Utils.getText(C0149R.string.bind_target) + LogCollector.LINE_SEPARATOR, "#ffffff00", "bold"));
                textView2.append(Utils.getColoredText(((BindItem) getItem(position)).TargetDir.replace("~chelpus_disabled~", BuildConfig.VERSION_NAME), "#ffffffff", "italic"));
                return row;
            }
        };
        this.lv.setAdapter(listAppsFragment.adaptBind);
        this.lv.invalidateViews();
        this.lv.setBackgroundColor(-16777216);
        button.setOnClickListener(new C00782());
    }

    protected void onResume() {
        super.onResume();
    }

    protected void onPause() {
        super.onPause();
    }

    public static ArrayList<BindItem> getBindes(Context context) {
        ArrayList<BindItem> tempbindes = new ArrayList();
        File bindfile = new File(context.getDir("binder", 0) + "/bind.txt");
        try {
            if (!bindfile.exists()) {
                bindfile.createNewFile();
            }
            FileInputStream fis = new FileInputStream(bindfile);
            BufferedReader br = new BufferedReader(new InputStreamReader(fis, "UTF-8"));
            String data = BuildConfig.VERSION_NAME;
            String temp = BuildConfig.VERSION_NAME;
            while (true) {
                data = br.readLine();
                if (data == null) {
                    break;
                }
                String[] tails = data.split(";");
                if (tails.length == 2) {
                    System.out.println(tails[0] + " ; " + tails[1]);
                    tempbindes.add(new BindItem(tails[0], tails[1]));
                }
            }
            fis.close();
        } catch (FileNotFoundException e) {
            System.out.println("Not found bind.txt");
        } catch (IOException e2) {
            System.out.println(BuildConfig.VERSION_NAME + e2);
        }
        return tempbindes;
    }

    public static void savetoFile(ArrayList<BindItem> list, Context context) {
        File bindfile = new File(context.getDir("binder", 0) + "/bind.txt");
        try {
            if (!bindfile.exists()) {
                bindfile.createNewFile();
            }
            StringBuilder sb = new StringBuilder();
            Iterator it = list.iterator();
            while (it.hasNext()) {
                BindItem item = (BindItem) it.next();
                sb.append(item.SourceDir + ";" + item.TargetDir + LogCollector.LINE_SEPARATOR);
            }
            FileOutputStream fos = new FileOutputStream(bindfile);
            fos.write(sb.toString().getBytes());
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void getDir(String dirPath, ListView lv) {
        this.myPath.setText(Utils.getText(C0149R.string.binder_browser_current_dir) + " " + dirPath);
        this.current = new ItemFile(InternalZipConstants.ZIP_FILE_SEPARATOR, dirPath);
        ArrayList<ItemFile> item = new ArrayList();
        File f = new File(dirPath);
        File[] files = f.listFiles();
        if (files == null) {
        }
        if (!dirPath.equals(this.root)) {
            item.add(new ItemFile(this.root));
            item.add(new ItemFile("../", f.getParent()));
        }
        for (File file : files) {
            if (file.canRead() && (file.isDirectory() || file.toString().toLowerCase().endsWith(".apk"))) {
                item.add(new ItemFile(file.toString()));
            }
        }
        ArrayAdapter<ItemFile> fileList = new ArrayAdapter<ItemFile>(this, C0149R.layout.file_browser_row, item) {
            public View getView(int position, View convertView, ViewGroup parent) {
                ItemFile current = (ItemFile) getItem(position);
                View row = convertView;
                if (row == null) {
                    row = BinderActivity.this.getLayoutInflater().inflate(C0149R.layout.file_browser_row, parent, false);
                }
                TextView textView = (TextView) row.findViewById(C0149R.id.rowtext);
                textView.setTextAppearance(BinderActivity.this.context, listAppsFragment.getSizeText());
                textView.setText(current.file);
                ImageView icon = (ImageView) row.findViewById(C0149R.id.image);
                if (position == 0 || position == 1) {
                    try {
                        if (((ItemFile) getItem(1)).file.equals("../")) {
                            icon.setImageDrawable(BinderActivity.this.getResources().getDrawable(C0149R.drawable.fb_back));
                            return row;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    } catch (OutOfMemoryError u) {
                        u.printStackTrace();
                        System.gc();
                    }
                }
                if (new File(current.full).isDirectory()) {
                    icon.setImageDrawable(BinderActivity.this.getResources().getDrawable(C0149R.drawable.fb_folder));
                } else {
                    icon.setImageDrawable(BinderActivity.this.getResources().getDrawable(C0149R.drawable.file_icon));
                }
                return row;
            }
        };
        fileList.sort(new byNameFile());
        lv.setAdapter(fileList);
        fileList.notifyDataSetChanged();
    }
}
