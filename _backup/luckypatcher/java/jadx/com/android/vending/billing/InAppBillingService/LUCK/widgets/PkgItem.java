package com.android.vending.billing.InAppBillingService.LUCK.widgets;

import com.android.vending.billing.InAppBillingService.LUCK.BuildConfig;

public class PkgItem {
    public String label = BuildConfig.VERSION_NAME;
    public String package_name = BuildConfig.VERSION_NAME;

    public PkgItem(String pkg, String lab) {
        this.package_name = pkg;
        this.label = lab;
    }
}
