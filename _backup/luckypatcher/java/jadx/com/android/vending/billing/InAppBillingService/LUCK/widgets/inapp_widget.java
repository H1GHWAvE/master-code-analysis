package com.android.vending.billing.InAppBillingService.LUCK.widgets;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.widget.RemoteViews;
import android.widget.Toast;
import com.android.vending.billing.InAppBillingService.LUCK.BuildConfig;
import com.android.vending.billing.InAppBillingService.LUCK.C0149R;
import com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment;
import com.google.android.finsky.billing.iab.InAppBillingService;
import com.google.android.finsky.billing.iab.MarketBillingService;

public class inapp_widget extends AppWidgetProvider {
    public static String ACTION_WIDGET_RECEIVER = "ActionReceiverInAppWidget";
    public static String ACTION_WIDGET_RECEIVER_Updater = "ActionReceiverWidgetInAppUpdate";

    public static void pushWidgetUpdate(Context context, RemoteViews remoteViews) {
        AppWidgetManager.getInstance(context).updateAppWidget(new ComponentName(context, inapp_widget.class), remoteViews);
    }

    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), C0149R.layout.widget_inapp);
        Intent active = new Intent(context, inapp_widget.class);
        active.setAction(ACTION_WIDGET_RECEIVER);
        active.putExtra("msg", "Hello Habrahabr");
        remoteViews.setOnClickPendingIntent(C0149R.id.widget_button2, PendingIntent.getBroadcast(context, 0, active, 0));
        remoteViews.setTextViewText(C0149R.id.widget_button2, "InApp");
        remoteViews.setViewVisibility(C0149R.id.progressBar_widget_inapp, 8);
        if (context.getPackageManager().getComponentEnabledSetting(new ComponentName(context, MarketBillingService.class)) == 2 || context.getPackageManager().getComponentEnabledSetting(new ComponentName(context, InAppBillingService.class)) == 2) {
            remoteViews.setTextColor(C0149R.id.widget_button2, Color.parseColor("#FF0000"));
        } else {
            remoteViews.setTextColor(C0149R.id.widget_button2, Color.parseColor("#00FF00"));
        }
        appWidgetManager.updateAppWidget(appWidgetIds, remoteViews);
    }

    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        String action = intent.getAction();
        if (ACTION_WIDGET_RECEIVER.equals(action)) {
            listAppsFragment.init();
            RemoteViews remoteViews = new RemoteViews(context.getPackageName(), C0149R.layout.widget_inapp);
            remoteViews.setTextViewText(C0149R.id.widget_button2, BuildConfig.VERSION_NAME);
            remoteViews.setViewVisibility(C0149R.id.progressBar_widget_inapp, 0);
            AppWidgetManager gm = AppWidgetManager.getInstance(context);
            gm.updateAppWidget(gm.getAppWidgetIds(new ComponentName(context, inapp_widget.class)), remoteViews);
            if (context.getPackageManager().getComponentEnabledSetting(new ComponentName(context, MarketBillingService.class)) == 2 || context.getPackageManager().getComponentEnabledSetting(new ComponentName(context, InAppBillingService.class)) == 2) {
                listAppsFragment.getPkgMng().setComponentEnabledSetting(new ComponentName(listAppsFragment.getInstance(), MarketBillingService.class), 1, 1);
                listAppsFragment.getPkgMng().setComponentEnabledSetting(new ComponentName(listAppsFragment.getInstance(), InAppBillingService.class), 1, 1);
                Toast.makeText(context, "InApp-ON", 0).show();
            } else {
                listAppsFragment.getPkgMng().setComponentEnabledSetting(new ComponentName(listAppsFragment.getInstance(), MarketBillingService.class), 2, 1);
                listAppsFragment.getPkgMng().setComponentEnabledSetting(new ComponentName(listAppsFragment.getInstance(), InAppBillingService.class), 2, 1);
                Intent in = new Intent(listAppsFragment.getInstance(), inapp_widget.class);
                in.setPackage(listAppsFragment.getInstance().getPackageName());
                in.setAction(ACTION_WIDGET_RECEIVER_Updater);
                listAppsFragment.getInstance().sendBroadcast(in);
                Toast.makeText(context, "InApp-OFF", 0).show();
            }
        }
        if (ACTION_WIDGET_RECEIVER_Updater.equals(action)) {
            try {
                listAppsFragment.appDisabler = true;
                gm = AppWidgetManager.getInstance(context);
                onUpdate(context, gm, gm.getAppWidgetIds(new ComponentName(context, inapp_widget.class)));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
