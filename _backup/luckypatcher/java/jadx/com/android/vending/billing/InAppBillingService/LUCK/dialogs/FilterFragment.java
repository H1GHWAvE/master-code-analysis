package com.android.vending.billing.InAppBillingService.LUCK.dialogs;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import com.android.vending.billing.InAppBillingService.LUCK.BuildConfig;
import com.android.vending.billing.InAppBillingService.LUCK.C0149R;
import com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment;

public class FilterFragment extends Fragment {

    class C02012 implements OnClickListener {
        C02012() {
        }

        public void onClick(View v) {
            try {
                new AdvancedFilter().showDialog();
            } catch (Exception e) {
            }
        }
    }

    class C02023 implements TextWatcher {
        C02023() {
        }

        public void afterTextChanged(Editable s) {
            try {
                if (listAppsFragment.plia != null) {
                    listAppsFragment.plia.getFilter().filter(s.toString());
                    listAppsFragment.plia.notifyDataSetChanged();
                }
            } catch (Exception e) {
            }
        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(C0149R.layout.fragment_filter, container, false);
        final EditText filter = (EditText) view.findViewById(C0149R.id.editTextFilter);
        ((Button) view.findViewById(C0149R.id.button_clear)).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                try {
                    if (listAppsFragment.plia != null) {
                        listAppsFragment.plia.getFilter().filter(BuildConfig.VERSION_NAME);
                        listAppsFragment.plia.notifyDataSetChanged();
                    }
                    filter.setText(BuildConfig.VERSION_NAME);
                } catch (Exception e) {
                }
            }
        });
        ((Button) view.findViewById(C0149R.id.button_adv)).setOnClickListener(new C02012());
        filter.addTextChangedListener(new C02023());
        return view;
    }
}
