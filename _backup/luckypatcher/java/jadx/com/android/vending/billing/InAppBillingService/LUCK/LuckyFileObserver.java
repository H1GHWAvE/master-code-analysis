package com.android.vending.billing.InAppBillingService.LUCK;

import android.os.FileObserver;
import android.support.v4.media.TransportMediator;
import com.chelpus.Utils;
import net.lingala.zip4j.util.InternalZipConstants;
import org.tukaani.xz.DeltaOptions;
import org.tukaani.xz.common.Util;
import pxb.android.ResConst;

public class LuckyFileObserver extends FileObserver {
    public String absolutePath;
    public int f1i = 0;

    public LuckyFileObserver(String path) {
        super(path, 4095);
        this.absolutePath = path;
    }

    public void onEvent(int event, String path) {
        System.out.println(this.absolutePath + InternalZipConstants.ZIP_FILE_SEPARATOR + path + LogCollector.LINE_SEPARATOR);
        if (path != null) {
            if ((event & DeltaOptions.DISTANCE_MAX) != 0) {
                Utils.copyFile(InternalZipConstants.ZIP_FILE_SEPARATOR + path, "/mnt/sdcard/!Capture" + this.f1i + ".jar", false, true);
                this.f1i++;
                System.out.println(this.absolutePath + InternalZipConstants.ZIP_FILE_SEPARATOR + path + " is created\n");
            }
            if ((event & 32) != 0) {
                System.out.println(path + " is opened\n");
            }
            if ((event & 1) != 0) {
                System.out.println(this.absolutePath + InternalZipConstants.ZIP_FILE_SEPARATOR + path + " is accessed/read\n");
            }
            if ((event & 2) != 0) {
                System.out.println(this.absolutePath + InternalZipConstants.ZIP_FILE_SEPARATOR + path + " is modified\n");
            }
            if ((event & 16) != 0) {
                System.out.println(path + " is closed\n");
            }
            if ((event & 8) != 0) {
                System.out.println(this.absolutePath + InternalZipConstants.ZIP_FILE_SEPARATOR + path + " is written and closed\n");
            }
            if ((event & ResConst.RES_TABLE_PACKAGE_TYPE) != 0) {
                System.out.println(this.absolutePath + InternalZipConstants.ZIP_FILE_SEPARATOR + path + " is deleted\n");
            }
            if ((event & Util.BLOCK_HEADER_SIZE_MAX) != 0) {
                System.out.println(this.absolutePath + InternalZipConstants.ZIP_FILE_SEPARATOR + " is deleted\n");
            }
            if ((event & 64) != 0) {
                System.out.println(this.absolutePath + InternalZipConstants.ZIP_FILE_SEPARATOR + path + " is moved to somewhere " + LogCollector.LINE_SEPARATOR);
            }
            if ((event & TransportMediator.FLAG_KEY_MEDIA_NEXT) != 0) {
                System.out.println("File is moved to " + this.absolutePath + InternalZipConstants.ZIP_FILE_SEPARATOR + path + LogCollector.LINE_SEPARATOR);
            }
            if ((event & InternalZipConstants.UFT8_NAMES_FLAG) != 0) {
                System.out.println(path + " is moved\n");
            }
            if ((event & 4) != 0) {
                System.out.println(this.absolutePath + InternalZipConstants.ZIP_FILE_SEPARATOR + path + " is changed (permissions, owner, timestamp)\n");
            }
        }
    }
}
