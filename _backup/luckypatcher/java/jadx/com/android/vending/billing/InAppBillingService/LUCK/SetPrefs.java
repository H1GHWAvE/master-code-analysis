package com.android.vending.billing.InAppBillingService.LUCK;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import com.chelpus.Utils;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;
import net.lingala.zip4j.util.InternalZipConstants;

public class SetPrefs extends PreferenceActivity {
    public Context context;
    OnPreferenceChangeListener numberCheckListener = new OnPreferenceChangeListener() {
        public boolean onPreferenceChange(Preference preference, Object newValue) {
            SharedPreferences settings = listAppsFragment.getInstance().getSharedPreferences("config", 4);
            if (preference != SetPrefs.this.findPreference("days_on_up") || !SetPrefs.this.numberCheck(newValue)) {
                return false;
            }
            settings.edit().putInt("days_on_up", Integer.valueOf(newValue.toString()).intValue()).commit();
            preference.getEditor().putString("days_on_up", newValue.toString());
            settings.edit().putBoolean("settings_change", true).commit();
            settings.edit().putBoolean("lang_change", true).commit();
            return true;
        }
    };

    class C01501 implements OnPreferenceChangeListener {
        C01501() {
        }

        public boolean onPreferenceChange(Preference arg0, Object newValue) {
            SharedPreferences settings = listAppsFragment.getInstance().getSharedPreferences("config", 0);
            if (arg0 == SetPrefs.this.findPreference(listAppsFragment.SETTINGS_VIEWSIZE) && newValue.equals("2")) {
                settings.edit().putInt(listAppsFragment.SETTINGS_VIEWSIZE, 2).commit();
                settings.edit().putBoolean("settings_change", true).commit();
                return true;
            } else if (arg0 == SetPrefs.this.findPreference(listAppsFragment.SETTINGS_VIEWSIZE) && newValue.equals("1")) {
                settings.edit().putInt(listAppsFragment.SETTINGS_VIEWSIZE, 1).commit();
                settings.edit().putBoolean("settings_change", true).commit();
                return true;
            } else if (arg0 != SetPrefs.this.findPreference(listAppsFragment.SETTINGS_VIEWSIZE) || !newValue.equals("0")) {
                return false;
            } else {
                settings.edit().putInt(listAppsFragment.SETTINGS_VIEWSIZE, 0).commit();
                settings.edit().putBoolean("settings_change", true).commit();
                return true;
            }
        }
    }

    class C01582 implements OnPreferenceChangeListener {
        C01582() {
        }

        public boolean onPreferenceChange(Preference arg0, Object newValue) {
            SharedPreferences settings = listAppsFragment.getInstance().getSharedPreferences("config", 0);
            if (arg0 == SetPrefs.this.findPreference("root_force") && newValue.equals("2")) {
                listAppsFragment.su = false;
                settings.edit().putInt("root_force", 2).commit();
                settings.edit().putBoolean("settings_change", true).commit();
                return true;
            } else if (arg0 == SetPrefs.this.findPreference("root_force") && newValue.equals("1")) {
                listAppsFragment.su = true;
                settings.edit().putInt("root_force", 1).commit();
                settings.edit().putBoolean("settings_change", true).commit();
                return true;
            } else if (arg0 != SetPrefs.this.findPreference("root_force") || !newValue.equals("0")) {
                return false;
            } else {
                settings.edit().putInt("root_force", 0).commit();
                settings.edit().putBoolean("settings_change", true).commit();
                return true;
            }
        }
    }

    class C01593 implements OnPreferenceChangeListener {
        C01593() {
        }

        public boolean onPreferenceChange(Preference arg0, Object newValue) {
            SharedPreferences settings = listAppsFragment.getInstance().getSharedPreferences("config", 0);
            if (arg0 == SetPrefs.this.findPreference("orientstion") && newValue.equals("2")) {
                settings.edit().putInt("orientstion", 3).commit();
                SetPrefs.this.setRequestedOrientation(1);
                settings.edit().putBoolean("settings_change", true).commit();
                return true;
            } else if (arg0 == SetPrefs.this.findPreference("orientstion") && newValue.equals("1")) {
                settings.edit().putInt("orientstion", 1).commit();
                SetPrefs.this.setRequestedOrientation(0);
                settings.edit().putBoolean("settings_change", true).commit();
                return true;
            } else if (arg0 != SetPrefs.this.findPreference("orientstion") || !newValue.equals("3")) {
                return false;
            } else {
                settings.edit().putInt("orientstion", 3).commit();
                SetPrefs.this.setRequestedOrientation(4);
                settings.edit().putBoolean("settings_change", true).commit();
                return true;
            }
        }
    }

    class C01604 implements OnPreferenceChangeListener {
        C01604() {
        }

        public boolean onPreferenceChange(Preference arg0, Object newValue) {
            SharedPreferences settings = listAppsFragment.getInstance().getSharedPreferences("config", 0);
            if (arg0 == SetPrefs.this.findPreference("sortby") && newValue.equals("1")) {
                settings.edit().putInt("sortby", 1).commit();
                settings.edit().putBoolean("settings_change", true).commit();
                return true;
            } else if (arg0 == SetPrefs.this.findPreference("sortby") && newValue.equals("3")) {
                settings.edit().putInt("sortby", 3).commit();
                settings.edit().putBoolean("settings_change", true).commit();
                return true;
            } else if (arg0 != SetPrefs.this.findPreference("sortby") || !newValue.equals("2")) {
                return false;
            } else {
                settings.edit().putInt("sortby", 2).commit();
                settings.edit().putBoolean("settings_change", true).commit();
                return true;
            }
        }
    }

    class C01615 implements OnPreferenceChangeListener {
        C01615() {
        }

        public boolean onPreferenceChange(Preference arg0, Object newValue) {
            SharedPreferences settings = listAppsFragment.getInstance().getSharedPreferences("config", 0);
            Intent intent;
            if (arg0 == SetPrefs.this.findPreference("language") && newValue.equals("1")) {
                Locale appLoc = Resources.getSystem().getConfiguration().locale;
                Locale.setDefault(Locale.getDefault());
                Configuration appConfig = new Configuration();
                appConfig.locale = appLoc;
                listAppsFragment.getRes().updateConfiguration(appConfig, listAppsFragment.getRes().getDisplayMetrics());
                settings.edit().putInt("language", 1).commit();
                intent = SetPrefs.this.getIntent();
                intent.addFlags(AccessibilityNodeInfoCompat.ACTION_SET_SELECTION);
                SetPrefs.this.finish();
                SetPrefs.this.startActivity(intent);
                settings.edit().putBoolean("settings_change", true).commit();
                settings.edit().putBoolean("lang_change", true).commit();
                return true;
            } else if (arg0 != SetPrefs.this.findPreference("language") || !newValue.equals("2")) {
                return false;
            } else {
                Locale appLoc2 = new Locale("en");
                Locale.setDefault(appLoc2);
                Configuration appConfig2 = new Configuration();
                appConfig2.locale = appLoc2;
                listAppsFragment.getRes().updateConfiguration(appConfig2, listAppsFragment.getRes().getDisplayMetrics());
                settings.edit().putInt("language", 2).commit();
                intent = SetPrefs.this.getIntent();
                intent.addFlags(AccessibilityNodeInfoCompat.ACTION_SET_SELECTION);
                SetPrefs.this.finish();
                SetPrefs.this.startActivity(intent);
                settings.edit().putBoolean("settings_change", true).commit();
                settings.edit().putBoolean("lang_change", true).commit();
                return true;
            }
        }
    }

    class C01626 implements OnPreferenceChangeListener {
        C01626() {
        }

        public boolean onPreferenceChange(Preference arg0, Object newValue) {
            SharedPreferences settings = listAppsFragment.getInstance().getSharedPreferences("config", 0);
            if (arg0 == SetPrefs.this.findPreference("systemapp") && ((Boolean) newValue).booleanValue()) {
                settings.edit().putBoolean("systemapp", true).commit();
                arg0.getEditor().putBoolean("systemapp", true);
                settings.edit().putBoolean("settings_change", true).commit();
                settings.edit().putBoolean("lang_change", true).commit();
                return true;
            } else if (arg0 != SetPrefs.this.findPreference("systemapp") || ((Boolean) newValue).booleanValue()) {
                return false;
            } else {
                settings.edit().putBoolean("systemapp", false).commit();
                arg0.getEditor().putBoolean("systemapp", false);
                settings.edit().putBoolean("settings_change", true).commit();
                settings.edit().putBoolean("lang_change", true).commit();
                return true;
            }
        }
    }

    class C01637 implements OnPreferenceChangeListener {
        C01637() {
        }

        public boolean onPreferenceChange(Preference arg0, Object newValue) {
            SharedPreferences settings = listAppsFragment.getInstance().getSharedPreferences("config", 0);
            if (arg0 == SetPrefs.this.findPreference("confirm_exit") && ((Boolean) newValue).booleanValue()) {
                settings.edit().putBoolean("confirm_exit", true).commit();
                arg0.getEditor().putBoolean("confirm_exit", true);
                return true;
            } else if (arg0 != SetPrefs.this.findPreference("confirm_exit") || ((Boolean) newValue).booleanValue()) {
                return false;
            } else {
                settings.edit().putBoolean("confirm_exit", false).commit();
                arg0.getEditor().putBoolean("confirm_exit", false);
                return true;
            }
        }
    }

    class C01648 implements OnPreferenceChangeListener {
        C01648() {
        }

        public boolean onPreferenceChange(Preference arg0, Object newValue) {
            SharedPreferences settings = listAppsFragment.getInstance().getSharedPreferences("config", 0);
            if (arg0 == SetPrefs.this.findPreference("apkname") && newValue.equals("0")) {
                settings.edit().putInt("apkname", 0).commit();
                settings.edit().putBoolean("settings_change", true).commit();
                return true;
            } else if (arg0 != SetPrefs.this.findPreference("apkname") || !newValue.equals("1")) {
                return false;
            } else {
                settings.edit().putInt("apkname", 1).commit();
                settings.edit().putBoolean("settings_change", true).commit();
                return true;
            }
        }
    }

    class C01659 implements OnPreferenceChangeListener {
        C01659() {
        }

        public boolean onPreferenceChange(Preference arg0, Object newValue) {
            SharedPreferences settings = listAppsFragment.getInstance().getSharedPreferences("config", 0);
            if (arg0 == SetPrefs.this.findPreference("lvlapp") && ((Boolean) newValue).booleanValue()) {
                settings.edit().putBoolean("lvlapp", true).commit();
                arg0.getEditor().putBoolean("lvlapp", true);
                settings.edit().putBoolean("settings_change", true).commit();
                settings.edit().putBoolean("lang_change", true).commit();
                return true;
            } else if (arg0 != SetPrefs.this.findPreference("lvlapp") || ((Boolean) newValue).booleanValue()) {
                return false;
            } else {
                settings.edit().putBoolean("lvlapp", false).commit();
                arg0.getEditor().putBoolean("lvlapp", false);
                settings.edit().putBoolean("settings_change", true).commit();
                settings.edit().putBoolean("lang_change", true).commit();
                return true;
            }
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = this;
        SharedPreferences settings = listAppsFragment.getInstance().getSharedPreferences("config", 0);
        SharedPreferences sett = listAppsFragment.getInstance().getSharedPreferences("SetPrefs", 0);
        Editor editor2 = listAppsFragment.getInstance().getSharedPreferences("config", 0).edit();
        editor2.putBoolean("Update0", true);
        editor2.commit();
        String lng = settings.getString("force_language", "default");
        if (lng.equals("default")) {
            Locale appLoc3 = Resources.getSystem().getConfiguration().locale;
            Locale.setDefault(Locale.getDefault());
            Configuration appConfig3 = new Configuration();
            appConfig3.locale = appLoc3;
            listAppsFragment.getRes().updateConfiguration(appConfig3, listAppsFragment.getRes().getDisplayMetrics());
        } else {
            Locale locale = null;
            String[] tails = lng.split("_");
            if (tails.length == 1) {
                locale = new Locale(tails[0]);
            }
            if (tails.length == 2) {
                locale = new Locale(tails[0], tails[1], BuildConfig.VERSION_NAME);
            }
            if (tails.length == 3) {
                locale = new Locale(tails[0], tails[1], tails[2]);
            }
            Locale.setDefault(locale);
            Configuration appConfig2 = new Configuration();
            appConfig2.locale = locale;
            listAppsFragment.getRes().updateConfiguration(appConfig2, listAppsFragment.getRes().getDisplayMetrics());
        }
        if (settings.getInt("orientstion", 3) == 1) {
            setRequestedOrientation(0);
        }
        if (settings.getInt("orientstion", 3) == 2) {
            setRequestedOrientation(1);
        }
        if (settings.getInt("orientstion", 3) == 3) {
            setRequestedOrientation(4);
        }
        PreferenceManager prefMgr = getPreferenceManager();
        prefMgr.setSharedPreferencesName("SetPrefs");
        prefMgr.setSharedPreferencesMode(0);
        sett.edit().putString(listAppsFragment.SETTINGS_VIEWSIZE, BuildConfig.VERSION_NAME + settings.getInt(listAppsFragment.SETTINGS_VIEWSIZE, 0)).commit();
        sett.edit().putString("root_force", BuildConfig.VERSION_NAME + settings.getInt("root_force", 0)).commit();
        sett.edit().putBoolean("confirm_exit", settings.getBoolean("confirm_exit", true)).commit();
        sett.edit().putString("orientstion", BuildConfig.VERSION_NAME + settings.getInt("orientstion", 3)).commit();
        sett.edit().putString("sortby", BuildConfig.VERSION_NAME + settings.getInt("sortby", 2)).commit();
        sett.edit().putString("language", BuildConfig.VERSION_NAME + settings.getInt("language", 1)).commit();
        sett.edit().putBoolean("systemapp", settings.getBoolean("systemapp", false)).commit();
        sett.edit().putString("apkname", BuildConfig.VERSION_NAME + settings.getInt("apkname", 0)).commit();
        sett.edit().putBoolean("lvlapp", settings.getBoolean("lvlapp", true)).commit();
        sett.edit().putBoolean("adsapp", settings.getBoolean("adsapp", true)).commit();
        sett.edit().putBoolean("customapp", settings.getBoolean("customapp", true)).commit();
        sett.edit().putBoolean("modifapp", settings.getBoolean("modifapp", true)).commit();
        sett.edit().putBoolean("hide_notify", settings.getBoolean("hide_notify", false)).commit();
        sett.edit().putBoolean("fixedapp", settings.getBoolean("fixedapp", true)).commit();
        sett.edit().putBoolean("noneapp", settings.getBoolean("noneapp", true)).commit();
        sett.edit().putString("path", settings.getString("basepath", "Noting")).commit();
        sett.edit().putString("extStorageDirectory", settings.getString("extStorageDirectory", "Noting")).commit();
        sett.edit().putBoolean("vibration", settings.getBoolean("vibration", false)).commit();
        sett.edit().putBoolean("disable_autoupdate", settings.getBoolean("disable_autoupdate", false)).commit();
        sett.edit().putBoolean("fast_start", settings.getBoolean("fast_start", false)).commit();
        sett.edit().putBoolean("no_icon", settings.getBoolean("no_icon", false)).commit();
        sett.edit().putBoolean("hide_title", settings.getBoolean("hide_title", false)).commit();
        sett.edit().putString("days_on_up", BuildConfig.VERSION_NAME + settings.getInt("days_on_up", 1)).commit();
        addPreferencesFromResource(C0149R.xml.preferences);
        getPreferenceScreen().findPreference("days_on_up").setOnPreferenceChangeListener(this.numberCheckListener);
        findPreference(listAppsFragment.SETTINGS_VIEWSIZE).setOnPreferenceChangeListener(new C01501());
        findPreference("root_force").setOnPreferenceChangeListener(new C01582());
        findPreference("orientstion").setOnPreferenceChangeListener(new C01593());
        findPreference("sortby").setOnPreferenceChangeListener(new C01604());
        findPreference("language").setOnPreferenceChangeListener(new C01615());
        findPreference("systemapp").setOnPreferenceChangeListener(new C01626());
        findPreference("confirm_exit").setOnPreferenceChangeListener(new C01637());
        findPreference("apkname").setOnPreferenceChangeListener(new C01648());
        findPreference("lvlapp").setOnPreferenceChangeListener(new C01659());
        findPreference("adsapp").setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference arg0, Object newValue) {
                SharedPreferences settings = listAppsFragment.getInstance().getSharedPreferences("config", 0);
                if (arg0 == SetPrefs.this.findPreference("adsapp") && ((Boolean) newValue).booleanValue()) {
                    settings.edit().putBoolean("adsapp", true).commit();
                    arg0.getEditor().putBoolean("adsapp", true);
                    settings.edit().putBoolean("settings_change", true).commit();
                    settings.edit().putBoolean("lang_change", true).commit();
                    return true;
                } else if (arg0 != SetPrefs.this.findPreference("adsapp") || ((Boolean) newValue).booleanValue()) {
                    return false;
                } else {
                    settings.edit().putBoolean("adsapp", false).commit();
                    arg0.getEditor().putBoolean("adsapp", false);
                    settings.edit().putBoolean("settings_change", true).commit();
                    settings.edit().putBoolean("lang_change", true).commit();
                    return true;
                }
            }
        });
        findPreference("customapp").setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference arg0, Object newValue) {
                SharedPreferences settings = listAppsFragment.getInstance().getSharedPreferences("config", 0);
                if (arg0 == SetPrefs.this.findPreference("customapp") && ((Boolean) newValue).booleanValue()) {
                    settings.edit().putBoolean("customapp", true).commit();
                    arg0.getEditor().putBoolean("customapp", true);
                    settings.edit().putBoolean("settings_change", true).commit();
                    settings.edit().putBoolean("lang_change", true).commit();
                    return true;
                } else if (arg0 != SetPrefs.this.findPreference("customapp") || ((Boolean) newValue).booleanValue()) {
                    return false;
                } else {
                    settings.edit().putBoolean("customapp", false).commit();
                    arg0.getEditor().putBoolean("customapp", false);
                    settings.edit().putBoolean("settings_change", true).commit();
                    settings.edit().putBoolean("lang_change", true).commit();
                    return true;
                }
            }
        });
        findPreference("modifapp").setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference arg0, Object newValue) {
                SharedPreferences settings = listAppsFragment.getInstance().getSharedPreferences("config", 0);
                if (arg0 == SetPrefs.this.findPreference("modifapp") && ((Boolean) newValue).booleanValue()) {
                    settings.edit().putBoolean("modifapp", true).commit();
                    arg0.getEditor().putBoolean("modifapp", true);
                    settings.edit().putBoolean("settings_change", true).commit();
                    settings.edit().putBoolean("lang_change", true).commit();
                    return true;
                } else if (arg0 != SetPrefs.this.findPreference("modifapp") || ((Boolean) newValue).booleanValue()) {
                    return false;
                } else {
                    settings.edit().putBoolean("modifapp", false).commit();
                    arg0.getEditor().putBoolean("modifapp", false);
                    settings.edit().putBoolean("settings_change", true).commit();
                    settings.edit().putBoolean("lang_change", true).commit();
                    return true;
                }
            }
        });
        findPreference("fixedapp").setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference arg0, Object newValue) {
                SharedPreferences settings = listAppsFragment.getInstance().getSharedPreferences("config", 0);
                if (arg0 == SetPrefs.this.findPreference("fixedapp") && ((Boolean) newValue).booleanValue()) {
                    settings.edit().putBoolean("fixedapp", true).commit();
                    arg0.getEditor().putBoolean("fixedapp", true);
                    settings.edit().putBoolean("settings_change", true).commit();
                    settings.edit().putBoolean("lang_change", true).commit();
                    return true;
                } else if (arg0 != SetPrefs.this.findPreference("fixedapp") || ((Boolean) newValue).booleanValue()) {
                    return false;
                } else {
                    settings.edit().putBoolean("fixedapp", false).commit();
                    arg0.getEditor().putBoolean("fixedapp", false);
                    settings.edit().putBoolean("settings_change", true).commit();
                    settings.edit().putBoolean("lang_change", true).commit();
                    return true;
                }
            }
        });
        findPreference("noneapp").setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference arg0, Object newValue) {
                SharedPreferences settings = listAppsFragment.getInstance().getSharedPreferences("config", 0);
                if (arg0 == SetPrefs.this.findPreference("noneapp") && ((Boolean) newValue).booleanValue()) {
                    settings.edit().putBoolean("noneapp", true).commit();
                    arg0.getEditor().putBoolean("noneapp", true);
                    settings.edit().putBoolean("settings_change", true).commit();
                    settings.edit().putBoolean("lang_change", true).commit();
                    return true;
                } else if (arg0 != SetPrefs.this.findPreference("noneapp") || ((Boolean) newValue).booleanValue()) {
                    return false;
                } else {
                    settings.edit().putBoolean("noneapp", false).commit();
                    arg0.getEditor().putBoolean("noneapp", false);
                    settings.edit().putBoolean("settings_change", true).commit();
                    settings.edit().putBoolean("lang_change", true).commit();
                    return true;
                }
            }
        });
        findPreference("hide_notify").setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference arg0, Object newValue) {
                SharedPreferences settings = listAppsFragment.getInstance().getSharedPreferences("config", 0);
                if (arg0 == SetPrefs.this.findPreference("hide_notify") && ((Boolean) newValue).booleanValue()) {
                    settings.edit().putBoolean("hide_notify", true).commit();
                    arg0.getEditor().putBoolean("hide_notify", true);
                    return true;
                } else if (arg0 != SetPrefs.this.findPreference("hide_notify") || ((Boolean) newValue).booleanValue()) {
                    return false;
                } else {
                    settings.edit().putBoolean("hide_notify", false).commit();
                    arg0.getEditor().putBoolean("hide_notify", false);
                    return true;
                }
            }
        });
        findPreference("vibration").setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference arg0, Object newValue) {
                SharedPreferences settings = listAppsFragment.getInstance().getSharedPreferences("config", 0);
                if (arg0 == SetPrefs.this.findPreference("vibration") && ((Boolean) newValue).booleanValue()) {
                    settings.edit().putBoolean("vibration", true).commit();
                    arg0.getEditor().putBoolean("vibration", true);
                    return true;
                } else if (arg0 != SetPrefs.this.findPreference("vibration") || ((Boolean) newValue).booleanValue()) {
                    return false;
                } else {
                    settings.edit().putBoolean("vibration", false).commit();
                    arg0.getEditor().putBoolean("vibration", false);
                    return true;
                }
            }
        });
        findPreference("disable_autoupdate").setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference arg0, Object newValue) {
                SharedPreferences settings = listAppsFragment.getInstance().getSharedPreferences("config", 0);
                if (arg0 == SetPrefs.this.findPreference("disable_autoupdate") && ((Boolean) newValue).booleanValue()) {
                    settings.edit().putBoolean("disable_autoupdate", true).commit();
                    arg0.getEditor().putBoolean("disable_autoupdate", true);
                    return true;
                } else if (arg0 != SetPrefs.this.findPreference("disable_autoupdate") || ((Boolean) newValue).booleanValue()) {
                    return false;
                } else {
                    settings.edit().putBoolean("disable_autoupdate", false).commit();
                    arg0.getEditor().putBoolean("disable_autoupdate", false);
                    return true;
                }
            }
        });
        findPreference("fast_start").setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference arg0, Object newValue) {
                SharedPreferences settings = listAppsFragment.getInstance().getSharedPreferences("config", 0);
                if (arg0 == SetPrefs.this.findPreference("fast_start") && ((Boolean) newValue).booleanValue()) {
                    settings.edit().putBoolean("fast_start", true).commit();
                    arg0.getEditor().putBoolean("fast_start", true);
                    return true;
                } else if (arg0 != SetPrefs.this.findPreference("fast_start") || ((Boolean) newValue).booleanValue()) {
                    return false;
                } else {
                    settings.edit().putBoolean("fast_start", false).commit();
                    arg0.getEditor().putBoolean("fast_start", false);
                    return true;
                }
            }
        });
        findPreference("no_icon").setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference arg0, Object newValue) {
                SharedPreferences settings = listAppsFragment.getInstance().getSharedPreferences("config", 0);
                if (arg0 == SetPrefs.this.findPreference("no_icon") && ((Boolean) newValue).booleanValue()) {
                    settings.edit().putBoolean("no_icon", true).commit();
                    arg0.getEditor().putBoolean("no_icon", true);
                    return true;
                } else if (arg0 != SetPrefs.this.findPreference("no_icon") || ((Boolean) newValue).booleanValue()) {
                    return false;
                } else {
                    settings.edit().putBoolean("no_icon", false).commit();
                    arg0.getEditor().putBoolean("no_icon", false);
                    return true;
                }
            }
        });
        findPreference("hide_title").setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference arg0, Object newValue) {
                SharedPreferences settings = listAppsFragment.getInstance().getSharedPreferences("config", 0);
                if (arg0 == SetPrefs.this.findPreference("hide_title") && ((Boolean) newValue).booleanValue()) {
                    settings.edit().putBoolean("hide_title", true).commit();
                    arg0.getEditor().putBoolean("hide_title", true);
                    return true;
                } else if (arg0 != SetPrefs.this.findPreference("hide_title") || ((Boolean) newValue).booleanValue()) {
                    return false;
                } else {
                    settings.edit().putBoolean("hide_title", false).commit();
                    arg0.getEditor().putBoolean("hide_title", false);
                    return true;
                }
            }
        });
        findPreference("path").setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            ProgressDialog progress = null;

            public boolean onPreferenceChange(Preference arg0, Object newValue) {
                final SharedPreferences settings = listAppsFragment.getInstance().getSharedPreferences("config", 0);
                if (arg0 == SetPrefs.this.findPreference("path") && newValue.toString().contains(InternalZipConstants.ZIP_FILE_SEPARATOR) && !newValue.toString().equals(InternalZipConstants.ZIP_FILE_SEPARATOR)) {
                    String[] tailsdir = newValue.toString().trim().replaceAll("\\s+", ".").split("\\/+");
                    String data_dirs = BuildConfig.VERSION_NAME;
                    for (String aTailsdir : tailsdir) {
                        if (!aTailsdir.equals(BuildConfig.VERSION_NAME)) {
                            data_dirs = data_dirs + InternalZipConstants.ZIP_FILE_SEPARATOR + aTailsdir;
                        }
                    }
                    final String data_dir = data_dirs;
                    if (new File(data_dir).exists()) {
                        SetPrefs.this.showMessage(SetPrefs.this, Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.error_path_exists));
                    } else if (SetPrefs.this.testPath(true, data_dir) && !data_dir.startsWith(SetPrefs.this.getDir("sdcard", 0).getAbsolutePath())) {
                        settings.edit().putString("path", data_dir).commit();
                        settings.edit().putBoolean("manual_path", true).commit();
                        arg0.getEditor().putString("path", data_dir).commit();
                        settings.edit().putBoolean("path_changed", true).commit();
                        final File srcFolder = new File(settings.getString("basepath", "Noting"));
                        final File destFolder = new File(data_dir);
                        if (srcFolder.exists()) {
                            this.progress = new ProgressDialog(SetPrefs.this);
                            this.progress.setCancelable(false);
                            this.progress.setMessage(SetPrefs.this.getString(C0149R.string.wait));
                            this.progress.show();
                            final Handler handler = new Handler() {
                                public void handleMessage(Message msg) {
                                    System.out.println("LuckyPatcher: message poluchil: !" + msg);
                                    try {
                                        if (AnonymousClass21.this.progress.isShowing()) {
                                            AnonymousClass21.this.progress.dismiss();
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    if (msg.what == 0) {
                                        settings.edit().putString("basepath", data_dir).commit();
                                        listAppsFragment.basepath = data_dir;
                                        SetPrefs.this.showMessage(SetPrefs.this, Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.move_data) + data_dir);
                                    }
                                    if (msg.what == 1) {
                                        SetPrefs.this.showMessage(SetPrefs.this, Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.no_space));
                                    }
                                }
                            };
                            new Thread(new Runnable() {
                                public void run() {
                                    try {
                                        Utils.copyFolder(srcFolder, destFolder);
                                        new Utils(BuildConfig.VERSION_NAME).deleteFolder(srcFolder);
                                        handler.sendEmptyMessage(0);
                                    } catch (Exception e2) {
                                        e2.printStackTrace();
                                        handler.sendEmptyMessage(1);
                                    }
                                }
                            }).start();
                        } else {
                            System.out.println("Directory does not exist.");
                        }
                        return true;
                    }
                }
                SetPrefs.this.showMessage(SetPrefs.this, Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.error_path));
                return false;
            }
        });
        findPreference("help").setOnPreferenceClickListener(new OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                SetPrefs.this.startActivity(new Intent(SetPrefs.this.getBaseContext(), HelpActivity.class));
                return true;
            }
        });
        findPreference("upd").setOnPreferenceClickListener(new OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                if (listAppsFragment.frag != null) {
                    listAppsFragment.frag.runUpdate();
                }
                SetPrefs.this.finish();
                return true;
            }
        });
        findPreference("sendlog").setOnPreferenceClickListener(new OnPreferenceClickListener() {

            class C01531 extends AsyncTask<Void, Void, Boolean> {
                ProgressDialog pd;

                C01531() {
                }

                protected Boolean doInBackground(Void... params) {
                    try {
                        if (listAppsFragment.mLogCollector == null) {
                            listAppsFragment.mLogCollector = new LogCollector();
                        }
                        return Boolean.valueOf(listAppsFragment.mLogCollector.collect(listAppsFragment.getInstance(), true));
                    } catch (Exception e) {
                        e.printStackTrace();
                        return Boolean.valueOf(false);
                    }
                }

                protected void onPreExecute() {
                    this.pd = new ProgressDialog(SetPrefs.this.context);
                    this.pd.setTitle("Progress");
                    this.pd.setMessage(Utils.getText(C0149R.string.collect_logs));
                    this.pd.setIndeterminate(true);
                    this.pd.show();
                }

                protected void onPostExecute(Boolean result) {
                    try {
                        if (this.pd != null && this.pd.isShowing()) {
                            this.pd.dismiss();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (result.booleanValue()) {
                        try {
                            listAppsFragment.mLogCollector.sendLog(listAppsFragment.getInstance(), "lp.chelpus@gmail.com", "Error Log", "Lucky Patcher " + listAppsFragment.getPkgMng().getPackageInfo(listAppsFragment.frag.getContext().getPackageName(), 0).versionName);
                            return;
                        } catch (NameNotFoundException e2) {
                            e2.printStackTrace();
                            return;
                        }
                    }
                    Builder logbuilder = new Builder(SetPrefs.this.context);
                    logbuilder.setTitle("Error").setMessage(Utils.getText(C0149R.string.error_collect_logs)).setNegativeButton("OK", null);
                    logbuilder.create().show();
                }
            }

            public boolean onPreferenceClick(Preference preference) {
                new C01531().execute(new Void[0]);
                return true;
            }
        });
        findPreference("about").setOnPreferenceClickListener(new OnPreferenceClickListener() {

            class C01541 implements OnCancelListener {
                C01541() {
                }

                public void onCancel(DialogInterface dialog) {
                    dialog.dismiss();
                }
            }

            class C01552 implements OnClickListener {
                C01552() {
                }

                public void onClick(DialogInterface dialog, int which) {
                    if (new File(listAppsFragment.basepath + "/Changes/changelog.txt").exists()) {
                        SetPrefs.this.showMessage(SetPrefs.this.context, Utils.getText(C0149R.string.changelog), Utils.read_from_file(new File(listAppsFragment.basepath + "/Changes/changelog.txt")));
                    } else {
                        SetPrefs.this.showMessage(SetPrefs.this.context, Utils.getText(C0149R.string.changelog), Utils.getText(C0149R.string.not_found_changelog));
                    }
                }
            }

            public boolean onPreferenceClick(Preference preference) {
                LinearLayout d = (LinearLayout) View.inflate(SetPrefs.this.context, C0149R.layout.aboutdialog, null);
                LinearLayout body = (LinearLayout) d.findViewById(C0149R.id.dialogbodyscroll).findViewById(C0149R.id.dialogbody);
                TextView tv = (TextView) body.findViewById(C0149R.id.intonlydesc);
                tv.append(Utils.getColoredText(Utils.getText(C0149R.string.vers) + listAppsFragment.version + LogCollector.LINE_SEPARATOR, "#ffffffff", "bold"));
                tv.append(Utils.getColoredText("----------------------------------\n\n", "#ffffffff", "bold"));
                tv.append(Utils.getColoredText(Utils.getText(C0149R.string.about_working_dir) + " " + listAppsFragment.basepath + "\n\n", "#ffffffff", "bold"));
                tv.append(Utils.getColoredText(Utils.getText(C0149R.string.punkt1) + "\n\n", "#ffff0000", "bold"));
                tv.append(Utils.getColoredText(Utils.getText(C0149R.string.aboutlp) + LogCollector.LINE_SEPARATOR, "#ffffff00", "bold"));
                tv.append(Utils.getColoredText("\nThanks to:\nUsed source code: pyler, Mikanoshi@4pda.ru\nSite WebIconSet.com - Smile icons\np0izn@xda - English translation\nJang Seung Hun - Korean translation\nDerDownloader7,Simon Plantone - German translation\nAndrew Q., Simoconfa - Italian translation\ntrBilimBEKo,Z.Zeynalov,Noss_bar - Turkish translation\nSpoetnic - Dutch translation\n\u67d2\u6708\u96ea@BNB520,\u5356\u840c\u5b50,Hiapk-\u5929\u4f7f - Simplified Chinese translation\neauland, foXaCe - French translation\nrhrarhra@Mobilism.org, alireza_simkesh@XDA - Persian translation\nAlftronics & TheCh - Portuguese translation\nCaio Fons\u00eaca - Portuguese (Brasil) translation\n\u6f22\u9ed1\u7389,Hiapk-\u5929\u4f7f - Traditional Chinese translation\ns_h - Hebrew translation\nXcooM - Polish translation\nGizmo[SK],pyler - Slovak translation\nHarmeet Singh - Hindi translation\nPROXIMO - Bulgarian translation\nCulum - Indonesian translation\nslycog@XDA - Custom Patch Help translate\nCheng Sokdara (homi3kh) - Khmer translation\nrkononenko, masterlist@forpda, Volodiimr - Ukrainian translation\nJeduRocks@XDA - Spanish translation\nPaul Diosteanu - Romanian translation\ngidano - Hungarian translation\nOrbit09, Renek - Czech translation\ni_Droidi@Twitter - Arabic translation\nbobo1704 - Greek translation\nJack_Rover - Finnish translation\nWan Mohammad - Malay translation\nNguy\u1ec5n \u0110\u1ee9c L\u01b0\u1ee3ng - Vietnamese translation\nSOORAJ SR(fb.com/rsoorajs) - Malayalam translation\nEkberjan - Uighur translation\nevildog1 - Danish translation\nHzy980512 - Japanese translation\nNihar Patel) Gujarati translation\nChelpus, maksnogin - Russian translation\nIcons to menu - Sergey Kamashki\nTemplate for ADS - And123\n\nSupport by email: lp.chelpus@gmail.com\nGood Luck!\nChelpuS", "#ffffffff", "bold"));
                new Builder(SetPrefs.this.context).setTitle(C0149R.string.abouttitle).setCancelable(true).setIcon(17301659).setPositiveButton(Utils.getText(17039370), null).setNeutralButton("Changelog", new C01552()).setView(d).setOnCancelListener(new C01541()).create().show();
                return true;
            }
        });
        findPreference("language").setOnPreferenceClickListener(new OnPreferenceClickListener() {
            Dialog dialog6;

            class C01572 implements OnItemClickListener {
                C01572() {
                }

                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if (AnonymousClass26.this.dialog6.isShowing()) {
                        AnonymousClass26.this.dialog6.dismiss();
                    }
                    SharedPreferences settings = listAppsFragment.getInstance().getSharedPreferences("config", 0);
                    String[] tails = ((String) parent.getItemAtPosition(position)).split("_");
                    Locale locale = null;
                    if (tails.length == 1) {
                        locale = new Locale(tails[0]);
                    }
                    if (tails.length == 2) {
                        locale = new Locale(tails[0], tails[1], BuildConfig.VERSION_NAME);
                    }
                    if (tails.length == 3) {
                        locale = new Locale(tails[0], tails[1], tails[2]);
                    }
                    Locale.setDefault(locale);
                    Configuration appConfig2 = new Configuration();
                    appConfig2.locale = locale;
                    listAppsFragment.getRes().updateConfiguration(appConfig2, listAppsFragment.getRes().getDisplayMetrics());
                    settings.edit().putString("force_language", (String) parent.getItemAtPosition(position)).commit();
                    Intent intent = SetPrefs.this.getIntent();
                    intent.addFlags(AccessibilityNodeInfoCompat.ACTION_SET_SELECTION);
                    SetPrefs.this.finish();
                    SetPrefs.this.startActivity(intent);
                    settings.edit().putBoolean("settings_change", true).commit();
                    settings.edit().putBoolean("lang_change", true).commit();
                }
            }

            public boolean onPreferenceClick(Preference preference) {
                listAppsFragment.getRes();
                String[] langs = SetPrefs.this.getAssets().getLocales();
                ArrayList<String> s = new ArrayList();
                s.add("en_US");
                s.add("ar");
                s.add("bg");
                s.add("cs");
                s.add("da");
                s.add("de");
                s.add("el");
                s.add("es");
                s.add("fa");
                s.add("fi");
                s.add("fr");
                s.add("he");
                s.add("hi");
                s.add("hu");
                s.add("id");
                s.add("in");
                s.add("it");
                s.add("iw");
                s.add("ja");
                s.add("km");
                s.add("km_KH");
                s.add("ko");
                s.add("lt");
                s.add("ml");
                s.add("my");
                s.add("nl");
                s.add("pl");
                s.add("pt");
                s.add("pt_BR");
                s.add("ro");
                s.add("ru");
                s.add("sk");
                s.add("tr");
                s.add("ug");
                s.add("uk");
                s.add("vi");
                s.add("zh_CN");
                s.add("zh_HK");
                s.add("zh_TW");
                ArrayAdapter<String> adapt2 = new ArrayAdapter<String>(SetPrefs.this.context, C0149R.layout.contextmenu, s) {
                    public View getView(int position, View convertView, ViewGroup parent) {
                        View view = super.getView(position, convertView, parent);
                        TextView textView = (TextView) view.findViewById(C0149R.id.conttext);
                        textView.setTextColor(-1);
                        String it = (String) getItem(position);
                        Locale tmp = new Locale(it, it);
                        textView.setText(tmp.getDisplayName(tmp));
                        if (it.equals("my")) {
                            textView.setText("Malay translation");
                        }
                        textView.setTypeface(null, 1);
                        return view;
                    }
                };
                ListView modeList = new ListView(SetPrefs.this.context);
                Builder builder9 = new Builder(SetPrefs.this.context);
                if (adapt2 != null) {
                    adapt2.setNotifyOnChange(true);
                    modeList.setAdapter(adapt2);
                    modeList.invalidateViews();
                    modeList.setCacheColorHint(-16777216);
                    modeList.setBackgroundColor(-16777216);
                    modeList.setOnItemClickListener(new C01572());
                    builder9.setView(modeList);
                }
                this.dialog6 = builder9.create();
                this.dialog6.show();
                return true;
            }
        });
    }

    public static void copyFolder(File src, File dest) throws IOException {
        if (src.isDirectory()) {
            src.renameTo(dest);
            String[] files = src.list();
            if (files != null) {
                for (String file : files) {
                    copyFolder(new File(src, file), new File(dest, file));
                }
                return;
            }
            return;
        }
        src.renameTo(dest);
    }

    public boolean testPath(boolean showDialog, String path) {
        System.out.println("test path.");
        try {
            if (!new File(path).exists()) {
                new File(path).mkdirs();
            }
            if (!new File(path).exists()) {
                showMessage(this, Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.messagePath));
                return false;
            } else if (new File(path + "/tmp.txt").createNewFile()) {
                new File(path + "/tmp.txt").delete();
                return true;
            } else if (!showDialog) {
                return false;
            } else {
                showMessage(this, Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.messagePath));
                return false;
            }
        } catch (IOException e) {
            if (!showDialog) {
                return false;
            }
            showMessage(this, Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.messagePath));
            return false;
        }
    }

    private void showMessage(Context context, String title, String message) {
        final Dialog dialog = new Dialog(context);
        dialog.setTitle(title);
        dialog.setContentView(C0149R.layout.message);
        ((TextView) dialog.findViewById(C0149R.id.messageText)).setText(message);
        ((Button) dialog.findViewById(C0149R.id.buttonOk)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private boolean numberCheck(Object newValue) {
        if (!newValue.toString().equals(BuildConfig.VERSION_NAME) && newValue.toString().matches("\\d*")) {
            return true;
        }
        showMessage(this, Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.is_an_invalid_number));
        return false;
    }
}
