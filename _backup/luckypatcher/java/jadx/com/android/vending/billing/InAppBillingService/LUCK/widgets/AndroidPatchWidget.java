package com.android.vending.billing.InAppBillingService.LUCK.widgets;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.support.v4.internal.view.SupportMenu;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.widget.RemoteViews;
import com.android.vending.billing.InAppBillingService.LUCK.BuildConfig;
import com.android.vending.billing.InAppBillingService.LUCK.C0149R;
import com.android.vending.billing.InAppBillingService.LUCK.LogCollector;
import com.chelpus.Utils;

public class AndroidPatchWidget extends AppWidgetProvider {
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), C0149R.layout.android_patch_widget);
        int N = appWidgetIds.length;
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    public void onEnabled(Context context) {
    }

    public void onDisabled(Context context) {
    }

    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager, int appWidgetId) {
        int offset;
        int offset2;
        CharSequence widgetText = context.getString(C0149R.string.appwidget_text);
        RemoteViews views = new RemoteViews(context.getPackageName(), C0149R.layout.android_patch_widget);
        int i = 0;
        boolean patch1 = false;
        boolean patch2 = false;
        boolean patch3 = false;
        String str = BuildConfig.VERSION_NAME;
        if (Utils.checkCoreJarPatch11()) {
            i = 0 + 1;
            patch1 = true;
        }
        if (Utils.checkCoreJarPatch12()) {
            i++;
            patch1 = true;
        }
        if (i > 0) {
            str = Utils.getText(C0149R.string.contextcorepatch1) + "\n(" + i + "/2 patched)";
        } else {
            str = Utils.getText(C0149R.string.contextcorepatch1) + "\n(not patched)";
        }
        if (Utils.checkCoreJarPatch20()) {
            str = str + LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.contextcorepatch2) + "\n(patched)";
            patch2 = true;
        } else {
            str = str + LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.contextcorepatch2) + "\n(not patched)";
        }
        if (Utils.checkCoreJarPatch30(context.getPackageManager())) {
            str = str + LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.contextcorepatch3) + "\n(patched)";
            patch3 = true;
        } else {
            str = str + LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.contextcorepatch3) + "\n(not patched)";
        }
        SpannableString color_str = new SpannableString(str);
        if (patch1) {
            offset = Utils.getText(C0149R.string.contextcorepatch1).length() + (" (" + i + "/2 patched)").length();
            color_str.setSpan(new ForegroundColorSpan(-16711936), Utils.getText(C0149R.string.contextcorepatch1).length(), offset, 0);
        } else {
            offset = Utils.getText(C0149R.string.contextcorepatch1).length() + " (not patched)".length();
            color_str.setSpan(new ForegroundColorSpan(SupportMenu.CATEGORY_MASK), Utils.getText(C0149R.string.contextcorepatch1).length(), offset, 0);
        }
        if (patch2) {
            offset2 = ((Utils.getText(C0149R.string.contextcorepatch2).length() + offset) + 1) + " (patched)".length();
            color_str.setSpan(new ForegroundColorSpan(-16711936), (Utils.getText(C0149R.string.contextcorepatch2).length() + offset) + 1, offset2, 0);
        } else {
            offset2 = ((Utils.getText(C0149R.string.contextcorepatch2).length() + 1) + offset) + " (not patched)".length();
            color_str.setSpan(new ForegroundColorSpan(SupportMenu.CATEGORY_MASK), (Utils.getText(C0149R.string.contextcorepatch2).length() + 1) + offset, offset2, 0);
        }
        if (patch3) {
            color_str.setSpan(new ForegroundColorSpan(-16711936), (Utils.getText(C0149R.string.contextcorepatch3).length() + offset2) + 1, ((Utils.getText(C0149R.string.contextcorepatch3).length() + offset2) + 1) + " (patched)".length(), 0);
        } else {
            int offset3 = ((Utils.getText(C0149R.string.contextcorepatch3).length() + offset2) + 1) + " (not patched)".length();
            System.out.println(offset);
            System.out.println(offset2);
            System.out.println(str.length());
            color_str.setSpan(new ForegroundColorSpan(SupportMenu.CATEGORY_MASK), (Utils.getText(C0149R.string.contextcorepatch3).length() + offset2) + 1, offset3, 0);
        }
        views.setTextViewText(C0149R.id.appwidget_text, color_str);
        appWidgetManager.updateAppWidget(appWidgetId, views);
    }
}
