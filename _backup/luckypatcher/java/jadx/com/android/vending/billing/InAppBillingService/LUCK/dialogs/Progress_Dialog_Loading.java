package com.android.vending.billing.InAppBillingService.LUCK.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import com.android.vending.billing.InAppBillingService.LUCK.BuildConfig;
import com.android.vending.billing.InAppBillingService.LUCK.C0149R;
import com.android.vending.billing.InAppBillingService.LUCK.ProgressDlg;
import com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment;
import com.chelpus.Utils;

public class Progress_Dialog_Loading {
    public static ProgressDlg dialog = null;
    FragmentActivity context = null;
    Dialog dialog2 = null;
    FragmentManager fm = null;
    String message = BuildConfig.VERSION_NAME;
    String title = BuildConfig.VERSION_NAME;

    class C02281 implements OnCancelListener {
        C02281() {
        }

        public void onCancel(DialogInterface dialog) {
            if (listAppsFragment.su) {
                Utils.exitRoot();
            }
        }
    }

    class C02292 implements Runnable {
        C02292() {
        }

        public void run() {
            if (listAppsFragment.frag != null) {
                listAppsFragment.frag.getChildFragmentManager().executePendingTransactions();
            }
        }
    }

    class C02303 implements Runnable {
        C02303() {
        }

        public void run() {
            if (listAppsFragment.frag != null) {
                listAppsFragment.frag.getChildFragmentManager().executePendingTransactions();
            }
        }
    }

    class C02314 implements Runnable {
        C02314() {
        }

        public void run() {
            if (listAppsFragment.frag != null) {
                listAppsFragment.frag.getChildFragmentManager().executePendingTransactions();
            }
        }
    }

    class C02325 implements Runnable {
        C02325() {
        }

        public void run() {
            if (listAppsFragment.frag != null) {
                listAppsFragment.frag.getChildFragmentManager().executePendingTransactions();
            }
        }
    }

    class C02336 implements Runnable {
        C02336() {
        }

        public void run() {
            if (listAppsFragment.frag != null) {
                listAppsFragment.frag.getChildFragmentManager().executePendingTransactions();
            }
        }
    }

    class C02347 implements Runnable {
        C02347() {
        }

        public void run() {
            if (listAppsFragment.frag != null) {
                listAppsFragment.frag.getChildFragmentManager().executePendingTransactions();
            }
        }
    }

    public static Progress_Dialog_Loading newInstance() {
        return new Progress_Dialog_Loading();
    }

    public void showDialog() {
        if (this.dialog2 == null) {
            this.dialog2 = onCreateDialog();
        }
        if (this.dialog2 != null) {
            this.dialog2.show();
        }
    }

    public Dialog onCreateDialog() {
        dialog = new ProgressDlg(listAppsFragment.frag.getContext());
        dialog.setIncrementStyle();
        if (this.title.equals(BuildConfig.VERSION_NAME)) {
            this.title = Utils.getText(C0149R.string.download);
        }
        dialog.setTitle(this.title);
        if (this.message.equals(BuildConfig.VERSION_NAME)) {
            this.message = Utils.getText(C0149R.string.wait);
        }
        dialog.setMessage(this.message);
        dialog.setIcon(C0149R.drawable.ic_wait);
        dialog.setCancelable(false);
        dialog.setOnCancelListener(new C02281());
        return dialog.create();
    }

    public void setMessage(String text) {
        this.message = text;
        if (dialog == null) {
            onCreateDialog();
        }
        dialog.setMessage(this.message);
        try {
            if (!Utils.onMainThread()) {
                listAppsFragment.frag.runToMain(new C02292());
            } else if (listAppsFragment.frag != null) {
                listAppsFragment.frag.getChildFragmentManager().executePendingTransactions();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setIndeterminate(boolean indeterminate, Activity activity) {
        if (dialog == null) {
            onCreateDialog();
        }
        if (indeterminate) {
            dialog.setProgressNumberFormat(BuildConfig.VERSION_NAME);
        } else {
            dialog.setProgressNumberFormat("%1d/%2d");
        }
        if (indeterminate) {
            dialog.setDefaultStyle();
        } else {
            dialog.setIncrementStyle();
        }
        try {
            if (!Utils.onMainThread()) {
                listAppsFragment.frag.runToMain(new C02303());
            } else if (listAppsFragment.frag != null) {
                listAppsFragment.frag.getChildFragmentManager().executePendingTransactions();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setMax(int max) {
        if (dialog == null) {
            onCreateDialog();
        }
        dialog.setMax(max);
        try {
            if (!Utils.onMainThread()) {
                listAppsFragment.frag.runToMain(new C02314());
            } else if (listAppsFragment.frag != null) {
                listAppsFragment.frag.getChildFragmentManager().executePendingTransactions();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setProgressNumberFormat(String format) {
        if (dialog == null) {
            onCreateDialog();
        }
        dialog.setProgressNumberFormat(format);
        try {
            if (!Utils.onMainThread()) {
                listAppsFragment.frag.runToMain(new C02325());
            } else if (listAppsFragment.frag != null) {
                listAppsFragment.frag.getChildFragmentManager().executePendingTransactions();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setProgress(int progress) {
        if (dialog == null) {
            onCreateDialog();
        }
        dialog.setProgress(progress);
        try {
            if (!Utils.onMainThread()) {
                listAppsFragment.frag.runToMain(new C02336());
            } else if (listAppsFragment.frag != null) {
                listAppsFragment.frag.getChildFragmentManager().executePendingTransactions();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isShowing() {
        if (dialog == null || !dialog.isShowing()) {
            return false;
        }
        return true;
    }

    public void setTitle(String text) {
        this.title = text;
        if (dialog == null) {
            onCreateDialog();
        }
        if (dialog != null) {
            dialog.setTitle(this.title);
            try {
                if (!Utils.onMainThread()) {
                    listAppsFragment.frag.runToMain(new C02347());
                } else if (listAppsFragment.frag != null) {
                    listAppsFragment.frag.getChildFragmentManager().executePendingTransactions();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void setCancelable(boolean trig) {
        if (this.dialog2 != null) {
            this.dialog2.setCancelable(trig);
        }
    }

    public void dismiss() {
        if (this.dialog2 != null) {
            this.dialog2.dismiss();
            this.dialog2 = null;
        }
    }
}
