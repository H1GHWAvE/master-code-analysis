package com.android.vending.billing.InAppBillingService.LUCK.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.TextView.BufferType;
import com.android.vending.billing.InAppBillingService.LUCK.AlertDlg;
import com.android.vending.billing.InAppBillingService.LUCK.BuildConfig;
import com.android.vending.billing.InAppBillingService.LUCK.C0149R;
import com.android.vending.billing.InAppBillingService.LUCK.LogCollector;
import com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment;
import com.chelpus.Utils;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import net.lingala.zip4j.util.InternalZipConstants;
import org.tukaani.xz.LZMA2Options;

public class App_Dialog {
    Dialog dialog = null;

    class C01902 implements OnCancelListener {
        C01902() {
        }

        public void onCancel(DialogInterface dialog) {
        }
    }

    public void showDialog() {
        if (this.dialog == null) {
            this.dialog = onCreateDialog();
        }
        if (this.dialog != null) {
            this.dialog.show();
        }
    }

    public Dialog onCreateDialog() {
        System.out.println("App Dialog create.");
        if (listAppsFragment.frag == null || listAppsFragment.frag.getContext() == null) {
            dismiss();
        }
        final SpannableStringBuilder builder = new SpannableStringBuilder();
        final SpannableStringBuilder builder2 = new SpannableStringBuilder();
        final SpannableStringBuilder builder3 = new SpannableStringBuilder();
        LinearLayout d = (LinearLayout) View.inflate(listAppsFragment.frag.getContext(), C0149R.layout.applicationdialog, null);
        LinearLayout scrbody = (LinearLayout) d.findViewById(C0149R.id.appbodyscroll).findViewById(C0149R.id.appdialogbody);
        final TextView txt = (TextView) scrbody.findViewById(C0149R.id.app_textView);
        final TextView txt2 = (TextView) scrbody.findViewById(C0149R.id.app_textView2);
        final TextView txt3 = (TextView) scrbody.findViewById(C0149R.id.app_textView3);
        final TextView txt4 = (TextView) scrbody.findViewById(C0149R.id.app_textView31);
        final ProgressBar bar = (ProgressBar) scrbody.findViewById(C0149R.id.progressBar1);
        try {
            ((ImageView) scrbody.findViewById(C0149R.id.app_imageView)).setImageDrawable(listAppsFragment.getPkgMng().getApplicationIcon(listAppsFragment.pli.pkgName));
        } catch (NameNotFoundException e) {
            try {
                e.printStackTrace();
            } catch (OutOfMemoryError E) {
                E.printStackTrace();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        new Thread(new Runnable() {

            class C01862 implements Runnable {
                C01862() {
                }

                public void run() {
                    txt2.setText(builder, BufferType.SPANNABLE);
                }
            }

            class C01873 implements Runnable {
                C01873() {
                }

                public void run() {
                    txt4.setText(builder3, BufferType.SPANNABLE);
                }
            }

            class C01884 implements Runnable {
                C01884() {
                }

                public void run() {
                    txt3.setText(builder2, BufferType.SPANNABLE);
                    bar.setVisibility(8);
                }
            }

            public void run() {
                String str2 = BuildConfig.VERSION_NAME;
                final SpannableString ios = Utils.getColoredText(listAppsFragment.pli.name, "#be6e17", "bold");
                listAppsFragment.handler.post(new Runnable() {
                    public void run() {
                        txt.setText(ios);
                        txt2.setText(BuildConfig.VERSION_NAME);
                    }
                });
                if (listAppsFragment.pli.enable) {
                    str2 = Utils.getText(C0149R.string.enabled_package) + LogCollector.LINE_SEPARATOR;
                    builder.append(Utils.getColoredText(str2, BuildConfig.VERSION_NAME, BuildConfig.VERSION_NAME));
                } else {
                    str2 = Utils.getText(C0149R.string.disabled_package) + LogCollector.LINE_SEPARATOR;
                    builder.append(Utils.getColoredText(str2, BuildConfig.VERSION_NAME, BuildConfig.VERSION_NAME));
                }
                if (listAppsFragment.pli.custom) {
                    builder.append(Utils.getColoredText(Utils.getText(C0149R.string.statcustom) + LogCollector.LINE_SEPARATOR, -990142, BuildConfig.VERSION_NAME));
                }
                if (listAppsFragment.pli.lvl) {
                    builder.append(Utils.getColoredText(Utils.getText(C0149R.string.statlvl) + LogCollector.LINE_SEPARATOR, -16711821, BuildConfig.VERSION_NAME));
                }
                if (listAppsFragment.pli.ads) {
                    builder.append(Utils.getColoredText(Utils.getText(C0149R.string.statads) + LogCollector.LINE_SEPARATOR, -990142, BuildConfig.VERSION_NAME));
                }
                if (!(listAppsFragment.pli.ads || listAppsFragment.pli.lvl || listAppsFragment.pli.custom)) {
                    builder.append(Utils.getColoredText(Utils.getText(C0149R.string.statnull) + LogCollector.LINE_SEPARATOR, -65451, BuildConfig.VERSION_NAME));
                }
                if (listAppsFragment.pli.modified) {
                    builder.append(Utils.getColoredText(Utils.getText(C0149R.string.statmodified) + LogCollector.LINE_SEPARATOR, -16711821, BuildConfig.VERSION_NAME));
                } else {
                    str2 = Utils.getText(C0149R.string.statnotmodified) + LogCollector.LINE_SEPARATOR;
                    builder.append(Utils.getColoredText(str2, BuildConfig.VERSION_NAME, BuildConfig.VERSION_NAME));
                }
                if (listAppsFragment.pli.odex) {
                    builder.append(Utils.getColoredText(Utils.getText(C0149R.string.statfix) + LogCollector.LINE_SEPARATOR, -990142, BuildConfig.VERSION_NAME));
                } else {
                    str2 = Utils.getText(C0149R.string.statnotfix) + LogCollector.LINE_SEPARATOR;
                    builder.append(Utils.getColoredText(str2, BuildConfig.VERSION_NAME, BuildConfig.VERSION_NAME));
                }
                if (listAppsFragment.pli.system) {
                    builder.append(Utils.getColoredText(Utils.getText(C0149R.string.statsys), -162281, BuildConfig.VERSION_NAME));
                } else {
                    builder.append(Utils.getColoredText(Utils.getText(C0149R.string.statnotsys), -16711821, BuildConfig.VERSION_NAME));
                }
                listAppsFragment.handler.post(new C01862());
                String[] requestedPermissions = new String[0];
                String[] decripts = new String[0];
                PackageManager pm = listAppsFragment.getPkgMng();
                builder3.clear();
                requestedPermissions = pm.getPackageInfo(listAppsFragment.pli.pkgName, LZMA2Options.DICT_SIZE_MIN).requestedPermissions;
                if (requestedPermissions != null && requestedPermissions.length > 0) {
                    decripts = new String[requestedPermissions.length];
                    for (int i = 0; i < requestedPermissions.length; i++) {
                        builder3.append(Utils.getColoredText(requestedPermissions[i] + LogCollector.LINE_SEPARATOR, "#6699cc", "bold"));
                        String result = null;
                        try {
                            result = pm.getPermissionInfo(requestedPermissions[i], 0).loadDescription(pm).toString();
                        } catch (NameNotFoundException e) {
                            result = null;
                        } catch (Exception e2) {
                        }
                        if (result == null) {
                            try {
                                str2 = Utils.getText(C0149R.string.permission_not_descr) + "\n\n";
                            } catch (NameNotFoundException e3) {
                                e3.printStackTrace();
                            }
                        } else {
                            str2 = result + "\n\n";
                        }
                        builder3.append(Utils.getColoredText(str2, BuildConfig.VERSION_NAME, BuildConfig.VERSION_NAME));
                    }
                }
                listAppsFragment.handler.post(new C01873());
                builder2.clear();
                builder2.append(Utils.getColoredText("Package name:\n", "#6699cc", "bold"));
                str2 = listAppsFragment.pli.pkgName + LogCollector.LINE_SEPARATOR;
                builder2.append(Utils.getColoredText(str2, BuildConfig.VERSION_NAME, BuildConfig.VERSION_NAME));
                builder2.append(Utils.getColoredText(Utils.getText(C0149R.string.stat_launch_activity) + LogCollector.LINE_SEPARATOR, "#6699cc", "bold"));
                try {
                    str2 = listAppsFragment.getPkgMng().getLaunchIntentForPackage(listAppsFragment.pli.pkgName).getComponent().getClassName() + LogCollector.LINE_SEPARATOR;
                    builder2.append(Utils.getColoredText(str2, BuildConfig.VERSION_NAME, BuildConfig.VERSION_NAME));
                } catch (Exception e32) {
                    e32.printStackTrace();
                }
                try {
                    long size;
                    builder2.append(Utils.getColoredText(Utils.getText(C0149R.string.statappplace) + LogCollector.LINE_SEPARATOR, "#6699cc", "bold"));
                    try {
                        str2 = listAppsFragment.getPkgMng().getPackageInfo(listAppsFragment.pli.pkgName, 0).applicationInfo.sourceDir;
                        builder2.append(Utils.getColoredText(str2, BuildConfig.VERSION_NAME, BuildConfig.VERSION_NAME));
                        builder2.append(Utils.getColoredText(LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.statdataplace) + LogCollector.LINE_SEPARATOR, "#6699cc", "bold"));
                        str2 = listAppsFragment.getPkgMng().getPackageInfo(listAppsFragment.pli.pkgName, 0).applicationInfo.dataDir + InternalZipConstants.ZIP_FILE_SEPARATOR;
                        builder2.append(Utils.getColoredText(str2, BuildConfig.VERSION_NAME, BuildConfig.VERSION_NAME));
                        builder2.append(Utils.getColoredText(LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.statversion) + " ", "#6699cc", "bold"));
                        str2 = listAppsFragment.getPkgMng().getPackageInfo(listAppsFragment.pli.pkgName, 0).versionName;
                        builder2.append(Utils.getColoredText(str2, BuildConfig.VERSION_NAME, BuildConfig.VERSION_NAME));
                        builder2.append(Utils.getColoredText(LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.statbuild) + " ", "#6699cc", "bold"));
                        str2 = BuildConfig.VERSION_NAME + listAppsFragment.getPkgMng().getPackageInfo(listAppsFragment.pli.pkgName, 0).versionCode;
                        builder2.append(Utils.getColoredText(str2, BuildConfig.VERSION_NAME, BuildConfig.VERSION_NAME));
                        builder2.append(Utils.getColoredText(LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.statuserid) + " ", "#6699cc", "bold"));
                        str2 = BuildConfig.VERSION_NAME + listAppsFragment.getPkgMng().getPackageInfo(listAppsFragment.pli.pkgName, 0).applicationInfo.uid;
                        builder2.append(Utils.getColoredText(str2, BuildConfig.VERSION_NAME, BuildConfig.VERSION_NAME));
                        builder2.append(Utils.getColoredText(LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.install_date) + " ", "#6699cc", "bold"));
                        str2 = BuildConfig.VERSION_NAME + new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date(((long) listAppsFragment.pli.updatetime) * 1000));
                        System.out.println(listAppsFragment.pli.updatetime);
                        System.out.println(((long) listAppsFragment.pli.updatetime) * 1000);
                        builder2.append(Utils.getColoredText(str2, BuildConfig.VERSION_NAME, BuildConfig.VERSION_NAME));
                        builder2.append(Utils.getColoredText("\n\n" + Utils.getText(C0149R.string.apk_size) + " ", "#6699cc", "bold"));
                        str2 = String.format("%.3f", new Object[]{Float.valueOf(((float) new File(listAppsFragment.getPkgMng().getPackageInfo(listAppsFragment.pli.pkgName, 0).applicationInfo.sourceDir).length()) / 1048576.0f)}) + " Mb";
                        builder2.append(Utils.getColoredText(str2, BuildConfig.VERSION_NAME, BuildConfig.VERSION_NAME));
                    } catch (NameNotFoundException e1) {
                        e1.printStackTrace();
                    }
                    try {
                        builder2.append(Utils.getColoredText(LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.dalvik_cache_size) + " ", "#6699cc", "bold"));
                        size = 0;
                        try {
                            size = Utils.getFileDalvikCache(listAppsFragment.getPkgMng().getPackageInfo(listAppsFragment.pli.pkgName, 0).applicationInfo.sourceDir).length();
                        } catch (Exception e4) {
                        }
                        str2 = String.format("%.3f", new Object[]{Float.valueOf(((float) size) / 1048576.0f)}) + " Mb";
                        builder2.append(Utils.getColoredText(str2, BuildConfig.VERSION_NAME, BuildConfig.VERSION_NAME));
                    } catch (Exception e5) {
                    }
                    if (listAppsFragment.su) {
                        try {
                            builder2.append(Utils.getColoredText(LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.data_dir_size) + " ", "#6699cc", "bold"));
                            String size2 = BuildConfig.VERSION_NAME;
                            try {
                                size = new Utils(BuildConfig.VERSION_NAME).cmdRoot(listAppsFragment.dalvikruncommand + ".checkDataSize " + listAppsFragment.getPkgMng().getPackageInfo(listAppsFragment.pli.pkgName, 0).applicationInfo.dataDir).replace("SU Java-Code Running!\n", BuildConfig.VERSION_NAME);
                            } catch (Exception e6) {
                            }
                            str2 = size.replace(LogCollector.LINE_SEPARATOR, BuildConfig.VERSION_NAME).replace("\r", BuildConfig.VERSION_NAME) + " Mb";
                            builder2.append(Utils.getColoredText(str2, BuildConfig.VERSION_NAME, BuildConfig.VERSION_NAME));
                        } catch (Exception e7) {
                        }
                    }
                    listAppsFragment.handler.post(new C01884());
                } catch (NullPointerException e8) {
                    e8.printStackTrace();
                }
            }
        }).start();
        Dialog tempdialog = new AlertDlg(listAppsFragment.frag.getContext(), true).setView(d).create();
        tempdialog.setCancelable(true);
        tempdialog.setOnCancelListener(new C01902());
        return tempdialog;
    }

    public void dismiss() {
        if (this.dialog != null) {
            this.dialog.dismiss();
            this.dialog = null;
        }
    }
}
