package com.android.vending.billing.InAppBillingService.LUCK.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.support.v4.app.FragmentManager;
import com.android.vending.billing.InAppBillingService.LUCK.BuildConfig;
import com.android.vending.billing.InAppBillingService.LUCK.ProgressDlg;
import com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment;
import com.chelpus.Utils;

public class Progress_Dialog_2 {
    public static ProgressDlg dialog;
    Dialog dialog2 = null;
    FragmentManager fm = null;
    String message = BuildConfig.VERSION_NAME;
    String title = BuildConfig.VERSION_NAME;

    class C02221 implements OnCancelListener {
        C02221() {
        }

        public void onCancel(DialogInterface dialog) {
            if (listAppsFragment.su) {
                Utils.exitRoot();
            }
        }
    }

    class C02232 implements Runnable {
        C02232() {
        }

        public void run() {
            if (listAppsFragment.frag != null) {
                listAppsFragment.frag.getChildFragmentManager().executePendingTransactions();
            }
        }
    }

    class C02243 implements Runnable {
        C02243() {
        }

        public void run() {
            if (listAppsFragment.frag != null) {
                listAppsFragment.frag.getChildFragmentManager().executePendingTransactions();
            }
        }
    }

    class C02254 implements Runnable {
        C02254() {
        }

        public void run() {
            if (listAppsFragment.frag != null) {
                listAppsFragment.frag.getChildFragmentManager().executePendingTransactions();
            }
        }
    }

    class C02265 implements Runnable {
        C02265() {
        }

        public void run() {
            if (listAppsFragment.frag != null) {
                listAppsFragment.frag.getChildFragmentManager().executePendingTransactions();
            }
        }
    }

    class C02276 implements Runnable {
        C02276() {
        }

        public void run() {
            if (listAppsFragment.frag != null) {
                listAppsFragment.frag.getChildFragmentManager().executePendingTransactions();
            }
        }
    }

    public static Progress_Dialog_2 newInstance() {
        return new Progress_Dialog_2();
    }

    public void showDialog() {
        if (this.dialog2 == null) {
            this.dialog2 = onCreateDialog();
        }
        if (this.dialog2 != null) {
            this.dialog2.show();
        }
    }

    public Dialog onCreateDialog() {
        dialog = new ProgressDlg(listAppsFragment.frag.getContext());
        if (this.message.equals(BuildConfig.VERSION_NAME)) {
            this.message = "Loading...";
        }
        dialog.setMessage(this.message);
        dialog.setCancelable(true);
        dialog.setOnCancelListener(new C02221());
        return dialog.create();
    }

    public void setMessage(String text) {
        this.message = text;
        if (dialog == null) {
            onCreateDialog();
        }
        dialog.setMessage(this.message);
        listAppsFragment.frag.getContext().runOnUiThread(new C02232());
    }

    public void setIndeterminate(boolean indeterminate) {
        if (dialog == null) {
            onCreateDialog();
        }
        if (indeterminate) {
            dialog.setDefaultStyle();
        } else {
            dialog.setIncrementStyle();
        }
        listAppsFragment.frag.getContext().runOnUiThread(new C02243());
    }

    public void setMax(int max) {
        if (dialog == null) {
            onCreateDialog();
        }
        dialog.setMax(max);
        listAppsFragment.frag.getContext().runOnUiThread(new C02254());
    }

    public void setProgress(int progress) {
        if (dialog == null) {
            onCreateDialog();
        }
        dialog.setProgress(progress);
        listAppsFragment.frag.getContext().runOnUiThread(new C02265());
    }

    public void setCancelable(boolean trig) {
        if (this.dialog2 != null) {
            this.dialog2.setCancelable(trig);
        }
    }

    public boolean isShowing() {
        if (this.dialog2 == null || !this.dialog2.isShowing()) {
            return false;
        }
        return true;
    }

    public void setTitle(String text) {
        if (dialog == null) {
            onCreateDialog();
        }
        this.title = text;
        dialog.setTitle(this.title);
        listAppsFragment.frag.getContext().runOnUiThread(new C02276());
    }

    public void dismiss() {
        if (this.dialog2 != null) {
            this.dialog2.dismiss();
            this.dialog2 = null;
        }
    }
}
