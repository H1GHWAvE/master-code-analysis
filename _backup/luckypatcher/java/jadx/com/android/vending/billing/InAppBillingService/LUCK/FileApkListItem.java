package com.android.vending.billing.InAppBillingService.LUCK;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import java.io.File;

public class FileApkListItem {
    public File backupfile;
    public Drawable icon;
    public String name = "Bad file";
    public String pkgName;
    public int versionCode;
    public String versionName;

    public FileApkListItem(Context context, File file, boolean getIcon) throws IllegalArgumentException {
        this.backupfile = file;
        PackageManager pm = listAppsFragment.getPkgMng();
        PackageInfo info = pm.getPackageArchiveInfo(file.toString(), 1);
        if (info != null) {
            ApplicationInfo appInfo = info.applicationInfo;
            if (VERSION.SDK_INT >= 8) {
                appInfo.sourceDir = file.toString();
                appInfo.publicSourceDir = file.toString();
            }
            this.pkgName = info.packageName;
            this.name = appInfo.loadLabel(pm).toString();
            if (getIcon) {
                int imgSize = (int) ((35.0f * listAppsFragment.getRes().getDisplayMetrics().density) + 0.5f);
                Bitmap iconka = null;
                try {
                    iconka = ((BitmapDrawable) appInfo.loadIcon(pm)).getBitmap();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                int width = iconka.getWidth();
                int height = iconka.getHeight();
                float scaleWidth = ((float) imgSize) / ((float) width);
                float scaleHeight = ((float) imgSize) / ((float) height);
                Matrix bMatrix = new Matrix();
                bMatrix.postScale(scaleWidth, scaleHeight);
                this.icon = new BitmapDrawable(Bitmap.createBitmap(iconka, 0, 0, width, height, bMatrix, true));
                appInfo.loadIcon(pm);
            }
            this.versionName = info.versionName;
            this.versionCode = info.versionCode;
            return;
        }
        throw new IllegalArgumentException("oblom");
    }
}
