package com.android.vending.billing.InAppBillingService.LUCK;

public class PatchesItemAuto {
    public String marker = BuildConfig.VERSION_NAME;
    public boolean markerTrig = false;
    public byte[] origByte;
    public int[] origMask;
    public boolean pattern = false;
    public byte[] repByte;
    public int[] repMask;
    public boolean result = false;
    public String resultText = BuildConfig.VERSION_NAME;

    public PatchesItemAuto(byte[] orig, int[] mask, byte[] repl, int[] rmask, boolean patt, String ResultText, String mark) {
        this.origByte = new byte[orig.length];
        this.origByte = orig;
        this.origMask = new int[mask.length];
        this.origMask = mask;
        this.repByte = new byte[repl.length];
        this.repByte = repl;
        this.repMask = new int[rmask.length];
        this.repMask = rmask;
        this.pattern = patt;
        this.resultText = ResultText;
        this.marker = mark;
    }
}
