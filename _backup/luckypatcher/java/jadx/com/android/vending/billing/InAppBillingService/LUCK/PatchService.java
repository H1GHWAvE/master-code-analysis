package com.android.vending.billing.InAppBillingService.LUCK;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.widget.Toast;
import com.android.vending.billing.InAppBillingService.LUCK.widgets.AppDisablerWidget;
import com.android.vending.billing.InAppBillingService.LUCK.widgets.BinderWidget;
import com.chelpus.Utils;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import net.lingala.zip4j.util.InternalZipConstants;
import org.json.JSONException;
import org.json.JSONObject;

public class PatchService extends Service {
    public static int notifyIndex = 50;
    public Context context = null;
    final Handler handler = new Handler();
    private Thread f2t;

    class C01341 implements Runnable {

        class C01311 implements Runnable {
            C01311() {
            }

            public void run() {
                Toast.makeText(PatchService.this.getApplicationContext(), "LuckyPatcher: clear dalvik-cache failed. Please clear dalvik-cache manual.", 1).show();
            }
        }

        class C01322 implements Runnable {
            C01322() {
            }

            public void run() {
                listAppsFragment.binderWidget = true;
                Intent i = new Intent(PatchService.this.context, BinderWidget.class);
                i.setAction(BinderWidget.ACTION_WIDGET_RECEIVER_Updater);
                PatchService.this.sendBroadcast(i);
            }
        }

        class C01333 implements Runnable {
            C01333() {
            }

            public void run() {
                listAppsFragment.appDisabler = true;
                Intent i = new Intent(PatchService.this.getApplicationContext(), AppDisablerWidget.class);
                i.setAction(AppDisablerWidget.ACTION_WIDGET_RECEIVER_Updater);
                PatchService.this.sendBroadcast(i);
            }
        }

        C01341() {
        }

        public void run() {
            Exception e;
            BufferedReader br;
            String data;
            String out;
            String[] tails;
            int[] ids;
            File file = new File(PatchService.this.getApplicationContext().getFilesDir() + "/AndroidManifest.xml");
            if (file.exists()) {
                file.delete();
            }
            file = new File(PatchService.this.getApplicationContext().getFilesDir() + "/classes.dex");
            if (file.exists()) {
                file.delete();
            }
            file = new File(PatchService.this.getApplicationContext().getFilesDir() + "/classes.dex.apk");
            if (file.exists()) {
                file.delete();
            }
            if (Utils.exists(listAppsFragment.toolfilesdir + "/ClearDalvik.on")) {
                String str = BuildConfig.VERSION_NAME;
                Utils.remount("/system", InternalZipConstants.WRITE_MODE);
                try {
                    System.out.println(new Utils(BuildConfig.VERSION_NAME).cmdRoot(listAppsFragment.dalvikruncommand + ".clearDalvikCache " + PatchService.this.getApplicationContext().getFilesDir().getAbsolutePath()));
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
                PatchService.this.handler.post(new C01311());
                System.out.println(PatchService.this.getApplicationContext().getFilesDir() + "/reboot");
                Utils.reboot();
            }
            String supath = PatchService.this.getApplicationInfo().sourceDir;
            PackageManager pm = PatchService.this.getPackageManager();
            String system = "not_system";
            try {
                File[] list = PatchService.this.getApplicationContext().getDir("bootlist", 0).listFiles();
                System.out.println("LuckyPatcher (count patches to boot): " + list.length);
                if (list.length > 0) {
                    PatchService.this.dexoptcopy();
                }
                int w = 0;
                File tmp1 = file;
                while (true) {
                    File tmp12;
                    try {
                        if (w >= list.length) {
                            break;
                        }
                        System.out.println(Environment.getExternalStorageState());
                        try {
                            if (list[w].getName().endsWith("bootlist") || list[w].getName().endsWith("dexopt-wrapper") || list[w].getName().endsWith("zip") || list[w].getName().endsWith("zipalign") || list[w].getName().endsWith("busybox") || list[w].getName().toLowerCase().endsWith(".db") || list[w].getName().toLowerCase().endsWith("dalvikvm") || list[w].getName().toLowerCase().endsWith("ClearDalvik.on")) {
                                tmp12 = tmp1;
                                w++;
                                tmp1 = tmp12;
                            } else {
                                file = new File(PatchService.this.getApplicationContext().getFilesDir() + "/AndroidManifest.xml");
                                try {
                                    if (file.exists()) {
                                        file.delete();
                                    }
                                    file = new File(PatchService.this.getApplicationContext().getFilesDir() + "/classes.dex");
                                    if (file.exists()) {
                                        file.delete();
                                    }
                                    file = new File(PatchService.this.getApplicationContext().getFilesDir() + "/classes.dex.apk");
                                    if (file.exists()) {
                                        file.delete();
                                    }
                                    System.out.println("LuckyPatcher (Apply Patch on Boot): " + list[w].getName() + " index:" + w);
                                    String str2 = "empty";
                                    try {
                                        str2 = "empty";
                                        try {
                                            str2 = pm.getPackageInfo(list[w].getName(), 0).applicationInfo.sourceDir;
                                        } catch (Exception e22) {
                                            System.out.println("LuckyPatcher (Boot - PackageManager): " + e22);
                                        }
                                        System.out.println("LuckyPatcher (AppDir to patch): " + str2);
                                        if (str2.equals("empty")) {
                                            str2 = "/mnt/asec/" + list[w].getName() + "-1/pkg.apk";
                                        }
                                        if (str2.contains("/system/app")) {
                                            system = "system";
                                        }
                                        if (!str2.contains("/system/app")) {
                                            system = "not_system";
                                        }
                                        String[] async = new String[]{BuildConfig.VERSION_NAME, list[w].getName(), PatchService.this.getApplicationContext().getFilesDir().toString()};
                                        try {
                                            str = BuildConfig.VERSION_NAME;
                                            System.out.println("LuckyPatcher (BootRun):  " + async[1]);
                                            Utils.remount("/mnt/asec/" + async[1], InternalZipConstants.WRITE_MODE);
                                            Utils.remount("/mnt/asec/" + async[1] + "-1", InternalZipConstants.WRITE_MODE);
                                            Utils.remount("/mnt/asec/" + async[1] + "-2", InternalZipConstants.WRITE_MODE);
                                            String uid = String.valueOf(listAppsFragment.getPkgMng().getPackageInfo(list[w].getName(), 0).applicationInfo.uid);
                                            String run_command = listAppsFragment.dalvikruncommandWithFramework;
                                            if (!Utils.isWithFramework()) {
                                                run_command = listAppsFragment.dalvikruncommand;
                                            }
                                            System.out.println(run_command + ".custompatch " + list[w].getName() + " " + list[w].toString() + " " + str2 + " " + Environment.getExternalStorageDirectory() + " " + PatchService.this.getApplicationContext().getFilesDir() + " " + supath + " " + system + " " + Build.CPU_ABI + " " + "com.chelpus.root.utils" + " " + Utils.getCurrentRuntimeValue() + " " + uid + LogCollector.LINE_SEPARATOR);
                                            str = new Utils(BuildConfig.VERSION_NAME).cmdRoot(run_command + ".custompatch " + list[w].getName() + " " + list[w].toString() + " " + str2 + " " + Environment.getExternalStorageDirectory() + " " + PatchService.this.getApplicationContext().getFilesDir() + " " + supath + " " + system + " " + Build.CPU_ABI + " " + "com.chelpus.root.utils" + " " + Utils.getCurrentRuntimeValue() + " " + uid + LogCollector.LINE_SEPARATOR);
                                            System.out.println("LuckyPatcher (BootResult):\n" + str);
                                            PatchService.this.showNotify(PatchService.notifyIndex, "Lucky Patcher - " + Utils.getText(C0149R.string.notify_patch_on_boot), Utils.getText(C0149R.string.notify_patch_on_boot), async[1] + " " + Utils.getText(C0149R.string.notify_patched));
                                            PatchService.notifyIndex++;
                                            String[] mass = new String[str.split(LogCollector.LINE_SEPARATOR).length];
                                            mass = str.split(LogCollector.LINE_SEPARATOR);
                                            br = new BufferedReader(new InputStreamReader(new FileInputStream(list[w]), "UTF-8"));
                                            data = BuildConfig.VERSION_NAME;
                                            String[] txtdata = new String[2000];
                                            boolean component = false;
                                            while (true) {
                                                data = br.readLine();
                                                if (data == null) {
                                                    break;
                                                }
                                                txtdata[0] = data;
                                                if (txtdata[0].toUpperCase().contains("[") && txtdata[0].toUpperCase().contains("]")) {
                                                    component = false;
                                                }
                                                if (txtdata[0].toUpperCase().contains("[COMPONENT]")) {
                                                    component = true;
                                                }
                                                if (component) {
                                                    String value1 = BuildConfig.VERSION_NAME;
                                                    if (txtdata[0].contains("enable")) {
                                                        try {
                                                            value1 = new JSONObject(txtdata[0]).getString("enable");
                                                        } catch (JSONException e3) {
                                                            System.out.println("Error get component!");
                                                        }
                                                        value1 = value1.trim();
                                                        new Utils(BuildConfig.VERSION_NAME).cmdRoot("pm enable '" + listAppsFragment.pli.pkgName + InternalZipConstants.ZIP_FILE_SEPARATOR + value1 + "'");
                                                    }
                                                    if (txtdata[0].contains("disable")) {
                                                        try {
                                                            value1 = new JSONObject(txtdata[0]).getString("disable");
                                                        } catch (JSONException e4) {
                                                            System.out.println("Error get component!");
                                                        }
                                                        try {
                                                            value1 = value1.trim();
                                                            new Utils(BuildConfig.VERSION_NAME).cmdRoot("pm disable '" + listAppsFragment.pli.pkgName + InternalZipConstants.ZIP_FILE_SEPARATOR + value1 + "'");
                                                        } catch (Exception e222) {
                                                            e222.printStackTrace();
                                                        }
                                                    } else {
                                                        continue;
                                                    }
                                                }
                                            }
                                            for (int i = 0; i < mass.length; i++) {
                                            }
                                        } catch (Exception e2222) {
                                            System.out.println("LuckyPatcher (BootPatchError3): " + e2222);
                                        }
                                    } catch (Exception e22222) {
                                        System.out.println("LuckyPatcher (noBootError2): " + e22222);
                                    }
                                } catch (Exception e5) {
                                    e22222 = e5;
                                }
                                w++;
                                tmp1 = tmp12;
                            }
                        } catch (Exception e6) {
                            e22222 = e6;
                            tmp12 = tmp1;
                            System.out.println("LuckyPatcher (noBootError3): " + e22222);
                            w++;
                            tmp1 = tmp12;
                        }
                    } catch (Exception e7) {
                        e22222 = e7;
                        tmp12 = tmp1;
                    }
                }
            } catch (Exception e8) {
                e22222 = e8;
            }
            File bindfile = new File(PatchService.this.getDir("binder", 0) + "/bind.txt");
            if (bindfile.exists() && bindfile.length() > 0) {
                System.out.println("LuckyPatcher binder start!");
                try {
                    if (!bindfile.exists()) {
                        bindfile.createNewFile();
                    }
                    FileInputStream fis = new FileInputStream(bindfile);
                    br = new BufferedReader(new InputStreamReader(fis, "UTF-8"));
                    data = BuildConfig.VERSION_NAME;
                    String temp = BuildConfig.VERSION_NAME;
                    out = BuildConfig.VERSION_NAME;
                    while (true) {
                        data = br.readLine();
                        if (data != null) {
                            break;
                        }
                        tails = data.split(";");
                        if (tails.length == 2) {
                            Utils.verify_bind_and_run("mount", "-o bind '" + tails[0] + "' '" + tails[1] + "'", tails[0], tails[1]);
                            if (Utils.checkBind(new BindItem(tails[0], tails[1]))) {
                                System.out.println("LuckyPatcher: show notify");
                                out = out + PatchService.this.getString(C0149R.string.notify_directory_binder_from) + " " + tails[0] + " " + PatchService.this.getString(C0149R.string.notify_directory_binder_to) + " " + tails[1] + LogCollector.LINE_SEPARATOR;
                            }
                        }
                    }
                    PatchService.this.showNotify(PatchService.notifyIndex, "Lucky Patcher - " + PatchService.this.getString(C0149R.string.notify_directory_binder), PatchService.this.getString(C0149R.string.notify_directory_binder), out);
                    PatchService.notifyIndex++;
                    try {
                        ids = AppWidgetManager.getInstance(PatchService.this.context).getAppWidgetIds(new ComponentName(PatchService.this.context, BinderWidget.class));
                        if (ids != null) {
                            System.out.println("Binders " + ids.length);
                            if (ids.length > 0) {
                                listAppsFragment.binderWidget = true;
                                PatchService.this.handler.post(new C01322());
                            }
                        }
                    } catch (Exception e222222) {
                        e222222.printStackTrace();
                    }
                    fis.close();
                } catch (FileNotFoundException e9) {
                    System.out.println("Not found bind.txt");
                } catch (IOException e10) {
                    System.out.println(BuildConfig.VERSION_NAME + e10);
                }
            }
            try {
                ids = AppWidgetManager.getInstance(PatchService.this.context).getAppWidgetIds(new ComponentName(PatchService.this.context, AppDisablerWidget.class));
                if (ids != null) {
                    System.out.println("AppDisablers " + ids.length);
                    if (ids.length > 0) {
                        listAppsFragment.appDisabler = true;
                        PatchService.this.handler.post(new C01333());
                    }
                }
            } catch (Exception e2222222) {
                e2222222.printStackTrace();
            }
            listAppsFragment.patchOnBoot = false;
            listAppsFragment.getConfig().edit().putBoolean("trigger_for_good_android_patch_on_boot", false).commit();
            Utils.exit();
            System.out.println("LuckyPatcher OnBootError: " + e2222222);
            e2222222.printStackTrace();
            File bindfile2 = new File(PatchService.this.getDir("binder", 0) + "/bind.txt");
            System.out.println("LuckyPatcher binder start!");
            if (bindfile2.exists()) {
                bindfile2.createNewFile();
            }
            FileInputStream fis2 = new FileInputStream(bindfile2);
            br = new BufferedReader(new InputStreamReader(fis2, "UTF-8"));
            data = BuildConfig.VERSION_NAME;
            String temp2 = BuildConfig.VERSION_NAME;
            out = BuildConfig.VERSION_NAME;
            while (true) {
                data = br.readLine();
                if (data != null) {
                    break;
                }
                tails = data.split(";");
                if (tails.length == 2) {
                    Utils.verify_bind_and_run("mount", "-o bind '" + tails[0] + "' '" + tails[1] + "'", tails[0], tails[1]);
                    if (Utils.checkBind(new BindItem(tails[0], tails[1]))) {
                        System.out.println("LuckyPatcher: show notify");
                        out = out + PatchService.this.getString(C0149R.string.notify_directory_binder_from) + " " + tails[0] + " " + PatchService.this.getString(C0149R.string.notify_directory_binder_to) + " " + tails[1] + LogCollector.LINE_SEPARATOR;
                    }
                }
            }
            PatchService.this.showNotify(PatchService.notifyIndex, "Lucky Patcher - " + PatchService.this.getString(C0149R.string.notify_directory_binder), PatchService.this.getString(C0149R.string.notify_directory_binder), out);
            PatchService.notifyIndex++;
            ids = AppWidgetManager.getInstance(PatchService.this.context).getAppWidgetIds(new ComponentName(PatchService.this.context, BinderWidget.class));
            if (ids != null) {
                System.out.println("Binders " + ids.length);
                if (ids.length > 0) {
                    listAppsFragment.binderWidget = true;
                    PatchService.this.handler.post(new C01322());
                }
            }
            fis2.close();
            ids = AppWidgetManager.getInstance(PatchService.this.context).getAppWidgetIds(new ComponentName(PatchService.this.context, AppDisablerWidget.class));
            if (ids != null) {
                System.out.println("AppDisablers " + ids.length);
                if (ids.length > 0) {
                    listAppsFragment.appDisabler = true;
                    PatchService.this.handler.post(new C01333());
                }
            }
            listAppsFragment.patchOnBoot = false;
            listAppsFragment.getConfig().edit().putBoolean("trigger_for_good_android_patch_on_boot", false).commit();
            Utils.exit();
        }
    }

    public void onCreate() {
        System.out.println("LuckyPatcher: Create service");
        listAppsFragment.patchOnBoot = true;
        listAppsFragment.init();
        this.context = this;
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        listAppsFragment.patchOnBoot = true;
        this.f2t = new Thread(new C01341());
        System.out.println("LuckyPatcher: Start thread patch!");
        this.f2t.setPriority(10);
        this.f2t.start();
        return 2;
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onDestroy() {
        super.onDestroy();
        System.out.println("Killing Service!!!!!!!!!!!!!!!!!!!!!!!");
        Utils.exit();
    }

    private void showNotify(int id, String title, String tickerText, String text) {
        if (!listAppsFragment.getConfig().getBoolean("hide_notify", false)) {
            long when = System.currentTimeMillis();
            Context context2 = getApplicationContext();
            String contentTitle = title;
            String contentText = text;
            PendingIntent contentIntent = PendingIntent.getActivity(this, 0, new Intent(this, patchActivity.class), 0);
            NotificationManager notificationManager = (NotificationManager) getSystemService("notification");
            Notification notification = new Notification(C0149R.drawable.ic_notify, tickerText, when);
            notification.setLatestEventInfo(context2, contentTitle, contentText, contentIntent);
            notificationManager.notify(id, notification);
        }
    }

    public void dexoptcopy() {
        File tempopt = new File(listAppsFragment.toolfilesdir + InternalZipConstants.ZIP_FILE_SEPARATOR + "dexopt-wrapper");
        if (tempopt.exists() && tempopt.length() == Utils.getRawLength(C0149R.raw.dexopt)) {
            Utils.run_all("chmod 777 " + new File(getApplicationContext().getFilesDir() + InternalZipConstants.ZIP_FILE_SEPARATOR + "dexopt-wrapper"));
            Utils.run_all("chown 0.0 " + new File(getApplicationContext().getFilesDir() + InternalZipConstants.ZIP_FILE_SEPARATOR + "dexopt-wrapper"));
            Utils.run_all("chmod 0:0 " + new File(getApplicationContext().getFilesDir() + InternalZipConstants.ZIP_FILE_SEPARATOR + "dexopt-wrapper"));
            return;
        }
        try {
            Utils.getRawToFile(C0149R.raw.dexopt, new File(listAppsFragment.toolfilesdir + "/dexopt-wrapper"));
        } catch (Exception e) {
        }
        try {
            Utils.chmod(new File(getApplicationContext().getFilesDir() + InternalZipConstants.ZIP_FILE_SEPARATOR + "dexopt-wrapper"), 777);
        } catch (Exception e2) {
            System.out.println(e2);
            e2.printStackTrace();
        }
        Utils.run_all("chmod 777 " + new File(getApplicationContext().getFilesDir() + InternalZipConstants.ZIP_FILE_SEPARATOR + "dexopt-wrapper"));
        Utils.run_all("chown 0.0 " + new File(getApplicationContext().getFilesDir() + InternalZipConstants.ZIP_FILE_SEPARATOR + "dexopt-wrapper"));
        Utils.run_all("chmod 0:0 " + new File(getApplicationContext().getFilesDir() + InternalZipConstants.ZIP_FILE_SEPARATOR + "dexopt-wrapper"));
    }
}
