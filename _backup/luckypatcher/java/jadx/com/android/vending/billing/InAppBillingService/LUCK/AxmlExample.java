package com.android.vending.billing.InAppBillingService.LUCK;

import com.chelpus.Common;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import pxb.android.axml.AxmlReader;
import pxb.android.axml.AxmlVisitor;
import pxb.android.axml.AxmlWriter;
import pxb.android.axml.NodeVisitor;

public class AxmlExample {
    public static boolean result = false;
    ArrayList<String> activities = new ArrayList();
    boolean found1 = false;
    boolean found2 = false;
    ArrayList<String> permissions = new ArrayList();

    public void disablePermisson(File manifestFile, ArrayList<String> permiss, ArrayList<String> activ) throws IOException {
        this.permissions = permiss;
        this.activities = activ;
        byte[] data1 = new byte[((int) manifestFile.length())];
        try {
            new FileInputStream(manifestFile).read(data1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        AxmlReader ar = new AxmlReader(data1);
        AxmlWriter aw = new AxmlWriter();
        ar.accept(new AxmlVisitor(aw) {
            public NodeVisitor child(String ns, String name) {
                return new NodeVisitor(super.child(ns, name)) {
                    public NodeVisitor child(String ns, String name) {
                        return name.equals("uses-permission") ? new NodeVisitor(super.child(ns, name)) {
                            public void attr(String ns, String name, int resourceId, int type, Object obj) {
                                if ("http://schemas.android.com/apk/res/android".equals(ns) && name.equals("name")) {
                                    String serviceClass = (String) obj;
                                    System.out.println("name " + name + " " + "obj " + obj);
                                    AxmlExample.this.found1 = false;
                                    Iterator it = AxmlExample.this.permissions.iterator();
                                    Object obj2 = obj;
                                    while (it.hasNext()) {
                                        String perm = (String) it.next();
                                        if (serviceClass.contains(perm)) {
                                            String obj3;
                                            System.out.println("Found " + obj2);
                                            if (serviceClass.startsWith("disabled_")) {
                                                obj3 = serviceClass.replace("disabled_", BuildConfig.VERSION_NAME);
                                            } else {
                                                obj3 = serviceClass.replace(perm, "disabled_" + perm);
                                            }
                                            AxmlExample.this.found1 = true;
                                            super.attr(ns, name, resourceId, type, obj3);
                                            obj2 = obj3;
                                        }
                                    }
                                    if (!AxmlExample.this.found1) {
                                        super.attr(ns, name, resourceId, type, obj2);
                                        return;
                                    }
                                    return;
                                }
                                super.attr(ns, name, resourceId, type, obj);
                            }
                        } : new NodeVisitor(super.child(ns, name)) {
                            public NodeVisitor child(String ns, String name) {
                                return name.equals("activity") ? new NodeVisitor(super.child(ns, name)) {
                                    public void attr(String ns, String name, int resourceId, int type, Object obj) {
                                        if ("http://schemas.android.com/apk/res/android".equals(ns) && name.equals("name")) {
                                            String serviceClass = (String) obj;
                                            System.out.println("name " + name + " " + "obj " + obj);
                                            AxmlExample.this.found2 = false;
                                            Iterator it = AxmlExample.this.activities.iterator();
                                            Object obj2 = obj;
                                            while (it.hasNext()) {
                                                if (((String) it.next()).replace("chelpus_", BuildConfig.VERSION_NAME).contains(serviceClass)) {
                                                    String obj3;
                                                    System.out.println("Found " + obj2);
                                                    if (serviceClass.replace("chelpus_", BuildConfig.VERSION_NAME).startsWith("disabled_")) {
                                                        obj3 = serviceClass.replace("disabled_", BuildConfig.VERSION_NAME);
                                                    } else {
                                                        obj3 = serviceClass.replace(serviceClass, "disabled_" + serviceClass);
                                                    }
                                                    AxmlExample.this.found2 = true;
                                                    super.attr(ns, name, resourceId, type, obj3);
                                                    obj2 = obj3;
                                                }
                                            }
                                            super.attr(ns, name, resourceId, type, obj2);
                                            return;
                                        }
                                        super.attr(ns, name, resourceId, type, obj);
                                    }
                                } : super.child(ns, name);
                            }

                            public void end() {
                                System.out.println("End");
                                super.end();
                            }
                        };
                    }
                };
            }
        });
        byte[] data = aw.toByteArray();
        try {
            manifestFile.delete();
            manifestFile.createNewFile();
            FileOutputStream fo = new FileOutputStream(manifestFile);
            fo.write(data);
            fo.close();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    public boolean changeTargetApi(File manifestFile, String targetApi) throws IOException {
        result = false;
        byte[] data1 = new byte[((int) manifestFile.length())];
        try {
            new FileInputStream(manifestFile).read(data1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        AxmlReader ar = new AxmlReader(data1);
        AxmlWriter aw = new AxmlWriter();
        ar.accept(new AxmlVisitor(aw) {
            public NodeVisitor child(String ns, String name) {
                return new NodeVisitor(super.child(ns, name)) {
                    public NodeVisitor child(String ns, String name) {
                        return name.equals("uses-sdk") ? new NodeVisitor(super.child(ns, name)) {
                            public void attr(String ns, String name, int resourceId, int type, Object obj) {
                                System.out.println(ns + " " + name + " " + resourceId + " " + type + " " + obj);
                                if ("http://schemas.android.com/apk/res/android".equals(ns) && name.equals("targetSdkVersion")) {
                                    Integer androidVersion = (Integer) obj;
                                    System.out.println(name + ":" + obj + " " + androidVersion);
                                    AxmlExample.this.found1 = false;
                                    if (androidVersion.intValue() >= 21) {
                                        System.out.println("Found " + obj);
                                        super.attr(ns, name, resourceId, type, Integer.valueOf(20));
                                        AxmlExample.result = true;
                                        return;
                                    }
                                    return;
                                }
                                super.attr(ns, name, resourceId, type, obj);
                            }
                        } : new NodeVisitor(super.child(ns, name)) {
                            public NodeVisitor child(String ns, String name) {
                                return super.child(ns, name);
                            }

                            public void end() {
                                super.end();
                            }
                        };
                    }
                };
            }
        });
        byte[] data = aw.toByteArray();
        try {
            manifestFile.delete();
            manifestFile.createNewFile();
            FileOutputStream fo = new FileOutputStream(manifestFile);
            fo.write(data);
            fo.close();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        return result;
    }

    public boolean changePackageName(File manifestFile, final String oldPackageName, final String newPackageName) throws IOException {
        result = false;
        byte[] data1 = new byte[((int) manifestFile.length())];
        try {
            new FileInputStream(manifestFile).read(data1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        AxmlReader ar = new AxmlReader(data1);
        AxmlWriter aw = new AxmlWriter();
        ar.accept(new AxmlVisitor(aw) {
            public NodeVisitor child(String ns, String name) {
                return new NodeVisitor(super.child(ns, name)) {
                    public void attr(String ns, String name, int resourceId, int type, Object obj) {
                        System.out.println(ns + " " + name + " " + resourceId + " " + type + " " + obj);
                        if (name.equals(Common.PACKAGE)) {
                            String o = obj.toString().replaceAll(oldPackageName, newPackageName);
                            System.out.println("Replace package name to:" + o);
                            super.attr(ns, name, resourceId, type, o);
                            AxmlExample.result = true;
                            return;
                        }
                        super.attr(ns, name, resourceId, type, obj);
                    }

                    public NodeVisitor child(String ns, String name) {
                        System.out.println(ns + " " + name);
                        return new NodeVisitor(super.child(ns, name)) {
                            public void attr(String ns, String name, int resourceId, int type, Object obj) {
                                System.out.println(ns + " " + name + " " + resourceId + " " + type + " " + obj);
                                if (type == 3) {
                                    String o;
                                    boolean trig = false;
                                    if (obj.toString().contains("permission.C2D_MESSAGE") && obj.toString().startsWith(oldPackageName)) {
                                        o = obj.toString().replaceAll(oldPackageName, newPackageName);
                                        System.out.println("Replace package name to:" + o);
                                        super.attr(ns, name, resourceId, type, o);
                                        AxmlExample.result = true;
                                        trig = true;
                                    }
                                    if (obj.toString().startsWith(".")) {
                                        o = oldPackageName + obj.toString();
                                        System.out.println("Replace package name to:" + o);
                                        super.attr(ns, name, resourceId, type, o);
                                        AxmlExample.result = true;
                                        trig = true;
                                    }
                                    if (!trig) {
                                        super.attr(ns, name, resourceId, type, obj);
                                        return;
                                    }
                                    return;
                                }
                                super.attr(ns, name, resourceId, type, obj);
                            }

                            public NodeVisitor child(String ns, String name) {
                                System.out.println(ns + " " + name);
                                return new NodeVisitor(super.child(ns, name)) {
                                    public void attr(String ns, String name, int resourceId, int type, Object obj) {
                                        System.out.println(ns + " " + name + " " + resourceId + " " + type + " " + obj);
                                        if (type == 3 && obj.toString().startsWith(".")) {
                                            String o = oldPackageName + obj.toString();
                                            System.out.println("Replace package name to:" + o);
                                            super.attr(ns, name, resourceId, type, o);
                                            AxmlExample.result = true;
                                            return;
                                        }
                                        super.attr(ns, name, resourceId, type, obj);
                                    }

                                    public NodeVisitor child(String ns, String name) {
                                        System.out.println(ns + " " + name);
                                        return new NodeVisitor(super.child(ns, name)) {
                                            public void attr(String ns, String name, int resourceId, int type, Object obj) {
                                                System.out.println(ns + " " + name + " " + resourceId + " " + type + " " + obj);
                                                if (type == 3 && obj.toString().startsWith(".")) {
                                                    String o = oldPackageName + obj.toString();
                                                    System.out.println("Replace package name to:" + o);
                                                    super.attr(ns, name, resourceId, type, o);
                                                    AxmlExample.result = true;
                                                    return;
                                                }
                                                super.attr(ns, name, resourceId, type, obj);
                                            }
                                        };
                                    }
                                };
                            }
                        };
                    }
                };
            }
        });
        byte[] data = aw.toByteArray();
        try {
            manifestFile.delete();
            manifestFile.createNewFile();
            FileOutputStream fo = new FileOutputStream(manifestFile);
            fo.write(data);
            fo.close();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        return result;
    }
}
