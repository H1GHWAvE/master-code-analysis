package com.android.vending.billing.InAppBillingService.LUCK;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.Filter.FilterResults;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.chelpus.Common;
import com.chelpus.Utils;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import pxb.android.axml.AxmlParser;

public class PkgListItemAdapter extends BaseExpandableListAdapter {
    public static final int TEXT_DEFAULT = 0;
    public static final int TEXT_LARGE = 2;
    public static final int TEXT_MEDIUM = 1;
    public static final int TEXT_SMALL = 0;
    boolean Ready = false;
    ArrayList<PkgListItem> add;
    public int[] childMenu = null;
    public int[] childMenuNoRoot = null;
    public int[] childMenuSystem = null;
    private Context context;
    public PkgListItem[] data;
    ArrayList<PkgListItem> del;
    private Drawable disabled = listAppsFragment.getRes().getDrawable(C0149R.drawable.context_lvl);
    private ImageView imgIcon;
    private int layoutResourceId;
    private TextView on_top_txt;
    public PkgListItem[] orig;
    private int size;
    public Comparator<PkgListItem> sorter;
    private TextView txtStatus;
    private TextView txtTitle;

    class C01351 implements OnClickListener {
        C01351() {
        }

        public void onClick(View v) {
            PkgListItem it = PkgListItemAdapter.this.getGroup(((Integer) v.getTag()).intValue());
            if (it.selected) {
                PkgListItemAdapter.this.getGroup(((Integer) v.getTag()).intValue()).selected = false;
                System.out.println(it.selected);
                listAppsFragment.selectedApps.remove(it);
                if (listAppsFragment.selectedApps.size() == 0) {
                    ((Button) listAppsFragment.patchAct.findViewById(C0149R.id.title_button)).setText(Utils.getText(C0149R.string.cancel));
                }
                System.out.println(listAppsFragment.selectedApps.size());
            } else {
                PkgListItemAdapter.this.getGroup(((Integer) v.getTag()).intValue()).selected = true;
                System.out.println(it.selected);
                if (listAppsFragment.selectedApps.size() == 0) {
                    ((Button) listAppsFragment.patchAct.findViewById(C0149R.id.title_button)).setText(listAppsFragment.adapterSelectType);
                }
                listAppsFragment.selectedApps.add(it);
            }
            PkgListItemAdapter.this.notifyDataSetChanged();
        }
    }

    class C01362 extends Filter {
        C01362() {
        }

        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults oReturn = new FilterResults();
            ArrayList<PkgListItem> results = new ArrayList();
            if (PkgListItemAdapter.this.orig == null) {
                PkgListItemAdapter.this.orig = PkgListItemAdapter.this.data;
            }
            if (constraint != null) {
                if (PkgListItemAdapter.this.orig != null && PkgListItemAdapter.this.orig.length > 0) {
                    PkgListItem[] pkgListItemArr = PkgListItemAdapter.this.orig;
                    int length = pkgListItemArr.length;
                    for (int i = PkgListItemAdapter.TEXT_DEFAULT; i < length; i += PkgListItemAdapter.TEXT_MEDIUM) {
                        PkgListItem g = pkgListItemArr[i];
                        if (g.name.toLowerCase().contains(constraint.toString().toLowerCase()) || g.pkgName.toLowerCase().contains(constraint.toString().toLowerCase())) {
                            results.add(g);
                        }
                    }
                }
                PkgListItem[] tmp = new PkgListItem[results.size()];
                results.toArray(tmp);
                oReturn.values = tmp;
            }
            return oReturn;
        }

        protected void publishResults(CharSequence constraint, FilterResults results) {
            PkgListItemAdapter.this.data = (PkgListItem[]) results.values;
            PkgListItemAdapter.this.notifyDataSetChanged();
        }
    }

    class Refresh_Packages implements Runnable {

        class C01371 implements Runnable {
            C01371() {
            }

            public void run() {
                listAppsFragment.refresh = false;
                try {
                    PkgListItem pkg;
                    Iterator it = PkgListItemAdapter.this.add.iterator();
                    while (it.hasNext()) {
                        pkg = (PkgListItem) it.next();
                        if (PkgListItemAdapter.this.getItem(pkg.pkgName) == null) {
                            PkgListItemAdapter.this.add(pkg);
                            PkgListItemAdapter.this.notifyDataSetChanged();
                            PkgListItemAdapter.this.sort();
                            PkgListItemAdapter.this.notifyDataSetChanged();
                        }
                    }
                    it = PkgListItemAdapter.this.del.iterator();
                    while (it.hasNext()) {
                        pkg = (PkgListItem) it.next();
                        if (PkgListItemAdapter.this.getItem(pkg.pkgName) != null) {
                            PkgListItemAdapter.this.remove(pkg.pkgName);
                            PkgListItemAdapter.this.notifyDataSetChanged();
                            PkgListItemAdapter.this.sort();
                            PkgListItemAdapter.this.notifyDataSetChanged();
                        }
                    }
                } catch (Exception e) {
                    System.out.println("LuckyPatcher (AddApps): on progress adapter not found!");
                    e.printStackTrace();
                }
            }
        }

        class C01382 implements Runnable {
            C01382() {
            }

            public void run() {
                try {
                    PkgListItemAdapter.this.sort();
                    PkgListItemAdapter.this.notifyDataSetChanged();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                System.out.println("LuckyPatcher: refresh Packages finished.");
            }
        }

        class C01393 implements Runnable {
            C01393() {
            }

            public void run() {
                try {
                    PkgListItemAdapter.this.sort();
                    PkgListItemAdapter.this.notifyDataSetChanged();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                System.out.println("LuckyPatcher: refresh Packages finished.");
            }
        }

        Refresh_Packages() {
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            int i;
            int i2;
            PkgListItem item;
            listAppsFragment.refresh = false;
            PkgListItemAdapter.this.add = new ArrayList();
            PkgListItemAdapter.this.del = new ArrayList();
            ArrayList<PkgListItem> patAll = new ArrayList();
            String[] packages = listAppsFragment.getPackages();
            if (listAppsFragment.database == null) {
                listAppsFragment.database = new DatabaseHelper(listAppsFragment.getInstance());
            }
            patAll = listAppsFragment.database.getPackage(true, true);
            boolean sysshow = listAppsFragment.getConfig().getBoolean("systemapp", false);
            PkgListItemAdapter.this.add.clear();
            String[] patArray = new String[patAll.size()];
            int k = PkgListItemAdapter.TEXT_DEFAULT;
            Iterator it = patAll.iterator();
            while (it.hasNext()) {
                patArray[k] = ((PkgListItem) it.next()).pkgName;
                k += PkgListItemAdapter.TEXT_MEDIUM;
            }
            int length = packages.length;
            for (i = PkgListItemAdapter.TEXT_DEFAULT; i < length; i += PkgListItemAdapter.TEXT_MEDIUM) {
                int length2;
                String pkgname = packages[i];
                boolean found = false;
                try {
                    length2 = patArray.length;
                    for (i2 = PkgListItemAdapter.TEXT_DEFAULT; i2 < length2; i2 += PkgListItemAdapter.TEXT_MEDIUM) {
                        if (patArray[i2].trim().equals(pkgname)) {
                            found = true;
                        }
                        if (pkgname.equals(Common.ANDROID_PKG) || pkgname.equals(PkgListItemAdapter.this.context.getPackageName())) {
                            found = true;
                        }
                    }
                    if (!found) {
                        PkgListItem local = new PkgListItem(listAppsFragment.getInstance(), pkgname, listAppsFragment.days, true);
                        local.saveItem();
                        PkgListItemAdapter.this.add.add(local);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("LuckyPatcher (refreshAddApplication): Error " + e);
                } catch (IllegalArgumentException e2) {
                    System.out.println("LuckyPatcher (refreshAddApps1): " + e2);
                }
            }
            long t = System.currentTimeMillis();
            try {
                PkgListItem[] pkgListItemArr = PkgListItemAdapter.this.data;
                length2 = pkgListItemArr.length;
                for (i = PkgListItemAdapter.TEXT_DEFAULT; i < length2; i += PkgListItemAdapter.TEXT_MEDIUM) {
                    item = pkgListItemArr[i];
                    boolean marker = false;
                    int length3 = packages.length;
                    for (i2 = PkgListItemAdapter.TEXT_DEFAULT; i2 < length3; i2 += PkgListItemAdapter.TEXT_MEDIUM) {
                        if (item.pkgName.equals(packages[i2])) {
                            marker = true;
                        }
                    }
                    if (!marker) {
                        PkgListItemAdapter.this.del.add(item);
                        System.out.println("delete item!");
                    }
                }
                if (PkgListItemAdapter.this.add != null || PkgListItemAdapter.this.add.size() > 0 || PkgListItemAdapter.this.del != null || PkgListItemAdapter.this.del.size() > 0) {
                    ((Activity) PkgListItemAdapter.this.context).runOnUiThread(new C01371());
                }
            } catch (Exception e3) {
                System.out.println("LuckyPatcher (refreshApps2): Error Package Scan! " + e3);
                e3.printStackTrace();
            }
            boolean concurent_test = true;
            while (concurent_test) {
                try {
                    PkgListItem[] pkgListItemArr2 = PkgListItemAdapter.this.data;
                    length = pkgListItemArr2.length;
                    for (i2 = PkgListItemAdapter.TEXT_DEFAULT; i2 < length; i2 += PkgListItemAdapter.TEXT_MEDIUM) {
                        item = pkgListItemArr2[i2];
                        local = new PkgListItem(listAppsFragment.getInstance(), item.pkgName, listAppsFragment.days, false);
                        PkgListItem tmp = PkgListItemAdapter.this.getItem(item.pkgName);
                        if (!tmp.equalsPli(local)) {
                            tmp.pkgName = local.pkgName;
                            tmp.name = local.name;
                            tmp.storepref = local.storepref;
                            tmp.stored = local.stored;
                            tmp.hidden = local.hidden;
                            tmp.statusi = local.statusi;
                            tmp.boot_ads = local.boot_ads;
                            tmp.boot_lvl = local.boot_lvl;
                            tmp.boot_custom = local.boot_custom;
                            tmp.boot_manual = local.boot_manual;
                            tmp.custom = local.custom;
                            tmp.lvl = local.lvl;
                            tmp.ads = local.ads;
                            tmp.modified = local.modified;
                            tmp.system = local.system;
                            tmp.odex = local.odex;
                            tmp.billing = local.billing;
                            tmp.updatetime = local.updatetime;
                            local.saveItem();
                        }
                    }
                } catch (ConcurrentModificationException e4) {
                    concurent_test = true;
                    System.out.println("LuckyPatcher:retry refresh app (concurent).");
                } catch (Exception e32) {
                    System.out.println("LuckyPatcher (refreshApps3): Error Package Scan! " + e32);
                    e32.printStackTrace();
                }
                concurent_test = false;
            }
            System.out.println(System.currentTimeMillis() - t);
            listAppsFragment.addapps = false;
            ((Activity) PkgListItemAdapter.this.context).runOnUiThread(new C01393());
        }
    }

    public PkgListItemAdapter(Context context, int layout, List<PkgListItem> p) {
        this.context = context;
        this.layoutResourceId = layout;
        this.size = TEXT_DEFAULT;
        this.data = (PkgListItem[]) p.toArray(new PkgListItem[p.size()]);
        listAppsFragment.plia = this;
        Drawable iconka = this.disabled;
        iconka.setColorFilter(Color.parseColor("#fe6c00"), Mode.MULTIPLY);
        this.disabled = iconka;
    }

    public PkgListItemAdapter(Context context, int layout, int s, List<PkgListItem> p) {
        this.context = context;
        this.layoutResourceId = layout;
        this.size = s;
        this.data = (PkgListItem[]) p.toArray(new PkgListItem[p.size()]);
        listAppsFragment.plia = this;
        Drawable iconka = this.disabled;
        iconka.setColorFilter(Color.parseColor("#fe6c00"), Mode.MULTIPLY);
        this.disabled = iconka;
    }

    public Integer getChild(int arg0, int arg1) {
        return Integer.valueOf(this.childMenu[arg1]);
    }

    public long getChildId(int groupPosition, int childPosition) {
        return (long) ((groupPosition * 10) + childPosition);
    }

    public int getChildrenCount(int groupPosition) {
        if (getGroup(groupPosition) != null) {
            String directory = null;
            try {
                directory = listAppsFragment.getPkgMng().getPackageInfo(getGroup(groupPosition).pkgName, TEXT_DEFAULT).applicationInfo.sourceDir;
            } catch (NameNotFoundException e) {
                try {
                    e.printStackTrace();
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
            if (!(!listAppsFragment.su || directory == null || directory.startsWith("/system/"))) {
                this.childMenu = new int[8];
                this.childMenu[TEXT_DEFAULT] = C0149R.string.app_info;
                this.childMenu[TEXT_MEDIUM] = C0149R.string.contextlaunchapp;
                this.childMenu[TEXT_LARGE] = C0149R.string.advanced_menu;
                this.childMenu[3] = C0149R.string.context_tools;
                this.childMenu[4] = C0149R.string.uninstallapp;
                this.childMenu[5] = C0149R.string.cleardata;
                if (directory.startsWith("/mnt/asec/")) {
                    this.childMenu[6] = C0149R.string.move_to_internal;
                } else {
                    this.childMenu[6] = C0149R.string.move_to_sdcard;
                }
                this.childMenu[7] = C0149R.string.app_dialog_no_root_app_control;
            }
            if (listAppsFragment.su && directory != null && directory.startsWith("/system/")) {
                this.childMenu = new int[7];
                this.childMenu[TEXT_DEFAULT] = C0149R.string.app_info;
                this.childMenu[TEXT_MEDIUM] = C0149R.string.contextlaunchapp;
                this.childMenu[TEXT_LARGE] = C0149R.string.advanced_menu;
                this.childMenu[3] = C0149R.string.context_tools;
                this.childMenu[4] = C0149R.string.uninstallapp;
                this.childMenu[5] = C0149R.string.cleardata;
                this.childMenu[6] = C0149R.string.app_dialog_no_root_app_control;
            }
            if (!listAppsFragment.su) {
                this.childMenu = new int[6];
                this.childMenu[TEXT_DEFAULT] = C0149R.string.app_info;
                this.childMenu[TEXT_MEDIUM] = C0149R.string.contextlaunchapp;
                this.childMenu[TEXT_LARGE] = C0149R.string.advanced_menu;
                this.childMenu[3] = C0149R.string.context_tools;
                this.childMenu[4] = C0149R.string.uninstallapp;
                this.childMenu[5] = C0149R.string.app_dialog_no_root_app_control;
            }
            if (directory == null) {
                this.childMenu = new int[TEXT_MEDIUM];
                this.childMenu[TEXT_DEFAULT] = C0149R.string.app_not_found_adapter;
            }
        }
        if (this.childMenu == null) {
            return TEXT_DEFAULT;
        }
        return this.childMenu.length;
    }

    public PkgListItem getItem(int groupPosition) {
        return this.data[groupPosition];
    }

    public int getGroupCount() {
        return this.data.length;
    }

    public long getGroupId(int groupPosition) {
        return (long) groupPosition;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        try {
            PkgListItem p = getGroup(groupPosition);
            if (p == null || p.hidden) {
                return new View(this.context);
            }
            String statusi;
            String lng;
            Locale locale;
            String[] tails;
            Configuration appConfig2;
            Locale appLoc3;
            Configuration appConfig3;
            int i;
            int i2;
            int i3;
            File apk;
            TextView textView;
            StringBuilder append;
            Float[] fArr;
            View row = convertView;
            if (row == null) {
                row = ((LayoutInflater) this.context.getSystemService("layout_inflater")).inflate(this.layoutResourceId, parent, false);
            }
            this.txtTitle = (TextView) row.findViewById(C0149R.id.txtTitle);
            this.txtStatus = (TextView) row.findViewById(C0149R.id.txtStatus);
            this.imgIcon = (ImageView) row.findViewById(C0149R.id.imgIcon);
            this.on_top_txt = (TextView) row.findViewById(C0149R.id.move_top);
            CheckBox chk = (CheckBox) row.findViewById(C0149R.id.CheckBox1);
            CheckBox chk2 = (CheckBox) row.findViewById(C0149R.id.CheckBox2);
            TextView chk3 = (TextView) row.findViewById(C0149R.id.partition);
            CheckBox select = (CheckBox) row.findViewById(C0149R.id.CheckBoxSelect);
            if (listAppsFragment.adapterSelect) {
                select.setVisibility(TEXT_DEFAULT);
                chk.setVisibility(8);
                chk2.setVisibility(8);
                chk3.setVisibility(8);
                this.on_top_txt.setVisibility(8);
                select.setTag(Integer.valueOf(groupPosition));
                select.setOnClickListener(new C01351());
            } else if (select.getVisibility() == 0) {
                select.setVisibility(8);
                chk.setVisibility(TEXT_DEFAULT);
                chk2.setVisibility(TEXT_DEFAULT);
                chk3.setVisibility(TEXT_DEFAULT);
                this.on_top_txt.setVisibility(TEXT_DEFAULT);
            }
            if (p.selected) {
                select.setChecked(true);
            } else {
                select.setChecked(false);
            }
            chk2.setChecked(p.modified);
            chk2.setClickable(false);
            if (p.system) {
                if (listAppsFragment.getPkgMng().getPackageInfo(p.pkgName, TEXT_DEFAULT).applicationInfo.sourceDir.startsWith("/system")) {
                    chk3.setText("SYS");
                    chk3.setTextColor(-65281);
                } else {
                    chk3.setText("INT");
                    chk3.setTextColor(-16711936);
                }
            } else if (p.on_sd) {
                chk3.setText("SD");
                chk3.setTextColor(-256);
            } else {
                chk3.setText("INT");
                chk3.setTextColor(-16711936);
            }
            chk3.setClickable(false);
            chk.setChecked(p.odex);
            chk.setClickable(false);
            this.txtTitle.setText(p.name);
            try {
                if (!listAppsFragment.getConfig().getBoolean("no_icon", false)) {
                    if (listAppsFragment.goodMemory) {
                        this.imgIcon.setImageDrawable(null);
                        if (p.icon == null) {
                            int imgSize = (int) ((35.0f * listAppsFragment.getRes().getDisplayMetrics().density) + 0.5f);
                            Bitmap iconka = null;
                            try {
                                iconka = ((BitmapDrawable) listAppsFragment.getPkgMng().getApplicationIcon(p.pkgName)).getBitmap();
                            } catch (NameNotFoundException e) {
                                e.printStackTrace();
                            }
                            int width = iconka.getWidth();
                            int height = iconka.getHeight();
                            float scaleWidth = ((float) imgSize) / ((float) width);
                            float scaleHeight = ((float) imgSize) / ((float) height);
                            Matrix bMatrix = new Matrix();
                            bMatrix.postScale(scaleWidth, scaleHeight);
                            p.icon = new BitmapDrawable(Bitmap.createBitmap(iconka, TEXT_DEFAULT, TEXT_DEFAULT, width, height, bMatrix, true));
                            p.saveItem();
                        }
                        if (p.enable) {
                            this.imgIcon.setImageDrawable(p.icon);
                        } else {
                            this.imgIcon.setImageDrawable(this.disabled);
                        }
                    } else {
                        Toast.makeText(this.context, "Out of memory! Icon not loaded!", TEXT_DEFAULT).show();
                    }
                }
            } catch (OutOfMemoryError E) {
                E.printStackTrace();
                System.gc();
                listAppsFragment.getConfig().edit().putBoolean("no_icon", true).commit();
            } catch (Exception e2) {
                e2.printStackTrace();
                remove(p.pkgName);
                notifyDataSetChanged();
                System.out.println("LuckyPatcher(PackageListItemAdapter): " + e2);
            }
            this.txtTitle.setTextAppearance(this.context, listAppsFragment.getSizeText());
            this.txtStatus.setTextAppearance(this.context, listAppsFragment.getSizeText());
            this.txtStatus.setTextAppearance(this.context, 16973894);
            String color = "#ffcc7943";
            switch (p.stored) {
                case TEXT_MEDIUM /*1*/:
                    color = "#fff0e442";
                case TEXT_LARGE /*2*/:
                    color = "#ff00ff73";
                    if (!(p.stored != 0 || p.ads || p.lvl)) {
                        color = "#ffff0055";
                    }
                    if (p.stored == 0 && p.ads) {
                        color = "#ff00ffff";
                    }
                    if (p.stored == 0 && p.lvl) {
                        color = "#ff00ff73";
                    }
                    this.txtTitle.setTextColor(Color.parseColor(color));
                    this.txtStatus.setTextColor(-7829368);
                    statusi = BuildConfig.VERSION_NAME;
                    lng = listAppsFragment.getConfig().getString("force_language", "default");
                    if (lng.equals("default")) {
                        locale = null;
                        tails = lng.split("_");
                        if (tails.length == TEXT_MEDIUM) {
                            locale = new Locale(tails[TEXT_DEFAULT]);
                        }
                        if (tails.length == TEXT_LARGE) {
                            locale = new Locale(tails[TEXT_DEFAULT], tails[TEXT_MEDIUM], BuildConfig.VERSION_NAME);
                        }
                        if (tails.length == 3) {
                            locale = new Locale(tails[TEXT_DEFAULT], tails[TEXT_MEDIUM], tails[TEXT_LARGE]);
                        }
                        Locale.setDefault(locale);
                        appConfig2 = new Configuration();
                        appConfig2.locale = locale;
                        try {
                            listAppsFragment.getRes().updateConfiguration(appConfig2, listAppsFragment.getRes().getDisplayMetrics());
                        } catch (Exception e22) {
                            e22.printStackTrace();
                        }
                    } else {
                        appLoc3 = Resources.getSystem().getConfiguration().locale;
                        Locale.setDefault(Locale.getDefault());
                        appConfig3 = new Configuration();
                        appConfig3.locale = appLoc3;
                        try {
                            listAppsFragment.getRes().updateConfiguration(appConfig3, listAppsFragment.getRes().getDisplayMetrics());
                        } catch (Exception e222) {
                            e222.printStackTrace();
                        }
                    }
                    if (p.ads) {
                        statusi = Utils.getText(C0149R.string.statads);
                    }
                    if (p.lvl) {
                        if (!(p.ads || p.billing)) {
                            statusi = Utils.getText(C0149R.string.statnotfound);
                        }
                    } else if (p.ads) {
                        statusi = Utils.getText(C0149R.string.statlvl);
                    } else {
                        statusi = statusi + LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.statlvl);
                    }
                    if (p.custom) {
                        statusi = Utils.getText(C0149R.string.statcustom);
                    }
                    if (p.custom) {
                        this.txtTitle.setTextColor(-256);
                    }
                    for (i = TEXT_DEFAULT; i < listAppsFragment.bootlist.length; i += TEXT_MEDIUM) {
                        if (!p.boot_ads || p.boot_custom || p.boot_lvl || p.boot_manual) {
                            this.txtTitle.setTextColor(-65281);
                        }
                    }
                    if (p.billing) {
                        if (statusi.equals(BuildConfig.VERSION_NAME)) {
                            statusi = statusi + LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.statbilling);
                        } else {
                            statusi = Utils.getText(C0149R.string.statbilling);
                        }
                        i2 = p.lvl ? TEXT_MEDIUM : TEXT_DEFAULT;
                        if (p.custom) {
                            i3 = TEXT_MEDIUM;
                        } else {
                            i3 = TEXT_DEFAULT;
                        }
                        if ((i3 & i2) != 0) {
                            this.txtTitle.setTextColor(Color.parseColor("#c5b5ff"));
                        }
                    }
                    this.txtStatus.setText(statusi);
                    if (p.system) {
                        this.txtTitle.setTextColor(-162281);
                    }
                    this.txtTitle.setTypeface(this.txtTitle.getTypeface(), TEXT_DEFAULT);
                    if (!p.enable) {
                        this.txtTitle.setTextColor(-7829368);
                        this.txtStatus.setTextColor(-7829368);
                    }
                    if (p.stored != 0) {
                        this.on_top_txt.setVisibility(TEXT_DEFAULT);
                        this.on_top_txt.setText(Utils.getText(C0149R.string.on_top_apps_marker));
                    } else {
                        this.on_top_txt.setVisibility(4);
                    }
                    if (!listAppsFragment.adapterSelect) {
                        return row;
                    }
                    this.on_top_txt.setVisibility(8);
                    if (listAppsFragment.adapterSelectType == C0149R.string.button_move_to_sdcard && listAppsFragment.adapterSelectType != C0149R.string.button_uninstall_apps) {
                        return row;
                    }
                    this.on_top_txt.setVisibility(8);
                    apk = new File(listAppsFragment.getPkgMng().getPackageInfo(p.pkgName, TEXT_DEFAULT).applicationInfo.sourceDir);
                    textView = this.txtStatus;
                    append = new StringBuilder().append(Utils.getText(C0149R.string.apk_size)).append(" ");
                    fArr = new Object[TEXT_MEDIUM];
                    fArr[TEXT_DEFAULT] = Float.valueOf(((float) apk.length()) / 1048576.0f);
                    textView.setText(append.append(String.format("%.3f", fArr)).append(" Mb").toString());
                    return row;
                case AxmlParser.START_NS /*4*/:
                    color = "#ff00ffff";
                    color = "#ffff0055";
                    color = "#ff00ffff";
                    color = "#ff00ff73";
                    this.txtTitle.setTextColor(Color.parseColor(color));
                    this.txtStatus.setTextColor(-7829368);
                    statusi = BuildConfig.VERSION_NAME;
                    lng = listAppsFragment.getConfig().getString("force_language", "default");
                    if (lng.equals("default")) {
                        locale = null;
                        tails = lng.split("_");
                        if (tails.length == TEXT_MEDIUM) {
                            locale = new Locale(tails[TEXT_DEFAULT]);
                        }
                        if (tails.length == TEXT_LARGE) {
                            locale = new Locale(tails[TEXT_DEFAULT], tails[TEXT_MEDIUM], BuildConfig.VERSION_NAME);
                        }
                        if (tails.length == 3) {
                            locale = new Locale(tails[TEXT_DEFAULT], tails[TEXT_MEDIUM], tails[TEXT_LARGE]);
                        }
                        Locale.setDefault(locale);
                        appConfig2 = new Configuration();
                        appConfig2.locale = locale;
                        listAppsFragment.getRes().updateConfiguration(appConfig2, listAppsFragment.getRes().getDisplayMetrics());
                    } else {
                        appLoc3 = Resources.getSystem().getConfiguration().locale;
                        Locale.setDefault(Locale.getDefault());
                        appConfig3 = new Configuration();
                        appConfig3.locale = appLoc3;
                        listAppsFragment.getRes().updateConfiguration(appConfig3, listAppsFragment.getRes().getDisplayMetrics());
                    }
                    if (p.ads) {
                        statusi = Utils.getText(C0149R.string.statads);
                    }
                    if (p.lvl) {
                        statusi = Utils.getText(C0149R.string.statnotfound);
                        break;
                    } else if (p.ads) {
                        statusi = statusi + LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.statlvl);
                    } else {
                        statusi = Utils.getText(C0149R.string.statlvl);
                    }
                    if (p.custom) {
                        statusi = Utils.getText(C0149R.string.statcustom);
                    }
                    if (p.custom) {
                        this.txtTitle.setTextColor(-256);
                    }
                    for (i = TEXT_DEFAULT; i < listAppsFragment.bootlist.length; i += TEXT_MEDIUM) {
                        if (p.boot_ads) {
                            break;
                        }
                        this.txtTitle.setTextColor(-65281);
                    }
                    if (p.billing) {
                        if (statusi.equals(BuildConfig.VERSION_NAME)) {
                            statusi = statusi + LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.statbilling);
                        } else {
                            statusi = Utils.getText(C0149R.string.statbilling);
                        }
                        if (p.lvl) {
                        }
                        if (p.custom) {
                            i3 = TEXT_DEFAULT;
                        } else {
                            i3 = TEXT_MEDIUM;
                        }
                        if ((i3 & i2) != 0) {
                            this.txtTitle.setTextColor(Color.parseColor("#c5b5ff"));
                        }
                    }
                    this.txtStatus.setText(statusi);
                    if (p.system) {
                        this.txtTitle.setTextColor(-162281);
                    }
                    this.txtTitle.setTypeface(this.txtTitle.getTypeface(), TEXT_DEFAULT);
                    if (p.enable) {
                        this.txtTitle.setTextColor(-7829368);
                        this.txtStatus.setTextColor(-7829368);
                    }
                    if (p.stored != 0) {
                        this.on_top_txt.setVisibility(4);
                    } else {
                        this.on_top_txt.setVisibility(TEXT_DEFAULT);
                        this.on_top_txt.setText(Utils.getText(C0149R.string.on_top_apps_marker));
                    }
                    if (!listAppsFragment.adapterSelect) {
                        return row;
                    }
                    this.on_top_txt.setVisibility(8);
                    if (listAppsFragment.adapterSelectType == C0149R.string.button_move_to_sdcard) {
                        break;
                    }
                    this.on_top_txt.setVisibility(8);
                    apk = new File(listAppsFragment.getPkgMng().getPackageInfo(p.pkgName, TEXT_DEFAULT).applicationInfo.sourceDir);
                    textView = this.txtStatus;
                    append = new StringBuilder().append(Utils.getText(C0149R.string.apk_size)).append(" ");
                    fArr = new Object[TEXT_MEDIUM];
                    fArr[TEXT_DEFAULT] = Float.valueOf(((float) apk.length()) / 1048576.0f);
                    textView.setText(append.append(String.format("%.3f", fArr)).append(" Mb").toString());
                    return row;
                case AxmlParser.TEXT /*6*/:
                    color = "#ffff0055";
                    color = "#ffff0055";
                    color = "#ff00ffff";
                    color = "#ff00ff73";
                    this.txtTitle.setTextColor(Color.parseColor(color));
                    this.txtStatus.setTextColor(-7829368);
                    statusi = BuildConfig.VERSION_NAME;
                    lng = listAppsFragment.getConfig().getString("force_language", "default");
                    if (lng.equals("default")) {
                        appLoc3 = Resources.getSystem().getConfiguration().locale;
                        Locale.setDefault(Locale.getDefault());
                        appConfig3 = new Configuration();
                        appConfig3.locale = appLoc3;
                        listAppsFragment.getRes().updateConfiguration(appConfig3, listAppsFragment.getRes().getDisplayMetrics());
                    } else {
                        locale = null;
                        tails = lng.split("_");
                        if (tails.length == TEXT_MEDIUM) {
                            locale = new Locale(tails[TEXT_DEFAULT]);
                        }
                        if (tails.length == TEXT_LARGE) {
                            locale = new Locale(tails[TEXT_DEFAULT], tails[TEXT_MEDIUM], BuildConfig.VERSION_NAME);
                        }
                        if (tails.length == 3) {
                            locale = new Locale(tails[TEXT_DEFAULT], tails[TEXT_MEDIUM], tails[TEXT_LARGE]);
                        }
                        Locale.setDefault(locale);
                        appConfig2 = new Configuration();
                        appConfig2.locale = locale;
                        listAppsFragment.getRes().updateConfiguration(appConfig2, listAppsFragment.getRes().getDisplayMetrics());
                    }
                    if (p.ads) {
                        statusi = Utils.getText(C0149R.string.statads);
                    }
                    if (p.lvl) {
                        statusi = Utils.getText(C0149R.string.statnotfound);
                        break;
                    } else if (p.ads) {
                        statusi = Utils.getText(C0149R.string.statlvl);
                    } else {
                        statusi = statusi + LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.statlvl);
                    }
                    if (p.custom) {
                        statusi = Utils.getText(C0149R.string.statcustom);
                    }
                    if (p.custom) {
                        this.txtTitle.setTextColor(-256);
                    }
                    for (i = TEXT_DEFAULT; i < listAppsFragment.bootlist.length; i += TEXT_MEDIUM) {
                        if (p.boot_ads) {
                            break;
                        }
                        this.txtTitle.setTextColor(-65281);
                    }
                    if (p.billing) {
                        if (statusi.equals(BuildConfig.VERSION_NAME)) {
                            statusi = Utils.getText(C0149R.string.statbilling);
                        } else {
                            statusi = statusi + LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.statbilling);
                        }
                        if (p.lvl) {
                        }
                        if (p.custom) {
                            i3 = TEXT_MEDIUM;
                        } else {
                            i3 = TEXT_DEFAULT;
                        }
                        if ((i3 & i2) != 0) {
                            this.txtTitle.setTextColor(Color.parseColor("#c5b5ff"));
                        }
                    }
                    this.txtStatus.setText(statusi);
                    if (p.system) {
                        this.txtTitle.setTextColor(-162281);
                    }
                    this.txtTitle.setTypeface(this.txtTitle.getTypeface(), TEXT_DEFAULT);
                    if (p.enable) {
                        this.txtTitle.setTextColor(-7829368);
                        this.txtStatus.setTextColor(-7829368);
                    }
                    if (p.stored != 0) {
                        this.on_top_txt.setVisibility(TEXT_DEFAULT);
                        this.on_top_txt.setText(Utils.getText(C0149R.string.on_top_apps_marker));
                    } else {
                        this.on_top_txt.setVisibility(4);
                    }
                    if (!listAppsFragment.adapterSelect) {
                        return row;
                    }
                    this.on_top_txt.setVisibility(8);
                    if (listAppsFragment.adapterSelectType == C0149R.string.button_move_to_sdcard) {
                        break;
                    }
                    this.on_top_txt.setVisibility(8);
                    apk = new File(listAppsFragment.getPkgMng().getPackageInfo(p.pkgName, TEXT_DEFAULT).applicationInfo.sourceDir);
                    textView = this.txtStatus;
                    append = new StringBuilder().append(Utils.getText(C0149R.string.apk_size)).append(" ");
                    fArr = new Object[TEXT_MEDIUM];
                    fArr[TEXT_DEFAULT] = Float.valueOf(((float) apk.length()) / 1048576.0f);
                    textView.setText(append.append(String.format("%.3f", fArr)).append(" Mb").toString());
                    return row;
            }
            color = "#ffff0055";
            color = "#ff00ffff";
            color = "#ff00ff73";
            this.txtTitle.setTextColor(Color.parseColor(color));
            this.txtStatus.setTextColor(-7829368);
            statusi = BuildConfig.VERSION_NAME;
            lng = listAppsFragment.getConfig().getString("force_language", "default");
            if (lng.equals("default")) {
                locale = null;
                tails = lng.split("_");
                if (tails.length == TEXT_MEDIUM) {
                    locale = new Locale(tails[TEXT_DEFAULT]);
                }
                if (tails.length == TEXT_LARGE) {
                    locale = new Locale(tails[TEXT_DEFAULT], tails[TEXT_MEDIUM], BuildConfig.VERSION_NAME);
                }
                if (tails.length == 3) {
                    locale = new Locale(tails[TEXT_DEFAULT], tails[TEXT_MEDIUM], tails[TEXT_LARGE]);
                }
                Locale.setDefault(locale);
                appConfig2 = new Configuration();
                appConfig2.locale = locale;
                listAppsFragment.getRes().updateConfiguration(appConfig2, listAppsFragment.getRes().getDisplayMetrics());
            } else {
                appLoc3 = Resources.getSystem().getConfiguration().locale;
                Locale.setDefault(Locale.getDefault());
                appConfig3 = new Configuration();
                appConfig3.locale = appLoc3;
                listAppsFragment.getRes().updateConfiguration(appConfig3, listAppsFragment.getRes().getDisplayMetrics());
            }
            if (p.ads) {
                statusi = Utils.getText(C0149R.string.statads);
            }
            if (p.lvl) {
                statusi = Utils.getText(C0149R.string.statnotfound);
            } else if (p.ads) {
                statusi = statusi + LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.statlvl);
            } else {
                statusi = Utils.getText(C0149R.string.statlvl);
            }
            if (p.custom) {
                statusi = Utils.getText(C0149R.string.statcustom);
            }
            if (p.custom) {
                this.txtTitle.setTextColor(-256);
            }
            for (i = TEXT_DEFAULT; i < listAppsFragment.bootlist.length; i += TEXT_MEDIUM) {
                if (p.boot_ads) {
                }
                this.txtTitle.setTextColor(-65281);
            }
            if (p.billing) {
                if (statusi.equals(BuildConfig.VERSION_NAME)) {
                    statusi = statusi + LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.statbilling);
                } else {
                    statusi = Utils.getText(C0149R.string.statbilling);
                }
                if (p.lvl) {
                }
                if (p.custom) {
                    i3 = TEXT_DEFAULT;
                } else {
                    i3 = TEXT_MEDIUM;
                }
                if ((i3 & i2) != 0) {
                    this.txtTitle.setTextColor(Color.parseColor("#c5b5ff"));
                }
            }
            this.txtStatus.setText(statusi);
            if (p.system) {
                this.txtTitle.setTextColor(-162281);
            }
            this.txtTitle.setTypeface(this.txtTitle.getTypeface(), TEXT_DEFAULT);
            if (p.enable) {
                this.txtTitle.setTextColor(-7829368);
                this.txtStatus.setTextColor(-7829368);
            }
            if (p.stored != 0) {
                this.on_top_txt.setVisibility(4);
            } else {
                this.on_top_txt.setVisibility(TEXT_DEFAULT);
                this.on_top_txt.setText(Utils.getText(C0149R.string.on_top_apps_marker));
            }
            if (!listAppsFragment.adapterSelect) {
                return row;
            }
            this.on_top_txt.setVisibility(8);
            if (listAppsFragment.adapterSelectType == C0149R.string.button_move_to_sdcard) {
            }
            this.on_top_txt.setVisibility(8);
            apk = new File(listAppsFragment.getPkgMng().getPackageInfo(p.pkgName, TEXT_DEFAULT).applicationInfo.sourceDir);
            textView = this.txtStatus;
            append = new StringBuilder().append(Utils.getText(C0149R.string.apk_size)).append(" ");
            fArr = new Object[TEXT_MEDIUM];
            fArr[TEXT_DEFAULT] = Float.valueOf(((float) apk.length()) / 1048576.0f);
            textView.setText(append.append(String.format("%.3f", fArr)).append(" Mb").toString());
            return row;
        } catch (Exception e3) {
            return new View(this.context);
        }
    }

    public void onGroupCollapsed(int groupPosition) {
        super.onGroupCollapsed(groupPosition);
    }

    public void onGroupExpanded(int groupPosition) {
        if (groupPosition != listAppsFragment.lastExpandedGroupPosition) {
            listAppsFragment.lv.collapseGroup(listAppsFragment.lastExpandedGroupPosition);
        }
        super.onGroupExpanded(groupPosition);
        listAppsFragment.pli = getGroup(groupPosition);
        listAppsFragment.lastExpandedGroupPosition = groupPosition;
    }

    public boolean hasStableIds() {
        return true;
    }

    public void onGroupCollapsedAll() {
        if (this.data.length > 0) {
            for (int i = TEXT_DEFAULT; i < this.data.length; i += TEXT_MEDIUM) {
                listAppsFragment.pli = null;
                listAppsFragment.lv.collapseGroup(i);
            }
            listAppsFragment.pli = null;
        }
    }

    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public void setTextSize(int s) {
        this.size = s;
        notifyDataSetChanged();
    }

    public void remove(String pkgName) {
        if (checkItem(pkgName)) {
            try {
                PkgListItem[] tmp = new PkgListItem[(this.data.length - 1)];
                int j = TEXT_DEFAULT;
                for (int i = TEXT_DEFAULT; i < this.data.length; i += TEXT_MEDIUM) {
                    if (this.data[i].pkgName.equals(pkgName)) {
                        listAppsFragment.lv.collapseGroup(i);
                    } else {
                        tmp[j] = this.data[i];
                        j += TEXT_MEDIUM;
                    }
                }
                this.data = tmp;
                notifyDataSetChanged();
            } catch (Exception e) {
            }
        }
    }

    public void add(PkgListItem item) {
        System.out.println("add " + item.pkgName);
        ArrayList<PkgListItem> tmp = new ArrayList(Arrays.asList(this.data));
        tmp.add(item);
        this.data = new PkgListItem[tmp.size()];
        tmp.toArray(this.data);
        sort();
        notifyDataSetChanged();
    }

    public void sort() {
        try {
            Arrays.sort(this.data, this.sorter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public PkgListItem getItem(String name) {
        PkgListItem[] pkgListItemArr = this.data;
        int length = pkgListItemArr.length;
        for (int i = TEXT_DEFAULT; i < length; i += TEXT_MEDIUM) {
            PkgListItem aData = pkgListItemArr[i];
            if (aData.pkgName.contentEquals(name)) {
                return aData;
            }
        }
        return null;
    }

    public boolean checkItem(String name) {
        try {
            PkgListItem[] pkgListItemArr = this.data;
            int length = pkgListItemArr.length;
            for (int i = TEXT_DEFAULT; i < length; i += TEXT_MEDIUM) {
                if (pkgListItemArr[i].pkgName.contentEquals(name)) {
                    return true;
                }
            }
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public void notifyDataSetChanged(PkgListItem pli) {
        super.notifyDataSetChanged();
        if (listAppsFragment.database == null) {
            listAppsFragment.database = new DatabaseHelper(this.context);
        }
        listAppsFragment.database.savePackage(pli);
    }

    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    public PkgListItem getGroup(int groupPosition) {
        return this.data[groupPosition];
    }

    public void refreshPkgs(boolean saveTrigger) {
        if (listAppsFragment.addapps || !listAppsFragment.refresh) {
            System.out.println("LuckyPatcher: finalize refreshPackages.");
            listAppsFragment.refresh = false;
            return;
        }
        System.out.println("LuckyPatcher: start refreshPackages.");
        listAppsFragment.addapps = true;
        Thread refresh = new Thread(new Refresh_Packages());
        refresh.setPriority(TEXT_MEDIUM);
        refresh.start();
    }

    public int size() {
        return this.data.length;
    }

    public void updateItem(String name) {
        try {
            PkgListItem[] pkgListItemArr = this.data;
            int length = pkgListItemArr.length;
            for (int i = TEXT_DEFAULT; i < length; i += TEXT_MEDIUM) {
                PkgListItem aData = pkgListItemArr[i];
                if (aData.pkgName.contentEquals(name)) {
                    PkgListItem item = new PkgListItem(this.context, name, listAppsFragment.days, false);
                    if (!item.equalsPli(aData)) {
                        item.saveItem();
                        aData.pkgName = item.pkgName;
                        aData.name = item.name;
                        aData.storepref = item.storepref;
                        aData.stored = item.stored;
                        aData.hidden = item.hidden;
                        aData.boot_ads = item.boot_ads;
                        aData.boot_lvl = item.boot_lvl;
                        aData.boot_custom = item.boot_custom;
                        aData.boot_manual = item.boot_manual;
                        aData.custom = item.custom;
                        aData.on_sd = Utils.isInstalledOnSdCard(item.pkgName);
                        aData.lvl = item.lvl;
                        aData.ads = item.ads;
                        aData.billing = item.billing;
                        if (listAppsFragment.getConfig().getBoolean(name, false)) {
                            aData.modified = true;
                        }
                        aData.system = item.system;
                        aData.updatetime = item.updatetime;
                        if (Utils.isOdex(listAppsFragment.getPkgMng().getApplicationInfo(name, TEXT_DEFAULT).sourceDir)) {
                            aData.odex = true;
                        } else {
                            aData.odex = false;
                        }
                        aData.enable = item.enable;
                        return;
                    }
                    return;
                }
            }
        } catch (Exception e) {
            System.out.println("LuckyPatcher (updateItem PkgListItemAdapter):" + e);
            e.printStackTrace();
        }
    }

    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = ((LayoutInflater) this.context.getSystemService("layout_inflater")).inflate(C0149R.layout.child_view, null);
        }
        TextView textChild = (TextView) convertView.findViewById(C0149R.id.textChild);
        textChild.setTextAppearance(this.context, listAppsFragment.getSizeText());
        textChild.setTextAppearance(this.context, listAppsFragment.getSizeText());
        textChild.setText(Utils.getText(getChild(groupPosition, childPosition).intValue()));
        ImageView image = (ImageView) convertView.findViewById(C0149R.id.child_image);
        switch (getChild(groupPosition, childPosition).intValue()) {
            case C0149R.string.advanced_menu:
                image.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.menu));
                break;
            case C0149R.string.app_dialog_no_root_app_control:
                image.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.move_to));
                break;
            case C0149R.string.app_info:
                image.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.info_app));
                break;
            case C0149R.string.cleardata:
                image.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.clear));
                break;
            case C0149R.string.context_tools:
                image.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.menu));
                break;
            case C0149R.string.contextlaunchapp:
                image.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.run));
                break;
            case C0149R.string.move_to_internal:
                image.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.move_to));
                break;
            case C0149R.string.move_to_sdcard:
                image.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.move_to));
                break;
            case C0149R.string.uninstallapp:
                image.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.remove));
                break;
        }
        return convertView;
    }

    public Filter getFilter() {
        return new C01362();
    }
}
