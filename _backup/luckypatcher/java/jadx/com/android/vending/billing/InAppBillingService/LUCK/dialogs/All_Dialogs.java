package com.android.vending.billing.InAppBillingService.LUCK.dialogs;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.GradientDrawable.Orientation;
import android.os.AsyncTask;
import android.support.v4.internal.view.SupportMenu;
import android.support.v4.view.MotionEventCompat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.android.vending.billing.InAppBillingService.LUCK.AlertDlg;
import com.android.vending.billing.InAppBillingService.LUCK.BootListItemAdapter;
import com.android.vending.billing.InAppBillingService.LUCK.BuildConfig;
import com.android.vending.billing.InAppBillingService.LUCK.C0149R;
import com.android.vending.billing.InAppBillingService.LUCK.Components;
import com.android.vending.billing.InAppBillingService.LUCK.CoreItem;
import com.android.vending.billing.InAppBillingService.LUCK.FileApkListItem;
import com.android.vending.billing.InAppBillingService.LUCK.LogCollector;
import com.android.vending.billing.InAppBillingService.LUCK.Patterns;
import com.android.vending.billing.InAppBillingService.LUCK.Perm;
import com.android.vending.billing.InAppBillingService.LUCK.PkgListItem;
import com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment;
import com.android.vending.billing.InAppBillingService.LUCK.patchActivity;
import com.chelpus.Common;
import com.chelpus.Utils;
import com.google.android.finsky.billing.iab.google.util.Base64;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Comparator;
import net.lingala.zip4j.util.InternalZipConstants;
import org.json.JSONException;
import org.json.JSONObject;
import org.tukaani.xz.LZMA2Options;
import pxb.android.axml.AxmlParser;

public class All_Dialogs {
    public static final int ADD_BOOT = 2;
    public static final int CREATE_APK = 0;
    public static final int CUSTOM_PATCH = 1;
    Dialog dialog = null;
    public int dialog_int = MotionEventCompat.ACTION_MASK;

    class C01731 implements OnItemClickListener {
        C01731() {
        }

        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
            Perm id1 = (Perm) listAppsFragment.perm_adapt.getItem(position);
            if (id1.Status) {
                id1.Status = false;
            } else {
                id1.Status = true;
            }
            listAppsFragment.perm_adapt.notifyDataSetChanged();
        }
    }

    class C01742 implements OnClickListener {
        C01742() {
        }

        public void onClick(DialogInterface dialog, int which) {
            ArrayList<String> permiss = new ArrayList();
            ArrayList<String> activ = new ArrayList();
            for (int i = All_Dialogs.CREATE_APK; i < listAppsFragment.perm_adapt.getCount(); i += All_Dialogs.CUSTOM_PATCH) {
                Perm id1 = (Perm) listAppsFragment.perm_adapt.getItem(i);
                if (!(id1.Status || id1.Name.contains("chelpus_"))) {
                    permiss.add(id1.Name);
                }
                if (id1.Status && id1.Name.startsWith("chelpa_per_")) {
                    permiss.add(id1.Name.replace("chelpa_per_", BuildConfig.VERSION_NAME));
                    System.out.println(id1.Name.replace("chelpa_per_", BuildConfig.VERSION_NAME));
                }
                if (!(id1.Status || !id1.Name.contains("chelpus_") || id1.Name.contains("disabled_"))) {
                    activ.add(id1.Name.replace("chelpus_", BuildConfig.VERSION_NAME));
                    System.out.println(BuildConfig.VERSION_NAME + id1.Name.replace("chelpus_", BuildConfig.VERSION_NAME));
                }
                if (id1.Status && id1.Name.contains("chelpus_disabled_")) {
                    activ.add(id1.Name.replace("chelpus_", BuildConfig.VERSION_NAME));
                    System.out.println(BuildConfig.VERSION_NAME + id1.Name.replace("chelpus_", BuildConfig.VERSION_NAME));
                }
            }
            if (listAppsFragment.rebuldApk.equals(BuildConfig.VERSION_NAME)) {
                listAppsFragment.frag.createapkpermissions(permiss, activ);
            } else {
                listAppsFragment.frag.toolbar_createapkpermissions(permiss, activ);
            }
        }
    }

    class C01773 implements OnItemClickListener {
        C01773() {
        }

        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
            Perm id1 = (Perm) listAppsFragment.perm_adapt.getItem(position);
            if (id1.Status) {
                id1.Status = false;
            } else {
                id1.Status = true;
            }
            listAppsFragment.perm_adapt.notifyDataSetChanged();
        }
    }

    class C01794 implements OnClickListener {
        C01794() {
        }

        public void onClick(DialogInterface dialog, int which) {
            ArrayList<String> permiss = new ArrayList();
            ArrayList<String> activ = new ArrayList();
            for (int i = All_Dialogs.CREATE_APK; i < listAppsFragment.perm_adapt.getCount(); i += All_Dialogs.CUSTOM_PATCH) {
                Perm id1 = (Perm) listAppsFragment.perm_adapt.getItem(i);
                if (!(id1.Status || id1.Name.contains("chelpus_"))) {
                    permiss.add(id1.Name);
                }
                if (id1.Status && id1.Name.startsWith("chelpa_per_")) {
                    permiss.add(id1.Name.replace("chelpa_per_", BuildConfig.VERSION_NAME));
                    System.out.println(id1.Name.replace("chelpa_per_", BuildConfig.VERSION_NAME));
                }
                if (!(id1.Status || !id1.Name.contains("chelpus_") || id1.Name.contains("disabled_"))) {
                    activ.add(id1.Name.replace("chelpus_", BuildConfig.VERSION_NAME));
                    System.out.println(BuildConfig.VERSION_NAME + id1.Name.replace("chelpus_", BuildConfig.VERSION_NAME));
                }
                if (id1.Status && id1.Name.contains("chelpus_disabled_")) {
                    activ.add(id1.Name.replace("chelpus_", BuildConfig.VERSION_NAME));
                    System.out.println(BuildConfig.VERSION_NAME + id1.Name.replace("chelpus_", BuildConfig.VERSION_NAME));
                }
            }
            listAppsFragment.frag.apkpermissions(listAppsFragment.pli, permiss, activ);
        }
    }

    class C01805 implements OnItemClickListener {
        C01805() {
        }

        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
            Perm id1 = (Perm) listAppsFragment.perm_adapt.getItem(position);
            Utils utils;
            String[] strArr;
            if (id1.Status) {
                utils = new Utils(BuildConfig.VERSION_NAME);
                strArr = new String[All_Dialogs.CUSTOM_PATCH];
                strArr[All_Dialogs.CREATE_APK] = "pm disable '" + listAppsFragment.pli.pkgName + InternalZipConstants.ZIP_FILE_SEPARATOR + id1.Name + "'";
                utils.cmdRoot(strArr);
            } else {
                utils = new Utils(BuildConfig.VERSION_NAME);
                strArr = new String[All_Dialogs.CUSTOM_PATCH];
                strArr[All_Dialogs.CREATE_APK] = "pm enable '" + listAppsFragment.pli.pkgName + InternalZipConstants.ZIP_FILE_SEPARATOR + id1.Name + "'";
                utils.cmdRoot(strArr);
            }
            if (listAppsFragment.getPkgMng().getComponentEnabledSetting(new ComponentName(listAppsFragment.pli.pkgName, id1.Name)) == All_Dialogs.ADD_BOOT) {
                id1.Status = false;
            } else {
                id1.Status = true;
            }
            listAppsFragment.perm_adapt.notifyDataSetChanged();
            listAppsFragment.pli.modified = true;
            listAppsFragment.plia.notifyDataSetChanged(listAppsFragment.pli);
            listAppsFragment.getConfig().edit().putBoolean(listAppsFragment.pli.pkgName, true).commit();
        }
    }

    class C01816 implements OnClickListener {
        C01816() {
        }

        public void onClick(DialogInterface dialog, int which) {
            try {
                Intent i = listAppsFragment.getPkgMng().getLaunchIntentForPackage(listAppsFragment.pli.pkgName);
                if (listAppsFragment.patchAct != null) {
                    listAppsFragment.patchAct.startActivity(i);
                }
            } catch (Exception e) {
                Toast.makeText(listAppsFragment.frag.getContext(), Utils.getText(C0149R.string.error_launch), All_Dialogs.CUSTOM_PATCH).show();
            }
            listAppsFragment.removeDialogLP(29);
        }
    }

    class C01827 implements OnItemClickListener {
        C01827() {
        }

        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
            Components id1 = (Components) listAppsFragment.component_adapt.getItem(position);
            if (!id1.header) {
                Utils utils;
                String[] strArr;
                if (id1.Status) {
                    utils = new Utils(BuildConfig.VERSION_NAME);
                    strArr = new String[All_Dialogs.CUSTOM_PATCH];
                    strArr[All_Dialogs.CREATE_APK] = "pm disable '" + listAppsFragment.pli.pkgName + InternalZipConstants.ZIP_FILE_SEPARATOR + id1.Name + "'";
                    utils.cmdRoot(strArr);
                } else {
                    utils = new Utils(BuildConfig.VERSION_NAME);
                    strArr = new String[All_Dialogs.CUSTOM_PATCH];
                    strArr[All_Dialogs.CREATE_APK] = "pm enable '" + listAppsFragment.pli.pkgName + InternalZipConstants.ZIP_FILE_SEPARATOR + id1.Name + "'";
                    utils.cmdRoot(strArr);
                }
                if (listAppsFragment.getPkgMng().getComponentEnabledSetting(new ComponentName(listAppsFragment.pli.pkgName, id1.Name)) == All_Dialogs.ADD_BOOT) {
                    id1.Status = false;
                } else {
                    id1.Status = true;
                }
                listAppsFragment.component_adapt.notifyDataSetChanged();
                listAppsFragment.pli.modified = true;
                listAppsFragment.plia.notifyDataSetChanged(listAppsFragment.pli);
                listAppsFragment.getConfig().edit().putBoolean(listAppsFragment.pli.pkgName, true).commit();
            }
        }
    }

    class C01838 implements OnClickListener {
        C01838() {
        }

        public void onClick(DialogInterface dialog, int which) {
            try {
                Intent i = listAppsFragment.getPkgMng().getLaunchIntentForPackage(listAppsFragment.pli.pkgName);
                if (listAppsFragment.patchAct != null) {
                    listAppsFragment.patchAct.startActivity(i);
                }
            } catch (Exception e) {
                Toast.makeText(listAppsFragment.frag.getContext(), Utils.getText(C0149R.string.error_launch), All_Dialogs.CUSTOM_PATCH).show();
            }
            listAppsFragment com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment = listAppsFragment.frag;
            listAppsFragment.removeDialogLP(31);
        }
    }

    class C01849 implements OnItemClickListener {
        C01849() {
        }

        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
            Perm id1 = (Perm) listAppsFragment.perm_adapt.getItem(position);
            if (id1.Status) {
                id1.Status = false;
            } else {
                id1.Status = true;
            }
            listAppsFragment.perm_adapt.notifyDataSetChanged();
        }
    }

    public class byPkgName implements Comparator<PkgListItem> {
        public int compare(PkgListItem a, PkgListItem b) {
            if (a == null || b == null) {
                throw new ClassCastException();
            } else if (a.stored != 0 && b.stored != 0) {
                return a.toString().compareToIgnoreCase(b.toString());
            } else {
                int ret = Integer.valueOf(a.stored).compareTo(Integer.valueOf(b.stored));
                if (ret == 0) {
                    return a.toString().compareToIgnoreCase(b.toString());
                }
                return ret;
            }
        }
    }

    public void showDialog() {
        if (this.dialog == null) {
            this.dialog = onCreateDialog();
        }
        if (this.dialog != null) {
            this.dialog.show();
        }
    }

    public void dismiss() {
        if (this.dialog != null) {
            this.dialog.dismiss();
            this.dialog = null;
        }
    }

    public boolean isVisible() {
        if (this.dialog != null) {
            return this.dialog.isShowing();
        }
        return false;
    }

    public Dialog onCreateDialog() {
        AlertDlg alertDlg;
        System.out.println("Create dialog");
        this.dialog_int = listAppsFragment.dialog_int;
        if (listAppsFragment.frag == null || listAppsFragment.frag.getContext() == null) {
            dismiss();
        }
        try {
            System.out.println("All Dialog create.");
            int[] iArr;
            switch (listAppsFragment.dialog_int) {
                case AxmlParser.END_TAG /*3*/:
                    AlertDlg builder = new AlertDlg(listAppsFragment.frag.getContext());
                    listAppsFragment.frag.refresh_boot();
                    Context context = listAppsFragment.frag.getContext();
                    SharedPreferences config = listAppsFragment.getConfig();
                    String str = listAppsFragment.SETTINGS_VIEWSIZE;
                    listAppsFragment com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment = listAppsFragment.frag;
                    listAppsFragment.adapter_boot = new BootListItemAdapter(context, C0149R.layout.bootlistitemview, config.getInt(str, CREATE_APK), listAppsFragment.boot_pat);
                    listAppsFragment.adapter_boot.sorter = new byPkgName();
                    try {
                        listAppsFragment.plia.notifyDataSetChanged();
                    } catch (Exception e) {
                        System.out.println("LuckyPatcher(Bootlist dialog): " + e);
                    }
                    listAppsFragment.adapter_boot.notifyDataSetChanged();
                    builder.setTitle(Utils.getText(C0149R.string.delboot));
                    builder.setAdapter(listAppsFragment.adapter_boot, new OnItemClickListener() {
                        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                            PkgListItem select = (PkgListItem) listAppsFragment.adapter_boot.getItem(position);
                            if (select.pkgName.equals(Utils.getText(C0149R.string.android_patches_for_bootlist))) {
                                Utils.turn_off_patch_on_boot_all();
                            } else {
                                int i;
                                try {
                                    listAppsFragment.getPkgMng().getPackageInfo(select.pkgName, All_Dialogs.CREATE_APK);
                                    for (i = All_Dialogs.CREATE_APK; i < listAppsFragment.plia.data.length; i += All_Dialogs.CUSTOM_PATCH) {
                                        if (listAppsFragment.plia.data[i].pkgName.equals(select.pkgName)) {
                                            listAppsFragment.plia.data[i].boot_ads = false;
                                            listAppsFragment.plia.data[i].boot_lvl = false;
                                            if (listAppsFragment.plia.data[i].boot_custom) {
                                                if (new File(listAppsFragment.frag.getContext().getDir("bootlist", All_Dialogs.CREATE_APK) + InternalZipConstants.ZIP_FILE_SEPARATOR + listAppsFragment.plia.data[i].pkgName).exists()) {
                                                    new File(listAppsFragment.frag.getContext().getDir("bootlist", All_Dialogs.CREATE_APK) + InternalZipConstants.ZIP_FILE_SEPARATOR + listAppsFragment.plia.data[i].pkgName).delete();
                                                }
                                                listAppsFragment.plia.data[i].boot_custom = false;
                                            }
                                            listAppsFragment.plia.data[i].boot_manual = false;
                                            listAppsFragment.plia.notifyDataSetChanged(listAppsFragment.plia.data[i]);
                                        }
                                    }
                                } catch (NameNotFoundException e) {
                                    if (new File(listAppsFragment.frag.getContext().getDir("bootlist", All_Dialogs.CREATE_APK) + InternalZipConstants.ZIP_FILE_SEPARATOR + select.pkgName).exists()) {
                                        new File(listAppsFragment.frag.getContext().getDir("bootlist", All_Dialogs.CREATE_APK) + InternalZipConstants.ZIP_FILE_SEPARATOR + select.pkgName).delete();
                                    }
                                }
                                StringBuilder stringBuilder = new StringBuilder();
                                stringBuilder = new StringBuilder();
                                i = All_Dialogs.CREATE_APK;
                                while (i < listAppsFragment.plia.data.length) {
                                    if (listAppsFragment.plia.data[i].boot_ads || listAppsFragment.plia.data[i].boot_custom || listAppsFragment.plia.data[i].boot_lvl || listAppsFragment.plia.data[i].boot_manual) {
                                        stringBuilder.append(listAppsFragment.plia.data[i].pkgName);
                                        if (listAppsFragment.plia.data[i].boot_ads) {
                                            stringBuilder.append("%ads");
                                        }
                                        if (listAppsFragment.plia.data[i].boot_lvl) {
                                            stringBuilder.append("%lvl");
                                        }
                                        if (listAppsFragment.plia.data[i].boot_custom) {
                                            stringBuilder.append("%custom");
                                        }
                                        if (listAppsFragment.plia.data[i].boot_manual) {
                                            stringBuilder.append("%object");
                                        }
                                        stringBuilder.append(",");
                                    }
                                    i += All_Dialogs.CUSTOM_PATCH;
                                }
                                stringBuilder.append(LogCollector.LINE_SEPARATOR);
                                try {
                                    FileOutputStream fos = new FileOutputStream(new File(listAppsFragment.frag.getContext().getDir("bootlist", All_Dialogs.CREATE_APK) + "/bootlist"));
                                    fos.write(stringBuilder.toString().getBytes());
                                    fos.close();
                                } catch (FileNotFoundException e2) {
                                } catch (Exception e3) {
                                }
                            }
                            listAppsFragment.plia.notifyDataSetChanged();
                            System.out.println("asd1");
                            listAppsFragment com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment = listAppsFragment.frag;
                            listAppsFragment.showDialogLP(3);
                        }
                    });
                    System.out.println("asd2");
                    return builder.create();
                case listAppsFragment.PERM_CONTEXT_DIALOG /*10*/:
                    listAppsFragment.changedPermissions = new ArrayList();
                    AlertDlg builder6 = new AlertDlg(listAppsFragment.frag.getContext());
                    if (listAppsFragment.perm_adapt != null) {
                        listAppsFragment.perm_adapt.setNotifyOnChange(true);
                        builder6.setAdapterNotClose(true);
                        builder6.setAdapter(listAppsFragment.perm_adapt, new OnItemClickListener() {
                            public void onItemClick(AdapterView<?> adapterView, View view, final int position, long id) {
                                listAppsFragment.frag.runWithWait(new Runnable() {

                                    class C01701 implements Runnable {
                                        C01701() {
                                        }

                                        public void run() {
                                            listAppsFragment com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment = listAppsFragment.frag;
                                            listAppsFragment.removeDialogLP(10);
                                            com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment = listAppsFragment.frag;
                                            listAppsFragment.showDialogLP(10);
                                        }
                                    }

                                    public void run() {
                                        listAppsFragment.frag.disable_permission(listAppsFragment.pli, (Perm) listAppsFragment.perm_adapt.getItem(position));
                                        listAppsFragment.frag.contextpermissions();
                                        listAppsFragment.frag.runToMain(new C01701());
                                    }
                                });
                            }
                        });
                        builder6.setPositiveButton(Utils.getText(C0149R.string.perm_ok), (OnClickListener) new OnClickListener() {

                            class C01721 implements Runnable {
                                C01721() {
                                }

                                /* JADX WARNING: inconsistent code. */
                                /* Code decompiled incorrectly, please refer to instructions dump. */
                                public void run() {
                                    String PackagesPermissions = null;
                                    try {
                                        if (Utils.exists("/data/system/packages.xml")) {
                                            PackagesPermissions = "/data/system/packages.xml";
                                        }
                                    } catch (Exception e) {
                                        System.out.println("LuckyPatcher (Get packages.xml Error): " + e);
                                    }
                                    try {
                                        if (Utils.exists("/dbdata/system/packages.xml")) {
                                            PackagesPermissions = "/dbdata/system/packages.xml";
                                        }
                                    } catch (Exception e2) {
                                        System.out.println("LuckyPatcher (Get packages.xml Error): " + e2);
                                    }
                                    try {
                                    } catch (Exception e3) {
                                        String str = "777";
                                    }
                                    Utils.copyFile(listAppsFragment.frag.getContext().getDir("packages", All_Dialogs.CREATE_APK).getAbsolutePath() + "/packages.xml", PackagesPermissions, false, false);
                                    Utils.run_all("chmod 777 " + PackagesPermissions);
                                    Utils.run_all("chown 1000.1000 " + PackagesPermissions);
                                    Utils.run_all("chown 1000:1000 " + PackagesPermissions);
                                    listAppsFragment.plia.getItem(listAppsFragment.pli.pkgName).modified = true;
                                    listAppsFragment.getConfig().edit().putBoolean(listAppsFragment.plia.getItem(listAppsFragment.pli.pkgName).pkgName, true).commit();
                                    Utils.reboot();
                                    Utils.run_all("killall -9 zygote");
                                }
                            }

                            public void onClick(DialogInterface dialog, int which) {
                                listAppsFragment.frag.runWithWait(new C01721());
                            }
                        });
                    }
                    return builder6.create();
                case listAppsFragment.PATCH_CONTEXT_DIALOG /*13*/:
                    AlertDlg builder7 = new AlertDlg(listAppsFragment.frag.getContext());
                    if (listAppsFragment.patch_adapt != null) {
                        listAppsFragment.patch_adapt.setNotifyOnChange(true);
                        builder7.setAdapterNotClose(true);
                        builder7.setAdapter(listAppsFragment.patch_adapt, new OnItemClickListener() {
                            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                                Patterns id1 = (Patterns) listAppsFragment.patch_adapt.getItem(position);
                                if (id1.Status) {
                                    id1.Status = false;
                                } else {
                                    id1.Status = true;
                                }
                                if (position == 4 && id1.Status) {
                                    id1 = (Patterns) listAppsFragment.patch_adapt.getItem(5);
                                    id1.Status = false;
                                }
                                if (position == 5 && r0.Status) {
                                    id1 = (Patterns) listAppsFragment.patch_adapt.getItem(4);
                                    id1.Status = false;
                                }
                                if (position == 6 && id1.Status) {
                                    ((Patterns) listAppsFragment.patch_adapt.getItem(7)).Status = true;
                                    id1 = (Patterns) listAppsFragment.patch_adapt.getItem(8);
                                    id1.Status = false;
                                }
                                if (position == 8 && id1.Status) {
                                    ((Patterns) listAppsFragment.patch_adapt.getItem(7)).Status = true;
                                    ((Patterns) listAppsFragment.patch_adapt.getItem(6)).Status = false;
                                }
                                listAppsFragment.patch_adapt.notifyDataSetChanged();
                            }
                        });
                        builder7.setPositiveButton(Utils.getText(C0149R.string.patch_ok), (OnClickListener) new OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                String result = "1";
                                int count = listAppsFragment.patch_adapt.getCount();
                                int i = All_Dialogs.CREATE_APK;
                                while (i < count) {
                                    Patterns patt = (Patterns) listAppsFragment.patch_adapt.getItem(i);
                                    if (patt.Status && i < 6) {
                                        result = result + "pattern" + (i + All_Dialogs.CUSTOM_PATCH) + "_";
                                    }
                                    if (i == 6 && patt.Status) {
                                        result = result + "copyDC" + "_";
                                    }
                                    if (i == 7 && patt.Status) {
                                        result = result + "backup" + "_";
                                    }
                                    if (i == 8 && patt.Status) {
                                        result = result + "deleteDC" + "_";
                                    }
                                    i += All_Dialogs.CUSTOM_PATCH;
                                }
                                listAppsFragment.frag.addIgnoreOdex(listAppsFragment.pli, result);
                            }
                        });
                    }
                    return builder7.create();
                case listAppsFragment.CUSTOM_PATCH_SELECTOR /*16*/:
                    AlertDlg builder9 = new AlertDlg(listAppsFragment.frag.getContext());
                    if (listAppsFragment.adapt != null) {
                        listAppsFragment.adapt.setNotifyOnChange(true);
                        builder9.setAdapter(listAppsFragment.adapt, new OnItemClickListener() {
                            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                                listAppsFragment com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment;
                                switch (listAppsFragment.func) {
                                    case All_Dialogs.CREATE_APK /*0*/:
                                        com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment = listAppsFragment.frag;
                                        listAppsFragment.removeDialogLP(16);
                                        listAppsFragment.customselect = (File) listAppsFragment.adapt.getItem(position);
                                        com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment = listAppsFragment.frag;
                                        listAppsFragment.showDialogLP(15);
                                        return;
                                    case All_Dialogs.CUSTOM_PATCH /*1*/:
                                        com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment = listAppsFragment.frag;
                                        listAppsFragment.removeDialogLP(16);
                                        listAppsFragment.customselect = (File) listAppsFragment.adapt.getItem(position);
                                        com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment = listAppsFragment.frag;
                                        listAppsFragment.showDialogLP(15);
                                        return;
                                    case All_Dialogs.ADD_BOOT /*2*/:
                                        com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment = listAppsFragment.frag;
                                        listAppsFragment.removeDialogLP(16);
                                        listAppsFragment.customselect = (File) listAppsFragment.adapt.getItem(position);
                                        listAppsFragment.frag.bootadd(listAppsFragment.pli, "custom");
                                        return;
                                    default:
                                        return;
                                }
                            }
                        });
                    }
                    return builder9.create();
                case listAppsFragment.ADS_PATCH_CONTEXT_DIALOG /*17*/:
                    AlertDlg adsbuilder7 = new AlertDlg(listAppsFragment.frag.getContext());
                    if (listAppsFragment.patch_adapt != null) {
                        listAppsFragment.patch_adapt.setNotifyOnChange(true);
                        adsbuilder7.setAdapterNotClose(true);
                        adsbuilder7.setAdapter(listAppsFragment.patch_adapt, new OnItemClickListener() {
                            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                                Patterns id1 = (Patterns) listAppsFragment.patch_adapt.getItem(position);
                                if (id1.Status) {
                                    id1.Status = false;
                                } else {
                                    id1.Status = true;
                                }
                                if (position == All_Dialogs.CUSTOM_PATCH && id1.Status) {
                                    id1 = (Patterns) listAppsFragment.patch_adapt.getItem(All_Dialogs.ADD_BOOT);
                                    id1.Status = false;
                                }
                                if (position == All_Dialogs.ADD_BOOT && r0.Status) {
                                    id1 = (Patterns) listAppsFragment.patch_adapt.getItem(All_Dialogs.CUSTOM_PATCH);
                                    id1.Status = false;
                                }
                                if (position == 9 && id1.Status) {
                                    ((Patterns) listAppsFragment.patch_adapt.getItem(10)).Status = true;
                                    id1 = (Patterns) listAppsFragment.patch_adapt.getItem(11);
                                    id1.Status = false;
                                }
                                if (position == 11 && id1.Status) {
                                    ((Patterns) listAppsFragment.patch_adapt.getItem(10)).Status = true;
                                    ((Patterns) listAppsFragment.patch_adapt.getItem(9)).Status = false;
                                }
                                listAppsFragment.patch_adapt.notifyDataSetChanged();
                            }
                        });
                        adsbuilder7.setPositiveButton(Utils.getText(C0149R.string.patch_ok), (OnClickListener) new OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                String result = "1";
                                int count = listAppsFragment.patch_adapt.getCount();
                                int i = All_Dialogs.CREATE_APK;
                                while (i < count) {
                                    Patterns patt = (Patterns) listAppsFragment.patch_adapt.getItem(i);
                                    if (patt.Status && i < 7) {
                                        result = result + "pattern" + i + "_";
                                    }
                                    if (i == 7 && patt.Status) {
                                        result = result + "dependencies" + "_";
                                    }
                                    if (i == 8 && patt.Status) {
                                        result = result + "fulloffline" + "_";
                                    }
                                    if (i == 9 && patt.Status) {
                                        result = result + "copyDC" + "_";
                                    }
                                    if (i == 10 && patt.Status) {
                                        result = result + "backup" + "_";
                                    }
                                    if (i == 11 && patt.Status) {
                                        result = result + "deleteDC" + "_";
                                    }
                                    i += All_Dialogs.CUSTOM_PATCH;
                                }
                                listAppsFragment.frag.ads(listAppsFragment.pli, result);
                            }
                        });
                    }
                    return adsbuilder7.create();
                case listAppsFragment.Create_ADS_PATCH_CONTEXT_DIALOG /*18*/:
                    AlertDlg ads2builder7 = new AlertDlg(listAppsFragment.frag.getContext());
                    if (listAppsFragment.patch_adapt != null) {
                        listAppsFragment.patch_adapt.setNotifyOnChange(true);
                        ads2builder7.setAdapterNotClose(true);
                        ads2builder7.setAdapter(listAppsFragment.patch_adapt, new OnItemClickListener() {
                            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                                Patterns id1 = (Patterns) listAppsFragment.patch_adapt.getItem(position);
                                if (id1.Status) {
                                    id1.Status = false;
                                } else {
                                    id1.Status = true;
                                }
                                if (position == All_Dialogs.CUSTOM_PATCH && id1.Status) {
                                    id1 = (Patterns) listAppsFragment.patch_adapt.getItem(All_Dialogs.ADD_BOOT);
                                    id1.Status = false;
                                }
                                if (position == All_Dialogs.ADD_BOOT && id1.Status) {
                                    ((Patterns) listAppsFragment.patch_adapt.getItem(All_Dialogs.CUSTOM_PATCH)).Status = false;
                                }
                                listAppsFragment.patch_adapt.notifyDataSetChanged();
                            }
                        });
                        ads2builder7.setPositiveButton(Utils.getText(C0149R.string.create_perm_ok), (OnClickListener) new OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                String result = "1";
                                int count = listAppsFragment.patch_adapt.getCount();
                                int i = All_Dialogs.CREATE_APK;
                                while (i < count) {
                                    Patterns patt = (Patterns) listAppsFragment.patch_adapt.getItem(i);
                                    if (patt.Status && i < 7) {
                                        result = result + "pattern" + i + "_";
                                    }
                                    if (i == 7 && patt.Status) {
                                        result = result + "dependencies" + "_";
                                    }
                                    if (i == 8 && patt.Status) {
                                        result = result + "fulloffline" + "_";
                                    }
                                    i += All_Dialogs.CUSTOM_PATCH;
                                }
                                if (listAppsFragment.rebuldApk.equals(BuildConfig.VERSION_NAME)) {
                                    listAppsFragment.frag.createapkads(result);
                                } else {
                                    listAppsFragment.frag.toolbar_createapkads(result);
                                }
                            }
                        });
                    }
                    return ads2builder7.create();
                case listAppsFragment.Create_LVL_PATCH_CONTEXT_DIALOG /*19*/:
                    alertDlg = new AlertDlg(listAppsFragment.frag.getContext());
                    if (listAppsFragment.patch_adapt != null) {
                        listAppsFragment.patch_adapt.setNotifyOnChange(true);
                        alertDlg.setAdapterNotClose(true);
                        alertDlg.setAdapter(listAppsFragment.patch_adapt, new OnItemClickListener() {
                            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                                Patterns id1 = (Patterns) listAppsFragment.patch_adapt.getItem(position);
                                if (id1.Status) {
                                    id1.Status = false;
                                } else {
                                    id1.Status = true;
                                }
                                if (position == 0 && id1.Status) {
                                    id1 = (Patterns) listAppsFragment.patch_adapt.getItem(All_Dialogs.CUSTOM_PATCH);
                                    id1.Status = false;
                                }
                                if (position == All_Dialogs.CUSTOM_PATCH && r0.Status) {
                                    id1 = (Patterns) listAppsFragment.patch_adapt.getItem(All_Dialogs.CREATE_APK);
                                    id1.Status = false;
                                }
                                if (position == 3 && id1.Status) {
                                    ((Patterns) listAppsFragment.patch_adapt.getItem(All_Dialogs.CREATE_APK)).Status = false;
                                    ((Patterns) listAppsFragment.patch_adapt.getItem(All_Dialogs.CUSTOM_PATCH)).Status = false;
                                    ((Patterns) listAppsFragment.patch_adapt.getItem(All_Dialogs.ADD_BOOT)).Status = false;
                                    id1 = (Patterns) listAppsFragment.patch_adapt.getItem(4);
                                    id1.Status = false;
                                }
                                if (position == 4 && id1.Status) {
                                    ((Patterns) listAppsFragment.patch_adapt.getItem(All_Dialogs.CREATE_APK)).Status = false;
                                    ((Patterns) listAppsFragment.patch_adapt.getItem(All_Dialogs.CUSTOM_PATCH)).Status = false;
                                    ((Patterns) listAppsFragment.patch_adapt.getItem(All_Dialogs.ADD_BOOT)).Status = false;
                                    ((Patterns) listAppsFragment.patch_adapt.getItem(3)).Status = false;
                                }
                                listAppsFragment.patch_adapt.notifyDataSetChanged();
                            }
                        });
                        alertDlg.setPositiveButton(Utils.getText(C0149R.string.create_perm_ok), (OnClickListener) new OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                String result = "1";
                                int count = listAppsFragment.patch_adapt.getCount();
                                for (int i = All_Dialogs.CREATE_APK; i < count; i += All_Dialogs.CUSTOM_PATCH) {
                                    Patterns patt = (Patterns) listAppsFragment.patch_adapt.getItem(i);
                                    if (i == 0 && patt.Status) {
                                        result = "pattern1_pattern2_pattern3_pattern5_";
                                    }
                                    if (i == All_Dialogs.CUSTOM_PATCH && patt.Status) {
                                        result = "pattern1_pattern2_pattern3_pattern6_";
                                    }
                                    if (i == All_Dialogs.ADD_BOOT && patt.Status) {
                                        result = result + "pattern4" + "_";
                                    }
                                    if (i == 3 && patt.Status) {
                                        result = result + "amazon" + "_";
                                    }
                                    if (i == 4 && patt.Status) {
                                        result = result + "samsung" + "_";
                                    }
                                    if (i == 5 && patt.Status) {
                                        result = result + "dependencies" + "_";
                                    }
                                }
                                if (listAppsFragment.rebuldApk.equals(BuildConfig.VERSION_NAME)) {
                                    listAppsFragment.frag.createapklvl(result);
                                } else {
                                    listAppsFragment.frag.toolbar_createapklvl(result);
                                }
                            }
                        });
                    }
                    return alertDlg.create();
                case LZMA2Options.MF_BT4 /*20*/:
                    alertDlg = new AlertDlg(listAppsFragment.frag.getContext());
                    if (listAppsFragment.patch_adapt != null) {
                        listAppsFragment.patch_adapt.setNotifyOnChange(true);
                        alertDlg.setAdapterNotClose(true);
                        alertDlg.setAdapter(listAppsFragment.patch_adapt, new OnItemClickListener() {
                            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                                Patterns id1 = (Patterns) listAppsFragment.patch_adapt.getItem(position);
                                if (id1.Status) {
                                    id1.Status = false;
                                } else {
                                    id1.Status = true;
                                }
                                if (position == 0 && id1.Status) {
                                    id1 = (Patterns) listAppsFragment.patch_adapt.getItem(All_Dialogs.CUSTOM_PATCH);
                                    id1.Status = false;
                                }
                                if (position == All_Dialogs.CUSTOM_PATCH && r0.Status) {
                                    id1 = (Patterns) listAppsFragment.patch_adapt.getItem(All_Dialogs.CREATE_APK);
                                    id1.Status = false;
                                }
                                if (position == 3 && r0.Status) {
                                    ((Patterns) listAppsFragment.patch_adapt.getItem(All_Dialogs.CREATE_APK)).Status = false;
                                    ((Patterns) listAppsFragment.patch_adapt.getItem(All_Dialogs.CUSTOM_PATCH)).Status = false;
                                    ((Patterns) listAppsFragment.patch_adapt.getItem(All_Dialogs.ADD_BOOT)).Status = false;
                                    id1 = (Patterns) listAppsFragment.patch_adapt.getItem(4);
                                    id1.Status = false;
                                }
                                if (position == 4 && r0.Status) {
                                    ((Patterns) listAppsFragment.patch_adapt.getItem(All_Dialogs.CREATE_APK)).Status = false;
                                    ((Patterns) listAppsFragment.patch_adapt.getItem(All_Dialogs.CUSTOM_PATCH)).Status = false;
                                    ((Patterns) listAppsFragment.patch_adapt.getItem(All_Dialogs.ADD_BOOT)).Status = false;
                                    id1 = (Patterns) listAppsFragment.patch_adapt.getItem(3);
                                    id1.Status = false;
                                }
                                if (position == 6 && id1.Status) {
                                    ((Patterns) listAppsFragment.patch_adapt.getItem(7)).Status = true;
                                    id1 = (Patterns) listAppsFragment.patch_adapt.getItem(8);
                                    id1.Status = false;
                                }
                                if (position == 8 && id1.Status) {
                                    ((Patterns) listAppsFragment.patch_adapt.getItem(7)).Status = true;
                                    ((Patterns) listAppsFragment.patch_adapt.getItem(6)).Status = false;
                                }
                                listAppsFragment.patch_adapt.notifyDataSetChanged();
                            }
                        });
                        alertDlg.setPositiveButton(Utils.getText(C0149R.string.patch_ok), (OnClickListener) new OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                String result = "1";
                                int count = listAppsFragment.patch_adapt.getCount();
                                for (int i = All_Dialogs.CREATE_APK; i < count; i += All_Dialogs.CUSTOM_PATCH) {
                                    Patterns patt = (Patterns) listAppsFragment.patch_adapt.getItem(i);
                                    if (i == 0 && patt.Status) {
                                        result = "pattern1_pattern2_pattern3_pattern5_";
                                    }
                                    if (i == All_Dialogs.CUSTOM_PATCH && patt.Status) {
                                        result = "pattern1_pattern2_pattern3_pattern6_";
                                    }
                                    if (i == All_Dialogs.ADD_BOOT && patt.Status) {
                                        result = result + "pattern4" + "_";
                                    }
                                    if (i == 3 && patt.Status) {
                                        result = result + "amazon" + "_";
                                    }
                                    if (i == 4 && patt.Status) {
                                        result = result + "samsung" + "_";
                                    }
                                    if (i == 5 && patt.Status) {
                                        result = result + "dependencies" + "_";
                                    }
                                    if (i == 6 && patt.Status) {
                                        result = result + "copyDC" + "_";
                                    }
                                    if (i == 7 && patt.Status) {
                                        result = result + "backup" + "_";
                                    }
                                    if (i == 8 && patt.Status) {
                                        result = result + "deleteDC" + "_";
                                    }
                                }
                                listAppsFragment.frag.addIgnoreOdex(listAppsFragment.pli, result);
                            }
                        });
                    }
                    return alertDlg.create();
                case listAppsFragment.Create_PERM_CONTEXT_DIALOG /*21*/:
                    alertDlg = new AlertDlg(listAppsFragment.frag.getContext());
                    if (listAppsFragment.perm_adapt != null) {
                        listAppsFragment.perm_adapt.setNotifyOnChange(true);
                        alertDlg.setAdapterNotClose(true);
                        alertDlg.setAdapter(listAppsFragment.perm_adapt, new C01731());
                        alertDlg.setPositiveButton(Utils.getText(C0149R.string.create_perm_ok), (OnClickListener) new C01742());
                    }
                    return alertDlg.create();
                case listAppsFragment.Safe_PERM_CONTEXT_DIALOG /*22*/:
                    alertDlg = new AlertDlg(listAppsFragment.frag.getContext());
                    if (listAppsFragment.perm_adapt != null) {
                        listAppsFragment.perm_adapt.setNotifyOnChange(true);
                        alertDlg.setAdapterNotClose(true);
                        alertDlg.setAdapter(listAppsFragment.perm_adapt, new C01773());
                        alertDlg.setPositiveButton(Utils.getText(C0149R.string.safe_perm_ok), (OnClickListener) new C01794());
                    }
                    return alertDlg.create();
                case listAppsFragment.CONTEXT_DIALOG_CorePatches /*24*/:
                    LinearLayout d = (LinearLayout) View.inflate(listAppsFragment.frag.getContext(), C0149R.layout.corepatch_layout, null);
                    ListView list = (ListView) d.findViewById(C0149R.id.coreListView);
                    final CheckBox chk = (CheckBox) d.findViewById(C0149R.id.checkBoxDalvik);
                    chk.setChecked(true);
                    chk.setText(Utils.getText(C0149R.string.core_option));
                    chk.setMaxLines(CUSTOM_PATCH);
                    if (Utils.getFileDalvikCache("/system/framework/core.jar") == null || Utils.getCurrentRuntimeValue().equals("ART")) {
                        listAppsFragment.patchOnlyDalvikCore = false;
                        chk.setChecked(false);
                        chk.setEnabled(false);
                    } else {
                        chk.setChecked(true);
                        listAppsFragment.patchOnlyDalvikCore = true;
                    }
                    chk.setOnCheckedChangeListener(new OnCheckedChangeListener() {
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            if (Utils.getCurrentRuntimeValue().equals("ART")) {
                                chk.setChecked(false);
                                chk.setEnabled(false);
                                listAppsFragment.patchOnlyDalvikCore = false;
                                return;
                            }
                            listAppsFragment.patchOnlyDalvikCore = isChecked;
                        }
                    });
                    alertDlg = new AlertDlg(listAppsFragment.frag.getContext());
                    if (listAppsFragment.patch_adapt != null) {
                        listAppsFragment.patch_adapt.setNotifyOnChange(true);
                        list.setAdapter(listAppsFragment.patch_adapt);
                        iArr = new int[3];
                        list.setDivider(new GradientDrawable(Orientation.RIGHT_LEFT, new int[]{-1715492012, -4215980, -1715492012}));
                        list.setDividerHeight(CUSTOM_PATCH);
                        list.setOnItemClickListener(new OnItemClickListener() {
                            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                                CoreItem id1 = (CoreItem) listAppsFragment.patch_adapt.getItem(position);
                                if (id1.Status) {
                                    id1.Status = false;
                                } else {
                                    CoreItem id5 = (CoreItem) listAppsFragment.patch_adapt.getItem(5);
                                    if (!(((CoreItem) listAppsFragment.patch_adapt.getItem(4)).Status || id5.Status || id1.disabled)) {
                                        id1.Status = true;
                                    }
                                }
                                CoreItem id2 = (CoreItem) listAppsFragment.patch_adapt.getItem(All_Dialogs.ADD_BOOT);
                                CoreItem id3 = (CoreItem) listAppsFragment.patch_adapt.getItem(3);
                                if ((position == 0 || position == All_Dialogs.CUSTOM_PATCH) && id1.Status && ((id2.Status || id3.Status) && (!Utils.exists("/system/framework/core.odex") || Utils.getCurrentRuntimeValue().contains("ART")))) {
                                    id3.Status = false;
                                    id2.Status = false;
                                    listAppsFragment.frag.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.patch_to_Android_At_once));
                                }
                                if ((position == All_Dialogs.ADD_BOOT || position == 3) && id1.Status && ((!Utils.exists("/system/framework/core.odex") || Utils.getCurrentRuntimeValue().contains("ART")) && (((CoreItem) listAppsFragment.patch_adapt.getItem(All_Dialogs.CREATE_APK)).Status || ((CoreItem) listAppsFragment.patch_adapt.getItem(All_Dialogs.CUSTOM_PATCH)).Status))) {
                                    ((CoreItem) listAppsFragment.patch_adapt.getItem(All_Dialogs.CREATE_APK)).Status = false;
                                    ((CoreItem) listAppsFragment.patch_adapt.getItem(All_Dialogs.CUSTOM_PATCH)).Status = false;
                                    listAppsFragment.frag.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.patch_to_Android_At_once));
                                }
                                if (position == 4 && id1.Status) {
                                    ((CoreItem) listAppsFragment.patch_adapt.getItem(All_Dialogs.CREATE_APK)).Status = false;
                                    ((CoreItem) listAppsFragment.patch_adapt.getItem(All_Dialogs.CUSTOM_PATCH)).Status = false;
                                    ((CoreItem) listAppsFragment.patch_adapt.getItem(All_Dialogs.ADD_BOOT)).Status = false;
                                    ((CoreItem) listAppsFragment.patch_adapt.getItem(3)).Status = false;
                                }
                                if (position == 5 && id1.Status) {
                                    ((CoreItem) listAppsFragment.patch_adapt.getItem(All_Dialogs.CREATE_APK)).Status = false;
                                    ((CoreItem) listAppsFragment.patch_adapt.getItem(All_Dialogs.CUSTOM_PATCH)).Status = false;
                                    ((CoreItem) listAppsFragment.patch_adapt.getItem(All_Dialogs.ADD_BOOT)).Status = false;
                                    ((CoreItem) listAppsFragment.patch_adapt.getItem(3)).Status = false;
                                }
                                listAppsFragment.patch_adapt.notifyDataSetChanged();
                            }
                        });
                        alertDlg.setView(d);
                        alertDlg.setPositiveButton(Utils.getText(C0149R.string.patch_ok), (OnClickListener) new OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                String result = "patch";
                                int count = listAppsFragment.patch_adapt.getCount();
                                boolean a = false;
                                boolean b = false;
                                boolean c = false;
                                boolean d = false;
                                for (int i = All_Dialogs.CREATE_APK; i < count; i += All_Dialogs.CUSTOM_PATCH) {
                                    CoreItem patt = (CoreItem) listAppsFragment.patch_adapt.getItem(i);
                                    if (i == 0 && patt.Status) {
                                        result = result + "_patch1";
                                        a = true;
                                    }
                                    if (i == All_Dialogs.CUSTOM_PATCH && patt.Status) {
                                        result = result + "_patch2";
                                        b = true;
                                    }
                                    if (i == All_Dialogs.ADD_BOOT && patt.Status) {
                                        result = result + "_patch3";
                                        c = true;
                                    }
                                    if (i == 3 && patt.Status) {
                                        result = result + "_patch4";
                                        c = true;
                                    }
                                    if (i == 4 && patt.Status) {
                                        result = "restoreCore";
                                        d = true;
                                    }
                                    if (i == 5 && patt.Status) {
                                        if (result.contains("restoreCore")) {
                                            result = result + "_restoreServices";
                                        } else {
                                            result = "restoreServices";
                                        }
                                        d = true;
                                    }
                                }
                                if (a || b || c || d) {
                                    listAppsFragment.frag.corepatch(result);
                                }
                            }
                        });
                    }
                    return alertDlg.create();
                case listAppsFragment.Safe_Signature_PERM_CONTEXT_DIALOG /*25*/:
                    alertDlg = new AlertDlg(listAppsFragment.frag.getContext());
                    if (listAppsFragment.perm_adapt != null) {
                        listAppsFragment.perm_adapt.setNotifyOnChange(true);
                        alertDlg.setAdapterNotClose(true);
                        alertDlg.setAdapter(listAppsFragment.perm_adapt, new C01849());
                        alertDlg.setPositiveButton(Utils.getText(C0149R.string.safe_perm_ok), (OnClickListener) new OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                ArrayList<String> permiss = new ArrayList();
                                ArrayList<String> activ = new ArrayList();
                                for (int i = All_Dialogs.CREATE_APK; i < listAppsFragment.perm_adapt.getCount(); i += All_Dialogs.CUSTOM_PATCH) {
                                    Perm id1 = (Perm) listAppsFragment.perm_adapt.getItem(i);
                                    if (!(id1.Status || id1.Name.contains("chelpus_"))) {
                                        permiss.add(id1.Name);
                                    }
                                    if (id1.Status && id1.Name.startsWith("chelpa_per_")) {
                                        permiss.add(id1.Name.replace("chelpa_per_", BuildConfig.VERSION_NAME));
                                        System.out.println(id1.Name.replace("chelpa_per_", BuildConfig.VERSION_NAME));
                                    }
                                    if (!(id1.Status || !id1.Name.contains("chelpus_") || id1.Name.contains("disabled_"))) {
                                        activ.add(id1.Name.replace("chelpus_", BuildConfig.VERSION_NAME));
                                        System.out.println(BuildConfig.VERSION_NAME + id1.Name.replace("chelpus_", BuildConfig.VERSION_NAME));
                                    }
                                    if (id1.Status && id1.Name.contains("chelpus_disabled_")) {
                                        activ.add(id1.Name.replace("chelpus_", BuildConfig.VERSION_NAME));
                                        System.out.println(BuildConfig.VERSION_NAME + id1.Name.replace("chelpus_", BuildConfig.VERSION_NAME));
                                    }
                                }
                                listAppsFragment.frag.apkpermissions_safe(listAppsFragment.pli, permiss, activ);
                            }
                        });
                    }
                    return alertDlg.create();
                case listAppsFragment.BACKUP_SELECTOR /*26*/:
                    AlertDlg backup_builder = new AlertDlg(listAppsFragment.frag.getContext());
                    if (listAppsFragment.adapt != null) {
                        listAppsFragment.adapt.setNotifyOnChange(true);
                        backup_builder.setAdapter(listAppsFragment.adapt, new OnItemClickListener() {
                            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                                listAppsFragment com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment = listAppsFragment.frag;
                                listAppsFragment.removeDialogLP(26);
                                listAppsFragment.frag.restore(listAppsFragment.pli, (File) listAppsFragment.adapt.getItem(position));
                            }
                        });
                    }
                    return backup_builder.create();
                case patchActivity.RESTORE_FROM_BACKUP /*28*/:
                    if (listAppsFragment.adapt != null) {
                        listAppsFragment.adapt.setNotifyOnChange(true);
                    }
                    alertDlg = new AlertDlg(listAppsFragment.frag.getContext(), true);
                    alertDlg.setAdapter(listAppsFragment.adapt, new OnItemClickListener() {
                        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                            listAppsFragment com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment = listAppsFragment.frag;
                            listAppsFragment.removeDialogLP(28);
                            listAppsFragment.frag.toolbar_restore((FileApkListItem) listAppsFragment.adapt.getItem(position), false);
                        }
                    });
                    alertDlg.setCancelable(true);
                    alertDlg.setOnCancelListener(new OnCancelListener() {
                        public void onCancel(DialogInterface dialog) {
                        }
                    });
                    return alertDlg.create();
                case listAppsFragment.Disable_Activity_CONTEXT_DIALOG /*29*/:
                    AlertDlg ads1builder6 = new AlertDlg(listAppsFragment.frag.getContext());
                    if (listAppsFragment.perm_adapt != null) {
                        listAppsFragment.perm_adapt.setNotifyOnChange(true);
                        ads1builder6.setAdapterNotClose(true);
                        ads1builder6.setAdapter(listAppsFragment.perm_adapt, new C01805());
                        ads1builder6.setPositiveButton(Utils.getText(C0149R.string.launchbutton), (OnClickListener) new C01816());
                    }
                    return ads1builder6.create();
                case listAppsFragment.Disable_COMPONENTS_CONTEXT_DIALOG /*31*/:
                    alertDlg = new AlertDlg(listAppsFragment.frag.getContext());
                    if (listAppsFragment.component_adapt != null) {
                        listAppsFragment.component_adapt.setNotifyOnChange(true);
                        alertDlg.setAdapterNotClose(true);
                        alertDlg.setAdapter(listAppsFragment.component_adapt, new C01827());
                        alertDlg.setPositiveButton(Utils.getText(C0149R.string.launchbutton), (OnClickListener) new C01838());
                    }
                    return alertDlg.create();
                case listAppsFragment.SUPPORT_PATCH_CONTEXT_DIALOG /*34*/:
                    alertDlg = new AlertDlg(listAppsFragment.frag.getContext());
                    if (listAppsFragment.patch_adapt != null) {
                        listAppsFragment.patch_adapt.setNotifyOnChange(true);
                        alertDlg.setAdapterNotClose(true);
                        alertDlg.setAdapter(listAppsFragment.patch_adapt, new OnItemClickListener() {
                            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                                Patterns id1 = (Patterns) listAppsFragment.patch_adapt.getItem(position);
                                if (id1.Status) {
                                    id1.Status = false;
                                } else {
                                    id1.Status = true;
                                }
                                if (position == 3 && id1.Status) {
                                    ((Patterns) listAppsFragment.patch_adapt.getItem(4)).Status = true;
                                    id1 = (Patterns) listAppsFragment.patch_adapt.getItem(5);
                                    id1.Status = false;
                                }
                                if (position == 5 && id1.Status) {
                                    ((Patterns) listAppsFragment.patch_adapt.getItem(4)).Status = true;
                                    ((Patterns) listAppsFragment.patch_adapt.getItem(3)).Status = false;
                                }
                                listAppsFragment.patch_adapt.notifyDataSetChanged();
                            }
                        });
                        alertDlg.setPositiveButton(Utils.getText(C0149R.string.patch_ok), (OnClickListener) new OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                String result = "1";
                                int count = listAppsFragment.patch_adapt.getCount();
                                int i = All_Dialogs.CREATE_APK;
                                while (i < count) {
                                    Patterns patt = (Patterns) listAppsFragment.patch_adapt.getItem(i);
                                    if (patt.Status && i < 3) {
                                        result = result + "pattern" + i + "_";
                                    }
                                    if (i == 3 && patt.Status) {
                                        result = result + "copyDC" + "_";
                                    }
                                    if (i == 4 && patt.Status) {
                                        result = result + "backup" + "_";
                                    }
                                    if (i == 5 && patt.Status) {
                                        result = result + "deleteDC" + "_";
                                    }
                                    i += All_Dialogs.CUSTOM_PATCH;
                                }
                                listAppsFragment.frag.support(listAppsFragment.pli, result);
                            }
                        });
                    }
                    return alertDlg.create();
                case listAppsFragment.create_SUPPORT_PATCH_CONTEXT_DIALOG /*35*/:
                    alertDlg = new AlertDlg(listAppsFragment.frag.getContext());
                    if (listAppsFragment.patch_adapt != null) {
                        listAppsFragment.patch_adapt.setNotifyOnChange(true);
                        alertDlg.setAdapterNotClose(true);
                        alertDlg.setAdapter(listAppsFragment.patch_adapt, new OnItemClickListener() {
                            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                                Patterns id1 = (Patterns) listAppsFragment.patch_adapt.getItem(position);
                                if (id1.Status) {
                                    id1.Status = false;
                                } else {
                                    id1.Status = true;
                                }
                                listAppsFragment.patch_adapt.notifyDataSetChanged();
                            }
                        });
                        alertDlg.setPositiveButton(Utils.getText(C0149R.string.create_perm_ok), (OnClickListener) new OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                String result = "1";
                                int count = listAppsFragment.patch_adapt.getCount();
                                int i = All_Dialogs.CREATE_APK;
                                while (i < count) {
                                    if (((Patterns) listAppsFragment.patch_adapt.getItem(i)).Status && i < 3) {
                                        result = result + "pattern" + i + "_";
                                    }
                                    i += All_Dialogs.CUSTOM_PATCH;
                                }
                                if (listAppsFragment.rebuldApk.equals(BuildConfig.VERSION_NAME)) {
                                    listAppsFragment.frag.createapksupport(result);
                                } else {
                                    listAppsFragment.frag.toolbar_createapksupport(result);
                                }
                            }
                        });
                    }
                    return alertDlg.create();
                case listAppsFragment.DALVIK_CORE_PATCH /*37*/:
                    int color;
                    LinearLayout body = (LinearLayout) View.inflate(listAppsFragment.frag.getContext(), C0149R.layout.core_patch_result, null);
                    TextView txt = (TextView) body.findViewById(C0149R.id.textResults);
                    final CheckBox chk_b = (CheckBox) body.findViewById(C0149R.id.CheckBox1);
                    String apply_text = BuildConfig.VERSION_NAME;
                    txt.setText(Utils.getText(C0149R.string.core_only_dalvik_cache_finished));
                    if (listAppsFragment.result_core_patch.contains("_patch1")) {
                        int i = CREATE_APK;
                        if (listAppsFragment.str.contains("Core patched!") || listAppsFragment.str.contains("Core 2 patched!")) {
                            apply_text = Utils.getText(C0149R.string.core_patch_apply);
                            if (listAppsFragment.str.contains("Core patched!")) {
                                i = CREATE_APK + CUSTOM_PATCH;
                            }
                            if (listAppsFragment.str.contains("Core 2 patched!")) {
                                i += CUSTOM_PATCH;
                            }
                            color = -16711936;
                        } else {
                            apply_text = Utils.getText(C0149R.string.core_patch_nopatch);
                            color = SupportMenu.CATEGORY_MASK;
                        }
                        txt.append(Utils.getText(C0149R.string.contextcorepatch1) + " " + i + "/2 ");
                        txt.append(Utils.getColoredText(apply_text + LogCollector.LINE_SEPARATOR, color, BuildConfig.VERSION_NAME));
                    }
                    if (listAppsFragment.result_core_patch.contains("_patch2")) {
                        if (listAppsFragment.str.contains("Core unsigned install patched!")) {
                            apply_text = Utils.getText(C0149R.string.core_patch_apply);
                            color = -16711936;
                        } else {
                            apply_text = Utils.getText(C0149R.string.core_patch_nopatch);
                            color = SupportMenu.CATEGORY_MASK;
                        }
                        txt.append(Utils.getText(C0149R.string.contextcorepatch2) + " ");
                        txt.append(Utils.getColoredText(apply_text + LogCollector.LINE_SEPARATOR, color, BuildConfig.VERSION_NAME));
                    }
                    if (listAppsFragment.result_core_patch.contains("_patch3")) {
                        if (listAppsFragment.str.contains("Core4 patched!")) {
                            apply_text = Utils.getText(C0149R.string.core_patch_apply);
                            color = -16711936;
                        } else {
                            apply_text = Utils.getText(C0149R.string.core_patch_apply);
                            color = SupportMenu.CATEGORY_MASK;
                        }
                        txt.append(Utils.getText(C0149R.string.contextcorepatch3) + " ");
                        txt.append(Utils.getColoredText(apply_text + LogCollector.LINE_SEPARATOR, color, BuildConfig.VERSION_NAME));
                    }
                    if (listAppsFragment.result_core_patch.contains("restore")) {
                        chk_b.setChecked(false);
                        chk_b.setEnabled(false);
                        Utils.turn_off_patch_on_boot_all();
                        if (listAppsFragment.str.contains("Core restored!") || listAppsFragment.str.contains("Core 2 restored!")) {
                            apply_text = Utils.getText(C0149R.string.restored);
                            txt.append(Utils.getText(C0149R.string.contextcorepatch1) + " ");
                            txt.append(Utils.getColoredText(apply_text + LogCollector.LINE_SEPARATOR, -16711936, BuildConfig.VERSION_NAME));
                        }
                        if (listAppsFragment.str.contains("Core unsigned install restored!")) {
                            apply_text = Utils.getText(C0149R.string.restored);
                            txt.append(Utils.getText(C0149R.string.contextcorepatch2) + " ");
                            txt.append(Utils.getColoredText(apply_text + LogCollector.LINE_SEPARATOR, -16711936, BuildConfig.VERSION_NAME));
                        }
                        if (listAppsFragment.str.contains("Core4 restored!")) {
                            apply_text = Utils.getText(C0149R.string.restored);
                            txt.append(Utils.getText(C0149R.string.contextcorepatch3) + " ");
                            txt.append(Utils.getColoredText(apply_text + LogCollector.LINE_SEPARATOR, -16711936, BuildConfig.VERSION_NAME));
                        }
                    }
                    txt.append(LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.core_only_dalvik_cache_end));
                    chk_b.setChecked(false);
                    chk_b.setText(Utils.getText(C0149R.string.core_result_checkbox));
                    if (chk_b.isChecked()) {
                        Utils.turn_on_patch_on_boot(listAppsFragment.result_core_patch);
                    } else {
                        Utils.turn_off_patch_on_boot(listAppsFragment.result_core_patch);
                    }
                    chk_b.setOnCheckedChangeListener(new OnCheckedChangeListener() {
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            if (isChecked) {
                                Utils.turn_on_patch_on_boot(listAppsFragment.result_core_patch);
                            } else {
                                Utils.turn_off_patch_on_boot(listAppsFragment.result_core_patch);
                            }
                        }
                    });
                    alertDlg = new AlertDlg(listAppsFragment.frag.getContext());
                    alertDlg.setView(body);
                    alertDlg.setPositiveButton(Utils.getText(C0149R.string.ok), (OnClickListener) new OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            if (chk_b.isChecked()) {
                                Utils.turn_on_patch_on_boot(listAppsFragment.result_core_patch);
                            } else {
                                Utils.turn_off_patch_on_boot(listAppsFragment.result_core_patch);
                            }
                        }
                    }).setTitle((int) C0149R.string.information);
                    return alertDlg.create();
                case listAppsFragment.CONTEXT_DIALOG_Xposed_Patches /*39*/:
                    View d_x = (LinearLayout) View.inflate(listAppsFragment.frag.getContext(), C0149R.layout.corepatch_layout, null);
                    CheckBox chk_x = (CheckBox) d_x.findViewById(C0149R.id.checkBoxDalvik);
                    chk_x.setChecked(Utils.readXposedParamBoolean().getBoolean("module_on"));
                    if (Utils.isXposedEnabled()) {
                        chk_x.setEnabled(true);
                        chk_x.setText(Utils.getText(C0149R.string.xposed_module_option));
                    } else {
                        chk_x.setEnabled(false);
                        chk_x.setChecked(false);
                        chk_x.setText(Utils.getText(C0149R.string.xposed_option_off));
                    }
                    chk_x.setMaxLines(ADD_BOOT);
                    AlertDlg builder_x = new AlertDlg(listAppsFragment.frag.getContext());
                    if (listAppsFragment.patch_adapt != null) {
                        listAppsFragment.patch_adapt.setNotifyOnChange(true);
                        ListView list_x = (ListView) d_x.findViewById(C0149R.id.coreListView);
                        list_x.setAdapter(listAppsFragment.patch_adapt);
                        iArr = new int[3];
                        list_x.setDivider(new GradientDrawable(Orientation.RIGHT_LEFT, new int[]{-1715492012, -4215980, -1715492012}));
                        list_x.setDividerHeight(CUSTOM_PATCH);
                        if (Utils.isXposedEnabled()) {
                            list_x.setOnItemClickListener(new OnItemClickListener() {
                                public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                                    CoreItem id1 = (CoreItem) listAppsFragment.patch_adapt.getItem(position);
                                    System.out.println(BuildConfig.VERSION_NAME + id1.Status);
                                    if (id1.Status) {
                                        id1.Status = false;
                                    } else {
                                        id1.Status = true;
                                    }
                                    System.out.println(BuildConfig.VERSION_NAME + ((CoreItem) listAppsFragment.patch_adapt.getItem(position)).Status);
                                    listAppsFragment.patch_adapt.notifyDataSetChanged();
                                }
                            });
                        }
                        builder_x.setView(d_x);
                        if (Utils.isXposedEnabled()) {
                            final CheckBox checkBox = chk_x;
                            builder_x.setPositiveButton(Utils.getText(C0149R.string.usepatch), (OnClickListener) new OnClickListener() {

                                class C01761 implements Runnable {
                                    C01761() {
                                    }

                                    public void run() {
                                        int count = listAppsFragment.patch_adapt.getCount();
                                        final JSONObject settings = new JSONObject();
                                        for (int i = All_Dialogs.CREATE_APK; i < count; i += All_Dialogs.CUSTOM_PATCH) {
                                            CoreItem patt = (CoreItem) listAppsFragment.patch_adapt.getItem(i);
                                            System.out.println(BuildConfig.VERSION_NAME + patt.Status);
                                            if (i == 0) {
                                                try {
                                                    settings.put("patch1", patt.Status);
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                            if (i == All_Dialogs.CUSTOM_PATCH) {
                                                try {
                                                    settings.put("patch2", patt.Status);
                                                } catch (JSONException e2) {
                                                    e2.printStackTrace();
                                                }
                                            }
                                            if (i == All_Dialogs.ADD_BOOT) {
                                                try {
                                                    settings.put("patch3", patt.Status);
                                                } catch (JSONException e22) {
                                                    e22.printStackTrace();
                                                }
                                            }
                                            if (i == 3) {
                                                try {
                                                    PackageInfo pi = Utils.getPkgInfo(Common.GOOGLEPLAY_PKG, All_Dialogs.CREATE_APK);
                                                    if (pi != null) {
                                                        long lenght = 0;
                                                        try {
                                                            lenght = new File(Utils.getPlaceForOdex(pi.applicationInfo.sourceDir, false)).length();
                                                        } catch (Exception e3) {
                                                        }
                                                        if (lenght <= 1048576 && lenght != 0) {
                                                            try {
                                                                Utils utils = new Utils(BuildConfig.VERSION_NAME);
                                                                String[] strArr = new String[All_Dialogs.CUSTOM_PATCH];
                                                                strArr[All_Dialogs.CREATE_APK] = listAppsFragment.dalvikruncommand + ".pinfo " + pi.applicationInfo.sourceDir + " " + listAppsFragment.toolfilesdir + " " + String.valueOf(pi.applicationInfo.uid) + " recovery";
                                                                utils.cmdRoot(strArr);
                                                                Utils.market_billing_services(true);
                                                                Utils.market_licensing_services(true);
                                                                Utils.kill(Common.GOOGLEPLAY_PKG);
                                                            } catch (Exception e4) {
                                                                e4.printStackTrace();
                                                            }
                                                        }
                                                    }
                                                    settings.put("patch4", patt.Status);
                                                } catch (JSONException e222) {
                                                    e222.printStackTrace();
                                                }
                                            }
                                            if (i == 4) {
                                                try {
                                                    settings.put("hide", patt.Status);
                                                } catch (JSONException e2222) {
                                                    e2222.printStackTrace();
                                                }
                                            }
                                        }
                                        try {
                                            settings.put("module_on", checkBox.isChecked());
                                        } catch (JSONException e22222) {
                                            e22222.printStackTrace();
                                        }
                                        new Thread(new Runnable() {
                                            public void run() {
                                                System.out.println("write:" + settings.toString());
                                                String enc = Base64.encode(settings.toString().getBytes());
                                                Utils utils = new Utils(BuildConfig.VERSION_NAME);
                                                String[] strArr = new String[All_Dialogs.CUSTOM_PATCH];
                                                strArr[All_Dialogs.CREATE_APK] = listAppsFragment.dalvikruncommand + ".WriteSettingsXposed " + enc;
                                                utils.cmdRoot(strArr);
                                            }
                                        }).start();
                                    }
                                }

                                public void onClick(DialogInterface dialog, int which) {
                                    new Thread(new C01761()).start();
                                }
                            });
                        } else {
                            builder_x.setPositiveButton(Utils.getText(C0149R.string.ok), null);
                        }
                    }
                    return builder_x.create();
                case listAppsFragment.DIALOG_PROGRESS_COLLECTING_LOG /*3255*/:
                    Dialog progressDialog = new ProgressDialog(listAppsFragment.frag.getContext());
                    progressDialog.setTitle("Progress");
                    progressDialog.setMessage(Utils.getText(C0149R.string.collect_logs));
                    progressDialog.setIndeterminate(true);
                    return progressDialog;
                case listAppsFragment.DIALOG_SEND_LOG /*345350*/:
                case patchActivity.DIALOG_REPORT_FORCE_CLOSE /*3535788*/:
                    String message;
                    alertDlg = new AlertDlg(listAppsFragment.frag.getContext());
                    if (listAppsFragment.dialog_int == 345350) {
                        message = Utils.getText(C0149R.string.send_logs);
                    } else {
                        message = Utils.getText(C0149R.string.error_detect);
                    }
                    alertDlg.setTitle(Utils.getText(C0149R.string.warning)).setIcon(C0149R.drawable.ic_bolen).setMessage(message).setPositiveButton(Utils.getText(C0149R.string.Yes), new OnClickListener() {

                        class C01781 extends AsyncTask<Void, Void, Boolean> {
                            C01781() {
                            }

                            protected Boolean doInBackground(Void... params) {
                                return Boolean.valueOf(listAppsFragment.mLogCollector.collect(listAppsFragment.frag.getContext(), true));
                            }

                            protected void onPreExecute() {
                                listAppsFragment com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment = listAppsFragment.frag;
                                listAppsFragment.showDialogLP(listAppsFragment.DIALOG_PROGRESS_COLLECTING_LOG);
                            }

                            protected void onPostExecute(Boolean result) {
                                listAppsFragment com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment = listAppsFragment.frag;
                                listAppsFragment.removeDialogLP(listAppsFragment.DIALOG_PROGRESS_COLLECTING_LOG);
                                if (result.booleanValue()) {
                                    try {
                                        listAppsFragment.mLogCollector.sendLog(listAppsFragment.frag.getContext(), "lp.chelpus@gmail.com", "Error Log", "Lucky Patcher " + listAppsFragment.getPkgMng().getPackageInfo(listAppsFragment.frag.getContext().getPackageName(), All_Dialogs.CREATE_APK).versionName);
                                        return;
                                    } catch (NameNotFoundException e) {
                                        e.printStackTrace();
                                        return;
                                    }
                                }
                                com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment = listAppsFragment.frag;
                                listAppsFragment.showDialogLP(listAppsFragment.DIALOG_FAILED_TO_COLLECT_LOGS);
                            }
                        }

                        public void onClick(DialogInterface dialog, int which) {
                            new C01781().execute(new Void[All_Dialogs.CREATE_APK]);
                        }
                    }).setNegativeButton(Utils.getText(C0149R.string.no), new OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    return alertDlg.create();
                case listAppsFragment.DIALOG_FAILED_TO_COLLECT_LOGS /*3535122*/:
                    alertDlg = new AlertDlg(listAppsFragment.frag.getContext());
                    alertDlg.setTitle("Error").setMessage(Utils.getText(C0149R.string.error_collect_logs)).setNegativeButton("OK", null);
                    return alertDlg.create();
                default:
                    return null;
            }
        } catch (Exception e2) {
            System.out.println("LuckyPatcher (Create Dialog): " + e2);
            e2.printStackTrace();
            alertDlg = new AlertDlg(listAppsFragment.frag.getContext());
            alertDlg.setTitle("Error").setMessage("Sorry...\nShow Dialog - Error...").setNegativeButton("OK", null);
            return alertDlg.create();
        }
        System.out.println("LuckyPatcher (Create Dialog): " + e2);
        e2.printStackTrace();
        alertDlg = new AlertDlg(listAppsFragment.frag.getContext());
        alertDlg.setTitle("Error").setMessage("Sorry...\nShow Dialog - Error...").setNegativeButton("OK", null);
        return alertDlg.create();
    }
}
