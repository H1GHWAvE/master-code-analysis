package com.android.vending.billing.InAppBillingService.LUCK;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import com.chelpus.Utils;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import pxb.android.axml.AxmlParser;

public class MenuItemAdapter extends BaseExpandableListAdapter {
    public static final int TEXT_DEFAULT = 0;
    public static final int TEXT_LARGE = 2;
    public static final int TEXT_MEDIUM = 1;
    public static final int TEXT_SMALL = 0;
    public Context context;
    public MenuItem[] groups;
    public ArrayList<View> groupsViews;
    private ImageView imgIcon;
    private int size;
    public Comparator<PkgListItem> sorter;

    class C00971 implements OnCheckedChangeListener {
        C00971() {
        }

        public void onCheckedChanged(RadioGroup group, int checkedId) {
            switch (checkedId) {
                case C0149R.id.radioSmall:
                    listAppsFragment.getConfig().edit().putInt(listAppsFragment.SETTINGS_VIEWSIZE, MenuItemAdapter.TEXT_DEFAULT).commit();
                    listAppsFragment.getConfig().edit().putBoolean("settings_change", true).commit();
                    listAppsFragment.runResume = true;
                    return;
                case C0149R.id.radioMedium:
                    listAppsFragment.getConfig().edit().putInt(listAppsFragment.SETTINGS_VIEWSIZE, MenuItemAdapter.TEXT_MEDIUM).commit();
                    listAppsFragment.getConfig().edit().putBoolean("settings_change", true).commit();
                    listAppsFragment.runResume = true;
                    return;
                case C0149R.id.radioLarge:
                    listAppsFragment.getConfig().edit().putInt(listAppsFragment.SETTINGS_VIEWSIZE, MenuItemAdapter.TEXT_LARGE).commit();
                    listAppsFragment.getConfig().edit().putBoolean("settings_change", true).commit();
                    listAppsFragment.runResume = true;
                    return;
                default:
                    return;
            }
        }
    }

    class C00982 implements OnCheckedChangeListener {
        C00982() {
        }

        public void onCheckedChanged(RadioGroup group, int checkedId) {
            switch (checkedId) {
                case C0149R.id.radioSensor:
                    listAppsFragment.getConfig().edit().putInt("orientstion", 3).commit();
                    listAppsFragment.getConfig().edit().putBoolean("settings_change", true).commit();
                    listAppsFragment.runResume = true;
                    return;
                case C0149R.id.radioPortret:
                    listAppsFragment.getConfig().edit().putInt("orientstion", MenuItemAdapter.TEXT_LARGE).commit();
                    listAppsFragment.getConfig().edit().putBoolean("settings_change", true).commit();
                    listAppsFragment.runResume = true;
                    return;
                case C0149R.id.radioLandscape:
                    listAppsFragment.getConfig().edit().putInt("orientstion", MenuItemAdapter.TEXT_MEDIUM).commit();
                    listAppsFragment.getConfig().edit().putBoolean("settings_change", true).commit();
                    listAppsFragment.runResume = true;
                    return;
                default:
                    return;
            }
        }
    }

    class C00993 implements OnCheckedChangeListener {
        C00993() {
        }

        public void onCheckedChanged(RadioGroup group, int checkedId) {
            switch (checkedId) {
                case C0149R.id.radioName:
                    listAppsFragment.getConfig().edit().putInt("sortby", MenuItemAdapter.TEXT_MEDIUM).commit();
                    listAppsFragment.getConfig().edit().putBoolean("settings_change", true).commit();
                    listAppsFragment.runResume = true;
                    return;
                case C0149R.id.radioStatus:
                    listAppsFragment.getConfig().edit().putInt("sortby", MenuItemAdapter.TEXT_LARGE).commit();
                    listAppsFragment.getConfig().edit().putBoolean("settings_change", true).commit();
                    listAppsFragment.runResume = true;
                    return;
                case C0149R.id.radioTime:
                    listAppsFragment.getConfig().edit().putInt("sortby", 3).commit();
                    listAppsFragment.getConfig().edit().putBoolean("settings_change", true).commit();
                    listAppsFragment.runResume = true;
                    return;
                default:
                    return;
            }
        }
    }

    class C01004 implements CompoundButton.OnCheckedChangeListener {
        C01004() {
        }

        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            listAppsFragment.getConfig().edit().putBoolean("lvlapp", isChecked).commit();
            listAppsFragment.getConfig().edit().putBoolean("settings_change", true).commit();
            listAppsFragment.getConfig().edit().putBoolean("lang_change", true).commit();
            listAppsFragment.runResume = true;
        }
    }

    class C01015 implements CompoundButton.OnCheckedChangeListener {
        C01015() {
        }

        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            listAppsFragment.getConfig().edit().putBoolean("adsapp", isChecked).commit();
            listAppsFragment.getConfig().edit().putBoolean("settings_change", true).commit();
            listAppsFragment.getConfig().edit().putBoolean("lang_change", true).commit();
            listAppsFragment.runResume = true;
        }
    }

    class C01026 implements CompoundButton.OnCheckedChangeListener {
        C01026() {
        }

        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            listAppsFragment.getConfig().edit().putBoolean("customapp", isChecked).commit();
            listAppsFragment.getConfig().edit().putBoolean("settings_change", true).commit();
            listAppsFragment.getConfig().edit().putBoolean("lang_change", true).commit();
            listAppsFragment.runResume = true;
        }
    }

    class C01037 implements CompoundButton.OnCheckedChangeListener {
        C01037() {
        }

        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            listAppsFragment.getConfig().edit().putBoolean("modifapp", isChecked).commit();
            listAppsFragment.getConfig().edit().putBoolean("settings_change", true).commit();
            listAppsFragment.getConfig().edit().putBoolean("lang_change", true).commit();
            listAppsFragment.runResume = true;
        }
    }

    class C01048 implements CompoundButton.OnCheckedChangeListener {
        C01048() {
        }

        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            listAppsFragment.getConfig().edit().putBoolean("fixedapp", isChecked).commit();
            listAppsFragment.getConfig().edit().putBoolean("settings_change", true).commit();
            listAppsFragment.getConfig().edit().putBoolean("lang_change", true).commit();
            listAppsFragment.runResume = true;
        }
    }

    class C01059 implements CompoundButton.OnCheckedChangeListener {
        C01059() {
        }

        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            listAppsFragment.getConfig().edit().putBoolean("noneapp", isChecked).commit();
            listAppsFragment.getConfig().edit().putBoolean("settings_change", true).commit();
            listAppsFragment.getConfig().edit().putBoolean("lang_change", true).commit();
            listAppsFragment.runResume = true;
        }
    }

    public MenuItemAdapter(Context context, List<MenuItem> groups) {
        this.groups = new MenuItem[TEXT_DEFAULT];
        this.context = null;
        this.groupsViews = null;
        this.context = context;
        this.size = TEXT_DEFAULT;
        this.groups = (MenuItem[]) groups.toArray(new MenuItem[groups.size()]);
        listAppsFragment.menu_adapter = this;
    }

    public MenuItemAdapter(Context context, int sizeText, List<MenuItem> groups) {
        this.groups = new MenuItem[TEXT_DEFAULT];
        this.context = null;
        this.groupsViews = null;
        this.context = context;
        this.size = sizeText;
        this.groups = (MenuItem[]) groups.toArray(new MenuItem[groups.size()]);
        listAppsFragment.menu_adapter = this;
    }

    public void add(ArrayList<MenuItem> groupsIn) {
        this.groups = (MenuItem[]) groupsIn.toArray(new MenuItem[groupsIn.size()]);
        notifyDataSetChanged();
    }

    public int getGroupCount() {
        if (this.groups == null || this.groups.length == 0) {
            return TEXT_DEFAULT;
        }
        return this.groups.length;
    }

    public int getChildrenCount(int groupPosition) {
        if (this.groups == null || this.groups.length == 0) {
            return TEXT_DEFAULT;
        }
        return this.groups[groupPosition].childs.size();
    }

    public MenuItem getGroup(int groupPosition) {
        return this.groups[groupPosition];
    }

    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    public boolean hasStableIds() {
        return false;
    }

    public void onGroupCollapsed(int groupPosition) {
        super.onGroupCollapsed(groupPosition);
    }

    public void onGroupExpanded(int groupPosition) {
        super.onGroupExpanded(groupPosition);
    }

    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        MenuItem menuItem = getGroup(groupPosition);
        int group = menuItem.punkt_menu;
        try {
            convertView = (View) this.groupsViews.get(groupPosition);
        } catch (Exception e) {
            convertView = null;
        }
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) this.context.getSystemService("layout_inflater");
            switch (menuItem.type) {
                case AxmlParser.END_TAG /*3*/:
                    convertView = inflater.inflate(C0149R.layout.group_check_view, null);
                    break;
                default:
                    convertView = inflater.inflate(C0149R.layout.group_view, null);
                    break;
            }
            if (this.groupsViews == null) {
                this.groupsViews = new ArrayList(this.size);
                MenuItem[] menuItemArr = this.groups;
                int length = menuItemArr.length;
                for (int i = TEXT_DEFAULT; i < length; i += TEXT_MEDIUM) {
                    MenuItem it = menuItemArr[i];
                    this.groupsViews.add(null);
                }
            }
        }
        convertView.setClickable(false);
        switch (menuItem.type) {
            case TEXT_DEFAULT /*0*/:
                TextView textGroup = (TextView) convertView.findViewById(C0149R.id.textGroup);
                textGroup.setTextAppearance(this.context, listAppsFragment.getSizeText());
                textGroup.setTextAppearance(this.context, listAppsFragment.getSizeText());
                textGroup.setText(Utils.getText(group));
                textGroup.setTypeface(textGroup.getTypeface(), TEXT_MEDIUM);
                ImageView icon = (ImageView) convertView.findViewById(C0149R.id.group_image);
                icon.setImageDrawable(getImage(group));
                icon.setVisibility(TEXT_DEFAULT);
                int color = getColor(group);
                textGroup.setTextColor(color);
                icon.setColorFilter(color, Mode.MULTIPLY);
                break;
            case TEXT_MEDIUM /*1*/:
                ((LinearLayout) convertView.findViewById(C0149R.id.parentViewGroup)).setPadding(10, 5, 50, 5);
                TextView textGroup2 = (TextView) convertView.findViewById(C0149R.id.textGroup);
                textGroup2.setTextAppearance(this.context, listAppsFragment.getSizeText());
                textGroup2.setTextAppearance(this.context, listAppsFragment.getSizeText());
                textGroup2.setTextColor(Color.parseColor("#000000"));
                textGroup2.setTypeface(textGroup2.getTypeface(), TEXT_MEDIUM);
                ((ImageView) convertView.findViewById(C0149R.id.group_image)).setVisibility(8);
                convertView.setBackgroundColor(Color.parseColor("#9F9F9F"));
                textGroup2.setText(Utils.getText(group));
                break;
            case TEXT_LARGE /*2*/:
                TextView textGroup3 = (TextView) convertView.findViewById(C0149R.id.textGroup);
                textGroup3.setTextAppearance(this.context, listAppsFragment.getSizeText());
                textGroup3.setTextAppearance(this.context, listAppsFragment.getSizeText());
                textGroup3.setText(Utils.getText(group));
                textGroup3.setTypeface(textGroup3.getTypeface(), TEXT_MEDIUM);
                textGroup3.setTextColor(Color.parseColor("#feeb9c"));
                ImageView icon3 = (ImageView) convertView.findViewById(C0149R.id.group_image);
                icon3.setImageDrawable(getImage(group));
                icon3.setVisibility(TEXT_DEFAULT);
                break;
            case AxmlParser.END_TAG /*3*/:
                TextView textGroup4 = (TextView) convertView.findViewById(C0149R.id.textGroup);
                textGroup4.setTextAppearance(this.context, listAppsFragment.getSizeText());
                textGroup4.setTextAppearance(this.context, listAppsFragment.getSizeText());
                textGroup4.setText(Utils.getText(group));
                textGroup4.setTypeface(textGroup4.getTypeface(), TEXT_MEDIUM);
                textGroup4.setTextColor(Color.parseColor("#feeb9c"));
                ImageView icon4 = (ImageView) convertView.findViewById(C0149R.id.group_image);
                CheckBox chk = (CheckBox) convertView.findViewById(C0149R.id.checkBoxPref);
                chk.setChecked(listAppsFragment.getConfig().getBoolean(menuItem.booleanPref, menuItem.booleanPrefDef));
                chk.setClickable(false);
                icon4.setImageDrawable(getImage(group));
                icon4.setVisibility(TEXT_DEFAULT);
                break;
        }
        if (menuItem.punkt_menu_descr != 0) {
            TextView textGroupDescr = (TextView) convertView.findViewById(C0149R.id.textGroupDescr);
            textGroupDescr.setText(Utils.getText(menuItem.punkt_menu_descr));
            textGroupDescr.setTextColor(-3355444);
        } else {
            ((TextView) convertView.findViewById(C0149R.id.textGroupDescr)).setVisibility(8);
        }
        this.groupsViews.set(groupPosition, convertView);
        return convertView;
    }

    public void setTextSize(int s) {
        this.size = s;
        notifyDataSetChanged();
    }

    public Integer getChild(int groupPosition, int childPosition) {
        if (this.groups == null || this.groups.length == 0) {
            return null;
        }
        return (Integer) this.groups[groupPosition].childs.get(childPosition);
    }

    public long getGroupId(int groupPosition) {
        return 0;
    }

    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        MenuItem item = getGroup(groupPosition);
        int child_item = getChild(groupPosition, childPosition).intValue();
        switch (child_item) {
            case TEXT_MEDIUM /*1*/:
                convertView = ((LayoutInflater) this.context.getSystemService("layout_inflater")).inflate(C0149R.layout.child_text_size_pref_view, null);
                RadioGroup group = (RadioGroup) convertView.findViewById(C0149R.id.radioGroupView);
                switch (listAppsFragment.getConfig().getInt(listAppsFragment.SETTINGS_VIEWSIZE, TEXT_DEFAULT)) {
                    case TEXT_DEFAULT /*0*/:
                        ((RadioButton) group.findViewById(C0149R.id.radioSmall)).setChecked(true);
                        break;
                    case TEXT_MEDIUM /*1*/:
                        ((RadioButton) group.findViewById(C0149R.id.radioMedium)).setChecked(true);
                        break;
                    case TEXT_LARGE /*2*/:
                        ((RadioButton) group.findViewById(C0149R.id.radioLarge)).setChecked(true);
                        break;
                }
                group.setOnCheckedChangeListener(new C00971());
                return convertView;
            case TEXT_LARGE /*2*/:
                convertView = ((LayoutInflater) this.context.getSystemService("layout_inflater")).inflate(C0149R.layout.child_orientation_pref_view, null);
                RadioGroup group4 = (RadioGroup) convertView.findViewById(C0149R.id.radioGroupOrient);
                switch (listAppsFragment.getConfig().getInt("orientstion", 3)) {
                    case TEXT_MEDIUM /*1*/:
                        ((RadioButton) group4.findViewById(C0149R.id.radioLandscape)).setChecked(true);
                        break;
                    case TEXT_LARGE /*2*/:
                        ((RadioButton) group4.findViewById(C0149R.id.radioPortret)).setChecked(true);
                        break;
                    case AxmlParser.END_TAG /*3*/:
                        ((RadioButton) group4.findViewById(C0149R.id.radioSensor)).setChecked(true);
                        break;
                }
                group4.setOnCheckedChangeListener(new C00982());
                return convertView;
            case AxmlParser.END_TAG /*3*/:
                convertView = ((LayoutInflater) this.context.getSystemService("layout_inflater")).inflate(C0149R.layout.child_sort_pref_view, null);
                RadioGroup group5 = (RadioGroup) convertView.findViewById(C0149R.id.radioGroupSort);
                switch (listAppsFragment.getConfig().getInt("sortby", TEXT_LARGE)) {
                    case TEXT_MEDIUM /*1*/:
                        ((RadioButton) group5.findViewById(C0149R.id.radioName)).setChecked(true);
                        break;
                    case TEXT_LARGE /*2*/:
                        ((RadioButton) group5.findViewById(C0149R.id.radioStatus)).setChecked(true);
                        break;
                    case AxmlParser.END_TAG /*3*/:
                        ((RadioButton) group5.findViewById(C0149R.id.radioTime)).setChecked(true);
                        break;
                }
                group5.setOnCheckedChangeListener(new C00993());
                return convertView;
            case AxmlParser.START_NS /*4*/:
                convertView = ((LayoutInflater) this.context.getSystemService("layout_inflater")).inflate(C0149R.layout.child_categoty_pref_view, null);
                CheckBox chkLVL = (CheckBox) convertView.findViewById(C0149R.id.checkBoxLVL);
                chkLVL.setChecked(listAppsFragment.getConfig().getBoolean("lvlapp", true));
                chkLVL.append(Utils.getColoredText(LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.lvldescr), -7829368, BuildConfig.VERSION_NAME));
                chkLVL.setOnCheckedChangeListener(new C01004());
                CheckBox chkADS = (CheckBox) convertView.findViewById(C0149R.id.checkBoxADS);
                chkADS.setChecked(listAppsFragment.getConfig().getBoolean("adsapp", true));
                chkADS.append(Utils.getColoredText(LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.adsdescr), -7829368, BuildConfig.VERSION_NAME));
                chkADS.setOnCheckedChangeListener(new C01015());
                CheckBox chkCustom = (CheckBox) convertView.findViewById(C0149R.id.checkBoxCUSTOM);
                chkCustom.setChecked(listAppsFragment.getConfig().getBoolean("customapp", true));
                chkCustom.append(Utils.getColoredText(LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.customdescr), -7829368, BuildConfig.VERSION_NAME));
                chkCustom.setOnCheckedChangeListener(new C01026());
                CheckBox chkMOD = (CheckBox) convertView.findViewById(C0149R.id.checkBoxModify);
                chkMOD.setChecked(listAppsFragment.getConfig().getBoolean("modifapp", true));
                chkMOD.append(Utils.getColoredText(LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.modifdescr), -7829368, BuildConfig.VERSION_NAME));
                chkMOD.setOnCheckedChangeListener(new C01037());
                CheckBox chkFIX = (CheckBox) convertView.findViewById(C0149R.id.checkBoxFIX);
                chkFIX.setChecked(listAppsFragment.getConfig().getBoolean("fixedapp", true));
                chkFIX.append(Utils.getColoredText(LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.fixeddescr), -7829368, BuildConfig.VERSION_NAME));
                chkFIX.setOnCheckedChangeListener(new C01048());
                CheckBox chkNONE = (CheckBox) convertView.findViewById(C0149R.id.checkBoxNONE);
                chkNONE.setChecked(listAppsFragment.getConfig().getBoolean("noneapp", true));
                chkNONE.append(Utils.getColoredText(LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.nonedescr), -7829368, BuildConfig.VERSION_NAME));
                chkNONE.setOnCheckedChangeListener(new C01059());
                CheckBox chkSYS = (CheckBox) convertView.findViewById(C0149R.id.checkBoxSYS);
                chkSYS.setChecked(listAppsFragment.getConfig().getBoolean("systemapp", false));
                chkSYS.append(Utils.getColoredText(LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.sysdescr), -7829368, BuildConfig.VERSION_NAME));
                chkSYS.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        listAppsFragment.getConfig().edit().putBoolean("systemapp", isChecked).commit();
                        listAppsFragment.getConfig().edit().putBoolean("settings_change", true).commit();
                        listAppsFragment.getConfig().edit().putBoolean("lang_change", true).commit();
                        listAppsFragment.runResume = true;
                    }
                });
                return convertView;
            case AxmlParser.END_NS /*5*/:
                convertView = ((LayoutInflater) this.context.getSystemService("layout_inflater")).inflate(C0149R.layout.child_root_pref_view, null);
                RadioGroup group7 = (RadioGroup) convertView.findViewById(C0149R.id.radioGroupRoot);
                switch (listAppsFragment.getConfig().getInt("root_force", TEXT_DEFAULT)) {
                    case TEXT_DEFAULT /*0*/:
                        ((RadioButton) group7.findViewById(C0149R.id.radioRootAuto)).setChecked(true);
                        break;
                    case TEXT_MEDIUM /*1*/:
                        ((RadioButton) group7.findViewById(C0149R.id.radioRootOn)).setChecked(true);
                        break;
                    case TEXT_LARGE /*2*/:
                        ((RadioButton) group7.findViewById(C0149R.id.radioRootOff)).setChecked(true);
                        break;
                }
                group7.setOnCheckedChangeListener(new OnCheckedChangeListener() {
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        switch (checkedId) {
                            case C0149R.id.radioRootAuto:
                                listAppsFragment.getConfig().edit().putInt("root_force", MenuItemAdapter.TEXT_DEFAULT).commit();
                                listAppsFragment.getConfig().edit().putBoolean("settings_change", true).commit();
                                listAppsFragment.runResume = true;
                                return;
                            case C0149R.id.radioRootOn:
                                listAppsFragment.getConfig().edit().putInt("root_force", MenuItemAdapter.TEXT_MEDIUM).commit();
                                listAppsFragment.getConfig().edit().putBoolean("settings_change", true).commit();
                                listAppsFragment.runResume = true;
                                return;
                            case C0149R.id.radioRootOff:
                                listAppsFragment.getConfig().edit().putInt("root_force", MenuItemAdapter.TEXT_LARGE).commit();
                                listAppsFragment.getConfig().edit().putBoolean("settings_change", true).commit();
                                listAppsFragment.runResume = true;
                                return;
                            default:
                                return;
                        }
                    }
                });
                return convertView;
            case AxmlParser.TEXT /*6*/:
                convertView = ((LayoutInflater) this.context.getSystemService("layout_inflater")).inflate(C0149R.layout.child_name_sheme_pref_view, null);
                RadioGroup group8 = (RadioGroup) convertView.findViewById(C0149R.id.radioGroupNameSheme);
                switch (listAppsFragment.getConfig().getInt("apkname", TEXT_MEDIUM)) {
                    case TEXT_DEFAULT /*0*/:
                        ((RadioButton) group8.findViewById(C0149R.id.radioLabel)).setChecked(true);
                        break;
                    case TEXT_MEDIUM /*1*/:
                        ((RadioButton) group8.findViewById(C0149R.id.radioPkg)).setChecked(true);
                        break;
                }
                group8.setOnCheckedChangeListener(new OnCheckedChangeListener() {
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        switch (checkedId) {
                            case C0149R.id.radioLabel:
                                listAppsFragment.getConfig().edit().putInt("apkname", MenuItemAdapter.TEXT_DEFAULT).commit();
                                listAppsFragment.getConfig().edit().putBoolean("settings_change", true).commit();
                                listAppsFragment.runResume = true;
                                return;
                            case C0149R.id.radioPkg:
                                listAppsFragment.getConfig().edit().putInt("apkname", MenuItemAdapter.TEXT_MEDIUM).commit();
                                listAppsFragment.getConfig().edit().putBoolean("settings_change", true).commit();
                                listAppsFragment.runResume = true;
                                return;
                            default:
                                return;
                        }
                    }
                });
                return convertView;
            case AxmlParser.END_FILE /*7*/:
                convertView = ((LayoutInflater) this.context.getSystemService("layout_inflater")).inflate(C0149R.layout.child_icon_change_pref_view, null);
                RadioGroup group9 = (RadioGroup) convertView.findViewById(C0149R.id.radioGroupIconChange);
                switch (listAppsFragment.getConfig().getInt("default_icon_for_lp", TEXT_DEFAULT)) {
                    case TEXT_DEFAULT /*0*/:
                        ((RadioButton) group9.findViewById(C0149R.id.radioDefIcon)).setChecked(true);
                        break;
                    case TEXT_MEDIUM /*1*/:
                        ((RadioButton) group9.findViewById(C0149R.id.radioIcon2)).setChecked(true);
                        break;
                    case TEXT_LARGE /*2*/:
                        ((RadioButton) group9.findViewById(C0149R.id.radioIcon3)).setChecked(true);
                        break;
                    case AxmlParser.END_TAG /*3*/:
                        ((RadioButton) group9.findViewById(C0149R.id.radioIcon4)).setChecked(true);
                        break;
                    case AxmlParser.START_NS /*4*/:
                        ((RadioButton) group9.findViewById(C0149R.id.radioIcon5)).setChecked(true);
                        break;
                    case AxmlParser.END_NS /*5*/:
                        ((RadioButton) group9.findViewById(C0149R.id.radioIcon6)).setChecked(true);
                        break;
                }
                group9.setOnCheckedChangeListener(new OnCheckedChangeListener() {

                    class C00911 implements Runnable {
                        C00911() {
                        }

                        public void run() {
                            listAppsFragment com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment = listAppsFragment.frag;
                            listAppsFragment.showDialogLP(11);
                            listAppsFragment.progress2.setCancelable(false);
                            listAppsFragment.progress2.setMessage(Utils.getText(C0149R.string.wait));
                        }
                    }

                    class C00922 implements Runnable {
                        C00922() {
                        }

                        public void run() {
                            listAppsFragment com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment = listAppsFragment.frag;
                            listAppsFragment.showDialogLP(11);
                            listAppsFragment.progress2.setCancelable(false);
                            listAppsFragment.progress2.setMessage(Utils.getText(C0149R.string.wait));
                        }
                    }

                    class C00933 implements Runnable {
                        C00933() {
                        }

                        public void run() {
                            listAppsFragment com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment = listAppsFragment.frag;
                            listAppsFragment.showDialogLP(11);
                            listAppsFragment.progress2.setCancelable(false);
                            listAppsFragment.progress2.setMessage(Utils.getText(C0149R.string.wait));
                        }
                    }

                    class C00944 implements Runnable {
                        C00944() {
                        }

                        public void run() {
                            listAppsFragment com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment = listAppsFragment.frag;
                            listAppsFragment.showDialogLP(11);
                            listAppsFragment.progress2.setCancelable(false);
                            listAppsFragment.progress2.setMessage(Utils.getText(C0149R.string.wait));
                        }
                    }

                    class C00955 implements Runnable {
                        C00955() {
                        }

                        public void run() {
                            listAppsFragment com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment = listAppsFragment.frag;
                            listAppsFragment.showDialogLP(11);
                            listAppsFragment.progress2.setCancelable(false);
                            listAppsFragment.progress2.setMessage(Utils.getText(C0149R.string.wait));
                        }
                    }

                    class C00966 implements Runnable {
                        C00966() {
                        }

                        public void run() {
                            listAppsFragment com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment = listAppsFragment.frag;
                            listAppsFragment.showDialogLP(11);
                            listAppsFragment.progress2.setCancelable(false);
                            listAppsFragment.progress2.setMessage(Utils.getText(C0149R.string.wait));
                        }
                    }

                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        switch (checkedId) {
                            case C0149R.id.radioDefIcon:
                                listAppsFragment.getConfig().edit().putInt("default_icon_for_lp", MenuItemAdapter.TEXT_DEFAULT).commit();
                                listAppsFragment.frag.runToMain(new C00911());
                                Utils.setIcon(MenuItemAdapter.TEXT_DEFAULT);
                                listAppsFragment.getConfig().edit().putBoolean("settings_change", true).commit();
                                listAppsFragment.runResume = true;
                                return;
                            case C0149R.id.radioIcon2:
                                listAppsFragment.getConfig().edit().putInt("default_icon_for_lp", MenuItemAdapter.TEXT_MEDIUM).commit();
                                listAppsFragment.frag.runToMain(new C00922());
                                Utils.setIcon(MenuItemAdapter.TEXT_MEDIUM);
                                listAppsFragment.getConfig().edit().putBoolean("settings_change", true).commit();
                                listAppsFragment.runResume = true;
                                return;
                            case C0149R.id.radioIcon3:
                                listAppsFragment.getConfig().edit().putInt("default_icon_for_lp", MenuItemAdapter.TEXT_LARGE).commit();
                                listAppsFragment.frag.runToMain(new C00933());
                                Utils.setIcon(MenuItemAdapter.TEXT_LARGE);
                                listAppsFragment.getConfig().edit().putBoolean("settings_change", true).commit();
                                listAppsFragment.runResume = true;
                                return;
                            case C0149R.id.radioIcon4:
                                listAppsFragment.getConfig().edit().putInt("default_icon_for_lp", 3).commit();
                                listAppsFragment.frag.runToMain(new C00944());
                                Utils.setIcon(3);
                                listAppsFragment.getConfig().edit().putBoolean("settings_change", true).commit();
                                listAppsFragment.runResume = true;
                                return;
                            case C0149R.id.radioIcon5:
                                listAppsFragment.getConfig().edit().putInt("default_icon_for_lp", 4).commit();
                                listAppsFragment.frag.runToMain(new C00955());
                                Utils.setIcon(4);
                                listAppsFragment.getConfig().edit().putBoolean("settings_change", true).commit();
                                listAppsFragment.runResume = true;
                                return;
                            case C0149R.id.radioIcon6:
                                listAppsFragment.getConfig().edit().putInt("default_icon_for_lp", 5).commit();
                                listAppsFragment.frag.runToMain(new C00966());
                                Utils.setIcon(5);
                                listAppsFragment.getConfig().edit().putBoolean("settings_change", true).commit();
                                listAppsFragment.runResume = true;
                                return;
                            default:
                                return;
                        }
                    }
                });
                return convertView;
            default:
                convertView = ((LayoutInflater) this.context.getSystemService("layout_inflater")).inflate(C0149R.layout.child_view, null);
                TextView textChild = (TextView) convertView.findViewById(C0149R.id.textChild);
                textChild.setTextAppearance(this.context, listAppsFragment.getSizeText());
                textChild.setTextAppearance(this.context, listAppsFragment.getSizeText());
                textChild.setText(Utils.getText(child_item));
                ImageView image = (ImageView) convertView.findViewById(C0149R.id.child_image);
                image.setImageDrawable(getImage(item.punkt_menu));
                int color = getColor(getGroup(groupPosition).punkt_menu);
                image.setColorFilter(color, Mode.MULTIPLY);
                textChild.setTextColor(color);
                return convertView;
        }
    }

    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public void clear() {
        this.groups = new MenuItem[TEXT_DEFAULT];
        this.groupsViews = null;
        notifyDataSetChanged();
    }

    Drawable getImage(int group) {
        Drawable iconka;
        switch (group) {
            case C0149R.string.aboutmenu:
                return listAppsFragment.getRes().getDrawable(C0149R.mipmap.ic_group_run);
            case C0149R.string.apk_create_option:
                return listAppsFragment.getRes().getDrawable(C0149R.mipmap.ic_group_selectable);
            case C0149R.string.back:
                return listAppsFragment.getRes().getDrawable(C0149R.drawable.menu_back);
            case C0149R.string.billing_hack_menu_disable:
                iconka = listAppsFragment.getRes().getDrawable(C0149R.drawable.android);
                iconka.setColorFilter(Color.parseColor("#FFFFFF"), Mode.SRC_IN);
                return iconka;
            case C0149R.string.billing_hack_menu_enable:
                iconka = listAppsFragment.getRes().getDrawable(C0149R.drawable.android);
                iconka.setColorFilter(Color.parseColor("#FFFFFF"), Mode.SRC_IN);
                return iconka;
            case C0149R.string.bootview:
                iconka = listAppsFragment.getRes().getDrawable(C0149R.drawable.auto);
                iconka.setColorFilter(Color.parseColor("#FFFFFF"), Mode.SRC_IN);
                return iconka;
            case C0149R.string.change_icon_lp:
                return listAppsFragment.getRes().getDrawable(C0149R.mipmap.ic_group_selectable);
            case C0149R.string.cleardalvik:
                return listAppsFragment.getRes().getDrawable(C0149R.drawable.context_ads);
            case C0149R.string.context_batch_operations:
                return listAppsFragment.getRes().getDrawable(C0149R.drawable.auto);
            case C0149R.string.context_move_selected_apps_to_sdcard:
                return listAppsFragment.getRes().getDrawable(C0149R.drawable.auto);
            case C0149R.string.context_uninstall_selected_apps:
                return listAppsFragment.getRes().getDrawable(C0149R.drawable.context_ads);
            case C0149R.string.corepatches:
                iconka = listAppsFragment.getRes().getDrawable(C0149R.drawable.android);
                iconka.setColorFilter(Color.parseColor("#FFFFFF"), Mode.SRC_IN);
                return iconka;
            case C0149R.string.days_on_up:
                return listAppsFragment.getRes().getDrawable(C0149R.mipmap.ic_group_editable);
            case C0149R.string.dir_change:
                return listAppsFragment.getRes().getDrawable(C0149R.mipmap.ic_group_editable);
            case C0149R.string.dirbinder:
                return listAppsFragment.getRes().getDrawable(C0149R.drawable.context_permissions);
            case C0149R.string.disable_autoupdate:
                return listAppsFragment.getRes().getDrawable(C0149R.mipmap.ic_group_checkable);
            case C0149R.string.fast_start:
                return listAppsFragment.getRes().getDrawable(C0149R.mipmap.ic_group_checkable);
            case C0149R.string.filter:
                return listAppsFragment.getRes().getDrawable(C0149R.mipmap.ic_group_check_items);
            case C0149R.string.help:
                return listAppsFragment.getRes().getDrawable(C0149R.mipmap.ic_group_run);
            case C0149R.string.hide_notify:
                return listAppsFragment.getRes().getDrawable(C0149R.mipmap.ic_group_checkable);
            case C0149R.string.hide_title_app:
                return listAppsFragment.getRes().getDrawable(C0149R.mipmap.ic_group_checkable);
            case C0149R.string.langmenu:
                return listAppsFragment.getRes().getDrawable(C0149R.mipmap.ic_group_check_items);
            case C0149R.string.licensing_hack_menu_disable:
                iconka = listAppsFragment.getRes().getDrawable(C0149R.drawable.android);
                iconka.setColorFilter(Color.parseColor("#FFFFFF"), Mode.SRC_IN);
                return iconka;
            case C0149R.string.licensing_hack_menu_enable:
                iconka = listAppsFragment.getRes().getDrawable(C0149R.drawable.android);
                iconka.setColorFilter(Color.parseColor("#FFFFFF"), Mode.SRC_IN);
                return iconka;
            case C0149R.string.market_install:
                return listAppsFragment.getRes().getDrawable(C0149R.drawable.context_restore);
            case C0149R.string.mod_market_check:
                return listAppsFragment.getRes().getDrawable(C0149R.drawable.context_restore);
            case C0149R.string.no_icon:
                return listAppsFragment.getRes().getDrawable(C0149R.mipmap.ic_group_checkable);
            case C0149R.string.odex_all_system_app:
                return listAppsFragment.getRes().getDrawable(C0149R.drawable.context_deodex);
            case C0149R.string.orientmenu:
                return listAppsFragment.getRes().getDrawable(C0149R.mipmap.ic_group_selectable);
            case C0149R.string.reboot:
                return listAppsFragment.getRes().getDrawable(C0149R.drawable.context_patch_reboot);
            case C0149R.string.remove_all_saved_purchases:
                iconka = listAppsFragment.getRes().getDrawable(C0149R.drawable.android);
                iconka.setColorFilter(Color.parseColor("#FFFFFF"), Mode.SRC_IN);
                return iconka;
            case C0149R.string.removefixes:
                return listAppsFragment.getRes().getDrawable(C0149R.drawable.context_ads);
            case C0149R.string.sendlog:
                return listAppsFragment.getRes().getDrawable(C0149R.mipmap.ic_group_run);
            case C0149R.string.set_default_to_install:
                iconka = listAppsFragment.getRes().getDrawable(C0149R.drawable.auto);
                iconka.setColorFilter(Color.parseColor("#FFFFFF"), Mode.SRC_IN);
                return iconka;
            case C0149R.string.setting_confirm_exit:
                return listAppsFragment.getRes().getDrawable(C0149R.mipmap.ic_group_checkable);
            case C0149R.string.settings_force_root:
                return listAppsFragment.getRes().getDrawable(C0149R.mipmap.ic_group_selectable);
            case C0149R.string.sortmenu:
                return listAppsFragment.getRes().getDrawable(C0149R.mipmap.ic_group_selectable);
            case C0149R.string.text_size:
                return listAppsFragment.getRes().getDrawable(C0149R.mipmap.ic_group_selectable);
            case C0149R.string.toolbar_adfree:
                return listAppsFragment.getRes().getDrawable(C0149R.drawable.context_ads);
            case C0149R.string.update:
                return listAppsFragment.getRes().getDrawable(C0149R.mipmap.ic_group_run);
            case C0149R.string.vibration:
                return listAppsFragment.getRes().getDrawable(C0149R.mipmap.ic_group_checkable);
            case C0149R.string.xposed_settings:
                iconka = listAppsFragment.getRes().getDrawable(C0149R.drawable.android);
                iconka.setColorFilter(Color.parseColor("#FFFFFF"), Mode.SRC_IN);
                return iconka;
            default:
                return null;
        }
    }

    private int getColor(int group) {
        int color = Color.parseColor("#DDDDDD");
        switch (group) {
            case C0149R.string.bootview:
                return Color.parseColor("#99cccc");
            case C0149R.string.cleardalvik:
                return Color.parseColor("#c2f055");
            case C0149R.string.context_batch_operations:
                return Color.parseColor("#99cccc");
            case C0149R.string.context_clearhosts:
                return Color.parseColor("#c2f055");
            case C0149R.string.corepatches:
                return Color.parseColor("#cc99cc");
            case C0149R.string.dirbinder:
                return Color.parseColor("#c2f055");
            case C0149R.string.market_install:
                return Color.parseColor("#cc99cc");
            case C0149R.string.mod_market_check:
                return Color.parseColor("#cc99cc");
            case C0149R.string.odex_all_system_app:
                return Color.parseColor("#ffffbb");
            case C0149R.string.reboot:
                return Color.parseColor("#fe6c00");
            case C0149R.string.remove_all_saved_purchases:
                return Color.parseColor("#c2f055");
            case C0149R.string.removefixes:
                return Color.parseColor("#c2f055");
            case C0149R.string.root_needed:
                return Color.parseColor("#fe6c00");
            case C0149R.string.set_default_to_install:
                return Color.parseColor("#ffffbb");
            case C0149R.string.toolbar_adfree:
                return Color.parseColor("#c2f055");
            case C0149R.string.xposed_settings:
                return Color.parseColor("#cc99cc");
            default:
                return color;
        }
    }
}
