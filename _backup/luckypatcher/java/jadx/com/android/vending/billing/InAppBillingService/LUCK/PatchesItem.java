package com.android.vending.billing.InAppBillingService.LUCK;

public class PatchesItem {
    public String group = BuildConfig.VERSION_NAME;
    public boolean insert = false;
    public byte[] origByte;
    public int[] origMask;
    public byte[] repByte;
    public int[] repMask;
    public boolean result = false;

    public PatchesItem(byte[] orig, int[] mask, byte[] repl, int[] rmask, String groupka, boolean ins) {
        this.origByte = new byte[orig.length];
        this.origByte = orig;
        this.origMask = new int[mask.length];
        this.origMask = mask;
        this.repByte = new byte[repl.length];
        this.repByte = repl;
        this.repMask = new int[rmask.length];
        this.repMask = rmask;
        this.group = groupka;
        this.insert = ins;
    }
}
