package com.android.vending.billing.InAppBillingService.LUCK;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.android.vending.billing.InAppBillingService.LUCK.widgets.BinderWidget;
import com.chelpus.Utils;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

public class BinderLuckyPatcher extends BroadcastReceiver {
    public static Context contextB = null;

    public void onReceive(final Context context, Intent intent) {
        System.out.println("LuckyPatcher Intent: " + intent.toString());
        contextB = context;
        listAppsFragment.init();
        if (listAppsFragment.su) {
            if (intent.getAction().equals("android.intent.action.MEDIA_SCANNER_FINISHED") || intent.getAction().equals("android.intent.action.MEDIA_MOUNTED")) {
                new Thread(new Runnable() {
                    public void run() {
                        File bindfile = new File(context.getDir("binder", 0) + "/bind.txt");
                        if (bindfile.exists() && bindfile.length() > 0) {
                            System.out.println("LuckyPatcher binder start!");
                            try {
                                if (!bindfile.exists()) {
                                    bindfile.createNewFile();
                                }
                                FileInputStream fis = new FileInputStream(bindfile);
                                BufferedReader br = new BufferedReader(new InputStreamReader(fis, "UTF-8"));
                                String data = BuildConfig.VERSION_NAME;
                                String temp = BuildConfig.VERSION_NAME;
                                while (true) {
                                    data = br.readLine();
                                    if (data == null) {
                                        break;
                                    }
                                    String[] tails = data.split(";");
                                    if (tails.length == 2) {
                                        Utils.verify_bind_and_run("mount", "-o bind '" + tails[0] + "' '" + tails[1] + "'", tails[0], tails[1]);
                                    }
                                }
                                Intent i = new Intent(context, BinderWidget.class);
                                i.setAction(BinderWidget.ACTION_WIDGET_RECEIVER_Updater);
                                context.sendBroadcast(i);
                                fis.close();
                            } catch (FileNotFoundException e) {
                                System.out.println("Not found bind.txt");
                            } catch (IOException e2) {
                                System.out.println(BuildConfig.VERSION_NAME + e2);
                            }
                            listAppsFragment.patchOnBoot = false;
                            Utils.exit();
                        }
                    }
                }).start();
                listAppsFragment.patchOnBoot = true;
            }
            if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
                File bindfile = new File(context.getDir("binder", 0) + "/bind.txt");
                if (bindfile.exists() && bindfile.length() > 0) {
                    System.out.println("LuckyPatcher binder start!");
                    try {
                        if (!bindfile.exists()) {
                            bindfile.createNewFile();
                        }
                        FileInputStream fis = new FileInputStream(bindfile);
                        BufferedReader br = new BufferedReader(new InputStreamReader(fis, "UTF-8"));
                        String data = BuildConfig.VERSION_NAME;
                        String temp = BuildConfig.VERSION_NAME;
                        while (true) {
                            data = br.readLine();
                            if (data == null) {
                                break;
                            }
                            String[] tails = data.split(";");
                            if (tails.length == 2) {
                                Utils.verify_bind_and_run("mount", "-o bind '" + tails[0] + "' '" + tails[1] + "'", tails[0], tails[1]);
                            }
                        }
                        Intent i = new Intent(context, BinderWidget.class);
                        i.setAction(BinderWidget.ACTION_WIDGET_RECEIVER_Updater);
                        context.sendBroadcast(i);
                        fis.close();
                    } catch (FileNotFoundException e) {
                        System.out.println("Not found bind.txt");
                    } catch (IOException e2) {
                        System.out.println(BuildConfig.VERSION_NAME + e2);
                    }
                }
            }
        }
        if (!listAppsFragment.getConfig().getBoolean("OnBootService", false)) {
            Utils.exit();
        }
    }
}
