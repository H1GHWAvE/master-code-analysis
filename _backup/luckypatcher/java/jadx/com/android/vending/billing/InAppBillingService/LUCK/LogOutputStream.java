package com.android.vending.billing.InAppBillingService.LUCK;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class LogOutputStream extends OutputStream {
    public String allresult = BuildConfig.VERSION_NAME;
    private ByteArrayOutputStream bos = new ByteArrayOutputStream();
    private String name;

    public LogOutputStream(String name) {
        this.name = name;
    }

    public void write(int b) throws IOException {
        if (b == 10) {
            this.allresult += new String(this.bos.toByteArray()) + LogCollector.LINE_SEPARATOR;
            this.bos = new ByteArrayOutputStream();
            return;
        }
        this.bos.write(b);
    }
}
