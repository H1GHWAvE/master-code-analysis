package com.android.vending.billing.InAppBillingService.LUCK.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.pm.PackageManager.NameNotFoundException;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.android.vending.billing.InAppBillingService.LUCK.AlertDlg;
import com.android.vending.billing.InAppBillingService.LUCK.BuildConfig;
import com.android.vending.billing.InAppBillingService.LUCK.C0149R;
import com.android.vending.billing.InAppBillingService.LUCK.LogCollector;
import com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment;
import com.chelpus.Utils;
import java.io.File;
import net.lingala.zip4j.util.InternalZipConstants;

public class Create_Apk_Support_Dialog {
    Dialog dialog = null;
    String resultFile = BuildConfig.VERSION_NAME;

    class C01931 implements OnClickListener {
        C01931() {
        }

        public void onClick(DialogInterface dialog, int which) {
            listAppsFragment.patchAct.show_file_explorer(Create_Apk_Support_Dialog.this.resultFile);
        }
    }

    public void showDialog() {
        if (this.dialog == null) {
            this.dialog = onCreateDialog();
        }
        if (this.dialog != null) {
            this.dialog.show();
        }
    }

    public Dialog onCreateDialog() {
        System.out.println("Create apk support Dialog create.");
        if (listAppsFragment.frag == null || listAppsFragment.frag.getContext() == null) {
            dismiss();
        }
        LinearLayout d = (LinearLayout) View.inflate(listAppsFragment.frag.getContext(), C0149R.layout.custompatchdialog, null);
        LinearLayout body = (LinearLayout) d.findViewById(C0149R.id.custompatchbodyscroll).findViewById(C0149R.id.dialogbodycustompatch);
        String versionPkg = ".v.";
        try {
            versionPkg = ".v." + listAppsFragment.getPkgMng().getPackageInfo(listAppsFragment.pli.pkgName, 0).versionName + ".b." + listAppsFragment.getPkgMng().getPackageInfo(listAppsFragment.pli.pkgName, 0).versionCode;
        } catch (NameNotFoundException e) {
            e.printStackTrace();
        } catch (NullPointerException e2) {
            dismiss();
        }
        TextView tv = (TextView) body.findViewById(C0149R.id.custom_log);
        if (listAppsFragment.str == null) {
            listAppsFragment.str = " ";
        }
        listAppsFragment com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment = listAppsFragment.frag;
        listAppsFragment.patch_dialog_text_builder(tv, true);
        if (listAppsFragment.pli != null) {
            this.resultFile = listAppsFragment.basepath + "/Modified/" + listAppsFragment.pli.name.replaceAll(" ", ".").replaceAll(InternalZipConstants.ZIP_FILE_SEPARATOR, ".") + InternalZipConstants.ZIP_FILE_SEPARATOR;
            File apkcrk = new File(this.resultFile + listAppsFragment.pli.name.replaceAll(" ", ".").replaceAll(InternalZipConstants.ZIP_FILE_SEPARATOR, ".") + versionPkg + ".crk.Support.apk");
            File apkcrk2 = new File(this.resultFile + listAppsFragment.pli.pkgName + versionPkg + ".crk.Support.apk");
            if (apkcrk.exists() || apkcrk2.exists()) {
                if (apkcrk.exists() && listAppsFragment.getConfig().getInt("apkname", 0) == 0) {
                    listAppsFragment.str = Utils.getText(C0149R.string.createdialog1) + " " + listAppsFragment.pli.name + " " + Utils.getText(C0149R.string.createdialog2) + LogCollector.LINE_SEPARATOR + this.resultFile + "\n\n" + Utils.getText(C0149R.string.createdialog5) + LogCollector.LINE_SEPARATOR + listAppsFragment.pli.name.replaceAll(" ", ".").replaceAll(InternalZipConstants.ZIP_FILE_SEPARATOR, ".") + versionPkg + ".crk.Support.apk" + Utils.getText(C0149R.string.createdialog3);
                }
                if (apkcrk2.exists() && listAppsFragment.getConfig().getInt("apkname", 0) == 1) {
                    listAppsFragment.str = Utils.getText(C0149R.string.createdialog1) + " " + listAppsFragment.pli.name + " " + Utils.getText(C0149R.string.createdialog2) + LogCollector.LINE_SEPARATOR + this.resultFile + "\n\n" + Utils.getText(C0149R.string.createdialog5) + LogCollector.LINE_SEPARATOR + listAppsFragment.pli.pkgName + versionPkg + ".crk.Support.apk" + Utils.getText(C0149R.string.createdialog3);
                }
                ((TextView) body.findViewById(C0149R.id.custom_log)).append(Utils.getColoredText(listAppsFragment.str, -990142, "bold"));
            } else {
                listAppsFragment.str = Utils.getText(C0149R.string.createdialog1) + " " + listAppsFragment.pli.name + Utils.getText(C0149R.string.createdialog4);
                ((TextView) body.findViewById(C0149R.id.custom_log)).append(Utils.getColoredText(listAppsFragment.str, -65451, "bold"));
            }
        }
        return new AlertDlg(listAppsFragment.frag.getContext()).setTitle(Utils.getText(C0149R.string.PatchResult)).setCancelable(true).setIcon(C0149R.drawable.ic_angel).setPositiveButton(Utils.getText(17039370), null).setNeutralButton(Utils.getText(C0149R.string.go_to_file_path), new C01931()).setView(d).create();
    }

    public void dismiss() {
        if (this.dialog != null) {
            this.dialog.dismiss();
            this.dialog = null;
        }
    }
}
