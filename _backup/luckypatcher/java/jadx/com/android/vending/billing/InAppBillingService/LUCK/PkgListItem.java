package com.android.vending.billing.InAppBillingService.LUCK;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.app.FragmentTransaction;
import com.chelpus.Utils;
import java.io.File;
import org.tukaani.xz.LZMA2Options;
import pxb.android.axml.AxmlParser;

public class PkgListItem {
    public static final int PKG_STORED_EXTERNAL = 1;
    public static final int PKG_STORED_INTERNAL = 0;
    public static final int PKG_STOREPREF_AUTO = 0;
    public static final int PKG_STOREPREF_EXT = 2;
    public static final int PKG_STOREPREF_INT = 1;
    public static final int lastIntPosition = 10;
    public static final int noneAppIntPosition = 6;
    public boolean ads = false;
    public boolean billing = false;
    public boolean boot_ads = false;
    public boolean boot_custom = false;
    public boolean boot_lvl = false;
    public boolean boot_manual = false;
    public boolean custom = false;
    public boolean enable = false;
    public boolean hidden = false;
    public Drawable icon;
    public boolean lvl = false;
    public boolean modified = false;
    public String name;
    public boolean odex = false;
    public boolean on_sd = false;
    public String pkgName;
    public boolean selected = false;
    public String statusi = BuildConfig.VERSION_NAME;
    public int stored;
    public int storepref;
    public boolean system = false;
    public int updatetime = PKG_STOREPREF_AUTO;

    public PkgListItem(Context context, PkgListItem p) {
        this.pkgName = p.pkgName;
        this.name = p.name;
        this.storepref = p.storepref;
        this.stored = p.stored;
        this.hidden = p.hidden;
        this.statusi = p.statusi;
        this.boot_ads = p.boot_ads;
        this.boot_lvl = p.boot_lvl;
        this.boot_custom = p.boot_custom;
        this.boot_manual = p.boot_manual;
        this.custom = p.custom;
        this.lvl = p.lvl;
        this.ads = p.ads;
        this.modified = p.modified;
        this.on_sd = Utils.isInstalledOnSdCard(this.pkgName);
        this.system = p.system;
        this.odex = p.odex;
        this.icon = p.icon;
        this.enable = p.enable;
        if (!this.enable) {
            this.stored = lastIntPosition;
        }
        this.updatetime = p.updatetime;
    }

    public PkgListItem(Context context, String pkgNamein, String pkgLabel, int storedin, int storeprefin, int hiddenin, String statusiin, int boot_adsin, int boot_lvlin, int boot_customin, int boot_manualin, int customin, int lvlin, int adsin, int modifiedin, int systemin, int odexin, Bitmap iconin, int updatetimein, int billingin, boolean enablein, boolean sd_on, boolean skipIconRead) {
        this.pkgName = pkgNamein;
        this.name = pkgLabel;
        this.storepref = storeprefin;
        this.stored = storedin;
        this.enable = enablein;
        if (!this.enable) {
            this.stored = lastIntPosition;
        }
        if (hiddenin == 0) {
            this.hidden = false;
        } else {
            this.hidden = true;
        }
        this.statusi = statusiin;
        if (boot_adsin == 0) {
            this.boot_ads = false;
        } else {
            this.boot_ads = true;
        }
        if (boot_lvlin == 0) {
            this.boot_lvl = false;
        } else {
            this.boot_lvl = true;
        }
        if (boot_customin == 0) {
            this.boot_custom = false;
        } else {
            this.boot_custom = true;
        }
        if (boot_manualin == 0) {
            this.boot_manual = false;
        } else {
            this.boot_manual = true;
        }
        if (customin == 0) {
            this.custom = false;
        } else {
            this.custom = true;
        }
        if (lvlin == 0) {
            this.lvl = false;
        } else {
            this.lvl = true;
        }
        if (adsin == 0) {
            this.ads = false;
        } else {
            this.ads = true;
        }
        if (modifiedin == 0) {
            this.modified = false;
        } else {
            this.modified = true;
        }
        this.on_sd = sd_on;
        if (systemin == 0) {
            this.system = false;
        } else {
            this.system = true;
        }
        if (odexin == 0) {
            this.odex = false;
        } else {
            this.odex = true;
        }
        if (billingin == 0) {
            this.billing = false;
        } else {
            this.billing = true;
        }
        if (!skipIconRead) {
            if (iconin != null) {
                this.updatetime = updatetimein;
                this.icon = new BitmapDrawable(iconin);
                return;
            }
            int imgSize = (int) ((35.0f * listAppsFragment.getRes().getDisplayMetrics().density) + 0.5f);
            try {
                Bitmap iconka = ((BitmapDrawable) context.getPackageManager().getApplicationIcon(this.pkgName)).getBitmap();
                int width = iconka.getWidth();
                int height = iconka.getHeight();
                float scaleWidth = ((float) imgSize) / ((float) width);
                float scaleHeight = ((float) imgSize) / ((float) height);
                Matrix bMatrix = new Matrix();
                bMatrix.postScale(scaleWidth, scaleHeight);
                this.icon = new BitmapDrawable(Bitmap.createBitmap(iconka, PKG_STOREPREF_AUTO, PKG_STOREPREF_AUTO, width, height, bMatrix, true));
            } catch (NameNotFoundException e) {
                e.printStackTrace();
            } catch (Exception e2) {
                e2.printStackTrace();
            } catch (OutOfMemoryError e3) {
            }
        }
    }

    public PkgListItem(Context context, String packName, int days, boolean iconTrig) throws IllegalArgumentException {
        PackageInfo pkginfo;
        int d;
        PackageManager pm = listAppsFragment.getPkgMng();
        ApplicationInfo appInfo = null;
        if (iconTrig) {
            try {
                int imgSize = (int) ((35.0f * listAppsFragment.getRes().getDisplayMetrics().density) + 0.5f);
                try {
                    Bitmap iconka = ((BitmapDrawable) pm.getApplicationIcon(packName)).getBitmap();
                    int width = iconka.getWidth();
                    int height = iconka.getHeight();
                    float scaleWidth = ((float) imgSize) / ((float) width);
                    float scaleHeight = ((float) imgSize) / ((float) height);
                    Matrix bMatrix = new Matrix();
                    bMatrix.postScale(scaleWidth, scaleHeight);
                    this.icon = new BitmapDrawable(Bitmap.createBitmap(iconka, PKG_STOREPREF_AUTO, PKG_STOREPREF_AUTO, width, height, bMatrix, true));
                } catch (NameNotFoundException e) {
                    e.printStackTrace();
                } catch (Exception e2) {
                    e2.printStackTrace();
                } catch (OutOfMemoryError e3) {
                }
            } catch (Exception e4) {
            }
        }
        String packageName = packName;
        this.pkgName = packName;
        this.on_sd = Utils.isInstalledOnSdCard(this.pkgName);
        this.boot_ads = false;
        this.boot_lvl = false;
        this.boot_custom = false;
        this.boot_manual = false;
        this.custom = false;
        this.lvl = false;
        this.ads = false;
        this.modified = false;
        this.system = false;
        if (packageName == null || packageName == BuildConfig.VERSION_NAME) {
            throw new IllegalArgumentException("package scan error");
        }
        pm = listAppsFragment.getPkgMng();
        this.pkgName = packName;
        if (PKG_STOREPREF_AUTO == null) {
            try {
                appInfo = pm.getPackageInfo(packName, PKG_STOREPREF_AUTO).applicationInfo;
            } catch (NameNotFoundException e5) {
                System.out.println(this.pkgName);
                throw new IllegalArgumentException("package scan error");
            } catch (NullPointerException e6) {
                e6.printStackTrace();
                listAppsFragment.getRes();
                pkginfo = null;
                try {
                    pkginfo = pm.getPackageInfo(packageName, FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                    for (d = PKG_STOREPREF_AUTO; d < pkginfo.activities.length; d += PKG_STOREPREF_INT) {
                        if (Utils.isAds(pkginfo.activities[d].name)) {
                            this.stored = 4;
                            this.ads = true;
                        }
                    }
                } catch (Exception e7) {
                }
                if (pkginfo == null) {
                    try {
                        pkginfo = listAppsFragment.getPkgMng().getPackageInfo(packageName, LZMA2Options.DICT_SIZE_MIN);
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
                if (pkginfo.requestedPermissions != null && pkginfo.requestedPermissions.length != 0) {
                    for (d = PKG_STOREPREF_AUTO; d < pkginfo.requestedPermissions.length; d += PKG_STOREPREF_INT) {
                        if (pkginfo.requestedPermissions[d].toUpperCase().contains("BILLING") && !this.pkgName.equals(context.getApplicationContext().getPackageName())) {
                            this.billing = true;
                            if (!this.lvl) {
                                this.stored = 3;
                            }
                        }
                        if (pkginfo.requestedPermissions[d].contains("CHECK_LICENSE") && !this.pkgName.equals(context.getApplicationContext().getPackageName())) {
                            if (!this.ads) {
                                this.lvl = true;
                                this.stored = PKG_STOREPREF_EXT;
                                if (this.lvl && this.billing) {
                                    break;
                                }
                            }
                            this.stored = PKG_STOREPREF_EXT;
                            this.lvl = true;
                            if (this.lvl && this.billing) {
                                break;
                            }
                        }
                    }
                }
                if (!(this.lvl || this.ads || this.billing)) {
                    this.stored = noneAppIntPosition;
                }
                try {
                    if (Utils.isCustomPatchesForPkg(this.pkgName)) {
                        this.stored = PKG_STOREPREF_INT;
                        this.custom = true;
                    }
                    if (Math.abs(((int) (System.currentTimeMillis() / 1000)) - this.updatetime) < 86400 * days) {
                        this.stored = PKG_STOREPREF_AUTO;
                    }
                } catch (Exception e12) {
                    System.out.println("LuckyPatcher (PkgListItem): Custom patches not found! " + e12);
                    e12.printStackTrace();
                }
            }
        }
        this.enable = appInfo.enabled;
        this.name = pm.getPackageInfo(packName, PKG_STOREPREF_AUTO).applicationInfo.loadLabel(pm).toString();
        String pkgdir = pm.getApplicationInfo(this.pkgName, PKG_STOREPREF_AUTO).sourceDir;
        this.updatetime = (int) (Utils.getfirstInstallTime(this.pkgName, false) / 1000);
        if (Utils.isOdex(pkgdir)) {
            this.odex = true;
        } else {
            this.odex = false;
        }
        this.stored = PKG_STOREPREF_EXT;
        this.storepref = PKG_STOREPREF_AUTO;
        this.modified = Utils.isModified(this.pkgName, listAppsFragment.getInstance());
        if (listAppsFragment.bootlist != null) {
            for (int i = PKG_STOREPREF_AUTO; i < listAppsFragment.bootlist.length; i += PKG_STOREPREF_INT) {
                String[] templist = new String[listAppsFragment.bootlist[i].split("%").length];
                templist = listAppsFragment.bootlist[i].split("%");
                if (templist[PKG_STOREPREF_AUTO].endsWith(this.pkgName)) {
                    for (int j = PKG_STOREPREF_INT; j < templist.length; j += PKG_STOREPREF_INT) {
                        if (templist[j].contains("ads")) {
                            this.boot_ads = true;
                        }
                        if (templist[j].contains("lvl")) {
                            this.boot_lvl = true;
                        }
                        if (templist[j].contains("custom")) {
                            this.boot_custom = true;
                        }
                    }
                }
            }
        }
        File[] list = context.getDir("bootlist", PKG_STOREPREF_AUTO).listFiles();
        if (list != null) {
            int length = list.length;
            for (int i2 = PKG_STOREPREF_AUTO; i2 < length; i2 += PKG_STOREPREF_INT) {
                File aList = list[i2];
                if (!aList.getName().endsWith("bootlist") && this.pkgName.equals(aList.getName())) {
                    this.boot_custom = true;
                }
            }
        }
        listAppsFragment.getRes();
        if (appInfo == null) {
            try {
                appInfo = pm.getPackageInfo(packName, PKG_STOREPREF_AUTO).applicationInfo;
            } catch (NullPointerException e62) {
                System.out.println("LuckyPatcher (PkgListItem): " + e62);
            }
        }
        if ((appInfo.flags & PKG_STOREPREF_INT) != 0) {
            this.system = true;
        }
        pkginfo = null;
        try {
            pkginfo = pm.getPackageInfo(packageName, FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            for (d = PKG_STOREPREF_AUTO; d < pkginfo.activities.length; d += PKG_STOREPREF_INT) {
                if (Utils.isAds(pkginfo.activities[d].name)) {
                    this.stored = 4;
                    this.ads = true;
                }
            }
        } catch (Exception e8) {
        }
        if (pkginfo == null) {
            try {
                pkginfo = listAppsFragment.getPkgMng().getPackageInfo(packageName, LZMA2Options.DICT_SIZE_MIN);
            } catch (Exception e122) {
                e122.printStackTrace();
            }
        }
        if (pkginfo.requestedPermissions != null && pkginfo.requestedPermissions.length != 0) {
            for (d = PKG_STOREPREF_AUTO; d < pkginfo.requestedPermissions.length; d += PKG_STOREPREF_INT) {
                if (pkginfo.requestedPermissions[d].toUpperCase().contains("BILLING") && !this.pkgName.equals(context.getApplicationContext().getPackageName())) {
                    this.billing = true;
                    if (!this.lvl) {
                        this.stored = 3;
                    }
                }
                if (pkginfo.requestedPermissions[d].contains("CHECK_LICENSE") && !this.pkgName.equals(context.getApplicationContext().getPackageName())) {
                    if (!this.ads) {
                        this.lvl = true;
                        this.stored = PKG_STOREPREF_EXT;
                        if (this.lvl && this.billing) {
                            break;
                        }
                    }
                    this.stored = PKG_STOREPREF_EXT;
                    this.lvl = true;
                    if (this.lvl && this.billing) {
                        break;
                    }
                }
            }
        }
        if (!(this.lvl || this.ads || this.billing)) {
            this.stored = noneAppIntPosition;
        }
        try {
            if (Utils.isCustomPatchesForPkg(this.pkgName)) {
                this.stored = PKG_STOREPREF_INT;
                this.custom = true;
            }
            if (Math.abs(((int) (System.currentTimeMillis() / 1000)) - this.updatetime) < 86400 * days) {
                this.stored = PKG_STOREPREF_AUTO;
            }
        } catch (Exception e1222) {
            System.out.println("LuckyPatcher (PkgListItem): Custom patches not found! " + e1222);
            e1222.printStackTrace();
        }
        if (!this.enable) {
            this.stored = lastIntPosition;
        }
    }

    public String toString() {
        return this.name;
    }

    public boolean equalsPli(PkgListItem item) {
        try {
            if (this.pkgName.equals(item.pkgName) && this.name.equals(item.name) && this.stored == item.stored && this.storepref == item.storepref && this.hidden == item.hidden && this.boot_ads == item.boot_ads && this.boot_lvl == item.boot_lvl && this.boot_custom == item.boot_custom && this.boot_manual == item.boot_manual && this.custom == item.custom && this.lvl == item.lvl && this.ads == item.ads && this.modified == item.modified && this.on_sd == item.on_sd && this.system == item.system && this.odex == item.odex && this.enable == item.enable && this.billing == item.billing && this.updatetime == item.updatetime) {
                return true;
            }
            return false;
        } catch (NullPointerException e) {
            System.out.println("LuckyPacther: " + item.pkgName);
            e.printStackTrace();
            return false;
        }
    }

    public void saveItem() {
        if (listAppsFragment.database == null) {
            listAppsFragment.database = new DatabaseHelper(listAppsFragment.getInstance());
        }
        listAppsFragment.database.savePackage(this);
    }

    public int getColor() {
        switch (this.stored) {
            case PKG_STOREPREF_AUTO /*0*/:
                return C0149R.color.yellow;
            case PKG_STOREPREF_INT /*1*/:
                return C0149R.color.autoext;
            case PKG_STOREPREF_EXT /*2*/:
                return C0149R.color.prefint;
            case AxmlParser.END_TAG /*3*/:
                return C0149R.color.intonly;
            default:
                return C0149R.color.impossible;
        }
    }
}
