package com.android.vending.billing.InAppBillingService.LUCK.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Handler;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.android.vending.billing.InAppBillingService.LUCK.AlertDlg;
import com.android.vending.billing.InAppBillingService.LUCK.C0149R;
import com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment;
import com.chelpus.Utils;

public class Custom_Patch_Dialog {
    Dialog dialog = null;

    class C01971 implements OnClickListener {
        C01971() {
        }

        public void onClick(DialogInterface dialog, int which) {
            final Handler handler = new Handler();
            new Thread(new Runnable() {

                class C01941 implements Runnable {
                    C01941() {
                    }

                    public void run() {
                        try {
                            listAppsFragment.patchAct.startActivity(listAppsFragment.getPkgMng().getLaunchIntentForPackage(listAppsFragment.pli.pkgName));
                            listAppsFragment com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment = listAppsFragment.frag;
                            listAppsFragment.removeDialogLP(4);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                class C01952 implements Runnable {
                    C01952() {
                    }

                    public void run() {
                        Toast.makeText(listAppsFragment.frag.getContext(), Utils.getText(C0149R.string.error_launch), 1).show();
                    }
                }

                public void run() {
                    try {
                        Utils.kill(listAppsFragment.pli.pkgName);
                        Utils.run_all("killall " + listAppsFragment.pli.pkgName);
                        handler.post(new C01941());
                    } catch (Exception e) {
                        if (listAppsFragment.frag != null) {
                            listAppsFragment.frag.runToMain(new C01952());
                        }
                    }
                }
            }).start();
        }
    }

    public void showDialog() {
        if (this.dialog == null) {
            this.dialog = onCreateDialog();
        }
        if (this.dialog != null) {
            this.dialog.show();
        }
    }

    public Dialog onCreateDialog() {
        System.out.println("Custom Dialog create.");
        if (listAppsFragment.frag == null || listAppsFragment.frag.getContext() == null) {
            dismiss();
        }
        LinearLayout d = (LinearLayout) View.inflate(listAppsFragment.frag.getContext(), C0149R.layout.custompatchdialog, null);
        try {
            LinearLayout body = (LinearLayout) d.findViewById(C0149R.id.custompatchbodyscroll).findViewById(C0149R.id.dialogbodycustompatch);
            if (listAppsFragment.str == null) {
                listAppsFragment.str = " ";
            }
            if (!listAppsFragment.str.contains("SU Java-Code Running!")) {
                listAppsFragment.str = "Root not stable. Try again or update your root.";
            }
            if (!(listAppsFragment.str.contains("Error LP:") || listAppsFragment.str.contains("Object not found!"))) {
                ((TextView) body.findViewById(C0149R.id.custom_log)).append(Utils.getColoredText(listAppsFragment.str, "#ff00ff73", "bold"));
            }
            if (listAppsFragment.str.contains("Error LP:") || listAppsFragment.str.contains("Object not found!")) {
                ((TextView) body.findViewById(C0149R.id.custom_log)).append(Utils.getColoredText(listAppsFragment.str, "#ffff0055", "bold"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            dismiss();
        }
        AlertDlg builder = new AlertDlg(listAppsFragment.frag.getContext());
        builder.setIcon(C0149R.drawable.ic_angel);
        builder.setTitle(Utils.getText(C0149R.string.PatchResult));
        builder.setCancelable(true);
        builder.setPositiveButton(Utils.getText(17039370), null);
        if (listAppsFragment.func != 0) {
            builder.setNeutralButton((int) C0149R.string.launchbutton, new C01971());
        }
        builder.setView(d);
        return builder.create();
    }

    public void dismiss() {
        if (this.dialog != null) {
            this.dialog.dismiss();
            this.dialog = null;
        }
    }
}
