package com.android.vending.billing.InAppBillingService.LUCK.widgets;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Color;
import android.os.Handler;
import android.widget.RemoteViews;
import android.widget.Toast;
import com.android.vending.billing.InAppBillingService.LUCK.BuildConfig;
import com.android.vending.billing.InAppBillingService.LUCK.C0149R;
import com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment;
import com.chelpus.Utils;

public class AppDisablerWidget extends AppWidgetProvider {
    public static String ACTION_WIDGET_RECEIVER = "ActionReceiverWidgetAppDisabler";
    public static String ACTION_WIDGET_RECEIVER_Updater = "ActionReceiverWidgetAppDisablerUpdate";

    public static void pushWidgetUpdate(Context context, RemoteViews remoteViews) {
        AppWidgetManager.getInstance(context).updateAppWidget(new ComponentName(context, AppDisablerWidget.class), remoteViews);
    }

    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        for (int updateAppWidget : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, updateAppWidget);
        }
    }

    public void onDeleted(Context context, int[] appWidgetIds) {
        for (int deleteTitlePref : appWidgetIds) {
            AppDisablerWidgetConfigureActivity.deleteTitlePref(context, deleteTitlePref);
        }
    }

    public void onEnabled(Context context) {
    }

    public void onDisabled(Context context) {
    }

    static void updateAppWidget(final Context context, final AppWidgetManager appWidgetManager, final int appWidgetId) {
        listAppsFragment.init();
        Thread binder = new Thread(new Runnable() {
            public void run() {
                RemoteViews views;
                if (listAppsFragment.su) {
                    String package_name = AppDisablerWidgetConfigureActivity.loadTitlePref(context, appWidgetId);
                    try {
                        PackageManager pm = context.getPackageManager();
                        views = new RemoteViews(context.getPackageName(), C0149R.layout.app_disabler_widget);
                        views.setTextViewText(C0149R.id.appwidget_text, pm.getApplicationLabel(pm.getApplicationInfo(package_name, 0)));
                        if (pm.getPackageInfo(package_name, 0).applicationInfo.enabled) {
                            views.setTextColor(C0149R.id.appwidget_text, Color.parseColor("#00FF00"));
                            views.setInt(C0149R.id.toggleButton_off, "setBackgroundResource", C0149R.drawable.switch_on);
                        } else {
                            views.setTextColor(C0149R.id.appwidget_text, Color.parseColor("#FF0000"));
                            views.setInt(C0149R.id.toggleButton_off, "setBackgroundResource", C0149R.drawable.switch_off);
                        }
                        Intent active = new Intent(context, AppDisablerWidget.class);
                        active.setAction(AppDisablerWidget.ACTION_WIDGET_RECEIVER);
                        active.putExtra("appWidgetId", appWidgetId);
                        active.putExtra("msg", package_name);
                        views.setOnClickPendingIntent(C0149R.id.button, PendingIntent.getBroadcast(context, appWidgetId, active, 0));
                        try {
                            appWidgetManager.updateAppWidget(appWidgetId, views);
                            return;
                        } catch (Exception e) {
                            e.printStackTrace();
                            return;
                        }
                    } catch (NameNotFoundException e2) {
                        views = new RemoteViews(context.getPackageName(), C0149R.layout.app_disabler_widget);
                        views.setOnClickPendingIntent(C0149R.id.button, PendingIntent.getBroadcast(context, appWidgetId, new Intent(context, AppDisablerWidget.class), 0));
                        views.setInt(C0149R.id.toggleButton_off, "setBackgroundResource", C0149R.drawable.switch_off);
                        views.setTextColor(C0149R.id.appwidget_text, Color.parseColor("#AAAAAA"));
                        views.setTextViewText(C0149R.id.appwidget_text, Utils.getText(C0149R.string.app_disabler_widget_pkg_not_found));
                        try {
                            appWidgetManager.updateAppWidget(appWidgetId, views);
                            return;
                        } catch (Exception e1) {
                            e1.printStackTrace();
                            return;
                        }
                    }
                }
                views = new RemoteViews(context.getPackageName(), C0149R.layout.app_disabler_widget);
                views.setOnClickPendingIntent(C0149R.id.button, PendingIntent.getBroadcast(context, appWidgetId, new Intent(context, AppDisablerWidget.class), 0));
                views.setInt(C0149R.id.toggleButton_off, "setBackgroundResource", C0149R.drawable.switch_off);
                views.setTextColor(C0149R.id.appwidget_text, Color.parseColor("#AAAAAA"));
                views.setTextViewText(C0149R.id.appwidget_text, "you need root access");
                try {
                    AppWidgetManager.getInstance(context).updateAppWidget(appWidgetId, views);
                } catch (Exception e3) {
                    e3.printStackTrace();
                }
            }
        });
        binder.setPriority(10);
        binder.start();
    }

    public void onReceive(final Context context, final Intent intent) {
        String action = intent.getAction();
        if (ACTION_WIDGET_RECEIVER.equals(action)) {
            listAppsFragment.init();
            final Handler handler = new Handler();
            Thread t = new Thread(new Runnable() {

                class C05113 implements Runnable {
                    C05113() {
                    }

                    public void run() {
                        AppWidgetManager gm = AppWidgetManager.getInstance(context);
                        AppDisablerWidget.this.onUpdate(context, gm, gm.getAppWidgetIds(new ComponentName(context, AppDisablerWidget.class)));
                    }
                }

                public void run() {
                    try {
                        PackageManager pm = context.getPackageManager();
                        String pkgName = BuildConfig.VERSION_NAME;
                        int id = intent.getIntExtra("appWidgetId", -1);
                        if (id != -1 && !AppDisablerWidgetConfigureActivity.loadTitlePref(context, id).equals("NOT_SAVED_APP_DISABLER")) {
                            pkgName = AppDisablerWidgetConfigureActivity.loadTitlePref(context, id);
                            RemoteViews remoteViews = new RemoteViews(context.getPackageName(), C0149R.layout.app_disabler_widget);
                            String result = BuildConfig.VERSION_NAME;
                            final String fin_item;
                            if (pm.getPackageInfo(pkgName, 0).applicationInfo.enabled) {
                                result = new Utils(BuildConfig.VERSION_NAME).cmdRoot("pm disable " + pkgName);
                                remoteViews.setInt(C0149R.id.toggleButton_off, "setBackgroundResource", C0149R.drawable.switch_off);
                                remoteViews.setTextColor(C0149R.id.appwidget_text, Color.parseColor("#FF0000"));
                                fin_item = pkgName;
                                handler.post(new Runnable() {
                                    public void run() {
                                        Toast.makeText(context, "OFF " + fin_item, 0).show();
                                        AppWidgetManager gm = AppWidgetManager.getInstance(context);
                                        AppDisablerWidget.this.onUpdate(context, gm, gm.getAppWidgetIds(new ComponentName(context, AppDisablerWidget.class)));
                                    }
                                });
                                return;
                            }
                            result = new Utils(BuildConfig.VERSION_NAME).cmdRoot("pm enable " + pkgName);
                            remoteViews.setTextColor(C0149R.id.appwidget_text, Color.parseColor("#00FF00"));
                            remoteViews.setInt(C0149R.id.toggleButton_off, "setBackgroundResource", C0149R.drawable.switch_on);
                            fin_item = pkgName;
                            handler.post(new Runnable() {
                                public void run() {
                                    Toast.makeText(context, "ON " + fin_item, 0).show();
                                    AppWidgetManager gm = AppWidgetManager.getInstance(context);
                                    AppDisablerWidget.this.onUpdate(context, gm, gm.getAppWidgetIds(new ComponentName(context, AppDisablerWidget.class)));
                                }
                            });
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        RemoteViews views = new RemoteViews(context.getPackageName(), C0149R.layout.app_disabler_widget);
                        views.setBoolean(C0149R.id.toggleButton_off, "setEnabled", false);
                        views.setInt(C0149R.id.toggleButton_off, "setBackgroundResource", C0149R.drawable.switch_off);
                        views.setTextColor(C0149R.id.appwidget_text, Color.parseColor("#AAAAAA"));
                        views.setTextViewText(C0149R.id.appwidget_text, Utils.getText(C0149R.string.app_disabler_widget_pkg_not_found));
                        handler.post(new C05113());
                    }
                }
            });
            t.setPriority(10);
            t.start();
        }
        if (ACTION_WIDGET_RECEIVER_Updater.equals(action)) {
            try {
                listAppsFragment.appDisabler = true;
                AppWidgetManager gm = AppWidgetManager.getInstance(context);
                onUpdate(context, gm, gm.getAppWidgetIds(new ComponentName(context, AppDisablerWidget.class)));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        super.onReceive(context, intent);
    }
}
