package com.android.vending.billing.InAppBillingService.LUCK.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import com.android.vending.billing.InAppBillingService.LUCK.AlertDlg;
import com.android.vending.billing.InAppBillingService.LUCK.C0149R;
import com.android.vending.billing.InAppBillingService.LUCK.LogCollector;
import com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment;
import com.chelpus.Utils;
import org.json.JSONException;
import org.json.JSONObject;

public class Ext_Patch_Dialog {
    Dialog dialog = null;

    class C01981 implements OnItemSelectedListener {
        C01981() {
        }

        public void onItemSelected(AdapterView<?> adapterView, View selectedItemView, int position, long id) {
            if (position != 0) {
                listAppsFragment.CurentSelect = position;
            }
            listAppsFragment.tvt.invalidate();
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }

    class C01992 implements OnCancelListener {
        C01992() {
        }

        public void onCancel(DialogInterface dialog) {
        }
    }

    public void showDialog() {
        if (this.dialog == null) {
            this.dialog = onCreateDialog();
        }
        if (this.dialog != null) {
            this.dialog.show();
        }
    }

    public Dialog onCreateDialog() {
        System.out.println("Ext Patch Dialog create.");
        if (listAppsFragment.frag == null || listAppsFragment.frag.getContext() == null) {
            dismiss();
        }
        LinearLayout d = (LinearLayout) View.inflate(listAppsFragment.frag.getContext(), C0149R.layout.extpatchdialog, null);
        LinearLayout body = (LinearLayout) d.findViewById(C0149R.id.extpatchbodyscroll).findViewById(C0149R.id.extdialogbodypatch);
        int objects = 0;
        int i = 0;
        if (listAppsFragment.str == null) {
            listAppsFragment.str = " ";
        }
        listAppsFragment.CurentSelect = 0;
        listAppsFragment.contextext = listAppsFragment.frag.getContext();
        String value = null;
        String[] qwerty = new String[listAppsFragment.str.split(LogCollector.LINE_SEPARATOR).length];
        qwerty = listAppsFragment.str.split(LogCollector.LINE_SEPARATOR);
        boolean json = true;
        while (json && i < qwerty.length) {
            try {
                value = new JSONObject(qwerty[i]).getString("objects");
                json = false;
                i = 0;
            } catch (JSONException e) {
                json = true;
                i++;
                objects = 0;
            }
        }
        if (value != null) {
            objects = Integer.parseInt(value);
        }
        String[] array_spinner = null;
        if (objects != 0) {
            array_spinner = new String[(objects + 1)];
            while (i < array_spinner.length) {
                if (i == 0) {
                    array_spinner[0] = "Please Select";
                } else {
                    array_spinner[i] = "Object N" + i;
                }
                i++;
            }
        }
        String str2 = Utils.getText(C0149R.string.extpat1) + " " + objects + " " + Utils.getText(C0149R.string.extpat2);
        listAppsFragment.tvt = (TextView) body.findViewById(C0149R.id.doetogo);
        listAppsFragment.tvt.append(Utils.getColoredText(str2, -16711821, "bold"));
        Spinner sp = (Spinner) body.findViewById(C0149R.id.spinner1);
        if (array_spinner != null) {
            listAppsFragment.CurentSelect = 0;
            sp.setAdapter(new ArrayAdapter(listAppsFragment.frag.getContext(), 17367048, array_spinner));
            sp.setOnItemSelectedListener(new C01981());
        }
        if (objects == 0) {
            sp.setEnabled(false);
            listAppsFragment.CurentSelect = 0;
        }
        str2 = Utils.getText(C0149R.string.extpat3);
        listAppsFragment.tvt = (TextView) body.findViewById(C0149R.id.posle);
        listAppsFragment.tvt.append(Utils.getColoredText(str2, -990142, "bold"));
        float a = 0.0f + 1.0f;
        if (!listAppsFragment.str.contains("SU Java-Code Running!")) {
            str2 = LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.no_root) + LogCollector.LINE_SEPARATOR;
            listAppsFragment.tvt = (TextView) body.findViewById(C0149R.id.posle);
            listAppsFragment.tvt.append(Utils.getColoredText(str2, -16711681, "bold"));
        }
        Dialog tempdialog = new AlertDlg(listAppsFragment.frag.getContext(), true).setView(d).create();
        tempdialog.setTitle(Utils.getText(C0149R.string.PatchResult));
        tempdialog.setCancelable(true);
        tempdialog.setOnCancelListener(new C01992());
        return tempdialog;
    }

    public void dismiss() {
        if (this.dialog != null) {
            this.dialog.dismiss();
            this.dialog = null;
        }
    }
}
