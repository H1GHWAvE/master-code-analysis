package com.android.vending.billing.InAppBillingService.LUCK;

import java.util.List;

public class MenuItem {
    public String booleanPref = null;
    public boolean booleanPrefDef = false;
    public List<Integer> childs;
    public boolean icon = false;
    public int punkt_menu;
    public int punkt_menu_descr = 0;
    public Runnable runCode = null;
    public int type = 0;
    public int typeChild = 0;

    public MenuItem(int group, List<Integer> childs_menu, boolean hasIcon) {
        this.punkt_menu = group;
        this.childs = childs_menu;
        this.icon = hasIcon;
    }

    public MenuItem(int group, List<Integer> childs_menu, int type, boolean hasIcon) {
        this.punkt_menu = group;
        this.childs = childs_menu;
        this.icon = hasIcon;
        this.type = type;
    }

    public MenuItem(int group, int descr, List<Integer> childs_menu, int type, boolean hasIcon) {
        this.punkt_menu = group;
        this.childs = childs_menu;
        this.icon = hasIcon;
        this.type = type;
        this.punkt_menu_descr = descr;
    }

    public MenuItem(int group, int descr, List<Integer> childs_menu, int type, boolean hasIcon, String booleanPref, boolean defBoolPref, Runnable runCode) {
        this.punkt_menu = group;
        this.childs = childs_menu;
        this.icon = hasIcon;
        this.type = type;
        this.punkt_menu_descr = descr;
        this.runCode = runCode;
        this.booleanPref = booleanPref;
        this.booleanPrefDef = defBoolPref;
    }

    public MenuItem(int group, List<Integer> childs_menu, int type, int typeChield, boolean hasIcon) {
        this.punkt_menu = group;
        this.childs = childs_menu;
        this.icon = hasIcon;
        this.type = type;
        this.typeChild = typeChield;
    }

    public MenuItem(int group, int descr, List<Integer> childs_menu, int type, int typeChield, boolean hasIcon) {
        this.punkt_menu = group;
        this.childs = childs_menu;
        this.icon = hasIcon;
        this.type = type;
        this.typeChild = typeChield;
        this.punkt_menu_descr = descr;
    }
}
