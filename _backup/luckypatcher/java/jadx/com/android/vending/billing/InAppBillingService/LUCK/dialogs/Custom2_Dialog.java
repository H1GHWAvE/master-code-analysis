package com.android.vending.billing.InAppBillingService.LUCK.dialogs;

import android.app.Dialog;
import android.content.pm.PackageManager.NameNotFoundException;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.android.vending.billing.InAppBillingService.LUCK.AlertDlg;
import com.android.vending.billing.InAppBillingService.LUCK.BuildConfig;
import com.android.vending.billing.InAppBillingService.LUCK.C0149R;
import com.android.vending.billing.InAppBillingService.LUCK.LogCollector;
import com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment;
import com.chelpus.Utils;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

public class Custom2_Dialog {
    Dialog dialog = null;

    public void showDialog() {
        if (this.dialog == null) {
            this.dialog = onCreateDialog();
        }
        if (this.dialog != null) {
            this.dialog.show();
        }
    }

    public Dialog onCreateDialog() {
        RelativeLayout d = (RelativeLayout) View.inflate(listAppsFragment.frag.getContext(), C0149R.layout.customdialog, null);
        System.out.println("Custom2 Dialog create.");
        if (listAppsFragment.frag == null || listAppsFragment.frag.getContext() == null) {
            dismiss();
        }
        RelativeLayout scrbody = (RelativeLayout) d.findViewById(C0149R.id.appbodyscroll).findViewById(C0149R.id.appdialogbodycustom);
        d.findViewById(C0149R.id.relativeLayout1);
        try {
            TextView txt2 = (TextView) scrbody.findViewById(C0149R.id.app_textView2);
            scrbody.findViewById(C0149R.id.app_textView3);
            txt2.setText(Utils.getColoredText("-----------------------------------------------\n", BuildConfig.VERSION_NAME, "bold"));
            txt2.append(Utils.getColoredText(Utils.getText(C0149R.string.custom_sel_app) + LogCollector.LINE_SEPARATOR, -16711821, "bold"));
            txt2.append(Utils.getColoredText("-----------------------------------------------\n", BuildConfig.VERSION_NAME, "bold"));
            if (listAppsFragment.rebuldApk.equals(BuildConfig.VERSION_NAME)) {
                txt2.append(Utils.getColoredText(LogCollector.LINE_SEPARATOR + listAppsFragment.pli.name, -16711821, "bold"));
                txt2.append(Utils.getColoredText(LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.statversion) + " ", -16711821, "bold"));
                txt2.append(Utils.getColoredText(listAppsFragment.getPkgMng().getPackageInfo(listAppsFragment.pli.pkgName, 0).versionName, -16711821, "bold"));
                txt2.append(Utils.getColoredText(LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.statbuild) + " ", -16711821, "bold"));
                txt2.append(Utils.getColoredText(BuildConfig.VERSION_NAME + listAppsFragment.getPkgMng().getPackageInfo(listAppsFragment.pli.pkgName, 0).versionCode + LogCollector.LINE_SEPARATOR, -16711821, "bold"));
            } else {
                txt2.append(Utils.getColoredText(LogCollector.LINE_SEPARATOR + Utils.getApkLabelName(listAppsFragment.rebuldApk), -16711821, "bold"));
                txt2.append(Utils.getColoredText(LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.statversion) + " ", -16711821, "bold"));
                txt2.append(Utils.getColoredText(Utils.getApkPackageInfo(listAppsFragment.rebuldApk).versionName, -16711821, "bold"));
                txt2.append(Utils.getColoredText(LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.statbuild) + " ", -16711821, "bold"));
                txt2.append(Utils.getColoredText(BuildConfig.VERSION_NAME + Utils.getApkPackageInfo(listAppsFragment.rebuldApk).versionCode + LogCollector.LINE_SEPARATOR, -16711821, "bold"));
            }
            txt2.append(Utils.getColoredText("\n-----------------------------------------------\n", BuildConfig.VERSION_NAME, "bold"));
            String str = BuildConfig.VERSION_NAME;
            try {
                FileInputStream fis = new FileInputStream(listAppsFragment.customselect.toString());
                BufferedReader br = new BufferedReader(new InputStreamReader(fis, "UTF-8"));
                String data = BuildConfig.VERSION_NAME;
                String[] txtdata = new String[2000];
                boolean begin = false;
                while (true) {
                    data = br.readLine();
                    if (data == null) {
                        break;
                    }
                    txtdata[0] = data;
                    if (begin && (txtdata[0].contains("[") || txtdata[0].contains("]") || txtdata[0].contains("{"))) {
                        System.out.println(BuildConfig.VERSION_NAME + str + LogCollector.LINE_SEPARATOR);
                        begin = false;
                    }
                    if (begin) {
                        str = str + LogCollector.LINE_SEPARATOR + txtdata[0];
                    }
                    if (txtdata[0].toUpperCase().contains("[BEGIN]")) {
                        begin = true;
                    }
                }
                fis.close();
            } catch (FileNotFoundException e) {
                System.out.println("Custom Patch not Found in\n/sdcard/LuckyPatcher/\n");
            } catch (IOException e2) {
                System.out.println(BuildConfig.VERSION_NAME + e2);
            }
            txt2.append(Utils.getColoredText(Utils.getText(C0149R.string.custom_descript) + LogCollector.LINE_SEPARATOR, -990142, "bold"));
            txt2.append(Utils.getColoredText("-----------------------------------------------\n", BuildConfig.VERSION_NAME, "bold"));
            txt2.append(Utils.getColoredText(str, -990142, "bold"));
        } catch (NullPointerException e3) {
        } catch (NameNotFoundException e4) {
            e4.printStackTrace();
        }
        return new AlertDlg(listAppsFragment.frag.getContext()).setCancelable(true).setView(d).create();
    }

    public void dismiss() {
        if (this.dialog != null) {
            this.dialog.dismiss();
            this.dialog = null;
        }
    }
}
