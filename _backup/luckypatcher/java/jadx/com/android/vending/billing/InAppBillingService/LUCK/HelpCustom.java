package com.android.vending.billing.InAppBillingService.LUCK;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import pxb.android.ResConst;

public class HelpCustom extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        WebView webview = new WebView(this);
        setContentView(webview);
        webview.getSettings().setDefaultFontSize(14);
        InputStream inputStream = getResources().openRawResource(C0149R.raw.help_custom_patch);
        byte[] buffer = new byte[ResConst.RES_TABLE_PACKAGE_TYPE];
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        while (true) {
            try {
                int len1 = inputStream.read(buffer);
                if (len1 != -1) {
                    baos.write(buffer, 0, len1);
                } else {
                    webview.loadData(baos.toString(), "text/html; charset=UTF-8", null);
                    return;
                }
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }
        }
    }
}
