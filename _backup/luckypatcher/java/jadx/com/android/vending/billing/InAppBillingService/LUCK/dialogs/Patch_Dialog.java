package com.android.vending.billing.InAppBillingService.LUCK.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.android.vending.billing.InAppBillingService.LUCK.AlertDlg;
import com.android.vending.billing.InAppBillingService.LUCK.C0149R;
import com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment;
import com.chelpus.Utils;

public class Patch_Dialog {
    Dialog dialog = null;

    class C02151 implements OnClickListener {

        class C02141 implements Runnable {

            class C02131 implements Runnable {
                C02131() {
                }

                public void run() {
                    Intent i = listAppsFragment.getPkgMng().getLaunchIntentForPackage(listAppsFragment.pli.pkgName);
                    if (listAppsFragment.patchAct != null) {
                        listAppsFragment.patchAct.startActivity(i);
                    }
                }
            }

            C02141() {
            }

            public void run() {
                try {
                    Utils.kill(listAppsFragment.pli.pkgName);
                    Utils.run_all("killall " + listAppsFragment.pli.pkgName);
                    listAppsFragment.runToMainStatic(new C02131());
                } catch (Exception e) {
                    Toast.makeText(listAppsFragment.getInstance(), Utils.getText(C0149R.string.error_launch), 1).show();
                }
            }
        }

        C02151() {
        }

        public void onClick(DialogInterface dialog, int which) {
            new Thread(new C02141()).start();
            Patch_Dialog.this.dismiss();
        }
    }

    public void showDialog() {
        if (this.dialog == null) {
            this.dialog = onCreateDialog();
        }
        if (this.dialog != null) {
            this.dialog.show();
        }
    }

    public Dialog onCreateDialog() {
        System.out.println("Patch Dialog create.");
        if (listAppsFragment.frag == null || listAppsFragment.frag.getContext() == null) {
            dismiss();
        }
        LinearLayout d = (LinearLayout) View.inflate(listAppsFragment.frag.getContext(), C0149R.layout.patchdialog, null);
        TextView tv = (TextView) ((LinearLayout) d.findViewById(C0149R.id.patchbodyscroll).findViewById(C0149R.id.dialogbodypatch)).findViewById(C0149R.id.patchdesc);
        try {
            listAppsFragment com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment = listAppsFragment.frag;
            listAppsFragment.patch_dialog_text_builder(tv, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        AlertDlg builder = new AlertDlg(listAppsFragment.frag.getContext());
        builder.setIcon(C0149R.drawable.ic_angel);
        builder.setTitle(Utils.getText(C0149R.string.PatchResult));
        return builder.setCancelable(true).setPositiveButton(Utils.getText(17039370), null).setNeutralButton((int) C0149R.string.launchbutton, new C02151()).setView(d).create();
    }

    public void dismiss() {
        if (this.dialog != null) {
            this.dialog.dismiss();
            this.dialog = null;
        }
    }
}
