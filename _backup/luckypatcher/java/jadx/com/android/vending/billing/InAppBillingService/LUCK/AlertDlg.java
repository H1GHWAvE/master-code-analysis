package com.android.vending.billing.InAppBillingService.LUCK;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.GradientDrawable.Orientation;
import android.os.Bundle;
import android.support.v4.view.MotionEventCompat;
import android.text.SpannableString;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import org.tukaani.xz.LZMA2Options;
import pxb.android.axml.AxmlParser;

public class AlertDlg {
    public ArrayAdapter adapter = null;
    public Context context = null;
    public Dialog dialog = null;
    public boolean not_close = false;
    public View root = null;

    class C00481 implements OnGlobalLayoutListener {
        int displayWidth = 0;
        int orient = MotionEventCompat.ACTION_MASK;
        int setForSize = 0;

        C00481() {
        }

        public void onGlobalLayout() {
            Window w = AlertDlg.this.dialog.getWindow();
            int current_width = w.getDecorView().getWidth();
            int display_height = listAppsFragment.getRes().getDisplayMetrics().heightPixels;
            int display_width = listAppsFragment.getRes().getDisplayMetrics().widthPixels;
            if (display_height > display_width) {
                this.orient = 0;
            } else {
                this.orient = 1;
            }
            if (this.displayWidth == 0 || this.displayWidth != display_width) {
                this.setForSize = 0;
                this.displayWidth = display_width;
            }
            if (this.setForSize == 0 || current_width != this.setForSize) {
                w.setGravity(17);
                LayoutParams lp = w.getAttributes();
                int max_width = (int) (((double) display_width) * 0.98d);
                int min_width = (int) (((double) display_width) * 0.75d);
                System.out.println("wight:" + current_width);
                switch (this.orient) {
                    case LZMA2Options.MODE_UNCOMPRESSED /*0*/:
                        if (display_width >= 900) {
                            if (current_width < max_width) {
                                if (current_width >= min_width) {
                                    lp.width = current_width;
                                    break;
                                }
                                lp.width = min_width;
                                this.setForSize = min_width;
                                break;
                            }
                            lp.width = max_width;
                            this.setForSize = max_width;
                            break;
                        } else if (current_width < min_width) {
                            lp.width = min_width;
                            this.setForSize = min_width;
                            break;
                        } else {
                            lp.width = max_width;
                            this.setForSize = max_width;
                            break;
                        }
                    case AxmlParser.START_FILE /*1*/:
                        if (display_width >= 900) {
                            if (current_width >= min_width) {
                                lp.width = current_width;
                                break;
                            }
                            lp.width = min_width;
                            this.setForSize = min_width;
                            break;
                        } else if (current_width < min_width) {
                            lp.width = min_width;
                            this.setForSize = min_width;
                            break;
                        }
                        break;
                }
                this.setForSize = lp.width;
                w.setAttributes(lp);
            }
        }
    }

    public AlertDlg(Context context) {
        this.context = context;
        this.dialog = new Dialog(context);
        this.dialog.requestWindowFeature(1);
        this.dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        this.dialog.getWindow().getDecorView().getViewTreeObserver().addOnGlobalLayoutListener(new C00481());
        this.dialog.setContentView(C0149R.layout.alert_dialog);
        this.dialog.setCancelable(true);
    }

    public AlertDlg(Context context, int style) {
        this.context = context;
        this.dialog = new Dialog(context, style);
        this.dialog.setContentView(C0149R.layout.alert_dialog);
        this.dialog.setCancelable(true);
    }

    public AlertDlg(Context context, boolean fullscreen) {
        this.context = context;
        this.dialog = new Dialog(context, C0149R.style.full_screen_dialog) {
            protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                ((LinearLayout) findViewById(C0149R.id.alert_dialog_body)).setBackgroundColor(Color.parseColor("#000000"));
                getWindow().setLayout(-1, -1);
            }
        };
        this.dialog.setContentView(C0149R.layout.alert_dialog);
        this.dialog.setCancelable(true);
    }

    public AlertDlg setView(View view) {
        LinearLayout button_layout = (LinearLayout) this.dialog.findViewById(C0149R.id.button_alert_layout);
        ((LinearLayout) this.dialog.findViewById(C0149R.id.message_layout)).addView(view);
        return this;
    }

    public AlertDlg setIcon(int id) {
        ((LinearLayout) this.dialog.findViewById(C0149R.id.title_layout)).setVisibility(0);
        ImageView icon = (ImageView) this.dialog.findViewById(C0149R.id.title_icon);
        icon.setVisibility(0);
        icon.setImageDrawable(this.context.getResources().getDrawable(id));
        ((TextView) this.dialog.findViewById(C0149R.id.line0)).setVisibility(0);
        return this;
    }

    public AlertDlg setTitle(String str) {
        ((LinearLayout) this.dialog.findViewById(C0149R.id.title_layout)).setVisibility(0);
        ((TextView) this.dialog.findViewById(C0149R.id.title_text)).setText(str);
        ((TextView) this.dialog.findViewById(C0149R.id.line0)).setVisibility(0);
        return this;
    }

    public AlertDlg setTitle(int str) {
        ((LinearLayout) this.dialog.findViewById(C0149R.id.title_layout)).setVisibility(0);
        ((TextView) this.dialog.findViewById(C0149R.id.title_text)).setText(str);
        return this;
    }

    public AlertDlg setMessage(String message) {
        TextView title = (TextView) this.dialog.findViewById(C0149R.id.dialog_message);
        ((ScrollView) this.dialog.findViewById(C0149R.id.dialog_message_container)).setVisibility(0);
        title.setMovementMethod(new ScrollingMovementMethod());
        title.setText(message);
        return this;
    }

    public AlertDlg setMessage(SpannableString message) {
        TextView title = (TextView) this.dialog.findViewById(C0149R.id.dialog_message);
        ((ScrollView) this.dialog.findViewById(C0149R.id.dialog_message_container)).setVisibility(0);
        title.setMovementMethod(new ScrollingMovementMethod());
        title.setText(message);
        return this;
    }

    public AlertDlg setCancelable(boolean cancel) {
        this.dialog.setCancelable(cancel);
        return this;
    }

    public AlertDlg setPositiveButton(String text, final OnClickListener click_listner) {
        Button button = (Button) this.dialog.findViewById(C0149R.id.button_yes);
        button.setVisibility(0);
        button.setText(text);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (click_listner != null) {
                    click_listner.onClick(AlertDlg.this.dialog, -1);
                }
                AlertDlg.this.dialog.dismiss();
            }
        });
        return this;
    }

    public AlertDlg setNeutralButton(String text, final OnClickListener click_listner) {
        Button button = (Button) this.dialog.findViewById(C0149R.id.button_neutral);
        button.setVisibility(0);
        button.setText(text);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (click_listner != null) {
                    click_listner.onClick(AlertDlg.this.dialog, -3);
                }
                AlertDlg.this.dialog.dismiss();
            }
        });
        return this;
    }

    public AlertDlg setNegativeButton(String text, final OnClickListener click_listner) {
        Button button = (Button) this.dialog.findViewById(C0149R.id.button_no);
        button.setVisibility(0);
        button.setText(text);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (click_listner != null) {
                    click_listner.onClick(AlertDlg.this.dialog, -2);
                }
                AlertDlg.this.dialog.dismiss();
            }
        });
        return this;
    }

    public AlertDlg setPositiveButton(int text, final OnClickListener click_listner) {
        Button button = (Button) this.dialog.findViewById(C0149R.id.button_yes);
        button.setVisibility(0);
        button.setText(text);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (click_listner != null) {
                    click_listner.onClick(AlertDlg.this.dialog, -1);
                }
                AlertDlg.this.dialog.dismiss();
            }
        });
        return this;
    }

    public AlertDlg setNeutralButton(int text, final OnClickListener click_listner) {
        Button button = (Button) this.dialog.findViewById(C0149R.id.button_neutral);
        button.setVisibility(0);
        button.setText(text);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (click_listner != null) {
                    click_listner.onClick(AlertDlg.this.dialog, -3);
                }
                AlertDlg.this.dialog.dismiss();
            }
        });
        return this;
    }

    public AlertDlg setNegativeButton(int text, final OnClickListener click_listner) {
        Button button = (Button) this.dialog.findViewById(C0149R.id.button_no);
        button.setVisibility(0);
        button.setText(text);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (click_listner != null) {
                    click_listner.onClick(AlertDlg.this.dialog, -2);
                }
                AlertDlg.this.dialog.dismiss();
            }
        });
        return this;
    }

    public AlertDlg setOnCancelListener(OnCancelListener cancel_listner) {
        this.dialog.setOnCancelListener(cancel_listner);
        return this;
    }

    public AlertDlg setAdapter(final ArrayAdapter array_adapter, final OnItemClickListener ok_l) {
        this.adapter = array_adapter;
        ListView list = (ListView) this.dialog.findViewById(C0149R.id.dialog_listview);
        LinearLayout button_layout = (LinearLayout) this.dialog.findViewById(C0149R.id.button_alert_layout);
        list.setVisibility(0);
        list.setAdapter(array_adapter);
        list.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ok_l.onItemClick(parent, view, position, id);
                array_adapter.notifyDataSetChanged();
                if (!AlertDlg.this.not_close) {
                    AlertDlg.this.dialog.dismiss();
                }
            }
        });
        list.setDivider(new GradientDrawable(Orientation.RIGHT_LEFT, new int[]{868199252, 868199252, 868199252}));
        list.setDividerHeight(3);
        return this;
    }

    public void setAdapterNotClose(boolean bool) {
        this.not_close = bool;
    }

    public Dialog create() {
        return this.dialog;
    }
}
