package com.android.vending.billing.InAppBillingService.LUCK;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class OnAlarmReceiver extends BroadcastReceiver {
    public static String ACTION_WIDGET_RECEIVER = "ActionOnTimeLucky";

    public void onReceive(Context ctx, final Intent intent) {
        new Thread(new Runnable() {
            public void run() {
                System.out.println(intent.getAction());
                if (intent.getAction().equals(OnAlarmReceiver.ACTION_WIDGET_RECEIVER)) {
                    listAppsFragment.getConfig().edit().putBoolean("trigger_for_good_android_patch_on_boot", false).commit();
                    if (listAppsFragment.su && listAppsFragment.getConfig().getBoolean("OnBootService", false)) {
                        listAppsFragment.getConfig().edit().putBoolean("OnBootService", false).commit();
                        listAppsFragment.patchOnBoot = true;
                        listAppsFragment.getInstance().startService(new Intent(listAppsFragment.getInstance(), PatchService.class));
                    }
                }
            }
        }).start();
    }
}
