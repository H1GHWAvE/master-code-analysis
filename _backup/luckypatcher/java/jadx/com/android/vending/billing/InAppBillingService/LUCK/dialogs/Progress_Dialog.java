package com.android.vending.billing.InAppBillingService.LUCK.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.support.v4.app.FragmentManager;
import com.android.vending.billing.InAppBillingService.LUCK.BuildConfig;
import com.android.vending.billing.InAppBillingService.LUCK.C0149R;
import com.android.vending.billing.InAppBillingService.LUCK.ProgressDlg;
import com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment;
import com.chelpus.Utils;

public class Progress_Dialog {
    public static ProgressDlg dialog;
    Dialog dialog2 = null;
    FragmentManager fm = null;
    String message = BuildConfig.VERSION_NAME;
    String title = BuildConfig.VERSION_NAME;

    class C02161 implements OnCancelListener {
        C02161() {
        }

        public void onCancel(DialogInterface dialog) {
            if (listAppsFragment.su) {
                Utils.exitRoot();
            }
        }
    }

    class C02172 implements Runnable {
        C02172() {
        }

        public void run() {
            listAppsFragment.frag.getChildFragmentManager().executePendingTransactions();
        }
    }

    class C02183 implements Runnable {
        C02183() {
        }

        public void run() {
            listAppsFragment.frag.getChildFragmentManager().executePendingTransactions();
        }
    }

    class C02194 implements Runnable {
        C02194() {
        }

        public void run() {
            listAppsFragment.frag.getChildFragmentManager().executePendingTransactions();
        }
    }

    class C02205 implements Runnable {
        C02205() {
        }

        public void run() {
            listAppsFragment.frag.getChildFragmentManager().executePendingTransactions();
        }
    }

    class C02216 implements Runnable {
        C02216() {
        }

        public void run() {
            listAppsFragment.frag.getChildFragmentManager().executePendingTransactions();
        }
    }

    public static Progress_Dialog newInstance() {
        return new Progress_Dialog();
    }

    public void showDialog() {
        if (this.dialog2 == null) {
            this.dialog2 = onCreateDialog();
        }
        if (this.dialog2 != null) {
            this.dialog2.show();
        }
    }

    public Dialog onCreateDialog() {
        dialog = new ProgressDlg(listAppsFragment.frag.getContext());
        if (this.title.equals(BuildConfig.VERSION_NAME)) {
            this.title = Utils.getText(C0149R.string.wait);
        }
        dialog.setTitle(this.title);
        if (this.message.equals(BuildConfig.VERSION_NAME)) {
            this.message = Utils.getText(C0149R.string.loadpkg);
        }
        dialog.setMessage(this.message);
        dialog.setCancelable(true);
        dialog.setOnCancelListener(new C02161());
        return dialog.create();
    }

    public void setMessage(String text) {
        if (dialog == null) {
            onCreateDialog();
        }
        this.message = text;
        dialog.setMessage(this.message);
        listAppsFragment.frag.getContext().runOnUiThread(new C02172());
    }

    public void setIndeterminate(boolean indeterminate) {
        if (dialog == null) {
            onCreateDialog();
        }
        if (indeterminate) {
            dialog.setDefaultStyle();
        } else {
            dialog.setIncrementStyle();
        }
        listAppsFragment.frag.getContext().runOnUiThread(new C02183());
    }

    public void setMax(int max) {
        if (dialog == null) {
            onCreateDialog();
        }
        dialog.setMax(max);
        listAppsFragment.frag.getContext().runOnUiThread(new C02194());
    }

    public void setProgress(int progress) {
        if (dialog == null) {
            onCreateDialog();
        }
        dialog.setProgress(progress);
        listAppsFragment.frag.getContext().runOnUiThread(new C02205());
    }

    public boolean isShowing() {
        if (this.dialog2 == null || !this.dialog2.isShowing()) {
            return false;
        }
        return true;
    }

    public void setTitle(String text) {
        if (dialog == null) {
            onCreateDialog();
        }
        this.title = text;
        dialog.setTitle(this.title);
        listAppsFragment.frag.getContext().runOnUiThread(new C02216());
    }

    public void setCancelable(boolean trig) {
        if (this.dialog2 != null) {
            this.dialog2.setCancelable(trig);
        }
    }

    public void dismiss() {
        if (this.dialog2 != null) {
            this.dialog2.dismiss();
            this.dialog2 = null;
        }
    }
}
