package com.android.vending.billing.InAppBillingService.LUCK;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.widget.Toast;

public class InstallApp extends Activity {
    public Context context;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Toast.makeText(this, BuildConfig.VERSION_NAME + getIntent().getDataString(), 0).show();
    }
}
