package com.android.vending.billing.InAppBillingService.LUCK;

import android.app.ActivityManager;
import android.app.ActivityManager.MemoryInfo;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnKeyListener;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.media.TransportMediator;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.android.vending.billing.InAppBillingService.LUCK.dialogs.FilterFragment;
import com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment.ItemFile;
import com.chelpus.Common;
import com.chelpus.Utils;
import com.google.android.finsky.billing.iab.InAppBillingService;
import com.google.android.finsky.billing.iab.MarketBillingService;
import com.google.android.finsky.services.LicensingService;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import net.lingala.zip4j.util.InternalZipConstants;
import net.lingala.zip4j.util.Zip4jConstants;
import org.json.JSONException;
import org.json.JSONObject;

public class patchActivity extends FragmentActivity {
    public static final int APP_DIALOG = 6;
    public static final int CONTEXT_DIALOG = 7;
    public static final int CREATE_APK = 0;
    public static final int CUSTOM2_DIALOG = 15;
    public static final int CUSTOM_PATCH = 1;
    public static final int DIALOG_REPORT_FORCE_CLOSE = 3535788;
    public static final int MARKET_INSTALL_DIALOG = 30;
    public static final int PROGRESS_DIALOG2 = 11;
    public static final int RESTORE_FROM_BACKUP = 28;
    private static final int SETTINGS_ORIENT_LANDSCAPE = 1;
    private static final int SETTINGS_ORIENT_PORTRET = 2;
    public static final int SETTINGS_VIEWSIZE_DEFAULT = 0;
    private static final int SETTINGS_VIEWSIZE_SMALL = 0;
    public static listAppsFragment frag = null;
    boolean mIsRestoredToTop = false;

    class C04851 implements OnCancelListener {
        C04851() {
        }

        public void onCancel(DialogInterface dialogInterface) {
            patchActivity.this.finish();
            System.exit(patchActivity.CREATE_APK);
        }
    }

    class C04942 implements OnClickListener {
        C04942() {
        }

        public void onClick(DialogInterface dialog, int which) {
            patchActivity.this.finish();
            System.exit(patchActivity.CREATE_APK);
        }
    }

    class C04983 implements OnClickListener {

        class C04971 extends AsyncTask<Void, Void, Boolean> {

            class C04951 implements OnCancelListener {
                C04951() {
                }

                public void onCancel(DialogInterface dialogInterface) {
                    patchActivity.this.finish();
                    System.exit(patchActivity.CREATE_APK);
                }
            }

            class C04962 implements OnClickListener {
                C04962() {
                }

                public void onClick(DialogInterface dialogInterface, int i) {
                    patchActivity.this.finish();
                    System.exit(patchActivity.CREATE_APK);
                }
            }

            C04971() {
            }

            protected Boolean doInBackground(Void... params) {
                return Boolean.valueOf(new File(listAppsFragment.basepath + "/Log/log.txt").exists());
            }

            protected void onPreExecute() {
            }

            protected void onPostExecute(Boolean result) {
                if (result.booleanValue()) {
                    try {
                        listAppsFragment.mLogCollector.sendLog(patchActivity.this, "lp.chelpus@gmail.com", "Error Log", "Lucky Patcher " + patchActivity.this.getPackageManager().getPackageInfo(patchActivity.this.getPackageName(), patchActivity.CREATE_APK).versionName);
                    } catch (NameNotFoundException e) {
                        e.printStackTrace();
                    }
                    patchActivity.this.finish();
                    System.exit(patchActivity.CREATE_APK);
                    return;
                }
                Builder logbuilder2 = new Builder(patchActivity.this);
                logbuilder2.setTitle("Error").setMessage(Utils.getText(C0149R.string.error_collect_logs)).setNegativeButton("OK", new C04962()).setOnCancelListener(new C04951());
                logbuilder2.create().show();
            }
        }

        C04983() {
        }

        public void onClick(DialogInterface dialog, int which) {
            new C04971().execute(new Void[patchActivity.CREATE_APK]);
        }
    }

    class C04994 implements OnClickListener {
        C04994() {
        }

        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case Zip4jConstants.ENC_NO_ENCRYPTION /*-1*/:
                    patchActivity.this.finish();
                    System.exit(patchActivity.CREATE_APK);
                    return;
                default:
                    return;
            }
        }
    }

    class C05005 implements Runnable {
        C05005() {
        }

        public void run() {
            int location = listAppsFragment.getConfig().getInt("Install_location", 3);
            if (location == 3) {
                return;
            }
            if (listAppsFragment.su) {
                Utils utils = new Utils(BuildConfig.VERSION_NAME);
                String[] strArr = new String[patchActivity.SETTINGS_ORIENT_PORTRET];
                strArr[patchActivity.CREATE_APK] = "pm setInstallLocation " + location;
                strArr[patchActivity.SETTINGS_ORIENT_LANDSCAPE] = "skipOut";
                utils.cmdRoot(strArr);
                utils = new Utils(BuildConfig.VERSION_NAME);
                strArr = new String[patchActivity.SETTINGS_ORIENT_PORTRET];
                strArr[patchActivity.CREATE_APK] = "pm set-install-location " + location;
                strArr[patchActivity.SETTINGS_ORIENT_LANDSCAPE] = "skipOut";
                utils.cmdRoot(strArr);
                return;
            }
            if (VERSION.SDK_INT < 19) {
                String[] strArr2 = new String[patchActivity.SETTINGS_ORIENT_PORTRET];
                strArr2[patchActivity.CREATE_APK] = "pm setInstallLocation " + location;
                strArr2[patchActivity.SETTINGS_ORIENT_LANDSCAPE] = "skipOut";
                Utils.cmd(strArr2);
            }
            if (VERSION.SDK_INT < 19) {
                strArr2 = new String[patchActivity.SETTINGS_ORIENT_PORTRET];
                strArr2[patchActivity.CREATE_APK] = "pm set-install-location " + location;
                strArr2[patchActivity.SETTINGS_ORIENT_LANDSCAPE] = "skipOut";
                Utils.cmd(strArr2);
            }
        }
    }

    class C05046 implements Runnable {

        class C05022 implements Runnable {
            C05022() {
            }

            public void run() {
                listAppsFragment com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment = patchActivity.frag;
                listAppsFragment.removeDialogLP(38);
                com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment = patchActivity.frag;
                listAppsFragment.showDialogLP(38);
            }
        }

        class C05033 implements Runnable {
            C05033() {
            }

            public void run() {
                patchActivity.frag.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.unknown_error));
            }
        }

        C05046() {
        }

        public void run() {
            PackageInfo info = null;
            try {
                info = listAppsFragment.getPkgMng().getPackageInfo(Common.GOOGLEPLAY_PKG, patchActivity.CREATE_APK);
            } catch (NameNotFoundException e1) {
                e1.printStackTrace();
            } catch (IllegalArgumentException e12) {
                e12.printStackTrace();
            }
            ArrayList<Integer> items = new ArrayList();
            items.add(Integer.valueOf(C0149R.string.licensing_hack_menu));
            if (listAppsFragment.su && info != null) {
                items.add(Integer.valueOf(C0149R.string.disable_license_services_google));
            }
            items.add(Integer.valueOf(C0149R.string.billing_hack_menu));
            if (listAppsFragment.su && info != null) {
                items.add(Integer.valueOf(C0149R.string.disable_inapp_services_google));
                if (Utils.checkCoreJarPatch11() && Utils.checkCoreJarPatch12()) {
                    if (Utils.isXposedEnabled()) {
                        ArrayList<CoreItem> s = new ArrayList();
                        JSONObject settings = null;
                        try {
                            settings = Utils.readXposedParamBoolean();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        boolean Xposed4 = false;
                        if (settings != null) {
                            Xposed4 = settings.optBoolean("patch4", false);
                        }
                        if (!Xposed4) {
                            items.add(Integer.valueOf(C0149R.string.proxyGP));
                        }
                    } else {
                        items.add(Integer.valueOf(C0149R.string.proxyGP));
                    }
                }
            }
            items.add(Integer.valueOf(C0149R.string.switcher_auto_backup));
            items.add(Integer.valueOf(C0149R.string.switcher_auto_backup_only_gp));
            if (listAppsFragment.su) {
                items.add(Integer.valueOf(C0149R.string.switcher_auto_integrate_update));
                items.add(Integer.valueOf(C0149R.string.switcher_auto_move_to_sd));
                items.add(Integer.valueOf(C0149R.string.switcher_auto_move_to_internal));
            }
            if (items.size() != 0) {
                listAppsFragment.adapt = new ArrayAdapter<Integer>(patchActivity.this, C0149R.layout.switchers_context_menu, items) {
                    ArrayAdapter<Integer> mAdapter = this;

                    public View getView(int position, View convertView, ViewGroup parent) {
                        View row = convertView;
                        Integer current = (Integer) getItem(position);
                        if (row == null) {
                            row = ((LayoutInflater) listAppsFragment.getInstance().getSystemService("layout_inflater")).inflate(C0149R.layout.switchers_context_menu, parent, false);
                        }
                        TextView textView = (TextView) row.findViewById(C0149R.id.conttext);
                        ImageView icon = (ImageView) row.findViewById(C0149R.id.imgIcon);
                        icon.setImageDrawable(null);
                        textView.setTextAppearance(getContext(), listAppsFragment.getSizeText());
                        textView.setCompoundDrawablePadding((int) ((5.0f * listAppsFragment.getRes().getDisplayMetrics().density) + 0.5f));
                        textView.setTextColor(-1);
                        int d;
                        switch (current.intValue()) {
                            case C0149R.string.billing_hack_menu:
                                if (listAppsFragment.getInstance().getPackageManager().getComponentEnabledSetting(new ComponentName(listAppsFragment.getInstance(), MarketBillingService.class)) == patchActivity.SETTINGS_ORIENT_PORTRET || listAppsFragment.getInstance().getPackageManager().getComponentEnabledSetting(new ComponentName(listAppsFragment.getInstance(), InAppBillingService.class)) == patchActivity.SETTINGS_ORIENT_PORTRET) {
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.mipmap.vertical_switcher_off));
                                    textView.setText(Utils.getColoredText(Utils.getText(C0149R.string.billing_hack_menu), "#FF0000", "bold"));
                                } else {
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.mipmap.vertical_switcher_on));
                                    textView.setText(Utils.getColoredText(Utils.getText(C0149R.string.billing_hack_menu), "#00FF00", "bold"));
                                }
                                textView.append(LogCollector.LINE_SEPARATOR + Utils.getColoredText(Utils.getText(C0149R.string.billing_hack_menu_descr), "#AAAAAA", BuildConfig.VERSION_NAME));
                                break;
                            case C0149R.string.disable_inapp_services_google:
                                PackageInfo info = null;
                                try {
                                    info = listAppsFragment.getPkgMng().getPackageInfo(Common.GOOGLEPLAY_PKG, 4);
                                } catch (NameNotFoundException e1) {
                                    e1.printStackTrace();
                                }
                                if (!(info == null || info.services == null || info.services.length == 0)) {
                                    d = patchActivity.CREATE_APK;
                                    while (d < info.services.length) {
                                        try {
                                            if ((info.services[d].name.endsWith("InAppBillingService") || info.services[d].name.endsWith("MarketBillingService")) && listAppsFragment.getPkgMng().getComponentEnabledSetting(new ComponentName(Common.GOOGLEPLAY_PKG, info.services[d].name)) != patchActivity.SETTINGS_ORIENT_LANDSCAPE) {
                                                icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.mipmap.vertical_switcher_off));
                                                textView.setText(Utils.getColoredText(Utils.getText(C0149R.string.disable_inapp_services_google), "#FF0000", "bold"));
                                            }
                                            if ((info.services[d].name.endsWith("InAppBillingService") || info.services[d].name.endsWith("MarketBillingService")) && listAppsFragment.getPkgMng().getComponentEnabledSetting(new ComponentName(Common.GOOGLEPLAY_PKG, info.services[d].name)) == patchActivity.SETTINGS_ORIENT_LANDSCAPE) {
                                                icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.mipmap.vertical_switcher_on));
                                                textView.setText(Utils.getColoredText(Utils.getText(C0149R.string.disable_inapp_services_google), "#00FF00", "bold"));
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        d += patchActivity.SETTINGS_ORIENT_LANDSCAPE;
                                    }
                                }
                                textView.append(LogCollector.LINE_SEPARATOR + Utils.getColoredText(Utils.getText(C0149R.string.disable_inapp_services_google_descr), "#AAAAAA", BuildConfig.VERSION_NAME));
                                break;
                            case C0149R.string.disable_license_services_google:
                                PackageInfo info2 = null;
                                try {
                                    info2 = listAppsFragment.getPkgMng().getPackageInfo(Common.GOOGLEPLAY_PKG, 4);
                                } catch (NameNotFoundException e12) {
                                    e12.printStackTrace();
                                }
                                if (!(info2 == null || info2.services == null || info2.services.length == 0)) {
                                    d = patchActivity.CREATE_APK;
                                    while (d < info2.services.length) {
                                        try {
                                            if (info2.services[d].name.endsWith("LicensingService") && listAppsFragment.getPkgMng().getComponentEnabledSetting(new ComponentName(Common.GOOGLEPLAY_PKG, info2.services[d].name)) != patchActivity.SETTINGS_ORIENT_LANDSCAPE) {
                                                icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.mipmap.vertical_switcher_off));
                                                textView.setText(Utils.getColoredText(Utils.getText(C0149R.string.disable_license_services_google), "#FF0000", "bold"));
                                            }
                                            if (info2.services[d].name.endsWith("LicensingService") && listAppsFragment.getPkgMng().getComponentEnabledSetting(new ComponentName(Common.GOOGLEPLAY_PKG, info2.services[d].name)) == patchActivity.SETTINGS_ORIENT_LANDSCAPE) {
                                                icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.mipmap.vertical_switcher_on));
                                                textView.setText(Utils.getColoredText(Utils.getText(C0149R.string.disable_license_services_google), "#00FF00", "bold"));
                                            }
                                        } catch (Exception e2) {
                                            e2.printStackTrace();
                                        }
                                        d += patchActivity.SETTINGS_ORIENT_LANDSCAPE;
                                    }
                                }
                                textView.append(LogCollector.LINE_SEPARATOR + Utils.getColoredText(Utils.getText(C0149R.string.disable_license_services_google_descr), "#AAAAAA", BuildConfig.VERSION_NAME));
                                break;
                            case C0149R.string.licensing_hack_menu:
                                if (listAppsFragment.getInstance().getPackageManager().getComponentEnabledSetting(new ComponentName(listAppsFragment.getInstance(), LicensingService.class)) == patchActivity.SETTINGS_ORIENT_PORTRET) {
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.mipmap.vertical_switcher_off));
                                    textView.setText(Utils.getColoredText(Utils.getText(C0149R.string.licensing_hack_menu), "#FF0000", "bold"));
                                } else {
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.mipmap.vertical_switcher_on));
                                    textView.setText(Utils.getColoredText(Utils.getText(C0149R.string.licensing_hack_menu), "#00FF00", "bold"));
                                }
                                textView.append(LogCollector.LINE_SEPARATOR + Utils.getColoredText(Utils.getText(C0149R.string.licensing_hack_menu_descr), "#AAAAAA", BuildConfig.VERSION_NAME));
                                break;
                            case C0149R.string.switcher_auto_backup:
                                if (listAppsFragment.getConfig().getBoolean("switch_auto_backup_apk", false)) {
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.mipmap.vertical_switcher_on));
                                    textView.setText(Utils.getColoredText(Utils.getText(current.intValue()), "#00FF00", "bold"));
                                } else {
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.mipmap.vertical_switcher_off));
                                    textView.setText(Utils.getColoredText(Utils.getText(current.intValue()), "#FF0000", "bold"));
                                }
                                textView.append(LogCollector.LINE_SEPARATOR + Utils.getColoredText(Utils.getText(C0149R.string.switcher_auto_backup_descr), "#AAAAAA", BuildConfig.VERSION_NAME));
                                break;
                            case C0149R.string.switcher_auto_backup_only_gp:
                                if (listAppsFragment.getConfig().getBoolean("switch_auto_backup_apk_only_gp", false)) {
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.mipmap.vertical_switcher_on));
                                    textView.setText(Utils.getColoredText(Utils.getText(current.intValue()), "#00FF00", "bold"));
                                } else {
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.mipmap.vertical_switcher_off));
                                    textView.setText(Utils.getColoredText(Utils.getText(current.intValue()), "#FF0000", "bold"));
                                }
                                textView.append(LogCollector.LINE_SEPARATOR + Utils.getColoredText(Utils.getText(C0149R.string.switcher_auto_backup_only_gp_descr), "#AAAAAA", BuildConfig.VERSION_NAME));
                                break;
                            case C0149R.string.switcher_auto_integrate_update:
                                if (listAppsFragment.getConfig().getBoolean("switch_auto_integrate_update", false)) {
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.mipmap.vertical_switcher_on));
                                    textView.setText(Utils.getColoredText(Utils.getText(current.intValue()), "#00FF00", "bold"));
                                } else {
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.mipmap.vertical_switcher_off));
                                    textView.setText(Utils.getColoredText(Utils.getText(current.intValue()), "#FF0000", "bold"));
                                }
                                textView.append(LogCollector.LINE_SEPARATOR + Utils.getColoredText(Utils.getText(C0149R.string.switcher_auto_integrate_update_descr), "#AAAAAA", BuildConfig.VERSION_NAME));
                                break;
                            case C0149R.string.switcher_auto_move_to_internal:
                                if (listAppsFragment.getConfig().getBoolean("switch_auto_move_to_internal", false)) {
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.mipmap.vertical_switcher_on));
                                    textView.setText(Utils.getColoredText(Utils.getText(current.intValue()), "#00FF00", "bold"));
                                } else {
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.mipmap.vertical_switcher_off));
                                    textView.setText(Utils.getColoredText(Utils.getText(current.intValue()), "#FF0000", "bold"));
                                }
                                textView.append(LogCollector.LINE_SEPARATOR + Utils.getColoredText(Utils.getText(C0149R.string.switcher_auto_move_to_internal_descr), "#AAAAAA", BuildConfig.VERSION_NAME));
                                break;
                            case C0149R.string.switcher_auto_move_to_sd:
                                if (listAppsFragment.getConfig().getBoolean("switch_auto_move_to_sd", false)) {
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.mipmap.vertical_switcher_on));
                                    textView.setText(Utils.getColoredText(Utils.getText(current.intValue()), "#00FF00", "bold"));
                                } else {
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.mipmap.vertical_switcher_off));
                                    textView.setText(Utils.getColoredText(Utils.getText(current.intValue()), "#FF0000", "bold"));
                                }
                                textView.append(LogCollector.LINE_SEPARATOR + Utils.getColoredText(Utils.getText(C0149R.string.switcher_auto_move_to_sd_descr), "#AAAAAA", BuildConfig.VERSION_NAME));
                                break;
                            case C0149R.string.proxyGP:
                                long lenght = 0;
                                try {
                                    lenght = new File(Utils.getPlaceForOdex(Utils.getPkgInfo(Common.GOOGLEPLAY_PKG, patchActivity.CREATE_APK).applicationInfo.sourceDir, true)).length();
                                } catch (Exception e3) {
                                }
                                if (lenght > 1048576 || lenght == 0) {
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.mipmap.vertical_switcher_off));
                                    textView.setText(Utils.getColoredText(Utils.getText(C0149R.string.proxyGP), "#FF0000", "bold"));
                                } else {
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.mipmap.vertical_switcher_on));
                                    textView.setText(Utils.getColoredText(Utils.getText(C0149R.string.proxyGP), "#00FF00", "bold"));
                                }
                                textView.append(LogCollector.LINE_SEPARATOR + Utils.getColoredText(Utils.getText(C0149R.string.proxyGP_descr), "#AAAAAA", BuildConfig.VERSION_NAME));
                                break;
                        }
                        return row;
                    }
                };
                patchActivity.this.runOnUiThread(new C05022());
                return;
            }
            patchActivity.this.runOnUiThread(new C05033());
        }
    }

    class C05057 extends ArrayList<Integer> {
        C05057() {
            add(Integer.valueOf(C0149R.string.context_move_selected_apps_to_sdcard));
            add(Integer.valueOf(C0149R.string.context_move_selected_apps_to_internal));
            add(Integer.valueOf(C0149R.string.context_uninstall_selected_apps));
            add(Integer.valueOf(C0149R.string.context_integrate_update_selected_apps));
        }
    }

    class C05068 extends ArrayList<Integer> {
        C05068() {
            add(Integer.valueOf(C0149R.string.context_disable_google_ads));
            add(Integer.valueOf(C0149R.string.context_blockads));
            add(Integer.valueOf(C0149R.string.context_unblockads));
            add(Integer.valueOf(C0149R.string.context_clearhosts));
        }
    }

    class C05079 extends ArrayList<Integer> {
        C05079() {
            add(Integer.valueOf(C0149R.string.context_enable_google_ads));
            add(Integer.valueOf(C0149R.string.context_blockads));
            add(Integer.valueOf(C0149R.string.context_unblockads));
            add(Integer.valueOf(C0149R.string.context_clearhosts));
        }
    }

    final class byNameApkFile implements Comparator<FileApkListItem> {
        byNameApkFile() {
        }

        public int compare(FileApkListItem a, FileApkListItem b) {
            if (a == null || b == null) {
                throw new ClassCastException();
            }
            if (a.pkgName.equals(b.pkgName)) {
                if (a.versionCode > b.versionCode) {
                    return patchActivity.SETTINGS_ORIENT_LANDSCAPE;
                }
                if (a.versionCode < b.versionCode) {
                    return -1;
                }
                if (a.versionCode == b.versionCode) {
                    return patchActivity.CREATE_APK;
                }
            }
            return a.name.compareToIgnoreCase(b.name);
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        System.out.println("LuckyPatcher: create activity");
        if (getSharedPreferences("config", 4).getBoolean("force_close", false)) {
            System.out.println("LP FC detected!");
            try {
                getSharedPreferences("config", 4).edit().putBoolean("force_close", false).commit();
                listAppsFragment.mLogCollector = new LogCollector();
                listAppsFragment.init();
                Builder logbuilder = new Builder(this);
                logbuilder.setTitle(Utils.getText(C0149R.string.warning)).setIcon(17301543).setMessage(Utils.getText(C0149R.string.error_detect)).setPositiveButton(Utils.getText(C0149R.string.Yes), new C04983()).setNegativeButton(Utils.getText(C0149R.string.no), new C04942()).setOnCancelListener(new C04851());
                logbuilder.create().show();
                return;
            } catch (Exception e) {
                e.printStackTrace();
                getSharedPreferences("config", 4).edit().putBoolean("force_close", false).commit();
                finish();
                System.exit(CREATE_APK);
                return;
            }
        }
        listAppsFragment.desktop_launch = true;
        listAppsFragment.patchAct = this;
        listAppsFragment.appcontext = this;
        if (listAppsFragment.getConfig().getBoolean("hide_title", false)) {
            int api = VERSION.SDK_INT;
            if (api >= 14 && ViewConfiguration.get(this).hasPermanentMenuKey()) {
                requestWindowFeature(SETTINGS_ORIENT_LANDSCAPE);
            }
            if (api <= 10) {
                requestWindowFeature(SETTINGS_ORIENT_LANDSCAPE);
            }
        }
        setContentView(C0149R.layout.activity_main);
        try {
            if (Utils.getText(C0149R.string.busybox_not_found) == null) {
                finish();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            finish();
        }
        if (listAppsFragment.getConfig().getInt("orientstion", 3) == SETTINGS_ORIENT_LANDSCAPE) {
            setRequestedOrientation(CREATE_APK);
        }
        if (listAppsFragment.getConfig().getInt("orientstion", 3) == SETTINGS_ORIENT_PORTRET) {
            setRequestedOrientation(SETTINGS_ORIENT_LANDSCAPE);
        }
        if (listAppsFragment.getConfig().getInt("orientstion", 3) == 3) {
            setRequestedOrientation(4);
        }
        getWindow().addFlags(TransportMediator.FLAG_KEY_MEDIA_NEXT);
    }

    public void onBackPressed() {
        try {
            if (listAppsFragment.menu_open) {
                listAppsFragment.frag.hideMenu();
            } else if (listAppsFragment.adapterSelect) {
                listAppsFragment.frag.resetBatchOperation();
            } else if (listAppsFragment.getConfig().getBoolean("confirm_exit", true)) {
                OnClickListener dialogClickListener = new C04994();
                Utils.showDialogYesNo(Utils.getText(C0149R.string.app_name), Utils.getText(C0149R.string.message_exit), dialogClickListener, dialogClickListener, null);
            } else {
                super.onBackPressed();
            }
        } catch (Exception e) {
            e.printStackTrace();
            super.onBackPressed();
        }
    }

    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        System.out.println("Lucky Patcher: on new intent activity.");
        listAppsFragment.patchAct = this;
        listAppsFragment.handler = new Handler();
    }

    public void finish() {
        super.finish();
        if (VERSION.SDK_INT >= 19 && !isTaskRoot() && this.mIsRestoredToTop) {
            ((ActivityManager) getSystemService("activity")).moveTaskToFront(getTaskId(), SETTINGS_ORIENT_PORTRET);
        }
    }

    public void onStart() {
        super.onStart();
        System.out.println("Lucky Patcher: start activity.");
        listAppsFragment.patchAct = this;
        listAppsFragment.handler = new Handler();
    }

    public void onPause() {
        try {
            super.onPause();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Lucky Patcher: activity pause.");
        listAppsFragment.patchAct = this;
        listAppsFragment.handler = new Handler();
    }

    public void onResume() {
        try {
            super.onResume();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Lucky Patcher: activity resume.");
        listAppsFragment.patchAct = this;
        listAppsFragment.handler = new Handler();
    }

    public void onMemoryLow() {
        listAppsFragment.goodMemory = false;
        System.out.println("LuckyPatcher (onMemoryLow): started!");
    }

    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        System.out.println("onWindowFocusChanged");
        listAppsFragment.patchAct = this;
        listAppsFragment.handler = new Handler();
        if (listAppsFragment.return_from_control_panel && hasFocus) {
            listAppsFragment.createExpandMenu();
            listAppsFragment.return_from_control_panel = false;
            if (listAppsFragment.pli != null) {
                listAppsFragment.getConfig().edit().remove(listAppsFragment.pli.pkgName).commit();
            }
            if (listAppsFragment.plia != null) {
                listAppsFragment.plia.notifyDataSetChanged();
            }
            listAppsFragment.refresh = true;
            listAppsFragment.removeDialogLP(APP_DIALOG);
            new Thread(new C05005()).start();
        }
        if (listAppsFragment.plia != null) {
            listAppsFragment.plia.notifyDataSetChanged();
        }
        MemoryInfo mi = new MemoryInfo();
        ((ActivityManager) listAppsFragment.getInstance().getSystemService("activity")).getMemoryInfo(mi);
        System.out.println("LuckyPatcher " + listAppsFragment.version + " (FreeMemory): " + (mi.availMem / 1048576) + " lowMemory:" + mi.lowMemory + " TrashOld:" + (mi.threshold / 1048576));
        if ((listAppsFragment.firstrun == null || listAppsFragment.firstrun.booleanValue()) && listAppsFragment.plia != null && hasFocus) {
            listAppsFragment.refresh = true;
            listAppsFragment.plia.refreshPkgs(true);
            listAppsFragment.firstrun = Boolean.valueOf(false);
        }
        if (listAppsFragment.plia != null && hasFocus && listAppsFragment.refresh) {
            listAppsFragment.plia.refreshPkgs(false);
        }
    }

    public void launch_click(View view) {
        if (listAppsFragment.getConfig().getBoolean("vibration", false)) {
            frag.vib = (Vibrator) getSystemService("vibrator");
            frag.vib.vibrate(50);
        }
        listAppsFragment com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment = frag;
        listAppsFragment.removeDialogLP(APP_DIALOG);
        try {
            Utils.run_all("killall " + listAppsFragment.pli.pkgName);
            startActivity(listAppsFragment.getPkgMng().getLaunchIntentForPackage(listAppsFragment.pli.pkgName));
        } catch (RuntimeException e) {
            e.printStackTrace();
        } catch (Exception e2) {
            Toast.makeText(this, Utils.getText(C0149R.string.error_launch), SETTINGS_ORIENT_LANDSCAPE).show();
        }
    }

    public void saveobject_click(View v) {
        try {
            File f = new File(listAppsFragment.basepath + InternalZipConstants.ZIP_FILE_SEPARATOR + listAppsFragment.pli.pkgName + ".txt");
            if (f.exists()) {
                f.delete();
            }
            FileWriter fw = new FileWriter(f);
            fw.write("[BEGIN]\ngetActivity() Custom Patch generated by Luckypatcher the manual mode! For Object N" + listAppsFragment.CurentSelect + "...\n[CLASSES]\n{\"object\":\"" + listAppsFragment.CurentSelect + "\"}\n[ODEX]\n[END]\nApplication patched on object N" + listAppsFragment.CurentSelect + ". Please test...\nIf all works well. Make a \"Dalvik-cache Fix Apply\".");
            fw.close();
            listAppsFragment.tvt.setText(Utils.getColoredText("Object N" + listAppsFragment.CurentSelect + " " + Utils.getText(C0149R.string.saved_object), "#ff00ff73", "bold"));
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(this, "Error while saving file", SETTINGS_ORIENT_LANDSCAPE).show();
        } catch (Exception e2) {
            e2.printStackTrace();
            Toast.makeText(this, "Error while saving file", SETTINGS_ORIENT_LANDSCAPE).show();
        }
    }

    public void fixobject_click(View v) {
        try {
            frag.odex(listAppsFragment.pli);
            listAppsFragment.tvt.setText(Utils.getColoredText(BuildConfig.VERSION_NAME + Utils.getText(C0149R.string.allchanges), "#ff00ff73", "bold"));
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, "Error while saving file", SETTINGS_ORIENT_LANDSCAPE).show();
        }
    }

    public void patch_click(View v) {
        if (listAppsFragment.CurentSelect != 0) {
            Utils.kill(listAppsFragment.pli.pkgName);
            listAppsFragment.str = BuildConfig.VERSION_NAME;
            Utils.kill(listAppsFragment.plipack);
            Utils utils = new Utils(BuildConfig.VERSION_NAME);
            String[] strArr = new String[SETTINGS_ORIENT_LANDSCAPE];
            strArr[CREATE_APK] = listAppsFragment.dalvikruncommand + ".nerorunpatch " + listAppsFragment.plipack + " " + "object" + listAppsFragment.CurentSelect;
            listAppsFragment.str = utils.cmdRoot(strArr);
            if (listAppsFragment.str.contains("Done")) {
                listAppsFragment.tvt.setText(Utils.getColoredText("Object N" + listAppsFragment.CurentSelect + LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.extres1), "#ff00ff73", "bold"));
                return;
            }
            listAppsFragment.tvt.setText(Utils.getColoredText("Object N" + listAppsFragment.CurentSelect + LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.extres2), "#ffff0055", "bold"));
        }
    }

    public void restore_click(View v) {
        try {
            String str4 = BuildConfig.VERSION_NAME;
            listAppsFragment.str = BuildConfig.VERSION_NAME;
            Utils utils = new Utils(BuildConfig.VERSION_NAME);
            String[] strArr = new String[SETTINGS_ORIENT_LANDSCAPE];
            strArr[CREATE_APK] = listAppsFragment.dalvikruncommand + ".restore " + listAppsFragment.pli.pkgName;
            listAppsFragment.tvt.setText(Utils.getColoredText(utils.cmdRoot(strArr), "#ff00ff73", "bold"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void backup_click(View v) {
        try {
            String str3 = BuildConfig.VERSION_NAME;
            listAppsFragment.str = BuildConfig.VERSION_NAME;
            Utils utils = new Utils(BuildConfig.VERSION_NAME);
            String[] strArr = new String[SETTINGS_ORIENT_LANDSCAPE];
            strArr[CREATE_APK] = listAppsFragment.dalvikruncommand + ".backup " + listAppsFragment.pli.pkgName;
            listAppsFragment.tvt.setText(Utils.getColoredText(utils.cmdRoot(strArr), "#ff00ff73", "bold"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void apply_click(View v) {
        listAppsFragment com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment = frag;
        listAppsFragment.removeDialogLP(CUSTOM2_DIALOG);
        if (listAppsFragment.func == SETTINGS_ORIENT_LANDSCAPE) {
            frag.custompatch(listAppsFragment.pli);
        }
        if (listAppsFragment.func == 0) {
            frag.createapkcustom();
        }
    }

    public void cancel_click(View v) {
        listAppsFragment com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment = frag;
        listAppsFragment.removeDialogLP(CUSTOM2_DIALOG);
    }

    public void toolbar_menu_click(View v) {
        frag.getActivity().openOptionsMenu();
    }

    public void toolbar_market_install_click(View v) {
        listAppsFragment com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment = frag;
        listAppsFragment.removeDialogLP(MARKET_INSTALL_DIALOG);
        com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment = frag;
        listAppsFragment.showDialogLP(MARKET_INSTALL_DIALOG);
    }

    public void toolbar_switchers_click(View v) {
        Thread t = new Thread(new C05046());
        t.setPriority(10);
        t.start();
    }

    public void toolbar_system_utils_click(View v) {
        if (listAppsFragment.menu_open) {
            listAppsFragment.frag.hideMenu();
            return;
        }
        ArrayList<MenuItem> s = new ArrayList();
        s.add(new MenuItem(C0149R.string.back, new ArrayList(), true));
        if (listAppsFragment.su) {
            s.add(new MenuItem(C0149R.string.bootview, new ArrayList(), true));
            s.add(new MenuItem(C0149R.string.context_batch_operations, new C05057(), true));
            List<ResolveInfo> serv = listAppsFragment.getPkgMng().queryIntentServices(new Intent("com.google.android.gms.ads.identifier.service.START"), CREATE_APK);
            if (serv == null || serv.size() <= 0) {
                s.add(new MenuItem(C0149R.string.toolbar_adfree, new C05079(), true));
            } else {
                s.add(new MenuItem(C0149R.string.toolbar_adfree, new C05068(), true));
            }
        }
        s.add(new MenuItem(C0149R.string.remove_all_saved_purchases, new ArrayList(), true));
        if (listAppsFragment.su) {
            s.add(new MenuItem(C0149R.string.dirbinder, new ArrayList(), true));
            s.add(new MenuItem(C0149R.string.corepatches, new ArrayList(), true));
            if (Utils.isXposedEnabled()) {
                s.add(new MenuItem(C0149R.string.xposed_settings, new ArrayList(), true));
            }
            s.add(new MenuItem(C0149R.string.mod_market_check, new ArrayList(), true));
            s.add(new MenuItem(C0149R.string.market_install, new ArrayList(), true));
            s.add(new MenuItem(C0149R.string.odex_all_system_app, new ArrayList(), true));
            s.add(new MenuItem(C0149R.string.removefixes, new ArrayList(), true));
            s.add(new MenuItem(C0149R.string.cleardalvik, new ArrayList(), true));
        }
        if (listAppsFragment.api < 19 || listAppsFragment.su) {
            s.add(new MenuItem(C0149R.string.set_default_to_install, new ArrayList<Integer>() {
                {
                    add(Integer.valueOf(C0149R.string.set_default_to_install_auto));
                    add(Integer.valueOf(C0149R.string.set_default_to_install_internal_memory));
                    add(Integer.valueOf(C0149R.string.set_default_to_install_sdcard));
                }
            }, true));
        }
        if (listAppsFragment.su) {
            s.add(new MenuItem(C0149R.string.reboot, new ArrayList(), true));
        }
        try {
            if (listAppsFragment.menu_adapter != null) {
                listAppsFragment.menu_adapter.add(s);
                listAppsFragment.frag.showMenu();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void toolbar_settings_click() {
        if (listAppsFragment.menu_open) {
            listAppsFragment.frag.hideMenu();
            return;
        }
        ArrayList<MenuItem> s = new ArrayList();
        s.add(new MenuItem(C0149R.string.back, new ArrayList(), true));
        s.add(new MenuItem(C0149R.string.viewmenu, new ArrayList(), SETTINGS_ORIENT_LANDSCAPE, true));
        s.add(new MenuItem(C0149R.string.text_size, new ArrayList<Integer>() {
            {
                add(Integer.valueOf(patchActivity.SETTINGS_ORIENT_LANDSCAPE));
            }
        }, SETTINGS_ORIENT_PORTRET, true));
        s.add(new MenuItem(C0149R.string.orientmenu, new ArrayList<Integer>() {
            {
                add(Integer.valueOf(patchActivity.SETTINGS_ORIENT_PORTRET));
            }
        }, SETTINGS_ORIENT_PORTRET, true));
        s.add(new MenuItem(C0149R.string.sortmenu, new ArrayList<Integer>() {
            {
                add(Integer.valueOf(3));
            }
        }, SETTINGS_ORIENT_PORTRET, true));
        s.add(new MenuItem((int) C0149R.string.filter, (int) C0149R.string.filterdescr, new ArrayList<Integer>() {
            {
                add(Integer.valueOf(4));
            }
        }, (int) SETTINGS_ORIENT_PORTRET, true));
        s.add(new MenuItem(C0149R.string.others, new ArrayList(), SETTINGS_ORIENT_LANDSCAPE, true));
        s.add(new MenuItem(C0149R.string.langmenu, new ArrayList(), SETTINGS_ORIENT_PORTRET, true));
        s.add(new MenuItem((int) C0149R.string.settings_force_root, (int) C0149R.string.settings_force_root_description, new ArrayList<Integer>() {
            {
                add(Integer.valueOf(5));
            }
        }, (int) SETTINGS_ORIENT_PORTRET, true));
        s.add(new MenuItem((int) C0149R.string.dir_change, (int) C0149R.string.dir_change_descr, new ArrayList(), (int) SETTINGS_ORIENT_PORTRET, true));
        s.add(new MenuItem((int) C0149R.string.days_on_up, (int) C0149R.string.days_on_up_descr, new ArrayList(), (int) SETTINGS_ORIENT_PORTRET, true));
        s.add(new MenuItem(C0149R.string.setting_confirm_exit, C0149R.string.setting_confirm_exit_descr, new ArrayList(), 3, true, "confirm_exit", true, new Runnable() {
            public void run() {
                if (listAppsFragment.getConfig().getBoolean("confirm_exit", true)) {
                    listAppsFragment.getConfig().edit().putBoolean("confirm_exit", false).commit();
                } else {
                    listAppsFragment.getConfig().edit().putBoolean("confirm_exit", true).commit();
                }
                listAppsFragment.menu_adapter.notifyDataSetChanged();
            }
        }));
        s.add(new MenuItem(C0149R.string.fast_start, C0149R.string.fast_start_descr, new ArrayList(), 3, true, "fast_start", false, new Runnable() {
            public void run() {
                if (listAppsFragment.getConfig().getBoolean("fast_start", true)) {
                    listAppsFragment.getConfig().edit().putBoolean("fast_start", false).commit();
                } else {
                    listAppsFragment.getConfig().edit().putBoolean("fast_start", true).commit();
                }
                listAppsFragment.menu_adapter.notifyDataSetChanged();
                listAppsFragment.runResume = true;
            }
        }));
        s.add(new MenuItem(C0149R.string.no_icon, C0149R.string.no_icon_descr, new ArrayList(), 3, true, "no_icon", false, new Runnable() {
            public void run() {
                if (listAppsFragment.getConfig().getBoolean("no_icon", false)) {
                    listAppsFragment.getConfig().edit().putBoolean("no_icon", false).commit();
                } else {
                    listAppsFragment.getConfig().edit().putBoolean("no_icon", true).commit();
                }
                listAppsFragment.menu_adapter.notifyDataSetChanged();
                listAppsFragment.runResume = true;
            }
        }));
        s.add(new MenuItem((int) C0149R.string.apk_create_option, (int) C0149R.string.apkdescr, new ArrayList<Integer>() {
            {
                add(Integer.valueOf(patchActivity.APP_DIALOG));
            }
        }, (int) SETTINGS_ORIENT_PORTRET, true));
        s.add(new MenuItem((int) C0149R.string.change_icon_lp, (int) C0149R.string.change_icon_lp_descr, new ArrayList<Integer>() {
            {
                add(Integer.valueOf(patchActivity.CONTEXT_DIALOG));
            }
        }, (int) SETTINGS_ORIENT_PORTRET, true));
        s.add(new MenuItem(C0149R.string.hide_notify, C0149R.string.hide_notify_descr, new ArrayList(), 3, true, "hide_notify", false, new Runnable() {
            public void run() {
                if (listAppsFragment.getConfig().getBoolean("hide_notify", false)) {
                    listAppsFragment.getConfig().edit().putBoolean("hide_notify", false).commit();
                } else {
                    listAppsFragment.getConfig().edit().putBoolean("hide_notify", true).commit();
                }
                listAppsFragment.menu_adapter.notifyDataSetChanged();
                listAppsFragment.runResume = true;
            }
        }));
        s.add(new MenuItem(C0149R.string.hide_title_app, C0149R.string.hide_title_app_descr, new ArrayList(), 3, true, "hide_title", false, new Runnable() {
            public void run() {
                if (listAppsFragment.getConfig().getBoolean("hide_title", false)) {
                    listAppsFragment.getConfig().edit().putBoolean("hide_title", false).commit();
                } else {
                    listAppsFragment.getConfig().edit().putBoolean("hide_title", true).commit();
                }
                listAppsFragment.menu_adapter.notifyDataSetChanged();
                listAppsFragment.runResume = true;
            }
        }));
        s.add(new MenuItem(C0149R.string.disable_autoupdate, C0149R.string.disable_autoupdate_descr, new ArrayList(), 3, true, "disable_autoupdate", false, new Runnable() {
            public void run() {
                if (listAppsFragment.getConfig().getBoolean("disable_autoupdate", false)) {
                    listAppsFragment.getConfig().edit().putBoolean("disable_autoupdate", false).commit();
                } else {
                    listAppsFragment.getConfig().edit().putBoolean("disable_autoupdate", true).commit();
                }
                listAppsFragment.menu_adapter.notifyDataSetChanged();
                listAppsFragment.runResume = true;
            }
        }));
        s.add(new MenuItem(C0149R.string.vibration, C0149R.string.vibration_descr, new ArrayList(), 3, true, "vibration", false, new Runnable() {
            public void run() {
                if (listAppsFragment.getConfig().getBoolean("vibration", false)) {
                    listAppsFragment.getConfig().edit().putBoolean("vibration", false).commit();
                } else {
                    listAppsFragment.getConfig().edit().putBoolean("vibration", true).commit();
                }
                listAppsFragment.menu_adapter.notifyDataSetChanged();
                listAppsFragment.runResume = true;
            }
        }));
        s.add(new MenuItem(C0149R.string.help, new ArrayList(), SETTINGS_ORIENT_PORTRET, true));
        s.add(new MenuItem(C0149R.string.update, new ArrayList(), SETTINGS_ORIENT_PORTRET, true));
        s.add(new MenuItem(C0149R.string.sendlog, new ArrayList(), SETTINGS_ORIENT_PORTRET, true));
        s.add(new MenuItem(C0149R.string.aboutmenu, new ArrayList(), SETTINGS_ORIENT_PORTRET, true));
        try {
            if (listAppsFragment.menu_adapter != null) {
                listAppsFragment.menu_adapter.add(s);
                listAppsFragment.frag.showMenu();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void toolbar_refresh_click(View v) {
        try {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            listAppsFragment com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment = frag;
            if (listAppsFragment.filter == null) {
                com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment = frag;
                listAppsFragment.filter = new FilterFragment();
                listAppsFragment com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment2 = frag;
                ft.add((int) C0149R.id.fragment_filter, listAppsFragment.filter);
                ft.commit();
                return;
            }
            com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment = frag;
            ft.remove(listAppsFragment.filter);
            ft.commit();
            com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment = frag;
            listAppsFragment.filter.onDestroy();
            com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment = frag;
            listAppsFragment.filter = null;
            com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment = frag;
            listAppsFragment.plia.getFilter().filter(BuildConfig.VERSION_NAME);
            com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment = frag;
            listAppsFragment.plia.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void toolbar_backups_click(View v) {
        new File(listAppsFragment.basepath + "/Backup/").mkdirs();
        listAppsFragment com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment = frag;
        listAppsFragment.showDialogLP(PROGRESS_DIALOG2);
        listAppsFragment.progress2.setCancelable(false);
        listAppsFragment.progress2.setMessage(Utils.getText(C0149R.string.wait));
        Thread t = new Thread(new Runnable() {

            class C04892 implements Runnable {
                C04892() {
                }

                public void run() {
                    listAppsFragment.adapt.sort(new byNameApkFile());
                    listAppsFragment com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment = patchActivity.frag;
                    listAppsFragment.removeDialogLP(patchActivity.RESTORE_FROM_BACKUP);
                    com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment = patchActivity.frag;
                    listAppsFragment.showDialogLP(patchActivity.RESTORE_FROM_BACKUP);
                    com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment = patchActivity.frag;
                    listAppsFragment.removeDialogLP(patchActivity.PROGRESS_DIALOG2);
                }
            }

            class C04903 implements Runnable {
                C04903() {
                }

                public void run() {
                    patchActivity.frag.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.backups_not_found) + " " + listAppsFragment.basepath + "/Backup");
                    listAppsFragment com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment = patchActivity.frag;
                    listAppsFragment.removeDialogLP(patchActivity.PROGRESS_DIALOG2);
                }
            }

            class C04914 implements Runnable {
                C04914() {
                }

                public void run() {
                    patchActivity.frag.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.backups_not_found) + " " + listAppsFragment.basepath + "/Backup");
                    listAppsFragment com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment = patchActivity.frag;
                    listAppsFragment.removeDialogLP(patchActivity.PROGRESS_DIALOG2);
                }
            }

            public void run() {
                ArrayList<FileApkListItem> items = new ArrayList();
                String[] files = new File(listAppsFragment.basepath + "/Backup").list();
                if (files == null || files.length == 0) {
                    patchActivity.this.runOnUiThread(new C04914());
                    return;
                }
                int length = files.length;
                for (int i = patchActivity.CREATE_APK; i < length; i += patchActivity.SETTINGS_ORIENT_LANDSCAPE) {
                    try {
                        items.add(new FileApkListItem(listAppsFragment.getInstance(), new File(listAppsFragment.basepath + "/Backup/" + files[i]), true));
                    } catch (Exception e) {
                    }
                }
                if (items.size() != 0) {
                    listAppsFragment.adapt = new ArrayAdapter<FileApkListItem>(patchActivity.this, C0149R.layout.backup_context_menu, items) {
                        ArrayAdapter<FileApkListItem> mAdapter = this;

                        class C04871 implements View.OnClickListener {
                            C04871() {
                            }

                            public void onClick(View view) {
                                final FileApkListItem cur = (FileApkListItem) view.getTag();
                                OnClickListener dialogClickListener = new OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        switch (which) {
                                            case Zip4jConstants.ENC_NO_ENCRYPTION /*-1*/:
                                                cur.backupfile.delete();
                                                C04881.this.mAdapter.remove(cur);
                                                C04881.this.mAdapter.notifyDataSetChanged();
                                                return;
                                            default:
                                                return;
                                        }
                                    }
                                };
                                Utils.showDialog(new AlertDlg(listAppsFragment.frag.getContext()).setTitle(Utils.getText(C0149R.string.warning)).setMessage(Utils.getText(C0149R.string.backup_delete_message) + " " + cur.name + "?").setPositiveButton(Utils.getText(C0149R.string.Yes), dialogClickListener).setNegativeButton(Utils.getText(C0149R.string.no), dialogClickListener).create());
                            }
                        }

                        public View getView(int position, View convertView, ViewGroup parent) {
                            View row = convertView;
                            FileApkListItem current = (FileApkListItem) getItem(position);
                            if (row == null) {
                                row = ((LayoutInflater) listAppsFragment.getInstance().getSystemService("layout_inflater")).inflate(C0149R.layout.backup_context_menu, parent, false);
                            }
                            TextView textView = (TextView) row.findViewById(C0149R.id.conttext);
                            ImageView icon = (ImageView) row.findViewById(C0149R.id.imgIcon);
                            Button btn = (Button) row.findViewById(C0149R.id.button_del);
                            btn.setTag(current);
                            btn.setOnClickListener(new C04871());
                            icon.setImageDrawable(null);
                            textView.setTextAppearance(getContext(), listAppsFragment.getSizeText());
                            textView.setCompoundDrawablePadding((int) ((5.0f * listAppsFragment.getRes().getDisplayMetrics().density) + 0.5f));
                            textView.setTextColor(-1);
                            textView.setText(Utils.getColoredText(current.name, "#A1C2F3", "bold"));
                            textView.append(Utils.getColoredText(LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.restore_version) + ": " + current.versionName + " " + Utils.getText(C0149R.string.restore_build) + ": " + current.versionCode, "#a0a0a0", BuildConfig.VERSION_NAME));
                            icon.setImageDrawable(current.icon);
                            return row;
                        }
                    };
                    patchActivity.this.runOnUiThread(new C04892());
                    return;
                }
                patchActivity.this.runOnUiThread(new C04903());
            }
        });
        t.setPriority(10);
        t.start();
    }

    public void toolbar_rebuild_click(View v) {
        try {
            listAppsFragment.plia.onGroupCollapsedAll();
            listAppsFragment.pli = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
        LinearLayout d = (LinearLayout) View.inflate(this, C0149R.layout.file_browser, null);
        Dialog tempdialog = new AlertDlg((Context) this, true).setView(d).create();
        tempdialog.setCancelable(false);
        tempdialog.setOnKeyListener(new OnKeyListener() {
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                System.out.println(event);
                if (keyCode == 4 && event.getAction() == patchActivity.SETTINGS_ORIENT_LANDSCAPE) {
                    try {
                        listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
                        if (((ItemFile) patchActivity.frag.filebrowser.getAdapter().getItem(patchActivity.SETTINGS_ORIENT_LANDSCAPE)).file.equals("../")) {
                            patchActivity.frag.getDir(((ItemFile) patchActivity.frag.filebrowser.getAdapter().getItem(patchActivity.SETTINGS_ORIENT_LANDSCAPE)).path, patchActivity.frag.filebrowser, true);
                        } else {
                            dialog.dismiss();
                        }
                    } catch (IndexOutOfBoundsException e) {
                        dialog.dismiss();
                    } catch (Exception e2) {
                        dialog.dismiss();
                    }
                }
                return true;
            }
        });
        tempdialog.show();
        frag.myPath = (TextView) tempdialog.findViewById(C0149R.id.path);
        String storages_path = new File(listAppsFragment.extStorageDirectory).getParent();
        while (new File(new File(storages_path).getParent()).getParent() != null) {
            try {
                storages_path = new File(storages_path).getParent();
                System.out.println("Parent directory:" + storages_path);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        frag.root = storages_path;
        ((ListView) d.findViewById(C0149R.id.list)).setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> l, View arg1, int position, long id) {
                ItemFile item = (ItemFile) l.getAdapter().getItem(position);
                File file = new File(item.full);
                if (!file.isDirectory()) {
                    patchActivity.frag.current = item;
                    listAppsFragment.rebuldApk = patchActivity.frag.current.full;
                    ArrayList<Integer> s = new ArrayList();
                    if (item.file.equals("core.jar") || item.file.equals("core.odex") || item.file.equals("services.jar") || item.file.equals("services.odex") || item.file.equals("core-libart.jar") || item.file.equals("boot.oat")) {
                        if (!item.file.endsWith(".jar")) {
                            s.add(Integer.valueOf(C0149R.string.context_patch_framework));
                        } else if (Utils.classes_test(new File(item.full))) {
                            s.add(Integer.valueOf(C0149R.string.context_patch_framework));
                        } else {
                            patchActivity.frag.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.error_classes_not_found));
                        }
                    }
                    if (item.file.endsWith(".apk")) {
                        s.add(Integer.valueOf(C0149R.string.context_install));
                        s.add(Integer.valueOf(C0149R.string.context_rebuild));
                        if (!listAppsFragment.su) {
                            s.add(Integer.valueOf(C0149R.string.context_uninstall_if_installed));
                        }
                        if (listAppsFragment.su) {
                            s.add(Integer.valueOf(C0149R.string.context_install_as_system));
                        }
                        s.add(Integer.valueOf(C0149R.string.delete_file));
                        s.add(Integer.valueOf(C0149R.string.share_this_app));
                    }
                    if (s.size() != 0) {
                        listAppsFragment.adapt = new ArrayAdapter<Integer>(patchActivity.frag.getContext(), C0149R.layout.contextmenu, s) {
                            public View getView(int position, View convertView, ViewGroup parent) {
                                View view = super.getView(position, convertView, parent);
                                TextView textView = (TextView) view.findViewById(C0149R.id.conttext);
                                textView.setTextAppearance(getContext(), listAppsFragment.getSizeText());
                                textView.setCompoundDrawablePadding((int) ((5.0f * listAppsFragment.getRes().getDisplayMetrics().density) + 0.5f));
                                textView.setTextColor(-1);
                                textView.setText(Utils.getText(((Integer) getItem(position)).intValue()));
                                textView.setTypeface(null, patchActivity.SETTINGS_ORIENT_LANDSCAPE);
                                return view;
                            }
                        };
                        listAppsFragment com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment = patchActivity.frag;
                        listAppsFragment.removeDialogLP(patchActivity.CONTEXT_DIALOG);
                        com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment = patchActivity.frag;
                        listAppsFragment.showDialogLP(patchActivity.CONTEXT_DIALOG);
                    }
                } else if (!file.canRead() || file.listFiles() == null) {
                    Utils.showDialog(new AlertDlg(patchActivity.frag.getContext()).setIcon(17301659).setTitle("[" + file.getName() + "] folder can't be read!").setPositiveButton("OK", null).create());
                } else {
                    patchActivity.frag.filebrowser = (ListView) l;
                    patchActivity.frag.getDir(new File(item.full).getPath(), (ListView) l, true);
                }
            }
        });
        frag.filebrowser = (ListView) d.findViewById(C0149R.id.list);
        try {
            frag.getDir(frag.root, (ListView) d.findViewById(C0149R.id.list), true);
        } catch (Exception e3) {
            try {
                frag.root = new File(listAppsFragment.basepath).getParent();
                frag.getDir(frag.root, (ListView) d.findViewById(C0149R.id.list), true);
            } catch (Exception e4) {
                frag.root = listAppsFragment.basepath;
                frag.getDir(frag.root, (ListView) d.findViewById(C0149R.id.list), true);
            }
        }
    }

    public void show_file_explorer(String rootDir) {
        try {
            listAppsFragment.plia.onGroupCollapsedAll();
            listAppsFragment.pli = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
        LinearLayout d = (LinearLayout) View.inflate(this, C0149R.layout.file_browser, null);
        Dialog tempdialog = new AlertDlg((Context) this, true).setView(d).create();
        tempdialog.setCancelable(false);
        tempdialog.setOnKeyListener(new OnKeyListener() {
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                System.out.println(event);
                if (keyCode == 4 && event.getAction() == patchActivity.SETTINGS_ORIENT_LANDSCAPE) {
                    try {
                        if (((ItemFile) patchActivity.frag.filebrowser.getAdapter().getItem(patchActivity.SETTINGS_ORIENT_LANDSCAPE)).file.equals("../")) {
                            patchActivity.frag.getDir(((ItemFile) patchActivity.frag.filebrowser.getAdapter().getItem(patchActivity.SETTINGS_ORIENT_LANDSCAPE)).path, patchActivity.frag.filebrowser, false);
                        } else {
                            dialog.dismiss();
                        }
                    } catch (IndexOutOfBoundsException e) {
                        dialog.dismiss();
                    } catch (Exception e2) {
                        dialog.dismiss();
                    }
                }
                return true;
            }
        });
        tempdialog.show();
        frag.myPath = (TextView) tempdialog.findViewById(C0149R.id.path);
        frag.root = rootDir;
        ((ListView) d.findViewById(C0149R.id.list)).setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> l, View arg1, int position, long id) {
                ItemFile item = (ItemFile) l.getAdapter().getItem(position);
                File file = new File(item.full);
                if (!file.isDirectory()) {
                    patchActivity.frag.current = item;
                    listAppsFragment.rebuldApk = patchActivity.frag.current.full;
                    ArrayList<Integer> s = new ArrayList();
                    if (item.file.equals("core.jar") || item.file.equals("core.odex") || item.file.equals("services.jar") || item.file.equals("services.odex") || item.file.equals("core-libart.jar") || item.file.equals("boot.oat")) {
                        if (!item.file.endsWith(".jar")) {
                            s.add(Integer.valueOf(C0149R.string.context_patch_framework));
                        } else if (Utils.classes_test(new File(item.full))) {
                            s.add(Integer.valueOf(C0149R.string.context_patch_framework));
                        } else {
                            patchActivity.frag.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.error_classes_not_found));
                        }
                    }
                    if (item.file.endsWith(".apk")) {
                        s.add(Integer.valueOf(C0149R.string.context_install));
                        s.add(Integer.valueOf(C0149R.string.context_rebuild));
                        if (!listAppsFragment.su) {
                            s.add(Integer.valueOf(C0149R.string.context_uninstall_if_installed));
                        }
                        if (listAppsFragment.su) {
                            s.add(Integer.valueOf(C0149R.string.context_install_as_system));
                        }
                        s.add(Integer.valueOf(C0149R.string.delete_file));
                        s.add(Integer.valueOf(C0149R.string.share_this_app));
                    }
                    if (s.size() != 0) {
                        listAppsFragment.adapt = new ArrayAdapter<Integer>(patchActivity.frag.getContext(), C0149R.layout.contextmenu, s) {
                            public View getView(int position, View convertView, ViewGroup parent) {
                                View view = super.getView(position, convertView, parent);
                                TextView textView = (TextView) view.findViewById(C0149R.id.conttext);
                                textView.setTextAppearance(getContext(), listAppsFragment.getSizeText());
                                textView.setCompoundDrawablePadding((int) ((5.0f * listAppsFragment.getRes().getDisplayMetrics().density) + 0.5f));
                                textView.setTextColor(-1);
                                textView.setText(Utils.getText(((Integer) getItem(position)).intValue()));
                                textView.setTypeface(null, patchActivity.SETTINGS_ORIENT_LANDSCAPE);
                                return view;
                            }
                        };
                        listAppsFragment com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment = patchActivity.frag;
                        listAppsFragment.removeDialogLP(patchActivity.CONTEXT_DIALOG);
                        com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment = patchActivity.frag;
                        listAppsFragment.showDialogLP(patchActivity.CONTEXT_DIALOG);
                    }
                } else if (!file.canRead() || file.listFiles() == null) {
                    Utils.showDialog(new AlertDlg(patchActivity.frag.getContext()).setIcon(17301659).setTitle("[" + file.getName() + "] folder can't be read!").setPositiveButton("OK", null).create());
                } else {
                    patchActivity.frag.filebrowser = (ListView) l;
                    patchActivity.frag.getDir(new File(item.full).getPath(), (ListView) l, false);
                }
            }
        });
        frag.filebrowser = (ListView) d.findViewById(C0149R.id.list);
        try {
            frag.getDir(frag.root, (ListView) d.findViewById(C0149R.id.list), false);
        } catch (Exception e2) {
            try {
                frag.root = new File(listAppsFragment.basepath).getParent();
                frag.getDir(frag.root, (ListView) d.findViewById(C0149R.id.list), false);
            } catch (Exception e3) {
                frag.root = listAppsFragment.basepath;
                frag.getDir(frag.root, (ListView) d.findViewById(C0149R.id.list), false);
            }
        }
    }

    public void mod_market_check(View v) {
        listAppsFragment.frag.connectToLicensing();
    }
}
