package com.android.vending.billing.InAppBillingService.LUCK;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.PorterDuff.Mode;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.StatFs;
import android.os.Vibrator;
import android.provider.Settings.Secure;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.media.TransportMediator;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat;
import android.text.Editable;
import android.util.Xml;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ExpandableListView.OnGroupExpandListener;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.android.vending.billing.InAppBillingService.LUCK.dialogs.All_Dialogs;
import com.android.vending.billing.InAppBillingService.LUCK.dialogs.App_Dialog;
import com.android.vending.billing.InAppBillingService.LUCK.dialogs.Create_Apk_Ads_Dialog;
import com.android.vending.billing.InAppBillingService.LUCK.dialogs.Create_Apk_Dialog;
import com.android.vending.billing.InAppBillingService.LUCK.dialogs.Create_Apk_Support_Dialog;
import com.android.vending.billing.InAppBillingService.LUCK.dialogs.Custom2_Dialog;
import com.android.vending.billing.InAppBillingService.LUCK.dialogs.Custom_Patch_Dialog;
import com.android.vending.billing.InAppBillingService.LUCK.dialogs.Ext_Patch_Dialog;
import com.android.vending.billing.InAppBillingService.LUCK.dialogs.FilterFragment;
import com.android.vending.billing.InAppBillingService.LUCK.dialogs.Market_Install_Dialog;
import com.android.vending.billing.InAppBillingService.LUCK.dialogs.Menu_Dialog;
import com.android.vending.billing.InAppBillingService.LUCK.dialogs.Menu_Dialog_Static;
import com.android.vending.billing.InAppBillingService.LUCK.dialogs.Patch_Dialog;
import com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog;
import com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog_2;
import com.android.vending.billing.InAppBillingService.LUCK.dialogs.Progress_Dialog_Loading;
import com.android.vending.billing.InAppBillingService.LUCK.widgets.AndroidPatchWidget;
import com.android.vending.billing.InAppBillingService.LUCK.widgets.AppDisablerWidget;
import com.android.vending.billing.InAppBillingService.LUCK.widgets.BinderWidget;
import com.android.vending.billing.InAppBillingService.LUCK.widgets.inapp_widget;
import com.android.vending.billing.InAppBillingService.LUCK.widgets.lvl_widget;
import com.android.vending.licensing.ILicenseResultListener.Stub;
import com.android.vending.licensing.ILicensingService;
import com.chelpus.Common;
import com.chelpus.Utils;
import com.chelpus.root.utils.cloneApp;
import com.chelpus.root.utils.corepatch;
import com.chelpus.root.utils.createapkcustom;
import com.chelpus.root.utils.odexrunpatch;
import com.chelpus.root.utils.runpatchads;
import com.chelpus.root.utils.runpatchsupportOld;
import com.google.android.finsky.billing.iab.DbHelper;
import com.google.android.finsky.billing.iab.InAppBillingService;
import com.google.android.finsky.billing.iab.ItemsListItem;
import com.google.android.finsky.billing.iab.MarketBillingService;
import com.google.android.finsky.services.LicensingService;
import com.google.android.vending.licensing.AESObfuscator;
import com.google.android.vending.licensing.ValidationException;
import com.google.android.vending.licensing.util.Base64;
import com.google.android.vending.licensing.util.Base64DecoderException;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.RandomAccessFile;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ServerSocket;
import java.net.URL;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileChannel.MapMode;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Semaphore;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.util.InternalZipConstants;
import net.lingala.zip4j.util.Zip4jConstants;
import org.json.JSONException;
import org.json.JSONObject;
import org.tukaani.xz.LZMA2Options;
import org.tukaani.xz.common.Util;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;
import org.xmlpull.v1.XmlSerializer;

public class listAppsFragment extends Fragment {
    public static final int ABOUT_DIALOG = 0;
    public static final int ADD_BOOT = 2;
    public static final int ADS_PATCH_CONTEXT_DIALOG = 17;
    public static final int APP_DIALOG = 6;
    public static final int BACKUP_SELECTOR = 26;
    public static final int CHECKABLE_LIST_ITEM = 3;
    public static final int CHILD_CATEGORY_SELECT_ITEM = 4;
    public static final int CHILD_ICON_CHANGE_SELECT_ITEM = 7;
    public static final int CHILD_NAME_SHEME_SELECT_ITEM = 6;
    public static final int CHILD_ORIENT_SELECT_ITEM = 2;
    public static final int CHILD_ROOT_SELECT_ITEM = 5;
    public static final int CHILD_SORT_SELECT_ITEM = 3;
    public static final int CHILD_TEXT_SIZE_SELECT_ITEM = 1;
    public static final int CLICKABLE_LIST_ITEM = 2;
    public static final int CONTEXT_DIALOG = 7;
    public static final int CONTEXT_DIALOG_CorePatches = 24;
    public static final int CONTEXT_DIALOG_STATIC = 38;
    public static final int CONTEXT_DIALOG_Xposed_Patches = 39;
    public static final int CREATEAPK_ADS_DIALOG = 9;
    public static final int CREATEAPK_DIALOG = 8;
    public static final int CREATEAPK_SUPPORT_DIALOG = 36;
    public static final int CREATE_APK = 0;
    public static final int CUSTOM2_DIALOG = 15;
    public static final int CUSTOM_PATCH = 1;
    public static final int CUSTOM_PATCH_SELECTOR = 16;
    public static final int Create_ADS_PATCH_CONTEXT_DIALOG = 18;
    public static final int Create_LVL_PATCH_CONTEXT_DIALOG = 19;
    public static final int Create_PERM_CONTEXT_DIALOG = 21;
    public static int CurentSelect = 0;
    public static final int Custom_PATCH_DIALOG = 4;
    public static final int DALVIK_CORE_PATCH = 37;
    public static final int DIALOG_BOOT_FILE = 3;
    public static final int DIALOG_FAILED_TO_COLLECT_LOGS = 3535122;
    public static final int DIALOG_PROGRESS_COLLECTING_LOG = 3255;
    public static final int DIALOG_REPORT_FORCE_CLOSE = 3535788;
    public static final int DIALOG_SEND_LOG = 345350;
    public static final int Disable_Activity_CONTEXT_DIALOG = 29;
    public static final int Disable_COMPONENTS_CONTEXT_DIALOG = 31;
    public static final int EXT_PATCH_DIALOG = 5;
    public static final int LOADING_PROGRESS_DIALOG = 23;
    public static final int LVL_PATCH_CONTEXT_DIALOG = 20;
    public static final int MARKET_INSTALL_DIALOG = 30;
    private static final long MILLIS_PER_MINUTE = 60000;
    public static final int PATCH_CONTEXT_DIALOG = 13;
    public static final int PATCH_DIALOG = 2;
    public static final int PERM_CONTEXT_DIALOG = 10;
    public static final int PROGRESS_DIALOG = 1;
    public static final int PROGRESS_DIALOG2 = 11;
    public static final int RESTORE_FROM_BACKUP = 28;
    private static final String SETTINGS_ORIENT = "orientstion";
    private static final int SETTINGS_ORIENT_LANDSCAPE = 1;
    private static final int SETTINGS_ORIENT_PORTRET = 2;
    private static final String SETTINGS_SORTBY = "sortby";
    private static final int SETTINGS_SORTBY_DEFAULT = 2;
    private static final int SETTINGS_SORTBY_NAME = 1;
    private static final int SETTINGS_SORTBY_STATUS = 2;
    private static final int SETTINGS_SORTBY_TIME = 3;
    public static final String SETTINGS_VIEWSIZE = "viewsize";
    public static final int SETTINGS_VIEWSIZE_DEFAULT = 0;
    private static final int SETTINGS_VIEWSIZE_SMALL = 0;
    public static final int SUPPORT_PATCH_CONTEXT_DIALOG = 34;
    public static final int SYS_INFO = 27;
    public static final int Safe_PERM_CONTEXT_DIALOG = 22;
    public static final int Safe_Signature_PERM_CONTEXT_DIALOG = 25;
    public static final int TITLE_LIST_ITEM = 1;
    public static ArrayAdapter adapt = null;
    public static ArrayAdapter<BindItem> adaptBind = null;
    public static boolean adapterSelect = false;
    public static int adapterSelectType = SETTINGS_VIEWSIZE_SMALL;
    public static BootListItemAdapter adapter_boot = null;
    public static boolean addapps = false;
    public static int advancedFilter = SETTINGS_VIEWSIZE_SMALL;
    public static final int advanced_filter_apps_with_internet = 1931;
    public static final int advanced_filter_apps_with_without_internet = 1932;
    public static final int advanced_filter_for_apps_move2sd = 1929;
    public static final int advanced_filter_for_user_apps = 1930;
    static All_Dialogs all_d = null;
    public static int api = SETTINGS_VIEWSIZE_SMALL;
    public static boolean appDisabler = false;
    static App_Dialog app_d = null;
    public static Context appcontext = null;
    public static boolean asNeedUser = false;
    public static boolean asUser = false;
    public static String basepath = null;
    public static SQLiteDatabase billing_db = null;
    public static boolean binderWidget = false;
    public static boolean binder_process = false;
    public static ArrayList<PkgListItem> boot_pat = null;
    public static String[] bootlist = null;
    static Custom2_Dialog c2_d = null;
    static Create_Apk_Ads_Dialog c_a_a_d = null;
    static Create_Apk_Dialog c_a_d = null;
    static Create_Apk_Support_Dialog c_a_s_d = null;
    static Custom_Patch_Dialog c_p_d = null;
    public static ArrayList<String> changedPermissions = null;
    public static boolean checktools = false;
    public static ArrayAdapter<Components> component_adapt = null;
    public static SharedPreferences config = null;
    public static Context contextext = null;
    public static int countRoot = SETTINGS_VIEWSIZE_SMALL;
    public static final int create_SUPPORT_PATCH_CONTEXT_DIALOG = 35;
    public static File[] customlist = null;
    public static File customselect = null;
    public static String dalvikruncommand = BuildConfig.VERSION_NAME;
    public static String dalvikruncommandWithFramework = BuildConfig.VERSION_NAME;
    public static ArrayList<PkgListItem> data;
    public static DatabaseHelper database = null;
    public static int days = SETTINGS_VIEWSIZE_SMALL;
    public static boolean desktop_launch = false;
    public static int dialog_int = MotionEventCompat.ACTION_MASK;
    static Ext_Patch_Dialog e_p_d = null;
    public static String errorOutput = BuildConfig.VERSION_NAME;
    public static String extStorageDirectory = null;
    public static boolean fast_start = false;
    public static boolean fileNotSpace = false;
    public static LuckyFileObserver fileOb;
    public static boolean filePatch3 = false;
    public static FilterFragment filter = null;
    public static TimerFirstRunTask firstRunTask;
    public static Boolean firstrun = null;
    public static listAppsFragment frag;
    public static int func;
    public static boolean goodMemory = true;
    public static Handler handler = null;
    public static Handler handlerToast = null;
    private static final char[] hex = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
    public static boolean init = false;
    public static boolean install_market_to_system = false;
    private static Context instance;
    public static int lastExpandedGroupPosition = -1;
    public static Intent launchActivity = null;
    public static String luckyPackage;
    public static ExpandableListView lv;
    public static LogCollector mLogCollector = null;
    static ServiceConnection mServiceConnL;
    static Market_Install_Dialog m_i_d = null;
    static Menu menu1;
    public static MenuItemAdapter menu_adapter;
    static Menu_Dialog menu_d = null;
    static Menu_Dialog_Static menu_d_static = null;
    public static ExpandableListView menu_lv;
    public static boolean menu_open = false;
    public static patchActivity patchAct;
    public static PatchData patchData = null;
    public static boolean patchOnBoot = false;
    public static boolean patchOnlyDalvikCore = false;
    public static ArrayAdapter patch_adapt;
    static Patch_Dialog patch_d = null;
    public static ArrayAdapter<Perm> perm_adapt;
    public static PackageManager pkgManager = null;
    public static String pkgNam = "beleberda";
    public static PkgListItem pli;
    public static PkgListItemAdapter plia;
    public static String plipack;
    public static Progress_Dialog progress;
    public static Progress_Dialog_2 progress2;
    public static Progress_Dialog_Loading progress_loading;
    public static String rebuldApk = BuildConfig.VERSION_NAME;
    public static boolean refresh = false;
    public static Resources res = null;
    public static Resources resources = null;
    public static String result = BuildConfig.VERSION_NAME;
    public static String result_core_patch = BuildConfig.VERSION_NAME;
    public static boolean return_from_control_panel = false;
    public static boolean runResume = false;
    public static String runtime = BuildConfig.VERSION_NAME;
    static byte[] salt = new byte[]{(byte) -33, (byte) 43, (byte) 12, (byte) -98, (byte) -78, (byte) -24, (byte) 65, (byte) -42, (byte) 28, (byte) 17, (byte) -112, (byte) 99, (byte) 66, (byte) -34, (byte) -11, (byte) 13, (byte) -99, (byte) 32, (byte) -62, (byte) 89};
    public static String searchTag = "beleberda";
    public static ArrayList<PkgListItem> selectedApps = new ArrayList();
    public static Semaphore semaphoreRoot = new Semaphore(TITLE_LIST_ITEM);
    public static boolean server = false;
    public static ServerSocket serverSocket = null;
    public static boolean sqlScan = false;
    public static boolean sqlScanLite = false;
    public static Boolean startUnderRoot;
    public static String str;
    public static boolean su = false;
    public static DataInputStream suErrorInputStream = null;
    public static DataInputStream suInputStream = null;
    public static DataOutputStream suOutputStream = null;
    public static Process suProcess = null;
    public static String supath;
    public static int textSize = SETTINGS_VIEWSIZE_SMALL;
    public static String toolfilesdir;
    public static String[] tools = null;
    public static TextView tvt;
    public static TimerUnusedOdexTask unusedOdexTask;
    public static String verMarket = BuildConfig.VERSION_NAME;
    public static String version;
    public static int versionCodeLocal = SETTINGS_VIEWSIZE_SMALL;
    public static String[] viewbootlist;
    public static boolean xposedNotify = false;
    public String ApkName;
    public String AppName;
    public String BuildVersionChanges = BuildConfig.VERSION_NAME;
    public String BuildVersionPath = BuildConfig.VERSION_NAME;
    public String Changes = BuildConfig.VERSION_NAME;
    public int FalseHttp = SETTINGS_VIEWSIZE_SMALL;
    public String InstallAppPackageName;
    public String Mirror1 = BuildConfig.VERSION_NAME;
    public String PackageName;
    public String Text = BuildConfig.VERSION_NAME;
    public int VersionCode;
    public String VersionName = BuildConfig.VERSION_NAME;
    private ArrayList<String> activities;
    private FileApkListItem apkitem = null;
    private File backup = null;
    View but = null;
    public int count = SETTINGS_VIEWSIZE_SMALL;
    public ItemFile current;
    String dalvikcache = BuildConfig.VERSION_NAME;
    String dalvikcache2 = BuildConfig.VERSION_NAME;
    String datadir = "empty";
    public ListView filebrowser;
    ILicensingService mServiceL;
    public int maxcount = SETTINGS_VIEWSIZE_SMALL;
    TextView myPath;
    String package_name = "com.emulator.fpse";
    private ArrayList<String> permissions;
    String prefs_dir = "shared_prefs";
    File prefs_file;
    String prefs_name = "com.android.vending.licensing.ServerManagedPolicy.xml";
    int responseCode = MotionEventCompat.ACTION_MASK;
    String root;
    int start = SETTINGS_VIEWSIZE_SMALL;
    public String urlpath;
    Vibrator vib;

    class C03392 implements Runnable {

        class C03091 implements Runnable {
            C03091() {
            }

            public void run() {
                Toast.makeText(listAppsFragment.this.getContext(), Utils.getText(C0149R.string.error_app_installed_on_sdcard), listAppsFragment.TITLE_LIST_ITEM).show();
            }
        }

        class C03102 implements Runnable {
            C03102() {
            }

            public void run() {
                Toast.makeText(listAppsFragment.this.getContext(), "SDCard mounted:\n" + listAppsFragment.basepath.replace("/LuckyPatcher", BuildConfig.VERSION_NAME), listAppsFragment.SETTINGS_VIEWSIZE_SMALL).show();
            }
        }

        class C03113 implements Runnable {
            C03113() {
            }

            public void run() {
                Toast.makeText(listAppsFragment.this.getContext(), Utils.getText(C0149R.string.not_detect), listAppsFragment.SETTINGS_VIEWSIZE_SMALL).show();
            }
        }

        class C03124 implements Runnable {
            C03124() {
            }

            public void run() {
                Toast.makeText(listAppsFragment.this.getContext(), Utils.getText(C0149R.string.not_detect), listAppsFragment.SETTINGS_VIEWSIZE_SMALL).show();
            }
        }

        class C03135 implements Runnable {
            C03135() {
            }

            public void run() {
                Toast.makeText(listAppsFragment.this.getContext(), "SDCard mounted:\n" + listAppsFragment.basepath.replace("/LuckyPatcher", BuildConfig.VERSION_NAME), listAppsFragment.SETTINGS_VIEWSIZE_SMALL).show();
            }
        }

        class C03146 implements Runnable {
            C03146() {
            }

            public void run() {
                Toast.makeText(listAppsFragment.this.getContext(), Utils.getText(C0149R.string.not_detect), listAppsFragment.SETTINGS_VIEWSIZE_SMALL).show();
            }
        }

        class C03157 implements Runnable {
            C03157() {
            }

            public void run() {
                Toast.makeText(listAppsFragment.this.getContext(), Utils.getText(C0149R.string.not_detect), listAppsFragment.SETTINGS_VIEWSIZE_SMALL).show();
            }
        }

        class C03168 implements Runnable {
            C03168() {
            }

            public void run() {
                listAppsFragment.this.populateAdapter(listAppsFragment.data, listAppsFragment.this.getSortPref());
            }
        }

        class C03179 implements Runnable {
            C03179() {
            }

            public void run() {
                listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.root_access_not_found));
            }
        }

        C03392() {
        }

        public void run() {
            boolean newversion;
            Utils utils;
            String[] strArr;
            File tmp1 = new File(LuckyApp.getInstance().getFilesDir() + "/AndroidManifest.xml");
            if (tmp1.exists()) {
                tmp1.delete();
            }
            tmp1 = new File(LuckyApp.getInstance().getFilesDir() + "/classes.dex");
            if (tmp1.exists()) {
                tmp1.delete();
            }
            tmp1 = new File(LuckyApp.getInstance().getFilesDir() + "/classes.dex.apk");
            if (tmp1.exists()) {
                tmp1.delete();
            }
            if (listAppsFragment.versionCodeLocal == 0) {
                listAppsFragment.versionCodeLocal = 2000;
            }
            if (Utils.isInstalledOnSdCard(listAppsFragment.getInstance().getPackageName()) && listAppsFragment.handler != null) {
                listAppsFragment.handler.post(new C03091());
            }
            Editor editor2 = listAppsFragment.getConfig().edit();
            editor2.putBoolean("Update0", true);
            editor2.commit();
            listAppsFragment.extStorageDirectory = Environment.getExternalStorageDirectory().getAbsolutePath();
            try {
                System.out.println("LuckyPatcher (env sdcard): " + listAppsFragment.extStorageDirectory);
            } catch (Exception e) {
                e.printStackTrace();
            }
            listAppsFragment.basepath = listAppsFragment.extStorageDirectory + "/LuckyPatcher";
            try {
                if (listAppsFragment.api > listAppsFragment.Create_ADS_PATCH_CONTEXT_DIALOG) {
                    listAppsFragment.basepath = listAppsFragment.getInstance().getExternalFilesDir("LuckyPatcher").getAbsolutePath();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
                new File(listAppsFragment.extStorageDirectory + "/Android/data/" + listAppsFragment.luckyPackage + "/files/LuckyPatcher").mkdirs();
                listAppsFragment.basepath = listAppsFragment.extStorageDirectory + "/Android/data/" + listAppsFragment.luckyPackage + "/files/LuckyPatcher";
            }
            System.out.println(listAppsFragment.basepath);
            if (listAppsFragment.getConfig().getBoolean(listAppsFragment.version, false)) {
                newversion = false;
            } else {
                listAppsFragment.getConfig().edit().putBoolean(listAppsFragment.version, true).commit();
                newversion = true;
            }
            System.out.println("LuckyPatcher (sd environment): " + listAppsFragment.basepath);
            ArrayList arrayList;
            ArrayList<File> sdcard_dirs;
            Iterator it;
            if (listAppsFragment.getConfig().getBoolean("manual_path", false)) {
                listAppsFragment.basepath = listAppsFragment.getConfig().getString("basepath", listAppsFragment.basepath);
                System.out.println("LuckyPatcher (saved sdcard): " + listAppsFragment.extStorageDirectory);
                if (listAppsFragment.su && listAppsFragment.api > listAppsFragment.CUSTOM_PATCH_SELECTOR && listAppsFragment.basepath.startsWith("/storage/emulated/0/")) {
                    try {
                        new File(listAppsFragment.basepath + "/test.tmp").createNewFile();
                        if (new File(listAppsFragment.basepath + "/test.tmp").exists()) {
                            listAppsFragment.str = BuildConfig.VERSION_NAME;
                            utils = new Utils(BuildConfig.VERSION_NAME);
                            strArr = new String[listAppsFragment.TITLE_LIST_ITEM];
                            strArr[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = listAppsFragment.dalvikruncommand + ".check_sd_from_root " + "'" + listAppsFragment.basepath + "'";
                            listAppsFragment.str = utils.cmdRoot(strArr);
                            listAppsFragment.startUnderRoot = Boolean.valueOf(false);
                            System.out.println(listAppsFragment.str);
                            if (!listAppsFragment.str.contains("LuckyPatcher: Found test.tmp") && new File(listAppsFragment.basepath + "/test.tmp").exists()) {
                                listAppsFragment.str = BuildConfig.VERSION_NAME;
                                utils = new Utils(BuildConfig.VERSION_NAME);
                                strArr = new String[listAppsFragment.TITLE_LIST_ITEM];
                                strArr[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = listAppsFragment.dalvikruncommand + ".check_sd_from_root " + "'" + listAppsFragment.basepath.replace("/storage/emulated/0/", "/storage/emulated/legacy/") + "'";
                                listAppsFragment.str = utils.cmdRoot(strArr);
                                listAppsFragment.startUnderRoot = Boolean.valueOf(false);
                                System.out.println(listAppsFragment.str);
                                if (!listAppsFragment.str.contains("LuckyPatcher: Found test.tmp") || new File(listAppsFragment.basepath + "/test.tmp").exists()) {
                                    new File(listAppsFragment.basepath + "/test.tmp").delete();
                                } else {
                                    listAppsFragment.getConfig().edit().putString("extStorageDirectory", listAppsFragment.extStorageDirectory.replace("/storage/emulated/0/", "/storage/emulated/legacy/")).commit();
                                    listAppsFragment.getConfig().edit().putString("basepath", listAppsFragment.basepath.replace("/storage/emulated/0/", "/storage/emulated/legacy/")).commit();
                                    listAppsFragment.getConfig().edit().putString("path", listAppsFragment.basepath.replace("/storage/emulated/0/", "/storage/emulated/legacy/")).commit();
                                    listAppsFragment.basepath = listAppsFragment.basepath.replace("/storage/emulated/0/", "/storage/emulated/legacy/");
                                }
                            }
                        }
                    } catch (Exception e22) {
                        e22.printStackTrace();
                    }
                }
                if (listAppsFragment.this.testSD(false)) {
                    listAppsFragment.this.copyData(listAppsFragment.basepath, newversion);
                } else {
                    arrayList = new ArrayList();
                    sdcard_dirs = listAppsFragment.getStorage();
                    if (sdcard_dirs.size() > 0) {
                        it = sdcard_dirs.iterator();
                        while (it.hasNext()) {
                            listAppsFragment.basepath = ((File) it.next()).getAbsolutePath() + "/LuckyPatcher";
                            System.out.println("LuckyPatcher: sdcard from Environment.getExternalStorageDirectory() error... Test " + listAppsFragment.basepath);
                            if (listAppsFragment.this.testSD(false)) {
                                if (listAppsFragment.handler != null) {
                                    listAppsFragment.handler.post(new C03102());
                                }
                                System.out.println("LuckyPatcher: " + listAppsFragment.basepath + " work... Use to LP.");
                                if (listAppsFragment.this.testSD(false)) {
                                    try {
                                        listAppsFragment.extStorageDirectory = Environment.getExternalStorageDirectory().getAbsolutePath();
                                        try {
                                            listAppsFragment.basepath = listAppsFragment.getInstance().getExternalFilesDir("LuckyPatcher").getAbsolutePath();
                                        } catch (Exception e222) {
                                            e222.printStackTrace();
                                            new File(listAppsFragment.extStorageDirectory + "/Android/data/" + listAppsFragment.luckyPackage + "/files/LuckyPatcher").mkdirs();
                                            listAppsFragment.basepath = listAppsFragment.extStorageDirectory + "/Android/data/" + listAppsFragment.luckyPackage + "/files/LuckyPatcher";
                                        }
                                    } catch (Exception e2222) {
                                        e2222.printStackTrace();
                                    }
                                    if (listAppsFragment.this.testSD(false)) {
                                        if (listAppsFragment.handler != null) {
                                            listAppsFragment.handler.post(new C03113());
                                        }
                                        listAppsFragment.basepath = listAppsFragment.this.getContext().getDir("sdcard", listAppsFragment.SETTINGS_SORTBY_STATUS).getAbsolutePath() + "/LuckyPatcher";
                                        listAppsFragment.this.copyData(listAppsFragment.basepath, newversion);
                                    } else {
                                        listAppsFragment.getConfig().edit().putString("extStorageDirectory", listAppsFragment.extStorageDirectory).commit();
                                        listAppsFragment.getConfig().edit().putString("basepath", listAppsFragment.basepath).commit();
                                        listAppsFragment.getConfig().edit().putString("path", listAppsFragment.basepath).commit();
                                        listAppsFragment.getConfig().edit().putBoolean("manual_path", true).commit();
                                    }
                                } else {
                                    listAppsFragment.this.copyData(listAppsFragment.basepath, newversion);
                                }
                            }
                        }
                        if (listAppsFragment.this.testSD(false)) {
                            listAppsFragment.this.copyData(listAppsFragment.basepath, newversion);
                        } else {
                            listAppsFragment.extStorageDirectory = Environment.getExternalStorageDirectory().getAbsolutePath();
                            listAppsFragment.basepath = listAppsFragment.getInstance().getExternalFilesDir("LuckyPatcher").getAbsolutePath();
                            if (listAppsFragment.this.testSD(false)) {
                                listAppsFragment.getConfig().edit().putString("extStorageDirectory", listAppsFragment.extStorageDirectory).commit();
                                listAppsFragment.getConfig().edit().putString("basepath", listAppsFragment.basepath).commit();
                                listAppsFragment.getConfig().edit().putString("path", listAppsFragment.basepath).commit();
                                listAppsFragment.getConfig().edit().putBoolean("manual_path", true).commit();
                            } else {
                                if (listAppsFragment.handler != null) {
                                    listAppsFragment.handler.post(new C03113());
                                }
                                listAppsFragment.basepath = listAppsFragment.this.getContext().getDir("sdcard", listAppsFragment.SETTINGS_SORTBY_STATUS).getAbsolutePath() + "/LuckyPatcher";
                                listAppsFragment.this.copyData(listAppsFragment.basepath, newversion);
                            }
                        }
                    } else {
                        if (listAppsFragment.handler != null) {
                            listAppsFragment.handler.post(new C03124());
                        }
                        listAppsFragment.basepath = listAppsFragment.this.getContext().getDir("sdcard", listAppsFragment.SETTINGS_SORTBY_STATUS).getAbsolutePath() + "/LuckyPatcher";
                        listAppsFragment.this.copyData(listAppsFragment.basepath, newversion);
                    }
                }
            } else if (listAppsFragment.this.testSD(false)) {
                listAppsFragment.this.copyData(listAppsFragment.basepath, newversion);
                listAppsFragment.getConfig().edit().putString("extStorageDirectory", listAppsFragment.extStorageDirectory).commit();
                listAppsFragment.getConfig().edit().putString("basepath", listAppsFragment.basepath).commit();
                listAppsFragment.getConfig().edit().putString("path", listAppsFragment.basepath).commit();
                listAppsFragment.getConfig().edit().putBoolean("manual_path", true).commit();
            } else {
                System.out.println("LuckyPatcher (sdcard test faled): " + listAppsFragment.basepath);
                arrayList = new ArrayList();
                sdcard_dirs = listAppsFragment.getStorage();
                if (sdcard_dirs.size() > 0) {
                    it = sdcard_dirs.iterator();
                    while (it.hasNext()) {
                        File dir = (File) it.next();
                        listAppsFragment.basepath = dir.getAbsolutePath();
                        System.out.println("LuckyPatcher: sdcard from Environment.getExternalStorageDirectory() error... Test " + listAppsFragment.basepath);
                        if (listAppsFragment.this.testSD(false)) {
                            if (listAppsFragment.handler != null) {
                                listAppsFragment.handler.post(new C03135());
                            }
                            System.out.println("LuckyPatcher: " + listAppsFragment.basepath + " work... Use to LP.");
                            listAppsFragment.extStorageDirectory = dir.getAbsolutePath();
                            listAppsFragment.basepath = listAppsFragment.extStorageDirectory + "/LuckyPatcher";
                            if (listAppsFragment.this.testSD(false)) {
                                try {
                                    listAppsFragment.extStorageDirectory = Environment.getExternalStorageDirectory().getAbsolutePath();
                                    try {
                                        listAppsFragment.basepath = listAppsFragment.getInstance().getExternalFilesDir("LuckyPatcher").getAbsolutePath();
                                    } catch (Exception e22222) {
                                        e22222.printStackTrace();
                                        new File(listAppsFragment.extStorageDirectory + "/Android/data/" + listAppsFragment.luckyPackage + "/files/LuckyPatcher").mkdirs();
                                        listAppsFragment.basepath = listAppsFragment.extStorageDirectory + "/Android/data/" + listAppsFragment.luckyPackage + "/files/LuckyPatcher";
                                    }
                                } catch (Exception e222222) {
                                    e222222.printStackTrace();
                                }
                                if (listAppsFragment.this.testSD(false)) {
                                    if (listAppsFragment.handler != null) {
                                        listAppsFragment.handler.post(new C03146());
                                    }
                                    listAppsFragment.basepath = listAppsFragment.this.getContext().getDir("sdcard", listAppsFragment.SETTINGS_SORTBY_STATUS).getAbsolutePath() + "/LuckyPatcher";
                                } else {
                                    listAppsFragment.getConfig().edit().putString("extStorageDirectory", listAppsFragment.extStorageDirectory).commit();
                                    listAppsFragment.getConfig().edit().putString("basepath", listAppsFragment.basepath).commit();
                                    listAppsFragment.getConfig().edit().putString("path", listAppsFragment.basepath).commit();
                                    listAppsFragment.getConfig().edit().putBoolean("manual_path", true).commit();
                                }
                            } else {
                                listAppsFragment.getConfig().edit().putString("extStorageDirectory", listAppsFragment.extStorageDirectory).commit();
                                listAppsFragment.getConfig().edit().putString("basepath", listAppsFragment.basepath).commit();
                                listAppsFragment.getConfig().edit().putString("path", listAppsFragment.basepath).commit();
                                listAppsFragment.getConfig().edit().putBoolean("manual_path", true).commit();
                            }
                            listAppsFragment.this.copyData(listAppsFragment.basepath, newversion);
                        }
                    }
                    if (listAppsFragment.this.testSD(false)) {
                        listAppsFragment.getConfig().edit().putString("extStorageDirectory", listAppsFragment.extStorageDirectory).commit();
                        listAppsFragment.getConfig().edit().putString("basepath", listAppsFragment.basepath).commit();
                        listAppsFragment.getConfig().edit().putString("path", listAppsFragment.basepath).commit();
                        listAppsFragment.getConfig().edit().putBoolean("manual_path", true).commit();
                    } else {
                        listAppsFragment.extStorageDirectory = Environment.getExternalStorageDirectory().getAbsolutePath();
                        listAppsFragment.basepath = listAppsFragment.getInstance().getExternalFilesDir("LuckyPatcher").getAbsolutePath();
                        if (listAppsFragment.this.testSD(false)) {
                            listAppsFragment.getConfig().edit().putString("extStorageDirectory", listAppsFragment.extStorageDirectory).commit();
                            listAppsFragment.getConfig().edit().putString("basepath", listAppsFragment.basepath).commit();
                            listAppsFragment.getConfig().edit().putString("path", listAppsFragment.basepath).commit();
                            listAppsFragment.getConfig().edit().putBoolean("manual_path", true).commit();
                        } else {
                            if (listAppsFragment.handler != null) {
                                listAppsFragment.handler.post(new C03146());
                            }
                            listAppsFragment.basepath = listAppsFragment.this.getContext().getDir("sdcard", listAppsFragment.SETTINGS_SORTBY_STATUS).getAbsolutePath() + "/LuckyPatcher";
                        }
                    }
                    listAppsFragment.this.copyData(listAppsFragment.basepath, newversion);
                } else {
                    if (listAppsFragment.handler != null) {
                        listAppsFragment.handler.post(new C03157());
                    }
                    listAppsFragment.basepath = listAppsFragment.this.getContext().getDir("sdcard", listAppsFragment.SETTINGS_SORTBY_STATUS).getAbsolutePath() + "/LuckyPatcher";
                }
                listAppsFragment.this.copyData(listAppsFragment.basepath, newversion);
            }
            if (listAppsFragment.getConfig().getBoolean("first_run_lvl_emulation", true)) {
                listAppsFragment.getPkgMng().setComponentEnabledSetting(new ComponentName(listAppsFragment.getInstance(), LicensingService.class), listAppsFragment.SETTINGS_SORTBY_STATUS, listAppsFragment.TITLE_LIST_ITEM);
                listAppsFragment.getConfig().edit().putBoolean("first_run_lvl_emulation", false).commit();
            }
            if (listAppsFragment.data == null) {
                listAppsFragment.sqlScan = true;
                Thread scan = new Thread(new AppScanning());
                scan.setPriority(listAppsFragment.PERM_CONTEXT_DIALOG);
                scan.start();
            } else {
                listAppsFragment.this.runToMain(new C03168());
            }
            Timer timer = new Timer("UnusedOdexTimer");
            listAppsFragment.unusedOdexTask = new TimerUnusedOdexTask();
            timer.schedule(listAppsFragment.unusedOdexTask, 5000);
            timer = new Timer("FirstRunTimer");
            listAppsFragment.firstRunTask = new TimerFirstRunTask();
            timer.schedule(listAppsFragment.firstRunTask, 15000);
            String dalvik = BuildConfig.VERSION_NAME;
            if (new File("/system/bin/dalvikvm").exists()) {
                dalvik = "/system/bin/dalvikvm";
            }
            if (new File("/system/bin/dalvikvm32").exists()) {
                dalvik = "/system/bin/dalvikvm32";
            }
            if (dalvik.equals(BuildConfig.VERSION_NAME)) {
                File dalvikvm = new File(listAppsFragment.getInstance().getFilesDir() + "/dalvikvm");
                if (dalvikvm.exists()) {
                    dalvik = dalvikvm.getAbsolutePath();
                } else if (Utils.getRawToFile(C0149R.raw.dalvikvm_armv7a, dalvikvm)) {
                    Utils.run_all("chmod 777 " + dalvikvm.getAbsolutePath());
                    Utils.run_all("chown 0.0 " + dalvikvm.getAbsolutePath());
                    Utils.run_all("chown 0:0 " + dalvikvm.getAbsolutePath());
                    dalvik = dalvikvm.getAbsolutePath();
                }
            }
            listAppsFragment.dalvikruncommand = dalvik + " -Xbootclasspath:" + System.getenv("BOOTCLASSPATH") + " -Xverify:none -Xdexopt:none -cp " + LuckyApp.getInstance().getPackageCodePath() + " com.chelpus.root.utils";
            listAppsFragment.dalvikruncommandWithFramework = dalvik + " -Xbootclasspath:" + System.getenv("BOOTCLASSPATH") + " -Xverify:none -Xdexopt:none -cp " + LuckyApp.getInstance().getPackageCodePath() + " com.android.internal.util.WithFramework" + " com.chelpus.root.utils";
            if (listAppsFragment.su) {
                try {
                    utils = new Utils(BuildConfig.VERSION_NAME);
                    strArr = new String[listAppsFragment.TITLE_LIST_ITEM];
                    strArr[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = listAppsFragment.dalvikruncommand + ".checkRoot " + listAppsFragment.getInstance().getFilesDir() + " " + listAppsFragment.api + " " + listAppsFragment.runtime;
                    String result = utils.cmdRoot(strArr);
                    listAppsFragment.startUnderRoot = Boolean.valueOf(false);
                    System.out.println(result);
                    try {
                        if (result.contains("Unused ODEX in /data/app/ removed!")) {
                            String[] lines = result.split(LogCollector.LINE_SEPARATOR);
                            if (lines != null && lines.length > 0) {
                                int length = lines.length;
                                for (int i = listAppsFragment.SETTINGS_VIEWSIZE_SMALL; i < length; i += listAppsFragment.TITLE_LIST_ITEM) {
                                    String line = lines[i];
                                    if (line.startsWith("UnusedOdexList:")) {
                                        line = line.replaceAll("UnusedOdexList:", BuildConfig.VERSION_NAME).replaceAll("/data/app/", BuildConfig.VERSION_NAME);
                                        System.out.println(line);
                                        String[] files = line.split("\\|");
                                        if (files != null && files.length > 0) {
                                            System.out.println("files:" + files.length);
                                            for (int i2 = listAppsFragment.SETTINGS_VIEWSIZE_SMALL; i2 < files.length; i2 += listAppsFragment.TITLE_LIST_ITEM) {
                                                System.out.println("show notify:" + files[i2]);
                                                listAppsFragment.this.showNotify(i2, "Lucky AppManager - " + Utils.getText(C0149R.string.unused_odex_delete), Utils.getText(C0149R.string.unused_odex_delete), files[i2]);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } catch (Exception e2222222) {
                        e2222222.printStackTrace();
                    }
                    if (listAppsFragment.getConfig().getInt("root_force", listAppsFragment.SETTINGS_VIEWSIZE_SMALL) == 0) {
                        if (result.contains("root found!")) {
                            if (result.contains("busybox not found!") && listAppsFragment.handler != null) {
                                listAppsFragment.handler.post(new Runnable() {
                                    public void run() {
                                        Toast.makeText(listAppsFragment.this.getContext(), Utils.getText(C0149R.string.busybox_not_found), listAppsFragment.TITLE_LIST_ITEM).show();
                                    }
                                });
                            }
                            if (!(Utils.isWithFramework() || listAppsFragment.handler == null)) {
                                listAppsFragment.handler.post(new Runnable() {
                                    public void run() {
                                    }
                                });
                            }
                        } else {
                            listAppsFragment.su = false;
                            if (listAppsFragment.handler != null) {
                                listAppsFragment.handler.post(new C03179());
                            }
                        }
                    }
                    if (!listAppsFragment.getConfig().getBoolean("change_lp_pakgname", false)) {
                        new Thread(new Runnable() {
                            public void run() {
                                Utils utils = new Utils(BuildConfig.VERSION_NAME);
                                String[] strArr = new String[listAppsFragment.TITLE_LIST_ITEM];
                                strArr[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = "pm uninstall -k com.forpda.lp";
                                utils.cmdRoot(strArr);
                                try {
                                    listAppsFragment.getPkgMng().getPackageInfo("com.forpda.lp", listAppsFragment.SETTINGS_VIEWSIZE_SMALL);
                                } catch (NameNotFoundException e) {
                                    listAppsFragment.getConfig().edit().putBoolean("change_lp_pakgname", true).commit();
                                }
                            }
                        }).start();
                    }
                } catch (Exception e22222222) {
                    e22222222.printStackTrace();
                }
            }
            if (listAppsFragment.su) {
                listAppsFragment.this.setRootWidgetEnabled(true);
            } else {
                listAppsFragment.this.setRootWidgetEnabled(false);
            }
        }
    }

    class C03845 implements Runnable {

        class C03531 implements Runnable {

            class C03521 implements OnClickListener {
                C03521() {
                }

                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case PagerAdapter.POSITION_NONE /*-2*/:
                            if (listAppsFragment.this.VersionCode != 999) {
                                Editor editor = listAppsFragment.getConfig().edit();
                                editor.putBoolean("Update" + listAppsFragment.this.VersionCode, true);
                                editor.commit();
                                return;
                            }
                            return;
                        case Zip4jConstants.ENC_NO_ENCRYPTION /*-1*/:
                            DownloadVersion downloadVersion = new DownloadVersion();
                            String[] strArr = new String[listAppsFragment.TITLE_LIST_ITEM];
                            strArr[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = "Gruzimssite";
                            downloadVersion.execute(strArr);
                            return;
                        default:
                            return;
                    }
                }
            }

            C03531() {
            }

            public void run() {
                OnClickListener dialogClickListener = new C03521();
                Utils.showDialogCustomYes(BuildConfig.VERSION_NAME, Utils.getText(C0149R.string.new_version) + "\n\n" + Utils.getText(C0149R.string.begin_changelog) + " " + listAppsFragment.version + "\n\nChangeLog:\n\n" + listAppsFragment.this.Changes, Utils.getText(C0149R.string.update), dialogClickListener, dialogClickListener, null);
            }
        }

        C03845() {
        }

        public void run() {
            ByteArrayOutputStream baos;
            byte[] buffer;
            int len1;
            listAppsFragment com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment;
            listAppsFragment.this.Text = "New";
            listAppsFragment.this.FalseHttp = listAppsFragment.SETTINGS_VIEWSIZE_SMALL;
            listAppsFragment.this.VersionCode = listAppsFragment.SETTINGS_VIEWSIZE_SMALL;
            listAppsFragment.this.ApkName = "LuckyPatcher.apk";
            listAppsFragment.this.AppName = "Lucky Patcher";
            listAppsFragment.this.Mirror1 = "http://content.wuala.com/contents/chelpus/Luckypatcher/";
            listAppsFragment.this.BuildVersionPath = "http://chelpus.defcon5.biz/Version.txt";
            listAppsFragment.this.BuildVersionChanges = "http://chelpus.defcon5.biz/Changelogs.txt";
            listAppsFragment.this.PackageName = "package:" + listAppsFragment.luckyPackage;
            listAppsFragment.this.urlpath = "http://chelpus.defcon5.biz/" + listAppsFragment.this.ApkName;
            File outputFile = new File(new File(listAppsFragment.basepath + "/Download/"), listAppsFragment.this.ApkName);
            if (outputFile.exists()) {
                outputFile.delete();
            }
            for (boolean mirror = true; mirror; mirror = false) {
                InputStream in;
                try {
                    HttpURLConnection c = (HttpURLConnection) new URL(listAppsFragment.this.BuildVersionPath).openConnection();
                    c.setRequestMethod("GET");
                    c.setConnectTimeout(1000000);
                    c.setUseCaches(false);
                    c.setRequestProperty("Cache-Control", "no-cache");
                    c.connect();
                    in = c.getInputStream();
                    baos = new ByteArrayOutputStream();
                    buffer = new byte[AccessibilityNodeInfoCompat.ACTION_SCROLL_BACKWARD];
                    while (true) {
                        len1 = in.read(buffer);
                        if (len1 == -1) {
                            break;
                        }
                        baos.write(buffer, listAppsFragment.SETTINGS_VIEWSIZE_SMALL, len1);
                    }
                    String s = baos.toString();
                    listAppsFragment.this.VersionCode = Integer.parseInt(s);
                    baos.close();
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e2) {
                    e2.printStackTrace();
                    com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment = listAppsFragment.this;
                    com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment.FalseHttp += listAppsFragment.TITLE_LIST_ITEM;
                } catch (NumberFormatException e3) {
                    com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment = listAppsFragment.this;
                    com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment.FalseHttp += listAppsFragment.TITLE_LIST_ITEM;
                } catch (Exception e4) {
                    e4.printStackTrace();
                }
            }
            if (listAppsFragment.this.VersionCode <= listAppsFragment.versionCodeLocal) {
                return;
            }
            if (listAppsFragment.this.VersionCode != 999 || !listAppsFragment.getConfig().getBoolean("999", false)) {
                try {
                    c = (HttpURLConnection) new URL(listAppsFragment.this.BuildVersionChanges).openConnection();
                    c.setRequestMethod("GET");
                    c.setConnectTimeout(1000000);
                    c.setUseCaches(false);
                    c.setRequestProperty("Cache-Control", "no-cache");
                    c.connect();
                    in = c.getInputStream();
                    baos = new ByteArrayOutputStream();
                    buffer = new byte[AccessibilityNodeInfoCompat.ACTION_SCROLL_BACKWARD];
                    while (true) {
                        len1 = in.read(buffer);
                        if (len1 == -1) {
                            break;
                        }
                        baos.write(buffer, listAppsFragment.SETTINGS_VIEWSIZE_SMALL, len1);
                    }
                    listAppsFragment.this.Changes = baos.toString();
                    baos.close();
                } catch (MalformedURLException e5) {
                    e5.printStackTrace();
                } catch (IOException e22) {
                    e22.printStackTrace();
                } catch (NumberFormatException e6) {
                } catch (Exception e42) {
                    e42.printStackTrace();
                }
                listAppsFragment.this.runToMain(new C03531());
            }
        }
    }

    class C04016 implements OnClickListener {

        class C03871 implements Runnable {

            class C03851 implements Runnable {
                C03851() {
                }

                public void run() {
                    listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                    listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.odexed_not_all_apps));
                    listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
                }
            }

            class C03862 implements Runnable {
                C03862() {
                }

                public void run() {
                    listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                    listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
                }
            }

            C03871() {
            }

            public void run() {
                Utils.remount("/system", InternalZipConstants.WRITE_MODE);
                Utils utils = new Utils(BuildConfig.VERSION_NAME);
                String[] strArr = new String[listAppsFragment.TITLE_LIST_ITEM];
                strArr[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = listAppsFragment.dalvikruncommand + ".odexSystemApps " + listAppsFragment.toolfilesdir;
                listAppsFragment.str = utils.cmdRoot(strArr);
                if (listAppsFragment.str.contains("Not enought space!") || listAppsFragment.str.contains("IO Exception!")) {
                    listAppsFragment.this.runToMain(new C03851());
                } else {
                    listAppsFragment.this.runToMain(new C03862());
                }
                Utils.reboot();
            }
        }

        C04016() {
        }

        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case Zip4jConstants.ENC_NO_ENCRYPTION /*-1*/:
                    try {
                        listAppsFragment.this.runWithWait(new C03871());
                        return;
                    } catch (Exception e) {
                        Toast.makeText(listAppsFragment.this.getContext(), BuildConfig.VERSION_NAME + e, listAppsFragment.TITLE_LIST_ITEM).show();
                        return;
                    }
                default:
                    return;
            }
        }
    }

    class C04147 implements OnClickListener {

        class C04031 implements Runnable {

            class C04021 implements Runnable {
                C04021() {
                }

                public void run() {
                    listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                    listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
                }
            }

            C04031() {
            }

            public void run() {
                listAppsFragment.getInstance().deleteDatabase("BillingRestoreTransactions");
                listAppsFragment.this.runToMain(new C04021());
            }
        }

        C04147() {
        }

        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case Zip4jConstants.ENC_NO_ENCRYPTION /*-1*/:
                    try {
                        listAppsFragment.this.runWithWait(new C04031());
                        return;
                    } catch (Exception e) {
                        Toast.makeText(listAppsFragment.this.getContext(), BuildConfig.VERSION_NAME + e, listAppsFragment.TITLE_LIST_ITEM).show();
                        return;
                    }
                default:
                    return;
            }
        }
    }

    class C04268 implements OnClickListener {

        class C04161 implements Runnable {

            class C04151 implements Runnable {
                C04151() {
                }

                public void run() {
                    listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                    listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
                }
            }

            C04161() {
            }

            public void run() {
                Utils.remount("/system", InternalZipConstants.WRITE_MODE);
                Utils utils = new Utils(BuildConfig.VERSION_NAME);
                String[] strArr = new String[listAppsFragment.TITLE_LIST_ITEM];
                strArr[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = listAppsFragment.dalvikruncommand + ".ClearODEXfiles " + listAppsFragment.toolfilesdir;
                listAppsFragment.str = utils.cmdRoot(strArr);
                listAppsFragment.refresh = true;
                Utils.remount("/system", "ro");
                listAppsFragment.this.runToMain(new C04151());
                Utils.reboot();
            }
        }

        C04268() {
        }

        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case Zip4jConstants.ENC_NO_ENCRYPTION /*-1*/:
                    try {
                        listAppsFragment.this.runWithWait(new C04161());
                        return;
                    } catch (Exception e) {
                        Toast.makeText(listAppsFragment.this.getContext(), BuildConfig.VERSION_NAME + e, listAppsFragment.TITLE_LIST_ITEM).show();
                        return;
                    }
                default:
                    return;
            }
        }
    }

    class C04679 implements OnClickListener {

        class C04281 implements Runnable {

            class C04271 implements Runnable {
                C04271() {
                }

                public void run() {
                    listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                    listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
                }
            }

            C04281() {
            }

            public void run() {
                Utils.run_all("rm /data/dalvik-cache/*");
                Utils.run_all("rm /cache/dalvik-cache/*");
                Utils.run_all("rm /data/dalvik-cache/arm/*");
                Utils.run_all("rm /cache/dalvik-cache/arm/*");
                Utils.reboot();
                listAppsFragment.this.runToMain(new C04271());
                Utils.reboot();
            }
        }

        C04679() {
        }

        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case Zip4jConstants.ENC_NO_ENCRYPTION /*-1*/:
                    try {
                        listAppsFragment.this.runWithWait(new C04281());
                        return;
                    } catch (Exception e) {
                        Toast.makeText(listAppsFragment.this.getContext(), BuildConfig.VERSION_NAME + e, listAppsFragment.TITLE_LIST_ITEM).show();
                        return;
                    }
                default:
                    return;
            }
        }
    }

    public class AppScanning implements Runnable {

        class C04681 implements Runnable {
            C04681() {
            }

            public void run() {
                try {
                    listAppsFragment.removeDialogLP(listAppsFragment.LOADING_PROGRESS_DIALOG);
                } catch (Exception e) {
                }
                listAppsFragment.showDialogLP(listAppsFragment.TITLE_LIST_ITEM);
            }
        }

        class C04692 implements Runnable {
            C04692() {
            }

            public void run() {
                try {
                    listAppsFragment.removeDialogLP(listAppsFragment.LOADING_PROGRESS_DIALOG);
                } catch (Exception e) {
                }
                listAppsFragment.showDialogLP(listAppsFragment.TITLE_LIST_ITEM);
            }
        }

        class C04703 implements Runnable {
            C04703() {
            }

            public void run() {
                try {
                    listAppsFragment.removeDialogLP(listAppsFragment.TITLE_LIST_ITEM);
                } catch (Exception e) {
                }
                listAppsFragment.showDialogLP(listAppsFragment.LOADING_PROGRESS_DIALOG);
                listAppsFragment.progress_loading.setMessage(Utils.getText(C0149R.string.wait) + "...");
                listAppsFragment.progress_loading.setCancelable(true);
                listAppsFragment.progress_loading.setMax(listAppsFragment.this.maxcount);
                listAppsFragment.progress_loading.setTitle(Utils.getText(C0149R.string.loadpkg));
            }
        }

        class C04714 implements Runnable {
            C04714() {
            }

            public void run() {
                try {
                    listAppsFragment.progress_loading.setProgress(listAppsFragment.this.count);
                } catch (Exception e) {
                }
            }
        }

        class C04725 implements Runnable {
            C04725() {
            }

            public void run() {
                try {
                    listAppsFragment.progress_loading.setProgress(listAppsFragment.this.count);
                } catch (Exception e) {
                }
            }
        }

        class C04736 implements Runnable {
            C04736() {
            }

            public void run() {
                listAppsFragment.this.showMessage(Utils.getText(C0149R.string.information), Utils.getText(C0149R.string.apps_not_found));
            }
        }

        class C04747 implements Runnable {
            C04747() {
            }

            public void run() {
                try {
                    listAppsFragment.removeDialogLP(listAppsFragment.LOADING_PROGRESS_DIALOG);
                } catch (Exception e) {
                }
                try {
                    listAppsFragment.removeDialogLP(listAppsFragment.TITLE_LIST_ITEM);
                } catch (Exception e2) {
                }
                if (listAppsFragment.sqlScanLite) {
                    listAppsFragment.this.populateAdapter(listAppsFragment.data, listAppsFragment.this.getSortPref());
                }
                if (listAppsFragment.sqlScan) {
                    listAppsFragment.this.populateAdapter(listAppsFragment.data, listAppsFragment.this.getSortPref());
                }
            }
        }

        public void run() {
            listAppsFragment.this.runToMain(new C04681());
            listAppsFragment.data = null;
            listAppsFragment.data = new ArrayList();
            if (listAppsFragment.database == null) {
                listAppsFragment.database = new DatabaseHelper(listAppsFragment.getInstance());
            }
            try {
                if (listAppsFragment.sqlScanLite) {
                    listAppsFragment.this.runToMain(new C04692());
                }
            } catch (Exception e) {
            }
            boolean skipIcon = false;
            if (listAppsFragment.fast_start) {
                skipIcon = true;
            }
            if (!skipIcon) {
                skipIcon = listAppsFragment.getConfig().getBoolean("no_icon", false);
            }
            if (listAppsFragment.advancedFilter == 0) {
                listAppsFragment.data = listAppsFragment.database.getPackage(false, skipIcon);
            } else {
                listAppsFragment.data = listAppsFragment.database.getPackage(true, skipIcon);
            }
            if (listAppsFragment.data.size() == 0 && listAppsFragment.advancedFilter == 0) {
                listAppsFragment.data.clear();
                String[] packages = listAppsFragment.getPackages();
                listAppsFragment.this.maxcount = packages.length;
                try {
                    listAppsFragment.this.runToMain(new C04703());
                } catch (Exception e2) {
                }
                try {
                    listAppsFragment.this.count = listAppsFragment.TITLE_LIST_ITEM;
                    boolean sysshow = listAppsFragment.getConfig().getBoolean("systemapp", false);
                    try {
                        listAppsFragment.this.runToMain(new C04714());
                    } catch (Exception e3) {
                    }
                    boolean iconTrigger = true;
                    if (listAppsFragment.getConfig().getBoolean("no_icon", false)) {
                        iconTrigger = false;
                    }
                    int length = packages.length;
                    for (int i = listAppsFragment.SETTINGS_VIEWSIZE_SMALL; i < length; i += listAppsFragment.TITLE_LIST_ITEM) {
                        String pkgname = packages[i];
                        try {
                            listAppsFragment com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment = listAppsFragment.this;
                            com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment.count += listAppsFragment.TITLE_LIST_ITEM;
                            PkgListItem local = new PkgListItem(listAppsFragment.getInstance(), pkgname, listAppsFragment.days, iconTrigger);
                            local.saveItem();
                            if (local.boot_ads || local.boot_custom || local.boot_lvl || local.boot_manual) {
                                listAppsFragment.boot_pat.add(local);
                            }
                        } catch (IllegalArgumentException e4) {
                        }
                        listAppsFragment.this.runToMain(new C04725());
                    }
                    listAppsFragment.data = listAppsFragment.database.getPackage(false, false);
                } catch (Exception e5) {
                    System.out.println("LuckyPatcher (AppScanner): " + e5);
                    e5.printStackTrace();
                }
            }
            if (listAppsFragment.data.size() == 0 && listAppsFragment.advancedFilter != 0) {
                listAppsFragment.advancedFilter = listAppsFragment.SETTINGS_VIEWSIZE_SMALL;
                if (listAppsFragment.adapterSelect) {
                    listAppsFragment.this.resetBatchOperation();
                }
                listAppsFragment.handler.post(new C04736());
            }
            listAppsFragment.this.runToMain(new C04747());
        }
    }

    class CheckUpdate implements Runnable {

        class C04751 implements Runnable {
            C04751() {
            }

            public void run() {
                GetChangeLog getChangeLog = new GetChangeLog();
                String[] strArr = new String[listAppsFragment.TITLE_LIST_ITEM];
                strArr[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = BuildConfig.VERSION_NAME;
                getChangeLog.execute(strArr);
            }
        }

        CheckUpdate() {
        }

        public void run() {
            System.out.println("LuckyPatcher: check new version to Internet.");
            listAppsFragment.this.BuildVersionPath = "http://chelpus.defcon5.biz/Version.txt";
            listAppsFragment.this.VersionCode = listAppsFragment.SETTINGS_VIEWSIZE_SMALL;
            for (boolean mirror = true; mirror; mirror = false) {
                try {
                    HttpURLConnection c = (HttpURLConnection) new URL(listAppsFragment.this.BuildVersionPath).openConnection();
                    c.setRequestMethod("GET");
                    c.setConnectTimeout(1000000);
                    c.setUseCaches(false);
                    c.setRequestProperty("Cache-Control", "no-cache");
                    c.connect();
                    InputStream in = c.getInputStream();
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    byte[] buffer = new byte[AccessibilityNodeInfoCompat.ACTION_SCROLL_BACKWARD];
                    while (true) {
                        int len1 = in.read(buffer);
                        if (len1 == -1) {
                            break;
                        }
                        baos.write(buffer, listAppsFragment.SETTINGS_VIEWSIZE_SMALL, len1);
                    }
                    String s2 = baos.toString();
                    listAppsFragment.this.VersionCode = Integer.parseInt(s2);
                    baos.close();
                } catch (MalformedURLException e) {
                } catch (IOException e2) {
                    System.out.println("LuckyPatcher(AutoUpdate): Internet connection not found.");
                } catch (NumberFormatException e3) {
                } catch (Exception e4) {
                    try {
                        e4.printStackTrace();
                    } catch (Exception e5) {
                        System.out.println("LuckyPatcher: Internet permission removed from LP.");
                    }
                }
            }
            listAppsFragment.getConfig().edit();
            try {
                if (!listAppsFragment.getConfig().getBoolean("Update" + listAppsFragment.this.VersionCode, false)) {
                    if (listAppsFragment.this.VersionCode <= listAppsFragment.versionCodeLocal) {
                        return;
                    }
                    if (listAppsFragment.this.VersionCode != 999 || !listAppsFragment.getConfig().getBoolean("999", false)) {
                        listAppsFragment.this.runToMain(new C04751());
                        if (listAppsFragment.this.FalseHttp >= listAppsFragment.SETTINGS_SORTBY_STATUS) {
                            Toast.makeText(listAppsFragment.this.getContext(), Utils.getText(C0149R.string.no_site), listAppsFragment.SETTINGS_VIEWSIZE_SMALL).show();
                        }
                    }
                }
            } catch (Exception e42) {
                Toast.makeText(listAppsFragment.this.getContext(), BuildConfig.VERSION_NAME + e42, listAppsFragment.SETTINGS_VIEWSIZE_SMALL).show();
                e42.printStackTrace();
            }
        }
    }

    class CustomPatch implements Runnable {
        private String appdir = BuildConfig.VERSION_NAME;
        private String cmd1 = BuildConfig.VERSION_NAME;
        private String cmd2 = BuildConfig.VERSION_NAME;
        private File custom = null;
        private String filesdir = BuildConfig.VERSION_NAME;
        private String result = null;
        private String system_app = BuildConfig.VERSION_NAME;
        private String uid = BuildConfig.VERSION_NAME;

        class C04761 implements Runnable {
            C04761() {
            }

            public void run() {
                if (listAppsFragment.progress_loading != null && listAppsFragment.progress_loading.isShowing()) {
                    listAppsFragment.progress_loading.setMessage(Utils.getText(C0149R.string.patch_step1));
                    listAppsFragment.progress_loading.setProgress(listAppsFragment.TITLE_LIST_ITEM);
                }
            }
        }

        class C04772 implements Runnable {
            C04772() {
            }

            public void run() {
                if (listAppsFragment.progress_loading != null && listAppsFragment.progress_loading.isShowing()) {
                    listAppsFragment.progress_loading.setMessage(Utils.getText(C0149R.string.patch_step2));
                    listAppsFragment.progress_loading.setProgress(listAppsFragment.SETTINGS_VIEWSIZE_SMALL);
                    listAppsFragment.progress_loading.setProgressNumberFormat(Utils.getText(C0149R.string.patch_step_http));
                }
            }
        }

        class C04783 implements Runnable {
            C04783() {
            }

            public void run() {
                listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.no_freespace_patch));
            }
        }

        class C04794 implements Runnable {
            C04794() {
            }

            public void run() {
                listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.error_create_oat));
            }
        }

        class C04805 implements Runnable {
            C04805() {
            }

            public void run() {
                listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.no_space_in_data_all));
            }
        }

        class C04816 implements Runnable {
            C04816() {
            }

            public void run() {
                listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.unknown_error));
            }
        }

        class C04827 implements Runnable {
            C04827() {
            }

            public void run() {
                try {
                    listAppsFragment.plia.getItem(listAppsFragment.pli.pkgName).modified = true;
                    listAppsFragment.refresh = true;
                    listAppsFragment.getConfig().edit().putBoolean(listAppsFragment.plia.getItem(listAppsFragment.pli.pkgName).pkgName, true).commit();
                    listAppsFragment.plia.notifyDataSetChanged(listAppsFragment.plia.getItem(listAppsFragment.pli.pkgName));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                listAppsFragment.removeDialogLP(listAppsFragment.LOADING_PROGRESS_DIALOG);
                listAppsFragment.removeDialogLP(listAppsFragment.Custom_PATCH_DIALOG);
                listAppsFragment.showDialogLP(listAppsFragment.Custom_PATCH_DIALOG);
            }
        }

        CustomPatch() {
        }

        public void run() {
            System.out.println("Start");
            try {
                PkgListItem plil = listAppsFragment.pli;
                listAppsFragment.str = BuildConfig.VERSION_NAME;
                listAppsFragment.this.runToMain(new C04761());
                listAppsFragment.this.runToMain(new C04772());
                this.result = BuildConfig.VERSION_NAME;
                Utils utils = new Utils(BuildConfig.VERSION_NAME);
                String[] strArr = new String[listAppsFragment.TITLE_LIST_ITEM];
                strArr[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = this.cmd1;
                this.result = utils.cmdRoot(strArr);
                listAppsFragment.startUnderRoot = Boolean.valueOf(false);
                if (this.result.contains("Create odex error 1")) {
                    listAppsFragment.this.runToMain(new C04783());
                }
                if (this.result.contains("Create odex error 2")) {
                    listAppsFragment.this.runToMain(new C04794());
                }
                if (this.result.contains("Create odex error 3")) {
                    listAppsFragment.this.runToMain(new C04805());
                }
                if (this.result.contains("Create odex error 4")) {
                    listAppsFragment.this.runToMain(new C04816());
                }
                listAppsFragment.this.afterCustomPatch(this.custom, this.cmd1, this.cmd2, this.appdir, this.filesdir, this.uid, this.result);
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("luuuuu " + e);
                listAppsFragment.str = this.result;
                listAppsFragment.this.runToMain(new C04827());
            }
        }
    }

    public class DownloadCustomPatch extends AsyncTask<String, Integer, Boolean> {
        boolean cacheFound = false;
        boolean corruptdownload = false;
        String filename = "mod.market4.apk";
        boolean internet_not_found = false;
        int numbytes = listAppsFragment.SETTINGS_VIEWSIZE_SMALL;

        protected void onPreExecute() {
            super.onPreExecute();
            listAppsFragment.showDialogLP(listAppsFragment.LOADING_PROGRESS_DIALOG);
        }

        protected Boolean doInBackground(String... params) {
            String urlpath = "http://chelpus.defcon5.biz/" + params[listAppsFragment.SETTINGS_VIEWSIZE_SMALL];
            this.filename = params[listAppsFragment.SETTINGS_VIEWSIZE_SMALL];
            try {
                HttpURLConnection con = (HttpURLConnection) new URL(urlpath).openConnection();
                con.setRequestMethod("HEAD");
                con.setRequestProperty("Cache-Control", "no-cache");
                con.setConnectTimeout(1000000);
                con.connect();
                this.numbytes = Integer.parseInt(con.getHeaderField("Content-length"));
                PrintStream printStream = System.out;
                Object[] objArr = new Object[listAppsFragment.SETTINGS_SORTBY_STATUS];
                objArr[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = Integer.valueOf(this.numbytes);
                objArr[listAppsFragment.TITLE_LIST_ITEM] = Float.valueOf(((float) this.numbytes) / 1048576.0f);
                printStream.println(String.format("%s bytes found, %s Mb", objArr));
                con.disconnect();
            } catch (Exception e) {
                e.printStackTrace();
                this.internet_not_found = true;
                this.numbytes = listAppsFragment.SETTINGS_VIEWSIZE_SMALL;
            }
            if (listAppsFragment.progress_loading != null && listAppsFragment.progress_loading.isShowing()) {
                listAppsFragment.progress_loading.setMax(this.numbytes / Util.BLOCK_HEADER_SIZE_MAX);
            }
            this.cacheFound = false;
            if (new File(listAppsFragment.basepath + "/Download/" + params[listAppsFragment.SETTINGS_VIEWSIZE_SMALL]).exists()) {
                if (this.internet_not_found) {
                    this.cacheFound = true;
                } else {
                    System.out.println(new File(listAppsFragment.basepath + "/Download/" + params[listAppsFragment.SETTINGS_VIEWSIZE_SMALL]).length());
                    System.out.println(this.numbytes);
                    if (new File(listAppsFragment.basepath + "/Download/" + params[listAppsFragment.SETTINGS_VIEWSIZE_SMALL]).length() == ((long) this.numbytes) || this.numbytes == 0) {
                        this.cacheFound = true;
                    }
                }
            }
            if (!this.internet_not_found && !this.cacheFound) {
                try {
                    HttpURLConnection c = (HttpURLConnection) new URL(urlpath).openConnection();
                    c.setConnectTimeout(1000000);
                    c.setRequestMethod("GET");
                    c.setUseCaches(false);
                    c.setRequestProperty("Cache-Control", "no-cache");
                    c.connect();
                    File file = new File(listAppsFragment.basepath + "/Download/");
                    if (!file.exists()) {
                        file.mkdirs();
                    }
                    File outputFile = new File(file, params[listAppsFragment.SETTINGS_VIEWSIZE_SMALL]);
                    if (outputFile.exists()) {
                        outputFile.delete();
                    }
                    FileOutputStream fos = new FileOutputStream(outputFile);
                    InputStream is = c.getInputStream();
                    byte[] buffer = new byte[AccessibilityNodeInfoCompat.ACTION_SCROLL_BACKWARD];
                    int size = listAppsFragment.SETTINGS_VIEWSIZE_SMALL;
                    while (true) {
                        int len1 = is.read(buffer);
                        if (len1 == -1) {
                            break;
                        }
                        size += len1;
                        Integer[] numArr = new Integer[listAppsFragment.TITLE_LIST_ITEM];
                        numArr[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = Integer.valueOf(size / Util.BLOCK_HEADER_SIZE_MAX);
                        publishProgress(numArr);
                        fos.write(buffer, listAppsFragment.SETTINGS_VIEWSIZE_SMALL, len1);
                    }
                    fos.close();
                    is.close();
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
                if (new File(listAppsFragment.basepath + "/Download/" + params[listAppsFragment.SETTINGS_VIEWSIZE_SMALL]).exists()) {
                    if (new File(listAppsFragment.basepath + "/Download/" + params[listAppsFragment.SETTINGS_VIEWSIZE_SMALL]).length() == ((long) this.numbytes)) {
                        this.cacheFound = true;
                    } else {
                        new File(listAppsFragment.basepath + "/Download/" + params[listAppsFragment.SETTINGS_VIEWSIZE_SMALL]).delete();
                        this.corruptdownload = true;
                        this.cacheFound = false;
                    }
                }
            } else if (new File(listAppsFragment.basepath + "/Download/" + params[listAppsFragment.SETTINGS_VIEWSIZE_SMALL]).exists() && !this.cacheFound) {
                new File(listAppsFragment.basepath + "/Download/" + params[listAppsFragment.SETTINGS_VIEWSIZE_SMALL]).delete();
            }
            return Boolean.valueOf(true);
        }

        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            if (listAppsFragment.progress_loading != null && listAppsFragment.progress_loading.isShowing()) {
                listAppsFragment.progress_loading.setProgress(values[listAppsFragment.SETTINGS_VIEWSIZE_SMALL].intValue());
            }
        }

        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            listAppsFragment.removeDialogLP(listAppsFragment.LOADING_PROGRESS_DIALOG);
            if (this.corruptdownload) {
                listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.corrupt_download));
            }
            if (this.internet_not_found && !this.cacheFound) {
                listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.internet_not_found));
            }
            if (this.cacheFound && new File(listAppsFragment.basepath + "/Download/" + this.filename).exists()) {
                try {
                    FileApkListItem mrt = new FileApkListItem(listAppsFragment.getInstance(), new File(listAppsFragment.basepath + "/Download/" + this.filename), false);
                    if (listAppsFragment.asUser) {
                        listAppsFragment.this.toolbar_restore(mrt, true);
                    } else {
                        listAppsFragment.this.install_system(mrt);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.corrupt_download));
                }
            }
        }
    }

    public class DownloadFile extends AsyncTask<String, Integer, Boolean> {
        boolean cacheFound = false;
        boolean corruptdownload = false;
        boolean downloadedFound = false;
        String filename = "mod.market4.apk";
        boolean internet_not_found = false;
        int numbytes = listAppsFragment.SETTINGS_VIEWSIZE_SMALL;
        Runnable on_post_run_cached = null;
        Runnable on_post_run_downloaded = null;

        protected void onPreExecute() {
            super.onPreExecute();
            listAppsFragment.showDialogLP(listAppsFragment.LOADING_PROGRESS_DIALOG);
        }

        protected Boolean doInBackground(String... params) {
            String urlpath = "http://chelpus.defcon5.biz/" + params[listAppsFragment.SETTINGS_VIEWSIZE_SMALL];
            this.filename = params[listAppsFragment.SETTINGS_VIEWSIZE_SMALL];
            try {
                HttpURLConnection con = (HttpURLConnection) new URL(urlpath).openConnection();
                con.setRequestMethod("HEAD");
                con.setRequestProperty("Cache-Control", "no-cache");
                con.setConnectTimeout(1000000);
                con.connect();
                this.numbytes = Integer.parseInt(con.getHeaderField("Content-length"));
                PrintStream printStream = System.out;
                Object[] objArr = new Object[listAppsFragment.SETTINGS_SORTBY_STATUS];
                objArr[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = Integer.valueOf(this.numbytes);
                objArr[listAppsFragment.TITLE_LIST_ITEM] = Float.valueOf(((float) this.numbytes) / 1048576.0f);
                printStream.println(String.format("%s bytes found, %s Mb", objArr));
                con.disconnect();
            } catch (Exception e) {
                e.printStackTrace();
                this.internet_not_found = true;
                this.numbytes = listAppsFragment.SETTINGS_VIEWSIZE_SMALL;
            }
            if (listAppsFragment.progress_loading != null && listAppsFragment.progress_loading.isShowing()) {
                listAppsFragment.progress_loading.setMax(this.numbytes / Util.BLOCK_HEADER_SIZE_MAX);
            }
            this.cacheFound = false;
            if (new File(listAppsFragment.basepath + "/Download/" + params[listAppsFragment.SETTINGS_VIEWSIZE_SMALL]).exists()) {
                if (this.internet_not_found) {
                    this.cacheFound = true;
                } else {
                    System.out.println(new File(listAppsFragment.basepath + "/Download/" + params[listAppsFragment.SETTINGS_VIEWSIZE_SMALL]).length());
                    System.out.println(this.numbytes);
                    if (new File(listAppsFragment.basepath + "/Download/" + params[listAppsFragment.SETTINGS_VIEWSIZE_SMALL]).length() == ((long) this.numbytes) || this.numbytes == 0) {
                        this.cacheFound = true;
                    }
                }
            }
            if (!this.internet_not_found && !this.cacheFound) {
                try {
                    HttpURLConnection c = (HttpURLConnection) new URL(urlpath).openConnection();
                    c.setRequestMethod("GET");
                    c.setConnectTimeout(1000000);
                    c.setUseCaches(false);
                    c.setRequestProperty("Cache-Control", "no-cache");
                    c.connect();
                    File file = new File(listAppsFragment.basepath + "/Download/");
                    if (!file.exists()) {
                        file.mkdirs();
                    }
                    File outputFile = new File(file, params[listAppsFragment.SETTINGS_VIEWSIZE_SMALL]);
                    if (outputFile.exists()) {
                        outputFile.delete();
                    }
                    FileOutputStream fos = new FileOutputStream(outputFile);
                    InputStream is = c.getInputStream();
                    byte[] buffer = new byte[AccessibilityNodeInfoCompat.ACTION_SCROLL_BACKWARD];
                    int size = listAppsFragment.SETTINGS_VIEWSIZE_SMALL;
                    while (true) {
                        int len1 = is.read(buffer);
                        if (len1 == -1) {
                            break;
                        }
                        size += len1;
                        Integer[] numArr = new Integer[listAppsFragment.TITLE_LIST_ITEM];
                        numArr[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = Integer.valueOf(size / Util.BLOCK_HEADER_SIZE_MAX);
                        publishProgress(numArr);
                        fos.write(buffer, listAppsFragment.SETTINGS_VIEWSIZE_SMALL, len1);
                    }
                    fos.close();
                    is.close();
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
                if (new File(listAppsFragment.basepath + "/Download/" + params[listAppsFragment.SETTINGS_VIEWSIZE_SMALL]).exists()) {
                    if (new File(listAppsFragment.basepath + "/Download/" + params[listAppsFragment.SETTINGS_VIEWSIZE_SMALL]).length() == ((long) this.numbytes)) {
                        this.cacheFound = true;
                        this.downloadedFound = true;
                    } else {
                        new File(listAppsFragment.basepath + "/Download/" + params[listAppsFragment.SETTINGS_VIEWSIZE_SMALL]).delete();
                        this.corruptdownload = true;
                        this.cacheFound = false;
                    }
                }
            } else if (new File(listAppsFragment.basepath + "/Download/" + params[listAppsFragment.SETTINGS_VIEWSIZE_SMALL]).exists() && !this.cacheFound) {
                new File(listAppsFragment.basepath + "/Download/" + params[listAppsFragment.SETTINGS_VIEWSIZE_SMALL]).delete();
            }
            return Boolean.valueOf(true);
        }

        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            if (listAppsFragment.progress_loading != null && listAppsFragment.progress_loading.isShowing()) {
                listAppsFragment.progress_loading.setProgress(values[listAppsFragment.SETTINGS_VIEWSIZE_SMALL].intValue());
            }
        }

        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            listAppsFragment.removeDialogLP(listAppsFragment.LOADING_PROGRESS_DIALOG);
            if (this.corruptdownload) {
                listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.corrupt_download));
            }
            if (this.internet_not_found && !this.cacheFound) {
                listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.internet_not_found));
            }
            if (this.cacheFound && new File(listAppsFragment.basepath + "/Download/" + this.filename).exists()) {
                try {
                    if (!(!this.cacheFound || this.downloadedFound || this.on_post_run_cached == null)) {
                        this.on_post_run_cached.run();
                    }
                    if (this.downloadedFound && this.on_post_run_downloaded != null) {
                        this.on_post_run_downloaded.run();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.corrupt_download));
                }
            }
        }
    }

    class DownloadVersion extends AsyncTask<String, Integer, Boolean> {
        boolean corruptdownload = false;
        boolean host_down = false;
        int numbytes = listAppsFragment.SETTINGS_VIEWSIZE_SMALL;

        DownloadVersion() {
        }

        protected void onPreExecute() {
            super.onPreExecute();
            listAppsFragment.showDialogLP(listAppsFragment.LOADING_PROGRESS_DIALOG);
        }

        protected Boolean doInBackground(String... params) {
            try {
                HttpURLConnection con = (HttpURLConnection) new URL(listAppsFragment.this.urlpath).openConnection();
                con.setRequestMethod("HEAD");
                con.setConnectTimeout(1000000);
                con.setRequestProperty("Cache-Control", "no-cache");
                con.connect();
                this.numbytes = Integer.parseInt(con.getHeaderField("Content-length"));
                PrintStream printStream = System.out;
                Object[] objArr = new Object[listAppsFragment.SETTINGS_SORTBY_STATUS];
                objArr[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = Integer.valueOf(this.numbytes);
                objArr[listAppsFragment.TITLE_LIST_ITEM] = Float.valueOf(((float) this.numbytes) / 1048576.0f);
                printStream.println(String.format("%s bytes found, %s Mb", objArr));
                con.disconnect();
            } catch (Exception e) {
                this.numbytes = listAppsFragment.SETTINGS_VIEWSIZE_SMALL;
                e.printStackTrace();
            }
            if (this.numbytes != 0) {
                HttpURLConnection c;
                if (listAppsFragment.progress_loading != null && listAppsFragment.progress_loading.isShowing()) {
                    listAppsFragment.progress_loading.setMax(this.numbytes / Util.BLOCK_HEADER_SIZE_MAX);
                }
                try {
                    c = (HttpURLConnection) new URL(listAppsFragment.this.urlpath).openConnection();
                    c.setRequestMethod("GET");
                    c.setUseCaches(false);
                    c.setConnectTimeout(600000);
                    c.setRequestProperty("Cache-Control", "no-cache");
                    c.connect();
                    File file = new File(listAppsFragment.basepath + "/Download/");
                    if (!file.exists()) {
                        file.mkdirs();
                    }
                    File outputFile = new File(file, listAppsFragment.this.ApkName);
                    if (outputFile.exists()) {
                        outputFile.delete();
                    }
                    FileOutputStream fos = new FileOutputStream(outputFile);
                    InputStream is = c.getInputStream();
                    byte[] buffer = new byte[AccessibilityNodeInfoCompat.ACTION_SCROLL_BACKWARD];
                    int size = listAppsFragment.SETTINGS_VIEWSIZE_SMALL;
                    while (true) {
                        int len1 = is.read(buffer);
                        if (len1 == -1) {
                            break;
                        }
                        size += len1;
                        Integer[] numArr = new Integer[listAppsFragment.TITLE_LIST_ITEM];
                        numArr[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = Integer.valueOf(size / Util.BLOCK_HEADER_SIZE_MAX);
                        publishProgress(numArr);
                        fos.write(buffer, listAppsFragment.SETTINGS_VIEWSIZE_SMALL, len1);
                    }
                    fos.close();
                    is.close();
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(listAppsFragment.this.PackageName));
                try {
                    c = (HttpURLConnection) new URL("http://chelpus.defcon5.biz/link_counter.php?ver=" + listAppsFragment.this.VersionCode).openConnection();
                    c.setRequestMethod("GET");
                    c.setConnectTimeout(1000000);
                    c.setUseCaches(false);
                    c.setRequestProperty("Cache-Control", "no-cache");
                    c.connect();
                    c.getInputStream();
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    c.disconnect();
                } catch (MalformedURLException e3) {
                    e3.printStackTrace();
                } catch (IOException e22) {
                    e22.printStackTrace();
                } catch (NumberFormatException e4) {
                } catch (Exception e5) {
                    e5.printStackTrace();
                }
                if (new File(listAppsFragment.basepath + "/Download/" + listAppsFragment.this.ApkName).exists()) {
                    if (new File(listAppsFragment.basepath + "/Download/" + listAppsFragment.this.ApkName).length() == ((long) this.numbytes)) {
                        intent.setDataAndType(Uri.fromFile(new File(listAppsFragment.basepath + "/Download/" + listAppsFragment.this.ApkName)), "application/vnd.android.package-archive");
                        intent.setFlags(AccessibilityNodeInfoCompat.ACTION_SET_SELECTION);
                        listAppsFragment.patchAct.startActivity(intent);
                        if (listAppsFragment.this.VersionCode == 999) {
                            Editor editor = listAppsFragment.getConfig().edit();
                            editor.putBoolean("999", true);
                            editor.commit();
                            Utils utils = new Utils(BuildConfig.VERSION_NAME);
                            String[] strArr = new String[listAppsFragment.TITLE_LIST_ITEM];
                            strArr[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = "pm uninstall -k " + listAppsFragment.luckyPackage;
                            utils.cmdRoot(strArr);
                            Utils.kill(listAppsFragment.luckyPackage);
                        }
                    } else {
                        new File(listAppsFragment.basepath + "/Download/" + listAppsFragment.this.ApkName).delete();
                        this.corruptdownload = true;
                    }
                }
            } else {
                this.host_down = true;
            }
            return Boolean.valueOf(true);
        }

        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            if (listAppsFragment.progress_loading != null) {
                listAppsFragment.progress_loading.setProgress(values[listAppsFragment.SETTINGS_VIEWSIZE_SMALL].intValue());
            }
        }

        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            listAppsFragment.removeDialogLP(listAppsFragment.LOADING_PROGRESS_DIALOG);
            if (this.corruptdownload) {
                listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.corrupt_download));
            }
            if (this.host_down) {
                listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.host_down));
            }
        }
    }

    class GetChangeLog extends AsyncTask<String, Void, Boolean> {

        class C04831 implements OnClickListener {
            C04831() {
            }

            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case PagerAdapter.POSITION_NONE /*-2*/:
                        if (listAppsFragment.this.VersionCode != 999) {
                            Editor editor = listAppsFragment.getConfig().edit();
                            editor.putBoolean("Update" + listAppsFragment.this.VersionCode, true);
                            editor.commit();
                            return;
                        }
                        return;
                    case Zip4jConstants.ENC_NO_ENCRYPTION /*-1*/:
                        DownloadVersion downloadVersion = new DownloadVersion();
                        String[] strArr = new String[listAppsFragment.TITLE_LIST_ITEM];
                        strArr[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = "Gruzimssite";
                        downloadVersion.execute(strArr);
                        return;
                    default:
                        return;
                }
            }
        }

        GetChangeLog() {
        }

        protected Boolean doInBackground(String... params) {
            InputStream in;
            listAppsFragment com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment;
            System.out.println("LuckyPatcher: download changelog.");
            listAppsFragment.this.Text = "New";
            listAppsFragment.this.FalseHttp = listAppsFragment.SETTINGS_VIEWSIZE_SMALL;
            listAppsFragment.this.VersionCode = listAppsFragment.SETTINGS_VIEWSIZE_SMALL;
            listAppsFragment.this.ApkName = "LuckyPatcher.apk";
            listAppsFragment.this.AppName = "Lucky Patcher";
            listAppsFragment.this.Mirror1 = "http://content.wuala.com/contents/chelpus/Luckypatcher/";
            listAppsFragment.this.BuildVersionPath = "http://chelpus.defcon5.biz/Version.txt";
            listAppsFragment.this.BuildVersionChanges = "http://chelpus.defcon5.biz/Changelogs.txt";
            listAppsFragment.this.PackageName = "package:" + listAppsFragment.luckyPackage;
            listAppsFragment.this.urlpath = "http://chelpus.defcon5.biz/" + listAppsFragment.this.ApkName;
            File outputFile = new File(new File(listAppsFragment.basepath + "/Download/"), listAppsFragment.this.ApkName);
            if (outputFile.exists()) {
                outputFile.delete();
            }
            for (boolean mirror = true; mirror; mirror = false) {
                ByteArrayOutputStream baos;
                byte[] buffer;
                int len1;
                try {
                    HttpURLConnection c = (HttpURLConnection) new URL(listAppsFragment.this.BuildVersionPath).openConnection();
                    c.setRequestMethod("GET");
                    c.setConnectTimeout(1000000);
                    c.setUseCaches(false);
                    c.setRequestProperty("Cache-Control", "no-cache");
                    c.connect();
                    in = c.getInputStream();
                    baos = new ByteArrayOutputStream();
                    buffer = new byte[AccessibilityNodeInfoCompat.ACTION_SCROLL_BACKWARD];
                    while (true) {
                        len1 = in.read(buffer);
                        if (len1 == -1) {
                            break;
                        }
                        baos.write(buffer, listAppsFragment.SETTINGS_VIEWSIZE_SMALL, len1);
                    }
                    String s = baos.toString();
                    listAppsFragment.this.VersionCode = Integer.parseInt(s);
                    baos.close();
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e2) {
                    e2.printStackTrace();
                    com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment = listAppsFragment.this;
                    com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment.FalseHttp += listAppsFragment.TITLE_LIST_ITEM;
                } catch (NumberFormatException e3) {
                    com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment = listAppsFragment.this;
                    com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment.FalseHttp += listAppsFragment.TITLE_LIST_ITEM;
                } catch (Exception e4) {
                    e4.printStackTrace();
                }
            }
            if (listAppsFragment.this.VersionCode > listAppsFragment.versionCodeLocal && !(listAppsFragment.this.VersionCode == 999 && listAppsFragment.getConfig().getBoolean("999", false))) {
                try {
                    c = (HttpURLConnection) new URL(listAppsFragment.this.BuildVersionChanges).openConnection();
                    c.setRequestMethod("GET");
                    c.setConnectTimeout(1000000);
                    c.setUseCaches(false);
                    c.setRequestProperty("Cache-Control", "no-cache");
                    c.connect();
                    in = c.getInputStream();
                    baos = new ByteArrayOutputStream();
                    buffer = new byte[AccessibilityNodeInfoCompat.ACTION_SCROLL_BACKWARD];
                    while (true) {
                        len1 = in.read(buffer);
                        if (len1 == -1) {
                            break;
                        }
                        baos.write(buffer, listAppsFragment.SETTINGS_VIEWSIZE_SMALL, len1);
                    }
                    listAppsFragment.this.Changes = baos.toString();
                    Utils.save_text_to_file(new File(listAppsFragment.basepath + "/Changes/changelog.txt"), listAppsFragment.this.Changes);
                    baos.close();
                } catch (MalformedURLException e5) {
                    e5.printStackTrace();
                } catch (IOException e22) {
                    e22.printStackTrace();
                } catch (NumberFormatException e6) {
                } catch (Exception e42) {
                    e42.printStackTrace();
                }
            }
            return Boolean.valueOf(true);
        }

        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            OnClickListener dialogClickListener = new C04831();
            Utils.showDialogCustomYes(BuildConfig.VERSION_NAME, Utils.getText(C0149R.string.new_version) + "\n\n" + Utils.getText(C0149R.string.begin_changelog) + " " + listAppsFragment.version + "\n\nChangeLog:\n\n" + listAppsFragment.this.Changes, Utils.getText(C0149R.string.update), dialogClickListener, dialogClickListener, null);
        }
    }

    public class ItemFile {
        public String file = BuildConfig.VERSION_NAME;
        public String full = BuildConfig.VERSION_NAME;
        public String path = BuildConfig.VERSION_NAME;

        public ItemFile(String pathfile) {
            this.full = pathfile;
            this.path = new File(pathfile).getPath();
            this.file = new File(pathfile).getName();
        }

        public ItemFile(String filename, String path1) {
            this.full = path1;
            this.path = path1;
            this.file = filename;
        }
    }

    public class TimerFirstRunTask extends TimerTask {
        public static final int DIALOG_REPORT_FORCE_CLOSE = 3535788;
        boolean isPaused = false;

        class C04841 implements Runnable {
            C04841() {
            }

            public void run() {
                if (!listAppsFragment.getConfig().getBoolean("disable_autoupdate", false)) {
                    listAppsFragment.this.runUpdateTask();
                }
            }
        }

        public void run() {
            listAppsFragment.this.runToMain(new C04841());
        }
    }

    public class TimerUnusedOdexTask extends TimerTask {
        boolean isPaused = false;

        public void run() {
            if (listAppsFragment.su) {
                listAppsFragment.this.busyboxcopy();
                listAppsFragment.this.pinfocopy();
                listAppsFragment.this.iptablescopy();
                listAppsFragment.this.rebootcopy();
            }
        }
    }

    class Unusedodex implements Runnable {
        Unusedodex() {
        }

        public void run() {
        }
    }

    class UpdateVersion extends AsyncTask<String, Void, Boolean> {
        UpdateVersion() {
        }

        protected void onPreExecute() {
            super.onPreExecute();
            listAppsFragment.showDialogLP(listAppsFragment.PROGRESS_DIALOG2);
        }

        protected Boolean doInBackground(String... params) {
            listAppsFragment.this.BuildVersionPath = "http://chelpus.defcon5.biz/Version.txt";
            listAppsFragment.this.VersionCode = listAppsFragment.SETTINGS_VIEWSIZE_SMALL;
            for (boolean mirror = true; mirror; mirror = false) {
                try {
                    HttpURLConnection c = (HttpURLConnection) new URL(listAppsFragment.this.BuildVersionPath).openConnection();
                    c.setRequestMethod("GET");
                    c.setConnectTimeout(1000000);
                    c.setUseCaches(false);
                    c.setRequestProperty("Cache-Control", "no-cache");
                    c.connect();
                    InputStream in = c.getInputStream();
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    byte[] buffer = new byte[AccessibilityNodeInfoCompat.ACTION_SCROLL_BACKWARD];
                    while (true) {
                        int len1 = in.read(buffer);
                        if (len1 == -1) {
                            break;
                        }
                        baos.write(buffer, listAppsFragment.SETTINGS_VIEWSIZE_SMALL, len1);
                    }
                    String s2 = baos.toString();
                    listAppsFragment.this.VersionCode = Integer.parseInt(s2);
                    baos.close();
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                    return Boolean.valueOf(true);
                } catch (IOException e2) {
                    e2.printStackTrace();
                    return Boolean.valueOf(true);
                } catch (NumberFormatException e3) {
                    return Boolean.valueOf(true);
                } catch (Exception e4) {
                    e4.printStackTrace();
                }
            }
            return Boolean.valueOf(true);
        }

        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
            try {
                if (listAppsFragment.this.VersionCode <= listAppsFragment.versionCodeLocal || (listAppsFragment.this.VersionCode == 999 && listAppsFragment.getConfig().getBoolean("999", false))) {
                    listAppsFragment.this.showMessage(Utils.getText(C0149R.string.title_upd), Utils.getText(C0149R.string.equals_version));
                }
                if (listAppsFragment.this.VersionCode <= listAppsFragment.versionCodeLocal) {
                    return;
                }
                if (listAppsFragment.this.VersionCode != 999 || !listAppsFragment.getConfig().getBoolean("999", false)) {
                    listAppsFragment.this.updateapp();
                    if (listAppsFragment.this.FalseHttp >= listAppsFragment.SETTINGS_SORTBY_STATUS) {
                        Toast.makeText(listAppsFragment.this.getContext(), Utils.getText(C0149R.string.no_site), listAppsFragment.SETTINGS_VIEWSIZE_SMALL).show();
                    }
                }
            } catch (NameNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    final class byNameApkFile implements Comparator<FileApkListItem> {
        byNameApkFile() {
        }

        public int compare(FileApkListItem a, FileApkListItem b) {
            if (a == null || b == null) {
                throw new ClassCastException();
            }
            if (a.pkgName.equals(b.pkgName)) {
                if (a.versionCode > b.versionCode) {
                    return listAppsFragment.TITLE_LIST_ITEM;
                }
                if (a.versionCode < b.versionCode) {
                    return -1;
                }
                if (a.versionCode == b.versionCode) {
                    return listAppsFragment.SETTINGS_VIEWSIZE_SMALL;
                }
            }
            return a.name.compareToIgnoreCase(b.name);
        }
    }

    final class byNameFile implements Comparator<ItemFile> {
        byNameFile() {
        }

        public int compare(ItemFile a, ItemFile b) {
            if (a == null || b == null) {
                throw new ClassCastException();
            } else if (new File(a.full).isDirectory() && new File(b.full).isDirectory()) {
                return a.full.compareToIgnoreCase(b.full);
            } else {
                if (new File(a.full).isDirectory() && !new File(b.full).isDirectory()) {
                    return -1;
                }
                if (!new File(a.full).isDirectory() && !new File(b.full).isDirectory()) {
                    return a.full.compareToIgnoreCase(b.full);
                }
                if (new File(a.full).isDirectory() || !new File(b.full).isDirectory()) {
                    return a.full.compareToIgnoreCase(b.full);
                }
                return listAppsFragment.TITLE_LIST_ITEM;
            }
        }
    }

    class byPkgName implements Comparator<PkgListItem> {
        byPkgName() {
        }

        public int compare(PkgListItem a, PkgListItem b) {
            if (a == null || b == null) {
                throw new ClassCastException();
            } else if (a.stored != 0 && b.stored != 0) {
                return a.toString().compareToIgnoreCase(b.toString());
            } else {
                int ret = Integer.valueOf(a.stored).compareTo(Integer.valueOf(b.stored));
                if (ret == 0) {
                    return a.toString().compareToIgnoreCase(b.toString());
                }
                return ret;
            }
        }
    }

    final class byPkgStatus implements Comparator<PkgListItem> {
        byPkgStatus() {
        }

        public int compare(PkgListItem a, PkgListItem b) {
            if (a == null || b == null) {
                throw new ClassCastException();
            }
            int ret = Integer.valueOf(a.stored).compareTo(Integer.valueOf(b.stored));
            if (ret != 0) {
                return ret;
            }
            if (a.stored == 0) {
                int statA = MotionEventCompat.ACTION_MASK;
                int statB = MotionEventCompat.ACTION_MASK;
                if (!(a.ads || a.lvl)) {
                    statA = listAppsFragment.Custom_PATCH_DIALOG;
                }
                if (!(b.ads || b.lvl)) {
                    statB = listAppsFragment.Custom_PATCH_DIALOG;
                }
                if (a.ads) {
                    statA = listAppsFragment.SETTINGS_SORTBY_TIME;
                }
                if (b.ads) {
                    statB = listAppsFragment.SETTINGS_SORTBY_TIME;
                }
                if (a.lvl) {
                    statA = listAppsFragment.SETTINGS_SORTBY_STATUS;
                }
                if (b.lvl) {
                    statB = listAppsFragment.SETTINGS_SORTBY_STATUS;
                }
                if (a.custom) {
                    statA = listAppsFragment.TITLE_LIST_ITEM;
                }
                if (b.custom) {
                    statB = listAppsFragment.TITLE_LIST_ITEM;
                }
                return Integer.valueOf(statA).compareTo(Integer.valueOf(statB));
            }
            try {
                return a.toString().compareToIgnoreCase(b.toString());
            } catch (NullPointerException e) {
                e.printStackTrace();
                return listAppsFragment.SETTINGS_VIEWSIZE_SMALL;
            }
        }
    }

    final class byPkgTime implements Comparator<PkgListItem> {
        byPkgTime() {
        }

        public int compare(PkgListItem a, PkgListItem b) {
            if (a != null && b != null) {
                return Integer.valueOf(b.updatetime).compareTo(Integer.valueOf(a.updatetime));
            }
            throw new ClassCastException();
        }
    }

    static {
        String[] strArr = new String[TITLE_LIST_ITEM];
        strArr[SETTINGS_VIEWSIZE_SMALL] = "empty";
        bootlist = strArr;
        strArr = new String[TITLE_LIST_ITEM];
        strArr[SETTINGS_VIEWSIZE_SMALL] = "empty";
        viewbootlist = strArr;
    }

    public FragmentActivity getContext() {
        if (isAdded()) {
            patchAct = (patchActivity) getActivity();
        }
        if (patchAct == null) {
            patchAct = (patchActivity) LuckyApp.getInstance();
        }
        return patchAct;
    }

    private static String byteArray2Hex(byte[] bytes) {
        StringBuilder sb = new StringBuilder(bytes.length * SETTINGS_SORTBY_STATUS);
        int length = bytes.length;
        for (int i = SETTINGS_VIEWSIZE_SMALL; i < length; i += TITLE_LIST_ITEM) {
            byte b = bytes[i];
            sb.append(hex[(b & 240) >> Custom_PATCH_DIALOG]);
            sb.append(hex[b & CUSTOM2_DIALOG]);
        }
        return sb.toString();
    }

    private static String bytes2String(byte[] bytes) {
        StringBuilder string = new StringBuilder();
        int length = bytes.length;
        for (int i = SETTINGS_VIEWSIZE_SMALL; i < length; i += TITLE_LIST_ITEM) {
            String hexString = Integer.toHexString(bytes[i] & MotionEventCompat.ACTION_MASK);
            if (hexString.length() == TITLE_LIST_ITEM) {
                hexString = "0" + hexString;
            }
            string.append(hexString);
        }
        return string.toString();
    }

    public static PackageManager getPkgMng() {
        if (pkgManager == null) {
            pkgManager = getInstance().getPackageManager();
        }
        return pkgManager;
    }

    public static Resources getRes() {
        if (resources == null) {
            resources = getInstance().getResources();
        }
        return resources;
    }

    public void showMenu() {
        menu_open = true;
        menu_adapter.notifyDataSetChanged();
        menu_lv.setVisibility(SETTINGS_VIEWSIZE_SMALL);
        ((LinearLayout) patchAct.findViewById(C0149R.id.listapps)).setVisibility(CREATEAPK_DIALOG);
    }

    public void hideMenu() {
        menu_open = false;
        menu_adapter.clear();
        menu_lv.setVisibility(CREATEAPK_DIALOG);
        ((LinearLayout) patchAct.findViewById(C0149R.id.listapps)).setVisibility(SETTINGS_VIEWSIZE_SMALL);
        if (runResume) {
            onResume();
            runResume = false;
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view;
        System.out.println("View created");
        if (su) {
            view = inflater.inflate(C0149R.layout.pkglistviewlayout, container);
        } else {
            view = inflater.inflate(C0149R.layout.pkglistviewlayout_no_root, container);
        }
        lv = (ExpandableListView) view.findViewById(C0149R.id.explistapps);
        menu_lv = (ExpandableListView) view.findViewById(C0149R.id.menu_list);
        HorizontalScrollView sv = (HorizontalScrollView) view.findViewById(C0149R.id.horizontalScrollToolbar);
        Button bt = (Button) sv.findViewById(C0149R.id.button8);
        if (bt != null) {
            bt.setTextAppearance(getContext(), getSizeText());
            bt.setTextColor(Color.parseColor("#999999"));
        }
        bt = (Button) sv.findViewById(C0149R.id.button7);
        if (bt != null) {
            bt.setTextAppearance(getContext(), getSizeText());
            bt.setTextColor(Color.parseColor("#999999"));
        }
        bt = (Button) sv.findViewById(C0149R.id.button1);
        if (bt != null) {
            bt.setTextAppearance(getContext(), getSizeText());
            bt.setTextColor(Color.parseColor("#999999"));
        }
        bt = (Button) sv.findViewById(C0149R.id.button2);
        if (bt != null) {
            bt.setTextAppearance(getContext(), getSizeText());
            bt.setTextColor(Color.parseColor("#999999"));
        }
        bt = (Button) sv.findViewById(C0149R.id.button3);
        if (bt != null) {
            bt.setTextAppearance(getContext(), getSizeText());
            bt.setTextColor(Color.parseColor("#999999"));
        }
        bt = (Button) sv.findViewById(C0149R.id.button4);
        if (bt != null) {
            bt.setTextAppearance(getContext(), getSizeText());
            bt.setTextColor(Color.parseColor("#999999"));
        }
        if (data == null) {
            sqlScan = true;
            Thread scan = new Thread(new AppScanning());
            scan.setPriority(PERM_CONTEXT_DIALOG);
            scan.start();
        } else {
            populateAdapter(data, getSortPref());
        }
        return view;
    }

    public static void init() {
        if (!init) {
            init = true;
            instance = LuckyApp.getInstance();
            if (firstrun == null) {
                firstrun = Boolean.valueOf(true);
            }
            luckyPackage = LuckyApp.getInstance().getApplicationInfo().packageName;
            startUnderRoot = Boolean.valueOf(false);
            api = VERSION.SDK_INT;
            try {
                versionCodeLocal = getPkgMng().getPackageInfo(LuckyApp.getInstance().getPackageName(), SETTINGS_VIEWSIZE_SMALL).versionCode;
                version = getPkgMng().getPackageInfo(LuckyApp.getInstance().getPackageName(), SETTINGS_VIEWSIZE_SMALL).versionName;
            } catch (Exception e) {
                e.printStackTrace();
            }
            System.out.println("LuckyPatcher " + version + ": Application Start!");
            if (getConfig().getBoolean("fast_start", false)) {
                fast_start = true;
            }
            String lng = getConfig().getString("force_language", "default");
            try {
                if (lng.equals("default")) {
                    Locale appLoc3 = Resources.getSystem().getConfiguration().locale;
                    Locale.setDefault(Locale.getDefault());
                    Configuration appConfig3 = new Configuration();
                    appConfig3.locale = appLoc3;
                    LuckyApp.getInstance().getResources().updateConfiguration(appConfig3, LuckyApp.getInstance().getResources().getDisplayMetrics());
                } else {
                    Locale locale = null;
                    String[] tails = lng.split("_");
                    if (tails.length == TITLE_LIST_ITEM) {
                        locale = new Locale(tails[SETTINGS_VIEWSIZE_SMALL]);
                    }
                    if (tails.length == SETTINGS_SORTBY_STATUS) {
                        locale = new Locale(tails[SETTINGS_VIEWSIZE_SMALL], tails[TITLE_LIST_ITEM], BuildConfig.VERSION_NAME);
                    }
                    if (tails.length == SETTINGS_SORTBY_TIME) {
                        locale = new Locale(tails[SETTINGS_VIEWSIZE_SMALL], tails[TITLE_LIST_ITEM], tails[SETTINGS_SORTBY_STATUS]);
                    }
                    Locale.setDefault(locale);
                    Configuration appConfig2 = new Configuration();
                    appConfig2.locale = locale;
                    LuckyApp.getInstance().getResources().updateConfiguration(appConfig2, LuckyApp.getInstance().getResources().getDisplayMetrics());
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            boot_pat = new ArrayList();
            try {
                toolfilesdir = LuckyApp.getInstance().getFilesDir().getAbsolutePath();
            } catch (Exception e3) {
            }
            supath = LuckyApp.getInstance().getPackageCodePath();
            int r = getConfig().getInt("root_force", SETTINGS_VIEWSIZE_SMALL);
            if (r == 0 && (new File("/system/bin/su").exists() || new File("/system/xbin/su").exists())) {
                su = true;
            }
            if (r == TITLE_LIST_ITEM) {
                su = true;
            }
            if (r == SETTINGS_SORTBY_STATUS) {
                su = false;
            }
            String dalvik = BuildConfig.VERSION_NAME;
            extStorageDirectory = Environment.getExternalStorageDirectory().getAbsolutePath();
            basepath = extStorageDirectory + "/LuckyPatcher";
            try {
                if (api > Create_ADS_PATCH_CONTEXT_DIALOG) {
                    basepath = LuckyApp.getInstance().getExternalFilesDir("LuckyPatcher").getAbsolutePath();
                }
            } catch (Exception e22) {
                e22.printStackTrace();
                new File(extStorageDirectory + "/Android/data/" + luckyPackage + "/files/LuckyPatcher").mkdirs();
                basepath = extStorageDirectory + "/Android/data/" + luckyPackage + "/files/LuckyPatcher";
            }
            basepath = extStorageDirectory + "/LuckyPatcher";
            if (getConfig().getBoolean("manual_path", false)) {
                basepath = getConfig().getString("basepath", basepath);
            }
            if (new File("/system/bin/dalvikvm").exists()) {
                dalvik = "/system/bin/dalvikvm";
            }
            if (new File("/system/bin/dalvikvm32").exists()) {
                dalvik = "/system/bin/dalvikvm32";
            }
            if (dalvik.equals(BuildConfig.VERSION_NAME) && su) {
                File dalvikvm = new File(LuckyApp.getInstance().getFilesDir() + "/dalvikvm");
                if (dalvikvm.exists()) {
                    dalvik = dalvikvm.getAbsolutePath();
                    Utils.run_all("chmod 777 " + dalvikvm.getAbsolutePath());
                    Utils.run_all("chown 0.0 " + dalvikvm.getAbsolutePath());
                    Utils.run_all("chown 0:0 " + dalvikvm.getAbsolutePath());
                } else if (Utils.getRawToFile(C0149R.raw.dalvikvm_armv7a, dalvikvm)) {
                    Utils.run_all("chmod 777 " + dalvikvm.getAbsolutePath());
                    Utils.run_all("chown 0.0 " + dalvikvm.getAbsolutePath());
                    Utils.run_all("chown 0:0 " + dalvikvm.getAbsolutePath());
                    dalvik = dalvikvm.getAbsolutePath();
                }
            }
            dalvikruncommand = dalvik + " -Xbootclasspath:" + System.getenv("BOOTCLASSPATH") + " -Xverify:none -Xdexopt:none -cp " + LuckyApp.getInstance().getPackageCodePath() + " com.chelpus.root.utils";
            dalvikruncommandWithFramework = dalvik + " -Xbootclasspath:" + System.getenv("BOOTCLASSPATH") + " -Xverify:none -Xdexopt:none -cp " + LuckyApp.getInstance().getPackageCodePath() + " com.android.internal.util.WithFramework" + " com.chelpus.root.utils";
            if (runtime.equals(BuildConfig.VERSION_NAME) || runtime == null) {
                String resultRuntime = "UNKNOWN";
                try {
                    resultRuntime = Utils.checkRuntimeFromCache(getPkgMng().getPackageInfo(getInstance().getPackageName(), SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir);
                } catch (NameNotFoundException e4) {
                    e4.printStackTrace();
                }
                System.out.println("Runtime:" + resultRuntime);
                runtime = resultRuntime;
            }
            if (su && !new File("/data/lp/lp_utils").exists()) {
                System.out.println("Tools not found in /data. Try create.");
                final File link_to_utils = new File("/data/lp/lp_utils");
                if (!link_to_utils.exists()) {
                    new Thread(new Runnable() {
                        public void run() {
                            Utils.run_all("mkdir /data/lp");
                            Utils.run_all("chmod 777 /data/lp");
                            Utils.save_text_to_file(link_to_utils, listAppsFragment.toolfilesdir + "%chelpus%" + listAppsFragment.api + "%chelpus%" + listAppsFragment.runtime);
                            Utils.run_all("chmod 777 /data/lp/lp_utils");
                            Utils.initXposedParam();
                        }
                    }).start();
                }
            }
            days = getConfig().getInt("days_on_up", TITLE_LIST_ITEM);
            getLaunchIntent();
        }
    }

    public static Intent getLaunchIntent() {
        if (launchActivity == null) {
            launchActivity = getPkgMng().getLaunchIntentForPackage(luckyPackage);
        }
        return launchActivity;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        System.out.println("LuckyPatcher: create fragment.");
        setRetainInstance(true);
        init();
        frag = this;
        handler = new Handler();
        if (!patchOnBoot) {
            try {
                ((NotificationManager) getContext().getSystemService("notification")).cancelAll();
            } catch (SecurityException e) {
                e.printStackTrace();
            }
        }
        if (isAdded()) {
            patchAct = (patchActivity) super.getActivity();
        }
        appcontext = super.getActivity();
        Thread start = new Thread(new C03392());
        start.setPriority(PERM_CONTEXT_DIALOG);
        start.start();
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setRetainInstance(true);
        patchActivity.frag = this;
        setHasOptionsMenu(true);
        frag = this;
        handler = new Handler();
    }

    public void onPause() {
        super.onPause();
        System.out.println("onPause");
        if (isAdded()) {
            patchAct = (patchActivity) getActivity();
        }
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu1 = menu;
        inflater.inflate(C0149R.menu.mainmenu, menu);
    }

    public void onPrepareOptionsMenu(Menu menu) {
        if (!su) {
            menu.removeItem(C0149R.id.truble);
        }
        if (!Utils.hasXposed()) {
            menu.removeItem(C0149R.id.xposed_settings);
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case C0149R.id.download_custom_patches:
                download_custom_patches();
                return true;
            case C0149R.id.settings:
                ((patchActivity) getActivity()).toolbar_settings_click();
                return true;
            case C0149R.id.xposed_settings:
                contextXposedPatch();
                return true;
            case C0149R.id.truble:
                contexttruble();
                removeDialogLP(CONTEXT_DIALOG);
                showDialogLP(CONTEXT_DIALOG);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onResume() {
        super.onResume();
        System.out.println("onResume()");
        try {
            if (isAdded()) {
                patchAct = (patchActivity) getActivity();
            }
        } catch (Exception e) {
            System.out.println("LuckyPatcher (onResume): sdcard not found!");
            e.printStackTrace();
        }
        try {
            if (getConfig().getBoolean("fast_start", false)) {
                fast_start = true;
            }
            if (getConfig().getBoolean("path_changed", false)) {
                refresh = true;
                getConfig().edit().putBoolean("path_changed", false).commit();
                getConfig().edit().putString("basepath", basepath).commit();
                if (new File(basepath).mkdirs() || new File(basepath).exists()) {
                    Toast.makeText(getActivity(), BuildConfig.VERSION_NAME + Utils.getText(C0149R.string.dirr_chg) + LogCollector.LINE_SEPARATOR + basepath, SETTINGS_VIEWSIZE_SMALL).show();
                }
            }
            if (getConfig().getBoolean("settings_change", false)) {
                refresh = true;
                getConfig().edit().putBoolean("settings_change", false).commit();
                sqlScanLite = true;
                sqlScan = false;
                Thread scan = new Thread(new AppScanning());
                scan.setPriority(PERM_CONTEXT_DIALOG);
                switch (getConfig().getInt(SETTINGS_VIEWSIZE, SETTINGS_VIEWSIZE_SMALL)) {
                    case SETTINGS_VIEWSIZE_SMALL /*0*/:
                        textSize = 16973894;
                        break;
                    case TITLE_LIST_ITEM /*1*/:
                        textSize = 16973892;
                        break;
                    default:
                        textSize = 16973890;
                        break;
                }
                scan.start();
                HorizontalScrollView sv = (HorizontalScrollView) getActivity().findViewById(C0149R.id.horizontalScrollToolbar);
                Button bt = (Button) sv.findViewById(C0149R.id.button8);
                if (bt != null) {
                    bt.setTextAppearance(getContext(), getSizeText());
                    bt.setTextColor(Color.parseColor("#999999"));
                }
                bt = (Button) sv.findViewById(C0149R.id.button7);
                if (bt != null) {
                    bt.setTextAppearance(getContext(), getSizeText());
                    bt.setTextColor(Color.parseColor("#999999"));
                }
                bt = (Button) sv.findViewById(C0149R.id.button1);
                if (bt != null) {
                    bt.setTextAppearance(getContext(), getSizeText());
                    bt.setTextColor(Color.parseColor("#999999"));
                }
                bt = (Button) sv.findViewById(C0149R.id.button2);
                if (bt != null) {
                    bt.setTextAppearance(getContext(), getSizeText());
                    bt.setTextColor(Color.parseColor("#999999"));
                }
                bt = (Button) sv.findViewById(C0149R.id.button3);
                if (bt != null) {
                    bt.setTextAppearance(getContext(), getSizeText());
                    bt.setTextColor(Color.parseColor("#999999"));
                }
                bt = (Button) sv.findViewById(C0149R.id.button4);
                if (bt != null) {
                    bt.setTextAppearance(getContext(), getSizeText());
                    bt.setTextColor(Color.parseColor("#999999"));
                }
                if (getConfig().getBoolean("lang_change", false)) {
                    getConfig().edit().putBoolean("lang_change", false).commit();
                    String lng = getConfig().getString("force_language", "default");
                    Thread scan2;
                    if (lng.equals("default")) {
                        Locale appLoc = Resources.getSystem().getConfiguration().locale;
                        Locale.setDefault(Locale.getDefault());
                        Configuration appConfig = new Configuration();
                        appConfig.locale = appLoc;
                        getRes().updateConfiguration(appConfig, getRes().getDisplayMetrics());
                        menu1.clear();
                        getActivity().getMenuInflater().inflate(C0149R.menu.mainmenu, menu1);
                        sqlScanLite = true;
                        sqlScan = false;
                        scan2 = new Thread(new AppScanning());
                        scan.setPriority(PERM_CONTEXT_DIALOG);
                        scan2.start();
                        plia.notifyDataSetChanged();
                    } else {
                        Locale locale = null;
                        String[] tails = lng.split("_");
                        if (tails.length == TITLE_LIST_ITEM) {
                            locale = new Locale(tails[SETTINGS_VIEWSIZE_SMALL]);
                        }
                        if (tails.length == SETTINGS_SORTBY_STATUS) {
                            locale = new Locale(tails[SETTINGS_VIEWSIZE_SMALL], tails[TITLE_LIST_ITEM], BuildConfig.VERSION_NAME);
                        }
                        if (tails.length == SETTINGS_SORTBY_TIME) {
                            locale = new Locale(tails[SETTINGS_VIEWSIZE_SMALL], tails[TITLE_LIST_ITEM], tails[SETTINGS_SORTBY_STATUS]);
                        }
                        Locale.setDefault(locale);
                        Configuration appConfig2 = new Configuration();
                        appConfig2.locale = locale;
                        getRes().updateConfiguration(appConfig2, getRes().getDisplayMetrics());
                        menu1.clear();
                        getActivity().getMenuInflater().inflate(C0149R.menu.mainmenu, menu1);
                        sqlScanLite = true;
                        sqlScan = false;
                        scan2 = new Thread(new AppScanning());
                        scan.setPriority(PERM_CONTEXT_DIALOG);
                        scan2.start();
                        plia.notifyDataSetChanged();
                    }
                }
                if (getConfig().getInt(SETTINGS_ORIENT, SETTINGS_SORTBY_TIME) == TITLE_LIST_ITEM) {
                    getActivity().setRequestedOrientation(SETTINGS_VIEWSIZE_SMALL);
                }
                if (getConfig().getInt(SETTINGS_ORIENT, SETTINGS_SORTBY_TIME) == SETTINGS_SORTBY_STATUS) {
                    getActivity().setRequestedOrientation(TITLE_LIST_ITEM);
                }
                if (getConfig().getInt(SETTINGS_ORIENT, SETTINGS_SORTBY_TIME) == SETTINGS_SORTBY_TIME) {
                    getActivity().setRequestedOrientation(Custom_PATCH_DIALOG);
                }
                if (getConfig().getInt(SETTINGS_SORTBY, SETTINGS_SORTBY_STATUS) == TITLE_LIST_ITEM) {
                    plia.sorter = new byPkgName();
                    plia.sort();
                }
                if (getConfig().getInt(SETTINGS_SORTBY, SETTINGS_SORTBY_STATUS) == SETTINGS_SORTBY_STATUS) {
                    plia.sorter = new byPkgStatus();
                    plia.sort();
                }
                if (getConfig().getInt(SETTINGS_SORTBY, SETTINGS_SORTBY_STATUS) == SETTINGS_SORTBY_TIME) {
                    plia.sorter = new byPkgTime();
                    plia.sort();
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void onDestroyView() {
        super.onDestroyView();
    }

    public void onDestroy() {
        desktop_launch = false;
        if (su) {
            exitSu();
        }
        System.out.println("LuckyPatcher: EXIT!");
        System.runFinalizersOnExit(true);
        super.onDestroy();
        if (!patchOnBoot) {
            System.exit(SETTINGS_VIEWSIZE_SMALL);
        }
        getContext().finish();
    }

    public static Context getInstance() {
        try {
            instance = LuckyApp.getInstance();
            if (instance == null) {
                System.out.println("LuckyApp Instance == null");
                if (patchAct != null) {
                    instance = patchAct.getApplicationContext();
                    if (instance == null) {
                        instance = patchAct;
                    }
                    if (instance == null) {
                        System.out.println("Return Instance from acivity == null");
                    }
                } else if (instance == null) {
                    System.out.println("Return Instance == null");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return instance;
    }

    public static SharedPreferences getConfig() {
        if (config == null) {
            config = getInstance().getSharedPreferences("config", Custom_PATCH_DIALOG);
        }
        return config;
    }

    public static Process getSu() throws IOException {
        if (suProcess == null) {
            suProcess = Runtime.getRuntime().exec("su");
            suOutputStream = new DataOutputStream(suProcess.getOutputStream());
            suInputStream = new DataInputStream(suProcess.getInputStream());
            suErrorInputStream = new DataInputStream(suProcess.getErrorStream());
        } else {
            try {
                suOutputStream.writeBytes("echo chelpusstart!\n");
            } catch (Exception e) {
                exitSu();
                suProcess = Runtime.getRuntime().exec("su");
                suOutputStream = new DataOutputStream(suProcess.getOutputStream());
                suInputStream = new DataInputStream(suProcess.getInputStream());
                suErrorInputStream = new DataInputStream(suProcess.getErrorStream());
            }
        }
        return suProcess;
    }

    public static void exitSu() {
        Utils.exitRoot();
    }

    public static int getSizeText() {
        if (textSize != 0) {
            return textSize;
        }
        switch (getInstance().getSharedPreferences("config", Custom_PATCH_DIALOG).getInt(SETTINGS_VIEWSIZE, SETTINGS_VIEWSIZE_SMALL)) {
            case SETTINGS_VIEWSIZE_SMALL /*0*/:
                textSize = 16973894;
                break;
            case TITLE_LIST_ITEM /*1*/:
                textSize = 16973892;
                break;
            default:
                textSize = 16973890;
                break;
        }
        return textSize;
    }

    private Comparator<PkgListItem> getSortPref() {
        switch (getConfig().getInt(SETTINGS_SORTBY, SETTINGS_SORTBY_STATUS)) {
            case TITLE_LIST_ITEM /*1*/:
                return new byPkgName();
            case SETTINGS_SORTBY_TIME /*3*/:
                return new byPkgTime();
            default:
                return new byPkgStatus();
        }
    }

    private boolean copyData(String path, boolean rewriteAll) {
        if (!new File(path).exists()) {
            new File(path).mkdirs();
        }
        String[] files2 = null;
        try {
            String[] files = getRes().getAssets().list(BuildConfig.VERSION_NAME);
            if (!rewriteAll) {
                files2 = new File(path).list();
            }
            int length = files.length;
            for (int i = SETTINGS_VIEWSIZE_SMALL; i < length; i += TITLE_LIST_ITEM) {
                String file = files[i];
                if (!(file.contains("dexopt-wrapper") || file.equals("zip") || file.equals("zipalign") || file.equals("testkey.pk8") || file.equals("testkey.x509.pem") || file.equals("testkey.sbt") || file.equals("busybox") || file.equals("fonts") || file.equals("xposed_init") || file.equals("fonts") || file.equals("images") || file.equals("sounds"))) {
                    boolean contain = false;
                    int j;
                    if (!rewriteAll) {
                        j = SETTINGS_VIEWSIZE_SMALL;
                        while (j < files2.length) {
                            try {
                                if (file.equals(new File(files2[j]).getName())) {
                                    contain = true;
                                }
                                j += TITLE_LIST_ITEM;
                            } catch (NullPointerException e) {
                                contain = false;
                            }
                        }
                    } else if (file.equals("AdsBlockList_user_edit.txt")) {
                        files2 = new File(path).list();
                        for (j = SETTINGS_VIEWSIZE_SMALL; j < files2.length; j += TITLE_LIST_ITEM) {
                            if (file.equals(new File(files2[j]).getName())) {
                                contain = true;
                            }
                        }
                        files2 = null;
                    } else {
                        contain = false;
                    }
                    if (contain) {
                        continue;
                    } else {
                        try {
                            Utils.getAssets(file, path);
                        } catch (FileNotFoundException e2) {
                        } catch (Exception e3) {
                            e3.printStackTrace();
                            return false;
                        }
                    }
                }
            }
            return true;
        } catch (Exception e32) {
            e32.printStackTrace();
            return false;
        }
    }

    public boolean testSD(boolean showDialog) {
        try {
            if (basepath.startsWith(getContext().getDir("sdcard", SETTINGS_VIEWSIZE_SMALL).getAbsolutePath())) {
                System.out.println("LuckyPatcher (sdcard to internal memory): " + basepath);
                if (!showDialog) {
                    return false;
                }
                showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.messageSD));
                return false;
            }
            if (!new File(basepath).exists()) {
                new File(basepath).mkdirs();
            }
            if (new File(basepath).exists()) {
                new File(basepath + "/tmp.txt").delete();
                System.out.println("LuckyPatcher (sdcard test create file): " + basepath);
                if (new File(basepath + "/tmp.txt").createNewFile()) {
                    System.out.println("LuckyPatcher (sdcard test create file true): " + basepath);
                    new File(basepath + "/tmp.txt").delete();
                    return true;
                } else if (!showDialog) {
                    return false;
                } else {
                    showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.messageSD));
                    return false;
                }
            }
            System.out.println("LuckyPatcher (sdcard directory not found and not created): " + basepath);
            return false;
        } catch (IOException e) {
            if (!showDialog) {
                return false;
            }
            showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.messageSD));
            return false;
        }
    }

    public static void toast(final String message) {
        if (handlerToast != null) {
            handlerToast.post(new Runnable() {
                public void run() {
                    Toast.makeText(listAppsFragment.getInstance(), message, listAppsFragment.TITLE_LIST_ITEM).show();
                }
            });
        }
    }

    public void showMessage(final String title, final String message) {
        runToMain(new Runnable() {
            public void run() {
                AlertDlg dialog = new AlertDlg(listAppsFragment.this.getContext());
                dialog.setTitle(title);
                dialog.setMessage(message);
                dialog.setPositiveButton(17039370, null);
                try {
                    Utils.showDialog(dialog.create());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void updateapp() throws NameNotFoundException {
        Thread t = new Thread(new C03845());
        t.setPriority(TITLE_LIST_ITEM);
        t.start();
    }

    public void odex_all_system_app() {
        OnClickListener dialogClickListener = new C04016();
        Utils.showDialogYesNo(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.odex_all_system_app_message), dialogClickListener, dialogClickListener, null);
    }

    public void remove_all_saved_purchases() {
        OnClickListener dialogClickListener = new C04147();
        Utils.showDialogYesNo(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.remove_all_saved_purchases_message), dialogClickListener, dialogClickListener, null);
    }

    public void removefixes() {
        OnClickListener dialogClickListener = new C04268();
        Utils.showDialogYesNo(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.message_remove_all_odex), dialogClickListener, dialogClickListener, null);
    }

    public void cleardalvik() {
        OnClickListener dialogClickListener = new C04679();
        Utils.showDialogYesNo(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.message_clear_all_dalvik_cache), dialogClickListener, dialogClickListener, null);
    }

    public void dialog_reboot() {
        OnClickListener dialogClickListener = new OnClickListener() {

            class C02351 implements Runnable {
                C02351() {
                }

                public void run() {
                    Utils.reboot();
                }
            }

            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case Zip4jConstants.ENC_NO_ENCRYPTION /*-1*/:
                        try {
                            listAppsFragment.this.runWithWait(new C02351());
                            return;
                        } catch (Exception e) {
                            Toast.makeText(listAppsFragment.this.getContext(), BuildConfig.VERSION_NAME + e, listAppsFragment.TITLE_LIST_ITEM).show();
                            return;
                        }
                    default:
                        return;
                }
            }
        };
        Utils.showDialogYesNo(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.message_reboot), dialogClickListener, dialogClickListener, null);
    }

    public void setDefInstallAuto() {
        getConfig().edit().putInt("Install_location", SETTINGS_VIEWSIZE_SMALL).commit();
        if (su) {
            Utils utils = new Utils(BuildConfig.VERSION_NAME);
            String[] strArr = new String[SETTINGS_SORTBY_STATUS];
            strArr[SETTINGS_VIEWSIZE_SMALL] = "pm setInstallLocation 0";
            strArr[TITLE_LIST_ITEM] = "skipOut";
            utils.cmdRoot(strArr);
            utils = new Utils(BuildConfig.VERSION_NAME);
            strArr = new String[SETTINGS_SORTBY_STATUS];
            strArr[SETTINGS_VIEWSIZE_SMALL] = "pm set-install-location 0";
            strArr[TITLE_LIST_ITEM] = "skipOut";
            utils.cmdRoot(strArr);
            return;
        }
        String[] strArr2 = new String[SETTINGS_SORTBY_STATUS];
        strArr2[SETTINGS_VIEWSIZE_SMALL] = "pm setInstallLocation 0";
        strArr2[TITLE_LIST_ITEM] = "skipOut";
        Utils.cmd(strArr2);
        strArr2 = new String[SETTINGS_SORTBY_STATUS];
        strArr2[SETTINGS_VIEWSIZE_SMALL] = "pm set-install-location 0";
        strArr2[TITLE_LIST_ITEM] = "skipOut";
        Utils.cmd(strArr2);
    }

    public void setDefInstallSDcard() {
        getConfig().edit().putInt("Install_location", SETTINGS_SORTBY_STATUS).commit();
        if (su) {
            Utils utils = new Utils(BuildConfig.VERSION_NAME);
            String[] strArr = new String[SETTINGS_SORTBY_STATUS];
            strArr[SETTINGS_VIEWSIZE_SMALL] = "pm setInstallLocation 2";
            strArr[TITLE_LIST_ITEM] = "skipOut";
            utils.cmdRoot(strArr);
            utils = new Utils(BuildConfig.VERSION_NAME);
            strArr = new String[SETTINGS_SORTBY_STATUS];
            strArr[SETTINGS_VIEWSIZE_SMALL] = "pm set-install-location 2";
            strArr[TITLE_LIST_ITEM] = "skipOut";
            utils.cmdRoot(strArr);
            return;
        }
        String[] strArr2 = new String[SETTINGS_SORTBY_STATUS];
        strArr2[SETTINGS_VIEWSIZE_SMALL] = "pm setInstallLocation 2";
        strArr2[TITLE_LIST_ITEM] = "skipOut";
        Utils.cmd(strArr2);
        strArr2 = new String[SETTINGS_SORTBY_STATUS];
        strArr2[SETTINGS_VIEWSIZE_SMALL] = "pm set-install-location 2";
        strArr2[TITLE_LIST_ITEM] = "skipOut";
        Utils.cmd(strArr2);
    }

    public void setDefInstallInternalMemory() {
        getConfig().edit().putInt("Install_location", TITLE_LIST_ITEM).commit();
        if (su) {
            Utils utils = new Utils(BuildConfig.VERSION_NAME);
            String[] strArr = new String[SETTINGS_SORTBY_STATUS];
            strArr[SETTINGS_VIEWSIZE_SMALL] = "pm setInstallLocation 1";
            strArr[TITLE_LIST_ITEM] = "skipOut";
            utils.cmdRoot(strArr);
            utils = new Utils(BuildConfig.VERSION_NAME);
            strArr = new String[SETTINGS_SORTBY_STATUS];
            strArr[SETTINGS_VIEWSIZE_SMALL] = "pm set-install-location 1";
            strArr[TITLE_LIST_ITEM] = "skipOut";
            utils.cmdRoot(strArr);
            return;
        }
        String[] strArr2 = new String[SETTINGS_SORTBY_STATUS];
        strArr2[SETTINGS_VIEWSIZE_SMALL] = "pm setInstallLocation 1";
        strArr2[TITLE_LIST_ITEM] = "skipOut";
        Utils.cmd(strArr2);
        strArr2 = new String[SETTINGS_SORTBY_STATUS];
        strArr2[SETTINGS_VIEWSIZE_SMALL] = "pm set-install-location 1";
        strArr2[TITLE_LIST_ITEM] = "skipOut";
        Utils.cmd(strArr2);
    }

    public void setDefInstall() {
        ArrayList<Integer> s = new ArrayList();
        s.add(Integer.valueOf(C0149R.string.set_default_to_install_auto));
        s.add(Integer.valueOf(C0149R.string.set_default_to_install_internal_memory));
        s.add(Integer.valueOf(C0149R.string.set_default_to_install_sdcard));
        adapt = new ArrayAdapter<Integer>(getContext(), C0149R.layout.contextmenu, s) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                TextView textView = (TextView) view.findViewById(C0149R.id.conttext);
                textView.setTextAppearance(getContext(), listAppsFragment.getSizeText());
                textView.setCompoundDrawablePadding((int) ((5.0f * listAppsFragment.getRes().getDisplayMetrics().density) + 0.5f));
                textView.setTextColor(-1);
                textView.setText(Utils.getText(((Integer) getItem(position)).intValue()));
                textView.setTypeface(null, listAppsFragment.TITLE_LIST_ITEM);
                return view;
            }
        };
        removeDialogLP(CONTEXT_DIALOG);
        showDialogLP(CONTEXT_DIALOG);
    }

    public void contexttoolbarcreateframework() {
        System.out.println("LuckyPatcher: Core patch start!");
        Thread t = new Thread(new Runnable() {

            class C02701 implements Runnable {
                C02701() {
                }

                public void run() {
                    listAppsFragment.showDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                    listAppsFragment.progress2.setCancelable(false);
                    listAppsFragment.progress2.setMessage(Utils.getText(C0149R.string.wait));
                }
            }

            class C02712 implements Runnable {
                C02712() {
                }

                public void run() {
                    listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                    listAppsFragment.frag.getDir(Utils.getDirs(new File(listAppsFragment.rebuldApk)).getAbsolutePath(), listAppsFragment.frag.filebrowser, false);
                    if (new File(listAppsFragment.rebuldApk.replace("/core.odex", "/core-patched.odex").replace("/core.jar", "/core-patched.jar").replace("/services.odex", "/services-patched.odex").replace("/services.jar", "/services-patched.jar").replace("/core-libart.jar", "/core-libart-patched.jar").replace("/boot.oat", "/boot-patched.oat")).exists() && !new File(listAppsFragment.rebuldApk).exists()) {
                        listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), new File(listAppsFragment.rebuldApk).getName() + " " + Utils.getText(C0149R.string.framework_file_patched) + " " + new File(listAppsFragment.rebuldApk.replace("/core.odex", "/core-patched.odex").replace("/core.jar", "/core-patched.jar").replace("/services.odex", "/services-patched.odex").replace("/services.jar", "/services-patched.jar").replace("/core-libart.jar", "/core-libart-patched.jar").replace("/boot.oat", "/boot-patched.oat")).getName());
                    } else if (!corepatch.not_found_bytes_for_patch) {
                        listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.unknown_error));
                    }
                }
            }

            public void run() {
                listAppsFragment.this.runToMain(new C02701());
                try {
                    listAppsFragment.str = BuildConfig.VERSION_NAME;
                    String[] param = new String[listAppsFragment.EXT_PATCH_DIALOG];
                    param[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = "all";
                    param[listAppsFragment.TITLE_LIST_ITEM] = listAppsFragment.rebuldApk;
                    param[listAppsFragment.SETTINGS_SORTBY_STATUS] = listAppsFragment.rebuldApk;
                    param[listAppsFragment.SETTINGS_SORTBY_TIME] = listAppsFragment.getInstance().getFilesDir().getAbsolutePath();
                    param[listAppsFragment.Custom_PATCH_DIALOG] = "framework";
                    corepatch.main(param);
                    if (corepatch.not_found_bytes_for_patch) {
                        listAppsFragment.this.showMessage(Utils.getText(C0149R.string.information), Utils.getText(C0149R.string.framework_patch_not_result));
                    }
                    System.out.println(listAppsFragment.str);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                listAppsFragment.this.runToMain(new C02712());
            }
        });
        t.setPriority(PERM_CONTEXT_DIALOG);
        t.start();
    }

    public void corepatch(final String result) {
        System.out.println("LuckyPatcher: Core patch start!");
        Thread t = new Thread(new Runnable() {

            class C02831 implements Runnable {
                C02831() {
                }

                public void run() {
                    listAppsFragment.showDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                    listAppsFragment.progress2.setCancelable(false);
                    listAppsFragment.progress2.setMessage(Utils.getText(C0149R.string.wait));
                }
            }

            class C02842 implements Runnable {
                C02842() {
                }

                public void run() {
                    listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.core_error_dalvik_cache_not_found_for_core.jar));
                }
            }

            class C02853 implements Runnable {
                C02853() {
                }

                public void run() {
                    listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.core_error_dalvik_cache_not_found_for_services.jar));
                }
            }

            class C02874 implements Runnable {

                class C02861 implements Runnable {
                    C02861() {
                    }

                    public void run() {
                        listAppsFragment.removeDialogLP(listAppsFragment.DALVIK_CORE_PATCH);
                        listAppsFragment.showDialogLP(listAppsFragment.DALVIK_CORE_PATCH);
                    }
                }

                C02874() {
                }

                public void run() {
                    listAppsFragment.result_core_patch = result;
                    listAppsFragment.this.runToMain(new C02861());
                }
            }

            class C02885 implements Runnable {
                C02885() {
                }

                public void run() {
                    listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.unknown_error));
                }
            }

            class C02916 implements Runnable {

                class C02891 implements OnClickListener {
                    C02891() {
                    }

                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            listAppsFragment.str = BuildConfig.VERSION_NAME;
                            System.out.println("LuckyPatcher: java root finished");
                            String cmd = listAppsFragment.dalvikruncommand + ".corecopy ";
                            if (Utils.exists("/system/framework/core.patched") || Utils.exists("/data/data/core.odex")) {
                                cmd = cmd + listAppsFragment.this.dalvikcache + " ";
                            } else {
                                cmd = cmd + "empty ";
                            }
                            cmd = cmd + "odex ";
                            if (Utils.exists("/system/framework/services.patched")) {
                                cmd = cmd + listAppsFragment.this.dalvikcache2;
                            } else {
                                cmd = cmd + "empty";
                            }
                            cmd = cmd + " " + listAppsFragment.toolfilesdir;
                            System.out.println(cmd);
                            Utils utils = new Utils(BuildConfig.VERSION_NAME);
                            String[] strArr = new String[listAppsFragment.TITLE_LIST_ITEM];
                            strArr[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = cmd;
                            System.out.println(utils.cmdRoot(strArr));
                            Utils.reboot();
                        } catch (Exception e) {
                        }
                    }
                }

                class C02902 implements OnClickListener {
                    C02902() {
                    }

                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            listAppsFragment.str = BuildConfig.VERSION_NAME;
                            System.out.println("LuckyPatcher: java root finished");
                            Utils utils = new Utils(BuildConfig.VERSION_NAME);
                            String[] strArr = new String[listAppsFragment.TITLE_LIST_ITEM];
                            strArr[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = listAppsFragment.dalvikruncommand + ".corecopy " + listAppsFragment.this.dalvikcache + " " + "delete";
                            System.out.println(utils.cmdRoot(strArr));
                            Utils.reboot();
                        } catch (Exception e) {
                        }
                    }
                }

                C02916() {
                }

                public void run() {
                    Utils.showDialogYesNo(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.messageODEX1), new C02891(), new C02902(), null);
                }
            }

            class C02927 implements Runnable {
                C02927() {
                }

                public void run() {
                    listAppsFragment.this.showMessageInfo(listAppsFragment.this.getContext(), Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.no_space_in_system_all));
                }
            }

            class C02938 implements Runnable {
                C02938() {
                }

                public void run() {
                    listAppsFragment.this.showMessageInfo(listAppsFragment.this.getContext(), Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.no_space_in_system_all));
                }
            }

            class C02949 implements Runnable {
                C02949() {
                }

                public void run() {
                    listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                    listAppsFragment.this.vibrateEnd();
                }
            }

            public void run() {
                listAppsFragment.this.runToMain(new C02831());
                try {
                    Utils utils;
                    String[] strArr;
                    if (listAppsFragment.patchOnlyDalvikCore) {
                        System.out.println("patch only dalvik cache mode");
                        listAppsFragment.this.dalvikcache = BuildConfig.VERSION_NAME;
                        listAppsFragment.this.dalvikcache2 = BuildConfig.VERSION_NAME;
                        listAppsFragment.result_core_patch = BuildConfig.VERSION_NAME;
                        listAppsFragment.this.dalvikcache = Utils.getFileDalvikCache("/system/framework/core.jar").getAbsolutePath();
                        if (listAppsFragment.this.dalvikcache == null) {
                            listAppsFragment.this.runToMain(new C02842());
                        }
                        listAppsFragment.this.dalvikcache2 = Utils.getFileDalvikCache("/system/framework/services.jar").getAbsolutePath();
                        if (listAppsFragment.this.dalvikcache2 == null) {
                            listAppsFragment.this.runToMain(new C02853());
                        }
                        if (listAppsFragment.this.dalvikcache == null || listAppsFragment.this.dalvikcache2 == null || listAppsFragment.this.dalvikcache.equals(BuildConfig.VERSION_NAME) || listAppsFragment.this.dalvikcache2.equals(BuildConfig.VERSION_NAME)) {
                            listAppsFragment.this.runToMain(new C02885());
                        } else {
                            utils = new Utils(BuildConfig.VERSION_NAME);
                            strArr = new String[listAppsFragment.TITLE_LIST_ITEM];
                            strArr[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = listAppsFragment.dalvikruncommand + ".corepatch " + result + " " + listAppsFragment.this.dalvikcache + " " + listAppsFragment.this.dalvikcache2 + " " + listAppsFragment.getInstance().getFilesDir() + " OnlyDalvik";
                            listAppsFragment.str = utils.cmdRoot(strArr);
                            System.out.println(listAppsFragment.str);
                            listAppsFragment.this.runToMain(new C02874());
                        }
                        if (listAppsFragment.install_market_to_system) {
                            Utils.reboot();
                        }
                        listAppsFragment.this.runToMain(new C02949());
                    }
                    Utils.turn_off_patch_on_boot_all();
                    if (!Utils.isOdex("/system/framework/core.jar") || new File(Utils.getPlaceForOdex("/system/framework/core.jar", false)).length() == 0 || !Utils.isOdex("/system/framework/services.jar") || new File(Utils.getPlaceForOdex("system/framework/services.jar", false)).length() == 0) {
                        String patched_core;
                        System.out.println("start odex framework");
                        Utils.remount("/system", InternalZipConstants.WRITE_MODE);
                        Utils.run_all("chattr -ai " + Utils.getPlaceForOdex("/system/framework/core.jar", false));
                        Utils.run_all("chattr -ai " + Utils.getPlaceForOdex("/system/framework/services.jar", false));
                        Utils.run_all("chattr -ai /system/framework/core.jar");
                        Utils.run_all("chattr -ai /system/framework/services.jar");
                        Utils.run_all("chattr -ai /system/framework/core-libart.jar");
                        String[] strings2 = new String[listAppsFragment.CHILD_NAME_SHEME_SELECT_ITEM];
                        strings2[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = "/data/dalvik-cache/";
                        strings2[listAppsFragment.TITLE_LIST_ITEM] = "/data/dalvik-cache/arm/";
                        strings2[listAppsFragment.SETTINGS_SORTBY_STATUS] = "/sd-ext/data/dalvik-cache/";
                        strings2[listAppsFragment.SETTINGS_SORTBY_TIME] = "/cache/dalvik-cache/";
                        strings2[listAppsFragment.Custom_PATCH_DIALOG] = "/sd-ext/data/cache/dalvik-cache/";
                        strings2[listAppsFragment.EXT_PATCH_DIALOG] = "/data/cache/dalvik-cache/";
                        int length = strings2.length;
                        for (int i = listAppsFragment.SETTINGS_VIEWSIZE_SMALL; i < length; i += listAppsFragment.TITLE_LIST_ITEM) {
                            String tail = strings2[i];
                            System.out.println("LuckyPatcher: search dalvik-cache! " + tail + "system@framework@core.jar@classes.dex");
                            if (Utils.exists(tail + "system@framework@core.jar@classes.dex") || Utils.exists(tail + "system@framework@services.jar@classes.dex") || Utils.getCurrentRuntimeValue().equals("ART")) {
                                if (Utils.getCurrentRuntimeValue().equals("ART")) {
                                    System.out.println("LuckyPatcher: art patch");
                                } else {
                                    System.out.println("LuckyPatcher: found dalvik-cache! " + tail + "system@framework@core.jar@classes.dex");
                                }
                                listAppsFragment.this.dalvikcache = tail + "system@framework@core.jar@classes.dex";
                                listAppsFragment.this.dalvikcache2 = tail + "system@framework@services.jar@classes.dex";
                                listAppsFragment.str = BuildConfig.VERSION_NAME;
                                if (Utils.isOdex("/system/framework/core.jar") || new File(Utils.getPlaceForOdex("/system/framework/core.jar", false)).length() != 0) {
                                    listAppsFragment.this.dalvikcache = Utils.getPlaceForOdex("/system/framework/core.jar", false);
                                }
                                if (Utils.isOdex("/system/framework/services.jar") || new File(Utils.getPlaceForOdex("/system/framework/services.jar", false)).length() != 0) {
                                    listAppsFragment.this.dalvikcache2 = Utils.getPlaceForOdex("/system/framework/services.jar", false);
                                }
                                Utils.remount("/system", InternalZipConstants.WRITE_MODE);
                                utils = new Utils(BuildConfig.VERSION_NAME);
                                strArr = new String[listAppsFragment.TITLE_LIST_ITEM];
                                strArr[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = listAppsFragment.dalvikruncommand + ".corepatch " + result + " " + listAppsFragment.this.dalvikcache + " " + listAppsFragment.this.dalvikcache2 + " " + listAppsFragment.getInstance().getFilesDir() + " " + Utils.getCurrentRuntimeValue();
                                listAppsFragment.str = utils.cmdRoot(strArr);
                                System.out.println(listAppsFragment.str);
                                if (!Utils.exists("/system/framework/core.patched") || Utils.exists("/data/data/core.odex")) {
                                    System.out.println("Result code for found core.patched");
                                    patched_core = BuildConfig.VERSION_NAME;
                                    if (Utils.exists("/system/framework/core.patched")) {
                                        patched_core = "/system/framework/core.patched";
                                    }
                                    if (Utils.exists("/data/data/core.odex")) {
                                        patched_core = "/data/data/core.odex";
                                    }
                                    Utils.run_all("chmod 0644 " + patched_core);
                                    Utils.run_all("chown 0.0 " + patched_core);
                                    Utils.run_all("chown 0:0 " + patched_core);
                                    if (Utils.exists("/system/framework/services.jar")) {
                                        Utils.run_all("chmod 0644 /system/framework/services.jar");
                                        Utils.run_all("chown 0.0 /system/framework/services.jar");
                                        Utils.run_all("chown 0:0 /system/framework/services.jar");
                                    }
                                    if (result.contains("restore")) {
                                        listAppsFragment.this.runToMain(new C02916());
                                    } else {
                                        Utils.reboot();
                                    }
                                    listAppsFragment.this.runToMain(new C02949());
                                }
                                if (listAppsFragment.str.contains("LuckyPatcher: odex not equal lenght packed! Not enougth space in /system/!")) {
                                    listAppsFragment.this.runToMain(new C02927());
                                }
                                if (listAppsFragment.str.contains("LuckyPatcher: odex not equal lenght patched! Not enougth space in /system/!")) {
                                    listAppsFragment.this.runToMain(new C02938());
                                } else {
                                    Utils.reboot();
                                }
                                listAppsFragment.this.runToMain(new C02949());
                            }
                        }
                        if (Utils.exists("/system/framework/core.patched")) {
                        }
                        System.out.println("Result code for found core.patched");
                        patched_core = BuildConfig.VERSION_NAME;
                        if (Utils.exists("/system/framework/core.patched")) {
                            patched_core = "/system/framework/core.patched";
                        }
                        if (Utils.exists("/data/data/core.odex")) {
                            patched_core = "/data/data/core.odex";
                        }
                        Utils.run_all("chmod 0644 " + patched_core);
                        Utils.run_all("chown 0.0 " + patched_core);
                        Utils.run_all("chown 0:0 " + patched_core);
                        if (Utils.exists("/system/framework/services.jar")) {
                            Utils.run_all("chmod 0644 /system/framework/services.jar");
                            Utils.run_all("chown 0.0 /system/framework/services.jar");
                            Utils.run_all("chown 0:0 /system/framework/services.jar");
                        }
                        if (result.contains("restore")) {
                            Utils.reboot();
                        } else {
                            listAppsFragment.this.runToMain(new C02916());
                        }
                        listAppsFragment.this.runToMain(new C02949());
                    }
                    listAppsFragment.str = BuildConfig.VERSION_NAME;
                    Utils.remount("/system", InternalZipConstants.WRITE_MODE);
                    Utils.run_all("chattr -ai " + Utils.getPlaceForOdex("/system/framework/core.jar", false));
                    Utils.run_all("chattr -ai " + Utils.getPlaceForOdex("/system/framework/services.jar", false));
                    Utils.run_all("chattr -ai /system/framework/core.jar");
                    Utils.run_all("chattr -ai /system/framework/services.jar");
                    Utils.run_all("chattr -ai /system/framework/core-libart.jar");
                    utils = new Utils(BuildConfig.VERSION_NAME);
                    strArr = new String[listAppsFragment.TITLE_LIST_ITEM];
                    strArr[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = listAppsFragment.dalvikruncommand + ".corepatch " + result + " " + Utils.getPlaceForOdex("/system/framework/core.jar", false) + " " + Utils.getPlaceForOdex("/system/framework/services.jar", false) + " " + listAppsFragment.getInstance().getFilesDir() + " " + Utils.getCurrentRuntimeValue();
                    listAppsFragment.str = utils.cmdRoot(strArr);
                    System.out.println(listAppsFragment.str);
                    Utils.reboot();
                    listAppsFragment.this.runToMain(new C02949());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        t.setPriority(PERM_CONTEXT_DIALOG);
        if (Utils.isInstalledOnSdCard(getInstance().getPackageName())) {
            showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.error_app_installed_on_sdcard_block));
        } else {
            t.start();
        }
    }

    public void odex(PkgListItem pli) {
        try {
            String system_app = "not_system";
            if (pli.system) {
                system_app = "system";
            }
            str = BuildConfig.VERSION_NAME;
            Utils utils = new Utils(BuildConfig.VERSION_NAME);
            String[] strArr = new String[TITLE_LIST_ITEM];
            strArr[SETTINGS_VIEWSIZE_SMALL] = dalvikruncommand + ".odex " + pli.pkgName + " " + getPkgMng().getPackageInfo(pli.pkgName, SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir + " " + system_app + " " + getInstance().getFilesDir() + " " + getPkgMng().getPackageInfo(pli.pkgName, SETTINGS_VIEWSIZE_SMALL).applicationInfo.uid;
            str = utils.cmdRoot(strArr);
            String[] mass = new String[str.split(LogCollector.LINE_SEPARATOR).length];
            mass = str.split(LogCollector.LINE_SEPARATOR);
            int length = mass.length;
            for (int i = SETTINGS_VIEWSIZE_SMALL; i < length; i += TITLE_LIST_ITEM) {
                String mas = mass[i];
                if (mas.contains("Changes Fix to: ")) {
                    String mass2 = mas.replace("Changes Fix to: ", BuildConfig.VERSION_NAME);
                    plia.getItem(pli.pkgName).odex = true;
                    plia.notifyDataSetChanged(plia.getItem(pli.pkgName));
                    Toast.makeText(getContext(), "Dalvik-cache fixed!", TITLE_LIST_ITEM).show();
                }
            }
            plia.updateItem(pli.pkgName);
            plia.notifyDataSetChanged();
        } catch (Exception e) {
            Toast.makeText(getContext(), BuildConfig.VERSION_NAME + e, TITLE_LIST_ITEM).show();
        }
    }

    public void deodex(final PkgListItem pli) {
        OnClickListener dialogClickListener = new OnClickListener() {

            class C03001 implements Runnable {

                class C02971 implements Runnable {
                    C02971() {
                    }

                    public void run() {
                        Toast.makeText(listAppsFragment.this.getContext(), Utils.getText(C0149R.string.remodex), listAppsFragment.TITLE_LIST_ITEM).show();
                        listAppsFragment.plia.updateItem(pli.pkgName);
                        listAppsFragment.plia.notifyDataSetChanged(listAppsFragment.plia.getItem(pli.pkgName));
                    }
                }

                class C02982 implements Runnable {
                    C02982() {
                    }

                    public void run() {
                        Toast.makeText(listAppsFragment.this.getContext(), Utils.getText(C0149R.string.remodexerror), listAppsFragment.TITLE_LIST_ITEM).show();
                    }
                }

                C03001() {
                }

                public void run() {
                    try {
                        listAppsFragment.str = BuildConfig.VERSION_NAME;
                        String apk_file = listAppsFragment.getPkgMng().getPackageInfo(pli.pkgName, listAppsFragment.SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir;
                        Utils utils = new Utils(BuildConfig.VERSION_NAME);
                        String[] strArr = new String[listAppsFragment.TITLE_LIST_ITEM];
                        strArr[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = listAppsFragment.dalvikruncommand + ".deodex " + pli.pkgName + " " + apk_file;
                        listAppsFragment.str = utils.cmdRoot(strArr);
                        if (Utils.getCurrentRuntimeValue().equals("ART")) {
                            File dalv = Utils.getFileDalvikCache(apk_file);
                            if (dalv != null) {
                                Utils.run_all("rm " + dalv.getAbsolutePath());
                            }
                            dalv = Utils.getFileDalvikCacheName(apk_file);
                            String instructions = BuildConfig.VERSION_NAME;
                            if (dalv.getAbsolutePath().contains("/arm/")) {
                                instructions = "arm";
                            }
                            if (dalv.getAbsolutePath().contains("/arm64/")) {
                                instructions = "arm64";
                            }
                            if (dalv.getAbsolutePath().contains("/x86/")) {
                                instructions = "x86";
                            }
                            PrintStream printStream = System.out;
                            Utils utils2 = new Utils(BuildConfig.VERSION_NAME);
                            String[] strArr2 = new String[listAppsFragment.SETTINGS_SORTBY_STATUS];
                            strArr2[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = "dex2oat --dex-file=" + apk_file + " --oat-file=" + dalv.getAbsolutePath();
                            strArr2[listAppsFragment.TITLE_LIST_ITEM] = "--instruction-set=" + instructions;
                            printStream.println(utils2.cmdRoot(strArr2));
                            Utils.run_all("chmod 644 " + dalv.getAbsolutePath());
                            Utils.run_all("chown 1000:" + listAppsFragment.getPkgMng().getPackageInfo(pli.pkgName, listAppsFragment.SETTINGS_VIEWSIZE_SMALL).applicationInfo.uid + " " + dalv.getAbsolutePath());
                            Utils.run_all("chown 1000." + listAppsFragment.getPkgMng().getPackageInfo(pli.pkgName, listAppsFragment.SETTINGS_VIEWSIZE_SMALL).applicationInfo.uid + " " + dalv.getAbsolutePath());
                        }
                        if (Utils.exists(Utils.getPlaceForOdex(listAppsFragment.getPkgMng().getPackageInfo(pli.pkgName, listAppsFragment.SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir, false))) {
                            listAppsFragment.this.runToMain(new C02982());
                        } else {
                            listAppsFragment.this.runToMain(new C02971());
                        }
                    } catch (final Exception e) {
                        listAppsFragment.this.runToMain(new Runnable() {
                            public void run() {
                                Toast.makeText(listAppsFragment.this.getContext(), BuildConfig.VERSION_NAME + e, listAppsFragment.TITLE_LIST_ITEM).show();
                            }
                        });
                    }
                }
            }

            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case Zip4jConstants.ENC_NO_ENCRYPTION /*-1*/:
                        try {
                            listAppsFragment.this.runWithWait(new C03001());
                            return;
                        } catch (Exception e) {
                            Toast.makeText(listAppsFragment.this.getContext(), BuildConfig.VERSION_NAME + e, listAppsFragment.TITLE_LIST_ITEM).show();
                            return;
                        }
                    default:
                        return;
                }
            }
        };
        Utils.showDialogYesNo(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.message_delete_odex_file), dialogClickListener, dialogClickListener, null);
    }

    public void runextendetpatch(PkgListItem pli) {
        try {
            plipack = pli.pkgName;
            supath = getContext().getPackageCodePath();
            str = BuildConfig.VERSION_NAME;
            Utils utils = new Utils(BuildConfig.VERSION_NAME);
            String[] strArr = new String[TITLE_LIST_ITEM];
            strArr[SETTINGS_VIEWSIZE_SMALL] = dalvikruncommand + ".nerorunpatch " + pli.pkgName + " search";
            str = utils.cmdRoot(strArr);
            removeDialogLP(EXT_PATCH_DIALOG);
            showDialogLP(EXT_PATCH_DIALOG);
        } catch (Exception e) {
            Toast.makeText(getContext(), BuildConfig.VERSION_NAME + e, TITLE_LIST_ITEM).show();
        }
    }

    public void ads(PkgListItem pli, String Pat) {
        try {
            str = BuildConfig.VERSION_NAME;
            String cmd = BuildConfig.VERSION_NAME;
            String system_app = "not_system";
            if (pli.system) {
                system_app = "system";
            }
            String uid = String.valueOf(getPkgMng().getPackageInfo(pli.pkgName, SETTINGS_VIEWSIZE_SMALL).applicationInfo.uid);
            cmd = dalvikruncommand + ".runpatchads " + pli.pkgName + " " + Pat + " " + getPkgMng().getPackageInfo(pli.pkgName, SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir + " " + getContext().getFilesDir() + " " + system_app + " " + "a" + " " + Utils.getCurrentRuntimeValue() + " '" + basepath + "/AdsBlockList.txt'" + " " + uid;
            System.out.println(Pat);
            if (Pat.contains("copyDC")) {
                cmd = dalvikruncommand + ".runpatchads " + pli.pkgName + " " + Pat + " " + getPkgMng().getPackageInfo(pli.pkgName, SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir + " " + getContext().getFilesDir() + " " + system_app + " " + "copyDC" + " " + Utils.getCurrentRuntimeValue() + " '" + basepath + "/AdsBlockList.txt'" + " " + uid;
                System.out.println(cmd);
            }
            patchData = new PatchData();
            patchData.pli = pli;
            patchData.typePatch = "ADS";
            patchData.appdir = getPkgMng().getPackageInfo(pli.pkgName, SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir;
            patchData.pkg = pli.pkgName;
            patchData.system_app = system_app;
            patchData.filesdir = getContext().getFilesDir().getAbsolutePath();
            patchData.uid = String.valueOf(getPkgMng().getPackageInfo(pli.pkgName, SETTINGS_VIEWSIZE_SMALL).applicationInfo.uid);
            patchData.cmd1 = cmd;
            patchData.cmd2 = dalvikruncommand + ".checkOdex " + getPkgMng().getPackageInfo(pli.pkgName, SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir + " " + getContext().getFilesDir();
            patchData.result = Pat;
            LvlAdsPatch();
        } catch (Exception e) {
        }
    }

    public void support(PkgListItem pli, String Pat) {
        try {
            str = BuildConfig.VERSION_NAME;
            String cmd = BuildConfig.VERSION_NAME;
            String system_app = "not_system";
            if (pli.system) {
                system_app = "system";
            }
            System.out.println(Pat);
            String uid = String.valueOf(getPkgMng().getPackageInfo(pli.pkgName, SETTINGS_VIEWSIZE_SMALL).applicationInfo.uid);
            String func_name = "runpatchsupportOld";
            if (api > LVL_PATCH_CONTEXT_DIALOG) {
                func_name = "runpatchsupport";
            }
            cmd = dalvikruncommand + "." + func_name + " " + pli.pkgName + " " + Pat + " " + getPkgMng().getPackageInfo(pli.pkgName, SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir + " " + getContext().getFilesDir() + " " + system_app + " " + "a" + " " + Utils.getCurrentRuntimeValue() + " " + uid;
            System.out.println(Pat);
            if (Pat.contains("copyDC")) {
                cmd = dalvikruncommand + "." + func_name + " " + pli.pkgName + " " + Pat + " " + getPkgMng().getPackageInfo(pli.pkgName, SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir + " " + getContext().getFilesDir() + " " + system_app + " " + "copyDC" + " " + Utils.getCurrentRuntimeValue() + " " + uid;
                System.out.println(cmd);
            }
            patchData = new PatchData();
            patchData.pli = pli;
            patchData.typePatch = "SUPPORT";
            patchData.appdir = getPkgMng().getPackageInfo(pli.pkgName, SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir;
            patchData.pkg = pli.pkgName;
            patchData.uid = String.valueOf(getPkgMng().getPackageInfo(pli.pkgName, SETTINGS_VIEWSIZE_SMALL).applicationInfo.uid);
            patchData.cmd1 = cmd;
            patchData.result = Pat;
            LvlAdsPatch();
        } catch (Exception e) {
        }
    }

    private void LvlAdsPatch() {
        try {
            showDialogLP(LOADING_PROGRESS_DIALOG);
            progress_loading.setMessage(Utils.getText(C0149R.string.wait) + "...");
            progress_loading.setCancelable(false);
            progress_loading.setMax(CHILD_NAME_SHEME_SELECT_ITEM);
            progress_loading.setTitle(Utils.getText(C0149R.string.wait));
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (isAdded()) {
            patchAct = (patchActivity) getActivity();
        }
        Thread t2 = new Thread(new Runnable() {

            class C03011 implements Runnable {
                C03011() {
                }

                public void run() {
                    if (listAppsFragment.progress_loading != null && listAppsFragment.progress_loading.isShowing()) {
                        listAppsFragment.progress_loading.setMessage(Utils.getText(C0149R.string.patch_step2));
                        listAppsFragment.progress_loading.setProgressNumberFormat(Utils.getText(C0149R.string.patch_step_http));
                    }
                }
            }

            class C03022 implements Runnable {
                C03022() {
                }

                public void run() {
                    listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.no_freespace_patch));
                }
            }

            class C03033 implements Runnable {
                C03033() {
                }

                public void run() {
                    listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.error_create_oat));
                }
            }

            class C03044 implements Runnable {
                C03044() {
                }

                public void run() {
                    listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.no_space_in_data_all));
                }
            }

            class C03055 implements Runnable {
                C03055() {
                }

                public void run() {
                    listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.unknown_error));
                }
            }

            class C03066 implements Runnable {
                C03066() {
                }

                public void run() {
                    listAppsFragment.removeDialogLP(listAppsFragment.LOADING_PROGRESS_DIALOG);
                    if (listAppsFragment.patchData.typePatch.equals("ADS")) {
                        try {
                            listAppsFragment.plia.getItem(listAppsFragment.patchData.pli.pkgName).modified = true;
                            if (!listAppsFragment.patchData.result.contains("copyDC")) {
                                listAppsFragment.plia.getItem(listAppsFragment.patchData.pli.pkgName).odex = true;
                            }
                            listAppsFragment.plia.notifyDataSetChanged(listAppsFragment.plia.getItem(listAppsFragment.patchData.pli.pkgName));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        listAppsFragment.getConfig().edit().putBoolean(listAppsFragment.plia.getItem(listAppsFragment.patchData.pli.pkgName).pkgName, true).commit();
                    }
                    if (listAppsFragment.patchData.typePatch.equals("SUPPORT")) {
                        try {
                            listAppsFragment.plia.getItem(listAppsFragment.patchData.pli.pkgName).modified = true;
                            if (!listAppsFragment.patchData.result.contains("copyDC")) {
                                listAppsFragment.plia.getItem(listAppsFragment.patchData.pli.pkgName).odex = true;
                            }
                            listAppsFragment.plia.notifyDataSetChanged(listAppsFragment.plia.getItem(listAppsFragment.patchData.pli.pkgName));
                        } catch (Exception e2) {
                            e2.printStackTrace();
                        }
                        listAppsFragment.getConfig().edit().putBoolean(listAppsFragment.plia.getItem(listAppsFragment.patchData.pli.pkgName).pkgName, true).commit();
                    }
                    if (listAppsFragment.patchData.typePatch.equals("LVL")) {
                        try {
                            listAppsFragment.plia.getItem(listAppsFragment.patchData.pli.pkgName).modified = true;
                            if (!listAppsFragment.patchData.result.contains("copyDC")) {
                                listAppsFragment.plia.getItem(listAppsFragment.patchData.pli.pkgName).odex = true;
                            }
                            listAppsFragment.plia.notifyDataSetChanged(listAppsFragment.plia.getItem(listAppsFragment.patchData.pli.pkgName));
                            listAppsFragment.getConfig().edit().putBoolean(listAppsFragment.plia.getItem(listAppsFragment.patchData.pli.pkgName).pkgName, true).commit();
                        } catch (Exception e22) {
                            e22.printStackTrace();
                        }
                    }
                    listAppsFragment.removeDialogLP(listAppsFragment.SETTINGS_SORTBY_STATUS);
                    listAppsFragment.showDialogLP(listAppsFragment.SETTINGS_SORTBY_STATUS);
                    if (listAppsFragment.str.contains("not enought free space for copy dalvik cache to odex.")) {
                        listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.no_freespace_patch));
                    }
                }
            }

            public void run() {
                System.out.println("3");
                String appdir = listAppsFragment.patchData.appdir;
                String pkg = listAppsFragment.patchData.pkg;
                String system_app = listAppsFragment.patchData.system_app;
                String cmd1 = listAppsFragment.patchData.cmd1;
                try {
                    listAppsFragment.str = BuildConfig.VERSION_NAME;
                    System.out.println(Utils.getCurrentRuntimeValue());
                    listAppsFragment.this.runToMain(new C03011());
                    listAppsFragment.result = BuildConfig.VERSION_NAME;
                    Utils utils = new Utils(BuildConfig.VERSION_NAME);
                    String[] strArr = new String[listAppsFragment.TITLE_LIST_ITEM];
                    strArr[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = cmd1;
                    listAppsFragment.str = utils.cmdRoot(strArr);
                    listAppsFragment.startUnderRoot = Boolean.valueOf(false);
                    if (listAppsFragment.str.contains("chelpus_return_1")) {
                        listAppsFragment.this.runToMain(new C03022());
                    }
                    if (listAppsFragment.str.contains("chelpus_return_2")) {
                        listAppsFragment.this.runToMain(new C03033());
                    }
                    if (listAppsFragment.str.contains("chelpus_return_3")) {
                        listAppsFragment.this.runToMain(new C03044());
                    }
                    if (listAppsFragment.str.contains("chelpus_return_4")) {
                        listAppsFragment.this.runToMain(new C03055());
                    }
                    listAppsFragment.this.afterPatch(listAppsFragment.patchData.result);
                } catch (Exception e) {
                    System.out.println("LuckyPatcher Error LVLADS: " + e);
                    e.printStackTrace();
                    try {
                        listAppsFragment.this.runToMain(new C03066());
                    } catch (Exception e2) {
                        System.out.println("LuckyPatcher: handler Null.");
                    }
                }
            }
        });
        t2.setPriority(PERM_CONTEXT_DIALOG);
        t2.start();
    }

    private void afterPatch(String result) {
        System.out.println(str);
        System.out.println("3");
        String appdir = patchData.appdir;
        String pkg = patchData.pkg;
        try {
            if (result.contains("backup")) {
                new File(basepath + "/Backup/").mkdirs();
                Utils.copyFile(appdir, basepath + "/Backup/" + pkg + ".apk", false, false);
            }
        } catch (Exception e) {
            System.out.println("LuckyPatcher LVLADS: " + e);
            e.printStackTrace();
        }
        runToMain(new Runnable() {

            class C03071 implements Runnable {
                C03071() {
                }

                public void run() {
                    listAppsFragment.plia.updateItem(listAppsFragment.patchData.pli.pkgName);
                    listAppsFragment.plia.notifyDataSetChanged();
                }
            }

            public void run() {
                listAppsFragment.removeDialogLP(listAppsFragment.LOADING_PROGRESS_DIALOG);
                if (listAppsFragment.patchData.typePatch.equals("ADS")) {
                    try {
                        listAppsFragment.plia.getItem(listAppsFragment.patchData.pli.pkgName).modified = true;
                        if (!listAppsFragment.patchData.result.contains("copyDC")) {
                            listAppsFragment.plia.getItem(listAppsFragment.patchData.pli.pkgName).odex = true;
                        }
                        listAppsFragment.plia.notifyDataSetChanged(listAppsFragment.plia.getItem(listAppsFragment.patchData.pli.pkgName));
                        listAppsFragment.getConfig().edit().putBoolean(listAppsFragment.plia.getItem(listAppsFragment.patchData.pli.pkgName).pkgName, true).commit();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (listAppsFragment.patchData.typePatch.equals("SUPPORT")) {
                    try {
                        listAppsFragment.plia.getItem(listAppsFragment.patchData.pli.pkgName).modified = true;
                        if (!listAppsFragment.patchData.result.contains("copyDC")) {
                            listAppsFragment.plia.getItem(listAppsFragment.patchData.pli.pkgName).odex = true;
                        }
                        listAppsFragment.plia.notifyDataSetChanged(listAppsFragment.plia.getItem(listAppsFragment.patchData.pli.pkgName));
                        listAppsFragment.getConfig().edit().putBoolean(listAppsFragment.plia.getItem(listAppsFragment.patchData.pli.pkgName).pkgName, true).commit();
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }
                if (listAppsFragment.patchData.typePatch.equals("LVL")) {
                    try {
                        listAppsFragment.plia.getItem(listAppsFragment.patchData.pli.pkgName).modified = true;
                        if (!listAppsFragment.patchData.result.contains("copyDC")) {
                            listAppsFragment.plia.getItem(listAppsFragment.patchData.pli.pkgName).odex = true;
                        }
                        listAppsFragment.plia.notifyDataSetChanged(listAppsFragment.plia.getItem(listAppsFragment.patchData.pli.pkgName));
                        listAppsFragment.getConfig().edit().putBoolean(listAppsFragment.plia.getItem(listAppsFragment.patchData.pli.pkgName).pkgName, true).commit();
                    } catch (Exception e22) {
                        e22.printStackTrace();
                    }
                }
                listAppsFragment.removeDialogLP(listAppsFragment.SETTINGS_SORTBY_STATUS);
                listAppsFragment.showDialogLP(listAppsFragment.SETTINGS_SORTBY_STATUS);
                if (listAppsFragment.str.contains("not enought free space for copy dalvik cache to odex.")) {
                    listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.no_freespace_patch));
                }
                listAppsFragment.this.runToMain(new C03071());
            }
        });
    }

    void custompatch(PkgListItem pli) {
        try {
            String supath = getContext().getPackageCodePath();
            func = TITLE_LIST_ITEM;
            String system_app = "not_system";
            if (pli.system) {
                system_app = "system";
            }
            String custom = customselect.toString();
            if (!customselect.getName().equals("ver.5.5.01_com.keramidas.TitaniumBackup.txt")) {
                new ArrayList().clear();
                new Thread(new Runnable() {
                    public void run() {
                    }
                }).start();
                str = BuildConfig.VERSION_NAME;
                CustomPatch custompatch = new CustomPatch();
                custompatch.appdir = getPkgMng().getPackageInfo(pli.pkgName, SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir;
                custompatch.system_app = system_app;
                String uid = String.valueOf(getPkgMng().getPackageInfo(pli.pkgName, SETTINGS_VIEWSIZE_SMALL).applicationInfo.uid);
                String run_command = dalvikruncommandWithFramework;
                if (!Utils.isWithFramework()) {
                    run_command = dalvikruncommand;
                }
                custompatch.cmd1 = run_command + ".custompatch " + pli.pkgName + " " + custom + " " + getPkgMng().getPackageInfo(pli.pkgName, SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir + " " + basepath + " " + getContext().getFilesDir() + " " + supath + " " + system_app + " " + Build.CPU_ABI + " " + "com.chelpus.root.utils" + " " + Utils.getCurrentRuntimeValue() + " " + uid + LogCollector.LINE_SEPARATOR;
                custompatch.filesdir = getContext().getFilesDir().getAbsolutePath();
                custompatch.uid = String.valueOf(getPkgMng().getPackageInfo(pli.pkgName, SETTINGS_VIEWSIZE_SMALL).applicationInfo.uid);
                custompatch.custom = new File(custom);
                showDialogLP(LOADING_PROGRESS_DIALOG);
                progress_loading.setMessage(Utils.getText(C0149R.string.wait) + "...");
                progress_loading.setCancelable(false);
                progress_loading.setMax(CHILD_NAME_SHEME_SELECT_ITEM);
                progress_loading.setTitle(Utils.getText(C0149R.string.wait));
                Thread t2 = new Thread(custompatch);
                t2.setPriority(PERM_CONTEXT_DIALOG);
                t2.start();
            } else if (customselect.getName().equals("ver.5.5.01_com.keramidas.TitaniumBackup.txt")) {
                custom_patch_ker();
                plia.getItem(pli.pkgName).modified = true;
                getConfig().edit().putBoolean(plia.getItem(pli.pkgName).pkgName, true).commit();
                plia.notifyDataSetChanged(plia.getItem(pli.pkgName));
                str = "SU Java-Code Running!\nCongratulations! Titanium Backup cracked!\nRun Titanium Backup and check License State!\nIf Application not PRO, please reboot!\nGood Luck!\nChelpus.";
                removeDialogLP(Custom_PATCH_DIALOG);
                showDialogLP(Custom_PATCH_DIALOG);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Luckypatcher " + e);
        }
    }

    private void afterCustomPatch(File custom, String cmd1, String cmd2, String appdir, String filesdir, String uid, String result) {
        try {
            int i;
            PkgListItem plil = pli;
            System.out.println(result);
            runToMain(new Runnable() {
                public void run() {
                    listAppsFragment.plia.updateItem(listAppsFragment.pli.pkgName);
                    listAppsFragment.plia.notifyDataSetChanged();
                }
            });
            boolean withFramework = Utils.isWithFramework();
            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(custom), "UTF-8"));
            String data = BuildConfig.VERSION_NAME;
            String[] txtdata = new String[2000];
            boolean component = false;
            while (true) {
                data = br.readLine();
                if (data == null) {
                    break;
                }
                txtdata[SETTINGS_VIEWSIZE_SMALL] = data;
                if (txtdata[SETTINGS_VIEWSIZE_SMALL].toUpperCase().contains("[") && txtdata[SETTINGS_VIEWSIZE_SMALL].toUpperCase().contains("]")) {
                    component = false;
                }
                if (txtdata[SETTINGS_VIEWSIZE_SMALL].toUpperCase().contains("[COMPONENT]")) {
                    component = true;
                }
                if (component) {
                    Utils utils;
                    String[] strArr;
                    String value1 = BuildConfig.VERSION_NAME;
                    if (txtdata[SETTINGS_VIEWSIZE_SMALL].contains("enable")) {
                        try {
                            value1 = new JSONObject(txtdata[SETTINGS_VIEWSIZE_SMALL]).getString("enable");
                        } catch (JSONException e) {
                            System.out.println("Error get component!");
                        }
                        try {
                            value1 = value1.trim();
                            utils = new Utils(BuildConfig.VERSION_NAME);
                            strArr = new String[TITLE_LIST_ITEM];
                            strArr[SETTINGS_VIEWSIZE_SMALL] = "pm enable '" + pli.pkgName + InternalZipConstants.ZIP_FILE_SEPARATOR + value1 + "'";
                            utils.cmdRoot(strArr);
                        } catch (Exception e2) {
                            e2.printStackTrace();
                        }
                    }
                    if (txtdata[SETTINGS_VIEWSIZE_SMALL].contains("disable")) {
                        try {
                            value1 = new JSONObject(txtdata[SETTINGS_VIEWSIZE_SMALL]).getString("disable");
                        } catch (JSONException e3) {
                            System.out.println("Error get component!");
                        }
                        value1 = value1.trim();
                        utils = new Utils(BuildConfig.VERSION_NAME);
                        strArr = new String[TITLE_LIST_ITEM];
                        strArr[SETTINGS_VIEWSIZE_SMALL] = "pm disable '" + pli.pkgName + InternalZipConstants.ZIP_FILE_SEPARATOR + value1 + "'";
                        utils.cmdRoot(strArr);
                    } else {
                        continue;
                    }
                }
            }
            if (result.contains("Patch on Reboot added!") && custom != null) {
                try {
                    Utils.copyFile(custom, new File(getContext().getDir("bootlist", SETTINGS_VIEWSIZE_SMALL) + InternalZipConstants.ZIP_FILE_SEPARATOR + plil.pkgName));
                } catch (Exception e4) {
                }
            }
            String[] mass = new String[result.split(LogCollector.LINE_SEPARATOR).length];
            mass = result.split(LogCollector.LINE_SEPARATOR);
            File sqldb = null;
            for (i = SETTINGS_VIEWSIZE_SMALL; i < mass.length; i += TITLE_LIST_ITEM) {
                if (!withFramework) {
                    if (mass[i].equals("Open SqLite database")) {
                        sqldb = new File(mass[i + TITLE_LIST_ITEM]);
                    }
                    if (mass[i].equals("Execute:")) {
                        try {
                            Utils.run_all("chmod 777 " + filesdir + InternalZipConstants.ZIP_FILE_SEPARATOR + sqldb.getName());
                            SQLiteDatabase db3 = SQLiteDatabase.openDatabase(filesdir + InternalZipConstants.ZIP_FILE_SEPARATOR + sqldb.getName(), null, SETTINGS_VIEWSIZE_SMALL);
                            db3.execSQL(mass[i + TITLE_LIST_ITEM]);
                            System.out.println(mass[i + TITLE_LIST_ITEM]);
                            db3.close();
                        } catch (Exception e22) {
                            System.out.println("LuckyPatcher: SQL error - " + e22);
                        }
                    } else {
                        continue;
                    }
                }
            }
            if (!withFramework) {
                for (i = SETTINGS_VIEWSIZE_SMALL; i < mass.length; i += TITLE_LIST_ITEM) {
                    if (mass[i].equals("Open SqLite database")) {
                        sqldb = new File(mass[i + TITLE_LIST_ITEM]);
                        File file = new File(filesdir + InternalZipConstants.ZIP_FILE_SEPARATOR + sqldb.getName());
                        if (file.exists()) {
                            if (sqldb != null && Utils.exists(sqldb.getAbsolutePath() + "-wal")) {
                                Utils.run_all("chmod 777 " + sqldb.getAbsolutePath() + "-wal");
                                Utils.run_all("rm " + sqldb.getAbsolutePath() + "-wal");
                            }
                            if (Utils.exists(sqldb.getAbsolutePath() + "-shm")) {
                                Utils.run_all("chmod 777 " + sqldb.getAbsolutePath() + "-shm");
                                Utils.run_all("rm " + sqldb.getAbsolutePath() + "-shm");
                            }
                            Utils.copyFile(filesdir + InternalZipConstants.ZIP_FILE_SEPARATOR + sqldb.getName(), sqldb.getAbsolutePath(), true, true);
                            file.delete();
                        }
                    }
                }
            }
            runToMain(new Runnable() {
                public void run() {
                    if (listAppsFragment.progress_loading != null && listAppsFragment.progress_loading.isShowing()) {
                        listAppsFragment.progress_loading.setMessage(Utils.getText(C0149R.string.patch_step6));
                        listAppsFragment.progress_loading.setProgress(listAppsFragment.CHILD_NAME_SHEME_SELECT_ITEM);
                    }
                }
            });
            if (cmd1.contains("patch.by.sanx_com.maxmpz.audioplayer")) {
                pa();
            }
            if (cmd1.contains("patch_ren.by.ramzezzz_com.maxmpz.audioplayer.txt")) {
                pa2();
            }
            if (cmd1.contains("patch.by.sanx_com.emulator.fpse")) {
                fpse();
            }
            Utils.kill(pli.pkgName);
            str = result;
            runToMain(new Runnable() {
                public void run() {
                    try {
                        listAppsFragment.plia.getItem(listAppsFragment.pli.pkgName).modified = true;
                        listAppsFragment.refresh = true;
                        listAppsFragment.getConfig().edit().putBoolean(listAppsFragment.plia.getItem(listAppsFragment.pli.pkgName).pkgName, true).commit();
                        listAppsFragment.plia.notifyDataSetChanged(listAppsFragment.plia.getItem(listAppsFragment.pli.pkgName));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    listAppsFragment.removeDialogLP(listAppsFragment.LOADING_PROGRESS_DIALOG);
                    listAppsFragment.removeDialogLP(listAppsFragment.Custom_PATCH_DIALOG);
                    listAppsFragment.showDialogLP(listAppsFragment.Custom_PATCH_DIALOG);
                }
            });
        } catch (Exception e222) {
            e222.printStackTrace();
            System.out.println("luuuuu " + e222);
            str = result;
            runToMain(new Runnable() {
                public void run() {
                    try {
                        listAppsFragment.plia.getItem(listAppsFragment.pli.pkgName).modified = true;
                        listAppsFragment.refresh = true;
                        listAppsFragment.getConfig().edit().putBoolean(listAppsFragment.plia.getItem(listAppsFragment.pli.pkgName).pkgName, true).commit();
                        listAppsFragment.plia.notifyDataSetChanged(listAppsFragment.plia.getItem(listAppsFragment.pli.pkgName));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    listAppsFragment.removeDialogLP(listAppsFragment.LOADING_PROGRESS_DIALOG);
                    listAppsFragment.removeDialogLP(listAppsFragment.Custom_PATCH_DIALOG);
                    listAppsFragment.showDialogLP(listAppsFragment.Custom_PATCH_DIALOG);
                }
            });
        }
    }

    public void createapklvl(String resul) {
        result = resul;
        if (testSD(true)) {
            runWithWait(new Runnable() {

                class C03181 implements Runnable {
                    C03181() {
                    }

                    public void run() {
                        listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.file_not_found));
                        listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                        listAppsFragment.this.vibrateEnd();
                    }
                }

                public void run() {
                    PkgListItem pli = listAppsFragment.pli;
                    String prefix = BuildConfig.VERSION_NAME;
                    String versionPkg = BuildConfig.VERSION_NAME;
                    try {
                        versionPkg = ".v." + listAppsFragment.getPkgMng().getPackageInfo(listAppsFragment.pli.pkgName, listAppsFragment.SETTINGS_VIEWSIZE_SMALL).versionName + ".b." + listAppsFragment.getPkgMng().getPackageInfo(listAppsFragment.pli.pkgName, listAppsFragment.SETTINGS_VIEWSIZE_SMALL).versionCode;
                        listAppsFragment.str = BuildConfig.VERSION_NAME;
                        listAppsFragment.this.getSignatureKeys();
                        if (listAppsFragment.result.contains("dependencies")) {
                            prefix = "crk.Dependencies";
                        }
                        if (listAppsFragment.result.contains("pattern5")) {
                            prefix = "crk.LVL.Auto";
                        }
                        if (listAppsFragment.result.contains("pattern6")) {
                            prefix = "crk.LVL.AutoInverse";
                        }
                        if (listAppsFragment.result.contains("pattern4")) {
                            prefix = "crk.LVL.Extreme";
                        }
                        if (listAppsFragment.result.contains("amazon")) {
                            prefix = "crk.Amazon";
                        }
                        if (listAppsFragment.result.contains("samsung")) {
                            prefix = "crk.SamsungApps";
                        }
                        System.out.println("Patterns for patch: " + listAppsFragment.result);
                        String path = listAppsFragment.getPkgMng().getPackageInfo(pli.pkgName, listAppsFragment.SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir;
                        if (path.contains("/mnt/asec/")) {
                            Utils.remount(path.replace("/pkg.apk", BuildConfig.VERSION_NAME), InternalZipConstants.WRITE_MODE);
                        }
                        if (path.startsWith("/system/")) {
                            Utils.remount("/system", InternalZipConstants.WRITE_MODE);
                        }
                        Utils.run_all("chmod 644 " + path);
                        if (new File(path).exists()) {
                            File apkcrk;
                            ArrayList add_files;
                            Iterator it;
                            String[] param = new String[listAppsFragment.CONTEXT_DIALOG];
                            param[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = pli.pkgName;
                            param[listAppsFragment.TITLE_LIST_ITEM] = listAppsFragment.result;
                            param[listAppsFragment.SETTINGS_SORTBY_STATUS] = path;
                            param[listAppsFragment.SETTINGS_SORTBY_TIME] = listAppsFragment.toolfilesdir;
                            param[listAppsFragment.Custom_PATCH_DIALOG] = "not_system";
                            param[listAppsFragment.EXT_PATCH_DIALOG] = listAppsFragment.basepath;
                            param[listAppsFragment.CHILD_NAME_SHEME_SELECT_ITEM] = "createAPK";
                            odexrunpatch patch = new odexrunpatch();
                            listAppsFragment.startUnderRoot = Boolean.valueOf(false);
                            odexrunpatch.main(param);
                            listAppsFragment.str = odexrunpatch.result;
                            System.out.println(listAppsFragment.str);
                            String fix = BuildConfig.VERSION_NAME;
                            if (listAppsFragment.result.contains("dependencies")) {
                                fix = "DependenciesMode Lucky";
                            }
                            if (listAppsFragment.result.contains("pattern5")) {
                                fix = "NormalMode Lucky";
                            }
                            if (listAppsFragment.result.contains("pattern6")) {
                                fix = "InverseNormalMode Lucky";
                            }
                            if (listAppsFragment.result.contains("pattern4")) {
                                fix = "ExtremeMode Lucky";
                            }
                            if (listAppsFragment.result.contains("amazon")) {
                                fix = "AmazonMode Lucky";
                            }
                            if (listAppsFragment.result.contains("samsung")) {
                                fix = "SamsungMode Lucky";
                            }
                            listAppsFragment.str += fix;
                            ZipSignerLP signer = new ZipSignerLP();
                            X509Certificate cert = signer.readPublicKey(new File(listAppsFragment.basepath + "/Modified/Keys/" + "testkey.x509.pem").toURI().toURL());
                            byte[] sigBlockTemplate = signer.readContentAsBytes(new File(listAppsFragment.basepath + "/Modified/Keys/" + "testkey.sbt").toURI().toURL());
                            signer.setKeys("custom", cert, signer.readPrivateKey(new File(listAppsFragment.basepath + "/Modified/Keys/" + "testkey.pk8").toURI().toURL(), "lp"), sigBlockTemplate);
                            try {
                                apkcrk = new File(listAppsFragment.basepath + "/Modified/" + pli.pkgName + ".apk");
                                add_files = new ArrayList();
                                if (odexrunpatch.classesFiles != null && odexrunpatch.classesFiles.size() > 0) {
                                    it = odexrunpatch.classesFiles.iterator();
                                    while (it.hasNext()) {
                                        add_files.add(new AddFilesItem(((File) it.next()).getAbsolutePath(), listAppsFragment.basepath + "/Modified/"));
                                    }
                                }
                                signer.signZip(listAppsFragment.basepath + "/Modified/" + pli.pkgName + ".apk", listAppsFragment.basepath + "/Modified/" + pli.pkgName + "." + prefix + ".signed.apk", add_files);
                                if (apkcrk.exists()) {
                                    apkcrk.delete();
                                }
                            } catch (Throwable th) {
                                apkcrk = new File(listAppsFragment.basepath + "/Modified/" + pli.pkgName + ".apk");
                                if (apkcrk.exists()) {
                                    apkcrk.delete();
                                }
                            }
                            if (listAppsFragment.str.contains("compress classes.dex error")) {
                                try {
                                    ZipFile zipFile = new ZipFile(listAppsFragment.basepath + "/Modified/" + pli.pkgName + "." + prefix + ".signed.apk");
                                    ArrayList<File> filesToAdd = new ArrayList();
                                    if (odexrunpatch.classesFiles != null && odexrunpatch.classesFiles.size() > 0) {
                                        it = odexrunpatch.classesFiles.iterator();
                                        while (it.hasNext()) {
                                            filesToAdd.add((File) it.next());
                                        }
                                    }
                                    ZipParameters parameters = new ZipParameters();
                                    parameters.setCompressionMethod(listAppsFragment.SETTINGS_VIEWSIZE_SMALL);
                                    zipFile.addFiles(filesToAdd, parameters);
                                    new File(listAppsFragment.basepath + "/Modified/" + pli.pkgName + "." + prefix + ".signed.apk").renameTo(new File(listAppsFragment.basepath + "/Modified/" + pli.pkgName + ".apk"));
                                    try {
                                        apkcrk = new File(listAppsFragment.basepath + "/Modified/" + pli.pkgName + ".apk");
                                        add_files = new ArrayList();
                                        if (odexrunpatch.classesFiles != null && odexrunpatch.classesFiles.size() > 0) {
                                            it = odexrunpatch.classesFiles.iterator();
                                            while (it.hasNext()) {
                                                add_files.add(new AddFilesItem(((File) it.next()).getAbsolutePath(), listAppsFragment.basepath + "/Modified/"));
                                            }
                                        }
                                        signer.signZip(listAppsFragment.basepath + "/Modified/" + pli.pkgName + ".apk", listAppsFragment.basepath + "/Modified/" + pli.pkgName + "." + prefix + ".signed.apk", add_files);
                                        if (apkcrk.exists()) {
                                            apkcrk.delete();
                                        }
                                    } catch (Throwable th2) {
                                        apkcrk = new File(listAppsFragment.basepath + "/Modified/" + pli.pkgName + ".apk");
                                        if (apkcrk.exists()) {
                                            apkcrk.delete();
                                        }
                                    }
                                } catch (ZipException e) {
                                    System.out.println("compress classes.dex error" + e);
                                    e.printStackTrace();
                                } catch (Exception e2) {
                                    System.out.println(e2);
                                } catch (IOException localIOException) {
                                    localIOException.printStackTrace();
                                }
                            }
                            if (odexrunpatch.classesFiles != null && odexrunpatch.classesFiles.size() > 0) {
                                it = odexrunpatch.classesFiles.iterator();
                                while (it.hasNext()) {
                                    File cl = (File) it.next();
                                    if (cl.exists()) {
                                        cl.delete();
                                    }
                                }
                            }
                            apkcrk = new File(listAppsFragment.basepath + "/Modified/" + pli.pkgName + "." + prefix + ".signed.apk");
                            File finalapk = new File(listAppsFragment.basepath + "/Modified/" + pli.pkgName + "." + prefix + ".Removed.apk");
                            if (apkcrk.exists() && finalapk.exists()) {
                                apkcrk.delete();
                            }
                            File folderstorage = new File(listAppsFragment.basepath + "/Modified/" + pli.name.replaceAll(" ", ".").replaceAll(InternalZipConstants.ZIP_FILE_SEPARATOR, "."));
                            folderstorage.mkdirs();
                            if (!finalapk.exists() && listAppsFragment.getConfig().getInt("apkname", listAppsFragment.SETTINGS_VIEWSIZE_SMALL) == 0) {
                                apkcrk.renameTo(new File(folderstorage.getCanonicalFile() + InternalZipConstants.ZIP_FILE_SEPARATOR + pli.name.replaceAll(" ", ".").replaceAll(InternalZipConstants.ZIP_FILE_SEPARATOR, ".") + versionPkg + "." + prefix + ".Removed.apk"));
                            }
                            if (!(finalapk.exists() || listAppsFragment.getConfig().getInt("apkname", listAppsFragment.SETTINGS_VIEWSIZE_SMALL) == 0)) {
                                apkcrk.renameTo(new File(folderstorage.getCanonicalFile() + InternalZipConstants.ZIP_FILE_SEPARATOR + pli.pkgName + versionPkg + "." + prefix + ".Removed.apk"));
                            }
                            if (finalapk.exists() && listAppsFragment.getConfig().getInt("apkname", listAppsFragment.SETTINGS_VIEWSIZE_SMALL) == 0) {
                                finalapk.renameTo(new File(folderstorage.getCanonicalFile() + InternalZipConstants.ZIP_FILE_SEPARATOR + pli.name.replaceAll(" ", ".").replaceAll(InternalZipConstants.ZIP_FILE_SEPARATOR, ".") + versionPkg + "." + prefix + ".Removed.apk"));
                            }
                            if (finalapk.exists() && listAppsFragment.getConfig().getInt("apkname", listAppsFragment.SETTINGS_VIEWSIZE_SMALL) != 0) {
                                finalapk.renameTo(new File(folderstorage.getCanonicalFile() + InternalZipConstants.ZIP_FILE_SEPARATOR + pli.pkgName + versionPkg + "." + prefix + ".Removed.apk"));
                            }
                            final PkgListItem pkgListItem = pli;
                            listAppsFragment.this.runToMain(new Runnable() {
                                public void run() {
                                    listAppsFragment.removeDialogLP(listAppsFragment.CREATEAPK_DIALOG);
                                    listAppsFragment.pli = pkgListItem;
                                    listAppsFragment.showDialogLP(listAppsFragment.CREATEAPK_DIALOG);
                                    listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
                                    listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                                    if (listAppsFragment.getConfig().getBoolean("vibration", false)) {
                                        listAppsFragment.this.vib = (Vibrator) listAppsFragment.this.getContext().getSystemService("vibrator");
                                        listAppsFragment.this.vib.vibrate(50);
                                    }
                                }
                            });
                            return;
                        }
                        listAppsFragment.this.runToMain(new C03181());
                    } catch (IOException localIOException2) {
                        localIOException2.printStackTrace();
                    } catch (Exception e22) {
                        e22.printStackTrace();
                    }
                }
            });
        }
    }

    public void toolbar_createapklvl(String resul) {
        result = resul;
        if (testSD(true)) {
            runWithWait(new Runnable() {

                class C03201 implements Runnable {
                    C03201() {
                    }

                    public void run() {
                        listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.unknown_error));
                        listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                        listAppsFragment.this.vibrateEnd();
                    }
                }

                class C03212 implements Runnable {
                    C03212() {
                    }

                    public void run() {
                        listAppsFragment.this.showMessage(Utils.getText(C0149R.string.rebuild_info), Utils.getText(C0149R.string.rebuild_message) + " " + listAppsFragment.rebuldApk);
                        listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                        listAppsFragment.this.vibrateEnd();
                    }
                }

                public void run() {
                    File apkcrk;
                    ApplicationInfo pli = Utils.getApkInfo(listAppsFragment.rebuldApk);
                    PackageInfo pkgInfo = Utils.getApkPackageInfo(listAppsFragment.rebuldApk);
                    String prefix = BuildConfig.VERSION_NAME;
                    String versionPkg = BuildConfig.VERSION_NAME;
                    try {
                        versionPkg = ".v." + pkgInfo.versionName + ".b." + pkgInfo.versionCode;
                    } catch (Exception e) {
                        e.printStackTrace();
                        versionPkg = ".v.unknown";
                    }
                    try {
                        ArrayList add_files;
                        Iterator it;
                        listAppsFragment.str = BuildConfig.VERSION_NAME;
                        listAppsFragment.this.getSignatureKeys();
                        String[] param = new String[listAppsFragment.CONTEXT_DIALOG];
                        param[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = pli.packageName;
                        param[listAppsFragment.TITLE_LIST_ITEM] = listAppsFragment.result;
                        param[listAppsFragment.SETTINGS_SORTBY_STATUS] = listAppsFragment.rebuldApk;
                        param[listAppsFragment.SETTINGS_SORTBY_TIME] = listAppsFragment.this.getContext().getFilesDir().toString();
                        param[listAppsFragment.Custom_PATCH_DIALOG] = "not_system";
                        param[listAppsFragment.EXT_PATCH_DIALOG] = listAppsFragment.basepath;
                        param[listAppsFragment.CHILD_NAME_SHEME_SELECT_ITEM] = "createAPK";
                        odexrunpatch.main(param);
                        listAppsFragment.startUnderRoot = Boolean.valueOf(false);
                        System.out.println("LuckyPatcher (rebuild): classes.dex patched.");
                        if (listAppsFragment.result.contains("dependencies")) {
                            prefix = "crk.Dependencies";
                        }
                        if (listAppsFragment.result.contains("pattern5")) {
                            prefix = "crk.LVL.Auto";
                        }
                        if (listAppsFragment.result.contains("pattern6")) {
                            prefix = "crk.LVL.AutoInverse";
                        }
                        if (listAppsFragment.result.contains("pattern4")) {
                            prefix = "crk.LVL.Extreme";
                        }
                        if (listAppsFragment.result.contains("amazon")) {
                            prefix = "crk.Amazon";
                        }
                        if (listAppsFragment.result.contains("samsung")) {
                            prefix = "crk.SamsungApps";
                        }
                        String fix = BuildConfig.VERSION_NAME;
                        if (listAppsFragment.result.contains("dependencies")) {
                            fix = "DependenciesMode Lucky";
                        }
                        if (listAppsFragment.result.contains("pattern5")) {
                            fix = "NormalMode Lucky";
                        }
                        if (listAppsFragment.result.contains("pattern6")) {
                            fix = "InverseNormalMode Lucky";
                        }
                        if (listAppsFragment.result.contains("pattern4")) {
                            fix = "ExtremeMode Lucky";
                        }
                        if (listAppsFragment.result.contains("amazon")) {
                            fix = "AmazonMode Lucky";
                        }
                        if (listAppsFragment.result.contains("samsung")) {
                            fix = "SamsungMode Lucky";
                        }
                        listAppsFragment.str += fix;
                        ZipSignerLP signer = new ZipSignerLP();
                        X509Certificate cert = signer.readPublicKey(new File(listAppsFragment.basepath + "/Modified/Keys/" + "testkey.x509.pem").toURI().toURL());
                        byte[] sigBlockTemplate = signer.readContentAsBytes(new File(listAppsFragment.basepath + "/Modified/Keys/" + "testkey.sbt").toURI().toURL());
                        signer.setKeys("custom", cert, signer.readPrivateKey(new File(listAppsFragment.basepath + "/Modified/Keys/" + "testkey.pk8").toURI().toURL(), "lp"), sigBlockTemplate);
                        try {
                            apkcrk = new File(listAppsFragment.basepath + "/Modified/" + pli.packageName + ".apk");
                            add_files = new ArrayList();
                            if (odexrunpatch.classesFiles != null && odexrunpatch.classesFiles.size() > 0) {
                                it = odexrunpatch.classesFiles.iterator();
                                while (it.hasNext()) {
                                    add_files.add(new AddFilesItem(((File) it.next()).getAbsolutePath(), listAppsFragment.basepath + "/Modified/"));
                                }
                            }
                            signer.signZip(listAppsFragment.basepath + "/Modified/" + pli.packageName + ".apk", listAppsFragment.basepath + "/Modified/" + pli.packageName + "." + prefix + ".signed.apk", add_files);
                            if (apkcrk.exists()) {
                                apkcrk.delete();
                            }
                        } catch (Throwable th) {
                            apkcrk = new File(listAppsFragment.basepath + "/Modified/" + pli.packageName + ".apk");
                            if (apkcrk.exists()) {
                                apkcrk.delete();
                            }
                        }
                        if (listAppsFragment.str.contains("compress classes.dex error")) {
                            try {
                                ZipFile zipFile = new ZipFile(listAppsFragment.basepath + "/Modified/" + pli.packageName + "." + prefix + ".signed.apk");
                                ArrayList<File> filesToAdd = new ArrayList();
                                if (odexrunpatch.classesFiles != null && odexrunpatch.classesFiles.size() > 0) {
                                    it = odexrunpatch.classesFiles.iterator();
                                    while (it.hasNext()) {
                                        filesToAdd.add((File) it.next());
                                    }
                                }
                                ZipParameters parameters = new ZipParameters();
                                parameters.setCompressionMethod(listAppsFragment.SETTINGS_VIEWSIZE_SMALL);
                                zipFile.addFiles(filesToAdd, parameters);
                                new File(listAppsFragment.basepath + "/Modified/" + pli.packageName + "." + prefix + ".signed.apk").renameTo(new File(listAppsFragment.basepath + "/Modified/" + pli.packageName + ".apk"));
                                try {
                                    apkcrk = new File(listAppsFragment.basepath + "/Modified/" + pli.packageName + ".apk");
                                    add_files = new ArrayList();
                                    if (odexrunpatch.classesFiles != null && odexrunpatch.classesFiles.size() > 0) {
                                        it = odexrunpatch.classesFiles.iterator();
                                        while (it.hasNext()) {
                                            add_files.add(new AddFilesItem(((File) it.next()).getAbsolutePath(), listAppsFragment.basepath + "/Modified/"));
                                        }
                                    }
                                    signer.signZip(listAppsFragment.basepath + "/Modified/" + pli.packageName + ".apk", listAppsFragment.basepath + "/Modified/" + pli.packageName + "." + prefix + ".signed.apk", add_files);
                                    if (apkcrk.exists()) {
                                        apkcrk.delete();
                                    }
                                } catch (Throwable th2) {
                                    apkcrk = new File(listAppsFragment.basepath + "/Modified/" + pli.packageName + ".apk");
                                    if (apkcrk.exists()) {
                                        apkcrk.delete();
                                    }
                                }
                            } catch (ZipException e2) {
                                System.out.println("compress classes.dex error" + e2);
                                e2.printStackTrace();
                            } catch (Exception e3) {
                                System.out.println(e3);
                            }
                        }
                        if (odexrunpatch.classesFiles != null && odexrunpatch.classesFiles.size() > 0) {
                            it = odexrunpatch.classesFiles.iterator();
                            while (it.hasNext()) {
                                File cl = (File) it.next();
                                if (cl.exists()) {
                                    cl.delete();
                                }
                            }
                        }
                        apkcrk = new File(listAppsFragment.basepath + "/Modified/" + pli.packageName + "." + prefix + ".signed.apk");
                        File finalapk = new File(listAppsFragment.basepath + "/Modified/" + pli.packageName + "." + prefix + ".Removed.apk");
                        if (apkcrk.exists() && finalapk.exists()) {
                            apkcrk.delete();
                        }
                        File folderstorage = new File(listAppsFragment.basepath + "/Modified/" + Utils.getApkLabelName(listAppsFragment.rebuldApk).replaceAll(" ", "."));
                        folderstorage.mkdirs();
                        String resultfile = BuildConfig.VERSION_NAME;
                        if (!finalapk.exists() && listAppsFragment.getConfig().getInt("apkname", listAppsFragment.SETTINGS_VIEWSIZE_SMALL) == 0) {
                            resultfile = folderstorage.getCanonicalFile() + InternalZipConstants.ZIP_FILE_SEPARATOR + Utils.getApkLabelName(listAppsFragment.rebuldApk).replace(" ", ".") + versionPkg + "." + prefix + ".Removed.apk";
                            apkcrk.renameTo(new File(resultfile));
                        }
                        if (!(finalapk.exists() || listAppsFragment.getConfig().getInt("apkname", listAppsFragment.SETTINGS_VIEWSIZE_SMALL) == 0)) {
                            resultfile = folderstorage.getCanonicalFile() + InternalZipConstants.ZIP_FILE_SEPARATOR + pli.packageName + versionPkg + "." + prefix + ".Removed.apk";
                            apkcrk.renameTo(new File(resultfile));
                        }
                        if (finalapk.exists() && listAppsFragment.getConfig().getInt("apkname", listAppsFragment.SETTINGS_VIEWSIZE_SMALL) == 0) {
                            resultfile = folderstorage.getCanonicalFile() + InternalZipConstants.ZIP_FILE_SEPARATOR + Utils.getApkLabelName(listAppsFragment.rebuldApk).replace(" ", ".") + versionPkg + "." + prefix + ".Removed.apk";
                            finalapk.renameTo(new File(resultfile));
                        }
                        if (finalapk.exists() && listAppsFragment.getConfig().getInt("apkname", listAppsFragment.SETTINGS_VIEWSIZE_SMALL) != 0) {
                            resultfile = folderstorage.getCanonicalFile() + InternalZipConstants.ZIP_FILE_SEPARATOR + pli.packageName + versionPkg + "." + prefix + ".Removed.apk";
                            finalapk.renameTo(new File(resultfile));
                        }
                        listAppsFragment.rebuldApk = resultfile;
                        listAppsFragment.this.runToMain(new C03212());
                    } catch (Exception e32) {
                        e32.printStackTrace();
                        listAppsFragment.this.runToMain(new C03201());
                    }
                }
            });
        }
    }

    void createapkcustom() {
        System.out.println("Create with custom patch.");
        if (testSD(true)) {
            runWithWait(new Runnable() {

                class C03221 implements Runnable {
                    C03221() {
                    }

                    public void run() {
                        listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.file_not_found));
                        listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                        listAppsFragment.this.vibrateEnd();
                    }
                }

                class C03232 implements Runnable {
                    C03232() {
                    }

                    public void run() {
                        listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.unknown_error));
                        listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                        listAppsFragment.this.vibrateEnd();
                    }
                }

                class C03243 implements Runnable {
                    C03243() {
                    }

                    public void run() {
                        listAppsFragment.removeDialogLP(listAppsFragment.Custom_PATCH_DIALOG);
                        listAppsFragment.showDialogLP(listAppsFragment.Custom_PATCH_DIALOG);
                        listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                        listAppsFragment.this.vibrateEnd();
                    }
                }

                class C03254 implements Runnable {
                    C03254() {
                    }

                    public void run() {
                        listAppsFragment.removeDialogLP(listAppsFragment.Custom_PATCH_DIALOG);
                        listAppsFragment.showDialogLP(listAppsFragment.Custom_PATCH_DIALOG);
                        listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                        listAppsFragment.this.vibrateEnd();
                    }
                }

                public void run() {
                    File apkcrk;
                    String versionPkg;
                    String custom;
                    String[] param;
                    createapkcustom cr_custom;
                    ZipSignerLP signer;
                    X509Certificate cert;
                    byte[] sigBlockTemplate;
                    ArrayList add_files;
                    Iterator it;
                    File finalapk;
                    File folderstorage;
                    if (listAppsFragment.rebuldApk.equals(BuildConfig.VERSION_NAME)) {
                        PkgListItem pli = listAppsFragment.pli;
                        versionPkg = BuildConfig.VERSION_NAME;
                        try {
                            versionPkg = ".v." + listAppsFragment.getPkgMng().getPackageInfo(pli.pkgName, listAppsFragment.SETTINGS_VIEWSIZE_SMALL).versionName + ".b." + listAppsFragment.getPkgMng().getPackageInfo(pli.pkgName, listAppsFragment.SETTINGS_VIEWSIZE_SMALL).versionCode;
                            listAppsFragment.supath = listAppsFragment.this.getContext().getPackageCodePath();
                            custom = listAppsFragment.customselect.toString();
                            listAppsFragment.str = BuildConfig.VERSION_NAME;
                            listAppsFragment.this.getSignatureKeys();
                            String path = listAppsFragment.getPkgMng().getPackageInfo(pli.pkgName, listAppsFragment.SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir;
                            if (path.contains("/mnt/asec/")) {
                                Utils.remount(path.replace("/pkg.apk", BuildConfig.VERSION_NAME), InternalZipConstants.WRITE_MODE);
                            }
                            if (path.startsWith("/system/")) {
                                Utils.remount("/system", InternalZipConstants.WRITE_MODE);
                            }
                            Utils.run_all("chmod 644 " + path);
                            if (new File(path).exists()) {
                                param = new String[listAppsFragment.CONTEXT_DIALOG];
                                param[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = pli.pkgName;
                                param[listAppsFragment.TITLE_LIST_ITEM] = custom;
                                param[listAppsFragment.SETTINGS_SORTBY_STATUS] = path;
                                param[listAppsFragment.SETTINGS_SORTBY_TIME] = listAppsFragment.basepath;
                                param[listAppsFragment.Custom_PATCH_DIALOG] = listAppsFragment.this.getContext().getFilesDir().getAbsolutePath();
                                param[listAppsFragment.EXT_PATCH_DIALOG] = listAppsFragment.supath;
                                param[listAppsFragment.CHILD_NAME_SHEME_SELECT_ITEM] = "com.chelpus.root.utils";
                                cr_custom = new createapkcustom();
                                listAppsFragment.str = createapkcustom.main(param);
                                try {
                                    signer = new ZipSignerLP();
                                    cert = signer.readPublicKey(new File(listAppsFragment.basepath + "/Modified/Keys/" + "testkey.x509.pem").toURI().toURL());
                                    sigBlockTemplate = signer.readContentAsBytes(new File(listAppsFragment.basepath + "/Modified/Keys/" + "testkey.sbt").toURI().toURL());
                                    signer.setKeys("custom", cert, signer.readPrivateKey(new File(listAppsFragment.basepath + "/Modified/Keys/" + "testkey.pk8").toURI().toURL(), "lp"), sigBlockTemplate);
                                    apkcrk = new File(listAppsFragment.basepath + "/Modified/" + pli.pkgName + ".apk");
                                    add_files = new ArrayList();
                                    if (createapkcustom.classesFiles != null && createapkcustom.classesFiles.size() > 0) {
                                        it = createapkcustom.classesFiles.iterator();
                                        while (it.hasNext()) {
                                            add_files.add(new AddFilesItem(((File) it.next()).getAbsolutePath(), listAppsFragment.basepath + "/Modified/"));
                                        }
                                    }
                                    signer.signZip(listAppsFragment.basepath + "/Modified/" + pli.pkgName + ".apk", listAppsFragment.basepath + "/Modified/" + pli.pkgName + ".crk.Custom.Patch.signed.apk", add_files);
                                    if (new File(listAppsFragment.basepath + "/Modified/" + pli.pkgName + ".crk.Custom.Patch.signed.apk").length() == 0) {
                                        new File(listAppsFragment.basepath + "/Modified/" + pli.pkgName + ".crk.Custom.Patch.signed.apk").delete();
                                        listAppsFragment.this.runToMain(new C03232());
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    System.out.println(e);
                                } catch (IOException localIOException) {
                                    localIOException.printStackTrace();
                                } catch (Throwable th) {
                                    apkcrk = new File(listAppsFragment.basepath + "/Modified/" + pli.pkgName + ".apk");
                                    if (apkcrk.exists()) {
                                        apkcrk.delete();
                                    }
                                }
                                if (apkcrk.exists()) {
                                    apkcrk.delete();
                                }
                                apkcrk = new File(listAppsFragment.basepath + "/Modified/" + pli.pkgName + ".crk.Custom.Patch.signed.apk");
                                finalapk = new File(listAppsFragment.basepath + "/Modified/" + pli.pkgName + ".crk.Custom.Patch.apk");
                                if (apkcrk.exists() && finalapk.exists()) {
                                    apkcrk.delete();
                                }
                                folderstorage = new File(listAppsFragment.basepath + "/Modified/" + pli.name.replaceAll(" ", ".").replaceAll(InternalZipConstants.ZIP_FILE_SEPARATOR, "."));
                                folderstorage.mkdirs();
                                if (!finalapk.exists() && listAppsFragment.getConfig().getInt("apkname", listAppsFragment.SETTINGS_VIEWSIZE_SMALL) == 0) {
                                    apkcrk.renameTo(new File(folderstorage.getCanonicalFile() + InternalZipConstants.ZIP_FILE_SEPARATOR + pli.name.replaceAll(" ", ".").replaceAll(InternalZipConstants.ZIP_FILE_SEPARATOR, ".") + versionPkg + ".crk.Custom.Patch.apk"));
                                }
                                if (!(finalapk.exists() || listAppsFragment.getConfig().getInt("apkname", listAppsFragment.SETTINGS_VIEWSIZE_SMALL) == 0)) {
                                    apkcrk.renameTo(new File(folderstorage.getCanonicalFile() + InternalZipConstants.ZIP_FILE_SEPARATOR + pli.pkgName + versionPkg + ".crk.Custom.Patch.apk"));
                                }
                                if (finalapk.exists() && listAppsFragment.getConfig().getInt("apkname", listAppsFragment.SETTINGS_VIEWSIZE_SMALL) == 0) {
                                    finalapk.renameTo(new File(folderstorage.getCanonicalFile() + InternalZipConstants.ZIP_FILE_SEPARATOR + pli.name.replaceAll(" ", ".").replaceAll(InternalZipConstants.ZIP_FILE_SEPARATOR, ".") + versionPkg + ".crk.Custom.Patch.apk"));
                                }
                                if (finalapk.exists() && listAppsFragment.getConfig().getInt("apkname", listAppsFragment.SETTINGS_VIEWSIZE_SMALL) != 0) {
                                    finalapk.renameTo(new File(folderstorage.getCanonicalFile() + InternalZipConstants.ZIP_FILE_SEPARATOR + pli.pkgName + versionPkg + ".crk.Custom.Patch.apk"));
                                }
                                listAppsFragment.this.runToMain(new C03243());
                                return;
                            }
                            listAppsFragment.this.runToMain(new C03221());
                        } catch (IOException localIOException2) {
                            localIOException2.printStackTrace();
                        } catch (Exception e2) {
                            e2.printStackTrace();
                        }
                    } else {
                        ApplicationInfo pli2 = Utils.getApkInfo(listAppsFragment.rebuldApk);
                        PackageInfo pkgInfo = Utils.getApkPackageInfo(listAppsFragment.rebuldApk);
                        versionPkg = BuildConfig.VERSION_NAME;
                        try {
                            versionPkg = ".v." + pkgInfo.versionName + ".b." + pkgInfo.versionCode;
                            listAppsFragment.supath = listAppsFragment.this.getContext().getPackageCodePath();
                            custom = listAppsFragment.customselect.toString();
                            listAppsFragment.str = BuildConfig.VERSION_NAME;
                            listAppsFragment.this.getSignatureKeys();
                            param = new String[listAppsFragment.CONTEXT_DIALOG];
                            param[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = pli2.packageName;
                            param[listAppsFragment.TITLE_LIST_ITEM] = custom;
                            param[listAppsFragment.SETTINGS_SORTBY_STATUS] = listAppsFragment.rebuldApk;
                            param[listAppsFragment.SETTINGS_SORTBY_TIME] = listAppsFragment.basepath;
                            param[listAppsFragment.Custom_PATCH_DIALOG] = listAppsFragment.toolfilesdir;
                            param[listAppsFragment.EXT_PATCH_DIALOG] = listAppsFragment.supath;
                            param[listAppsFragment.CHILD_NAME_SHEME_SELECT_ITEM] = "com.chelpus.root.utils";
                            cr_custom = new createapkcustom();
                            listAppsFragment.str = createapkcustom.main(param);
                            try {
                                signer = new ZipSignerLP();
                                cert = signer.readPublicKey(new File(listAppsFragment.basepath + "/Modified/Keys/" + "testkey.x509.pem").toURI().toURL());
                                sigBlockTemplate = signer.readContentAsBytes(new File(listAppsFragment.basepath + "/Modified/Keys/" + "testkey.sbt").toURI().toURL());
                                signer.setKeys("custom", cert, signer.readPrivateKey(new File(listAppsFragment.basepath + "/Modified/Keys/" + "testkey.pk8").toURI().toURL(), "lp"), sigBlockTemplate);
                                apkcrk = new File(listAppsFragment.basepath + "/Modified/" + pli2.packageName + ".apk");
                                add_files = new ArrayList();
                                if (createapkcustom.classesFiles != null && createapkcustom.classesFiles.size() > 0) {
                                    it = createapkcustom.classesFiles.iterator();
                                    while (it.hasNext()) {
                                        add_files.add(new AddFilesItem(((File) it.next()).getAbsolutePath(), listAppsFragment.basepath + "/Modified/"));
                                    }
                                }
                                signer.signZip(listAppsFragment.basepath + "/Modified/" + pli2.packageName + ".apk", listAppsFragment.basepath + "/Modified/" + pli2.packageName + ".crk.Custom.Patch.signed.apk", add_files);
                            } catch (Exception e22) {
                                System.out.println(e22);
                            } catch (IOException localIOException22) {
                                localIOException22.printStackTrace();
                            } catch (Throwable th2) {
                                apkcrk = new File(listAppsFragment.basepath + "/Modified/" + pli2.packageName + ".apk");
                                if (apkcrk.exists()) {
                                    apkcrk.delete();
                                }
                            }
                            if (apkcrk.exists()) {
                                apkcrk.delete();
                            }
                            apkcrk = new File(listAppsFragment.basepath + "/Modified/" + pli2.packageName + ".crk.Custom.Patch.signed.apk");
                            finalapk = new File(listAppsFragment.basepath + "/Modified/" + pli2.packageName + ".crk.Custom.Patch.apk");
                            if (apkcrk.exists() && finalapk.exists()) {
                                apkcrk.delete();
                            }
                            folderstorage = new File(listAppsFragment.basepath + "/Modified/" + Utils.getApkLabelName(listAppsFragment.rebuldApk).replace(" ", "."));
                            folderstorage.mkdirs();
                            if (!finalapk.exists() && listAppsFragment.getConfig().getInt("apkname", listAppsFragment.SETTINGS_VIEWSIZE_SMALL) == 0) {
                                apkcrk.renameTo(new File(folderstorage.getCanonicalFile() + InternalZipConstants.ZIP_FILE_SEPARATOR + Utils.getApkLabelName(listAppsFragment.rebuldApk).replace(" ", ".") + versionPkg + ".crk.Custom.Patch.apk"));
                            }
                            if (!(finalapk.exists() || listAppsFragment.getConfig().getInt("apkname", listAppsFragment.SETTINGS_VIEWSIZE_SMALL) == 0)) {
                                apkcrk.renameTo(new File(folderstorage.getCanonicalFile() + InternalZipConstants.ZIP_FILE_SEPARATOR + pli2.packageName + versionPkg + ".crk.Custom.Patch.apk"));
                            }
                            if (finalapk.exists() && listAppsFragment.getConfig().getInt("apkname", listAppsFragment.SETTINGS_VIEWSIZE_SMALL) == 0) {
                                finalapk.renameTo(new File(folderstorage.getCanonicalFile() + InternalZipConstants.ZIP_FILE_SEPARATOR + Utils.getApkLabelName(listAppsFragment.rebuldApk).replace(" ", ".") + versionPkg + ".crk.Custom.Patch.apk"));
                            }
                            if (finalapk.exists() && listAppsFragment.getConfig().getInt("apkname", listAppsFragment.SETTINGS_VIEWSIZE_SMALL) != 0) {
                                finalapk.renameTo(new File(folderstorage.getCanonicalFile() + InternalZipConstants.ZIP_FILE_SEPARATOR + pli2.packageName + versionPkg + ".crk.Custom.Patch.apk"));
                            }
                        } catch (IOException localIOException222) {
                            localIOException222.printStackTrace();
                        } catch (Exception e222) {
                            e222.printStackTrace();
                        }
                        listAppsFragment.this.runToMain(new C03254());
                        listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
                    }
                }
            });
        }
    }

    public void createapkads(String Patterns) {
        if (testSD(true)) {
            result = Patterns;
            runWithWait(new Runnable() {

                class C03261 implements Runnable {
                    C03261() {
                    }

                    public void run() {
                        listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.file_not_found));
                        listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                        listAppsFragment.this.vibrateEnd();
                    }
                }

                public void run() {
                    PkgListItem pli = listAppsFragment.pli;
                    String versionPkg = BuildConfig.VERSION_NAME;
                    try {
                        versionPkg = ".v." + listAppsFragment.getPkgMng().getPackageInfo(pli.pkgName, listAppsFragment.SETTINGS_VIEWSIZE_SMALL).versionName + ".b." + listAppsFragment.getPkgMng().getPackageInfo(pli.pkgName, listAppsFragment.SETTINGS_VIEWSIZE_SMALL).versionCode;
                        listAppsFragment.str = BuildConfig.VERSION_NAME;
                        listAppsFragment.this.getSignatureKeys();
                        String path = listAppsFragment.getPkgMng().getPackageInfo(pli.pkgName, listAppsFragment.SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir;
                        if (path.contains("/mnt/asec/")) {
                            Utils.remount(path.replace("/pkg.apk", BuildConfig.VERSION_NAME), InternalZipConstants.WRITE_MODE);
                        }
                        if (path.startsWith("/system/")) {
                            Utils.remount("/system", InternalZipConstants.WRITE_MODE);
                        }
                        Utils.run_all("chmod 644 " + path);
                        if (new File(path).exists()) {
                            File apkcrk;
                            ArrayList add_files;
                            Iterator it;
                            String[] param = new String[listAppsFragment.CREATEAPK_DIALOG];
                            param[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = pli.pkgName;
                            param[listAppsFragment.TITLE_LIST_ITEM] = listAppsFragment.result;
                            param[listAppsFragment.SETTINGS_SORTBY_STATUS] = path;
                            param[listAppsFragment.SETTINGS_SORTBY_TIME] = listAppsFragment.toolfilesdir;
                            param[listAppsFragment.Custom_PATCH_DIALOG] = "not_system";
                            param[listAppsFragment.EXT_PATCH_DIALOG] = listAppsFragment.basepath;
                            param[listAppsFragment.CHILD_NAME_SHEME_SELECT_ITEM] = "createAPK";
                            param[listAppsFragment.CONTEXT_DIALOG] = listAppsFragment.basepath + "/AdsBlockList.txt";
                            runpatchads patch = new runpatchads();
                            listAppsFragment.startUnderRoot = Boolean.valueOf(false);
                            runpatchads.main(param);
                            listAppsFragment.str = "runpatchads\n" + runpatchads.result;
                            ZipSignerLP signer = new ZipSignerLP();
                            X509Certificate cert = signer.readPublicKey(new File(listAppsFragment.basepath + "/Modified/Keys/" + "testkey.x509.pem").toURI().toURL());
                            byte[] sigBlockTemplate = signer.readContentAsBytes(new File(listAppsFragment.basepath + "/Modified/Keys/" + "testkey.sbt").toURI().toURL());
                            signer.setKeys("custom", cert, signer.readPrivateKey(new File(listAppsFragment.basepath + "/Modified/Keys/" + "testkey.pk8").toURI().toURL(), "lp"), sigBlockTemplate);
                            try {
                                apkcrk = new File(listAppsFragment.basepath + "/Modified/" + pli.pkgName + ".apk");
                                add_files = new ArrayList();
                                if (runpatchads.classesFiles != null && runpatchads.classesFiles.size() > 0) {
                                    it = runpatchads.classesFiles.iterator();
                                    while (it.hasNext()) {
                                        add_files.add(new AddFilesItem(((File) it.next()).getAbsolutePath(), listAppsFragment.basepath + "/Modified/"));
                                    }
                                }
                                signer.signZip(listAppsFragment.basepath + "/Modified/" + pli.pkgName + ".apk", listAppsFragment.basepath + "/Modified/" + pli.pkgName + ".crk.ADS.signed.apk", add_files);
                                if (apkcrk.exists()) {
                                    apkcrk.delete();
                                }
                            } catch (Throwable th) {
                                apkcrk = new File(listAppsFragment.basepath + "/Modified/" + pli.pkgName + ".apk");
                                if (apkcrk.exists()) {
                                    apkcrk.delete();
                                }
                            }
                            if (listAppsFragment.str.contains("compress classes.dex error")) {
                                String prefix = "crk.ADS";
                                try {
                                    ZipFile zipFile = new ZipFile(listAppsFragment.basepath + "/Modified/" + pli.pkgName + "." + prefix + ".signed.apk");
                                    ArrayList<File> filesToAdd = new ArrayList();
                                    if (runpatchads.classesFiles != null && runpatchads.classesFiles.size() > 0) {
                                        it = runpatchads.classesFiles.iterator();
                                        while (it.hasNext()) {
                                            filesToAdd.add((File) it.next());
                                        }
                                    }
                                    ZipParameters parameters = new ZipParameters();
                                    parameters.setCompressionMethod(listAppsFragment.SETTINGS_VIEWSIZE_SMALL);
                                    zipFile.addFiles(filesToAdd, parameters);
                                    new File(listAppsFragment.basepath + "/Modified/" + pli.pkgName + "." + prefix + ".signed.apk").renameTo(new File(listAppsFragment.basepath + "/Modified/" + pli.pkgName + ".apk"));
                                    try {
                                        apkcrk = new File(listAppsFragment.basepath + "/Modified/" + pli.pkgName + ".apk");
                                        add_files = new ArrayList();
                                        if (runpatchads.classesFiles != null && runpatchads.classesFiles.size() > 0) {
                                            it = runpatchads.classesFiles.iterator();
                                            while (it.hasNext()) {
                                                add_files.add(new AddFilesItem(((File) it.next()).getAbsolutePath(), listAppsFragment.basepath + "/Modified/"));
                                            }
                                        }
                                        signer.signZip(listAppsFragment.basepath + "/Modified/" + pli.pkgName + ".apk", listAppsFragment.basepath + "/Modified/" + pli.pkgName + "." + prefix + ".signed.apk", add_files);
                                        if (apkcrk.exists()) {
                                            apkcrk.delete();
                                        }
                                    } catch (Throwable th2) {
                                        apkcrk = new File(listAppsFragment.basepath + "/Modified/" + pli.pkgName + ".apk");
                                        if (apkcrk.exists()) {
                                            apkcrk.delete();
                                        }
                                    }
                                } catch (ZipException e) {
                                    System.out.println("compress classes.dex error" + e);
                                    e.printStackTrace();
                                } catch (Exception e2) {
                                    System.out.println(e2);
                                } catch (IOException localIOException) {
                                    localIOException.printStackTrace();
                                }
                            }
                            if (runpatchads.classesFiles != null && runpatchads.classesFiles.size() > 0) {
                                it = runpatchads.classesFiles.iterator();
                                while (it.hasNext()) {
                                    File cl = (File) it.next();
                                    if (cl.exists()) {
                                        cl.delete();
                                    }
                                }
                            }
                            apkcrk = new File(listAppsFragment.basepath + "/Modified/" + pli.pkgName + ".crk.ADS.signed.apk");
                            File finalapk = new File(listAppsFragment.basepath + "/Modified/" + pli.pkgName + ".crk.ADS.Removed.apk");
                            if (apkcrk.exists() && finalapk.exists()) {
                                apkcrk.delete();
                            }
                            File folderstorage = new File(listAppsFragment.basepath + "/Modified/" + pli.name.replaceAll(" ", ".").replaceAll(InternalZipConstants.ZIP_FILE_SEPARATOR, "."));
                            folderstorage.mkdirs();
                            if (!finalapk.exists() && listAppsFragment.getConfig().getInt("apkname", listAppsFragment.SETTINGS_VIEWSIZE_SMALL) == 0) {
                                apkcrk.renameTo(new File(folderstorage.getCanonicalFile() + InternalZipConstants.ZIP_FILE_SEPARATOR + pli.name.replaceAll(" ", ".").replaceAll(InternalZipConstants.ZIP_FILE_SEPARATOR, ".") + versionPkg + ".crk.ADS.Removed.apk"));
                            }
                            if (!(finalapk.exists() || listAppsFragment.getConfig().getInt("apkname", listAppsFragment.SETTINGS_VIEWSIZE_SMALL) == 0)) {
                                apkcrk.renameTo(new File(folderstorage.getCanonicalFile() + InternalZipConstants.ZIP_FILE_SEPARATOR + pli.pkgName + versionPkg + ".crk.ADS.Removed.apk"));
                            }
                            if (finalapk.exists() && listAppsFragment.getConfig().getInt("apkname", listAppsFragment.SETTINGS_VIEWSIZE_SMALL) == 0) {
                                finalapk.renameTo(new File(folderstorage.getCanonicalFile() + InternalZipConstants.ZIP_FILE_SEPARATOR + pli.name.replaceAll(" ", ".").replaceAll(InternalZipConstants.ZIP_FILE_SEPARATOR, ".") + versionPkg + ".crk.ADS.Removed.apk"));
                            }
                            if (finalapk.exists() && listAppsFragment.getConfig().getInt("apkname", listAppsFragment.SETTINGS_VIEWSIZE_SMALL) != 0) {
                                finalapk.renameTo(new File(folderstorage.getCanonicalFile() + InternalZipConstants.ZIP_FILE_SEPARATOR + pli.pkgName + versionPkg + ".crk.ADS.Removed.apk"));
                            }
                            final PkgListItem pkgListItem = pli;
                            listAppsFragment.this.runToMain(new Runnable() {
                                public void run() {
                                    listAppsFragment.removeDialogLP(listAppsFragment.CREATEAPK_ADS_DIALOG);
                                    listAppsFragment.pli = pkgListItem;
                                    listAppsFragment.showDialogLP(listAppsFragment.CREATEAPK_ADS_DIALOG);
                                    listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
                                    listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                                    if (listAppsFragment.getConfig().getBoolean("vibration", false)) {
                                        listAppsFragment.this.vib = (Vibrator) listAppsFragment.this.getContext().getSystemService("vibrator");
                                        listAppsFragment.this.vib.vibrate(50);
                                    }
                                }
                            });
                            return;
                        }
                        listAppsFragment.this.runToMain(new C03261());
                    } catch (IOException localIOException2) {
                        localIOException2.printStackTrace();
                    } catch (Exception e22) {
                        e22.printStackTrace();
                    }
                }
            });
        }
    }

    public void toolbar_createapkads(String Patterns) {
        result = Patterns;
        if (testSD(true)) {
            runWithWait(new Runnable() {

                class C03281 implements Runnable {
                    C03281() {
                    }

                    public void run() {
                        listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.unknown_error));
                        listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                        listAppsFragment.this.vibrateEnd();
                    }
                }

                class C03292 implements Runnable {
                    C03292() {
                    }

                    public void run() {
                        listAppsFragment.this.showMessage(Utils.getText(C0149R.string.rebuild_info), Utils.getText(C0149R.string.rebuild_message) + " " + listAppsFragment.rebuldApk);
                        listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                        listAppsFragment.this.vibrateEnd();
                    }
                }

                public void run() {
                    ApplicationInfo pli = Utils.getApkInfo(listAppsFragment.rebuldApk);
                    PackageInfo pkgInfo = Utils.getApkPackageInfo(listAppsFragment.rebuldApk);
                    String versionPkg = BuildConfig.VERSION_NAME;
                    try {
                        File apkcrk;
                        ArrayList add_files;
                        Iterator it;
                        versionPkg = ".v." + pkgInfo.versionName + ".b." + pkgInfo.versionCode;
                        listAppsFragment.str = BuildConfig.VERSION_NAME;
                        String[] param = new String[listAppsFragment.CREATEAPK_DIALOG];
                        param[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = pli.packageName;
                        param[listAppsFragment.TITLE_LIST_ITEM] = listAppsFragment.result;
                        param[listAppsFragment.SETTINGS_SORTBY_STATUS] = listAppsFragment.rebuldApk;
                        param[listAppsFragment.SETTINGS_SORTBY_TIME] = listAppsFragment.this.getContext().getFilesDir().toString();
                        param[listAppsFragment.Custom_PATCH_DIALOG] = "not_system";
                        param[listAppsFragment.EXT_PATCH_DIALOG] = listAppsFragment.basepath;
                        param[listAppsFragment.CHILD_NAME_SHEME_SELECT_ITEM] = "createAPK";
                        param[listAppsFragment.CONTEXT_DIALOG] = listAppsFragment.basepath + "/AdsBlockList.txt";
                        runpatchads.main(param);
                        listAppsFragment.startUnderRoot = Boolean.valueOf(false);
                        System.out.println("LuckyPatcher (rebuild): classes.dex patched.");
                        listAppsFragment.this.getSignatureKeys();
                        ZipSignerLP signer = new ZipSignerLP();
                        X509Certificate cert = signer.readPublicKey(new File(listAppsFragment.basepath + "/Modified/Keys/" + "testkey.x509.pem").toURI().toURL());
                        byte[] sigBlockTemplate = signer.readContentAsBytes(new File(listAppsFragment.basepath + "/Modified/Keys/" + "testkey.sbt").toURI().toURL());
                        signer.setKeys("custom", cert, signer.readPrivateKey(new File(listAppsFragment.basepath + "/Modified/Keys/" + "testkey.pk8").toURI().toURL(), "lp"), sigBlockTemplate);
                        try {
                            apkcrk = new File(listAppsFragment.basepath + "/Modified/" + pli.packageName + ".apk");
                            add_files = new ArrayList();
                            if (runpatchads.classesFiles != null && runpatchads.classesFiles.size() > 0) {
                                it = runpatchads.classesFiles.iterator();
                                while (it.hasNext()) {
                                    add_files.add(new AddFilesItem(((File) it.next()).getAbsolutePath(), listAppsFragment.basepath + "/Modified/"));
                                }
                            }
                            signer.signZip(listAppsFragment.basepath + "/Modified/" + pli.packageName + ".apk", listAppsFragment.basepath + "/Modified/" + pli.packageName + ".crk.ADS.signed.apk", add_files);
                            if (apkcrk.exists()) {
                                apkcrk.delete();
                            }
                        } catch (Throwable th) {
                            apkcrk = new File(listAppsFragment.basepath + "/Modified/" + pli.packageName + ".apk");
                            if (apkcrk.exists()) {
                                apkcrk.delete();
                            }
                        }
                        if (listAppsFragment.str.contains("compress classes.dex error")) {
                            String prefix = "crk.ADS";
                            try {
                                ZipFile zipFile = new ZipFile(listAppsFragment.basepath + "/Modified/" + pli.packageName + "." + prefix + ".signed.apk");
                                ArrayList<File> filesToAdd = new ArrayList();
                                if (runpatchads.classesFiles != null && runpatchads.classesFiles.size() > 0) {
                                    it = runpatchads.classesFiles.iterator();
                                    while (it.hasNext()) {
                                        filesToAdd.add((File) it.next());
                                    }
                                }
                                ZipParameters parameters = new ZipParameters();
                                parameters.setCompressionMethod(listAppsFragment.SETTINGS_VIEWSIZE_SMALL);
                                zipFile.addFiles(filesToAdd, parameters);
                                new File(listAppsFragment.basepath + "/Modified/" + pli.packageName + "." + prefix + ".signed.apk").renameTo(new File(listAppsFragment.basepath + "/Modified/" + pli.packageName + ".apk"));
                                try {
                                    apkcrk = new File(listAppsFragment.basepath + "/Modified/" + pli.packageName + ".apk");
                                    add_files = new ArrayList();
                                    if (runpatchads.classesFiles != null && runpatchads.classesFiles.size() > 0) {
                                        it = runpatchads.classesFiles.iterator();
                                        while (it.hasNext()) {
                                            add_files.add(new AddFilesItem(((File) it.next()).getAbsolutePath(), listAppsFragment.basepath + "/Modified/"));
                                        }
                                    }
                                    signer.signZip(listAppsFragment.basepath + "/Modified/" + pli.packageName + ".apk", listAppsFragment.basepath + "/Modified/" + pli.packageName + "." + prefix + ".signed.apk", add_files);
                                    if (apkcrk.exists()) {
                                        apkcrk.delete();
                                    }
                                } catch (Throwable th2) {
                                    apkcrk = new File(listAppsFragment.basepath + "/Modified/" + pli.packageName + ".apk");
                                    if (apkcrk.exists()) {
                                        apkcrk.delete();
                                    }
                                }
                            } catch (ZipException e) {
                                System.out.println("compress classes.dex error" + e);
                                e.printStackTrace();
                            } catch (Exception e2) {
                                System.out.println(e2);
                            }
                        }
                        if (runpatchads.classesFiles != null && runpatchads.classesFiles.size() > 0) {
                            it = runpatchads.classesFiles.iterator();
                            while (it.hasNext()) {
                                File cl = (File) it.next();
                                if (cl.exists()) {
                                    cl.delete();
                                }
                            }
                        }
                        apkcrk = new File(listAppsFragment.basepath + "/Modified/" + pli.packageName + ".crk.ADS.signed.apk");
                        File finalapk = new File(listAppsFragment.basepath + "/Modified/" + pli.packageName + ".crk.ADS.Removed.apk");
                        if (apkcrk.exists() && finalapk.exists()) {
                            apkcrk.delete();
                        }
                        File folderstorage = new File(listAppsFragment.basepath + "/Modified/" + Utils.getApkLabelName(listAppsFragment.rebuldApk).replaceAll(" ", "."));
                        folderstorage.mkdirs();
                        String resultfile = BuildConfig.VERSION_NAME;
                        if (!finalapk.exists() && listAppsFragment.getConfig().getInt("apkname", listAppsFragment.SETTINGS_VIEWSIZE_SMALL) == 0) {
                            resultfile = folderstorage.getCanonicalFile() + InternalZipConstants.ZIP_FILE_SEPARATOR + Utils.getApkLabelName(listAppsFragment.rebuldApk).replace(" ", ".") + versionPkg + ".crk.ADS.Removed.apk";
                            apkcrk.renameTo(new File(resultfile));
                        }
                        if (!(finalapk.exists() || listAppsFragment.getConfig().getInt("apkname", listAppsFragment.SETTINGS_VIEWSIZE_SMALL) == 0)) {
                            resultfile = folderstorage.getCanonicalFile() + InternalZipConstants.ZIP_FILE_SEPARATOR + pli.packageName + versionPkg + ".crk.ADS.Removed.apk";
                            apkcrk.renameTo(new File(resultfile));
                        }
                        if (finalapk.exists() && listAppsFragment.getConfig().getInt("apkname", listAppsFragment.SETTINGS_VIEWSIZE_SMALL) == 0) {
                            resultfile = folderstorage.getCanonicalFile() + InternalZipConstants.ZIP_FILE_SEPARATOR + Utils.getApkLabelName(listAppsFragment.rebuldApk).replace(" ", ".") + versionPkg + ".crk.ADS.Removed.apk";
                            finalapk.renameTo(new File(resultfile));
                        }
                        if (finalapk.exists() && listAppsFragment.getConfig().getInt("apkname", listAppsFragment.SETTINGS_VIEWSIZE_SMALL) != 0) {
                            resultfile = folderstorage.getCanonicalFile() + InternalZipConstants.ZIP_FILE_SEPARATOR + pli.packageName + versionPkg + ".crk.ADS.Removed.apk";
                            finalapk.renameTo(new File(resultfile));
                        }
                        listAppsFragment.rebuldApk = resultfile;
                        listAppsFragment.this.runToMain(new C03292());
                    } catch (Exception e3) {
                        listAppsFragment.this.runToMain(new C03281());
                    }
                }
            });
        }
    }

    public void cloneApplication() {
        if (testSD(true)) {
            runWithWait(new Runnable() {

                class C03301 implements Runnable {
                    C03301() {
                    }

                    public void run() {
                        listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.file_not_found));
                        listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                        listAppsFragment.this.vibrateEnd();
                    }
                }

                public void run() {
                    Exception e;
                    IOException localIOException;
                    File file;
                    PkgListItem pli = listAppsFragment.pli;
                    File resFile = null;
                    String versionPkg = BuildConfig.VERSION_NAME;
                    String tmp;
                    final PkgListItem pkgListItem;
                    final String str;
                    try {
                        versionPkg = ".v." + listAppsFragment.getPkgMng().getPackageInfo(pli.pkgName, listAppsFragment.SETTINGS_VIEWSIZE_SMALL).versionName + ".b." + listAppsFragment.getPkgMng().getPackageInfo(pli.pkgName, listAppsFragment.SETTINGS_VIEWSIZE_SMALL).versionCode;
                        listAppsFragment.str = BuildConfig.VERSION_NAME;
                        listAppsFragment.this.getSignatureKeys();
                        String path = listAppsFragment.getPkgMng().getPackageInfo(pli.pkgName, listAppsFragment.SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir;
                        if (path.contains("/mnt/asec/")) {
                            Utils.remount(path.replace("/pkg.apk", BuildConfig.VERSION_NAME), InternalZipConstants.WRITE_MODE);
                        }
                        if (path.startsWith("/system/")) {
                            Utils.remount("/system", InternalZipConstants.WRITE_MODE);
                        }
                        Utils.run_all("chmod 644 " + path);
                        if (new File(path).exists()) {
                            File apkcrk;
                            ArrayList add_files;
                            Iterator it;
                            File file2;
                            String[] param = new String[listAppsFragment.Custom_PATCH_DIALOG];
                            param[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = pli.pkgName;
                            param[listAppsFragment.TITLE_LIST_ITEM] = path;
                            param[listAppsFragment.SETTINGS_SORTBY_STATUS] = listAppsFragment.toolfilesdir;
                            param[listAppsFragment.SETTINGS_SORTBY_TIME] = listAppsFragment.basepath;
                            cloneApp clone = new cloneApp();
                            listAppsFragment.startUnderRoot = Boolean.valueOf(false);
                            cloneApp.main(param);
                            listAppsFragment.str = "CloneAppResult\n" + cloneApp.result;
                            ZipSignerLP signer = new ZipSignerLP();
                            X509Certificate cert = signer.readPublicKey(new File(listAppsFragment.basepath + "/Modified/Keys/" + "testkey.x509.pem").toURI().toURL());
                            byte[] sigBlockTemplate = signer.readContentAsBytes(new File(listAppsFragment.basepath + "/Modified/Keys/" + "testkey.sbt").toURI().toURL());
                            signer.setKeys("custom", cert, signer.readPrivateKey(new File(listAppsFragment.basepath + "/Modified/Keys/" + "testkey.pk8").toURI().toURL(), "lp"), sigBlockTemplate);
                            try {
                                apkcrk = new File(listAppsFragment.basepath + "/Modified/" + pli.pkgName + ".apk");
                                add_files = new ArrayList();
                                if (cloneApp.classesFiles != null && cloneApp.classesFiles.size() > 0) {
                                    it = cloneApp.classesFiles.iterator();
                                    while (it.hasNext()) {
                                        add_files.add(new AddFilesItem(((File) it.next()).getAbsolutePath(), listAppsFragment.basepath + "/Modified/"));
                                    }
                                }
                                if (new File(listAppsFragment.basepath + "/Modified/AndroidManifest.xml").exists()) {
                                    System.out.println("Found androidmanifest with changed target api");
                                    add_files.add(new AddFilesItem(listAppsFragment.basepath + "/Modified/AndroidManifest.xml", listAppsFragment.basepath + "/Modified/"));
                                }
                                signer.signZip(listAppsFragment.basepath + "/Modified/" + pli.pkgName + ".apk", listAppsFragment.basepath + "/Modified/" + pli.pkgName + ".Clone.signed.apk", add_files);
                                if (apkcrk.exists()) {
                                    apkcrk.delete();
                                }
                            } catch (Throwable th) {
                                apkcrk = new File(listAppsFragment.basepath + "/Modified/" + pli.pkgName + ".apk");
                                if (apkcrk.exists()) {
                                    apkcrk.delete();
                                }
                            }
                            if (listAppsFragment.str.contains("compress classes.dex error")) {
                                String prefix = "Clone";
                                try {
                                    ZipFile zipFile = new ZipFile(listAppsFragment.basepath + "/Modified/" + pli.pkgName + "." + prefix + ".signed.apk");
                                    ArrayList<File> filesToAdd = new ArrayList();
                                    if (cloneApp.classesFiles != null && cloneApp.classesFiles.size() > 0) {
                                        it = cloneApp.classesFiles.iterator();
                                        while (it.hasNext()) {
                                            filesToAdd.add((File) it.next());
                                        }
                                    }
                                    ZipParameters parameters = new ZipParameters();
                                    parameters.setCompressionMethod(listAppsFragment.SETTINGS_VIEWSIZE_SMALL);
                                    zipFile.addFiles(filesToAdd, parameters);
                                    new File(listAppsFragment.basepath + "/Modified/" + pli.pkgName + "." + prefix + ".signed.apk").renameTo(new File(listAppsFragment.basepath + "/Modified/" + pli.pkgName + ".apk"));
                                    try {
                                        apkcrk = new File(listAppsFragment.basepath + "/Modified/" + pli.pkgName + ".apk");
                                        add_files = new ArrayList();
                                        if (cloneApp.classesFiles != null && cloneApp.classesFiles.size() > 0) {
                                            it = cloneApp.classesFiles.iterator();
                                            while (it.hasNext()) {
                                                add_files.add(new AddFilesItem(((File) it.next()).getAbsolutePath(), listAppsFragment.basepath + "/Modified/"));
                                            }
                                        }
                                        signer.signZip(listAppsFragment.basepath + "/Modified/" + pli.pkgName + ".apk", listAppsFragment.basepath + "/Modified/" + pli.pkgName + "." + prefix + ".signed.apk", add_files);
                                        if (apkcrk.exists()) {
                                            apkcrk.delete();
                                        }
                                    } catch (Throwable th2) {
                                        apkcrk = new File(listAppsFragment.basepath + "/Modified/" + pli.pkgName + ".apk");
                                        if (apkcrk.exists()) {
                                            apkcrk.delete();
                                        }
                                    }
                                } catch (ZipException e2) {
                                    System.out.println("compress classes.dex error" + e2);
                                    e2.printStackTrace();
                                } catch (Exception e3) {
                                    System.out.println(e3);
                                } catch (IOException e4) {
                                    localIOException = e4;
                                    localIOException.printStackTrace();
                                    tmp = BuildConfig.VERSION_NAME;
                                    if (resFile != null) {
                                        tmp = resFile.getAbsolutePath();
                                    }
                                    pkgListItem = pli;
                                    str = tmp;
                                    listAppsFragment.this.runToMain(new Runnable() {

                                        class C03311 implements OnClickListener {
                                            C03311() {
                                            }

                                            public void onClick(DialogInterface dialog, int which) {
                                                listAppsFragment.patchAct.show_file_explorer(Utils.getDirs(new File(str)).getAbsolutePath());
                                            }
                                        }

                                        class C03322 implements OnClickListener {
                                            C03322() {
                                            }

                                            public void onClick(DialogInterface dialog, int which) {
                                                try {
                                                    listAppsFragment.this.toolbar_restore(new FileApkListItem(listAppsFragment.getInstance(), new File(str), false), false);
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                    listAppsFragment.this.showMessage(Utils.getText(C0149R.string.error), Utils.getText(C0149R.string.unknown_error));
                                                }
                                            }
                                        }

                                        class C03333 implements OnClickListener {
                                            C03333() {
                                            }

                                            public void onClick(DialogInterface dialog, int which) {
                                                listAppsFragment.patchAct.show_file_explorer(str);
                                            }
                                        }

                                        class C03344 implements OnClickListener {
                                            C03344() {
                                            }

                                            public void onClick(DialogInterface dialog, int which) {
                                                listAppsFragment.patchAct.show_file_explorer(str);
                                            }
                                        }

                                        public void run() {
                                            listAppsFragment.removeDialogLP(listAppsFragment.CREATEAPK_SUPPORT_DIALOG);
                                            listAppsFragment.pli = pkgListItem;
                                            String message = BuildConfig.VERSION_NAME;
                                            if (str.equals(BuildConfig.VERSION_NAME) || !new File(str).exists()) {
                                                new AlertDlg(listAppsFragment.frag.getContext()).setTitle(pkgListItem.name).setCancelable(true).setIcon(C0149R.drawable.ic_angel).setMessage(Utils.getColoredText(Utils.getText(C0149R.string.createdialog1) + " " + listAppsFragment.pli.name + Utils.getText(C0149R.string.createdialog4), -65451, "bold")).setPositiveButton(Utils.getText(C0149R.string.install_cloned_app), new C03344()).setNeutralButton(Utils.getText(C0149R.string.go_to_file_path), new C03333()).setNegativeButton(Utils.getText(C0149R.string.ok), null).create().show();
                                            } else {
                                                new AlertDlg(listAppsFragment.frag.getContext()).setTitle(pkgListItem.name).setCancelable(true).setIcon(C0149R.drawable.ic_angel).setMessage(Utils.getText(C0149R.string.createdialog1_clone) + " " + listAppsFragment.pli.name + " " + Utils.getText(C0149R.string.createdialog2) + LogCollector.LINE_SEPARATOR + Utils.getDirs(new File(str)) + "\n\n" + Utils.getText(C0149R.string.createdialog5) + LogCollector.LINE_SEPARATOR + new File(str).getName() + "\n\n" + Utils.getText(C0149R.string.createdialog3_clone)).setPositiveButton(Utils.getText(C0149R.string.install_cloned_app), new C03322()).setNeutralButton(Utils.getText(C0149R.string.go_to_file_path), new C03311()).setNegativeButton(Utils.getText(C0149R.string.ok), null).create().show();
                                            }
                                            listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
                                            listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                                            listAppsFragment.this.vibrateEnd();
                                        }
                                    });
                                }
                            }
                            if (cloneApp.classesFiles != null && cloneApp.classesFiles.size() > 0) {
                                it = cloneApp.classesFiles.iterator();
                                while (it.hasNext()) {
                                    File cl = (File) it.next();
                                    if (cl.exists()) {
                                        cl.delete();
                                    }
                                }
                            }
                            apkcrk = new File(listAppsFragment.basepath + "/Modified/" + pli.pkgName + ".Clone.signed.apk");
                            File finalapk = new File(listAppsFragment.basepath + "/Modified/" + pli.pkgName + ".Clone.apk");
                            if (apkcrk.exists() && finalapk.exists()) {
                                apkcrk.delete();
                            }
                            File folderstorage = new File(listAppsFragment.basepath + "/Modified/" + pli.name.replaceAll(" ", ".").replaceAll(InternalZipConstants.ZIP_FILE_SEPARATOR, "."));
                            folderstorage.mkdirs();
                            if (finalapk.exists() || listAppsFragment.getConfig().getInt("apkname", listAppsFragment.SETTINGS_VIEWSIZE_SMALL) != 0) {
                                file = null;
                            } else {
                                file2 = new File(folderstorage.getCanonicalFile() + InternalZipConstants.ZIP_FILE_SEPARATOR + pli.name.replaceAll(" ", ".").replaceAll(InternalZipConstants.ZIP_FILE_SEPARATOR, ".") + versionPkg + ".Clone.apk");
                                try {
                                    apkcrk.renameTo(file2);
                                } catch (IOException e5) {
                                    localIOException = e5;
                                    resFile = file;
                                    localIOException.printStackTrace();
                                    tmp = BuildConfig.VERSION_NAME;
                                    if (resFile != null) {
                                        tmp = resFile.getAbsolutePath();
                                    }
                                    pkgListItem = pli;
                                    str = tmp;
                                    listAppsFragment.this.runToMain(/* anonymous class already generated */);
                                } catch (Exception e6) {
                                    e3 = e6;
                                    resFile = file;
                                    e3.printStackTrace();
                                    tmp = BuildConfig.VERSION_NAME;
                                    if (resFile != null) {
                                        tmp = resFile.getAbsolutePath();
                                    }
                                    pkgListItem = pli;
                                    str = tmp;
                                    listAppsFragment.this.runToMain(/* anonymous class already generated */);
                                }
                            }
                            if (!(finalapk.exists() || listAppsFragment.getConfig().getInt("apkname", listAppsFragment.SETTINGS_VIEWSIZE_SMALL) == 0)) {
                                file2 = new File(folderstorage.getCanonicalFile() + InternalZipConstants.ZIP_FILE_SEPARATOR + pli.pkgName + versionPkg + ".Clone.apk");
                                apkcrk.renameTo(file2);
                                file = file2;
                            }
                            if (finalapk.exists() && listAppsFragment.getConfig().getInt("apkname", listAppsFragment.SETTINGS_VIEWSIZE_SMALL) == 0) {
                                file2 = new File(folderstorage.getCanonicalFile() + InternalZipConstants.ZIP_FILE_SEPARATOR + pli.name.replaceAll(" ", ".").replaceAll(InternalZipConstants.ZIP_FILE_SEPARATOR, ".") + versionPkg + ".Clone.apk");
                                finalapk.renameTo(file2);
                                file = file2;
                            }
                            if (!finalapk.exists() || listAppsFragment.getConfig().getInt("apkname", listAppsFragment.SETTINGS_VIEWSIZE_SMALL) == 0) {
                                resFile = file;
                                tmp = BuildConfig.VERSION_NAME;
                                if (resFile != null) {
                                    tmp = resFile.getAbsolutePath();
                                }
                                pkgListItem = pli;
                                str = tmp;
                                listAppsFragment.this.runToMain(/* anonymous class already generated */);
                            }
                            file2 = new File(folderstorage.getCanonicalFile() + InternalZipConstants.ZIP_FILE_SEPARATOR + pli.pkgName + versionPkg + ".Clone.apk");
                            finalapk.renameTo(file2);
                            tmp = BuildConfig.VERSION_NAME;
                            if (resFile != null) {
                                tmp = resFile.getAbsolutePath();
                            }
                            pkgListItem = pli;
                            str = tmp;
                            listAppsFragment.this.runToMain(/* anonymous class already generated */);
                        }
                        listAppsFragment.this.runToMain(new C03301());
                    } catch (IOException e42) {
                        localIOException = e42;
                        localIOException.printStackTrace();
                        tmp = BuildConfig.VERSION_NAME;
                        if (resFile != null) {
                            tmp = resFile.getAbsolutePath();
                        }
                        pkgListItem = pli;
                        str = tmp;
                        listAppsFragment.this.runToMain(/* anonymous class already generated */);
                    } catch (Exception e7) {
                        e3 = e7;
                        e3.printStackTrace();
                        tmp = BuildConfig.VERSION_NAME;
                        if (resFile != null) {
                            tmp = resFile.getAbsolutePath();
                        }
                        pkgListItem = pli;
                        str = tmp;
                        listAppsFragment.this.runToMain(/* anonymous class already generated */);
                    }
                }
            });
        }
    }

    public void toolbar_cloneApplication() {
        if (testSD(true)) {
            runWithWait(new Runnable() {

                class C03361 implements Runnable {
                    C03361() {
                    }

                    public void run() {
                        listAppsFragment.this.showMessage(Utils.getText(C0149R.string.rebuild_info), Utils.getText(C0149R.string.rebuild_message) + " " + listAppsFragment.rebuldApk);
                        listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                        listAppsFragment.this.vibrateEnd();
                    }
                }

                public void run() {
                    Exception e;
                    IOException localIOException;
                    File file;
                    File resFile = null;
                    ApplicationInfo pli = Utils.getApkInfo(listAppsFragment.rebuldApk);
                    PackageInfo pkgInfo = Utils.getApkPackageInfo(listAppsFragment.rebuldApk);
                    String versionPkg = BuildConfig.VERSION_NAME;
                    try {
                        File apkcrk;
                        ArrayList add_files;
                        Iterator it;
                        File file2;
                        versionPkg = ".v." + pkgInfo.versionName + ".b." + pkgInfo.versionCode;
                        listAppsFragment.str = BuildConfig.VERSION_NAME;
                        String[] param = new String[listAppsFragment.Custom_PATCH_DIALOG];
                        param[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = pkgInfo.packageName;
                        param[listAppsFragment.TITLE_LIST_ITEM] = listAppsFragment.rebuldApk;
                        param[listAppsFragment.SETTINGS_SORTBY_STATUS] = listAppsFragment.toolfilesdir;
                        param[listAppsFragment.SETTINGS_SORTBY_TIME] = listAppsFragment.basepath;
                        cloneApp clone = new cloneApp();
                        listAppsFragment.startUnderRoot = Boolean.valueOf(false);
                        cloneApp.main(param);
                        listAppsFragment.str = "CloneAppResult\n" + cloneApp.result;
                        ZipSignerLP signer = new ZipSignerLP();
                        X509Certificate cert = signer.readPublicKey(new File(listAppsFragment.basepath + "/Modified/Keys/" + "testkey.x509.pem").toURI().toURL());
                        byte[] sigBlockTemplate = signer.readContentAsBytes(new File(listAppsFragment.basepath + "/Modified/Keys/" + "testkey.sbt").toURI().toURL());
                        signer.setKeys("custom", cert, signer.readPrivateKey(new File(listAppsFragment.basepath + "/Modified/Keys/" + "testkey.pk8").toURI().toURL(), "lp"), sigBlockTemplate);
                        try {
                            apkcrk = new File(listAppsFragment.basepath + "/Modified/" + pli.packageName + ".apk");
                            add_files = new ArrayList();
                            if (cloneApp.classesFiles != null && cloneApp.classesFiles.size() > 0) {
                                it = cloneApp.classesFiles.iterator();
                                while (it.hasNext()) {
                                    add_files.add(new AddFilesItem(((File) it.next()).getAbsolutePath(), listAppsFragment.basepath + "/Modified/"));
                                }
                            }
                            if (new File(listAppsFragment.basepath + "/Modified/AndroidManifest.xml").exists()) {
                                System.out.println("Found androidmanifest with changed target api");
                                add_files.add(new AddFilesItem(listAppsFragment.basepath + "/Modified/AndroidManifest.xml", listAppsFragment.basepath + "/Modified/"));
                            }
                            signer.signZip(listAppsFragment.basepath + "/Modified/" + pli.packageName + ".apk", listAppsFragment.basepath + "/Modified/" + pli.packageName + ".Clone.signed.apk", add_files);
                            if (apkcrk.exists()) {
                                apkcrk.delete();
                            }
                        } catch (Throwable th) {
                            apkcrk = new File(listAppsFragment.basepath + "/Modified/" + pli.packageName + ".apk");
                            if (apkcrk.exists()) {
                                apkcrk.delete();
                            }
                        }
                        if (listAppsFragment.str.contains("compress classes.dex error")) {
                            String prefix = "Clone";
                            try {
                                ZipFile zipFile = new ZipFile(listAppsFragment.basepath + "/Modified/" + pli.packageName + "." + prefix + ".signed.apk");
                                ArrayList<File> filesToAdd = new ArrayList();
                                if (cloneApp.classesFiles != null && cloneApp.classesFiles.size() > 0) {
                                    it = cloneApp.classesFiles.iterator();
                                    while (it.hasNext()) {
                                        filesToAdd.add((File) it.next());
                                    }
                                }
                                ZipParameters parameters = new ZipParameters();
                                parameters.setCompressionMethod(listAppsFragment.SETTINGS_VIEWSIZE_SMALL);
                                zipFile.addFiles(filesToAdd, parameters);
                                new File(listAppsFragment.basepath + "/Modified/" + pli.packageName + "." + prefix + ".signed.apk").renameTo(new File(listAppsFragment.basepath + "/Modified/" + pli.packageName + ".apk"));
                                try {
                                    apkcrk = new File(listAppsFragment.basepath + "/Modified/" + pli.packageName + ".apk");
                                    add_files = new ArrayList();
                                    if (cloneApp.classesFiles != null && cloneApp.classesFiles.size() > 0) {
                                        it = cloneApp.classesFiles.iterator();
                                        while (it.hasNext()) {
                                            add_files.add(new AddFilesItem(((File) it.next()).getAbsolutePath(), listAppsFragment.basepath + "/Modified/"));
                                        }
                                    }
                                    signer.signZip(listAppsFragment.basepath + "/Modified/" + pli.packageName + ".apk", listAppsFragment.basepath + "/Modified/" + pli.packageName + "." + prefix + ".signed.apk", add_files);
                                    if (apkcrk.exists()) {
                                        apkcrk.delete();
                                    }
                                } catch (Throwable th2) {
                                    apkcrk = new File(listAppsFragment.basepath + "/Modified/" + pli.packageName + ".apk");
                                    if (apkcrk.exists()) {
                                        apkcrk.delete();
                                    }
                                }
                            } catch (ZipException e2) {
                                System.out.println("compress classes.dex error" + e2);
                                e2.printStackTrace();
                            } catch (Exception e3) {
                                System.out.println(e3);
                            } catch (IOException e4) {
                                localIOException = e4;
                                localIOException.printStackTrace();
                                listAppsFragment.rebuldApk = resFile.getAbsolutePath();
                                listAppsFragment.this.runToMain(new C03361());
                            }
                        }
                        if (cloneApp.classesFiles != null && cloneApp.classesFiles.size() > 0) {
                            it = cloneApp.classesFiles.iterator();
                            while (it.hasNext()) {
                                File cl = (File) it.next();
                                if (cl.exists()) {
                                    cl.delete();
                                }
                            }
                        }
                        apkcrk = new File(listAppsFragment.basepath + "/Modified/" + pli.packageName + ".Clone.signed.apk");
                        File finalapk = new File(listAppsFragment.basepath + "/Modified/" + pli.packageName + ".Clone.apk");
                        if (apkcrk.exists() && finalapk.exists()) {
                            apkcrk.delete();
                        }
                        File folderstorage = new File(listAppsFragment.basepath + "/Modified/" + Utils.getApkLabelName(listAppsFragment.rebuldApk).replaceAll(" ", ".").replaceAll(InternalZipConstants.ZIP_FILE_SEPARATOR, "."));
                        folderstorage.mkdirs();
                        if (finalapk.exists() || listAppsFragment.getConfig().getInt("apkname", listAppsFragment.SETTINGS_VIEWSIZE_SMALL) != 0) {
                            file = null;
                        } else {
                            file2 = new File(folderstorage.getCanonicalFile() + InternalZipConstants.ZIP_FILE_SEPARATOR + Utils.getApkLabelName(listAppsFragment.rebuldApk).replaceAll(" ", ".").replaceAll(InternalZipConstants.ZIP_FILE_SEPARATOR, ".") + versionPkg + ".Clone.apk");
                            try {
                                apkcrk.renameTo(file2);
                            } catch (IOException e5) {
                                localIOException = e5;
                                resFile = file;
                                localIOException.printStackTrace();
                                listAppsFragment.rebuldApk = resFile.getAbsolutePath();
                                listAppsFragment.this.runToMain(new C03361());
                            } catch (Exception e6) {
                                e3 = e6;
                                resFile = file;
                                e3.printStackTrace();
                                listAppsFragment.rebuldApk = resFile.getAbsolutePath();
                                listAppsFragment.this.runToMain(new C03361());
                            }
                        }
                        if (!(finalapk.exists() || listAppsFragment.getConfig().getInt("apkname", listAppsFragment.SETTINGS_VIEWSIZE_SMALL) == 0)) {
                            file2 = new File(folderstorage.getCanonicalFile() + InternalZipConstants.ZIP_FILE_SEPARATOR + pli.packageName + versionPkg + ".Clone.apk");
                            apkcrk.renameTo(file2);
                            file = file2;
                        }
                        if (finalapk.exists() && listAppsFragment.getConfig().getInt("apkname", listAppsFragment.SETTINGS_VIEWSIZE_SMALL) == 0) {
                            file2 = new File(folderstorage.getCanonicalFile() + InternalZipConstants.ZIP_FILE_SEPARATOR + Utils.getApkLabelName(listAppsFragment.rebuldApk).replaceAll(" ", ".").replaceAll(InternalZipConstants.ZIP_FILE_SEPARATOR, ".") + versionPkg + ".Clone.apk");
                            finalapk.renameTo(file2);
                            file = file2;
                        }
                        if (!finalapk.exists() || listAppsFragment.getConfig().getInt("apkname", listAppsFragment.SETTINGS_VIEWSIZE_SMALL) == 0) {
                            resFile = file;
                            listAppsFragment.rebuldApk = resFile.getAbsolutePath();
                            listAppsFragment.this.runToMain(new C03361());
                        }
                        file2 = new File(folderstorage.getCanonicalFile() + InternalZipConstants.ZIP_FILE_SEPARATOR + pli.packageName + versionPkg + ".Clone.apk");
                        finalapk.renameTo(file2);
                        listAppsFragment.rebuldApk = resFile.getAbsolutePath();
                        listAppsFragment.this.runToMain(new C03361());
                    } catch (IOException e42) {
                        localIOException = e42;
                        localIOException.printStackTrace();
                        listAppsFragment.rebuldApk = resFile.getAbsolutePath();
                        listAppsFragment.this.runToMain(new C03361());
                    } catch (Exception e7) {
                        e3 = e7;
                        e3.printStackTrace();
                        listAppsFragment.rebuldApk = resFile.getAbsolutePath();
                        listAppsFragment.this.runToMain(new C03361());
                    }
                }
            });
        }
    }

    public void createapksupport(String Patterns) {
        if (testSD(true)) {
            result = Patterns;
            runWithWait(new Runnable() {

                class C03371 implements Runnable {
                    C03371() {
                    }

                    public void run() {
                        listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.file_not_found));
                        listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                        listAppsFragment.this.vibrateEnd();
                    }
                }

                public void run() {
                    PkgListItem pli = listAppsFragment.pli;
                    String versionPkg = BuildConfig.VERSION_NAME;
                    try {
                        versionPkg = ".v." + listAppsFragment.getPkgMng().getPackageInfo(pli.pkgName, listAppsFragment.SETTINGS_VIEWSIZE_SMALL).versionName + ".b." + listAppsFragment.getPkgMng().getPackageInfo(pli.pkgName, listAppsFragment.SETTINGS_VIEWSIZE_SMALL).versionCode;
                        listAppsFragment.str = BuildConfig.VERSION_NAME;
                        listAppsFragment.this.getSignatureKeys();
                        String path = listAppsFragment.getPkgMng().getPackageInfo(pli.pkgName, listAppsFragment.SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir;
                        if (path.contains("/mnt/asec/")) {
                            Utils.remount(path.replace("/pkg.apk", BuildConfig.VERSION_NAME), InternalZipConstants.WRITE_MODE);
                        }
                        if (path.startsWith("/system/")) {
                            Utils.remount("/system", InternalZipConstants.WRITE_MODE);
                        }
                        Utils.run_all("chmod 644 " + path);
                        if (new File(path).exists()) {
                            File apkcrk;
                            ArrayList add_files;
                            Iterator it;
                            String[] param = new String[listAppsFragment.CONTEXT_DIALOG];
                            param[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = pli.pkgName;
                            param[listAppsFragment.TITLE_LIST_ITEM] = listAppsFragment.result;
                            param[listAppsFragment.SETTINGS_SORTBY_STATUS] = path;
                            param[listAppsFragment.SETTINGS_SORTBY_TIME] = listAppsFragment.toolfilesdir;
                            param[listAppsFragment.Custom_PATCH_DIALOG] = "not_system";
                            param[listAppsFragment.EXT_PATCH_DIALOG] = listAppsFragment.basepath;
                            param[listAppsFragment.CHILD_NAME_SHEME_SELECT_ITEM] = "createAPK";
                            runpatchsupportOld patch = new runpatchsupportOld();
                            listAppsFragment.startUnderRoot = Boolean.valueOf(false);
                            runpatchsupportOld.main(param);
                            listAppsFragment.str = "runpatchsupport\n" + runpatchsupportOld.result;
                            ZipSignerLP signer = new ZipSignerLP();
                            X509Certificate cert = signer.readPublicKey(new File(listAppsFragment.basepath + "/Modified/Keys/" + "testkey.x509.pem").toURI().toURL());
                            byte[] sigBlockTemplate = signer.readContentAsBytes(new File(listAppsFragment.basepath + "/Modified/Keys/" + "testkey.sbt").toURI().toURL());
                            signer.setKeys("custom", cert, signer.readPrivateKey(new File(listAppsFragment.basepath + "/Modified/Keys/" + "testkey.pk8").toURI().toURL(), "lp"), sigBlockTemplate);
                            try {
                                apkcrk = new File(listAppsFragment.basepath + "/Modified/" + pli.pkgName + ".apk");
                                add_files = new ArrayList();
                                if (runpatchsupportOld.classesFiles != null && runpatchsupportOld.classesFiles.size() > 0) {
                                    it = runpatchsupportOld.classesFiles.iterator();
                                    while (it.hasNext()) {
                                        add_files.add(new AddFilesItem(((File) it.next()).getAbsolutePath(), listAppsFragment.basepath + "/Modified/"));
                                    }
                                }
                                if (new File(listAppsFragment.basepath + "/Modified/AndroidManifest.xml").exists()) {
                                    System.out.println("Found androidmanifest with changed target api");
                                    add_files.add(new AddFilesItem(listAppsFragment.basepath + "/Modified/AndroidManifest.xml", listAppsFragment.basepath + "/Modified/"));
                                }
                                signer.signZip(listAppsFragment.basepath + "/Modified/" + pli.pkgName + ".apk", listAppsFragment.basepath + "/Modified/" + pli.pkgName + ".crk.Support.signed.apk", add_files);
                                if (apkcrk.exists()) {
                                    apkcrk.delete();
                                }
                            } catch (Throwable th) {
                                apkcrk = new File(listAppsFragment.basepath + "/Modified/" + pli.pkgName + ".apk");
                                if (apkcrk.exists()) {
                                    apkcrk.delete();
                                }
                            }
                            if (listAppsFragment.str.contains("compress classes.dex error")) {
                                String prefix = "crk.Support";
                                try {
                                    ZipFile zipFile = new ZipFile(listAppsFragment.basepath + "/Modified/" + pli.pkgName + "." + prefix + ".signed.apk");
                                    ArrayList<File> filesToAdd = new ArrayList();
                                    if (runpatchsupportOld.classesFiles != null && runpatchsupportOld.classesFiles.size() > 0) {
                                        it = runpatchsupportOld.classesFiles.iterator();
                                        while (it.hasNext()) {
                                            filesToAdd.add((File) it.next());
                                        }
                                    }
                                    ZipParameters parameters = new ZipParameters();
                                    parameters.setCompressionMethod(listAppsFragment.SETTINGS_VIEWSIZE_SMALL);
                                    zipFile.addFiles(filesToAdd, parameters);
                                    new File(listAppsFragment.basepath + "/Modified/" + pli.pkgName + "." + prefix + ".signed.apk").renameTo(new File(listAppsFragment.basepath + "/Modified/" + pli.pkgName + ".apk"));
                                    try {
                                        apkcrk = new File(listAppsFragment.basepath + "/Modified/" + pli.pkgName + ".apk");
                                        add_files = new ArrayList();
                                        if (runpatchsupportOld.classesFiles != null && runpatchsupportOld.classesFiles.size() > 0) {
                                            it = runpatchsupportOld.classesFiles.iterator();
                                            while (it.hasNext()) {
                                                add_files.add(new AddFilesItem(((File) it.next()).getAbsolutePath(), listAppsFragment.basepath + "/Modified/"));
                                            }
                                        }
                                        signer.signZip(listAppsFragment.basepath + "/Modified/" + pli.pkgName + ".apk", listAppsFragment.basepath + "/Modified/" + pli.pkgName + "." + prefix + ".signed.apk", add_files);
                                        if (apkcrk.exists()) {
                                            apkcrk.delete();
                                        }
                                    } catch (Throwable th2) {
                                        apkcrk = new File(listAppsFragment.basepath + "/Modified/" + pli.pkgName + ".apk");
                                        if (apkcrk.exists()) {
                                            apkcrk.delete();
                                        }
                                    }
                                } catch (ZipException e) {
                                    System.out.println("compress classes.dex error" + e);
                                    e.printStackTrace();
                                } catch (Exception e2) {
                                    System.out.println(e2);
                                } catch (IOException localIOException) {
                                    localIOException.printStackTrace();
                                }
                            }
                            if (runpatchsupportOld.classesFiles != null && runpatchsupportOld.classesFiles.size() > 0) {
                                it = runpatchsupportOld.classesFiles.iterator();
                                while (it.hasNext()) {
                                    File cl = (File) it.next();
                                    if (cl.exists()) {
                                        cl.delete();
                                    }
                                }
                            }
                            apkcrk = new File(listAppsFragment.basepath + "/Modified/" + pli.pkgName + ".crk.Support.signed.apk");
                            File finalapk = new File(listAppsFragment.basepath + "/Modified/" + pli.pkgName + ".crk.Support.apk");
                            if (apkcrk.exists() && finalapk.exists()) {
                                apkcrk.delete();
                            }
                            File folderstorage = new File(listAppsFragment.basepath + "/Modified/" + pli.name.replaceAll(" ", ".").replaceAll(InternalZipConstants.ZIP_FILE_SEPARATOR, "."));
                            folderstorage.mkdirs();
                            if (!finalapk.exists() && listAppsFragment.getConfig().getInt("apkname", listAppsFragment.SETTINGS_VIEWSIZE_SMALL) == 0) {
                                apkcrk.renameTo(new File(folderstorage.getCanonicalFile() + InternalZipConstants.ZIP_FILE_SEPARATOR + pli.name.replaceAll(" ", ".").replaceAll(InternalZipConstants.ZIP_FILE_SEPARATOR, ".") + versionPkg + ".crk.Support.apk"));
                            }
                            if (!(finalapk.exists() || listAppsFragment.getConfig().getInt("apkname", listAppsFragment.SETTINGS_VIEWSIZE_SMALL) == 0)) {
                                apkcrk.renameTo(new File(folderstorage.getCanonicalFile() + InternalZipConstants.ZIP_FILE_SEPARATOR + pli.pkgName + versionPkg + ".crk.Support.apk"));
                            }
                            if (finalapk.exists() && listAppsFragment.getConfig().getInt("apkname", listAppsFragment.SETTINGS_VIEWSIZE_SMALL) == 0) {
                                finalapk.renameTo(new File(folderstorage.getCanonicalFile() + InternalZipConstants.ZIP_FILE_SEPARATOR + pli.name.replaceAll(" ", ".").replaceAll(InternalZipConstants.ZIP_FILE_SEPARATOR, ".") + versionPkg + ".crk.Support.apk"));
                            }
                            if (finalapk.exists() && listAppsFragment.getConfig().getInt("apkname", listAppsFragment.SETTINGS_VIEWSIZE_SMALL) != 0) {
                                finalapk.renameTo(new File(folderstorage.getCanonicalFile() + InternalZipConstants.ZIP_FILE_SEPARATOR + pli.pkgName + versionPkg + ".crk.Support.apk"));
                            }
                            final PkgListItem pkgListItem = pli;
                            listAppsFragment.this.runToMain(new Runnable() {
                                public void run() {
                                    listAppsFragment.removeDialogLP(listAppsFragment.CREATEAPK_SUPPORT_DIALOG);
                                    listAppsFragment.pli = pkgListItem;
                                    listAppsFragment.showDialogLP(listAppsFragment.CREATEAPK_SUPPORT_DIALOG);
                                    listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
                                    listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                                    listAppsFragment.this.vibrateEnd();
                                }
                            });
                            return;
                        }
                        listAppsFragment.this.runToMain(new C03371());
                    } catch (IOException localIOException2) {
                        localIOException2.printStackTrace();
                    } catch (Exception e22) {
                        e22.printStackTrace();
                    }
                }
            });
        }
    }

    public void toolbar_createapksupport(String Patterns) {
        result = Patterns;
        if (testSD(true)) {
            runWithWait(new Runnable() {

                class C03401 implements Runnable {
                    C03401() {
                    }

                    public void run() {
                        listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.unknown_error));
                        listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                        listAppsFragment.this.vibrateEnd();
                    }
                }

                class C03412 implements Runnable {
                    C03412() {
                    }

                    public void run() {
                        listAppsFragment.this.showMessage(Utils.getText(C0149R.string.rebuild_info), Utils.getText(C0149R.string.rebuild_message) + " " + listAppsFragment.rebuldApk);
                        listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                        listAppsFragment.this.vibrateEnd();
                    }
                }

                public void run() {
                    ApplicationInfo pli = Utils.getApkInfo(listAppsFragment.rebuldApk);
                    PackageInfo pkgInfo = Utils.getApkPackageInfo(listAppsFragment.rebuldApk);
                    String versionPkg = BuildConfig.VERSION_NAME;
                    try {
                        File apkcrk;
                        ArrayList add_files;
                        Iterator it;
                        versionPkg = ".v." + pkgInfo.versionName + ".b." + pkgInfo.versionCode;
                        listAppsFragment.str = BuildConfig.VERSION_NAME;
                        String[] param = new String[listAppsFragment.CONTEXT_DIALOG];
                        param[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = pli.packageName;
                        param[listAppsFragment.TITLE_LIST_ITEM] = listAppsFragment.result;
                        param[listAppsFragment.SETTINGS_SORTBY_STATUS] = listAppsFragment.rebuldApk;
                        param[listAppsFragment.SETTINGS_SORTBY_TIME] = listAppsFragment.this.getContext().getFilesDir().toString();
                        param[listAppsFragment.Custom_PATCH_DIALOG] = "not_system";
                        param[listAppsFragment.EXT_PATCH_DIALOG] = listAppsFragment.basepath;
                        param[listAppsFragment.CHILD_NAME_SHEME_SELECT_ITEM] = "createAPK";
                        runpatchsupportOld.main(param);
                        listAppsFragment.startUnderRoot = Boolean.valueOf(false);
                        System.out.println("LuckyPatcher (rebuild): classes.dex patched.");
                        listAppsFragment.this.getSignatureKeys();
                        ZipSignerLP signer = new ZipSignerLP();
                        X509Certificate cert = signer.readPublicKey(new File(listAppsFragment.basepath + "/Modified/Keys/" + "testkey.x509.pem").toURI().toURL());
                        byte[] sigBlockTemplate = signer.readContentAsBytes(new File(listAppsFragment.basepath + "/Modified/Keys/" + "testkey.sbt").toURI().toURL());
                        signer.setKeys("custom", cert, signer.readPrivateKey(new File(listAppsFragment.basepath + "/Modified/Keys/" + "testkey.pk8").toURI().toURL(), "lp"), sigBlockTemplate);
                        try {
                            apkcrk = new File(listAppsFragment.basepath + "/Modified/" + pli.packageName + ".apk");
                            add_files = new ArrayList();
                            if (runpatchsupportOld.classesFiles != null && runpatchsupportOld.classesFiles.size() > 0) {
                                it = runpatchsupportOld.classesFiles.iterator();
                                while (it.hasNext()) {
                                    add_files.add(new AddFilesItem(((File) it.next()).getAbsolutePath(), listAppsFragment.basepath + "/Modified/"));
                                }
                            }
                            if (new File(listAppsFragment.basepath + "/Modified/AndroidManifest.xml").exists()) {
                                System.out.println("Found androidmanifest with changed target api");
                                add_files.add(new AddFilesItem(listAppsFragment.basepath + "/Modified/AndroidManifest.xml", listAppsFragment.basepath + "/Modified/"));
                            }
                            signer.signZip(listAppsFragment.basepath + "/Modified/" + pli.packageName + ".apk", listAppsFragment.basepath + "/Modified/" + pli.packageName + ".crk.Support.signed.apk", add_files);
                            if (apkcrk.exists()) {
                                apkcrk.delete();
                            }
                        } catch (Throwable th) {
                            apkcrk = new File(listAppsFragment.basepath + "/Modified/" + pli.packageName + ".apk");
                            if (apkcrk.exists()) {
                                apkcrk.delete();
                            }
                        }
                        if (listAppsFragment.str.contains("compress classes.dex error")) {
                            String prefix = "crk.Support";
                            try {
                                ZipFile zipFile = new ZipFile(listAppsFragment.basepath + "/Modified/" + pli.packageName + "." + prefix + ".signed.apk");
                                ArrayList<File> filesToAdd = new ArrayList();
                                if (runpatchsupportOld.classesFiles != null && runpatchsupportOld.classesFiles.size() > 0) {
                                    it = runpatchsupportOld.classesFiles.iterator();
                                    while (it.hasNext()) {
                                        filesToAdd.add((File) it.next());
                                    }
                                }
                                ZipParameters parameters = new ZipParameters();
                                parameters.setCompressionMethod(listAppsFragment.SETTINGS_VIEWSIZE_SMALL);
                                zipFile.addFiles(filesToAdd, parameters);
                                new File(listAppsFragment.basepath + "/Modified/" + pli.packageName + "." + prefix + ".signed.apk").renameTo(new File(listAppsFragment.basepath + "/Modified/" + pli.packageName + ".apk"));
                                try {
                                    apkcrk = new File(listAppsFragment.basepath + "/Modified/" + pli.packageName + ".apk");
                                    add_files = new ArrayList();
                                    if (runpatchsupportOld.classesFiles != null && runpatchsupportOld.classesFiles.size() > 0) {
                                        it = runpatchsupportOld.classesFiles.iterator();
                                        while (it.hasNext()) {
                                            add_files.add(new AddFilesItem(((File) it.next()).getAbsolutePath(), listAppsFragment.basepath + "/Modified/"));
                                        }
                                    }
                                    signer.signZip(listAppsFragment.basepath + "/Modified/" + pli.packageName + ".apk", listAppsFragment.basepath + "/Modified/" + pli.packageName + "." + prefix + ".signed.apk", add_files);
                                    if (apkcrk.exists()) {
                                        apkcrk.delete();
                                    }
                                } catch (Throwable th2) {
                                    apkcrk = new File(listAppsFragment.basepath + "/Modified/" + pli.packageName + ".apk");
                                    if (apkcrk.exists()) {
                                        apkcrk.delete();
                                    }
                                }
                            } catch (ZipException e) {
                                System.out.println("compress classes.dex error" + e);
                                e.printStackTrace();
                            } catch (Exception e2) {
                                System.out.println(e2);
                            }
                        }
                        if (runpatchsupportOld.classesFiles != null && runpatchsupportOld.classesFiles.size() > 0) {
                            it = runpatchsupportOld.classesFiles.iterator();
                            while (it.hasNext()) {
                                File cl = (File) it.next();
                                if (cl.exists()) {
                                    cl.delete();
                                }
                            }
                        }
                        apkcrk = new File(listAppsFragment.basepath + "/Modified/" + pli.packageName + ".crk.Support.signed.apk");
                        File finalapk = new File(listAppsFragment.basepath + "/Modified/" + pli.packageName + ".crk.Support.apk");
                        if (apkcrk.exists() && finalapk.exists()) {
                            apkcrk.delete();
                        }
                        File folderstorage = new File(listAppsFragment.basepath + "/Modified/" + Utils.getApkLabelName(listAppsFragment.rebuldApk).replaceAll(" ", "."));
                        folderstorage.mkdirs();
                        String resultfile = BuildConfig.VERSION_NAME;
                        if (!finalapk.exists() && listAppsFragment.getConfig().getInt("apkname", listAppsFragment.SETTINGS_VIEWSIZE_SMALL) == 0) {
                            resultfile = folderstorage.getCanonicalFile() + InternalZipConstants.ZIP_FILE_SEPARATOR + Utils.getApkLabelName(listAppsFragment.rebuldApk).replace(" ", ".") + versionPkg + ".crk.Support.apk";
                            apkcrk.renameTo(new File(resultfile));
                        }
                        if (!(finalapk.exists() || listAppsFragment.getConfig().getInt("apkname", listAppsFragment.SETTINGS_VIEWSIZE_SMALL) == 0)) {
                            resultfile = folderstorage.getCanonicalFile() + InternalZipConstants.ZIP_FILE_SEPARATOR + pli.packageName + versionPkg + ".crk.Support.apk";
                            apkcrk.renameTo(new File(resultfile));
                        }
                        if (finalapk.exists() && listAppsFragment.getConfig().getInt("apkname", listAppsFragment.SETTINGS_VIEWSIZE_SMALL) == 0) {
                            resultfile = folderstorage.getCanonicalFile() + InternalZipConstants.ZIP_FILE_SEPARATOR + Utils.getApkLabelName(listAppsFragment.rebuldApk).replace(" ", ".") + versionPkg + ".crk.Support.apk";
                            finalapk.renameTo(new File(resultfile));
                        }
                        if (finalapk.exists() && listAppsFragment.getConfig().getInt("apkname", listAppsFragment.SETTINGS_VIEWSIZE_SMALL) != 0) {
                            resultfile = folderstorage.getCanonicalFile() + InternalZipConstants.ZIP_FILE_SEPARATOR + pli.packageName + versionPkg + ".crk.Support.apk";
                            finalapk.renameTo(new File(resultfile));
                        }
                        listAppsFragment.rebuldApk = resultfile;
                        listAppsFragment.this.runToMain(new C03412());
                    } catch (Exception e3) {
                        listAppsFragment.this.runToMain(new C03401());
                    }
                }
            });
        }
    }

    public void vibrateEnd() {
        if (getConfig().getBoolean("vibration", false)) {
            this.vib = (Vibrator) getInstance().getSystemService("vibrator");
            this.vib.vibrate(2500);
        }
    }

    public void bootadd(PkgListItem pli, String option) {
        int i;
        StringBuilder sbi = new StringBuilder();
        File bootfile = new File(getContext().getDir("bootlist", SETTINGS_VIEWSIZE_SMALL) + "/bootlist");
        if (option.contains("ads")) {
            pli.boot_ads = true;
            sbi = new StringBuilder();
            i = SETTINGS_VIEWSIZE_SMALL;
            while (i < plia.data.length) {
                if (plia.data[i].boot_ads || plia.data[i].boot_custom || plia.data[i].boot_lvl || plia.data[i].boot_manual) {
                    sbi.append(plia.data[i].pkgName);
                    if (plia.data[i].boot_ads) {
                        sbi.append("%ads");
                    }
                    if (plia.data[i].boot_lvl) {
                        sbi.append("%lvl");
                    }
                    if (plia.data[i].boot_custom) {
                        sbi.append("%custom");
                    }
                    if (plia.data[i].boot_manual) {
                        sbi.append("%object");
                    }
                    sbi.append(",");
                }
                i += TITLE_LIST_ITEM;
            }
            sbi.append(LogCollector.LINE_SEPARATOR);
            try {
                FileOutputStream fos = new FileOutputStream(bootfile);
                fos.write(sbi.toString().getBytes());
                fos.close();
            } catch (FileNotFoundException e) {
                Toast.makeText(getContext(), "Error: bootlist file are not found!", TITLE_LIST_ITEM).show();
            } catch (Exception e2) {
                Toast.makeText(getContext(), BuildConfig.VERSION_NAME + e2, TITLE_LIST_ITEM).show();
            }
            plia.notifyDataSetChanged(pli);
        }
        if (option.contains("lvl")) {
            pli.boot_lvl = true;
            sbi = new StringBuilder();
            i = SETTINGS_VIEWSIZE_SMALL;
            while (i < plia.data.length) {
                if (plia.data[i].boot_ads || plia.data[i].boot_custom || plia.data[i].boot_lvl || plia.data[i].boot_manual) {
                    sbi.append(plia.data[i].pkgName);
                    if (plia.data[i].boot_ads) {
                        sbi.append("%ads");
                    }
                    if (plia.data[i].boot_lvl) {
                        sbi.append("%lvl");
                    }
                    if (plia.data[i].boot_custom) {
                        sbi.append("%custom");
                    }
                    if (plia.data[i].boot_manual) {
                        sbi.append("%object");
                    }
                    sbi.append(",");
                }
                i += TITLE_LIST_ITEM;
            }
            sbi.append(LogCollector.LINE_SEPARATOR);
            try {
                fos = new FileOutputStream(bootfile);
                fos.write(sbi.toString().getBytes());
                fos.close();
            } catch (FileNotFoundException e3) {
                Toast.makeText(getContext(), "Error: bootlist file are not found!", TITLE_LIST_ITEM).show();
            } catch (Exception e22) {
                Toast.makeText(getContext(), BuildConfig.VERSION_NAME + e22, TITLE_LIST_ITEM).show();
            }
            plia.notifyDataSetChanged(pli);
        }
        if (option.contains("custom")) {
            pli.boot_custom = true;
            File custom = customselect;
            if (custom != null) {
                try {
                    Utils.copyFile(custom, new File(getContext().getDir("bootlist", SETTINGS_VIEWSIZE_SMALL) + InternalZipConstants.ZIP_FILE_SEPARATOR + pli.pkgName));
                } catch (Exception e4) {
                }
                plia.notifyDataSetChanged(pli);
            }
        }
        removeDialogLP(SETTINGS_SORTBY_TIME);
        refresh_boot();
        adapter_boot = new BootListItemAdapter(getContext(), C0149R.layout.bootlistitemview, getConfig().getInt(SETTINGS_VIEWSIZE, SETTINGS_VIEWSIZE_SMALL), boot_pat);
        adapter_boot.notifyDataSetChanged();
    }

    public void disable_permission(PkgListItem plis, Perm permission) {
        Element e;
        String sharedUserId = null;
        if (new File(getContext().getDir("packages", SETTINGS_VIEWSIZE_SMALL).getAbsolutePath() + "/packages1.xml").exists()) {
            File file;
            Utils utils;
            try {
                String readTarget = getContext().getDir("packages", SETTINGS_VIEWSIZE_SMALL).getAbsolutePath() + "/packages1.xml";
                String writeTarget = getContext().getDir("packages", SETTINGS_VIEWSIZE_SMALL).getAbsolutePath() + "/packages.xml";
                Utils utils2 = new Utils(BuildConfig.VERSION_NAME);
                Utils.run_all("chmod 777 " + readTarget);
                utils2 = new Utils(BuildConfig.VERSION_NAME);
                Utils.run_all("chmod 777 " + writeTarget);
                try {
                    Node node;
                    NodeList pkgParams;
                    int j;
                    Node nodeC;
                    NodeList perms;
                    int k;
                    Element per;
                    Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new FileInputStream(readTarget));
                    NodeList packages = doc.getElementsByTagName(Common.PACKAGE);
                    int i = SETTINGS_VIEWSIZE_SMALL;
                    while (i < packages.getLength()) {
                        node = packages.item(i);
                        e = (Element) packages.item(i);
                        if (e.getAttribute("name").equals(pli.pkgName)) {
                            if (!e.getAttribute("sharedUserId").isEmpty()) {
                                sharedUserId = e.getAttribute("sharedUserId");
                            }
                            pkgParams = node.getChildNodes();
                            for (j = SETTINGS_VIEWSIZE_SMALL; j < pkgParams.getLength(); j += TITLE_LIST_ITEM) {
                                nodeC = pkgParams.item(j);
                                System.out.println("node:" + nodeC.getNodeName());
                                if (nodeC.getNodeName().equals("perms")) {
                                    perms = nodeC.getChildNodes();
                                    for (k = SETTINGS_VIEWSIZE_SMALL; k < perms.getLength(); k += TITLE_LIST_ITEM) {
                                        System.out.println("perm0:" + perms.item(k).getNodeName());
                                        if (perms.item(k).getNodeName().equals("item")) {
                                            per = (Element) perms.item(k);
                                            if (per.getAttribute("name").equals(permission.Name)) {
                                                if (per.getAttribute("name").contains("chelpa.disabled.")) {
                                                    perms.item(k).getAttributes().getNamedItem("name").setTextContent(per.getAttribute("name").replaceAll("chelpa.disabled.", BuildConfig.VERSION_NAME));
                                                } else {
                                                    perms.item(k).getAttributes().getNamedItem("name").setTextContent("chelpa.disabled." + per.getAttribute("name"));
                                                }
                                                if (!changedPermissions.contains(permission.Name)) {
                                                    changedPermissions.add(permission.Name);
                                                }
                                            }
                                        }
                                    }
                                }
                                if (nodeC.getNodeName().equals("disabled-components")) {
                                    perms = nodeC.getChildNodes();
                                    for (k = SETTINGS_VIEWSIZE_SMALL; k < perms.getLength(); k += TITLE_LIST_ITEM) {
                                        System.out.println("perm0:" + perms.item(k).getNodeName());
                                        if (perms.item(k).getNodeName().equals("item")) {
                                            per = (Element) perms.item(k);
                                            if (per.getAttribute("name").equals(permission.Name)) {
                                                if (per.getAttribute("name").contains("chelpa.disabled.")) {
                                                    perms.item(k).getAttributes().getNamedItem("name").setTextContent(per.getAttribute("name").replaceAll("chelpa.disabled.", BuildConfig.VERSION_NAME));
                                                } else {
                                                    perms.item(k).getAttributes().getNamedItem("name").setTextContent("chelpa.disabled." + per.getAttribute("name"));
                                                }
                                                if (!changedPermissions.contains(permission.Name)) {
                                                    changedPermissions.add(permission.Name);
                                                }
                                            }
                                        }
                                    }
                                }
                                if (nodeC.getNodeName().equals("enabled-components")) {
                                    perms = nodeC.getChildNodes();
                                    for (k = SETTINGS_VIEWSIZE_SMALL; k < perms.getLength(); k += TITLE_LIST_ITEM) {
                                        System.out.println("perm0:" + perms.item(k).getNodeName());
                                        if (perms.item(k).getNodeName().equals("item")) {
                                            per = (Element) perms.item(k);
                                            if (per.getAttribute("name").equals(permission.Name)) {
                                                if (per.getAttribute("name").contains("chelpa.disabled.")) {
                                                    perms.item(k).getAttributes().getNamedItem("name").setTextContent(per.getAttribute("name").replaceAll("chelpa.disabled.", BuildConfig.VERSION_NAME));
                                                } else {
                                                    perms.item(k).getAttributes().getNamedItem("name").setTextContent("chelpa.disabled." + per.getAttribute("name"));
                                                }
                                                if (!changedPermissions.contains(permission.Name)) {
                                                    changedPermissions.add(permission.Name);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        } else {
                            i += TITLE_LIST_ITEM;
                        }
                    }
                    packages = doc.getElementsByTagName("updated-package");
                    for (i = SETTINGS_VIEWSIZE_SMALL; i < packages.getLength(); i += TITLE_LIST_ITEM) {
                        node = packages.item(i);
                        e = (Element) packages.item(i);
                        if (e.getAttribute("name").equals(pli.pkgName)) {
                            if (!e.getAttribute("sharedUserId").isEmpty()) {
                                sharedUserId = e.getAttribute("sharedUserId");
                            }
                            pkgParams = node.getChildNodes();
                            for (j = SETTINGS_VIEWSIZE_SMALL; j < pkgParams.getLength(); j += TITLE_LIST_ITEM) {
                                nodeC = pkgParams.item(j);
                                System.out.println("node:" + nodeC.getNodeName());
                                if (nodeC.getNodeName().equals("perms")) {
                                    perms = nodeC.getChildNodes();
                                    for (k = SETTINGS_VIEWSIZE_SMALL; k < perms.getLength(); k += TITLE_LIST_ITEM) {
                                        System.out.println("perm0:" + perms.item(k).getNodeName());
                                        if (perms.item(k).getNodeName().equals("item")) {
                                            per = (Element) perms.item(k);
                                            if (per.getAttribute("name").equals(permission.Name)) {
                                                if (per.getAttribute("name").contains("chelpa.disabled.")) {
                                                    perms.item(k).getAttributes().getNamedItem("name").setTextContent(per.getAttribute("name").replaceAll("chelpa.disabled.", BuildConfig.VERSION_NAME));
                                                } else {
                                                    perms.item(k).getAttributes().getNamedItem("name").setTextContent("chelpa.disabled." + per.getAttribute("name"));
                                                }
                                                if (!changedPermissions.contains(permission.Name)) {
                                                    changedPermissions.add(permission.Name);
                                                }
                                            }
                                        }
                                    }
                                }
                                if (nodeC.getNodeName().equals("disabled-components")) {
                                    perms = nodeC.getChildNodes();
                                    for (k = SETTINGS_VIEWSIZE_SMALL; k < perms.getLength(); k += TITLE_LIST_ITEM) {
                                        System.out.println("perm0:" + perms.item(k).getNodeName());
                                        if (perms.item(k).getNodeName().equals("item")) {
                                            per = (Element) perms.item(k);
                                            if (per.getAttribute("name").equals(permission.Name)) {
                                                if (per.getAttribute("name").contains("chelpa.disabled.")) {
                                                    perms.item(k).getAttributes().getNamedItem("name").setTextContent(per.getAttribute("name").replaceAll("chelpa.disabled.", BuildConfig.VERSION_NAME));
                                                } else {
                                                    perms.item(k).getAttributes().getNamedItem("name").setTextContent("chelpa.disabled." + per.getAttribute("name"));
                                                }
                                                if (!changedPermissions.contains(permission.Name)) {
                                                    changedPermissions.add(permission.Name);
                                                }
                                            }
                                        }
                                    }
                                }
                                if (nodeC.getNodeName().equals("enabled-components")) {
                                    perms = nodeC.getChildNodes();
                                    for (k = SETTINGS_VIEWSIZE_SMALL; k < perms.getLength(); k += TITLE_LIST_ITEM) {
                                        System.out.println("perm0:" + perms.item(k).getNodeName());
                                        if (perms.item(k).getNodeName().equals("item")) {
                                            per = (Element) perms.item(k);
                                            if (per.getAttribute("name").equals(permission.Name)) {
                                                if (per.getAttribute("name").contains("chelpa.disabled.")) {
                                                    perms.item(k).getAttributes().getNamedItem("name").setTextContent(per.getAttribute("name").replaceAll("chelpa.disabled.", BuildConfig.VERSION_NAME));
                                                } else {
                                                    perms.item(k).getAttributes().getNamedItem("name").setTextContent("chelpa.disabled." + per.getAttribute("name"));
                                                }
                                                if (!changedPermissions.contains(permission.Name)) {
                                                    changedPermissions.add(permission.Name);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            if (sharedUserId != null) {
                                packages = doc.getElementsByTagName("shared-user");
                                i = SETTINGS_VIEWSIZE_SMALL;
                                while (i < packages.getLength()) {
                                    node = packages.item(i);
                                    if (((Element) packages.item(i)).getAttribute("userId").equals(sharedUserId)) {
                                        i += TITLE_LIST_ITEM;
                                    } else {
                                        pkgParams = node.getChildNodes();
                                        for (j = SETTINGS_VIEWSIZE_SMALL; j < pkgParams.getLength(); j += TITLE_LIST_ITEM) {
                                            nodeC = pkgParams.item(j);
                                            System.out.println("node:" + nodeC.getNodeName());
                                            if (nodeC.getNodeName().equals("perms")) {
                                                perms = nodeC.getChildNodes();
                                                for (k = SETTINGS_VIEWSIZE_SMALL; k < perms.getLength(); k += TITLE_LIST_ITEM) {
                                                    System.out.println("perm0:" + perms.item(k).getNodeName());
                                                    if (perms.item(k).getNodeName().equals("item")) {
                                                        per = (Element) perms.item(k);
                                                        if (!per.getAttribute("name").equals(permission.Name)) {
                                                            if (per.getAttribute("name").contains("chelpa.disabled.")) {
                                                                perms.item(k).getAttributes().getNamedItem("name").setTextContent("chelpa.disabled." + per.getAttribute("name"));
                                                            } else {
                                                                perms.item(k).getAttributes().getNamedItem("name").setTextContent(per.getAttribute("name").replaceAll("chelpa.disabled.", BuildConfig.VERSION_NAME));
                                                            }
                                                            if (!changedPermissions.contains(permission.Name)) {
                                                                changedPermissions.add(permission.Name);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            if (nodeC.getNodeName().equals("disabled-components")) {
                                                perms = nodeC.getChildNodes();
                                                for (k = SETTINGS_VIEWSIZE_SMALL; k < perms.getLength(); k += TITLE_LIST_ITEM) {
                                                    System.out.println("perm0:" + perms.item(k).getNodeName());
                                                    if (perms.item(k).getNodeName().equals("item")) {
                                                        per = (Element) perms.item(k);
                                                        if (!per.getAttribute("name").equals(permission.Name)) {
                                                            if (per.getAttribute("name").contains("chelpa.disabled.")) {
                                                                perms.item(k).getAttributes().getNamedItem("name").setTextContent("chelpa.disabled." + per.getAttribute("name"));
                                                            } else {
                                                                perms.item(k).getAttributes().getNamedItem("name").setTextContent(per.getAttribute("name").replaceAll("chelpa.disabled.", BuildConfig.VERSION_NAME));
                                                            }
                                                            if (!changedPermissions.contains(permission.Name)) {
                                                                changedPermissions.add(permission.Name);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            if (nodeC.getNodeName().equals("enabled-components")) {
                                                perms = nodeC.getChildNodes();
                                                for (k = SETTINGS_VIEWSIZE_SMALL; k < perms.getLength(); k += TITLE_LIST_ITEM) {
                                                    System.out.println("perm0:" + perms.item(k).getNodeName());
                                                    if (perms.item(k).getNodeName().equals("item")) {
                                                        per = (Element) perms.item(k);
                                                        if (!per.getAttribute("name").equals(permission.Name)) {
                                                            if (per.getAttribute("name").contains("chelpa.disabled.")) {
                                                                perms.item(k).getAttributes().getNamedItem("name").setTextContent("chelpa.disabled." + per.getAttribute("name"));
                                                            } else {
                                                                perms.item(k).getAttributes().getNamedItem("name").setTextContent(per.getAttribute("name").replaceAll("chelpa.disabled.", BuildConfig.VERSION_NAME));
                                                            }
                                                            if (!changedPermissions.contains(permission.Name)) {
                                                                changedPermissions.add(permission.Name);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            TransformerFactory.newInstance().newTransformer().transform(new DOMSource(doc), new StreamResult(new File(writeTarget)));
                            System.out.println("Done");
                            file = new File(getContext().getDir("packages", SETTINGS_VIEWSIZE_SMALL) + "/packages1.xml");
                            if (file.exists()) {
                                file.delete();
                            }
                            file = new File(getContext().getDir("packages", SETTINGS_VIEWSIZE_SMALL) + "/packages.xml");
                            if (file.exists()) {
                                utils = new Utils(BuildConfig.VERSION_NAME);
                                try {
                                    Utils.copyFile(file, file);
                                } catch (IllegalArgumentException e2) {
                                    e2.printStackTrace();
                                    return;
                                } catch (Exception e3) {
                                    e3.printStackTrace();
                                    return;
                                }
                            }
                        }
                    }
                    if (sharedUserId != null) {
                        packages = doc.getElementsByTagName("shared-user");
                        i = SETTINGS_VIEWSIZE_SMALL;
                        while (i < packages.getLength()) {
                            node = packages.item(i);
                            if (((Element) packages.item(i)).getAttribute("userId").equals(sharedUserId)) {
                                i += TITLE_LIST_ITEM;
                            } else {
                                pkgParams = node.getChildNodes();
                                for (j = SETTINGS_VIEWSIZE_SMALL; j < pkgParams.getLength(); j += TITLE_LIST_ITEM) {
                                    nodeC = pkgParams.item(j);
                                    System.out.println("node:" + nodeC.getNodeName());
                                    if (nodeC.getNodeName().equals("perms")) {
                                        perms = nodeC.getChildNodes();
                                        for (k = SETTINGS_VIEWSIZE_SMALL; k < perms.getLength(); k += TITLE_LIST_ITEM) {
                                            System.out.println("perm0:" + perms.item(k).getNodeName());
                                            if (perms.item(k).getNodeName().equals("item")) {
                                                per = (Element) perms.item(k);
                                                if (!per.getAttribute("name").equals(permission.Name)) {
                                                    if (per.getAttribute("name").contains("chelpa.disabled.")) {
                                                        perms.item(k).getAttributes().getNamedItem("name").setTextContent("chelpa.disabled." + per.getAttribute("name"));
                                                    } else {
                                                        perms.item(k).getAttributes().getNamedItem("name").setTextContent(per.getAttribute("name").replaceAll("chelpa.disabled.", BuildConfig.VERSION_NAME));
                                                    }
                                                    if (!changedPermissions.contains(permission.Name)) {
                                                        changedPermissions.add(permission.Name);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    if (nodeC.getNodeName().equals("disabled-components")) {
                                        perms = nodeC.getChildNodes();
                                        for (k = SETTINGS_VIEWSIZE_SMALL; k < perms.getLength(); k += TITLE_LIST_ITEM) {
                                            System.out.println("perm0:" + perms.item(k).getNodeName());
                                            if (perms.item(k).getNodeName().equals("item")) {
                                                per = (Element) perms.item(k);
                                                if (!per.getAttribute("name").equals(permission.Name)) {
                                                    if (per.getAttribute("name").contains("chelpa.disabled.")) {
                                                        perms.item(k).getAttributes().getNamedItem("name").setTextContent("chelpa.disabled." + per.getAttribute("name"));
                                                    } else {
                                                        perms.item(k).getAttributes().getNamedItem("name").setTextContent(per.getAttribute("name").replaceAll("chelpa.disabled.", BuildConfig.VERSION_NAME));
                                                    }
                                                    if (!changedPermissions.contains(permission.Name)) {
                                                        changedPermissions.add(permission.Name);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    if (nodeC.getNodeName().equals("enabled-components")) {
                                        perms = nodeC.getChildNodes();
                                        for (k = SETTINGS_VIEWSIZE_SMALL; k < perms.getLength(); k += TITLE_LIST_ITEM) {
                                            System.out.println("perm0:" + perms.item(k).getNodeName());
                                            if (perms.item(k).getNodeName().equals("item")) {
                                                per = (Element) perms.item(k);
                                                if (!per.getAttribute("name").equals(permission.Name)) {
                                                    if (per.getAttribute("name").contains("chelpa.disabled.")) {
                                                        perms.item(k).getAttributes().getNamedItem("name").setTextContent("chelpa.disabled." + per.getAttribute("name"));
                                                    } else {
                                                        perms.item(k).getAttributes().getNamedItem("name").setTextContent(per.getAttribute("name").replaceAll("chelpa.disabled.", BuildConfig.VERSION_NAME));
                                                    }
                                                    if (!changedPermissions.contains(permission.Name)) {
                                                        changedPermissions.add(permission.Name);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    TransformerFactory.newInstance().newTransformer().transform(new DOMSource(doc), new StreamResult(new File(writeTarget)));
                    System.out.println("Done");
                } catch (ParserConfigurationException pce) {
                    pce.printStackTrace();
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                } catch (SAXException sae) {
                    sae.printStackTrace();
                } catch (Element e4) {
                    e4.printStackTrace();
                } catch (Element e42) {
                    e42.printStackTrace();
                }
            } catch (Element e422) {
                e422.printStackTrace();
            }
            file = new File(getContext().getDir("packages", SETTINGS_VIEWSIZE_SMALL) + "/packages1.xml");
            if (file.exists()) {
                file.delete();
            }
            file = new File(getContext().getDir("packages", SETTINGS_VIEWSIZE_SMALL) + "/packages.xml");
            if (file.exists()) {
                utils = new Utils(BuildConfig.VERSION_NAME);
                Utils.copyFile(file, file);
            }
        }
    }

    public void set_internet(final boolean on) {
        new Thread(new Runnable() {
            public void run() {
                if (on) {
                    Utils utils = new Utils(BuildConfig.VERSION_NAME);
                    String[] strArr = new String[listAppsFragment.TITLE_LIST_ITEM];
                    strArr[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = "pm enable " + listAppsFragment.pli.pkgName + "/android.permission.INTERNET";
                    utils.cmdRoot(strArr);
                    return;
                }
                utils = new Utils(BuildConfig.VERSION_NAME);
                strArr = new String[listAppsFragment.TITLE_LIST_ITEM];
                strArr[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = "pm disable " + listAppsFragment.pli.pkgName + "/android.permission.INTERNET";
                utils.cmdRoot(strArr);
            }
        }).start();
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void runId(int id) {
        PackageInfo info;
        int d;
        Utils utils;
        String[] strArr;
        Intent intent;
        switch (id) {
            case C0149R.string.billing_hack_menu:
                if (getInstance().getPackageManager().getComponentEnabledSetting(new ComponentName(getInstance(), MarketBillingService.class)) == SETTINGS_SORTBY_STATUS || getInstance().getPackageManager().getComponentEnabledSetting(new ComponentName(getInstance(), InAppBillingService.class)) == SETTINGS_SORTBY_STATUS) {
                    getPkgMng().setComponentEnabledSetting(new ComponentName(getInstance(), MarketBillingService.class), TITLE_LIST_ITEM, TITLE_LIST_ITEM);
                    getPkgMng().setComponentEnabledSetting(new ComponentName(getInstance(), InAppBillingService.class), TITLE_LIST_ITEM, TITLE_LIST_ITEM);
                } else {
                    getPkgMng().setComponentEnabledSetting(new ComponentName(getInstance(), MarketBillingService.class), SETTINGS_SORTBY_STATUS, TITLE_LIST_ITEM);
                    getPkgMng().setComponentEnabledSetting(new ComponentName(getInstance(), InAppBillingService.class), SETTINGS_SORTBY_STATUS, TITLE_LIST_ITEM);
                    Utils.market_billing_services(true);
                }
                Intent in2 = new Intent(getInstance(), inapp_widget.class);
                in2.setPackage(getInstance().getPackageName());
                in2.setAction(inapp_widget.ACTION_WIDGET_RECEIVER_Updater);
                getInstance().sendBroadcast(in2);
                break;
            case C0149R.string.bootview:
                removeDialogLP(SETTINGS_SORTBY_TIME);
                showDialogLP(SETTINGS_SORTBY_TIME);
            case C0149R.string.cleardalvik:
                cleardalvik();
            case C0149R.string.cloneApplication:
                removeDialogLP(CONTEXT_DIALOG);
                if (rebuldApk.equals(BuildConfig.VERSION_NAME)) {
                    Utils.showDialogYesNo(pli.name, Utils.getText(C0149R.string.create_clone_app), new OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            listAppsFragment.this.cloneApplication();
                        }
                    }, null, null);
                } else {
                    Utils.showDialogYesNo(new File(rebuldApk).getName(), Utils.getText(C0149R.string.create_clone_app), new OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            listAppsFragment.this.toolbar_cloneApplication();
                        }
                    }, null, null);
                }
            case C0149R.string.context_automodeslvl:
                removeDialogLP(LVL_PATCH_CONTEXT_DIALOG);
                contextselpatchlvl(true);
                removeDialogLP(CONTEXT_DIALOG);
                showDialogLP(LVL_PATCH_CONTEXT_DIALOG);
            case C0149R.string.context_backup_apk:
                backup(pli);
            case C0149R.string.context_backup_data:
                backup_data();
            case C0149R.string.context_blockads:
                removeDialogLP(CONTEXT_DIALOG);
                toolbar_addfree_on();
            case C0149R.string.context_change_components_menu:
                contextpermmenu();
                showDialogLP(CONTEXT_DIALOG);
            case C0149R.string.context_clearhosts:
                removeDialogLP(CONTEXT_DIALOG);
                toolbar_addfree_clear();
            case C0149R.string.context_crt_apk_permission:
                removeDialogLP(Create_PERM_CONTEXT_DIALOG);
                getpermissions();
                removeDialogLP(CONTEXT_DIALOG);
                showDialogLP(Create_PERM_CONTEXT_DIALOG);
            case C0149R.string.context_dexopt_app:
                try {
                    dexopt_app(getPkgMng().getPackageInfo(pli.pkgName, SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir, pli.pkgName, BuildConfig.VERSION_NAME + getPkgMng().getPackageInfo(pli.pkgName, SETTINGS_VIEWSIZE_SMALL).applicationInfo.uid);
                } catch (Exception e) {
                    e.printStackTrace();
                    showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.error_get_pkgInfo));
                }
            case C0149R.string.context_disableActivity:
                removeDialogLP(Disable_Activity_CONTEXT_DIALOG);
                getLPActivity();
                removeDialogLP(CONTEXT_DIALOG);
                showDialogLP(Disable_Activity_CONTEXT_DIALOG);
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.context_disableComponents:
                removeDialogLP(Disable_COMPONENTS_CONTEXT_DIALOG);
                getComponents();
                removeDialogLP(CONTEXT_DIALOG);
                showDialogLP(Disable_COMPONENTS_CONTEXT_DIALOG);
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.context_disable_google_ads:
                if (su) {
                    System.out.println("Disable Google services ads");
                    info = null;
                    try {
                        info = getPkgMng().getPackageInfo("com.google.android.gms", Custom_PATCH_DIALOG);
                    } catch (NameNotFoundException e1) {
                        e1.printStackTrace();
                    }
                    if (info != null) {
                        if (!(info.services == null || info.services.length == 0)) {
                            d = SETTINGS_VIEWSIZE_SMALL;
                            while (d < info.services.length) {
                                try {
                                    if (!(!info.services[d].name.startsWith("com.google.android.gms.ads.identifier.") || getPkgMng().getComponentEnabledSetting(new ComponentName("com.google.android.gms", info.services[d].name)) == SETTINGS_SORTBY_STATUS || info.services[d].name.startsWith("com.google.android.gms.ads.social") || info.services[d].name.startsWith("com.google.android.gms.ads.jams."))) {
                                        System.out.println("Disable Google services ads:" + info.services[d].name);
                                        showDialogLP(PROGRESS_DIALOG2);
                                        progress2.setCancelable(true);
                                        progress2.setMessage(Utils.getText(C0149R.string.wait));
                                        utils = new Utils(BuildConfig.VERSION_NAME);
                                        strArr = new String[SETTINGS_SORTBY_STATUS];
                                        strArr[SETTINGS_VIEWSIZE_SMALL] = "pm disable 'com.google.android.gms/" + info.services[d].name + "'";
                                        strArr[TITLE_LIST_ITEM] = "skipOut";
                                        utils.cmdRoot(strArr);
                                    }
                                } catch (Exception e2) {
                                    e2.printStackTrace();
                                }
                                d += TITLE_LIST_ITEM;
                            }
                        }
                    }
                }
            case C0149R.string.context_disable_package:
                try {
                    disable_package(pli.pkgName, false);
                } catch (Exception e22) {
                    e22.printStackTrace();
                    showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.error_get_pkgInfo));
                }
            case C0149R.string.context_enable_google_ads:
                if (su) {
                    System.out.println("Enable Google services ads");
                    info = null;
                    try {
                        info = getPkgMng().getPackageInfo("com.google.android.gms", Custom_PATCH_DIALOG);
                    } catch (NameNotFoundException e12) {
                        e12.printStackTrace();
                    }
                    if (info != null) {
                        if (!(info.services == null || info.services.length == 0)) {
                            d = SETTINGS_VIEWSIZE_SMALL;
                            while (d < info.services.length) {
                                try {
                                    if (info.services[d].name.startsWith("com.google.android.gms.ads") && getPkgMng().getComponentEnabledSetting(new ComponentName("com.google.android.gms", info.services[d].name)) == SETTINGS_SORTBY_STATUS) {
                                        System.out.println("Enable Google services ads:" + info.services[d].name);
                                        showDialogLP(PROGRESS_DIALOG2);
                                        progress2.setCancelable(true);
                                        progress2.setMessage(Utils.getText(C0149R.string.wait));
                                        utils = new Utils(BuildConfig.VERSION_NAME);
                                        strArr = new String[SETTINGS_SORTBY_STATUS];
                                        strArr[SETTINGS_VIEWSIZE_SMALL] = "pm enable 'com.google.android.gms/" + info.services[d].name + "'";
                                        strArr[TITLE_LIST_ITEM] = "skipOut";
                                        utils.cmdRoot(strArr);
                                    }
                                } catch (Exception e222) {
                                    e222.printStackTrace();
                                }
                                d += TITLE_LIST_ITEM;
                            }
                        }
                    }
                }
            case C0149R.string.context_enable_package:
                try {
                    disable_package(pli.pkgName, true);
                } catch (Exception e2222) {
                    e2222.printStackTrace();
                    showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.error_get_pkgInfo));
                }
            case C0149R.string.context_hard_apk_permission:
                runWithWait(new Runnable() {

                    class C03441 implements Runnable {
                        C03441() {
                        }

                        public void run() {
                            listAppsFragment.removeDialogLP(listAppsFragment.CONTEXT_DIALOG);
                            listAppsFragment.showDialogLP(listAppsFragment.PERM_CONTEXT_DIALOG);
                        }
                    }

                    public void run() {
                        listAppsFragment.removeDialogLP(listAppsFragment.PERM_CONTEXT_DIALOG);
                        listAppsFragment.this.getpackagesxml();
                        listAppsFragment.this.contextpermissions();
                        listAppsFragment.this.runToMain(new C03441());
                    }
                });
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.context_install:
                FileApkListItem apkfile = new FileApkListItem(getInstance(), new File(rebuldApk), false);
                removeDialogLP(CONTEXT_DIALOG);
                toolbar_restore(apkfile, false);
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.context_install_as_system:
                removeDialogLP(CONTEXT_DIALOG);
                toolbar_install_system(new FileApkListItem(getInstance(), new File(rebuldApk), false));
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.context_integrate_update_selected_apps:
                adapterSelectType = C0149R.string.button_integrate_update_apps;
                selected_apps();
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.context_move_selected_apps_to_internal:
                adapterSelectType = C0149R.string.button_move_to_internal;
                selected_apps();
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.context_move_selected_apps_to_sdcard:
                adapterSelectType = C0149R.string.button_move_to_sdcard;
                selected_apps();
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.context_rebuild:
                rebuldApk = this.current.full;
                contexttoolbarcreateapk();
                removeDialogLP(CONTEXT_DIALOG);
                showDialogLP(CONTEXT_DIALOG);
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.context_removeAds:
                removeDialogLP(ADS_PATCH_CONTEXT_DIALOG);
                contextselpatchads(true);
                removeDialogLP(CONTEXT_DIALOG);
                showDialogLP(ADS_PATCH_CONTEXT_DIALOG);
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.context_remove_saved_purchaces:
                context_remove_saved_purchases();
                removeDialogLP(CONTEXT_DIALOG);
                showDialogLP(CONTEXT_DIALOG);
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.context_restore_apk:
                removeDialogLP(CONTEXT_DIALOG);
                backupselector();
                removeDialogLP(BACKUP_SELECTOR);
                showDialogLP(BACKUP_SELECTOR);
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.context_restore_data:
                restore_data();
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.context_safe_apk_permission:
                removeDialogLP(Safe_PERM_CONTEXT_DIALOG);
                getpermissions();
                removeDialogLP(CONTEXT_DIALOG);
                showDialogLP(Safe_PERM_CONTEXT_DIALOG);
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.context_safe_sign_apk_permission:
                String filesDir = BuildConfig.VERSION_NAME;
                try {
                    filesDir = getPkgMng().getPackageInfo(pli.pkgName, SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir;
                } catch (NameNotFoundException e122) {
                    e122.printStackTrace();
                } catch (Exception e22222) {
                    e22222.printStackTrace();
                }
                if (Utils.checkCoreJarPatch20() || (pli.system && filesDir.startsWith("/system/app"))) {
                    removeDialogLP(Safe_Signature_PERM_CONTEXT_DIALOG);
                    getpermissions();
                    removeDialogLP(CONTEXT_DIALOG);
                    showDialogLP(Safe_Signature_PERM_CONTEXT_DIALOG);
                } else {
                    showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.safe_perm_use_warning));
                }
                break;
            case C0149R.string.context_support_patch:
                removeDialogLP(SUPPORT_PATCH_CONTEXT_DIALOG);
                contextsupport(false);
                removeDialogLP(CONTEXT_DIALOG);
                showDialogLP(SUPPORT_PATCH_CONTEXT_DIALOG);
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.context_unblockads:
                removeDialogLP(CONTEXT_DIALOG);
                toolbar_addfree_off();
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.context_uninstall_if_installed:
                FileApkListItem apkfile2 = new FileApkListItem(getInstance(), new File(rebuldApk), false);
                removeDialogLP(CONTEXT_DIALOG);
                toolbar_uninstall(apkfile2);
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.context_uninstall_selected_apps:
                adapterSelectType = C0149R.string.button_uninstall_apps;
                selected_apps();
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.contextads:
                removeDialogLP(ADS_PATCH_CONTEXT_DIALOG);
                contextads();
                removeDialogLP(CONTEXT_DIALOG);
                showDialogLP(CONTEXT_DIALOG);
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.contextbackup:
                context_backup_menu();
                removeDialogLP(CONTEXT_DIALOG);
                showDialogLP(CONTEXT_DIALOG);
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.contextboot:
                removeDialogLP(CONTEXT_DIALOG);
                custompatchselector();
                if (adapt.getCount() <= TITLE_LIST_ITEM) {
                    removeDialogLP(CUSTOM2_DIALOG);
                    customselect = (File) adapt.getItem(SETTINGS_VIEWSIZE_SMALL);
                    bootadd(pli, "custom");
                } else {
                    func = SETTINGS_SORTBY_STATUS;
                    removeDialogLP(CUSTOM_PATCH_SELECTOR);
                    showDialogLP(CUSTOM_PATCH_SELECTOR);
                }
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.contextbootads:
                bootadd(pli, "ads");
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.contextbootcustom:
                bootadd(pli, "custom");
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.contextbootlvl:
                bootadd(pli, "lvl");
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.contextcustompatch:
                removeDialogLP(CONTEXT_DIALOG);
                custompatchselector();
                if (adapt.getCount() <= 0 || adapt.getCount() > TITLE_LIST_ITEM) {
                    func = TITLE_LIST_ITEM;
                    showDialogLP(CUSTOM_PATCH_SELECTOR);
                } else {
                    func = TITLE_LIST_ITEM;
                    removeDialogLP(CUSTOM2_DIALOG);
                    customselect = (File) adapt.getItem(SETTINGS_VIEWSIZE_SMALL);
                    showDialogLP(CUSTOM2_DIALOG);
                }
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.contextdeodex:
                deodex(pli);
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.contextextpatch:
                runextendetpatch(pli);
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.contextfix:
                contextfix();
                removeDialogLP(CONTEXT_DIALOG);
                showDialogLP(CONTEXT_DIALOG);
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.contextignore:
                contextlvl();
                removeDialogLP(CONTEXT_DIALOG);
                showDialogLP(CONTEXT_DIALOG);
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.contextlivepatch:
                startActivity(new Intent(getContext(), LivepatchActivity.class));
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.contextodex:
                odex(pli);
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.contextrestore:
                context_restore_menu();
                removeDialogLP(CONTEXT_DIALOG);
                showDialogLP(CONTEXT_DIALOG);
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.contextselpatt:
                removeDialogLP(PATCH_CONTEXT_DIALOG);
                contextselpatch();
                removeDialogLP(CONTEXT_DIALOG);
                showDialogLP(PATCH_CONTEXT_DIALOG);
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.corepatches:
                contextCorePatch();
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.createapk:
                contextcreateapk();
                removeDialogLP(CONTEXT_DIALOG);
                showDialogLP(CONTEXT_DIALOG);
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.createapkads:
                removeDialogLP(Create_ADS_PATCH_CONTEXT_DIALOG);
                contextselpatchads(false);
                removeDialogLP(CONTEXT_DIALOG);
                showDialogLP(Create_ADS_PATCH_CONTEXT_DIALOG);
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.createapkcustom:
                removeDialogLP(CONTEXT_DIALOG);
                custompatchselector();
                if (adapt.getCount() <= TITLE_LIST_ITEM) {
                    removeDialogLP(CUSTOM2_DIALOG);
                    customselect = (File) adapt.getItem(SETTINGS_VIEWSIZE_SMALL);
                    showDialogLP(CUSTOM2_DIALOG);
                    func = SETTINGS_VIEWSIZE_SMALL;
                } else {
                    func = SETTINGS_VIEWSIZE_SMALL;
                    removeDialogLP(CUSTOM2_DIALOG);
                    removeDialogLP(CUSTOM_PATCH_SELECTOR);
                    showDialogLP(CUSTOM_PATCH_SELECTOR);
                }
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.createapklvl:
                removeDialogLP(Create_LVL_PATCH_CONTEXT_DIALOG);
                contextselpatchlvl(false);
                removeDialogLP(CONTEXT_DIALOG);
                showDialogLP(Create_LVL_PATCH_CONTEXT_DIALOG);
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.createapksupport:
                removeDialogLP(create_SUPPORT_PATCH_CONTEXT_DIALOG);
                contextsupport(true);
                removeDialogLP(CONTEXT_DIALOG);
                showDialogLP(create_SUPPORT_PATCH_CONTEXT_DIALOG);
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.delete_file:
                removeDialogLP(CONTEXT_DIALOG);
                delete_file();
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.dirbinder:
                startActivity(new Intent(getContext(), BinderActivity.class));
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.disable_inapp_services_google:
                if (su) {
                    info = Utils.getPkgInfo(Common.GOOGLEPLAY_PKG, Custom_PATCH_DIALOG);
                    boolean showed = false;
                    if (!(info == null || info.services == null || info.services.length == 0)) {
                        d = SETTINGS_VIEWSIZE_SMALL;
                        while (d < info.services.length) {
                            try {
                                if ((info.services[d].name.endsWith("InAppBillingService") || info.services[d].name.endsWith("MarketBillingService")) && getPkgMng().getComponentEnabledSetting(new ComponentName(Common.GOOGLEPLAY_PKG, info.services[d].name)) != TITLE_LIST_ITEM) {
                                    showDialogLP(PROGRESS_DIALOG2);
                                    progress2.setCancelable(true);
                                    progress2.setMessage(Utils.getText(C0149R.string.wait));
                                    Utils.market_billing_services(true);
                                }
                                if ((info.services[d].name.endsWith("InAppBillingService") || info.services[d].name.endsWith("MarketBillingService")) && getPkgMng().getComponentEnabledSetting(new ComponentName(Common.GOOGLEPLAY_PKG, info.services[d].name)) == TITLE_LIST_ITEM) {
                                    showDialogLP(PROGRESS_DIALOG2);
                                    progress2.setCancelable(true);
                                    progress2.setMessage(Utils.getText(C0149R.string.wait));
                                    Utils.market_billing_services(false);
                                    if (!showed) {
                                        showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.message_warning_InApp_service_disable));
                                    }
                                    showed = true;
                                }
                            } catch (Exception e222222) {
                                e222222.printStackTrace();
                            }
                            d += TITLE_LIST_ITEM;
                        }
                    }
                }
            case C0149R.string.disable_license_services_google:
                if (su) {
                    info = Utils.getPkgInfo(Common.GOOGLEPLAY_PKG, Custom_PATCH_DIALOG);
                    if (!(info == null || info.services == null || info.services.length == 0)) {
                        d = SETTINGS_VIEWSIZE_SMALL;
                        while (d < info.services.length) {
                            try {
                                if (info.services[d].name.endsWith("LicensingService") && getPkgMng().getComponentEnabledSetting(new ComponentName(Common.GOOGLEPLAY_PKG, info.services[d].name)) != TITLE_LIST_ITEM) {
                                    showDialogLP(PROGRESS_DIALOG2);
                                    progress2.setCancelable(true);
                                    progress2.setMessage(Utils.getText(C0149R.string.wait));
                                    Utils.market_licensing_services(true);
                                }
                                if (info.services[d].name.endsWith("LicensingService") && getPkgMng().getComponentEnabledSetting(new ComponentName(Common.GOOGLEPLAY_PKG, info.services[d].name)) == TITLE_LIST_ITEM) {
                                    showDialogLP(PROGRESS_DIALOG2);
                                    progress2.setCancelable(true);
                                    progress2.setMessage(Utils.getText(C0149R.string.wait));
                                    Utils.market_licensing_services(false);
                                    showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.message_warning_LVL_service_disable));
                                }
                            } catch (Exception e2222222) {
                                e2222222.printStackTrace();
                            }
                            d += TITLE_LIST_ITEM;
                        }
                    }
                }
            case C0149R.string.installsupersu:
                intent = new Intent("android.intent.action.VIEW", Uri.parse("https://play.google.com/store/apps/details?id=eu.chainfire.supersu"));
                intent.addFlags(1073741824);
                startActivity(intent);
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.integrate_updates_to_system:
                ArrayList<PkgListItem> arrayList = new ArrayList(TITLE_LIST_ITEM);
                arrayList.add(pli);
                integrate_to_system(arrayList, true, false);
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.licensing_hack_menu:
                if (getInstance().getPackageManager().getComponentEnabledSetting(new ComponentName(getInstance(), LicensingService.class)) == SETTINGS_SORTBY_STATUS) {
                    getPkgMng().setComponentEnabledSetting(new ComponentName(getInstance(), LicensingService.class), TITLE_LIST_ITEM, TITLE_LIST_ITEM);
                    showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.message_warning_LVL_emulation_service_enable));
                } else {
                    getPkgMng().setComponentEnabledSetting(new ComponentName(getInstance(), LicensingService.class), SETTINGS_SORTBY_STATUS, TITLE_LIST_ITEM);
                    Utils.market_licensing_services(true);
                }
                Intent in3 = new Intent(getInstance(), lvl_widget.class);
                in3.setPackage(getInstance().getPackageName());
                in3.setAction(lvl_widget.ACTION_WIDGET_RECEIVER_Updater);
                getInstance().sendBroadcast(in3);
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.market_install:
                removeDialogLP(MARKET_INSTALL_DIALOG);
                showDialogLP(MARKET_INSTALL_DIALOG);
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.mod_market_check:
                mod_market_check();
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.move_to_sys:
                move_to_system_click();
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.new_method_lvl:
                removeDialogLP(CONTEXT_DIALOG);
                new_method_lvl();
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.odex_all_system_app:
                odex_all_system_app();
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.permission:
                removeDialogLP(PERM_CONTEXT_DIALOG);
                getpackagesxml();
                contextpermissions();
                removeDialogLP(CONTEXT_DIALOG);
                showDialogLP(PERM_CONTEXT_DIALOG);
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.reboot:
                dialog_reboot();
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.remove_all_saved_purchases:
                remove_all_saved_purchases();
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.removefixes:
                removefixes();
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.set_default_to_install:
                setDefInstall();
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.set_default_to_install_auto:
                setDefInstallAuto();
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.set_default_to_install_internal_memory:
                setDefInstallInternalMemory();
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.set_default_to_install_sdcard:
                setDefInstallSDcard();
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.set_switchers_def:
                getConfig().edit().putBoolean("switch_auto_backup_apk", false).commit();
                getConfig().edit().putBoolean("switch_auto_backup_apk_only_gp", false).commit();
                getConfig().edit().putBoolean("switch_auto_integrate_update", false).commit();
                getConfig().edit().putBoolean("switch_auto_move_to_sd", false).commit();
                getConfig().edit().putBoolean("switch_auto_move_to_internal", false).commit();
                getPkgMng().setComponentEnabledSetting(new ComponentName(getInstance(), LicensingService.class), SETTINGS_SORTBY_STATUS, TITLE_LIST_ITEM);
                getPkgMng().setComponentEnabledSetting(new ComponentName(getInstance(), MarketBillingService.class), SETTINGS_SORTBY_STATUS, TITLE_LIST_ITEM);
                getPkgMng().setComponentEnabledSetting(new ComponentName(getInstance(), InAppBillingService.class), SETTINGS_SORTBY_STATUS, TITLE_LIST_ITEM);
                Utils.market_billing_services(true);
                Utils.market_licensing_services(true);
                Intent in4 = new Intent(getInstance(), inapp_widget.class);
                in4.setPackage(getInstance().getPackageName());
                in4.setAction(inapp_widget.ACTION_WIDGET_RECEIVER_Updater);
                getInstance().sendBroadcast(in4);
                Intent in5 = new Intent(getInstance(), lvl_widget.class);
                in5.setPackage(getInstance().getPackageName());
                in5.setAction(lvl_widget.ACTION_WIDGET_RECEIVER_Updater);
                getInstance().sendBroadcast(in5);
                runWithWait(new Runnable() {

                    class C03421 implements Runnable {
                        C03421() {
                        }

                        public void run() {
                            listAppsFragment.showDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                            listAppsFragment.progress2.setCancelable(true);
                            listAppsFragment.progress2.setMessage(Utils.getText(C0149R.string.wait));
                        }
                    }

                    class C03432 implements Runnable {
                        C03432() {
                        }

                        public void run() {
                            listAppsFragment.adapt.notifyDataSetChanged();
                        }
                    }

                    public void run() {
                        PackageInfo pi = Utils.getPkgInfo(Common.GOOGLEPLAY_PKG, listAppsFragment.SETTINGS_VIEWSIZE_SMALL);
                        if (pi != null) {
                            long lenght = 0;
                            try {
                                lenght = new File(Utils.getPlaceForOdex(pi.applicationInfo.sourceDir, false)).length();
                            } catch (Exception e) {
                            }
                            if (lenght <= 1048576 && lenght != 0) {
                                listAppsFragment.this.runToMain(new C03421());
                                try {
                                    Utils utils = new Utils(BuildConfig.VERSION_NAME);
                                    String[] strArr = new String[listAppsFragment.TITLE_LIST_ITEM];
                                    strArr[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = listAppsFragment.dalvikruncommand + ".pinfo " + pi.applicationInfo.sourceDir + " " + listAppsFragment.toolfilesdir + " " + String.valueOf(pi.applicationInfo.uid) + " recovery";
                                    utils.cmdRoot(strArr);
                                    Utils.market_billing_services(true);
                                    Utils.market_licensing_services(true);
                                    Utils.kill(Common.GOOGLEPLAY_PKG);
                                    listAppsFragment.this.runToMain(new C03432());
                                } catch (Exception e2) {
                                    e2.printStackTrace();
                                }
                            }
                        }
                    }
                });
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.share_this_app:
                removeDialogLP(CONTEXT_DIALOG);
                share_app();
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.switcher_auto_backup:
                if (getConfig().getBoolean("switch_auto_backup_apk", false)) {
                    getConfig().edit().putBoolean("switch_auto_backup_apk", false).commit();
                } else {
                    getConfig().edit().putBoolean("switch_auto_backup_apk", true).commit();
                    getConfig().edit().putBoolean("switch_auto_backup_apk_only_gp", false).commit();
                }
            case C0149R.string.switcher_auto_backup_only_gp:
                if (getConfig().getBoolean("switch_auto_backup_apk_only_gp", false)) {
                    getConfig().edit().putBoolean("switch_auto_backup_apk_only_gp", false).commit();
                } else {
                    getConfig().edit().putBoolean("switch_auto_backup_apk_only_gp", true).commit();
                    getConfig().edit().putBoolean("switch_auto_backup_apk", false).commit();
                }
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.switcher_auto_integrate_update:
                if (getConfig().getBoolean("switch_auto_integrate_update", false)) {
                    getConfig().edit().putBoolean("switch_auto_integrate_update", false).commit();
                } else {
                    getConfig().edit().putBoolean("switch_auto_integrate_update", true).commit();
                }
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.switcher_auto_move_to_internal:
                if (getConfig().getBoolean("switch_auto_move_to_internal", false)) {
                    getConfig().edit().putBoolean("switch_auto_move_to_internal", false).commit();
                } else {
                    getConfig().edit().putBoolean("switch_auto_move_to_internal", true).commit();
                    getConfig().edit().putBoolean("switch_auto_move_to_sd", false).commit();
                }
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.switcher_auto_move_to_sd:
                if (getConfig().getBoolean("switch_auto_move_to_sd", false)) {
                    getConfig().edit().putBoolean("switch_auto_move_to_sd", false).commit();
                } else {
                    getConfig().edit().putBoolean("switch_auto_move_to_sd", true).commit();
                    getConfig().edit().putBoolean("switch_auto_move_to_internal", false).commit();
                }
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.updatebusybox:
                intent = new Intent("android.intent.action.VIEW", Uri.parse("https://play.google.com/store/apps/details?id=stericson.busybox&feature"));
                intent.addFlags(1073741824);
                startActivity(intent);
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.xposed_settings:
                contextXposedPatch();
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.context_block_internet_selected_apps:
                adapterSelectType = C0149R.string.button_block_internet;
                selected_apps();
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.context_patch_framework:
                rebuldApk = this.current.full;
                contexttoolbarcreateframework();
                removeDialogLP(CONTEXT_DIALOG);
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.context_unblock_internet_selected_apps:
                adapterSelectType = C0149R.string.button_unblock_internet;
                selected_apps();
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.tools_menu_block_internet:
                set_internet(false);
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.tools_menu_unblock_internet:
                set_internet(true);
                if (!menu_open) {
                    hideMenu();
                }
            case C0149R.string.proxyGP:
                boolean run = false;
                if (Utils.isXposedEnabled()) {
                    ArrayList<CoreItem> s = new ArrayList();
                    JSONObject settings = null;
                    try {
                        settings = Utils.readXposedParamBoolean();
                    } catch (JSONException e3) {
                        e3.printStackTrace();
                    }
                    boolean Xposed4 = false;
                    if (settings != null) {
                        Xposed4 = settings.optBoolean("patch4", false);
                    }
                    if (!Xposed4) {
                        run = true;
                    }
                } else {
                    run = true;
                }
                if (run) {
                    showDialogLP(PROGRESS_DIALOG2);
                    progress2.setCancelable(true);
                    progress2.setMessage(Utils.getText(C0149R.string.wait));
                    runWithWait(new Runnable() {

                        class C03451 implements Runnable {
                            C03451() {
                            }

                            public void run() {
                                listAppsFragment.adapt.notifyDataSetChanged();
                            }
                        }

                        class C03462 implements Runnable {
                            C03462() {
                            }

                            public void run() {
                                listAppsFragment.showDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                                listAppsFragment.progress2.setCancelable(true);
                                listAppsFragment.progress2.setMessage(Utils.getText(C0149R.string.wait));
                            }
                        }

                        class C03473 implements Runnable {
                            C03473() {
                            }

                            public void run() {
                                listAppsFragment.adapt.notifyDataSetChanged();
                            }
                        }

                        public void run() {
                            PackageInfo pi = Utils.getPkgInfo(Common.GOOGLEPLAY_PKG, listAppsFragment.SETTINGS_VIEWSIZE_SMALL);
                            if (pi != null) {
                                if (!new File(listAppsFragment.toolfilesdir + "/p.apk").exists()) {
                                    listAppsFragment.this.pinfocopy();
                                }
                                long lenght = 0;
                                try {
                                    lenght = new File(Utils.getPlaceForOdex(pi.applicationInfo.sourceDir, false)).length();
                                } catch (Exception e) {
                                }
                                Utils utils;
                                String[] strArr;
                                if (lenght > 1048576 || lenght == 0) {
                                    Utils.kill(Common.GOOGLEPLAY_PKG);
                                    utils = new Utils(BuildConfig.VERSION_NAME);
                                    strArr = new String[listAppsFragment.TITLE_LIST_ITEM];
                                    strArr[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = listAppsFragment.dalvikruncommand + ".pinfo " + pi.applicationInfo.sourceDir + " " + listAppsFragment.toolfilesdir + " " + String.valueOf(pi.applicationInfo.uid) + " proxy";
                                    utils.cmdRoot(strArr);
                                    listAppsFragment.getPkgMng().setComponentEnabledSetting(new ComponentName(listAppsFragment.getInstance(), MarketBillingService.class), listAppsFragment.SETTINGS_SORTBY_STATUS, listAppsFragment.TITLE_LIST_ITEM);
                                    listAppsFragment.getPkgMng().setComponentEnabledSetting(new ComponentName(listAppsFragment.getInstance(), InAppBillingService.class), listAppsFragment.SETTINGS_SORTBY_STATUS, listAppsFragment.TITLE_LIST_ITEM);
                                    listAppsFragment.getPkgMng().setComponentEnabledSetting(new ComponentName(listAppsFragment.getInstance(), MarketBillingService.class), listAppsFragment.TITLE_LIST_ITEM, listAppsFragment.TITLE_LIST_ITEM);
                                    listAppsFragment.getPkgMng().setComponentEnabledSetting(new ComponentName(listAppsFragment.getInstance(), InAppBillingService.class), listAppsFragment.TITLE_LIST_ITEM, listAppsFragment.TITLE_LIST_ITEM);
                                    Utils.market_billing_services(true);
                                    Utils.market_licensing_services(true);
                                    Utils.kill(Common.GOOGLEPLAY_PKG);
                                    listAppsFragment.this.runToMain(new C03451());
                                    return;
                                }
                                listAppsFragment.this.runToMain(new C03462());
                                try {
                                    utils = new Utils(BuildConfig.VERSION_NAME);
                                    strArr = new String[listAppsFragment.TITLE_LIST_ITEM];
                                    strArr[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = listAppsFragment.dalvikruncommand + ".pinfo " + pi.applicationInfo.sourceDir + " " + listAppsFragment.toolfilesdir + " " + String.valueOf(pi.applicationInfo.uid) + " recovery";
                                    utils.cmdRoot(strArr);
                                    Utils.market_billing_services(true);
                                    Utils.market_licensing_services(true);
                                    Utils.kill(Common.GOOGLEPLAY_PKG);
                                    listAppsFragment.this.runToMain(new C03473());
                                } catch (Exception e2) {
                                    e2.printStackTrace();
                                }
                            }
                        }
                    });
                }
        }
        try {
            if (!menu_open) {
                hideMenu();
            }
        } catch (Exception e22222222) {
            e22222222.printStackTrace();
        }
    }

    private void populateAdapter(ArrayList<PkgListItem> pap, Comparator<PkgListItem> s) {
        if (pap == null || pap.size() == 0) {
            lv.invalidate();
            return;
        }
        int layout;
        Collections.sort(pap, s);
        if (getConfig().getBoolean("no_icon", false)) {
            layout = C0149R.layout.pkglistitemview_no_icon;
        } else {
            layout = C0149R.layout.pkglistitemview;
        }
        plia = new PkgListItemAdapter(getContext(), layout, getConfig().getInt(SETTINGS_VIEWSIZE, SETTINGS_VIEWSIZE_SMALL), pap);
        if (menu_adapter == null) {
            menu_adapter = new MenuItemAdapter(getContext(), getConfig().getInt(SETTINGS_VIEWSIZE, SETTINGS_VIEWSIZE_SMALL), new ArrayList());
        }
        adapter_boot = new BootListItemAdapter(getContext(), C0149R.layout.bootlistitemview, getConfig().getInt(SETTINGS_VIEWSIZE, SETTINGS_VIEWSIZE_SMALL), boot_pat);
        plia.sorter = s;
        removeDialogLP(SETTINGS_SORTBY_TIME);
        lv.invalidate();
        menu_lv.invalidate();
        menu_lv.setGroupIndicator(null);
        menu_lv.setAdapter(menu_adapter);
        menu_lv.setOnGroupClickListener(new OnGroupClickListener() {
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                MenuItem item = listAppsFragment.menu_adapter.getGroup(groupPosition);
                if (item.childs.size() != 0 && item.type != listAppsFragment.TITLE_LIST_ITEM) {
                    return false;
                }
                switch (item.type) {
                    case listAppsFragment.TITLE_LIST_ITEM /*1*/:
                        return true;
                    case listAppsFragment.SETTINGS_SORTBY_STATUS /*2*/:
                        switch (item.punkt_menu) {
                            case C0149R.string.aboutmenu:
                                listAppsFragment.this.showAbout();
                                return true;
                            case C0149R.string.days_on_up:
                                listAppsFragment.this.changeDayOnUp();
                                return true;
                            case C0149R.string.dir_change:
                                listAppsFragment.this.changeDefaultDir();
                                return true;
                            case C0149R.string.help:
                                listAppsFragment.this.startActivity(new Intent(listAppsFragment.patchAct, HelpActivity.class));
                                return true;
                            case C0149R.string.langmenu:
                                listAppsFragment.this.selectLanguage();
                                return true;
                            case C0149R.string.sendlog:
                                listAppsFragment.this.sendLog();
                                return true;
                            case C0149R.string.update:
                                if (listAppsFragment.frag == null) {
                                    return true;
                                }
                                listAppsFragment.this.runUpdate();
                                return true;
                            default:
                                return true;
                        }
                    case listAppsFragment.SETTINGS_SORTBY_TIME /*3*/:
                        listAppsFragment.this.runToMain(item.runCode);
                        return true;
                    default:
                        listAppsFragment.this.runId(item.punkt_menu);
                        return true;
                }
            }
        });
        menu_lv.setOnChildClickListener(new OnChildClickListener() {
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                listAppsFragment.this.runId(((Integer) listAppsFragment.menu_adapter.getGroup(groupPosition).childs.get(childPosition)).intValue());
                return false;
            }
        });
        lv.setAdapter(plia);
        lv.setOnChildClickListener(new OnChildClickListener() {
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                if (listAppsFragment.getConfig().getBoolean("vibration", false)) {
                    listAppsFragment.this.vib = (Vibrator) listAppsFragment.this.getContext().getSystemService("vibrator");
                    listAppsFragment.this.vib.vibrate(50);
                }
                listAppsFragment.pli = listAppsFragment.plia.getItem(groupPosition);
                switch (listAppsFragment.plia.getChild(groupPosition, childPosition).intValue()) {
                    case C0149R.string.advanced_menu:
                        listAppsFragment.this.contextmenu_click();
                        break;
                    case C0149R.string.app_dialog_no_root_app_control:
                        listAppsFragment.this.show_app_manager_click();
                        break;
                    case C0149R.string.app_info:
                        listAppsFragment.plia.updateItem(listAppsFragment.pli.pkgName);
                        listAppsFragment.plia.notifyDataSetChanged(listAppsFragment.plia.getItem(groupPosition));
                        listAppsFragment.removeDialogLP(listAppsFragment.CHILD_NAME_SHEME_SELECT_ITEM);
                        listAppsFragment.showDialogLP(listAppsFragment.CHILD_NAME_SHEME_SELECT_ITEM);
                        break;
                    case C0149R.string.cleardata:
                        listAppsFragment.this.cleardata_click();
                        break;
                    case C0149R.string.context_tools:
                        listAppsFragment.this.contextmenutools_click();
                        break;
                    case C0149R.string.contextlaunchapp:
                        listAppsFragment.this.launch_click();
                        break;
                    case C0149R.string.move_to_internal:
                        listAppsFragment.this.move_to_internal_memory_click();
                        break;
                    case C0149R.string.move_to_sdcard:
                        listAppsFragment.this.move_to_sdcard_click();
                        break;
                    case C0149R.string.uninstallapp:
                        listAppsFragment.this.uninstall_click();
                        break;
                }
                return false;
            }
        });
        lv.setDividerHeight(SETTINGS_SORTBY_STATUS);
        lv.setGroupIndicator(null);
        lv.setClickable(true);
        lv.setLongClickable(true);
        lv.setOnGroupExpandListener(new OnGroupExpandListener() {
            public void onGroupExpand(int groupPosition) {
            }
        });
        lv.setOnGroupClickListener(new OnGroupClickListener() {
            public boolean onGroupClick(ExpandableListView parent, View arg1, final int groupPosition, long arg3) {
                System.out.println(parent.getItemAtPosition(groupPosition));
                System.out.println(groupPosition);
                listAppsFragment.pli = listAppsFragment.plia.getItem(groupPosition);
                listAppsFragment.createExpandMenu();
                if (parent.isGroupExpanded(groupPosition)) {
                    parent.collapseGroup(groupPosition);
                } else {
                    parent.expandGroup(groupPosition);
                    if (listAppsFragment.api > listAppsFragment.CONTEXT_DIALOG) {
                        parent.smoothScrollToPosition(groupPosition);
                    } else {
                        listAppsFragment.lv.clearFocus();
                        listAppsFragment.lv.post(new Runnable() {
                            public void run() {
                                listAppsFragment.lv.setSelection(groupPosition);
                            }
                        });
                    }
                }
                return true;
            }
        });
        lv.setOnItemLongClickListener(new OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                if (listAppsFragment.getConfig().getBoolean("vibration", false)) {
                    listAppsFragment.this.vib = (Vibrator) listAppsFragment.this.getContext().getSystemService("vibrator");
                    listAppsFragment.this.vib.vibrate(50);
                }
                try {
                    listAppsFragment.pli = (PkgListItem) parent.getItemAtPosition(position);
                    listAppsFragment.this.contextlevel0();
                } catch (Exception e) {
                    System.out.println("LuckyPatcher (Context menu long click): " + e);
                    e.printStackTrace();
                }
                return false;
            }
        });
    }

    public void showAbout() {
        LinearLayout d = (LinearLayout) View.inflate(patchAct, C0149R.layout.aboutdialog, null);
        LinearLayout body = (LinearLayout) d.findViewById(C0149R.id.dialogbodyscroll).findViewById(C0149R.id.dialogbody);
        TextView tv = (TextView) body.findViewById(C0149R.id.intonlydesc);
        tv.append(Utils.getColoredText(Utils.getText(C0149R.string.vers) + version + LogCollector.LINE_SEPARATOR, "#ffffffff", "bold"));
        tv.append(Utils.getColoredText("----------------------------------\n\n", "#ffffffff", "bold"));
        tv.append(Utils.getColoredText(Utils.getText(C0149R.string.about_working_dir) + " " + basepath + "\n\n", "#ffffffff", "bold"));
        tv.append(Utils.getColoredText(Utils.getText(C0149R.string.punkt1) + "\n\n", "#ffff0000", "bold"));
        tv.append(Utils.getColoredText(Utils.getText(C0149R.string.aboutlp) + LogCollector.LINE_SEPARATOR, "#ffffff00", "bold"));
        tv.append(Utils.getColoredText("\nThanks to:\nUsed source code: pyler, Mikanoshi@4pda.ru\nSite WebIconSet.com - Smile icons\np0izn@xda - English translation\nJang Seung Hun - Korean translation\nDerDownloader7,Simon Plantone - German translation\nAndrew Q., Simoconfa - Italian translation\ntrBilimBEKo,Z.Zeynalov,Noss_bar - Turkish translation\nSpoetnic - Dutch translation\n\u67d2\u6708\u96ea@BNB520,\u5356\u840c\u5b50,Hiapk-\u5929\u4f7f - Simplified Chinese translation\neauland, foXaCe - French translation\nrhrarhra@Mobilism.org, alireza_simkesh@XDA - Persian translation\nAlftronics & TheCh - Portuguese translation\nCaio Fons\u00eaca - Portuguese (Brasil) translation\n\u6f22\u9ed1\u7389,Hiapk-\u5929\u4f7f - Traditional Chinese translation\ns_h - Hebrew translation\nXcooM - Polish translation\nGizmo[SK],pyler - Slovak translation\nHarmeet Singh - Hindi translation\nPROXIMO - Bulgarian translation\nCulum - Indonesian translation\nslycog@XDA - Custom Patch Help translate\nCheng Sokdara (homi3kh) - Khmer translation\nrkononenko, masterlist@forpda, Volodiimr - Ukrainian translation\nJeduRocks@XDA - Spanish translation\nPaul Diosteanu - Romanian translation\ngidano - Hungarian translation\nOrbit09, Renek - Czech translation\ni_Droidi@Twitter - Arabic translation\nbobo1704 - Greek translation\nJack_Rover, 6sto - Finnish translation\nWan Mohammad - Malay translation\nNguy\u1ec5n \u0110\u1ee9c L\u01b0\u1ee3ng - Vietnamese translation\nSOORAJ SR(fb.com/rsoorajs) - Malayalam translation\nXiveZ - Belarusian translation\nEkberjan - Uighur translation\nevildog1 - Danish translation\nHzy980512 - Japanese translation\nChelpus, maksnogin - Russian translation\nIcons to menu - Sergey Kamashki\nTemplate for ADS - And123\n\nSupport by email: lp.chelpus@gmail.com\nGood Luck!\nChelpuS", "#ffffffff", "bold"));
        new AlertDlg(patchAct).setTitle((int) C0149R.string.abouttitle).setCancelable(true).setIcon(17301659).setPositiveButton(Utils.getText(17039370), null).setNeutralButton("Changelog", new OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (new File(listAppsFragment.basepath + "/Changes/changelog.txt").exists()) {
                    listAppsFragment.this.showMessage(Utils.getText(C0149R.string.changelog), Utils.read_from_file(new File(listAppsFragment.basepath + "/Changes/changelog.txt")));
                } else {
                    listAppsFragment.this.showMessage(Utils.getText(C0149R.string.changelog), Utils.getText(C0149R.string.not_found_changelog));
                }
            }
        }).setView(d).setOnCancelListener(new OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                dialog.dismiss();
            }
        }).create().show();
    }

    public void sendLog() {
        new AsyncTask<Void, Void, Boolean>() {
            ProgressDialog pd;

            protected Boolean doInBackground(Void... params) {
                try {
                    if (listAppsFragment.mLogCollector == null) {
                        listAppsFragment.mLogCollector = new LogCollector();
                    }
                    return Boolean.valueOf(listAppsFragment.mLogCollector.collect(listAppsFragment.getInstance(), true));
                } catch (Exception e) {
                    e.printStackTrace();
                    return Boolean.valueOf(false);
                }
            }

            protected void onPreExecute() {
                this.pd = new ProgressDialog(listAppsFragment.patchAct);
                this.pd.setTitle("Progress");
                this.pd.setMessage(Utils.getText(C0149R.string.collect_logs));
                this.pd.setIndeterminate(true);
                this.pd.show();
            }

            protected void onPostExecute(Boolean result) {
                try {
                    if (this.pd != null && this.pd.isShowing()) {
                        this.pd.dismiss();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (result.booleanValue()) {
                    try {
                        listAppsFragment.mLogCollector.sendLog(listAppsFragment.getInstance(), "lp.chelpus@gmail.com", "Error Log", "Lucky Patcher " + listAppsFragment.getPkgMng().getPackageInfo(listAppsFragment.frag.getContext().getPackageName(), listAppsFragment.SETTINGS_VIEWSIZE_SMALL).versionName);
                        return;
                    } catch (NameNotFoundException e2) {
                        e2.printStackTrace();
                        return;
                    }
                }
                Builder logbuilder = new Builder(listAppsFragment.patchAct);
                logbuilder.setTitle("Error").setMessage(Utils.getText(C0149R.string.error_collect_logs)).setNegativeButton("OK", null);
                logbuilder.create().show();
            }
        }.execute(new Void[SETTINGS_VIEWSIZE_SMALL]);
    }

    public void changeDayOnUp() {
        AlertDlg alert = new AlertDlg(frag.getContext());
        alert.setTitle((int) C0149R.string.days_on_up);
        LinearLayout d = (LinearLayout) View.inflate(frag.getContext(), C0149R.layout.child_day_on_up_pref_view, null);
        final EditText ed = (EditText) d.findViewById(C0149R.id.editTextDOU);
        ed.setText(BuildConfig.VERSION_NAME + getConfig().getInt("days_on_up", TITLE_LIST_ITEM));
        alert.setView(d);
        alert.setNegativeButton((int) C0149R.string.no, null).setPositiveButton((int) C0149R.string.Yes, new OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Editable newValue = ed.getText();
                if (listAppsFragment.this.numberCheck(newValue)) {
                    listAppsFragment.getConfig().edit().putInt("days_on_up", Integer.valueOf(newValue.toString()).intValue()).commit();
                    listAppsFragment.getConfig().edit().putBoolean("settings_change", true).commit();
                    listAppsFragment.getConfig().edit().putBoolean("lang_change", true).commit();
                    listAppsFragment.runResume = true;
                }
            }
        }).create().show();
    }

    private boolean numberCheck(Object newValue) {
        if (!newValue.toString().equals(BuildConfig.VERSION_NAME) && newValue.toString().matches("\\d*")) {
            return true;
        }
        showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.is_an_invalid_number));
        return false;
    }

    public void changeDefaultDir() {
        AlertDlg alert = new AlertDlg(frag.getContext());
        alert.setTitle((int) C0149R.string.dir_change);
        LinearLayout d = (LinearLayout) View.inflate(frag.getContext(), C0149R.layout.child_change_dir_pref_view, null);
        final EditText ed = (EditText) d.findViewById(C0149R.id.editTextDir);
        ed.setText(getConfig().getString("basepath", InternalZipConstants.ZIP_FILE_SEPARATOR));
        alert.setView(d);
        alert.setNegativeButton((int) C0149R.string.no, null).setPositiveButton((int) C0149R.string.Yes, new OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Editable newValue = ed.getText();
                if (!newValue.toString().contains(InternalZipConstants.ZIP_FILE_SEPARATOR) || newValue.toString().equals(InternalZipConstants.ZIP_FILE_SEPARATOR)) {
                    listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.error_path));
                    return;
                }
                String[] tailsdir = newValue.toString().trim().replaceAll("\\s+", ".").split("\\/+");
                String data_dirs = BuildConfig.VERSION_NAME;
                int length = tailsdir.length;
                for (int i = listAppsFragment.SETTINGS_VIEWSIZE_SMALL; i < length; i += listAppsFragment.TITLE_LIST_ITEM) {
                    String aTailsdir = tailsdir[i];
                    if (!aTailsdir.equals(BuildConfig.VERSION_NAME)) {
                        data_dirs = data_dirs + InternalZipConstants.ZIP_FILE_SEPARATOR + aTailsdir;
                    }
                }
                final String data_dir = data_dirs;
                if (new File(data_dir).exists()) {
                    listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.error_path_exists));
                } else if (listAppsFragment.this.testPath(true, data_dir) && !data_dir.startsWith(listAppsFragment.getInstance().getDir("sdcard", listAppsFragment.SETTINGS_VIEWSIZE_SMALL).getAbsolutePath())) {
                    listAppsFragment.getConfig().edit().putString("path", data_dir).commit();
                    listAppsFragment.getConfig().edit().putBoolean("manual_path", true).commit();
                    listAppsFragment.getConfig().edit().putBoolean("path_changed", true).commit();
                    final File srcFolder = new File(listAppsFragment.getConfig().getString("basepath", "Noting"));
                    final File destFolder = new File(data_dir);
                    if (srcFolder.exists()) {
                        listAppsFragment.this.runWithWait(new Runnable() {
                            public void run() {
                                try {
                                    Utils.copyFolder(srcFolder, destFolder);
                                    new Utils(BuildConfig.VERSION_NAME).deleteFolder(srcFolder);
                                    listAppsFragment.getConfig().edit().putString("basepath", data_dir).commit();
                                    listAppsFragment.basepath = data_dir;
                                    listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.move_data) + data_dir);
                                } catch (Exception e2) {
                                    e2.printStackTrace();
                                    listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.no_space));
                                }
                            }
                        });
                    } else {
                        System.out.println("Directory does not exist.");
                    }
                }
            }
        }).create().show();
    }

    public boolean testPath(boolean showDialog, String path) {
        System.out.println("test path.");
        try {
            if (!new File(path).exists()) {
                new File(path).mkdirs();
            }
            if (!new File(path).exists()) {
                showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.messagePath));
                return false;
            } else if (new File(path + "/tmp.txt").createNewFile()) {
                new File(path + "/tmp.txt").delete();
                return true;
            } else if (!showDialog) {
                return false;
            } else {
                showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.messagePath));
                return false;
            }
        } catch (IOException e) {
            if (!showDialog) {
                return false;
            }
            showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.messagePath));
            return false;
        }
    }

    public void selectLanguage() {
        ArrayList<String> s = new ArrayList();
        s.add("en_US");
        s.add("ar");
        s.add("be");
        s.add("bg");
        s.add("cs");
        s.add("da");
        s.add("de");
        s.add("el");
        s.add("es");
        s.add("fa");
        s.add("fi");
        s.add("fr");
        s.add("gu");
        s.add("he");
        s.add("hi");
        s.add("hu");
        s.add("id");
        s.add("in");
        s.add("it");
        s.add("iw");
        s.add("ja");
        s.add("km");
        s.add("km_KH");
        s.add("ko");
        s.add("lt");
        s.add("ml");
        s.add("my");
        s.add("nl");
        s.add("pl");
        s.add("pt");
        s.add("pt_rBR");
        s.add("ro");
        s.add("ru");
        s.add("sk");
        s.add("tr");
        s.add("ug");
        s.add("uk");
        s.add("vi");
        s.add("zh_CN");
        s.add("zh_HK");
        s.add("zh_TW");
        AlertDlg alert = new AlertDlg(patchAct);
        alert.setAdapter(new ArrayAdapter<String>(patchAct, C0149R.layout.contextmenu, s) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                TextView textView = (TextView) view.findViewById(C0149R.id.conttext);
                textView.setTextColor(-1);
                String it = (String) getItem(position);
                Locale tmp = new Locale(it, it);
                textView.setText(tmp.getDisplayName(tmp));
                if (it.equals("my")) {
                    textView.setText("Malay translation");
                }
                if (it.equals("be")) {
                    textView.setText("\u0411\u0435\u043b\u0430\u0440\u0443\u0441\u043a\u0430\u044f (Be\u0142arusian)");
                }
                if (it.equals("zh_TW")) {
                    textView.setText("Chinese (Taiwan)");
                }
                if (it.equals("zh_CN")) {
                    textView.setText("Chinese (China)");
                }
                if (it.equals("zh_HK")) {
                    textView.setText("Chinese (Hong Kong)");
                }
                if (it.equals("pt_rBR")) {
                    textView.setText("Portuguese (Brazil)");
                }
                textView.setTypeface(null, listAppsFragment.TITLE_LIST_ITEM);
                return view;
            }
        }, new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                SharedPreferences settings = listAppsFragment.getInstance().getSharedPreferences("config", listAppsFragment.SETTINGS_VIEWSIZE_SMALL);
                String[] tails = ((String) parent.getItemAtPosition(position)).split("_");
                Locale locale = null;
                if (tails.length == listAppsFragment.TITLE_LIST_ITEM) {
                    locale = new Locale(tails[listAppsFragment.SETTINGS_VIEWSIZE_SMALL]);
                }
                if (tails.length == listAppsFragment.SETTINGS_SORTBY_STATUS) {
                    locale = new Locale(tails[listAppsFragment.SETTINGS_VIEWSIZE_SMALL], tails[listAppsFragment.TITLE_LIST_ITEM], BuildConfig.VERSION_NAME);
                }
                if (tails.length == listAppsFragment.SETTINGS_SORTBY_TIME) {
                    locale = new Locale(tails[listAppsFragment.SETTINGS_VIEWSIZE_SMALL], tails[listAppsFragment.TITLE_LIST_ITEM], tails[listAppsFragment.SETTINGS_SORTBY_STATUS]);
                }
                Locale.setDefault(locale);
                Configuration appConfig2 = new Configuration();
                appConfig2.locale = locale;
                listAppsFragment.getRes().updateConfiguration(appConfig2, listAppsFragment.getRes().getDisplayMetrics());
                listAppsFragment.getConfig().edit().putString("force_language", (String) parent.getItemAtPosition(position)).commit();
                listAppsFragment.getConfig().edit().putBoolean("settings_change", true).commit();
                listAppsFragment.getConfig().edit().putBoolean("lang_change", true).commit();
                listAppsFragment.this.onResume();
            }
        });
        alert.create().show();
    }

    public static void createExpandMenu() {
        if (pli != null) {
            String directory = null;
            try {
                directory = getPkgMng().getPackageInfo(pli.pkgName, SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir;
            } catch (NameNotFoundException e) {
                try {
                    e.printStackTrace();
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
            if (!(!su || directory == null || directory.startsWith("/system/"))) {
                plia.childMenu = new int[CHILD_NAME_SHEME_SELECT_ITEM];
                plia.childMenu[SETTINGS_VIEWSIZE_SMALL] = C0149R.string.app_info;
                plia.childMenu[TITLE_LIST_ITEM] = C0149R.string.contextlaunchapp;
                plia.childMenu[SETTINGS_SORTBY_STATUS] = C0149R.string.advanced_menu;
                plia.childMenu[SETTINGS_SORTBY_TIME] = C0149R.string.uninstallapp;
                plia.childMenu[Custom_PATCH_DIALOG] = C0149R.string.cleardata;
                if (directory.startsWith("/mnt/asec/")) {
                    plia.childMenu[EXT_PATCH_DIALOG] = C0149R.string.move_to_internal;
                } else {
                    plia.childMenu[EXT_PATCH_DIALOG] = C0149R.string.move_to_sdcard;
                }
            }
            if (su && directory != null && directory.startsWith("/system/")) {
                plia.childMenu = new int[EXT_PATCH_DIALOG];
                plia.childMenu[SETTINGS_VIEWSIZE_SMALL] = C0149R.string.app_info;
                plia.childMenu[TITLE_LIST_ITEM] = C0149R.string.contextlaunchapp;
                plia.childMenu[SETTINGS_SORTBY_STATUS] = C0149R.string.advanced_menu;
                plia.childMenu[SETTINGS_SORTBY_TIME] = C0149R.string.uninstallapp;
                plia.childMenu[Custom_PATCH_DIALOG] = C0149R.string.cleardata;
            }
            if (!su) {
                plia.childMenu = new int[EXT_PATCH_DIALOG];
                plia.childMenu[SETTINGS_VIEWSIZE_SMALL] = C0149R.string.app_info;
                plia.childMenu[TITLE_LIST_ITEM] = C0149R.string.contextlaunchapp;
                plia.childMenu[SETTINGS_SORTBY_STATUS] = C0149R.string.advanced_menu;
                plia.childMenu[SETTINGS_SORTBY_TIME] = C0149R.string.uninstallapp;
                plia.childMenu[Custom_PATCH_DIALOG] = C0149R.string.app_dialog_no_root_app_control;
            }
            if (directory == null) {
                plia.childMenu = new int[TITLE_LIST_ITEM];
                plia.childMenu[SETTINGS_VIEWSIZE_SMALL] = C0149R.string.apk_not_found_error;
            }
        }
    }

    public void expandAll() {
        int count = plia.getGroupCount();
        for (int i = SETTINGS_VIEWSIZE_SMALL; i < count; i += TITLE_LIST_ITEM) {
            lv.expandGroup(i);
        }
    }

    public void collapseAll(int iskluchenie) {
        try {
            int count = plia.getGroupCount();
            for (int i = SETTINGS_VIEWSIZE_SMALL; i < count; i += TITLE_LIST_ITEM) {
                if (i != iskluchenie) {
                    lv.collapseGroup(i);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addIgnoreOdex(PkgListItem pli, String result) {
        try {
            str = BuildConfig.VERSION_NAME;
            String cmd = BuildConfig.VERSION_NAME;
            String system_app = "not_system";
            if (pli.system) {
                system_app = "system";
            }
            String uid = String.valueOf(getPkgMng().getPackageInfo(pli.pkgName, SETTINGS_VIEWSIZE_SMALL).applicationInfo.uid);
            cmd = dalvikruncommand + ".odexrunpatch " + pli.pkgName + " " + result + " " + getPkgMng().getPackageInfo(pli.pkgName, SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir + " " + getContext().getFilesDir() + " " + system_app + " " + "a" + " " + Utils.getCurrentRuntimeValue() + " " + uid + LogCollector.LINE_SEPARATOR;
            if (result.contains("copyDC")) {
                cmd = dalvikruncommand + ".odexrunpatch " + pli.pkgName + " " + result + " " + getPkgMng().getPackageInfo(pli.pkgName, SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir + " " + getContext().getFilesDir() + " " + system_app + " " + "copyDC" + " " + Utils.getCurrentRuntimeValue() + " " + uid + LogCollector.LINE_SEPARATOR;
            }
            System.out.println(cmd);
            patchData = new PatchData();
            patchData.pli = pli;
            patchData.typePatch = "LVL";
            patchData.appdir = getPkgMng().getPackageInfo(pli.pkgName, SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir;
            patchData.pkg = pli.pkgName;
            patchData.system_app = system_app;
            patchData.filesdir = getContext().getFilesDir().getAbsolutePath();
            patchData.uid = String.valueOf(getPkgMng().getPackageInfo(pli.pkgName, SETTINGS_VIEWSIZE_SMALL).applicationInfo.uid);
            patchData.cmd1 = cmd;
            patchData.cmd2 = dalvikruncommand + ".checkOdex " + getPkgMng().getPackageInfo(pli.pkgName, SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir + " " + getInstance().getFilesDir() + LogCollector.LINE_SEPARATOR;
            patchData.result = result;
            LvlAdsPatch();
        } catch (Exception e) {
        }
    }

    public void launch_click() {
        if (getConfig().getBoolean("vibration", false)) {
            this.vib = (Vibrator) getContext().getSystemService("vibrator");
            this.vib.vibrate(50);
        }
        removeDialogLP(CHILD_NAME_SHEME_SELECT_ITEM);
        new Thread(new Runnable() {

            class C03541 implements Runnable {
                C03541() {
                }

                public void run() {
                    Toast.makeText(listAppsFragment.getInstance(), Utils.getText(C0149R.string.error_launch), listAppsFragment.TITLE_LIST_ITEM).show();
                }
            }

            public void run() {
                try {
                    Utils.kill(listAppsFragment.pli.pkgName);
                    listAppsFragment.this.startActivity(listAppsFragment.getPkgMng().getLaunchIntentForPackage(listAppsFragment.pli.pkgName));
                } catch (Exception e) {
                    listAppsFragment.this.runToMain(new C03541());
                }
            }
        }).start();
    }

    public void move_to_system_click() {
        if (getConfig().getBoolean("vibration", false)) {
            this.vib = (Vibrator) getContext().getSystemService("vibrator");
            this.vib.vibrate(50);
        }
        OnClickListener dialogClickListener = new OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case Zip4jConstants.ENC_NO_ENCRYPTION /*-1*/:
                        try {
                            listAppsFragment.patchData = new PatchData();
                            listAppsFragment.patchData.pli = listAppsFragment.pli;
                            listAppsFragment.patchData.appdir = listAppsFragment.getPkgMng().getPackageInfo(listAppsFragment.pli.pkgName, listAppsFragment.SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir;
                            System.out.println("Start uninstall to " + listAppsFragment.patchData.appdir);
                        } catch (Exception e) {
                        }
                        try {
                            String cmd1 = listAppsFragment.dalvikruncommand + ".move_to_system " + listAppsFragment.pli.pkgName + " " + listAppsFragment.getPkgMng().getPackageInfo(listAppsFragment.pli.pkgName, listAppsFragment.SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir + " " + listAppsFragment.getPkgMng().getPackageInfo(listAppsFragment.pli.pkgName, listAppsFragment.SETTINGS_VIEWSIZE_SMALL).applicationInfo.dataDir + " " + listAppsFragment.toolfilesdir + " " + listAppsFragment.api + LogCollector.LINE_SEPARATOR;
                            listAppsFragment.this.moveToSystem(listAppsFragment.pli.pkgName, cmd1);
                        } catch (NameNotFoundException e2) {
                            e2.printStackTrace();
                        }
                        try {
                            listAppsFragment.getConfig().edit().remove(listAppsFragment.pli.pkgName).commit();
                            listAppsFragment.plia.remove(listAppsFragment.pli.pkgName);
                            listAppsFragment.plia.notifyDataSetChanged();
                            listAppsFragment.refresh = true;
                        } catch (Exception e3) {
                        }
                        listAppsFragment.removeDialogLP(listAppsFragment.CHILD_NAME_SHEME_SELECT_ITEM);
                        return;
                    default:
                        return;
                }
            }
        };
        Utils.showDialogYesNo(pli.name, Utils.getText(C0149R.string.move_to_system), dialogClickListener, dialogClickListener, null);
    }

    public void move_to_internal_memory_click() {
        if (getConfig().getBoolean("vibration", false)) {
            this.vib = (Vibrator) getContext().getSystemService("vibrator");
            this.vib.vibrate(50);
        }
        OnClickListener dialogClickListener = new OnClickListener() {

            class C03561 implements Runnable {
                C03561() {
                }

                public void run() {
                    String apk_file = BuildConfig.VERSION_NAME;
                    try {
                        apk_file = listAppsFragment.getPkgMng().getPackageInfo(listAppsFragment.pli.pkgName, listAppsFragment.SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir;
                        System.out.println("Start move to internal" + apk_file);
                    } catch (Exception e) {
                    }
                    Utils utils = new Utils(BuildConfig.VERSION_NAME);
                    String[] strArr = new String[listAppsFragment.TITLE_LIST_ITEM];
                    strArr[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = "pm install -r -f -i com.android.vending " + apk_file;
                    final String cmd1 = utils.cmdRoot(strArr);
                    listAppsFragment.this.runToMain(new Runnable() {
                        public void run() {
                            if (cmd1.equals("~")) {
                                listAppsFragment.this.showMessage(Utils.getText(C0149R.string.error), Utils.getText(C0149R.string.message_error_move_to_internal) + listAppsFragment.errorOutput);
                            } else {
                                listAppsFragment.this.showMessage(listAppsFragment.pli.name, Utils.getText(C0149R.string.message_done_move_to_internal) + " " + cmd1);
                            }
                            new Utils("w").waitLP(2000);
                            listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                            listAppsFragment.plia.updateItem(listAppsFragment.pli.pkgName);
                            listAppsFragment.plia.notifyDataSetChanged();
                        }
                    });
                }
            }

            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case Zip4jConstants.ENC_NO_ENCRYPTION /*-1*/:
                        listAppsFragment.this.runWithWait(new C03561());
                        return;
                    default:
                        return;
                }
            }
        };
        Utils.showDialogYesNo(pli.name, Utils.getText(C0149R.string.move_to_internal_message), dialogClickListener, dialogClickListener, null);
    }

    public void move_to_sdcard_click() {
        if (getConfig().getBoolean("vibration", false)) {
            this.vib = (Vibrator) getContext().getSystemService("vibrator");
            this.vib.vibrate(50);
        }
        OnClickListener dialogClickListener = new OnClickListener() {

            class C03581 implements Runnable {
                C03581() {
                }

                public void run() {
                    String apk_file = BuildConfig.VERSION_NAME;
                    try {
                        apk_file = listAppsFragment.getPkgMng().getPackageInfo(listAppsFragment.pli.pkgName, listAppsFragment.SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir;
                        System.out.println("Start move to sdcard" + apk_file);
                    } catch (Exception e) {
                    }
                    Utils utils = new Utils(BuildConfig.VERSION_NAME);
                    String[] strArr = new String[listAppsFragment.TITLE_LIST_ITEM];
                    strArr[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = "pm install -r -s -i com.android.vending " + apk_file;
                    final String cmd1 = utils.cmdRoot(strArr);
                    listAppsFragment.this.runToMain(new Runnable() {
                        public void run() {
                            if (cmd1.equals("~")) {
                                listAppsFragment.this.showMessage(Utils.getText(C0149R.string.error), Utils.getText(C0149R.string.message_error_move_to_sdcard) + listAppsFragment.errorOutput);
                            } else {
                                listAppsFragment.this.showMessage(listAppsFragment.pli.name, Utils.getText(C0149R.string.message_done_move_to_sdcard) + " " + cmd1);
                            }
                            new Utils("w").waitLP(2000);
                            listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                            listAppsFragment.plia.updateItem(listAppsFragment.pli.pkgName);
                            listAppsFragment.plia.notifyDataSetChanged();
                        }
                    });
                }
            }

            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case Zip4jConstants.ENC_NO_ENCRYPTION /*-1*/:
                        listAppsFragment.this.runWithWait(new C03581());
                        return;
                    default:
                        return;
                }
            }
        };
        Utils.showDialogYesNo(pli.name, Utils.getText(C0149R.string.move_to_sdcard_message), dialogClickListener, dialogClickListener, null);
    }

    public void show_app_manager_click() {
        if (getConfig().getBoolean("vibration", false)) {
            this.vib = (Vibrator) getContext().getSystemService("vibrator");
            this.vib.vibrate(50);
        }
        Intent intent;
        if (VERSION.SDK_INT >= CREATEAPK_ADS_DIALOG) {
            intent = new Intent("android.settings.APPLICATION_DETAILS_SETTINGS", Uri.parse("package:" + pli.pkgName));
            intent.setFlags(AccessibilityNodeInfoCompat.ACTION_SET_SELECTION);
            startActivity(intent);
        } else {
            intent = new Intent("android.intent.action.VIEW");
            intent.setClassName(Common.SETTINGS_PKG, "com.android.settings.InstalledAppDetails");
            intent.putExtra("com.android.settings.ApplicationPkgName", pli.pkgName);
            intent.putExtra("pkg", pli.pkgName);
            intent.setFlags(AccessibilityNodeInfoCompat.ACTION_SET_SELECTION);
            startActivity(intent);
        }
        return_from_control_panel = true;
    }

    public void uninstall_click() {
        if (getConfig().getBoolean("vibration", false)) {
            this.vib = (Vibrator) getContext().getSystemService("vibrator");
            this.vib.vibrate(50);
        }
        OnClickListener dialogClickListener = new OnClickListener() {

            class C03611 implements Runnable {

                class C03591 implements Runnable {
                    C03591() {
                    }

                    public void run() {
                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                    }
                }

                class C03602 implements Runnable {
                    C03602() {
                    }

                    public void run() {
                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                    }
                }

                C03611() {
                }

                public void run() {
                    try {
                        String appapk = BuildConfig.VERSION_NAME;
                        try {
                            appapk = listAppsFragment.getPkgMng().getPackageInfo(listAppsFragment.pli.pkgName, listAppsFragment.SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir;
                        } catch (NameNotFoundException e) {
                        }
                        Utils utils = new Utils(BuildConfig.VERSION_NAME);
                        String[] strArr = new String[listAppsFragment.TITLE_LIST_ITEM];
                        strArr[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = "pm uninstall " + listAppsFragment.pli.pkgName;
                        utils.cmdRoot(strArr);
                        if (!appapk.equals(BuildConfig.VERSION_NAME)) {
                            Utils.run_all("rm " + Utils.getPlaceForOdex(appapk, false));
                        }
                        listAppsFragment.this.runToMain(new C03602());
                    } catch (Exception e2) {
                        System.out.println("Move to system " + e2);
                        listAppsFragment.this.runToMain(new C03591());
                    }
                }
            }

            class C03642 implements Runnable {

                class C03621 implements Runnable {
                    C03621() {
                    }

                    public void run() {
                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                        listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.unknown_error));
                    }
                }

                class C03632 implements Runnable {
                    C03632() {
                    }

                    public void run() {
                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                    }
                }

                C03642() {
                }

                public void run() {
                    String pkgName = BuildConfig.VERSION_NAME;
                    String appapk = BuildConfig.VERSION_NAME;
                    String datadir = BuildConfig.VERSION_NAME;
                    try {
                        pkgName = listAppsFragment.pli.pkgName;
                        appapk = listAppsFragment.getPkgMng().getPackageInfo(pkgName, listAppsFragment.SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir;
                        datadir = listAppsFragment.getPkgMng().getPackageInfo(pkgName, listAppsFragment.SETTINGS_VIEWSIZE_SMALL).applicationInfo.dataDir;
                        if (appapk.startsWith("/data/app") || appapk.startsWith("/mnt/asec/")) {
                            Utils utils = new Utils(BuildConfig.VERSION_NAME);
                            String[] strArr = new String[listAppsFragment.TITLE_LIST_ITEM];
                            strArr[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = "pm uninstall " + pkgName;
                            utils.cmdRoot(strArr);
                        }
                        appapk = listAppsFragment.getPkgMng().getPackageInfo(pkgName, listAppsFragment.SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir;
                        datadir = "empty";
                        datadir = listAppsFragment.getPkgMng().getPackageInfo(pkgName, listAppsFragment.SETTINGS_VIEWSIZE_SMALL).applicationInfo.dataDir;
                    } catch (NameNotFoundException e) {
                        e.printStackTrace();
                    }
                    try {
                        String cmd1 = listAppsFragment.dalvikruncommand + ".uninstall " + listAppsFragment.pli.pkgName + " " + appapk + " " + datadir + LogCollector.LINE_SEPARATOR;
                        String str = BuildConfig.VERSION_NAME;
                        Utils.kill(pkgName);
                        Utils.remount("/system", InternalZipConstants.WRITE_MODE);
                        Utils.run_all("chattr -ai " + appapk);
                        utils = new Utils(BuildConfig.VERSION_NAME);
                        strArr = new String[listAppsFragment.TITLE_LIST_ITEM];
                        strArr[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = cmd1;
                        System.out.println(utils.cmdRoot(strArr));
                        Utils.run_all("chmod 777 " + appapk);
                        Utils.run_all("rm '" + appapk + "'");
                        Utils.removePkgFromSystem(pkgName);
                        listAppsFragment.this.runToMain(new C03632());
                    } catch (Exception e2) {
                        System.out.println("LuckyPatcher Uninstall: " + e2);
                        listAppsFragment.this.runToMain(new C03621());
                    }
                }
            }

            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case Zip4jConstants.ENC_NO_ENCRYPTION /*-1*/:
                        try {
                            System.out.println("Start uninstall to " + listAppsFragment.patchData.appdir);
                        } catch (Exception e) {
                        }
                        try {
                            if (listAppsFragment.pli.system) {
                                if (listAppsFragment.su) {
                                    listAppsFragment.this.runWithWait(new C03642());
                                } else {
                                    listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.root_needed));
                                }
                                listAppsFragment.getConfig().edit().remove(listAppsFragment.pli.pkgName).commit();
                                listAppsFragment.plia.remove(listAppsFragment.pli.pkgName);
                                listAppsFragment.plia.notifyDataSetChanged();
                                listAppsFragment.removeDialogLP(listAppsFragment.CHILD_NAME_SHEME_SELECT_ITEM);
                                return;
                            }
                            if (listAppsFragment.su) {
                                listAppsFragment.this.runWithWait(new C03611());
                            } else {
                                Intent uninstallIntent = new Intent("android.intent.action.DELETE", Uri.parse("package:" + listAppsFragment.pli.pkgName));
                                uninstallIntent.setFlags(AccessibilityNodeInfoCompat.ACTION_SET_SELECTION);
                                listAppsFragment.this.startActivity(uninstallIntent);
                                listAppsFragment.refresh = true;
                            }
                            try {
                                listAppsFragment.getConfig().edit().remove(listAppsFragment.pli.pkgName).commit();
                                listAppsFragment.plia.remove(listAppsFragment.pli.pkgName);
                                listAppsFragment.plia.notifyDataSetChanged();
                            } catch (Exception e2) {
                            }
                            listAppsFragment.removeDialogLP(listAppsFragment.CHILD_NAME_SHEME_SELECT_ITEM);
                            return;
                        } catch (Exception e3) {
                            e3.printStackTrace();
                        }
                    default:
                        return;
                }
            }
        };
        Utils.showDialogYesNo(pli.name, Utils.getText(C0149R.string.uninstall), dialogClickListener, dialogClickListener, null);
    }

    public void contextmenu_click() {
        if (getConfig().getBoolean("vibration", false)) {
            this.vib = (Vibrator) getContext().getSystemService("vibrator");
            this.vib.vibrate(50);
        }
        try {
            removeDialogLP(CHILD_NAME_SHEME_SELECT_ITEM);
            contextlevelpatch0();
        } catch (Exception e) {
            System.out.println("LuckyPatcher (Context menu click): " + e);
            e.printStackTrace();
        }
    }

    public void contextmenutools_click() {
        if (getConfig().getBoolean("vibration", false)) {
            this.vib = (Vibrator) getContext().getSystemService("vibrator");
            this.vib.vibrate(50);
        }
        try {
            removeDialogLP(CHILD_NAME_SHEME_SELECT_ITEM);
            contextleveltools0();
        } catch (Exception e) {
            System.out.println("LuckyPatcher (Context menu click): " + e);
            e.printStackTrace();
        }
    }

    public void cleardata_click() {
        if (getConfig().getBoolean("vibration", false)) {
            this.vib = (Vibrator) getContext().getSystemService("vibrator");
            this.vib.vibrate(50);
        }
        if (su) {
            OnClickListener dialogClickListener = new OnClickListener() {

                class C03671 implements Runnable {

                    class C03651 implements Runnable {
                        C03651() {
                        }

                        public void run() {
                            listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                            listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.unknown_error));
                        }
                    }

                    class C03662 implements Runnable {
                        C03662() {
                        }

                        public void run() {
                            listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                        }
                    }

                    C03671() {
                    }

                    public void run() {
                        try {
                            Utils.kill(listAppsFragment.pli.pkgName);
                            listAppsFragment.str = BuildConfig.VERSION_NAME;
                            Utils utils = new Utils(BuildConfig.VERSION_NAME);
                            String[] strArr = new String[listAppsFragment.TITLE_LIST_ITEM];
                            strArr[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = listAppsFragment.dalvikruncommand + ".cleardata " + listAppsFragment.getPkgMng().getPackageInfo(listAppsFragment.pli.pkgName, listAppsFragment.SETTINGS_VIEWSIZE_SMALL).applicationInfo.dataDir;
                            listAppsFragment.str = utils.cmdRoot(strArr);
                        } catch (Exception e) {
                            System.out.println("LuckyPatcher Uninstall: " + e);
                            listAppsFragment.this.runToMain(new C03651());
                        }
                        listAppsFragment.this.runToMain(new C03662());
                    }
                }

                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case Zip4jConstants.ENC_NO_ENCRYPTION /*-1*/:
                            if (listAppsFragment.su) {
                                try {
                                    listAppsFragment.this.runWithWait(new C03671());
                                    return;
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    return;
                                }
                            }
                            listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.root_needed));
                            return;
                        default:
                            return;
                    }
                }
            };
            Utils.showDialogYesNo(pli.name, Utils.getText(C0149R.string.message_clear_data), dialogClickListener, dialogClickListener, null);
        } else if (VERSION.SDK_INT >= CREATEAPK_ADS_DIALOG) {
            intent = new Intent("android.settings.APPLICATION_DETAILS_SETTINGS", Uri.parse("package:" + pli.pkgName));
            intent.setFlags(AccessibilityNodeInfoCompat.ACTION_SET_SELECTION);
            startActivity(intent);
        } else {
            intent = new Intent("android.intent.action.VIEW");
            intent.setClassName(Common.SETTINGS_PKG, "com.android.settings.InstalledAppDetails");
            intent.putExtra("com.android.settings.ApplicationPkgName", pli.pkgName);
            intent.putExtra("pkg", pli.pkgName);
            intent.setFlags(AccessibilityNodeInfoCompat.ACTION_SET_SELECTION);
            startActivity(intent);
        }
    }

    public void toolbar_addfree_on() {
        runWithWait(new Runnable() {

            class C03681 implements Runnable {
                C03681() {
                }

                public void run() {
                    listAppsFragment.this.showMessageInfo(listAppsFragment.this.getContext(), Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.no_space_in_data_all));
                    listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                }
            }

            class C03692 implements Runnable {
                C03692() {
                }

                public void run() {
                    listAppsFragment.this.showMessageInfo(listAppsFragment.this.getContext(), Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.no_space_in_system_all));
                    listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                }
            }

            class C03703 implements Runnable {
                C03703() {
                }

                public void run() {
                    Toast.makeText(listAppsFragment.getInstance(), Utils.getText(C0149R.string.hosts_updated), listAppsFragment.TITLE_LIST_ITEM).show();
                    listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                }
            }

            class C03714 implements Runnable {
                C03714() {
                }

                public void run() {
                    listAppsFragment.this.showMessageInfo(listAppsFragment.this.getContext(), Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.error_out_of_memory));
                    listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                }
            }

            class C03725 implements Runnable {
                C03725() {
                }

                public void run() {
                    listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.unknown_error));
                    listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                }
            }

            class C03736 implements Runnable {
                C03736() {
                }

                public void run() {
                    listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                }
            }

            public void run() {
                File tmphosts = new File(listAppsFragment.getInstance().getDir("tmp", listAppsFragment.SETTINGS_VIEWSIZE_SMALL) + "/hosts.txt");
                File rawfile = new File(listAppsFragment.getInstance().getDir("tmp", listAppsFragment.SETTINGS_VIEWSIZE_SMALL) + "/raw.txt");
                Utils.getRawToFile(C0149R.raw.hosts, rawfile);
                Utils utils = new Utils(BuildConfig.VERSION_NAME);
                String[] strArr = new String[listAppsFragment.TITLE_LIST_ITEM];
                strArr[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = listAppsFragment.dalvikruncommand + ".AdsBlockON " + "/system/etc/hosts" + " " + tmphosts.getAbsolutePath() + " " + rawfile.getAbsolutePath() + " " + listAppsFragment.toolfilesdir;
                String result = utils.cmdRoot(strArr);
                System.out.println(result);
                if (result.contains("no_space_to_data")) {
                    listAppsFragment.this.runToMain(new C03681());
                }
                if (result.contains("no_space_to_system")) {
                    listAppsFragment.this.runToMain(new C03692());
                }
                if (result.contains("host updated!")) {
                    listAppsFragment.this.runToMain(new C03703());
                }
                if (result.contains("out.of.memory")) {
                    listAppsFragment.this.runToMain(new C03714());
                }
                if (result.contains("unknown error")) {
                    listAppsFragment.this.runToMain(new C03725());
                }
                listAppsFragment.this.runToMain(new C03736());
                if (rawfile.exists()) {
                    rawfile.delete();
                }
                if (tmphosts.exists()) {
                    tmphosts.delete();
                }
            }
        });
    }

    public void toolbar_addfree_off() {
        runWithWait(new Runnable() {

            class C03741 implements Runnable {
                C03741() {
                }

                public void run() {
                    listAppsFragment.this.showMessageInfo(listAppsFragment.this.getContext(), Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.no_space_in_data_all));
                    listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                }
            }

            class C03752 implements Runnable {
                C03752() {
                }

                public void run() {
                    listAppsFragment.this.showMessageInfo(listAppsFragment.this.getContext(), Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.no_space_in_system_all));
                    listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                }
            }

            class C03763 implements Runnable {
                C03763() {
                }

                public void run() {
                    Toast.makeText(listAppsFragment.getInstance(), Utils.getText(C0149R.string.hosts_updated), listAppsFragment.TITLE_LIST_ITEM).show();
                    listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                }
            }

            class C03774 implements Runnable {
                C03774() {
                }

                public void run() {
                    listAppsFragment.this.showMessageInfo(listAppsFragment.this.getContext(), Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.error_out_of_memory));
                    listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                }
            }

            class C03785 implements Runnable {
                C03785() {
                }

                public void run() {
                    listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.unknown_error));
                    listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                }
            }

            class C03796 implements Runnable {
                C03796() {
                }

                public void run() {
                    Toast.makeText(listAppsFragment.getInstance(), Utils.getText(C0149R.string.hosts_not_found_changes), listAppsFragment.TITLE_LIST_ITEM).show();
                    listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                }
            }

            public void run() {
                File tmphosts = new File(listAppsFragment.getInstance().getDir("tmp", listAppsFragment.SETTINGS_VIEWSIZE_SMALL) + "/hosts.txt");
                Utils utils = new Utils(BuildConfig.VERSION_NAME);
                String[] strArr = new String[listAppsFragment.TITLE_LIST_ITEM];
                strArr[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = listAppsFragment.dalvikruncommand + ".AdsBlockOFF " + "/system/etc/hosts" + " " + tmphosts.getAbsolutePath() + " " + listAppsFragment.toolfilesdir;
                String result = utils.cmdRoot(strArr);
                System.out.println(result);
                if (result.contains("no_space_to_data")) {
                    listAppsFragment.this.runToMain(new C03741());
                }
                if (result.contains("no_space_to_system")) {
                    listAppsFragment.this.runToMain(new C03752());
                }
                if (result.contains("host updated!")) {
                    listAppsFragment.this.runToMain(new C03763());
                }
                if (result.contains("out.of.memory")) {
                    listAppsFragment.this.runToMain(new C03774());
                }
                if (result.contains("unknown error")) {
                    listAppsFragment.this.runToMain(new C03785());
                }
                if (!result.contains("Changes remove from host")) {
                    listAppsFragment.this.runToMain(new C03796());
                }
            }
        });
    }

    public void toolbar_addfree_clear() {
        Utils.remount("/system", InternalZipConstants.WRITE_MODE);
        Utils.run_all("chattr -ai /system/etc/hosts");
        Utils utils = new Utils(BuildConfig.VERSION_NAME);
        String[] strArr = new String[TITLE_LIST_ITEM];
        strArr[SETTINGS_VIEWSIZE_SMALL] = dalvikruncommand + ".reworkhost";
        utils.cmdRoot(strArr);
        Utils.remount("/system", "ro");
        Toast.makeText(getInstance(), Utils.getText(C0149R.string.hosts_default), TITLE_LIST_ITEM).show();
    }

    public void getDir(String dirPath, ListView lv, boolean addLPpatch) {
        System.out.println("dir");
        this.myPath.setText("Location: " + dirPath);
        ArrayList<ItemFile> item = new ArrayList();
        File f = new File(dirPath);
        File[] files = f.listFiles();
        if (files == null) {
        }
        if (!dirPath.equals(this.root)) {
            item.add(new ItemFile(this.root));
            item.add(new ItemFile("../", f.getParent()));
        } else if (!this.root.equals(basepath) && addLPpatch) {
            item.add(new ItemFile(basepath));
        }
        int length = files.length;
        for (int i = SETTINGS_VIEWSIZE_SMALL; i < length; i += TITLE_LIST_ITEM) {
            File file = files[i];
            if (file.canRead() && (file.isDirectory() || file.toString().toLowerCase().endsWith(".apk") || file.toString().toLowerCase().endsWith(".odex") || file.toString().toLowerCase().endsWith(".jar") || file.toString().toLowerCase().endsWith(".oat"))) {
                item.add(new ItemFile(file.toString()));
            }
        }
        ArrayAdapter<ItemFile> fileList = new ArrayAdapter<ItemFile>(getContext(), C0149R.layout.file_browser_row, item) {
            public View getView(int position, View convertView, ViewGroup parent) {
                ItemFile current = (ItemFile) getItem(position);
                View row = convertView;
                if (row == null) {
                    row = ((LayoutInflater) listAppsFragment.getInstance().getSystemService("layout_inflater")).inflate(C0149R.layout.file_browser_row, parent, false);
                }
                TextView textView = (TextView) row.findViewById(C0149R.id.rowtext);
                textView.setTextAppearance(getContext(), listAppsFragment.getSizeText());
                textView.setText(current.file);
                ImageView icon = (ImageView) row.findViewById(C0149R.id.image);
                if (position == 0 || position == listAppsFragment.TITLE_LIST_ITEM) {
                    try {
                        if (getCount() > listAppsFragment.TITLE_LIST_ITEM && ((ItemFile) getItem(listAppsFragment.TITLE_LIST_ITEM)).file.equals("../")) {
                            icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.fb_back));
                            return row;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    } catch (OutOfMemoryError u) {
                        u.printStackTrace();
                        System.gc();
                    }
                }
                if (new File(current.full).isDirectory()) {
                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.fb_folder));
                } else {
                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.file_icon));
                }
                return row;
            }
        };
        fileList.sort(new byNameFile());
        lv.setAdapter(fileList);
        fileList.notifyDataSetChanged();
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void refresh_boot() {
        boot_pat = new ArrayList();
        String for_patch = getConfig().getString("patch_dalvik_on_boot_patterns", BuildConfig.VERSION_NAME);
        String pattern = BuildConfig.VERSION_NAME;
        if (!for_patch.equals(BuildConfig.VERSION_NAME)) {
            if (!for_patch.contains("patch1")) {
                if (!for_patch.contains("patch1")) {
                }
            }
            boot_pat.add(new PkgListItem(getContext(), Utils.getText(C0149R.string.android_patches_for_bootlist), Utils.getText(C0149R.string.android_patches_for_bootlist), SETTINGS_VIEWSIZE_SMALL, SETTINGS_VIEWSIZE_SMALL, SETTINGS_VIEWSIZE_SMALL, BuildConfig.VERSION_NAME, SETTINGS_VIEWSIZE_SMALL, SETTINGS_VIEWSIZE_SMALL, TITLE_LIST_ITEM, SETTINGS_VIEWSIZE_SMALL, TITLE_LIST_ITEM, SETTINGS_VIEWSIZE_SMALL, SETTINGS_VIEWSIZE_SMALL, SETTINGS_VIEWSIZE_SMALL, SETTINGS_VIEWSIZE_SMALL, SETTINGS_VIEWSIZE_SMALL, null, SETTINGS_VIEWSIZE_SMALL, SETTINGS_VIEWSIZE_SMALL, false, false, false));
        }
        try {
            File[] list = getContext().getDir("bootlist", SETTINGS_VIEWSIZE_SMALL).listFiles();
            int length = list.length;
            for (int i = SETTINGS_VIEWSIZE_SMALL; i < length; i += TITLE_LIST_ITEM) {
                File aList = list[i];
                if (!aList.getName().endsWith("bootlist")) {
                    try {
                        getPkgMng().getPackageInfo(aList.getName(), SETTINGS_VIEWSIZE_SMALL);
                    } catch (NameNotFoundException e) {
                        boot_pat.add(new PkgListItem(getContext(), aList.getName(), aList.getName(), SETTINGS_VIEWSIZE_SMALL, SETTINGS_VIEWSIZE_SMALL, SETTINGS_VIEWSIZE_SMALL, "\ufffd\ufffd\ufffd\ufffd\ufffd\ufffd\ufffd\ufffd\ufffd\ufffd \ufffd\ufffd\ufffd\ufffd\ufffd\ufffd\ufffd", SETTINGS_VIEWSIZE_SMALL, SETTINGS_VIEWSIZE_SMALL, TITLE_LIST_ITEM, SETTINGS_VIEWSIZE_SMALL, TITLE_LIST_ITEM, SETTINGS_VIEWSIZE_SMALL, SETTINGS_VIEWSIZE_SMALL, SETTINGS_VIEWSIZE_SMALL, SETTINGS_VIEWSIZE_SMALL, SETTINGS_VIEWSIZE_SMALL, null, SETTINGS_VIEWSIZE_SMALL, SETTINGS_VIEWSIZE_SMALL, false, false, false));
                    }
                }
            }
            int i2 = SETTINGS_VIEWSIZE_SMALL;
            while (i2 < plia.data.length) {
                if (plia.data[i2].boot_custom && new File(getContext().getDir("bootlist", SETTINGS_VIEWSIZE_SMALL) + InternalZipConstants.ZIP_FILE_SEPARATOR + plia.data[i2].pkgName).exists()) {
                    boot_pat.add(new PkgListItem(getContext(), plia.data[i2]));
                } else if (plia.data[i2].boot_ads || plia.data[i2].boot_lvl || plia.data[i2].boot_manual) {
                    boot_pat.add(new PkgListItem(getContext(), plia.data[i2]));
                }
                i2 += TITLE_LIST_ITEM;
            }
        } catch (Exception e2) {
            System.out.println("LuckyPatcher Error: Refresh BootList");
        }
    }

    public void contextlevel0() {
        runWithWait(new Runnable() {

            class C03833 implements Runnable {

                class C03821 implements OnClickListener {
                    C03821() {
                    }

                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case Zip4jConstants.ENC_NO_ENCRYPTION /*-1*/:
                                listAppsFragment com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment = listAppsFragment.frag;
                                listAppsFragment.removeDialogLP(listAppsFragment.CONTEXT_DIALOG);
                                com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment = listAppsFragment.frag;
                                listAppsFragment.showDialogLP(listAppsFragment.CONTEXT_DIALOG);
                                return;
                            default:
                                return;
                        }
                    }
                }

                C03833() {
                }

                public void run() {
                    listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                    if (listAppsFragment.pli.system) {
                        Utils.showDialogYesNo(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.dialog_system), new C03821(), null, null);
                        return;
                    }
                    listAppsFragment.removeDialogLP(listAppsFragment.CONTEXT_DIALOG);
                    listAppsFragment.showDialogLP(listAppsFragment.CONTEXT_DIALOG);
                }
            }

            public void run() {
                int length;
                int i;
                final ArrayList<Integer> s = new ArrayList();
                if (listAppsFragment.su) {
                    if (listAppsFragment.pli.custom) {
                        s.add(Integer.valueOf(C0149R.string.contextcustompatch));
                    }
                    Thread cont = new Thread(new Runnable() {
                        public void run() {
                            String prefs_dir = "shared_prefs";
                            String prefs_name = "com.android.vending.licensing.ServerManagedPolicy.xml";
                            boolean preffound = false;
                            if (Utils.exists("/data/data/" + listAppsFragment.pli.pkgName + InternalZipConstants.ZIP_FILE_SEPARATOR + prefs_dir + InternalZipConstants.ZIP_FILE_SEPARATOR + prefs_name)) {
                                listAppsFragment.this.prefs_file = new File("/data/data/" + listAppsFragment.pli.pkgName + InternalZipConstants.ZIP_FILE_SEPARATOR + prefs_dir + InternalZipConstants.ZIP_FILE_SEPARATOR + prefs_name);
                                preffound = true;
                            }
                            if (Utils.exists("/dbdata/databases/" + listAppsFragment.pli.pkgName + InternalZipConstants.ZIP_FILE_SEPARATOR + prefs_dir + InternalZipConstants.ZIP_FILE_SEPARATOR + prefs_name)) {
                                listAppsFragment.this.prefs_file = new File("/dbdata/databases/" + listAppsFragment.pli.pkgName + InternalZipConstants.ZIP_FILE_SEPARATOR + prefs_dir + InternalZipConstants.ZIP_FILE_SEPARATOR + prefs_name);
                                preffound = true;
                            }
                            if (preffound) {
                                s.add(Integer.valueOf(C0149R.string.new_method_lvl));
                            }
                        }
                    });
                    cont.start();
                    try {
                        cont.join();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    s.add(Integer.valueOf(C0149R.string.contextignore));
                    s.add(Integer.valueOf(C0149R.string.contextads));
                    s.add(Integer.valueOf(C0149R.string.context_support_patch));
                    s.add(Integer.valueOf(C0149R.string.context_change_components_menu));
                }
                s.add(Integer.valueOf(C0149R.string.createapk));
                if (!listAppsFragment.pli.system) {
                    s.add(Integer.valueOf(C0149R.string.cloneApplication));
                }
                if (listAppsFragment.pli.odex && listAppsFragment.su) {
                    s.add(Integer.valueOf(C0149R.string.contextdeodex));
                }
                if (listAppsFragment.pli.billing) {
                    s.add(Integer.valueOf(C0149R.string.context_remove_saved_purchaces));
                }
                boolean backupFound = false;
                try {
                    String[] files = new File(listAppsFragment.basepath + "/Backup").list();
                    length = files.length;
                    for (i = listAppsFragment.SETTINGS_VIEWSIZE_SMALL; i < length; i += listAppsFragment.TITLE_LIST_ITEM) {
                        String file = files[i];
                        if (new File(file).getName().startsWith(listAppsFragment.pli.pkgName + ".ver") || new File(file).getName().startsWith(listAppsFragment.pli.name + ".ver") || new File(file).getName().equals(listAppsFragment.pli.pkgName + ".apk")) {
                            backupFound = true;
                            break;
                        }
                    }
                } catch (Exception e2) {
                    System.out.println("LuckyPatcher error: backup files or directory not found!");
                }
                String[] list = new File(listAppsFragment.basepath + "/Backup/Data/" + listAppsFragment.pli.pkgName).list();
                boolean datafound = false;
                if (!(list == null || list.length == 0)) {
                    length = list.length;
                    for (i = listAppsFragment.SETTINGS_VIEWSIZE_SMALL; i < length; i += listAppsFragment.TITLE_LIST_ITEM) {
                        if (list[i].endsWith(".lpbkp")) {
                            datafound = true;
                        }
                    }
                }
                if (backupFound || datafound) {
                    if (backupFound && !listAppsFragment.pli.system) {
                        s.add(Integer.valueOf(C0149R.string.contextrestore));
                    } else if (listAppsFragment.su && datafound) {
                        s.add(Integer.valueOf(C0149R.string.contextrestore));
                    }
                }
                if (!listAppsFragment.pli.system || listAppsFragment.su) {
                    s.add(Integer.valueOf(C0149R.string.contextbackup));
                }
                if (listAppsFragment.su) {
                    if (!listAppsFragment.pli.system && listAppsFragment.pli.custom) {
                        s.add(Integer.valueOf(C0149R.string.contextboot));
                    }
                    if (!listAppsFragment.pli.system) {
                        s.add(Integer.valueOf(C0149R.string.contextlivepatch));
                    }
                }
                if (listAppsFragment.su) {
                    s.add(Integer.valueOf(C0149R.string.context_dexopt_app));
                    if (listAppsFragment.pli.enable) {
                        s.add(Integer.valueOf(C0149R.string.context_disable_package));
                    } else {
                        s.add(Integer.valueOf(C0149R.string.context_enable_package));
                    }
                }
                if (listAppsFragment.su) {
                    if (!listAppsFragment.pli.system) {
                        s.add(Integer.valueOf(C0149R.string.move_to_sys));
                    }
                    try {
                        String apk_file = listAppsFragment.getPkgMng().getPackageInfo(listAppsFragment.pli.pkgName, listAppsFragment.SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir;
                        if (listAppsFragment.pli.system && (apk_file.startsWith("/data/app") || apk_file.startsWith("/mnt/asec") || apk_file.startsWith("/data/priv-app"))) {
                            s.add(Integer.valueOf(C0149R.string.integrate_updates_to_system));
                        }
                    } catch (NameNotFoundException e3) {
                        e3.printStackTrace();
                    }
                }
                s.add(Integer.valueOf(C0149R.string.share_this_app));
                listAppsFragment.adapt = new ArrayAdapter<Integer>(listAppsFragment.this.getContext(), C0149R.layout.icon_context_menu, s) {
                    public View getView(int position, View convertView, ViewGroup parent) {
                        View row = convertView;
                        if (row == null) {
                            row = ((LayoutInflater) listAppsFragment.getInstance().getSystemService("layout_inflater")).inflate(C0149R.layout.icon_context_menu, parent, false);
                        }
                        TextView textView = (TextView) row.findViewById(C0149R.id.conttext);
                        ImageView icon = (ImageView) row.findViewById(C0149R.id.imgIcon);
                        icon.setImageDrawable(null);
                        textView.setTextAppearance(getContext(), listAppsFragment.getSizeText());
                        textView.setTextColor(-1);
                        try {
                            switch (((Integer) getItem(position)).intValue()) {
                                case C0149R.string.cloneApplication:
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.mipmap.clone));
                                    icon.setColorFilter(Color.parseColor("#ffcc66"), Mode.MULTIPLY);
                                    textView.setTextColor(Color.parseColor("#ffcc66"));
                                    break;
                                case C0149R.string.context_backup_data:
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.context_backup_data));
                                    break;
                                case C0149R.string.context_change_components_menu:
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.context_permissions));
                                    icon.setColorFilter(Color.parseColor("#cc99cc"), Mode.MULTIPLY);
                                    textView.setTextColor(Color.parseColor("#cc99cc"));
                                    break;
                                case C0149R.string.context_dexopt_app:
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.context_odex));
                                    icon.setColorFilter(Color.parseColor("#ffffbb"), Mode.MULTIPLY);
                                    textView.setTextColor(Color.parseColor("#ffffbb"));
                                    break;
                                case C0149R.string.context_disable_package:
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.context_lvl));
                                    icon.setColorFilter(Color.parseColor("#fe6c00"), Mode.MULTIPLY);
                                    textView.setTextColor(Color.parseColor("#fe6c00"));
                                    break;
                                case C0149R.string.context_enable_package:
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.context_patch_reboot));
                                    icon.setColorFilter(Color.parseColor("#c2f055"), Mode.MULTIPLY);
                                    textView.setTextColor(Color.parseColor("#c2f055"));
                                    break;
                                case C0149R.string.context_remove_saved_purchaces:
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.context_lvl));
                                    icon.setColorFilter(Color.parseColor("#66cc66"), Mode.MULTIPLY);
                                    textView.setTextColor(Color.parseColor("#66cc66"));
                                    break;
                                case C0149R.string.context_restore_data:
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.context_restore));
                                    break;
                                case C0149R.string.context_safe_sign_apk_permission:
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.context_permissions));
                                    break;
                                case C0149R.string.context_support_patch:
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.context_lvl));
                                    icon.setColorFilter(Color.parseColor("#66cc66"), Mode.MULTIPLY);
                                    textView.setTextColor(Color.parseColor("#66cc66"));
                                    break;
                                case C0149R.string.contextads:
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.context_ads));
                                    icon.setColorFilter(Color.parseColor("#99cccc"), Mode.MULTIPLY);
                                    textView.setTextColor(Color.parseColor("#99cccc"));
                                    break;
                                case C0149R.string.contextbackup:
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.context_backup));
                                    icon.setColorFilter(Color.parseColor("#cccccc"), Mode.MULTIPLY);
                                    textView.setTextColor(Color.parseColor("#cccccc"));
                                    break;
                                case C0149R.string.contextboot:
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.context_patch_reboot));
                                    icon.setColorFilter(Color.parseColor("#8f8f8f"), Mode.MULTIPLY);
                                    textView.setTextColor(Color.parseColor("#8f8f8f"));
                                    break;
                                case C0149R.string.contextcustompatch:
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.context_custom_patch));
                                    icon.setColorFilter(Color.parseColor("#ffff99"), Mode.MULTIPLY);
                                    textView.setTextColor(Color.parseColor("#ffff99"));
                                    break;
                                case C0149R.string.contextdeodex:
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.context_deodex));
                                    icon.setColorFilter(Color.parseColor("#9999cc"), Mode.MULTIPLY);
                                    textView.setTextColor(Color.parseColor("#9999cc"));
                                    break;
                                case C0149R.string.contextignore:
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.context_lvl));
                                    icon.setColorFilter(Color.parseColor("#66cc66"), Mode.MULTIPLY);
                                    textView.setTextColor(Color.parseColor("#66cc66"));
                                    break;
                                case C0149R.string.contextlivepatch:
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.context_patcher));
                                    icon.setColorFilter(Color.parseColor("#8f8f8f"), Mode.MULTIPLY);
                                    textView.setTextColor(Color.parseColor("#8f8f8f"));
                                    break;
                                case C0149R.string.contextrestore:
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.context_restore));
                                    icon.setColorFilter(Color.parseColor("#cccccc"), Mode.MULTIPLY);
                                    textView.setTextColor(Color.parseColor("#cccccc"));
                                    break;
                                case C0149R.string.createapk:
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.context_create_apk));
                                    icon.setColorFilter(Color.parseColor("#ffcc66"), Mode.MULTIPLY);
                                    textView.setTextColor(Color.parseColor("#ffcc66"));
                                    break;
                                case C0149R.string.integrate_updates_to_system:
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.move_to));
                                    icon.setColorFilter(Color.parseColor("#cccccc"), Mode.MULTIPLY);
                                    textView.setTextColor(Color.parseColor("#cccccc"));
                                    break;
                                case C0149R.string.move_to_sys:
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.move_to));
                                    icon.setColorFilter(Color.parseColor("#cccccc"), Mode.MULTIPLY);
                                    textView.setTextColor(Color.parseColor("#cccccc"));
                                    break;
                                case C0149R.string.new_method_lvl:
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.context_lvl));
                                    icon.setColorFilter(Color.parseColor("#66cc66"), Mode.MULTIPLY);
                                    textView.setTextColor(Color.parseColor("#66cc66"));
                                    break;
                                case C0149R.string.share_this_app:
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.context_share));
                                    icon.setColorFilter(Color.parseColor("#cccccc"), Mode.MULTIPLY);
                                    textView.setTextColor(Color.parseColor("#cccccc"));
                                    break;
                                case C0149R.string.tools_menu_block_internet:
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.context_ads));
                                    icon.setColorFilter(Color.parseColor("#fe6c00"), Mode.MULTIPLY);
                                    textView.setTextColor(Color.parseColor("#fe6c00"));
                                    break;
                                case C0149R.string.tools_menu_unblock_internet:
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.context_create_apk));
                                    icon.setColorFilter(Color.parseColor("#c2f055"), Mode.MULTIPLY);
                                    textView.setTextColor(Color.parseColor("#c2f055"));
                                    break;
                            }
                        } catch (OutOfMemoryError u) {
                            u.printStackTrace();
                            System.gc();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        textView.setCompoundDrawablePadding((int) ((5.0f * listAppsFragment.getRes().getDisplayMetrics().density) + 0.5f));
                        textView.setText(Utils.getText(((Integer) getItem(position)).intValue()));
                        textView.setTypeface(null, listAppsFragment.TITLE_LIST_ITEM);
                        return row;
                    }
                };
                listAppsFragment.this.runToMain(new C03833());
            }
        });
    }

    public void contextlevelpatch0() {
        runWithWait(new Runnable() {

            class C03913 implements Runnable {

                class C03901 implements OnClickListener {
                    C03901() {
                    }

                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case Zip4jConstants.ENC_NO_ENCRYPTION /*-1*/:
                                listAppsFragment com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment = listAppsFragment.frag;
                                listAppsFragment.removeDialogLP(listAppsFragment.CONTEXT_DIALOG);
                                com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment = listAppsFragment.frag;
                                listAppsFragment.showDialogLP(listAppsFragment.CONTEXT_DIALOG);
                                return;
                            default:
                                return;
                        }
                    }
                }

                C03913() {
                }

                public void run() {
                    listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                    if (listAppsFragment.pli.system) {
                        Utils.showDialogYesNo(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.dialog_system), new C03901(), null, null);
                        return;
                    }
                    listAppsFragment.removeDialogLP(listAppsFragment.CONTEXT_DIALOG);
                    listAppsFragment.showDialogLP(listAppsFragment.CONTEXT_DIALOG);
                }
            }

            public void run() {
                final ArrayList<Integer> s = new ArrayList();
                if (listAppsFragment.su) {
                    if (listAppsFragment.pli.custom) {
                        s.add(Integer.valueOf(C0149R.string.contextcustompatch));
                    }
                    String prefs_dir = "shared_prefs";
                    String prefs_name = "com.android.vending.licensing.ServerManagedPolicy.xml";
                    Thread cont = new Thread(new Runnable() {
                        public void run() {
                            String prefs_dir = "shared_prefs";
                            String prefs_name = "com.android.vending.licensing.ServerManagedPolicy.xml";
                            boolean preffound = false;
                            if (Utils.exists("/data/data/" + listAppsFragment.pli.pkgName + InternalZipConstants.ZIP_FILE_SEPARATOR + prefs_dir + InternalZipConstants.ZIP_FILE_SEPARATOR + prefs_name)) {
                                listAppsFragment.this.prefs_file = new File("/data/data/" + listAppsFragment.pli.pkgName + InternalZipConstants.ZIP_FILE_SEPARATOR + prefs_dir + InternalZipConstants.ZIP_FILE_SEPARATOR + prefs_name);
                                preffound = true;
                            }
                            if (Utils.exists("/dbdata/databases/" + listAppsFragment.pli.pkgName + InternalZipConstants.ZIP_FILE_SEPARATOR + prefs_dir + InternalZipConstants.ZIP_FILE_SEPARATOR + prefs_name)) {
                                listAppsFragment.this.prefs_file = new File("/dbdata/databases/" + listAppsFragment.pli.pkgName + InternalZipConstants.ZIP_FILE_SEPARATOR + prefs_dir + InternalZipConstants.ZIP_FILE_SEPARATOR + prefs_name);
                                preffound = true;
                            }
                            if (preffound) {
                                s.add(Integer.valueOf(C0149R.string.new_method_lvl));
                            }
                        }
                    });
                    cont.start();
                    try {
                        cont.join();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if (false) {
                        s.add(Integer.valueOf(C0149R.string.new_method_lvl));
                    }
                    s.add(Integer.valueOf(C0149R.string.contextignore));
                    s.add(Integer.valueOf(C0149R.string.contextads));
                    s.add(Integer.valueOf(C0149R.string.context_support_patch));
                    s.add(Integer.valueOf(C0149R.string.context_change_components_menu));
                }
                s.add(Integer.valueOf(C0149R.string.createapk));
                if (listAppsFragment.pli.odex && listAppsFragment.su) {
                    s.add(Integer.valueOf(C0149R.string.contextdeodex));
                }
                if (listAppsFragment.su) {
                    if (!listAppsFragment.pli.system && listAppsFragment.pli.custom) {
                        s.add(Integer.valueOf(C0149R.string.contextboot));
                    }
                    if (!listAppsFragment.pli.system) {
                        s.add(Integer.valueOf(C0149R.string.contextlivepatch));
                    }
                }
                listAppsFragment.adapt = new ArrayAdapter<Integer>(listAppsFragment.this.getContext(), C0149R.layout.icon_context_menu, s) {
                    public View getView(int position, View convertView, ViewGroup parent) {
                        View row = convertView;
                        if (row == null) {
                            row = ((LayoutInflater) listAppsFragment.getInstance().getSystemService("layout_inflater")).inflate(C0149R.layout.icon_context_menu, parent, false);
                        }
                        TextView textView = (TextView) row.findViewById(C0149R.id.conttext);
                        ImageView icon = (ImageView) row.findViewById(C0149R.id.imgIcon);
                        icon.setImageDrawable(null);
                        textView.setTextAppearance(getContext(), listAppsFragment.getSizeText());
                        textView.setTextColor(-1);
                        try {
                            switch (((Integer) getItem(position)).intValue()) {
                                case C0149R.string.context_change_components_menu:
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.context_permissions));
                                    icon.setColorFilter(Color.parseColor("#cc99cc"), Mode.MULTIPLY);
                                    textView.setTextColor(Color.parseColor("#cc99cc"));
                                    break;
                                case C0149R.string.context_safe_sign_apk_permission:
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.context_permissions));
                                    break;
                                case C0149R.string.context_support_patch:
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.context_lvl));
                                    icon.setColorFilter(Color.parseColor("#66cc66"), Mode.MULTIPLY);
                                    textView.setTextColor(Color.parseColor("#66cc66"));
                                    break;
                                case C0149R.string.contextads:
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.context_ads));
                                    icon.setColorFilter(Color.parseColor("#99cccc"), Mode.MULTIPLY);
                                    textView.setTextColor(Color.parseColor("#99cccc"));
                                    break;
                                case C0149R.string.contextboot:
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.context_patch_reboot));
                                    icon.setColorFilter(Color.parseColor("#8f8f8f"), Mode.MULTIPLY);
                                    textView.setTextColor(Color.parseColor("#8f8f8f"));
                                    break;
                                case C0149R.string.contextcustompatch:
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.context_custom_patch));
                                    icon.setColorFilter(Color.parseColor("#ffff99"), Mode.MULTIPLY);
                                    textView.setTextColor(Color.parseColor("#ffff99"));
                                    break;
                                case C0149R.string.contextdeodex:
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.context_deodex));
                                    icon.setColorFilter(Color.parseColor("#9999cc"), Mode.MULTIPLY);
                                    textView.setTextColor(Color.parseColor("#9999cc"));
                                    break;
                                case C0149R.string.contextignore:
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.context_lvl));
                                    icon.setColorFilter(Color.parseColor("#66cc66"), Mode.MULTIPLY);
                                    textView.setTextColor(Color.parseColor("#66cc66"));
                                    break;
                                case C0149R.string.contextlivepatch:
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.context_patcher));
                                    icon.setColorFilter(Color.parseColor("#8f8f8f"), Mode.MULTIPLY);
                                    textView.setTextColor(Color.parseColor("#8f8f8f"));
                                    break;
                                case C0149R.string.createapk:
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.context_create_apk));
                                    icon.setColorFilter(Color.parseColor("#ffcc66"), Mode.MULTIPLY);
                                    textView.setTextColor(Color.parseColor("#ffcc66"));
                                    break;
                                case C0149R.string.new_method_lvl:
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.context_lvl));
                                    icon.setColorFilter(Color.parseColor("#66cc66"), Mode.MULTIPLY);
                                    textView.setTextColor(Color.parseColor("#66cc66"));
                                    break;
                            }
                        } catch (OutOfMemoryError u) {
                            u.printStackTrace();
                            System.gc();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        textView.setCompoundDrawablePadding((int) ((5.0f * listAppsFragment.getRes().getDisplayMetrics().density) + 0.5f));
                        textView.setText(Utils.getText(((Integer) getItem(position)).intValue()));
                        textView.setTypeface(null, listAppsFragment.TITLE_LIST_ITEM);
                        return row;
                    }
                };
                listAppsFragment.this.runToMain(new C03913());
            }
        });
    }

    public void contextleveltools0() {
        runWithWait(new Runnable() {

            class C03942 implements Runnable {

                class C03931 implements OnClickListener {
                    C03931() {
                    }

                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case Zip4jConstants.ENC_NO_ENCRYPTION /*-1*/:
                                listAppsFragment com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment = listAppsFragment.frag;
                                listAppsFragment.removeDialogLP(listAppsFragment.CONTEXT_DIALOG);
                                com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment = listAppsFragment.frag;
                                listAppsFragment.showDialogLP(listAppsFragment.CONTEXT_DIALOG);
                                return;
                            default:
                                return;
                        }
                    }
                }

                C03942() {
                }

                public void run() {
                    listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                    if (listAppsFragment.pli.system) {
                        Utils.showDialogYesNo(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.dialog_system), new C03931(), null, null);
                        return;
                    }
                    listAppsFragment.removeDialogLP(listAppsFragment.CONTEXT_DIALOG);
                    listAppsFragment.showDialogLP(listAppsFragment.CONTEXT_DIALOG);
                }
            }

            public void run() {
                int i;
                int i2 = listAppsFragment.SETTINGS_VIEWSIZE_SMALL;
                ArrayList<Integer> s = new ArrayList();
                if (!listAppsFragment.pli.system) {
                    s.add(Integer.valueOf(C0149R.string.cloneApplication));
                }
                if (listAppsFragment.pli.billing) {
                    s.add(Integer.valueOf(C0149R.string.context_remove_saved_purchaces));
                }
                if (listAppsFragment.su) {
                    if (listAppsFragment.pli.enable) {
                        s.add(Integer.valueOf(C0149R.string.context_disable_package));
                    } else {
                        s.add(Integer.valueOf(C0149R.string.context_enable_package));
                    }
                    if (!listAppsFragment.pli.system) {
                        s.add(Integer.valueOf(C0149R.string.move_to_sys));
                    }
                    try {
                        String apk_file = listAppsFragment.getPkgMng().getPackageInfo(listAppsFragment.pli.pkgName, listAppsFragment.SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir;
                        if (listAppsFragment.pli.system && (apk_file.startsWith("/data/app") || apk_file.startsWith("/mnt/asec") || apk_file.startsWith("/data/priv-app"))) {
                            s.add(Integer.valueOf(C0149R.string.integrate_updates_to_system));
                        }
                    } catch (NameNotFoundException e) {
                        e.printStackTrace();
                    }
                }
                boolean backupFound = false;
                try {
                    String[] files = new File(listAppsFragment.basepath + "/Backup").list();
                    int length = files.length;
                    for (i = listAppsFragment.SETTINGS_VIEWSIZE_SMALL; i < length; i += listAppsFragment.TITLE_LIST_ITEM) {
                        String file = files[i];
                        if (new File(file).getName().startsWith(listAppsFragment.pli.pkgName) || new File(file).getName().startsWith(listAppsFragment.pli.name)) {
                            backupFound = true;
                            break;
                        }
                    }
                } catch (Exception e2) {
                    System.out.println("LuckyPatcher error: backup files or directory not found!");
                }
                String[] list = new File(listAppsFragment.basepath + "/Backup/Data/" + listAppsFragment.pli.pkgName).list();
                boolean datafound = false;
                if (!(list == null || list.length == 0)) {
                    i = list.length;
                    while (i2 < i) {
                        if (list[i2].endsWith(".lpbkp")) {
                            datafound = true;
                        }
                        i2 += listAppsFragment.TITLE_LIST_ITEM;
                    }
                }
                if (backupFound || datafound) {
                    if (backupFound && !listAppsFragment.pli.system) {
                        s.add(Integer.valueOf(C0149R.string.contextrestore));
                    } else if (listAppsFragment.su && datafound) {
                        s.add(Integer.valueOf(C0149R.string.contextrestore));
                    }
                }
                if (!listAppsFragment.pli.system || listAppsFragment.su) {
                    s.add(Integer.valueOf(C0149R.string.contextbackup));
                }
                if (listAppsFragment.su) {
                    s.add(Integer.valueOf(C0149R.string.context_dexopt_app));
                }
                if (listAppsFragment.pli.odex && listAppsFragment.su) {
                    s.add(Integer.valueOf(C0149R.string.contextdeodex));
                }
                s.add(Integer.valueOf(C0149R.string.share_this_app));
                listAppsFragment.adapt = new ArrayAdapter<Integer>(listAppsFragment.this.getContext(), C0149R.layout.icon_context_menu, s) {
                    public View getView(int position, View convertView, ViewGroup parent) {
                        View row = convertView;
                        if (row == null) {
                            row = ((LayoutInflater) listAppsFragment.getInstance().getSystemService("layout_inflater")).inflate(C0149R.layout.icon_context_menu, parent, false);
                        }
                        TextView textView = (TextView) row.findViewById(C0149R.id.conttext);
                        ImageView icon = (ImageView) row.findViewById(C0149R.id.imgIcon);
                        icon.setImageDrawable(null);
                        textView.setTextAppearance(getContext(), listAppsFragment.getSizeText());
                        textView.setTextColor(-1);
                        try {
                            switch (((Integer) getItem(position)).intValue()) {
                                case C0149R.string.cloneApplication:
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.mipmap.clone));
                                    icon.setColorFilter(Color.parseColor("#ffcc66"), Mode.MULTIPLY);
                                    textView.setTextColor(Color.parseColor("#ffcc66"));
                                    break;
                                case C0149R.string.context_backup_data:
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.context_backup_data));
                                    break;
                                case C0149R.string.context_dexopt_app:
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.context_odex));
                                    icon.setColorFilter(Color.parseColor("#ffffbb"), Mode.MULTIPLY);
                                    textView.setTextColor(Color.parseColor("#ffffbb"));
                                    break;
                                case C0149R.string.context_disable_package:
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.context_lvl));
                                    icon.setColorFilter(Color.parseColor("#fe6c00"), Mode.MULTIPLY);
                                    textView.setTextColor(Color.parseColor("#fe6c00"));
                                    break;
                                case C0149R.string.context_enable_package:
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.context_patch_reboot));
                                    icon.setColorFilter(Color.parseColor("#c2f055"), Mode.MULTIPLY);
                                    textView.setTextColor(Color.parseColor("#c2f055"));
                                    break;
                                case C0149R.string.context_remove_saved_purchaces:
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.context_lvl));
                                    icon.setColorFilter(Color.parseColor("#66cc66"), Mode.MULTIPLY);
                                    textView.setTextColor(Color.parseColor("#66cc66"));
                                    break;
                                case C0149R.string.context_restore_data:
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.context_restore));
                                    break;
                                case C0149R.string.contextbackup:
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.context_backup));
                                    icon.setColorFilter(Color.parseColor("#cccccc"), Mode.MULTIPLY);
                                    textView.setTextColor(Color.parseColor("#cccccc"));
                                    break;
                                case C0149R.string.contextdeodex:
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.context_deodex));
                                    icon.setColorFilter(Color.parseColor("#9999cc"), Mode.MULTIPLY);
                                    textView.setTextColor(Color.parseColor("#9999cc"));
                                    break;
                                case C0149R.string.contextrestore:
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.context_restore));
                                    icon.setColorFilter(Color.parseColor("#cccccc"), Mode.MULTIPLY);
                                    textView.setTextColor(Color.parseColor("#cccccc"));
                                    break;
                                case C0149R.string.integrate_updates_to_system:
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.move_to));
                                    icon.setColorFilter(Color.parseColor("#cccccc"), Mode.MULTIPLY);
                                    textView.setTextColor(Color.parseColor("#cccccc"));
                                    break;
                                case C0149R.string.move_to_sys:
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.move_to));
                                    icon.setColorFilter(Color.parseColor("#cccccc"), Mode.MULTIPLY);
                                    textView.setTextColor(Color.parseColor("#cccccc"));
                                    break;
                                case C0149R.string.share_this_app:
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.context_share));
                                    icon.setColorFilter(Color.parseColor("#cccccc"), Mode.MULTIPLY);
                                    textView.setTextColor(Color.parseColor("#cccccc"));
                                    break;
                                case C0149R.string.tools_menu_block_internet:
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.context_ads));
                                    icon.setColorFilter(Color.parseColor("#fe6c00"), Mode.MULTIPLY);
                                    textView.setTextColor(Color.parseColor("#fe6c00"));
                                    break;
                                case C0149R.string.tools_menu_unblock_internet:
                                    icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.context_create_apk));
                                    icon.setColorFilter(Color.parseColor("#c2f055"), Mode.MULTIPLY);
                                    textView.setTextColor(Color.parseColor("#c2f055"));
                                    break;
                            }
                        } catch (OutOfMemoryError u) {
                            u.printStackTrace();
                            System.gc();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        textView.setCompoundDrawablePadding((int) ((5.0f * listAppsFragment.getRes().getDisplayMetrics().density) + 0.5f));
                        textView.setText(Utils.getText(((Integer) getItem(position)).intValue()));
                        textView.setTypeface(null, listAppsFragment.TITLE_LIST_ITEM);
                        return row;
                    }
                };
                listAppsFragment.this.runToMain(new C03942());
            }
        });
    }

    public void contextfix() {
        ArrayList<Integer> s = new ArrayList();
        s.add(Integer.valueOf(C0149R.string.contextodex));
        if (!pli.system) {
            s.add(Integer.valueOf(C0149R.string.contextdeodex));
        }
        if (!pli.system) {
            s.add(Integer.valueOf(C0149R.string.contextboot));
        }
        adapt = new ArrayAdapter<Integer>(getContext(), C0149R.layout.contextmenu, s) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                TextView textView = (TextView) view.findViewById(C0149R.id.conttext);
                textView.setTextAppearance(getContext(), listAppsFragment.getSizeText());
                textView.setCompoundDrawablePadding((int) ((5.0f * listAppsFragment.getRes().getDisplayMetrics().density) + 0.5f));
                textView.setTextColor(-1);
                textView.setText(Utils.getText(((Integer) getItem(position)).intValue()));
                textView.setTypeface(null, listAppsFragment.TITLE_LIST_ITEM);
                return view;
            }
        };
    }

    public void contextboot() {
        ArrayList<Integer> s = new ArrayList();
        s.add(Integer.valueOf(C0149R.string.contextbootcustom));
        s.add(Integer.valueOf(C0149R.string.contextbootlvl));
        s.add(Integer.valueOf(C0149R.string.contextbootads));
        adapt = new ArrayAdapter<Integer>(getContext(), C0149R.layout.contextmenu, s) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                TextView textView = (TextView) view.findViewById(C0149R.id.conttext);
                textView.setTextAppearance(getContext(), listAppsFragment.getSizeText());
                textView.setCompoundDrawablePadding((int) ((5.0f * listAppsFragment.getRes().getDisplayMetrics().density) + 0.5f));
                textView.setTextColor(-1);
                textView.setText(Utils.getText(((Integer) getItem(position)).intValue()));
                textView.setTypeface(null, listAppsFragment.TITLE_LIST_ITEM);
                return view;
            }
        };
    }

    public void dexopt_app(final String appdir, final String pkgName, final String uid) {
        runWithWait(new Runnable() {

            class C03951 implements Runnable {
                C03951() {
                }

                public void run() {
                    try {
                        listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.error_odex_app_impossible));
                    } catch (Exception e) {
                    }
                }
            }

            class C03973 implements Runnable {
                C03973() {
                }

                public void run() {
                    listAppsFragment.plia.updateItem(listAppsFragment.pli.pkgName);
                    listAppsFragment.plia.notifyDataSetChanged();
                }
            }

            public void run() {
                try {
                    String filesdir = listAppsFragment.this.getContext().getFilesDir().getAbsolutePath();
                    String dexopt = Utils.getOdexForCreate(appdir, uid);
                    String uid = BuildConfig.VERSION_NAME;
                    if (appdir.startsWith("/system/")) {
                        uid = "0";
                    } else {
                        uid = String.valueOf(listAppsFragment.getPkgMng().getPackageInfo(pkgName, listAppsFragment.SETTINGS_VIEWSIZE_SMALL).applicationInfo.uid);
                    }
                    Utils.remount(appdir, InternalZipConstants.WRITE_MODE);
                    if (new File(dexopt).exists()) {
                        new File(dexopt).delete();
                        Utils.run_all("rm " + dexopt);
                    }
                    if (new File(dexopt).exists()) {
                        System.out.println("Ne udalos udalit");
                    }
                    Utils.kill(pkgName);
                    Utils.dexoptcopy();
                    Utils.run_all("chown 0.0 " + filesdir + "/dexopt-wrapper");
                    Utils.run_all("chown 0:0 " + filesdir + "/dexopt-wrapper");
                    Utils.run_all("chmod 777 " + filesdir + "/dexopt-wrapper");
                    Utils utils = new Utils(BuildConfig.VERSION_NAME);
                    String[] strArr = new String[listAppsFragment.TITLE_LIST_ITEM];
                    strArr[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = listAppsFragment.dalvikruncommand + ".optimizedex " + appdir + " " + uid + " " + Utils.getCurrentRuntimeValue();
                    String result = utils.cmdRoot(strArr);
                    System.out.println(result);
                    if (result.contains("chelpus_return_1") || result.contains("chelpus_return_4")) {
                        new File(dexopt).delete();
                        Utils.run_all("rm " + dexopt);
                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                        listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.error_odex_app));
                    }
                    if (result.contains("chelpus_return_10")) {
                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                        listAppsFragment.this.runToMain(new C03951());
                    }
                    if (appdir.startsWith("/system/")) {
                        Utils.run_all("chmod 0644 " + dexopt);
                        Utils.run_all("chown 0.0 " + dexopt);
                        Utils.run_all("chown 0:0 " + dexopt);
                    } else {
                        Utils.run_all("chmod 0644 " + dexopt);
                        Utils.run_all("chown 1000." + uid + " " + dexopt);
                        Utils.run_all("chown 1000:" + uid + " " + dexopt);
                    }
                } catch (final Exception e) {
                    e.printStackTrace();
                    listAppsFragment.this.runToMain(new Runnable() {
                        public void run() {
                            listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), e.toString());
                            listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                        }
                    });
                }
                listAppsFragment.this.runToMain(new C03973());
                listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
            }
        });
        System.out.println("LuckyAppManager: Start core.jar test!");
    }

    public void disable_package(final String pkgName, final boolean on) {
        runWithWait(new Runnable() {

            class C03981 implements Runnable {
                C03981() {
                }

                public void run() {
                    try {
                        listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.unknown_error));
                    } catch (Exception e) {
                    }
                }
            }

            class C04003 implements Runnable {
                C04003() {
                }

                public void run() {
                    listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                    listAppsFragment.plia.updateItem(listAppsFragment.pli.pkgName);
                    listAppsFragment.plia.notifyDataSetChanged();
                }
            }

            public void run() {
                try {
                    String result = BuildConfig.VERSION_NAME;
                    Utils utils;
                    String[] strArr;
                    if (on) {
                        utils = new Utils(BuildConfig.VERSION_NAME);
                        strArr = new String[listAppsFragment.TITLE_LIST_ITEM];
                        strArr[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = "pm enable " + pkgName;
                        result = utils.cmdRoot(strArr);
                    } else {
                        utils = new Utils(BuildConfig.VERSION_NAME);
                        strArr = new String[listAppsFragment.TITLE_LIST_ITEM];
                        strArr[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = "pm disable " + pkgName;
                        result = utils.cmdRoot(strArr);
                    }
                    System.out.println(result);
                    if (result.contains("failed")) {
                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                        listAppsFragment.this.runToMain(new C03981());
                    }
                } catch (final Exception e) {
                    e.printStackTrace();
                    listAppsFragment.this.runToMain(new Runnable() {
                        public void run() {
                            listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), e.toString());
                            listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                            listAppsFragment.plia.updateItem(listAppsFragment.pli.pkgName);
                            listAppsFragment.plia.notifyDataSetChanged();
                        }
                    });
                }
                listAppsFragment.this.runToMain(new C04003());
                Intent in = new Intent(listAppsFragment.getInstance(), AppDisablerWidget.class);
                in.setAction(AppDisablerWidget.ACTION_WIDGET_RECEIVER_Updater);
                listAppsFragment.getInstance().sendBroadcast(in);
            }
        });
        System.out.println("LuckyPatcher: Start core.jar test!");
    }

    public void context_backup_menu() {
        ArrayList<Integer> s = new ArrayList();
        if (!pli.system) {
            s.add(Integer.valueOf(C0149R.string.context_backup_apk));
            if (su) {
                s.add(Integer.valueOf(C0149R.string.context_backup_data));
            }
        } else if (su) {
            s.add(Integer.valueOf(C0149R.string.context_backup_data));
        }
        adapt = new ArrayAdapter<Integer>(getContext(), C0149R.layout.contextmenu, s) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                TextView textView = (TextView) view.findViewById(C0149R.id.conttext);
                textView.setTextAppearance(getContext(), listAppsFragment.getSizeText());
                textView.setCompoundDrawablePadding((int) ((5.0f * listAppsFragment.getRes().getDisplayMetrics().density) + 0.5f));
                textView.setTextColor(-1);
                textView.setText(Utils.getText(((Integer) getItem(position)).intValue()));
                textView.setTypeface(null, listAppsFragment.TITLE_LIST_ITEM);
                return view;
            }
        };
    }

    public void context_restore_menu() {
        int i;
        int i2 = SETTINGS_VIEWSIZE_SMALL;
        ArrayList<Integer> s = new ArrayList();
        boolean backupFound = false;
        try {
            String[] files = new File(basepath + "/Backup").list();
            int length = files.length;
            for (i = SETTINGS_VIEWSIZE_SMALL; i < length; i += TITLE_LIST_ITEM) {
                String file = files[i];
                if (new File(file).getName().startsWith(pli.pkgName) || new File(file).getName().startsWith(pli.name)) {
                    backupFound = true;
                    break;
                }
            }
        } catch (Exception e) {
            System.out.println("LuckyPatcher error: backup files or directory not found!");
        }
        String[] list = new File(basepath + "/Backup/Data/" + pli.pkgName).list();
        boolean datafound = false;
        if (!(list == null || list.length == 0)) {
            i = list.length;
            while (i2 < i) {
                if (list[i2].endsWith(".lpbkp")) {
                    datafound = true;
                }
                i2 += TITLE_LIST_ITEM;
            }
        }
        if (!pli.system) {
            if (backupFound) {
                s.add(Integer.valueOf(C0149R.string.context_restore_apk));
            }
            if (datafound && su) {
                s.add(Integer.valueOf(C0149R.string.context_restore_data));
            }
        } else if (datafound && su) {
            s.add(Integer.valueOf(C0149R.string.context_restore_data));
        }
        adapt = new ArrayAdapter<Integer>(getContext(), C0149R.layout.contextmenu, s) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                TextView textView = (TextView) view.findViewById(C0149R.id.conttext);
                textView.setTextAppearance(getContext(), listAppsFragment.getSizeText());
                textView.setCompoundDrawablePadding((int) ((5.0f * listAppsFragment.getRes().getDisplayMetrics().density) + 0.5f));
                textView.setTextColor(-1);
                textView.setText(Utils.getText(((Integer) getItem(position)).intValue()));
                textView.setTypeface(null, listAppsFragment.TITLE_LIST_ITEM);
                return view;
            }
        };
    }

    public void contextpermmenu() {
        ArrayList<Integer> s = new ArrayList();
        if (pli.system) {
            s.add(Integer.valueOf(C0149R.string.context_disableComponents));
            s.add(Integer.valueOf(C0149R.string.context_safe_sign_apk_permission));
        } else {
            s.add(Integer.valueOf(C0149R.string.context_disableComponents));
            s.add(Integer.valueOf(C0149R.string.context_hard_apk_permission));
            s.add(Integer.valueOf(C0149R.string.context_safe_sign_apk_permission));
            s.add(Integer.valueOf(C0149R.string.context_safe_apk_permission));
        }
        adapt = new ArrayAdapter<Integer>(getContext(), C0149R.layout.contextmenu, s) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                TextView textView = (TextView) view.findViewById(C0149R.id.conttext);
                textView.setTextAppearance(getContext(), listAppsFragment.getSizeText());
                textView.setCompoundDrawablePadding((int) ((5.0f * listAppsFragment.getRes().getDisplayMetrics().density) + 0.5f));
                textView.setTextColor(-1);
                textView.setText(Utils.getText(((Integer) getItem(position)).intValue()));
                textView.setTypeface(null, listAppsFragment.TITLE_LIST_ITEM);
                return view;
            }
        };
    }

    public void contextlvl() {
        ArrayList<Integer> s = new ArrayList();
        s.add(Integer.valueOf(C0149R.string.context_automodeslvl));
        if (!pli.system) {
            s.add(Integer.valueOf(C0149R.string.contextextpatch));
        }
        s.add(Integer.valueOf(C0149R.string.contextselpatt));
        adapt = new ArrayAdapter<Integer>(getContext(), C0149R.layout.contextmenu, s) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                TextView textView = (TextView) view.findViewById(C0149R.id.conttext);
                textView.setTextAppearance(getContext(), listAppsFragment.getSizeText());
                textView.setCompoundDrawablePadding((int) ((5.0f * listAppsFragment.getRes().getDisplayMetrics().density) + 0.5f));
                textView.setTextColor(-1);
                textView.setText(Utils.getText(((Integer) getItem(position)).intValue()));
                textView.setTypeface(null, listAppsFragment.TITLE_LIST_ITEM);
                return view;
            }
        };
    }

    public void contextads() {
        ArrayList<Integer> s = new ArrayList();
        List<ResolveInfo> serv = getPkgMng().queryIntentServices(new Intent("com.google.android.gms.ads.identifier.service.START"), SETTINGS_VIEWSIZE_SMALL);
        if (serv != null && serv.size() > 0) {
            s.add(Integer.valueOf(C0149R.string.context_disable_google_ads));
        }
        s.add(Integer.valueOf(C0149R.string.context_removeAds));
        s.add(Integer.valueOf(C0149R.string.context_disableActivity));
        adapt = new ArrayAdapter<Integer>(getContext(), C0149R.layout.contextmenu, s) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                TextView textView = (TextView) view.findViewById(C0149R.id.conttext);
                textView.setTextAppearance(getContext(), listAppsFragment.getSizeText());
                textView.setCompoundDrawablePadding((int) ((5.0f * listAppsFragment.getRes().getDisplayMetrics().density) + 0.5f));
                textView.setTextColor(-1);
                textView.setText(Utils.getText(((Integer) getItem(position)).intValue()));
                textView.setTypeface(null, listAppsFragment.TITLE_LIST_ITEM);
                return view;
            }
        };
    }

    public void contextsupport(boolean rebuild) {
        ArrayList<Patterns> s = new ArrayList();
        s.add(new Patterns(Utils.getText(C0149R.string.support_patch_lvl), true));
        s.add(new Patterns(Utils.getText(C0149R.string.support_patch_billing), true));
        s.add(new Patterns(Utils.getText(C0149R.string.support_patch_free_intent), false));
        if (!rebuild) {
            s.add(new Patterns(Utils.getText(C0149R.string.contextpattlvl4), false));
            s.add(new Patterns(Utils.getText(C0149R.string.contextpattlvl5), false));
            s.add(new Patterns(Utils.getText(C0149R.string.contextpattlvl6), false));
        }
        if (patch_adapt != null) {
            patch_adapt.notifyDataSetChanged();
        }
        final boolean z = rebuild;
        patch_adapt = new ArrayAdapter<Patterns>(getContext(), C0149R.layout.contextmenu, s) {
            TextView txtStatus;
            TextView txtTitle;

            public View getView(int position, View convertView, ViewGroup parent) {
                Patterns p = (Patterns) getItem(position);
                View row = convertView;
                row = ((LayoutInflater) listAppsFragment.getInstance().getSystemService("layout_inflater")).inflate(C0149R.layout.selectpatch, parent, false);
                this.txtTitle = (TextView) row.findViewById(C0149R.id.txtTitle);
                this.txtStatus = (TextView) row.findViewById(C0149R.id.txtStatus);
                this.txtTitle.setTextAppearance(getContext(), listAppsFragment.getSizeText());
                this.txtStatus.setTextAppearance(getContext(), listAppsFragment.getSizeText());
                CheckBox chk = (CheckBox) row.findViewById(C0149R.id.CheckBox1);
                chk.setChecked(p.Status);
                chk.setClickable(false);
                this.txtStatus.setTextAppearance(getContext(), 16973894);
                this.txtStatus.setTextColor(-7829368);
                this.txtTitle.setTextColor(-1);
                this.txtTitle.setText(((Patterns) getItem(position)).Name);
                this.txtTitle.setTypeface(null, listAppsFragment.TITLE_LIST_ITEM);
                String str2 = ((Patterns) getItem(position)).Name;
                if (position == listAppsFragment.SETTINGS_SORTBY_TIME || position == listAppsFragment.Custom_PATCH_DIALOG) {
                    this.txtTitle.setText(Utils.getColoredText(str2, "#ffffff00", "bold"));
                } else if (position == listAppsFragment.EXT_PATCH_DIALOG) {
                    this.txtTitle.setText(Utils.getColoredText(str2, "#ffff0000", "bold"));
                } else {
                    this.txtTitle.setText(Utils.getColoredText(str2, "#ff00ff00", "bold"));
                }
                if (position == 0) {
                    str2 = Utils.getText(C0149R.string.support_patch_lvl_descr);
                }
                if (position == listAppsFragment.TITLE_LIST_ITEM) {
                    str2 = Utils.getText(C0149R.string.support_patch_billing_descr);
                }
                if (position == listAppsFragment.SETTINGS_SORTBY_STATUS) {
                    str2 = Utils.getText(C0149R.string.support_patch_free_intent_descr);
                }
                if (!z) {
                    if (position == listAppsFragment.SETTINGS_SORTBY_TIME) {
                        str2 = Utils.getText(C0149R.string.contextpattlvl4_descr);
                    }
                    if (position == listAppsFragment.Custom_PATCH_DIALOG) {
                        str2 = Utils.getText(C0149R.string.contextpattlvl5_descr);
                    }
                    if (position == listAppsFragment.EXT_PATCH_DIALOG) {
                        str2 = Utils.getText(C0149R.string.contextpattlvl6_descr);
                    }
                }
                this.txtStatus.append(Utils.getColoredText(str2, "#ff888888", "italic"));
                return row;
            }
        };
    }

    public void contextcreateapk() {
        ArrayList<Integer> s = new ArrayList();
        if (pli.custom) {
            s.add(Integer.valueOf(C0149R.string.createapkcustom));
        }
        s.add(Integer.valueOf(C0149R.string.createapklvl));
        s.add(Integer.valueOf(C0149R.string.createapkads));
        s.add(Integer.valueOf(C0149R.string.createapksupport));
        s.add(Integer.valueOf(C0149R.string.context_crt_apk_permission));
        adapt = new ArrayAdapter<Integer>(getContext(), C0149R.layout.icon_context_menu, s) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View row = convertView;
                if (row == null) {
                    row = ((LayoutInflater) listAppsFragment.getInstance().getSystemService("layout_inflater")).inflate(C0149R.layout.icon_context_menu, parent, false);
                }
                TextView textView = (TextView) row.findViewById(C0149R.id.conttext);
                ImageView icon = (ImageView) row.findViewById(C0149R.id.imgIcon);
                icon.setImageDrawable(null);
                textView.setTextAppearance(getContext(), listAppsFragment.getSizeText());
                textView.setTextColor(-1);
                try {
                    switch (((Integer) getItem(position)).intValue()) {
                        case C0149R.string.context_crt_apk_permission:
                            icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.context_permissions));
                            icon.setColorFilter(Color.parseColor("#cc99cc"), Mode.MULTIPLY);
                            textView.setTextColor(Color.parseColor("#cc99cc"));
                            break;
                        case C0149R.string.createapkads:
                            icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.context_ads));
                            icon.setColorFilter(Color.parseColor("#99cccc"), Mode.MULTIPLY);
                            textView.setTextColor(Color.parseColor("#99cccc"));
                            break;
                        case C0149R.string.createapkcustom:
                            icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.context_custom_patch));
                            icon.setColorFilter(Color.parseColor("#ffff99"), Mode.MULTIPLY);
                            textView.setTextColor(Color.parseColor("#ffff99"));
                            break;
                        case C0149R.string.createapklvl:
                            icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.context_lvl));
                            icon.setColorFilter(Color.parseColor("#66cc66"), Mode.MULTIPLY);
                            textView.setTextColor(Color.parseColor("#66cc66"));
                            break;
                        case C0149R.string.createapksupport:
                            icon.setImageDrawable(listAppsFragment.getRes().getDrawable(C0149R.drawable.context_lvl));
                            icon.setColorFilter(Color.parseColor("#66cc66"), Mode.MULTIPLY);
                            textView.setTextColor(Color.parseColor("#66cc66"));
                            break;
                    }
                } catch (OutOfMemoryError u) {
                    u.printStackTrace();
                    System.gc();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                textView.setCompoundDrawablePadding((int) ((5.0f * listAppsFragment.getRes().getDisplayMetrics().density) + 0.5f));
                textView.setText(Utils.getText(((Integer) getItem(position)).intValue()));
                textView.setTypeface(null, listAppsFragment.TITLE_LIST_ITEM);
                return row;
            }
        };
    }

    public void context_remove_saved_purchases() {
        ArrayList<ItemsListItem> list = new DbHelper(getContext(), pli.pkgName).getItems();
        if (list.size() == 0) {
            list.add(new ItemsListItem(Utils.getText(C0149R.string.empty), BuildConfig.VERSION_NAME, BuildConfig.VERSION_NAME));
        }
        adapt = new ArrayAdapter<ItemsListItem>(getContext(), C0149R.layout.contextmenu, list) {
            ArrayAdapter<ItemsListItem> adapter = this;

            class C04041 implements View.OnClickListener {
                C04041() {
                }

                public void onClick(View v) {
                    System.out.println(((ItemsListItem) v.getTag()).itemID);
                    new DbHelper(listAppsFragment.getInstance(), listAppsFragment.pli.pkgName).deleteItem((ItemsListItem) v.getTag());
                    AnonymousClass73.this.adapter.remove((ItemsListItem) v.getTag());
                    if (AnonymousClass73.this.adapter.getCount() == 0) {
                        AnonymousClass73.this.adapter.add(new ItemsListItem(Utils.getText(C0149R.string.empty), BuildConfig.VERSION_NAME, BuildConfig.VERSION_NAME));
                    }
                    AnonymousClass73.this.adapter.notifyDataSetChanged();
                }
            }

            public View getView(int position, View convertView, ViewGroup parent) {
                ItemsListItem item = (ItemsListItem) getItem(position);
                View row = convertView;
                if (row == null) {
                    row = ((LayoutInflater) listAppsFragment.getInstance().getSystemService("layout_inflater")).inflate(C0149R.layout.purchases_for_delete, parent, false);
                }
                TextView textView = (TextView) row.findViewById(C0149R.id.conttext);
                Button button = (Button) row.findViewById(C0149R.id.button_del);
                if (item.itemID.equals(Utils.getText(C0149R.string.empty)) && item.pData.equals(BuildConfig.VERSION_NAME) && item.pSignature.equals(BuildConfig.VERSION_NAME)) {
                    button.setVisibility(listAppsFragment.CREATEAPK_DIALOG);
                } else {
                    button.setTag(item);
                    button.setOnClickListener(new C04041());
                }
                textView.setTextAppearance(getContext(), listAppsFragment.getSizeText());
                textView.setCompoundDrawablePadding((int) ((5.0f * listAppsFragment.getRes().getDisplayMetrics().density) + 0.5f));
                textView.setTextColor(-1);
                textView.setText(item.itemID);
                textView.setTypeface(null, listAppsFragment.TITLE_LIST_ITEM);
                return row;
            }
        };
    }

    public void contexttoolbarcreateapk() {
        ArrayList<Integer> s = new ArrayList();
        if (Utils.isCustomPatchesForPkg(Utils.getApkPackageInfo(rebuldApk).packageName)) {
            s.add(Integer.valueOf(C0149R.string.createapkcustom));
        }
        s.add(Integer.valueOf(C0149R.string.createapklvl));
        s.add(Integer.valueOf(C0149R.string.createapkads));
        s.add(Integer.valueOf(C0149R.string.createapksupport));
        s.add(Integer.valueOf(C0149R.string.context_crt_apk_permission));
        s.add(Integer.valueOf(C0149R.string.cloneApplication));
        adapt = new ArrayAdapter<Integer>(getContext(), C0149R.layout.contextmenu, s) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                TextView textView = (TextView) view.findViewById(C0149R.id.conttext);
                textView.setTextAppearance(getContext(), listAppsFragment.getSizeText());
                textView.setCompoundDrawablePadding((int) ((5.0f * listAppsFragment.getRes().getDisplayMetrics().density) + 0.5f));
                textView.setTextColor(-1);
                textView.setText(Utils.getText(((Integer) getItem(position)).intValue()));
                textView.setTypeface(null, listAppsFragment.TITLE_LIST_ITEM);
                return view;
            }
        };
    }

    public void contexttruble() {
        ArrayList<Integer> s = new ArrayList();
        s.add(Integer.valueOf(C0149R.string.installsupersu));
        s.add(Integer.valueOf(C0149R.string.updatebusybox));
        adapt = new ArrayAdapter<Integer>(getContext(), C0149R.layout.contextmenu, s) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                TextView textView = (TextView) view.findViewById(C0149R.id.conttext);
                textView.setTextAppearance(getContext(), listAppsFragment.getSizeText());
                textView.setCompoundDrawablePadding((int) ((5.0f * listAppsFragment.getRes().getDisplayMetrics().density) + 0.5f));
                textView.setTextColor(-1);
                textView.setText(Utils.getText(((Integer) getItem(position)).intValue()));
                textView.setTypeface(null, listAppsFragment.TITLE_LIST_ITEM);
                return view;
            }
        };
    }

    public void contextCorePatch() {
        runWithWait(new Runnable() {

            class C04071 implements Runnable {
                C04071() {
                }

                public void run() {
                    ArrayList<CoreItem> s = new ArrayList();
                    s.add(new CoreItem(Utils.getText(C0149R.string.contextcorepatch1), false));
                    s.add(new CoreItem(Utils.getText(C0149R.string.contextcorepatch2), false));
                    s.add(new CoreItem(Utils.getText(C0149R.string.contextcorepatch3), false));
                    s.add(new CoreItem(Utils.getText(C0149R.string.contextcorepatch4), false));
                    s.add(new CoreItem(Utils.getText(C0149R.string.contextcorepatchrestore), false));
                    s.add(new CoreItem(Utils.getText(C0149R.string.contextcorepatchrestore2), false));
                    if (listAppsFragment.patch_adapt != null) {
                        listAppsFragment.patch_adapt.notifyDataSetChanged();
                    }
                    listAppsFragment.patch_adapt = new ArrayAdapter<CoreItem>(listAppsFragment.this.getContext(), C0149R.layout.contextmenu, s) {
                        TextView txtStatus;
                        TextView txtStatusPatch;
                        TextView txtTitle;

                        class C04051 implements Runnable {
                            C04051() {
                            }

                            public void run() {
                                if (Utils.hasXposed()) {
                                    listAppsFragment.frag.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.xposed_notify));
                                }
                            }
                        }

                        public View getView(int position, View convertView, ViewGroup parent) {
                            if (listAppsFragment.xposedNotify) {
                                listAppsFragment com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment = listAppsFragment.frag;
                                listAppsFragment.patchAct.runOnUiThread(new C04051());
                                listAppsFragment.xposedNotify = false;
                            }
                            CoreItem p = (CoreItem) getItem(position);
                            View row = convertView;
                            row = ((LayoutInflater) listAppsFragment.getInstance().getSystemService("layout_inflater")).inflate(C0149R.layout.corepatch, parent, false);
                            this.txtTitle = (TextView) row.findViewById(C0149R.id.txtTitle);
                            this.txtStatus = (TextView) row.findViewById(C0149R.id.txtStatus);
                            this.txtStatusPatch = (TextView) row.findViewById(C0149R.id.txtStatusPatch);
                            this.txtTitle.setTextAppearance(getContext(), listAppsFragment.getSizeText());
                            this.txtStatus.setTextAppearance(getContext(), listAppsFragment.getSizeText());
                            this.txtStatusPatch.setTextAppearance(getContext(), listAppsFragment.getSizeText());
                            CheckBox chk = (CheckBox) row.findViewById(C0149R.id.CheckBox1);
                            chk.setChecked(p.Status);
                            if (position == 0) {
                                if (Utils.checkCoreJarPatch11()) {
                                    int i = listAppsFragment.SETTINGS_VIEWSIZE_SMALL;
                                    if (Utils.checkCoreJarPatch11()) {
                                        i = listAppsFragment.SETTINGS_VIEWSIZE_SMALL + listAppsFragment.TITLE_LIST_ITEM;
                                    }
                                    if (Utils.checkCoreJarPatch12()) {
                                        i += listAppsFragment.TITLE_LIST_ITEM;
                                    }
                                    if (i == listAppsFragment.SETTINGS_SORTBY_STATUS) {
                                        chk.setEnabled(false);
                                        p.disabled = true;
                                        this.txtStatusPatch.setText("2/2 " + Utils.getText(C0149R.string.core_patch_apply));
                                    }
                                    if (i == listAppsFragment.TITLE_LIST_ITEM) {
                                        this.txtStatusPatch.setText("1/2 " + Utils.getText(C0149R.string.core_patch_apply));
                                    }
                                } else if (listAppsFragment.fileNotSpace) {
                                    this.txtStatusPatch.setText(Utils.getText(C0149R.string.core_patch_nopatch_no_space1));
                                } else {
                                    this.txtStatusPatch.setText(Utils.getText(C0149R.string.core_patch_nopatch));
                                }
                            }
                            if (position == listAppsFragment.TITLE_LIST_ITEM) {
                                if (Utils.checkCoreJarPatch20()) {
                                    chk.setEnabled(false);
                                    p.disabled = true;
                                    this.txtStatusPatch.setText(Utils.getText(C0149R.string.core_patch_apply));
                                } else if (listAppsFragment.fileNotSpace) {
                                    this.txtStatusPatch.setText(Utils.getText(C0149R.string.core_patch_nopatch_no_space1));
                                } else {
                                    this.txtStatusPatch.setText(Utils.getText(C0149R.string.core_patch_nopatch));
                                }
                            }
                            if (position == listAppsFragment.SETTINGS_SORTBY_STATUS) {
                                if (listAppsFragment.filePatch3 || Utils.checkCoreJarPatch30(listAppsFragment.getPkgMng())) {
                                    chk.setEnabled(false);
                                    p.disabled = true;
                                    this.txtStatusPatch.setText(Utils.getText(C0149R.string.core_patch_apply));
                                } else if (listAppsFragment.fileNotSpace) {
                                    this.txtStatusPatch.setText(Utils.getText(C0149R.string.core_patch_nopatch_no_space1));
                                } else {
                                    this.txtStatusPatch.setText(Utils.getText(C0149R.string.core_patch_nopatch));
                                }
                            }
                            if (position == listAppsFragment.SETTINGS_SORTBY_TIME) {
                                chk.setEnabled(false);
                                if (listAppsFragment.filePatch3 || Utils.checkCoreJarPatch40()) {
                                    chk.setEnabled(false);
                                    p.disabled = true;
                                    this.txtStatusPatch.setText(Utils.getText(C0149R.string.core_patch_apply));
                                } else if (listAppsFragment.fileNotSpace) {
                                    this.txtStatusPatch.setText(Utils.getText(C0149R.string.core_patch_nopatch_no_space1));
                                } else {
                                    this.txtStatusPatch.setText(Utils.getText(C0149R.string.core_patch_nopatch));
                                }
                                this.txtStatusPatch.append(Utils.getColoredText(LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.contextcorepatch4_note), "#FF0000", "normal"));
                            }
                            if (position == listAppsFragment.Custom_PATCH_DIALOG) {
                                this.txtStatusPatch.setText(Utils.getColoredText(Utils.getText(C0149R.string.contextcorepatchrestore_descr), "#ff888888", "italic"));
                            }
                            if (position == listAppsFragment.EXT_PATCH_DIALOG) {
                                this.txtStatusPatch.setText(Utils.getColoredText(Utils.getText(C0149R.string.contextcorepatchrestore2_descr), "#ff888888", "italic"));
                            }
                            chk.setClickable(false);
                            this.txtStatus.setTextAppearance(getContext(), 16973894);
                            this.txtStatus.setTextColor(-7829368);
                            this.txtTitle.setTextColor(-1);
                            this.txtTitle.setText(((CoreItem) getItem(position)).Name);
                            this.txtTitle.setTypeface(null, listAppsFragment.TITLE_LIST_ITEM);
                            String str2 = ((CoreItem) getItem(position)).Name;
                            if (position == 0 || position == listAppsFragment.TITLE_LIST_ITEM) {
                                this.txtTitle.setText(Utils.getColoredText(str2, "#ff00ff00", "bold"));
                            } else if (position == listAppsFragment.Custom_PATCH_DIALOG || position == listAppsFragment.EXT_PATCH_DIALOG) {
                                this.txtTitle.setText(Utils.getColoredText(str2, "#ffff0000", "bold"));
                            } else {
                                this.txtTitle.setText(Utils.getColoredText(str2, "#ff00ff00", "bold"));
                            }
                            if (position == 0) {
                                str2 = Utils.getText(C0149R.string.contextcorepatch1_descr);
                            }
                            if (position == listAppsFragment.TITLE_LIST_ITEM) {
                                str2 = Utils.getText(C0149R.string.contextcorepatch2_descr);
                            }
                            if (position == listAppsFragment.SETTINGS_SORTBY_STATUS) {
                                str2 = Utils.getText(C0149R.string.contextcorepatch3_descr) + LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.contextcorepatch3_descr_plus);
                            }
                            if (position == listAppsFragment.SETTINGS_SORTBY_TIME) {
                                str2 = Utils.getText(C0149R.string.contextcorepatch4_descr);
                            }
                            if (position == listAppsFragment.Custom_PATCH_DIALOG) {
                                str2 = BuildConfig.VERSION_NAME;
                            }
                            if (position == listAppsFragment.EXT_PATCH_DIALOG) {
                                str2 = BuildConfig.VERSION_NAME;
                            }
                            this.txtStatus.append(Utils.getColoredText(str2, "#ff888888", "italic"));
                            return row;
                        }
                    };
                    listAppsFragment.removeDialogLP(listAppsFragment.CONTEXT_DIALOG_CorePatches);
                    listAppsFragment.showDialogLP(listAppsFragment.CONTEXT_DIALOG_CorePatches);
                    listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                }
            }

            public void run() {
                listAppsFragment.xposedNotify = true;
                try {
                    listAppsFragment.fileNotSpace = Utils.exists("/system/framework/not.space");
                    listAppsFragment.filePatch3 = Utils.exists("/system/framework/patch3.done");
                    listAppsFragment.this.runToMain(new C04071());
                } catch (Exception e) {
                    System.out.println("LuckyPatcher: handler Null.");
                }
            }
        });
        System.out.println("LuckyPatcher: Start core.jar test!");
    }

    public void contextXposedPatch() {
        runWithWait(new Runnable() {

            class C04091 implements Runnable {
                C04091() {
                }

                public void run() {
                    ArrayList<CoreItem> s = new ArrayList();
                    JSONObject settings = null;
                    try {
                        settings = Utils.readXposedParamBoolean();
                    } catch (JSONException e) {
                        e.printStackTrace();
                        s.add(new CoreItem(Utils.getText(C0149R.string.contextcorepatch1), true));
                        s.add(new CoreItem(Utils.getText(C0149R.string.contextcorepatch2), true));
                        s.add(new CoreItem(Utils.getText(C0149R.string.contextcorepatch3), true));
                        s.add(new CoreItem(Utils.getText(C0149R.string.contextcorepatch4), true));
                        s.add(new CoreItem(Utils.getText(C0149R.string.context_hide_lp), false));
                    }
                    if (settings != null) {
                        s.clear();
                        s.add(new CoreItem(Utils.getText(C0149R.string.contextcorepatch1), settings.optBoolean("patch1", true)));
                        s.add(new CoreItem(Utils.getText(C0149R.string.contextcorepatch2), settings.optBoolean("patch2", true)));
                        s.add(new CoreItem(Utils.getText(C0149R.string.contextcorepatch3), settings.optBoolean("patch3", true)));
                        s.add(new CoreItem(Utils.getText(C0149R.string.contextcorepatch4), settings.optBoolean("patch4", true)));
                        s.add(new CoreItem(Utils.getText(C0149R.string.context_hide_lp), settings.optBoolean("hide", false)));
                    }
                    if (listAppsFragment.patch_adapt != null) {
                        listAppsFragment.patch_adapt.notifyDataSetChanged();
                    }
                    listAppsFragment.patch_adapt = new ArrayAdapter<CoreItem>(listAppsFragment.this.getContext(), C0149R.layout.contextmenu, s) {
                        TextView txtStatus;
                        TextView txtTitle;

                        public View getView(int position, View convertView, ViewGroup parent) {
                            CoreItem p = (CoreItem) getItem(position);
                            View row = convertView;
                            row = ((LayoutInflater) listAppsFragment.getInstance().getSystemService("layout_inflater")).inflate(C0149R.layout.xposedpatch, parent, false);
                            this.txtTitle = (TextView) row.findViewById(C0149R.id.txtTitle);
                            this.txtStatus = (TextView) row.findViewById(C0149R.id.txtStatus);
                            this.txtTitle.setTextAppearance(getContext(), listAppsFragment.getSizeText());
                            this.txtStatus.setTextAppearance(getContext(), listAppsFragment.getSizeText());
                            CheckBox chk = (CheckBox) row.findViewById(C0149R.id.CheckBox1);
                            chk.setChecked(p.Status);
                            if (Utils.isXposedEnabled()) {
                                chk.setEnabled(true);
                            } else {
                                chk.setEnabled(false);
                            }
                            chk.setClickable(false);
                            this.txtStatus.setTextAppearance(getContext(), 16973894);
                            this.txtStatus.setTextColor(-7829368);
                            this.txtTitle.setTextColor(-1);
                            this.txtTitle.setText(((CoreItem) getItem(position)).Name);
                            this.txtTitle.setTypeface(null, listAppsFragment.TITLE_LIST_ITEM);
                            String str2 = ((CoreItem) getItem(position)).Name;
                            this.txtTitle.setText(Utils.getColoredText(str2, "#ff00ff00", "bold"));
                            if (position == 0) {
                                str2 = Utils.getText(C0149R.string.contextcorepatch1_descr);
                            }
                            if (position == listAppsFragment.TITLE_LIST_ITEM) {
                                str2 = Utils.getText(C0149R.string.contextcorepatch2_descr);
                            }
                            if (position == listAppsFragment.SETTINGS_SORTBY_STATUS) {
                                str2 = Utils.getText(C0149R.string.contextcorepatch3_descr) + LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.contextcorepatch3_descr_plus);
                            }
                            if (position == listAppsFragment.SETTINGS_SORTBY_TIME) {
                                str2 = Utils.getText(C0149R.string.contextcorepatch4_descr_xposed);
                            }
                            if (position == listAppsFragment.Custom_PATCH_DIALOG) {
                                str2 = Utils.getText(C0149R.string.context_hide_lp_descr);
                            }
                            this.txtStatus.append(Utils.getColoredText(str2, "#ff888888", "italic"));
                            return row;
                        }
                    };
                    listAppsFragment.removeDialogLP(listAppsFragment.CONTEXT_DIALOG_Xposed_Patches);
                    listAppsFragment.showDialogLP(listAppsFragment.CONTEXT_DIALOG_Xposed_Patches);
                    listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                }
            }

            public void run() {
                try {
                    listAppsFragment.this.runToMain(new C04091());
                } catch (Exception e) {
                    System.out.println("LuckyPatcher: handler Null.");
                }
            }
        });
        System.out.println("LuckyPatcher: Start core.jar test!");
    }

    public void backupselector() {
        ArrayList<File> s = new ArrayList();
        String[] files = new File(basepath + "/Backup").list();
        int length = files.length;
        for (int i = SETTINGS_VIEWSIZE_SMALL; i < length; i += TITLE_LIST_ITEM) {
            String file = files[i];
            if (new File(file).getName().startsWith(pli.pkgName + ".ver") || new File(file).getName().startsWith(pli.name + ".ver") || new File(file).getName().equals(pli.pkgName + ".apk")) {
                s.add(new File(basepath + "/Backup/" + file));
            }
        }
        adapt = new ArrayAdapter<File>(getContext(), C0149R.layout.contextmenu, s) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                TextView textView = (TextView) view.findViewById(C0149R.id.conttext);
                textView.setTextAppearance(getContext(), listAppsFragment.getSizeText());
                textView.setCompoundDrawablePadding((int) ((5.0f * listAppsFragment.getRes().getDisplayMetrics().density) + 0.5f));
                textView.setTextColor(-1);
                String filename = ((File) getItem(position)).getName();
                if (filename.equals(listAppsFragment.pli.pkgName + ".apk")) {
                    textView.setText(listAppsFragment.pli.pkgName);
                } else {
                    System.out.println(filename);
                    textView.setText(filename.replace(listAppsFragment.pli.pkgName, BuildConfig.VERSION_NAME).replace(".ver.", "Version: ").replace(".build.", " build: ").replace(".apk", BuildConfig.VERSION_NAME));
                }
                textView.setTypeface(null, listAppsFragment.TITLE_LIST_ITEM);
                return view;
            }
        };
    }

    public void connectToLicensing() {
        Exception e;
        mServiceConnL = new ServiceConnection() {

            class C06551 extends Stub {

                class C04101 implements Runnable {
                    C04101() {
                    }

                    public void run() {
                        listAppsFragment.this.showMessage(Utils.getText(C0149R.string.mod_market_check_title), Utils.getText(C0149R.string.mod_market_check_internet_off));
                    }
                }

                class C04112 implements Runnable {
                    C04112() {
                    }

                    public void run() {
                        listAppsFragment.this.showMessage(Utils.getText(C0149R.string.mod_market_check_title), Utils.getText(C0149R.string.mod_market_check_true));
                    }
                }

                class C04123 implements Runnable {
                    C04123() {
                    }

                    public void run() {
                        listAppsFragment.this.showMessage(Utils.getText(C0149R.string.mod_market_check_title), Utils.getText(C0149R.string.mod_market_check_failed));
                    }
                }

                class C04134 implements Runnable {
                    C04134() {
                    }

                    public void run() {
                        listAppsFragment.this.showMessage(Utils.getText(C0149R.string.mod_market_check_title), Utils.getText(C0149R.string.mod_market_check_failed));
                    }
                }

                C06551() {
                }

                public void verifyLicense(int responseCode1, String signedData1, String signature1) throws RemoteException {
                    System.out.println(responseCode1);
                    listAppsFragment.this.responseCode = responseCode1;
                    listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                    switch (responseCode1) {
                        case listAppsFragment.SETTINGS_VIEWSIZE_SMALL /*0*/:
                            if (signedData1 != null && signature1 != null && Utils.checkCoreJarPatch11() && Utils.checkCoreJarPatch12()) {
                                listAppsFragment.handler.post(new C04112());
                                break;
                            } else {
                                listAppsFragment.handler.post(new C04123());
                                break;
                            }
                            break;
                        case listAppsFragment.SETTINGS_SORTBY_TIME /*3*/:
                            listAppsFragment.handler.post(new C04101());
                            break;
                        default:
                            listAppsFragment.handler.post(new C04134());
                            break;
                    }
                    System.out.println(BuildConfig.VERSION_NAME + signedData1);
                    System.out.println(signature1);
                    listAppsFragment.this.cleanupService();
                }
            }

            public void onServiceDisconnected(ComponentName name) {
                System.out.println("Licensing service disconnected.");
                listAppsFragment.this.mServiceL = null;
                listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
            }

            public void onServiceConnected(ComponentName name, IBinder service) {
                System.out.println("Licensing service try to connect.");
                if (listAppsFragment.this.mServiceL == null) {
                    listAppsFragment.this.mServiceL = ILicensingService.Stub.asInterface(service);
                    try {
                        listAppsFragment.this.mServiceL.checkLicense(new Random().nextLong(), listAppsFragment.getInstance().getPackageName(), new C06551());
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        boolean bindResult;
        if (this.mServiceL == null) {
            try {
                Intent intent = new Intent(new String(Base64.decode("Y29tLmFuZHJvaWQudmVuZGluZy5saWNlbnNpbmcuSUxpY2Vuc2luZ1NlcnZpY2U=")));
                intent.setPackage(Common.GOOGLEPLAY_PKG);
                intent.putExtra("xexe", "lp");
                bindResult = false;
                try {
                    if (!getPkgMng().queryIntentServices(intent, SETTINGS_VIEWSIZE_SMALL).isEmpty()) {
                        Intent intent2 = intent;
                        for (ResolveInfo se : getPkgMng().queryIntentServices(intent, SETTINGS_VIEWSIZE_SMALL)) {
                            try {
                                if (se.serviceInfo.packageName == null || !se.serviceInfo.packageName.equals(Common.GOOGLEPLAY_PKG)) {
                                    intent = intent2;
                                } else {
                                    ComponentName component = new ComponentName(se.serviceInfo.packageName, se.serviceInfo.name);
                                    intent = new Intent();
                                    intent.setComponent(component);
                                    intent.putExtra("xexe", "lp");
                                    handler.post(new Runnable() {
                                        public void run() {
                                            listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                                            listAppsFragment.showDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                                            listAppsFragment.progress2.setCancelable(true);
                                            listAppsFragment.progress2.setMessage(Utils.getText(C0149R.string.wait));
                                        }
                                    });
                                    bindResult = getInstance().bindService(intent, mServiceConnL, TITLE_LIST_ITEM);
                                }
                                intent2 = intent;
                            } catch (Exception e2) {
                                e = e2;
                                intent = intent2;
                            }
                        }
                    }
                } catch (Exception e3) {
                    e = e3;
                }
                if (bindResult) {
                    cleanupService();
                    removeDialogLP(PROGRESS_DIALOG2);
                }
            } catch (SecurityException e4) {
                e4.printStackTrace();
                cleanupService();
                return;
            } catch (Base64DecoderException e5) {
                e5.printStackTrace();
                cleanupService();
                return;
            }
        }
        return;
        e.printStackTrace();
        removeDialogLP(PROGRESS_DIALOG2);
        if (bindResult) {
            cleanupService();
            removeDialogLP(PROGRESS_DIALOG2);
        }
    }

    private void cleanupService() {
        if (this.mServiceL != null) {
            try {
                getInstance().unbindService(mServiceConnL);
            } catch (IllegalArgumentException e) {
            }
            this.mServiceL = null;
        }
    }

    public void mod_market_check() {
        new Thread(new Runnable() {
            public void run() {
                listAppsFragment.this.connectToLicensing();
            }
        }).start();
    }

    public void custompatchselector() {
        ArrayList<File> s;
        if (rebuldApk.equals(BuildConfig.VERSION_NAME)) {
            s = Utils.getCustomPatchesForPkg(pli.pkgName);
        } else {
            s = Utils.getCustomPatchesForPkg(Utils.getApkInfo(rebuldApk).packageName);
        }
        adapt = new ArrayAdapter<File>(getContext(), C0149R.layout.contextmenu, s) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                TextView textView = (TextView) view.findViewById(C0149R.id.conttext);
                textView.setTextAppearance(getContext(), listAppsFragment.getSizeText());
                textView.setCompoundDrawablePadding((int) ((5.0f * listAppsFragment.getRes().getDisplayMetrics().density) + 0.5f));
                textView.setTextColor(-1);
                textView.setText(((File) getItem(position)).getName().replace(".txt", BuildConfig.VERSION_NAME).replace("%ALL%_", BuildConfig.VERSION_NAME).replace("_%ALL%", BuildConfig.VERSION_NAME));
                textView.setTypeface(null, listAppsFragment.TITLE_LIST_ITEM);
                return view;
            }
        };
    }

    public void contextselpatch() {
        ArrayList<Patterns> s = new ArrayList();
        s.add(new Patterns(Utils.getText(C0149R.string.contextpatt1), false));
        s.add(new Patterns(Utils.getText(C0149R.string.contextpatt2), false));
        s.add(new Patterns(Utils.getText(C0149R.string.contextpatt3), false));
        s.add(new Patterns(Utils.getText(C0149R.string.contextpatt4), false));
        s.add(new Patterns(Utils.getText(C0149R.string.contextpatt5), false));
        s.add(new Patterns(Utils.getText(C0149R.string.contextpatt6), false));
        s.add(new Patterns(Utils.getText(C0149R.string.contextpattlvl4), false));
        s.add(new Patterns(Utils.getText(C0149R.string.contextpattlvl5), false));
        s.add(new Patterns(Utils.getText(C0149R.string.contextpattlvl6), false));
        if (patch_adapt != null) {
            patch_adapt.notifyDataSetChanged();
        }
        patch_adapt = new ArrayAdapter<Patterns>(getContext(), C0149R.layout.contextmenu, s) {
            TextView txtStatus;
            TextView txtTitle;

            public View getView(int position, View convertView, ViewGroup parent) {
                Patterns p = (Patterns) getItem(position);
                View row = convertView;
                row = ((LayoutInflater) listAppsFragment.getInstance().getSystemService("layout_inflater")).inflate(C0149R.layout.selectpatch, parent, false);
                this.txtTitle = (TextView) row.findViewById(C0149R.id.txtTitle);
                this.txtStatus = (TextView) row.findViewById(C0149R.id.txtStatus);
                this.txtTitle.setTextAppearance(getContext(), listAppsFragment.getSizeText());
                this.txtStatus.setTextAppearance(getContext(), listAppsFragment.getSizeText());
                CheckBox chk = (CheckBox) row.findViewById(C0149R.id.CheckBox1);
                chk.setChecked(p.Status);
                chk.setClickable(false);
                this.txtStatus.setTextAppearance(getContext(), 16973894);
                this.txtStatus.setTextColor(-7829368);
                this.txtTitle.setTextColor(-1);
                this.txtTitle.setText(((Patterns) getItem(position)).Name);
                this.txtTitle.setTypeface(null, listAppsFragment.TITLE_LIST_ITEM);
                String str2 = ((Patterns) getItem(position)).Name;
                if (position == listAppsFragment.CHILD_NAME_SHEME_SELECT_ITEM || position == listAppsFragment.CONTEXT_DIALOG) {
                    this.txtTitle.setText(Utils.getColoredText(str2, "#ffffff00", "bold"));
                } else if (position == listAppsFragment.CREATEAPK_DIALOG) {
                    this.txtTitle.setText(Utils.getColoredText(str2, "#ffff0000", "bold"));
                } else {
                    this.txtTitle.setText(Utils.getColoredText(str2, "#ff00ff00", "bold"));
                }
                if (position == 0) {
                    str2 = Utils.getText(C0149R.string.contextpatt1_descr);
                }
                if (position == listAppsFragment.TITLE_LIST_ITEM) {
                    str2 = Utils.getText(C0149R.string.contextpatt2_descr);
                }
                if (position == listAppsFragment.SETTINGS_SORTBY_STATUS) {
                    str2 = Utils.getText(C0149R.string.contextpatt3_descr);
                }
                if (position == listAppsFragment.SETTINGS_SORTBY_TIME) {
                    str2 = Utils.getText(C0149R.string.contextpatt4_descr);
                }
                if (position == listAppsFragment.Custom_PATCH_DIALOG) {
                    str2 = Utils.getText(C0149R.string.contextpatt5_descr);
                }
                if (position == listAppsFragment.EXT_PATCH_DIALOG) {
                    str2 = Utils.getText(C0149R.string.contextpatt6_descr);
                }
                if (position == listAppsFragment.CHILD_NAME_SHEME_SELECT_ITEM || position == listAppsFragment.CONTEXT_DIALOG || position == listAppsFragment.CREATEAPK_DIALOG) {
                    str2 = BuildConfig.VERSION_NAME;
                }
                this.txtStatus.append(Utils.getColoredText(str2, "#ff888888", "italic"));
                return row;
            }
        };
    }

    public void contextselpatchlvl(boolean installpatch) {
        ArrayList<Patterns> s = new ArrayList();
        s.add(new Patterns(Utils.getText(C0149R.string.contextpattlvl1), true));
        s.add(new Patterns(Utils.getText(C0149R.string.contextpattlvl2), false));
        s.add(new Patterns(Utils.getText(C0149R.string.contextpattlvl3), false));
        s.add(new Patterns(Utils.getText(C0149R.string.contextpattamazon), false));
        s.add(new Patterns(Utils.getText(C0149R.string.contextpattsamsung), false));
        s.add(new Patterns(Utils.getText(C0149R.string.context_dependencies), false));
        if (installpatch) {
            s.add(new Patterns(Utils.getText(C0149R.string.contextpattlvl4), false));
        }
        if (installpatch) {
            s.add(new Patterns(Utils.getText(C0149R.string.contextpattlvl5), false));
        }
        if (installpatch) {
            s.add(new Patterns(Utils.getText(C0149R.string.contextpattlvl6), false));
        }
        if (patch_adapt != null) {
            patch_adapt.notifyDataSetChanged();
        }
        final boolean z = installpatch;
        patch_adapt = new ArrayAdapter<Patterns>(getContext(), C0149R.layout.contextmenu, s) {
            TextView txtStatus;
            TextView txtTitle;

            public View getView(int position, View convertView, ViewGroup parent) {
                Patterns p = (Patterns) getItem(position);
                View row = convertView;
                row = ((LayoutInflater) listAppsFragment.getInstance().getSystemService("layout_inflater")).inflate(C0149R.layout.selectpatch, parent, false);
                this.txtTitle = (TextView) row.findViewById(C0149R.id.txtTitle);
                this.txtStatus = (TextView) row.findViewById(C0149R.id.txtStatus);
                this.txtTitle.setTextAppearance(getContext(), listAppsFragment.getSizeText());
                this.txtStatus.setTextAppearance(getContext(), listAppsFragment.getSizeText());
                CheckBox chk = (CheckBox) row.findViewById(C0149R.id.CheckBox1);
                chk.setChecked(p.Status);
                chk.setClickable(false);
                this.txtStatus.setTextAppearance(getContext(), 16973894);
                this.txtStatus.setTextColor(-7829368);
                this.txtTitle.setTextColor(-1);
                this.txtTitle.setText(((Patterns) getItem(position)).Name);
                this.txtTitle.setTypeface(null, listAppsFragment.TITLE_LIST_ITEM);
                String str2 = ((Patterns) getItem(position)).Name;
                if ((position == listAppsFragment.CHILD_NAME_SHEME_SELECT_ITEM || position == listAppsFragment.CONTEXT_DIALOG) && z) {
                    this.txtTitle.setText(Utils.getColoredText(str2, "#ffffff00", "bold"));
                } else if (position == listAppsFragment.CREATEAPK_DIALOG && z) {
                    this.txtTitle.setText(Utils.getColoredText(str2, "#ffff0000", "bold"));
                } else {
                    this.txtTitle.setText(Utils.getColoredText(str2, "#ff00ff00", "bold"));
                }
                if (position == 0) {
                    str2 = Utils.getText(C0149R.string.contextpattlvl1_descr);
                }
                if (position == listAppsFragment.TITLE_LIST_ITEM) {
                    str2 = Utils.getText(C0149R.string.contextpattlvl2_descr);
                }
                if (position == listAppsFragment.SETTINGS_SORTBY_STATUS) {
                    str2 = Utils.getText(C0149R.string.contextpattlvl3_descr);
                }
                if (position == listAppsFragment.SETTINGS_SORTBY_TIME) {
                    str2 = Utils.getText(C0149R.string.contextpattamazon_descr);
                }
                if (position == listAppsFragment.Custom_PATCH_DIALOG) {
                    str2 = Utils.getText(C0149R.string.contextpattsamsung_descr);
                }
                if (position == listAppsFragment.EXT_PATCH_DIALOG) {
                    str2 = Utils.getText(C0149R.string.contextpattdependencies_descr);
                }
                if (position == listAppsFragment.CHILD_NAME_SHEME_SELECT_ITEM && z) {
                    str2 = Utils.getText(C0149R.string.contextpattlvl4_descr);
                }
                if (position == listAppsFragment.CONTEXT_DIALOG && z) {
                    str2 = Utils.getText(C0149R.string.contextpattlvl5_descr);
                }
                if (position == listAppsFragment.CREATEAPK_DIALOG && z) {
                    str2 = Utils.getText(C0149R.string.contextpattlvl6_descr);
                }
                this.txtStatus.append(Utils.getColoredText(str2, "#ff888888", "italic"));
                return row;
            }
        };
    }

    public void contextselpatchads(boolean installpatch) {
        ArrayList<Patterns> s = new ArrayList();
        s.add(new Patterns(Utils.getText(C0149R.string.contextblockfileads), true));
        s.add(new Patterns(Utils.getText(C0149R.string.contextpattads1), true));
        s.add(new Patterns(Utils.getText(C0149R.string.contextpattads2), false));
        s.add(new Patterns(Utils.getText(C0149R.string.contextpattads3), true));
        s.add(new Patterns(Utils.getText(C0149R.string.contextpattads5), false));
        s.add(new Patterns(Utils.getText(C0149R.string.contextpattads6), true));
        s.add(new Patterns(Utils.getText(C0149R.string.contextpattads4), true));
        s.add(new Patterns(Utils.getText(C0149R.string.context_dependencies), false));
        s.add(new Patterns(Utils.getText(C0149R.string.contextfulloffline), false));
        if (installpatch) {
            s.add(new Patterns(Utils.getText(C0149R.string.contextpattlvl4), false));
        }
        if (installpatch) {
            s.add(new Patterns(Utils.getText(C0149R.string.contextpattlvl5), false));
        }
        if (installpatch) {
            s.add(new Patterns(Utils.getText(C0149R.string.contextpattlvl6), false));
        }
        if (patch_adapt != null) {
            patch_adapt.notifyDataSetChanged();
        }
        final boolean z = installpatch;
        patch_adapt = new ArrayAdapter<Patterns>(getContext(), C0149R.layout.contextmenu, s) {
            TextView txtStatus;
            TextView txtTitle;

            public View getView(int position, View convertView, ViewGroup parent) {
                Patterns p = (Patterns) getItem(position);
                View row = convertView;
                row = ((LayoutInflater) listAppsFragment.getInstance().getSystemService("layout_inflater")).inflate(C0149R.layout.selectpatch, parent, false);
                this.txtTitle = (TextView) row.findViewById(C0149R.id.txtTitle);
                this.txtStatus = (TextView) row.findViewById(C0149R.id.txtStatus);
                this.txtTitle.setTextAppearance(getContext(), listAppsFragment.getSizeText());
                this.txtStatus.setTextAppearance(getContext(), listAppsFragment.getSizeText());
                CheckBox chk = (CheckBox) row.findViewById(C0149R.id.CheckBox1);
                chk.setChecked(p.Status);
                chk.setClickable(false);
                this.txtStatus.setTextAppearance(getContext(), 16973894);
                this.txtStatus.setTextColor(-7829368);
                this.txtTitle.setTextColor(-1);
                this.txtTitle.setText(((Patterns) getItem(position)).Name);
                this.txtTitle.setTypeface(null, listAppsFragment.TITLE_LIST_ITEM);
                String str2 = ((Patterns) getItem(position)).Name;
                if (position == listAppsFragment.CREATEAPK_ADS_DIALOG || position == listAppsFragment.PERM_CONTEXT_DIALOG) {
                    this.txtTitle.setText(Utils.getColoredText(str2, "#ffffff00", "bold"));
                } else if (position == listAppsFragment.PROGRESS_DIALOG2) {
                    this.txtTitle.setText(Utils.getColoredText(str2, "#ffff0000", "bold"));
                } else {
                    this.txtTitle.setText(Utils.getColoredText(str2, "#ff00ff00", "bold"));
                }
                if (position == 0) {
                    str2 = Utils.getText(C0149R.string.contextblockfileads_descr);
                }
                if (position == listAppsFragment.TITLE_LIST_ITEM) {
                    str2 = Utils.getText(C0149R.string.contextpattads1_descr);
                }
                if (position == listAppsFragment.SETTINGS_SORTBY_STATUS) {
                    str2 = Utils.getText(C0149R.string.contextpattads2_descr);
                }
                if (position == listAppsFragment.SETTINGS_SORTBY_TIME) {
                    str2 = Utils.getText(C0149R.string.contextpattads3_descr);
                }
                if (position == listAppsFragment.Custom_PATCH_DIALOG) {
                    str2 = Utils.getText(C0149R.string.contextpattads5_descr);
                }
                if (position == listAppsFragment.EXT_PATCH_DIALOG) {
                    str2 = Utils.getText(C0149R.string.contextpattads6_descr);
                }
                if (position == listAppsFragment.CHILD_NAME_SHEME_SELECT_ITEM) {
                    str2 = Utils.getText(C0149R.string.contextpattads4_descr);
                }
                if (position == listAppsFragment.CONTEXT_DIALOG) {
                    str2 = Utils.getText(C0149R.string.contextpattdependencies_descr);
                }
                if (position == listAppsFragment.CREATEAPK_DIALOG) {
                    str2 = Utils.getText(C0149R.string.contextfulloffline_descr);
                }
                if (z) {
                    if (position == listAppsFragment.CREATEAPK_ADS_DIALOG) {
                        str2 = Utils.getText(C0149R.string.contextpattlvl4_descr);
                    }
                    if (position == listAppsFragment.PERM_CONTEXT_DIALOG) {
                        str2 = Utils.getText(C0149R.string.contextpattlvl5_descr);
                    }
                    if (position == listAppsFragment.PROGRESS_DIALOG2) {
                        str2 = Utils.getText(C0149R.string.contextpattlvl6_descr);
                    }
                }
                this.txtStatus.append(Utils.getColoredText(str2, "#ff888888", "italic"));
                return row;
            }
        };
    }

    public void getpackagesxml() {
        File tmpfile = new File(getContext().getDir("packages", SETTINGS_VIEWSIZE_SMALL) + "/packages1.xml");
        if (tmpfile.exists()) {
            tmpfile.delete();
        }
        tmpfile = new File(getContext().getDir("packages", SETTINGS_VIEWSIZE_SMALL) + "/packages.xml");
        if (tmpfile.exists()) {
            tmpfile.delete();
        }
        try {
            Utils.copyFile("/data/system/packages.xml", getContext().getDir("packages", SETTINGS_VIEWSIZE_SMALL).getAbsolutePath() + "/packages1.xml", false, true);
            Utils.copyFile("/data/system/packages.xml", getContext().getDir("packages", SETTINGS_VIEWSIZE_SMALL).getAbsolutePath() + "/packages.xml", false, true);
            Utils.run_all("chmod 0777 " + getContext().getDir("packages", SETTINGS_VIEWSIZE_SMALL).getAbsolutePath() + "/packages1.xml");
            Utils.run_all("chmod 0777 " + getContext().getDir("packages", SETTINGS_VIEWSIZE_SMALL).getAbsolutePath() + "/packages.xml");
        } catch (Exception e) {
            Toast.makeText(getContext(), BuildConfig.VERSION_NAME + e, TITLE_LIST_ITEM).show();
        }
        String PackagesPermissions = getContext().getDir("packages", SETTINGS_VIEWSIZE_SMALL).getAbsolutePath() + "/packages.xml";
        if (!new File(PackagesPermissions).exists() && new File(PackagesPermissions).length() == 0) {
            try {
                Utils.copyFile("/dbdata/system/packages.xml", getContext().getDir("packages", SETTINGS_VIEWSIZE_SMALL).getAbsolutePath() + "/packages1.xml", false, true);
                Utils.copyFile("/dbdata/system/packages.xml", getContext().getDir("packages", SETTINGS_VIEWSIZE_SMALL).getAbsolutePath() + "/packages.xml", false, true);
                Utils.run_all("chmod 0777 " + getContext().getDir("packages", SETTINGS_VIEWSIZE_SMALL).getAbsolutePath() + "/packages1.xml");
                Utils.run_all("chmod 0777 " + getContext().getDir("packages", SETTINGS_VIEWSIZE_SMALL).getAbsolutePath() + "/packages.xml");
            } catch (Exception e2) {
                Toast.makeText(getContext(), BuildConfig.VERSION_NAME + e2, TITLE_LIST_ITEM).show();
            }
            if (!new File(getContext().getDir("packages", SETTINGS_VIEWSIZE_SMALL).getAbsolutePath() + "/packages.xml").exists()) {
                Toast.makeText(getContext(), "Error: packages.xml not found!", TITLE_LIST_ITEM).show();
            }
        }
    }

    public void contextpermissions() {
        ArrayList arrayList;
        final ArrayList<Perm> arrayList2;
        String sharedUserId = null;
        PackageInfo info = null;
        try {
            info = getPkgMng().getPackageInfo(pli.pkgName, LZMA2Options.DICT_SIZE_MIN);
        } catch (NameNotFoundException e1) {
            e1.printStackTrace();
        }
        pkgNam = pli.pkgName;
        searchTag = Common.PACKAGE;
        ArrayList<Perm> tmpList = new ArrayList();
        String PackagesPermissions = getContext().getDir("packages", SETTINGS_VIEWSIZE_SMALL).getAbsolutePath() + "/packages.xml";
        if (!new File(PackagesPermissions).exists()) {
            Toast.makeText(getContext(), "Error: packages.xml not found!", TITLE_LIST_ITEM).show();
        }
        try {
            Node node;
            Element e;
            NodeList pkgParams;
            int j;
            Node nodeC;
            NodeList perms;
            int k;
            Element per;
            boolean found;
            int d;
            Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new FileInputStream(PackagesPermissions));
            NodeList packages = doc.getElementsByTagName(Common.PACKAGE);
            int i = SETTINGS_VIEWSIZE_SMALL;
            while (i < packages.getLength()) {
                node = packages.item(i);
                e = (Element) packages.item(i);
                if (e.getAttribute("name").equals(pli.pkgName)) {
                    if (!e.getAttribute("sharedUserId").isEmpty()) {
                        sharedUserId = e.getAttribute("sharedUserId");
                    }
                    pkgParams = node.getChildNodes();
                    for (j = SETTINGS_VIEWSIZE_SMALL; j < pkgParams.getLength(); j += TITLE_LIST_ITEM) {
                        nodeC = pkgParams.item(j);
                        System.out.println("node:" + nodeC.getNodeName());
                        if (nodeC.getNodeName().equals("perms")) {
                            perms = nodeC.getChildNodes();
                            for (k = SETTINGS_VIEWSIZE_SMALL; k < perms.getLength(); k += TITLE_LIST_ITEM) {
                                System.out.println("perm0:" + perms.item(k).getNodeName());
                                if (perms.item(k).getNodeName().equals("item")) {
                                    per = (Element) perms.item(k);
                                    if (per.getAttribute("name").contains("chelpa.disabled.")) {
                                        tmpList.add(new Perm(per.getAttribute("name"), false));
                                    } else {
                                        tmpList.add(new Perm(per.getAttribute("name"), true));
                                    }
                                    System.out.println("perm:" + per.getAttribute("name"));
                                }
                            }
                        }
                        if (nodeC.getNodeName().equals("disabled-components")) {
                            perms = nodeC.getChildNodes();
                            for (k = SETTINGS_VIEWSIZE_SMALL; k < perms.getLength(); k += TITLE_LIST_ITEM) {
                                System.out.println("perm0:" + perms.item(k).getNodeName());
                                if (perms.item(k).getNodeName().equals("item")) {
                                    per = (Element) perms.item(k);
                                    if (per.getAttribute("name").contains("chelpa.disabled.")) {
                                        if (!(info.requestedPermissions == null || info.requestedPermissions.length == 0)) {
                                            found = false;
                                            for (d = SETTINGS_VIEWSIZE_SMALL; d < info.requestedPermissions.length; d += TITLE_LIST_ITEM) {
                                                if (info.requestedPermissions[d].equals(per.getAttribute("name"))) {
                                                    found = true;
                                                    break;
                                                }
                                            }
                                            if (found) {
                                                tmpList.add(new Perm(per.getAttribute("name"), false));
                                            }
                                        }
                                    } else if (!(info.requestedPermissions == null || info.requestedPermissions.length == 0)) {
                                        found = false;
                                        for (d = SETTINGS_VIEWSIZE_SMALL; d < info.requestedPermissions.length; d += TITLE_LIST_ITEM) {
                                            if (info.requestedPermissions[d].equals(per.getAttribute("name"))) {
                                                found = true;
                                                break;
                                            }
                                        }
                                        if (found) {
                                            tmpList.add(new Perm(per.getAttribute("name"), true));
                                        }
                                    }
                                    System.out.println("perm:" + per.getAttribute("name"));
                                }
                            }
                        }
                        if (nodeC.getNodeName().equals("enabled-components")) {
                            perms = nodeC.getChildNodes();
                            for (k = SETTINGS_VIEWSIZE_SMALL; k < perms.getLength(); k += TITLE_LIST_ITEM) {
                                System.out.println("perm0:" + perms.item(k).getNodeName());
                                if (perms.item(k).getNodeName().equals("item")) {
                                    per = (Element) perms.item(k);
                                    if (per.getAttribute("name").contains("chelpa.disabled.")) {
                                        if (!(info.requestedPermissions == null || info.requestedPermissions.length == 0)) {
                                            found = false;
                                            for (d = SETTINGS_VIEWSIZE_SMALL; d < info.requestedPermissions.length; d += TITLE_LIST_ITEM) {
                                                if (info.requestedPermissions[d].equals(per.getAttribute("name"))) {
                                                    found = true;
                                                    break;
                                                }
                                            }
                                            if (found) {
                                                tmpList.add(new Perm(per.getAttribute("name"), false));
                                            }
                                        }
                                    } else if (!(info.requestedPermissions == null || info.requestedPermissions.length == 0)) {
                                        found = false;
                                        for (d = SETTINGS_VIEWSIZE_SMALL; d < info.requestedPermissions.length; d += TITLE_LIST_ITEM) {
                                            if (info.requestedPermissions[d].equals(per.getAttribute("name"))) {
                                                found = true;
                                                break;
                                            }
                                        }
                                        if (found) {
                                            tmpList.add(new Perm(per.getAttribute("name"), true));
                                        }
                                    }
                                    System.out.println("perm:" + per.getAttribute("name"));
                                }
                            }
                        }
                    }
                } else {
                    i += TITLE_LIST_ITEM;
                }
            }
            packages = doc.getElementsByTagName("updated-package");
            for (i = SETTINGS_VIEWSIZE_SMALL; i < packages.getLength(); i += TITLE_LIST_ITEM) {
                node = packages.item(i);
                e = (Element) packages.item(i);
                if (e.getAttribute("name").equals(pli.pkgName)) {
                    if (!e.getAttribute("sharedUserId").isEmpty()) {
                        sharedUserId = e.getAttribute("sharedUserId");
                    }
                    pkgParams = node.getChildNodes();
                    for (j = SETTINGS_VIEWSIZE_SMALL; j < pkgParams.getLength(); j += TITLE_LIST_ITEM) {
                        nodeC = pkgParams.item(j);
                        System.out.println("node:" + nodeC.getNodeName());
                        if (nodeC.getNodeName().equals("perms")) {
                            perms = nodeC.getChildNodes();
                            for (k = SETTINGS_VIEWSIZE_SMALL; k < perms.getLength(); k += TITLE_LIST_ITEM) {
                                System.out.println("perm0:" + perms.item(k).getNodeName());
                                if (perms.item(k).getNodeName().equals("item")) {
                                    per = (Element) perms.item(k);
                                    if (per.getAttribute("name").contains("chelpa.disabled.")) {
                                        tmpList.add(new Perm(per.getAttribute("name"), false));
                                    } else {
                                        tmpList.add(new Perm(per.getAttribute("name"), true));
                                    }
                                    System.out.println("perm:" + per.getAttribute("name"));
                                }
                            }
                        }
                        if (nodeC.getNodeName().equals("disabled-components")) {
                            perms = nodeC.getChildNodes();
                            for (k = SETTINGS_VIEWSIZE_SMALL; k < perms.getLength(); k += TITLE_LIST_ITEM) {
                                System.out.println("perm0:" + perms.item(k).getNodeName());
                                if (perms.item(k).getNodeName().equals("item")) {
                                    per = (Element) perms.item(k);
                                    if (per.getAttribute("name").contains("chelpa.disabled.")) {
                                        if (!(info.requestedPermissions == null || info.requestedPermissions.length == 0)) {
                                            found = false;
                                            for (d = SETTINGS_VIEWSIZE_SMALL; d < info.requestedPermissions.length; d += TITLE_LIST_ITEM) {
                                                if (info.requestedPermissions[d].equals(per.getAttribute("name"))) {
                                                    found = true;
                                                    break;
                                                }
                                            }
                                            if (found) {
                                                tmpList.add(new Perm(per.getAttribute("name"), false));
                                            }
                                        }
                                    } else if (!(info.requestedPermissions == null || info.requestedPermissions.length == 0)) {
                                        found = false;
                                        for (d = SETTINGS_VIEWSIZE_SMALL; d < info.requestedPermissions.length; d += TITLE_LIST_ITEM) {
                                            if (info.requestedPermissions[d].equals(per.getAttribute("name"))) {
                                                found = true;
                                                break;
                                            }
                                        }
                                        if (found) {
                                            tmpList.add(new Perm(per.getAttribute("name"), true));
                                        }
                                    }
                                    System.out.println("perm:" + per.getAttribute("name"));
                                }
                            }
                        }
                        if (nodeC.getNodeName().equals("enabled-components")) {
                            perms = nodeC.getChildNodes();
                            for (k = SETTINGS_VIEWSIZE_SMALL; k < perms.getLength(); k += TITLE_LIST_ITEM) {
                                System.out.println("perm0:" + perms.item(k).getNodeName());
                                if (perms.item(k).getNodeName().equals("item")) {
                                    per = (Element) perms.item(k);
                                    if (per.getAttribute("name").contains("chelpa.disabled.")) {
                                        if (!(info.requestedPermissions == null || info.requestedPermissions.length == 0)) {
                                            found = false;
                                            for (d = SETTINGS_VIEWSIZE_SMALL; d < info.requestedPermissions.length; d += TITLE_LIST_ITEM) {
                                                if (info.requestedPermissions[d].equals(per.getAttribute("name"))) {
                                                    found = true;
                                                    break;
                                                }
                                            }
                                            if (found) {
                                                tmpList.add(new Perm(per.getAttribute("name"), false));
                                            }
                                        }
                                    } else if (!(info.requestedPermissions == null || info.requestedPermissions.length == 0)) {
                                        found = false;
                                        for (d = SETTINGS_VIEWSIZE_SMALL; d < info.requestedPermissions.length; d += TITLE_LIST_ITEM) {
                                            if (info.requestedPermissions[d].equals(per.getAttribute("name"))) {
                                                found = true;
                                                break;
                                            }
                                        }
                                        if (found) {
                                            tmpList.add(new Perm(per.getAttribute("name"), true));
                                        }
                                    }
                                    System.out.println("perm:" + per.getAttribute("name"));
                                }
                            }
                        }
                    }
                    if (sharedUserId != null) {
                        packages = doc.getElementsByTagName("shared-user");
                        i = SETTINGS_VIEWSIZE_SMALL;
                        while (i < packages.getLength()) {
                            node = packages.item(i);
                            if (((Element) packages.item(i)).getAttribute("userId").equals(sharedUserId)) {
                                i += TITLE_LIST_ITEM;
                            } else {
                                pkgParams = node.getChildNodes();
                                for (j = SETTINGS_VIEWSIZE_SMALL; j < pkgParams.getLength(); j += TITLE_LIST_ITEM) {
                                    nodeC = pkgParams.item(j);
                                    System.out.println("node:" + nodeC.getNodeName());
                                    if (nodeC.getNodeName().equals("perms")) {
                                        perms = nodeC.getChildNodes();
                                        for (k = SETTINGS_VIEWSIZE_SMALL; k < perms.getLength(); k += TITLE_LIST_ITEM) {
                                            System.out.println("perm0:" + perms.item(k).getNodeName());
                                            if (perms.item(k).getNodeName().equals("item")) {
                                                per = (Element) perms.item(k);
                                                if (per.getAttribute("name").contains("chelpa.disabled.")) {
                                                    tmpList.add(new Perm(per.getAttribute("name"), true));
                                                } else {
                                                    tmpList.add(new Perm(per.getAttribute("name"), false));
                                                }
                                                System.out.println("perm:" + per.getAttribute("name"));
                                            }
                                        }
                                    }
                                    if (nodeC.getNodeName().equals("disabled-components")) {
                                        perms = nodeC.getChildNodes();
                                        for (k = SETTINGS_VIEWSIZE_SMALL; k < perms.getLength(); k += TITLE_LIST_ITEM) {
                                            System.out.println("perm0:" + perms.item(k).getNodeName());
                                            if (perms.item(k).getNodeName().equals("item")) {
                                                per = (Element) perms.item(k);
                                                if (per.getAttribute("name").contains("chelpa.disabled.")) {
                                                    if (!(info.requestedPermissions == null || info.requestedPermissions.length == 0)) {
                                                        found = false;
                                                        for (d = SETTINGS_VIEWSIZE_SMALL; d < info.requestedPermissions.length; d += TITLE_LIST_ITEM) {
                                                            if (info.requestedPermissions[d].equals(per.getAttribute("name"))) {
                                                                found = true;
                                                                break;
                                                            }
                                                        }
                                                        if (found) {
                                                            tmpList.add(new Perm(per.getAttribute("name"), true));
                                                        }
                                                    }
                                                } else if (!(info.requestedPermissions == null || info.requestedPermissions.length == 0)) {
                                                    found = false;
                                                    for (d = SETTINGS_VIEWSIZE_SMALL; d < info.requestedPermissions.length; d += TITLE_LIST_ITEM) {
                                                        if (info.requestedPermissions[d].equals(per.getAttribute("name"))) {
                                                            found = true;
                                                            break;
                                                        }
                                                    }
                                                    if (found) {
                                                        tmpList.add(new Perm(per.getAttribute("name"), false));
                                                    }
                                                }
                                                System.out.println("perm:" + per.getAttribute("name"));
                                            }
                                        }
                                    }
                                    if (nodeC.getNodeName().equals("enabled-components")) {
                                        perms = nodeC.getChildNodes();
                                        for (k = SETTINGS_VIEWSIZE_SMALL; k < perms.getLength(); k += TITLE_LIST_ITEM) {
                                            System.out.println("perm0:" + perms.item(k).getNodeName());
                                            if (perms.item(k).getNodeName().equals("item")) {
                                                per = (Element) perms.item(k);
                                                if (per.getAttribute("name").contains("chelpa.disabled.")) {
                                                    if (!(info.requestedPermissions == null || info.requestedPermissions.length == 0)) {
                                                        found = false;
                                                        for (d = SETTINGS_VIEWSIZE_SMALL; d < info.requestedPermissions.length; d += TITLE_LIST_ITEM) {
                                                            if (info.requestedPermissions[d].equals(per.getAttribute("name"))) {
                                                                found = true;
                                                                break;
                                                            }
                                                        }
                                                        if (found) {
                                                            tmpList.add(new Perm(per.getAttribute("name"), true));
                                                        }
                                                    }
                                                } else if (!(info.requestedPermissions == null || info.requestedPermissions.length == 0)) {
                                                    found = false;
                                                    for (d = SETTINGS_VIEWSIZE_SMALL; d < info.requestedPermissions.length; d += TITLE_LIST_ITEM) {
                                                        if (info.requestedPermissions[d].equals(per.getAttribute("name"))) {
                                                            found = true;
                                                            break;
                                                        }
                                                    }
                                                    if (found) {
                                                        tmpList.add(new Perm(per.getAttribute("name"), false));
                                                    }
                                                }
                                                System.out.println("perm:" + per.getAttribute("name"));
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    arrayList = new ArrayList();
                    if (tmpList.size() <= 0) {
                        tmpList.add(new Perm(Utils.getText(C0149R.string.permission_not_found), false));
                    }
                    arrayList2 = tmpList;
                    runToMain(new Runnable() {
                        public void run() {
                            if (listAppsFragment.perm_adapt != null) {
                                listAppsFragment.perm_adapt.notifyDataSetChanged();
                            }
                            listAppsFragment.perm_adapt = new ArrayAdapter<Perm>(listAppsFragment.this.getContext(), C0149R.layout.contextmenu, arrayList2) {
                                public View getView(int position, View convertView, ViewGroup parent) {
                                    View view = super.getView(position, convertView, parent);
                                    TextView textView = (TextView) view.findViewById(C0149R.id.conttext);
                                    textView.setTextAppearance(getContext(), listAppsFragment.getSizeText());
                                    textView.setCompoundDrawablePadding((int) ((5.0f * listAppsFragment.getRes().getDisplayMetrics().density) + 0.5f));
                                    String str2 = ((Perm) getItem(position)).Name.replace("chelpa.disabled.", BuildConfig.VERSION_NAME).replace("android.permission.", BuildConfig.VERSION_NAME).replace("com.android.vending.", BuildConfig.VERSION_NAME) + LogCollector.LINE_SEPARATOR;
                                    if (((Perm) getItem(position)).Status) {
                                        textView.setText(Utils.getColoredText(str2, "#ff00ff00", "bold"));
                                    } else {
                                        textView.setText(Utils.getColoredText(str2, "#ffff0000", "bold"));
                                    }
                                    PackageManager pm = listAppsFragment.getPkgMng();
                                    try {
                                        str2 = pm.getPermissionInfo(((Perm) getItem(position)).Name.replace("chelpa.disabled.", BuildConfig.VERSION_NAME), listAppsFragment.SETTINGS_VIEWSIZE_SMALL).loadDescription(pm).toString();
                                        if (str2 == null) {
                                            textView.append(Utils.getColoredText(Utils.getText(C0149R.string.permission_not_descr), "#ff888888", "italic"));
                                        } else {
                                            textView.append(Utils.getColoredText(str2, "#ff888888", "italic"));
                                        }
                                    } catch (NameNotFoundException e) {
                                        textView.append(Utils.getColoredText(Utils.getText(C0149R.string.permission_not_descr), "#ff888888", "italic"));
                                    } catch (NullPointerException e2) {
                                        textView.append(Utils.getColoredText(Utils.getText(C0149R.string.permission_not_descr), "#ff888888", "italic"));
                                    }
                                    return view;
                                }
                            };
                        }
                    });
                }
            }
            if (sharedUserId != null) {
                packages = doc.getElementsByTagName("shared-user");
                i = SETTINGS_VIEWSIZE_SMALL;
                while (i < packages.getLength()) {
                    node = packages.item(i);
                    if (((Element) packages.item(i)).getAttribute("userId").equals(sharedUserId)) {
                        i += TITLE_LIST_ITEM;
                    } else {
                        pkgParams = node.getChildNodes();
                        for (j = SETTINGS_VIEWSIZE_SMALL; j < pkgParams.getLength(); j += TITLE_LIST_ITEM) {
                            nodeC = pkgParams.item(j);
                            System.out.println("node:" + nodeC.getNodeName());
                            if (nodeC.getNodeName().equals("perms")) {
                                perms = nodeC.getChildNodes();
                                for (k = SETTINGS_VIEWSIZE_SMALL; k < perms.getLength(); k += TITLE_LIST_ITEM) {
                                    System.out.println("perm0:" + perms.item(k).getNodeName());
                                    if (perms.item(k).getNodeName().equals("item")) {
                                        per = (Element) perms.item(k);
                                        if (per.getAttribute("name").contains("chelpa.disabled.")) {
                                            tmpList.add(new Perm(per.getAttribute("name"), true));
                                        } else {
                                            tmpList.add(new Perm(per.getAttribute("name"), false));
                                        }
                                        System.out.println("perm:" + per.getAttribute("name"));
                                    }
                                }
                            }
                            if (nodeC.getNodeName().equals("disabled-components")) {
                                perms = nodeC.getChildNodes();
                                for (k = SETTINGS_VIEWSIZE_SMALL; k < perms.getLength(); k += TITLE_LIST_ITEM) {
                                    System.out.println("perm0:" + perms.item(k).getNodeName());
                                    if (perms.item(k).getNodeName().equals("item")) {
                                        per = (Element) perms.item(k);
                                        if (per.getAttribute("name").contains("chelpa.disabled.")) {
                                            found = false;
                                            for (d = SETTINGS_VIEWSIZE_SMALL; d < info.requestedPermissions.length; d += TITLE_LIST_ITEM) {
                                                if (info.requestedPermissions[d].equals(per.getAttribute("name"))) {
                                                    found = true;
                                                    break;
                                                }
                                            }
                                            if (found) {
                                                tmpList.add(new Perm(per.getAttribute("name"), true));
                                            }
                                        } else {
                                            found = false;
                                            for (d = SETTINGS_VIEWSIZE_SMALL; d < info.requestedPermissions.length; d += TITLE_LIST_ITEM) {
                                                if (info.requestedPermissions[d].equals(per.getAttribute("name"))) {
                                                    found = true;
                                                    break;
                                                }
                                            }
                                            if (found) {
                                                tmpList.add(new Perm(per.getAttribute("name"), false));
                                            }
                                        }
                                        System.out.println("perm:" + per.getAttribute("name"));
                                    }
                                }
                            }
                            if (nodeC.getNodeName().equals("enabled-components")) {
                                perms = nodeC.getChildNodes();
                                for (k = SETTINGS_VIEWSIZE_SMALL; k < perms.getLength(); k += TITLE_LIST_ITEM) {
                                    System.out.println("perm0:" + perms.item(k).getNodeName());
                                    if (perms.item(k).getNodeName().equals("item")) {
                                        per = (Element) perms.item(k);
                                        if (per.getAttribute("name").contains("chelpa.disabled.")) {
                                            found = false;
                                            for (d = SETTINGS_VIEWSIZE_SMALL; d < info.requestedPermissions.length; d += TITLE_LIST_ITEM) {
                                                if (info.requestedPermissions[d].equals(per.getAttribute("name"))) {
                                                    found = true;
                                                    break;
                                                }
                                            }
                                            if (found) {
                                                tmpList.add(new Perm(per.getAttribute("name"), true));
                                            }
                                        } else {
                                            found = false;
                                            for (d = SETTINGS_VIEWSIZE_SMALL; d < info.requestedPermissions.length; d += TITLE_LIST_ITEM) {
                                                if (info.requestedPermissions[d].equals(per.getAttribute("name"))) {
                                                    found = true;
                                                    break;
                                                }
                                            }
                                            if (found) {
                                                tmpList.add(new Perm(per.getAttribute("name"), false));
                                            }
                                        }
                                        System.out.println("perm:" + per.getAttribute("name"));
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } catch (SAXException sae) {
            sae.printStackTrace();
        }
        arrayList = new ArrayList();
        if (tmpList.size() <= 0) {
            tmpList.add(new Perm(Utils.getText(C0149R.string.permission_not_found), false));
        }
        arrayList2 = tmpList;
        runToMain(/* anonymous class already generated */);
    }

    void download_custom_patches() {
        DownloadFile cpd = new DownloadFile();
        cpd.filename = "CustomPatches.zip";
        cpd.on_post_run_cached = new Runnable() {

            class C04191 implements Runnable {

                class C04181 implements Runnable {
                    C04181() {
                    }

                    public void run() {
                        listAppsFragment.this.showMessage(Utils.getText(C0149R.string.information), Utils.getText(C0149R.string.custom_patches_update_not_found));
                    }
                }

                C04191() {
                }

                public void run() {
                    try {
                        FileInputStream fin = new FileInputStream(new File(listAppsFragment.basepath + "/Download/CustomPatches.zip"));
                        ZipInputStream zin = new ZipInputStream(fin);
                        while (true) {
                            ZipEntry ze = zin.getNextEntry();
                            if (ze != null) {
                                String extrFile = listAppsFragment.basepath + InternalZipConstants.ZIP_FILE_SEPARATOR + ze.getName();
                                new File(listAppsFragment.basepath).mkdirs();
                                FileOutputStream fout = new FileOutputStream(extrFile);
                                byte[] buffer = new byte[InternalZipConstants.UFT8_NAMES_FLAG];
                                while (true) {
                                    int length = zin.read(buffer);
                                    if (length != -1) {
                                        fout.write(buffer, listAppsFragment.SETTINGS_VIEWSIZE_SMALL, length);
                                    }
                                }
                            } else {
                                zin.close();
                                fin.close();
                                listAppsFragment.this.runToMain(new C04181());
                                return;
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            public void run() {
                System.out.println("Unpack custom patches");
                listAppsFragment.this.runWithWait(new C04191());
            }
        };
        cpd.on_post_run_downloaded = new Runnable() {

            class C04211 implements Runnable {

                class C04201 implements Runnable {
                    C04201() {
                    }

                    public void run() {
                        listAppsFragment.this.showMessage(Utils.getText(C0149R.string.information), Utils.getText(C0149R.string.all_custom_patches_updated));
                    }
                }

                C04211() {
                }

                public void run() {
                    try {
                        FileInputStream fin = new FileInputStream(new File(listAppsFragment.basepath + "/Download/CustomPatches.zip"));
                        ZipInputStream zin = new ZipInputStream(fin);
                        while (true) {
                            ZipEntry ze = zin.getNextEntry();
                            if (ze != null) {
                                String extrFile = listAppsFragment.basepath + InternalZipConstants.ZIP_FILE_SEPARATOR + ze.getName();
                                new File(listAppsFragment.basepath).mkdirs();
                                FileOutputStream fout = new FileOutputStream(extrFile);
                                byte[] buffer = new byte[InternalZipConstants.UFT8_NAMES_FLAG];
                                while (true) {
                                    int length = zin.read(buffer);
                                    if (length != -1) {
                                        fout.write(buffer, listAppsFragment.SETTINGS_VIEWSIZE_SMALL, length);
                                    }
                                }
                            } else {
                                zin.close();
                                fin.close();
                                listAppsFragment.this.runToMain(new C04201());
                                return;
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            public void run() {
                System.out.println("Unpack custom patches");
                listAppsFragment.this.runWithWait(new C04211());
            }
        };
        String[] strArr = new String[TITLE_LIST_ITEM];
        strArr[SETTINGS_VIEWSIZE_SMALL] = "CustomPatches.zip";
        cpd.execute(strArr);
    }

    private void installToSystem(final String pkg, final String cmd1) {
        runWithWait(new Runnable() {

            class C04221 implements Runnable {
                C04221() {
                }

                public void run() {
                    listAppsFragment.this.showMessageInfo(listAppsFragment.this.getContext(), Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.no_space_in_system));
                    listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                }
            }

            class C04232 implements Runnable {
                C04232() {
                }

                public void run() {
                    listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.unknown_error));
                    listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                }
            }

            class C04253 implements Runnable {

                class C04241 implements OnClickListener {
                    C04241() {
                    }

                    public void onClick(DialogInterface dialog, int which) {
                        Utils.reboot();
                    }
                }

                C04253() {
                }

                public void run() {
                    Utils.showDialogYesNo(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.system_app_change), new C04241(), null, null);
                    listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                }
            }

            public void run() {
                try {
                    String str = BuildConfig.VERSION_NAME;
                    Utils.kill(pkg);
                    Utils.remount("/system", InternalZipConstants.WRITE_MODE);
                    Utils utils = new Utils(BuildConfig.VERSION_NAME);
                    String[] strArr = new String[listAppsFragment.TITLE_LIST_ITEM];
                    strArr[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = cmd1;
                    str = utils.cmdRoot(strArr);
                    System.out.println(BuildConfig.VERSION_NAME + str);
                    if (str.contains("In /system space not found!")) {
                        listAppsFragment.this.runToMain(new C04221());
                        return;
                    }
                    String system_app_dir = "/system/app/";
                    try {
                        if (new File("/system/priv-app").exists() && new File("/system/priv-app").list() != null) {
                            system_app_dir = "/system/priv-app/";
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (listAppsFragment.api >= listAppsFragment.Create_PERM_CONTEXT_DIALOG) {
                        system_app_dir = "/system/priv-app/" + pkg + InternalZipConstants.ZIP_FILE_SEPARATOR;
                    }
                    Utils.run_all("chmod 0644 " + system_app_dir + pkg + ".apk");
                    Utils.run_all("chown 0.0 " + system_app_dir + pkg + ".apk");
                    Utils.run_all("chown 0:0 " + system_app_dir + pkg + ".apk");
                    listAppsFragment.this.runToMain(new C04253());
                } catch (Exception e2) {
                    System.out.println("Move to system " + e2);
                    listAppsFragment.this.runToMain(new C04232());
                }
            }
        });
    }

    private void moveToSystem(final String pkg, final String cmd1) {
        runWithWait(new Runnable() {

            class C04291 implements Runnable {
                C04291() {
                }

                public void run() {
                    listAppsFragment.this.showMessageInfo(listAppsFragment.this.getContext(), Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.no_space_in_system));
                    listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                }
            }

            class C04302 implements Runnable {
                C04302() {
                }

                public void run() {
                    listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.unknown_error));
                    listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                }
            }

            class C04323 implements Runnable {

                class C04311 implements OnClickListener {
                    C04311() {
                    }

                    public void onClick(DialogInterface dialog, int which) {
                        Utils.reboot();
                    }
                }

                C04323() {
                }

                public void run() {
                    listAppsFragment.getConfig().edit().remove(listAppsFragment.pli.pkgName).commit();
                    listAppsFragment.plia.remove(listAppsFragment.pli.pkgName);
                    listAppsFragment.plia.notifyDataSetChanged();
                    Utils.showDialogYesNo(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.system_app_change), new C04311(), null, null);
                    listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                }
            }

            public void run() {
                try {
                    String str = BuildConfig.VERSION_NAME;
                    Utils.kill(pkg);
                    Utils.removePkgFromSystem(pkg);
                    Utils utils = new Utils(BuildConfig.VERSION_NAME);
                    String[] strArr = new String[listAppsFragment.TITLE_LIST_ITEM];
                    strArr[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = cmd1;
                    str = utils.cmdRoot(strArr);
                    System.out.println(BuildConfig.VERSION_NAME + str);
                    if (str.contains("In /system space not found!")) {
                        listAppsFragment.this.runToMain(new C04291());
                        return;
                    }
                    String system_app_dir = "/system/app/";
                    try {
                        if (new File("/system/priv-app").exists() && new File("/system/priv-app").list() != null) {
                            system_app_dir = "/system/priv-app/";
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Utils.run_all("chmod 0644 " + system_app_dir + pkg + ".apk");
                    Utils.run_all("chown 0.0 " + system_app_dir + pkg + ".apk");
                    Utils.run_all("chown 0:0 " + system_app_dir + pkg + ".apk");
                    listAppsFragment.this.runToMain(new C04323());
                } catch (Exception e2) {
                    System.out.println("Move to system " + e2);
                    listAppsFragment.this.runToMain(new C04302());
                }
            }
        });
    }

    public void sqlitecopy() {
        File tempopt = new File(toolfilesdir + InternalZipConstants.ZIP_FILE_SEPARATOR + "sqlite3");
        if (!tempopt.exists()) {
            try {
                Utils.getAssets("sqlite3", toolfilesdir);
            } catch (Exception e) {
            }
            Utils.run_all("chmod 777 " + tempopt.getAbsolutePath());
        }
    }

    public void pinfocopy() {
        File tempopt = new File(toolfilesdir + InternalZipConstants.ZIP_FILE_SEPARATOR + "p.apk");
        long lenght = Utils.getRawLength(C0149R.raw.p);
        if (!tempopt.exists() || tempopt.length() != lenght) {
            if (tempopt.length() != lenght) {
                System.out.println("LuckyPatcher: p.info version updated. " + lenght + " " + tempopt.length());
                if (tempopt.exists()) {
                    tempopt.delete();
                }
            }
            try {
                Utils.getRawToFile(C0149R.raw.p, new File(toolfilesdir + "/p.apk"));
            } catch (Exception e) {
            }
            try {
                Utils.chmod(new File(tempopt.getAbsolutePath()), 3583);
            } catch (Exception e2) {
                System.out.println(e2);
                e2.printStackTrace();
            }
            Utils.run_all("chmod 06777 " + tempopt.getAbsolutePath());
            Utils.run_all("chown 0.0 " + tempopt.getAbsolutePath());
            Utils.run_all("chown 0:0 " + tempopt.getAbsolutePath());
        }
    }

    public void busyboxcopy() {
        int resFile;
        File tempopt = new File(toolfilesdir + InternalZipConstants.ZIP_FILE_SEPARATOR + "busybox");
        if (Build.CPU_ABI.toLowerCase().trim().equals("x86")) {
            resFile = C0149R.raw.busyboxx86;
        } else if (Build.CPU_ABI.toUpperCase().trim().equals("MIPS")) {
            resFile = C0149R.raw.busyboxmips;
        } else {
            resFile = C0149R.raw.busybox;
        }
        long lenght = Utils.getRawLength(resFile);
        if (!tempopt.exists() || tempopt.length() != lenght) {
            if (tempopt.length() != lenght) {
                System.out.println("LuckyPatcher: busybox version updated. " + lenght + " " + tempopt.length());
                if (tempopt.exists()) {
                    tempopt.delete();
                }
            }
            try {
                Utils.getRawToFile(resFile, new File(toolfilesdir + "/busybox"));
            } catch (Exception e) {
            }
            try {
                Utils.chmod(new File(tempopt.getAbsolutePath()), 3583);
            } catch (Exception e2) {
                System.out.println(e2);
                e2.printStackTrace();
            }
            Utils.run_all("chmod 06777 " + tempopt.getAbsolutePath());
            Utils.run_all("chown 0.0 " + tempopt.getAbsolutePath());
            Utils.run_all("chown 0:0 " + tempopt.getAbsolutePath());
        }
    }

    public void iptablescopy() {
    }

    public void rebootcopy() {
        int resFile;
        File tempopt = new File(toolfilesdir + InternalZipConstants.ZIP_FILE_SEPARATOR + "reboot");
        if (Build.CPU_ABI.toLowerCase().trim().equals("x86")) {
            resFile = C0149R.raw.rebootx86;
        } else if (Build.CPU_ABI.toUpperCase().trim().equals("MIPS")) {
            resFile = C0149R.raw.rebootmips;
        } else {
            resFile = C0149R.raw.reboot;
        }
        if (!tempopt.exists() || tempopt.length() != Utils.getRawLength(resFile)) {
            try {
                Utils.getRawToFile(resFile, new File(toolfilesdir + "/reboot"));
            } catch (Exception e) {
            }
            try {
                Utils.chmod(new File(tempopt.getAbsolutePath()), 777);
            } catch (Exception e2) {
                System.out.println(e2);
                e2.printStackTrace();
            }
            Utils.run_all("chmod 777 " + tempopt.getAbsolutePath());
            Utils.run_all("chown 0.0 " + tempopt.getAbsolutePath());
            Utils.run_all("chown 0:0 " + tempopt.getAbsolutePath());
        }
    }

    public void busyboxinstall() {
        try {
            Utils.remount("/system", InternalZipConstants.WRITE_MODE);
            Utils.copyFile(getContext().getFilesDir().getAbsolutePath() + "/busybox", "/system/xbin/busybox", false, true);
        } catch (Exception e) {
        }
        try {
            Utils.remount("/system", InternalZipConstants.WRITE_MODE);
            Utils.run_all("chown 0.2000 /system/xbin/busybox");
            Utils.run_all("chown 0:2000 /system/xbin/busybox");
            Utils.run_all("chmod 04755 /system/xbin/busybox");
            Utils utils = new Utils(BuildConfig.VERSION_NAME);
            String[] strArr = new String[TITLE_LIST_ITEM];
            strArr[SETTINGS_VIEWSIZE_SMALL] = "/system/xbin/busybox --install -s /system/xbin";
            utils.cmdRoot(strArr);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public static ArrayList<File> getStorage() {
        String s = BuildConfig.VERSION_NAME;
        ArrayList<File> sdcard_dirs = new ArrayList();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            ProcessBuilder processBuilder = new ProcessBuilder(new String[SETTINGS_VIEWSIZE_SMALL]);
            String[] strArr = new String[TITLE_LIST_ITEM];
            strArr[SETTINGS_VIEWSIZE_SMALL] = "mount";
            Process process = processBuilder.command(strArr).redirectErrorStream(true).start();
            process.waitFor();
            InputStream is = process.getInputStream();
            byte[] buffer = new byte[AccessibilityNodeInfoCompat.ACTION_SCROLL_BACKWARD];
            while (true) {
                int len1 = is.read(buffer);
                if (len1 == -1) {
                    break;
                }
                baos.write(buffer, SETTINGS_VIEWSIZE_SMALL, len1);
            }
            is.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        s = baos.toString();
        ArrayList<Mount> mounts = null;
        try {
            mounts = Utils.getMounts();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        String[] lines = s.split(LogCollector.LINE_SEPARATOR);
        sdcard_dirs.clear();
        int length = lines.length;
        for (int i = SETTINGS_VIEWSIZE_SMALL; i < length; i += TITLE_LIST_ITEM) {
            String line1 = lines[i];
            System.out.println("LuckyPatcher: mount line - " + line1);
            if ((line1.toLowerCase().contains("vold") && line1.toLowerCase().contains("vfat") && !line1.toLowerCase().contains("asec")) || line1.toLowerCase().contains("sdcard")) {
                String[] blocks = line1.split("\\s+");
                if (blocks[TITLE_LIST_ITEM].equals("on")) {
                    sdcard_dirs.add(new File(blocks[SETTINGS_SORTBY_STATUS]));
                } else {
                    sdcard_dirs.add(new File(blocks[TITLE_LIST_ITEM]));
                }
            }
        }
        Iterator it = sdcard_dirs.iterator();
        while (it.hasNext()) {
            System.out.println("LuckyPatcher: SDcard found to " + ((File) it.next()));
        }
        try {
            if (sdcard_dirs.size() == 0) {
                sdcard_dirs.clear();
                it = mounts.iterator();
                while (it.hasNext()) {
                    Mount line = (Mount) it.next();
                    if (line.getDevice().toString().toLowerCase().contains("vold") && line.getType().toLowerCase().contains("vfat") && !line.getMountPoint().toString().toLowerCase().contains("asec")) {
                        sdcard_dirs.add(line.getMountPoint());
                    }
                }
                it = sdcard_dirs.iterator();
                while (it.hasNext()) {
                    System.out.println("LuckyPatcher: SDcard fount to " + ((File) it.next()));
                }
            }
        } catch (Exception e22) {
            e22.printStackTrace();
        }
        return sdcard_dirs;
    }

    private void pa2() {
        Utils utils = new Utils(BuildConfig.VERSION_NAME);
        String[] strArr = new String[TITLE_LIST_ITEM];
        strArr[SETTINGS_VIEWSIZE_SMALL] = dalvikruncommand + ".pa2 1";
        utils.cmdRoot(strArr);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void pa() {
        if (Utils.exists("/data/data/com.maxmpz.audioplayer/databases/folders.db-wal")) {
            Utils.run_all("rm /data/data/com.maxmpz.audioplayer/databases/folders.db-wal");
        }
        if (Utils.exists("/data/data/com.maxmpz.audioplayer/databases/folders.db-shm")) {
            Utils.run_all("rm /data/data/com.maxmpz.audioplayer/databases/folders.db-shm");
        }
        byte[] bArr = new byte[LVL_PATCH_CONTEXT_DIALOG];
        byte[] bArr2 = new byte[]{(byte) 19, (byte) -31, (byte) -111, (byte) -52, (byte) 0, (byte) 41, (byte) 12, (byte) 98, (byte) 21, (byte) 88, (byte) 14, (byte) 2, (byte) -34, (byte) 94, (byte) 24, (byte) -9, (byte) 31, (byte) 22, (byte) -95, (byte) 99};
        AESObfuscator aESObfuscator = new AESObfuscator(bArr2, "com.maxmpz.audioplayer", Secure.getString(getContext().getContentResolver(), "android_id") + Build.FINGERPRINT);
        AESObfuscator.header = "com.maxmpz.audioplayer|";
        byte[] copyIV = new byte[]{(byte) 16, (byte) 74, (byte) 71, (byte) -80, (byte) 32, (byte) 101, (byte) -47, (byte) 72, (byte) 117, (byte) -14, (byte) 0, (byte) -29, (byte) 70, (byte) 65, (byte) -12, (byte) 74};
        String validityTimestamp = aESObfuscator.obfuscate("2147483645|1344880878|0|1407088878", BuildConfig.VERSION_NAME);
        String[] strArr;
        if (Utils.isWithFramework()) {
            Utils utils = new Utils(BuildConfig.VERSION_NAME);
            strArr = new String[TITLE_LIST_ITEM];
            strArr[SETTINGS_VIEWSIZE_SMALL] = dalvikruncommandWithFramework + ".pa " + validityTimestamp + " " + toolfilesdir;
            String result = utils.cmdRoot(strArr);
            startUnderRoot = Boolean.valueOf(false);
            System.out.println(result);
        } else {
            SQLiteDatabase db3 = null;
            Utils.copyFile("/data/data/com.maxmpz.audioplayer/databases/folders.db", getContext().getFilesDir().getAbsolutePath() + "/folders.db", false, false);
            String filesdir = getContext().getFilesDir().getAbsolutePath();
            Utils.run_all("chmod 777 " + filesdir + "/folders.db");
            if (new File(filesdir + "/folders.db").exists()) {
                db3 = SQLiteDatabase.openDatabase(filesdir + "/folders.db", null, SETTINGS_VIEWSIZE_SMALL);
                strArr = new String[Custom_PATCH_DIALOG];
                strArr[SETTINGS_VIEWSIZE_SMALL] = "_id";
                strArr[TITLE_LIST_ITEM] = "path";
                strArr[SETTINGS_SORTBY_STATUS] = "path_hash";
                strArr[SETTINGS_SORTBY_TIME] = "updated_at";
                Cursor c = db3.query("storages", strArr, null, null, null, null, null);
                c.moveToFirst();
                String pa = BuildConfig.VERSION_NAME;
                String id = BuildConfig.VERSION_NAME;
                boolean write = false;
                while (true) {
                    id = c.getString(c.getColumnIndex("_id"));
                    pa = c.getString(c.getColumnIndex("path"));
                    try {
                        if (!c.moveToNext()) {
                            break;
                        }
                        try {
                            if (pa.length() < Custom_PATCH_DIALOG) {
                                break;
                            }
                            System.out.println("2 " + pa);
                            if (pa == null) {
                                pa = "/mnt/sdcard";
                            }
                            db3.execSQL("UPDATE storages SET path='" + pa + "',path_hash='" + validityTimestamp + "',updated_at='" + System.currentTimeMillis() + "' WHERE _id='" + id + "'");
                            write = true;
                        } catch (Exception e) {
                            pa = Environment.getExternalStorageDirectory().getAbsolutePath();
                            System.out.println("3 " + pa);
                        }
                    } catch (Exception e2) {
                        c.close();
                    }
                }
                c.close();
                if (pa == null) {
                    pa = "/mnt/sdcard";
                }
                System.out.println("4 " + pa);
                if (!write) {
                    db3.execSQL("UPDATE storages SET path='" + pa + "',path_hash='" + validityTimestamp + "',updated_at='" + System.currentTimeMillis() + "'");
                }
                db3.close();
                Utils.copyFile(getContext().getFilesDir().getAbsolutePath() + "/folders.db", "/data/data/com.maxmpz.audioplayer/databases/folders.db", true, true);
            } else {
                db3.close();
                str = "SU Java-Code Running!\nYou must run Poweramp before patch!\nRun Poweramp and apply custom patch again!\n\nGood Luck!\nSaNX@forpda.ru";
            }
            if (new File(filesdir + "/folders.db").exists()) {
                new File(filesdir + "/folders.db").delete();
            } else {
                str = "SU Java-Code Running!\nYou must run Poweramp before patch!\nRun Poweramp and apply custom patch again!\n\nGood Luck!\nSaNX@forpda.ru";
            }
        }
        File file = new File("/data/data/com.maxmpz.audioplayer/shared_prefs/l.xml");
        file = new File("/dbdata/databases/com.maxmpz.audioplayer/shared_prefs/l.xml");
        if (Utils.exists(file.getAbsolutePath())) {
            Utils.run_all("rm " + file.getAbsolutePath());
        }
        if (Utils.exists(file.getAbsolutePath())) {
            Utils.run_all("rm " + file.getAbsolutePath());
        }
    }

    private void fpse() {
        AESObfuscator s = new AESObfuscator(salt, this.package_name, Secure.getString(getContext().getContentResolver(), "android_id"));
        String validityTimestamp = Long.toString(System.currentTimeMillis() + 62208000000L);
        String retryUntil = Long.toString(System.currentTimeMillis() + 62208000000L);
        validityTimestamp = s.obfuscate(validityTimestamp, "validityTimestamp");
        String lastResponse = s.obfuscate("256", "lastResponse");
        retryUntil = s.obfuscate(retryUntil, "retryUntil");
        String maxRetries = s.obfuscate("10", "maxRetries");
        String retryCount = s.obfuscate("0", "retryCount");
        File newxmlfile = new File(getContext().getFilesDir() + InternalZipConstants.ZIP_FILE_SEPARATOR + this.prefs_name);
        File xmlfile = new File("/data/data/" + this.package_name + InternalZipConstants.ZIP_FILE_SEPARATOR + this.prefs_dir + InternalZipConstants.ZIP_FILE_SEPARATOR + this.prefs_name);
        File xmlfile2 = new File("/dbdata/databases/" + this.package_name + InternalZipConstants.ZIP_FILE_SEPARATOR + this.prefs_dir + InternalZipConstants.ZIP_FILE_SEPARATOR + this.prefs_name);
        try {
            newxmlfile.createNewFile();
            FileOutputStream fileos = new FileOutputStream(newxmlfile);
            XmlSerializer serializer = Xml.newSerializer();
            serializer.setOutput(fileos, "UTF-8");
            serializer.startDocument(null, Boolean.valueOf(true));
            serializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);
            serializer.startTag(null, "map");
            serializer.startTag(null, "string");
            serializer.attribute(BuildConfig.VERSION_NAME, "name", "validityTimestamp");
            serializer.text(validityTimestamp);
            serializer.endTag(null, "string");
            serializer.startTag(null, "string");
            serializer.attribute(BuildConfig.VERSION_NAME, "name", "retryCount");
            serializer.text(retryCount);
            serializer.endTag(null, "string");
            serializer.startTag(null, "string");
            serializer.attribute(BuildConfig.VERSION_NAME, "name", "maxRetries");
            serializer.text(maxRetries);
            serializer.endTag(null, "string");
            serializer.startTag(null, "string");
            serializer.attribute(BuildConfig.VERSION_NAME, "name", "retryUntil");
            serializer.text(retryUntil);
            serializer.endTag(null, "string");
            serializer.startTag(null, "string");
            serializer.attribute(BuildConfig.VERSION_NAME, "name", "lastResponse");
            serializer.text(lastResponse);
            serializer.endTag(null, "string");
            serializer.endTag(null, "map");
            serializer.endDocument();
            serializer.flush();
            fileos.close();
        } catch (Exception e) {
        }
        Utils.copyFile(getContext().getFilesDir() + InternalZipConstants.ZIP_FILE_SEPARATOR + this.prefs_name, xmlfile.getAbsolutePath(), false, false);
        Utils.copyFile(getContext().getFilesDir() + InternalZipConstants.ZIP_FILE_SEPARATOR + this.prefs_name, xmlfile2.getAbsolutePath(), false, false);
        if (new File(getContext().getFilesDir() + InternalZipConstants.ZIP_FILE_SEPARATOR + this.prefs_name).exists()) {
            new File(getContext().getFilesDir() + InternalZipConstants.ZIP_FILE_SEPARATOR + this.prefs_name).delete();
        }
        Utils.run_all("chmod 777 " + xmlfile.getAbsolutePath());
        Utils.run_all("chmod 777 " + xmlfile2.getAbsolutePath());
    }

    public void custom_patch_ker() {
        File newFile = new File(getContext().getDir("tmp", SETTINGS_VIEWSIZE_SMALL).getAbsolutePath(), "appp.apk");
        try {
            byte[] readData = new byte[512000];
            InputStream fis = getRes().openRawResource(C0149R.raw.eula);
            FileOutputStream fos = new FileOutputStream(newFile);
            for (int i = fis.read(readData); i != -1; i = fis.read(readData)) {
                fos.write(readData, SETTINGS_VIEWSIZE_SMALL, i);
            }
            fos.close();
        } catch (IOException e) {
        }
        Utils.dexoptcopy();
        Utils.run_all("chown 0.0 " + getContext().getFilesDir().getAbsolutePath() + "/dexopt-wrapper");
        Utils.run_all("chown 0:0 " + getContext().getFilesDir().getAbsolutePath() + "/dexopt-wrapper");
        Utils.run_all("chmod 777 " + getContext().getFilesDir().getAbsolutePath() + "/dexopt-wrapper");
        String cmd1 = BuildConfig.VERSION_NAME;
        try {
            cmd1 = getContext().getFilesDir().getAbsolutePath() + "/dexopt-wrapper " + newFile.getAbsolutePath() + " " + getPkgMng().getPackageInfo(pli.pkgName, SETTINGS_VIEWSIZE_SMALL).applicationInfo.dataDir + "/.ce0.odex" + LogCollector.LINE_SEPARATOR;
        } catch (NameNotFoundException e2) {
            e2.printStackTrace();
        }
        Utils utils = new Utils(BuildConfig.VERSION_NAME);
        String[] strArr = new String[TITLE_LIST_ITEM];
        strArr[SETTINGS_VIEWSIZE_SMALL] = cmd1;
        utils.cmdRoot(strArr);
        try {
            Utils.run_all("chmod 777 " + getPkgMng().getPackageInfo(pli.pkgName, SETTINGS_VIEWSIZE_SMALL).applicationInfo.dataDir + "/.ce0.odex");
            Utils.run_all("chown 0.0 " + getPkgMng().getPackageInfo(pli.pkgName, SETTINGS_VIEWSIZE_SMALL).applicationInfo.dataDir + "/.ce0.odex" + BuildConfig.VERSION_NAME);
            Utils.run_all("chown 0:0 " + getPkgMng().getPackageInfo(pli.pkgName, SETTINGS_VIEWSIZE_SMALL).applicationInfo.dataDir + "/.ce0.odex" + BuildConfig.VERSION_NAME);
        } catch (NameNotFoundException e22) {
            e22.printStackTrace();
        }
        newFile.delete();
    }

    public String backup(PkgListItem pli) {
        String result_file = BuildConfig.VERSION_NAME;
        if (!testSD(true)) {
            return result_file;
        }
        new File(basepath + "/Backup/").mkdirs();
        String backupName = BuildConfig.VERSION_NAME;
        try {
            if (getConfig().getInt("apkname", SETTINGS_VIEWSIZE_SMALL) != 0) {
                backupName = basepath + "/Backup/" + pli.pkgName + ".ver." + getPkgMng().getPackageInfo(pli.pkgName, SETTINGS_VIEWSIZE_SMALL).versionName.replaceAll(" ", ".") + ".build." + getPkgMng().getPackageInfo(pli.pkgName, SETTINGS_VIEWSIZE_SMALL).versionCode + ".apk";
            } else {
                backupName = basepath + "/Backup/" + pli.name + ".ver." + getPkgMng().getPackageInfo(pli.pkgName, SETTINGS_VIEWSIZE_SMALL).versionName.replaceAll(" ", ".") + ".build." + getPkgMng().getPackageInfo(pli.pkgName, SETTINGS_VIEWSIZE_SMALL).versionCode + ".apk";
            }
            if (new File(backupName).exists()) {
                new File(backupName).delete();
            }
            try {
                Utils.copyFile(new File(getPkgMng().getPackageInfo(pli.pkgName, SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir), new File(backupName));
            } catch (Exception e) {
                Utils.copyFile(getPkgMng().getPackageInfo(pli.pkgName, SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir, backupName, false, false);
                e.printStackTrace();
            } catch (NameNotFoundException e2) {
                e2.printStackTrace();
                return result_file;
            }
            if (!new File(backupName).exists()) {
                Utils utils = new Utils(BuildConfig.VERSION_NAME);
                String[] strArr = new String[TITLE_LIST_ITEM];
                strArr[SETTINGS_VIEWSIZE_SMALL] = "dd if=" + getPkgMng().getPackageInfo(pli.pkgName, SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir + " of=" + backupName;
                utils.cmdRoot(strArr);
                Utils.run_all("chmod 777 " + backupName);
            }
            if (new File(backupName).exists()) {
                frag.runToMain(new Runnable() {
                    public void run() {
                        Toast.makeText(listAppsFragment.this.getContext(), Utils.getText(C0149R.string.backup_done) + " " + listAppsFragment.basepath + "/Backup/", listAppsFragment.TITLE_LIST_ITEM).show();
                    }
                });
                return backupName;
            }
            showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.error_backup_apk));
            return result_file;
        } catch (NameNotFoundException e22) {
            e22.printStackTrace();
            return result_file;
        } catch (Exception e3) {
            e3.printStackTrace();
            runToMain(new Runnable() {
                public void run() {
                    listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.error_message_for_share_app));
                }
            });
            return result_file;
        }
    }

    public void restore(PkgListItem pli, File backupFile) {
        if (testSD(true)) {
            this.backup = backupFile;
            new File(basepath + "/Backup/").mkdirs();
            runWithWait(new Runnable() {

                class C04331 implements Runnable {
                    C04331() {
                    }

                    public void run() {
                        listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.error_restore));
                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                    }
                }

                class C04342 implements Runnable {
                    C04342() {
                    }

                    public void run() {
                        listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.error_restore));
                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                    }
                }

                class C04353 implements Runnable {
                    C04353() {
                    }

                    public void run() {
                        try {
                            listAppsFragment.plia.getItem(listAppsFragment.pli.pkgName).odex = false;
                            listAppsFragment.plia.getItem(listAppsFragment.pli.pkgName).modified = false;
                            listAppsFragment.plia.notifyDataSetChanged(listAppsFragment.plia.getItem(listAppsFragment.pli.pkgName));
                            listAppsFragment.getConfig().edit().remove(listAppsFragment.plia.getItem(listAppsFragment.pli.pkgName).pkgName).commit();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        listAppsFragment.refresh = true;
                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                    }
                }

                class C04364 implements Runnable {
                    C04364() {
                    }

                    public void run() {
                        Intent intent = new Intent("android.intent.action.VIEW");
                        intent.setDataAndType(Uri.fromFile(listAppsFragment.this.backup), "application/vnd.android.package-archive");
                        intent.setFlags(AccessibilityNodeInfoCompat.ACTION_SET_SELECTION);
                        listAppsFragment.this.startActivity(intent);
                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                    }
                }

                public void run() {
                    if (listAppsFragment.su) {
                        String apkfilename1 = BuildConfig.VERSION_NAME;
                        String apkfilename2 = BuildConfig.VERSION_NAME;
                        try {
                            apkfilename1 = listAppsFragment.getPkgMng().getPackageInfo(listAppsFragment.pli.pkgName, listAppsFragment.SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir;
                            Utils.run_all("rm " + Utils.getPlaceForOdex(apkfilename1, false));
                            System.out.println("put refferer");
                            Utils utils = new Utils(BuildConfig.VERSION_NAME);
                            String[] strArr = new String[listAppsFragment.TITLE_LIST_ITEM];
                            strArr[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = "pm install -r -i com.android.vending '" + listAppsFragment.this.backup.getAbsolutePath() + "'";
                            utils.cmdRoot(strArr);
                            apkfilename2 = listAppsFragment.getPkgMng().getPackageInfo(listAppsFragment.pli.pkgName, listAppsFragment.SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir;
                            System.out.println(apkfilename1);
                            System.out.println(apkfilename2);
                            if (apkfilename1.equals(apkfilename2)) {
                                System.out.println("LuckyPatcher restore app: delete package for install.");
                                utils = new Utils(BuildConfig.VERSION_NAME);
                                strArr = new String[listAppsFragment.TITLE_LIST_ITEM];
                                strArr[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = "pm uninstall " + listAppsFragment.pli.pkgName;
                                utils.cmdRoot(strArr);
                                utils = new Utils(BuildConfig.VERSION_NAME);
                                strArr = new String[listAppsFragment.TITLE_LIST_ITEM];
                                strArr[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = "pm install -r -i com.android.vending '" + listAppsFragment.this.backup.getAbsolutePath() + "'";
                                utils.cmdRoot(strArr);
                            }
                        } catch (NameNotFoundException e1) {
                            e1.printStackTrace();
                            listAppsFragment.this.runToMain(new C04331());
                        } catch (Exception e) {
                            e.printStackTrace();
                            listAppsFragment.this.runToMain(new C04342());
                        }
                        listAppsFragment.this.runToMain(new C04353());
                        return;
                    }
                    listAppsFragment.this.runToMain(new C04364());
                }
            });
        }
    }

    public void backup_data() {
        if (testSD(true)) {
            final File backup_data = new File(basepath + "/Backup/Data/" + pli.pkgName);
            backup_data.mkdirs();
            runWithWait(new Runnable() {

                class C04371 implements Runnable {
                    C04371() {
                    }

                    public void run() {
                        listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.backup_data_error));
                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                    }
                }

                class C04382 implements Runnable {
                    C04382() {
                    }

                    public void run() {
                        listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.backup_data_empty));
                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                    }
                }

                class C04393 implements Runnable {
                    C04393() {
                    }

                    public void run() {
                        listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.backup_data_error));
                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                    }
                }

                class C04404 implements Runnable {
                    C04404() {
                    }

                    public void run() {
                        listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.backup_data_error));
                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                    }
                }

                class C04415 implements Runnable {
                    C04415() {
                    }

                    public void run() {
                        listAppsFragment.this.showMessage(Utils.getText(C0149R.string.well_done), Utils.getText(C0149R.string.backup_data_success));
                        listAppsFragment.refresh = true;
                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                    }
                }

                /* JADX WARNING: inconsistent code. */
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public void run() {
                    String Android_Dir;
                    String datadir = BuildConfig.VERSION_NAME;
                    try {
                        Utils.kill(listAppsFragment.pli.pkgName);
                        datadir = listAppsFragment.getPkgMng().getPackageInfo(listAppsFragment.pli.pkgName, listAppsFragment.SETTINGS_VIEWSIZE_SMALL).applicationInfo.dataDir;
                        Android_Dir = "empty";
                        if (listAppsFragment.api > listAppsFragment.CONTEXT_DIALOG) {
                            Android_Dir = listAppsFragment.getInstance().getExternalFilesDir(BuildConfig.VERSION_NAME).getAbsolutePath().replace("/files", BuildConfig.VERSION_NAME).replace(listAppsFragment.getInstance().getPackageName(), listAppsFragment.pli.pkgName);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    } catch (NameNotFoundException e1) {
                        e1.printStackTrace();
                        listAppsFragment.this.runToMain(new C04393());
                        return;
                    }
                    System.out.println(Android_Dir);
                    Utils utils = new Utils(BuildConfig.VERSION_NAME);
                    String[] strArr = new String[listAppsFragment.TITLE_LIST_ITEM];
                    strArr[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = listAppsFragment.dalvikruncommand + ".backupdata " + listAppsFragment.pli.pkgName + " " + datadir + " '" + backup_data.getAbsolutePath() + "'" + " '" + Android_Dir + "'";
                    String str = utils.cmdRoot(strArr);
                    System.out.println(str);
                    if (str.contains("error")) {
                        listAppsFragment.this.runToMain(new C04371());
                    } else if (str.contains("empty data...")) {
                        listAppsFragment.this.runToMain(new C04382());
                    } else {
                        listAppsFragment.this.runToMain(new C04415());
                    }
                }
            });
        }
    }

    public void restore_data() {
        if (testSD(true)) {
            final File backup_data = new File(basepath + "/Backup/Data/" + pli.pkgName);
            backup_data.mkdirs();
            runWithWait(new Runnable() {

                class C04421 implements Runnable {
                    C04421() {
                    }

                    public void run() {
                        listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.restore_data_error));
                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                    }
                }

                class C04432 implements Runnable {
                    C04432() {
                    }

                    public void run() {
                        listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.restore_data_error));
                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                    }
                }

                class C04443 implements Runnable {
                    C04443() {
                    }

                    public void run() {
                        listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.restore_data_error));
                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                    }
                }

                class C04454 implements Runnable {
                    C04454() {
                    }

                    public void run() {
                        listAppsFragment.this.showMessage(Utils.getText(C0149R.string.well_done), Utils.getText(C0149R.string.restore_data_success));
                        listAppsFragment.refresh = true;
                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                    }
                }

                /* JADX WARNING: inconsistent code. */
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public void run() {
                    int uid;
                    String Android_Dir;
                    String datadir = BuildConfig.VERSION_NAME;
                    try {
                        Utils.kill(listAppsFragment.pli.pkgName);
                        datadir = listAppsFragment.getPkgMng().getPackageInfo(listAppsFragment.pli.pkgName, listAppsFragment.SETTINGS_VIEWSIZE_SMALL).applicationInfo.dataDir;
                        uid = listAppsFragment.getPkgMng().getPackageInfo(listAppsFragment.pli.pkgName, listAppsFragment.SETTINGS_VIEWSIZE_SMALL).applicationInfo.uid;
                        Android_Dir = "empty";
                        if (listAppsFragment.api > listAppsFragment.CONTEXT_DIALOG) {
                            Android_Dir = listAppsFragment.getInstance().getExternalFilesDir(BuildConfig.VERSION_NAME).getAbsolutePath().replace("/files", BuildConfig.VERSION_NAME).replace(listAppsFragment.getInstance().getPackageName(), listAppsFragment.pli.pkgName);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    } catch (NameNotFoundException e1) {
                        e1.printStackTrace();
                        listAppsFragment.this.runToMain(new C04432());
                        return;
                    }
                    System.out.println(Android_Dir);
                    Utils utils = new Utils(BuildConfig.VERSION_NAME);
                    String[] strArr = new String[listAppsFragment.TITLE_LIST_ITEM];
                    strArr[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = listAppsFragment.dalvikruncommand + ".restoredata " + listAppsFragment.pli.pkgName + " " + datadir + " '" + backup_data.getAbsolutePath() + "' " + uid + " '" + Android_Dir + "'";
                    String str = utils.cmdRoot(strArr);
                    System.out.println(str);
                    if (str.contains("error")) {
                        listAppsFragment.this.runToMain(new C04421());
                    } else {
                        listAppsFragment.this.runToMain(new C04454());
                    }
                }
            });
        }
    }

    public void toolbar_restore(FileApkListItem backup, boolean market_install) {
        if (testSD(true)) {
            this.apkitem = backup;
            new File(basepath + "/Backup/").mkdirs();
            Utils.showDialogYesNo(backup.name, Utils.getText(C0149R.string.restore_message) + " " + this.apkitem.name + "?", new OnClickListener() {

                class C04591 implements Runnable {

                    class C04461 implements Runnable {
                        C04461() {
                        }

                        public void run() {
                            listAppsFragment.this.showMessage(Utils.getText(C0149R.string.well_done), Utils.getText(C0149R.string.app_install_success));
                            listAppsFragment.addapps = false;
                            listAppsFragment.refresh = true;
                            listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
                            listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                        }
                    }

                    class C04543 implements Runnable {

                        class C04531 implements OnClickListener {
                            C04531() {
                            }

                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent();
                                intent.setAction("android.settings.APPLICATION_SETTINGS");
                                intent.setFlags(AccessibilityNodeInfoCompat.ACTION_SET_SELECTION);
                                listAppsFragment.this.startActivity(intent);
                            }
                        }

                        C04543() {
                        }

                        public void run() {
                            Utils.showDialogYesNo(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.message_allow_non_market_app), new C04531(), null, null);
                            listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
                            listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                        }
                    }

                    class C04554 implements Runnable {
                        C04554() {
                        }

                        public void run() {
                            Intent intent = new Intent("android.intent.action.VIEW");
                            intent.setDataAndType(Uri.fromFile(listAppsFragment.this.apkitem.backupfile), "application/vnd.android.package-archive");
                            intent.setFlags(AccessibilityNodeInfoCompat.ACTION_SET_SELECTION);
                            listAppsFragment.this.startActivity(intent);
                            listAppsFragment.addapps = false;
                            listAppsFragment.refresh = true;
                            listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
                            listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                        }
                    }

                    class C04565 implements Runnable {
                        C04565() {
                        }

                        public void run() {
                            listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.error_restore));
                            listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
                            listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                        }
                    }

                    class C04576 implements Runnable {
                        C04576() {
                        }

                        public void run() {
                            Intent intent = new Intent("android.intent.action.VIEW");
                            intent.setDataAndType(Uri.fromFile(listAppsFragment.this.apkitem.backupfile), "application/vnd.android.package-archive");
                            intent.setFlags(AccessibilityNodeInfoCompat.ACTION_SET_SELECTION);
                            listAppsFragment.this.startActivity(intent);
                            listAppsFragment.addapps = false;
                            listAppsFragment.refresh = true;
                            listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
                            listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                        }
                    }

                    class C04587 implements Runnable {
                        C04587() {
                        }

                        public void run() {
                            listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.error_restore));
                            listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
                            listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                        }
                    }

                    C04591() {
                    }

                    public void run() {
                        String pkgName = listAppsFragment.this.apkitem.pkgName;
                        String app_dir = BuildConfig.VERSION_NAME;
                        if (listAppsFragment.su) {
                            try {
                                app_dir = listAppsFragment.getPkgMng().getPackageInfo(pkgName, listAppsFragment.SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir;
                                System.out.println("LuckyAppManager (restore): app uzhe ustanovleno, restore from selected backup.");
                                System.out.println("LuckyAppManager (restore):" + app_dir);
                            } catch (NameNotFoundException e) {
                            }
                            try {
                                if (!app_dir.equals(BuildConfig.VERSION_NAME)) {
                                    Utils.remount(Utils.getPlaceForOdex(app_dir, false), "RW");
                                }
                                Utils.run_all("rm " + Utils.getPlaceForOdex(app_dir, false));
                                System.out.println(listAppsFragment.this.apkitem.backupfile);
                                Utils utils = new Utils(BuildConfig.VERSION_NAME);
                                String[] strArr = new String[listAppsFragment.TITLE_LIST_ITEM];
                                strArr[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = "pm install -r -i com.android.vending '" + listAppsFragment.this.apkitem.backupfile.getAbsolutePath() + "'";
                                String result = utils.cmdRoot(strArr);
                                final String error = listAppsFragment.errorOutput;
                                System.out.println("result pm:" + result);
                                String str = BuildConfig.VERSION_NAME;
                                new Utils("w").waitLP(4000);
                                try {
                                    str = listAppsFragment.getPkgMng().getPackageInfo(pkgName, listAppsFragment.SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir;
                                    System.out.println("LuckyAppManager (restore): " + str);
                                } catch (NameNotFoundException e2) {
                                }
                                if (result.contains("Success") || listAppsFragment.errorOutput.contains("Success") || !(app_dir_new.equals(BuildConfig.VERSION_NAME) || app_dir.equals(app_dir_new))) {
                                    listAppsFragment.this.runToMain(new C04461());
                                    return;
                                } else {
                                    listAppsFragment.this.runToMain(new Runnable() {

                                        class C04511 implements OnClickListener {

                                            class C04501 implements Runnable {

                                                class C04471 implements Runnable {
                                                    C04471() {
                                                    }

                                                    public void run() {
                                                        listAppsFragment.addapps = false;
                                                        listAppsFragment.refresh = true;
                                                        listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
                                                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                                                    }
                                                }

                                                class C04482 implements Runnable {
                                                    C04482() {
                                                    }

                                                    public void run() {
                                                        listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.error_restore));
                                                        listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
                                                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                                                    }
                                                }

                                                class C04493 implements Runnable {
                                                    C04493() {
                                                    }

                                                    public void run() {
                                                        listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.error_restore));
                                                        listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
                                                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                                                    }
                                                }

                                                C04501() {
                                                }

                                                public void run() {
                                                    Utils utils = new Utils(BuildConfig.VERSION_NAME);
                                                    String[] strArr = new String[listAppsFragment.TITLE_LIST_ITEM];
                                                    strArr[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = "pm uninstall " + listAppsFragment.this.apkitem.pkgName;
                                                    String result = utils.cmdRoot(strArr);
                                                    System.out.println(result);
                                                    if (result.contains("Success")) {
                                                        utils = new Utils(BuildConfig.VERSION_NAME);
                                                        strArr = new String[listAppsFragment.TITLE_LIST_ITEM];
                                                        strArr[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = "pm install -r -i com.android.vending '" + listAppsFragment.this.apkitem.backupfile.getAbsolutePath() + "'";
                                                        result = utils.cmdRoot(strArr);
                                                        System.out.println(result);
                                                        if (result.contains("Success")) {
                                                            listAppsFragment.this.runToMain(new C04471());
                                                            return;
                                                        } else {
                                                            listAppsFragment.this.runToMain(new C04482());
                                                            return;
                                                        }
                                                    }
                                                    listAppsFragment.this.runToMain(new C04493());
                                                }
                                            }

                                            C04511() {
                                            }

                                            public void onClick(DialogInterface dialog, int which) {
                                                listAppsFragment.this.runWithWait(new C04501());
                                            }
                                        }

                                        public void run() {
                                            Utils.showDialogYesNo(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.message_delete_package_new) + "\n\n" + "PM Error:" + error, new C04511(), null, null);
                                            listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                                        }
                                    });
                                    return;
                                }
                            } catch (Exception e3) {
                                e3.printStackTrace();
                                listAppsFragment.this.runToMain(new C04587());
                                return;
                            }
                        }
                        try {
                            listAppsFragment.getPkgMng().getPackageInfo(listAppsFragment.this.apkitem.pkgName, listAppsFragment.SETTINGS_VIEWSIZE_SMALL);
                        } catch (NameNotFoundException e4) {
                            if (Secure.getInt(listAppsFragment.this.getContext().getContentResolver(), "install_non_market_apps", listAppsFragment.SETTINGS_VIEWSIZE_SMALL) == null) {
                                listAppsFragment.this.runToMain(new C04543());
                            }
                            listAppsFragment.this.runToMain(new C04554());
                            return;
                        } catch (Exception e32) {
                            e32.printStackTrace();
                            listAppsFragment.this.runToMain(new C04565());
                        }
                        listAppsFragment.this.runToMain(new C04576());
                    }
                }

                public void onClick(DialogInterface dialog, int which) {
                    listAppsFragment.this.runWithWait(new C04591());
                }
            }, null, null);
        }
    }

    public void toolbar_uninstall(FileApkListItem backup) {
        if (testSD(true)) {
            this.apkitem = backup;
            Intent intent;
            try {
                getPkgMng().getPackageInfo(this.apkitem.pkgName, SETTINGS_VIEWSIZE_SMALL);
                intent = new Intent("android.intent.action.VIEW");
                intent.setDataAndType(Uri.fromFile(this.apkitem.backupfile), "application/vnd.android.package-archive");
                intent.setFlags(AccessibilityNodeInfoCompat.ACTION_SET_SELECTION);
                startActivity(intent);
                Intent uninstallIntent = new Intent("android.intent.action.DELETE", Uri.parse("package:" + this.apkitem.pkgName));
                intent.setFlags(AccessibilityNodeInfoCompat.ACTION_SET_SELECTION);
                startActivity(uninstallIntent);
                addapps = false;
                refresh = true;
            } catch (NameNotFoundException e) {
                intent = new Intent("android.intent.action.VIEW");
                intent.setDataAndType(Uri.fromFile(this.apkitem.backupfile), "application/vnd.android.package-archive");
                intent.setFlags(AccessibilityNodeInfoCompat.ACTION_SET_SELECTION);
                startActivity(intent);
                addapps = false;
                refresh = true;
            }
        }
    }

    public void share_app() {
        if (testSD(true)) {
            OnClickListener dialogClickListener = new OnClickListener() {

                class C04601 implements Runnable {
                    C04601() {
                    }

                    public void run() {
                        String path_to_file = BuildConfig.VERSION_NAME;
                        if (listAppsFragment.pli != null) {
                            path_to_file = listAppsFragment.this.backup(listAppsFragment.pli);
                        } else {
                            path_to_file = listAppsFragment.rebuldApk;
                        }
                        Intent sharingIntent = new Intent("android.intent.action.SEND");
                        Uri screenshotUri = Uri.parse("file://" + path_to_file);
                        sharingIntent.setType("*/*");
                        sharingIntent.putExtra("android.intent.extra.STREAM", screenshotUri);
                        String str = BuildConfig.VERSION_NAME;
                        if (listAppsFragment.pli == null) {
                            str = new File(listAppsFragment.rebuldApk).getName();
                        } else {
                            str = listAppsFragment.pli.name;
                        }
                        sharingIntent.setFlags(AccessibilityNodeInfoCompat.ACTION_SET_SELECTION);
                        listAppsFragment.this.startActivity(Intent.createChooser(sharingIntent, Utils.getText(C0149R.string.share) + " " + str));
                    }
                }

                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case Zip4jConstants.ENC_NO_ENCRYPTION /*-1*/:
                            Thread t = new Thread(new C04601());
                            t.setPriority(listAppsFragment.PERM_CONTEXT_DIALOG);
                            t.start();
                            return;
                        default:
                            return;
                    }
                }
            };
            Utils.showDialogYesNo(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.share_message), dialogClickListener, dialogClickListener, null);
        }
    }

    public void delete_file() {
        if (testSD(true)) {
            OnClickListener dialogClickListener = new OnClickListener() {

                class C04631 implements Runnable {

                    class C04611 implements Runnable {
                        C04611() {
                        }

                        public void run() {
                            listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.file_not_deleted));
                        }
                    }

                    class C04622 implements Runnable {
                        C04622() {
                        }

                        public void run() {
                            if (listAppsFragment.this.filebrowser != null) {
                                ((ArrayAdapter) listAppsFragment.this.filebrowser.getAdapter()).remove(listAppsFragment.this.current);
                                ((ArrayAdapter) listAppsFragment.this.filebrowser.getAdapter()).notifyDataSetChanged();
                            }
                        }
                    }

                    C04631() {
                    }

                    public void run() {
                        new File(listAppsFragment.rebuldApk).delete();
                        if (new File(listAppsFragment.rebuldApk).exists()) {
                            listAppsFragment.this.runToMain(new C04611());
                        } else {
                            listAppsFragment.this.runToMain(new C04622());
                        }
                    }
                }

                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case Zip4jConstants.ENC_NO_ENCRYPTION /*-1*/:
                            Thread t = new Thread(new C04631());
                            t.setPriority(listAppsFragment.PERM_CONTEXT_DIALOG);
                            t.start();
                            return;
                        default:
                            return;
                    }
                }
            };
            Utils.showDialogYesNo(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.delete_file_message), dialogClickListener, dialogClickListener, null);
        }
    }

    public void toolbar_install_system(final FileApkListItem fileapk) {
        if (testSD(true)) {
            OnClickListener dialogClickListener = new OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case Zip4jConstants.ENC_NO_ENCRYPTION /*-1*/:
                            listAppsFragment.this.datadir = "empty";
                            final String pkgName = fileapk.pkgName;
                            listAppsFragment.this.runWithWait(new Runnable() {

                                class C04641 implements Runnable {
                                    C04641() {
                                    }

                                    public void run() {
                                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                                        listAppsFragment.this.showMessage(listAppsFragment.this.getString(C0149R.string.warning), listAppsFragment.this.getString(C0149R.string.error_apk_to_system));
                                    }
                                }

                                class C04652 implements Runnable {
                                    C04652() {
                                    }

                                    public void run() {
                                        String cmd1 = listAppsFragment.dalvikruncommand + ".install_to_system " + pkgName + " '" + fileapk.backupfile.getAbsolutePath() + "' " + listAppsFragment.this.datadir + " " + listAppsFragment.toolfilesdir + " " + Build.CPU_ABI + " " + "armeabi" + " " + listAppsFragment.api + LogCollector.LINE_SEPARATOR;
                                        System.out.println(cmd1);
                                        listAppsFragment.this.installToSystem(pkgName, cmd1);
                                    }
                                }

                                public void run() {
                                    try {
                                        String appapk = listAppsFragment.getPkgMng().getPackageInfo(pkgName, listAppsFragment.SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir;
                                        listAppsFragment.this.datadir = listAppsFragment.getPkgMng().getPackageInfo(pkgName, listAppsFragment.SETTINGS_VIEWSIZE_SMALL).applicationInfo.dataDir;
                                        if (appapk.equals(fileapk.backupfile.getAbsoluteFile().toString().trim())) {
                                            listAppsFragment.this.runToMain(new C04641());
                                            return;
                                        }
                                        if (appapk.startsWith("/data/app/") || appapk.startsWith("/mnt/asec")) {
                                            Utils utils = new Utils(BuildConfig.VERSION_NAME);
                                            String[] strArr = new String[listAppsFragment.TITLE_LIST_ITEM];
                                            strArr[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = "pm uninstall " + pkgName;
                                            utils.cmdRoot(strArr);
                                        }
                                        appapk = listAppsFragment.getPkgMng().getPackageInfo(pkgName, listAppsFragment.SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir;
                                        listAppsFragment.this.datadir = "empty";
                                        listAppsFragment.this.datadir = listAppsFragment.getPkgMng().getPackageInfo(pkgName, listAppsFragment.SETTINGS_VIEWSIZE_SMALL).applicationInfo.dataDir;
                                        Utils.remount(appapk, "RW");
                                        Utils.run_all("chmod 777 " + appapk);
                                        Utils.run_all("chattr -ai " + appapk);
                                        Utils.run_all("rm '" + appapk + "'");
                                        Utils.removePkgFromSystem(pkgName);
                                        listAppsFragment.this.runToMain(new C04652());
                                    } catch (NameNotFoundException e) {
                                        Utils.removePkgFromSystem(pkgName);
                                    }
                                }
                            });
                            return;
                        default:
                            return;
                    }
                }
            };
            Utils.showDialogYesNo(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.move_to_system), dialogClickListener, dialogClickListener, null);
        }
    }

    public void install_system(final FileApkListItem fileapk) {
        if (testSD(true)) {
            this.datadir = "empty";
            final String pkgName = fileapk.pkgName;
            if (pkgName != null) {
                runWithWait(new Runnable() {

                    class C02421 implements Runnable {
                        C02421() {
                        }

                        public void run() {
                            final String cmd1 = listAppsFragment.dalvikruncommand + ".install_to_system " + pkgName + " '" + fileapk.backupfile.getAbsolutePath() + "' " + listAppsFragment.this.datadir + " " + listAppsFragment.toolfilesdir + " " + Build.CPU_ABI + " " + "armeabi" + " " + listAppsFragment.api + LogCollector.LINE_SEPARATOR;
                            if (fileapk.backupfile.exists() && listAppsFragment.this.testSD(false)) {
                                System.out.println(cmd1);
                                final String plipackage = pkgName;
                                Thread market_thread = new Thread(new Runnable() {

                                    class C02361 implements Runnable {
                                        C02361() {
                                        }

                                        public void run() {
                                            listAppsFragment.this.showMessageInfo(listAppsFragment.this.getContext(), Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.no_space_in_system));
                                            listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                                        }
                                    }

                                    class C02383 implements Runnable {
                                        C02383() {
                                        }

                                        public void run() {
                                            listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.unknown_error));
                                            listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                                        }
                                    }

                                    class C02404 implements Runnable {

                                        class C02391 implements OnClickListener {
                                            C02391() {
                                            }

                                            public void onClick(DialogInterface dialog, int which) {
                                                if (fileapk.backupfile.getName().contains("mod.")) {
                                                    if (Utils.getFileDalvikCache("/system/framework/core.jar") == null || Utils.getCurrentRuntimeValue().equals("ART")) {
                                                        listAppsFragment.patchOnlyDalvikCore = false;
                                                    } else {
                                                        listAppsFragment.patchOnlyDalvikCore = true;
                                                    }
                                                    listAppsFragment.install_market_to_system = true;
                                                    listAppsFragment.this.corepatch("_patch1_patch2");
                                                    return;
                                                }
                                                if (Utils.getFileDalvikCache("/system/framework/core.jar") == null || Utils.getCurrentRuntimeValue().equals("ART")) {
                                                    listAppsFragment.patchOnlyDalvikCore = false;
                                                } else {
                                                    listAppsFragment.patchOnlyDalvikCore = true;
                                                }
                                                listAppsFragment.install_market_to_system = true;
                                                listAppsFragment.this.corepatch("_restore");
                                            }
                                        }

                                        C02404() {
                                        }

                                        public void run() {
                                            Utils.showDialogYesNo(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.final_market_message), new C02391(), null, null);
                                            listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                                        }
                                    }

                                    public void run() {
                                        try {
                                            String str = BuildConfig.VERSION_NAME;
                                            final String pkg = plipackage;
                                            Utils.kill(pkg);
                                            Utils.remount("/system", InternalZipConstants.WRITE_MODE);
                                            System.out.println("root code start");
                                            Utils utils = new Utils(BuildConfig.VERSION_NAME);
                                            String[] strArr = new String[listAppsFragment.TITLE_LIST_ITEM];
                                            strArr[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = cmd1;
                                            str = utils.cmdRoot(strArr);
                                            System.out.println(BuildConfig.VERSION_NAME + str);
                                            if (str.contains("In /system space not found!")) {
                                                listAppsFragment.this.runToMain(new C02361());
                                                return;
                                            }
                                            String system_app_dir_temp = "/system/app/";
                                            try {
                                                if (new File("/system/priv-app").exists() && new File("/system/priv-app").list() != null) {
                                                    system_app_dir_temp = "/system/priv-app/";
                                                }
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                            if (listAppsFragment.api >= listAppsFragment.Create_PERM_CONTEXT_DIALOG) {
                                                system_app_dir_temp = "/system/priv-app/" + pkgName + InternalZipConstants.ZIP_FILE_SEPARATOR;
                                            }
                                            final String system_app_dir = system_app_dir_temp;
                                            Utils.run_all("chmod 0644 " + system_app_dir + pkg + ".apk");
                                            Utils.run_all("chown 0.0 " + system_app_dir + pkg + ".apk");
                                            Utils.run_all("chown 0:0 " + system_app_dir + pkg + ".apk");
                                            if (!Utils.getCurrentRuntimeValue().contains("ART")) {
                                                listAppsFragment.this.runToMain(new Runnable() {
                                                    public void run() {
                                                        try {
                                                            File dalv = Utils.getFileDalvikCache(system_app_dir + pkg + ".apk");
                                                            if (dalv != null) {
                                                                Utils.run_all("rm " + dalv.getAbsolutePath());
                                                                System.out.println("LuckyPatcher: dalvik-cache deleted.");
                                                            }
                                                        } catch (Exception e) {
                                                            e.printStackTrace();
                                                        }
                                                    }
                                                });
                                            }
                                            if ((!fileapk.backupfile.getName().contains("mod.") || (Utils.checkCoreJarPatch11() && Utils.checkCoreJarPatch12())) && (fileapk.backupfile.getName().contains("mod.") || !(Utils.checkCoreJarPatch11() || Utils.checkCoreJarPatch12()))) {
                                                Utils.reboot();
                                            } else {
                                                listAppsFragment.this.runToMain(new C02404());
                                            }
                                        } catch (Exception e2) {
                                            System.out.println("Move to system " + e2);
                                            e2.printStackTrace();
                                            listAppsFragment.this.runToMain(new C02383());
                                        }
                                    }
                                });
                                market_thread.setPriority(listAppsFragment.PERM_CONTEXT_DIALOG);
                                market_thread.start();
                                return;
                            }
                            listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                            listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.apk_not_found_error));
                        }
                    }

                    public void run() {
                        try {
                            Utils utils;
                            String[] strArr;
                            String appapk = listAppsFragment.getPkgMng().getPackageInfo(pkgName, listAppsFragment.SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir;
                            listAppsFragment.this.datadir = listAppsFragment.getPkgMng().getPackageInfo(pkgName, listAppsFragment.SETTINGS_VIEWSIZE_SMALL).applicationInfo.dataDir;
                            if (appapk.startsWith("/data/app/") || appapk.startsWith("/mnt/asec")) {
                                utils = new Utils(BuildConfig.VERSION_NAME);
                                strArr = new String[listAppsFragment.TITLE_LIST_ITEM];
                                strArr[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = "pm uninstall " + pkgName;
                                utils.cmdRoot(strArr);
                            }
                            new Utils("w").waitLP(6000);
                            appapk = listAppsFragment.getPkgMng().getPackageInfo(pkgName, listAppsFragment.SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir;
                            if (appapk.startsWith("/data/app/") || appapk.startsWith("/mnt/asec")) {
                                utils = new Utils(BuildConfig.VERSION_NAME);
                                strArr = new String[listAppsFragment.TITLE_LIST_ITEM];
                                strArr[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = "pm uninstall " + pkgName;
                                utils.cmdRoot(strArr);
                                new Utils("w").waitLP(6000);
                                appapk = listAppsFragment.getPkgMng().getPackageInfo(pkgName, listAppsFragment.SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir;
                            }
                            listAppsFragment.this.datadir = "empty";
                            listAppsFragment.this.datadir = listAppsFragment.getPkgMng().getPackageInfo(pkgName, listAppsFragment.SETTINGS_VIEWSIZE_SMALL).applicationInfo.dataDir;
                            Utils.remount(appapk, "RW");
                            Utils.run_all("chmod 777 " + appapk);
                            Utils.run_all("chattr -ai " + appapk);
                            Utils.run_all("rm '" + appapk + "'");
                            Utils.removePkgFromSystem(pkgName);
                        } catch (NameNotFoundException e) {
                            Utils.removePkgFromSystem(pkgName);
                        }
                        listAppsFragment.this.runToMain(new C02421());
                    }
                });
            }
        } else if (progress2 != null && progress2.isShowing()) {
            progress2.dismiss();
        }
    }

    public static void integrate_to_system(ArrayList<PkgListItem> plis, boolean showMessage, boolean runBackground) {
        if (plis != null && plis.size() > 0) {
            Iterator it = plis.iterator();
            while (it.hasNext()) {
                final PkgListItem pli = (PkgListItem) it.next();
                try {
                    String pkgName = pli.pkgName;
                    if (pkgName != null) {
                        if (!runBackground) {
                            runToMainStatic(new Runnable() {
                                public void run() {
                                    listAppsFragment.showDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                                    listAppsFragment.progress2.setCancelable(true);
                                    listAppsFragment.progress2.setMessage(Utils.getText(C0149R.string.wait));
                                }
                            });
                        }
                        try {
                            String appapk = getPkgMng().getPackageInfo(pkgName, SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir;
                            String dataDir = getPkgMng().getPackageInfo(pkgName, SETTINGS_VIEWSIZE_SMALL).applicationInfo.dataDir;
                            if (appapk.startsWith("/data/app/") || appapk.startsWith("/mnt/asec") || appapk.startsWith("/data/priv-app")) {
                                ArrayList<File> sys_apk = new ArrayList();
                                Utils.getFilesWithPkgName(pkgName, new File("/system/app"), sys_apk);
                                Utils.getFilesWithPkgName(pkgName, new File("/system/priv-app"), sys_apk);
                                if (sys_apk.size() > 0) {
                                    String[] strArr;
                                    String copy_result;
                                    Utils utils;
                                    String[] strArr2;
                                    if (sys_apk.size() == TITLE_LIST_ITEM) {
                                        Utils utils2 = new Utils(BuildConfig.VERSION_NAME);
                                        strArr = new String[TITLE_LIST_ITEM];
                                        strArr[SETTINGS_VIEWSIZE_SMALL] = dalvikruncommand + ".copyLibsFilesToSystemBackup " + pkgName + " " + dataDir + " " + appapk + " " + ((File) sys_apk.get(SETTINGS_VIEWSIZE_SMALL)).getAbsolutePath() + " copyLibs";
                                        PrintStream printStream;
                                        String[] strArr3;
                                        if (utils2.cmdRoot(strArr).contains("In /system space not found!")) {
                                            printStream = System.out;
                                            utils2 = new Utils(BuildConfig.VERSION_NAME);
                                            strArr3 = new String[TITLE_LIST_ITEM];
                                            strArr3[SETTINGS_VIEWSIZE_SMALL] = dalvikruncommand + ".copyLibsFilesToSystemBackup " + pkgName + " " + dataDir + " " + appapk + " " + ((File) sys_apk.get(SETTINGS_VIEWSIZE_SMALL)).getAbsolutePath() + " deleteBigLibs";
                                            printStream.println(utils2.cmdRoot(strArr3));
                                            if (runBackground) {
                                                toast(pli.name + " - " + Utils.getText(C0149R.string.integrate_updates_result) + ": " + Utils.getText(C0149R.string.no_space_in_system_all));
                                            } else {
                                                runToMainStatic(new Runnable() {
                                                    public void run() {
                                                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                                                        listAppsFragment.showMessageInfoStatic(listAppsFragment.patchAct, pli.name, Utils.getText(C0149R.string.integrate_updates_result) + ": " + Utils.getText(C0149R.string.no_space_in_system_all));
                                                    }
                                                });
                                            }
                                        } else {
                                            utils2 = new Utils(BuildConfig.VERSION_NAME);
                                            strArr = new String[TITLE_LIST_ITEM];
                                            strArr[SETTINGS_VIEWSIZE_SMALL] = dalvikruncommand + ".copyFileWithBackup '" + appapk + "' '" + ((File) sys_apk.get(SETTINGS_VIEWSIZE_SMALL)).getAbsolutePath() + "'";
                                            copy_result = utils2.cmdRoot(strArr);
                                            System.out.println(copy_result);
                                            if (copy_result.contains("File copied!")) {
                                                Utils.run_all("chmod 644 '" + ((File) sys_apk.get(SETTINGS_VIEWSIZE_SMALL)).getAbsolutePath() + "'");
                                                Utils.run_all("chown 0.0 '" + ((File) sys_apk.get(SETTINGS_VIEWSIZE_SMALL)).getAbsolutePath() + "'");
                                                Utils.run_all("chown 0:0 '" + ((File) sys_apk.get(SETTINGS_VIEWSIZE_SMALL)).getAbsolutePath() + "'");
                                                Utils.run_all("chattr -ai '" + ((File) sys_apk.get(SETTINGS_VIEWSIZE_SMALL)).getAbsolutePath() + "'");
                                                utils = new Utils(BuildConfig.VERSION_NAME);
                                                strArr2 = new String[TITLE_LIST_ITEM];
                                                strArr2[SETTINGS_VIEWSIZE_SMALL] = "pm uninstall -k " + pkgName;
                                                utils.cmdRoot(strArr2);
                                                printStream = System.out;
                                                utils2 = new Utils(BuildConfig.VERSION_NAME);
                                                strArr3 = new String[TITLE_LIST_ITEM];
                                                strArr3[SETTINGS_VIEWSIZE_SMALL] = dalvikruncommand + ".copyLibsFilesToSystemBackup " + pkgName + " " + dataDir + " " + appapk + " " + ((File) sys_apk.get(SETTINGS_VIEWSIZE_SMALL)).getAbsolutePath() + " replaceOldLibs";
                                                printStream.println(utils2.cmdRoot(strArr3));
                                                if (showMessage && !runBackground) {
                                                    runToMainStatic(new Runnable() {
                                                        public void run() {
                                                            listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                                                            listAppsFragment.showMessageStatic(pli.name, Utils.getText(C0149R.string.integrate_updates_result) + ": " + Utils.getText(C0149R.string.well_done));
                                                        }
                                                    });
                                                }
                                                if (runBackground) {
                                                    toast(pli.name + " - " + Utils.getText(C0149R.string.integrate_updates_result) + ": " + Utils.getText(C0149R.string.well_done));
                                                }
                                            }
                                            if (copy_result.contains("Length of Files not equals. Destination deleted!")) {
                                                printStream = System.out;
                                                utils2 = new Utils(BuildConfig.VERSION_NAME);
                                                strArr3 = new String[TITLE_LIST_ITEM];
                                                strArr3[SETTINGS_VIEWSIZE_SMALL] = dalvikruncommand + ".copyLibsFilesToSystemBackup " + pkgName + " " + dataDir + " " + appapk + " " + ((File) sys_apk.get(SETTINGS_VIEWSIZE_SMALL)).getAbsolutePath() + " deleteBigLibs";
                                                printStream.println(utils2.cmdRoot(strArr3));
                                                if (runBackground) {
                                                    toast(pli.name + " - " + Utils.getText(C0149R.string.integrate_updates_result) + ": " + Utils.getText(C0149R.string.no_space_in_system_all));
                                                } else {
                                                    runToMainStatic(new Runnable() {
                                                        public void run() {
                                                            listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                                                            listAppsFragment.showMessageInfoStatic(listAppsFragment.patchAct, pli.name, Utils.getText(C0149R.string.integrate_updates_result) + ": " + Utils.getText(C0149R.string.no_space_in_system_all));
                                                        }
                                                    });
                                                }
                                            }
                                            if (copy_result.contains("Backup not replace original!")) {
                                                if (runBackground) {
                                                    toast(pli.name + " - " + Utils.getText(C0149R.string.integrate_updates_result) + ": " + Utils.getText(C0149R.string.unknown_error));
                                                } else {
                                                    runToMainStatic(new Runnable() {
                                                        public void run() {
                                                            listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                                                            listAppsFragment.showMessageStatic(pli.name, Utils.getText(C0149R.string.integrate_updates_result) + ": " + Utils.getText(C0149R.string.unknown_error));
                                                        }
                                                    });
                                                }
                                            }
                                        }
                                    } else {
                                        File sys;
                                        Utils utils3;
                                        boolean copy = false;
                                        boolean no_space = false;
                                        boolean unknown = false;
                                        File copied = null;
                                        Iterator it2 = sys_apk.iterator();
                                        while (it2.hasNext()) {
                                            sys = (File) it2.next();
                                            utils3 = new Utils(BuildConfig.VERSION_NAME);
                                            strArr = new String[TITLE_LIST_ITEM];
                                            strArr[SETTINGS_VIEWSIZE_SMALL] = dalvikruncommand + ".copyFileWithBackup '" + appapk + "' '" + sys.getAbsolutePath() + "'";
                                            copy_result = utils3.cmdRoot(strArr);
                                            System.out.println(copy_result);
                                            if (copy_result.contains("File copied!")) {
                                                Utils.run_all("chmod 644 '" + sys.getAbsolutePath() + "'");
                                                Utils.run_all("chown 0.0 '" + sys.getAbsolutePath() + "'");
                                                Utils.run_all("chown 0:0 '" + sys.getAbsolutePath() + "'");
                                                Utils.run_all("chattr -ai '" + sys.getAbsolutePath() + "'");
                                                copy = true;
                                                copied = sys;
                                            }
                                            if (copy_result.contains("Length of Files not equals. Destination deleted!")) {
                                                no_space = true;
                                                if (copy) {
                                                    Utils.run_all("chmod 644 '" + sys.getAbsolutePath() + "'");
                                                    Utils.run_all("chown 0.0 '" + sys.getAbsolutePath() + "'");
                                                    Utils.run_all("chown 0:0 '" + sys.getAbsolutePath() + "'");
                                                    Utils.run_all("chattr -ai '" + sys.getAbsolutePath() + "'");
                                                } else if (runBackground) {
                                                    toast(pli.name + " - " + Utils.getText(C0149R.string.integrate_updates_result) + ": " + Utils.getText(C0149R.string.no_space_in_system_all));
                                                } else {
                                                    runToMainStatic(new Runnable() {
                                                        public void run() {
                                                            listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                                                            listAppsFragment.showMessageInfoStatic(listAppsFragment.patchAct, pli.name, Utils.getText(C0149R.string.integrate_updates_result) + ": " + Utils.getText(C0149R.string.no_space_in_system_all));
                                                        }
                                                    });
                                                }
                                            }
                                            if (copy_result.contains("Backup not replace original!")) {
                                                unknown = true;
                                                if (copy) {
                                                    Utils.run_all("chmod 644 '" + sys.getAbsolutePath() + "'");
                                                    Utils.run_all("chown 0.0 '" + sys.getAbsolutePath() + "'");
                                                    Utils.run_all("chown 0:0 '" + sys.getAbsolutePath() + "'");
                                                    Utils.run_all("chattr -ai '" + sys.getAbsolutePath() + "'");
                                                } else if (runBackground) {
                                                    toast(pli.name + " - " + Utils.getText(C0149R.string.integrate_updates_result) + ": " + Utils.getText(C0149R.string.unknown_error));
                                                } else {
                                                    runToMainStatic(new Runnable() {
                                                        public void run() {
                                                            listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                                                            listAppsFragment.showMessageStatic(pli.name, Utils.getText(C0149R.string.integrate_updates_result) + ": " + Utils.getText(C0149R.string.unknown_error));
                                                        }
                                                    });
                                                }
                                            }
                                        }
                                        if (copy && !unknown && !no_space) {
                                            utils = new Utils(BuildConfig.VERSION_NAME);
                                            strArr2 = new String[TITLE_LIST_ITEM];
                                            strArr2[SETTINGS_VIEWSIZE_SMALL] = "pm uninstall -k " + pkgName;
                                            utils.cmdRoot(strArr2);
                                            appapk = getPkgMng().getPackageInfo(pkgName, SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir;
                                            it2 = sys_apk.iterator();
                                            while (it2.hasNext()) {
                                                sys = (File) it2.next();
                                                if (!sys.getAbsolutePath().equals(appapk)) {
                                                    Utils.run_all("rm '" + sys.getAbsolutePath() + "'");
                                                }
                                            }
                                            if (showMessage && !runBackground) {
                                                runToMainStatic(new Runnable() {
                                                    public void run() {
                                                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                                                        listAppsFragment.showMessageStatic(pli.name, Utils.getText(C0149R.string.integrate_updates_result) + ": " + Utils.getText(C0149R.string.well_done));
                                                    }
                                                });
                                            }
                                            if (runBackground) {
                                                toast(pli.name + " - " + Utils.getText(C0149R.string.integrate_updates_result) + ": " + Utils.getText(C0149R.string.well_done));
                                            }
                                        } else if (copy) {
                                            utils = new Utils(BuildConfig.VERSION_NAME);
                                            strArr2 = new String[TITLE_LIST_ITEM];
                                            strArr2[SETTINGS_VIEWSIZE_SMALL] = "pm uninstall -k " + pkgName;
                                            utils.cmdRoot(strArr2);
                                            appapk = getPkgMng().getPackageInfo(pkgName, SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir;
                                            if (copied.getAbsolutePath().equals(appapk)) {
                                                it2 = sys_apk.iterator();
                                                while (it2.hasNext()) {
                                                    sys = (File) it2.next();
                                                    if (!sys.getAbsolutePath().equals(appapk)) {
                                                        Utils.run_all("rm '" + sys.getAbsolutePath() + "'");
                                                    }
                                                }
                                            } else {
                                                Utils.run_all("rm '" + appapk + "'");
                                                PrintStream printStream2 = System.out;
                                                utils3 = new Utils(BuildConfig.VERSION_NAME);
                                                strArr = new String[TITLE_LIST_ITEM];
                                                strArr[SETTINGS_VIEWSIZE_SMALL] = dalvikruncommand + ".rename '" + copied.getAbsolutePath() + "' '" + appapk + "'";
                                                printStream2.println(utils3.cmdRoot(strArr));
                                                Utils.run_all("chmod 644 '" + appapk + "'");
                                                Utils.run_all("chown 0.0 '" + appapk + "'");
                                                Utils.run_all("chown 0:0 '" + appapk + "'");
                                                Utils.run_all("chattr -ai '" + appapk + "'");
                                                it2 = sys_apk.iterator();
                                                while (it2.hasNext()) {
                                                    sys = (File) it2.next();
                                                    if (!sys.getAbsolutePath().equals(appapk)) {
                                                        Utils.run_all("rm '" + sys.getAbsolutePath() + "'");
                                                    }
                                                }
                                            }
                                            if (showMessage && !runBackground) {
                                                runToMainStatic(new Runnable() {
                                                    public void run() {
                                                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                                                        listAppsFragment.showMessageStatic(pli.name, Utils.getText(C0149R.string.integrate_updates_result) + ": " + Utils.getText(C0149R.string.well_done));
                                                    }
                                                });
                                            }
                                            if (runBackground) {
                                                toast(pli.name + " - " + Utils.getText(C0149R.string.integrate_updates_result) + ": " + Utils.getText(C0149R.string.well_done));
                                            }
                                        }
                                    }
                                }
                            }
                        } catch (NameNotFoundException e) {
                            e.printStackTrace();
                        }
                        if (showMessage && !runBackground) {
                            runToMainStatic(new Runnable() {

                                class C02431 implements Runnable {
                                    C02431() {
                                    }

                                    public void run() {
                                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                                    }
                                }

                                public void run() {
                                    listAppsFragment.runToMainStatic(new C02431());
                                }
                            });
                        }
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        }
        if (showMessage && !runBackground) {
            runToMainStatic(new Runnable() {
                public void run() {
                    listAppsFragment.removeDialogLP(listAppsFragment.CHILD_NAME_SHEME_SELECT_ITEM);
                }
            });
        }
    }

    public void show_integrate_to_system(PkgListItem pli, final boolean showMessage) {
        if (getConfig().getBoolean("vibration", false)) {
            this.vib = (Vibrator) getContext().getSystemService("vibrator");
            this.vib.vibrate(50);
        }
        OnClickListener dialogClickListener = new OnClickListener() {

            class C02441 implements Runnable {
                C02441() {
                }

                public void run() {
                    listAppsFragment.this.show_integrate_to_system(listAppsFragment.pli, showMessage);
                }
            }

            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case Zip4jConstants.ENC_NO_ENCRYPTION /*-1*/:
                        new Thread(new C02441()).start();
                        return;
                    default:
                        return;
                }
            }
        };
        Utils.showDialogYesNo(pli.name, Utils.getText(C0149R.string.integrate_to_system), dialogClickListener, dialogClickListener, null);
    }

    public boolean createapkpermissions(ArrayList<String> perm, ArrayList<String> activ) {
        this.permissions = perm;
        this.activities = activ;
        if (testSD(true)) {
            runWithWait(new Runnable() {

                class C02451 implements Runnable {
                    C02451() {
                    }

                    public void run() {
                        listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.file_not_found));
                        listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                        listAppsFragment.this.vibrateEnd();
                    }
                }

                class C02462 implements Runnable {
                    C02462() {
                    }

                    public void run() {
                        listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.messageAndroidManifest));
                        listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                        listAppsFragment.this.vibrateEnd();
                    }
                }

                class C02473 implements Runnable {
                    C02473() {
                    }

                    public void run() {
                        listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.messageAndroidManifest));
                        listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                        listAppsFragment.this.vibrateEnd();
                    }
                }

                class C02484 implements Runnable {
                    C02484() {
                    }

                    public void run() {
                        listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.file_not_found));
                        listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                        listAppsFragment.this.vibrateEnd();
                    }
                }

                public void run() {
                    PkgListItem pli = listAppsFragment.pli;
                    String filesDir = null;
                    boolean error = false;
                    String versionPkg = BuildConfig.VERSION_NAME;
                    try {
                        versionPkg = ".v." + listAppsFragment.getPkgMng().getPackageInfo(pli.pkgName, listAppsFragment.SETTINGS_VIEWSIZE_SMALL).versionName + ".b." + listAppsFragment.getPkgMng().getPackageInfo(pli.pkgName, listAppsFragment.SETTINGS_VIEWSIZE_SMALL).versionCode;
                        filesDir = listAppsFragment.getPkgMng().getPackageInfo(pli.pkgName, listAppsFragment.SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir;
                        if (filesDir.contains("/mnt/asec/")) {
                            Utils.remount(filesDir.replace("/pkg.apk", BuildConfig.VERSION_NAME), InternalZipConstants.WRITE_MODE);
                        }
                        if (filesDir.startsWith("/system/")) {
                            Utils.remount("/system", InternalZipConstants.WRITE_MODE);
                        }
                        Utils.run_all("chmod 644 " + filesDir);
                        if (!new File(filesDir).exists()) {
                            listAppsFragment.this.runToMain(new C02451());
                            return;
                        }
                    } catch (NameNotFoundException e1) {
                        e1.printStackTrace();
                        error = true;
                    } catch (Exception e) {
                        error = true;
                    }
                    if (!error) {
                        if (new File(filesDir).exists()) {
                            String extrFile = null;
                            try {
                                FileInputStream fin = new FileInputStream(filesDir);
                                ZipInputStream zipInputStream = new ZipInputStream(fin);
                                while (true) {
                                    ZipEntry ze = zipInputStream.getNextEntry();
                                    if (ze == null) {
                                        break;
                                    } else if (ze.getName().equals("AndroidManifest.xml")) {
                                        extrFile = listAppsFragment.basepath + "/Modified/" + "AndroidManifest.xml";
                                        FileOutputStream fileOutputStream = new FileOutputStream(extrFile);
                                        byte[] buffer = new byte[AccessibilityNodeInfoCompat.ACTION_SCROLL_BACKWARD];
                                        while (true) {
                                            int length = zipInputStream.read(buffer);
                                            if (length == -1) {
                                                break;
                                            }
                                            fileOutputStream.write(buffer, listAppsFragment.SETTINGS_VIEWSIZE_SMALL, length);
                                        }
                                        zipInputStream.closeEntry();
                                        fileOutputStream.close();
                                    }
                                }
                                zipInputStream.close();
                                fin.close();
                            } catch (Exception e2) {
                                try {
                                    ZipFile zipFile = new ZipFile(filesDir);
                                    extrFile = listAppsFragment.basepath + "/Modified/" + "AndroidManifest.xml";
                                    zipFile.extractFile("AndroidManifest.xml", listAppsFragment.basepath + "/Modified");
                                } catch (ZipException e12) {
                                    System.out.println("Error classes.dex decompress! " + e12);
                                    e12.printStackTrace();
                                } catch (Exception e13) {
                                    System.out.println("Error classes.dex decompress! " + e13);
                                    e13.printStackTrace();
                                }
                            }
                            if (new File(listAppsFragment.basepath + "/Modified/AndroidManifest.xml").exists()) {
                                AxmlExample ex = new AxmlExample();
                                File file = new File(extrFile);
                                try {
                                    ex.disablePermisson(file, listAppsFragment.this.permissions, listAppsFragment.this.activities);
                                } catch (IOException e3) {
                                    e3.printStackTrace();
                                } catch (Exception e4) {
                                    e4.printStackTrace();
                                    listAppsFragment.this.runToMain(new C02473());
                                    return;
                                }
                                listAppsFragment.this.getSignatureKeys();
                                ZipSignerLP signer = new ZipSignerLP();
                                try {
                                    X509Certificate cert = signer.readPublicKey(new File(listAppsFragment.basepath + "/Modified/Keys/" + "testkey.x509.pem").toURI().toURL());
                                    byte[] sigBlockTemplate = signer.readContentAsBytes(new File(listAppsFragment.basepath + "/Modified/Keys/" + "testkey.sbt").toURI().toURL());
                                    signer.setKeys("custom", cert, signer.readPrivateKey(new File(listAppsFragment.basepath + "/Modified/Keys/" + "testkey.pk8").toURI().toURL(), "lp"), sigBlockTemplate);
                                    ArrayList add_files = new ArrayList();
                                    add_files.add(new AddFilesItem(listAppsFragment.basepath + "/Modified/AndroidManifest.xml", listAppsFragment.basepath + "/Modified/"));
                                    signer.signZip(filesDir, listAppsFragment.basepath + "/Modified/" + pli.pkgName + "." + "Permissions.Removed" + ".apk", add_files);
                                } catch (Throwable th) {
                                    File apkcrk = new File(listAppsFragment.basepath + "/Modified/" + pli.pkgName + ".apk");
                                    if (apkcrk.exists()) {
                                        apkcrk.delete();
                                    }
                                }
                                file = new File(listAppsFragment.basepath + "/Modified/" + pli.pkgName + "." + "Permissions.Removed" + ".apk");
                                if (file.exists()) {
                                    file = new File(listAppsFragment.basepath + "/Modified/" + pli.name.replaceAll(" ", ".").replaceAll(InternalZipConstants.ZIP_FILE_SEPARATOR, "."));
                                    file.mkdirs();
                                    try {
                                        if (listAppsFragment.getConfig().getInt("apkname", listAppsFragment.SETTINGS_VIEWSIZE_SMALL) == 0) {
                                            file.renameTo(new File(file.getCanonicalFile() + InternalZipConstants.ZIP_FILE_SEPARATOR + pli.name.replaceAll(" ", ".").replaceAll(InternalZipConstants.ZIP_FILE_SEPARATOR, ".") + versionPkg + "." + "Permissions.Removed" + ".apk"));
                                        }
                                        if (listAppsFragment.getConfig().getInt("apkname", listAppsFragment.SETTINGS_VIEWSIZE_SMALL) != 0) {
                                            file.renameTo(new File(file.getCanonicalFile() + InternalZipConstants.ZIP_FILE_SEPARATOR + pli.pkgName + versionPkg + "." + "Permissions.Removed" + ".apk"));
                                        }
                                    } catch (IOException e32) {
                                        e32.printStackTrace();
                                    }
                                }
                            } else {
                                listAppsFragment.this.runToMain(new C02462());
                                return;
                            }
                        }
                        listAppsFragment.this.runToMain(new C02484());
                    }
                    final PkgListItem pkgListItem = pli;
                    listAppsFragment.this.runToMain(new Runnable() {
                        public void run() {
                            listAppsFragment.str += "\nSU Java-Code Running!" + " PermissionRemovedMode Lucky";
                            listAppsFragment.pli = pkgListItem;
                            listAppsFragment.showDialogLP(listAppsFragment.CREATEAPK_DIALOG);
                            listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
                            listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                            listAppsFragment.this.vibrateEnd();
                        }
                    });
                }
            });
        }
        return true;
    }

    public boolean toolbar_createapkpermissions(ArrayList<String> perm, ArrayList<String> activ) {
        this.permissions = perm;
        this.activities = activ;
        if (testSD(true)) {
            runWithWait(new Runnable() {

                class C02501 implements Runnable {
                    C02501() {
                    }

                    public void run() {
                        listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.messageAndroidManifest));
                        listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                        listAppsFragment.this.vibrateEnd();
                    }
                }

                class C02512 implements Runnable {
                    C02512() {
                    }

                    public void run() {
                        listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.messageAndroidManifest));
                        listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                        listAppsFragment.this.vibrateEnd();
                    }
                }

                class C02523 implements Runnable {
                    C02523() {
                    }

                    public void run() {
                        listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.messageAndroidManifest));
                        listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                        listAppsFragment.this.vibrateEnd();
                    }
                }

                class C02534 implements Runnable {
                    C02534() {
                    }

                    public void run() {
                        listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.unknown_error));
                        listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                        listAppsFragment.this.vibrateEnd();
                    }
                }

                class C02545 implements Runnable {
                    C02545() {
                    }

                    public void run() {
                        listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.file_not_found));
                        listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                        listAppsFragment.this.vibrateEnd();
                    }
                }

                class C02556 implements Runnable {
                    C02556() {
                    }

                    public void run() {
                        listAppsFragment.this.showMessage(Utils.getText(C0149R.string.rebuild_info), Utils.getText(C0149R.string.rebuild_message) + " " + listAppsFragment.rebuldApk);
                        listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                        listAppsFragment.this.vibrateEnd();
                    }
                }

                public void run() {
                    ApplicationInfo pli = Utils.getApkInfo(listAppsFragment.rebuldApk);
                    PackageInfo pkgInfo = Utils.getApkPackageInfo(listAppsFragment.rebuldApk);
                    String versionPkg = BuildConfig.VERSION_NAME;
                    versionPkg = ".v." + pkgInfo.versionName + ".b." + pkgInfo.versionCode;
                    if (!false) {
                        String destFile = listAppsFragment.rebuldApk;
                        if (new File(listAppsFragment.rebuldApk).exists()) {
                            String extrFile = null;
                            try {
                                FileInputStream fin = new FileInputStream(destFile);
                                ZipInputStream zipInputStream = new ZipInputStream(fin);
                                while (true) {
                                    ZipEntry ze = zipInputStream.getNextEntry();
                                    if (ze == null) {
                                        break;
                                    } else if (ze.getName().equals("AndroidManifest.xml")) {
                                        extrFile = listAppsFragment.basepath + "/Modified/" + "AndroidManifest.xml";
                                        FileOutputStream fileOutputStream = new FileOutputStream(extrFile);
                                        byte[] buffer = new byte[AccessibilityNodeInfoCompat.ACTION_SCROLL_BACKWARD];
                                        while (true) {
                                            int length = zipInputStream.read(buffer);
                                            if (length == -1) {
                                                break;
                                            }
                                            fileOutputStream.write(buffer, listAppsFragment.SETTINGS_VIEWSIZE_SMALL, length);
                                        }
                                        zipInputStream.closeEntry();
                                        fileOutputStream.close();
                                    }
                                }
                                zipInputStream.close();
                                fin.close();
                            } catch (Exception e) {
                                try {
                                    ZipFile zipFile = new ZipFile(destFile);
                                    extrFile = listAppsFragment.basepath + "/Modified/" + "AndroidManifest.xml";
                                    zipFile.extractFile("AndroidManifest.xml", listAppsFragment.basepath + "/Modified");
                                } catch (ZipException e1) {
                                    System.out.println("Error classes.dex decompress! " + e1);
                                    e1.printStackTrace();
                                } catch (Exception e12) {
                                    System.out.println("Error classes.dex decompress! " + e12);
                                    e12.printStackTrace();
                                }
                            }
                            if (new File(listAppsFragment.basepath + "/Modified/AndroidManifest.xml").exists()) {
                                AxmlExample ex = new AxmlExample();
                                File file = new File(extrFile);
                                try {
                                    ex.disablePermisson(file, listAppsFragment.this.permissions, listAppsFragment.this.activities);
                                    listAppsFragment.this.getSignatureKeys();
                                    ZipSignerLP signer = new ZipSignerLP();
                                    try {
                                        X509Certificate cert = signer.readPublicKey(new File(listAppsFragment.basepath + "/Modified/Keys/" + "testkey.x509.pem").toURI().toURL());
                                        byte[] sigBlockTemplate = signer.readContentAsBytes(new File(listAppsFragment.basepath + "/Modified/Keys/" + "testkey.sbt").toURI().toURL());
                                        signer.setKeys("custom", cert, signer.readPrivateKey(new File(listAppsFragment.basepath + "/Modified/Keys/" + "testkey.pk8").toURI().toURL(), "lp"), sigBlockTemplate);
                                        ArrayList add_files = new ArrayList();
                                        add_files.add(new AddFilesItem(listAppsFragment.basepath + "/Modified/AndroidManifest.xml", listAppsFragment.basepath + "/Modified/"));
                                        signer.signZip(listAppsFragment.rebuldApk, listAppsFragment.basepath + "/Modified/" + pli.packageName + "." + "Permissions.Removed" + ".apk", add_files);
                                    } catch (Throwable th) {
                                    }
                                    File finalapk = new File(listAppsFragment.basepath + "/Modified/" + pli.packageName + "." + "Permissions.Removed" + ".apk");
                                    String resultfile = BuildConfig.VERSION_NAME;
                                    if (finalapk.exists()) {
                                        file = new File(listAppsFragment.basepath + "/Modified/" + Utils.getApkLabelName(listAppsFragment.rebuldApk).replaceAll(" ", "."));
                                        file.mkdirs();
                                        try {
                                            if (listAppsFragment.getConfig().getInt("apkname", listAppsFragment.SETTINGS_VIEWSIZE_SMALL) == 0) {
                                                resultfile = file.getCanonicalFile() + InternalZipConstants.ZIP_FILE_SEPARATOR + Utils.getApkLabelName(listAppsFragment.rebuldApk).replace(" ", ".") + versionPkg + "." + "Permissions.Removed" + ".apk";
                                                finalapk.renameTo(new File(resultfile));
                                            }
                                            if (listAppsFragment.getConfig().getInt("apkname", listAppsFragment.SETTINGS_VIEWSIZE_SMALL) != 0) {
                                                resultfile = file.getCanonicalFile() + InternalZipConstants.ZIP_FILE_SEPARATOR + pli.packageName + versionPkg + "." + "Permissions.Removed" + ".apk";
                                                finalapk.renameTo(new File(resultfile));
                                            }
                                            listAppsFragment.rebuldApk = resultfile;
                                        } catch (IOException e2) {
                                            e2.printStackTrace();
                                            listAppsFragment.this.runToMain(new C02534());
                                            return;
                                        }
                                    }
                                    listAppsFragment.this.runToMain(new C02556());
                                    return;
                                } catch (IOException e22) {
                                    e22.printStackTrace();
                                    listAppsFragment.this.runToMain(new C02512());
                                    return;
                                } catch (Exception e3) {
                                    e3.printStackTrace();
                                    listAppsFragment.this.runToMain(new C02523());
                                    return;
                                }
                            }
                            listAppsFragment.this.runToMain(new C02501());
                            return;
                        }
                        listAppsFragment.this.runToMain(new C02545());
                    }
                }
            });
        }
        return true;
    }

    public boolean apkpermissions(PkgListItem pli, ArrayList<String> perm, ArrayList<String> activ) {
        this.permissions = perm;
        this.activities = activ;
        if (testSD(true)) {
            runWithWait(new Runnable() {

                class C02561 implements Runnable {
                    C02561() {
                    }

                    public void run() {
                        listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.file_not_found));
                        listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                    }
                }

                class C02572 implements Runnable {
                    C02572() {
                    }

                    public void run() {
                        listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.messageAndroidManifest));
                        listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                    }
                }

                class C02583 implements Runnable {
                    C02583() {
                    }

                    public void run() {
                        listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.messageAndroidManifest));
                        listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                    }
                }

                class C02594 implements Runnable {
                    C02594() {
                    }

                    public void run() {
                        listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.file_not_found));
                        listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                    }
                }

                class C02605 implements Runnable {
                    C02605() {
                    }

                    public void run() {
                        listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.unknown_error));
                        listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                    }
                }

                class C02616 implements Runnable {
                    C02616() {
                    }

                    public void run() {
                        listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.file_not_found));
                        listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                    }
                }

                class C02627 implements Runnable {
                    C02627() {
                    }

                    public void run() {
                        listAppsFragment.this.showMessage(Utils.getText(C0149R.string.rebuild_info), Utils.getText(C0149R.string.well_done));
                        try {
                            listAppsFragment.pli.modified = true;
                            listAppsFragment.plia.notifyDataSetChanged(listAppsFragment.pli);
                            listAppsFragment.getConfig().edit().putBoolean(listAppsFragment.pli.pkgName, true).commit();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        listAppsFragment.refresh = true;
                        listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                    }
                }

                public void run() {
                    PkgListItem pli = listAppsFragment.pli;
                    String filesDir = null;
                    boolean error = false;
                    try {
                        filesDir = listAppsFragment.getPkgMng().getPackageInfo(pli.pkgName, listAppsFragment.SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir;
                    } catch (NameNotFoundException e1) {
                        e1.printStackTrace();
                        error = true;
                    } catch (Exception e) {
                        e.printStackTrace();
                        error = true;
                    }
                    if (error) {
                        listAppsFragment.this.runToMain(new C02616());
                    } else {
                        if (filesDir.contains("/mnt/asec/")) {
                            Utils.remount(filesDir.replace("/pkg.apk", BuildConfig.VERSION_NAME), InternalZipConstants.WRITE_MODE);
                        }
                        if (filesDir.startsWith("/system/")) {
                            Utils.remount("/system", InternalZipConstants.WRITE_MODE);
                        }
                        Utils.run_all("chmod 644 " + filesDir);
                        if (new File(filesDir).exists()) {
                            String extrFile = null;
                            try {
                                FileInputStream fin = new FileInputStream(filesDir);
                                ZipInputStream zipInputStream = new ZipInputStream(fin);
                                while (true) {
                                    ZipEntry ze = zipInputStream.getNextEntry();
                                    if (ze == null) {
                                        break;
                                    } else if (ze.getName().equals("AndroidManifest.xml")) {
                                        extrFile = listAppsFragment.basepath + "/Modified/" + "AndroidManifest.xml";
                                        FileOutputStream fileOutputStream = new FileOutputStream(extrFile);
                                        byte[] buffer = new byte[AccessibilityNodeInfoCompat.ACTION_SCROLL_BACKWARD];
                                        while (true) {
                                            int length = zipInputStream.read(buffer);
                                            if (length == -1) {
                                                break;
                                            }
                                            fileOutputStream.write(buffer, listAppsFragment.SETTINGS_VIEWSIZE_SMALL, length);
                                        }
                                        zipInputStream.closeEntry();
                                        fileOutputStream.close();
                                    }
                                }
                                zipInputStream.close();
                                fin.close();
                            } catch (Exception e2) {
                                try {
                                    ZipFile zipFile = new ZipFile(filesDir);
                                    extrFile = listAppsFragment.basepath + "/Modified/" + "AndroidManifest.xml";
                                    zipFile.extractFile("AndroidManifest.xml", listAppsFragment.basepath + "/Modified/");
                                } catch (ZipException e12) {
                                    System.out.println("Error classes.dex decompress! " + e12);
                                    System.out.println("Exception e1" + e2.toString());
                                } catch (Exception e13) {
                                    System.out.println("Error classes.dex decompress! " + e13);
                                    System.out.println("Exception e1" + e2.toString());
                                }
                                System.out.println("Decompress unzip " + e2);
                            }
                            if (new File(listAppsFragment.basepath + "/Modified/AndroidManifest.xml").exists()) {
                                AxmlExample ex = new AxmlExample();
                                File file = new File(extrFile);
                                try {
                                    ex.disablePermisson(file, listAppsFragment.this.permissions, listAppsFragment.this.activities);
                                } catch (IOException e3) {
                                    e3.printStackTrace();
                                } catch (Exception e22) {
                                    e22.printStackTrace();
                                    listAppsFragment.this.runToMain(new C02583());
                                    return;
                                }
                                ZipSignerLP signer = new ZipSignerLP();
                                String tmpapk = listAppsFragment.basepath + "/Modified/tmp.apk";
                                try {
                                    listAppsFragment.this.getSignatureKeys();
                                    X509Certificate cert = signer.readPublicKey(new File(listAppsFragment.basepath + "/Modified/Keys/" + "testkey.x509.pem").toURI().toURL());
                                    byte[] sigBlockTemplate = signer.readContentAsBytes(new File(listAppsFragment.basepath + "/Modified/Keys/" + "testkey.sbt").toURI().toURL());
                                    signer.setKeys("custom", cert, signer.readPrivateKey(new File(listAppsFragment.basepath + "/Modified/Keys/" + "testkey.pk8").toURI().toURL(), "lp"), sigBlockTemplate);
                                    ArrayList add_files = new ArrayList();
                                    add_files.add(new AddFilesItem(listAppsFragment.basepath + "/Modified/AndroidManifest.xml", listAppsFragment.basepath + "/Modified/"));
                                    signer.signZip(filesDir, tmpapk, add_files);
                                    if (!new File(tmpapk).exists()) {
                                        listAppsFragment.this.runToMain(new C02594());
                                        return;
                                    } else if (new File(tmpapk).exists()) {
                                        try {
                                            String srcDir = listAppsFragment.getPkgMng().getPackageInfo(pli.pkgName, listAppsFragment.SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir;
                                            if (srcDir.startsWith("/mnt/asec/")) {
                                                Utils.remount(filesDir.replace("/pkg.apk", BuildConfig.VERSION_NAME), InternalZipConstants.WRITE_MODE);
                                            }
                                            Utils utils = new Utils(BuildConfig.VERSION_NAME);
                                            String[] strArr = new String[listAppsFragment.TITLE_LIST_ITEM];
                                            strArr[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = "rm " + Utils.getPlaceForOdex(srcDir, false);
                                            utils.cmdRoot(strArr);
                                        } catch (NameNotFoundException e14) {
                                            e14.printStackTrace();
                                        } catch (Exception e222) {
                                            e222.printStackTrace();
                                        }
                                        PrintStream printStream = System.out;
                                        Utils utils2 = new Utils(BuildConfig.VERSION_NAME);
                                        String[] strArr2 = new String[listAppsFragment.SETTINGS_SORTBY_STATUS];
                                        strArr2[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = "pm uninstall " + pli.pkgName;
                                        strArr2[listAppsFragment.TITLE_LIST_ITEM] = "pm install -r -i com.android.vending '" + tmpapk + "'";
                                        printStream.println(utils2.cmdRoot(strArr2));
                                        if (new File(tmpapk).exists()) {
                                            new File(tmpapk).delete();
                                        }
                                    }
                                } catch (Throwable th) {
                                    File apkcrk = new File(tmpapk);
                                    if (apkcrk.exists()) {
                                        apkcrk.delete();
                                    }
                                    listAppsFragment.this.runToMain(new C02605());
                                    return;
                                }
                            }
                            listAppsFragment.this.runToMain(new C02572());
                            return;
                        }
                        listAppsFragment.this.runToMain(new C02561());
                        return;
                    }
                    listAppsFragment.this.runToMain(new C02627());
                }
            });
        }
        return true;
    }

    public boolean apkpermissions_safe(PkgListItem pli, ArrayList<String> perm, ArrayList<String> activ) {
        this.permissions = perm;
        this.activities = activ;
        if (testSD(true)) {
            runWithWait(new Runnable() {

                class C02631 implements Runnable {
                    C02631() {
                    }

                    public void run() {
                        listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.messageAndroidManifest));
                        listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                        listAppsFragment.this.vibrateEnd();
                    }
                }

                class C02642 implements Runnable {
                    C02642() {
                    }

                    public void run() {
                        listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.messageAndroidManifest));
                        listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                        listAppsFragment.this.vibrateEnd();
                    }
                }

                class C02663 implements Runnable {

                    class C02651 implements OnClickListener {
                        C02651() {
                        }

                        public void onClick(DialogInterface dialog, int which) {
                            Utils.reboot();
                        }
                    }

                    C02663() {
                    }

                    public void run() {
                        Utils.showDialogYesNo(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.system_app_change), new C02651(), null, null);
                        listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                        listAppsFragment.this.vibrateEnd();
                    }
                }

                class C02674 implements Runnable {
                    C02674() {
                    }

                    public void run() {
                        listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.file_not_found));
                        listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                        listAppsFragment.this.vibrateEnd();
                    }
                }

                class C02685 implements Runnable {
                    C02685() {
                    }

                    public void run() {
                        listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.app_not_found));
                        listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                        listAppsFragment.this.vibrateEnd();
                    }
                }

                class C02696 implements Runnable {
                    C02696() {
                    }

                    public void run() {
                        listAppsFragment.this.showMessage(Utils.getText(C0149R.string.rebuild_info), Utils.getText(C0149R.string.well_done));
                        try {
                            listAppsFragment.plia.getItem(listAppsFragment.pli.pkgName).modified = true;
                            listAppsFragment.plia.notifyDataSetChanged(listAppsFragment.plia.getItem(listAppsFragment.pli.pkgName));
                            listAppsFragment.getConfig().edit().putBoolean(listAppsFragment.plia.getItem(listAppsFragment.pli.pkgName).pkgName, true).commit();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        listAppsFragment.refresh = true;
                        listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
                        listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                        listAppsFragment.this.vibrateEnd();
                    }
                }

                public void run() {
                    ZipFile zipFile;
                    PkgListItem pli = listAppsFragment.pli;
                    String filesDir = null;
                    boolean error = false;
                    try {
                        filesDir = listAppsFragment.getPkgMng().getPackageInfo(pli.pkgName, listAppsFragment.SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir;
                        if (pli.system) {
                            Utils.remount("/system", InternalZipConstants.WRITE_MODE);
                        }
                    } catch (NameNotFoundException e1) {
                        e1.printStackTrace();
                        error = true;
                    } catch (Exception e) {
                        e.printStackTrace();
                        error = true;
                    }
                    if (error) {
                        listAppsFragment.this.runToMain(new C02685());
                    } else {
                        if (!new File(listAppsFragment.basepath + "/Modified").exists()) {
                            new File(listAppsFragment.basepath + "/Modified").mkdirs();
                        }
                        String destFile = listAppsFragment.basepath + "/Modified/" + new File(filesDir).getName();
                        if (!new File(listAppsFragment.basepath + "/Modified").exists()) {
                            new File(listAppsFragment.basepath + "/Modified").mkdirs();
                        }
                        Utils.run_all("chmod 777 " + filesDir);
                        if (new File(filesDir).exists()) {
                            System.out.println("LuckyPatcher (CopyPackageApk):" + destFile);
                            String extrFile = null;
                            try {
                                FileInputStream fin = new FileInputStream(filesDir);
                                ZipInputStream zipInputStream = new ZipInputStream(fin);
                                while (true) {
                                    ZipEntry ze = zipInputStream.getNextEntry();
                                    if (ze == null) {
                                        break;
                                    } else if (ze.getName().equals("AndroidManifest.xml")) {
                                        extrFile = listAppsFragment.basepath + "/Modified/" + "AndroidManifest.xml";
                                        FileOutputStream fout = new FileOutputStream(extrFile);
                                        byte[] buffer = new byte[AccessibilityNodeInfoCompat.ACTION_SCROLL_BACKWARD];
                                        while (true) {
                                            int length = zipInputStream.read(buffer);
                                            if (length == -1) {
                                                break;
                                            }
                                            fout.write(buffer, listAppsFragment.SETTINGS_VIEWSIZE_SMALL, length);
                                        }
                                        zipInputStream.closeEntry();
                                        fout.close();
                                    }
                                }
                                zipInputStream.close();
                                fin.close();
                            } catch (Exception e2) {
                                try {
                                    zipFile = new ZipFile(filesDir);
                                    extrFile = listAppsFragment.basepath + "/Modified/" + "AndroidManifest.xml";
                                    zipFile.extractFile("AndroidManifest.xml", listAppsFragment.basepath + "/Modified/");
                                } catch (ZipException e12) {
                                    System.out.println("Error classes.dex decompress! " + e12);
                                    System.out.println("Exception e1" + e2.toString());
                                } catch (Exception e13) {
                                    System.out.println("Error classes.dex decompress! " + e13);
                                    System.out.println("Exception e1" + e2.toString());
                                }
                                System.out.println("Decompress unzip " + e2);
                            }
                            if (new File(listAppsFragment.basepath + "/Modified/AndroidManifest.xml").exists()) {
                                AxmlExample ex = new AxmlExample();
                                File manifestFile = new File(extrFile);
                                try {
                                    ex.disablePermisson(manifestFile, listAppsFragment.this.permissions, listAppsFragment.this.activities);
                                } catch (IOException e3) {
                                    e3.printStackTrace();
                                } catch (Exception e22) {
                                    e22.printStackTrace();
                                    listAppsFragment.this.runToMain(new C02642());
                                    return;
                                }
                                try {
                                    ArrayList<AddFilesItem> add_files = new ArrayList();
                                    add_files.add(new AddFilesItem(listAppsFragment.basepath + "/Modified/AndroidManifest.xml", listAppsFragment.basepath + "/Modified/"));
                                    Utils.addFilesToZip(filesDir, destFile, add_files);
                                } catch (Exception e132) {
                                    e132.printStackTrace();
                                    try {
                                        zipFile = new ZipFile(destFile);
                                        ArrayList<File> filesToAdd = new ArrayList();
                                        filesToAdd.add(new File(listAppsFragment.basepath + "/Modified/AndroidManifest.xml"));
                                        ZipParameters parameters = new ZipParameters();
                                        parameters.setCompressionMethod(listAppsFragment.SETTINGS_VIEWSIZE_SMALL);
                                        parameters.setEncryptFiles(false);
                                        zipFile.addFiles(filesToAdd, parameters);
                                    } catch (ZipException e4) {
                                        e4.printStackTrace();
                                        System.out.println("Error classes.dex compress! " + e4);
                                    } catch (Exception e222) {
                                        e222.printStackTrace();
                                        System.out.println("Error classes.dex compress! " + e222);
                                    }
                                }
                                if (new File(listAppsFragment.basepath + "/Modified/AndroidManifest.xml").exists()) {
                                    new File(listAppsFragment.basepath + "/Modified/AndroidManifest.xml").delete();
                                }
                                if (new File(destFile).exists()) {
                                    if (!pli.system || (pli.system && !filesDir.startsWith("/system/"))) {
                                        Utils utils;
                                        String[] strArr;
                                        try {
                                            String srcDir = listAppsFragment.getPkgMng().getPackageInfo(pli.pkgName, listAppsFragment.SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir;
                                            if (srcDir.startsWith("/mnt/asec/")) {
                                                Utils.remount(filesDir.replace("/pkg.apk", BuildConfig.VERSION_NAME), InternalZipConstants.WRITE_MODE);
                                            }
                                            utils = new Utils(BuildConfig.VERSION_NAME);
                                            strArr = new String[listAppsFragment.TITLE_LIST_ITEM];
                                            strArr[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = "rm " + Utils.getPlaceForOdex(srcDir, false);
                                            utils.cmdRoot(strArr);
                                        } catch (NameNotFoundException e14) {
                                            e14.printStackTrace();
                                        } catch (Exception e2222) {
                                            e2222.printStackTrace();
                                        }
                                        utils = new Utils(BuildConfig.VERSION_NAME);
                                        strArr = new String[listAppsFragment.TITLE_LIST_ITEM];
                                        strArr[listAppsFragment.SETTINGS_VIEWSIZE_SMALL] = "pm install -r -i com.android.vending '" + destFile + "'";
                                        utils.cmdRoot(strArr);
                                    } else {
                                        Utils.copyFile(destFile, filesDir, true, true);
                                        Utils.run_all("chmod 0644 " + filesDir);
                                        Utils.run_all("chown 0:0 " + filesDir);
                                        Utils.run_all("chown 0.0 " + filesDir);
                                        listAppsFragment.this.runToMain(new C02663());
                                    }
                                }
                            } else {
                                listAppsFragment.this.runToMain(new C02631());
                                return;
                            }
                        }
                        listAppsFragment.this.runToMain(new C02674());
                    }
                    listAppsFragment.this.runToMain(new C02696());
                }
            });
        }
        return true;
    }

    public void getpermissions() {
        ArrayList<Perm> tmpList = new ArrayList();
        PackageManager pm;
        String[] requestedPermissions;
        int length;
        int i;
        String per;
        int d;
        if (rebuldApk.equals(BuildConfig.VERSION_NAME)) {
            pm = getPkgMng();
            try {
                requestedPermissions = pm.getPackageInfo(pli.pkgName, LZMA2Options.DICT_SIZE_MIN).requestedPermissions;
                if (requestedPermissions != null) {
                    length = requestedPermissions.length;
                    for (i = SETTINGS_VIEWSIZE_SMALL; i < length; i += TITLE_LIST_ITEM) {
                        per = requestedPermissions[i];
                        if (per.startsWith("disabled_")) {
                            tmpList.add(new Perm("chelpa_per_" + per.replace("disabled_", BuildConfig.VERSION_NAME), false));
                        } else {
                            tmpList.add(new Perm(per, true));
                        }
                    }
                }
            } catch (NameNotFoundException e1) {
                e1.printStackTrace();
            } catch (NullPointerException e) {
            }
            try {
                PackageInfo pkginfo = pm.getPackageInfo(pli.pkgName, TITLE_LIST_ITEM);
                for (d = SETTINGS_VIEWSIZE_SMALL; d < pkginfo.activities.length; d += TITLE_LIST_ITEM) {
                    if (!pkginfo.activities[d].name.startsWith("disabled_")) {
                        tmpList.add(new Perm("chelpus_" + pkginfo.activities[d].name, true));
                    }
                    if (pkginfo.activities[d].name.startsWith("disabled_")) {
                        tmpList.add(new Perm("chelpus_" + pkginfo.activities[d].name, false));
                    }
                }
            } catch (NameNotFoundException e12) {
                e12.printStackTrace();
            } catch (NullPointerException e2) {
            }
        } else {
            pm = getPkgMng();
            PackageInfo info = pm.getPackageArchiveInfo(rebuldApk, LZMA2Options.DICT_SIZE_MIN);
            if (info != null) {
                ApplicationInfo appInfo = info.applicationInfo;
                if (VERSION.SDK_INT >= CREATEAPK_DIALOG) {
                    appInfo.sourceDir = rebuldApk;
                    appInfo.publicSourceDir = rebuldApk;
                }
            }
            tmpList = new ArrayList();
            try {
                requestedPermissions = info.requestedPermissions;
                System.out.println(requestedPermissions);
                if (requestedPermissions != null) {
                    length = requestedPermissions.length;
                    for (i = SETTINGS_VIEWSIZE_SMALL; i < length; i += TITLE_LIST_ITEM) {
                        per = requestedPermissions[i];
                        System.out.println(per);
                        if (per.startsWith("disabled_")) {
                            tmpList.add(new Perm("chelpa_per_" + per.replace("disabled_", BuildConfig.VERSION_NAME), false));
                        } else {
                            tmpList.add(new Perm(per, true));
                        }
                    }
                }
            } catch (NullPointerException e3) {
            }
            info = pm.getPackageArchiveInfo(rebuldApk, TITLE_LIST_ITEM);
            for (d = SETTINGS_VIEWSIZE_SMALL; d < info.activities.length; d += TITLE_LIST_ITEM) {
                if (!info.activities[d].name.startsWith("disabled_")) {
                    tmpList.add(new Perm("chelpus_" + info.activities[d].name, true));
                }
                if (info.activities[d].name.startsWith("disabled_")) {
                    tmpList.add(new Perm("chelpus_" + info.activities[d].name, false));
                }
            }
        }
        if (tmpList.size() <= 0) {
            tmpList.add(new Perm(Utils.getText(C0149R.string.permission_not_found), false));
        }
        if (perm_adapt != null) {
            perm_adapt.notifyDataSetChanged();
        }
        perm_adapt = new ArrayAdapter<Perm>(getContext(), C0149R.layout.contextmenu, tmpList) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                TextView textView = (TextView) view.findViewById(C0149R.id.conttext);
                textView.setTextAppearance(getContext(), listAppsFragment.getSizeText());
                textView.setCompoundDrawablePadding((int) ((5.0f * listAppsFragment.getRes().getDisplayMetrics().density) + 0.5f));
                String str2 = ((Perm) getItem(position)).Name.replace("disabled_", BuildConfig.VERSION_NAME).replace("chelpa_per_", BuildConfig.VERSION_NAME).replace("chelpus_", BuildConfig.VERSION_NAME).replace("android.permission.", BuildConfig.VERSION_NAME).replace("com.android.vending.", BuildConfig.VERSION_NAME) + LogCollector.LINE_SEPARATOR;
                if (!((Perm) getItem(position)).Status) {
                    textView.setText(Utils.getColoredText(str2, "#ffff0000", "bold"));
                } else if (((Perm) getItem(position)).Status && ((Perm) getItem(position)).Name.contains("chelpus_")) {
                    textView.setText(Utils.getColoredText(str2, "#ff00ffff", "bold"));
                    if (Utils.isAds(((Perm) getItem(position)).Name)) {
                        textView.setText(Utils.getColoredText(str2, "#ffffff00", "bold"));
                    }
                } else {
                    textView.setText(Utils.getColoredText(str2, "#ff00ff00", "bold"));
                }
                PackageManager pm = listAppsFragment.getPkgMng();
                try {
                    str2 = pm.getPermissionInfo(((Perm) getItem(position)).Name.replace("chelpa_per_", BuildConfig.VERSION_NAME).replace("chelpus_", BuildConfig.VERSION_NAME), listAppsFragment.SETTINGS_VIEWSIZE_SMALL).loadDescription(pm).toString();
                    if (str2 == null) {
                        str2 = Utils.getText(C0149R.string.permission_not_descr);
                        if (((Perm) getItem(position)).Name.contains("chelpus_")) {
                            str2 = Utils.getText(C0149R.string.activity_descr);
                        }
                        if (((Perm) getItem(position)).Name.contains("chelpus_") && Utils.isAds(((Perm) getItem(position)).Name)) {
                            str2 = Utils.getText(C0149R.string.activity_ads_descr);
                        }
                        textView.append(Utils.getColoredText(str2, "#ff888888", "italic"));
                    } else {
                        textView.append(Utils.getColoredText(str2, "#ff888888", "italic"));
                    }
                } catch (NameNotFoundException e) {
                    str2 = Utils.getText(C0149R.string.permission_not_descr);
                    if (((Perm) getItem(position)).Name.contains("chelpus_")) {
                        str2 = Utils.getText(C0149R.string.activity_descr);
                    }
                    if (((Perm) getItem(position)).Name.contains("chelpus_") && Utils.isAds(((Perm) getItem(position)).Name)) {
                        str2 = Utils.getText(C0149R.string.activity_ads_descr);
                    }
                    textView.append(Utils.getColoredText(str2, "#ff888888", "italic"));
                } catch (NullPointerException e2) {
                    str2 = Utils.getText(C0149R.string.permission_not_descr);
                    if (((Perm) getItem(position)).Name.contains("chelpus_")) {
                        str2 = Utils.getText(C0149R.string.activity_descr);
                    }
                    if (((Perm) getItem(position)).Name.contains("chelpus_") && Utils.isAds(((Perm) getItem(position)).Name)) {
                        str2 = Utils.getText(C0149R.string.activity_ads_descr);
                    }
                    textView.append(Utils.getColoredText(str2, "#ff888888", "italic"));
                }
                return view;
            }
        };
    }

    public void getLPActivity() {
        ArrayList<Perm> tmpList = new ArrayList();
        PackageInfo info = null;
        try {
            info = getPkgMng().getPackageInfo(pli.pkgName, TITLE_LIST_ITEM);
        } catch (NameNotFoundException e1) {
            e1.printStackTrace();
        }
        for (int d = SETTINGS_VIEWSIZE_SMALL; d < info.activities.length; d += TITLE_LIST_ITEM) {
            if (getPkgMng().getComponentEnabledSetting(new ComponentName(pli.pkgName, info.activities[d].name)) == SETTINGS_SORTBY_STATUS) {
                tmpList.add(new Perm(info.activities[d].name, false));
            } else {
                tmpList.add(new Perm(info.activities[d].name, true));
            }
        }
        if (tmpList.size() <= 0) {
            tmpList.add(new Perm(Utils.getText(C0149R.string.permission_not_found), false));
        }
        if (perm_adapt != null) {
            perm_adapt.notifyDataSetChanged();
        }
        perm_adapt = new ArrayAdapter<Perm>(getContext(), C0149R.layout.contextmenu, tmpList) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                TextView textView = (TextView) view.findViewById(C0149R.id.conttext);
                textView.setTextAppearance(getContext(), listAppsFragment.getSizeText());
                textView.setCompoundDrawablePadding((int) ((5.0f * listAppsFragment.getRes().getDisplayMetrics().density) + 0.5f));
                String str2 = ((Perm) getItem(position)).Name.replace("disabled_", BuildConfig.VERSION_NAME).replace("chelpa_per_", BuildConfig.VERSION_NAME).replace("chelpus_", BuildConfig.VERSION_NAME).replace("android.permission.", BuildConfig.VERSION_NAME).replace("com.android.vending.", BuildConfig.VERSION_NAME) + LogCollector.LINE_SEPARATOR;
                if (((Perm) getItem(position)).Status) {
                    textView.setText(Utils.getColoredText(str2, "#ff00ffff", "bold"));
                    if (Utils.isAds(((Perm) getItem(position)).Name)) {
                        textView.setText(Utils.getColoredText(str2, "#ffffff00", "bold"));
                    }
                } else {
                    textView.setText(Utils.getColoredText(str2, "#ffff0000", "bold"));
                }
                if (Utils.isAds(((Perm) getItem(position)).Name)) {
                    str2 = Utils.getText(C0149R.string.activity_ads_descr);
                } else {
                    str2 = Utils.getText(C0149R.string.activity_descr);
                }
                textView.append(Utils.getColoredText(str2, "#ff888888", "italic"));
                return view;
            }
        };
        perm_adapt.sort(new Comparator<Perm>() {
            public int compare(Perm lhs, Perm rhs) {
                if (lhs == null || rhs == null) {
                    throw new ClassCastException();
                } else if (Utils.isAds(lhs.Name) && !Utils.isAds(rhs.Name)) {
                    return -1;
                } else {
                    if (!Utils.isAds(lhs.Name) && Utils.isAds(rhs.Name)) {
                        return listAppsFragment.TITLE_LIST_ITEM;
                    }
                    if (Utils.isAds(lhs.Name) && Utils.isAds(rhs.Name)) {
                        return lhs.Name.compareTo(rhs.Name);
                    }
                    return listAppsFragment.SETTINGS_VIEWSIZE_SMALL;
                }
            }
        });
    }

    public void getComponents() {
        int d;
        ArrayList<Components> tmpList = new ArrayList();
        PackageInfo info = null;
        try {
            info = getPkgMng().getPackageInfo(pli.pkgName, 4111);
        } catch (NameNotFoundException e1) {
            e1.printStackTrace();
        } catch (RuntimeException e) {
            e.printStackTrace();
            try {
                info = getPkgMng().getPackageInfo(pli.pkgName, 4110);
            } catch (NameNotFoundException e12) {
                e12.printStackTrace();
            }
        }
        if (!(info.activities == null || info.activities.length == 0)) {
            tmpList.add(new Components(Utils.getText(C0149R.string.header_activities), true, false, false, false, false, false, true));
            for (d = SETTINGS_VIEWSIZE_SMALL; d < info.activities.length; d += TITLE_LIST_ITEM) {
                if (getPkgMng().getComponentEnabledSetting(new ComponentName(pli.pkgName, info.activities[d].name)) == SETTINGS_SORTBY_STATUS) {
                    tmpList.add(new Components(info.activities[d].name, false, false, true, false, false, false, false));
                } else {
                    tmpList.add(new Components(info.activities[d].name, true, false, true, false, false, false, false));
                }
            }
        }
        if (!(info.receivers == null || info.receivers.length == 0)) {
            tmpList.add(new Components(Utils.getText(C0149R.string.header_receivers), true, false, false, false, false, false, true));
            for (d = SETTINGS_VIEWSIZE_SMALL; d < info.receivers.length; d += TITLE_LIST_ITEM) {
                if (getPkgMng().getComponentEnabledSetting(new ComponentName(pli.pkgName, info.receivers[d].name)) == SETTINGS_SORTBY_STATUS) {
                    tmpList.add(new Components(info.receivers[d].name, false, false, false, false, false, true, false));
                } else {
                    tmpList.add(new Components(info.receivers[d].name, true, false, false, false, false, true, false));
                }
            }
        }
        if (!(info.providers == null || info.providers.length == 0)) {
            tmpList.add(new Components(Utils.getText(C0149R.string.header_providers), true, false, false, false, false, false, true));
            for (d = SETTINGS_VIEWSIZE_SMALL; d < info.providers.length; d += TITLE_LIST_ITEM) {
                if (getPkgMng().getComponentEnabledSetting(new ComponentName(pli.pkgName, info.providers[d].name)) == SETTINGS_SORTBY_STATUS) {
                    tmpList.add(new Components(info.providers[d].name, false, false, false, true, false, false, false));
                } else {
                    tmpList.add(new Components(info.providers[d].name, true, false, false, true, false, false, false));
                }
            }
        }
        if (!(info.services == null || info.services.length == 0)) {
            tmpList.add(new Components(Utils.getText(C0149R.string.header_services), true, false, false, false, false, false, true));
            for (d = SETTINGS_VIEWSIZE_SMALL; d < info.services.length; d += TITLE_LIST_ITEM) {
                if (getPkgMng().getComponentEnabledSetting(new ComponentName(pli.pkgName, info.services[d].name)) == SETTINGS_SORTBY_STATUS) {
                    tmpList.add(new Components(info.services[d].name, false, false, false, false, true, false, false));
                } else {
                    tmpList.add(new Components(info.services[d].name, true, false, false, false, true, false, false));
                }
            }
        }
        if (tmpList.size() <= 0) {
            tmpList.add(new Components(Utils.getText(C0149R.string.permission_not_found), true, false, false, false, false, false, true));
        }
        if (component_adapt != null) {
            component_adapt.notifyDataSetChanged();
        }
        component_adapt = new ArrayAdapter<Components>(getContext(), C0149R.layout.contextmenu, tmpList) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = ((LayoutInflater) listAppsFragment.getInstance().getSystemService("layout_inflater")).inflate(C0149R.layout.contextmenu, parent, false);
                TextView textView = (TextView) view.findViewById(C0149R.id.conttext);
                textView.setTextAppearance(getContext(), listAppsFragment.getSizeText());
                textView.setCompoundDrawablePadding((int) ((5.0f * listAppsFragment.getRes().getDisplayMetrics().density) + 0.5f));
                if (((Components) getItem(position)).header) {
                    textView.setText(Utils.getColoredText(((Components) getItem(position)).Name, "#ffffffff", "bold"));
                    view.setBackgroundColor(-12303292);
                } else {
                    String str2;
                    if (((Components) getItem(position)).activity) {
                        str2 = ((Components) getItem(position)).Name + LogCollector.LINE_SEPARATOR;
                        if (((Components) getItem(position)).Status) {
                            textView.setText(Utils.getColoredText(str2, "#ff00ffff", "bold"));
                            if (Utils.isAds(((Components) getItem(position)).Name)) {
                                textView.setText(Utils.getColoredText(str2, "#ffffff00", "bold"));
                            }
                        } else {
                            textView.setText(Utils.getColoredText(str2, "#ffff0000", "bold"));
                        }
                        if (Utils.isAds(((Components) getItem(position)).Name)) {
                            str2 = Utils.getText(C0149R.string.activity_ads_descr);
                        } else {
                            str2 = Utils.getText(C0149R.string.activity_descr);
                        }
                        textView.append(Utils.getColoredText(str2, "#ff888888", "italic"));
                    }
                    if (((Components) getItem(position)).permission) {
                        str2 = ((Components) getItem(position)).Name + LogCollector.LINE_SEPARATOR;
                        if (((Components) getItem(position)).Status) {
                            textView.setText(Utils.getColoredText(str2, "#ff00ff00", "bold"));
                        } else {
                            textView.setText(Utils.getColoredText(str2, "#ffff0000", "bold"));
                        }
                        PackageManager pm = listAppsFragment.getPkgMng();
                        try {
                            str2 = pm.getPermissionInfo(((Components) getItem(position)).Name.replace("chelpa_per_", BuildConfig.VERSION_NAME).replace("chelpus_", BuildConfig.VERSION_NAME), listAppsFragment.SETTINGS_VIEWSIZE_SMALL).loadDescription(pm).toString();
                            if (str2 == null) {
                                textView.append(Utils.getColoredText(Utils.getText(C0149R.string.permission_not_descr), "#ff888888", "italic"));
                            } else {
                                textView.append(Utils.getColoredText(str2, "#ff888888", "italic"));
                            }
                        } catch (NameNotFoundException e) {
                            textView.append(Utils.getColoredText(Utils.getText(C0149R.string.permission_not_descr), "#ff888888", "italic"));
                        } catch (NullPointerException e2) {
                            textView.append(Utils.getColoredText(Utils.getText(C0149R.string.permission_not_descr), "#ff888888", "italic"));
                        }
                    }
                    if (((Components) getItem(position)).service) {
                        str2 = ((Components) getItem(position)).Name + LogCollector.LINE_SEPARATOR;
                        if (((Components) getItem(position)).Status) {
                            textView.setText(Utils.getColoredText(str2, "#ff00ff00", "bold"));
                        } else {
                            textView.setText(Utils.getColoredText(str2, "#ffff0000", "bold"));
                        }
                    }
                    if (((Components) getItem(position)).reciver) {
                        str2 = ((Components) getItem(position)).Name + LogCollector.LINE_SEPARATOR;
                        if (((Components) getItem(position)).Status) {
                            textView.setText(Utils.getColoredText(str2, "#ff00ffff", "bold"));
                        } else {
                            textView.setText(Utils.getColoredText(str2, "#ffff0000", "bold"));
                        }
                    }
                    if (((Components) getItem(position)).provider) {
                        str2 = ((Components) getItem(position)).Name + LogCollector.LINE_SEPARATOR;
                        if (((Components) getItem(position)).Status) {
                            textView.setText(Utils.getColoredText(str2, "#ff00ff00", "bold"));
                        } else {
                            textView.setText(Utils.getColoredText(str2, "#ffff0000", "bold"));
                        }
                    }
                }
                return view;
            }
        };
    }

    private void getSignatureKeys() {
        try {
            if (new File(toolfilesdir + InternalZipConstants.ZIP_FILE_SEPARATOR + "zipalign").exists() && new File(toolfilesdir + InternalZipConstants.ZIP_FILE_SEPARATOR + "zipalign").length() == Utils.getRawLength(C0149R.raw.zipalign)) {
                Utils.chmod(new File(toolfilesdir + InternalZipConstants.ZIP_FILE_SEPARATOR + "zipalign"), 777);
                Utils.run_all("chmod 777 " + toolfilesdir + InternalZipConstants.ZIP_FILE_SEPARATOR + "zipalign");
            } else {
                Utils.getRawToFile(C0149R.raw.zipalign, new File(toolfilesdir + "/zipalign"));
                Utils.chmod(new File(getInstance().getFilesDir() + InternalZipConstants.ZIP_FILE_SEPARATOR + "zipalign"), 777);
                Utils.run_all("chmod 777 " + getInstance().getFilesDir() + InternalZipConstants.ZIP_FILE_SEPARATOR + "zipalign");
            }
            Utils.getAssets("testkey.pk8", basepath + "/Modified/Keys");
            Utils.getAssets("testkey.sbt", basepath + "/Modified/Keys");
            Utils.getAssets("testkey.x509.pem", basepath + "/Modified/Keys");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showMessageInfo(Context context, String title, String message) {
        final Dialog dialog = new Dialog(context);
        StatFs statFs = new StatFs("/system");
        int Total = (statFs.getBlockCount() * statFs.getBlockSize()) / AccessibilityEventCompat.TYPE_TOUCH_INTERACTION_START;
        int Free = (statFs.getAvailableBlocks() * statFs.getBlockSize()) / AccessibilityEventCompat.TYPE_TOUCH_INTERACTION_START;
        int Busy = Total - Free;
        StatFs statFs2 = new StatFs("/data");
        int Total2 = (statFs2.getBlockCount() * statFs2.getBlockSize()) / AccessibilityEventCompat.TYPE_TOUCH_INTERACTION_START;
        int Free2 = (statFs2.getAvailableBlocks() * statFs2.getBlockSize()) / AccessibilityEventCompat.TYPE_TOUCH_INTERACTION_START;
        int Busy2 = Total - Free;
        dialog.setCancelable(true);
        dialog.setTitle(title);
        dialog.setContentView(C0149R.layout.freespace);
        ((TextView) dialog.findViewById(C0149R.id.messageText)).setText(message);
        ((TextView) dialog.findViewById(C0149R.id.textSystem)).setText("System ROM (/system): " + Total + "Mb (" + Free + "Mb free)");
        ProgressBar progress = (ProgressBar) dialog.findViewById(C0149R.id.progressBarSystem);
        progress.setMax(Total);
        progress.incrementProgressBy(Busy);
        ((TextView) dialog.findViewById(C0149R.id.textData)).setText("Internal SD (/data): " + Total2 + "Mb (" + Free2 + "Mb free)");
        ProgressBar progress2 = (ProgressBar) dialog.findViewById(C0149R.id.progressBarData);
        progress2.setMax(Total2);
        progress2.incrementProgressBy(Busy2);
        ((Button) dialog.findViewById(C0149R.id.buttonOk)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        Utils.showDialog(dialog);
    }

    public static void showMessageStatic(final String title, final String message) {
        runToMainStatic(new Runnable() {
            public void run() {
                AlertDlg dialog = new AlertDlg(listAppsFragment.patchAct);
                dialog.setTitle(title);
                dialog.setMessage(message);
                dialog.setPositiveButton(17039370, null);
                try {
                    Utils.showDialog(dialog.create());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private static void showMessageInfoStatic(Context context, String title, String message) {
        final Dialog dialog = new Dialog(context);
        StatFs statFs = new StatFs("/system");
        int Total = (statFs.getBlockCount() * statFs.getBlockSize()) / AccessibilityEventCompat.TYPE_TOUCH_INTERACTION_START;
        int Free = (statFs.getAvailableBlocks() * statFs.getBlockSize()) / AccessibilityEventCompat.TYPE_TOUCH_INTERACTION_START;
        int Busy = Total - Free;
        StatFs statFs2 = new StatFs("/data");
        int Total2 = (statFs2.getBlockCount() * statFs2.getBlockSize()) / AccessibilityEventCompat.TYPE_TOUCH_INTERACTION_START;
        int Free2 = (statFs2.getAvailableBlocks() * statFs2.getBlockSize()) / AccessibilityEventCompat.TYPE_TOUCH_INTERACTION_START;
        int Busy2 = Total - Free;
        dialog.setCancelable(true);
        dialog.setTitle(title);
        dialog.setContentView(C0149R.layout.freespace);
        ((TextView) dialog.findViewById(C0149R.id.messageText)).setText(message);
        ((TextView) dialog.findViewById(C0149R.id.textSystem)).setText("System ROM (/system): " + Total + "Mb (" + Free + "Mb free)");
        ProgressBar progress = (ProgressBar) dialog.findViewById(C0149R.id.progressBarSystem);
        progress.setMax(Total);
        progress.incrementProgressBy(Busy);
        ((TextView) dialog.findViewById(C0149R.id.textData)).setText("Internal SD (/data): " + Total2 + "Mb (" + Free2 + "Mb free)");
        ProgressBar progress2 = (ProgressBar) dialog.findViewById(C0149R.id.progressBarData);
        progress2.setMax(Total2);
        progress2.incrementProgressBy(Busy2);
        ((Button) dialog.findViewById(C0149R.id.buttonOk)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        Utils.showDialog(dialog);
    }

    public static String[] consolePM() {
        ArrayList<String> packages = new ArrayList();
        try {
            Process localProcess = Runtime.getRuntime().exec("pm list packages");
            localProcess.waitFor();
            DataInputStream localDataInputStream = new DataInputStream(localProcess.getInputStream());
            byte[] arrayOfByte = new byte[localDataInputStream.available()];
            localDataInputStream.read(arrayOfByte);
            String result = new String(arrayOfByte);
            localProcess.destroy();
            String[] tails = result.split(LogCollector.LINE_SEPARATOR);
            int length = tails.length;
            for (int i = SETTINGS_VIEWSIZE_SMALL; i < length; i += TITLE_LIST_ITEM) {
                String tail = tails[i];
                if (tail.startsWith("package:")) {
                    packages.add(tail.substring(CREATEAPK_DIALOG));
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        } catch (Exception e3) {
            System.out.println("LuckyPatcher (consolePM):" + e3);
            e3.printStackTrace();
        }
        String[] ret_packages = new String[packages.size()];
        int j = SETTINGS_VIEWSIZE_SMALL;
        Iterator it = packages.iterator();
        while (it.hasNext()) {
            ret_packages[j] = (String) it.next();
            j += TITLE_LIST_ITEM;
        }
        return ret_packages;
    }

    public static String[] getPackages() {
        int i;
        System.out.println("LuckyPatcher (getPackageManager install pkgs): start.");
        String[] packages = null;
        PackageManager pm = getPkgMng();
        try {
            List<PackageInfo> InstalledPackages = pm.getInstalledPackages(SETTINGS_VIEWSIZE_SMALL);
            try {
                packages = new String[InstalledPackages.size()];
                i = SETTINGS_VIEWSIZE_SMALL;
                for (PackageInfo pkg : InstalledPackages) {
                    packages[i] = pkg.packageName;
                    i += TITLE_LIST_ITEM;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e2) {
            System.out.println("LuckyPatcher (getPM install pkgs): " + e2);
            e2.printStackTrace();
        }
        if (packages == null || packages.length == 0) {
            System.out.println("LuckyPatcher (Intent PackageManager install pkgs): start.");
            try {
                List<ResolveInfo> list = pm.queryIntentActivities(new Intent("android.intent.action.MAIN"), TransportMediator.FLAG_KEY_MEDIA_NEXT);
                packages = new String[list.size()];
                i = SETTINGS_VIEWSIZE_SMALL;
                for (ResolveInfo appInfo : list) {
                    packages[i] = appInfo.activityInfo.packageName;
                    i += TITLE_LIST_ITEM;
                }
            } catch (Exception e3) {
            }
        }
        if (packages != null && packages.length != 0) {
            return packages;
        }
        System.out.println("LuckyPatcher (consolePM install pkgs): start.");
        return consolePM();
    }

    public static void showDialogLP(int dialog) {
        switch (dialog) {
            case TITLE_LIST_ITEM /*1*/:
                if (progress != null) {
                    progress.dismiss();
                    progress = null;
                    progress = Progress_Dialog.newInstance();
                    progress.showDialog();
                    return;
                }
                progress = Progress_Dialog.newInstance();
                progress.showDialog();
                return;
            case SETTINGS_SORTBY_STATUS /*2*/:
                if (patch_d != null) {
                    patch_d.dismiss();
                    patch_d = null;
                }
                patch_d = new Patch_Dialog();
                patch_d.showDialog();
                return;
            case Custom_PATCH_DIALOG /*4*/:
                if (c_p_d != null) {
                    c_p_d.dismiss();
                    c_p_d = null;
                }
                c_p_d = new Custom_Patch_Dialog();
                c_p_d.showDialog();
                return;
            case EXT_PATCH_DIALOG /*5*/:
                if (e_p_d != null) {
                    e_p_d.dismiss();
                    e_p_d = null;
                }
                e_p_d = new Ext_Patch_Dialog();
                e_p_d.showDialog();
                return;
            case CHILD_NAME_SHEME_SELECT_ITEM /*6*/:
                if (app_d != null) {
                    app_d.dismiss();
                    app_d = null;
                }
                app_d = new App_Dialog();
                app_d.showDialog();
                return;
            case CONTEXT_DIALOG /*7*/:
                if (menu_d != null) {
                    menu_d.dismiss();
                    menu_d = null;
                }
                menu_d = new Menu_Dialog();
                menu_d.showDialog();
                return;
            case CREATEAPK_DIALOG /*8*/:
                if (c_a_d != null) {
                    c_a_d.dismiss();
                    c_a_d = null;
                }
                c_a_d = new Create_Apk_Dialog();
                c_a_d.showDialog();
                return;
            case CREATEAPK_ADS_DIALOG /*9*/:
                if (c_a_a_d != null) {
                    c_a_a_d.dismiss();
                    c_a_a_d = null;
                }
                c_a_a_d = new Create_Apk_Ads_Dialog();
                c_a_a_d.showDialog();
                return;
            case PROGRESS_DIALOG2 /*11*/:
                if (progress2 != null) {
                    progress2.dismiss();
                    progress2 = null;
                    progress2 = Progress_Dialog_2.newInstance();
                    progress2.showDialog();
                    return;
                }
                progress2 = Progress_Dialog_2.newInstance();
                progress2.showDialog();
                return;
            case CUSTOM2_DIALOG /*15*/:
                if (c2_d != null) {
                    c2_d.dismiss();
                    c2_d = null;
                }
                c2_d = new Custom2_Dialog();
                c2_d.showDialog();
                return;
            case LOADING_PROGRESS_DIALOG /*23*/:
                if (progress_loading != null) {
                    progress_loading.dismiss();
                    progress_loading = null;
                    progress_loading = Progress_Dialog_Loading.newInstance();
                    progress_loading.showDialog();
                    return;
                }
                progress_loading = Progress_Dialog_Loading.newInstance();
                progress_loading.showDialog();
                return;
            case MARKET_INSTALL_DIALOG /*30*/:
                if (m_i_d != null) {
                    m_i_d.dismiss();
                    m_i_d = null;
                }
                m_i_d = new Market_Install_Dialog();
                m_i_d.showDialog();
                return;
            case CREATEAPK_SUPPORT_DIALOG /*36*/:
                if (c_a_s_d != null) {
                    c_a_s_d.dismiss();
                    c_a_s_d = null;
                }
                c_a_s_d = new Create_Apk_Support_Dialog();
                c_a_s_d.showDialog();
                return;
            case CONTEXT_DIALOG_STATIC /*38*/:
                if (menu_d_static != null) {
                    menu_d_static.dismiss();
                    menu_d_static = null;
                }
                menu_d_static = new Menu_Dialog_Static();
                menu_d_static.showDialog();
                return;
            default:
                try {
                    System.out.println("dialog " + dialog);
                    if (all_d != null) {
                        if (all_d.isVisible()) {
                            all_d.dismiss();
                        }
                        all_d = null;
                    }
                    all_d = new All_Dialogs();
                    dialog_int = dialog;
                    all_d.dialog_int = dialog;
                    all_d.showDialog();
                    return;
                } catch (RuntimeException e) {
                    e.printStackTrace();
                    return;
                } catch (Exception e2) {
                    e2.printStackTrace();
                    return;
                }
        }
    }

    public static void removeDialogLP(int dialog) {
        switch (dialog) {
            case TITLE_LIST_ITEM /*1*/:
                if (progress != null) {
                    progress.dismiss();
                    progress = null;
                    return;
                }
                return;
            case SETTINGS_SORTBY_STATUS /*2*/:
                if (patch_d != null) {
                    patch_d.dismiss();
                    patch_d = null;
                    return;
                }
                return;
            case Custom_PATCH_DIALOG /*4*/:
                if (c_p_d != null) {
                    c_p_d.dismiss();
                    c_p_d = null;
                    return;
                }
                return;
            case CHILD_NAME_SHEME_SELECT_ITEM /*6*/:
                if (app_d != null) {
                    app_d.dismiss();
                    app_d = null;
                    return;
                }
                return;
            case CONTEXT_DIALOG /*7*/:
                if (menu_d != null) {
                    menu_d.dismiss();
                    menu_d = null;
                    return;
                }
                return;
            case CREATEAPK_DIALOG /*8*/:
                if (c_a_d != null) {
                    c_a_d.dismiss();
                    c_a_d = null;
                    return;
                }
                return;
            case PROGRESS_DIALOG2 /*11*/:
                if (progress2 != null) {
                    progress2.dismiss();
                    progress2 = null;
                    return;
                }
                return;
            case CUSTOM2_DIALOG /*15*/:
                if (c2_d != null) {
                    c2_d.dismiss();
                    c2_d = null;
                    return;
                }
                return;
            case LOADING_PROGRESS_DIALOG /*23*/:
                if (progress_loading != null) {
                    progress_loading.dismiss();
                    progress_loading = null;
                    return;
                }
                return;
            case CONTEXT_DIALOG_STATIC /*38*/:
                if (menu_d_static != null) {
                    menu_d_static.dismiss();
                    menu_d_static = null;
                    return;
                }
                return;
            default:
                try {
                    if (all_d != null) {
                        if (all_d.dialog_int == dialog) {
                            all_d.dismiss();
                        }
                        all_d = null;
                        return;
                    }
                    return;
                } catch (RuntimeException e) {
                    e.printStackTrace();
                    return;
                } catch (Exception e2) {
                    e2.printStackTrace();
                    return;
                }
        }
    }

    private void showNotify(int id, String title, String tickerText, String text) {
        if (!getConfig().getBoolean("hide_notify", false)) {
            long when = System.currentTimeMillis();
            Context context2 = getContext();
            String contentTitle = title;
            String contentText = text;
            PendingIntent contentIntent = PendingIntent.getActivity(getContext(), SETTINGS_VIEWSIZE_SMALL, new Intent(getContext(), patchActivity.class), SETTINGS_VIEWSIZE_SMALL);
            NotificationManager notificationManager = (NotificationManager) getContext().getSystemService("notification");
            Notification notification = new Notification(C0149R.drawable.ic_notify, tickerText, when);
            Notification notification2 = new Notification();
            notification.setLatestEventInfo(context2, contentTitle, contentText, contentIntent);
            notificationManager.notify(id, notification);
        }
    }

    public void new_method_lvl() {
        runWithWait(new Runnable() {

            class C02721 implements Runnable {
                C02721() {
                }

                public void run() {
                    listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.error_not_found_storage_lic));
                    listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
                    listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                }
            }

            class C02732 implements Runnable {
                C02732() {
                }

                public void run() {
                    listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.error_not_found_storage_lic));
                    listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
                    listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                }
            }

            class C02743 implements Runnable {
                C02743() {
                }

                public void run() {
                    listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.error_not_found_lic_data));
                    listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
                    listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                }
            }

            class C02754 implements Runnable {
                C02754() {
                }

                public void run() {
                    listAppsFragment.this.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.error_not_found_lic_data));
                    listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
                    listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                }
            }

            class C02765 implements Runnable {
                C02765() {
                }

                public void run() {
                    listAppsFragment.this.showMessage(Utils.getText(C0149R.string.well_done), Utils.getText(C0149R.string.create_lic_done));
                    listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
                    listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                    try {
                        listAppsFragment.plia.getItem(listAppsFragment.pli.pkgName).modified = true;
                        listAppsFragment.plia.notifyDataSetChanged(listAppsFragment.plia.getItem(listAppsFragment.pli.pkgName));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    listAppsFragment.getConfig().edit().putBoolean(listAppsFragment.plia.getItem(listAppsFragment.pli.pkgName).pkgName, true).commit();
                }
            }

            /* JADX WARNING: inconsistent code. */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void run() {
                File file;
                FileChannel ChannelDex;
                File dalvikcache = null;
                ArrayList<byte[]> sol = new ArrayList();
                byte[] bArr = new byte[listAppsFragment.CREATEAPK_DIALOG];
                bArr = new byte[]{(byte) 0, (byte) 3, (byte) 1, (byte) 0, (byte) 20, (byte) 0, (byte) 0, (byte) 0};
                File xmlfile = listAppsFragment.this.prefs_file;
                try {
                    dalvikcache = Utils.getFileDalvikCache(listAppsFragment.getPkgMng().getPackageInfo(listAppsFragment.pli.pkgName, listAppsFragment.SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir);
                    if (dalvikcache == null) {
                        try {
                            file = new File(Utils.getPlaceForOdex(listAppsFragment.getPkgMng().getPackageInfo(listAppsFragment.pli.pkgName, listAppsFragment.SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir, false));
                            if (file.exists()) {
                                dalvikcache = file;
                            }
                        } catch (NameNotFoundException e1) {
                            e1.printStackTrace();
                        }
                    }
                } catch (NameNotFoundException e) {
                    e.printStackTrace();
                }
                try {
                    ChannelDex = new RandomAccessFile(dalvikcache, InternalZipConstants.READ_MODE).getChannel();
                    MappedByteBuffer fileBytes = ChannelDex.map(MapMode.READ_ONLY, 0, (long) ((int) ChannelDex.size()));
                    Object buffer = new byte[listAppsFragment.LVL_PATCH_CONTEXT_DIALOG];
                    long j = 0;
                    while (fileBytes.hasRemaining()) {
                        int curentPos = fileBytes.position();
                        if (fileBytes.get() == bArr[listAppsFragment.SETTINGS_VIEWSIZE_SMALL]) {
                            int i = listAppsFragment.TITLE_LIST_ITEM;
                            fileBytes.position(curentPos + listAppsFragment.TITLE_LIST_ITEM);
                            if (!fileBytes.hasRemaining()) {
                                break;
                            }
                            byte prufbyte = fileBytes.get();
                            while (prufbyte == bArr[i]) {
                                i += listAppsFragment.TITLE_LIST_ITEM;
                                if (i == bArr.length) {
                                    System.out.println("Check");
                                    fileBytes.get(buffer);
                                    int pos = fileBytes.position();
                                    if (fileBytes.get(pos) == (byte) 0) {
                                    }
                                    if (fileBytes.get(pos + listAppsFragment.CHILD_NAME_SHEME_SELECT_ITEM) == (byte) 0) {
                                    }
                                    fileBytes.position(curentPos + listAppsFragment.TITLE_LIST_ITEM);
                                } else {
                                    prufbyte = fileBytes.get();
                                }
                            }
                            fileBytes.position(curentPos + listAppsFragment.TITLE_LIST_ITEM);
                        }
                        fileBytes.position(curentPos + listAppsFragment.TITLE_LIST_ITEM);
                        j++;
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                    System.out.println(BuildConfig.VERSION_NAME + e2);
                } catch (FileNotFoundException localFileNotFoundException) {
                    localFileNotFoundException.printStackTrace();
                }
                ChannelDex.close();
                if (sol.size() > 0) {
                    file = new File(listAppsFragment.this.getContext().getFilesDir() + InternalZipConstants.ZIP_FILE_SEPARATOR + listAppsFragment.this.prefs_name);
                    String str = BuildConfig.VERSION_NAME;
                    String str2 = BuildConfig.VERSION_NAME;
                    String str3 = BuildConfig.VERSION_NAME;
                    String str4 = BuildConfig.VERSION_NAME;
                    String str5 = BuildConfig.VERSION_NAME;
                    boolean bLR = false;
                    boolean bVT = false;
                    boolean bRU = false;
                    boolean bMR = false;
                    boolean bRC = false;
                    String str6 = BuildConfig.VERSION_NAME;
                    String VTKey = BuildConfig.VERSION_NAME;
                    String RUKey = BuildConfig.VERSION_NAME;
                    String MRKey = BuildConfig.VERSION_NAME;
                    String RCKey = BuildConfig.VERSION_NAME;
                    if (Utils.exists(xmlfile.getAbsolutePath())) {
                        if (file.exists()) {
                            file.delete();
                        }
                        Utils.copyFile(xmlfile.getAbsolutePath(), file.getAbsolutePath(), false, false);
                        Utils.run_all("chmod 777 " + file.getAbsolutePath());
                        if (file.exists()) {
                            try {
                                XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
                                factory.setNamespaceAware(true);
                                XmlPullParser xpp = factory.newPullParser();
                                xpp.setInput(new FileReader(new File(listAppsFragment.this.getContext().getFilesDir() + InternalZipConstants.ZIP_FILE_SEPARATOR + listAppsFragment.this.prefs_name)));
                                int eventType = xpp.getEventType();
                                while (eventType != listAppsFragment.TITLE_LIST_ITEM) {
                                    if (eventType == listAppsFragment.SETTINGS_SORTBY_STATUS && xpp.getName().equals("string")) {
                                        if (eventType == listAppsFragment.SETTINGS_SORTBY_STATUS && xpp.getAttributeValue(listAppsFragment.SETTINGS_VIEWSIZE_SMALL).equals("lastResponse")) {
                                            while (eventType != listAppsFragment.TITLE_LIST_ITEM && eventType != listAppsFragment.SETTINGS_SORTBY_TIME) {
                                                eventType = xpp.next();
                                                if (eventType == listAppsFragment.Custom_PATCH_DIALOG) {
                                                    str = xpp.getText();
                                                    bLR = true;
                                                    break;
                                                }
                                            }
                                        }
                                        if (eventType == listAppsFragment.SETTINGS_SORTBY_STATUS && xpp.getAttributeValue(listAppsFragment.SETTINGS_VIEWSIZE_SMALL).equals("retryUntil")) {
                                            while (eventType != listAppsFragment.TITLE_LIST_ITEM && eventType != listAppsFragment.SETTINGS_SORTBY_TIME) {
                                                eventType = xpp.next();
                                                if (eventType == listAppsFragment.Custom_PATCH_DIALOG) {
                                                    str3 = xpp.getText();
                                                    bRU = true;
                                                    break;
                                                }
                                            }
                                        }
                                        if (eventType == listAppsFragment.SETTINGS_SORTBY_STATUS && xpp.getAttributeValue(listAppsFragment.SETTINGS_VIEWSIZE_SMALL).equals("maxRetries")) {
                                            while (eventType != listAppsFragment.TITLE_LIST_ITEM && eventType != listAppsFragment.SETTINGS_SORTBY_TIME) {
                                                eventType = xpp.next();
                                                if (eventType == listAppsFragment.Custom_PATCH_DIALOG) {
                                                    str4 = xpp.getText();
                                                    bMR = true;
                                                    break;
                                                }
                                            }
                                        }
                                        if (eventType == listAppsFragment.SETTINGS_SORTBY_STATUS && xpp.getAttributeValue(listAppsFragment.SETTINGS_VIEWSIZE_SMALL).equals("retryCount")) {
                                            while (eventType != listAppsFragment.TITLE_LIST_ITEM && eventType != listAppsFragment.SETTINGS_SORTBY_TIME) {
                                                eventType = xpp.next();
                                                if (eventType == listAppsFragment.Custom_PATCH_DIALOG) {
                                                    str5 = xpp.getText();
                                                    bRC = true;
                                                    break;
                                                }
                                            }
                                        }
                                        if (eventType == listAppsFragment.SETTINGS_SORTBY_STATUS && xpp.getAttributeValue(listAppsFragment.SETTINGS_VIEWSIZE_SMALL).equals("validityTimestamp")) {
                                            while (eventType != listAppsFragment.TITLE_LIST_ITEM && eventType != listAppsFragment.SETTINGS_SORTBY_TIME) {
                                                eventType = xpp.next();
                                                if (eventType == listAppsFragment.Custom_PATCH_DIALOG) {
                                                    str2 = xpp.getText();
                                                    bVT = true;
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                    eventType = xpp.next();
                                }
                            } catch (Exception e22) {
                                e22.printStackTrace();
                            }
                            String devid = Secure.getString(listAppsFragment.this.getContext().getContentResolver(), "android_id");
                            AESObfuscator s = null;
                            String validityTimestamp = Long.toString(System.currentTimeMillis() + 62208000000L);
                            boolean keyfound = false;
                            Iterator it = sol.iterator();
                            while (it.hasNext()) {
                                AESObfuscator aESObfuscator = new AESObfuscator((byte[]) it.next(), listAppsFragment.pli.pkgName, devid);
                                if (bLR) {
                                    try {
                                        str = aESObfuscator.unobfuscate(str, BuildConfig.VERSION_NAME);
                                    } catch (ValidationException e23) {
                                        e23.printStackTrace();
                                    } catch (Exception e222) {
                                        e222.printStackTrace();
                                    }
                                }
                                if (bVT) {
                                    try {
                                        str2 = aESObfuscator.unobfuscate(str2, BuildConfig.VERSION_NAME);
                                    } catch (ValidationException e232) {
                                        e232.printStackTrace();
                                    }
                                }
                                if (bRC) {
                                    try {
                                        str5 = aESObfuscator.unobfuscate(str5, BuildConfig.VERSION_NAME);
                                    } catch (ValidationException e2322) {
                                        e2322.printStackTrace();
                                    }
                                }
                                if (bMR) {
                                    try {
                                        str4 = aESObfuscator.unobfuscate(str4, BuildConfig.VERSION_NAME);
                                    } catch (ValidationException e23222) {
                                        e23222.printStackTrace();
                                    }
                                }
                                if (bRU) {
                                    try {
                                        str3 = aESObfuscator.unobfuscate(str3, BuildConfig.VERSION_NAME);
                                    } catch (ValidationException e232222) {
                                        e232222.printStackTrace();
                                    }
                                }
                                keyfound = true;
                            }
                            if (keyfound) {
                                String lastResponse = BuildConfig.VERSION_NAME;
                                if (str.contains("291") || str.contains("561") || str.contains("256")) {
                                    str6 = str.replace("256", BuildConfig.VERSION_NAME).replace("291", BuildConfig.VERSION_NAME).replace("561", BuildConfig.VERSION_NAME);
                                    lastResponse = "256";
                                }
                                if (str.contains("NOT_LICENSED") || str.contains("RETRY") || str.contains("LICENSED")) {
                                    str6 = str.replace("NOT_LICENSED", BuildConfig.VERSION_NAME).replace("RETRY", BuildConfig.VERSION_NAME).replace("LICENSED", BuildConfig.VERSION_NAME);
                                    lastResponse = "LICENSED";
                                }
                                VTKey = str2.replaceAll("[0-9]", BuildConfig.VERSION_NAME);
                                RCKey = str5.replaceAll("[0-9]", BuildConfig.VERSION_NAME);
                                MRKey = str4.replaceAll("[0-9]", BuildConfig.VERSION_NAME);
                                RUKey = str3.replaceAll("[0-9]", BuildConfig.VERSION_NAME);
                                if (!bVT && str6.equals("lastResponse")) {
                                    VTKey = "validityTimestamp";
                                }
                                if (!bRC && str6.equals("lastResponse")) {
                                    RCKey = "retryCount";
                                }
                                if (!bMR && str6.equals("lastResponse")) {
                                    MRKey = "maxRetries";
                                }
                                if (!bRU && str6.equals("lastResponse")) {
                                    RUKey = "retryUntil";
                                }
                                String retryUntil = Long.toString(System.currentTimeMillis() + 62208000000L);
                                validityTimestamp = s.obfuscate(validityTimestamp, VTKey);
                                lastResponse = s.obfuscate(lastResponse, str6);
                                retryUntil = s.obfuscate(retryUntil, RUKey);
                                String maxRetries = s.obfuscate("10", MRKey);
                                String retryCount = s.obfuscate("0", RCKey);
                                file = new File(listAppsFragment.this.getContext().getFilesDir() + InternalZipConstants.ZIP_FILE_SEPARATOR + listAppsFragment.this.prefs_name);
                                try {
                                    file.createNewFile();
                                    OutputStream fileOutputStream = new FileOutputStream(file);
                                    XmlSerializer serializer = Xml.newSerializer();
                                    serializer.setOutput(fileOutputStream, "UTF-8");
                                    serializer.startDocument(null, Boolean.valueOf(true));
                                    serializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);
                                    serializer.startTag(null, "map");
                                    serializer.startTag(null, "string");
                                    serializer.attribute(BuildConfig.VERSION_NAME, "name", "validityTimestamp");
                                    serializer.text(validityTimestamp);
                                    serializer.endTag(null, "string");
                                    serializer.startTag(null, "string");
                                    serializer.attribute(BuildConfig.VERSION_NAME, "name", "retryCount");
                                    serializer.text(retryCount);
                                    serializer.endTag(null, "string");
                                    serializer.startTag(null, "string");
                                    serializer.attribute(BuildConfig.VERSION_NAME, "name", "maxRetries");
                                    serializer.text(maxRetries);
                                    serializer.endTag(null, "string");
                                    serializer.startTag(null, "string");
                                    serializer.attribute(BuildConfig.VERSION_NAME, "name", "retryUntil");
                                    serializer.text(retryUntil);
                                    serializer.endTag(null, "string");
                                    serializer.startTag(null, "string");
                                    serializer.attribute(BuildConfig.VERSION_NAME, "name", "lastResponse");
                                    serializer.text(lastResponse);
                                    serializer.endTag(null, "string");
                                    serializer.endTag(null, "map");
                                    serializer.endDocument();
                                    serializer.flush();
                                    fileOutputStream.close();
                                } catch (Exception e3) {
                                }
                                Utils.copyFile(file.getAbsolutePath(), xmlfile.getAbsolutePath(), false, false);
                                if (file.exists()) {
                                    file.delete();
                                }
                                Utils.run_all("chmod 777 " + xmlfile.getAbsolutePath());
                                int uid = listAppsFragment.SETTINGS_VIEWSIZE_SMALL;
                                try {
                                    uid = listAppsFragment.getPkgMng().getApplicationInfo(listAppsFragment.pli.pkgName, listAppsFragment.SETTINGS_VIEWSIZE_SMALL).uid;
                                } catch (NameNotFoundException e4) {
                                    e4.printStackTrace();
                                }
                                if (uid != 0) {
                                    Utils.run_all("chown " + uid + "." + uid + " " + xmlfile.getAbsolutePath());
                                    Utils.run_all("chown " + uid + ":" + uid + " " + xmlfile.getAbsolutePath());
                                }
                                listAppsFragment.this.runToMain(new C02765());
                                return;
                            }
                            listAppsFragment.this.runToMain(new C02743());
                            return;
                        }
                        listAppsFragment.this.runToMain(new C02732());
                        return;
                    }
                    listAppsFragment.this.runToMain(new C02721());
                    return;
                }
                listAppsFragment.this.runToMain(new C02754());
            }
        });
    }

    public static void unzip(File apk) {
        boolean found1 = false;
        boolean found2 = false;
        try {
            FileInputStream fin = new FileInputStream(apk);
            ZipInputStream zipInputStream = new ZipInputStream(fin);
            for (ZipEntry ze = zipInputStream.getNextEntry(); ze != null && true; ze = zipInputStream.getNextEntry()) {
                String haystack = ze.getName();
                if (haystack.equals("classes.dex")) {
                    FileOutputStream fout = new FileOutputStream(getInstance().getFilesDir().getAbsolutePath() + InternalZipConstants.ZIP_FILE_SEPARATOR + "classes.dex");
                    byte[] buffer = new byte[InternalZipConstants.UFT8_NAMES_FLAG];
                    while (true) {
                        int length = zipInputStream.read(buffer);
                        if (length == -1) {
                            break;
                        }
                        fout.write(buffer, SETTINGS_VIEWSIZE_SMALL, length);
                    }
                    zipInputStream.closeEntry();
                    fout.close();
                    found1 = true;
                }
                if (haystack.equals("AndroidManifest.xml")) {
                    FileOutputStream fout2 = new FileOutputStream(getInstance().getFilesDir().getAbsolutePath() + InternalZipConstants.ZIP_FILE_SEPARATOR + "AndroidManifest.xml");
                    byte[] buffer2 = new byte[InternalZipConstants.UFT8_NAMES_FLAG];
                    while (true) {
                        int length2 = zipInputStream.read(buffer2);
                        if (length2 == -1) {
                            break;
                        }
                        fout2.write(buffer2, SETTINGS_VIEWSIZE_SMALL, length2);
                    }
                    zipInputStream.closeEntry();
                    fout2.close();
                    found2 = true;
                }
                if (found1 && found2) {
                    break;
                }
            }
            zipInputStream.close();
            fin.close();
        } catch (Exception e) {
            try {
                ZipFile zipFile = new ZipFile(apk);
                zipFile.extractFile("classes.dex", getInstance().getFilesDir().getAbsolutePath());
                zipFile.extractFile("AndroidManifest.xml", getInstance().getFilesDir().getAbsolutePath());
            } catch (ZipException e1) {
                System.out.println("Error classes.dex decompress! " + e1);
                System.out.println("Exception e1" + e.toString());
            } catch (Exception e12) {
                System.out.println("Error classes.dex decompress! " + e12);
                System.out.println("Exception e1" + e.toString());
            }
            System.out.println("Exception e" + e.toString());
        }
    }

    public static void zip(String apkname, String workingDir) {
        Exception e;
        try {
            ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(workingDir + InternalZipConstants.ZIP_FILE_SEPARATOR + apkname, false)));
            byte[] data = new byte[LZMA2Options.DICT_SIZE_MIN];
            FileInputStream fi = new FileInputStream(workingDir + "/classes.dex");
            BufferedInputStream origin = new BufferedInputStream(fi, LZMA2Options.DICT_SIZE_MIN);
            try {
                out.putNextEntry(new ZipEntry("classes.dex"));
                while (true) {
                    int count = origin.read(data, SETTINGS_VIEWSIZE_SMALL, LZMA2Options.DICT_SIZE_MIN);
                    if (count == -1) {
                        break;
                    }
                    out.write(data, SETTINGS_VIEWSIZE_SMALL, count);
                }
                origin.close();
                out.closeEntry();
                out.close();
                fi.close();
            } catch (Exception e2) {
                e = e2;
                BufferedInputStream bufferedInputStream = origin;
                e.printStackTrace();
                new File(workingDir + "/AndroidManifest.xml").delete();
                new File(workingDir + "/classes.dex").delete();
            }
        } catch (Exception e3) {
            e = e3;
            e.printStackTrace();
            new File(workingDir + "/AndroidManifest.xml").delete();
            new File(workingDir + "/classes.dex").delete();
        }
        new File(workingDir + "/AndroidManifest.xml").delete();
        new File(workingDir + "/classes.dex").delete();
    }

    public void runUpdate() {
        UpdateVersion updateVersion = new UpdateVersion();
        String[] strArr = new String[TITLE_LIST_ITEM];
        strArr[SETTINGS_VIEWSIZE_SMALL] = "novajaversia";
        updateVersion.execute(strArr);
    }

    public void toolbar_refresh_click() {
        if (plia != null) {
            Thread scan2 = new Thread(new AppScanning());
            scan2.setPriority(PERM_CONTEXT_DIALOG);
            scan2.start();
            refresh = true;
            plia.refreshPkgs(true);
        }
    }

    public void app_scanning() {
        if (plia != null) {
            Thread scan2 = new Thread(new AppScanning());
            scan2.setPriority(PERM_CONTEXT_DIALOG);
            scan2.start();
        }
    }

    public void runUpdateTask() {
        Thread u = new Thread(new CheckUpdate());
        u.setPriority(TITLE_LIST_ITEM);
        u.start();
    }

    public void runToMain(Runnable for_run) {
        if (frag != null) {
            frag.getContext().runOnUiThread(for_run);
        } else if (handler != null) {
            handler.post(for_run);
        }
    }

    public void runWithWait(final Runnable run) {
        Thread runT = new Thread(new Runnable() {

            class C02771 implements Runnable {
                C02771() {
                }

                public void run() {
                    listAppsFragment.showDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                    listAppsFragment.progress2.setCancelable(true);
                    listAppsFragment.progress2.setMessage(Utils.getText(C0149R.string.wait));
                }
            }

            class C02782 implements Runnable {
                C02782() {
                }

                public void run() {
                    listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
                }
            }

            public void run() {
                listAppsFragment.this.runToMain(new C02771());
                try {
                    run.run();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                listAppsFragment.this.runToMain(new C02782());
            }
        });
        runT.setPriority(PERM_CONTEXT_DIALOG);
        runT.start();
    }

    public static void runToMainStatic(Runnable for_run) {
        if (frag != null) {
            patchAct.runOnUiThread(for_run);
        } else if (handler != null) {
            handler.post(for_run);
        }
    }

    public void selected_apps() {
        adapterSelect = true;
        selectedApps.clear();
        TextView line2 = (TextView) patchAct.findViewById(C0149R.id.line_hide_vertical);
        ((TextView) patchAct.findViewById(C0149R.id.line_hide_horisontal)).setVisibility(SETTINGS_VIEWSIZE_SMALL);
        line2.setVisibility(SETTINGS_VIEWSIZE_SMALL);
        TextView title = (TextView) patchAct.findViewById(C0149R.id.title_list);
        title.setVisibility(SETTINGS_VIEWSIZE_SMALL);
        Button button = (Button) patchAct.findViewById(C0149R.id.title_button);
        button.setText(Utils.getText(C0149R.string.cancel));
        System.out.println(adapterSelectType);
        switch (adapterSelectType) {
            case C0149R.string.button_move_to_sdcard:
                System.out.println("found" + adapterSelectType);
                title.setText(Utils.getText(C0149R.string.title_for_select_move_to_sdcard));
                advancedFilter = advanced_filter_for_apps_move2sd;
                button.setOnClickListener(new View.OnClickListener() {

                    class C02791 implements Runnable {
                        C02791() {
                        }

                        public void run() {
                            ArrayList<String> commands = new ArrayList();
                            String statics = BuildConfig.VERSION_NAME;
                            if (listAppsFragment.su) {
                                statics = "pm install -r -s -i com.android.vending ";
                            } else {
                                statics = "pm install -r -s ";
                            }
                            ArrayList<PkgListItem> selApps = new ArrayList(listAppsFragment.selectedApps);
                            listAppsFragment.selectedApps.clear();
                            try {
                                Iterator it = selApps.iterator();
                                while (it.hasNext()) {
                                    PkgListItem item = (PkgListItem) it.next();
                                    String apk_file = BuildConfig.VERSION_NAME;
                                    try {
                                        apk_file = listAppsFragment.getPkgMng().getPackageInfo(item.pkgName, listAppsFragment.SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir;
                                        Utils.run_all("chmod 644 " + apk_file);
                                        System.out.println("Start move to sdcard" + apk_file);
                                    } catch (Exception e) {
                                    }
                                    if (!apk_file.equals(BuildConfig.VERSION_NAME)) {
                                        commands.add(statics + apk_file);
                                    }
                                }
                            } catch (Exception e2) {
                                e2.printStackTrace();
                            }
                            if (commands.size() > 0) {
                                String[] com = new String[commands.size()];
                                commands.toArray(com);
                                if (listAppsFragment.su) {
                                    new Utils(BuildConfig.VERSION_NAME).cmdRoot(com);
                                } else {
                                    Utils.cmd(com);
                                }
                            }
                            listAppsFragment.this.resetBatchOperation();
                        }
                    }

                    public void onClick(View v) {
                        listAppsFragment.this.runWithWait(new C02791());
                    }
                });
                break;
            case C0149R.string.button_block_internet:
                title.setText(Utils.getText(C0149R.string.title_for_block_internet));
                advancedFilter = advanced_filter_apps_with_internet;
                button.setOnClickListener(new View.OnClickListener() {

                    class C02821 implements Runnable {
                        C02821() {
                        }

                        public void run() {
                            ArrayList<String> commands = new ArrayList();
                            String str = BuildConfig.VERSION_NAME;
                            str = "pm revoke ";
                            ArrayList<PkgListItem> selApps = new ArrayList(listAppsFragment.selectedApps);
                            listAppsFragment.selectedApps.clear();
                            Iterator it = selApps.iterator();
                            while (it.hasNext()) {
                                PkgListItem item = (PkgListItem) it.next();
                                System.out.println("Block Internet" + BuildConfig.VERSION_NAME);
                                commands.add(str + item.pkgName + "/android.permission.INTERNET");
                            }
                            if (commands.size() > 0) {
                                String[] com = new String[commands.size()];
                                commands.toArray(com);
                                if (listAppsFragment.su) {
                                    new Utils(BuildConfig.VERSION_NAME).cmdRoot(com);
                                } else {
                                    Utils.cmd(com);
                                }
                            }
                            listAppsFragment.this.resetBatchOperation();
                        }
                    }

                    public void onClick(View v) {
                        listAppsFragment.this.runWithWait(new C02821());
                    }
                });
                break;
            case C0149R.string.button_integrate_update_apps:
                title.setText(Utils.getText(C0149R.string.title_for_select_integrate_update_apps));
                advancedFilter = C0149R.id.filter8;
                button.setOnClickListener(new View.OnClickListener() {

                    class C02961 implements Runnable {
                        C02961() {
                        }

                        public void run() {
                            ArrayList<PkgListItem> selApps = new ArrayList(listAppsFragment.selectedApps);
                            listAppsFragment.selectedApps.clear();
                            Iterator it = selApps.iterator();
                            while (it.hasNext()) {
                                System.out.println("Integrate update for " + ((PkgListItem) it.next()).pkgName);
                            }
                            listAppsFragment.integrate_to_system(selApps, false, false);
                            listAppsFragment.this.resetBatchOperation();
                        }
                    }

                    public void onClick(View v) {
                        new Thread(new C02961()).start();
                    }
                });
                break;
            case C0149R.string.button_move_to_internal:
                System.out.println("found" + adapterSelectType);
                title.setText(Utils.getText(C0149R.string.title_for_select_move_to_internal));
                advancedFilter = C0149R.id.filter6;
                button.setOnClickListener(new View.OnClickListener() {

                    class C02801 implements Runnable {
                        C02801() {
                        }

                        public void run() {
                            ArrayList<String> commands = new ArrayList();
                            String statics = BuildConfig.VERSION_NAME;
                            if (listAppsFragment.su) {
                                statics = "pm install -r -f -i com.android.vending ";
                            } else {
                                statics = "pm install -r -f ";
                            }
                            ArrayList<PkgListItem> selApps = new ArrayList(listAppsFragment.selectedApps);
                            listAppsFragment.selectedApps.clear();
                            Iterator it = selApps.iterator();
                            while (it.hasNext()) {
                                PkgListItem item = (PkgListItem) it.next();
                                String str = BuildConfig.VERSION_NAME;
                                try {
                                    str = listAppsFragment.getPkgMng().getPackageInfo(item.pkgName, listAppsFragment.SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir;
                                    Utils.run_all("chmod 644 " + str);
                                    System.out.println("Start move to internal " + str);
                                } catch (Exception e) {
                                }
                                if (!str.equals(BuildConfig.VERSION_NAME)) {
                                    commands.add(statics + str);
                                }
                            }
                            if (commands.size() > 0) {
                                String[] com = new String[commands.size()];
                                commands.toArray(com);
                                if (listAppsFragment.su) {
                                    new Utils(BuildConfig.VERSION_NAME).cmdRoot(com);
                                } else {
                                    Utils.cmd(com);
                                }
                            }
                            listAppsFragment.this.resetBatchOperation();
                        }
                    }

                    public void onClick(View v) {
                        listAppsFragment.this.runWithWait(new C02801());
                    }
                });
                break;
            case C0149R.string.button_unblock_internet:
                title.setText(Utils.getText(C0149R.string.title_for_unblock_internet));
                advancedFilter = advanced_filter_apps_with_without_internet;
                button.setOnClickListener(new View.OnClickListener() {

                    class C02951 implements Runnable {
                        C02951() {
                        }

                        public void run() {
                            ArrayList<String> commands = new ArrayList();
                            String str = BuildConfig.VERSION_NAME;
                            str = "pm grant ";
                            ArrayList<PkgListItem> selApps = new ArrayList(listAppsFragment.selectedApps);
                            listAppsFragment.selectedApps.clear();
                            Iterator it = selApps.iterator();
                            while (it.hasNext()) {
                                PkgListItem item = (PkgListItem) it.next();
                                System.out.println("Unblock Internet" + BuildConfig.VERSION_NAME);
                                commands.add(str + item.pkgName + "/android.permission.INTERNET");
                            }
                            if (commands.size() > 0) {
                                String[] com = new String[commands.size()];
                                commands.toArray(com);
                                if (listAppsFragment.su) {
                                    new Utils(BuildConfig.VERSION_NAME).cmdRoot(com);
                                } else {
                                    Utils.cmd(com);
                                }
                            }
                            listAppsFragment.this.resetBatchOperation();
                        }
                    }

                    public void onClick(View v) {
                        listAppsFragment.this.runWithWait(new C02951());
                    }
                });
                break;
            case C0149R.string.button_uninstall_apps:
                title.setText(Utils.getText(C0149R.string.title_for_select_uninstall));
                advancedFilter = advanced_filter_for_user_apps;
                button.setOnClickListener(new View.OnClickListener() {

                    class C02811 implements Runnable {
                        C02811() {
                        }

                        public void run() {
                            ArrayList<String> commands = new ArrayList();
                            String str = BuildConfig.VERSION_NAME;
                            str = "pm uninstall ";
                            ArrayList<PkgListItem> selApps = new ArrayList(listAppsFragment.selectedApps);
                            listAppsFragment.selectedApps.clear();
                            Iterator it = selApps.iterator();
                            while (it.hasNext()) {
                                PkgListItem item = (PkgListItem) it.next();
                                System.out.println("Uninstall" + BuildConfig.VERSION_NAME);
                                commands.add(str + item.pkgName);
                            }
                            if (commands.size() > 0) {
                                String[] com = new String[commands.size()];
                                commands.toArray(com);
                                if (listAppsFragment.su) {
                                    new Utils(BuildConfig.VERSION_NAME).cmdRoot(com);
                                } else {
                                    Utils.cmd(com);
                                }
                            }
                            listAppsFragment.this.resetBatchOperation();
                        }
                    }

                    public void onClick(View v) {
                        listAppsFragment.this.runWithWait(new C02811());
                    }
                });
                break;
        }
        button.setVisibility(SETTINGS_VIEWSIZE_SMALL);
        frag.app_scanning();
    }

    public void resetBatchOperation() {
        runToMain(new Runnable() {
            public void run() {
                listAppsFragment.adapterSelect = false;
                listAppsFragment.selectedApps.clear();
                listAppsFragment.advancedFilter = listAppsFragment.SETTINGS_VIEWSIZE_SMALL;
                TextView line2 = (TextView) listAppsFragment.patchAct.findViewById(C0149R.id.line_hide_vertical);
                ((TextView) listAppsFragment.patchAct.findViewById(C0149R.id.line_hide_horisontal)).setVisibility(listAppsFragment.CREATEAPK_DIALOG);
                line2.setVisibility(listAppsFragment.CREATEAPK_DIALOG);
                ((TextView) listAppsFragment.patchAct.findViewById(C0149R.id.title_list)).setVisibility(listAppsFragment.CREATEAPK_DIALOG);
                ((Button) listAppsFragment.patchAct.findViewById(C0149R.id.title_button)).setVisibility(listAppsFragment.CREATEAPK_DIALOG);
                listAppsFragment.this.app_scanning();
                listAppsFragment.removeDialogLP(listAppsFragment.PROGRESS_DIALOG2);
            }
        });
    }

    public static void patch_dialog_text_builder(TextView tv, boolean rebuild) {
        float a = 0.0f;
        float b = 0.0f;
        if (str != null) {
            int i;
            int j;
            if (str.contains("com.android.vending dependencies removed\n")) {
                tv.append(Utils.getColoredText(LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.patterndependencies) + LogCollector.LINE_SEPARATOR, "#ff00ff73", "bold"));
                a = 0.0f + 1.0f;
            }
            if (str == null) {
                str = " ";
            }
            if (str.contains("amazon patch N1!\n") && !str.contains("runpatchads")) {
                tv.append(Utils.getColoredText(LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.patternamazon) + LogCollector.LINE_SEPARATOR, "#ff00ff73", "bold"));
                a += 1.0f;
            }
            if (str.contains("samsung patch N") && !str.contains("runpatchads")) {
                tv.append(Utils.getColoredText(LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.patternsamsung) + LogCollector.LINE_SEPARATOR, "#ff00ff73", "bold"));
                a += 1.0f;
            }
            if (str.contains("Site from AdsBlockList blocked!")) {
                i = SETTINGS_VIEWSIZE_SMALL;
                while (Pattern.compile("Site from AdsBlockList blocked!").matcher(str).find()) {
                    i += TITLE_LIST_ITEM;
                }
                tv.append(Utils.getColoredText(LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.site_pattern) + " (" + i + ") " + LogCollector.LINE_SEPARATOR, "#ff00ff73", "bold"));
                a += 1.0f;
            }
            if (str.contains("ads1 Fixed!\n") || str.contains("support1 Fixed!\n")) {
                tv.append(Utils.getColoredText(LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.pattern1) + LogCollector.LINE_SEPARATOR, "#ff00ff73", "bold"));
                a += 1.0f;
            }
            if (str.contains("ads2 Fixed!\n") || str.contains("support2 Fixed!\n")) {
                tv.append(Utils.getColoredText(LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.pattern2) + LogCollector.LINE_SEPARATOR, "#ff00ff73", "bold"));
                a += 1.0f;
            }
            if (str.contains("ads3 Fixed!\n") || str.contains("support3 Fixed!\n")) {
                tv.append(Utils.getColoredText(LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.pattern3) + LogCollector.LINE_SEPARATOR, "#ff00ff73", "bold"));
                a += 1.0f;
            }
            if (str.contains("ads4 Fixed!\n") || str.contains("support4 Fixed!\n")) {
                tv.append(Utils.getColoredText(LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.pattern4) + LogCollector.LINE_SEPARATOR, "#ff00ff73", "bold"));
                a += 1.0f;
            }
            if (str.contains("ads5 Fixed!\n")) {
                tv.append(Utils.getColoredText(LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.pattern5) + LogCollector.LINE_SEPARATOR, "#ff00ff73", "bold"));
                a += 1.0f;
            }
            if (str.contains("ads6 Fixed!\n")) {
                tv.append(Utils.getColoredText(LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.pattern6) + LogCollector.LINE_SEPARATOR, "#ff00ff73", "bold"));
                a += 1.0f;
            }
            if (str.contains("lvl patch N1!\n")) {
                i = SETTINGS_VIEWSIZE_SMALL;
                while (Pattern.compile("lvl patch N1!\n").matcher(str).find()) {
                    i += TITLE_LIST_ITEM;
                }
                tv.append(Utils.getColoredText(LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.pattern1) + " (" + i + ")" + LogCollector.LINE_SEPARATOR, "#ff00ff73", "bold"));
                a += 1.0f;
            }
            if (str.contains("lvl patch N2!\n")) {
                i = SETTINGS_VIEWSIZE_SMALL;
                while (Pattern.compile("lvl patch N2!\n").matcher(str).find()) {
                    i += TITLE_LIST_ITEM;
                }
                tv.append(Utils.getColoredText(LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.pattern2) + " (" + i + ")" + LogCollector.LINE_SEPARATOR, "#ff00ff73", "bold"));
                a += 1.0f;
            }
            if (str.contains("lvl patch N3!\n")) {
                i = SETTINGS_VIEWSIZE_SMALL;
                while (Pattern.compile("lvl patch N3!\n").matcher(str).find()) {
                    i += TITLE_LIST_ITEM;
                }
                tv.append(Utils.getColoredText(LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.pattern3) + " (" + i + ")" + LogCollector.LINE_SEPARATOR, "#ff00ff73", "bold"));
                a += 1.0f;
            }
            if (str.contains("lvl patch N4!\n")) {
                i = SETTINGS_VIEWSIZE_SMALL;
                while (Pattern.compile("lvl patch N4!\n").matcher(str).find()) {
                    i += TITLE_LIST_ITEM;
                }
                tv.append(Utils.getColoredText(LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.pattern4) + " (" + i + ")" + LogCollector.LINE_SEPARATOR, "#ff00ff73", "bold"));
                a += 1.0f;
            }
            i = SETTINGS_VIEWSIZE_SMALL;
            if (str.contains("lvl patch N5!\n")) {
                while (Pattern.compile("lvl patch N5!\n").matcher(str).find()) {
                    i += TITLE_LIST_ITEM;
                }
                tv.append(Utils.getColoredText(LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.pattern5) + " (" + i + ")" + LogCollector.LINE_SEPARATOR, "#ff00ff73", "bold"));
                a += 1.0f;
            }
            if (str.contains("lvl patch N6!\n")) {
                j = SETTINGS_VIEWSIZE_SMALL;
                while (Pattern.compile("lvl patch N6!\n").matcher(str).find()) {
                    j += TITLE_LIST_ITEM;
                }
                tv.append(Utils.getColoredText(LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.pattern6) + " (" + j + ")" + LogCollector.LINE_SEPARATOR, "#ff00ff73", "bold"));
                a += 1.0f;
            }
            if (str.contains("lvl patch N7!\n")) {
                j = SETTINGS_VIEWSIZE_SMALL;
                while (Pattern.compile("lvl patch N7!\n").matcher(str).find()) {
                    j += TITLE_LIST_ITEM;
                }
                tv.append(Utils.getColoredText(LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.pattern7) + " (" + j + ")" + LogCollector.LINE_SEPARATOR, "#ff00ff73", "bold"));
                a += 1.0f;
            }
            if (!(str.contains("amazon patch N1!\n") || str.contains("runpatchads") || str.contains("runpatchsupport"))) {
                tv.append(Utils.getColoredText(LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.patternamazon_f) + LogCollector.LINE_SEPARATOR, "#ffff0055", "bold"));
                b = 0.0f + 1.0f;
            }
            if (!(str.contains("samsung patch N") || str.contains("runpatchads") || str.contains("runpatchsupport"))) {
                tv.append(Utils.getColoredText(LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.patternsamsung_f) + LogCollector.LINE_SEPARATOR, "#ffff0055", "bold"));
                b += 1.0f;
            }
            if (!(str.contains("lvl patch N1!\n") || str.contains("ads1 Fixed!\n") || str.contains("support1 Fixed!\n"))) {
                tv.append(Utils.getColoredText(LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.pattern1_f) + LogCollector.LINE_SEPARATOR, "#ffff0055", "bold"));
                b += 1.0f;
            }
            if (!(str.contains("lvl patch N2!\n") || str.contains("ads2 Fixed!\n") || str.contains("support2 Fixed!\n"))) {
                tv.append(Utils.getColoredText(LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.pattern2_f) + LogCollector.LINE_SEPARATOR, "#ffff0055", "bold"));
                b += 1.0f;
            }
            if (!(str.contains("lvl patch N3!\n") || str.contains("ads3 Fixed!\n") || str.contains("support3 Fixed!\n"))) {
                tv.append(Utils.getColoredText(LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.pattern3_f) + LogCollector.LINE_SEPARATOR, "#ffff0055", "bold"));
                b += 1.0f;
            }
            if (!(str.contains("lvl patch N4!\n") || str.contains("ads4 Fixed!\n") || str.contains("support4 Fixed!\n"))) {
                tv.append(Utils.getColoredText(LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.pattern4_f) + LogCollector.LINE_SEPARATOR, "#ffff0055", "bold"));
                b += 1.0f;
            }
            if (!(str.contains("lvl patch N5!\n") || str.contains("runpatchads") || str.contains("runpatchsupport"))) {
                tv.append(Utils.getColoredText(LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.pattern5_f) + LogCollector.LINE_SEPARATOR, "#ffff0055", "bold"));
                b += 1.0f;
            }
            if (!(str.contains("lvl patch N6!\n") || str.contains("runpatchads") || str.contains("runpatchsupport"))) {
                tv.append(Utils.getColoredText(LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.pattern6_f) + LogCollector.LINE_SEPARATOR, "#ffff0055", "bold"));
                b += 1.0f;
            }
            if (!(str.contains("lvl patch N7!\n") || str.contains("runpatchads") || str.contains("runpatchsupport"))) {
                tv.append(Utils.getColoredText(LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.pattern7_f) + LogCollector.LINE_SEPARATOR, "#ffff0055", "bold"));
                b += 1.0f;
            }
            if (str.contains("Error: Program files are not found!")) {
                tv.append(Utils.getColoredText(LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.P_not_found) + LogCollector.LINE_SEPARATOR, "#ffff0055", "bold"));
                b += 1.0f;
            }
            if (rebuild) {
                tv.append(Utils.getColoredText(LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.line) + LogCollector.LINE_SEPARATOR, "#fff0e442", "bold"));
            } else if (str.contains("Error:") || str.contains("Fixed") || str.contains("Failed") || str.contains("ads") || str.contains("lvl patch") || str.contains("SU Java-Code Running!")) {
                float result = (float) (Math.round((a / 7.0f) * 100.0f) - 1);
                String str2 = LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.have_luck) + " " + result + Utils.getText(C0149R.string.goodresult) + LogCollector.LINE_SEPARATOR;
                if (result < 0.0f) {
                    str2 = LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.have_luck) + " " + 0.0f + Utils.getText(C0149R.string.poorresult);
                }
                tv.append(Utils.getColoredText(str2, "#fff0e442", "bold"));
            } else {
                tv.setText(Utils.getColoredText(LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.no_root) + LogCollector.LINE_SEPARATOR, "#ff00ffff", "bold"));
            }
        }
    }

    public void setRootWidgetEnabled(boolean bEnable) {
        PackageManager rPackageManager = getInstance().getPackageManager();
        if (rPackageManager != null) {
            ComponentName rComponentName1 = new ComponentName(getInstance(), AppDisablerWidget.class);
            int nComponentEnabledState1 = rPackageManager.getComponentEnabledSetting(rComponentName1);
            ComponentName rComponentName2 = new ComponentName(getInstance(), BinderWidget.class);
            int nComponentEnabledState2 = rPackageManager.getComponentEnabledSetting(rComponentName2);
            ComponentName rComponentName3 = new ComponentName(getInstance(), AndroidPatchWidget.class);
            int nComponentEnabledState3 = rPackageManager.getComponentEnabledSetting(rComponentName3);
            if (bEnable) {
                if (nComponentEnabledState1 == SETTINGS_SORTBY_STATUS || nComponentEnabledState2 == SETTINGS_SORTBY_STATUS) {
                    rPackageManager.setComponentEnabledSetting(rComponentName1, TITLE_LIST_ITEM, TITLE_LIST_ITEM);
                    rPackageManager.setComponentEnabledSetting(rComponentName2, TITLE_LIST_ITEM, TITLE_LIST_ITEM);
                    rPackageManager.setComponentEnabledSetting(rComponentName3, TITLE_LIST_ITEM, TITLE_LIST_ITEM);
                }
            } else if (nComponentEnabledState1 == TITLE_LIST_ITEM || nComponentEnabledState2 == TITLE_LIST_ITEM) {
                rPackageManager.setComponentEnabledSetting(rComponentName1, SETTINGS_SORTBY_STATUS, TITLE_LIST_ITEM);
                rPackageManager.setComponentEnabledSetting(rComponentName2, SETTINGS_SORTBY_STATUS, TITLE_LIST_ITEM);
                rPackageManager.setComponentEnabledSetting(rComponentName3, SETTINGS_SORTBY_STATUS, TITLE_LIST_ITEM);
            }
        }
    }
}
