package com.android.vending.billing.InAppBillingService.LUCK;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Build;
import android.os.Build.VERSION;
import android.provider.Settings.Secure;
import com.chelpus.Utils;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.ZipParameters;

public class LogCollector {
    public static final String LINE_SEPARATOR = "\n";
    private static final String LOG_TAG = LogCollector.class.getSimpleName();
    private ArrayList<String> mLastLogs;
    private String mPackageName = listAppsFragment.getInstance().getPackageName();
    private Pattern mPattern = Pattern.compile(String.format("(.*)E\\/AndroidRuntime\\(\\s*\\d+\\)\\:\\s*at\\s%s.*", new Object[]{this.mPackageName.replace(".", "\\.")}));
    private SharedPreferences mPrefs;

    public LogCollector() {
        listAppsFragment.getInstance();
        this.mPrefs = listAppsFragment.getInstance().getSharedPreferences("LogCollector", 0);
        this.mLastLogs = new ArrayList();
        this.mPrefs.edit().clear().commit();
    }

    private void collectLog(List<String> outLines, String format, String buffer, String[] filterSpecs, boolean noRoot) {
        outLines.clear();
        ArrayList<String> params = new ArrayList();
        if (format == null) {
            format = "time";
        }
        params.add("-v");
        params.add(format);
        if (buffer != null) {
            params.add("-b");
            params.add(buffer);
        }
        if (filterSpecs != null) {
            Collections.addAll(params, filterSpecs);
        }
        Process process;
        BufferedReader bufferedReader;
        String line;
        DataOutputStream os;
        DataInputStream errorGobbler;
        byte[] arrayErrorOfByte;
        try {
            ArrayList<String> commandLine;
            String[] cmdLine;
            if (!listAppsFragment.su || noRoot) {
                System.out.println("Collect logs no root.");
                commandLine = new ArrayList();
                commandLine.add("logcat");
                commandLine.add("-d");
                commandLine.addAll(params);
                cmdLine = (String[]) commandLine.toArray(new String[commandLine.size()]);
                if (listAppsFragment.basepath.startsWith(listAppsFragment.patchAct.getDir("sdcard", 0).getAbsolutePath())) {
                    cmdLine = new String[]{"logcat", "-d", "System.out:*", "*:S"};
                }
                process = Runtime.getRuntime().exec(cmdLine);
                bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
                while (true) {
                    line = bufferedReader.readLine();
                    if (line == null) {
                        break;
                    }
                    outLines.add(line);
                }
                process.destroy();
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            } else {
                process = Runtime.getRuntime().exec("su");
                os = new DataOutputStream(process.getOutputStream());
                errorGobbler = new DataInputStream(process.getErrorStream());
                arrayErrorOfByte = new byte[errorGobbler.available()];
                errorGobbler.read(arrayErrorOfByte);
                System.out.println(new String(arrayErrorOfByte));
                bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
                if (listAppsFragment.basepath.startsWith(listAppsFragment.patchAct.getDir("sdcard", 0).getAbsolutePath())) {
                    os.writeBytes("logcat -d System.out:* *:S\n");
                } else {
                    os.writeBytes("logcat -d -v time\n");
                }
                os.flush();
                os.close();
                while (true) {
                    try {
                        line = bufferedReader.readLine();
                        if (line == null) {
                            break;
                        }
                        outLines.add(line);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
                process.destroy();
                System.out.println("Collect logs no root.");
                outLines.add("\n\n\n*********************************** NO ROOT *******************************************************\n\n\n");
                commandLine = new ArrayList();
                commandLine.add("logcat");
                commandLine.add("-d");
                commandLine.addAll(params);
                cmdLine = (String[]) commandLine.toArray(new String[commandLine.size()]);
                if (listAppsFragment.basepath.startsWith(listAppsFragment.patchAct.getDir("sdcard", 0).getAbsolutePath())) {
                    cmdLine = new String[]{"logcat", "-d", "System.out:*", "*:S"};
                }
                process = Runtime.getRuntime().exec(cmdLine);
                bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
                while (true) {
                    line = bufferedReader.readLine();
                    if (line == null) {
                        break;
                    }
                    outLines.add(line);
                }
                process.destroy();
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            }
            if (outLines.size() == 0) {
                process = Runtime.getRuntime().exec("su");
                os = new DataOutputStream(process.getOutputStream());
                errorGobbler = new DataInputStream(process.getErrorStream());
                arrayErrorOfByte = new byte[errorGobbler.available()];
                errorGobbler.read(arrayErrorOfByte);
                System.out.println(new String(arrayErrorOfByte));
                bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
                if (listAppsFragment.basepath.startsWith(listAppsFragment.patchAct.getDir("sdcard", 0).getAbsolutePath())) {
                    os.writeBytes("logcat -d System.out:* *:S\n");
                } else {
                    os.writeBytes("logcat -d\n");
                }
                os.flush();
                os.close();
                while (true) {
                    line = bufferedReader.readLine();
                    if (line == null) {
                        break;
                    }
                    outLines.add(line);
                }
                process.destroy();
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            }
        } catch (IOException e2) {
            try {
                process = Runtime.getRuntime().exec("su");
                os = new DataOutputStream(process.getOutputStream());
                errorGobbler = new DataInputStream(process.getErrorStream());
                arrayErrorOfByte = new byte[errorGobbler.available()];
                errorGobbler.read(arrayErrorOfByte);
                System.out.println(new String(arrayErrorOfByte));
                bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
                if (listAppsFragment.basepath.startsWith(listAppsFragment.patchAct.getDir("sdcard", 0).getAbsolutePath())) {
                    os.writeBytes("logcat -d System.out:* *:S\n");
                } else {
                    os.writeBytes("logcat -d\n");
                }
                os.flush();
                os.close();
                line = BuildConfig.VERSION_NAME;
                while (true) {
                    line = bufferedReader.readLine();
                    if (line != null) {
                        outLines.add(line);
                    } else {
                        process.destroy();
                        return;
                    }
                }
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }

    public boolean hasForceCloseHappened() {
        String[] filterSpecs = new String[]{"*:E"};
        ArrayList<String> lines = new ArrayList();
        if (listAppsFragment.su) {
            collectLog(lines, "time", null, filterSpecs, false);
        } else {
            collectLog(lines, "time", null, filterSpecs, true);
        }
        if (lines.size() > 0) {
            boolean forceClosedSinceLastCheck = false;
            boolean error_found = false;
            Iterator it = lines.iterator();
            while (it.hasNext()) {
                Matcher matcher = this.mPattern.matcher((String) it.next());
                boolean isMyStackTrace = matcher.matches();
                SharedPreferences prefs = this.mPrefs;
                if (isMyStackTrace) {
                    error_found = true;
                    String timestamp = matcher.group(1);
                    if (!prefs.getBoolean(timestamp, false)) {
                        forceClosedSinceLastCheck = true;
                        prefs.edit().putBoolean(timestamp, true).commit();
                    }
                }
            }
            if (forceClosedSinceLastCheck || error_found) {
                return forceClosedSinceLastCheck;
            }
            System.out.println("Clear all FC logcat prefs...");
            this.mPrefs.edit().clear().commit();
            return forceClosedSinceLastCheck;
        }
        System.out.println("Clear all FC logcat prefs...");
        this.mPrefs.edit().clear().commit();
        return false;
    }

    private String collectPhoneInfo() {
        return String.format("Carrier:%s\nModel:%s\nFirmware:%s\n", new Object[]{Build.BRAND, Build.MODEL, VERSION.RELEASE});
    }

    public boolean collect(Context context, boolean writeToFile) {
        IOException e;
        ArrayList<String> lines = this.mLastLogs;
        if (listAppsFragment.su) {
            collectLog(lines, null, null, null, false);
        } else {
            collectLog(lines, null, null, null, true);
        }
        if (writeToFile && lines.size() > 0) {
            String preface = BuildConfig.VERSION_NAME;
            try {
                preface = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
            } catch (NameNotFoundException e2) {
                e2.printStackTrace();
            }
            StringBuilder sb = new StringBuilder("Lucky Patcher " + preface).append(LINE_SEPARATOR);
            String phoneInfo = collectPhoneInfo();
            sb.append(LINE_SEPARATOR).append(phoneInfo);
            while (true) {
                try {
                    break;
                } catch (ConcurrentModificationException e3) {
                    System.out.println("LuckyPatcher (LogCollector): try again collect log.");
                    sb = new StringBuilder(preface).append(LINE_SEPARATOR);
                    sb.append(LINE_SEPARATOR).append(phoneInfo);
                }
            }
            Iterator it = lines.iterator();
            while (it.hasNext()) {
                sb.append(LINE_SEPARATOR).append((String) it.next());
            }
            String content = sb.toString();
            File logfile = new File("abrakakdabra");
            try {
                File logfile2 = new File(listAppsFragment.basepath + "/Log/log.txt");
                try {
                    new File(listAppsFragment.basepath + "/Log").mkdirs();
                    if (logfile2.exists()) {
                        logfile2.delete();
                    }
                    if (new File(listAppsFragment.basepath + "/Log/log.zip").exists()) {
                        new File(listAppsFragment.basepath + "/Log/log.zip").delete();
                    }
                    logfile2.createNewFile();
                    new FileOutputStream(logfile2).write(content.getBytes());
                } catch (IOException e4) {
                    e = e4;
                    logfile = logfile2;
                    e.printStackTrace();
                    if (lines.size() > 0) {
                        return false;
                    }
                    return true;
                }
            } catch (IOException e5) {
                e = e5;
                e.printStackTrace();
                if (lines.size() > 0) {
                    return true;
                }
                return false;
            }
        }
        if (lines.size() > 0) {
            return true;
        }
        return false;
    }

    public void sendLog(Context context, String email, String subject, String preface) {
        File logfile = new File(listAppsFragment.basepath + "/Log/log.txt");
        Uri uri = Uri.parse("file://");
        try {
            ZipFile zip = new ZipFile(listAppsFragment.basepath + "/Log/log.zip");
            ZipParameters parameters = new ZipParameters();
            parameters.setCompressionMethod(8);
            parameters.setCompressionLevel(5);
            zip.addFile(logfile, parameters);
            uri = Uri.parse("file://" + listAppsFragment.basepath + "/Log/log.zip");
        } catch (ZipException e) {
            e.printStackTrace();
        } catch (Exception e2) {
        }
        String dalvikvm_file = "not found";
        String content = BuildConfig.VERSION_NAME;
        if (Utils.exists("/system/bin/dalvikvm")) {
            dalvikvm_file = "/system/bin/dalvikvm";
        }
        if (Utils.exists("/system/bin/dalvikvm32")) {
            dalvikvm_file = "/system/bin/dalvikvm32";
        }
        if (listAppsFragment.basepath.startsWith(listAppsFragment.patchAct.getDir("sdcard", 0).getAbsolutePath())) {
            content = "Email contain log.txt with bugs by Lucky Patcher! Please describe the Problem and tap to send email.\n\n\nRoot:" + listAppsFragment.su + LINE_SEPARATOR + "Runtime: " + Utils.getCurrentRuntimeValue() + LINE_SEPARATOR + "DeviceID: " + Secure.getString(listAppsFragment.getInstance().getContentResolver(), "android_id") + LINE_SEPARATOR + Build.BOARD + LINE_SEPARATOR + Build.BRAND + LINE_SEPARATOR + Build.CPU_ABI + LINE_SEPARATOR + Build.DEVICE + LINE_SEPARATOR + Build.DISPLAY + LINE_SEPARATOR + Build.FINGERPRINT + LINE_SEPARATOR + "Lucky Patcher ver: " + listAppsFragment.version + LINE_SEPARATOR + "LP files directory: " + listAppsFragment.basepath + LINE_SEPARATOR + "dalvikvm file:" + dalvikvm_file + "\n\n" + content;
        } else {
            content = "Email contain log.txt with bugs by Lucky Patcher! Please describe the Problem and tap to send email.\n\n\nRoot:" + listAppsFragment.su + LINE_SEPARATOR + "Runtime: " + Utils.getCurrentRuntimeValue() + LINE_SEPARATOR + "DeviceID: " + Secure.getString(listAppsFragment.getInstance().getContentResolver(), "android_id") + LINE_SEPARATOR + Build.BOARD + LINE_SEPARATOR + Build.BRAND + LINE_SEPARATOR + Build.CPU_ABI + LINE_SEPARATOR + Build.DEVICE + LINE_SEPARATOR + Build.DISPLAY + LINE_SEPARATOR + Build.FINGERPRINT + LINE_SEPARATOR + "Lucky Patcher ver: " + listAppsFragment.version + LINE_SEPARATOR + "LP files directory: " + listAppsFragment.basepath + LINE_SEPARATOR + "dalvikvm file:" + dalvikvm_file + "\n\n";
        }
        try {
            for (Entry<String, String> entry : System.getenv().entrySet()) {
                content = content + LINE_SEPARATOR + ((String) entry.getKey()) + ":" + ((String) entry.getValue());
            }
        } catch (Exception e3) {
            e3.printStackTrace();
        }
        Intent intent = new Intent("android.intent.action.SEND");
        intent.setType("text/plain");
        intent.putExtra("android.intent.extra.EMAIL", new String[]{email});
        intent.putExtra("android.intent.extra.SUBJECT", subject);
        intent.putExtra("android.intent.extra.TEXT", content);
        intent.putExtra("android.intent.extra.STREAM", uri);
        try {
            listAppsFragment.patchAct.startActivity(Intent.createChooser(intent, "Send mail"));
        } catch (RuntimeException e4) {
            e4.printStackTrace();
        }
    }
}
