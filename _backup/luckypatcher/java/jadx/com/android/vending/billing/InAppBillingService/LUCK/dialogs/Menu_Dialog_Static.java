package com.android.vending.billing.InAppBillingService.LUCK.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import com.android.vending.billing.InAppBillingService.LUCK.AlertDlg;
import com.android.vending.billing.InAppBillingService.LUCK.BuildConfig;
import com.android.vending.billing.InAppBillingService.LUCK.C0149R;
import com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment;

public class Menu_Dialog_Static {
    Dialog dialog = null;

    class C02101 implements OnClickListener {
        C02101() {
        }

        public void onClick(DialogInterface dialog, int which) {
            listAppsFragment.frag.runId(C0149R.string.set_switchers_def);
        }
    }

    class C02112 implements OnItemClickListener {
        C02112() {
        }

        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
            try {
                listAppsFragment.frag.runId(((Integer) listAppsFragment.adapt.getItem(position)).intValue());
                listAppsFragment.adapt.notifyDataSetChanged();
            } catch (Exception e) {
                System.out.println("LuckyAppManager (ContextMenu): Error open! " + e);
                e.printStackTrace();
            }
        }
    }

    class C02123 implements OnCancelListener {
        C02123() {
        }

        public void onCancel(DialogInterface dialog) {
            listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
            System.out.println(listAppsFragment.rebuldApk);
        }
    }

    public void showDialog() {
        if (this.dialog == null) {
            this.dialog = onCreateDialog();
        }
        if (this.dialog != null) {
            this.dialog.show();
        }
    }

    public Dialog onCreateDialog() {
        try {
            System.out.println("Menu Dialog create.");
            if (listAppsFragment.frag == null || listAppsFragment.frag.getContext() == null) {
                dismiss();
            }
            AlertDlg builder5 = new AlertDlg(listAppsFragment.frag.getContext());
            builder5.setPositiveButton((int) C0149R.string.set_switchers_def, new C02101());
            if (listAppsFragment.adapt != null) {
                listAppsFragment.adapt.setNotifyOnChange(true);
                builder5.setAdapterNotClose(true);
                builder5.setAdapter(listAppsFragment.adapt, new C02112());
            }
            builder5.setOnCancelListener(new C02123());
            return builder5.create();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void dismiss() {
        if (this.dialog != null) {
            this.dialog.dismiss();
            this.dialog = null;
        }
    }
}
