package com.android.vending.billing.InAppBillingService.LUCK;

public class SearchItem {
    public byte[] origByte;
    public int[] origMask;
    public byte[] repByte;
    public boolean result = false;

    public SearchItem(byte[] orig, int[] mask) {
        this.origByte = new byte[orig.length];
        this.origByte = orig;
        this.origMask = new int[mask.length];
        this.origMask = mask;
    }
}
