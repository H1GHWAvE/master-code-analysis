package com.android.vending.billing.InAppBillingService.LUCK;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface.OnCancelListener;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.view.MotionEventCompat;
import android.view.View;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import org.tukaani.xz.LZMA2Options;
import pxb.android.axml.AxmlParser;

public class ProgressDlg {
    public static int max = 0;
    public Context context = null;
    public Dialog dialog = null;
    public String format = "%1d/%2d";
    public boolean incrementStyle = false;
    public View root = null;

    class C01401 implements OnGlobalLayoutListener {
        int current_width = 0;
        int displayWidth = 0;
        int orient = MotionEventCompat.ACTION_MASK;
        int setForSize = 0;

        C01401() {
        }

        public void onGlobalLayout() {
            if (ProgressDlg.this.incrementStyle) {
                Window w = ProgressDlg.this.dialog.getWindow();
                this.current_width = w.getDecorView().getWidth();
                int display_height = listAppsFragment.getRes().getDisplayMetrics().heightPixels;
                int display_width = listAppsFragment.getRes().getDisplayMetrics().widthPixels;
                if (display_height > display_width) {
                    this.orient = 0;
                } else {
                    this.orient = 1;
                }
                if (this.displayWidth == 0 || this.displayWidth != display_width) {
                    this.setForSize = 0;
                    this.displayWidth = display_width;
                }
                if (this.setForSize == 0 || this.current_width != this.setForSize) {
                    w.setGravity(17);
                    LayoutParams lp = w.getAttributes();
                    int max_width = (int) (((double) display_width) * 0.98d);
                    int min_width = (int) (((double) display_width) * 0.6d);
                    System.out.println("wight:" + this.current_width);
                    switch (this.orient) {
                        case LZMA2Options.MODE_UNCOMPRESSED /*0*/:
                            if (display_width >= 900) {
                                if (this.current_width < max_width) {
                                    if (this.current_width >= min_width) {
                                        lp.width = this.current_width;
                                        break;
                                    }
                                    lp.width = min_width;
                                    this.setForSize = min_width;
                                    break;
                                }
                                lp.width = max_width;
                                this.setForSize = max_width;
                                break;
                            } else if (this.current_width < min_width) {
                                lp.width = min_width;
                                this.setForSize = min_width;
                                break;
                            } else {
                                lp.width = max_width;
                                this.setForSize = max_width;
                                break;
                            }
                        case AxmlParser.START_FILE /*1*/:
                            if (display_width >= 900) {
                                if (this.current_width >= min_width) {
                                    lp.width = this.current_width;
                                    break;
                                }
                                lp.width = min_width;
                                this.setForSize = min_width;
                                break;
                            } else if (this.current_width < min_width) {
                                lp.width = min_width;
                                this.setForSize = min_width;
                                break;
                            }
                            break;
                    }
                    w.setAttributes(lp);
                }
            }
        }
    }

    class C01456 implements Runnable {
        C01456() {
        }

        public void run() {
            ((ProgressBar) ProgressDlg.this.dialog.findViewById(C0149R.id.progressBarIncrement)).setVisibility(0);
            ((LinearLayout) ProgressDlg.this.dialog.findViewById(C0149R.id.increment_info_layout)).setVisibility(0);
            TextView inc_info = (TextView) ProgressDlg.this.dialog.findViewById(C0149R.id.increment_text_progress);
        }
    }

    class C01467 implements Runnable {
        C01467() {
        }

        public void run() {
            ((ProgressBar) ProgressDlg.this.dialog.findViewById(C0149R.id.progressBarIncrement)).setVisibility(8);
            ((LinearLayout) ProgressDlg.this.dialog.findViewById(C0149R.id.increment_info_layout)).setVisibility(8);
        }
    }

    public ProgressDlg(Context context) {
        this.context = context;
        this.dialog = new Dialog(context);
        this.dialog.requestWindowFeature(1);
        this.dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        this.dialog.getWindow().getDecorView().getViewTreeObserver().addOnGlobalLayoutListener(new C01401());
        this.dialog.setContentView(C0149R.layout.progress_dialog);
        this.dialog.setCancelable(false);
        ProgressBar progressInc = (ProgressBar) this.dialog.findViewById(C0149R.id.progressBarIncrement);
        progressInc.setMax(0);
        progressInc.setProgress(0);
        progressInc.setSecondaryProgress(0);
    }

    public ProgressDlg setIcon(final int id) {
        try {
            ((Activity) this.context).runOnUiThread(new Runnable() {
                public void run() {
                    ((LinearLayout) ProgressDlg.this.dialog.findViewById(C0149R.id.title_layout)).setVisibility(0);
                    ImageView icon = (ImageView) ProgressDlg.this.dialog.findViewById(C0149R.id.title_icon);
                    icon.setVisibility(0);
                    icon.setImageDrawable(ProgressDlg.this.context.getResources().getDrawable(id));
                    ((TextView) ProgressDlg.this.dialog.findViewById(C0149R.id.line0)).setVisibility(0);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this;
    }

    public ProgressDlg setTitle(final String str) {
        try {
            ((Activity) this.context).runOnUiThread(new Runnable() {
                public void run() {
                    ((LinearLayout) ProgressDlg.this.dialog.findViewById(C0149R.id.title_layout)).setVisibility(0);
                    ((TextView) ProgressDlg.this.dialog.findViewById(C0149R.id.title_text)).setText(str);
                    ((TextView) ProgressDlg.this.dialog.findViewById(C0149R.id.line0)).setVisibility(0);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this;
    }

    public ProgressDlg setTitle(final int str) {
        try {
            ((Activity) this.context).runOnUiThread(new Runnable() {
                public void run() {
                    ((LinearLayout) ProgressDlg.this.dialog.findViewById(C0149R.id.title_layout)).setVisibility(0);
                    ((TextView) ProgressDlg.this.dialog.findViewById(C0149R.id.title_text)).setText(str);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this;
    }

    public ProgressDlg setMessage(final String message) {
        try {
            ((Activity) this.context).runOnUiThread(new Runnable() {
                public void run() {
                    ((TextView) ProgressDlg.this.dialog.findViewById(C0149R.id.dialog_message)).setText(message);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this;
    }

    public ProgressDlg setCancelable(boolean cancel) {
        this.dialog.setCancelable(cancel);
        return this;
    }

    public ProgressDlg setOnCancelListener(OnCancelListener cancel_listner) {
        this.dialog.setOnCancelListener(cancel_listner);
        return this;
    }

    public ProgressDlg setIncrementStyle() {
        this.incrementStyle = true;
        try {
            ((Activity) this.context).runOnUiThread(new C01456());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this;
    }

    public ProgressDlg setDefaultStyle() {
        this.incrementStyle = false;
        try {
            ((Activity) this.context).runOnUiThread(new C01467());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this;
    }

    public ProgressDlg setMax(final int max) {
        try {
            max = max;
            ProgressBar progressInc = (ProgressBar) this.dialog.findViewById(C0149R.id.progressBarIncrement);
            progressInc.setVisibility(0);
            progressInc.setMax(max);
            ((Activity) this.context).runOnUiThread(new Runnable() {
                public void run() {
                    ProgressBar progressInc = (ProgressBar) ProgressDlg.this.dialog.findViewById(C0149R.id.progressBarIncrement);
                    ((TextView) ProgressDlg.this.dialog.findViewById(C0149R.id.increment_text_progress)).setText(String.format(ProgressDlg.this.format, new Object[]{Integer.valueOf(progressInc.getProgress()), Integer.valueOf(max)}));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this;
    }

    public ProgressDlg setProgress(final int progress) {
        try {
            ((Activity) this.context).runOnUiThread(new Runnable() {
                public void run() {
                    ProgressBar progressInc = (ProgressBar) ProgressDlg.this.dialog.findViewById(C0149R.id.progressBarIncrement);
                    progressInc.setVisibility(0);
                    progressInc.setProgress(progress);
                    ((TextView) ProgressDlg.this.dialog.findViewById(C0149R.id.increment_text_progress)).setText(String.format(ProgressDlg.this.format, new Object[]{Integer.valueOf(progress), Integer.valueOf(ProgressDlg.max)}));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this;
    }

    public boolean isShowing() {
        return this.dialog.isShowing();
    }

    public void setProgressNumberFormat(String format1) {
        try {
            this.format = format1;
            ((Activity) this.context).runOnUiThread(new Runnable() {
                public void run() {
                    ProgressBar progressInc = (ProgressBar) ProgressDlg.this.dialog.findViewById(C0149R.id.progressBarIncrement);
                    ((TextView) ProgressDlg.this.dialog.findViewById(C0149R.id.increment_text_progress)).setText(String.format(ProgressDlg.this.format, new Object[]{Integer.valueOf(progressInc.getProgress()), Integer.valueOf(ProgressDlg.max)}));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Dialog create() {
        return this.dialog;
    }
}
