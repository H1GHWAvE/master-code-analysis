package com.android.vending.billing.InAppBillingService.LUCK;

public class TypesItem {
    public byte[] Type = null;
    public boolean bits32 = false;
    public boolean found_id_type = false;
    public byte[] id_type = null;
    public String type = null;

    public TypesItem(String type) {
        this.type = type;
        this.Type = new byte[4];
        this.bits32 = false;
        this.id_type = new byte[2];
    }
}
