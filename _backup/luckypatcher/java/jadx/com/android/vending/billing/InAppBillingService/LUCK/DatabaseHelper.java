package com.android.vending.billing.InAppBillingService.LUCK;

import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import com.chelpus.Common;
import com.chelpus.Utils;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;
import org.tukaani.xz.LZMA2Options;

public class DatabaseHelper extends SQLiteOpenHelper {
    static final String ads = "ads";
    static final String billing = "billing";
    static final String boot_ads = "boot_ads";
    static final String boot_custom = "boot_custom";
    static final String boot_lvl = "boot_lvl";
    static final String boot_manual = "boot_manual";
    public static Context contextdb = null;
    static final String custom = "custom";
    public static SQLiteDatabase db = null;
    static final String dbName = "PackagesDB";
    public static boolean getPackage = false;
    static final String hidden = "hidden";
    static final String icon = "icon";
    static final String lvl = "lvl";
    static final String modified = "modified";
    static final String odex = "odex";
    static final String packagesTable = "Packages";
    static final String pkgLabel = "pkgLabel";
    static final String pkgName = "pkgName";
    public static boolean savePackage = false;
    static final String statusi = "statusi";
    static final String stored = "stored";
    static final String storepref = "storepref";
    static final String system = "system";
    static final String updatetime = "updatetime";

    public DatabaseHelper(Context context) {
        super(context, dbName, null, 42);
        contextdb = context;
        try {
            db = getWritableDatabase();
            System.out.println("SQLite base version is " + db.getVersion());
            if (db.getVersion() != 42) {
                System.out.println("SQL delete and recreate.");
                db.execSQL("DROP TABLE IF EXISTS Packages");
                onCreate(db);
            }
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE Packages (pkgName TEXT PRIMARY KEY, pkgLabel TEXT, stored Integer, storepref Integer, hidden Integer, statusi TEXT, boot_ads Integer, boot_lvl Integer, boot_custom Integer, boot_manual Integer, custom Integer, lvl Integer, ads Integer, modified Integer, system Integer, odex Integer, icon BLOB, updatetime Integer, billing Integer );");
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS Packages");
        onCreate(db);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public ArrayList<PkgListItem> getPackage(boolean All, boolean skipIconRead) {
        ArrayList<PkgListItem> pat = new ArrayList();
        pat.clear();
        PackageManager pkg = listAppsFragment.getPkgMng();
        getPackage = true;
        try {
            Cursor c = db.query(packagesTable, new String[]{pkgName, pkgLabel, stored, storepref, hidden, statusi, boot_ads, boot_lvl, boot_custom, boot_manual, custom, lvl, ads, modified, system, odex, icon, updatetime, billing}, null, null, null, null, null);
            c.moveToFirst();
            do {
                try {
                    ApplicationInfo appInfo;
                    boolean enable;
                    boolean sd_on;
                    String pkgLabel;
                    int stored;
                    int storepref;
                    int hidden;
                    String statusi;
                    int boot_ads;
                    int boot_lvl;
                    int boot_custom;
                    int boot_manual;
                    int custom;
                    int lvl;
                    int ads;
                    int modified;
                    int system;
                    int odex;
                    int billing;
                    Bitmap icon;
                    PkgListItem item;
                    String pkgName = c.getString(c.getColumnIndexOrThrow(pkgName));
                    try {
                        appInfo = pkg.getPackageInfo(pkgName, 0).applicationInfo;
                        enable = appInfo.enabled;
                        sd_on = (appInfo.flags & AccessibilityEventCompat.TYPE_GESTURE_DETECTION_START) == 262144;
                        pkgLabel = c.getString(c.getColumnIndex(pkgLabel));
                        stored = c.getInt(c.getColumnIndex(stored));
                        storepref = c.getInt(c.getColumnIndex(storepref));
                        hidden = c.getInt(c.getColumnIndex(hidden));
                        statusi = c.getString(c.getColumnIndex(statusi));
                        boot_ads = c.getInt(c.getColumnIndex(boot_ads));
                        boot_lvl = c.getInt(c.getColumnIndex(boot_lvl));
                        boot_custom = c.getInt(c.getColumnIndex(boot_custom));
                        boot_manual = c.getInt(c.getColumnIndex(boot_manual));
                        custom = c.getInt(c.getColumnIndex(custom));
                        lvl = c.getInt(c.getColumnIndex(lvl));
                        ads = c.getInt(c.getColumnIndex(ads));
                        modified = c.getInt(c.getColumnIndex(modified));
                        system = c.getInt(c.getColumnIndex(system));
                        odex = c.getInt(c.getColumnIndex(odex));
                        billing = c.getInt(c.getColumnIndex(billing));
                        icon = null;
                        if (!skipIconRead) {
                            byte[] imageByteArray = c.getBlob(c.getColumnIndexOrThrow(icon));
                            if (imageByteArray != null) {
                                icon = BitmapFactory.decodeStream(new ByteArrayInputStream(imageByteArray));
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    } catch (OutOfMemoryError e2) {
                        icon = null;
                    } catch (NameNotFoundException e3) {
                    } catch (IllegalArgumentException e4) {
                    }
                    int updatetime = c.getInt(c.getColumnIndex(updatetime));
                    if (stored != 0) {
                        if (Math.abs(((int) (System.currentTimeMillis() / 1000)) - updatetime) < 86400 * listAppsFragment.days) {
                            stored = 0;
                            db.execSQL("UPDATE Packages SET stored=0 WHERE pkgName='" + pkgName + "'");
                        }
                        item = new PkgListItem(listAppsFragment.getInstance(), pkgName, pkgLabel, stored, storepref, hidden, statusi, boot_ads, boot_lvl, boot_custom, boot_manual, custom, lvl, ads, modified, system, odex, icon, updatetime, billing, enable, sd_on, skipIconRead);
                    } else if (Math.abs(((int) (System.currentTimeMillis() / 1000)) - updatetime) > 86400 * listAppsFragment.days) {
                        item = new PkgListItem(listAppsFragment.getInstance(), pkgName, listAppsFragment.days, skipIconRead);
                        savePackage(item);
                    } else {
                        item = new PkgListItem(listAppsFragment.getInstance(), pkgName, pkgLabel, stored, storepref, hidden, statusi, boot_ads, boot_lvl, boot_custom, boot_manual, custom, lvl, ads, modified, system, odex, icon, updatetime, billing, enable, sd_on, skipIconRead);
                    }
                    boolean sysshow = listAppsFragment.getConfig().getBoolean("systemapp", false);
                    if (All) {
                        if (!pkgName.equals(Common.ANDROID_PKG) && !pkgName.equals(contextdb.getPackageName())) {
                            if (listAppsFragment.advancedFilter != 0) {
                                switch (listAppsFragment.advancedFilter) {
                                    case listAppsFragment.advanced_filter_for_apps_move2sd /*1929*/:
                                        if (item.system || Utils.isInstalledOnSdCard(item.pkgName)) {
                                            throw new IllegalArgumentException("package scan error");
                                        }
                                    case listAppsFragment.advanced_filter_for_user_apps /*1930*/:
                                        if (item.system) {
                                            throw new IllegalArgumentException("package scan error");
                                        }
                                        break;
                                    case listAppsFragment.advanced_filter_apps_with_internet /*1931*/:
                                        if (pkg.checkPermission("android.permission.INTERNET", item.pkgName) == -1) {
                                            throw new IllegalArgumentException("package scan error");
                                        }
                                        if (pkg.getComponentEnabledSetting(new ComponentName(item.pkgName, "android.permission.INTERNET")) == 2) {
                                            throw new IllegalArgumentException("package scan error");
                                        }
                                        break;
                                    case listAppsFragment.advanced_filter_apps_with_without_internet /*1932*/:
                                        if (pkg.checkPermission("android.permission.INTERNET", item.pkgName) == -1) {
                                            throw new IllegalArgumentException("package scan error");
                                        }
                                        if (pkg.getComponentEnabledSetting(new ComponentName(item.pkgName, "android.permission.INTERNET")) != 1) {
                                            if (pkg.getComponentEnabledSetting(new ComponentName(item.pkgName, "android.permission.INTERNET")) == 0) {
                                            }
                                        }
                                        throw new IllegalArgumentException("package scan error");
                                    case C0149R.id.nofilter:
                                        listAppsFragment.advancedFilter = 0;
                                        break;
                                    case C0149R.id.filter0:
                                        if (Math.abs(((int) (System.currentTimeMillis() / 1000)) - updatetime) > 86400) {
                                            throw new IllegalArgumentException("package scan error");
                                        }
                                        break;
                                    case C0149R.id.filter1:
                                        if (!item.lvl) {
                                            throw new IllegalArgumentException("package scan error");
                                        }
                                        break;
                                    case C0149R.id.filter2:
                                        if (!item.ads) {
                                            throw new IllegalArgumentException("package scan error");
                                        }
                                        break;
                                    case C0149R.id.filter11:
                                        if (!item.custom) {
                                            throw new IllegalArgumentException("package scan error");
                                        }
                                        break;
                                    case C0149R.id.filter12:
                                        if (!item.modified) {
                                            throw new IllegalArgumentException("package scan error");
                                        }
                                        break;
                                    case C0149R.id.filter13:
                                        if (!item.odex) {
                                            throw new IllegalArgumentException("package scan error");
                                        }
                                        break;
                                    case C0149R.id.filter4:
                                        if (!item.billing) {
                                            throw new IllegalArgumentException("package scan error");
                                        }
                                        break;
                                    case C0149R.id.filter5:
                                        if (!appInfo.sourceDir.startsWith("/data/")) {
                                            throw new IllegalArgumentException("package scan error");
                                        }
                                        break;
                                    case C0149R.id.filter6:
                                        if (!appInfo.sourceDir.startsWith("/mnt/")) {
                                            throw new IllegalArgumentException("package scan error");
                                        }
                                        break;
                                    case C0149R.id.filter10:
                                        if (item.enable) {
                                            throw new IllegalArgumentException("package scan error");
                                        }
                                        break;
                                    case C0149R.id.filter7:
                                        if (!item.system) {
                                            throw new IllegalArgumentException("package scan error");
                                        }
                                        break;
                                    case C0149R.id.filter8:
                                        if (!(item.system && (appInfo.sourceDir.startsWith("/data/") || appInfo.sourceDir.startsWith("/mnt/")))) {
                                            throw new IllegalArgumentException("package scan error");
                                        }
                                    case C0149R.id.filter9:
                                        String[] perm = listAppsFragment.getPkgMng().getPackageInfo(item.pkgName, LZMA2Options.DICT_SIZE_MIN).requestedPermissions;
                                        boolean found = false;
                                        if (perm != null) {
                                            for (String p : perm) {
                                                if (p.toUpperCase().contains("SEND_SMS") || p.contains("CALL_PHONE")) {
                                                    found = true;
                                                }
                                            }
                                        }
                                        if (!found) {
                                            throw new IllegalArgumentException("package scan error");
                                        }
                                        break;
                                }
                            }
                        }
                        throw new IllegalArgumentException("package scan error");
                    } else if (pkgName.equals(Common.ANDROID_PKG) || pkgName.equals(listAppsFragment.class.getPackage().getName())) {
                        throw new IllegalArgumentException("package scan error");
                    } else if (item.system && !sysshow && !item.custom) {
                        throw new IllegalArgumentException("package scan filter");
                    } else if (!listAppsFragment.getConfig().getBoolean("lvlapp", true) && item.lvl && !item.ads && !item.custom && !item.odex && !item.modified) {
                        throw new IllegalArgumentException("package scan error");
                    } else if (!listAppsFragment.getConfig().getBoolean("adsapp", true) && item.ads && !item.lvl && !item.custom && !item.odex && !item.modified) {
                        throw new IllegalArgumentException("package scan error");
                    } else if (!listAppsFragment.getConfig().getBoolean("lvlapp", true) && !listAppsFragment.getConfig().getBoolean("adsapp", true) && item.lvl && item.ads && !item.custom && !item.odex && !item.modified) {
                        throw new IllegalArgumentException("package scan error");
                    } else if (!listAppsFragment.getConfig().getBoolean("noneapp", true) && stored == 6 && !item.odex && !item.modified) {
                        throw new IllegalArgumentException("package scan error");
                    } else if (!listAppsFragment.getConfig().getBoolean("customapp", true) && item.custom && !item.odex && !item.modified) {
                        throw new IllegalArgumentException("package scan error");
                    } else if (!listAppsFragment.getConfig().getBoolean("fixedapp", true) && item.odex && !item.modified) {
                        throw new IllegalArgumentException("package scan error");
                    } else if (!listAppsFragment.getConfig().getBoolean("modifapp", true) && !item.odex && item.modified) {
                        throw new IllegalArgumentException("package scan error");
                    } else if (!(listAppsFragment.getConfig().getBoolean("fixedapp", true) || listAppsFragment.getConfig().getBoolean("modifapp", true) || (!item.odex && !item.modified))) {
                        throw new IllegalArgumentException("package scan error");
                    }
                    pat.add(item);
                } catch (Exception e5) {
                }
                try {
                } catch (Exception e6) {
                    c.close();
                }
            } while (c.moveToNext());
            c.close();
            getPackage = false;
        } catch (Exception e7) {
            getPackage = false;
            System.out.println("LuckyPatcher-Error: getPackage " + e7);
        }
        return pat;
    }

    public void savePackage(PkgListItem savedObject) throws SQLiteException {
        try {
            savePackage = true;
            ContentValues cv = new ContentValues();
            cv.put(pkgName, savedObject.pkgName);
            cv.put(pkgLabel, savedObject.name);
            cv.put(stored, Integer.valueOf(savedObject.stored));
            cv.put(storepref, Integer.valueOf(savedObject.storepref));
            cv.put(hidden, Boolean.valueOf(savedObject.hidden));
            cv.put(statusi, savedObject.statusi);
            cv.put(boot_ads, Boolean.valueOf(savedObject.boot_ads));
            cv.put(boot_lvl, Boolean.valueOf(savedObject.boot_lvl));
            cv.put(boot_custom, Boolean.valueOf(savedObject.boot_custom));
            cv.put(boot_manual, Boolean.valueOf(savedObject.boot_manual));
            cv.put(custom, Boolean.valueOf(savedObject.custom));
            cv.put(lvl, Boolean.valueOf(savedObject.lvl));
            cv.put(ads, Boolean.valueOf(savedObject.ads));
            cv.put(modified, Boolean.valueOf(savedObject.modified));
            cv.put(system, Boolean.valueOf(savedObject.system));
            cv.put(odex, Boolean.valueOf(savedObject.odex));
            cv.put(updatetime, Integer.valueOf(savedObject.updatetime));
            cv.put(billing, Boolean.valueOf(savedObject.billing));
            String pkgdir = listAppsFragment.getPkgMng().getApplicationInfo(savedObject.pkgName, 0).sourceDir;
            Bitmap image = null;
            try {
                if (savedObject.icon != null) {
                    image = ((BitmapDrawable) savedObject.icon).getBitmap();
                }
                if (image != null) {
                    ByteArrayOutputStream out = new ByteArrayOutputStream();
                    image.compress(CompressFormat.PNG, 100, out);
                    cv.put(icon, out.toByteArray());
                }
            } catch (OutOfMemoryError e) {
            }
            try {
                db.insertOrThrow(packagesTable, pkgName, cv);
            } catch (Exception e2) {
                db.replace(packagesTable, null, cv);
            }
            savePackage = false;
            savePackage = false;
        } catch (Exception e3) {
            savePackage = false;
            System.out.println("LuckyPatcher-Error: savePackage " + e3);
        }
    }

    public void deletePackage(PkgListItem deleteObject) {
        try {
            db.delete(packagesTable, "pkgName = '" + deleteObject.pkgName + "'", null);
        } catch (Exception e) {
            System.out.println("LuckyPatcher-Error: deletePackage " + e);
        }
    }

    public void deletePackage(String pkgName) {
        try {
            db.delete(packagesTable, "pkgName = '" + pkgName + "'", null);
        } catch (Exception e) {
            System.out.println("LuckyPatcher-Error: deletePackage " + e);
        }
    }

    public void updatePackage(List<PkgListItem> list) {
    }

    public boolean isOpen() {
        if (db.isOpen()) {
            return true;
        }
        return false;
    }
}
