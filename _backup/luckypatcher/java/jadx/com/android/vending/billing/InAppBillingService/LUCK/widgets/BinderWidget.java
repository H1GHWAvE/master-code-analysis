package com.android.vending.billing.InAppBillingService.LUCK.widgets;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.widget.RemoteViews;
import android.widget.Toast;
import com.android.vending.billing.InAppBillingService.LUCK.BindItem;
import com.android.vending.billing.InAppBillingService.LUCK.BinderActivity;
import com.android.vending.billing.InAppBillingService.LUCK.BuildConfig;
import com.android.vending.billing.InAppBillingService.LUCK.C0149R;
import com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment;
import com.chelpus.Utils;
import java.util.ArrayList;
import java.util.Iterator;
import net.lingala.zip4j.util.InternalZipConstants;

public class BinderWidget extends AppWidgetProvider {
    public static String ACTION_WIDGET_RECEIVER = "ActionReceiverWidgetBinder";
    public static String ACTION_WIDGET_RECEIVER_Updater = "ActionReceiverWidgetBinderUpdate";
    public static BinderWidget widget = null;

    public static void pushWidgetUpdate(Context context, RemoteViews remoteViews) {
        AppWidgetManager.getInstance(context).updateAppWidget(new ComponentName(context, BinderWidget.class), remoteViews);
    }

    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        for (int updateAppWidget : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, updateAppWidget);
        }
    }

    public void onDeleted(Context context, int[] appWidgetIds) {
        for (int deleteTitlePref : appWidgetIds) {
            BinderWidgetConfigureActivity.deleteTitlePref(context, deleteTitlePref);
        }
    }

    public void onEnabled(Context context) {
    }

    public void onDisabled(Context context) {
    }

    static void updateAppWidget(final Context context, AppWidgetManager appWidgetManager, final int appWidgetId) {
        listAppsFragment.init();
        Thread binder = new Thread(new Runnable() {
            public void run() {
                RemoteViews views = new RemoteViews(context.getPackageName(), C0149R.layout.binder_widget);
                Intent active = new Intent(context, BinderWidget.class);
                active.setAction(BinderWidget.ACTION_WIDGET_RECEIVER);
                active.putExtra("appWidgetId", appWidgetId);
                views.setOnClickPendingIntent(C0149R.id.button, PendingIntent.getBroadcast(context, appWidgetId, active, 0));
                views.setInt(C0149R.id.toggleButton_off, "setBackgroundResource", C0149R.drawable.switch_off);
                views.setTextColor(C0149R.id.appwidget_text, Color.parseColor("#AAAAAA"));
                views.setTextViewText(C0149R.id.appwidget_text, "wait");
                try {
                    AppWidgetManager.getInstance(context).updateAppWidget(appWidgetId, views);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (listAppsFragment.su) {
                    String binditem = BinderWidgetConfigureActivity.loadTitlePref(context, appWidgetId);
                    System.out.println(binditem);
                    BindItem item = new BindItem(binditem.replaceAll("~chelpus_disabled~", BuildConfig.VERSION_NAME));
                    if (item.TargetDir != null && !item.TargetDir.equals(BuildConfig.VERSION_NAME) && item.SourceDir != null && !item.SourceDir.equals(BuildConfig.VERSION_NAME)) {
                        String[] strings = item.TargetDir.split(InternalZipConstants.ZIP_FILE_SEPARATOR);
                        views.setTextViewText(C0149R.id.appwidget_text, strings[strings.length - 1]);
                        if (Utils.checkBind(item)) {
                            views.setTextColor(C0149R.id.appwidget_text, Color.parseColor("#00FF00"));
                            views.setInt(C0149R.id.toggleButton_off, "setBackgroundResource", C0149R.drawable.switch_on);
                        } else {
                            views.setTextColor(C0149R.id.appwidget_text, Color.parseColor("#FF0000"));
                            views.setInt(C0149R.id.toggleButton_off, "setBackgroundResource", C0149R.drawable.switch_off);
                        }
                        boolean found = false;
                        Iterator it = BinderActivity.getBindes(context).iterator();
                        while (it.hasNext()) {
                            BindItem it2 = (BindItem) it.next();
                            if (it2.SourceDir.replaceAll("~chelpus_disabled~", BuildConfig.VERSION_NAME).equals(item.SourceDir.replaceAll("~chelpus_disabled~", BuildConfig.VERSION_NAME)) && it2.TargetDir.replaceAll("~chelpus_disabled~", BuildConfig.VERSION_NAME).equals(item.TargetDir.replaceAll("~chelpus_disabled~", BuildConfig.VERSION_NAME))) {
                                it2.TargetDir = it2.TargetDir.replaceAll("~chelpus_disabled~", BuildConfig.VERSION_NAME);
                                it2.SourceDir = it2.SourceDir.replaceAll("~chelpus_disabled~", BuildConfig.VERSION_NAME);
                                found = true;
                            }
                        }
                        if (!found) {
                            if (listAppsFragment.su) {
                                Utils.run_all("umount -f '" + item.TargetDir.replaceAll("~chelpus_disabled~", BuildConfig.VERSION_NAME) + "'");
                                Utils.run_all("umount -l '" + item.TargetDir.replaceAll("~chelpus_disabled~", BuildConfig.VERSION_NAME) + "'");
                            } else {
                                Utils.cmd("umount '" + item.TargetDir.replaceAll("~chelpus_disabled~", BuildConfig.VERSION_NAME) + "'");
                            }
                            views.setInt(C0149R.id.toggleButton_off, "setBackgroundResource", C0149R.drawable.switch_off);
                            views.setTextColor(C0149R.id.appwidget_text, Color.parseColor("#AAAAAA"));
                            views.setTextViewText(C0149R.id.appwidget_text, "unknown bind");
                            try {
                                AppWidgetManager.getInstance(context).updateAppWidget(appWidgetId, views);
                            } catch (Exception e2) {
                                e2.printStackTrace();
                            }
                        }
                        try {
                            AppWidgetManager.getInstance(context).updateAppWidget(appWidgetId, views);
                            return;
                        } catch (Exception e22) {
                            e22.printStackTrace();
                            return;
                        }
                    }
                    return;
                }
                views.setInt(C0149R.id.toggleButton_off, "setBackgroundResource", C0149R.drawable.switch_off);
                views.setTextColor(C0149R.id.appwidget_text, Color.parseColor("#AAAAAA"));
                views.setTextViewText(C0149R.id.appwidget_text, "you need root access");
                try {
                    AppWidgetManager.getInstance(context).updateAppWidget(appWidgetId, views);
                } catch (Exception e222) {
                    e222.printStackTrace();
                }
            }
        });
        binder.setPriority(10);
        binder.start();
    }

    public void onReceive(final Context context, final Intent intent) {
        super.onReceive(context, intent);
        String action = intent.getAction();
        if (ACTION_WIDGET_RECEIVER.equals(action)) {
            listAppsFragment.init();
            if (!BinderWidgetConfigureActivity.loadTitlePref(context, intent.getIntExtra("appWidgetId", -1)).equals("NOT_SAVED_BIND")) {
                widget = this;
            }
            final Handler handler = new Handler();
            try {
                Thread turn_binder = new Thread(new Runnable() {
                    public void run() {
                        listAppsFragment.binder_process = true;
                        Utils.exitRoot();
                        int id = intent.getIntExtra("appWidgetId", -1);
                        if (id != -1 && !BinderWidgetConfigureActivity.loadTitlePref(context, id).equals("NOT_SAVED_BIND")) {
                            BindItem item = new BindItem(BinderWidgetConfigureActivity.loadTitlePref(context, id));
                            RemoteViews remoteViews = new RemoteViews(context.getPackageName(), C0149R.layout.binder_widget);
                            item.TargetDir = item.TargetDir.replaceAll("~chelpus_disabled~", BuildConfig.VERSION_NAME);
                            item.SourceDir = item.SourceDir.replaceAll("~chelpus_disabled~", BuildConfig.VERSION_NAME);
                            ArrayList<BindItem> bindes;
                            boolean found;
                            Iterator it;
                            BindItem it2;
                            if (Utils.checkBind(item)) {
                                item.TargetDir = item.TargetDir.replaceAll("~chelpus_disabled~", BuildConfig.VERSION_NAME);
                                item.SourceDir = item.SourceDir.replaceAll("~chelpus_disabled~", BuildConfig.VERSION_NAME);
                                bindes = BinderActivity.getBindes(context);
                                found = false;
                                it = bindes.iterator();
                                while (it.hasNext()) {
                                    it2 = (BindItem) it.next();
                                    if (it2.SourceDir.replaceAll("~chelpus_disabled~", BuildConfig.VERSION_NAME).equals(item.SourceDir.replaceAll("~chelpus_disabled~", BuildConfig.VERSION_NAME)) && it2.TargetDir.replaceAll("~chelpus_disabled~", BuildConfig.VERSION_NAME).equals(item.TargetDir.replaceAll("~chelpus_disabled~", BuildConfig.VERSION_NAME))) {
                                        it2.TargetDir = "~chelpus_disabled~" + it2.TargetDir.replaceAll("~chelpus_disabled~", BuildConfig.VERSION_NAME);
                                        it2.SourceDir = it2.SourceDir;
                                        found = true;
                                    }
                                }
                                if (found) {
                                    BinderActivity.savetoFile(bindes, context);
                                    if (listAppsFragment.su) {
                                        Utils.run_all("umount -f '" + item.TargetDir.replaceAll("~chelpus_disabled~", BuildConfig.VERSION_NAME) + "'");
                                        Utils.run_all("umount -l '" + item.TargetDir.replaceAll("~chelpus_disabled~", BuildConfig.VERSION_NAME) + "'");
                                    } else {
                                        Utils.cmd("umount '" + item.TargetDir.replaceAll("~chelpus_disabled~", BuildConfig.VERSION_NAME) + "'");
                                    }
                                } else {
                                    if (listAppsFragment.su) {
                                        Utils.run_all("umount -f '" + item.TargetDir.replaceAll("~chelpus_disabled~", BuildConfig.VERSION_NAME) + "'");
                                        Utils.run_all("umount -l '" + item.TargetDir.replaceAll("~chelpus_disabled~", BuildConfig.VERSION_NAME) + "'");
                                    } else {
                                        Utils.cmd("umount '" + item.TargetDir.replaceAll("~chelpus_disabled~", BuildConfig.VERSION_NAME) + "'");
                                    }
                                    remoteViews.setInt(C0149R.id.toggleButton_off, "setBackgroundResource", C0149R.drawable.switch_off);
                                    remoteViews.setTextColor(C0149R.id.appwidget_text, Color.parseColor("#AAAAAA"));
                                }
                            } else {
                                bindes = BinderActivity.getBindes(context);
                                found = false;
                                it = bindes.iterator();
                                while (it.hasNext()) {
                                    it2 = (BindItem) it.next();
                                    if (it2.SourceDir.replaceAll("~chelpus_disabled~", BuildConfig.VERSION_NAME).equals(item.SourceDir.replaceAll("~chelpus_disabled~", BuildConfig.VERSION_NAME)) && it2.TargetDir.replaceAll("~chelpus_disabled~", BuildConfig.VERSION_NAME).equals(item.TargetDir.replaceAll("~chelpus_disabled~", BuildConfig.VERSION_NAME))) {
                                        it2.TargetDir = it2.TargetDir.replaceAll("~chelpus_disabled~", BuildConfig.VERSION_NAME);
                                        it2.SourceDir = it2.SourceDir.replaceAll("~chelpus_disabled~", BuildConfig.VERSION_NAME);
                                        BinderWidgetConfigureActivity.saveTitlePref(context, id, it2.toString());
                                        found = true;
                                    }
                                }
                                if (!found) {
                                    if (listAppsFragment.su) {
                                        Utils.run_all("umount -f '" + item.TargetDir.replaceAll("~chelpus_disabled~", BuildConfig.VERSION_NAME) + "'");
                                        Utils.run_all("umount -l '" + item.TargetDir.replaceAll("~chelpus_disabled~", BuildConfig.VERSION_NAME) + "'");
                                    } else {
                                        Utils.cmd("umount '" + item.TargetDir.replaceAll("~chelpus_disabled~", BuildConfig.VERSION_NAME) + "'");
                                    }
                                    remoteViews.setInt(C0149R.id.toggleButton_off, "setBackgroundResource", C0149R.drawable.switch_off);
                                    remoteViews.setTextColor(C0149R.id.appwidget_text, Color.parseColor("#AAAAAA"));
                                }
                                BinderActivity.savetoFile(bindes, context);
                                Utils.verify_bind_and_run("mount", "-o bind '" + item.SourceDir.replaceAll("~chelpus_disabled~", BuildConfig.VERSION_NAME) + "' '" + item.TargetDir.replaceAll("~chelpus_disabled~", BuildConfig.VERSION_NAME) + "'", item.SourceDir.replaceAll("~chelpus_disabled~", BuildConfig.VERSION_NAME), item.TargetDir.replaceAll("~chelpus_disabled~", BuildConfig.VERSION_NAME));
                            }
                            final BindItem fin_item;
                            if (Utils.checkBind(item)) {
                                remoteViews.setTextColor(C0149R.id.appwidget_text, Color.parseColor("#00FF00"));
                                remoteViews.setInt(C0149R.id.toggleButton_off, "setBackgroundResource", C0149R.drawable.switch_on);
                                fin_item = item;
                                handler.post(new Runnable() {
                                    public void run() {
                                        Toast.makeText(context, "ON " + fin_item.TargetDir.replaceAll("~chelpus_disabled~", BuildConfig.VERSION_NAME), 0).show();
                                    }
                                });
                            } else {
                                remoteViews.setTextColor(C0149R.id.appwidget_text, Color.parseColor("#FF0000"));
                                remoteViews.setInt(C0149R.id.toggleButton_off, "setBackgroundResource", C0149R.drawable.switch_off);
                                fin_item = item;
                                handler.post(new Runnable() {
                                    public void run() {
                                        Toast.makeText(context, "OFF " + fin_item.TargetDir.replaceAll("~chelpus_disabled~", BuildConfig.VERSION_NAME), 0).show();
                                    }
                                });
                            }
                            ComponentName myWidget = new ComponentName(context, BinderWidget.class);
                            AppWidgetManager.getInstance(context).updateAppWidget(id, remoteViews);
                            AppWidgetManager gm = AppWidgetManager.getInstance(context);
                            BinderWidget.widget.onUpdate(context, gm, gm.getAppWidgetIds(new ComponentName(context, BinderWidget.class)));
                            listAppsFragment.binder_process = false;
                        }
                    }
                });
                turn_binder.setPriority(10);
                if (!listAppsFragment.binder_process) {
                    turn_binder.start();
                }
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }
        if (ACTION_WIDGET_RECEIVER_Updater.equals(action)) {
            listAppsFragment.binderWidget = true;
            AppWidgetManager gm = AppWidgetManager.getInstance(context);
            onUpdate(context, gm, gm.getAppWidgetIds(new ComponentName(context, BinderWidget.class)));
        }
    }
}
