package com.android.vending.billing.InAppBillingService.LUCK;

public class StringItem {
    public boolean bits32 = false;
    public byte[] offset = null;
    public String str = BuildConfig.VERSION_NAME;

    public StringItem(String str, byte[] offset, boolean bits) {
        this.offset = offset;
        this.str = str;
        this.bits32 = bits;
    }
}
