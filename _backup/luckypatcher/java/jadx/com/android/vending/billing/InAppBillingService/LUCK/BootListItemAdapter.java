package com.android.vending.billing.InAppBillingService.LUCK;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.chelpus.Utils;
import java.util.Comparator;
import java.util.List;

public class BootListItemAdapter extends ArrayAdapter<PkgListItem> {
    public static final int TEXT_DEFAULT = 0;
    public static final int TEXT_LARGE = 2;
    public static final int TEXT_MEDIUM = 1;
    public static final int TEXT_SMALL = 0;
    Context context;
    List<PkgListItem> data;
    ImageView imgIcon;
    int layoutResourceId;
    int size;
    public Comparator<PkgListItem> sorter;
    TextView txtStatus;
    TextView txtTitle;

    public BootListItemAdapter(Context context, int layout, List<PkgListItem> p) {
        super(context, layout, p);
        this.context = context;
        this.layoutResourceId = layout;
        this.size = TEXT_DEFAULT;
        this.data = p;
    }

    public BootListItemAdapter(Context context, int layout, int s, List<PkgListItem> p) {
        super(context, layout, p);
        this.context = context;
        this.layoutResourceId = layout;
        this.size = s;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        PkgListItem p = (PkgListItem) getItem(position);
        parent.setBackgroundColor(-16777216);
        if (p.name != null && !p.boot_ads && !p.boot_custom && !p.boot_lvl && !p.boot_manual) {
            return new View(this.context);
        }
        View row = convertView;
        row = ((Activity) this.context).getLayoutInflater().inflate(this.layoutResourceId, parent, false);
        row.setBackgroundColor(-16777216);
        this.txtTitle = (TextView) row.findViewById(C0149R.id.txtTitle);
        this.txtStatus = (TextView) row.findViewById(C0149R.id.txtStatus);
        this.imgIcon = (ImageView) row.findViewById(C0149R.id.imgIcon);
        this.txtTitle.setText(p.name);
        this.imgIcon.setMaxHeight(TEXT_MEDIUM);
        this.imgIcon.setMaxWidth(TEXT_MEDIUM);
        try {
            this.imgIcon.setImageDrawable(listAppsFragment.getPkgMng().getApplicationIcon(p.pkgName));
        } catch (NameNotFoundException e) {
            e.printStackTrace();
        }
        this.txtTitle.setTextAppearance(this.context, listAppsFragment.getSizeText());
        this.txtStatus.setTextAppearance(this.context, listAppsFragment.getSizeText());
        this.txtTitle.setTextColor(-7829368);
        String boot = BuildConfig.VERSION_NAME;
        Resources r = listAppsFragment.getRes();
        if (p.boot_ads) {
            boot = boot + r.getString(C0149R.string.stat_boot_ads) + "; ";
            this.txtTitle.setTextColor(-16711681);
        }
        if (p.boot_lvl) {
            boot = boot + r.getString(C0149R.string.stat_boot_lvl) + "; ";
            this.txtTitle.setTextColor(-16711936);
        }
        if (p.boot_custom) {
            boot = boot + r.getString(C0149R.string.stat_boot_custom) + "; ";
            this.txtTitle.setTextColor(-256);
        }
        this.txtStatus.setText(boot);
        if (p.pkgName.equals(Utils.getText(C0149R.string.android_patches_for_bootlist))) {
            this.imgIcon.setImageResource(C0149R.drawable.ic_launcher);
            this.txtStatus.setText(Utils.getText(C0149R.string.android_patches_for_bootlist_status));
        }
        this.txtStatus.setTextAppearance(this.context, 16973894);
        this.txtStatus.setTextColor(-7829368);
        this.txtTitle.setBackgroundColor(-16777216);
        this.txtStatus.setBackgroundColor(-16777216);
        return row;
    }

    public int getViewTypeCount() {
        return TEXT_LARGE;
    }

    public void setTextSize(int s) {
        this.size = s;
        notifyDataSetChanged();
    }

    public void sort() {
        super.sort(this.sorter);
        notifyDataSetChanged();
    }

    public PkgListItem getItem(String name) {
        for (int i = TEXT_DEFAULT; i < getCount(); i += TEXT_MEDIUM) {
            if (((PkgListItem) getItem(i)).pkgName.contentEquals(name)) {
                return (PkgListItem) getItem(i);
            }
        }
        return null;
    }
}
