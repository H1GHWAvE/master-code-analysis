package com.android.vending.billing.InAppBillingService.LUCK;

import android.app.Activity;
import android.app.LocalActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TabHost;
import android.widget.TextView;

public class HelpActivity extends Activity {
    public Context context;
    LocalActivityManager mLocalActivityManager;
    TabHost tabHost;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(C0149R.layout.help);
        initTabs(savedInstanceState);
    }

    protected void onResume() {
        this.mLocalActivityManager.dispatchResume();
        super.onResume();
    }

    protected void onPause() {
        this.mLocalActivityManager.dispatchPause(isFinishing());
        super.onPause();
    }

    private void initTabs(Bundle savedInstanceState) {
        Resources res = getResources();
        this.tabHost = (TabHost) findViewById(16908306);
        this.mLocalActivityManager = new LocalActivityManager(this, false);
        this.mLocalActivityManager.dispatchCreate(savedInstanceState);
        this.tabHost.setup(this.mLocalActivityManager);
        TabHost tabHost = (TabHost) findViewById(16908306);
        tabHost.getTabWidget().setDividerDrawable(C0149R.drawable.tab_divider);
        View tabview1 = createTabView(tabHost.getContext(), res.getString(C0149R.string.help_common));
        View tabview2 = createTabView(tabHost.getContext(), res.getString(C0149R.string.help_custom));
        tabHost.addTab(tabHost.newTabSpec("Common").setIndicator(tabview1).setContent(new Intent().setClass(this, HelpCommon.class)));
        tabHost.addTab(tabHost.newTabSpec("Create").setIndicator(tabview2).setContent(new Intent().setClass(this, HelpCustom.class)));
        tabHost.setCurrentTab(0);
        tabHost.getTabWidget().setCurrentTab(0);
    }

    private static View createTabView(Context context, String text) {
        View view = LayoutInflater.from(context).inflate(C0149R.layout.tabs_bg, null);
        ((TextView) view.findViewById(C0149R.id.tabsText)).setText(text);
        return view;
    }
}
