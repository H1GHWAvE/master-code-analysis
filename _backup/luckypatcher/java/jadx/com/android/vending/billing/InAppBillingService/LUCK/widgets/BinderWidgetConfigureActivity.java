package com.android.vending.billing.InAppBillingService.LUCK.widgets;

import android.app.Activity;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import com.android.vending.billing.InAppBillingService.LUCK.BindItem;
import com.android.vending.billing.InAppBillingService.LUCK.BinderActivity;
import com.android.vending.billing.InAppBillingService.LUCK.BuildConfig;
import com.android.vending.billing.InAppBillingService.LUCK.C0149R;
import com.android.vending.billing.InAppBillingService.LUCK.LogCollector;
import com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment;
import com.chelpus.Utils;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class BinderWidgetConfigureActivity extends Activity {
    private static final String PREFS_NAME = "com.android.vending.billing.InAppBillingService.LUCK.widgets.BinderWidget";
    private static final String PREF_PREFIX_KEY = "appwidget_";
    public ArrayList<BindItem> bindes = null;
    public Context context;
    public ListView lv = null;
    int mAppWidgetId = 0;
    EditText mAppWidgetText;
    public int sizeText = 0;

    class C05202 implements OnItemClickListener {
        C05202() {
        }

        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            BindItem item = (BindItem) adapterView.getItemAtPosition(i);
            Context context = BinderWidgetConfigureActivity.this;
            BinderWidgetConfigureActivity.saveTitlePref(context, BinderWidgetConfigureActivity.this.mAppWidgetId, item.toString());
            BinderWidget.updateAppWidget(context, AppWidgetManager.getInstance(context), BinderWidgetConfigureActivity.this.mAppWidgetId);
            Intent resultValue = new Intent();
            resultValue.putExtra("appWidgetId", BinderWidgetConfigureActivity.this.mAppWidgetId);
            BinderWidgetConfigureActivity.this.setResult(-1, resultValue);
            BinderWidgetConfigureActivity.this.finish();
        }
    }

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setResult(0);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.mAppWidgetId = extras.getInt("appWidgetId", 0);
        }
        if (this.mAppWidgetId == 0) {
            finish();
            return;
        }
        this.context = this;
        File bindfile = new File(getDir("binder", 0) + "/bind.txt");
        if (!bindfile.exists()) {
            try {
                bindfile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        this.bindes = BinderActivity.getBindes(this.context);
        this.lv = new ListView(this.context);
        if (this.bindes.size() == 0) {
            this.bindes.add(new BindItem("empty", "empty"));
        }
        listAppsFragment.adaptBind = new ArrayAdapter<BindItem>(this, C0149R.layout.contextmenu, this.bindes) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                TextView textView = (TextView) view.findViewById(C0149R.id.conttext);
                textView.setTextAppearance(getContext(), listAppsFragment.getSizeText());
                textView.setTextAppearance(BinderWidgetConfigureActivity.this.context, BinderWidgetConfigureActivity.this.sizeText);
                BindItem item = (BindItem) getItem(position);
                if (item.SourceDir.equals("empty") && item.TargetDir.equals("empty")) {
                    textView.setText(Utils.getText(C0149R.string.empty) + LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.empty_binders));
                    view.setEnabled(false);
                    view.setClickable(false);
                } else {
                    textView.setText(Utils.getColoredText(Utils.getText(C0149R.string.bind_source) + LogCollector.LINE_SEPARATOR, "#ff00ff00", "bold"));
                    textView.append(Utils.getColoredText(item.SourceDir, "#ffffffff", "italic"));
                    textView.append(Utils.getColoredText(LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.bind_target) + LogCollector.LINE_SEPARATOR, "#ffffff00", "bold"));
                    textView.append(Utils.getColoredText(item.TargetDir.replace("~chelpus_disabled~", BuildConfig.VERSION_NAME), "#ffffffff", "italic"));
                }
                return view;
            }
        };
        this.lv.setAdapter(listAppsFragment.adaptBind);
        this.lv.invalidateViews();
        this.lv.setBackgroundColor(-16777216);
        if (!(this.bindes.size() == 1 && ((BindItem) this.bindes.get(0)).TargetDir.equals("empty") && ((BindItem) this.bindes.get(0)).SourceDir.equals("empty"))) {
            this.lv.setOnItemClickListener(new C05202());
        }
        setContentView(this.lv);
    }

    static void saveTitlePref(Context context, int appWidgetId, String text) {
        Editor prefs = context.getSharedPreferences(PREFS_NAME, 4).edit();
        prefs.putString(PREF_PREFIX_KEY + appWidgetId, text);
        prefs.commit();
    }

    static String loadTitlePref(Context context, int appWidgetId) {
        String titleValue = context.getSharedPreferences(PREFS_NAME, 4).getString(PREF_PREFIX_KEY + appWidgetId, null);
        return titleValue != null ? titleValue : "NOT_SAVED_BIND";
    }

    static void deleteTitlePref(Context context, int appWidgetId) {
        Editor prefs = context.getSharedPreferences(PREFS_NAME, 4).edit();
        prefs.remove(PREF_PREFIX_KEY + appWidgetId);
        prefs.commit();
    }
}
