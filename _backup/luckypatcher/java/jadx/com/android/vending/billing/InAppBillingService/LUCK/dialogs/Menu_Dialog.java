package com.android.vending.billing.InAppBillingService.LUCK.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import com.android.vending.billing.InAppBillingService.LUCK.AlertDlg;
import com.android.vending.billing.InAppBillingService.LUCK.BuildConfig;
import com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment;

public class Menu_Dialog {
    Dialog dialog = null;

    class C02081 implements OnItemClickListener {
        C02081() {
        }

        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
            try {
                listAppsFragment com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment = listAppsFragment.frag;
                listAppsFragment.removeDialogLP(7);
                listAppsFragment.frag.runId(((Integer) listAppsFragment.adapt.getItem(position)).intValue());
            } catch (Exception e) {
                System.out.println("LuckyPatcher (ContextMenu): Error open! " + e);
                e.printStackTrace();
            }
        }
    }

    class C02092 implements OnCancelListener {
        C02092() {
        }

        public void onCancel(DialogInterface dialog) {
            listAppsFragment.rebuldApk = BuildConfig.VERSION_NAME;
            System.out.println(listAppsFragment.rebuldApk);
        }
    }

    public void showDialog() {
        if (this.dialog == null) {
            this.dialog = onCreateDialog();
        }
        if (this.dialog != null) {
            this.dialog.show();
        }
    }

    public Dialog onCreateDialog() {
        try {
            System.out.println("Menu Dialog create.");
            if (listAppsFragment.frag == null || listAppsFragment.frag.getContext() == null) {
                dismiss();
            }
            AlertDlg builder5 = new AlertDlg(listAppsFragment.frag.getContext());
            if (listAppsFragment.adapt != null) {
                listAppsFragment.adapt.setNotifyOnChange(true);
                builder5.setAdapter(listAppsFragment.adapt, new C02081());
            }
            builder5.setOnCancelListener(new C02092());
            return builder5.create();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void dismiss() {
        if (this.dialog != null) {
            this.dialog.dismiss();
            this.dialog = null;
        }
    }
}
