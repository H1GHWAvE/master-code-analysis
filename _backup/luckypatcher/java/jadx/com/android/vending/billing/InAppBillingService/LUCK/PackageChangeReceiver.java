package com.android.vending.billing.InAppBillingService.LUCK;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Toast;
import com.android.vending.billing.InAppBillingService.LUCK.widgets.AppDisablerWidget;
import com.android.vending.billing.InAppBillingService.LUCK.widgets.inapp_widget;
import com.android.vending.billing.InAppBillingService.LUCK.widgets.lvl_widget;
import com.android.vending.licensing.ILicensingService;
import com.chelpus.Common;
import com.chelpus.Utils;
import com.google.android.finsky.billing.iab.InAppBillingService;
import com.google.android.finsky.billing.iab.MarketBillingService;
import com.google.android.finsky.billing.iab.google.util.IInAppBillingService;
import com.google.android.finsky.services.LicensingService;
import com.google.android.vending.licensing.util.Base64;
import com.google.android.vending.licensing.util.Base64DecoderException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class PackageChangeReceiver extends BroadcastReceiver {
    static ServiceConnection mServiceConn;
    static ServiceConnection mServiceConnL;
    boolean hackedBilling = false;
    Handler handler = null;
    IInAppBillingService mService;
    ILicensingService mServiceL;
    boolean mSetupDone = false;
    int responseCode = MotionEventCompat.ACTION_MASK;

    class C01242 implements ServiceConnection {

        class C01211 implements OnClickListener {
            C01211() {
            }

            public void onClick(View view) {
                Intent intent;
                if (VERSION.SDK_INT >= 9) {
                    intent = new Intent("android.settings.APPLICATION_DETAILS_SETTINGS", Uri.parse("package:com.android.vending"));
                    intent.setFlags(AccessibilityNodeInfoCompat.ACTION_SET_SELECTION);
                    intent.setFlags(268435456);
                    listAppsFragment.getInstance().startActivity(intent);
                } else {
                    intent = new Intent("android.intent.action.VIEW");
                    intent.setClassName(Common.SETTINGS_PKG, "com.android.settings.InstalledAppDetails");
                    intent.putExtra("com.android.settings.ApplicationPkgName", Common.GOOGLEPLAY_PKG);
                    intent.putExtra("pkg", Common.GOOGLEPLAY_PKG);
                    intent.setFlags(AccessibilityNodeInfoCompat.ACTION_SET_SELECTION);
                    intent.setFlags(268435456);
                    listAppsFragment.getInstance().startActivity(intent);
                }
                ((WindowManager) listAppsFragment.getInstance().getSystemService("window")).removeView(view.getRootView());
            }
        }

        class C01222 implements OnClickListener {
            C01222() {
            }

            public void onClick(View view) {
                ((WindowManager) listAppsFragment.getInstance().getSystemService("window")).removeView(view.getRootView());
            }
        }

        class C01233 implements Runnable {
            C01233() {
            }

            public void run() {
                Utils.market_billing_services(false);
            }
        }

        C01242() {
        }

        public void onServiceDisconnected(ComponentName name) {
            System.out.println("Billing service disconnected.");
            PackageChangeReceiver.this.mSetupDone = false;
            PackageChangeReceiver.this.mService = null;
        }

        public void onServiceConnected(ComponentName name, IBinder service) {
            System.out.println("Billing service try to connect.");
            if (name.getPackageName().equals(listAppsFragment.class.getPackage().getName())) {
                System.out.println("Billing service connected.");
                try {
                    listAppsFragment.getInstance().unbindService(PackageChangeReceiver.mServiceConn);
                    return;
                } catch (Exception e) {
                    e.printStackTrace();
                    return;
                }
            }
            System.out.println("Firmware not support hacked billing");
            if (listAppsFragment.su) {
                new Thread(new C01233()).start();
            } else {
                Utils.showSystemWindow(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.warning_inapp_emulation), new C01211(), new C01222());
                listAppsFragment.getPkgMng().setComponentEnabledSetting(new ComponentName(listAppsFragment.getInstance(), MarketBillingService.class), 2, 1);
                listAppsFragment.getPkgMng().setComponentEnabledSetting(new ComponentName(listAppsFragment.getInstance(), InAppBillingService.class), 2, 1);
            }
            Intent in = new Intent(listAppsFragment.getInstance(), inapp_widget.class);
            in.setAction(inapp_widget.ACTION_WIDGET_RECEIVER_Updater);
            listAppsFragment.getInstance().sendBroadcast(in);
            try {
                listAppsFragment.getInstance().unbindService(PackageChangeReceiver.mServiceConn);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    class C01283 implements ServiceConnection {

        class C01251 implements Runnable {
            C01251() {
            }

            public void run() {
                Utils.market_licensing_services(false);
            }
        }

        class C01262 implements OnClickListener {
            C01262() {
            }

            public void onClick(View view) {
                Intent intent;
                if (VERSION.SDK_INT >= 9) {
                    intent = new Intent("android.settings.APPLICATION_DETAILS_SETTINGS", Uri.parse("package:com.android.vending"));
                    intent.setFlags(AccessibilityNodeInfoCompat.ACTION_SET_SELECTION);
                    intent.setFlags(268435456);
                    listAppsFragment.getInstance().startActivity(intent);
                } else {
                    intent = new Intent("android.intent.action.VIEW");
                    intent.setClassName(Common.SETTINGS_PKG, "com.android.settings.InstalledAppDetails");
                    intent.putExtra("com.android.settings.ApplicationPkgName", Common.GOOGLEPLAY_PKG);
                    intent.putExtra("pkg", Common.GOOGLEPLAY_PKG);
                    intent.setFlags(AccessibilityNodeInfoCompat.ACTION_SET_SELECTION);
                    intent.setFlags(268435456);
                    listAppsFragment.getInstance().startActivity(intent);
                }
                ((WindowManager) listAppsFragment.getInstance().getSystemService("window")).removeView(view.getRootView());
            }
        }

        class C01273 implements OnClickListener {
            C01273() {
            }

            public void onClick(View view) {
                ((WindowManager) listAppsFragment.getInstance().getSystemService("window")).removeView(view.getRootView());
            }
        }

        C01283() {
        }

        public void onServiceDisconnected(ComponentName name) {
            System.out.println("Licensing service disconnected.");
            PackageChangeReceiver.this.mServiceL = null;
        }

        public void onServiceConnected(ComponentName name, IBinder service) {
            System.out.println("Licensing service try to connect.");
            if (!name.getPackageName().equals(listAppsFragment.class.getPackage().getName())) {
                if (listAppsFragment.su) {
                    new Thread(new C01251()).start();
                } else {
                    Utils.showSystemWindow(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.warning_inapp_emulation), new C01262(), new C01273());
                    listAppsFragment.getPkgMng().setComponentEnabledSetting(new ComponentName(listAppsFragment.getInstance(), LicensingService.class), 2, 1);
                }
                System.out.println("Firmware not support lvl emulation");
            }
            try {
                listAppsFragment.getInstance().unbindService(PackageChangeReceiver.mServiceConnL);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    class C01294 implements Runnable {
        C01294() {
        }

        public void run() {
            Toast.makeText(listAppsFragment.getInstance(), Utils.getText(C0149R.string.backup_done) + " " + listAppsFragment.basepath + "/Backup/", 1).show();
        }
    }

    class C01305 implements Runnable {
        C01305() {
        }

        public void run() {
            Toast.makeText(listAppsFragment.getInstance(), Utils.getText(C0149R.string.warning) + ":" + Utils.getText(C0149R.string.error_backup_apk), 1).show();
        }
    }

    String getPackageName(Intent intent) {
        Uri uri = intent.getData();
        return uri != null ? uri.getSchemeSpecificPart() : null;
    }

    public void onReceive(final Context ctx, final Intent intent) {
        this.handler = new Handler();
        new Thread(new Runnable() {

            class C01122 implements Runnable {
                C01122() {
                }

                public void run() {
                    listAppsFragment.removeDialogLP(11);
                }
            }

            class C01133 implements Runnable {
                C01133() {
                }

                public void run() {
                    listAppsFragment.removeDialogLP(11);
                }
            }

            class C01144 implements Runnable {
                C01144() {
                }

                public void run() {
                    listAppsFragment.removeDialogLP(11);
                    listAppsFragment.adapt.notifyDataSetChanged();
                }
            }

            class C01155 implements Runnable {
                C01155() {
                }

                public void run() {
                    if (listAppsFragment.su) {
                        Utils.market_billing_services(true);
                    }
                }
            }

            class C01166 implements Runnable {
                C01166() {
                }

                public void run() {
                    if (listAppsFragment.su) {
                        Utils.market_licensing_services(true);
                    }
                }
            }

            public void run() {
                final PkgListItem item;
                String pkgname;
                System.out.println(intent.getAction());
                if (intent.getAction().equals("android.intent.action.PACKAGE_CHANGED")) {
                    Intent in = new Intent(ctx, AppDisablerWidget.class);
                    in.setAction(AppDisablerWidget.ACTION_WIDGET_RECEIVER_Updater);
                    ctx.sendBroadcast(in);
                    listAppsFragment.init();
                    try {
                        String[] components;
                        System.out.println(intent.getData().toString());
                        if (PackageChangeReceiver.this.getPackageName(intent).equals(listAppsFragment.class.getPackage().getName())) {
                            components = intent.getStringArrayExtra("android.intent.extra.changed_component_name_list");
                            if (!(components == null || components.length <= 0 || listAppsFragment.frag == null)) {
                                System.out.println("update adapt " + components[0]);
                                if (components[0].contains("com.android.vending.billing.InAppBillingService.LUCK.MainActivity")) {
                                    try {
                                        if (listAppsFragment.su) {
                                            Intent i = new Intent("android.intent.action.MAIN");
                                            i.addCategory("android.intent.category.HOME");
                                            i.addCategory("android.intent.category.DEFAULT");
                                            for (ResolveInfo res : listAppsFragment.getPkgMng().queryIntentActivities(i, 0)) {
                                                if (res.activityInfo != null) {
                                                    final String str = res.activityInfo.processName;
                                                    final String str2 = res.activityInfo.packageName;
                                                    new Thread(new Runnable() {
                                                        public void run() {
                                                            Utils.kill(str);
                                                            Utils.kill(str2);
                                                        }
                                                    }).start();
                                                }
                                            }
                                        }
                                        listAppsFragment.frag.runToMain(new C01122());
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                        if (PackageChangeReceiver.this.getPackageName(intent).equals(Common.GOOGLEPLAY_PKG) || PackageChangeReceiver.this.getPackageName(intent).equals("com.google.android.gms")) {
                            components = intent.getStringArrayExtra("android.intent.extra.changed_component_name_list");
                            if (components != null && components.length > 0) {
                                if (PackageChangeReceiver.this.getPackageName(intent).equals("com.google.android.gms") && listAppsFragment.frag != null) {
                                    System.out.println("update adapt " + components[0]);
                                    listAppsFragment.frag.runToMain(new C01133());
                                }
                                if (!(listAppsFragment.adapt == null || listAppsFragment.frag == null)) {
                                    System.out.println("update adapt " + components[0]);
                                    listAppsFragment.frag.runToMain(new C01144());
                                }
                            }
                        }
                        if (PackageChangeReceiver.this.getPackageName(intent).equals(listAppsFragment.getInstance().getPackageName())) {
                            components = intent.getStringArrayExtra("android.intent.extra.changed_component_name_list");
                            if (components != null && components.length > 0) {
                                boolean billingServiceChanged = false;
                                boolean licensingServiceChanged = false;
                                boolean enableGoogle = false;
                                boolean enableLicGoogle = false;
                                for (String comp : components) {
                                    if (comp.equals("com.google.android.finsky.billing.iab.MarketBillingService") || comp.equals("com.google.android.finsky.billing.iab.InAppBillingService")) {
                                        System.out.println(comp);
                                        Intent in4 = new Intent(listAppsFragment.getInstance(), inapp_widget.class);
                                        in4.setPackage(listAppsFragment.getInstance().getPackageName());
                                        in4.setAction(inapp_widget.ACTION_WIDGET_RECEIVER_Updater);
                                        listAppsFragment.getInstance().sendBroadcast(in4);
                                        if (listAppsFragment.getPkgMng().getComponentEnabledSetting(new ComponentName(listAppsFragment.getInstance(), InAppBillingService.class)) == 1) {
                                            billingServiceChanged = true;
                                        } else {
                                            enableGoogle = true;
                                        }
                                    }
                                    if (comp.equals("com.licensinghack.LicensingService")) {
                                        System.out.println(comp);
                                        Intent in1 = new Intent(listAppsFragment.getInstance(), lvl_widget.class);
                                        in1.setPackage(listAppsFragment.getInstance().getPackageName());
                                        in1.setAction(lvl_widget.ACTION_WIDGET_RECEIVER_Updater);
                                        listAppsFragment.getInstance().sendBroadcast(in1);
                                        if (listAppsFragment.getPkgMng().getComponentEnabledSetting(new ComponentName(listAppsFragment.getInstance(), LicensingService.class)) == 1) {
                                            licensingServiceChanged = true;
                                        } else {
                                            enableLicGoogle = true;
                                        }
                                    }
                                }
                                if (billingServiceChanged) {
                                    PackageChangeReceiver.this.connectToBilling();
                                }
                                if (licensingServiceChanged) {
                                    PackageChangeReceiver.this.connectToLicensing();
                                }
                                if (enableGoogle) {
                                    new Thread(new C01155()).start();
                                }
                                if (enableLicGoogle) {
                                    new Thread(new C01166()).start();
                                }
                            }
                        }
                        listAppsFragment.days = listAppsFragment.getConfig().getInt("days_on_up", 1);
                        try {
                            item = new PkgListItem(listAppsFragment.getInstance(), PackageChangeReceiver.this.getPackageName(intent), listAppsFragment.days, false);
                            if (listAppsFragment.database == null) {
                                listAppsFragment.database = new DatabaseHelper(listAppsFragment.getInstance());
                            }
                            listAppsFragment.database.savePackage(item);
                            if (!(!listAppsFragment.desktop_launch || listAppsFragment.plia == null || listAppsFragment.frag == null || listAppsFragment.frag.getActivity() == null)) {
                                listAppsFragment.frag.getActivity().runOnUiThread(new Runnable() {
                                    public void run() {
                                        if (listAppsFragment.plia.checkItem(item.pkgName)) {
                                            listAppsFragment.plia.updateItem(item.pkgName);
                                            listAppsFragment.plia.sort();
                                        } else {
                                            listAppsFragment.plia.add(item);
                                        }
                                        listAppsFragment.plia.notifyDataSetChanged();
                                        listAppsFragment.plia.sort();
                                    }
                                });
                            }
                        } catch (Exception e2) {
                            e2.printStackTrace();
                            System.out.println("Item dont create. And dont add to database.");
                        }
                    } catch (RuntimeException e3) {
                        e3.printStackTrace();
                    }
                }
                if (intent.getAction().equals("android.intent.action.PACKAGE_REPLACED")) {
                    in = new Intent(ctx, AppDisablerWidget.class);
                    in.setAction(AppDisablerWidget.ACTION_WIDGET_RECEIVER_Updater);
                    ctx.sendBroadcast(in);
                    listAppsFragment.init();
                    try {
                        System.out.println(intent.getData().toString());
                        pkgname = PackageChangeReceiver.this.getPackageName(intent);
                        listAppsFragment.getConfig().edit().putBoolean(pkgname, false).commit();
                        if (listAppsFragment.su) {
                            if (new File("/data/app/" + pkgname + "-1.odex").exists()) {
                                Utils.run_all("rm /data/app/" + pkgname + "-1.odex");
                            }
                            if (new File("/data/app/" + pkgname + "-2.odex").exists()) {
                                Utils.run_all("rm /data/app/" + pkgname + "-2.odex");
                            }
                            if (new File("/data/app/" + pkgname + ".odex").exists()) {
                                Utils.run_all("rm /data/app/" + pkgname + ".odex");
                            }
                        }
                        if (pkgname.contains(Common.GOOGLEPLAY_PKG) || pkgname.contains(ctx.getPackageName())) {
                            if (listAppsFragment.getPkgMng().getComponentEnabledSetting(new ComponentName(ctx, LicensingService.class)) == 2) {
                                Utils.market_licensing_services(true);
                                listAppsFragment.getPkgMng().setComponentEnabledSetting(new ComponentName(listAppsFragment.getInstance(), LicensingService.class), 2, 1);
                            } else {
                                listAppsFragment.getPkgMng().setComponentEnabledSetting(new ComponentName(listAppsFragment.getInstance(), LicensingService.class), 1, 1);
                            }
                            if (listAppsFragment.getPkgMng().getComponentEnabledSetting(new ComponentName(ctx, MarketBillingService.class)) == 2 || listAppsFragment.getPkgMng().getComponentEnabledSetting(new ComponentName(ctx, InAppBillingService.class)) == 2) {
                                Utils.market_billing_services(true);
                                listAppsFragment.getPkgMng().setComponentEnabledSetting(new ComponentName(listAppsFragment.getInstance(), MarketBillingService.class), 2, 1);
                                listAppsFragment.getPkgMng().setComponentEnabledSetting(new ComponentName(listAppsFragment.getInstance(), InAppBillingService.class), 2, 1);
                            } else {
                                listAppsFragment.getPkgMng().setComponentEnabledSetting(new ComponentName(listAppsFragment.getInstance(), MarketBillingService.class), 1, 1);
                                listAppsFragment.getPkgMng().setComponentEnabledSetting(new ComponentName(listAppsFragment.getInstance(), InAppBillingService.class), 1, 1);
                            }
                        }
                        if (listAppsFragment.getConfig().getBoolean("manual_path", false)) {
                            listAppsFragment.days = listAppsFragment.getConfig().getInt("days_on_up", 1);
                            try {
                                item = new PkgListItem(listAppsFragment.getInstance(), PackageChangeReceiver.this.getPackageName(intent), listAppsFragment.days, false);
                                if (listAppsFragment.database == null) {
                                    listAppsFragment.database = new DatabaseHelper(listAppsFragment.getInstance());
                                }
                                listAppsFragment.database.savePackage(item);
                                if (!(!listAppsFragment.desktop_launch || listAppsFragment.plia == null || listAppsFragment.frag == null || listAppsFragment.frag.getActivity() == null)) {
                                    listAppsFragment.frag.getActivity().runOnUiThread(new Runnable() {
                                        public void run() {
                                            if (listAppsFragment.plia.checkItem(item.pkgName)) {
                                                listAppsFragment.plia.updateItem(item.pkgName);
                                            } else {
                                                listAppsFragment.plia.add(item);
                                            }
                                            listAppsFragment.plia.notifyDataSetChanged();
                                            listAppsFragment.plia.sort();
                                        }
                                    });
                                }
                            } catch (Exception e22) {
                                e22.printStackTrace();
                                System.out.println("Item dont create. And dont add to database.");
                            }
                        }
                        listAppsFragment.refresh = true;
                    } catch (RuntimeException e32) {
                        e32.printStackTrace();
                    }
                }
                if (intent.getAction().equals("android.intent.action.PACKAGE_ADDED")) {
                    in = new Intent(ctx, AppDisablerWidget.class);
                    in.setAction(AppDisablerWidget.ACTION_WIDGET_RECEIVER_Updater);
                    ctx.sendBroadcast(in);
                    listAppsFragment.init();
                    pkgname = PackageChangeReceiver.this.getPackageName(intent);
                    listAppsFragment.getConfig().edit().putBoolean(pkgname, false).commit();
                    if (listAppsFragment.getConfig().getBoolean("switch_auto_backup_apk", false)) {
                        System.out.println("Backup app on update");
                        if (!listAppsFragment.getConfig().getBoolean("switch_auto_backup_apk_only_gp", false)) {
                            PackageChangeReceiver.this.backup(pkgname);
                        }
                    }
                    if (listAppsFragment.getConfig().getBoolean("switch_auto_backup_apk_only_gp", false)) {
                        System.out.println("Backup app on update");
                        try {
                            if (listAppsFragment.getInstance().getPackageManager().getInstallerPackageName(pkgname).equals(Common.GOOGLEPLAY_PKG)) {
                                PackageChangeReceiver.this.backup(pkgname);
                            }
                        } catch (Exception e4) {
                        }
                    }
                    try {
                        if (listAppsFragment.handlerToast == null) {
                            listAppsFragment.handlerToast = PackageChangeReceiver.this.handler;
                        }
                        listAppsFragment.days = listAppsFragment.getConfig().getInt("days_on_up", 1);
                        item = new PkgListItem(listAppsFragment.getInstance(), PackageChangeReceiver.this.getPackageName(intent), listAppsFragment.days, false);
                        String appDir = BuildConfig.VERSION_NAME;
                        try {
                            appDir = listAppsFragment.getInstance().getPackageManager().getPackageInfo(item.pkgName, 0).applicationInfo.sourceDir;
                        } catch (NameNotFoundException e5) {
                            e5.printStackTrace();
                        }
                        if (item.system && appDir.startsWith("/data") && listAppsFragment.getConfig().getBoolean("switch_auto_integrate_update", false) && listAppsFragment.su) {
                            System.out.println("Integrate update to system on update app");
                            ArrayList<PkgListItem> plis = new ArrayList();
                            plis.add(item);
                            listAppsFragment.integrate_to_system(plis, false, true);
                        }
                        if (!item.system && appDir.startsWith("/data/") && listAppsFragment.getConfig().getBoolean("switch_auto_move_to_sd", false) && listAppsFragment.su) {
                            new Utils(BuildConfig.VERSION_NAME).cmdRoot("pm install -r -s -i com.android.vending " + appDir);
                        }
                        if (!item.system && appDir.startsWith("/mnt/") && listAppsFragment.getConfig().getBoolean("switch_auto_move_to_internal", false) && listAppsFragment.su) {
                            new Utils(BuildConfig.VERSION_NAME).cmdRoot("pm install -r -f -i com.android.vending " + appDir);
                        }
                        if (listAppsFragment.su) {
                            if (new File("/data/app/" + pkgname + "-1.odex").exists()) {
                                Utils.run_all("rm /data/app/" + pkgname + "-1.odex");
                            }
                            if (new File("/data/app/" + pkgname + "-2.odex").exists()) {
                                Utils.run_all("rm /data/app/" + pkgname + "-2.odex");
                            }
                            if (new File("/data/app/" + pkgname + ".odex").exists()) {
                                Utils.run_all("rm /data/app/" + pkgname + ".odex");
                            }
                        }
                        if (pkgname.contains(Common.GOOGLEPLAY_PKG)) {
                            if (listAppsFragment.getPkgMng().getComponentEnabledSetting(new ComponentName(ctx, LicensingService.class)) == 2) {
                                Utils.market_licensing_services(true);
                                listAppsFragment.getPkgMng().setComponentEnabledSetting(new ComponentName(listAppsFragment.getInstance(), LicensingService.class), 2, 1);
                            } else {
                                listAppsFragment.getPkgMng().setComponentEnabledSetting(new ComponentName(listAppsFragment.getInstance(), LicensingService.class), 1, 1);
                            }
                            if (listAppsFragment.getPkgMng().getComponentEnabledSetting(new ComponentName(ctx, MarketBillingService.class)) == 2 || listAppsFragment.getPkgMng().getComponentEnabledSetting(new ComponentName(ctx, InAppBillingService.class)) == 2) {
                                Utils.market_billing_services(true);
                                listAppsFragment.getPkgMng().setComponentEnabledSetting(new ComponentName(listAppsFragment.getInstance(), MarketBillingService.class), 2, 1);
                                listAppsFragment.getPkgMng().setComponentEnabledSetting(new ComponentName(listAppsFragment.getInstance(), InAppBillingService.class), 2, 1);
                            } else {
                                listAppsFragment.getPkgMng().setComponentEnabledSetting(new ComponentName(listAppsFragment.getInstance(), MarketBillingService.class), 1, 1);
                                listAppsFragment.getPkgMng().setComponentEnabledSetting(new ComponentName(listAppsFragment.getInstance(), InAppBillingService.class), 1, 1);
                            }
                        }
                        if (listAppsFragment.getConfig().getBoolean("manual_path", false)) {
                            listAppsFragment.days = listAppsFragment.getConfig().getInt("days_on_up", 1);
                            if (listAppsFragment.database == null) {
                                listAppsFragment.database = new DatabaseHelper(listAppsFragment.getInstance());
                            }
                            listAppsFragment.database.savePackage(item);
                            if (!(!listAppsFragment.desktop_launch || listAppsFragment.plia == null || listAppsFragment.frag == null || listAppsFragment.frag.getActivity() == null)) {
                                listAppsFragment.frag.getActivity().runOnUiThread(new Runnable() {
                                    public void run() {
                                        if (listAppsFragment.plia.checkItem(item.pkgName)) {
                                            listAppsFragment.plia.updateItem(item.pkgName);
                                        } else {
                                            listAppsFragment.plia.add(item);
                                        }
                                        listAppsFragment.plia.notifyDataSetChanged();
                                        listAppsFragment.plia.sort();
                                    }
                                });
                            }
                        }
                        listAppsFragment.refresh = true;
                    } catch (RuntimeException e322) {
                        e322.printStackTrace();
                    }
                }
                if (intent.getAction().equals("android.intent.action.PACKAGE_REMOVED")) {
                    in = new Intent(ctx, AppDisablerWidget.class);
                    in.setAction(AppDisablerWidget.ACTION_WIDGET_RECEIVER_Updater);
                    ctx.sendBroadcast(in);
                    System.out.println("delete trigger " + intent.getBooleanExtra("android.intent.extra.REPLACING", false));
                    if (!intent.getBooleanExtra("android.intent.extra.REPLACING", false)) {
                        listAppsFragment.init();
                        try {
                            pkgname = PackageChangeReceiver.this.getPackageName(intent);
                            listAppsFragment.getConfig().edit().remove(pkgname).commit();
                            if (listAppsFragment.su) {
                                if (listAppsFragment.api >= 21) {
                                    if (new File("/data/app/" + pkgname + "-1/arm").exists()) {
                                        Utils.run_all("rm -rf /data/app/" + pkgname + "-1");
                                    }
                                    if (new File("/data/app/" + pkgname + "-2/arm").exists()) {
                                        Utils.run_all("rm -rf /data/app/" + pkgname + "-2");
                                    }
                                    if (new File("/data/app/" + pkgname + "-3/arm").exists()) {
                                        Utils.run_all("rm -rf /data/app/" + pkgname + "-3");
                                    }
                                    if (new File("/data/app/" + pkgname + "-4/arm").exists()) {
                                        Utils.run_all("rm -rf /data/app/" + pkgname + "-4");
                                    }
                                    if (new File("/data/app/" + pkgname + "-1/x86").exists()) {
                                        Utils.run_all("rm -rf /data/app/" + pkgname + "-1");
                                    }
                                    if (new File("/data/app/" + pkgname + "-2/x86").exists()) {
                                        Utils.run_all("rm -rf /data/app/" + pkgname + "-2");
                                    }
                                    if (new File("/data/app/" + pkgname + "-3/x86").exists()) {
                                        Utils.run_all("rm -rf /data/app/" + pkgname + "-3");
                                    }
                                    if (new File("/data/app/" + pkgname + "-4/x86").exists()) {
                                        Utils.run_all("rm -rf /data/app/" + pkgname + "-4");
                                    }
                                }
                                if (new File("/data/app/" + pkgname + "-1.odex").exists()) {
                                    Utils.run_all("rm /data/app/" + pkgname + "-1.odex");
                                }
                                if (new File("/data/app/" + pkgname + "-2.odex").exists()) {
                                    Utils.run_all("rm /data/app/" + pkgname + "-2.odex");
                                }
                                if (new File("/data/app/" + pkgname + ".odex").exists()) {
                                    Utils.run_all("rm /data/app/" + pkgname + ".odex");
                                }
                            }
                            if (pkgname.contains(Common.GOOGLEPLAY_PKG)) {
                                if (listAppsFragment.getPkgMng().getComponentEnabledSetting(new ComponentName(ctx, LicensingService.class)) == 2) {
                                    Utils.market_licensing_services(true);
                                    listAppsFragment.getPkgMng().setComponentEnabledSetting(new ComponentName(listAppsFragment.getInstance(), LicensingService.class), 2, 1);
                                } else {
                                    listAppsFragment.getPkgMng().setComponentEnabledSetting(new ComponentName(listAppsFragment.getInstance(), LicensingService.class), 1, 1);
                                }
                                if (listAppsFragment.getPkgMng().getComponentEnabledSetting(new ComponentName(ctx, MarketBillingService.class)) == 2 || listAppsFragment.getPkgMng().getComponentEnabledSetting(new ComponentName(ctx, InAppBillingService.class)) == 2) {
                                    Utils.market_billing_services(true);
                                    listAppsFragment.getPkgMng().setComponentEnabledSetting(new ComponentName(listAppsFragment.getInstance(), MarketBillingService.class), 2, 1);
                                    listAppsFragment.getPkgMng().setComponentEnabledSetting(new ComponentName(listAppsFragment.getInstance(), InAppBillingService.class), 2, 1);
                                } else {
                                    listAppsFragment.getPkgMng().setComponentEnabledSetting(new ComponentName(listAppsFragment.getInstance(), MarketBillingService.class), 1, 1);
                                    listAppsFragment.getPkgMng().setComponentEnabledSetting(new ComponentName(listAppsFragment.getInstance(), InAppBillingService.class), 1, 1);
                                }
                            }
                            listAppsFragment.refresh = true;
                            String pkgName = PackageChangeReceiver.this.getPackageName(intent);
                            if (listAppsFragment.database == null) {
                                listAppsFragment.database = new DatabaseHelper(listAppsFragment.getInstance());
                            }
                            listAppsFragment.database.deletePackage(pkgName);
                            if (!(!listAppsFragment.desktop_launch || listAppsFragment.plia == null || listAppsFragment.frag == null || listAppsFragment.frag.getActivity() == null)) {
                                str = pkgName;
                                listAppsFragment.frag.getActivity().runOnUiThread(new Runnable() {
                                    public void run() {
                                        if (listAppsFragment.plia.checkItem(str)) {
                                            listAppsFragment.plia.remove(str);
                                        }
                                        listAppsFragment.plia.notifyDataSetChanged();
                                        listAppsFragment.plia.sort();
                                    }
                                });
                            }
                        } catch (RuntimeException e3222) {
                            e3222.printStackTrace();
                        }
                    }
                }
                if (!listAppsFragment.patchOnBoot && !listAppsFragment.desktop_launch) {
                    System.out.println("LP - exit.");
                }
            }
        }).start();
    }

    public void connectToBilling() {
        Intent serviceIntent;
        if (this.mSetupDone) {
            serviceIntent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
            try {
                if (mServiceConn != null) {
                    listAppsFragment.getInstance().unbindService(mServiceConn);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            this.mSetupDone = false;
            listAppsFragment.getInstance().bindService(serviceIntent, mServiceConn, 1);
        }
        mServiceConn = new C01242();
        serviceIntent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
        if (listAppsFragment.getPkgMng().queryIntentServices(serviceIntent, 0).isEmpty()) {
            System.out.println("Billing service unavailable on device.");
            if (listAppsFragment.su) {
                Utils.market_billing_services(true);
                listAppsFragment.getInstance().bindService(serviceIntent, mServiceConn, 1);
                Intent in = new Intent(listAppsFragment.getInstance(), inapp_widget.class);
                in.setAction(inapp_widget.ACTION_WIDGET_RECEIVER_Updater);
                listAppsFragment.getInstance().sendBroadcast(in);
                return;
            }
            return;
        }
        listAppsFragment.getInstance().bindService(serviceIntent, mServiceConn, 1);
    }

    public void connectToLicensing() {
        mServiceConnL = new C01283();
        if (this.mServiceL == null) {
            try {
                if (!listAppsFragment.getInstance().bindService(new Intent(new String(Base64.decode("Y29tLmFuZHJvaWQudmVuZGluZy5saWNlbnNpbmcuSUxpY2Vuc2luZ1NlcnZpY2U="))), mServiceConnL, 1)) {
                    cleanupService();
                }
            } catch (SecurityException e) {
                cleanupService();
            } catch (Base64DecoderException e2) {
                e2.printStackTrace();
                cleanupService();
            }
        }
    }

    private void cleanupService() {
        if (this.mServiceL != null) {
            try {
                listAppsFragment.getInstance().unbindService(mServiceConnL);
            } catch (IllegalArgumentException e) {
            }
            this.mServiceL = null;
        }
    }

    public String backup(String pkgName) {
        String result_file = BuildConfig.VERSION_NAME;
        if (!testSD()) {
            return result_file;
        }
        new File(listAppsFragment.basepath + "/Backup/").mkdirs();
        String backupName = BuildConfig.VERSION_NAME;
        try {
            if (listAppsFragment.getConfig().getInt("apkname", 0) != 0) {
                backupName = listAppsFragment.basepath + "/Backup/" + pkgName + ".ver." + listAppsFragment.getPkgMng().getPackageInfo(pkgName, 0).versionName.replaceAll(" ", ".") + ".build." + listAppsFragment.getPkgMng().getPackageInfo(pkgName, 0).versionCode + ".apk";
            } else {
                backupName = listAppsFragment.basepath + "/Backup/" + listAppsFragment.getPkgMng().getPackageInfo(pkgName, 0).applicationInfo.loadLabel(listAppsFragment.getPkgMng()).toString() + ".ver." + listAppsFragment.getPkgMng().getPackageInfo(pkgName, 0).versionName.replaceAll(" ", ".") + ".build." + listAppsFragment.getPkgMng().getPackageInfo(pkgName, 0).versionCode + ".apk";
            }
            if (new File(backupName).exists()) {
                new File(backupName).delete();
            }
            try {
                Utils.copyFile(new File(listAppsFragment.getPkgMng().getPackageInfo(pkgName, 0).applicationInfo.sourceDir), new File(backupName));
            } catch (Exception e) {
                Utils.copyFile(listAppsFragment.getPkgMng().getPackageInfo(pkgName, 0).applicationInfo.sourceDir, backupName, false, false);
                e.printStackTrace();
            } catch (NameNotFoundException e2) {
                e2.printStackTrace();
                return result_file;
            }
            if (!new File(backupName).exists()) {
                new Utils(BuildConfig.VERSION_NAME).cmdRoot("dd if=" + listAppsFragment.getPkgMng().getPackageInfo(pkgName, 0).applicationInfo.sourceDir + " of=" + backupName);
                Utils.run_all("chmod 777 " + backupName);
            }
            if (new File(backupName).exists()) {
                this.handler.post(new C01294());
                return backupName;
            }
            this.handler.post(new C01305());
            return result_file;
        } catch (NameNotFoundException e22) {
            e22.printStackTrace();
            return result_file;
        } catch (Exception e3) {
            e3.printStackTrace();
            return result_file;
        }
    }

    public boolean testSD() {
        try {
            if (listAppsFragment.basepath.startsWith(listAppsFragment.getInstance().getDir("sdcard", 0).getAbsolutePath())) {
                System.out.println("LuckyAppManager (sdcard to internal memory): " + listAppsFragment.basepath);
                return false;
            }
            if (!new File(listAppsFragment.basepath).exists()) {
                new File(listAppsFragment.basepath).mkdirs();
            }
            if (new File(listAppsFragment.basepath).exists()) {
                new File(listAppsFragment.basepath + "/tmp.txt").delete();
                System.out.println("LuckyAppManager (sdcard test create file): " + listAppsFragment.basepath);
                if (!new File(listAppsFragment.basepath + "/tmp.txt").createNewFile()) {
                    return false;
                }
                System.out.println("LuckyAppManager (sdcard test create file true): " + listAppsFragment.basepath);
                new File(listAppsFragment.basepath + "/tmp.txt").delete();
                return true;
            }
            System.out.println("LuckyAppManager (sdcard directory not found and not created): " + listAppsFragment.basepath);
            return false;
        } catch (IOException e) {
            return false;
        }
    }
}
