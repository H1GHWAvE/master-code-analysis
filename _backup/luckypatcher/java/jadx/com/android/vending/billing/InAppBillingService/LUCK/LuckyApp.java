package com.android.vending.billing.InAppBillingService.LUCK;

import android.app.Application;
import android.content.Context;
import java.lang.Thread.UncaughtExceptionHandler;

public class LuckyApp extends Application {
    static Context instance = null;

    class C00901 implements UncaughtExceptionHandler {
        C00901() {
        }

        public void uncaughtException(Thread arg0, Throwable arg1) {
            System.out.println("FATAL Exception LP " + arg1.toString());
            try {
                listAppsFragment.init();
            } catch (Exception e) {
                e.printStackTrace();
            }
            arg1.printStackTrace();
            boolean noStart = false;
            if (LuckyApp.this.getSharedPreferences("config", 4).getBoolean("force_close", false)) {
                noStart = true;
            }
            LuckyApp.this.getSharedPreferences("config", 4).edit().putBoolean("force_close", true).commit();
            try {
                new LogCollector().collect(LuckyApp.instance, true);
                if (!noStart) {
                    LuckyApp.this.startActivity(listAppsFragment.getPkgMng().getLaunchIntentForPackage(LuckyApp.this.getPackageName()));
                }
            } catch (RuntimeException e2) {
                e2.printStackTrace();
            } catch (Exception e3) {
                System.exit(0);
                e3.printStackTrace();
            }
            System.exit(0);
        }
    }

    public void onCreate() {
        super.onCreate();
        instance = getApplicationContext();
        Thread.setDefaultUncaughtExceptionHandler(new C00901());
    }

    public static Context getInstance() {
        return instance;
    }
}
