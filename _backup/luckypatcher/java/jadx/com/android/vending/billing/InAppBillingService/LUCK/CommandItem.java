package com.android.vending.billing.InAppBillingService.LUCK;

public class CommandItem {
    public byte[] Method = null;
    public byte[] Object = null;
    public boolean bits32 = false;
    public boolean found_id_object = false;
    public boolean found_index_command = false;
    public byte[] id_object = null;
    public byte[] index_command = null;
    public String method = null;
    public String object = null;

    public CommandItem(String obj, String met) {
        this.object = obj;
        this.method = met;
        this.Object = new byte[4];
        this.Method = new byte[4];
        this.bits32 = false;
        this.id_object = new byte[2];
        this.index_command = new byte[2];
    }
}
