package com.android.vending.billing.InAppBillingService.LUCK.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import com.android.vending.billing.InAppBillingService.LUCK.AlertDlg;
import com.android.vending.billing.InAppBillingService.LUCK.C0149R;
import com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment;
import com.chelpus.Utils;

public class AdvancedFilter {
    public static final int LOADING_PROGRESS_DIALOG = 23;
    AdvancedFilter adv;
    Dialog dialog;

    class C01661 implements OnCheckedChangeListener {
        C01661() {
        }

        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if (checkedId == C0149R.id.nofilter) {
                listAppsFragment.advancedFilter = 0;
            } else {
                listAppsFragment.advancedFilter = checkedId;
            }
        }
    }

    class C01672 implements OnClickListener {
        C01672() {
        }

        public void onClick(View v) {
            listAppsFragment.frag.app_scanning();
            AdvancedFilter.this.adv.dismiss();
        }
    }

    class C01683 implements OnClickListener {
        C01683() {
        }

        public void onClick(View v) {
            AdvancedFilter.this.adv.dismiss();
        }
    }

    class C01694 implements OnCancelListener {
        C01694() {
        }

        public void onCancel(DialogInterface dialog) {
        }
    }

    public AdvancedFilter() {
        this.adv = null;
        this.dialog = null;
        this.adv = this;
    }

    public void showDialog() {
        if (this.dialog == null) {
            this.dialog = onCreateDialog();
        }
        if (this.dialog != null) {
            this.dialog.show();
        }
    }

    public Dialog onCreateDialog() {
        System.out.println("Market install Dialog create.");
        if (listAppsFragment.frag == null || listAppsFragment.frag.getContext() == null) {
            dismiss();
        }
        LinearLayout d = (LinearLayout) View.inflate(listAppsFragment.frag.getContext(), C0149R.layout.advanced_filter, null);
        LinearLayout scrbody = (LinearLayout) d.findViewById(C0149R.id.filterbodyscroll).findViewById(C0149R.id.appdialogbody);
        TextView text = (TextView) scrbody.findViewById(C0149R.id.app_textView);
        RadioGroup group = (RadioGroup) scrbody.findViewById(C0149R.id.radioGroup1);
        group.getCheckedRadioButtonId();
        RadioButton nofilter = (RadioButton) group.findViewById(C0149R.id.nofilter);
        nofilter.setText(Utils.getText(C0149R.string.nofilter));
        if (listAppsFragment.advancedFilter == 0 || listAppsFragment.advancedFilter == C0149R.id.nofilter) {
            nofilter.setChecked(true);
        }
        RadioButton filter0 = (RadioButton) group.findViewById(C0149R.id.filter0);
        filter0.setText(Utils.getText(C0149R.string.filter0));
        if (listAppsFragment.advancedFilter == C0149R.id.filter0) {
            filter0.setChecked(true);
        }
        RadioButton filter1 = (RadioButton) group.findViewById(C0149R.id.filter1);
        filter1.setText(Utils.getText(C0149R.string.filter1));
        if (listAppsFragment.advancedFilter == C0149R.id.filter1) {
            filter1.setChecked(true);
        }
        RadioButton filter2 = (RadioButton) group.findViewById(C0149R.id.filter2);
        filter2.setText(Utils.getText(C0149R.string.filter2));
        if (listAppsFragment.advancedFilter == C0149R.id.filter2) {
            filter2.setChecked(true);
        }
        RadioButton filter11 = (RadioButton) group.findViewById(C0149R.id.filter11);
        filter11.setText(Utils.getText(C0149R.string.filter11));
        if (listAppsFragment.advancedFilter == C0149R.id.filter11) {
            filter11.setChecked(true);
        }
        RadioButton filter12 = (RadioButton) group.findViewById(C0149R.id.filter12);
        filter12.setText(Utils.getText(C0149R.string.filter12));
        if (listAppsFragment.advancedFilter == C0149R.id.filter12) {
            filter12.setChecked(true);
        }
        RadioButton filter13 = (RadioButton) group.findViewById(C0149R.id.filter13);
        filter13.setText(Utils.getText(C0149R.string.filter13));
        if (listAppsFragment.advancedFilter == C0149R.id.filter13) {
            filter13.setChecked(true);
        }
        RadioButton filter4 = (RadioButton) group.findViewById(C0149R.id.filter4);
        filter4.setText(Utils.getText(C0149R.string.filter4));
        if (listAppsFragment.advancedFilter == C0149R.id.filter4) {
            filter4.setChecked(true);
        }
        RadioButton filter5 = (RadioButton) group.findViewById(C0149R.id.filter5);
        filter5.setText(Utils.getText(C0149R.string.filter5));
        if (listAppsFragment.advancedFilter == C0149R.id.filter5) {
            filter5.setChecked(true);
        }
        RadioButton filter6 = (RadioButton) group.findViewById(C0149R.id.filter6);
        filter6.setText(Utils.getText(C0149R.string.filter6));
        if (listAppsFragment.advancedFilter == C0149R.id.filter6) {
            filter6.setChecked(true);
        }
        RadioButton filter7 = (RadioButton) group.findViewById(C0149R.id.filter7);
        filter7.setText(Utils.getText(C0149R.string.filter7));
        if (listAppsFragment.advancedFilter == C0149R.id.filter7) {
            filter7.setChecked(true);
        }
        RadioButton filter8 = (RadioButton) group.findViewById(C0149R.id.filter8);
        filter8.setText(Utils.getText(C0149R.string.filter8));
        if (listAppsFragment.advancedFilter == C0149R.id.filter8) {
            filter8.setChecked(true);
        }
        RadioButton filter9 = (RadioButton) group.findViewById(C0149R.id.filter9);
        filter9.setText(Utils.getText(C0149R.string.filter9));
        if (listAppsFragment.advancedFilter == C0149R.id.filter9) {
            filter9.setChecked(true);
        }
        RadioButton filter10 = (RadioButton) group.findViewById(C0149R.id.filter10);
        filter10.setText(Utils.getText(C0149R.string.filter10));
        if (listAppsFragment.advancedFilter == C0149R.id.filter10) {
            filter10.setChecked(true);
        }
        group.setOnCheckedChangeListener(new C01661());
        ((Button) d.findViewById(C0149R.id.ok_button)).setOnClickListener(new C01672());
        ((Button) d.findViewById(C0149R.id.cancel_button)).setOnClickListener(new C01683());
        Dialog tempdialog = new AlertDlg(listAppsFragment.frag.getContext(), true).setView(d).create();
        tempdialog.setCancelable(true);
        tempdialog.setOnCancelListener(new C01694());
        return tempdialog;
    }

    public void dismiss() {
        if (this.dialog != null) {
            this.dialog.dismiss();
            this.dialog = null;
        }
    }
}
