package com.android.vending.billing.InAppBillingService.LUCK.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import com.android.vending.billing.InAppBillingService.LUCK.AlertDlg;
import com.android.vending.billing.InAppBillingService.LUCK.C0149R;
import com.android.vending.billing.InAppBillingService.LUCK.FileApkListItem;
import com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment;
import com.chelpus.Common;
import com.chelpus.Utils;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import org.tukaani.xz.common.Util;

public class Market_Install_Dialog {
    public static final int LOADING_PROGRESS_DIALOG = 23;
    Dialog dialog = null;

    class C02031 implements OnCheckedChangeListener {
        C02031() {
        }

        public void onCheckedChanged(RadioGroup group, int checkedId) {
            switch (checkedId) {
                case C0149R.id.radio53:
                    listAppsFragment.verMarket = "mod.market53.apk";
                    return;
                case C0149R.id.radio50:
                    listAppsFragment.verMarket = "mod.market5.apk";
                    return;
                case C0149R.id.radio44:
                    listAppsFragment.verMarket = "mod.market4.4.apk";
                    return;
                case C0149R.id.radio4:
                    listAppsFragment.verMarket = "mod.market4.apk";
                    return;
                case C0149R.id.radio0:
                    listAppsFragment.verMarket = "mod.market3.apk";
                    return;
                case C0149R.id.radio1:
                    listAppsFragment.verMarket = "mod.market2.apk";
                    return;
                case C0149R.id.radio53o:
                    listAppsFragment.verMarket = "market53.apk";
                    return;
                case C0149R.id.radio50o:
                    listAppsFragment.verMarket = "market5.apk";
                    return;
                case C0149R.id.radio44o:
                    listAppsFragment.verMarket = "market4.4.apk";
                    return;
                case C0149R.id.radio5:
                    listAppsFragment.verMarket = "market4.apk";
                    return;
                case C0149R.id.radio2:
                    listAppsFragment.verMarket = "market3.apk";
                    return;
                case C0149R.id.radio3:
                    listAppsFragment.verMarket = "market2.apk";
                    return;
                default:
                    return;
            }
        }
    }

    class C02042 implements OnClickListener {
        C02042() {
        }

        public void onClick(View arg0) {
            CheckBox chk = (CheckBox) arg0;
            if (!listAppsFragment.asNeedUser) {
                listAppsFragment.frag.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.install_as_user_app_note));
                chk.setChecked(false);
                listAppsFragment.asUser = false;
            } else if (chk.isChecked()) {
                listAppsFragment.asUser = true;
            } else {
                listAppsFragment.asUser = false;
            }
        }
    }

    class C02063 implements OnClickListener {

        class C02051 extends AsyncTask<String, Integer, Boolean> {
            boolean cacheFound = false;
            boolean corruptdownload = false;
            String filename = "mod.market4.apk";
            boolean internet_not_found = false;
            int numbytes = 0;

            C02051() {
            }

            protected void onPreExecute() {
                super.onPreExecute();
                listAppsFragment com_android_vending_billing_InAppBillingService_LUCK_listAppsFragment = listAppsFragment.frag;
                listAppsFragment.showDialogLP(Market_Install_Dialog.LOADING_PROGRESS_DIALOG);
                listAppsFragment.progress_loading.setTitle(Utils.getText(C0149R.string.download));
                listAppsFragment.progress_loading.setMessage(Utils.getText(C0149R.string.wait));
                listAppsFragment.progress_loading.setCancelable(true);
            }

            protected Boolean doInBackground(String... params) {
                String urlpath = "http://chelpus.defcon5.biz/" + params[0];
                this.filename = params[0];
                try {
                    HttpURLConnection con = (HttpURLConnection) new URL(urlpath).openConnection();
                    con.setRequestMethod("HEAD");
                    con.setRequestProperty("Cache-Control", "no-cache");
                    con.connect();
                    this.numbytes = Integer.parseInt(con.getHeaderField("Content-length"));
                    System.out.println(String.format("%s bytes found, %s Mb", new Object[]{Integer.valueOf(this.numbytes), Float.valueOf(((float) this.numbytes) / 1048576.0f)}));
                    con.disconnect();
                } catch (NumberFormatException e) {
                    this.internet_not_found = true;
                } catch (Exception e2) {
                    e2.printStackTrace();
                    this.internet_not_found = true;
                }
                if (listAppsFragment.progress_loading != null && listAppsFragment.progress_loading.isShowing()) {
                    listAppsFragment.progress_loading.setMax(this.numbytes / Util.BLOCK_HEADER_SIZE_MAX);
                }
                this.cacheFound = false;
                if (new File(listAppsFragment.basepath + "/Download/" + params[0]).exists()) {
                    if (this.internet_not_found) {
                        this.cacheFound = true;
                    } else {
                        System.out.println(new File(listAppsFragment.basepath + "/Download/" + params[0]).length());
                        System.out.println(this.numbytes);
                        if (new File(listAppsFragment.basepath + "/Download/" + params[0]).length() == ((long) this.numbytes) || this.numbytes == 0) {
                            this.cacheFound = true;
                        }
                    }
                }
                if (!this.internet_not_found && !this.cacheFound) {
                    try {
                        HttpURLConnection c = (HttpURLConnection) new URL(urlpath).openConnection();
                        c.setRequestMethod("GET");
                        c.setUseCaches(false);
                        c.setRequestProperty("Cache-Control", "no-cache");
                        c.connect();
                        File file = new File(listAppsFragment.basepath + "/Download/");
                        if (!file.exists()) {
                            file.mkdirs();
                        }
                        File outputFile = new File(file, params[0]);
                        if (outputFile.exists()) {
                            outputFile.delete();
                        }
                        FileOutputStream fos = new FileOutputStream(outputFile);
                        InputStream is = c.getInputStream();
                        byte[] buffer = new byte[AccessibilityNodeInfoCompat.ACTION_SCROLL_BACKWARD];
                        int size = 0;
                        while (true) {
                            int len1 = is.read(buffer);
                            if (len1 == -1) {
                                break;
                            }
                            size += len1;
                            publishProgress(new Integer[]{Integer.valueOf(size / Util.BLOCK_HEADER_SIZE_MAX)});
                            fos.write(buffer, 0, len1);
                        }
                        fos.close();
                        is.close();
                    } catch (IOException e3) {
                        e3.printStackTrace();
                    }
                    if (new File(listAppsFragment.basepath + "/Download/" + params[0]).exists()) {
                        if (new File(listAppsFragment.basepath + "/Download/" + params[0]).length() == ((long) this.numbytes)) {
                            this.cacheFound = true;
                        } else {
                            new File(listAppsFragment.basepath + "/Download/" + params[0]).delete();
                            this.corruptdownload = true;
                            this.cacheFound = false;
                        }
                    }
                } else if (new File(listAppsFragment.basepath + "/Download/" + params[0]).exists() && !this.cacheFound) {
                    new File(listAppsFragment.basepath + "/Download/" + params[0]).delete();
                }
                return Boolean.valueOf(true);
            }

            protected void onProgressUpdate(Integer... values) {
                super.onProgressUpdate(values);
                if (listAppsFragment.progress_loading != null && listAppsFragment.progress_loading.isShowing()) {
                    listAppsFragment.progress_loading.setProgress(values[0].intValue());
                }
            }

            protected void onPostExecute(Boolean result) {
                super.onPostExecute(result);
                try {
                    if (listAppsFragment.progress_loading != null) {
                        listAppsFragment.progress_loading.dismiss();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (this.corruptdownload) {
                    listAppsFragment.frag.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.corrupt_download));
                }
                if (this.internet_not_found && !this.cacheFound) {
                    listAppsFragment.frag.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.internet_not_found));
                }
                if (this.cacheFound && new File(listAppsFragment.basepath + "/Download/" + this.filename).exists()) {
                    try {
                        FileApkListItem mrt = new FileApkListItem(listAppsFragment.getInstance(), new File(listAppsFragment.basepath + "/Download/" + this.filename), false);
                        if (listAppsFragment.asUser) {
                            listAppsFragment.frag.toolbar_restore(mrt, true);
                        } else if (Utils.isInstalledOnSdCard(listAppsFragment.getInstance().getPackageName())) {
                            listAppsFragment.frag.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.error_app_installed_on_sdcard_block));
                        } else {
                            listAppsFragment.frag.install_system(mrt);
                        }
                    } catch (Exception e2) {
                        e2.printStackTrace();
                        listAppsFragment.frag.showMessage(Utils.getText(C0149R.string.warning), Utils.getText(C0149R.string.corrupt_download));
                    }
                }
            }
        }

        C02063() {
        }

        public void onClick(View v) {
            new C02051().execute(new String[]{listAppsFragment.verMarket});
        }
    }

    class C02074 implements OnCancelListener {
        C02074() {
        }

        public void onCancel(DialogInterface dialog) {
        }
    }

    public void showDialog() {
        if (this.dialog == null) {
            this.dialog = onCreateDialog();
        }
        if (this.dialog != null) {
            this.dialog.show();
        }
    }

    public Dialog onCreateDialog() {
        System.out.println("Market install Dialog create.");
        if (listAppsFragment.frag == null || listAppsFragment.frag.getContext() == null) {
            dismiss();
        }
        LinearLayout d = (LinearLayout) View.inflate(listAppsFragment.frag.getContext(), C0149R.layout.gm_installer, null);
        LinearLayout scrbody = (LinearLayout) d.findViewById(C0149R.id.marketbodyscroll).findViewById(C0149R.id.appdialogbody);
        TextView text = (TextView) scrbody.findViewById(C0149R.id.market_textView);
        RadioGroup group = (RadioGroup) scrbody.findViewById(C0149R.id.radioGroup1);
        group.getCheckedRadioButtonId();
        listAppsFragment.verMarket = "mod.market53.apk";
        ((RadioButton) group.findViewById(C0149R.id.radio4)).setText(Utils.getText(C0149R.string.market_version5) + " 4.1.6 (Android 2.2 and UP)");
        ((RadioButton) group.findViewById(C0149R.id.radio5)).setText(Utils.getText(C0149R.string.market_version6) + " 4.1.6 (Android 2.2 and UP)");
        ((RadioButton) group.findViewById(C0149R.id.radio44)).setText(Utils.getText(C0149R.string.market_version5) + " 4.9.13 (Android 2.2 and UP)");
        ((RadioButton) group.findViewById(C0149R.id.radio44o)).setText(Utils.getText(C0149R.string.market_version6) + " 4.9.13 (Android 2.2 and UP)");
        ((RadioButton) group.findViewById(C0149R.id.radio50)).setText(Utils.getText(C0149R.string.market_version5) + " 5.1.11 (Android 2.2 and UP)");
        ((RadioButton) group.findViewById(C0149R.id.radio50o)).setText(Utils.getText(C0149R.string.market_version6) + " 5.1.11 (Android 2.2 and UP)");
        ((RadioButton) group.findViewById(C0149R.id.radio53)).setText(Utils.getText(C0149R.string.market_version5) + " 5.3.6 (Android 2.2 and UP)");
        ((RadioButton) group.findViewById(C0149R.id.radio53o)).setText(Utils.getText(C0149R.string.market_version6) + " 5.3.6 (Android 2.2 and UP)");
        group.setOnCheckedChangeListener(new C02031());
        Button button = (Button) d.findViewById(C0149R.id.market_button);
        CheckBox chk = (CheckBox) d.findViewById(C0149R.id.checkUserInstall);
        listAppsFragment.asUser = false;
        listAppsFragment.asNeedUser = false;
        try {
            ApplicationInfo ai = listAppsFragment.getPkgMng().getApplicationInfo(Common.GOOGLEPLAY_PKG, 0);
            Dialog tempdialog;
            if (Utils.checkCoreJarPatch11() && Utils.checkCoreJarPatch12() && Utils.checkCoreJarPatch20() && (ai.flags & 1) != 0) {
                chk.setEnabled(true);
                chk.setChecked(false);
                listAppsFragment.asUser = false;
                listAppsFragment.asNeedUser = true;
                chk.setOnClickListener(new C02042());
                text.setText(Utils.getColoredText("  " + Utils.getText(C0149R.string.internet_not_found), MotionEventCompat.ACTION_POINTER_INDEX_MASK, "bold"));
                text.append("\n\n" + Utils.getText(C0149R.string.market_description_text));
                button.setOnClickListener(new C02063());
                tempdialog = new AlertDlg(listAppsFragment.frag.getContext(), true).setView(d).create();
                tempdialog.setCancelable(true);
                tempdialog.setOnCancelListener(new C02074());
                return tempdialog;
            }
            listAppsFragment.asUser = false;
            listAppsFragment.asNeedUser = false;
            chk.setEnabled(true);
            chk.setChecked(false);
            chk.setOnClickListener(new C02042());
            text.setText(Utils.getColoredText("  " + Utils.getText(C0149R.string.internet_not_found), MotionEventCompat.ACTION_POINTER_INDEX_MASK, "bold"));
            text.append("\n\n" + Utils.getText(C0149R.string.market_description_text));
            button.setOnClickListener(new C02063());
            tempdialog = new AlertDlg(listAppsFragment.frag.getContext(), true).setView(d).create();
            tempdialog.setCancelable(true);
            tempdialog.setOnCancelListener(new C02074());
            return tempdialog;
        } catch (NameNotFoundException e) {
            listAppsFragment.asUser = false;
            listAppsFragment.asNeedUser = false;
            chk.setEnabled(true);
            chk.setChecked(false);
        } catch (Exception e2) {
            e2.printStackTrace();
            listAppsFragment.asUser = false;
            listAppsFragment.asNeedUser = false;
            chk.setEnabled(true);
            chk.setChecked(false);
        }
    }

    public void dismiss() {
        if (this.dialog != null) {
            this.dialog.dismiss();
            this.dialog = null;
        }
    }
}
