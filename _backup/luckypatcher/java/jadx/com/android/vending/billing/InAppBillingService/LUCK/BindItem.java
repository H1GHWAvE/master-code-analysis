package com.android.vending.billing.InAppBillingService.LUCK;

public class BindItem {
    public String SourceDir = BuildConfig.VERSION_NAME;
    public String TargetDir = BuildConfig.VERSION_NAME;

    public BindItem(String source, String Target) {
        this.SourceDir = source;
        this.TargetDir = Target;
    }

    public BindItem(String str) {
        String[] tails = str.split(";");
        if (tails.length == 2) {
            this.SourceDir = tails[0];
            this.TargetDir = tails[1];
        }
    }

    public String toString() {
        return this.SourceDir + ";" + this.TargetDir;
    }
}
