package com.android.vending.billing.InAppBillingService.LUCK;

import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.lang.reflect.Method;
import java.net.URL;
import java.security.DigestOutputStream;
import java.security.GeneralSecurityException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyStore;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Provider;
import java.security.Security;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Observable;
import java.util.Observer;
import java.util.TreeMap;
import java.util.jar.Attributes;
import java.util.jar.JarEntry;
import java.util.jar.JarOutputStream;
import java.util.jar.Manifest;
import java.util.regex.Pattern;
import java.util.zip.CRC32;
import java.util.zip.ZipEntry;
import javax.crypto.Cipher;
import javax.crypto.EncryptedPrivateKeyInfo;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import kellinwood.logging.LoggerInterface;
import kellinwood.logging.LoggerManager;
import kellinwood.security.zipsigner.AutoKeyException;
import kellinwood.security.zipsigner.Base64;
import kellinwood.security.zipsigner.HexDumpEncoder;
import kellinwood.security.zipsigner.KeySet;
import kellinwood.security.zipsigner.ProgressHelper;
import kellinwood.security.zipsigner.ProgressListener;
import kellinwood.security.zipsigner.ZipSignature;
import kellinwood.zipio.ZioEntry;
import kellinwood.zipio.ZipInput;
import kellinwood.zipio.ZipOutput;
import net.lingala.zip4j.util.InternalZipConstants;
import pxb.android.ResConst;

public class ZipSignerLP {
    private static final String CERT_RSA_NAME = "META-INF/CERT.RSA";
    private static final String CERT_SF_NAME = "META-INF/CERT.SF";
    public static final String KEY_NONE = "none";
    public static final String KEY_TESTKEY = "testkey";
    public static final String MODE_AUTO = "auto";
    public static final String MODE_AUTO_NONE = "auto-none";
    public static final String MODE_AUTO_TESTKEY = "auto-testkey";
    public static final String[] SUPPORTED_KEY_MODES = new String[]{MODE_AUTO_TESTKEY, MODE_AUTO, MODE_AUTO_NONE, "media", "platform", "shared", KEY_TESTKEY, KEY_NONE};
    static LoggerInterface log = null;
    private static Pattern stripPattern = Pattern.compile("^META-INF/(.*)[.](SF|RSA|DSA)$");
    Map<String, String> autoKeyDetect = new HashMap();
    AutoKeyObservable autoKeyObservable = new AutoKeyObservable();
    private boolean canceled = false;
    KeySet keySet = null;
    String keymode = KEY_TESTKEY;
    Map<String, KeySet> loadedKeys = new HashMap();
    private ProgressHelper progressHelper = new ProgressHelper();

    public static class AutoKeyObservable extends Observable {
        public void notifyObservers(Object arg) {
            super.setChanged();
            super.notifyObservers(arg);
        }
    }

    public static LoggerInterface getLogger() {
        if (log == null) {
            log = LoggerManager.getLogger(ZipSignerLP.class.getName());
        }
        return log;
    }

    public ZipSignerLP() {
        this.autoKeyDetect.put("aa9852bc5a53272ac8031d49b65e4b0e", "media");
        this.autoKeyDetect.put("e60418c4b638f20d0721e115674ca11f", "platform");
        this.autoKeyDetect.put("3e24e49741b60c215c010dc6048fca7d", "shared");
        this.autoKeyDetect.put("dab2cead827ef5313f28e22b6fa8479f", KEY_TESTKEY);
    }

    public void addAutoKeyObserver(Observer o) {
        this.autoKeyObservable.addObserver(o);
    }

    public String getKeymode() {
        return this.keymode;
    }

    public void setKeymode(String km) throws IOException, GeneralSecurityException {
        if (getLogger().isDebugEnabled()) {
            getLogger().debug("setKeymode: " + km);
        }
        this.keymode = km;
        if (this.keymode.startsWith(MODE_AUTO)) {
            this.keySet = null;
            return;
        }
        this.progressHelper.initProgress();
        loadKeys(this.keymode);
    }

    public static String[] getSupportedKeyModes() {
        return SUPPORTED_KEY_MODES;
    }

    protected String autoDetectKey(String mode, Map<String, ZioEntry> zioEntries) throws NoSuchAlgorithmException, IOException {
        boolean debug = getLogger().isDebugEnabled();
        if (!mode.startsWith(MODE_AUTO)) {
            return mode;
        }
        String keyName = null;
        for (Entry<String, ZioEntry> entry : zioEntries.entrySet()) {
            String entryName = (String) entry.getKey();
            if (entryName.startsWith("META-INF/") && entryName.endsWith(".RSA")) {
                MessageDigest md5 = MessageDigest.getInstance("MD5");
                byte[] entryData = ((ZioEntry) entry.getValue()).getData();
                if (entryData.length < 1458) {
                    break;
                }
                md5.update(entryData, 0, 1458);
                byte[] rawDigest = md5.digest();
                StringBuilder builder = new StringBuilder();
                int length = rawDigest.length;
                for (int i = 0; i < length; i++) {
                    builder.append(String.format("%02x", new Object[]{Byte.valueOf(rawDigest[i])}));
                }
                keyName = (String) this.autoKeyDetect.get(builder.toString());
                if (debug) {
                    if (keyName != null) {
                        getLogger().debug(String.format("Auto-determined key=%s using md5=%s", new Object[]{keyName, md5String}));
                    } else {
                        getLogger().debug(String.format("Auto key determination failed for md5=%s", new Object[]{md5String}));
                    }
                }
                if (keyName != null) {
                    return keyName;
                }
            }
        }
        if (mode.equals(MODE_AUTO_TESTKEY)) {
            if (debug) {
                getLogger().debug("Falling back to key=" + keyName);
            }
            return KEY_TESTKEY;
        }
        if (!mode.equals(MODE_AUTO_NONE)) {
            return null;
        }
        if (debug) {
            getLogger().debug("Unable to determine key, returning: none");
        }
        return KEY_NONE;
    }

    public void loadKeys(String name) throws IOException, GeneralSecurityException {
        this.keySet = (KeySet) this.loadedKeys.get(name);
        if (this.keySet == null) {
            this.keySet = new KeySet();
            this.keySet.setName(name);
            this.loadedKeys.put(name, this.keySet);
            if (!KEY_NONE.equals(name)) {
                this.progressHelper.progress(1, "Loading certificate and private key");
                this.keySet.setPrivateKey(readPrivateKey(getClass().getResource("/keys/" + name + ".pk8"), null));
                this.keySet.setPublicKey(readPublicKey(getClass().getResource("/keys/" + name + ".x509.pem")));
                URL sigBlockTemplateUrl = getClass().getResource("/keys/" + name + ".sbt");
                if (sigBlockTemplateUrl != null) {
                    this.keySet.setSigBlockTemplate(readContentAsBytes(sigBlockTemplateUrl));
                }
            }
        }
    }

    public void setKeys(String name, X509Certificate publicKey, PrivateKey privateKey, byte[] signatureBlockTemplate) {
        this.keySet = new KeySet(name, publicKey, privateKey, signatureBlockTemplate);
    }

    public KeySet getKeySet() {
        return this.keySet;
    }

    public void cancel() {
        this.canceled = true;
    }

    public void resetCanceled() {
        this.canceled = false;
    }

    public boolean isCanceled() {
        return this.canceled;
    }

    public void loadProvider(String providerClassName) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        Security.insertProviderAt((Provider) Class.forName(providerClassName).newInstance(), 1);
    }

    public X509Certificate readPublicKey(URL publicKeyUrl) throws IOException, GeneralSecurityException {
        InputStream input = publicKeyUrl.openStream();
        try {
            X509Certificate x509Certificate = (X509Certificate) CertificateFactory.getInstance("X.509").generateCertificate(input);
            return x509Certificate;
        } finally {
            input.close();
        }
    }

    private KeySpec decryptPrivateKey(byte[] encryptedPrivateKey, String keyPassword) throws GeneralSecurityException {
        try {
            EncryptedPrivateKeyInfo epkInfo = new EncryptedPrivateKeyInfo(encryptedPrivateKey);
            Key key = SecretKeyFactory.getInstance(epkInfo.getAlgName()).generateSecret(new PBEKeySpec(keyPassword.toCharArray()));
            Cipher cipher = Cipher.getInstance(epkInfo.getAlgName());
            cipher.init(2, key, epkInfo.getAlgParameters());
            try {
                return epkInfo.getKeySpec(cipher);
            } catch (InvalidKeySpecException ex) {
                getLogger().error("signapk: Password for private key may be bad.");
                throw ex;
            }
        } catch (IOException e) {
            return null;
        }
    }

    public byte[] readContentAsBytes(URL contentUrl) throws IOException {
        return readContentAsBytes(contentUrl.openStream());
    }

    public byte[] readContentAsBytes(InputStream input) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buffer = new byte[InternalZipConstants.UFT8_NAMES_FLAG];
        int numRead = input.read(buffer);
        while (numRead != -1) {
            baos.write(buffer, 0, numRead);
            numRead = input.read(buffer);
        }
        return baos.toByteArray();
    }

    public PrivateKey readPrivateKey(URL privateKeyUrl, String keyPassword) throws IOException, GeneralSecurityException {
        PrivateKey generatePrivate;
        InputStream input = new DataInputStream(privateKeyUrl.openStream());
        KeySpec spec;
        try {
            byte[] bytes = readContentAsBytes(input);
            spec = decryptPrivateKey(bytes, keyPassword);
            if (spec == null) {
                spec = new PKCS8EncodedKeySpec(bytes);
            }
            generatePrivate = KeyFactory.getInstance("RSA").generatePrivate(spec);
        } catch (InvalidKeySpecException e) {
            generatePrivate = KeyFactory.getInstance("DSA").generatePrivate(spec);
        } finally {
            input.close();
        }
        return generatePrivate;
    }

    private Manifest addDigestsToManifest(Map<String, ZioEntry> entries, ArrayList<AddFilesItem> modified_files) throws IOException, GeneralSecurityException {
        Exception e;
        Manifest input = null;
        ZioEntry manifestEntry = (ZioEntry) entries.get("META-INF/MANIFEST.MF");
        if (manifestEntry != null) {
            input = new Manifest();
            input.read(manifestEntry.getInputStream());
        }
        Manifest output = new Manifest();
        Attributes main = output.getMainAttributes();
        if (input != null) {
            main.putAll(input.getMainAttributes());
        } else {
            main.putValue("Manifest-Version", "1.0");
            main.putValue("Created-By", "1.0 (Android SignApk)");
        }
        MessageDigest md = MessageDigest.getInstance("SHA1");
        byte[] buffer = new byte[ResConst.RES_TABLE_PACKAGE_TYPE];
        TreeMap<String, ZioEntry> byName = new TreeMap();
        byName.putAll(entries);
        boolean debug = getLogger().isDebugEnabled();
        if (debug) {
            getLogger().debug("Manifest entries:");
        }
        for (ZioEntry entry : byName.values()) {
            if (this.canceled) {
                break;
            }
            String name = entry.getName();
            if (debug) {
                getLogger().debug(name);
            }
            if (!(entry.isDirectory() || name.equals("META-INF/MANIFEST.MF") || name.equals(CERT_SF_NAME) || name.equals(CERT_RSA_NAME))) {
                if (stripPattern == null || !stripPattern.matcher(name).matches()) {
                    this.progressHelper.progress(0, "Generating manifest");
                    InputStream data = entry.getInputStream();
                    Iterator it = modified_files.iterator();
                    while (it.hasNext()) {
                        AddFilesItem file = (AddFilesItem) it.next();
                        if (name.equals(file.fileName.replace(file.basePath, BuildConfig.VERSION_NAME))) {
                            try {
                                InputStream data2 = new FileInputStream(file.fileName);
                                try {
                                    System.out.println("LuckyPatcher (signer): Additional files to manifest added! " + file);
                                    data = data2;
                                } catch (Exception e2) {
                                    e = e2;
                                    data = data2;
                                    System.out.println(e);
                                }
                            } catch (Exception e3) {
                                e = e3;
                                System.out.println(e);
                            }
                        }
                    }
                    while (true) {
                        int num = data.read(buffer);
                        if (num <= 0) {
                            break;
                        }
                        md.update(buffer, 0, num);
                    }
                    data.close();
                    Attributes attributes = null;
                    if (input != null) {
                        Attributes inAttr = input.getAttributes(name);
                        if (inAttr != null) {
                            attributes = new Attributes(inAttr);
                        }
                    }
                    if (attributes == null) {
                        attributes = new Attributes();
                    }
                    attributes.putValue("SHA1-Digest", Base64.encode(md.digest()));
                    output.getEntries().put(name, attributes);
                }
            }
        }
        return output;
    }

    private void generateSignatureFile(Manifest manifest, OutputStream out) throws IOException, GeneralSecurityException {
        out.write("Signature-Version: 1.0\r\n".getBytes());
        out.write("Created-By: 1.0 (Android SignApk)\r\n".getBytes());
        MessageDigest md = MessageDigest.getInstance("SHA1");
        PrintStream print = new PrintStream(new DigestOutputStream(new ByteArrayOutputStream(), md), true, "UTF-8");
        manifest.write(print);
        print.flush();
        out.write(("SHA1-Digest-Manifest: " + Base64.encode(md.digest()) + "\r\n\r\n").getBytes());
        for (Entry<String, Attributes> entry : manifest.getEntries().entrySet()) {
            if (!this.canceled) {
                this.progressHelper.progress(0, "Generating signature file");
                String nameEntry = "Name: " + ((String) entry.getKey()) + "\r\n";
                print.print(nameEntry);
                for (Entry<Object, Object> att : ((Attributes) entry.getValue()).entrySet()) {
                    print.print(att.getKey() + ": " + att.getValue() + "\r\n");
                }
                print.print("\r\n");
                print.flush();
                out.write(nameEntry.getBytes());
                out.write(("SHA1-Digest: " + Base64.encode(md.digest()) + "\r\n\r\n").getBytes());
            } else {
                return;
            }
        }
    }

    private void writeSignatureBlock(byte[] signatureBlockTemplate, byte[] signatureBytes, X509Certificate publicKey, OutputStream out) throws IOException, GeneralSecurityException {
        if (signatureBlockTemplate != null) {
            out.write(signatureBlockTemplate);
            out.write(signatureBytes);
            return;
        }
        try {
            Method sigWriterMethod = Class.forName("kellinwood.sigblock.SignatureBlockWriter").getMethod("writeSignatureBlock", new Class[]{new byte[1].getClass(), X509Certificate.class, OutputStream.class});
            if (sigWriterMethod == null) {
                throw new IllegalStateException("writeSignatureBlock() method not found.");
            }
            sigWriterMethod.invoke(null, new Object[]{signatureBytes, publicKey, out});
        } catch (Exception x) {
            getLogger().error(x.getMessage(), x);
            throw new IllegalStateException("Failed to invoke writeSignatureBlock(): " + x.getClass().getName() + ": " + x.getMessage());
        }
    }

    private void copyFiles(Manifest manifest, Map<String, ZioEntry> input, JarOutputStream output, long timestamp, ArrayList<AddFilesItem> modified_files) throws IOException {
        Exception e;
        List<String> names = new ArrayList(manifest.getEntries().keySet());
        Collections.sort(names);
        for (String name : names) {
            ZioEntry inEntry = (ZioEntry) input.get(name);
            try {
                AddFilesItem file;
                File fileF;
                byte[] buffer;
                FileInputStream data;
                ZipEntry outEntry;
                int num;
                if (((ZioEntry) input.get(name)).getCompression() == (short) 0) {
                    ZipEntry outEntry2 = new JarEntry(((ZioEntry) input.get(name)).getName());
                    try {
                        CRC32 crc;
                        outEntry2.setMethod(0);
                        boolean mark2 = false;
                        Iterator it = modified_files.iterator();
                        while (it.hasNext()) {
                            file = (AddFilesItem) it.next();
                            if (name.equals(file.fileName.replace(file.basePath, BuildConfig.VERSION_NAME))) {
                                try {
                                    fileF = new File(file.fileName);
                                    buffer = new byte[((int) fileF.length())];
                                    data = new FileInputStream(file.fileName);
                                    data.read(buffer);
                                    data.close();
                                    outEntry2.setCompressedSize(fileF.length());
                                    outEntry2.setSize(fileF.length());
                                    crc = new CRC32();
                                    crc.update(buffer);
                                    outEntry2.setCrc(crc.getValue());
                                    outEntry2.setTime(((ZioEntry) input.get(name)).getTime());
                                    mark2 = true;
                                } catch (Exception e2) {
                                    System.out.println(e2);
                                }
                            }
                        }
                        if (!mark2) {
                            outEntry2.setCompressedSize((long) ((ZioEntry) input.get(name)).getSize());
                            outEntry2.setSize((long) ((ZioEntry) input.get(name)).getSize());
                            crc = new CRC32();
                            crc.update(inEntry.getData());
                            outEntry2.setCrc(crc.getValue());
                            outEntry2.setTime(((ZioEntry) input.get(name)).getTime());
                        }
                        outEntry = outEntry2;
                    } catch (Exception e3) {
                        e2 = e3;
                        JarEntry jarEntry = outEntry;
                    }
                } else {
                    JarEntry jarEntry2 = new JarEntry(name);
                    jarEntry2.setTime(((ZioEntry) input.get(name)).getTime());
                    jarEntry2.setMethod(inEntry.getCompression());
                    outEntry = jarEntry2;
                }
                boolean mark = false;
                Iterator it2 = modified_files.iterator();
                while (it2.hasNext()) {
                    file = (AddFilesItem) it2.next();
                    if (name.equals(file.fileName.replace(file.basePath, BuildConfig.VERSION_NAME))) {
                        try {
                            fileF = new File(file.fileName);
                            buffer = new byte[AccessibilityNodeInfoCompat.ACTION_SCROLL_BACKWARD];
                            data = new FileInputStream(file.fileName);
                            output.putNextEntry(outEntry);
                            while (true) {
                                num = data.read(buffer);
                                if (num <= 0) {
                                    break;
                                }
                                output.write(buffer, 0, num);
                            }
                            output.flush();
                            data.close();
                            mark = true;
                            fileF.delete();
                            System.out.println("LuckyPatcher (signer): Additional files added! " + file);
                        } catch (Exception e22) {
                            System.out.println(e22);
                        }
                    }
                }
                if (!mark) {
                    output.putNextEntry(outEntry);
                    InputStream data2 = inEntry.getInputStream();
                    buffer = new byte[AccessibilityNodeInfoCompat.ACTION_SCROLL_BACKWARD];
                    while (true) {
                        num = data2.read(buffer);
                        if (num <= 0) {
                            break;
                        }
                        output.write(buffer, 0, num);
                    }
                    output.flush();
                }
            } catch (Exception e4) {
                e22 = e4;
            }
        }
        return;
        System.out.println(e22);
        e22.printStackTrace();
    }

    private void copyFiles(Map<String, ZioEntry> input, ZipOutput output, ArrayList<AddFilesItem> modified_files) throws IOException {
        for (ZioEntry inEntry : input.values()) {
            String name = inEntry.getName();
            boolean mark = false;
            Iterator it = modified_files.iterator();
            while (it.hasNext()) {
                AddFilesItem file = (AddFilesItem) it.next();
                if (name.equals(file.fileName.replace(file.basePath, BuildConfig.VERSION_NAME))) {
                    try {
                        File fileF = new File(file.fileName);
                        byte[] buffer = new byte[AccessibilityNodeInfoCompat.ACTION_SCROLL_BACKWARD];
                        FileInputStream data = new FileInputStream(file.fileName);
                        ZioEntry addFile = new ZioEntry(name);
                        addFile.setCompression(inEntry.getCompression());
                        addFile.setTime(inEntry.getTime());
                        OutputStream str = addFile.getOutputStream();
                        CRC32 crc = new CRC32();
                        crc.reset();
                        while (true) {
                            int num = data.read(buffer);
                            if (num <= 0) {
                                break;
                            }
                            str.write(buffer, 0, num);
                            crc.update(buffer, 0, num);
                        }
                        str.flush();
                        data.close();
                        output.write(addFile);
                        mark = true;
                        fileF.delete();
                        System.out.println("LuckyPatcher (signer): Additional files added! " + file);
                    } catch (Exception e) {
                        System.out.println(e);
                    }
                }
            }
            if (!mark) {
                output.write(inEntry);
            }
        }
    }

    public void signZip(URL keystoreURL, String keystoreType, String keystorePw, String certAlias, String certPw, String inputZipFilename, String outputZipFilename, ArrayList<AddFilesItem> modified_files) throws ClassNotFoundException, IllegalAccessException, InstantiationException, IOException, GeneralSecurityException {
        InputStream keystoreStream = null;
        if (keystoreType == null) {
            try {
                keystoreType = KeyStore.getDefaultType();
            } catch (Throwable th) {
                if (keystoreStream != null) {
                    keystoreStream.close();
                }
            }
        }
        KeyStore keystore = KeyStore.getInstance(keystoreType);
        keystoreStream = keystoreURL.openStream();
        keystore.load(keystoreStream, keystorePw.toCharArray());
        setKeys("custom", (X509Certificate) keystore.getCertificate(certAlias), (PrivateKey) keystore.getKey(certAlias, certPw.toCharArray()), null);
        signZip(inputZipFilename, outputZipFilename, (ArrayList) modified_files);
        if (keystoreStream != null) {
            keystoreStream.close();
        }
    }

    public void signZip(Map<String, ZioEntry> zioEntries, String outputZipFilename, ArrayList<AddFilesItem> modified_files) throws IOException, GeneralSecurityException {
        this.progressHelper.initProgress();
        signZip(zioEntries, new FileOutputStream(outputZipFilename), outputZipFilename, modified_files);
    }

    public void signZip(String inputZipFilename, String outputZipFilename, ArrayList<AddFilesItem> modified_files) throws IOException, GeneralSecurityException {
        if (new File(inputZipFilename).getCanonicalFile().equals(new File(outputZipFilename).getCanonicalFile())) {
            throw new IllegalArgumentException("Input and output files are the same.  Specify a different name for the output.");
        }
        this.progressHelper.initProgress();
        this.progressHelper.progress(1, "Parsing the input's central directory");
        signZip(ZipInput.read(inputZipFilename).getEntries(), new FileOutputStream(outputZipFilename), outputZipFilename, modified_files);
    }

    public void signZip(Map<String, ZioEntry> zioEntries, OutputStream outputStream, String outputZipFilename, ArrayList<AddFilesItem> modified_files) throws IOException, GeneralSecurityException {
        Throwable th;
        ZipOutput zipOutput;
        boolean debug = getLogger().isDebugEnabled();
        this.progressHelper.initProgress();
        if (this.keySet == null) {
            if (this.keymode.startsWith(MODE_AUTO)) {
                String keyName = autoDetectKey(this.keymode, zioEntries);
                if (keyName == null) {
                    throw new AutoKeyException("Unable to auto-select key for signing " + new File(outputZipFilename).getName());
                }
                this.autoKeyObservable.notifyObservers(keyName);
                loadKeys(keyName);
            } else {
                throw new IllegalStateException("No keys configured for signing the file!");
            }
        }
        JarOutputStream outputJar;
        try {
            ZipOutput zipOutput2 = new ZipOutput(outputStream);
            try {
                outputJar = new JarOutputStream(new FileOutputStream(outputZipFilename));
            } catch (Throwable th2) {
                th = th2;
                outputJar = null;
                zipOutput = zipOutput2;
                outputJar.close();
                try {
                    new File(outputZipFilename).delete();
                } catch (Throwable t) {
                    getLogger().warning(t.getClass().getName() + ":" + t.getMessage());
                }
                throw th;
            }
            try {
                if (KEY_NONE.equals(this.keySet.getName())) {
                    this.progressHelper.setProgressTotalItems(zioEntries.size());
                    this.progressHelper.setProgressCurrentItem(0);
                    copyFiles(zioEntries, zipOutput2, modified_files);
                    zipOutput2.close();
                    if (!(outputJar == null || this.keymode.equals(KEY_NONE))) {
                        outputJar.close();
                    }
                    if (this.canceled && outputZipFilename != null) {
                        try {
                            new File(outputZipFilename).delete();
                            return;
                        } catch (Throwable t2) {
                            getLogger().warning(t2.getClass().getName() + ":" + t2.getMessage());
                            return;
                        }
                    }
                    return;
                }
                int progressTotalItems = 0;
                for (ZioEntry entry : zioEntries.values()) {
                    String name = entry.getName();
                    if (!entry.isDirectory()) {
                        if (!name.equals("META-INF/MANIFEST.MF")) {
                            if (!name.equals(CERT_SF_NAME)) {
                                if (!name.equals(CERT_RSA_NAME) && (stripPattern == null || !stripPattern.matcher(name).matches())) {
                                    progressTotalItems += 3;
                                }
                            }
                        }
                    }
                }
                this.progressHelper.setProgressTotalItems(progressTotalItems + 1);
                this.progressHelper.setProgressCurrentItem(0);
                long timestamp = this.keySet.getPublicKey().getNotBefore().getTime() + 3600000;
                Manifest manifest = addDigestsToManifest(zioEntries, modified_files);
                if (this.canceled) {
                    if (!(outputJar == null || this.keymode.equals(KEY_NONE))) {
                        outputJar.close();
                    }
                    if (this.canceled && outputZipFilename != null) {
                        try {
                            new File(outputZipFilename).delete();
                            return;
                        } catch (Throwable t22) {
                            getLogger().warning(t22.getClass().getName() + ":" + t22.getMessage());
                            return;
                        }
                    }
                    return;
                }
                JarEntry je = new JarEntry("META-INF/MANIFEST.MF");
                je.setTime(timestamp);
                outputJar.putNextEntry(je);
                manifest.write(outputJar);
                ZipSignature signature = new ZipSignature();
                signature.initSign(this.keySet.getPrivateKey());
                OutputStream out = new ByteArrayOutputStream();
                generateSignatureFile(manifest, out);
                if (this.canceled) {
                    if (!(outputJar == null || this.keymode.equals(KEY_NONE))) {
                        outputJar.close();
                    }
                    if (this.canceled && outputZipFilename != null) {
                        try {
                            new File(outputZipFilename).delete();
                            return;
                        } catch (Throwable t222) {
                            getLogger().warning(t222.getClass().getName() + ":" + t222.getMessage());
                            return;
                        }
                    }
                    return;
                }
                byte[] sfBytes = out.toByteArray();
                if (debug) {
                    getLogger().debug("Signature File: \n" + new String(sfBytes) + LogCollector.LINE_SEPARATOR + HexDumpEncoder.encode(sfBytes));
                }
                je = new JarEntry(CERT_SF_NAME);
                je.setTime(timestamp);
                outputJar.putNextEntry(je);
                outputJar.write(sfBytes);
                signature.update(sfBytes);
                byte[] signatureBytes = signature.sign();
                if (debug) {
                    MessageDigest md = MessageDigest.getInstance("SHA1");
                    md.update(sfBytes);
                    getLogger().debug("Sig File SHA1: \n" + HexDumpEncoder.encode(md.digest()));
                    getLogger().debug("Signature: \n" + HexDumpEncoder.encode(signatureBytes));
                    Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
                    cipher.init(2, this.keySet.getPublicKey());
                    getLogger().debug("Signature Decrypted: \n" + HexDumpEncoder.encode(cipher.doFinal(signatureBytes)));
                }
                this.progressHelper.progress(0, "Generating signature block file");
                je = new JarEntry(CERT_RSA_NAME);
                je.setTime(timestamp);
                outputJar.putNextEntry(je);
                writeSignatureBlock(this.keySet.getSigBlockTemplate(), signatureBytes, this.keySet.getPublicKey(), outputJar);
                if (this.canceled) {
                    if (!(outputJar == null || this.keymode.equals(KEY_NONE))) {
                        outputJar.close();
                    }
                    if (this.canceled && outputZipFilename != null) {
                        try {
                            new File(outputZipFilename).delete();
                            return;
                        } catch (Throwable t2222) {
                            getLogger().warning(t2222.getClass().getName() + ":" + t2222.getMessage());
                            return;
                        }
                    }
                    return;
                }
                copyFiles(manifest, zioEntries, outputJar, timestamp, modified_files);
                if (this.canceled) {
                    if (!(outputJar == null || this.keymode.equals(KEY_NONE))) {
                        outputJar.close();
                    }
                    if (this.canceled && outputZipFilename != null) {
                        try {
                            new File(outputZipFilename).delete();
                            return;
                        } catch (Throwable t22222) {
                            getLogger().warning(t22222.getClass().getName() + ":" + t22222.getMessage());
                            return;
                        }
                    }
                    return;
                }
                if (!(outputJar == null || this.keymode.equals(KEY_NONE))) {
                    outputJar.close();
                }
                if (this.canceled && outputZipFilename != null) {
                    try {
                        new File(outputZipFilename).delete();
                    } catch (Throwable t222222) {
                        getLogger().warning(t222222.getClass().getName() + ":" + t222222.getMessage());
                    }
                }
            } catch (Throwable th3) {
                th = th3;
                zipOutput = zipOutput2;
                if (!(outputJar == null || this.keymode.equals(KEY_NONE))) {
                    outputJar.close();
                }
                if (this.canceled && outputZipFilename != null) {
                    new File(outputZipFilename).delete();
                }
                throw th;
            }
        } catch (Throwable th4) {
            th = th4;
            outputJar = null;
            outputJar.close();
            new File(outputZipFilename).delete();
            throw th;
        }
    }

    public void addProgressListener(ProgressListener l) {
        this.progressHelper.addProgressListener(l);
    }

    public synchronized void removeProgressListener(ProgressListener l) {
        this.progressHelper.removeProgressListener(l);
    }
}
