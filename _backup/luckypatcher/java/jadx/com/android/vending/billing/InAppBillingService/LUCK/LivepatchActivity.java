package com.android.vending.billing.InAppBillingService.LUCK;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.chelpus.Utils;
import java.io.File;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.regex.Pattern;
import net.lingala.zip4j.util.InternalZipConstants;

public class LivepatchActivity extends Activity {
    private static final int DIALOG_LOAD_FILE = 3;
    private static final String SETTINGS_VIEWSIZE = "viewsize";
    private static final int SETTINGS_VIEWSIZE_SMALL = 0;
    private static final String TAG = "F_PATH";
    public static Context context;
    static InputFilter filter = new C00811();
    static InputFilter filter2 = new C00822();
    private static EditText orhex;
    public static String patch = BuildConfig.VERSION_NAME;
    private static EditText rephex;
    public static String selabpath = BuildConfig.VERSION_NAME;
    public static String selpath = BuildConfig.VERSION_NAME;
    public static String str;
    private static TextView tv;
    ListAdapter adapter;
    private String chosenFile;
    private Item[] fileList;
    private Boolean firstLvl = Boolean.valueOf(true);
    private File path = new File(Environment.getExternalStorageDirectory() + BuildConfig.VERSION_NAME);
    public PkgListItem pli;
    int start = SETTINGS_VIEWSIZE_SMALL;
    ArrayList<String> stri = new ArrayList();

    static class C00811 implements InputFilter {
        C00811() {
        }

        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            try {
                String checkedText = dest.toString() + source.toString();
                String pattern = getPattern();
                if (Pattern.compile("([\\s]){2}").matcher(checkedText).find()) {
                    return BuildConfig.VERSION_NAME;
                }
                if (Pattern.matches("([\\s]){2}", checkedText)) {
                    return BuildConfig.VERSION_NAME;
                }
                return null;
            } catch (Exception e) {
                return BuildConfig.VERSION_NAME;
            }
        }

        private String getPattern() {
            DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols();
            return "([0-9A-Fa-f*?]){2}||([0-9A-Fa-f*?]){1}";
        }
    }

    static class C00822 implements InputFilter {
        C00822() {
        }

        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            try {
                String checkedText = dest.toString() + source.toString();
                String pattern = getPattern();
                if (Pattern.compile("([\\s]){2}").matcher(checkedText).find()) {
                    return BuildConfig.VERSION_NAME;
                }
                if (Pattern.matches("([\\s]){2}", checkedText)) {
                    return BuildConfig.VERSION_NAME;
                }
                return null;
            } catch (Exception e) {
                return BuildConfig.VERSION_NAME;
            }
        }

        private String getPattern() {
            DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols();
            return "([0-9A-Fa-f*?]){2}||([0-9A-Fa-f*?]){1}";
        }
    }

    class C00833 implements TextWatcher {
        C00833() {
        }

        public void afterTextChanged(Editable s) {
        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
            LivepatchActivity.rephex.setText(s.toString().replace("\\s+", " "));
        }
    }

    class C00844 implements OnClickListener {
        C00844() {
        }

        public void onClick(View v) {
            System.out.println("run patch!");
            LivepatchActivity.this.livepatch_click();
        }
    }

    class C00855 implements OnClickListener {
        C00855() {
        }

        public void onClick(View v) {
            LivepatchActivity.this.launch_click();
        }
    }

    class C00866 implements OnClickListener {
        C00866() {
        }

        public void onClick(View v) {
            LivepatchActivity.this.backup_click();
        }
    }

    class C00877 implements OnClickListener {
        C00877() {
        }

        public void onClick(View v) {
            LivepatchActivity.this.restore_click();
        }
    }

    class C00888 implements OnClickListener {
        C00888() {
        }

        public void onClick(View v) {
            System.out.println("select_target!");
            try {
                LivepatchActivity.this.path = new File(LivepatchActivity.this.getPackageManager().getPackageInfo(LivepatchActivity.this.pli.pkgName, LivepatchActivity.SETTINGS_VIEWSIZE_SMALL).applicationInfo.dataDir);
                if (LivepatchActivity.this.getPackageManager().getPackageInfo(LivepatchActivity.this.pli.pkgName, LivepatchActivity.SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir.contains("/mnt/asec/")) {
                    LivepatchActivity.this.path = new File(LivepatchActivity.this.getPackageManager().getPackageInfo(LivepatchActivity.this.pli.pkgName, LivepatchActivity.SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir.replace("/pkg.apk", BuildConfig.VERSION_NAME));
                }
                LivepatchActivity.this.loadFileList();
                LivepatchActivity.this.removeDialog(LivepatchActivity.DIALOG_LOAD_FILE);
                LivepatchActivity.this.showDialog(LivepatchActivity.DIALOG_LOAD_FILE);
            } catch (NameNotFoundException e) {
                e.printStackTrace();
            } catch (Exception e2) {
                Toast.makeText(LivepatchActivity.this.getApplicationContext(), "System not respond!", 1).show();
            }
        }
    }

    class C00899 implements OnItemClickListener {
        C00899() {
        }

        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
            LivepatchActivity.this.chosenFile = LivepatchActivity.this.fileList[position].file;
            File sel = new File(LivepatchActivity.this.path + InternalZipConstants.ZIP_FILE_SEPARATOR + LivepatchActivity.this.chosenFile);
            if (sel.isDirectory()) {
                LivepatchActivity.this.firstLvl = Boolean.valueOf(false);
                LivepatchActivity.this.stri.add(LivepatchActivity.this.chosenFile);
                LivepatchActivity.this.fileList = null;
                LivepatchActivity.this.path = new File(sel + BuildConfig.VERSION_NAME);
                LivepatchActivity.this.loadFileList();
                LivepatchActivity.this.removeDialog(LivepatchActivity.DIALOG_LOAD_FILE);
                LivepatchActivity.this.showDialog(LivepatchActivity.DIALOG_LOAD_FILE);
                Log.d(LivepatchActivity.TAG, LivepatchActivity.this.path.getAbsolutePath());
            } else if (!LivepatchActivity.this.chosenFile.equalsIgnoreCase("up") || sel.exists()) {
                LivepatchActivity.selabpath = LivepatchActivity.this.path.getAbsolutePath();
                LivepatchActivity.selpath = LivepatchActivity.this.path.getAbsolutePath() + InternalZipConstants.ZIP_FILE_SEPARATOR + LivepatchActivity.this.chosenFile;
                LivepatchActivity.patch = "lib";
                LivepatchActivity.str = "Help:\n\n - Pattern for original hex (length>3):\n  AA 1F ** 01 4C\n ** - any byte;\n\n - Pattern for replacement hex(length>3):\n  EA 2F ** ** **\n ** - does not change the byte;\n\nNumber of original bytes  and modified bytes must be equal; ";
                LivepatchActivity.this.removeDialog(LivepatchActivity.DIALOG_LOAD_FILE);
                Resources r = LivepatchActivity.this.getApplication().getApplicationContext().getResources();
                LivepatchActivity.tv.setText(Utils.getColoredText("Target is " + LivepatchActivity.selpath + ".\n", "#ff00ff73", "bold"));
                LivepatchActivity.this.firstLvl = Boolean.valueOf(true);
                LivepatchActivity.this.stri.clear();
            } else {
                try {
                    LivepatchActivity.this.path = new File(LivepatchActivity.this.path.toString().substring(LivepatchActivity.SETTINGS_VIEWSIZE_SMALL, LivepatchActivity.this.path.toString().lastIndexOf((String) LivepatchActivity.this.stri.remove(LivepatchActivity.this.stri.size() - 1))));
                    LivepatchActivity.this.fileList = null;
                } catch (Exception e) {
                    e.printStackTrace();
                    LivepatchActivity.this.stri.clear();
                    LivepatchActivity.this.firstLvl = Boolean.valueOf(true);
                }
                if (LivepatchActivity.this.stri.isEmpty()) {
                    LivepatchActivity.this.firstLvl = Boolean.valueOf(true);
                }
                LivepatchActivity.this.loadFileList();
                LivepatchActivity.this.removeDialog(LivepatchActivity.DIALOG_LOAD_FILE);
                LivepatchActivity.this.showDialog(LivepatchActivity.DIALOG_LOAD_FILE);
                Log.d(LivepatchActivity.TAG, LivepatchActivity.this.path.getAbsolutePath());
            }
        }
    }

    private class Item {
        public String file;
        public int icon;

        public Item(String file, Integer icon) {
            this.file = file;
            if (icon != null) {
                this.icon = icon.intValue();
            }
        }

        public String toString() {
            return this.file;
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getApplicationContext();
        setContentView(C0149R.layout.livepatchdialog);
        this.pli = listAppsFragment.pli;
        LinearLayout body = (LinearLayout) findViewById(C0149R.id.patchbodyscroll).findViewById(C0149R.id.dialogbodypatch);
        patch = "dalvik";
        orhex = (EditText) findViewById(C0149R.id.originalText);
        rephex = (EditText) findViewById(C0149R.id.replacedText);
        orhex.setFilters(new InputFilter[]{filter});
        orhex.addTextChangedListener(new C00833());
        tv = (TextView) body.findViewById(C0149R.id.result);
        Resources r = getApplication().getApplicationContext().getResources();
        str = r.getString(C0149R.string.mpatcher_help);
        tv.setText(Utils.getColoredText(r.getString(C0149R.string.default_target) + "\n\n" + str, "#ff00ff73", "bold"));
        ((Button) findViewById(C0149R.id.liveputchbutton)).setOnClickListener(new C00844());
        ((Button) findViewById(C0149R.id.livelaunchbutton)).setOnClickListener(new C00855());
        ((Button) findViewById(C0149R.id.livebackupbutton)).setOnClickListener(new C00866());
        ((Button) findViewById(C0149R.id.liverestorebutton)).setOnClickListener(new C00877());
        ((Button) findViewById(C0149R.id.selecttargetbutton)).setOnClickListener(new C00888());
    }

    public void onStop() {
        super.onStop();
    }

    public void onRestart() {
        super.onRestart();
    }

    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DIALOG_LOAD_FILE /*3*/:
                Dialog dialog = new Dialog(this);
                ListView modeList = new ListView(this);
                Builder builder = new Builder(this);
                if (this.fileList == null) {
                    Log.e(TAG, "No files loaded");
                    return builder.create();
                }
                builder.setTitle("Choose your file");
                if (this.adapter != null) {
                    modeList.setAdapter(this.adapter);
                    modeList.invalidateViews();
                    modeList.setCacheColorHint(-16777216);
                    modeList.setBackgroundColor(-16777216);
                    modeList.setOnItemClickListener(new C00899());
                    builder.setView(modeList);
                }
                return builder.setOnCancelListener(new OnCancelListener() {
                    public void onCancel(DialogInterface dialog) {
                        LivepatchActivity.this.removeDialog(LivepatchActivity.DIALOG_LOAD_FILE);
                        LivepatchActivity.this.firstLvl = Boolean.valueOf(true);
                    }
                }).setCancelable(true).create();
            default:
                return null;
        }
    }

    public void launch_click() {
        try {
            startActivity(getPackageManager().getLaunchIntentForPackage(this.pli.pkgName));
        } catch (RuntimeException e) {
            e.printStackTrace();
        } catch (Exception e2) {
            Toast.makeText(getApplicationContext(), Utils.getText(C0149R.string.error_launch), 1).show();
        }
    }

    public void livepatch_click() {
        System.out.println("run patch");
        boolean verif = true;
        String orighex = orhex.getText().toString();
        String replhex = rephex.getText().toString();
        String pattern = "([0-9A-Fa-f*?]){2}||([0-9A-Fa-f*?]){1}";
        if (!(orighex == null || replhex == null)) {
            int i;
            String[] chk = orighex.split("\\s+");
            int length = chk.length;
            for (i = SETTINGS_VIEWSIZE_SMALL; i < length; i++) {
                if (!Pattern.matches(pattern, chk[i])) {
                    verif = false;
                    Toast.makeText(getApplication().getBaseContext(), Utils.getText(C0149R.string.error_original_pattern), SETTINGS_VIEWSIZE_SMALL).show();
                }
            }
            String[] chk2 = replhex.split("\\s+");
            length = chk2.length;
            for (i = SETTINGS_VIEWSIZE_SMALL; i < length; i++) {
                if (!Pattern.matches(pattern, chk2[i])) {
                    verif = false;
                    Toast.makeText(getApplication().getBaseContext(), Utils.getText(C0149R.string.error_replace_pattern), SETTINGS_VIEWSIZE_SMALL).show();
                }
            }
            if (chk.length != chk2.length) {
                verif = false;
                Toast.makeText(getApplication().getBaseContext(), Utils.getText(C0149R.string.error_lenghts_patterns), SETTINGS_VIEWSIZE_SMALL).show();
            }
        }
        if (verif) {
            String str2;
            if (patch == "dalvik") {
                try {
                    Toast.makeText(getApplication().getBaseContext(), "Search bytes... Please wait...", SETTINGS_VIEWSIZE_SMALL).show();
                    str = new Utils(BuildConfig.VERSION_NAME).cmdRoot(listAppsFragment.dalvikruncommand + ".liverunpatch " + this.pli.pkgName + " " + "\"" + orighex + "\"" + " " + "\"" + replhex + "\"");
                    str2 = str;
                    if (str.contains("Error")) {
                        tv.setText(Utils.getColoredText(str2, "#ffff0055", "bold"));
                    }
                    if (!str.contains("Error")) {
                        tv.setText(Utils.getColoredText(str2, "#ff00ff73", "bold"));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (patch == "lib") {
                try {
                    Toast.makeText(getApplication().getApplicationContext(), "Search bytes... Please wait...", SETTINGS_VIEWSIZE_SMALL).show();
                    if (getPackageManager().getPackageInfo(this.pli.pkgName, SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir.contains("/mnt/asec/")) {
                        Utils.remount(getPackageManager().getPackageInfo(this.pli.pkgName, SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir.replace("/pkg.apk", BuildConfig.VERSION_NAME), InternalZipConstants.WRITE_MODE);
                    }
                    str = new Utils(BuildConfig.VERSION_NAME).cmdRoot(listAppsFragment.dalvikruncommand + ".liverunpatchlib " + selpath + " " + "\"" + orighex + "\"" + " " + "\"" + replhex + "\"");
                    if (getPackageManager().getPackageInfo(this.pli.pkgName, SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir.contains("/mnt/asec/")) {
                        Utils.remount(getPackageManager().getPackageInfo(this.pli.pkgName, SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir.replace("/pkg.apk", BuildConfig.VERSION_NAME), "ro");
                    }
                    str2 = str;
                    Resources r = getApplication().getApplicationContext().getResources();
                    if (str.contains("Error")) {
                        tv.setText(Utils.getColoredText(str2, "#ffff0055", "bold"));
                    }
                    if (!str.contains("Error")) {
                        tv.setText(Utils.getColoredText(str2, "#ff00ff73", "bold"));
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        }
    }

    public void restore_click() {
        if (patch == "dalvik") {
            try {
                Toast.makeText(getApplication().getApplicationContext(), "Restore processing... Please wait...", SETTINGS_VIEWSIZE_SMALL).show();
                tv.setText(Utils.getColoredText(new Utils(BuildConfig.VERSION_NAME).cmdRoot(listAppsFragment.dalvikruncommand + ".live_restore " + this.pli.pkgName), "#ff00ff73", "bold"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (patch == "lib") {
            try {
                if (getPackageManager().getPackageInfo(this.pli.pkgName, SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir.contains("/mnt/asec/")) {
                    Utils.remount(getPackageManager().getPackageInfo(this.pli.pkgName, SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir.replace("/pkg.apk", BuildConfig.VERSION_NAME), InternalZipConstants.WRITE_MODE);
                }
                str = new Utils(BuildConfig.VERSION_NAME).cmdRoot(listAppsFragment.dalvikruncommand + ".live_restorelib " + selpath);
                if (getPackageManager().getPackageInfo(this.pli.pkgName, SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir.contains("/mnt/asec/")) {
                    Utils.remount(getPackageManager().getPackageInfo(this.pli.pkgName, SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir.replace("/pkg.apk", BuildConfig.VERSION_NAME), "ro");
                }
                String str2 = str;
                if (str.contains("Error")) {
                    tv.setText(Utils.getColoredText(str2, "#ffff0055", "bold"));
                }
                if (!str.contains("Error")) {
                    tv.setText(Utils.getColoredText(str2, "#ff00ff73", "bold"));
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    public void backup_click() {
        if (patch == "dalvik") {
            try {
                Toast.makeText(getApplication().getApplicationContext(), "Backup processing... Please wait...", SETTINGS_VIEWSIZE_SMALL).show();
                tv.setText(Utils.getColoredText(new Utils(BuildConfig.VERSION_NAME).cmdRoot(listAppsFragment.dalvikruncommand + ".live_backup " + this.pli.pkgName), "#ff00ff73", "bold"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (patch == "lib") {
            try {
                if (getPackageManager().getPackageInfo(this.pli.pkgName, SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir.contains("/mnt/asec/")) {
                    Utils.remount(getPackageManager().getPackageInfo(this.pli.pkgName, SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir.replace("/pkg.apk", BuildConfig.VERSION_NAME), InternalZipConstants.WRITE_MODE);
                }
                str = new Utils(BuildConfig.VERSION_NAME).cmdRoot(listAppsFragment.dalvikruncommand + ".live_backuplib " + selpath);
                if (getPackageManager().getPackageInfo(this.pli.pkgName, SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir.contains("/mnt/asec/")) {
                    Utils.remount(getPackageManager().getPackageInfo(this.pli.pkgName, SETTINGS_VIEWSIZE_SMALL).applicationInfo.sourceDir.replace("/pkg.apk", BuildConfig.VERSION_NAME), "ro");
                }
                String str2 = str;
                if (str.contains("Error")) {
                    tv.setText(Utils.getColoredText(str2, "#ffff0055", "bold"));
                }
                if (!str.contains("Error")) {
                    tv.setText(Utils.getColoredText(str2, "#ff00ff73", "bold"));
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    private void loadFileList() {
        String[] fList = new String[]{"empty"};
        try {
            fList = new Utils(BuildConfig.VERSION_NAME).cmdRoot("cd " + this.path, "ls").split(LogCollector.LINE_SEPARATOR);
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.fileList = new Item[fList.length];
        for (int i = SETTINGS_VIEWSIZE_SMALL; i < fList.length; i++) {
            if (fList[i].equals(BuildConfig.VERSION_NAME) && fList.length == 1) {
                this.fileList[i] = new Item("Up", Integer.valueOf(C0149R.drawable.fb_back));
            } else {
                this.fileList[i] = new Item(fList[i], Integer.valueOf(C0149R.drawable.file_icon));
                if (new File(this.path, fList[i]).isDirectory()) {
                    this.fileList[i].icon = C0149R.drawable.fb_folder;
                    Log.d("DIRECTORY", this.fileList[i].file);
                } else {
                    Log.d("FILE", this.fileList[i].file);
                }
            }
        }
        if (!(this.firstLvl.booleanValue() || (this.fileList.length == 1 && this.fileList[SETTINGS_VIEWSIZE_SMALL].file.equals("Up")))) {
            Item[] temp = new Item[(this.fileList.length + 1)];
            System.arraycopy(this.fileList, SETTINGS_VIEWSIZE_SMALL, temp, 1, this.fileList.length);
            temp[SETTINGS_VIEWSIZE_SMALL] = new Item("Up", Integer.valueOf(C0149R.drawable.fb_back));
            this.fileList = temp;
        }
        this.adapter = new ArrayAdapter<Item>(this, 17367057, 16908308, this.fileList) {
            public View getView(int position, View convertView, ViewGroup parent) {
                Item p = (Item) getItem(position);
                View view = super.getView(position, convertView, parent);
                TextView textView = (TextView) view.findViewById(16908308);
                LivepatchActivity.this.getApplicationContext().getResources();
                textView.setTextAppearance(getContext(), listAppsFragment.getSizeText());
                textView.setCompoundDrawablesWithIntrinsicBounds(LivepatchActivity.this.fileList[position].icon, LivepatchActivity.SETTINGS_VIEWSIZE_SMALL, LivepatchActivity.SETTINGS_VIEWSIZE_SMALL, LivepatchActivity.SETTINGS_VIEWSIZE_SMALL);
                textView.setCompoundDrawablePadding((int) ((5.0f * LivepatchActivity.this.getResources().getDisplayMetrics().density) + 0.5f));
                textView.setTextColor(-1);
                textView.setText(p.file);
                textView.setTypeface(null, 1);
                return view;
            }
        };
    }
}
