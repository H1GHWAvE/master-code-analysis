package com.android.vending.billing.InAppBillingService.LUCK.widgets;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.widget.RemoteViews;
import android.widget.Toast;
import com.android.vending.billing.InAppBillingService.LUCK.BuildConfig;
import com.android.vending.billing.InAppBillingService.LUCK.C0149R;
import com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment;
import com.chelpus.Utils;
import com.google.android.finsky.services.LicensingService;

public class lvl_widget extends AppWidgetProvider {
    public static String ACTION_WIDGET_RECEIVER = "ActionReceiverLVLWidget";
    public static String ACTION_WIDGET_RECEIVER_Updater = "ActionReceiverWidgetLVLUpdate";

    public static void pushWidgetUpdate(Context context, RemoteViews remoteViews) {
        AppWidgetManager.getInstance(context).updateAppWidget(new ComponentName(context, lvl_widget.class), remoteViews);
    }

    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), C0149R.layout.widget_lvl);
        Intent active = new Intent(context, lvl_widget.class);
        active.setAction(ACTION_WIDGET_RECEIVER);
        active.putExtra("msg", "Hello Habrahabr");
        remoteViews.setOnClickPendingIntent(C0149R.id.widget_button, PendingIntent.getBroadcast(context, 0, active, 0));
        remoteViews.setTextViewText(C0149R.id.widget_button, "LVL");
        remoteViews.setViewVisibility(C0149R.id.progressBar_widget_lvl, 8);
        if (context.getPackageManager().getComponentEnabledSetting(new ComponentName(context, LicensingService.class)) == 2) {
            remoteViews.setTextColor(C0149R.id.widget_button, Color.parseColor("#FF0000"));
        } else {
            remoteViews.setTextColor(C0149R.id.widget_button, Color.parseColor("#00FF00"));
        }
        appWidgetManager.updateAppWidget(appWidgetIds, remoteViews);
    }

    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (ACTION_WIDGET_RECEIVER.equals(action)) {
            listAppsFragment.init();
            RemoteViews remoteViews = new RemoteViews(context.getPackageName(), C0149R.layout.widget_lvl);
            remoteViews.setTextViewText(C0149R.id.widget_button, BuildConfig.VERSION_NAME);
            remoteViews.setViewVisibility(C0149R.id.progressBar_widget_lvl, 0);
            AppWidgetManager gm = AppWidgetManager.getInstance(context);
            gm.updateAppWidget(gm.getAppWidgetIds(new ComponentName(context, lvl_widget.class)), remoteViews);
            if (context.getPackageManager().getComponentEnabledSetting(new ComponentName(context, LicensingService.class)) == 2) {
                listAppsFragment.getPkgMng().setComponentEnabledSetting(new ComponentName(listAppsFragment.getInstance(), LicensingService.class), 1, 1);
                if (!listAppsFragment.getConfig().getBoolean("LVL_enable", false)) {
                    listAppsFragment.getConfig().edit().putBoolean("LVL_enable", true).commit();
                }
                Toast.makeText(context, "LVL-ON", 0).show();
            } else {
                listAppsFragment.getPkgMng().setComponentEnabledSetting(new ComponentName(listAppsFragment.getInstance(), LicensingService.class), 2, 1);
                if (listAppsFragment.su) {
                    Utils.market_licensing_services(true);
                }
                Toast.makeText(context, "LVL-OFF", 0).show();
            }
        }
        if (ACTION_WIDGET_RECEIVER_Updater.equals(action)) {
            try {
                listAppsFragment.appDisabler = true;
                gm = AppWidgetManager.getInstance(context);
                onUpdate(context, gm, gm.getAppWidgetIds(new ComponentName(context, lvl_widget.class)));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        super.onReceive(context, intent);
    }
}
