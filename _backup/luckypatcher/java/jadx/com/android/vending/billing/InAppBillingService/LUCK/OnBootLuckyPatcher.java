package com.android.vending.billing.InAppBillingService.LUCK;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v4.content.IntentCompat;
import android.support.v4.view.MotionEventCompat;
import android.widget.Toast;
import com.android.vending.billing.InAppBillingService.LUCK.widgets.BinderWidget;
import com.chelpus.Utils;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import net.lingala.zip4j.util.InternalZipConstants;

public class OnBootLuckyPatcher extends BroadcastReceiver {
    public static String[] bootlist = new String[]{"empty"};
    public static Context contextB = null;
    public static int notifyIndex = 0;

    class C01071 implements Runnable {
        C01071() {
        }

        public void run() {
            if (listAppsFragment.getConfig().getInt("Install_location", 3) == 3) {
                return;
            }
            if (listAppsFragment.su) {
                new Utils(BuildConfig.VERSION_NAME).cmdRoot("pm setInstallLocation " + location, "skipOut");
                new Utils(BuildConfig.VERSION_NAME).cmdRoot("pm set-install-location " + location, "skipOut");
                return;
            }
            Utils.cmd("pm setInstallLocation " + location, "skipOut");
            Utils.cmd("pm set-install-location " + location, "skipOut");
        }
    }

    public void onReceive(final Context context, Intent intent) {
        System.out.println("load LP");
        contextB = context;
        listAppsFragment.init();
        new Thread(new C01071()).start();
        if (listAppsFragment.su) {
            if (intent.getAction().equals("android.intent.action.UMS_DISCONNECTED") || intent.getAction().equals("android.intent.action.ACTION_POWER_DISCONNECTED")) {
                System.out.println("load LP");
                new Thread(new Runnable() {
                    public void run() {
                        File bindfile = new File(context.getDir("binder", 0) + "/bind.txt");
                        if (bindfile.exists() && bindfile.length() > 0) {
                            System.out.println("LuckyPatcher binder start!");
                            try {
                                if (!bindfile.exists()) {
                                    bindfile.createNewFile();
                                }
                                FileInputStream fis = new FileInputStream(bindfile);
                                BufferedReader br = new BufferedReader(new InputStreamReader(fis, "UTF-8"));
                                String data = BuildConfig.VERSION_NAME;
                                String temp = BuildConfig.VERSION_NAME;
                                while (true) {
                                    data = br.readLine();
                                    if (data == null) {
                                        break;
                                    }
                                    String[] tails = data.split(";");
                                    if (tails.length == 2) {
                                        Utils.verify_bind_and_run("mount", "-o bind '" + tails[0] + "' '" + tails[1] + "'", tails[0], tails[1]);
                                    }
                                }
                                Intent i = new Intent(context, BinderWidget.class);
                                i.setAction(BinderWidget.ACTION_WIDGET_RECEIVER_Updater);
                                context.sendBroadcast(i);
                                fis.close();
                            } catch (FileNotFoundException e) {
                                System.out.println("Not found bind.txt");
                            } catch (IOException e2) {
                                System.out.println(BuildConfig.VERSION_NAME + e2);
                            }
                            listAppsFragment.patchOnBoot = false;
                            Utils.exit();
                        }
                    }
                }).start();
                listAppsFragment.patchOnBoot = true;
            }
            if (intent.getAction().equals(IntentCompat.ACTION_EXTERNAL_APPLICATIONS_AVAILABLE)) {
                System.out.println("LuckyPatcher: ACTION_EXTERNAL_APPLICATIONS_AVAILABLE");
                if (listAppsFragment.getConfig().getBoolean("OnBootService", false)) {
                    listAppsFragment.getConfig().edit().putBoolean("OnBootService", false).commit();
                    listAppsFragment.patchOnBoot = true;
                    listAppsFragment.getInstance().startService(new Intent(listAppsFragment.getInstance(), PatchService.class));
                }
            }
            final Handler handler = new Handler();
            if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
                listAppsFragment.getConfig().edit().putBoolean("OnBootService", true).commit();
                AlarmManager mgr = (AlarmManager) context.getSystemService("alarm");
                Intent i = new Intent(context, OnAlarmReceiver.class);
                i.setAction(OnAlarmReceiver.ACTION_WIDGET_RECEIVER);
                mgr.set(2, 300000, PendingIntent.getBroadcast(context, 0, i, 0));
                new Thread(new Runnable() {

                    class C01091 implements Runnable {
                        C01091() {
                        }

                        public void run() {
                            Toast.makeText(OnBootLuckyPatcher.contextB.getApplicationContext(), "LuckyPatcher: clear dalvik-cache failed. Please clear dalvik-cache manual.", 1).show();
                        }
                    }

                    public void run() {
                        if (Utils.exists(listAppsFragment.toolfilesdir + "/ClearDalvik.on")) {
                            String str = BuildConfig.VERSION_NAME;
                            Utils.remount("/system", InternalZipConstants.WRITE_MODE);
                            try {
                                System.out.println(new Utils(BuildConfig.VERSION_NAME).cmdRoot(listAppsFragment.dalvikruncommand + ".clearDalvikCache " + OnBootLuckyPatcher.contextB.getApplicationContext().getFilesDir().getAbsolutePath()));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            handler.post(new C01091());
                            System.out.println(OnBootLuckyPatcher.contextB.getApplicationContext().getFilesDir() + "/reboot");
                            Utils.reboot();
                        }
                        if (!Utils.getCurrentRuntimeValue().equals("ART")) {
                            if (listAppsFragment.getConfig().getBoolean("trigger_for_good_android_patch_on_boot", false)) {
                                Utils.turn_off_patch_on_boot_all();
                            }
                            listAppsFragment.getConfig().edit().putBoolean("trigger_for_good_android_patch_on_boot", true).commit();
                            String for_patch = listAppsFragment.getConfig().getString("patch_dalvik_on_boot_patterns", BuildConfig.VERSION_NAME);
                            String pattern = BuildConfig.VERSION_NAME;
                            if (!for_patch.equals(BuildConfig.VERSION_NAME)) {
                                String dalvikcache;
                                String dalvikcache2;
                                if (!(!for_patch.contains("patch1") || Utils.checkCoreJarPatch11() || Utils.checkCoreJarPatch12())) {
                                    pattern = pattern + "_patch1_";
                                }
                                if (for_patch.contains("patch2") && !Utils.checkCoreJarPatch20()) {
                                    pattern = pattern + "_patch2_";
                                }
                                if (!pattern.equals(BuildConfig.VERSION_NAME)) {
                                    listAppsFragment.patchOnlyDalvikCore = true;
                                    listAppsFragment.install_market_to_system = false;
                                    System.out.println("patch only dalvik cache mode on boot");
                                    dalvikcache = BuildConfig.VERSION_NAME;
                                    dalvikcache2 = BuildConfig.VERSION_NAME;
                                    dalvikcache = Utils.getFileDalvikCache("/system/framework/core.jar").getAbsolutePath();
                                    if (dalvikcache == null) {
                                        System.out.println("dalvik cache for core.jar not found");
                                    }
                                    dalvikcache2 = Utils.getFileDalvikCache("/system/framework/services.jar").getAbsolutePath();
                                    if (dalvikcache2 == null) {
                                        System.out.println("dalvik cache for services.jar not found");
                                    }
                                    if (dalvikcache == null || dalvikcache2 == null || dalvikcache.equals(BuildConfig.VERSION_NAME) || dalvikcache2.equals(BuildConfig.VERSION_NAME)) {
                                        System.out.println("dalvik cache patch on boot skip");
                                    } else {
                                        listAppsFragment.str = new Utils(BuildConfig.VERSION_NAME).cmdRoot(listAppsFragment.dalvikruncommand + ".corepatch " + pattern + " " + dalvikcache + " " + dalvikcache2 + " " + listAppsFragment.getInstance().getFilesDir() + " OnlyDalvik");
                                        System.out.println(listAppsFragment.str);
                                        new Utils("w").waitLP(4000);
                                        if (listAppsFragment.str.contains("SU Java-Code Running!")) {
                                            if (!(!for_patch.contains("patch1") || Utils.checkCoreJarPatch11() || Utils.checkCoreJarPatch12())) {
                                                Utils.turn_off_patch_on_boot("patch1");
                                            }
                                            if (for_patch.contains("patch2") && !Utils.checkCoreJarPatch20()) {
                                                Utils.turn_off_patch_on_boot("patch2");
                                            }
                                            if (Utils.checkCoreJarPatch11() || Utils.checkCoreJarPatch12() || Utils.checkCoreJarPatch20()) {
                                                OnBootLuckyPatcher.this.showNotify(254, "Lucky Patcher - " + Utils.getText(C0149R.string.notify_android_patch_on_boot), Utils.getText(C0149R.string.notify_android_patch_on_boot), Utils.getText(C0149R.string.notify_android_patch_on_boot_mes_core.jar));
                                            }
                                        }
                                    }
                                }
                                pattern = BuildConfig.VERSION_NAME;
                                if (for_patch.contains("patch3") && !Utils.checkCoreJarPatch30(listAppsFragment.getPkgMng())) {
                                    pattern = "_patch3_";
                                }
                                if (!pattern.equals(BuildConfig.VERSION_NAME)) {
                                    listAppsFragment.patchOnlyDalvikCore = true;
                                    listAppsFragment.install_market_to_system = false;
                                    System.out.println("patch only dalvik cache mode on boot");
                                    dalvikcache = BuildConfig.VERSION_NAME;
                                    dalvikcache2 = BuildConfig.VERSION_NAME;
                                    dalvikcache = Utils.getFileDalvikCache("/system/framework/core.jar").getAbsolutePath();
                                    if (dalvikcache == null) {
                                        System.out.println("dalvik cache for core.jar not found");
                                    }
                                    dalvikcache2 = Utils.getFileDalvikCache("/system/framework/services.jar").getAbsolutePath();
                                    if (dalvikcache2 == null) {
                                        System.out.println("dalvik cache for services.jar not found");
                                    }
                                    if (dalvikcache == null || dalvikcache2 == null || dalvikcache.equals(BuildConfig.VERSION_NAME) || dalvikcache2.equals(BuildConfig.VERSION_NAME)) {
                                        System.out.println("dalvik cache patch on boot skip");
                                        return;
                                    }
                                    listAppsFragment.str = new Utils(BuildConfig.VERSION_NAME).cmdRoot(listAppsFragment.dalvikruncommand + ".corepatch " + pattern + " " + dalvikcache + " " + dalvikcache2 + " " + listAppsFragment.getInstance().getFilesDir() + " OnlyDalvik");
                                    System.out.println(listAppsFragment.str);
                                    if (listAppsFragment.str.contains("SU Java-Code Running!")) {
                                        new Utils("w").waitLP(4000);
                                        if (!for_patch.contains("patch3") || Utils.checkCoreJarPatch30(listAppsFragment.getPkgMng())) {
                                            OnBootLuckyPatcher.this.showNotify(MotionEventCompat.ACTION_MASK, "Lucky Patcher - " + Utils.getText(C0149R.string.notify_android_patch_on_boot), Utils.getText(C0149R.string.notify_android_patch_on_boot), Utils.getText(C0149R.string.notify_android_patch_on_boot_mes_services.jar));
                                        } else {
                                            Utils.turn_off_patch_on_boot("patch3");
                                        }
                                    }
                                }
                            }
                        }
                    }
                }).start();
            }
        }
        if (!listAppsFragment.getConfig().getBoolean("OnBootService", false)) {
            Utils.exit();
        }
    }

    private void showNotify(int id, String title, String tickerText, String text) {
        if (!listAppsFragment.getConfig().getBoolean("hide_notify", false)) {
            long when = System.currentTimeMillis();
            Context context2 = listAppsFragment.getInstance();
            String contentTitle = title;
            String contentText = text;
            PendingIntent contentIntent = PendingIntent.getActivity(listAppsFragment.getInstance(), 0, new Intent(listAppsFragment.getInstance(), patchActivity.class), 0);
            NotificationManager notificationManager = (NotificationManager) listAppsFragment.getInstance().getSystemService("notification");
            Notification notification = new Notification(C0149R.drawable.ic_notify, tickerText, when);
            notification.setLatestEventInfo(context2, contentTitle, contentText, contentIntent);
            notificationManager.notify(id, notification);
        }
    }
}
