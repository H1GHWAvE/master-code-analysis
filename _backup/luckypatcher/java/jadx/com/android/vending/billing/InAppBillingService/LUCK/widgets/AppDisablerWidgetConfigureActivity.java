package com.android.vending.billing.InAppBillingService.LUCK.widgets;

import android.app.Activity;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.android.vending.billing.InAppBillingService.LUCK.C0149R;
import com.android.vending.billing.InAppBillingService.LUCK.LogCollector;
import com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment;
import com.chelpus.Common;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

public class AppDisablerWidgetConfigureActivity extends Activity {
    private static final String PREFS_NAME = "com.android.vending.billing.InAppBillingService.LUCK.widgets.AppDisablerWidget";
    private static final String PREF_PREFIX_KEY = "appwidget_";
    public Context context;
    public ListView lv = null;
    int mAppWidgetId = 0;
    EditText mAppWidgetText;
    public String[] packages = null;
    public int sizeText = 0;

    class C05142 implements OnItemClickListener {
        C05142() {
        }

        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            PkgItem item = (PkgItem) adapterView.getItemAtPosition(i);
            Context context = AppDisablerWidgetConfigureActivity.this;
            AppDisablerWidgetConfigureActivity.saveTitlePref(context, AppDisablerWidgetConfigureActivity.this.mAppWidgetId, item.package_name);
            AppDisablerWidget.updateAppWidget(context, AppWidgetManager.getInstance(context), AppDisablerWidgetConfigureActivity.this.mAppWidgetId);
            Intent resultValue = new Intent();
            resultValue.putExtra("appWidgetId", AppDisablerWidgetConfigureActivity.this.mAppWidgetId);
            AppDisablerWidgetConfigureActivity.this.setResult(-1, resultValue);
            AppDisablerWidgetConfigureActivity.this.finish();
        }
    }

    class byPkgName implements Comparator<PkgItem> {
        byPkgName() {
        }

        public int compare(PkgItem a, PkgItem b) {
            return a.label.compareToIgnoreCase(b.label);
        }
    }

    public void onCreate(Bundle icicle) {
        int i = 0;
        super.onCreate(icicle);
        setResult(0);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.mAppWidgetId = extras.getInt("appWidgetId", 0);
        }
        if (this.mAppWidgetId == 0) {
            finish();
            return;
        }
        this.context = this;
        this.packages = listAppsFragment.getPackages();
        ArrayList<PkgItem> tmp = new ArrayList();
        String[] strArr = this.packages;
        int length = strArr.length;
        while (i < length) {
            String p = strArr[i];
            if (!p.equals(Common.ANDROID_PKG) || !p.equals(this.context.getPackageName())) {
                try {
                    tmp.add(new PkgItem(p, listAppsFragment.getPkgMng().getPackageInfo(p, 0).applicationInfo.loadLabel(listAppsFragment.getPkgMng()).toString()));
                } catch (NameNotFoundException e) {
                    e.printStackTrace();
                }
            }
            i++;
        }
        PkgItem[] pkgs = new PkgItem[tmp.size()];
        tmp.toArray(pkgs);
        try {
            Arrays.sort(pkgs, new byPkgName());
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        this.lv = new ListView(this.context);
        listAppsFragment.adapt = new ArrayAdapter<PkgItem>(this, C0149R.layout.icon_context_menu, pkgs) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View row = convertView;
                if (row == null) {
                    row = ((LayoutInflater) listAppsFragment.getInstance().getSystemService("layout_inflater")).inflate(C0149R.layout.icon_context_menu, parent, false);
                }
                TextView textView = (TextView) row.findViewById(C0149R.id.conttext);
                ImageView icon = (ImageView) row.findViewById(C0149R.id.imgIcon);
                icon.setImageDrawable(null);
                textView.setTextAppearance(getContext(), listAppsFragment.getSizeText());
                textView.setTextColor(-1);
                PkgItem item = (PkgItem) getItem(position);
                try {
                    PackageManager pm = listAppsFragment.getPkgMng();
                    int imgSize = (int) ((25.0f * listAppsFragment.getRes().getDisplayMetrics().density) + 0.5f);
                    try {
                        Bitmap iconka = ((BitmapDrawable) pm.getApplicationIcon(item.package_name)).getBitmap();
                        int width = iconka.getWidth();
                        int height = iconka.getHeight();
                        float scaleWidth = ((float) imgSize) / ((float) width);
                        float scaleHeight = ((float) imgSize) / ((float) height);
                        Matrix bMatrix = new Matrix();
                        bMatrix.postScale(scaleWidth, scaleHeight);
                        icon.setImageDrawable(new BitmapDrawable(Bitmap.createBitmap(iconka, 0, 0, width, height, bMatrix, true)));
                    } catch (NameNotFoundException e) {
                        e.printStackTrace();
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    } catch (OutOfMemoryError e3) {
                    }
                } catch (OutOfMemoryError u) {
                    u.printStackTrace();
                    System.gc();
                } catch (Exception e22) {
                    e22.printStackTrace();
                }
                textView.setCompoundDrawablePadding((int) ((5.0f * listAppsFragment.getRes().getDisplayMetrics().density) + 0.5f));
                textView.setText(item.label + LogCollector.LINE_SEPARATOR + item.package_name);
                textView.setTypeface(null, 1);
                return row;
            }
        };
        this.lv.setAdapter(listAppsFragment.adapt);
        this.lv.invalidateViews();
        this.lv.setBackgroundColor(-16777216);
        this.lv.setOnItemClickListener(new C05142());
        setContentView(this.lv);
    }

    static void saveTitlePref(Context context, int appWidgetId, String text) {
        Editor prefs = context.getSharedPreferences(PREFS_NAME, 4).edit();
        prefs.putString(PREF_PREFIX_KEY + appWidgetId, text);
        prefs.commit();
    }

    static String loadTitlePref(Context context, int appWidgetId) {
        String titleValue = context.getSharedPreferences(PREFS_NAME, 4).getString(PREF_PREFIX_KEY + appWidgetId, null);
        return titleValue != null ? titleValue : "NOT_SAVED_APP_DISABLER";
    }

    static void deleteTitlePref(Context context, int appWidgetId) {
        Editor prefs = context.getSharedPreferences(PREFS_NAME, 4).edit();
        prefs.remove(PREF_PREFIX_KEY + appWidgetId);
        prefs.commit();
    }
}
