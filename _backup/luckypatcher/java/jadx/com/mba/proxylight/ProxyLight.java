package com.mba.proxylight;

import com.android.vending.billing.InAppBillingService.LUCK.BuildConfig;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.channels.ClosedSelectorException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Stack;

public class ProxyLight {
    private List<RequestFilter> filters = new ArrayList();
    private Map<String, InetAddress> ipCache = new HashMap();
    private int port = 8080;
    private Stack<RequestProcessor> processors = new Stack();
    private String remoteProxyHost = null;
    private int remoteProxyPort = 8080;
    boolean running = false;
    private Selector selector = null;
    ServerSocketChannel server = null;

    class C06041 implements Runnable {

        class C06411 extends RequestProcessor {
            C06411() {
            }

            public void error(String message, Throwable t) {
                ProxyLight.this.error(message, t);
            }

            public void debug(String message) {
                ProxyLight.this.debug(message);
            }

            public void recycle() {
                ProxyLight.this.recycle(this);
            }

            public List<RequestFilter> getRequestFilters() {
                return ProxyLight.this.getRequestFilters();
            }

            public String getRemoteProxyHost() {
                return ProxyLight.this.getRemoteProxyHost();
            }

            public int getRemoteProxyPort() {
                return ProxyLight.this.getRemoteProxyPort();
            }

            public InetAddress resolve(String host) {
                return ProxyLight.this.resolve(host);
            }
        }

        C06041() {
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            Throwable t;
            try {
                ProxyLight.this.server = ServerSocketChannel.open();
                ProxyLight.this.server.configureBlocking(false);
                ProxyLight.this.server.socket().setReuseAddress(true);
                ProxyLight.this.server.socket().bind(new InetSocketAddress(8080));
                ProxyLight.this.selector = Selector.open();
                ProxyLight.this.server.register(ProxyLight.this.selector, 16);
                while (true) {
                    ProxyLight.this.selector.select();
                    if (ProxyLight.this.server == null) {
                        break;
                    }
                    Iterator<SelectionKey> it = ProxyLight.this.selector.selectedKeys().iterator();
                    while (it.hasNext()) {
                        SelectionKey key = (SelectionKey) it.next();
                        it.remove();
                        if (key.isValid() && key.isAcceptable()) {
                            RequestProcessor p = null;
                            try {
                                synchronized (ProxyLight.this.processors) {
                                    do {
                                        if (ProxyLight.this.processors.size() <= 0) {
                                            break;
                                        }
                                        p = (RequestProcessor) ProxyLight.this.processors.pop();
                                    } while (!p.isAlive());
                                    RequestProcessor p2 = p;
                                    try {
                                    } catch (Throwable th) {
                                        Throwable th2 = th;
                                        p = p2;
                                        throw th2;
                                        break;
                                    }
                                }
                            } catch (ClosedSelectorException e) {
                            } catch (Throwable th3) {
                                t = th3;
                            }
                        }
                    }
                }
            } catch (ClosedSelectorException e2) {
            } catch (Throwable t2) {
                ProxyLight.this.error(null, t2);
            } finally {
                ProxyLight.this.running = false;
            }
        }
    }

    public int getPort() {
        return this.port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public synchronized void start() throws Exception {
        if (!this.running) {
            this.running = true;
            Thread t = new Thread(new C06041());
            t.setDaemon(false);
            t.setName("ProxyLight server");
            t.start();
        }
    }

    protected InetAddress resolve(String host) {
        InetAddress retour = (InetAddress) this.ipCache.get(host);
        if (retour == null) {
            try {
                retour = InetAddress.getByName(host);
                this.ipCache.put(host, retour);
            } catch (UnknownHostException e) {
                return null;
            } catch (Throwable t) {
                error(BuildConfig.VERSION_NAME, t);
            }
        }
        return retour;
    }

    public void stop() {
        if (this.running) {
            try {
                this.server.close();
                this.server = null;
                this.selector.wakeup();
                this.selector = null;
                while (this.processors.size() > 0) {
                    ((RequestProcessor) this.processors.pop()).shutdown();
                }
            } catch (Exception e) {
                error(null, e);
            }
        }
    }

    public void error(String message, Throwable t) {
        if (message != null) {
            System.err.println(message);
        }
        if (t == null) {
        }
    }

    public void debug(String message) {
        if (message != null) {
            System.err.println(message);
        }
    }

    public boolean isRunning() {
        return this.running;
    }

    public void setRemoteProxy(String host, int port) {
        this.remoteProxyHost = host;
        this.remoteProxyPort = port;
    }

    public String getRemoteProxyHost() {
        return this.remoteProxyHost;
    }

    public int getRemoteProxyPort() {
        return this.remoteProxyPort;
    }

    public void recycle(RequestProcessor processor) {
        synchronized (this.processors) {
            this.processors.add(processor);
        }
    }

    public List<RequestFilter> getRequestFilters() {
        return this.filters;
    }
}
