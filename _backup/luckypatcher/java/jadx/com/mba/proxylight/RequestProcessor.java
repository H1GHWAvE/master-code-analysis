package com.mba.proxylight;

import android.support.v4.media.TransportMediator;
import com.android.vending.billing.InAppBillingService.LUCK.BuildConfig;
import com.chelpus.Utils;
import java.io.IOException;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.spi.SelectorProvider;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public abstract class RequestProcessor {
    private static final byte[] CONNECT_OK = "HTTP/1.0 200 Connection established\r\nProxy-agent: ProxyLight\r\n\r\n".getBytes();
    private static final String CRLF = "\r\n";
    private static long SOCKET_TIMEOUT = 15000;
    private static int processorsCount = 0;
    private static int processorsCpt = 1;
    private boolean alive;
    private Socket currentOutSocket;
    private String inData;
    private Socket inSocket;
    private String outData;
    private Map<String, Socket> outSockets;
    private int processorIdx;
    private ByteBuffer readBuffer;
    private char[] read_buf;
    int read_offset;
    private Selector selector;
    private boolean shutdown;
    private Thread f4t;

    class C06051 implements Runnable {
        C06051() {
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            RequestProcessor.access$008();
            while (true) {
                try {
                    synchronized (RequestProcessor.this) {
                        RequestProcessor.this.alive = true;
                        if (RequestProcessor.this.selector == null && !RequestProcessor.this.shutdown) {
                            try {
                                RequestProcessor.this.wait(20000);
                            } catch (InterruptedException e) {
                                RequestProcessor.this.error(null, e);
                                RequestProcessor.this.closeAll();
                                RequestProcessor.access$010();
                                RequestProcessor.this.error("Fin du processor " + RequestProcessor.this.processorIdx, null);
                                return;
                            }
                        }
                        if (RequestProcessor.this.shutdown) {
                            RequestProcessor.this.closeAll();
                            RequestProcessor.access$010();
                            RequestProcessor.this.error("Fin du processor " + RequestProcessor.this.processorIdx, null);
                            return;
                        }
                    }
                } catch (Throwable th) {
                    RequestProcessor.this.closeAll();
                    RequestProcessor.access$010();
                    RequestProcessor.this.error("Fin du processor " + RequestProcessor.this.processorIdx, null);
                }
            }
            try {
                if (so == RequestProcessor.this.currentOutSocket) {
                    RequestProcessor.this.currentOutSocket = null;
                }
            } catch (Exception e2) {
                Exception e3 = e2;
            }
            RequestProcessor.this.error(null, e3);
            System.out.println("inData:" + RequestProcessor.this.inData);
            RequestProcessor.this.inData = BuildConfig.VERSION_NAME;
            System.out.println("outData:" + RequestProcessor.this.outData);
            RequestProcessor.this.outData = BuildConfig.VERSION_NAME;
            RequestProcessor.this.closeAll();
            RequestProcessor.this.recycle();
        }
    }

    private enum REQUEST_STEP {
        STATUS_LINE,
        REQUEST_HEADERS,
        REQUEST_CONTENT,
        TRANSFER
    }

    public abstract void debug(String str);

    public abstract void error(String str, Throwable th);

    public abstract String getRemoteProxyHost();

    public abstract int getRemoteProxyPort();

    public abstract List<RequestFilter> getRequestFilters();

    public abstract void recycle();

    public abstract InetAddress resolve(String str);

    static /* synthetic */ int access$008() {
        int i = processorsCount;
        processorsCount = i + 1;
        return i;
    }

    static /* synthetic */ int access$010() {
        int i = processorsCount;
        processorsCount = i - 1;
        return i;
    }

    public RequestProcessor() throws IOException {
        this.f4t = null;
        this.alive = false;
        this.shutdown = false;
        this.processorIdx = 1;
        this.selector = null;
        this.readBuffer = ByteBuffer.allocate(TransportMediator.FLAG_KEY_MEDIA_NEXT);
        this.inSocket = null;
        this.inData = BuildConfig.VERSION_NAME;
        this.outData = BuildConfig.VERSION_NAME;
        this.outSockets = new HashMap();
        this.currentOutSocket = null;
        this.read_buf = new char[TransportMediator.FLAG_KEY_MEDIA_NEXT];
        this.read_offset = 0;
        this.f4t = new Thread(new C06051());
        Thread thread = this.f4t;
        StringBuilder append = new StringBuilder().append("ProxyLight processor - ");
        int i = processorsCpt;
        processorsCpt = i + 1;
        this.processorIdx = i;
        thread.setName(append.append(i).toString());
        this.f4t.setDaemon(true);
        this.f4t.start();
        while (!isAlive()) {
            new Utils("w").waitLP(50);
        }
        debug("Processeur " + this.processorIdx + " demarre.");
    }

    private void write(Socket socket, ByteBuffer b, long when) throws IOException {
        socket.socket.write(b);
        socket.lastWrite = when;
    }

    private int read(Socket socket, ByteBuffer b, long when) throws IOException {
        int retour = socket.socket.read(b);
        if (retour > 0) {
            socket.lastWrite = when;
        }
        return retour;
    }

    private void closeOutSocket(Socket out) {
        try {
            for (Entry<String, Socket> e : this.outSockets.entrySet()) {
                if (e.getValue() == out) {
                    this.outSockets.remove(e.getKey());
                    debug("Fermeture de la socket vers " + ((String) e.getKey()));
                    break;
                }
            }
            if (out.socket.isOpen()) {
                out.socket.close();
            }
        } catch (Exception e2) {
            error(BuildConfig.VERSION_NAME, e2);
        }
    }

    private void closeAll() {
        if (this.inSocket != null) {
            try {
                this.inSocket.socket.close();
            } catch (Exception e) {
                error(null, e);
            }
            this.inSocket = null;
        }
        for (Socket outSocket : this.outSockets.values()) {
            try {
                outSocket.socket.close();
            } catch (Exception e2) {
                error(null, e2);
            }
        }
        this.outSockets.clear();
        this.currentOutSocket = null;
        if (this.selector != null) {
            try {
                this.selector.wakeup();
            } catch (Exception e22) {
                error(null, e22);
            }
            this.selector = null;
        }
    }

    private boolean transfer(Socket inSocket, Socket outSocket, long when) throws IOException {
        this.readBuffer.clear();
        int numRead = read(inSocket, this.readBuffer, when);
        if (numRead == -1) {
            return false;
        }
        if (numRead > 0) {
            this.readBuffer.flip();
            write(outSocket, this.readBuffer, when);
        }
        return true;
    }

    public void process(SelectionKey key) throws IOException {
        synchronized (this) {
            ServerSocketChannel serverSocketChannel = (ServerSocketChannel) key.channel();
            this.inSocket = new Socket();
            this.inSocket.socket = serverSocketChannel.accept();
            this.inSocket.socket.configureBlocking(false);
            this.selector = SelectorProvider.provider().openSelector();
            this.inSocket.socket.register(this.selector, 1, this.inSocket);
            notify();
        }
    }

    private String readNext(ByteBuffer buffer) throws IOException {
        boolean atCR = false;
        while (buffer.remaining() > 0) {
            int ch = buffer.get();
            if (ch == -1 || ch == 10) {
                atCR = true;
                break;
            } else if (ch != 13) {
                if (this.read_offset == this.read_buf.length) {
                    char[] tmpbuf = this.read_buf;
                    this.read_buf = new char[(tmpbuf.length * 2)];
                    System.arraycopy(tmpbuf, 0, this.read_buf, 0, this.read_offset);
                }
                char[] cArr = this.read_buf;
                int i = this.read_offset;
                this.read_offset = i + 1;
                cArr[i] = (char) ch;
            }
        }
        if (!atCR) {
            return null;
        }
        String s = String.copyValueOf(this.read_buf, 0, this.read_offset);
        this.read_offset = 0;
        return s;
    }

    public void shutdown() {
        closeAll();
        this.shutdown = true;
        synchronized (this) {
            notify();
        }
    }

    public boolean isAlive() {
        return this.alive;
    }

    private boolean filterRequest(Request request) {
        List<RequestFilter> filters = getRequestFilters();
        if (filters.size() > 0) {
            for (int i = 0; i < filters.size(); i++) {
                if (((RequestFilter) filters.get(i)).filter(request)) {
                    return true;
                }
            }
        }
        return false;
    }
}
