package com.google.android.vending.licensing;

import org.tukaani.xz.DeltaOptions;

public class NullDeviceLimiter implements DeviceLimiter {
    public int isDeviceAllowed(String userId) {
        return DeltaOptions.DISTANCE_MAX;
    }
}
