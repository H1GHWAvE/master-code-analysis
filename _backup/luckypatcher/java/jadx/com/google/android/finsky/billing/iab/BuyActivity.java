package com.google.android.finsky.billing.iab;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import com.android.vending.billing.InAppBillingService.LUCK.BuildConfig;
import com.android.vending.billing.InAppBillingService.LUCK.C0149R;
import com.android.vending.billing.InAppBillingService.LUCK.LogCollector;
import com.chelpus.Utils;
import com.google.android.finsky.billing.iab.google.util.IabHelper;
import org.json.JSONException;
import org.json.JSONObject;
import org.tukaani.xz.common.Util;

public class BuyActivity extends Activity {
    public static final String BUY_INTENT = "com.google.android.finsky.billing.iab.BUY";
    public static final String EXTRA_DEV_PAYLOAD = "payload";
    public static final String EXTRA_PACKAGENAME = "packageName";
    public static final String EXTRA_PRODUCT_ID = "product";
    public static final String TAG = "BillingHack";
    Bundle bundle = null;
    public BuyActivity context = null;
    String devPayload = BuildConfig.VERSION_NAME;
    String packageName = BuildConfig.VERSION_NAME;
    String productId = BuildConfig.VERSION_NAME;
    String type = BuildConfig.VERSION_NAME;

    class C05862 implements OnClickListener {
        C05862() {
        }

        public void onClick(View v) {
            Intent data = new Intent();
            Bundle extras = new Bundle();
            extras.putInt(IabHelper.RESPONSE_CODE, 1);
            data.putExtras(extras);
            BuyActivity.this.setResult(0, data);
            BuyActivity.this.finish();
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = this;
        if (BUY_INTENT.equals(getIntent().getAction()) || savedInstanceState != null) {
            Log.d(TAG, "Buy intent!");
            System.out.println(getRequestedOrientation());
            if (savedInstanceState != null) {
                this.packageName = savedInstanceState.getString(EXTRA_PACKAGENAME);
                this.productId = savedInstanceState.getString(EXTRA_PRODUCT_ID);
                this.devPayload = savedInstanceState.getString(EXTRA_DEV_PAYLOAD);
                this.type = savedInstanceState.getString("Type");
            } else {
                this.packageName = getIntent().getExtras().getString(EXTRA_PACKAGENAME);
                this.productId = getIntent().getExtras().getString(EXTRA_PRODUCT_ID);
                this.devPayload = getIntent().getExtras().getString(EXTRA_DEV_PAYLOAD);
                this.type = getIntent().getExtras().getString("Type");
            }
            System.out.println(this.type);
            JSONObject purch = new JSONObject();
            try {
                purch.put("orderId", (Utils.getRandom(1000000000000000000L, Util.VLI_MAX) + Utils.getRandom(0, 9)) + "." + Utils.getRandom(1000000000000000L, 9999999999999999L));
                purch.put(EXTRA_PACKAGENAME, this.packageName);
                purch.put("productId", this.productId);
                purch.put("purchaseTime", new Long(System.currentTimeMillis()));
                purch.put("purchaseState", new Integer(0));
                purch.put("developerPayload", this.devPayload);
                purch.put("purchaseToken", Utils.getRandomStringLowerCase(24));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            final String pData = purch.toString();
            String autorepeat = getIntent().getExtras().getString("autorepeat");
            if (autorepeat != null) {
                Intent data = new Intent();
                Bundle extras = new Bundle();
                String signature = BuildConfig.VERSION_NAME;
                if (autorepeat.equals("1")) {
                    signature = Utils.gen_sha1withrsa(pData);
                } else {
                    signature = BuildConfig.VERSION_NAME;
                }
                extras.putInt(IabHelper.RESPONSE_CODE, 0);
                extras.putString(IabHelper.RESPONSE_INAPP_PURCHASE_DATA, pData);
                extras.putString(IabHelper.RESPONSE_INAPP_SIGNATURE, signature);
                data.putExtras(extras);
                setResult(-1, data);
                finish();
            }
            setContentView(C0149R.layout.buy_dialog);
            Button btnYes = (Button) findViewById(C0149R.id.button_yes);
            Button btnNo = (Button) findViewById(C0149R.id.button_no);
            final CheckBox check = (CheckBox) findViewById(C0149R.id.checkBox);
            if (!(Utils.checkCoreJarPatch11() || Utils.isRebuildedOrOdex(this.packageName, this))) {
                check.setChecked(true);
            }
            final CheckBox check2 = (CheckBox) findViewById(C0149R.id.checkBox2);
            final CheckBox check3 = (CheckBox) findViewById(C0149R.id.checkBox3);
            TextView text2 = (TextView) findViewById(C0149R.id.textView2);
            text2.setText(Utils.getText(C0149R.string.billing_hack2_auto_repeat_note));
            text2.append(LogCollector.LINE_SEPARATOR + Utils.getText(C0149R.string.billing_hack2_new));
            check2.setChecked(false);
            check3.setChecked(false);
            btnYes.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    Intent data = new Intent();
                    Bundle extras = new Bundle();
                    String signature = BuildConfig.VERSION_NAME;
                    String save_signature = BuildConfig.VERSION_NAME;
                    if (check.isChecked()) {
                        save_signature = BuildConfig.VERSION_NAME;
                        signature = BuildConfig.VERSION_NAME;
                    } else {
                        signature = Utils.gen_sha1withrsa(pData);
                        save_signature = "1";
                    }
                    if (check2.isChecked()) {
                        new DbHelper(BuyActivity.this.getApplicationContext(), BuyActivity.this.packageName).saveItem(new ItemsListItem(BuyActivity.this.productId, pData, save_signature));
                    }
                    if (check3.isChecked()) {
                        new DbHelper(BuyActivity.this.getApplicationContext(), BuyActivity.this.packageName).saveItem(new ItemsListItem(BuyActivity.this.productId, "auto.repeat.LP", save_signature));
                    }
                    extras.putInt(IabHelper.RESPONSE_CODE, 0);
                    extras.putString(IabHelper.RESPONSE_INAPP_PURCHASE_DATA, pData);
                    extras.putString(IabHelper.RESPONSE_INAPP_SIGNATURE, signature);
                    data.putExtras(extras);
                    BuyActivity.this.setResult(-1, data);
                    BuyActivity.this.finish();
                }
            });
            btnNo.setOnClickListener(new C05862());
            return;
        }
        finish();
    }

    protected void onSaveInstanceState(Bundle outState) {
        System.out.println("save instance");
        outState.putString(EXTRA_PACKAGENAME, this.packageName);
        outState.putString(EXTRA_PRODUCT_ID, this.productId);
        outState.putString(EXTRA_DEV_PAYLOAD, this.devPayload);
        outState.putString("Type", this.type);
        super.onSaveInstanceState(outState);
    }

    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        System.out.println("load instance");
        this.packageName = savedInstanceState.getString(EXTRA_PACKAGENAME);
        super.onRestoreInstanceState(savedInstanceState);
    }

    public void onConfigurationChanged(Configuration newConfig) {
        if (newConfig.orientation == 2) {
            setRequestedOrientation(0);
        } else {
            setRequestedOrientation(1);
        }
        super.onConfigurationChanged(newConfig);
    }
}
