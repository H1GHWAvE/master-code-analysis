package com.google.android.finsky.billing.iab;

public class ItemsListItem {
    public String itemID;
    public String pData;
    public String pSignature;

    public ItemsListItem(String itemId, String Data, String Signature) {
        this.itemID = itemId;
        this.pData = Data;
        this.pSignature = Signature;
    }
}
