package com.google.android.finsky.billing.iab;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import com.android.vending.billing.InAppBillingService.LUCK.PkgListItem;
import com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment;
import java.util.ArrayList;
import java.util.List;

public class DbHelper extends SQLiteOpenHelper {
    public static Context contextdb = null;
    static final String dbName = "BillingRestoreTransactions";
    public static boolean getPackage = false;
    static final String itemID = "itemID";
    static final String pData = "Data";
    static final String pSignature = "Signature";
    static String packageTable = "Packages";
    public static boolean savePackage = false;

    public DbHelper(Context context, String packageTable) {
        super(context, dbName, null, 41);
        contextdb = context;
        packageTable = packageTable;
        try {
            if (listAppsFragment.billing_db == null) {
                listAppsFragment.billing_db = getWritableDatabase();
            } else {
                listAppsFragment.billing_db.close();
                listAppsFragment.billing_db = getWritableDatabase();
            }
            listAppsFragment.billing_db = listAppsFragment.billing_db;
            if (listAppsFragment.billing_db.getVersion() != 41) {
                System.out.println("SQL delete and recreate.");
                listAppsFragment.billing_db.execSQL("DROP TABLE IF EXISTS " + packageTable);
                onCreate(listAppsFragment.billing_db);
            }
            onCreate(listAppsFragment.billing_db);
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
    }

    public DbHelper(Context context) {
        super(context, dbName, null, 41);
        contextdb = context;
        packageTable = packageTable;
        try {
            if (listAppsFragment.billing_db == null) {
                listAppsFragment.billing_db = getWritableDatabase();
            } else {
                listAppsFragment.billing_db.close();
                listAppsFragment.billing_db = getWritableDatabase();
            }
            System.out.println("SQLite base version is " + listAppsFragment.billing_db.getVersion());
            if (listAppsFragment.billing_db.getVersion() != 41) {
                System.out.println("SQL delete and recreate.");
                listAppsFragment.billing_db.execSQL("DROP TABLE IF EXISTS " + packageTable);
                onCreate(listAppsFragment.billing_db);
            }
            onCreate(listAppsFragment.billing_db);
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS '" + packageTable + "' (" + itemID + " TEXT PRIMARY KEY, " + pData + " TEXT, " + pSignature + " TEXT" + ");");
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS '" + packageTable + "'");
        onCreate(db);
    }

    public ArrayList<ItemsListItem> getItems() {
        ArrayList<ItemsListItem> pat = new ArrayList();
        pat.clear();
        try {
            Cursor c = listAppsFragment.billing_db.query("'" + packageTable + "'", new String[]{itemID, pData, pSignature}, null, null, null, null, null);
            c.moveToFirst();
            do {
                try {
                    try {
                        pat.add(new ItemsListItem(c.getString(c.getColumnIndexOrThrow(itemID)), c.getString(c.getColumnIndex(pData)), c.getString(c.getColumnIndex(pSignature))));
                    } catch (IllegalArgumentException e) {
                    }
                } catch (Exception e2) {
                }
                try {
                } catch (Exception e3) {
                    c.close();
                }
            } while (c.moveToNext());
            c.close();
            getPackage = false;
        } catch (Exception e4) {
            getPackage = false;
            System.out.println("LuckyPatcher-Error: getPackage " + e4);
        }
        return pat;
    }

    public void saveItem(ItemsListItem savedObject) throws SQLiteException {
        try {
            savePackage = true;
            ContentValues cv = new ContentValues();
            cv.put(itemID, savedObject.itemID);
            cv.put(pData, savedObject.pData);
            cv.put(pSignature, savedObject.pSignature);
            try {
                listAppsFragment.billing_db.insertOrThrow("'" + packageTable + "'", itemID, cv);
            } catch (Exception e) {
                listAppsFragment.billing_db.replace("'" + packageTable + "'", null, cv);
            }
            savePackage = false;
            savePackage = false;
        } catch (Exception e2) {
            savePackage = false;
            System.out.println("LuckyPatcher-Error: savePackage " + e2);
        }
    }

    public void deleteItem(ItemsListItem deleteObject) {
        try {
            listAppsFragment.billing_db.delete("'" + packageTable + "'", "itemID = '" + deleteObject.itemID + "'", null);
        } catch (Exception e) {
            System.out.println("LuckyPatcher-Error: deletePackage " + e);
        }
    }

    public void deleteItem(String itemId) {
        try {
            listAppsFragment.billing_db.delete("'" + packageTable + "'", "itemID = '" + itemId + "'", null);
        } catch (Exception e) {
            System.out.println("LuckyPatcher-Error: deletePackage " + e);
        }
    }

    public void deleteAll() {
    }

    public void updatePackage(List<PkgListItem> list) {
    }

    public boolean isOpen() {
        if (listAppsFragment.billing_db.isOpen()) {
            return true;
        }
        return false;
    }
}
