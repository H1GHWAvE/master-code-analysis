package com.chelpus.root.utils;

import com.android.vending.billing.InAppBillingService.LUCK.BuildConfig;
import com.android.vending.billing.InAppBillingService.LUCK.LogCollector;
import com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment;
import com.chelpus.Utils;
import java.io.File;
import java.io.IOException;
import org.tukaani.xz.LZMA2Options;
import pxb.android.axml.AxmlParser;

public class checkRoot {

    static class C05451 {
        C05451() {
        }
    }

    public static void main(String[] paramArrayOfString) {
        String api = BuildConfig.VERSION_NAME;
        String runtime = BuildConfig.VERSION_NAME;
        Utils.startRootJava(new C05451());
        String toolfilesdir = BuildConfig.VERSION_NAME;
        if (paramArrayOfString[0] != null) {
            toolfilesdir = paramArrayOfString[0];
        }
        if (paramArrayOfString[1] != null) {
            api = paramArrayOfString[1];
        }
        if (paramArrayOfString[2] != null) {
            runtime = paramArrayOfString[2];
        }
        listAppsFragment.toolfilesdir = toolfilesdir;
        System.out.println("root found!");
        File dir = new File("/data/lp");
        File utils_file = new File("/data/lp/lp_utils");
        if (utils_file.exists()) {
            String path = BuildConfig.VERSION_NAME;
            String apiS = BuildConfig.VERSION_NAME;
            String runtimeS = BuildConfig.VERSION_NAME;
            String[] params = Utils.read_from_file(utils_file).split("%chelpus%");
            if (params != null && params.length > 0) {
                for (int i = 0; i < params.length; i++) {
                    switch (i) {
                        case LZMA2Options.MODE_UNCOMPRESSED /*0*/:
                            path = params[i];
                            continue;
                        case AxmlParser.START_FILE /*1*/:
                            apiS = params[i];
                            break;
                        case AxmlParser.START_TAG /*2*/:
                            break;
                        default:
                            break;
                    }
                    runtimeS = params[i];
                }
            }
            if (path.equals(toolfilesdir) && api.equals(apiS) && runtime.equals(runtimeS)) {
                System.out.println("Utils file found and correct");
                Utils.run_all_no_root("chmod", "777", utils_file.getAbsolutePath());
            } else {
                System.out.println("Utils file contain incorrect path " + path);
                Utils.run_all_no_root("chmod", "777", utils_file.getAbsolutePath());
                Utils.run_all_no_root("chmod", "777", dir.getAbsolutePath());
                Utils.save_text_to_file(utils_file, toolfilesdir + "%chelpus%" + api + "%chelpus%" + runtime);
            }
        } else {
            dir.mkdirs();
            if (!dir.exists()) {
                System.out.println("LP: dirs for utils not created.");
            }
            try {
                utils_file.createNewFile();
                Utils.save_text_to_file(utils_file, toolfilesdir + "%chelpus%" + api + "%chelpus%" + runtime);
                Utils.run_all_no_root("chmod", "777", utils_file.getAbsolutePath());
                Utils.run_all_no_root("chmod", "777", dir.getAbsolutePath());
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("LP: file for utils not created.");
            }
        }
        if (!Utils.initXposedParam()) {
            System.out.println("Xposed settings not initialized");
        }
        if (!(new File("/system/xbin/busybox").exists() || new File("/system/bin/busybox").exists() || new File("/bin/busybox").exists() || new File("/sbin/busybox").exists())) {
            System.out.println("LuckyPatcher: busybox not install!");
            System.out.println("busybox not found!");
        }
        if (new File(toolfilesdir + "/dalvikvm").exists()) {
            String dalvikvm = toolfilesdir + "/dalvikvm";
            Utils.run_all_no_root("chmod", "777", dalvikvm);
            Utils.run_all_no_root("chown", "0.0", dalvikvm);
            Utils.run_all_no_root("chown", "0:0", dalvikvm);
        }
        if (new File(toolfilesdir + "/busybox").exists()) {
            String busybox = toolfilesdir + "/busybox";
            Utils.run_all_no_root("chmod", "06777", busybox);
            Utils.run_all_no_root("chown", "0.0", busybox);
            Utils.run_all_no_root("chown", "0:0", busybox);
        }
        if (new File(toolfilesdir + "/reboot").exists()) {
            String reboot = toolfilesdir + "/reboot";
            Utils.run_all_no_root("chmod", "777", reboot);
            Utils.run_all_no_root("chown", "0.0", reboot);
            Utils.run_all_no_root("chown", "0:0", reboot);
        }
        int j = 0;
        try {
            File appDir = new File("/data/app/");
            if (appDir.exists()) {
                File[] files = appDir.listFiles();
                System.out.print("\nUnusedOdexList:");
                for (File file : files) {
                    File TempFile = new File(Utils.changeExtension(file.toString(), "apk"));
                    if (file.toString().toLowerCase().endsWith(".odex") && !TempFile.exists()) {
                        file.delete();
                        System.out.print(file + "|");
                        j++;
                    }
                }
                System.out.print(LogCollector.LINE_SEPARATOR);
            }
        } catch (Exception e2) {
            System.out.println("Exception e" + e2.toString());
        }
        if (j > 0) {
            System.out.println("Unused ODEX in /data/app/ removed!");
        }
        Utils.exitFromRootJava();
    }
}
