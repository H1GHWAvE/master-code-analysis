package com.chelpus.root.utils;

import com.android.vending.billing.InAppBillingService.LUCK.AxmlExample;
import com.android.vending.billing.InAppBillingService.LUCK.BuildConfig;
import com.android.vending.billing.InAppBillingService.LUCK.CommandItem;
import com.android.vending.billing.InAppBillingService.LUCK.LogOutputStream;
import com.android.vending.billing.InAppBillingService.LUCK.PatchesItemAuto;
import com.android.vending.billing.InAppBillingService.LUCK.StringItem;
import com.android.vending.billing.InAppBillingService.LUCK.TypesItem;
import com.android.vending.billing.InAppBillingService.LUCK.listAppsFragment;
import com.chelpus.Common;
import com.chelpus.Utils;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileChannel.MapMode;
import java.security.DigestException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.zip.Adler32;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.util.InternalZipConstants;
import org.tukaani.xz.common.Util;

public class runpatchsupportOld {
    private static boolean ART = false;
    public static String appdir = "/sdcard/";
    public static ArrayList<File> classesFiles = new ArrayList();
    private static boolean copyDC = false;
    private static boolean createAPK = false;
    public static File crkapk;
    public static String dir = "/sdcard/";
    public static String dir2 = "/sdcard/";
    public static String dirapp = "/data/app/";
    public static ArrayList<File> filestopatch = null;
    private static boolean pattern1 = true;
    private static boolean pattern2 = true;
    private static boolean pattern3 = true;
    public static PrintStream print;
    public static String result;
    public static String sddir = "/sdcard/";
    public static boolean system = false;
    public static String uid = BuildConfig.VERSION_NAME;

    static class C05811 {
        C05811() {
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void main(String[] paramArrayOfString) {
        Iterator it;
        OutputStream logOutputStream = new LogOutputStream("System.out");
        print = new PrintStream(logOutputStream);
        Utils.startRootJava(new C05811());
        Utils.kill(paramArrayOfString[0]);
        print.println("Support-Code Running!");
        ArrayList<PatchesItemAuto> patchesList = new ArrayList();
        pattern1 = true;
        pattern2 = true;
        pattern3 = true;
        filestopatch = new ArrayList();
        try {
            for (File file : new File(paramArrayOfString[3]).listFiles()) {
                if (!(!file.isFile() || file.getName().equals("busybox") || file.getName().equals("reboot") || file.getName().equals("dalvikvm"))) {
                    file.delete();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            if (!paramArrayOfString[1].contains("pattern0")) {
                pattern1 = false;
            }
            if (!paramArrayOfString[1].contains("pattern1")) {
                pattern2 = false;
            }
            if (!paramArrayOfString[1].contains("pattern2")) {
                pattern3 = false;
            }
            if (paramArrayOfString[6] != null && paramArrayOfString[6].contains("createAPK")) {
                createAPK = true;
            }
            if (paramArrayOfString[6] != null && paramArrayOfString[6].contains("ART")) {
                ART = true;
            }
            if (paramArrayOfString[6] != null) {
                Utils.sendFromRoot(paramArrayOfString[6]);
            }
            if (paramArrayOfString[7] != null) {
                uid = paramArrayOfString[7];
            }
            System.out.println("uid:" + uid);
        } catch (NullPointerException e2) {
        } catch (Exception e3) {
        }
        try {
            if (paramArrayOfString[5].contains("copyDC")) {
                copyDC = true;
            }
        } catch (NullPointerException e4) {
        } catch (Exception e5) {
        }
        if (createAPK) {
            listAppsFragment.startUnderRoot = Boolean.valueOf(false);
        }
        ArrayList<String> origStr = new ArrayList();
        ArrayList<String> replStr = new ArrayList();
        ArrayList<Boolean> trigger = new ArrayList();
        ArrayList<String> ResultText = new ArrayList();
        ArrayList<String> markers = new ArrayList();
        ArrayList<Boolean> dontConvert = new ArrayList();
        origStr.add("1A ?? FF FF");
        replStr.add("1A ?? ?? ??");
        trigger.add(Boolean.valueOf(true));
        ResultText.add("(pak intekekt 0)");
        markers.add("search_pack");
        dontConvert.add(Boolean.valueOf(false));
        origStr.add("1B ?? FF FF FF FF");
        replStr.add("1B ?? ?? ?? ?? ??");
        trigger.add(Boolean.valueOf(true));
        ResultText.add("(pak intekekt 0)");
        markers.add("search_pack");
        dontConvert.add(Boolean.valueOf(false));
        origStr.add("1A ?? FF FF");
        replStr.add("1A ?? ?? ??");
        trigger.add(Boolean.valueOf(true));
        ResultText.add("(sha intekekt 2)");
        markers.add("search");
        dontConvert.add(Boolean.valueOf(false));
        origStr.add("1B ?? FF FF FF FF");
        replStr.add("1B ?? ?? ?? ?? ??");
        trigger.add(Boolean.valueOf(true));
        ResultText.add("(sha intekekt 2 32 bit)");
        markers.add("search");
        dontConvert.add(Boolean.valueOf(false));
        origStr.add("0A ?? 39 ?? ?? 00");
        replStr.add("12 S1 39 ?? ?? 00");
        trigger.add(Boolean.valueOf(false));
        ResultText.add("support2 Fixed!\n(sha intekekt 3)");
        markers.add("search");
        dontConvert.add(Boolean.valueOf(false));
        origStr.add("1A ?? FF FF 6E 20 ?? ?? ?? ?? 0C ??");
        replStr.add("1A ?? ?? ?? 00 00 00 00 00 00 00 00");
        trigger.add(Boolean.valueOf(false));
        ResultText.add("support1 Fixed!\n(pak intekekt)");
        markers.add("search_pack");
        dontConvert.add(Boolean.valueOf(false));
        origStr.add("1A ?? FF FF 6E 20 ?? ?? ?? ??");
        replStr.add("1A ?? ?? ?? 00 00 00 00 00 00");
        trigger.add(Boolean.valueOf(false));
        ResultText.add("support1 Fixed!\n(pak intekekt)");
        markers.add("search_pack");
        dontConvert.add(Boolean.valueOf(false));
        origStr.add("1A ?? FF FF 6E 20 ?? ?? ?? ??");
        replStr.add("1A ?? ?? ?? 00 00 00 00 00 00");
        trigger.add(Boolean.valueOf(false));
        ResultText.add("support1 Fixed!\n(pak intekekt)");
        markers.add("search_pack");
        dontConvert.add(Boolean.valueOf(true));
        origStr.add("1B ?? FF FF FF FF 6E 20 ?? ?? ?? ?? 0C ??");
        replStr.add("1B ?? ?? ?? ?? ?? 00 00 00 00 00 00 00 00");
        trigger.add(Boolean.valueOf(false));
        ResultText.add("support1 Fixed!\n(pak intekekt 32 bit)");
        markers.add("search_pack");
        dontConvert.add(Boolean.valueOf(false));
        origStr.add("1B ?? FF FF FF FF 6E 20 ?? ?? ?? ??");
        replStr.add("1B ?? ?? ?? ?? ?? 00 00 00 00 00 00");
        trigger.add(Boolean.valueOf(false));
        ResultText.add("support1 Fixed!\n(pak intekekt 32 bit)");
        markers.add("search_pack");
        dontConvert.add(Boolean.valueOf(false));
        origStr.add("1B ?? FF FF FF FF 6E 20 ?? ?? ?? ??");
        replStr.add("1B ?? ?? ?? ?? ?? 00 00 00 00 00 00");
        trigger.add(Boolean.valueOf(false));
        ResultText.add("support1 Fixed!\n(pak intekekt 32 bit)");
        markers.add("search_pack");
        dontConvert.add(Boolean.valueOf(true));
        origStr.add("6E 20 FF FF ?? 00 0A ??");
        replStr.add("6E 20 ?? ?? ?? 00 12 S1");
        trigger.add(Boolean.valueOf(false));
        ResultText.add("support2 Fixed!\n(sha intekekt 4)");
        markers.add(BuildConfig.VERSION_NAME);
        dontConvert.add(Boolean.valueOf(false));
        origStr.add("6E 20 FF FF ?? 00");
        replStr.add("00 00 00 00 00 00");
        trigger.add(Boolean.valueOf(false));
        ResultText.add("support3 Fixed!\n(intent for free)");
        markers.add(BuildConfig.VERSION_NAME);
        dontConvert.add(Boolean.valueOf(false));
        origStr.add("0A ?? 39 ?? ?? ??");
        replStr.add("12 S1 39 ?? ?? ??");
        trigger.add(Boolean.valueOf(false));
        ResultText.add("support1 Fixed!\n(ev1)");
        markers.add("search_sign_ver");
        dontConvert.add(Boolean.valueOf(false));
        origStr.add("0A ?? 38 ?? ?? ??");
        replStr.add("12 S1 38 ?? ?? ??");
        trigger.add(Boolean.valueOf(false));
        ResultText.add("support1 Fixed!\n(ev1)");
        markers.add("search_sign_ver");
        dontConvert.add(Boolean.valueOf(false));
        origStr.add("1C ?? FF FF");
        replStr.add("1C ?? ?? ??");
        trigger.add(Boolean.valueOf(false));
        ResultText.add("support1 Fixed!\n(si)");
        markers.add("search_sign_ver");
        dontConvert.add(Boolean.valueOf(false));
        origStr.add("23 ?? ?? ?? 1C ?? ?? ?? 12 ?? 4D ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0C ?? 1F ?? ?? ?? 6E ?? ?? ?? ?? ?? 0A ?? 39 ?? ?? ?? 1A ?? ?? ?? 1A");
        replStr.add("23 ?? ?? ?? 1C ?? ?? ?? 12 ?? 4D ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0C ?? 1F ?? ?? ?? 6E ?? ?? ?? ?? ?? 12 S1 39 ?? ?? ?? 1A ?? ?? ?? 1A");
        trigger.add(Boolean.valueOf(true));
        ResultText.add("support3 Fixed!\n(s)");
        markers.add(BuildConfig.VERSION_NAME);
        dontConvert.add(Boolean.valueOf(false));
        origStr.add("1A ?? ?? ?? 6E ?? ?? ?? ?? ?? 54 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 12 ?? 6E ?? ?? ?? ?? ?? 0C ?? 38 ?? ?? ?? 72 ?? ?? ?? ?? ?? 0A ?? 39 ?? ?? ?? 54 ?? ?? ?? 54 ?? ?? ?? 12 ?? 6E");
        replStr.add("1A ?? ?? ?? 00 00 00 00 00 00 54 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 12 ?? 6E ?? ?? ?? ?? ?? 0C ?? 38 ?? ?? ?? 72 ?? ?? ?? ?? ?? 0A ?? 39 ?? ?? ?? 54 ?? ?? ?? 54 ?? ?? ?? 12 ?? 6E");
        trigger.add(Boolean.valueOf(pattern2));
        ResultText.add("support4 Fixed!\n(pak)");
        markers.add(BuildConfig.VERSION_NAME);
        dontConvert.add(Boolean.valueOf(false));
        origStr.add("1A ?? ?? ?? 6E ?? ?? ?? ?? ?? 54 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0C ?? 72 ?? ?? ?? ?? ?? 0A ?? 39 ?? ?? ?? 54 ?? ?? ?? 54 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0E 00");
        replStr.add("1A ?? ?? ?? 00 00 00 00 00 00 54 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 6E ?? ?? ?? ?? ?? 0C ?? 72 ?? ?? ?? ?? ?? 0A ?? 39 ?? ?? ?? 54 ?? ?? ?? 54 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0E 00");
        trigger.add(Boolean.valueOf(pattern2));
        ResultText.add("support4 Fixed!\n(pak)");
        markers.add(BuildConfig.VERSION_NAME);
        dontConvert.add(Boolean.valueOf(false));
        origStr.add("1A ?? ?? ?? 6E ?? ?? ?? ?? ?? 54 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 12 ?? 6E ?? ?? ?? ?? ?? 0C ?? 72 ?? ?? ?? ?? ?? 0A ?? 39 ?? ?? ?? 54 ?? ?? ?? 54 ?? ?? ?? 12 ?? 6E ?? ?? ?? ?? ?? 0E 00");
        replStr.add("1A ?? ?? ?? 00 00 00 00 00 00 54 ?? ?? ?? 6E ?? ?? ?? ?? ?? 0C ?? 12 ?? 6E ?? ?? ?? ?? ?? 0C ?? 72 ?? ?? ?? ?? ?? 0A ?? 39 ?? ?? ?? 54 ?? ?? ?? 54 ?? ?? ?? 12 ?? 6E ?? ?? ?? ?? ?? 0E 00");
        trigger.add(Boolean.valueOf(pattern2));
        ResultText.add("support4 Fixed!\n(pak)");
        markers.add(BuildConfig.VERSION_NAME);
        dontConvert.add(Boolean.valueOf(false));
        if (paramArrayOfString[0].contains("com.jetappfactory.")) {
            origStr.add("71 30 ?? ?? ?? ?? 0C ?? 6E 20 ?? ?? ?? ?? 54 ?? ?? ?? 28 ??");
            replStr.add("71 30 ?? ?? ?? ?? 0C ?? 00 00 00 00 00 00 54 ?? ?? ?? 28 ??");
            trigger.add(Boolean.valueOf(pattern2));
            ResultText.add("support4 Fixed!\n(pak)");
            markers.add(BuildConfig.VERSION_NAME);
            dontConvert.add(Boolean.valueOf(false));
        }
        Utils.convertToPatchItemAuto(patchesList, origStr, replStr, trigger, ResultText, markers, dontConvert, Boolean.valueOf(false));
        try {
            if (listAppsFragment.startUnderRoot.booleanValue()) {
                Utils.remount(paramArrayOfString[2], "RW");
            }
            String packageName;
            File file2;
            File cl;
            if (createAPK || ART) {
                if (createAPK) {
                    packageName = paramArrayOfString[0];
                    appdir = paramArrayOfString[2];
                    sddir = paramArrayOfString[5];
                    clearTempSD();
                    file2 = new File(appdir);
                    unzipSD(file2);
                    crkapk = new File(sddir + "/Modified/" + packageName + ".apk");
                    Utils.copyFile(file2, crkapk);
                    File androidManifest = new File(sddir + "/Modified/AndroidManifest.xml");
                    if (androidManifest.exists()) {
                        if (!new AxmlExample().changeTargetApi(androidManifest, "19")) {
                            androidManifest.delete();
                        }
                        if (classesFiles == null || classesFiles.size() == 0) {
                            throw new FileNotFoundException();
                        }
                        filestopatch.clear();
                        it = classesFiles.iterator();
                        while (it.hasNext()) {
                            cl = (File) it.next();
                            if (cl.exists()) {
                                filestopatch.add(cl);
                            } else {
                                throw new FileNotFoundException();
                            }
                        }
                    }
                    throw new FileNotFoundException();
                }
                if (ART) {
                    Utils.sendFromRoot("ART mode create dex enabled.");
                    packageName = paramArrayOfString[0];
                    appdir = paramArrayOfString[2];
                    sddir = paramArrayOfString[3];
                    clearTempSD();
                    unzipART(new File(appdir));
                    if (classesFiles == null || classesFiles.size() == 0) {
                        throw new FileNotFoundException();
                    }
                    filestopatch.clear();
                    it = classesFiles.iterator();
                    while (it.hasNext()) {
                        cl = (File) it.next();
                        if (cl.exists()) {
                            filestopatch.add(cl);
                        } else {
                            throw new FileNotFoundException();
                        }
                    }
                }
            }
            dir = paramArrayOfString[3];
            dirapp = paramArrayOfString[2];
            clearTemp();
            if (paramArrayOfString[4].equals("not_system")) {
                system = false;
            }
            if (paramArrayOfString[4].equals("system")) {
                system = true;
            }
            filestopatch.clear();
            Utils.sendFromRoot("CLASSES mode create odex enabled.");
            packageName = paramArrayOfString[0];
            appdir = paramArrayOfString[2];
            sddir = paramArrayOfString[3];
            clearTempSD();
            file2 = new File(appdir);
            Utils.sendFromRoot("Get classes.dex.");
            print.println("Get classes.dex.");
            unzipART(file2);
            if (classesFiles == null || classesFiles.size() == 0) {
                throw new FileNotFoundException();
            }
            filestopatch.clear();
            it = classesFiles.iterator();
            while (it.hasNext()) {
                cl = (File) it.next();
                if (cl.exists()) {
                    filestopatch.add(cl);
                } else {
                    throw new FileNotFoundException();
                }
            }
            String odexstr = Utils.getPlaceForOdex(paramArrayOfString[2], true);
            File odexfile = new File(odexstr);
            if (odexfile.exists()) {
                odexfile.delete();
            }
            file2 = new File(odexstr.replace("-1", "-2"));
            if (file2.exists()) {
                file2.delete();
            }
            file2 = new File(odexstr.replace("-2", "-1"));
            if (file2.exists()) {
                file2.delete();
            }
        } catch (Exception e6) {
            e6.printStackTrace();
        } catch (FileNotFoundException e7) {
            Utils.sendFromRoot("Error: Program files are not found!\n\nCheck the location dalvik-cache to solve problems!\n\nDefault: /data/dalvik-cache/*");
        }
        Iterator it2 = filestopatch.iterator();
        while (it2.hasNext()) {
            ArrayList<StringItem> offsets;
            File filepatch = (File) it2.next();
            Utils.sendFromRoot("Find string id.");
            ArrayList<String> strings = new ArrayList();
            strings.add(Common.GOOGLEPLAY_PKG);
            strings.add("SHA1withRSA");
            strings.add("com.android.vending.billing.InAppBillingService.BIND");
            strings.add("Ljava/security/Signature;");
            strings.add("verify");
            strings.add("Landroid/content/Intent;");
            strings.add("setPackage");
            strings.add("engineVerify");
            strings.add("Ljava/security/SignatureSpi;");
            Utils.sendFromRoot("String analysis.");
            print.println("String analysis.");
            if (copyDC) {
                offsets = Utils.getStringIds(filepatch.getAbsolutePath(), strings, false);
            } else {
                offsets = Utils.getStringIds(filepatch.getAbsolutePath(), strings, false);
            }
            boolean f1 = false;
            boolean f2 = false;
            boolean f3 = false;
            boolean ev = false;
            boolean spi = false;
            ArrayList<TypesItem> types = new ArrayList();
            types.add(new TypesItem("Ljava/security/Signature;"));
            types.add(new TypesItem("Ljava/security/SignatureSpi;"));
            ArrayList<CommandItem> commands = new ArrayList();
            commands.add(new CommandItem("Ljava/security/Signature;", "verify"));
            commands.add(new CommandItem("Landroid/content/Intent;", "setPackage"));
            Iterator it3 = offsets.iterator();
            while (it3.hasNext()) {
                StringItem it4 = (StringItem) it3.next();
                it = types.iterator();
                while (it.hasNext()) {
                    TypesItem item = (TypesItem) it.next();
                    if (item.type.equals(it4.str)) {
                        item.Type = it4.offset;
                    }
                }
                it = commands.iterator();
                while (it.hasNext()) {
                    CommandItem item2 = (CommandItem) it.next();
                    if (item2.object.equals(it4.str)) {
                        item2.Object = it4.offset;
                    }
                    if (item2.method.equals(it4.str)) {
                        item2.Method = it4.offset;
                    }
                }
                if (it4.str.equals(Common.GOOGLEPLAY_PKG)) {
                    ((PatchesItemAuto) patchesList.get(5)).origByte[2] = it4.offset[0];
                    ((PatchesItemAuto) patchesList.get(5)).origByte[3] = it4.offset[1];
                    ((PatchesItemAuto) patchesList.get(6)).origByte[2] = it4.offset[0];
                    ((PatchesItemAuto) patchesList.get(6)).origByte[3] = it4.offset[1];
                    ((PatchesItemAuto) patchesList.get(7)).origByte[2] = it4.offset[0];
                    ((PatchesItemAuto) patchesList.get(7)).origByte[3] = it4.offset[1];
                    ((PatchesItemAuto) patchesList.get(8)).origByte[2] = it4.offset[0];
                    ((PatchesItemAuto) patchesList.get(8)).origByte[3] = it4.offset[1];
                    ((PatchesItemAuto) patchesList.get(8)).origByte[4] = it4.offset[2];
                    ((PatchesItemAuto) patchesList.get(8)).origByte[5] = it4.offset[3];
                    ((PatchesItemAuto) patchesList.get(9)).origByte[2] = it4.offset[0];
                    ((PatchesItemAuto) patchesList.get(9)).origByte[3] = it4.offset[1];
                    ((PatchesItemAuto) patchesList.get(9)).origByte[4] = it4.offset[2];
                    ((PatchesItemAuto) patchesList.get(9)).origByte[5] = it4.offset[3];
                    ((PatchesItemAuto) patchesList.get(10)).origByte[2] = it4.offset[0];
                    ((PatchesItemAuto) patchesList.get(10)).origByte[3] = it4.offset[1];
                    ((PatchesItemAuto) patchesList.get(10)).origByte[4] = it4.offset[2];
                    ((PatchesItemAuto) patchesList.get(10)).origByte[5] = it4.offset[3];
                    f1 = true;
                }
                if (it4.str.equals("com.android.vending.billing.InAppBillingService.BIND")) {
                    System.out.println("c.a.v.b.i " + it4.bits32 + " " + it4.offset[0] + " " + it4.offset[1] + " " + it4.offset[2] + " " + it4.offset[3]);
                    ((PatchesItemAuto) patchesList.get(0)).origByte[2] = it4.offset[0];
                    ((PatchesItemAuto) patchesList.get(0)).origByte[3] = it4.offset[1];
                    ((PatchesItemAuto) patchesList.get(1)).origByte[2] = it4.offset[0];
                    ((PatchesItemAuto) patchesList.get(1)).origByte[3] = it4.offset[1];
                    ((PatchesItemAuto) patchesList.get(1)).origByte[4] = it4.offset[2];
                    ((PatchesItemAuto) patchesList.get(1)).origByte[5] = it4.offset[3];
                    f2 = true;
                }
                if (it4.str.equals("SHA1withRSA")) {
                    ((PatchesItemAuto) patchesList.get(2)).origByte[2] = it4.offset[0];
                    ((PatchesItemAuto) patchesList.get(2)).origByte[3] = it4.offset[1];
                    ((PatchesItemAuto) patchesList.get(3)).origByte[2] = it4.offset[0];
                    ((PatchesItemAuto) patchesList.get(3)).origByte[3] = it4.offset[1];
                    ((PatchesItemAuto) patchesList.get(3)).origByte[4] = it4.offset[2];
                    ((PatchesItemAuto) patchesList.get(3)).origByte[5] = it4.offset[3];
                    f3 = true;
                }
                if (it4.str.equals("engineVerify")) {
                    ev = true;
                }
                if (it4.str.equals("Ljava/security/SignatureSpi;")) {
                    spi = true;
                }
            }
            if (!(f1 && f2)) {
                ((PatchesItemAuto) patchesList.get(0)).pattern = false;
                ((PatchesItemAuto) patchesList.get(1)).pattern = false;
                ((PatchesItemAuto) patchesList.get(5)).pattern = false;
                ((PatchesItemAuto) patchesList.get(6)).pattern = false;
                ((PatchesItemAuto) patchesList.get(7)).pattern = false;
                ((PatchesItemAuto) patchesList.get(8)).pattern = false;
                ((PatchesItemAuto) patchesList.get(8)).marker = BuildConfig.VERSION_NAME;
                ((PatchesItemAuto) patchesList.get(9)).pattern = false;
                ((PatchesItemAuto) patchesList.get(9)).marker = BuildConfig.VERSION_NAME;
                ((PatchesItemAuto) patchesList.get(10)).pattern = false;
                ((PatchesItemAuto) patchesList.get(10)).marker = BuildConfig.VERSION_NAME;
            }
            if (!f3) {
                ((PatchesItemAuto) patchesList.get(2)).pattern = false;
                ((PatchesItemAuto) patchesList.get(3)).pattern = false;
                ((PatchesItemAuto) patchesList.get(4)).pattern = false;
            }
            Utils.sendFromRoot("Parse data for patch.");
            print.println("Parse data for patch.");
            Utils.getMethodsIds(filepatch.getAbsolutePath(), commands, false);
            it3 = commands.iterator();
            while (it3.hasNext()) {
                item2 = (CommandItem) it3.next();
                if (item2.found_index_command) {
                    if (item2.object.equals("Ljava/security/Signature;")) {
                        ((PatchesItemAuto) patchesList.get(11)).origByte[2] = item2.index_command[0];
                        ((PatchesItemAuto) patchesList.get(11)).origByte[3] = item2.index_command[1];
                        ((PatchesItemAuto) patchesList.get(11)).pattern = true;
                    }
                    if (item2.object.equals("Landroid/content/Intent;") && pattern3) {
                        ((PatchesItemAuto) patchesList.get(12)).origByte[2] = item2.index_command[0];
                        ((PatchesItemAuto) patchesList.get(12)).origByte[3] = item2.index_command[1];
                        ((PatchesItemAuto) patchesList.get(12)).pattern = true;
                    }
                }
            }
            if (ev && spi) {
                Utils.getTypesIds(filepatch.getAbsolutePath(), types, false);
                it3 = types.iterator();
                while (it3.hasNext()) {
                    item = (TypesItem) it3.next();
                    if (item.found_id_type && item.type.equals("Ljava/security/SignatureSpi;")) {
                        ((PatchesItemAuto) patchesList.get(15)).origByte[2] = item.id_type[0];
                        ((PatchesItemAuto) patchesList.get(15)).origByte[3] = item.id_type[1];
                        ((PatchesItemAuto) patchesList.get(15)).pattern = true;
                    }
                }
            }
            PatchesItemAuto[] patchList = new PatchesItemAuto[patchesList.size()];
            int u = 0;
            it = patchesList.iterator();
            while (it.hasNext()) {
                patchList[u] = (PatchesItemAuto) it.next();
                u++;
            }
            Utils.sendFromRoot("Set Strings.");
            print.println("Set Strings.");
            ArrayList<byte[]> sitesFromFile = new ArrayList();
            sitesFromFile.add("com.android.vending.billing.InAppBillingService.BIND".getBytes());
            int count = Utils.setStringIds(filepatch.getAbsolutePath(), (byte[][]) sitesFromFile.toArray(new byte[sitesFromFile.size()][]), false, (byte) 76);
            while (count > 0) {
                count--;
                Utils.sendFromRoot("Reworked inapp string!");
                print.println("Reworked inapp string!");
            }
            long time = System.currentTimeMillis();
            FileChannel ChannelDex = new RandomAccessFile(filepatch, InternalZipConstants.WRITE_MODE).getChannel();
            Utils.sendFromRoot("Size file:" + ChannelDex.size());
            MappedByteBuffer fileBytes = ChannelDex.map(MapMode.READ_WRITE, 0, (long) ((int) ChannelDex.size()));
            int start_for_diaposon = 0;
            int start_for_diaposon_pak = 0;
            int start_for_diaposon_ev = 0;
            int period = 0;
            long j = 0;
            while (fileBytes.hasRemaining()) {
                if (!createAPK && fileBytes.position() - period > 149999) {
                    Utils.sendFromRoot("Progress size:" + fileBytes.position());
                    period = fileBytes.position();
                }
                int curentPos = fileBytes.position();
                byte curentByte = fileBytes.get();
                boolean increase = false;
                int b = 0;
                while (b < patchList.length) {
                    int i;
                    byte prufbyte;
                    PatchesItemAuto curpatch;
                    PatchesItemAuto patches = patchList[b];
                    fileBytes.position(curentPos);
                    if (patches.markerTrig && (b == 5 || b == 6 || b == 7 || b == 8 || b == 9 || b == 10)) {
                        if (!increase) {
                            start_for_diaposon_pak++;
                            increase = true;
                        }
                        if (start_for_diaposon_pak < 60) {
                            if (curentByte == patches.origByte[0]) {
                                if (patches.repMask[0] == 0) {
                                    patches.repByte[0] = curentByte;
                                }
                                i = 1;
                                fileBytes.position(curentPos + 1);
                                prufbyte = fileBytes.get();
                                while (true) {
                                    if (prufbyte != patches.origByte[i] && patches.origMask[i] != 1 && patches.origMask[i] != 20 && patches.origMask[i] != 21 && patches.origMask[i] != 23) {
                                        break;
                                    }
                                    if (patches.repMask[i] == 0) {
                                        patches.repByte[i] = prufbyte;
                                    }
                                    if (patches.repMask[i] == 20) {
                                        patches.repByte[i] = (byte) (prufbyte & 15);
                                    }
                                    if (patches.repMask[i] == 21) {
                                        patches.repByte[i] = (byte) ((prufbyte & 15) + 16);
                                    }
                                    i++;
                                    if (i == patches.origByte.length) {
                                        break;
                                    }
                                    prufbyte = fileBytes.get();
                                }
                                fileBytes.position(curentPos);
                                fileBytes.put(patches.repByte);
                                fileBytes.force();
                                Utils.sendFromRoot(patches.resultText);
                                print.println(patches.resultText);
                                patches.result = true;
                                patches.markerTrig = false;
                                it = patchesList.iterator();
                                while (it.hasNext()) {
                                    curpatch = (PatchesItemAuto) it.next();
                                    if (curpatch.marker.equals(patches.marker)) {
                                        curpatch.markerTrig = false;
                                    }
                                }
                                start_for_diaposon_pak = 0;
                                fileBytes.position(curentPos + 1);
                            }
                            fileBytes.position(curentPos + 1);
                        } else {
                            patches.markerTrig = false;
                            it = patchesList.iterator();
                            while (it.hasNext()) {
                                curpatch = (PatchesItemAuto) it.next();
                                if (curpatch.marker.equals(patches.marker)) {
                                    curpatch.markerTrig = false;
                                }
                            }
                            start_for_diaposon_pak = 0;
                            fileBytes.position(curentPos + 1);
                        }
                    }
                    if (patches.markerTrig && (b == 13 || b == 14)) {
                        print.println("search jump");
                        if (!increase) {
                            start_for_diaposon_ev++;
                            increase = true;
                        }
                        if (start_for_diaposon_ev < 90) {
                            if (curentByte == patches.origByte[0]) {
                                if (patches.repMask[0] == 0) {
                                    patches.repByte[0] = curentByte;
                                }
                                i = 1;
                                fileBytes.position(curentPos + 1);
                                prufbyte = fileBytes.get();
                                while (true) {
                                    if (prufbyte != patches.origByte[i] && patches.origMask[i] != 1 && patches.origMask[i] != 20 && patches.origMask[i] != 21 && patches.origMask[i] != 23) {
                                        break;
                                    }
                                    if (patches.repMask[i] == 0) {
                                        patches.repByte[i] = prufbyte;
                                    }
                                    if (patches.repMask[i] == 20) {
                                        patches.repByte[i] = (byte) (prufbyte & 15);
                                    }
                                    if (patches.repMask[i] == 21) {
                                        patches.repByte[i] = (byte) ((prufbyte & 15) + 16);
                                    }
                                    i++;
                                    if (i == patches.origByte.length) {
                                        break;
                                    }
                                    prufbyte = fileBytes.get();
                                }
                                fileBytes.position(curentPos);
                                fileBytes.put(patches.repByte);
                                fileBytes.force();
                                Utils.sendFromRoot(patches.resultText);
                                print.println(patches.resultText);
                                patches.result = true;
                                patches.markerTrig = false;
                                it = patchesList.iterator();
                                while (it.hasNext()) {
                                    curpatch = (PatchesItemAuto) it.next();
                                    if (curpatch.marker.equals(patches.marker)) {
                                        curpatch.markerTrig = false;
                                    }
                                }
                                start_for_diaposon_ev = 0;
                                fileBytes.position(curentPos + 1);
                            }
                            fileBytes.position(curentPos + 1);
                        } else {
                            patches.markerTrig = false;
                            it = patchesList.iterator();
                            while (it.hasNext()) {
                                curpatch = (PatchesItemAuto) it.next();
                                if (curpatch.marker.equals(patches.marker)) {
                                    curpatch.markerTrig = false;
                                }
                            }
                            start_for_diaposon_ev = 0;
                            fileBytes.position(curentPos + 1);
                        }
                    }
                    if (patches.markerTrig && b == 4) {
                        start_for_diaposon++;
                        if (start_for_diaposon < 90) {
                            if (curentByte == patches.origByte[0]) {
                                if (patches.repMask[0] == 0) {
                                    patches.repByte[0] = curentByte;
                                }
                                i = 1;
                                fileBytes.position(curentPos + 1);
                                prufbyte = fileBytes.get();
                                while (true) {
                                    if (prufbyte != patches.origByte[i] && patches.origMask[i] != 1 && patches.origMask[i] != 20 && patches.origMask[i] != 21 && patches.origMask[i] != 23) {
                                        break;
                                    }
                                    if (patches.repMask[i] == 0) {
                                        patches.repByte[i] = prufbyte;
                                    }
                                    if (patches.repMask[i] == 20) {
                                        patches.repByte[i] = (byte) (prufbyte & 15);
                                    }
                                    if (patches.repMask[i] == 21) {
                                        patches.repByte[i] = (byte) ((prufbyte & 15) + 16);
                                    }
                                    i++;
                                    if (i == patches.origByte.length) {
                                        break;
                                    }
                                    prufbyte = fileBytes.get();
                                }
                                fileBytes.position(curentPos);
                                fileBytes.put(patches.repByte);
                                fileBytes.force();
                                Utils.sendFromRoot(patches.resultText);
                                print.println(patches.resultText);
                                patches.result = true;
                                patches.markerTrig = false;
                                it = patchesList.iterator();
                                while (it.hasNext()) {
                                    curpatch = (PatchesItemAuto) it.next();
                                    if (curpatch.marker.equals(patches.marker)) {
                                        curpatch.markerTrig = false;
                                    }
                                }
                                start_for_diaposon = 0;
                                fileBytes.position(curentPos + 1);
                            }
                            fileBytes.position(curentPos + 1);
                        } else {
                            patches.markerTrig = false;
                            it = patchesList.iterator();
                            while (it.hasNext()) {
                                curpatch = (PatchesItemAuto) it.next();
                                if (curpatch.marker.equals(patches.marker)) {
                                    curpatch.markerTrig = false;
                                }
                            }
                            start_for_diaposon = 0;
                            fileBytes.position(curentPos + 1);
                        }
                    }
                    if (!patches.markerTrig && curentByte == patches.origByte[0] && patches.pattern) {
                        if (patches.repMask[0] == 0) {
                            patches.repByte[0] = curentByte;
                        }
                        i = 1;
                        fileBytes.position(curentPos + 1);
                        prufbyte = fileBytes.get();
                        while (true) {
                            if (prufbyte != patches.origByte[i] && patches.origMask[i] != 1 && patches.origMask[i] != 20 && patches.origMask[i] != 21 && patches.origMask[i] != 23) {
                                break;
                            }
                            if (patches.repMask[i] == 0) {
                                patches.repByte[i] = prufbyte;
                            }
                            if (patches.repMask[i] == 20) {
                                patches.repByte[i] = (byte) (prufbyte & 15);
                            }
                            if (patches.repMask[i] == 21) {
                                patches.repByte[i] = (byte) ((prufbyte & 15) + 16);
                            }
                            i++;
                            if (i == patches.origByte.length) {
                                break;
                            }
                            prufbyte = fileBytes.get();
                        }
                        fileBytes.position(curentPos);
                        fileBytes.put(patches.repByte);
                        fileBytes.force();
                        Utils.sendFromRoot(patches.resultText);
                        print.println(patches.resultText);
                        patches.result = true;
                        if (!patches.marker.equals(BuildConfig.VERSION_NAME)) {
                            patches.markerTrig = true;
                            it = patchesList.iterator();
                            while (it.hasNext()) {
                                curpatch = (PatchesItemAuto) it.next();
                                if (curpatch.marker.equals(patches.marker)) {
                                    curpatch.markerTrig = true;
                                }
                            }
                        }
                        fileBytes.position(curentPos + 1);
                    }
                    b++;
                }
                fileBytes.position(curentPos + 1);
                j++;
            }
            ChannelDex.close();
            Utils.sendFromRoot(BuildConfig.VERSION_NAME + ((System.currentTimeMillis() - time) / 1000));
            Utils.sendFromRoot("Analise Results:");
        }
        it = filestopatch.iterator();
        while (it.hasNext()) {
            Utils.fixadler((File) it.next());
            clearTempSD();
        }
        if (!createAPK) {
            Utils.sendFromRoot("Create ODEX:");
            int r = Utils.create_ODEX_root(paramArrayOfString[3], classesFiles, paramArrayOfString[2], uid, Utils.getOdexForCreate(paramArrayOfString[2], uid));
            Utils.sendFromRoot("chelpus_return_" + r);
            if (r == 0 && !ART) {
                Utils.afterPatch(paramArrayOfString[1], paramArrayOfString[2], Utils.getPlaceForOdex(paramArrayOfString[2], true), uid, paramArrayOfString[3]);
            }
        }
        Utils.sendFromRoot("Optional Steps After Patch:");
        if (!createAPK) {
            Utils.exitFromRootJava();
        }
        result = logOutputStream.allresult;
    }

    public static void fixadler(File destFile) {
        try {
            FileInputStream localFileInputStream = new FileInputStream(destFile);
            byte[] arrayOfByte = new byte[localFileInputStream.available()];
            localFileInputStream.read(arrayOfByte);
            calcSignature(arrayOfByte, 0);
            calcChecksum(arrayOfByte, 0);
            localFileInputStream.close();
            FileOutputStream localFileOutputStream = new FileOutputStream(destFile);
            localFileOutputStream.write(arrayOfByte);
            localFileOutputStream.close();
        } catch (Exception localException) {
            localException.printStackTrace();
        }
    }

    private static final void calcChecksum(byte[] paramArrayOfByte, int j) {
        Adler32 localAdler32 = new Adler32();
        localAdler32.update(paramArrayOfByte, 12, paramArrayOfByte.length - (j + 12));
        int i = (int) localAdler32.getValue();
        paramArrayOfByte[j + 8] = (byte) i;
        paramArrayOfByte[j + 9] = (byte) (i >> 8);
        paramArrayOfByte[j + 10] = (byte) (i >> 16);
        paramArrayOfByte[j + 11] = (byte) (i >> 24);
    }

    private static final void calcSignature(byte[] paramArrayOfByte, int j) {
        try {
            MessageDigest localMessageDigest = MessageDigest.getInstance("SHA-1");
            localMessageDigest.update(paramArrayOfByte, 32, paramArrayOfByte.length - (j + 32));
            try {
                int i = localMessageDigest.digest(paramArrayOfByte, j + 12, 20);
                if (i != 20) {
                    throw new RuntimeException("unexpected digest write:" + i + "bytes");
                }
            } catch (DigestException localDigestException) {
                throw new RuntimeException(localDigestException);
            }
        } catch (NoSuchAlgorithmException localNoSuchAlgorithmException) {
            throw new RuntimeException(localNoSuchAlgorithmException);
        }
    }

    public static void clearTemp() {
        try {
            File tempdex = new File(dir + "/AndroidManifest.xml");
            if (tempdex.exists()) {
                tempdex.delete();
            }
            if (classesFiles != null && classesFiles.size() > 0) {
                Iterator it = classesFiles.iterator();
                while (it.hasNext()) {
                    File cl = (File) it.next();
                    if (cl.exists()) {
                        cl.delete();
                    }
                }
            }
            tempdex = new File(dir + "/classes.dex");
            if (tempdex.exists()) {
                tempdex.delete();
            }
            tempdex = new File(dir + "/classes.dex.apk");
            if (tempdex.exists()) {
                tempdex.delete();
            }
        } catch (Exception e) {
            Utils.sendFromRoot(BuildConfig.VERSION_NAME + e.toString());
        }
    }

    public static boolean byteverify(MappedByteBuffer fileBytes, int curentPos, byte curentByte, byte[] byteOrig, byte[] mask, byte[] byteReplace, byte[] rep_mask, String log, boolean pattern) {
        if (curentByte == byteOrig[0] && pattern) {
            if (rep_mask[0] == (byte) 0) {
                byteReplace[0] = curentByte;
            }
            int i = 1;
            fileBytes.position(curentPos + 1);
            byte prufbyte = fileBytes.get();
            while (true) {
                if (prufbyte != byteOrig[i] && mask[i] != (byte) 1) {
                    break;
                }
                if (rep_mask[i] == (byte) 0) {
                    byteReplace[i] = prufbyte;
                }
                i++;
                if (i == byteOrig.length) {
                    fileBytes.position(curentPos);
                    fileBytes.put(byteReplace);
                    fileBytes.force();
                    Utils.sendFromRoot(log);
                    return true;
                }
                prufbyte = fileBytes.get();
            }
            fileBytes.position(curentPos + 1);
        }
        return false;
    }

    public static void unzipSD(File apk) {
        try {
            FileInputStream fin = new FileInputStream(apk);
            ZipInputStream zin = new ZipInputStream(fin);
            boolean classesdex = false;
            while (true) {
                ZipEntry ze = zin.getNextEntry();
                if (ze != null) {
                    FileOutputStream fout;
                    byte[] buffer;
                    int length;
                    if (ze.getName().toLowerCase().startsWith("classes") && ze.getName().endsWith(".dex") && !ze.getName().contains(InternalZipConstants.ZIP_FILE_SEPARATOR)) {
                        fout = new FileOutputStream(sddir + "/Modified/" + ze.getName());
                        buffer = new byte[Util.BLOCK_HEADER_SIZE_MAX];
                        while (true) {
                            length = zin.read(buffer);
                            if (length == -1) {
                                break;
                            }
                            fout.write(buffer, 0, length);
                        }
                        classesFiles.add(new File(sddir + "/Modified/" + ze.getName()));
                        classesdex = true;
                        if (!createAPK) {
                            zin.closeEntry();
                            fout.close();
                        }
                    }
                    if (ze.getName().equals("AndroidManifest.xml")) {
                        fout = new FileOutputStream(sddir + "/Modified/" + "AndroidManifest.xml");
                        buffer = new byte[Util.BLOCK_HEADER_SIZE_MAX];
                        while (true) {
                            length = zin.read(buffer);
                            if (length == -1) {
                                break;
                            }
                            fout.write(buffer, 0, length);
                        }
                        if (classesdex) {
                            zin.closeEntry();
                            fout.close();
                        }
                    }
                } else {
                    zin.close();
                    fin.close();
                    return;
                }
            }
        } catch (Exception e) {
            try {
                ZipFile zipFile = new ZipFile(apk);
                zipFile.extractFile("classes.dex", sddir + "/Modified/");
                classesFiles.add(new File(sddir + "/Modified/" + "classes.dex"));
                zipFile.extractFile("AndroidManifest.xml", sddir + "/Modified/");
            } catch (ZipException e1) {
                Utils.sendFromRoot("Error classes.dex decompress! " + e1);
                Utils.sendFromRoot("Exception e1" + e.toString());
            } catch (Exception e12) {
                Utils.sendFromRoot("Error classes.dex decompress! " + e12);
                Utils.sendFromRoot("Exception e1" + e.toString());
            }
            Utils.sendFromRoot("Decompressunzip " + e);
        }
    }

    public static void unzipART(File apk) {
        boolean found2 = false;
        try {
            FileInputStream fin = new FileInputStream(apk);
            ZipInputStream zipInputStream = new ZipInputStream(fin);
            for (ZipEntry ze = zipInputStream.getNextEntry(); ze != null && true; ze = zipInputStream.getNextEntry()) {
                String haystack = ze.getName();
                if (haystack.toLowerCase().startsWith("classes") && haystack.endsWith(".dex") && !haystack.contains(InternalZipConstants.ZIP_FILE_SEPARATOR)) {
                    FileOutputStream fout = new FileOutputStream(sddir + InternalZipConstants.ZIP_FILE_SEPARATOR + haystack);
                    byte[] buffer = new byte[InternalZipConstants.UFT8_NAMES_FLAG];
                    while (true) {
                        int length = zipInputStream.read(buffer);
                        if (length == -1) {
                            break;
                        }
                        fout.write(buffer, 0, length);
                    }
                    zipInputStream.closeEntry();
                    fout.close();
                    classesFiles.add(new File(sddir + InternalZipConstants.ZIP_FILE_SEPARATOR + haystack));
                    Utils.cmdParam("chmod", "777", sddir + InternalZipConstants.ZIP_FILE_SEPARATOR + haystack);
                }
                if (haystack.equals("AndroidManifest.xml")) {
                    FileOutputStream fout2 = new FileOutputStream(sddir + InternalZipConstants.ZIP_FILE_SEPARATOR + "AndroidManifest.xml");
                    byte[] buffer2 = new byte[InternalZipConstants.UFT8_NAMES_FLAG];
                    while (true) {
                        int length2 = zipInputStream.read(buffer2);
                        if (length2 == -1) {
                            break;
                        }
                        fout2.write(buffer2, 0, length2);
                    }
                    zipInputStream.closeEntry();
                    fout2.close();
                    Utils.cmdParam("chmod", "777", sddir + InternalZipConstants.ZIP_FILE_SEPARATOR + "AndroidManifest.xml");
                    found2 = true;
                }
                if (false && found2) {
                    break;
                }
            }
            zipInputStream.close();
            fin.close();
        } catch (Exception e) {
            try {
                ZipFile zipFile = new ZipFile(apk);
                zipFile.extractFile("classes.dex", sddir);
                classesFiles.add(new File(sddir + InternalZipConstants.ZIP_FILE_SEPARATOR + "classes.dex"));
                Utils.cmdParam("chmod", "777", sddir + InternalZipConstants.ZIP_FILE_SEPARATOR + "classes.dex");
                zipFile.extractFile("AndroidManifest.xml", sddir);
                Utils.cmdParam("chmod", "777", sddir + InternalZipConstants.ZIP_FILE_SEPARATOR + "AndroidManifest.xml");
            } catch (ZipException e1) {
                Utils.sendFromRoot("Error classes.dex decompress! " + e1);
                Utils.sendFromRoot("Exception e1" + e.toString());
            } catch (Exception e12) {
                Utils.sendFromRoot("Error classes.dex decompress! " + e12);
                Utils.sendFromRoot("Exception e1" + e.toString());
            }
            Utils.sendFromRoot("Exception e" + e.toString());
        }
    }

    public static void clearTempSD() {
        try {
            File tempdex = new File(sddir + "/Modified/classes.dex.apk");
            if (tempdex.exists()) {
                tempdex.delete();
            }
        } catch (Exception e) {
            Utils.sendFromRoot(BuildConfig.VERSION_NAME + e.toString());
        }
    }
}
